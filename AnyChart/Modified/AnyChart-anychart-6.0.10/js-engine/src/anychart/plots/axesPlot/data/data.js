/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.axesPlot.data.AxesPlotPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.data.AxesPlotSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.data.SingleValueSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.data.SingleValuePoint}</li>
 *  <li>@class {anychart.plots.axesPlot.data.StackablePoint}</li>
 *  <li>@class {anychart.plots.axesPlot.data.RangeStackablePoint}</li>
 *  <li>@class {anychart.plots.axesPlot.data.RangePoint}</li>
 *  <li>@class {anychart.plots.axesPlot.data.RangeSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.data.RangeGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.data');

goog.require('anychart.plots.seriesPlot.data');
goog.require('anychart.utils');
//-----------------------------------------------------------------------------------
//
//                          AxesGlobalSeriesSettings class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings = function() {
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.apply(this);
};

goog.inherits(anychart.plots.axesPlot.data.AxesGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings);
//-----------------------------------------------------------------------------------
//                          Properties
//-----------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.prototype.pointScatterWidth_ = .05;
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.prototype.getPointScatterWidth = function() {
    return this.pointScatterWidth_;
};
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.prototype.isPercentPointScatterWidth_ = true;
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.prototype.isPercentPointScatterWidth = function() {
    return this.isPercentPointScatterWidth_;
};
//-----------------------------------------------------------------------------------
//                          Deserialization
//-----------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.prototype.deserialize = function(config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'scatter_point_width')) {
        this.pointScatterWidth_ = des.getProp(config, 'scatter_point_width');
        this.isPercentPointScatterWidth_ = des.isPercent(this.pointScatterWidth_);
        this.pointScatterWidth_ = this.isPercentPointScatterWidth_ ? des.getPercent(this.pointScatterWidth_) : des.getNumber(this.pointScatterWidth_);
    }
};
//------------------------------------------------------------------------------
//
//                          AxesPlotPoint class
//
//------------------------------------------------------------------------------

/**
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.axesPlot.data.AxesPlotPoint = function() {
    anychart.plots.seriesPlot.data.BasePoint.apply(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.data.AxesPlotPoint, anychart.plots.seriesPlot.data.BasePoint);

/**
 * @protected
 * @type {Number}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.x_ = NaN;
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getX = function() {
    return this.x_;
};

/**
 * @protected
 * @type {anychart.plots.axesPlot.axes.categorization.Category}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.category_ = null;
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getCategory = function() {
    return this.category_;
};


/**
 * @protected
 * @type {anychart.plots.axesPlot.axes.categorization.Category}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.dataCategory_ = null;

/**
 * @protected
 * @type {int}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.clusterIndex_ = 0;
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getClusterIndex = function() {
    return this.clusterIndex_;
};

/**
 * @protected
 * @type {anychart.plots.axesPlot.axes.categorization.ClusterSettings}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.clusterSettings_ = null;
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getClusterSettings = function() {
    return this.clusterSettings_;
};

/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.isHorizontal_ = false;

/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.isInverted_ = false;

/**
 * @public
 * @param {Object} data
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.deserialize = function(data, svgManager) {
    this.checkMissing_(data);

    goog.base(this, 'deserialize', data);

    this.deserializeArgument_(data, svgManager);

    if (!this.isMissing())
        this.deserializeValue_(data);

    if (!this.isMissing() && this.dataCategory_) {
        this.dataCategory_.checkPoint(this);
        if (this.dataCategory_ != this.category_)
            this.category_.checkPoint(this);
    }

    this.series_.getArgumentAxis().getPoints().push(this);
    this.series_.getValueAxis().getPoints().push(this);
};
/**
 * Не спрашивайте нахуя тут svg manager...
 * @protected
 * @param {Object} data
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.deserializeArgument_ = function(data, svgManager) {
    var argumentAxis = this.series_.getArgumentAxis();

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'x'))
        this.x_ = argumentAxis.deserializeValue(des.getProp(data, 'x'));

    this.isHorizontal_ = argumentAxis.getPosition() == anychart.layout.Position.LEFT ||
        argumentAxis.getPosition() == anychart.layout.Position.RIGHT;

    var argumentAxisType = argumentAxis.getType();

    if (argumentAxisType == anychart.plots.axesPlot.axes.AxisType.VALUE) {
        if (des.hasProp(data, 'name') && isNaN(this.x_))
            this.x_ = argumentAxis.deserializeValue(des.getProp(data, 'name'));

        this.series_.getArgumentAxis().checkScaleValue(this.x_);
    } else {
        if (argumentAxisType == anychart.plots.axesPlot.axes.AxisType.CATEGORIZED) {
            this.dataCategory_ = this.category_ = argumentAxis.getCategory(this.name_, svgManager);
            this.x_ = this.category_.getIndex();
            this.clusterSettings_ = this.series_.getClusterSettings();
            this.paletteIndex_ = this.category_.getIndex();
            this.index_ = this.category_.getIndex();
        } else if (argumentAxisType == anychart.plots.axesPlot.axes.AxisType.CATEGORIZED_BY_SERIES) {
            this.category_ = this.series_.getCategory();
            this.dataCategory_ = this.globalSettings_.getPlot().getDataCategory(argumentAxis, this.name_);
            this.x_ = this.series_.getCategory().getIndex();
            if (this.series_.isSingleCluster()) {
                this.clusterIndex_ = 0;
            } else {
                this.getCategory().getClusterSettings().setNumClusters(this.getCategory().getClusterSettings().getNumClusters() + 1);
                this.clusterIndex_ = this.index_;
            }
            this.clusterSettings_ = this.getCategory().getClusterSettings();
        } else {
            throw new Error('Unknown argument axis type');
        }

        var valueAxis = this.series_.getValueAxis();

        if (this.series_.isSortable() && valueAxis.getScale().getMode() == anychart.scales.ScaleMode.SORTED_OVERLAY)
            this.category_.getSortedOverlay(valueAxis.getName(), this.series_.getType()).push(this);
    }
};

/**
 * @protected
 * @param {Object} data
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.deserializeValue_ = function(data) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.data.AxesPlotPoint.prototype.deserializeValueFromConfig_ = function(value) {
    return this.series_.getValueAxis().deserializeValue(value);
};


/**
 * @protected
 * @param {Object} data
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.checkMissing_ = function(data) {
    goog.abstractMethod();
};

/** @inheritDoc */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.initializeAfterInterpolate = function() {
    goog.base(this, 'initializeAfterInterpolate');

    if (this.dataCategory_)
        this.dataCategory_.checkPoint(this);
    if (this.dataCategory_ != null && this.category_ != null && this.dataCategory_ != this.category_)
        this.category_.checkPoint(this);
};

/** @inheritDoc */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.setAnchorPoint = function(point, anchor, bounds, padding) {
    var axis = this.series_.getArgumentAxis();
    if (anchor == anychart.layout.Anchor.X_AXIS)
        axis.setElementPosition(this, bounds, padding, point);
    else {
        anchor = this.checkAnchorOnInvert(anchor, axis);
        goog.base(this, 'setAnchorPoint', point, anchor, bounds, padding);
    }
};

anychart.plots.axesPlot.data.AxesPlotPoint.prototype.checkAnchorOnInvert = function(anchor, axis) {
    if (this.isInverted_)
        return  axis.getViewPort().getLayoutViewPort().getInvertedAnchor(anchor);
    else
        return  axis.getViewPort().getLayoutViewPort().getNormalAnchor(anchor);
};

/** @inheritDoc */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.setElementHorizontalAlign = function(point, anchor, hAlign, vAlign, bounds, padding) {
    if (anchor != anychart.layout.Anchor.FLOAT) {
        var axis = this.series_.getArgumentAxis();
        if (this.isInverted_)
            hAlign = axis.getViewPort().getLayoutViewPort().getInvertedHAlign(hAlign, vAlign);
        else
            hAlign = axis.getViewPort().getLayoutViewPort().getNormalHAlign(hAlign, vAlign);
    }
    goog.base(this, 'setElementHorizontalAlign', point, anchor, hAlign, vAlign, bounds, padding);
};

/** @inheritDoc */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.setElementVerticalAlign = function(point, anchor, hAlign, vAlign, bounds, padding) {
    if (anchor != anychart.layout.Anchor.FLOAT) {
        var axis = this.series_.getArgumentAxis();
        if (this.isInverted_)
            vAlign = axis.getViewPort().getLayoutViewPort().getInvertedVAlign(hAlign, vAlign);
        else
            vAlign = axis.getViewPort().getLayoutViewPort().getNormalVAlign(hAlign, vAlign);
    }
    goog.base(this, 'setElementVerticalAlign', point, anchor, hAlign, vAlign, bounds, padding);
};

/**
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.checkElementPosition = function(element) {
    if (element.getStyle().getNormal().getAnchor() == anychart.layout.Anchor.X_AXIS)
        this.series_.getArgumentAxis().addPointAxisElement(this, element);
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.resizeLabel = function(label, labelSprite) {
    if (labelSprite&&
        label&&
        label.getStyle() &&
        label.getStyle().getNormal() &&
        label.getStyle().getNormal().getAnchor() == anychart.layout.Anchor.X_AXIS)
    this.getPlot().getLabelsSprite(label).appendSprite(labelSprite);

    goog.base(this, 'resizeLabel', label, labelSprite);

};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.checkCategory = function(category) {
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%XValue', 0);
    this.tokensHash_.add('%XPercentOfSeries', 0);
    this.tokensHash_.add('%XPercentOfTotal', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getTokenType = function(token) {
    if (this.tokensHash_.isCategoryToken(token) && this.dataCategory_)
        return this.dataCategory_.getTokenType(token);

    if (this.tokensHash_.isYAxisToken(token))
        return this.series_.getValueAxis().getTokenType(this.tokensHash_.getAxisToken(token));

    if (this.tokensHash_.isXAxisToken(token))
        return this.series_.getArgumentAxis().getTokenType(this.tokensHash_.getAxisToken(token));

    if (this.series_.getArgumentAxis().isDateTimeAxis())
        if (token == '%XValue' || token == '%Name')
            return anychart.formatting.TextFormatTokenType.DATE;


    switch (token) {
        case '%XValue':
        case '%XPercentOfSeries':
        case '%XPercentOfTotal':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return  goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.calculatePointTokenValues = function() {
    goog.base(this, 'calculatePointTokenValues');
    if (this.x_) this.tokensHash_.add('%XValue', this.x_);
};
//------------------------------------------------------------------------------
//                    Formatting token value getters
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.getTokenValue = function(token) {
    if (!this.tokensHash_.has('%XPercentOfSeries'))
        this.tokensHash_.add(
            '%XPercentOfSeries',
            this.x_ / this.series_.getSeriesTokenValue('%SeriesXSum') * 100);

    if (!this.tokensHash_.has('%XPercentOfTotal'))
        this.tokensHash_.add(
            '%XPercentOfTotal',
            this.x_ / this.getPlot().getTokenValue('%DataPlotXSum') * 100);

    if (this.tokensHash_.isCategoryToken(token) && this.dataCategory_)
        return this.dataCategory_.getTokenValue(token);

    if (this.tokensHash_.isYAxisToken(token))
        return this.series_.getValueAxis().getTokenValue(
            this.tokensHash_.getAxisToken(token));

    if (this.tokensHash_.isXAxisToken(token))
        return this.series_.getArgumentAxis().getTokenValue(
            this.tokensHash_.getAxisToken(token));

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//                    Serialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesPlotPoint.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['XValue'] = this.x_;

    opt_target['XPercentOfSeries'] = this.tokensHash_.get("%XPercentOfSeries");
    opt_target['XPercentOfTotal'] = this.tokensHash_.get("%XPercentOfTotal");
    opt_target['XPercentOfCategory'] = this.tokensHash_.get("%XPercentOfCategory");

    opt_target['Category'] = this.dataCategory_ ? this.dataCategory_.getName() : null;

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          AxesPlotSeries class
//
//------------------------------------------------------------------------------
/**
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.axesPlot.data.AxesPlotSeries = function() {
    anychart.plots.seriesPlot.data.BaseSeries.call(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.data.AxesPlotSeries, anychart.plots.seriesPlot.data.BaseSeries);

/**
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.argumentAxis_ = null;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getArgumentAxis = function() {
    return this.argumentAxis_;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setArgumentAxis = function(value) {
    this.argumentAxis_ = value;
};

/**
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.valueAxis_ = null;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getValueAxis = function() {
    return this.valueAxis_;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setValueAxis = function(value) {
    this.valueAxis_ = value;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.category_ = null;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setCategory = function(value) {
    this.category_ = value;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getCategory = function() {
    return this.category_;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.clusterIndex_ = null;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setClusterIndex = function(value) {
    this.clusterIndex_ = value;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getClusterIndex = function() {
    return this.clusterIndex_;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.isSingleCluster_ = false;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setIsSingleCluster = function(value) {
    this.isSingleCluster_ = true;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.isSingleCluster = function() {
    return this.isSingleCluster_;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.clusterKey_ = NaN;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getClusterKey = function() {
    return this.clusterKey_;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setClusterKey = function(value) {
    this.clusterKey_ = value;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.isSortable_ = null;

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return true;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.clusterContainsOnlyOnePoint = function() {
    return false;
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.clusterSettings_ = null;
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setClusterSettings = function(value) {
    this.clusterSettings_ = value;
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getClusterSettings = function() {
    return this.clusterSettings_;
};

/**
 * @public
 * @param {anychart.scales.BaseScale} scale
 */
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.configureValueScale = function(scale) {
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesFirstXValue', 0);
    this.tokensHash_.add('%SeriesLastXValue', 0);
    this.tokensHash_.add('%SeriesXSum', 0);
    this.tokensHash_.add('%SeriesXMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesXMin', Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesXAverage', 0);
    this.tokensHash_.add('%SeriesYAxisName', '');
    this.tokensHash_.add('%SeriesXAxisName', '');

};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.getTokenType = function(token) {
    switch (token) {
        case '%SeriesFirstXValue':
        case '%SeriesLastXValue':
        case '%SeriesXSum':
        case '%SeriesXMin':
        case '%SeriesXMax':
        case '%SeriesXAverage' :
            return this.argumentAxis_.isDateTimeAxis() ?
                anychart.formatting.TextFormatTokenType.DATE :
                anychart.formatting.TextFormatTokenType.NUMBER;

        case '%SeriesYAxisName' :
        case '%SeriesXAxisName' :
            return anychart.formatting.TextFormatTokenType.TEXT;
    }
    return goog.base(this, 'getTokenType', token)
};
//------------------------------------------------------------------------------
//                          Formatting value calculation
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.checkPlot = function(plot) {
    goog.base(this, 'checkPlot', plot);
    var plotTokenHash = plot.getTokensHash();

    plotTokenHash.increment('%DataPlotXSum', this.tokensHash_.get('%SeriesXSum'));

    if (this.tokensHash_.get('%SeriesXMax') > plotTokenHash.get('%DataPlotXMax'))
        plotTokenHash.add('%DataPlotXMax', this.tokensHash_.get('%SeriesXMax'));

    if (this.tokensHash_.get('%SeriesXMin') < plotTokenHash.get('%DataPlotXMin'))
        plotTokenHash.add('%DataPlotXMin', this.tokensHash_.get('%SeriesXMin'));
    this.checkAxes();
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.checkPoint = function(dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);
    var pointX = dataPoint.getX();
    var cashedMax = Number(this.tokensHash_.get('%SeriesXMax'));
    var cashedMin = Number(this.tokensHash_.get('%SeriesXMin'));

    this.tokensHash_.increment('%SeriesXSum', pointX);
    if (pointX > cashedMax) this.tokensHash_.add('%SeriesXMax', pointX);
    if (pointX < cashedMin) this.tokensHash_.add('%SeriesXMin', pointX);
};

anychart.plots.axesPlot.data.AxesPlotSeries.prototype.checkAxes = function() {
    var tokensHash = this.getArgumentAxis().getTokensHash();

    tokensHash.increment('%AxisPointCount', this.points_.length);
    tokensHash.increment('%AxisSum', this.tokensHash_.get('%SeriesXSum'));

    if (this.tokensHash_.get('%SeriesXMax') > tokensHash.get('%AxisMax'))
        tokensHash.add('%AxisMax', this.tokensHash_.get('%SeriesXMax'));

    if (this.tokensHash_.get('%SeriesXMin') < tokensHash.get('%AxisMin'))
        tokensHash.add('%AxisMin', this.tokensHash_.get('%SeriesXMin'));

    this.getArgumentAxis().calculateAxisTokenValue();
};


anychart.plots.axesPlot.data.AxesPlotSeries.prototype.calculateSeriesTokenValues = function() {
    goog.base(this, 'calculateSeriesTokenValues');

    this.tokensHash_.add('%SeriesYAxisName', this.getValueAxis().getName());
    this.tokensHash_.add('%SeriesXAxisName', this.getArgumentAxis().getName());

    if (this.points_.length > 0 && this.getPointAt(0))
        this.tokensHash_.add('%SeriesFirstXValue', this.getPointAt(0).getX());

    if (this.points_.length > 0 && this.getPointAt(this.points_.length - 1))
        this.tokensHash_.add(
            '%SeriesLastXValue',
            this.getPointAt(this.points_.length - 1).getX());

    this.tokensHash_.add(
        '%SeriesXAverage',
        this.tokensHash_.get('%SeriesXSum') / this.points_.length);
};
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.recalculateSeriesTokenValues = function() {
    goog.base(this, 'recalculateSeriesTokenValues');
};
//------------------------------------------------------------------------------
//                    Serialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.AxesPlotSeries.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);

    opt_target['StartMarkerType'] = opt_target['EndMarkerType'] = this.markerType_;
    opt_target['FirstXValue'] = this.tokensHash_.get("%SeriesFirstXValue");
    opt_target['LastXValue'] = this.tokensHash_.get("%SeriesLastXValue");
    opt_target['XSum'] = this.tokensHash_.get('%SeriesXSum');
    opt_target['XMax'] = this.tokensHash_.get('%SeriesXMax');
    opt_target['XMin'] = this.tokensHash_.get('%SeriesXMin');
    opt_target['XAverage'] = this.tokensHash_.get("%SeriesXAverage");
    opt_target['XMedian'] = this.tokensHash_.get("%SeriesXMedian");
    opt_target['XMode'] = this.tokensHash_.get("%SeriesXMode");

    if (this.valueAxis_) opt_target['YAxis'] = this.valueAxis_.getName();
    if (this.argumentAxis_) opt_target['XAxis'] = this.argumentAxis_.getName();

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          SingleValueSeries class
//
//------------------------------------------------------------------------------

/**
 * @public
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesPlotSeries}
 */
anychart.plots.axesPlot.data.SingleValueSeries = function() {
    anychart.plots.axesPlot.data.AxesPlotSeries.apply(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.data.SingleValueSeries, anychart.plots.axesPlot.data.AxesPlotSeries);
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.SingleValueSeries.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesLastYValue', 0);
    this.tokensHash_.add('%SeriesYSum', 0);
    this.tokensHash_.add('%Value', 0);
    this.tokensHash_.add('%SeriesValue', 0);
    this.tokensHash_.add('%SeriesYMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYMin', Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYAverage', 0);

    this.tokensHash_.add('%MaxYValuePointName', '');
    this.tokensHash_.add('%MinYValuePointName', '');
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.SingleValueSeries.prototype.getTokenType = function(token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenType(token);

    switch (token) {
        case '%SeriesLastYValue':
        case '%SeriesFirstYValue':
        case '%SeriesYSum':
        case '%Value':
        case '%SeriesValue':
        case '%SeriesYMax':
        case '%SeriesYMin':
        case '%SeriesYAverage' :
            return this.valueAxis_.isDateTimeAxis() ?
                anychart.formatting.TextFormatTokenType.DATE :
                anychart.formatting.TextFormatTokenType.NUMBER;
        case '%MaxYValuePointName' :
        case '%MinYValuePointName' :
            return anychart.formatting.TextFormatTokenType.TEXT;
    }

    return goog.base(this, 'getTokenType', token)
};
//------------------------------------------------------------------------------
//                          Formatting value calculation
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.SingleValueSeries.prototype.checkPlot = function(plot) {
    goog.base(this, 'checkPlot', plot);
    var plotTokensCash = plot.getTokensHash();

    plotTokensCash.increment('%DataPlotYSum', this.tokensHash_.get('%SeriesYSum'));
    plotTokensCash.increment('%YBasedPointsCount', this.points_.length);


    if (this.tokensHash_.get('%SeriesYMax') > plotTokensCash.get('%DataPlotYMax')) {
        plotTokensCash.add('%DataPlotYMax', this.tokensHash_.get('%SeriesYMax'));
        plotTokensCash.add('%DataPlotMaxYValuePointName', this.tokensHash_.get('%SeriesMaxYValuePointName'));
        plotTokensCash.add('%DataPlotMaxYValuePointSeriesName', this.name_);
    }
    if (this.tokensHash_.get('%SeriesYMin') < plotTokensCash.get('%DataPlotYMin')) {
        plotTokensCash.add('%DataPlotYMin', this.tokensHash_.get('%SeriesYMin'));
        plotTokensCash.add('%DataPlotMinYValuePointName', this.tokensHash_.get('%SeriesMinYValuePointName'));
        plotTokensCash.add('%DataPlotMinYValuePointSeriesName', this.name_);
    }
    if (this.tokensHash_.get('%SeriesYSum') > plotTokensCash.get('%DataPlotMaxYSumSeries')) {
        plotTokensCash.add('%DataPlotMaxYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensCash.add('%DataPlotMaxYSumSeriesName', this.name_);
    }
    if (this.tokensHash_.get('%SeriesYSum') > plotTokensCash.get('%DataPlotMinYSumSeries')) {
        plotTokensCash.add('%DataPlotMinYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensCash.add('%DataPlotMinYSumSeriesName', this.name_);
    }
};
anychart.plots.axesPlot.data.SingleValueSeries.prototype.checkPoint = function(dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);
    this.tokensHash_.increment('%SeriesYSum', dataPoint.getY());
    this.tokensHash_.increment('%Value', dataPoint.getY());
    this.tokensHash_.increment('%SeriesValue', dataPoint.getY());

    if (dataPoint.getY() > this.tokensHash_.get('%SeriesYMax')) {
        this.tokensHash_.add('%SeriesYMax', dataPoint.getY());
        this.tokensHash_.add('%SeriesMaxYValuePointName', dataPoint.getName());
    }
    if (dataPoint.getY() < this.tokensHash_.get('%SeriesYMin')) {
        this.tokensHash_.add('%SeriesYMin', dataPoint.getY());
        this.tokensHash_.add('%SeriesMinYValuePointName', dataPoint.getName());
    }
};

anychart.plots.axesPlot.data.SingleValueSeries.prototype.checkAxes = function() {
    goog.base(this, 'checkAxes');
    var cashedTokens = this.getValueAxis().getTokensHash();

    cashedTokens.increment('%AxisPointCount', this.points_.length);
    cashedTokens.increment('%AxisSum', this.tokensHash_.get('%SeriesYSum'));

    if (this.tokensHash_.get('%SeriesYMax') > cashedTokens.get('%AxisMax'))
        cashedTokens.add('%AxisMax', this.tokensHash_.get('%SeriesYMax'));

    if (this.tokensHash_.get('%SeriesYMin') < cashedTokens.get('%AxisMin'))
        cashedTokens.add('%AxisMin', this.tokensHash_.get('%SeriesYMin'));

    this.getValueAxis().calculateAxisTokenValue();
};

anychart.plots.axesPlot.data.SingleValueSeries.prototype.calculateSeriesTokenValues = function() {
    goog.base(this, 'calculateSeriesTokenValues');

    this.tokensHash_.add(
        '%SeriesYAverage',
        this.tokensHash_.get('%SeriesYSum') / this.points_.length);

    if (this.points_ && this.getPointAt(0))
        this.tokensHash_.add('%SeriesFirstYValue', this.getPointAt(0).getY());

    if (this.points_ && this.getPointAt(this.points_.length - 1))
        this.tokensHash_.add('%SeriesLastYValue',
            this.getPointAt(this.points_.length - 1).getY());
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.SingleValueSeries.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['FirstYValue'] = this.tokensHash_.get('%SeriesFirstYValue');
    opt_target['LastYValue'] = this.tokensHash_.get('%SeriesLastYValue');
    opt_target['YSum'] = this.tokensHash_.get('%SeriesYSum');
    opt_target['YMax'] = this.tokensHash_.get('%SeriesYMax');
    opt_target['YMin'] = this.tokensHash_.get('%SeriesYMin');
    opt_target['YAverage'] = this.tokensHash_.get('%SeriesYAverage');
    opt_target['YMedian'] = this.tokensHash_.get('%SeriesYMedian');
    opt_target['YMode'] = this.tokensHash_.get('%SeriesYMode');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          SingleValuePoint class
//
//------------------------------------------------------------------------------
/**
 * @public
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesPlotPoint}
 */
anychart.plots.axesPlot.data.SingleValuePoint = function() {
    anychart.plots.axesPlot.data.AxesPlotPoint.apply(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.data.SingleValuePoint, anychart.plots.axesPlot.data.AxesPlotPoint);

anychart.plots.axesPlot.data.SingleValuePoint.prototype.y_ = null;
anychart.plots.axesPlot.data.SingleValuePoint.prototype.getY = function() {
    return this.y_;
};

/**
 * @public
 * @override
 * @param {Object} data
 */
anychart.plots.axesPlot.data.SingleValuePoint.prototype.checkMissing_ = function(data) {
    if (!anychart.utils.deserialization.hasProp(data, 'y')) {
        this.isMissing_ = true;
        return;
    }
    if (this.series_.getValueAxis().getType() == anychart.plots.axesPlot.axes.AxisType.VALUE)
        this.isMissing_ = isNaN(this.deserializeValueFromConfig_(anychart.utils.deserialization.getProp(data, 'y')));
};

/**
 * @public
 * @override
 * @param {Object} data
 */
anychart.plots.axesPlot.data.SingleValuePoint.prototype.deserializeValue_ = function(data) {
    if (this.series_.getValueAxis().getType() == anychart.plots.axesPlot.axes.AxisType.VALUE) {
        this.y_ = this.deserializeValueFromConfig_(anychart.utils.deserialization.getProp(data, 'y'));
        this.isMissing_ = isNaN(this.y_);
        if (!this.isMissing_)
            this.series_.getValueAxis().checkScaleValue(this.y_);
    } else {
        var categoryName = anychart.utils.deserialization.getStringProp(data, 'y');
        this.yCategory_ = this.valueAxis_.getCategory(categoryName);
        this.y = this.yCategory_.index;
        if (!this.isMissing_)
            this.yCategory_.checkPoint(this);
    }
    goog.base(this, 'deserializeValue_', data);
};

anychart.plots.axesPlot.data.SingleValuePoint.prototype.initializeAfterInterpolate = function() {
    this.series_.getValueAxis().checkScaleValue(this.y_);
    goog.base(this, 'initializeAfterInterpolate');
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.SingleValuePoint.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%Value', 0);
    this.tokensHash_.add('%YValue', 0);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.SingleValuePoint.prototype.getTokenType = function(token) {
    switch (token) {
        case '%Value':
        case '%YValue':
            return this.series_.getValueAxis().isDateTimeAxis() ?
                anychart.formatting.TextFormatTokenType.DATE :
                anychart.formatting.TextFormatTokenType.NUMBER;
        case '%YPercentOfSeries':
        case '%YPercentOfTotal':
        case '%YPercentOfCategory':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.data.SingleValuePoint.prototype.checkCategory = function (category) {
    var tokenHash = category.getTokensHash();
    tokenHash.increment('%CategoryYSum', this.y_);
    tokenHash.increment('%CategoryPointCount', 1);

    if (this.y_ > tokenHash.get('%CategoryYMax'))
        tokenHash.add('%CategoryYMax', this.y_);

    if (this.y_ < tokenHash.get('%CategoryYMin'))
        tokenHash.add('%CategoryYMin', this.y_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.data.SingleValuePoint.prototype.calculatePointTokenValues = function() {
    goog.base(this, 'calculatePointTokenValues');
    if (this.y_) this.tokensHash_.add('%YValue', this.y_);
    if (this.y_) this.tokensHash_.add('%Value', this.y_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.data.SingleValuePoint.prototype.recalculatePointTokenValues = function() {
};
//------------------------------------------------------------------------------
//                          Formatting value getters
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.SingleValuePoint.prototype.getTokenValue = function(token) {
    if (!this.tokensHash_.has('%YPercentOfSeries'))
        this.tokensHash_.add(
            '%YPercentOfSeries',
            this.getY() / this.series_.getSeriesTokenValue('%SeriesYSum') * 100);

    if (!this.tokensHash_.has('%YPercentOfTotal'))
        this.tokensHash_.add(
            '%YPercentOfTotal',
            this.getY() / this.getPlot().getTokenValue('%DataPlotYSum') * 100);

    if (!this.tokensHash_.has('%YPercentOfCategory') && this.dataCategory_)
        this.tokensHash_.add(
            '%YPercentOfCategory',
            this.getY() / this.dataCategory_.getTokenValue('%CategoryYSum') * 100);

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//                          Serialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.SingleValuePoint.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['YValue'] = this.y_;
    opt_target['YPercentOfSeries'] = this.tokensHash_.get('%YPercentOfSeries');
    opt_target['YPercentOfTotal'] = this.tokensHash_.get('%YPercentOfTotal');
    opt_target['YPercentOfCategory'] = this.tokensHash_.get('%YPercentOfCategory');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          StackablePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.data.StackablePoint = function() {
    anychart.plots.axesPlot.data.SingleValuePoint.call(this);
};

goog.inherits(anychart.plots.axesPlot.data.StackablePoint,
    anychart.plots.axesPlot.data.SingleValuePoint);

anychart.plots.axesPlot.data.StackablePoint.prototype.previousStackPoint_ = null;
anychart.plots.axesPlot.data.StackablePoint.prototype.setPreviousStackPoint = function(value) {
    this.previousStackPoint_ = value;
};
anychart.plots.axesPlot.data.StackablePoint.prototype.getPreviousStackPoint = function() {
    return this.previousStackPoint_;
};

anychart.plots.axesPlot.data.StackablePoint.prototype.stackValue_ = NaN;
anychart.plots.axesPlot.data.StackablePoint.prototype.getStackValue = function() {
    return this.stackValue_;
};

anychart.plots.axesPlot.data.StackablePoint.prototype.deserializeValue_ = function(config) {
    var valueAxis = this.series_.getValueAxis();
    if (valueAxis.getType() == anychart.plots.axesPlot.axes.AxisType.VALUE) {
        this.y_ = this.deserializeValueFromConfig_(anychart.utils.deserialization.getProp(config, 'y'));

        if (this.category_ != null) {
            switch (valueAxis.getScale().getMode()) {
                case anychart.scales.ScaleMode.STACKED:
                    this.stackValue_ = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), false).addPoint(this, this.y_);
                    valueAxis.checkScaleValue(this.stackValue_);
                    break;
                case anychart.scales.ScaleMode.PERCENT_STACKED:
                    this.stackValue_ = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), true).addPoint(this, this.y_);
                    break;
                default:
                    this.stackValue_ = this.y_;
            }
        } else {
            this.stackValue_ = this.y_;
            valueAxis.checkScaleValue(this.stackValue_);
        }
        this.isMissing_ = isNaN(this.y_);
        this.isInverted_ = (valueAxis.getScale().isInverted() && (this.y_ >= 0)) || (!valueAxis.getScale().isInverted() && (this.y_ < 0));

        if (!this.isMissing_)
            valueAxis.checkScaleValue(this.stackValue_);
    } else {
        goog.base(this, 'deserializeValue_', config);
    }
};

anychart.plots.axesPlot.data.StackablePoint.prototype.initializeAfterInterpolate = function() {
    var valueAxis = this.series_.getValueAxis();
    if (this.category_ != null) {
        switch (valueAxis.getScale().getMode()) {
            case anychart.scales.ScaleMode.STACKED:
                this.stackValue_ = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), false).addPoint(this, this.y_);
                valueAxis.checkScaleValue(this.stackValue_);
                break;
            case anychart.scales.ScaleMode.PERCENT_STACKED:
                this.stackValue_ = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), true).addPoint(this, this.y_);
                break;
            default:
                this.stackValue_ = this.y_;
        }
    } else {
        this.stackValue_ = this.y_;
        valueAxis.checkScaleValue(this.stackValue_);
    }
    this.isInverted_ = (valueAxis.getScale().isInverted() && (this.y_ >= 0)) || (!valueAxis.getScale().isInverted() && (this.y_ < 0));

    goog.base(this, 'initializeAfterInterpolate', this);
};

anychart.plots.axesPlot.data.StackablePoint.prototype.setStackMax = function(value) {
    this.stackValue_ *= 100 / value;
};

//------------------------------------------------------------------------------
//
//                          RangeStackablePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.data.RangeStackablePoint = function() {
    anychart.plots.axesPlot.data.StackablePoint.call(this);
};

goog.inherits(anychart.plots.axesPlot.data.RangeStackablePoint,
    anychart.plots.axesPlot.data.StackablePoint);

anychart.plots.axesPlot.data.RangeStackablePoint.prototype.startStackValue_ = NaN;
anychart.plots.axesPlot.data.RangeStackablePoint.prototype.getStartStackValue = function() {
    return this.startStackValue_;
};

anychart.plots.axesPlot.data.RangeStackablePoint.prototype.deserializeValue_ = function(config) {
    var valueAxis = this.series_.getValueAxis();
    if (valueAxis.getType() == anychart.plots.axesPlot.axes.AxisType.VALUE) {
        this.y_ = this.deserializeValueFromConfig_(anychart.utils.deserialization.getProp(config, 'y'));
        if (this.category_ != null) {
            var stackSettings;

            switch (valueAxis.getScale().getMode()) {
                case anychart.scales.ScaleMode.STACKED:
                    stackSettings = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), false);
                    this.startStackValue_ = stackSettings.getPrevious(this.y_);
                    this.stackValue_ = stackSettings.addPoint(this, this.y_);
                    valueAxis.checkScaleValue(this.startStackValue_);
                    valueAxis.checkScaleValue(this.stackValue_);
                    break;
                case anychart.scales.ScaleMode.PERCENT_STACKED:
                    stackSettings = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), true);
                    this.startStackValue_ = stackSettings.getPrevious(this.y_);
                    this.stackValue_ = stackSettings.addPoint(this, this.y_);
                    break;

                default:
                    this.startStackValue_ = NaN;
                    this.stackValue_ = this.y_;
                    valueAxis.checkScaleValue(this.stackValue_);
            }
        } else {
            this.startStackValue_ = NaN;
            this.stackValue_ = this.y_;
            valueAxis.checkScaleValue(this.stackValue_);
        }
        this.isMissing_ = isNaN(this.y_);
        this.isInverted_ = (valueAxis.getScale().isInverted() && (this.y_ >= 0)) || (!valueAxis.getScale().isInverted() && (this.y_ < 0));
    } else {
        goog.base(this, 'deserializeValue_', config);
        this.startStackValue_ = NaN;
        this.stackValue_ = this.y_;
    }
};

anychart.plots.axesPlot.data.RangeStackablePoint.prototype.initializeAfterInterpolate = function() {
    var valueAxis = this.series_.getValueAxis();
    if (this.category_ != null) {
        var stackSettings;

        switch (valueAxis.getScale().getMode()) {
            case anychart.scales.ScaleMode.STACKED:
                stackSettings = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), false);
                this.startStackValue_ = stackSettings.getPrevious(this.y_);
                this.stackValue_ = stackSettings.addPoint(this, this.y_);
                valueAxis.checkScaleValue(this.startStackValue_);
                valueAxis.checkScaleValue(this.stackValue_);
                break;
            case anychart.scales.ScaleMode.PERCENT_STACKED:
                stackSettings = this.category_.getStackSettings(valueAxis.getName(), this.series_.getType(), true);
                this.startStackValue_ = stackSettings.getPrevious(this.y_);
                this.stackValue_ = stackSettings.addPoint(this, this.y_);
                break;

            default:
                this.startStackValue_ = NaN;
                this.stackValue_ = this.y_;
                valueAxis.checkScaleValue(this.stackValue_);
        }
    } else {
        this.startStackValue_ = NaN;
        this.stackValue_ = this.y_;
        valueAxis.checkScaleValue(this.stackValue_);
    }
    this.isInverted_ = (valueAxis.getScale().isInverted() && (this.y_ >= 0)) || (!valueAxis.getScale().isInverted() && (this.y_ < 0));
};

anychart.plots.axesPlot.data.RangeStackablePoint.prototype.setStackMax = function(value) {
    this.startStackValue_ *= (value == 0) ? 0 : (100 / value);
    this.stackValue_ *= (value == 0) ? 0 : (100 / value);
};
//------------------------------------------------------------------------------
//
//                          RangePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangePoint}
 */
anychart.plots.axesPlot.data.RangePoint = function() {
    anychart.plots.axesPlot.data.AxesPlotPoint.apply(this);
};

goog.inherits(anychart.plots.axesPlot.data.RangePoint,
    anychart.plots.axesPlot.data.AxesPlotPoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangePoint.prototype.start_ = null;
anychart.plots.axesPlot.data.RangePoint.prototype.getStart = function() {
    return this.start_;
};
anychart.plots.axesPlot.data.RangePoint.prototype.end_ = null;
anychart.plots.axesPlot.data.RangePoint.prototype.getEnd = function() {
    return this.end_;
};
anychart.plots.axesPlot.data.RangePoint.prototype.range_ = null;
anychart.plots.axesPlot.data.RangePoint.prototype.getRange = function () {
    return this.range_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @override
 */
anychart.plots.axesPlot.data.RangePoint.prototype.checkMissing_ = function(data) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(data, 'start') || !des.hasProp(data, 'end')) {
        this.isMissing_ = true;
        return;
    }


    var valueAxis = this.getSeries().getValueAxis();
    if (valueAxis.isValueAxis()) {
        this.isMissing_ = isNaN(this.deserializeValueFromConfig_(des.getProp(data, 'start')));
        this.isMissing_ = this.isMissing() || isNaN(this.deserializeValueFromConfig_(des.getProp(data, 'end')));
    }
};

/**
 * @override
 */
anychart.plots.axesPlot.data.RangePoint.prototype.deserializeValue_ = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'start'))
        this.start_ = this.deserializeValueFromConfig_(des.getProp(data, 'start'));
    if (des.hasProp(data, 'end'))
        this.end_ = this.deserializeValueFromConfig_(des.getProp(data, 'end'));

    var valueAxis = this.getSeries().getValueAxis();
    this.isInverted_ =
        (valueAxis.getScale().isInverted() && (this.start_ < this.end_)) ||
            (!valueAxis.getScale().isInverted() && (this.start_ > this.end_));

    if (!this.isMissing()) {
        this.range_ = this.end_ - this.start_;
        valueAxis.checkScaleValue(this.start_);
        valueAxis.checkScaleValue(this.end_);
    }
};

/**
 * @override
 */
anychart.plots.axesPlot.data.RangePoint.prototype.initializeAfterInterpolate = function() {
    this.range_ = this.end_ - this.start_;
    var valueAxis = this.getSeries().getValueAxis();
    valueAxis.checkScaleValue(this.start_);
    valueAxis.checkScaleValue(this.end_);
    goog.base(this, 'initializeAfterInterpolate');
};
//------------------------------------------------------------------------------
//                          Elements initialization
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.axesPlot.data.RangePoint.prototype.additionalLabelSprite_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.axesPlot.data.RangePoint.prototype.additionalMarkerSprite_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.axesPlot.data.RangePoint.prototype.additionalTooltipSprite_ = null;
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangePoint.prototype.initializeElements = function() {
    goog.base(this, 'initializeElements');
    this.additionalLabelSprite_ = this.initElement(
        this.additionalLabel_,
        true);
    this.additionalMarkerSprite_ = this.initElement(
        this.additionalMarker_,
        true);
    this.additionalTooltipSprite_ = this.initElement(
        this.additionalTooltip_,
        false);
};
//------------------------------------------------------------------------------
//                          Update elements
//------------------------------------------------------------------------------
/**@protected*/
anychart.plots.axesPlot.data.RangePoint.prototype.updateElements = function() {
    goog.base(this, 'updateElements');
    this.updateElement(this.additionalLabel_, this.additionalLabelSprite_);
    this.updateElement(this.additionalMarker_, this.additionalMarkerSprite_);
};
/**@protected*/
anychart.plots.axesPlot.data.RangePoint.prototype.updateTooltips = function(event) {
    goog.base(this, 'updateTooltips', event);
    this.updateTooltip(
        event,
        this.additionalTooltip_,
        this.additionalTooltipSprite_);
};
anychart.plots.axesPlot.data.RangePoint.prototype.hideTooltips = function() {
    goog.base(this, 'hideTooltips');
    this.hideElement(this.additionalTooltipSprite_);
};
anychart.plots.axesPlot.data.RangePoint.prototype.moveTooltips = function(event) {
    goog.base(this, 'moveTooltips', event);
    this.moveTooltip(
        event,
        this.additionalTooltip_,
        this.additionalTooltipSprite_);

};
//------------------------------------------------------------------------------
//                  Resize elements.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangePoint.prototype.resizeElements = function() {
    goog.base(this, 'resizeElements');
    if (this.additionalLabelSprite_)
        this.resizeElement(this.additionalLabel_, this.additionalLabelSprite_);

    if (this.additionalMarkerSprite_)
        this.resizeElement(this.additionalMarker_, this.additionalMarkerSprite_);
};
//------------------------------------------------------------------------------
//                          Elements deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangePoint.prototype.additionalLabel_ = null;
anychart.plots.axesPlot.data.RangePoint.prototype.additionalMarker_ = null;
anychart.plots.axesPlot.data.RangePoint.prototype.additionalTooltip_ = null;
/**@override*/
anychart.plots.axesPlot.data.RangePoint.prototype.deserializeElements = function(data, stylesList) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'start_point')) {
        this.deserializeLabelElement_(des.getProp(data, 'start_point'), stylesList);
        this.deserializeMarkerElement_(des.getProp(data, 'start_point'), stylesList);
        this.deserializeTooltipElement_(des.getProp(data, 'start_point'), stylesList);
    }

    if (des.hasProp(data, 'end_point')) {
        this.deserializeAdditionalLabelElement_(des.getProp(data, 'end_point'), stylesList);
        this.deserializeAdditionalMarkerElement_(des.getProp(data, 'end_point'), stylesList);
        this.deserializeAdditionalTooltipElement_(des.getProp(data, 'end_point'), stylesList);
    }
};
/**@private*/
anychart.plots.axesPlot.data.RangePoint.prototype.deserializeAdditionalLabelElement_ = function(data, stylesList) {
    if (this.additionalLabel_) {
        if (anychart.utils.deserialization.hasProp(data, 'label')) {
            if (this.additionalLabel_.canDeserializeForPoint(
                anychart.utils.deserialization.getProp(data, 'label'))) {
                this.additionalLabel_ = this.additionalLabel_.copy();
                this.additionalLabel_.deserializePointSettings(
                    anychart.utils.deserialization.getProp(data, 'label'),
                    stylesList);
            }
        }
        this.checkElementPosition(this.additionalLabel_);
    }
};
/**@private*/
anychart.plots.axesPlot.data.RangePoint.prototype.deserializeAdditionalMarkerElement_ = function(data, stylesList) {
    if (this.additionalMarker_) {
        if (anychart.utils.deserialization.hasProp(data, 'marker')) {
            if (this.additionalMarker_.canDeserializeForPoint(
                anychart.utils.deserialization.getProp(data, 'marker'))) {
                this.additionalMarker_ = this.additionalMarker_.copy();
                this.additionalMarker_.deserializePointSettings(
                    anychart.utils.deserialization.getProp(data, 'marker'),
                    stylesList);
            }
        }
        this.checkElementPosition(this.additionalMarker_);
    }
};
/**@private*/
anychart.plots.axesPlot.data.RangePoint.prototype.deserializeAdditionalTooltipElement_ = function(data, stylesList) {
    if (this.additionalTooltip_ && anychart.utils.deserialization.hasProp(data, 'tooltip')) {
        if (this.additionalTooltip_.canDeserializeForPoint(
            anychart.utils.deserialization.getProp(data, 'tooltip'))) {
            this.additionalTooltip_ = this.additionalTooltip_.copy();
            this.additionalTooltip_.deserializePointSettings(
                anychart.utils.deserialization.getProp(data, 'tooltip'),
                stylesList);
        }
    }
    this.checkElementPosition(this.additionalTooltip_);
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.RangePoint.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%RangeStart', 0);
    this.tokensHash_.add('%YRangeStart', 0);
    this.tokensHash_.add('%RangeEnd', 0);
    this.tokensHash_.add('%YRangeEnd', 0);
    this.tokensHash_.add('%Range', 0);
    this.tokensHash_.add('%YRange', 0);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.RangePoint.prototype.getTokenType = function(token) {
    switch (token) {
        case '%RangeStart':
        case '%YRangeStart':
        case '%RangeEnd':
        case '%YRangeEnd':
            return this.series_.getValueAxis().isDateTimeAxis() ?
                anychart.formatting.TextFormatTokenType.DATE :
                anychart.formatting.TextFormatTokenType.NUMBER;
        case '%Range':
        case '%YRange':
        case '%Value':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangePoint.prototype.checkCategory = function (category) {
    goog.base(this, 'checkCategory', category);

    var tokenHash = category.getTokensHash();
    tokenHash.increment('%CategoryYRangeSum', this.range_);

    if (this.range_ > tokenHash.get('%CategoryYRangeMax'))
        tokenHash.add('%CategoryYRangeMax', this.range_);

    if (this.range_ < tokenHash.get('%CategoryYRangeMin'))
        tokenHash.add('%CategoryYRangeMin', this.range_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangePoint.prototype.calculatePointTokenValues = function() {
    goog.base(this, 'calculatePointTokenValues');
    this.tokensHash_.add('%RangeStart', this.start_);
    this.tokensHash_.add('%YRangeStart', this.start_);
    this.tokensHash_.add('%RangeEnd', this.end_);
    this.tokensHash_.add('%YRangeEnd', this.end_);
    this.tokensHash_.add('%Range', this.range_);
    this.tokensHash_.add('%YRange', this.range_);
    this.tokensHash_.add('%Value', this.range_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangePoint.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);

    opt_target['YRangeStart'] = this.start_;
    opt_target['YRangeEnd'] = this.end_;
    opt_target['YRange'] = this.range_;
    opt_target['StartValue'] = this.start_;
    opt_target['EndValue'] = this.end_;
    opt_target['StartMarkerType'] = this.markerType_;
    opt_target['EndMarkerType'] = this.markerType_;

    return opt_target;
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//                          RangeSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesPlotSeries}
 */
anychart.plots.axesPlot.data.RangeSeries = function() {
    anychart.plots.axesPlot.data.AxesPlotSeries.apply(this);
};

goog.inherits(anychart.plots.axesPlot.data.RangeSeries,
    anychart.plots.axesPlot.data.AxesPlotSeries);
//------------------------------------------------------------------------------
//                          Elementts
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangeSeries.prototype.additionalLabel_ = null;
anychart.plots.axesPlot.data.RangeSeries.prototype.additionalMarker_ = null;
anychart.plots.axesPlot.data.RangeSeries.prototype.additionalTooltip_ = null;
/**@override*/
anychart.plots.axesPlot.data.RangeSeries.prototype.deserializeElements = function(data, stylesList) {
    var des = anychart.utils.deserialization;

    this.deserializeLabelElement_(des.getProp(data, 'start_point'), stylesList);
    this.deserializeMarkerElement_(des.getProp(data, 'start_point'), stylesList);
    this.deserializeTooltipElement_(des.getProp(data, 'start_point'), stylesList);

    this.deserializeAdditionalLabelElement_(des.getProp(data, 'end_point'), stylesList);
    this.deserializeAdditionalMarkerElement_(des.getProp(data, 'end_point'), stylesList);
    this.deserializeAdditionalTooltipElement_(des.getProp(data, 'end_point'), stylesList);
};
/**@private*/
anychart.plots.axesPlot.data.RangeSeries.prototype.deserializeAdditionalLabelElement_ = function(data, stylesList) {
    if (this.additionalLabel_ && this.additionalLabel_.canDeserializeForSeries(anychart.utils.deserialization.getProp(data, 'label'))) {
        this.additionalLabel_ = this.additionalLabel_.copy();
        this.additionalLabel_.deserializeSeriesSettings(anychart.utils.deserialization.getProp(data, 'label'), stylesList);
    }
};
/**@private*/
anychart.plots.axesPlot.data.RangeSeries.prototype.deserializeAdditionalMarkerElement_ = function(data, stylesList) {
    if (this.additionalMarker_ && this.additionalMarker_.canDeserializeForSeries(anychart.utils.deserialization.getProp(data, 'marker'))) {
        this.additionalMarker_ = this.additionalMarker_.copy();
        this.additionalMarker_.deserializeSeriesSettings(anychart.utils.deserialization.getProp(data, 'marker'), stylesList);
    }
};
/**@private*/
anychart.plots.axesPlot.data.RangeSeries.prototype.deserializeAdditionalTooltipElement_ = function(data, stylesList) {
    if (this.additionalTooltip_ && this.additionalTooltip_.canDeserializeForSeries(anychart.utils.deserialization.getProp(data, 'tooltip'))) {
        this.additionalTooltip_ = this.additionalTooltip_.copy();
        this.additionalTooltip_.deserializeSeriesSettings(anychart.utils.deserialization.getProp(data, 'tooltip'), stylesList);
    }
};
/**
 * @override
 * @param {anychart.plots.seriesPlot.data.BaseDataElement} target Data element.
 */
anychart.plots.axesPlot.data.RangeSeries.prototype.setElements = function(target) {
    goog.base(this, 'setElements', target);
    target.additionalLabel_ = this.additionalLabel_;
    target.additionalMarker_ = this.additionalMarker_;
    target.additionalTooltip_ = this.additionalTooltip_;
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.RangeSeries.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesYRangeMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYRangeMin', Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYRangeSum', 0);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.data.RangeSeries.prototype.getTokenType = function(token) {
    switch (token) {
        case '%SeriesYRangeMax':
        case '%SeriesYRangeMin':
        case '%SeriesYRangeSum':
            return this.valueAxis_.isDateTimeAxis() ?
                anychart.formatting.TextFormatTokenType.DATE :
                anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangeSeries.prototype.checkPlot = function(plot) {
    var plotHatch = plot.getTokensHash();
    plotHatch.increment('%DataPlotYRangeSum', this.tokensHash_.get('%SeriesYRangeSum'));

    if (this.tokensHash_.get('%SeriesYRangeMax') > plotHatch.get('%DataPlotYRangeMax'))
        plotHatch.add('%DataPlotYRangeMax', this.tokensHash_.get('%SeriesYRangeMax'));

    if (this.tokensHash_.get('%SeriesYRangeMin') < plotHatch.get('%DataPlotYRangeMin'))
        plotHatch.add('%DataPlotYRangeMin', this.tokensHash_.get('%SeriesYRangeMin'));

    goog.base(this, 'checkPlot', plot);
};
anychart.plots.axesPlot.data.RangeSeries.prototype.checkPoint = function(dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);

    this.tokensHash_.increment('%SeriesYRangeSum', dataPoint.getRange());
    if (dataPoint.getRange() > this.tokensHash_.get('%SeriesYRangeMax'))
        this.tokensHash_.add('%SeriesYRangeMax', dataPoint.getRange());

    if (dataPoint.getRange() < this.tokensHash_.get('%SeriesYRangeMin'))
        this.tokensHash_.add('%SeriesYRangeMin', dataPoint.getRange());
};
/**@inheritDoc*/
anychart.plots.axesPlot.data.RangeSeries.prototype.calculateSeriesTokenValues = function() {
    goog.base(this, 'calculateSeriesTokenValues');
};
//------------------------------------------------------------------------------
//                    Serialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangeSeries.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['YRangeSum'] = this.tokensHash_.get('%SeriesRangeSum');
    opt_target['YRangeMax'] = this.tokensHash_.get('%SeriesRangeMax');
    opt_target['YRangeMin'] = this.tokensHash_.get('%SeriesRangeMin');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          RangeGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.apply(this);
};

goog.inherits(anychart.plots.axesPlot.data.RangeGlobalSeriesSettings,
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Elements
//------------------------------------------------------------------------------
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.additionalLabel_ = null;
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.additionalMarker_ = null;
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.additionalTooltip_ = null;
/**@override*/
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.deserializeElements = function(data, stylesList) {
    var des = anychart.utils.deserialization;

    this.deserializeLabelElement_(des.getProp(data, 'start_point'), stylesList);
    this.deserializeMarkerElement_(des.getProp(data, 'start_point'), stylesList);
    this.deserializeTooltipElement_(des.getProp(data, 'start_point'), stylesList);

    this.deserializeAdditionalLabelElement_(des.getProp(data, 'end_point'), stylesList);
    this.deserializeAdditionalMarkerElement_(des.getProp(data, 'end_point'), stylesList);
    this.deserializeAdditionalTooltipElement_(des.getProp(data, 'end_point'), stylesList);
};
/**@private*/
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.deserializeAdditionalLabelElement_ = function(data, stylesList) {
    this.additionalLabel_ = this.createLabelElement();
    if (!this.additionalLabel_) return;
    if (data && anychart.utils.deserialization.hasProp(data, 'label_settings'))
        this.additionalLabel_.deserializeGlobalSettings(
            anychart.utils.deserialization.getProp(data, 'label_settings'),
            stylesList);
    else this.additionalLabel_.setStyle(new anychart.styles.Style(
        this.additionalLabel_.getStyleStateClass()));
};
/**@private*/
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.deserializeAdditionalMarkerElement_ = function(data, stylesList) {
    this.additionalMarker_ = this.createMarkerElement();
    if (!this.additionalMarker_) return;
    if (data && anychart.utils.deserialization.hasProp(data, 'marker_settings'))
        this.additionalMarker_.deserializeGlobalSettings(
            anychart.utils.deserialization.getProp(data, 'marker_settings'),
            stylesList);
    else this.additionalMarker_.setStyle(new anychart.styles.Style(
        this.additionalMarker_.getStyleStateClass()));
};
/**@private*/
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeAdditionalTooltipElement_ = function(data, stylesList) {
    this.additionalTooltip_ = this.createTooltipElement();
    if (!this.additionalTooltip_) return;
    if (data && anychart.utils.deserialization.hasProp(data, 'tooltip_settings'))
        this.additionalTooltip_.deserializeGlobalSettings(
            anychart.utils.deserialization.getProp(
                data, 'tooltip_settings'),
            stylesList);
    else this.additionalTooltip_.setStyle(new anychart.styles.Style(
        this.additionalTooltip_.getStyleStateClass()));
};
/**
 * @override
 * @param {anychart.plots.seriesPlot.data.BaseDataElement} target Data element.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.setElements = function(target) {
    goog.base(this, 'setElements', target);
    target.additionalLabel_ = this.additionalLabel_;
    target.additionalMarker_ = this.additionalMarker_;
    target.additionalTooltip_ = this.additionalTooltip_;
};
//------------------------------------------------------------------------------
//                          Interpolators
//------------------------------------------------------------------------------
//todo:implement
/**
 * @private
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.startInterpolator_ = null;

/**
 * @private
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.endInterpolator_ = null;

/**
 * @public
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.initializeMissingInterpolators = function() {

};

/**
 * @public
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.missingCheckPointDuringDeserialize = function(point) {
};

/**
 * @public
 */
anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.prototype.missingInterpolate = function(points) {
};
