/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.bubble.BubblePoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bubble.BubbleSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.bubble');
goog.require('anychart.plots.axesPlot.data');
goog.require('anychart.plots.axesPlot.series.bubble.styles');
//------------------------------------------------------------------------------
//
//                          BubblePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bubble.BubblePoint = function() {
    anychart.plots.axesPlot.data.StackablePoint.apply(this);
    this.centerPoint_ = new anychart.utils.geom.Point();
};
goog.inherits(anychart.plots.axesPlot.series.bubble.BubblePoint,
        anychart.plots.axesPlot.data.StackablePoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.bubbleSize_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getBubbleSize = function() {
    return this.bubbleSize_;
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.pixelRadius_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getPixelRadius = function() {
    return this.pixelRadius_;
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.centerX_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getCenterX = function() {
    return this.centerX_;
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.centerY_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getCenterY = function() {
    return this.centerY_;
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.dRadius_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getDRadius = function() {
    return this.dRadius_;
};

anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.centerPoint_ = null;
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getCenterPoint = function() {
    return this.centerPoint_;
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.setCenterPoint = function(value) {
    this.centerPoint_ = value;
};
/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.cachedShape_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.calculateBounds = function() {
    this.getSeries().getArgumentAxis().transform(this, this.x_, this.centerPoint_);
    this.getSeries().getValueAxis().transform(this, this.stackValue_, this.centerPoint_);

    this.centerX_ = this.centerPoint_.x;
    this.centerY_ = this.centerPoint_.y;

    this.pixelRadius_ = this.getGlobalSettings().getBubbleSize(this.bubbleSize_) / 2;
    this.dRadius_ = 0;

    this.bounds_ = new anychart.utils.geom.Rectangle(
            this.centerX_ - this.pixelRadius_,
            this.centerY_ - this.pixelRadius_,
            this.pixelRadius_ * 2,
            this.pixelRadius_ * 2);



    this.bounds_.x += this.dRadius_;
    this.bounds_.y += this.dRadius_;
    this.bounds_.width -= this.dRadius_ * 2;
    this.bounds_.height -= this.dRadius_ * 2;

    if(this.currentState_.getBorder() && this.currentState_.getBorder().isEnabled())
        this.dRadius_ = this.currentState_.getBorder().getThickness() / 2;
};
/**
 * @private
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.returnBounds = function() {
    this.bounds_.x -= this.dRadius_;
    this.bounds_.y -= this.dRadius_;
    this.bounds_.width += this.dRadius_ * 2;
    this.bounds_.height += this.dRadius_ * 2;
};
/**
 * @param svgManager
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.createBubbleShape = function(svgManager) {
    var cx = this.centerX_ || 0;
    var cy = this.centerY_ || 0;
    var r = Math.abs(this.pixelRadius_ - this.dRadius_) || 0;
    return svgManager.createCircle(cx, cy, r);
};
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.updateBubbleShape = function(element) {
    this.calculateBounds();
    element.setAttribute('cx', this.centerX_);
    element.setAttribute('cy', this.centerY_);
    element.setAttribute('r', Math.abs(this.pixelRadius_ - this.dRadius_));
    this.returnBounds();
};
//------------------------------------------------------------------------------
//                          Override
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.sortedOverlayValue = function() {
    return this.bubbleSize_;
};

anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.deserializeValue_ = function(data) {
    goog.base(this, 'deserializeValue_', data);
    this.bubbleSize_ = anychart.utils.deserialization.getNumProp(data, 'size');

    if (isNaN(this.bubbleSize_)) {
        this.isMissing_ = true;
    } else {
        var settings = this.getGlobalSettings();
        if (this.bubbleSize_ > settings.getMaximumValueBubbleSize())
            settings.setMaximumValueBubbleSize(this.bubbleSize_);
        if (this.bubbleSize_ < settings.getMinimumValueBubbleSize())
            settings.setMinimumValueBubbleSize(this.bubbleSize_);
    }
};

anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.checkMissing_ = function(data) {
    goog.base(this, 'checkMissing_', data);
    var des = anychart.utils.deserialization;
    this.isMissing_ = this.isMissing_ || !des.hasProp(data, 'size') || isNaN(des.getNumProp(data, 'size'));
    if (!this.getGlobalSettings().isDisplayNegative() && des.getNumProp(data, 'size') < 0)
        this.isMissing_ = true;
};

/**
 * Determine if a rectangle should select this point
 * @public
 * @param {anychart.utils.geom.Rectangle} rect The rectangle defining the selection
 * @return boolean if the rectangle should select the point
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.isRectSelecting = function (rect) { 
    var r1 = new anychart.utils.geom.intersections.Point2D(rect.x, rect.y);
    var r2 = new anychart.utils.geom.intersections.Point2D(rect.x + rect.width, rect.y + rect.height);
    var c = new anychart.utils.geom.intersections.Point2D(this.getCenterX(), this.getCenterY());
    var cBounds = this.getBounds();
    var r = cBounds.width / 2;
    var inters = anychart.utils.geom.intersections.Intersection.intersectCircleRectangle(c, r, [r1, r2]);
    var found = inters && inters.points.length;
    if (found) {
        return true;
    }
    // check for containment of points in rect
    return rect.containsRect(cBounds) || cBounds.containsRect(rect);
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%BubbleSize', 0);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getTokenType = function(token) {
    switch (token) {
        case '%BubbleSize':
        case '%BubbleSizePercentOfSeries':
        case '%BubbleSizePercentOfTotal':
        case '%BubbleSizePercentOfCategory':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.calculatePointTokenValues = function() {
    goog.base(this, 'calculatePointTokenValues');
    this.tokensHash_.add('%BubbleSize', this.bubbleSize_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.recalculatePointTokenValues = function() {
};
//------------------------------------------------------------------------------
//                    Formatting value getters
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.getTokenValue = function(token) {
    if (!this.tokensHash_.has('%BubbleSizePercentOfSeries'))
        this.tokensHash_.add(
                '%BubbleSizePercentOfSeries',
                this.bubbleSize_ / this.series_.getSeriesTokenValue('%SeriesBubbleSizeSum') * 100);

    if (!this.tokensHash_.has('%BubbleSizePercentOfTotal'))
        this.tokensHash_.add(
                '%BubbleSizePercentOfTotal',
                this.bubbleSize_ / this.getPlot().getTokenValue('%DataPlotBubbleSizeSum') * 100);

    if (!this.tokensHash_.has('%BubbleSizePercentOfCategory') && this.dataCategory_)
        this.tokensHash_.add('%BubbleSizePercentOfCategory',
                this.bubbleSize_ / this.dataCategory_.getCategoryTokenValue('%CategoryBubbleSizeSum') * 100);

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//                    Serialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubblePoint.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['BubbleSize'] = this.bubbleSize_;
    opt_target['BubbleSizePercentOfSeries'] = this.tokensHash_.get('%BubbleSizePercentOfSeries');
    opt_target['BubbleSizePercentOfTotal'] = this.tokensHash_.get('%BubbleSizePercentOfTotal');
    opt_target['BubbleSizePercentOfCategory'] = this.tokensHash_.get('%BubbleSizePercentOfCategory');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          BubbleSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bubble.BubbleSeries = function() {
    anychart.plots.axesPlot.data.SingleValueSeries.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.BubbleSeries,
        anychart.plots.axesPlot.data.SingleValueSeries);

anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return false;
};
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.bubble.BubblePoint();
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesBubbleSizeSum', 0);
    this.tokensHash_.add('%SeriesBubbleMaxSize', -Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesBubbleMinSize', Number.MAX_VALUE);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.getTokenType = function(token) {
    switch (token) {
        case '%SeriesBubbleSizeSum':
        case '%SeriesBubbleMaxSize':
        case '%SeriesBubbleMinSize':
        case '%SeriesBubbleSizeAverage':
        case '%SeriesBubbleSizeMedian':
        case '%SeriesBubbleSizeMode':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token)
};
//------------------------------------------------------------------------------
//                          Formatting value calculation
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.checkPlot = function(plot) {
    goog.base(this, 'checkPlot', plot);
    var plotHash = plot.getTokensHash();

    plotHash.increment('%DataPlotBubbleSizeSum', this.tokensHash_.get('%SeriesBubbleSizeSum'));
    plotHash.increment('%DataPlotBubblePointCount', this.points_.length);

    if (this.tokensHash_.get('%SeriesBubbleMaxSize') > plotHash.get('%DataPlotBubbleMaxSize'))
        plotHash.add('%DataPlotBubbleMaxSize', this.tokensHash_.get('%SeriesBubbleMaxSize'));

    if (this.tokensHash_.get('%SeriesBubbleMinSize') < plotHash.get('%DataPlotBubbleMinSize'))
        plotHash.add('%DataPlotBubbleMinSize', this.tokensHash_.get('%SeriesBubbleMinSize'));

};
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.checkPoint = function(dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);
    this.tokensHash_.increment('%SeriesBubbleSizeSum', dataPoint.getBubbleSize());

    if (dataPoint.getBubbleSize() > this.tokensHash_.get('%SeriesBubbleMaxSize'))
        this.tokensHash_.add('%SeriesBubbleMaxSize', dataPoint.getBubbleSize());

    if (dataPoint.getBubbleSize() < this.tokensHash_.get('%SeriesBubbleMinSize'))
        this.tokensHash_.add('%SeriesBubbleMinSize', dataPoint.getBubbleSize());
};

anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.checkAxes = function() {
    this.setBubblesTokensToAxis(this.getArgumentAxis().getTokensHash());
    this.setBubblesTokensToAxis(this.getValueAxis().getTokensHash());
    goog.base(this, 'checkAxes');
};

anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.setBubblesTokensToAxis = function(axisHash) {
    axisHash.increment('%AxisBubbleSizeSum', this.tokensHash_.get('%SeriesBubbleSizeSum'));

    if (this.tokensHash_.get('%SeriesBubbleMaxSize') > axisHash.get('%AxisBubbleMax'))
        axisHash.add('%AxisBubbleSizeMax', this.tokensHash_.get('%SeriesBubbleMaxSize'));
    if (this.tokensHash_.get('%SeriesBubbleMinSize') < axisHash.get('%AxisBubbleSizeMin'))
        axisHash.add('%AxisBubbleSizeMin', this.tokensHash_.get('%SeriesBubbleMinSize'));
};

anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.calculateSeriesTokenValues = function() {
    goog.base(this, 'calculateSeriesTokenValues');
    this.tokensHash_.add('%SeriesBubbleSizeAverage', this.getSeriesTokenValue(
            '%SeriesBubbleSizeSum') /
            this.points_.length);
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleSeries.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);

    opt_target['BubbleSizeSum'] = this.tokensHash_.get('%SeriesBubbleSizeSum');
    opt_target['BubbleMaxSize'] = this.tokensHash_.get('%SeriesBubbleSizeMax');
    opt_target['BubbleMinSize'] = this.tokensHash_.get('%SeriesBubbleSizeMin');
    opt_target['BubbleSizeAverage'] = this.tokensHash_.get('%SeriesBubbleSizeAverage');
    opt_target['BubbleSizeMedian'] = this.tokensHash_.get('%SeriesBubbleSizeMedian');
    opt_target['BubbleSizeMode'] = this.tokensHash_.get('%SeriesBubbleSizeMode');

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          BubbleGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings,
        anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMultiplierBubbleSize_ = false;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMultiplierBubbleSize = function() {
    return this.isPercentMultiplierBubbleSize_;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.bubbleSizeMultiplier_ = 0;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getBubbleSizeMultiplier = function() {
    return this.bubbleSizeMultiplier_;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.minimumValueBubbleSize_ = Number.MAX_VALUE;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getMinimumValueBubbleSize = function() {
    return this.minimumValueBubbleSize_;
};
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.setMinimumValueBubbleSize = function(value) {
    this.minimumValueBubbleSize_ = value;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.maximumValueBubbleSize_ = -Number.MAX_VALUE;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getMaximumValueBubbleSize = function() {
    return this.maximumValueBubbleSize_;
};
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.setMaximumValueBubbleSize = function(value) {
    this.maximumValueBubbleSize_ = value;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMaximumBubbleSize_ = true;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMaximumBubbleSize = function() {
    return this.isPercentMaximumBubbleSize_;
};
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.maximumBubbleSize_ = .2;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getMaximumBubbleSize = function() {
    return this.maximumBubbleSize_;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMinimumBubbleSize_ = true;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isPercentMinimumBubbleSize = function() {
    return this.isPercentMinimumBubbleSize_;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.minimumBubbleSize_ = .01;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getMinimumBubbleSize = function() {
    return this.minimumBubbleSize_;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.displayNegative_ = true;
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.isDisplayNegative = function() {
    return  this.displayNegative_;
};
//------------------------------------------------------------------------------
//                          Size
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getBubbleSize = function(size) {
    var targetSize = Math.min(this.getPlot().getBounds().width, this.getPlot().getBounds().height);

    if (this.bubbleSizeMultiplier_ > 0) {
        if (this.isPercentMultiplierBubbleSize()) return size * this.bubbleSizeMultiplier_ * targetSize;
        else return size * this.bubbleSizeMultiplier_;
    }

    var minPixSize = this.isPercentMinimumBubbleSize() ?
            (targetSize * this.minimumBubbleSize_) :
            this.minimumBubbleSize_;
    var maxPixSize = this.isPercentMaximumBubbleSize() ?
            (targetSize * this.maximumBubbleSize_) :
            this.maximumBubbleSize_;

    if (isNaN(size) || (this.minimumValueBubbleSize_ == this.maximumValueBubbleSize_))
        return maxPixSize;

    var ratio = (size - this.minimumValueBubbleSize_) /
            (this.maximumValueBubbleSize_ - this.minimumValueBubbleSize_);
    return (minPixSize + ratio * (maxPixSize - minPixSize));
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.deserialize = function(data) {
    if (!data) return;
    goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'maximum_bubble_size')) {
        this.maximumBubbleSize_ = des.getProp(data, 'maximum_bubble_size');
        if (des.isPercent(this.maximumBubbleSize_)) {
            this.isPercentMaximumBubbleSize_ = true;
            this.maximumBubbleSize_ = des.getPercent(this.maximumBubbleSize_);
        } else {
            this.isPercentMaximumBubbleSize_ = false;
            this.maximumBubbleSize_ = des.getNum(this.maximumBubbleSize_);
        }
    }
    if (des.hasProp(data, 'minimum_bubble_size')) {
        this.minimumBubbleSize_ = des.getProp(data, 'minimum_bubble_size');
        if (des.isPercent(this.minimumBubbleSize_)) {
            this.isPercentMinimumBubbleSize_ = true;
            this.minimumBubbleSize_ = des.getPercent(this.minimumBubbleSize_);
        } else {
            this.isPercentMinimumBubbleSize_ = false;
            this.minimumBubbleSize_ = des.getNum(this.minimumBubbleSize_);
        }
    }

    if (des.hasProp(data, 'bubble_size_multiplier')) {
        this.bubbleSizeMultiplier_ = des.getProp(data, 'bubble_size_multiplier');
        if (des.isPercent(this.bubbleSizeMultiplier_)) {
            this.isPercentMultiplierBubbleSize_ = true;
            this.bubbleSizeMultiplier_ = des.getPercent(this.bubbleSizeMultiplier_);
        } else {
            this.isPercentMultiplierBubbleSize_ = false;
            this.bubbleSizeMultiplier_ = des.getNum(this.bubbleSizeMultiplier_);
        }
    }

    if (des.hasProp(data, 'display_negative_bubbles'))
        this.displayNegative_ = des.getBoolProp(data, 'display_negative_bubbles');
};
//------------------------------------------------------------------------------
//                          Override
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.createStyle = function(seriesType) {
    return new anychart.plots.axesPlot.series.bubble.styles.BubbleStyle();
};
anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series = new anychart.plots.axesPlot.series.bubble.BubbleSeries();
    series.type = anychart.series.SeriesType.BUBBLE;
    series.clusterKey = anychart.series.SeriesType.BUBBLE;
    return series;
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return 'bubble_series';
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return 'bubble_style';
};

anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings.prototype.sizeInterpolator_ = null;
