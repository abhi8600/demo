/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.elements.AxisGrid};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.elements.AxisTickmark};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.elements.AxisTitle};</li>
 * <ul>
 */

//namespace
goog.provide('anychart.plots.axesPlot.axes.elements');

//require
goog.require('anychart.visual.stroke');
goog.require('anychart.visual.fill');
goog.require('anychart.visual.hatchFill');
goog.require('anychart.visual.text');
goog.require('anychart.utils.geom');

//-----------------------------------------------------------------------------------
//
//                          AxisGrid class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent axis grid.
 * @public
 * @constructor
 */
anychart.plots.axesPlot.axes.elements.AxisGrid = function() {
};

/**
 * Indicates, is grid enabled.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.enabled_ = true;

/**
 * TODO: description
 * @return {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.isEnabled = function() { return this.enabled_; };

/**
 * TODO: description
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.interlaced_ = true;
/**
 * TODO: description
 * @return {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.isInterlaced = function() { return this.interlaced_; };

/**
 * Indicates, need draw first line.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.drawFirstLine_ = true;
/**
 * TODO: description
 * @return {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.drawFirstLine = function() { return this.drawFirstLine_; };

/**
 * Indicates, need draw last line.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.drawLastLine_ = true;
/**
 * TODO: description
 * @return {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.drawLastLine = function() { return this.drawLastLine_; };
//-----------------------------------------------------------------------------------
//
//                          Draw settings
//
//-----------------------------------------------------------------------------------
/**
 * Grid line settings.
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.line_ = null;
/**
 * TODO: description
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.getLine = function() { return this.line_; };

anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.hasLine = function() { return this.line_ != null; };

/**
 * ??? Grid fill settings.
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.evenFill_ = null;

/**
 * TODO: description
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.getEvenFill = function() { return this.evenFill_; };

anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.hasEvenFill = function() { return this.evenFill_ != null; };

/**
 * ??? Grid hatch fill settings.
 * @private
 * @type {anychart.visual.hatchFill.HatchFill}
 * }
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.evenHatchFill_ = null;
/**
 * TODO: description
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.getEvenHatchFill = function() { return this.evenHatchFill_; };

anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.hasEvenHatchFill = function() { return this.evenHatchFill_ != null; };
/**
 * ??? Grid fill settings.
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.oddFill_ = null;
/**
 * TODO: description
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.getOddFill = function() { return this.oddFill_; };

anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.hasOddFill = function() { return this.oddFill_ != null; };

/**
 * ??? Grid hatch fill settings.
 * @private
 * @type {anychart.visual.hatchFill.HatchFill
 * }
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.oddHatchFill_ = null;
/**
 * TODO: description
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.getOddHatchFill = function() { return this.oddHatchFill_; };


anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.hasOddHatchFill = function() { return this.oddHatchFill_ != null; };
/**
 * Deserialize axis grid settings.
 * @public
 * @param {Object} data JSON object with axis grid settings.
 */
anychart.plots.axesPlot.axes.elements.AxisGrid.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    //common settings
    if (des.hasProp(data, 'enabled')) this.enabled_ = des.getBoolProp(data, 'enabled');
    if (des.hasProp(data, 'interlaced')) this.interlaced_ = des.getBoolProp(data, 'interlaced');
    if (des.hasProp(data, 'draw_first_line')) this.drawFirstLine_ = des.getBoolProp(data, 'draw_first_line');
    if (des.hasProp(data, 'draw_last_line')) this.drawLastLine_ = des.getBoolProp(data, 'draw_last_line');
    
    if (des.isEnabled(des.getProp(data, 'line'))) {
        this.line_ = new anychart.visual.stroke.Stroke();
        this.line_.deserialize(des.getProp(data, 'line'));
    }

    if (this.interlaced_ && des.hasProp(data, 'interlaced_fills')) {
        var interlacedFills = des.getProp(data, 'interlaced_fills');
        //even settings
        if (des.hasProp(interlacedFills, 'even')) {
            var even = des.getProp(interlacedFills, 'even');
            if (des.hasProp(even, 'fill') && des.isEnabled(des.getProp(even, 'fill'))) {
                this.evenFill_ = new anychart.visual.fill.Fill();
                this.evenFill_.deserialize(des.getProp(even, 'fill'));
            }
            if (des.hasProp(even, 'hatch_fill') && des.isEnabled(des.getProp(even, 'hatch_fill'))) {
                this.evenHatchFill_ = new anychart.visual.hatchFill.HatchFill();
                this.evenHatchFill_.deserialize(des.getProp(even, 'hatch_fill'));
            }
        }
        //odd settings
        if (des.hasProp(interlacedFills, 'odd')) {
            var odd = des.getProp(interlacedFills, 'odd');
            if (des.hasProp(odd, 'fill') && des.isEnabled(des.getProp(odd, 'fill'))) {
                this.oddFill_ = new anychart.visual.fill.Fill();
                this.oddFill_.deserialize(des.getProp(odd, 'fill'));
            }
            if (des.hasProp(odd, 'hatch_fill') && des.isEnabled(des.getProp(odd, 'hatch_fill'))) {
                this.oddHatchFill_ = new anychart.visual.hatchFill.HatchFill();
                this.oddHatchFill_.deserialize(des.getProp(odd, 'hatch_fill'));
            }
        }
    }
};

//-----------------------------------------------------------------------------------
//
//                          AxisTickmark class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent axis tickmark.
 * @public
 * @constructor
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark = function() {
    this.stroke_ = new anychart.visual.stroke.Stroke();
    this.stroke_.setCaps(anychart.visual.stroke.Stroke.CapsType.NONE);
};

/**
 * Tickmark stroke settings.
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.stroke_ = null;

anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.hasStroke = function() {
    return this.stroke_ != null;
};
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.getStroke = function() {
    return this.stroke_;
};

/**
 * Tickmark size.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.size_ = 10;
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.getSize = function() { return this.size_; };

/**
 * Tickmark inside.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.inside_ = false;
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.isInside = function() { return this.inside_; };

/**
 * Tickmark outside.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.outside_ = true;
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.isOutside = function() { return this.outside_; };

/**
 * Tickmark opposite.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.opposite_ = false;

/**
 * Tickmark bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.bounds_ = null;

/**
 * Deserialize tickmark settings
 * @public
 * @param {object} data JSON object with tickmark settings.
 */
anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    this.stroke_.deserialize(data);
    if (des.hasProp(data, 'size')) this.size_ = des.getNumProp(data, 'size');
    if (des.hasProp(data, 'inside')) this.inside_ = des.getBoolProp(data, 'inside');
    if (des.hasProp(data, 'outside')) this.outside_ = des.getBoolProp(data, 'outside');
    if (des.hasProp(data, 'opposite')) this.opposite_ = des.getBoolProp(data, 'opposite');
    if (!this.inside_ && !this.outside_ && !this.opposite_)
        this.stroke_ = null;
    else
        this.bounds_ = new anychart.utils.geom.Rectangle();
};

anychart.plots.axesPlot.axes.elements.AxisTickmark.prototype.draw = function(container, isGradient, pixValue, offset, viewPort, style) {
    var thickness = this.stroke_.getThickness();

    var strokeStyle;
    var path;

    if (this.inside_) {
        if (isGradient)
            viewPort.setInsideAxisTickmarkBounds(thickness, pixValue, offset, this.size_, this.bounds_);

        strokeStyle = style || this.stroke_.getSVGStroke(container.getSVGManager(), this.bounds_);

        if (strokeStyle != null && strokeStyle.length > 0) {
            path = viewPort.svg_createInsideAxisTickmark(container.getSVGManager(), pixValue, offset, this.size_, this.stroke_.getThickness());
            path.setAttribute('style', strokeStyle);
            container.appendChild(path);
        }
    }

    if (this.outside_) {
        if (isGradient)
            viewPort.setOutsideAxisTickmarkBounds(thickness, pixValue, offset, this.size_, this.bounds_);

        strokeStyle = style || this.stroke_.getSVGStroke(container.getSVGManager(), this.bounds_);

        if (strokeStyle != null && strokeStyle.length > 0) {
            path = viewPort.svg_createOutsideAxisTickmark(container.getSVGManager(), pixValue, offset, this.size_, this.stroke_.getThickness());
            path.setAttribute('style', strokeStyle);
            container.appendChild(path);
        }
    }

    if (this.opposite_) {
        if (isGradient)
            viewPort.setOpposideAxisTickmarkBounds(thickness, pixValue, this.size_, this.bounds_);

        strokeStyle = style || this.stroke_.getSVGStroke(container.getSVGManager(), this.bounds_);

        if (strokeStyle != null && strokeStyle.length > 0) {
            path = viewPort.svg_createOppositeAxisTickmark(container.getSVGManager(), pixValue, offset, this.size_, this.stroke_.getThickness());
            path.setAttribute('style', strokeStyle);

            container.appendChild(path);
        }
    }
};

//-----------------------------------------------------------------------------------
//
//                          AxisTitle class
//
//-----------------------------------------------------------------------------------
/**
 * @public
 * @constructor
 * @param {anychart.plots.axesPlot.axes.Axis} axis
 * @param {anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort} viewPort
 * @extends {anychart.visual.text.BaseTitle}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle = function(axis, viewPort) {
    anychart.visual.text.BaseTitle.apply(this);
    this.axis_ = axis;
    this.viewPort_ = viewPort;
    this.position_ = new anychart.utils.geom.Point();
    this.viewPort_.getTitleViewPort().setTitleRotation(this);
};

goog.inherits(anychart.plots.axesPlot.axes.elements.AxisTitle, anychart.visual.text.BaseTitle);

/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.offset_ = 0;
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.getOffset = function() { return this.offset_; };
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.setOffset = function(value) { this.offset_ = value; };

/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.space_ = 0;
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.getSpace = function() { return this.space_; };

/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.position_ = null;

/**
 * @private
 * @type {anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.viewPort_ = null;

/**
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.axis_ = null;

anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.getTextFormatter = function() {
    return this.textFormatter_;
};


/** @inheritDoc */
anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.deserialize = function(data) {
    var rotation = this.getRotationAngle();
    goog.base(this, 'deserialize', data);
    this.setRotationAngle(-rotation - this.getRotationAngle());
};

anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.initialize = function(svgManager) {
    this.initSize(svgManager, this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale()));
    this.space_ = this.viewPort_.getLabelsViewPort().getTextSpace(this.getBounds()) + this.padding_;
    return this.space_;
};

anychart.plots.axesPlot.axes.elements.AxisTitle.prototype.drawAxisTitle = function(container) {
    this.viewPort_.getTitleViewPort().setTitlePosition(this, this.position_);
    var el = this.createSVGText(container.getSVGManager());
    el.setPosition(this.position_.x, this.position_.y);
    container.appendSprite(el);
};