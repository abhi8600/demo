/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.line.styles.LineStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.line.styles.LineStyle};</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.line.styles');
goog.require('anychart.styles.line');
//------------------------------------------------------------------------------
//
//                          LineStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.line.styles.LineStyleShape = function() {
    anychart.styles.line.LineStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.styles.LineStyleShape,
    anychart.styles.line.LineStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.styles.LineStyleShape.prototype.updatePath = function() {
    this.point_.calcBounds();
    if (this.point_.isLineDrawable())
        this.element_.setAttribute(
            'points',
            this.styleShapes_.getElementPath());
};
//------------------------------------------------------------------------------
//
//                       LineStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyleShapes}
 */
anychart.plots.axesPlot.series.line.styles.LineStyleShapes = function() {
    anychart.styles.line.LineStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.styles.LineStyleShapes,
    anychart.styles.line.LineStyleShapes);
//------------------------------------------------------------------------------
//                      Initialization.
//-----------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.styles.LineStyleShapes.prototype.createShapes = function() {
    this.lineShape_ = new anychart.plots.axesPlot.series.line.styles.LineStyleShape();
    this.lineShape_.initialize(this.point_, this);
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.styles.LineStyleShapes.prototype.createElement = function() {
    return this.point_.getSVGManager().createPolyline();
};
/**
 * @return {String}
 */
anychart.plots.axesPlot.series.line.styles.LineStyleShapes.prototype.getElementPath = function() {
    var svgManager = this.point_.getSVGManager();
    var drawingPoints = this.point_.getDrawingPoints();
    if (!drawingPoints) 
    	return;
    	
    var pointsCount = drawingPoints.length;
    if (pointsCount <= 1)
    	return;

    var pathData = svgManager.polyLineDraw(
        drawingPoints[0].x,
        drawingPoints[0].y);

    for (var i = 1; i < pointsCount; i++)
        pathData += svgManager.polyLineDraw(
            drawingPoints[i].x,
            drawingPoints[i].y);

    return pathData;
};
//------------------------------------------------------------------------------
//
//                       LineStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyle}
 */
anychart.plots.axesPlot.series.line.styles.LineStyle = function() {
    anychart.styles.line.LineStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.styles.LineStyle,
    anychart.styles.line.LineStyle);
//------------------------------------------------------------------------------
//                          StyleShapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.styles.LineStyle.prototype.createStyleShapes = function(point) {
    return new anychart.plots.axesPlot.series.line.styles.LineStyleShapes()
};
