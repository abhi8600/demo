/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot');
goog.require('anychart.plots.treeMapPlot.data');
goog.require('anychart.plots.treeMapPlot.data.tree');
goog.require('anychart.plots.treeMapPlot.elements');
goog.require('anychart.plots.treeMapPlot.styles');
goog.require('anychart.plots.treeMapPlot.templates');
//------------------------------------------------------------------------------
//
//                          TreeMapPlot class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.treeMapPlot.TreeMapPlot = function () {
    anychart.plots.seriesPlot.SeriesPlot.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.TreeMapPlot,
    anychart.plots.seriesPlot.SeriesPlot);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.ITreeMapItem}
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.rootTreeMapItem_ = null;
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.ITreeMapItem}
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.currRoot_ = null;
/**
 * @return {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.getDrawingPoints = function () {
    return this.drawingPoints_;
};
//------------------------------------------------------------------------------
//                          Templates.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.getTemplatesMerger = function () {
    return new anychart.plots.treeMapPlot.templates.TreeMapTemplateMerger();
};
//------------------------------------------------------------------------------
//                      Global settings.
//------------------------------------------------------------------------------
/**
 * @return {anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings}
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.getGlobalSettings = function () {
    return this.getCashedGlobalSeriesSettings_(anychart.series.SeriesType.HEAT_MAP);
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.createGlobalSeriesSettings = function (seriesType) {
    return new anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings();
};
//------------------------------------------------------------------------------
//                     Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.deserializeData_ = function (config, stylesList) {
    var des = anychart.utils.deserialization;
    var data = des.getProp(config, 'data');

    //palettes list
    var palettesList = new anychart.palettes.PalettesCollection();
    if (des.hasProp(config, 'palettes'))
        palettesList.deserialize(des.getProp(config, 'palettes'));

    //threshold list
    this.thresholdsList_ = new anychart.thresholds.ThresholdsList(des.getProp(config, 'thresholds'));

    //global settings
    var seriesSettingsNode = des.getProp(config, 'data_plot_settings');
    var globalSettings = this.getGlobalSeriesSettings_(
        seriesSettingsNode,
        data,
        anychart.series.SeriesType.HEAT_MAP,
        stylesList);

    //deserialization properties
    var desProp = new anychart.plots.treeMapPlot.data.TreeMapDesProp(
        0,
        data,
        stylesList,
        palettesList,
        null);

    //tree map factory
    var treeMapFactory = new anychart.plots.treeMapPlot.data.tree.TreeMapFactory();
    treeMapFactory.deserialize(data);

    //root item
    this.rootTreeMapItem_ = treeMapFactory.createTree(desProp, globalSettings);
    this.setRoot(this.rootTreeMapItem_);
    //thresholds list
    this.thresholdsList_.initializeAutomaticThresholds();
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.onAfterDeserializeData = function (chart) {
};
//------------------------------------------------------------------------------
//                        Current root item.
//------------------------------------------------------------------------------
/**
 * Set current root element of tree.
 * @param {anychart.plots.treeMapPlot.data.ITreeMapItem} root
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.setRoot = function (root) {
    if (this.currRoot_) this.currRoot_.setVisibility(false);
    this.currRoot_ = root;
    this.onBeforeDrawSeries_[0] = this.currRoot_;
    this.currRoot_.setVisibility(true);
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------

anychart.plots.treeMapPlot.TreeMapPlot.prototype.initialize = function (bounds, tooltipContainer, opt_drawBackground) {
    goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);
    this.sendBounds_();
    this.sprite_.updateClipPath(this.getBackgroundBounds());
    return this.sprite_;
};
/**@inheritDoc*/
anychart.plots.treeMapPlot.TreeMapPlot.prototype.initializeBottomPlotContainers_ = function () {
};
/**@inheritDoc*/
anychart.plots.treeMapPlot.TreeMapPlot.prototype.getBackgroundBounds = function () {
    return new anychart.utils.geom.Rectangle(0, 0, this.bounds_.width, this.bounds_.height)
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.resizeSeries = function () {
    this.sendBounds_();
    goog.base(this, 'resizeSeries');
    this.sprite_.updateClipPath(this.getBackgroundBounds());
};
//------------------------------------------------------------------------------
//                              Bounds.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.sendBounds_ = function () {
    if (!this.rootTreeMapItem_) return;
    this.currRoot_.setX(0);
    this.currRoot_.setY(0);
    this.currRoot_.setWidth((this.bounds_.width < 1) ? 1 : this.bounds_.width - 1);
    this.currRoot_.setHeight((this.bounds_.height < 1) ? 1 : this.bounds_.height - 1);
};
//------------------------------------------------------------------------------
//                  DrillDown.
//------------------------------------------------------------------------------
/**
 *
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.drillDown = function (newRoot) {
    if (newRoot == this.currRoot_) {
        if (newRoot != this.rootTreeMapItem_) this.setRoot(newRoot.getParent());
    } else this.setRoot(newRoot);
    this.resizeSeries();
};
//------------------------------------------------------------------------------
//                          Check no data.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.TreeMapPlot.prototype.checkNoData = function (data) {
    var des = anychart.utils.deserialization;
    return !data || !des.hasProp(data, 'data') || !des.hasProp(des.getProp(data, 'data'), 'point');
};

