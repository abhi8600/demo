/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.data.tree');
//------------------------------------------------------------------------------
//
//                      TreeOrientation class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapInputType = {
    'BY_TREE':0,
    'BY_PARENT':1,
    'BY_LEVEL':2
};
anychart.plots.treeMapPlot.data.tree.TreeMapInputType.deserialize = function (value) {
    switch (value) {
        case 'byparent':
            return anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_PARENT;

        case 'bylevel':
            return anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_LEVEL;

        default:
        case 'bytree':
            return anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_TREE;
    }
};
//------------------------------------------------------------------------------
//
//                          TreeItem class.
//
//------------------------------------------------------------------------------
/**
 * Used for input type "by_tree"
 * @constructor
 */
anychart.plots.treeMapPlot.data.tree.TreeItem = function() {
    this.children_ = [];
};
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.data_ = null;
/**
 * @return {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.getData = function() {
    return this.data_
};
/**
 * @param {Object} value
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.setData = function(value) {
    this.data_ = value;
};
/**
 * @private
 * @type {Array.<Object>}
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.children_ = null;
/**
 * @return {Array.<Object>}
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.getChildren = function() {
    return this.children_;
};
/**
 * @param {Object} child
 */
anychart.plots.treeMapPlot.data.tree.TreeItem.prototype.pushChild = function(child) {
    this.children_.push(child);
};
//------------------------------------------------------------------------------
//
//                      TreeMapFactory class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory = function () {

};
//------------------------------------------------------------------------------
//                          Factory properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.tree.TreeMapInputType}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.inputType_ = null;
//------------------------------------------------------------------------------
//                      Iterator properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.pointsData_ = null;
/**
 * @private
 * @type {int}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.pointsCount_ = null;
/**
 * @private
 * @type {int}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.i_ = null;
//------------------------------------------------------------------------------
//                  Parse tree properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.root_ = null;
/**
 * @private
 * @type {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.namedHash_ = null;
/**
 * @private
 * @type {Array.<anychart.plots.treeMapPlot.data.tree.TreeItem>}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.summaryHash_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize TreeMapFactory settings.
 * @param {Object} data
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    this.inputType_ = anychart.plots.treeMapPlot.data.tree.TreeMapInputType.deserialize(
        des.getEnumProp(data, 'input_mode'));

    switch (this.inputType_) {
        case anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_LEVEL:
            this.initIteratorProp_(data);
            break;
        case anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_PARENT:
            this.parseTree_(data);
            break;
    }
};
/**
 * Initialize iterator properties
 * @private
 * @param {Object} data
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.initIteratorProp_ = function (data) {
    this.i_ = 0;
    this.pointsData_ = anychart.utils.deserialization.getPropArray(data, 'point');
    this.pointsCount_ = this.pointsData_.length;

};
/**
 * Parse points tree.
 * @private
 * @param {Object} data
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.parseTree_ = function (data) {
    var des = anychart.utils.deserialization;
    var pointsData = des.getPropArray(data, 'point');
    var pointsCount = pointsData.length;

    this.namedHash_ = {};
    this.summaryHash_ = [];

    var node;
    var i;

    for (i = 0; i < pointsCount; i++) {
        var pointData = pointsData[i];
        node = new anychart.plots.treeMapPlot.data.tree.TreeItem();
        node.setData(pointData);
        this.summaryHash_.push(node);
        if (des.hasProp(pointData, 'id'))
            this.namedHash_[des.getStringProp(pointData, 'id')] = node;
    }

    this.root_ = new anychart.plots.treeMapPlot.data.tree.TreeItem();
    this.root_.setData(data);
    for (i = 0; i < pointsCount; i++) {
        node = this.summaryHash_[i];
        var nodeData = node.getData();
        if (des.hasProp(nodeData, 'parent')) {
            var parentId = des.getStringProp(nodeData, 'parent');
            if (this.namedHash_[parentId]) {
                this.namedHash_[parentId].pushChild(node);
                continue;
            }
        }
        this.root_.pushChild(node);
    }

    this.clearArray_(this.summaryHash_);
    this.clearHash_(this.namedHash_);
};
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.clearArray_ = function (array) {
    if (array == null) return;
    var ln = array.length;
    for (var i = 0; i < ln; i++) {
        array[i] = null;
        delete array[i];
    }
}

anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.clearHash_ = function (hash) {
    if (hash == null) return;
    for (var el in hash) {
        hash[el] = null;
        delete hash[el];
    }
}
//------------------------------------------------------------------------------
//                              Tree.
//------------------------------------------------------------------------------
/**
 * Create tree depends input type.
 * @param {anychart.plots.treeMapPlot.data.TreeMapDesProp} desProp
 * @param {anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings} globalSettings
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.createTree = function (desProp, globalSettings) {
    switch (this.inputType_) {
        case anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_LEVEL:
            return this.createByLevel_(desProp, globalSettings);
            break;
        case anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_PARENT:
            return this.createByParent_(this.root_, desProp, null, globalSettings, false);
            break;
        case anychart.plots.treeMapPlot.data.tree.TreeMapInputType.BY_TREE:
            return this.createByTree_(desProp, null, globalSettings, false);
    }
};
//------------------------------------------------------------------------------
//                  BY_LEVEL input mode.
//------------------------------------------------------------------------------
/**
 *
 * @param node
 * @param desProp
 * @param opt_parent
 * @param opt_globalSettings
 * @param opt_drawSelfHeader
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.createByParent_ = function (node, desProp, opt_parent, opt_globalSettings, opt_drawSelfHeader) {
    if (!node) return null;
    if (opt_drawSelfHeader == null || opt_drawSelfHeader == undefined)
        opt_drawSelfHeader = true

    var des = anychart.utils.deserialization;
    var pointsList = node.getChildren();
    var pointsCount = pointsList.length;
    var res;
    desProp.data = node.getData();
    if (pointsCount > 0 || opt_globalSettings) {
        res = new anychart.plots.treeMapPlot.data.TreeMapSeries();
        res.setParent(opt_parent);
        var series = res;
        series.setDrawSelfHeader(opt_drawSelfHeader);

        if (opt_globalSettings) {
            series.setGlobalSettings(opt_globalSettings);
            series.setActionsList(opt_globalSettings.getActionsList());
            series.setInteractivitySettings(opt_globalSettings);
            series.setStyle(opt_globalSettings.getStyle());
            opt_globalSettings.setElements(series)
        } else if(opt_parent) opt_parent.setElements(series);


        var threshold = desProp.threshold;
        series.deserializeData(desProp);

        var points = [];
        series.setPoints(points);
        series.setValue(0);
        for (var i = 0; i < pointsCount; i++) {
            var subNode = pointsList[i];
            desProp.index = i;
            desProp.data = subNode.getData();
            var point = this.createByParent_(subNode, desProp, series);
            if (point.getValue() != 0) {
                series.incValue(point.getValue());
                points.push(point);
            }
        }

        desProp.threshold = threshold;
        series.getGlobalSettings().sortPoints(series.getPoints());

        series.sendValueToPoint();
    } else {
        res = new anychart.plots.treeMapPlot.data.TreeMapPoint();
        res.setParent(opt_parent);
        res.deserializeData(desProp);
    }
    return res;
}
//------------------------------------------------------------------------------
//                  BY_LEVEL input mode.
//------------------------------------------------------------------------------
/**
 *
 * @param desProp
 * @param globalSettings
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.createByLevel_ = function (desProp, globalSettings) {
    var des = anychart.utils.deserialization;
    var pointsData = des.getPropArray(desProp.data, 'point');
    var pointsCount = pointsData.length;
    var series;

    if (pointsCount > 0 && globalSettings) {
        series = new anychart.plots.treeMapPlot.data.TreeMapSeries();
        series.setDrawSelfHeader(false);
        series.setParent(null);

        series.setGlobalSettings(globalSettings);
        series.setActionsList(globalSettings.getActionsList());
        series.setInteractivitySettings(globalSettings);
        series.setStyle(globalSettings.getStyle());
        globalSettings.setElements(series)

        var threshold = desProp.threshold;
        series.deserializeData(desProp);

        var points = [];
        series.setPoints(points)
        series.setValue(0);
        var i = 0;
        do {
            desProp.index = i++;
            desProp.data = this.getNext_(-Number.MAX_VALUE);
            if (!desProp.data) break;
            var point = this.createElement_(desProp, series, -Number.MAX_VALUE);
            series.incValue(point.getValue());
            series.getPoints().push(point);
        } while (true);

        desProp.threshold = threshold;
        series.getGlobalSettings().sortPoints(series.getPoints());
        series.sendValueToPoint();
    } else series = null;
    return series;
};
/**
 * @param {Number} parentLevel
 * @return {Object}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.getNext_ = function (parentLevel) {
    if (this.i_ < this.pointsCount_)
        if (this.getLevel_(this.pointsData_[this.i_]) <= parentLevel) return null;
        else  return this.pointsData_[this.i_++];
    else return null;
};

/**
 * @param {Object} pointData
 * @param {Number}
    */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.getLevel_ = function (pointData) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(pointData, 'level')) return 0;
    return des.getNumProp(pointData, 'level')
};
/**
 * @param {Number} currLevel
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.lookupForChildren_ = function (currLevel) {
    if (this.i_ >= this.pointsCount_) return false;
    return this.getLevel_(this.pointsData_[this.i_]) > currLevel;
};
/**
 *
 * @param deserParams
 * @param parent
 * @param parentLevel
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.createElement_ = function (deserParams, parent, parentLevel) {
    var res;
    var currLevel = this.getLevel_(deserParams.data);
    if (this.lookupForChildren_(currLevel)) {
        res = new anychart.plots.treeMapPlot.data.TreeMapSeries();
        res.setParent(parent);
        var series = res;

        series.setGlobalSettings(parent.getGlobalSettings());
        series.setActionsList(parent.getGlobalSettings().getActionsList());
        series.setInteractivitySettings(parent.getGlobalSettings());
        series.setStyle(parent.getGlobalSettings().getStyle());
        parent.getGlobalSettings().setElements(series)

        series.deserializeData(deserParams);

        var points = []
        series.setPoints(points);
        series.setValue(0);
        var i = 0;
        do {
            deserParams.index = i++;
            deserParams.data = this.getNext_(currLevel);
            if (!deserParams.data) break;
            var point = this.createElement_(deserParams, series, currLevel);
            series.incValue(point.getValue());
            points.push(point);
        } while (true);

        series.getGlobalSettings().sortPoints(series.getPoints());
        series.sendValueToPoint();
    } else {
        res = new anychart.plots.treeMapPlot.data.TreeMapPoint();
        res.setParent(parent);
        res.deserializeData(deserParams);
    }
    return res;
};
//------------------------------------------------------------------------------
//                      BY_TREE input mod
//------------------------------------------------------------------------------
/**
 *
 * @param desProp
 * @param opt_parent
 * @param opt_globalSettings
 * @param opt_drawSelfHeader
 */
anychart.plots.treeMapPlot.data.tree.TreeMapFactory.prototype.createByTree_ = function (desProp, opt_parent, opt_globalSettings, opt_drawSelfHeader) {
    if (!desProp) return null;
    if (opt_drawSelfHeader == null || opt_drawSelfHeader == undefined) opt_drawSelfHeader = true;

    var res;
    var des = anychart.utils.deserialization;
    var pointsData = des.getPropArray(desProp.data, 'point');
    var pointsCount = pointsData.length;

    if (pointsCount > 0 || opt_globalSettings) {
        res = new anychart.plots.treeMapPlot.data.TreeMapSeries();
        res.setParent(opt_parent);
        var series = res;
        series.setDrawSelfHeader(opt_drawSelfHeader);

        if (opt_globalSettings) {
            series.setGlobalSettings(opt_globalSettings);
            series.setActionsList(opt_globalSettings.getActionsList());
            series.setInteractivitySettings(opt_globalSettings);
            series.setStyle(opt_globalSettings.getStyle());
            opt_globalSettings.setElements(series)
        } else if (opt_parent) opt_parent.setElements(series)


        var threshold = desProp.threshold;
        series.deserializeData(desProp);

        var points = [];
        series.setPoints(points);
        series.setValue(0);

        for (var i = 0; i < pointsCount; i++) {
            desProp.index = i;
            desProp.data = pointsData[i];
            var point = this.createByTree_(desProp, series);
            series.incValue(point.getValue());
            points.push(point);
        }

        desProp.threshold = threshold;
        series.getGlobalSettings().sortPoints(series.getPoints());
        series.sendValueToPoint();

    } else {
        res = new anychart.plots.treeMapPlot.data.TreeMapPoint();
        res.setParent(opt_parent);
        res.deserializeData(desProp);
    }
    return res;


};
