/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.funnelPlot.data.FunnelPoint}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel2DPoint}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel3DPoint}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel3DDataIsWidthPoint}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.FunnelSeries}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel2DSeries}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel3DSeries}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries}</li>
 *  <li>@class {anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings}</li>.
 * <ul>
 */

goog.provide('anychart.plots.funnelPlot.data');

goog.require('anychart.plots.funnelPlot.drawingInfo');
goog.require('anychart.plots.funnelPlot.styles');
goog.require('anychart.plots.percentBasePlot.labels');
goog.require('anychart.plots.percentBasePlot.data');
goog.require('anychart.series');

//------------------------------------------------------------------------------
//
//                            FunnelPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.data.PercentBasePoint}
 */
anychart.plots.funnelPlot.data.FunnelPoint = function() {
    this.drawingInfo_ = new anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo();
    anychart.plots.percentBasePlot.data.PercentBasePoint.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.FunnelPoint,
        anychart.plots.percentBasePlot.data.PercentBasePoint);

//------------------------------------------------------------------------------
//                                   Properties
//------------------------------------------------------------------------------

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.centerHeight_ = null;

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.centerWidth_ = null;

/**
 * @type {Boolean}
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.is3D_ = null;
anychart.plots.funnelPlot.data.FunnelPoint.prototype.is3D = function() {
    return this.is3D_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.pointWidth_ = null;

//------------------------------------------------------------------------------
//                         Overrides, methods, etc..
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.setBounds = function() {
    var neckHeight = this.globalSettings_.getNeckHeight();
    var h = this.series_.getHeight();

    this.bounds_.x = Math.min(
            this.drawingInfo_.getLeftCenter().x,
            this.drawingInfo_.getLeftTop().x,
            this.drawingInfo_.getLeftBottom().x);

    if (this.bottomHeight_ < neckHeight && this.topHeight_ < neckHeight) {
        this.bounds_.width = this.drawingInfo_.getRightTop().x - this.drawingInfo_.getLeftBottom().x
    } else {
        this.bounds_.width = Math.max(
                this.drawingInfo_.getRightTop().x - this.drawingInfo_.getLeftTop().x,
                this.drawingInfo_.getRightBottom().x - this.drawingInfo_.getLeftBottom().x);
    }
    this.bounds_.y = Math.min(this.drawingInfo_.getLeftBottom().y,
            this.drawingInfo_.getLeftCenter().y,
            this.drawingInfo_.getLeftTop().y);
    this.bounds_.height = h * this.percentHeight_;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.setSectorXValue = function() {
    if (this.globalSettings_.isDataIsWidth()) {//получаем значения точек в секторах
        if (this.index_ != this.series_.getPoints().length - 1) {
            this.setSectorYWidth_();
        }
    } else {
        this.setSectorYHeight_();
    }
};

/**
 * @private
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.setSectorYHeight_ = function() {
    var neckHeight = this.globalSettings_.getNeckHeight();
    var baseWidth = this.globalSettings_.getBaseWidth();
    var MYCONST = (baseWidth * (1 - neckHeight)) / (1 - baseWidth);
    var inverted = this.globalSettings_.isInverted();

    this.bottomWidth_ = inverted ?
            ((this.bottomHeight_ - neckHeight + MYCONST) /
                    (1 - neckHeight + MYCONST)) :
            ((this.topHeight_ - neckHeight + MYCONST) /
                    (1 - neckHeight + MYCONST));
    this.topWidth_ = inverted ?
            ((this.topHeight_ - neckHeight + MYCONST) /
                    (1 - neckHeight + MYCONST)) :
            ((this.bottomHeight_ - neckHeight + MYCONST) /
                    (1 - neckHeight + MYCONST));
    this.centerHeight_ = this.topHeight_;
    this.centerWidth_ = inverted ? this.topWidth_ : this.bottomWidth_;

    if (this.topHeight_ < neckHeight && this.bottomHeight_ < neckHeight) {
        this.centerWidth_ = this.topWidth_ = this.bottomWidth_ = baseWidth;
    } else if (this.topHeight_ < neckHeight) {
        this.centerHeight_ = neckHeight;
        this.centerWidth_ = baseWidth;
        if (inverted) {
            this.topWidth_ = baseWidth;
        } else {
            this.bottomWidth_ = baseWidth;
        }
    }
};

/**
 * @private
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.setSectorYWidth_ = function() {
    var k = this.index_;
    var inverted = this.globalSettings_.isInverted();
    var baseWidth = this.globalSettings_.getBaseWidth();
    this.bottomWidth_ = (this.getCalculationValue() - this.series_.tokensHash_.get('%SeriesYMin')) /
            (this.series.tokensHash_.get('%SeriesYMax') -
                    this.series.tokensHash_.get('%SeriesYMin')) *
            (1 - baseWidth) + baseWidth;
    if (this.bottomWidth_ < baseWidth) this.bottomWidth_ = baseWidth;
    if (this.series_.getPointAt(k + 1) != null) {
        this.topWidth_ = this.series_.getPointAt(k + 1).getCalculationValue() -
                this.series_.tokensHash_.get('%SeriesYMin') /
                        (this.series_.tokensHash_.get('%SeriesYMax') -
                                this.series_.tokensHash_.get('%SeriesYMin')) *
                        (1 - baseWidth) + baseWidth;
        if (this.topWidth_ < baseWidth) this.topWidth_ = baseWidth;
    } else {
        this.topWidth_ = (this.getCalculationValue() -
                this.series_.tokensHash_.get('%SeriesYMin')) /
                (this.series_.tokensHash_.get('%SeriesYMax') -
                        this.series_.tokensHash_.get('%SeriesYMin')) *
                (1 - baseWidth) + baseWidth;
    }
};

/**
 * @private
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.getRealPercentHeight_ = function(padding, size3D) {
    var dataIsWidth = this.globalSettings_.isDataIsWidth();
    var baseWidth = this.globalSettings_.getBaseWidth();
    var inverted = this.globalSettings_.isInverted();
    var maxWidth = this.series_.getPointAt(0).getCalculationValue() /
            this.series_.tokensHash_.get('%SeriesYSum');


    if (this.globalSettings_.getBaseWidth() == 0 && !dataIsWidth) {
        return (1 - (padding) * (this.series_.getVisiblePoints() - 1));
    } else {
        if (inverted) {
            return (1 - padding * (this.series_.getVisiblePoints() - 1) -
                    baseWidth * size3D * 2);
        } else return (1 - padding * (this.series_.getVisiblePoints() - 1) -
                size3D * 2);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.getLabelPosition = function(state, element, bounds) {
    var placement = this.globalSettings_.getLabelsPlacementMode();
    var pos = new anychart.utils.geom.Point();
    var neckHeight = this.globalSettings_.getNeckHeight();

    var baseWidth = this.globalSettings_.getBaseWidth();
    var MYCONST = (baseWidth * (1 - neckHeight)) / (1 - baseWidth);
    var inverted = this.globalSettings_.isInverted();
    var lpm = anychart.plots.percentBasePlot.labels.LabelsPlacementMode;
    if (placement == lpm.INSIDE) {
        return this.getElementPosition(element, state, bounds);//todo wtd?!
    }
    if (placement == lpm.OUTSIDE_LEFT) {
        pos.x = (this.drawingInfo_.getLeftTop().x +
                this.drawingInfo_.getLeftBottom().x) / 2;
        pos.y = (this.drawingInfo_.getLeftTop().y +
                this.drawingInfo_.getLeftBottom().y) / 2;
        if (this.centerHeight_ != this.topHeight_) {
            if (pos.y >= this.drawingInfo_.getRightCenter().y) {
                pos.x = inverted ? (this.drawingInfo_.getLeftCenter().x +
                        this.drawingInfo_.getLeftBottom().x) / 2 :
                        this.drawingInfo_.getLeftCenter().x;
            } else {
                pos.x = inverted ? this.drawingInfo_.getLeftCenter().x :
                        (this.drawingInfo_.getLeftCenter().x +
                                this.drawingInfo_.getLeftTop().x) / 2
            }
        }
        anychart.layout.HorizontalAlign.apply(pos, anychart.layout.HorizontalAlign.LEFT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    if (placement == lpm.OUTSIDE_RIGHT) {
        pos.x = (this.drawingInfo_.getRightTop().x +
                this.drawingInfo_.getRightBottom().x) / 2;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        if (this.centerHeight_ != this.topHeight_) {
            if (pos.y >= this.drawingInfo_.getRightCenter().y) {
                pos.x = inverted ? (this.drawingInfo_.getRightCenter().x +
                        this.drawingInfo_.getRightBottom().x) / 2 :
                        this.drawingInfo_.getRightCenter().x;
            } else {
                pos.x = inverted ? this.drawingInfo_.getRightCenter().x :
                        (this.drawingInfo_.getRightCenter().x +
                                this.drawingInfo_.getRightTop().x) / 2;
            }

        }
        anychart.layout.HorizontalAlign.apply(pos, anychart.layout.HorizontalAlign.RIGHT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    if (placement == lpm.OUTSIDE_RIGHT_IN_COLUMN && this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x + this.series_.getWidth() -
                this.label_.getBounds(this,
                        this.label_.getStyle().getNormal(), element).width;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    if (placement == lpm.OUTSIDE_LEFT_IN_COLUMN && this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    return pos;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelPoint.prototype.setAnchorPoint = function(pos, anchor, bounds, padding) {
    this.drawingInfo_.setAnchorPoint(anchor, pos, this.globalSettings_.isInverted())
};
//------------------------------------------------------------------------------
//
//                         Funnel2DPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelPoint}
 */
anychart.plots.funnelPlot.data.Funnel2DPoint = function() {
    anychart.plots.funnelPlot.data.FunnelPoint.call(this);
    this.is3D_ = false;
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel2DPoint,
        anychart.plots.funnelPlot.data.FunnelPoint);
//------------------------------------------------------------------------------
//                                Initialize
//------------------------------------------------------------------------------
anychart.plots.funnelPlot.data.Funnel2DPoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                               Drawing.
//------------------------------------------------------------------------------
/**
 * @return {String}
 */
anychart.plots.funnelPlot.data.Funnel2DPoint.prototype.getPathData = function () {
    return this.createPathData_();
};
/**
 * @return {String}
 */
anychart.plots.funnelPlot.data.Funnel2DPoint.prototype.createPathData_ = function() {
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(this.drawingInfo_.getLeftTop().x, this.drawingInfo_.getLeftTop().y);

    pathData += svgManager.pLine(this.drawingInfo_.getLeftCenter().x, this.drawingInfo_.getLeftCenter().y);
    pathData += svgManager.pLine(this.drawingInfo_.getLeftBottom().x, this.drawingInfo_.getLeftBottom().y);
    pathData += svgManager.pLine(this.drawingInfo_.getRightBottom().x, this.drawingInfo_.getRightBottom().y);
    pathData += svgManager.pLine(this.drawingInfo_.getRightCenter().x, this.drawingInfo_.getRightCenter().y);
    pathData += svgManager.pLine(this.drawingInfo_.getRightTop().x, this.drawingInfo_.getRightTop().y);
    pathData += svgManager.pLine(this.drawingInfo_.getLeftTop().x, this.drawingInfo_.getLeftTop().y);
    pathData += svgManager.pFinalize();

    return pathData;
};
//------------------------------------------------------------------------------
//                              Percent height.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DPoint.prototype.getRealPercentHeight_ = function(padding, size3D) {
    return (1 - padding * (this.series_.getVisiblePoints() - 1));
};
//------------------------------------------------------------------------------
//
//                         Funnel3DPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelPoint}
 */
anychart.plots.funnelPlot.data.Funnel3DPoint = function() {
    anychart.plots.funnelPlot.data.FunnelPoint.call(this);
    this.is3D_ = true;
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel3DPoint,
        anychart.plots.funnelPlot.data.FunnelPoint);

//todo drawing

//------------------------------------------------------------------------------
//
//                         Funnel2DDataIsWidthPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelPoint}
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint = function() {
    anychart.plots.funnelPlot.data.FunnelPoint.call(this);
    this.is3D_ = false;
    this.drawingInfo = new anychart.plots.funnelPlot.drawingInfo.
            FunnelDIWDrawingInfo();
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint,
        anychart.plots.funnelPlot.data.FunnelPoint);

//------------------------------------------------------------------------------
//                                Properties
//------------------------------------------------------------------------------

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.baseWidth_ = null;

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.setSectorYValue = function(dataIsWidth, minValue, padding, realSummHeight, size3D) {
    this.baseWidth_ = this.series_.tokensHash_.get('%SeriesYMin') /
            this.series_.tokensHash_.get('%SeriesYMax');
    var firstPoint = this.series_.getPointAt(0);
    var size3D;
    var inverted = this.globalSettings_.isInverted();
    var previousPoint = this.series_.getPointAt(this.index_ - 1);
    if (inverted) {
        size3D = firstPoint.getCalculationValue() /
                this.series_.tokensHash_.get('%SeriesYMax') *
                this.globalSettings_.getSize3D();
    } else size3D = 0;
    if (!this.is3D()) size3D = 0;

    this.percentHeight_ = 1 / (this.series_.getPoints().length - 1);
    this.percentHeight_ *= this.getRealPercentHeight_(padding, size3D);
    this.bottomHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() -
            this.percentHeight_);
    this.topHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() - padding);
    this.topHeight_ -= size3D * 2;
    this.bottomHeight_ -= size3D * 2;
    if (this.index_ == this.series_.getPoints().length - 1) {
        this.topHeight_ = this.bottomHeight_ = previousPoint.topHeight_;
        this.percentHeight_ = 0;
    }
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.getRealPercentHeight_ = function(padding, size3D) {
    var inverted = this.globalSettings_.isInverted();
    var size3D;
    var firstPoint = this.series_.getPointAt(0);
    var lastPoint = this.series_.getPointAt(
            this.series_.getPoints().length - 1);
    if (inverted) {
        size3D = firstPoint.getCalculationValue() / this.series_.tokensHash_.
                get('%SeriesYMax') *
                this.globalSettings_.getSize3D();
    } else size3D = lastPoint.getCalculationValue() / this.series_.tokensHash_.
            get('%SeriesYMax') *
            this.globalSettings_.getSize3D();
    if (!this.is3D()) size3D = 0;
    return (1 - padding * (this.series_.getPoints().length - 2) -
            size3D * 2);
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.setSectorXValue = function() {
    var index = this.index_;
    var nextPoint = this.series_.getPointAt(index + 1);
    this.bottomWidth_ = this.getCalculationValue() / this.series_.tokensHash_.
            get('%SeriesYMax');
    if (nextPoint != null) {
        this.topWidth_ = nextPoint.getCalculationValue() / this.series_.tokensHash_.
                get('%SeriesYMax');
    } else this.topWidth_ = this.bottomWidth_;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.setBounds = function() {
    var h = this.series_.getHeight();
    this.bounds_.x = Math.min(this.drawingInfo_.getLeftTop().x,
            this.drawingInfo_.getLeftBottom().x);
    this.bounds_.width = Math.max(this.drawingInfo_.getRightTop().x -
            this.drawingInfo_.getLeftTop().x,
            this.drawingInfo_.getRightBottom().x -
                    this.drawingInfo_.getLeftBottom().x);
    this.bounds_.y = Math.min(this.drawingInfo_.getLeftBottom().y,
            this.drawingInfo_.getLeftTop().y);
    this.bounds_.height = h * this.percentHeight_;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.prototype.getLabelPosition = function(state, element, bounds) {
    var placement = this.globalSettings_.getLabelsPlacementMode();
    var pos = new anychart.utils.geom.Point();
    var inverted = this.globalSettings_.isInverted();
    var lpm = anychart.plots.percentBasePlot.labels.LabelsPlacementMode;
    if (placement == lpm.INSIDE) {
        return this.getElementPosition(element, state, bounds);//todo wtd?!
    }
    if (placement == lpm.OUTSIDE_LEFT) {
        pos.x = (this.drawingInfo_.getLeftTop().x +
                this.drawingInfo_.getLeftBottom().x) / 2;
        pos.y = (this.drawingInfo_.getLeftTop().y +
                this.drawingInfo_.getLeftBottom().y) / 2;
        anychart.layout.HorizontalAlign.apply(pos, anychart.layout.HorizontalAlign.LEFT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
        if (pos.x < this.series_.getSeriesBounds().x) {
            pos.x = this.series_.getSeriesBounds().x;
        }
    }
    if (placement == lpm.OUTSIDE_RIGHT) {
        pos.x = (this.drawingInfo_.getRightTop().x +
                this.drawingInfo_.getRightBottom().x) / 2;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.HorizontalAlign.apply(pos, anychart.layout.HorizontalAlign.RIGHT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
        if (pos.x + this.label_.getBounds(this,
                this.label_.getStyle().getNormal(), element).width >
                this.series_.getSeriesBounds().x + this.series_.getWidth()) {
            pos.x = this.series_.getSeriesBounds().x +
                    this.series_.getWidth() - this.label_.getBounds(this,
                    this.label_.getStyle().getNormal(), element).width;
        }
    }
    if (placement == lpm.OUTSIDE_RIGHT_IN_COLUMN && this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x + this.series_.getWidth() -
                this.label_.getBounds(this,
                        this.label_.getStyle().getNormal(), element).width;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    if (placement == lpm.OUTSIDE_LEFT_IN_COLUMN && this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.VerticalAlign.CENTER,
                bounds,
                state.getPadding());
    }
    if (this.index_ == this.series_.getPoints().length - 1)
        pos.y = inverted ? (pos.y - bounds.height / 2) :
                (pos.y + bounds.height / 2);
    return pos;
};

//------------------------------------------------------------------------------
//
//                         Funnel3DDataIsWidthPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint}
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthPoint = function() {
    anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint.call(this);
    this.is3D_ = true;
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel3DDataIsWidthPoint,
        anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint);

//todo draw



//------------------------------------------------------------------------------
//
//                          FunnelSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.data.PercentBaseSeries}
 */
anychart.plots.funnelPlot.data.FunnelSeries = function() {
    anychart.plots.percentBasePlot.data.PercentBaseSeries.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.FunnelSeries,
        anychart.plots.percentBasePlot.data.PercentBaseSeries);

//------------------------------------------------------------------------------
//                           Overrides, methods, etc.
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelSeries.prototype.setLabelsPosition = function() {
    var tmpSprite = new anychart.svg.SVGSprite(this.getPlot().getSVGManager());
//        return;
    var labelPos = new anychart.utils.geom.Point();
    var labelRect = new anychart.utils.geom.Rectangle();
    var w = this.getWidth();
    var h = this.getHeight();
    var neckHeight = this.globalSettings_.getNeckHeight() * h;
    var baseWidth = this.globalSettings_.getBaseWidth() * w;
    var deltaH = neckHeight - (h - neckHeight) * baseWidth /
            (w - baseWidth);
    var tangens = w / (h - deltaH) / 2;
    var labelH;
    var pos;
    var labelPadding;
    for (var i = 0; i < this.points_.length; i++) {
        var point = this.points_[i];
        if (point.getLabel().isEnabled() && !point.isMissing()) {
            pos = (point.drawingInfo_.getRightBottom().x +
                    point.drawingInfo_.getRightTop().x) / 2;
            labelRect = point.getLabel().getBounds(point,
                    point.getLabel().getStyle().getNormal(),
                    tmpSprite);
            labelPos = point.getLabelPosition(
                    point.getLabel().getStyle().getNormal(),
                    null, labelRect);
            labelPadding = point.getLabel().getStyle().getNormal().
                    getPadding();
            labelH = (point.bottomHeight_ + point.topHeight_) * h / 2 -
                    deltaH;
            if (labelPos.x - labelRect.width < this.seriesBounds_.x ||
                    labelPos.x + labelRect.width > this.getWidth() ||
                    pos + labelRect.width > this.getWidth()) {
                tangens = Math.min(tangens,
                        (w - labelRect.width - labelPadding) /
                                ((h - deltaH) + labelH));
            }
        }
    }
    this.shiftNumber_ = 2 * (h - deltaH) * tangens;
};

anychart.plots.funnelPlot.data.FunnelSeries.prototype.deserialize = function(config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
};

//------------------------------------------------------------------------------
//
//                     FunnelGlobalSeriesSettings class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 *@extends {anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings}
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings = function() {
    anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings,
        anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings);

//------------------------------------------------------------------------------
//                                Properties
//------------------------------------------------------------------------------

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.baseWidth_ = null;

anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.getBaseWidth = function() {
    return this.baseWidth_;
};

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.neckHeight_ = null;

anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.getNeckHeight = function() {
    return this.neckHeight_;
};

/**
 * @type {int}
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.mode_ = null;

anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.getMode = function() {
    return this.mode_;
};

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.createSeries = function() {
    var series;
    if (this.plot_.is3D()) {
        if (this.isDataIsWidth()) {
            series = new anychart.plots.funnelPlot.data.
                    Funnel3DDataIsWidthSeries()
        } else series = new anychart.plots.funnelPlot.data.Funnel3DSeries()
    } else {
        if (this.isDataIsWidth()) {
            series = new anychart.plots.funnelPlot.data.
                    Funnel2DDataIsWidthSeries()
        } else series = new anychart.plots.funnelPlot.data.Funnel2DSeries()
    }
    series.setType(anychart.series.SeriesType.FUNNEL);
    return series;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return 'funnel_series';
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return 'funnel_style';
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.isProgrammaticStyle = function() {
    return false;
};

anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.deserialize = function(config, styleList) {
    goog.base(this, 'deserialize', config, styleList);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'min_width')) {
        this.baseWidth_ = des.getRatioProp(config, 'min_width');
    }
    if (des.hasProp(config, 'neck_height')) {
        this.neckHeight_ = des.getRatioProp(config, 'neck_height');
    }
    if (des.hasProp(config, 'mode')) {
        switch (des.getEnumProp(config, 'mode')) {
            case 'square' :
                this.mode_ = anychart.plots.funnelPlot.
                        FunnelMode.SQUARE;
                break;
            case 'circular' :
                this.mode_ = anychart.plots.funnelPlot.
                        FunnelMode.CIRCULAR;
                break;
        }
    }
};

anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings.prototype.createStyle = function(data) {
    return new anychart.plots.funnelPlot.styles.FunnelStyle();
};

//------------------------------------------------------------------------------
//
//                         Funnel2DSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelSeries}
 */
anychart.plots.funnelPlot.data.Funnel2DSeries = function() {
    anychart.plots.funnelPlot.data.FunnelSeries.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel2DSeries,
        anychart.plots.funnelPlot.data.FunnelSeries);

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DSeries.prototype.createPoint = function() {
    return new anychart.plots.funnelPlot.data.Funnel2DPoint();
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DSeries.prototype.onBeforeDraw = function() {
    this.setCurrentY(1);
    this.setRealSummHeight_();
    var padding = this.globalSettings_.getPadding();
    var dataIsWidth = this.globalSettings_.isDataIsWidth();
    var inverted = this.globalSettings_.isInverted();
    var minValue = this.globalSettings_.getMinPointSize() * this.tokensHash_.get('%SeriesYSum');
    var size3D = this.globalSettings_.getSize3D();
    for (var i = 0; i < this.points_.length; i++) {
        var point = this.points_[i];
        point.setSectorYValue(dataIsWidth, minValue, padding, this.realSummHeight_, size3D);
        point.setSectorXValue();
        point.drawingInfo_.setSectorPoint(point, inverted, this.getWidth());
    }
    goog.base(this, 'onBeforeDraw');
};

//------------------------------------------------------------------------------
//
//                         Funnel3DSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelSeries}
 */
anychart.plots.funnelPlot.data.Funnel3DSeries = function() {
    anychart.plots.funnelPlot.data.FunnelSeries.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel3DSeries,
        anychart.plots.funnelPlot.data.FunnelSeries);

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DSeries.prototype.createPoint = function() {
    return new anychart.plots.funnelPlot.data.Funnel3DPoint();
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DSeries.prototype.addPointsToDrawing = function() {
    return false;
};

//------------------------------------------------------------------------------
//
//                         Funnel2DDataIsWidthSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.FunnelSeries}
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries = function() {
    anychart.plots.funnelPlot.data.FunnelSeries.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries,
        anychart.plots.funnelPlot.data.FunnelSeries);

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries.
        prototype.targetWidth_ = null;

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries.prototype.createPoint = function() {
    return new anychart.plots.funnelPlot.data.Funnel2DDataIsWidthPoint();
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries.prototype.onBeforeDraw = function() {

};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries.prototype.setLabelsPosition = function() {
    var labelPos = new anychart.utils.geom.Point();
    var labelRect = new anychart.utils.geom.Rectangle();
    var labelPadding;
    var pointWidth;
    this.targetWidth_ = this.seriesBounds_.getWidth();
    var pos;
    for (var i = 0; i < this.points_.length; i++) {
        var point = this.points_[i];
        if (point.getLabel().isEnabled() && !point.isMissing()) {
            pos = (point.drawingInfo_.getRightBottom().x +
                    point.drawingInfo_.getRightTop().x) / 2;
            labelRect = point.getLabel().getBounds(point,
                    point.getLabel().getStyle().getNormal(),
                    point.getSprite());
            labelPos = point.getLabelPosition(
                    point.getLabel().getStyle().getNormal(),
                    null, labelRect);
            labelPadding = point.getLabel().getStyle().getNormal().getPadding();
            pointWidth = (point.bottomWidth_ + point.topWidth_) / 2 *
                    this.seriesBounds_.getWidth();
            if ((labelPadding + labelRect.width + pointWidth / 2 +
                    this.seriesBounds_.getWidth() / 2) >
                    this.seriesBounds_.getWidth()) {
                this.targetWidth_ = Math.max(this.targetWidth_,
                        labelPadding + labelRect.width + pointWidth / 2 +
                                this.seriesBounds_.getWidth() / 2)
            }
        }
    }
    this.shiftNumber_ = Math.ceil(this.seriesBounds_.getWidth() -
            (this.targetWidth_ - this.seriesBounds_.getWidth()));
};

//------------------------------------------------------------------------------
//
//                         Funnel3DDataIsWidthSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries}
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries = function() {
    anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries.call(this);
};
goog.inherits(anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries,
        anychart.plots.funnelPlot.data.Funnel2DDataIsWidthSeries);

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries.prototype.createPoint = function() {
    return new anychart.plots.funnelPlot.data.Funnel3DDataIsWidthPoint();
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries.prototype.onBeforeDraw = function() {

};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries.prototype.onBeforeResize = function() {
    //todo 
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.data.Funnel3DDataIsWidthSeries.prototype.addPointsToDrawing = function() {
    return false;
};
