/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.funnelPlot.styles.FunnelFillWithStrokeStyleShape}</li>.
 *  <li>@class {anychart.plots.funnelPlot.styles.FunnelHatchFillStyleShape}</li>.
 *  <li>@class {anychart.plots.funnelPlot.styles.FunnelStyleShapes}</li>.
 *  <li>@class {anychart.plots.funnelPlot.styles.FunnelStyle}</li>.
 * <ul>
 */
goog.provide('anychart.plots.funnelPlot.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                          FunnelFillWithStrokeStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillWithStrokeStyleShape}
 */
anychart.plots.funnelPlot.styles.FunnelFillWithStrokeStyleShape = function() {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.funnelPlot.styles.FunnelFillWithStrokeStyleShape,
        anychart.styles.background.FillWithStrokeStyleShape);
/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.styles.FunnelFillWithStrokeStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updatePath(this.element_);
};
//------------------------------------------------------------------------------
//
//                          FunnelHatchFillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.HatchFillStyleShape}
 */
anychart.plots.funnelPlot.styles.FunnelHatchFillStyleShape = function() {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.funnelPlot.styles.FunnelHatchFillStyleShape,
        anychart.styles.background.HatchFillStyleShape);
/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.styles.FunnelHatchFillStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updatePath(this.element_);
};
//------------------------------------------------------------------------------
//
//                         FunnelStyleShapes class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.funnelPlot.styles.FunnelStyleShapes = function() {
    anychart.styles.background.BackgroundStyleShapes.call(this);
};
goog.inherits(anychart.plots.funnelPlot.styles.FunnelStyleShapes,
        anychart.styles.background.BackgroundStyleShapes);

/**@inheritDoc*/
anychart.plots.funnelPlot.styles.FunnelStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.funnelPlot.styles.FunnelFillWithStrokeStyleShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.funnelPlot.styles.FunnelHatchFillStyleShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
anychart.plots.funnelPlot.styles.FunnelStyleShapes.prototype.createElement = function() {
    var path = this.point_.getSVGManager().createPath();
    path.setAttribute('d', this.getPathData());
    return path;
};

anychart.plots.funnelPlot.styles.FunnelStyleShapes.prototype.getPathData = function() {
    return this.point_.getPathData();
};
anychart.plots.funnelPlot.styles.FunnelStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if(opt_updateData) this.point_.setBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
anychart.plots.funnelPlot.styles.FunnelStyleShapes.prototype.updatePath = function(element) {
    element.setAttribute('d', this.getPathData());
};
//------------------------------------------------------------------------------
//
//                          FunnelStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.funnelPlot.styles.FunnelStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.funnelPlot.styles.FunnelStyle,
        anychart.styles.background.BackgroundStyle);
/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.styles.FunnelStyle.prototype.createStyleShapes = function() {
    return new anychart.plots.funnelPlot.styles.FunnelStyleShapes();
};
/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.styles.FunnelStyle.prototype.getStyleStateClass = function() {
    return anychart.styles.background.BackgroundStyleState;
};