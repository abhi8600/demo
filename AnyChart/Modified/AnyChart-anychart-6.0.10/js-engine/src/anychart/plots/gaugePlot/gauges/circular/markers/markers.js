/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.markers.CircularGaugeAxisMarkerList}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.circular.markers');
