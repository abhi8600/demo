goog.provide('anychart.plots.gaugePlot.frames.linear');

goog.require('anychart.plots.gaugePlot.frames');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame class
//
//------------------------------------------------------------------------------
/**
 * Linear gauge frame.
 * @constructor
 * @param {anychart.plots.gaugePlot.frames.GaugeFrame} gauge
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame = function(gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame,
              anychart.plots.gaugePlot.frames.GaugeFrame);

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.minSide_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.thickness_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.innerThickness_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.outerThickness_ = NaN;

/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.fullBounds_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.autoFit = function(gaugeBounds) {
    var innerThickness = 0;
    if (this.innerStroke && this.innerStroke.hasBackground()) {
        innerThickness = this.innerStroke.getThickness();
    }

    var outerThickness = 0;
    if (this.outerStroke && this.outerStroke.hasBackground()) {
        outerThickness = this.outerStroke.getThickness();
    }

    this.minSide_ = Math.min(gaugeBounds.width, gaugeBounds.height);
    this.innerThickness_ = innerThickness;
    this.outerThickness_ = outerThickness;
    this.thickness_ = innerThickness + outerThickness;
    var d = (innerThickness + outerThickness + this.padding) * this.minSide_;
    this.fullBounds_ = gaugeBounds.clone();

    gaugeBounds.setLeft(gaugeBounds.getLeft() + d);
    gaugeBounds.setRight(gaugeBounds.getRight() - d);
    gaugeBounds.setTop(gaugeBounds.getTop() + d);
    gaugeBounds.setBottom(gaugeBounds.getBottom() - d);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.createInnerStroke = function() {
    return new anychart.plots.gaugePlot.frames.linear.InnerStroke_(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.createOuterStroke = function() {
    return new anychart.plots.gaugePlot.frames.linear.OuterStroke_(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.createBackground = function() {
    return new anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_(this);
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame.prototype.createPath = function(svgManager, bounds) {
    var corners;
    if (!this.corners) corners = new anychart.visual.corners.Corners();
    else corners = this.corners.clone();
    var r = this.minSide_/100;
    corners.setLeftTop(corners.getLeftTop()*r);
    corners.setLeftBottom(corners.getLeftBottom()*r);
    corners.setRightTop(corners.getRightTop()*r);
    corners.setRightBottom(corners.getRightBottom()*r);
    return corners.createSVGPolygonData(svgManager, bounds);
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @private
 * @constructor
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_ = function(frame) {
    anychart.plots.gaugePlot.visual.BackgroundBasedStroke.call(this);
    this.frame = frame;
};
goog.inherits(anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_,
              anychart.plots.gaugePlot.visual.BackgroundBasedStroke);

/**
 * @protected
 * @type {anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.frame = null;

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.createInnerStrokeElement = function(svgManager) {
    return svgManager.createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.createOuterStrokeElement = function(svgManager) {
    return svgManager.createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.createBackgroundElement = function(svgManager) {
    return svgManager.createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.drawInnerStroke = function(svgManager, container, bounds) {
    var style = null;
    if (this.getBorder() && this.getBorder().isEnabled()) {
        style = this.getBorder().getSVGStroke(svgManager, bounds);
    }else {
        style = svgManager.getEmptySVGStroke();
    }
    style += svgManager.getEmptySVGFill();
    container.setAttribute('d', this.getInnerPath_(svgManager, bounds));
    container.setAttribute('style', style);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.drawOuterStroke = function(svgManager, container, bounds) {
    var style = null;
    if (this.getBorder() && this.getBorder().isEnabled()) {
        style = this.getBorder().getSVGStroke(svgManager, bounds);
    }else {
        style = svgManager.getEmptySVGStroke();
    }
    style += svgManager.getEmptySVGFill();
    container.setAttribute('d', this.getOuterPath_(svgManager, bounds));
    container.setAttribute('style', style);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.drawBackground = function(svgManager, container, hatchContainer, bounds) {
    var fillStyle = null;
    var hatchFillStyle = null;
    var path = this.getBackgroundPath_(svgManager, bounds);
    if (this.getFill() && this.getFill().isEnabled()) {
        fillStyle = this.getFill().getSVGFill(svgManager, bounds);
        fillStyle += 'fill-rule: evenodd';
    }else {
        fillStyle = svgManager.getEmptySVGFill();
    }
    container.setAttribute('d', path);
    container.setAttribute('style', fillStyle);
    container.setAttribute('fill-rule', 'evenodd');

    if (this.getHatchFill() && this.getHatchFill().isEnabled()) {
        hatchFillStyle = this.getHatchFill().getSVGHatchFill(svgManager, bounds);
        hatchFillStyle += 'fill-rule: evenodd';
    }else {
        hatchFillStyle = svgManager.getEmptySVGStyle();
    }
    hatchContainer.setAttribute('d', path);
    hatchContainer.setAttribute('style', hatchFillStyle);
    hatchContainer.setAttribute('fill-rule', 'evenodd');
};

/**
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getInnerBounds_ = function(bounds) {
    var res = bounds.clone();
    var offset = this.getInnerOffset();
    res.x += offset;
    res.y += offset;
    res.width -= offset*2;
    res.height -= offset*2;
    return res;
};

/**
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getOuterBounds_ = function(bounds) {
    var res = bounds.clone();
    var offset = this.getOuterOffset();
    res.x += offset;
    res.y += offset;
    res.width -= offset*2;
    res.height -= offset*2;
    return res;
};

/**
 * @protected
 * @return {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getInnerOffset = goog.abstractMethod;

/**
 * @protected
 * @return {number}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getOuterOffset = goog.abstractMethod;

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {string}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getInnerPath_ = function(svgManager, bounds) {
    var innerBounds = this.getInnerBounds_(bounds);
    return this.frame.createPath(svgManager, innerBounds, true, true);
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {string}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getOuterPath_ = function(svgManager, bounds) {
    var outerBounds = this.getOuterBounds_(bounds);
    return this.frame.createPath(svgManager, outerBounds, true, true);
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {string}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.prototype.getBackgroundPath_ = function(svgManager, bounds) {
    var inner = this.getInnerBounds_(bounds);
    var outer = this.getOuterBounds_(bounds);

    return this.frame.createPath(svgManager, outer, true, false) + ' ' + this.frame.createPath(svgManager, inner, false, true);
};


//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.linear.InnerStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @private
 * @param {anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame} frame
 */
anychart.plots.gaugePlot.frames.linear.InnerStroke_ = function(frame) {
    anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.call(this, frame);
};
goog.inherits(anychart.plots.gaugePlot.frames.linear.InnerStroke_,
              anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_);

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.InnerStroke_.prototype.getInnerOffset = function() {
    return -(this.frame.padding) * this.frame.minSide_;
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.InnerStroke_.prototype.getOuterOffset = function() {
    return -(this.frame.innerThickness_ + this.frame.padding) * this.frame.minSide_;
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.linear.OuterStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @private
 * @param {anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame} frame
 */
anychart.plots.gaugePlot.frames.linear.OuterStroke_ = function(frame) {
    anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_.call(this, frame);
};
goog.inherits(anychart.plots.gaugePlot.frames.linear.OuterStroke_,
              anychart.plots.gaugePlot.frames.linear.LinearFrameStroke_);

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.OuterStroke_.prototype.getInnerOffset = function() {
    return -(this.frame.innerThickness_ + this.frame.padding) * this.frame.minSide_;
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.OuterStroke_.prototype.getOuterOffset = function() {
    return -(this.frame.thickness_ + this.frame.padding) * this.frame.minSide_;
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_ class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @private
 * @param {anychart.plots.gaugePlot.gauges.linear.LinearGauge} gauge
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_ = function(gauge) {
    anychart.visual.background.ShapeBackground.call(this);
    this.gauge_ = gauge;
};
goog.inherits(anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_,
              anychart.visual.background.ShapeBackground);

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.linear.LinearGauge}
 */
anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_.prototype.gauge_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_.prototype.createElement = function(svgManager) {
    return svgManager.createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.linear.LinearFrameBackground_.prototype.getElementPath = function(svgManager, bounds) {
    return this.gauge_.createPath(svgManager, bounds);
};
