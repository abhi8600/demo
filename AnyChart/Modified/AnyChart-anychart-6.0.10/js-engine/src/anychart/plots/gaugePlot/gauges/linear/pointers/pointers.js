/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.linear.pointers');
goog.require('anychart.plots.gaugePlot.gauges.linear.pointers.styles');
//------------------------------------------------------------------------------
//
//                    LinearGaugePointer class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer);
//------------------------------------------------------------------------------
//                         Margins.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    this.checkMargin();
    return this.sprite_;
};
/**
 * Check pointer margins.
 */
anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer.prototype.checkMargin = function () {
};
//------------------------------------------------------------------------------
//
//                          BarLinearGaugePointer class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.getStyleNodeName = function () {
    return 'bar_pointer_style';
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle();
};
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('BarLinearGaugePointer');
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.calculateBounds = function () {
    var startValue = this.getStartValue_();
    var endValue = this.getEndValue_();

    var percentW = (this.currentState_.isWidthAxisSizeDepend() ?
        this.axis_.getSize() : 1
        ) * this.currentState_.getWidth();

    var start = this.axis_.getComplexPosition(
        this.currentState_.getAlign(),
        this.currentState_.getPadding(),
        percentW,
        true);

    var end = this.axis_.getComplexPosition(
        this.currentState_.getAlign(),
        this.currentState_.getPadding(),
        percentW,
        false);

    var valStart = this.axis_.scale.transformValueToPixel(startValue);
    var valEnd = this.axis_.scale.transformValueToPixel(endValue);

    if (this.axis_.getGauge().isHorizontal()) {
        this.bounds_.y = Math.min(start, end);
        this.bounds_.height = Math.max(start, end) - this.bounds_.y;
        this.bounds_.x = Math.min(valStart, valEnd);
        this.bounds_.width = Math.max(valStart, valEnd) - this.bounds_.x;
    } else {
        this.bounds_.x = Math.min(start, end);
        this.bounds_.width = Math.max(start, end) - this.bounds_.x;
        this.bounds_.y = Math.min(valStart, valEnd);
        this.bounds_.height = Math.max(valStart, valEnd) - this.bounds_.y;
    }
};
/**
 * ND: Needs doc!
 * @private
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.getStartValue_ = function () {
    return this.currentState_.startFromZero() ? 0 : this.axis_.getScale().getMinimum();
};
/**
 * ND: Needs doc!
 * @private
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer.prototype.getEndValue_ = function () {
    return this.fixValueScaleOut(this.value_);
};
//------------------------------------------------------------------------------
//
//                    RangeBarGaugePointer class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer);
//------------------------------------------------------------------------------
//                   Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.endValue_ = null;
//------------------------------------------------------------------------------
//                     Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.getStyleNodeName = function () {
    return 'range_bar_pointer_style';
};

//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.deserialize = function (data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'start'))
        this.value_ = des.getNumProp(data, 'start');

    if (des.hasProp(data, 'end'))
        this.endValue_ = des.getNumProp(data, 'end');

    this.editable_ = false;
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.getStartValue_ = function () {
    return this.fixValueScaleOut(this.value_);
};
/**
 * ND: Needs doc!
 * @private
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.getEndValue_ = function () {
    return this.fixValueScaleOut(this.endValue_);
};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.getTokenValue = function (token) {
    if (token == '%Value') return this.endValue_ - this.value_;
    if (token == '%StartValue') return this.value_;
    if (token == '%EndValue') return this.endValue_;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer.prototype.getTokenType = function (token) {
    return anychart.formatting.TextFormatTokenType.NUMBER;
};

//------------------------------------------------------------------------------
//
//              MarkerLinearGaugePointer class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer = function () {
    goog.base(this);
};

goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer);
//------------------------------------------------------------------------------
//                   Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.markerPos_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.getMarkerPos = function () {
    return this.markerPos_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.markerSprite_ = null;
/**
 * ND: Needs doc!
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.getMarkerSprite = function () {
    return this.markerSprite_;
};
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.getStyleNodeName = function () {
    return 'marker_pointer_style';
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle();
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.initialize = function (svgManager) {
    this.markerSprite_ = new anychart.svg.SVGSprite(svgManager);
    goog.base(this, 'initialize', svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.markerSprite_.setId('MarkerPointerMarkerSprite');
    this.sprite_.appendSprite(this.markerSprite_);
    return  this.sprite_;
};
//------------------------------------------------------------------------------
//                      Bounds.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer.prototype.calculateBounds = function () {
    var marker = this.currentState_.getMarker();

    var percentH = (marker.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * marker.getWidth();
    var percentW = (marker.isHeightAxisSizeDepend() ? this.axis_.getSize() : 1) * marker.getHeight();

    var r = this.axis_.getPixelTargetSize();
    var w = percentW * r;
    var h = percentH * r;

    var isHorizontal = this.gauge_.isHorizontal();


    var position = this.axis_.getPosition(this.currentState_.getAlign(), this.currentState_.getPadding(), 0, false);

    var pos = new anychart.utils.geom.Point();
    this.axis_.setPoint(position, this.axis_.getScale().transformValueToPixel(this.fixValueScaleOut(this.value_)), pos);

    pos.x -= w / 2;
    pos.y -= h / 2;

    this.bounds_.x = -w / 2;
    this.bounds_.y = -h / 2;
    this.bounds_.width = w;
    this.bounds_.height = h;

    this.markerPos_ = new anychart.utils.geom.Point(-w / 2, -h / 2);

    var actualPos = pos.clone();
    if (isHorizontal) {
        this.markerSprite_.setY(pos.y + h / 2);
        switch (this.currentState_.getAlign()) {
            case anychart.plots.gaugePlot.layout.Align.INSIDE:
                this.markerSprite_.decrementY(h / 2);
                break;
            case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
                this.markerSprite_.incrementY(h / 2);
        }
        this.markerSprite_.setX(pos.x + w / 2);
    } else {
        this.markerSprite_.setX(pos.x + w / 2);
        switch (this.currentState_.getAlign()) {
            case anychart.plots.gaugePlot.layout.Align.INSIDE:
                this.markerSprite_.decrementX(w / 2);
                break;
            case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
                this.markerSprite_.incrementX(w / 2);
        }
        this.markerSprite_.setY(pos.y + h / 2);
    }

    var rotation = 0;
    if (marker.isAutoRotate()) {
        switch (this.currentState_.getAlign()) {
            case anychart.plots.gaugePlot.layout.Align.INSIDE:
            case anychart.plots.gaugePlot.layout.Align.CENTER:
                rotation = isHorizontal ? -180 : 90;
                break;
            case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
                rotation = isHorizontal ? 0 : -90;
                break;
        }
    }
    this.markerSprite_.setRotation(rotation + marker.getRotation());

    this.bounds_.x = actualPos.x;
    this.bounds_.y = actualPos.y;
};

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer = function () {
    goog.base(this);
    //this.bounds_ = new anychart.utils.geom.Rectangle();
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.getStyleNodeName = function () {
    return 'thermometer_pointer_style';
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle();
};
//------------------------------------------------------------------------------
//                  Paths.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.termPathData_ = null;
/**
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.getTermPathData = function () {
    return this.termPathData_;
};
/**
 * @private
 * @type {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.circlePath_ = null;
//------------------------------------------------------------------------------
//                  Margins.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.checkMargin = function () {
    var state = this.style_.getNormal();
    this.checkStateMargin_(this.style_.getNormal());
    this.checkStateMargin_(this.style_.getHover());
    this.checkStateMargin_(this.style_.getPushed());
    this.checkStateMargin_(this.style_.getSelectedNormal());
    this.checkStateMargin_(this.style_.getSelectedHover());
    var capSize = (state.isCapRadiusAxisSideDepend() ? this.axis_.getSize() : 1) * state.getCapRadius() * 2 + state.getCapPadding();

    if (this.axis_.getScale().isInverted())
        this.gauge_.setEndMargin(Math.max(this.gauge_.getEndMargin(), capSize));
    else
        this.gauge_.setStartMargin(Math.max(this.gauge_.getStartMargin(), capSize));
};

/**
 * ND: Needs doc!
 * @private
 * @param {anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState} state
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.checkStateMargin_ = function (state) {
    var percentW = (state.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * state.getWidth();
    var percentCapRadius = (state.isCapRadiusAxisSideDepend() ? this.axis_.getSize() : 1) * state.getCapRadius();
    if (percentCapRadius < (percentW / 2)) {
        state.setCapRadius((percentW / 2) / (state.isCapRadiusAxisSideDepend() ? this.axis_.getSize() : 1));
    }
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.calculatePaths = function () {
    var state = this.currentState_;

    var percentW = (state.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * state.getWidth();
    var percentCapRadius = (state.isCapRadiusAxisSideDepend() ? this.axis_.getSize() : 1) * state.getCapRadius();

    var start = this.axis_.getComplexPosition(state.getAlign(), state.getPadding(), percentW, true);
    var end = this.axis_.getComplexPosition(state.getAlign(), state.getPadding(), percentW, false);

    var valStart = this.axis_.getScale().transformValueToPixel(this.axis_.getScale().getMinimum());
    var actualVal = this.axis_.getScale().transformValueToPixel(this.fixValueScaleOut(this.value_));

    var targetSize = this.axis_.getPixelTargetSize();
    var r = targetSize * percentCapRadius;
    var w = targetSize * percentW;
    var padding = targetSize * state.getCapPadding();

    var bounds = new anychart.utils.geom.Rectangle();
    this.setThermometerBounds_(start, end, valStart, actualVal, r, padding, bounds);

    if (this.axis_.getScale().isInverted()) valStart += padding;
    else valStart -= padding;

    this.termPathData_ = this.getThermometerPathData(this.getSVGManager(), start, end, valStart, actualVal, r, w);
};

/**
 * ND: Needs doc!
 * @private
 * @param {Number} start
 * @param {Number} end
 * @param {Number} valStart
 * @param {Number} actualVal
 * @param {Number} r
 * @param {Number} capPadding
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.setThermometerBounds_ = function (start, end, valStart, actualVal, r, capPadding, bounds) {
    if (this.axis_.getGauge().isHorizontal()) {
        bounds.y = (start + end) / 2 - r;
        bounds.height = r * 2;
        if (this.axis_.getScale().isInverted()) {
            bounds.x = actualVal;
            bounds.width = valStart + r * 2 + capPadding - bounds.x;
        } else {
            bounds.x = valStart - r * 2 - capPadding;
            bounds.width = actualVal - bounds.x;
        }
    } else {
        bounds.x = (start + end) / 2 - r;
        bounds.width = r * 2;
        if (this.axis_.getScale().isInverted()) {
            bounds.y = actualVal;
            bounds.height = valStart + r * 2 - bounds.y;
        } else {
            bounds.y = valStart - r * 2;
            bounds.height = actualVal - bounds.y;
        }
    }
};
/**
 * ND: Needs doc!
 * @private
 * @param {Number} start
 * @param {Number} end
 * @param {Number} valStart
 * @param {Number} w
 * @param {Number} r
 * @param {anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis} axis
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.getBublCenter_ = function (start, end, valStart, w, r, axis) {
    var dA = Math.abs(Math.asin(w / (r * 2)) * 180 / Math.PI);

    var center = new anychart.utils.geom.Point();
    if (this.gauge_.isHorizontal()) {
        center.y = (start + end) / 2;
        center.x = axis.getScale().isInverted() ? (valStart + (1 / Math.tan(dA * Math.PI / 180)) * (end - start) / 2) :
            (valStart - (1 / Math.tan(dA * Math.PI / 180)) * (end - start) / 2);
    } else {
        center.x = (start + end) / 2;
        center.y = axis.getScale().isInverted() ? (valStart + (1 / Math.tan(dA * Math.PI / 180)) * (end - start) / 2) :
            ( valStart - (1 / Math.tan(dA * Math.PI / 180)) * (end - start) / 2);
    }

    return center;
};

//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} start
 * @param {Number} end
 * @param {Number} valStart
 * @param {Number} actualVal
 * @param {Number} r
 * @param {Number} w
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.getThermometerPathData = function (svgManager, start, end, valStart, actualVal, r, w) {
    if (this.gauge_.isHorizontal()) {
        return this.drawHorizontalThermometer(svgManager, start, end, valStart, actualVal, r, w, this.axis_.getScale().isInverted());
    } else {
        return this.drawVerticalThermometer(svgManager, start, end, valStart, actualVal, r, w, this.axis_.getScale().isInverted());
    }
};
/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} start
 * @param {Number} end
 * @param {Number} valStart
 * @param {Number} actualVal
 * @param {Number} r
 * @param {Number} w
 * @param {Boolean} inverted
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.drawHorizontalThermometer = function (svgManager, start, end, valStart, actualVal, r, w, inverted) {
    var dA = Math.abs(Math.asin(w / (r * 2)) * 180 / Math.PI);
    var angle1;
    var angle2;

    var pt1 = new anychart.utils.geom.Point(actualVal, start);
    var pt2 = new anychart.utils.geom.Point(actualVal, end);
    var pt3 = new anychart.utils.geom.Point(valStart, end);
    var pt4 = new anychart.utils.geom.Point(valStart, start);

    var pathData = svgManager.pMove(pt1.x, pt1.y);
    pathData += svgManager.pLine(pt2.x, pt2.y);
    pathData += svgManager.pLine(pt3.x, pt3.y);
    pathData += svgManager.pCircleArc(r, 1, 1, pt4.x, pt4.y, r);
    pathData += svgManager.pLine(pt4.x, pt4.y);
    pathData += svgManager.pLine(pt1.x, pt1.y);

    this.bounds_.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x);
    this.bounds_.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y);
    this.bounds_.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x) - this.bounds_.x;
    this.bounds_.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y) - this.bounds_.y;

    return pathData;
};
/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} start
 * @param {Number} end
 * @param {Number} valStart
 * @param {Number} actualVal
 * @param {Number} r
 * @param {Number} w
 * @param {Boolean} inverted
 */
anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer.prototype.drawVerticalThermometer = function (svgManager, start, end, valStart, actualVal, r, w, inverted) {
    var dA = Math.abs(Math.asin(w / (r * 2)) * 180 / Math.PI);
    var angle1;
    var angle2;

    var pt1 = new anychart.utils.geom.Point(start, actualVal);
    var pt2 = new anychart.utils.geom.Point(end, actualVal);
    var pt3 = new anychart.utils.geom.Point(end, valStart);
    var pt4 = new anychart.utils.geom.Point(start, valStart);

    var pathData = svgManager.pMove(pt1.x, pt1.y);
    pathData += svgManager.pLine(pt2.x, pt2.y);
    pathData += svgManager.pLine(pt3.x, pt3.y);
    pathData += svgManager.pCircleArc(r, 1, 1, pt4.x, pt4.y, r);
    pathData += svgManager.pLine(pt4.x, pt4.y);
    pathData += svgManager.pLine(pt1.x, pt1.y);

    this.bounds_.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x);
    this.bounds_.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y);
    this.bounds_.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x) - this.bounds_.x;
    this.bounds_.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y) - this.bounds_.y;

    return pathData;
};
//------------------------------------------------------------------------------
//
//                    TankLinearGaugePointer class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.LinearGaugePointer);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getStyleNodeName = function () {
    return 'tank_pointer_style';
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.bulbBounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getBulbBounds = function () {
    return this.bulbBounds_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.bodyBounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getBodyBounds = function () {
    return this.bodyBounds_;
};

//------------------------------------------------------------------------------
//                  Margins.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.checkMargin = function () {
    var padding = (this.currentState_.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * this.currentState_.getWidth() * 0.14880952380;
    this.gauge_.setEndMargin(Math.max(this.gauge_.getEndMargin(), padding));
    this.gauge_.setStartMargin(Math.max(this.gauge_.getStartMargin(), padding));
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getMainPathData = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getTopShadePathData = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getBottomShadePathData = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer.prototype.getTopBlikPathData = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//               TankHorizontalPointer class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.createStyle = function () {
    return  new anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle();
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.calculateBounds = function () {
    var percentW = (this.currentState_.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * this.currentState_.getWidth();
    var start = this.axis_.getComplexPosition(this.currentState_.getAlign(), this.currentState_.getPadding(), percentW, true);
    var totalMin = this.axis_.getScale().transformValueToPixel(this.axis_.getScale().getMinimum());
    var totalMax = this.axis_.getScale().transformValueToPixel(this.axis_.getScale().getMaximum());
    var val = this.axis_.getScale().transformValueToPixel(this.value_);
    var w = percentW * this.axis_.getPixelTargetSize();

    if (this.axis_.getScale().isInverted()) {
        this.bounds_ = new anychart.utils.geom.Rectangle(val, start, totalMin - val, w);
        this.bodyBounds_ = new anychart.utils.geom.Rectangle(val, start, totalMin - val, w);
        this.bulbBounds_ = new anychart.utils.geom.Rectangle(totalMax, start, val - totalMax, w);
    } else {
        this.bounds_ = new anychart.utils.geom.Rectangle(totalMin, start, val - totalMin, w);
        this.bodyBounds_ = new anychart.utils.geom.Rectangle(totalMin, start, val - totalMin, w);
        this.bulbBounds_ = new anychart.utils.geom.Rectangle(val, start, totalMax - val, w);
    }
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.getMainPathData = function (rc) {
    var svgManager = this.getSVGManager();
    var xR = rc.height * 0.14880952380;
    var yR = rc.height / 2;

    var yStart = rc.y + rc.height / 2 + anychart.utils.geom.DrawingUtils.getPointY(yR, 90);
    var pathData = svgManager.pMove(
        rc.x + anychart.utils.geom.DrawingUtils.getPointX(xR, 90),
        yStart
    );

    pathData += svgManager.pCircleArc(xR, 1, 1, rc.x, rc.y, yR);
    pathData += svgManager.pLine(rc.x + rc.width, rc.y);
    pathData += svgManager.pCircleArc(xR, 0, 1, rc.x + rc.width, yStart, yR);

    return pathData;
};

/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.getTopShadePathData = function (rc) {
    var xR = rc.height * 0.14880952380;
    var yR = rc.height / 2;
    var yStart = rc.y + rc.height / 2 + anychart.utils.geom.DrawingUtils.getPointY(yR, 90);
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x + rc.width + 1.5, rc.y + 1);
    pathData += svgManager.pCircleArc(xR, 1, 0, rc.x + rc.width + 1.5, yStart, yR);

    return pathData;
};
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.getBottomShadePathData = function (rc) {
    var xR = rc.height * 0.14880952380;
    var yR = rc.height / 2;
    var yStart = rc.y + rc.height / 2 + anychart.utils.geom.DrawingUtils.getPointY(yR, 90);
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x + 1, rc.y);
    pathData += svgManager.pCircleArc(xR, 0, 0, rc.x, yStart, yR);

    return pathData;
};
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer.prototype.getTopBlikPathData = function (rc) {
    var xR = rc.height * 0.14880952380;
    var yR = rc.height / 2;
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x + rc.width - 1, rc.y);
    pathData += svgManager.pCircleArc(xR, 1, 0, rc.x + rc.width - 1 + .1, rc.y, yR);

    return pathData;
};
//------------------------------------------------------------------------------
//
//               TankVerticalPointer class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer,
    anychart.plots.gaugePlot.gauges.linear.pointers.TankLinearGaugePointer);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.createStyle = function () {
    return  new anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle();
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.calculateBounds = function () {
    var percentW = (this.currentState_.isWidthAxisSizeDepend() ? this.axis_.getSize() : 1) * this.currentState_.getWidth();
    var start = this.axis_.getComplexPosition(this.currentState_.getAlign(), this.currentState_.getPadding(), percentW, true);
    var totalMin = this.axis_.getScale().transformValueToPixel(this.axis_.getScale().getMinimum());
    var totalMax = this.axis_.getScale().transformValueToPixel(this.axis_.getScale().getMaximum());
    var val = this.axis_.getScale().transformValueToPixel(this.value_);
    var w = percentW * this.axis_.getPixelTargetSize();

    if (this.axis_.getScale().isInverted()) {
        this.bounds_ = new anychart.utils.geom.Rectangle(start, val, w, totalMin - val);
        this.bodyBounds_ = new anychart.utils.geom.Rectangle(start, val, w, totalMin - val);
        this.bulbBounds_ = new anychart.utils.geom.Rectangle(start, totalMax, w, val - totalMax);
    } else {
        this.bounds_ = new anychart.utils.geom.Rectangle(start, totalMin, w, val - totalMin);
        this.bodyBounds_ = new anychart.utils.geom.Rectangle(start, totalMin, w, val - totalMin);
        this.bulbBounds_ = new anychart.utils.geom.Rectangle(start, val, w, totalMax - val);
    }
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.getMainPathData = function (rc) {
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x, rc.y);
    pathData += svgManager.pCircleArc(rc.width / 2, 0, 1, rc.x + rc.width, rc.y, rc.width * 0.14880952380);
    pathData += svgManager.pLine(rc.x + rc.width, rc.y + rc.height);
    pathData += svgManager.pCircleArc(rc.width / 2, 0, 1, rc.x, rc.y + rc.height, rc.width * 0.14880952380);

    return pathData;
};

/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.getTopShadePathData = function (rc) {
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x, rc.y);
    pathData += svgManager.pCircleArc(rc.width / 2, 1, 0, rc.x, rc.y -.1, rc.width * 0.14880952380);

    return pathData;
};
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.getBottomShadePathData = function (rc) {
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x, rc.y + rc.height - 1);
    pathData += svgManager.pCircleArc(rc.width / 2, 0, 0, rc.x + rc.width, rc.y + rc.height, rc.width * 0.14880952380);

    return pathData;
};
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle} rc
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer.prototype.getTopBlikPathData = function (rc) {
    var svgManager = this.getSVGManager();

    var pathData = svgManager.pMove(rc.x, rc.y);
    pathData += svgManager.pCircleArc(rc.width / 2, 1, 0, rc.x, rc.y -.1, rc.width * 0.14880952380);

    return pathData;
};













