goog.provide('anychart.plots.gaugePlot.templates');

goog.require('anychart.templates');
goog.require('anychart.utils');
goog.require('anychart.plots.gaugePlot.templates.DEFAULT_TEMPLATE');

/**
 * @constructor
 * @extends {anychart.templates.BaseTemplatesMerger}
 */
anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger = function() {
    anychart.templates.BaseTemplatesMerger.call(this);
};
goog.inherits(anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger,
              anychart.templates.BaseTemplatesMerger);

/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger.prototype.
                                            getBaseNodeName = function() {
    return 'gauge';
};

/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger.prototype.getPlotDefaultTemplate = function(){
    return this.mergeTemplates(goog.base(this, 'getPlotDefaultTemplate'), anychart.plots.gaugePlot.templates.DEFAULT_TEMPLATE);
};

/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger.prototype.
                            mergeChartNode = function (template, chart, res) {
    goog.base(this, 'mergeChartNode', template, chart, res);

    this.mergeGauge_(template, chart, res, 'circular_template');
    this.mergeGauge_(template, chart, res, 'linear_template');
    this.mergeGauge_(template, chart, res, 'label_template');
    this.mergeGauge_(template, chart, res, 'image_template');
    this.mergeGauge_(template, chart, res, 'indicator_template');

    var des = anychart.utils.deserialization;

    if (des.hasProp(chart, 'circular') && des.getPropArray(chart, 'circular').length > 0)
        des.setProp(res, 'circular', des.getPropArray(chart, 'circular'));

    if (des.hasProp(chart, 'linear') && des.getPropArray(chart, 'linear').length > 0)
        des.setProp(res, 'linear', des.getPropArray(chart, 'linear'));

    if (des.hasProp(chart, 'label') && des.getPropArray(chart, 'label').length > 0)
        des.setProp(res, 'label', des.getPropArray(chart, 'label'));

    if (des.hasProp(chart, 'image') && des.getPropArray(chart, 'image').length > 0)
        des.setProp(res, 'image', des.getPropArray(chart, 'image'));

    if (des.hasProp(chart, 'indicator') && des.getPropArray(chart, 'indicator').length > 0)
        des.setProp(res, 'indicator', des.getPropArray(chart, 'indicator'));
};

/**
 * @private
 * @param {Object} template
 * @param {Object} chart
 * @param {Object} res
 * @param {String} collectionName
 */
anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger.prototype.
                mergeGauge_ = function(template, chart, res, collectionName) {
    var des = anychart.utils.deserialization;
    var templateCollection = des.getPropArray(template, collectionName);
    var chartCollection = des.getPropArray(chart, collectionName);

    res[collectionName] = [];
    var children = res[collectionName];

    var i;
    var templatesCount = templateCollection.length;
    for (i = 0;i<templatesCount;i++) {
        if (des.hasProp(templateCollection[i], 'name'))
            templateCollection[i]['name'] = des.getLStringProp(templateCollection[i], 'name');
        children.push(templateCollection[i]);
    }

    var chartsCount = chartCollection.length;
    for (i = 0;i<chartsCount;i++) {
        if (des.hasProp(chartCollection[i], 'name')) {
            var gaugeName = des.getLStringProp(chartCollection[i], 'name');
            des.setProp(chartCollection[i], 'name', gaugeName);
            var gaugeAlreadyExists = false;
            for (var j = 0;j<children.length;j++) {
                if ((des.hasProp(children[i], 'name') && des.getStringProp(children[i], 'name') == gaugeName)) {
                    gaugeAlreadyExists = true;
                    res[j] = chartCollection[i];
                    break;
                }
            }
            if (!gaugeAlreadyExists)
                children.push(chartCollection[i]);
        }else {
            children.push(chartCollection[i]);
        }
    }
};
