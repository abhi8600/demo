/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.plots.gaugePlot.elements.GaugeLabel}</li>
 *     <li>@class {anychart.plots.gaugePlot.elements.LabelGauge}</li>
 * </ul>.
 */
goog.provide('anychart.plots.gaugePlot.elements');
goog.require('anychart.elements.label');
goog.require('anychart.elements.tooltip');
goog.require('anychart.plots.gaugePlot.elements.styles');
//------------------------------------------------------------------------------
//
//                      GaugeLabel class
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.elements.label.BaseLabelElement}
 *
 */
anychart.plots.gaugePlot.elements.GaugeLabel = function () {
    goog.base(this);
    this.enabled_ = true;
    this.underPointers_ = false;
};
goog.inherits(anychart.plots.gaugePlot.elements.GaugeLabel,
    anychart.elements.label.BaseLabelElement);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge}
 */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.gauge_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge}
 */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.getGauge = function () {
    return this.gauge_;
};
/**
 * @param {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge} value
 */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.setGauge = function (value) {
    this.gauge_ = value;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.underPointers_ = null;

//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle();
};
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.getElementContainer = function (point) {
    if (this.underPointers_) return point.getGauge().getLabelsBeforeSprite();
    else return point.getGauge().getLabelsAfterSprite();

};
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.deserializeGlobalSettings = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'enabled'))
        this.enabled_ = des.getBoolProp(data, 'enabled');

    if (des.hasProp(data, 'under_pointers'))
        this.underPointers_ = des.getBoolProp(data, 'under_pointers');

    this.deserializeStyleFromNode_(data, stylesList);
};
//------------------------------------------------------------------------------
//                      Position.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.getPosition = function (container, state, sprite) {
    var pos = new anychart.utils.geom.Point();

    switch (state.getPlacemenetMode()) {
        case anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_ANCHOR:
            container.getAnchorPoint(state.getAnchor(), pos);
            break;
        case anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_POINT:
        case anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_RECTANGLE:
            pos.x = this.gauge_.getBounds().x;
            pos.y = this.gauge_.getBounds().y;
            break;
    }
    return pos;
};
//------------------------------------------------------------------------------
//                  Utils.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.elements.GaugeLabel.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target = new anychart.plots.gaugePlot.elements.GaugeLabel();
    opt_target.setGauge(this.gauge_);
    opt_target.underPointers_ = this.underPointers_;
    return goog.base(this, 'copy', opt_target);
};

//------------------------------------------------------------------------------
//
//                          GaugeTooltip class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.elements.tooltip.BaseTooltipElement}
 *
 */
anychart.plots.gaugePlot.elements.GaugeTooltip = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.elements.GaugeTooltip,
    anychart.elements.tooltip.BaseTooltipElement);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.elements.GaugeTooltip.prototype.deserializeGlobalSettings = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'enabled'))
        this.enabled_ = des.getBoolProp(data, 'enabled');

    this.deserializeStyleFromNode_(data, stylesList);
};

anychart.plots.gaugePlot.elements.GaugeTooltip.prototype.getElementContainer = function (point) {
    return point.getAxis().getGauge().getTooltipSprite();
};
//------------------------------------------------------------------------------
//                  Utils.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.elements.GaugeTooltip.prototype.copy = function (opt_target) {
    if (!opt_target) opt_target = new anychart.plots.gaugePlot.elements.GaugeTooltip();
    return goog.base(this, 'copy', opt_target);
};
//------------------------------------------------------------------------------
//
//                      LabelGauge class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.label.BaseLabelElement}
 *
 */
anychart.plots.gaugePlot.elements.LabelGauge = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.elements.LabelGauge,
    anychart.elements.label.BaseLabelElement);

/**@inheritDoc*/
anychart.plots.gaugePlot.elements.LabelGauge.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.elements.LabelGauge.prototype.setBounds = function(newBounds) {
    goog.base(this, 'setBounds', newBounds);
    this.bounds.x = newBounds.x;
    this.bounds.y = newBounds.y;
    this.bounds.width = newBounds.width;
    this.bounds.height = newBounds.height;
};
