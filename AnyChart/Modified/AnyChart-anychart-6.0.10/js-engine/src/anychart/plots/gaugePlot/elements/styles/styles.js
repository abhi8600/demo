/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.elements.styles');
//------------------------------------------------------------------------------
//
//                      GaugeLabelStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
};
goog.inherits(anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState,
    anychart.elements.label.styles.LabelElementStyleState);
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState.prototype.createTextElement = function () {
    return new anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement();
};
/**
 * @return {}
 */
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState.prototype.getPlacemenetMode = function () {
    return this.textElement.getPlacemenetMode();
};
//------------------------------------------------------------------------------
//
//                          GaugeLabelStyle.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyleShapes}
 *
 */
anychart.plots.gaugePlot.elements.styles.GaugeStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.elements.styles.GaugeStyleShapes,
    anychart.elements.label.styles.LabelElementStyleShapes);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.elements.styles.GaugeStyleShapes.prototype.getDateTimeLocale = function (point) {
    return null;
};
anychart.plots.gaugePlot.elements.styles.GaugeStyleShapes.prototype.execUpdate = function (svgManager, textElement, sprite, color) {
    textElement.update(
        this.point_.getSVGManager(),
        sprite,
        0,
        0,
        this.point_.getAxis().getGauge().getBounds().width,
        this.point_.getAxis().getGauge().getBounds().height,
        color);
};
//------------------------------------------------------------------------------
//
//                    GaugeLabelStyle class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle,
    anychart.elements.label.styles.LabelElementStyle);
/** @inheritDoc */
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.elements.styles.GaugeStyleShapes();
};

/** @inheritDoc */
anychart.plots.gaugePlot.elements.styles.GaugeLabelStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.elements.styles.GaugeLabelStyleState;
};

