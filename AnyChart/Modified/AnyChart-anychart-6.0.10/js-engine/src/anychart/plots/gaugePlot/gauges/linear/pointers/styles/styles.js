/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.linear.pointers.styles');
//------------------------------------------------------------------------------
//
//                          BarPointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.width_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.getWidth = function () {
    return this.width_;
};
/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.isWidthAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.isWidthAxisSizeDepend = function () {
    return this.isWidthAxisSizeDepend_;
};
/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.startFromZero_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.startFromZero = function () {
    return this.startFromZero_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    if (des.hasProp(data, 'width')) {
        var strWidth = des.getStringProp(data, 'width');
        this.isWidthAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(strWidth);
        this.width_ = this.isWidthAxisSizeDepend_ ?
            gaugeDes.getAxisSizeFactor(strWidth) :
            des.getSimplePercent(strWidth);
    }

    if (des.hasProp(data, 'start_from_zero'))
        this.startFromZero_ = des.getBoolProp(data, 'start_from_zero');

    return data;
};

//------------------------------------------------------------------------------
//
//                          BarPointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.point_.getSVGManager().pRectByBounds(this.point_.getBounds()));
};
//------------------------------------------------------------------------------
//
//                          BarPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.BarPointerStyleShapes();
};
//------------------------------------------------------------------------------
//
//                    MarkerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle = function (useBackground) {
    this.width_ = .1;
    this.height_ = .1;
    this.shape_ = anychart.plots.gaugePlot.visual.GaugeShapeType.LINE;
    this.align_ = anychart.plots.gaugePlot.layout.Align.CENTER;
    this.padding_ = 0;
    if (useBackground) this.background_ = new anychart.visual.background.Background();
    this.isWidthAxisSizeDepend_ = false;
    this.isHeightAxisSizeDepend_ = false;
    this.rotation_ = 0;
    this.autoRotate_ = true;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.align_ = null;
/**
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getAlign = function () {
    return this.align_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getPadding = function () {
    return this.padding_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.width_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getWidth = function () {
    return this.width_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.isWidthAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.isWidthAxisSizeDepend = function () {
    return this.isWidthAxisSizeDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.height_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getHeight = function () {
    return this.height_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.isHeightAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.isHeightAxisSizeDepend = function () {
    return this.isHeightAxisSizeDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.visual.GaugeShapeType}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.shape_ = null;
/**
 * @return {anychart.plots.gaugePlot.visual.GaugeShapeType}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getShape = function () {
    return this.shape_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.rotation_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getRotation = function () {
    return this.rotation_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.autoRotate_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.isAutoRotate = function () {
    return this.autoRotate_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.background_ = null;
/**
 * @return {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.getBackground = function () {
    return this.background_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;
    var shapeType = anychart.plots.gaugePlot.visual.GaugeShapeType;

    if (des.hasProp(data, 'shape'))
        this.shape_ = shapeType.deserialize(des.getEnumProp(data, 'shape'));

    if (des.hasProp(data, 'align'))
        this.align_ = gaugeDes.getAlignProp(data, 'align');

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getSimplePercentProp(data, 'padding');

    if (des.hasProp(data, 'rotation'))
        this.rotation_ = des.getNumProp(data, 'rotation');

    if (des.hasProp(data, 'auto_rotate'))
        this.autoRotate_ = des.getBoolProp(data, 'auto_rotate');

    var temp;

    if (des.hasProp(data, 'width')) {
        temp = des.getStringProp(data, 'width');
        this.isWidthAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.width_ = this.isWidthAxisSizeDepend_ ? gaugeDes.getAxisSizeFactor(temp) : des.getSimplePercent(temp);
    }

    if (des.hasProp(data, 'height')) {
        temp = des.getStringProp(data, 'height');
        this.isHeightAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.height_ = this.isHeightAxisSizeDepend_ ? gaugeDes.getAxisSizeFactor(temp) : des.getSimplePercent(temp);
    }

    if (!shapeType.shapeIsRectangle(this.shape_))
        this.width_ = this.height_ = Math.min(this.width_, this.height_);

    if (this.background_)
        this.background_.deserialize(data);
};
//------------------------------------------------------------------------------
//
//                 MarkerPointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState = function () {
    goog.base(this);
    this.marker_ = new anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle(false);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState.prototype.marker_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState.prototype.getMarker = function () {
    return this.marker_;
};
//------------------------------------------------------------------------------
//                         Deserialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.marker_.deserialize(data);
    return data;
};
//------------------------------------------------------------------------------
//
//                MarkerPointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes = function () {
    goog.base(this);
    this.shapeDrawer_ = new anychart.plots.gaugePlot.visual.GaugeShapeDrawer();
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.visual.GaugeShapeDrawer}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes.prototype.shapeDrawer_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes.prototype.getSprite = function () {
    return this.point_.getMarkerSprite();
};
//------------------------------------------------------------------------------
//                      Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    this.point_.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.shapeDrawer_.getSVGPathData(
        this.point_.getCurrentState().getMarker().getShape(),
        this.point_.getSVGManager(),
        Math.min(this.point_.getBounds().width, this.point_.getBounds().height),
        this.point_.getMarkerPos().x,
        this.point_.getMarkerPos().y,
        this.point_.getBounds().width,
        this.point_.getBounds().height,
        this.point_.getCurrentState().needUpdateBorder() ? this.point_.getCurrentState().getBorder().getThickness() : 1
    ));
};
//------------------------------------------------------------------------------
//
//                          MarkerPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleShapes();
};
//------------------------------------------------------------------------------
//
//               ThermometerPointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState = function () {
    goog.base(this);
    this.capRadius_ = .15;
    this.capPadding_ = 0;
    this.isWidthAxisSizeDepend_ = true;
    this.isCapRadiusAxisSideDepend_ = true;
    this.width_ = 1;
    this.capRadius_ = 1;
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.width_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.getWidth = function () {
    return this.width_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.isWidthAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.isWidthAxisSizeDepend = function () {
    return this.isWidthAxisSizeDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.capRadius_ = null;
/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.setCapRadius = function (value) {
    this.capRadius_ = value;
};
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.getCapRadius = function () {
    return this.capRadius_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.isCapRadiusAxisSideDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.isCapRadiusAxisSideDepend = function () {
    return this.isCapRadiusAxisSideDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.capPadding_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.getCapPadding = function () {
    return this.capPadding_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.bulb_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    var temp;
    if (des.hasProp(data, 'width')) {
        temp = des.getStringProp(data, 'width');
        this.isWidthAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.width_ = this.isWidthAxisSizeDepend_ ? gaugeDes.getAxisSizeFactor(temp) : des.getNum(temp) / 100
    }

    if (des.hasProp(data, 'bulb_radius')) {
        temp = des.getStringProp(data, 'bulb_radius');
        this.isCapRadiusAxisSideDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.capRadius_ = this.isCapRadiusAxisSideDepend_ ? gaugeDes.getAxisSizeFactor(temp) : des.getNum(temp) / 100
    }

    if (des.hasProp(data, 'bulb_padding'))
        this.capPadding_ = des.getSimplePercentProp(data, 'bulb_padding');

    if (des.isEnabledProp(data, 'blub')) {
        this.bulb_ = new anychart.visual.background.Background();
        this.bulb_.deserialize(des.getProp(data, 'blub'));
    }

    return data;
};
//------------------------------------------------------------------------------
//
//                          ThermometerPointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                  Update.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calculatePaths();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};

//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.point_.getTermPathData());
};

//------------------------------------------------------------------------------
//
//                          ThermometerPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                             Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.ThermometerPointerStyleShapes();
};
//------------------------------------------------------------------------------
//
//                     TankStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState = function () {
    goog.base(this);
    this.width_ = .1;
    this.isWidthAxisSizeDepend_ = false;
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);
//------------------------------------------------------------------------------
//                         Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.width_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.getWidth = function () {
    return this.width_;
};
/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.isWidthAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.isWidthAxisSizeDepend = function () {
    return this.isWidthAxisSizeDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {int}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.color_ = '%color';
//------------------------------------------------------------------------------
//                  Drawing properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bulbMainFill_ = null;
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bulbBlick_ = null;
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bulbTopBlick_ = null;
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bodyMainFill_ = null;
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bodyBlick_ = null;
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.bodyTopBlick_ = null;
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.shade_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    if (des.hasProp(data, 'width')) {
        var strWidth = des.getStringProp(data, 'width');
        this.isWidthAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(strWidth);
        this.width_ = this.isWidthAxisSizeDepend_ ? gaugeDes.getAxisSizeFactor(strWidth) : des.getSimplePercent(strWidth);
    }
    if (des.hasProp(data, 'color'))
        this.color_ = des.getLStringProp(data, 'color');

    //deserialize drawing properties

    //bulb and body fill
    this.bulbMainFill_ = this.deserializeMailFill('0xFFFFFF', 0.3);
    this.bodyMainFill_ = this.deserializeMailFill(this.color_, 1);

    //bottom blick
    this.shade_ = new anychart.visual.stroke.Stroke();
    this.shade_.deserialize(
        {
            'thickness':'3',
            'color':'0xFFFFFF',
            'opacity':'0.3'
        }
    );

    //Blik
    this.bulbBlick_ = this.deserializeBlik(0.3);
    this.bodyBlick_ = this.deserializeBlik(1);

    //top blick
    this.bulbTopBlick_ = this.deserializeTopBlik('0xFFFFFF', 0.3);
    this.bodyTopBlick_ = this.deserializeTopBlik(this.color_, 1);
    return data;
};

/**
 * ND: Needs doc!
 * @param {String} color
 * @param {Number} opacity
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.deserializeMailFill = function (color, opacity) {
    //colors
    var color1 = 'DarkColor(' + color + ')';
    var color2 = 'LightColor(' + color + ')';

    //fill
    var fill = new anychart.visual.fill.Fill();
    fill.deserialize({
        'type':'gradient',
        'gradient':{
            'key':[
                {'color':color1, 'position':'0', 'opacity':opacity },
                {'color':color1, 'position':'0.05', 'opacity':opacity },
                {'color':color2, 'position':'0.85', 'opacity':opacity },
                {'color':color2, 'position':'0.85', 'opacity':opacity },
                {'color':color1, 'position':'1', 'opacity':opacity }
            ]
        }
    });
    return fill;
};
/**
 * @param {Number} opacity
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.deserializeBlik = function (opacity) {
    var fill = new anychart.visual.fill.Fill();
    fill.deserialize({
        'type':'gradient',
        'gradient':{
            'key':[
                {'color':'0xFFFFFF', 'position':'0', 'opacity':0 },
                {'color':'0xFFFFFF', 'position':'0.2', 'opacity':Number(160.0 / 255.0) * opacity},
                {'color':'0xFFFFFF', 'position':'0.25', 'opacity':Number(140.0 / 255.0) * opacity},
                {'color':'0xFFFFFF', 'position':'0.3', 'opacity':Number(30.0 / 255.0) * opacity},
                {'color':'0xFFFFFF', 'position':'0.35', 'opacity':0 },
                {'color':'0xFFFFFF', 'position':'1', 'opacity':0 }
            ]
        }
    });
    return fill;
};
/**
 * ND: Needs doc!
 * @param {String} color
 * @param {Number} opacity
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState.prototype.deserializeTopBlik = function (color, opacity) {
    var color1 = 'DarkColor(' + color + ')';
    var fill = new anychart.visual.fill.Fill();
    fill.deserialize({
        'type':'gradient',
        'gradient':{
            'key':[
                {'color':'0xFFFFFF', 'position':'0', 'opacity':0.1 * opacity },
                {'color':color1, 'position':'1', 'opacity':opacity}
            ]
        }
    });
    return fill;
};
//------------------------------------------------------------------------------
//
//                       TankHorizontalStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleState = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleState,
    anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.bodyMainFill_.getGradient().setAngle(-90);
    this.bulbMainFill_.getGradient().setAngle(-90);
    this.bodyBlick_.getGradient().setAngle(-90);
    this.bulbBlick_.getGradient().setAngle(-90);
    this.bodyTopBlick_.getGradient().setAngle(140);
    this.bulbTopBlick_.getGradient().setAngle(140);
    return data;
};
//------------------------------------------------------------------------------
//
//                       TankVerticalStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleState = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleState,
    anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleState);
//------------------------------------------------------------------------------
//                   Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.bodyTopBlick_.getGradient().setAngle(50);
    this.bulbTopBlick_.getGradient().setAngle(50);
    return data;
};
//------------------------------------------------------------------------------
//
//                     TankStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes,
    anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bulbMainShape_ = null;
/**
 * @private
 * @type {anychart.styles.StrokeStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bulbTopBlick_ = null;
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bulbBlick_ = null;
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bulbTopShade_ = null;
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bodyMainShape_ = null;
/**
 * @private
 * @type {anychart.styles.StrokeStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bodyTopBlick_ = null;
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bodyBlick_ = null;
/**
 * @private
 * @type {anychart.styles.FillStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bodyTopShade_ = null;
/**
 * @private
 * @type {anychart.styles.StrokeStyleShape}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.bottomBlick_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.createShapes = function () {
    //bulb main shape
    this.bulbMainShape_ = new anychart.styles.FillStyleShape();
    this.bulbMainShape_.initialize(this.point_, this, this.createPathElement);
    this.bulbMainShape_.getFillSettings = function (styleState) {
        return styleState.bulbMainFill_
    };
    this.bulbMainShape_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getMainPathData(this.point_.getBulbBounds())
        )
    };

    ////bulb top shade
    this.bulbTopShade_ = new anychart.styles.StrokeStyleShape();
    this.bulbTopShade_.initialize(this.point_, this, this.createPathElement);
    this.bulbTopShade_.getStrokeSettings = function (styleState) {
        return styleState.shade_
    };
    this.bulbTopShade_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getTopShadePathData(this.point_.getBulbBounds())
        )
    };

    //bulb blik shape
    this.bulbBlick_ = new anychart.styles.FillStyleShape();
    this.bulbBlick_.initialize(this.point_, this, this.createPathElement);
    this.bulbBlick_.getFillSettings = function (styleState) {
        return styleState.bulbBlick_
    };
    this.bulbBlick_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getMainPathData(this.point_.getBulbBounds())
        )
    };

    //bulb top blick
    this.bulbTopBlick_ = new anychart.styles.FillStyleShape();
    this.bulbTopBlick_.initialize(this.point_, this, this.createPathElement);
    this.bulbTopBlick_.getFillSettings = function (styleState) {
        return styleState.bulbTopBlick_
    };
    this.bulbTopBlick_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getTopBlikPathData(this.point_.getBulbBounds())
        )
    };


    //body main shape
    this.bodyMainShape_ = new anychart.styles.FillStyleShape();
    this.bodyMainShape_.initialize(this.point_, this, this.createPathElement);
    this.bodyMainShape_.getFillSettings = function (styleState) {
        return styleState.bodyMainFill_
    };
    this.bodyMainShape_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getMainPathData(this.point_.getBodyBounds())
        )
    };

    //body top shade shape
    this.bodyTopShade_ = new anychart.styles.StrokeStyleShape();
    this.bodyTopShade_.initialize(this.point_, this, this.createPathElement);
    this.bodyTopShade_.getStrokeSettings = function (styleState) {
        return styleState.shade_
    };
    this.bodyTopShade_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getTopShadePathData(this.point_.getBodyBounds())
        )
    };

    //body blik shape
    this.bodyBlick_ = new anychart.styles.FillStyleShape();
    this.bodyBlick_.initialize(this.point_, this, this.createPathElement);
    this.bodyBlick_.getFillSettings = function (styleState) {
        return styleState.bodyBlick_
    };
    this.bodyBlick_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getMainPathData(this.point_.getBodyBounds())
        )
    };

    //body bottom blik
    this.bottomBlick_ = new anychart.styles.StrokeStyleShape();
    this.bottomBlick_.initialize(this.point_, this, this.createPathElement);
    this.bottomBlick_.getStrokeSettings = function (styleState) {
        return styleState.shade_
    };
    this.bottomBlick_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getBottomShadePathData(this.point_.getBodyBounds())
        )
    };

    //body top blick
    this.bodyTopBlick_ = new anychart.styles.FillStyleShape();
    this.bodyTopBlick_.initialize(this.point_, this, this.createPathElement);
    this.bodyTopBlick_.getFillSettings = function (styleState) {
        return styleState.bodyTopBlick_
    };
    this.bodyTopBlick_.updatePath = function () {
        this.element_.setAttribute(
            'd',
            this.point_.getTopBlikPathData(this.point_.getBodyBounds())
        )
    };
};

/**
 * ND: Needs doc!
 * @return {SVGPathElement}
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.createPathElement = function () {
    return this.point_.getSVGManager().createPath()
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.placeShapes = function () {
    if (this.point_.getAxis().getScale().isInverted()) {
        this.placeBulbShapes_();
        this.placeBodyShapes_();
    } else {
        this.placeBodyShapes_();
        this.placeBulbShapes_();
    }
};
/**
 * ND: Needs doc!
 * @private
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.placeBodyShapes_ = function () {
    var sprite = this.point_.getSprite();
    sprite.appendChild(this.bodyMainShape_.getElement());
    sprite.appendChild(this.bodyTopShade_.getElement());
    sprite.appendChild(this.bottomBlick_.getElement());
    sprite.appendChild(this.bodyBlick_.getElement());
    sprite.appendChild(this.bodyTopBlick_.getElement());
};
/**
 * ND: Needs doc!
 * @private
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.placeBulbShapes_ = function () {
    var sprite = this.point_.getSprite();
    sprite.appendChild(this.bulbMainShape_.getElement());
    sprite.appendChild(this.bulbTopShade_.getElement());
    sprite.appendChild(this.bulbBlick_.getElement());
    sprite.appendChild(this.bulbTopBlick_.getElement());
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calculateBounds();

    this.bulbMainShape_.update(opt_styleState, opt_updateData);
    this.bulbTopShade_.update(opt_styleState, opt_updateData);
    this.bulbBlick_.update(opt_styleState, opt_updateData);
    this.bulbTopBlick_.update(opt_styleState, opt_updateData);

    this.bodyMainShape_.update(opt_styleState, opt_updateData);
    this.bodyTopShade_.update(opt_styleState, opt_updateData);
    this.bodyBlick_.update(opt_styleState, opt_updateData);
    this.bodyTopBlick_.update(opt_styleState, opt_updateData);

    this.bottomBlick_.update(opt_styleState, opt_updateData);
};
//------------------------------------------------------------------------------
//
//                TankHorizontalStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleShapes,
    anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleShapes.prototype.placeShapes = function () {
    if (this.point_.getAxis().getScale().isInverted()) {
        this.placeBulbShapes_();
        this.placeBodyShapes_();
    } else {
        this.placeBodyShapes_();
        this.placeBulbShapes_();
    }
};
//------------------------------------------------------------------------------
//
//               TankVerticalStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleShapes,
    anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankStyleShapes);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleShapes.prototype.placeShapes = function () {
    if (this.point_.getAxis().getScale().isInverted()) {
        this.placeBodyShapes_();
        this.placeBulbShapes_();
    } else {
        this.placeBulbShapes_();
        this.placeBodyShapes_();
    }
};
//------------------------------------------------------------------------------
//
//                      TankHorizontalStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle,
    anychart.styles.Style);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankHorizontalStyleShapes();
};
//------------------------------------------------------------------------------
//
//                      TankVerticalStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle,
    anychart.styles.Style);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.linear.pointers.styles.TankVerticalStyleShapes();
};













