/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.axisBased.pointer');

goog.require('anychart.plots.gaugePlot.gauges.axisBased.pointer.styles');
//------------------------------------------------------------------------------
//
//                     GaugePointer class
//
//------------------------------------------------------------------------------
/**
 * Base gauge pointer.
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer = function () {
    this.bounds_ = new anychart.utils.geom.Rectangle();
    this.selected_ = false;
    this.isOver_ = false;
    this.editable_ = false;
    this.selectable_ = false;
    this.color_ = anychart.utils.deserialization.getColor('#F1683C');

};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.gauge_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getGauge = function () {
    return this.gauge_;
};

/**
 * @param {anychart.plots.gaugePlot.gauges.GaugeBase} gauge Gauge which contains pointer.
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setGauge = function (gauge) {
    this.gauge_ = gauge;
};
/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.selected_ = false;

/**
 * @protected
 * @type {int}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.type_ = -1;

/**
 * @protected
 * @type {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.name_ = null;
/**
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getName = function () {
    return this.name_;
};
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.hasName = function () {
    return this.name_ && this.name_ != '';
};

/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.editable_ = false;
/**
 * @param {Boolean} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setEditable = function (value) {
    this.editable_ = value;
};
/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.selectable_ = false;
/**
 * @param {Boolean} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setSelectable = function (value) {
    this.selectable_ = value;
};

/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.useHandCursor_ = true;
/**
 * @param {Boolean} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setUseHandCursor = function (value) {
    this.useHandCursor_ = value;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.axis_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getAxis = function () {
    return this.axis_;
};
/**
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setAxis = function (value) {
    this.axis_ = value;
};

/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getSprite = function () {
    return this.sprite_;
};
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getSVGManager = function () {
    return this.sprite_.getSVGManager();
};
/**
 * @private
 * @type {Object}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.customAttributes_ = null;
/**
 * @private
 * @type {anychart.actions.ActionsList}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.actions_ = null;
//------------------------------------------------------------------------------
//                      Styles.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.style_ = null;
/**
 * @return {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getStyle = function () {
    return this.style_;
};
/**
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.currentState_ = null;
/**
 * @return {anychart.styles.StyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getCurrentState = function () {
    return this.currentState_;
};
/**
 * @private
 * @type {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.currentStateString_ = null;
//------------------------------------------------------------------------------
//                          Elements.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.elements.GaugeLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.label_ = null;
/**
 * @param {anychart.plots.gaugePlot.elements.GaugeLabel} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setLabel = function (value) {
    this.label_ = value;
};
/**
 * @private
 * @type {anychart.plots.gaugePlot.elements.GaugeLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.tooltip_ = null;
/**
 * @param {anychart.plots.gaugePlot.elements.GaugeLabel} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setTooltip = function (value) {
    this.tooltip_ = value;
};
//------------------------------------------------------------------------------
//                      Elements sprite.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.labelSprite_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.tooltipSprite_ = null;
//------------------------------------------------------------------------------
//                  Value.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.value_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getValue = function () {
    return this.value_;
};
/**
 * ND: Needs doc!
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setValue = function(value) {
    this.value_ = value;
};

/**
 * @private
 * @type {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.strValue_ = null;

/**
 * ND: Needs doc!
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.fixValueScaleOut = function (value) {
    var scale = this.axis_.getScale();
    if (!scale.isMinimumAuto_ && value < scale.getMinimum()) return scale.getMinimum();
    if (!scale.isMaximumAuto_ && value > scale.getMaximum()) return scale.getMaximum();
    return value;
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getBounds = function () {
    return this.bounds_;
};
//------------------------------------------------------------------------------
//                  Visual.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.color.Color}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.color_ = null;
/**
 * @return {anychart.visual.color.Color}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getColor = function () {
    return this.color_;
};
/**
 * @param {anychart.visual.color.Color} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setColor = function (value) {
    this.color_ = value;
};
/**
 * @private
 * @type {anychart.visual.hatchFill.HatchFill.HatchFillType}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.hatchType_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill.HatchFillType}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getHatchType = function () {
    return this.hatchType_;
};
/**
 * @param {anychart.visual.hatchFill.HatchFill.HatchFillType} value
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setHatchType = function (value) {
    this.hatchType_ = value;
};
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @protected
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getStyleNodeName = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @protected
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getInlineStyleName = function () {
    return this.getStyleNodeName();
};
/**
 * ND: Needs doc!
 * @protected
 * @return {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.createStyle = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @private
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setStyle_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    var styleName = des.hasProp(data, 'style') ? des.getLStringProp(data, 'style') : 'anychart_default';


    var styleNodeName = this.getStyleNodeName();

    var styleData = this.getInlineStyleName() ? des.getProp(data, this.getInlineStyleName()) : data;
    var actualStyleXML;

    if (styleData) {
        actualStyleXML = stylesList.getStyleByMerge(styleNodeName, styleData, styleName);
    } else {
        var cashedStyle = stylesList.getStyle(styleNodeName, styleName);
        if (cashedStyle != null) {
            this.style_ = cashedStyle;
            return;
        } else {
            actualStyleXML = stylesList.getStyleByApplicator(styleNodeName, styleName);
        }
    }

    if (!actualStyleXML) {
        this.style_ = stylesList.getStyle(styleNodeName, 'anychart_default');
    } else {
        this.style_ = this.createStyle();
        this.style_.deserialize(actualStyleXML);

        if (styleData)
            stylesList.addStyle(styleNodeName, styleName, this.style_);
    }
}
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @param {Object} data
 * @param {anychart.styles.StylesList} styleList
 * @param {anychart.chartView.MainChartView} mainChartView
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.deserialize = function (data, styleList, mainChartView) {
    var des = anychart.utils.deserialization;
    this.setStyle_(data, styleList);
    this.rotateGradient(this.style_);

    if (des.hasProp(data, 'name'))
        this.name_ = des.getProp(data, 'name');

    if (des.hasProp(data, 'value'))
        this.value_ = des.getNumProp(data, 'value');

    if (des.hasProp(data, 'selected'))
        this.selected_ = des.getBoolProp(data, 'selected');

    if (des.hasProp(data, 'editable'))
        this.editable_ = des.getBoolProp(data, 'editable');

    if (des.hasProp(data, 'use_hand_cursor'))
        this.useHandCursor_ = des.getBoolProp(data, 'use_hand_cursor');

    if (des.hasProp(data, 'allow_select'))
        this.selectable_ = des.getBoolProp(data, 'allow_select');

    if (des.hasProp(data, 'attributes')) {
        this.customAttributes_ = {};
        var attributesData = des.getProp(data, 'attributes');
        var attributes = des.getPropArray(attributesData, 'attribute');
        var attrsCount = attributes.length;

        for (var i = 0; i < attrsCount; i++) {
            var atr = attributes[i];
            if (des.hasProp(atr, 'name')) {
                //old version support
                var tempVal;
                if (des.hasProp(atr, 'custom_attribute_value'))
                    tempVal = des.getProp(atr, 'custom_attribute_value');
                else if (des.hasProp(atr, 'value'))
                    tempVal = des.getProp(atr, 'value');

                if (tempVal) this.customAttributes_['%' + des.getStringProp(atr, 'name')] = tempVal;
            }
        }
    }

    if (des.hasProp(data, 'actions')) {
        this.actions_ = new anychart.actions.ActionsList(mainChartView, this);
        this.actions_.deserialize(des.getProp(data, 'actions'));
    }

    if (des.hasProp(data, 'color'))
        this.color_ = des.getColorProp(data, 'color');

    if (des.hasProp(data, 'hath_type'))
        this.hatchType_ = anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(
            des.getLStringProp(data, 'hatch_type'));

    if (des.hasProp(data, 'label'))
        this.label_.deserializeGlobalSettings(des.getProp(data, 'label'), styleList);

    if (des.hasProp(data, 'tooltip'))
        this.tooltip_.deserializeGlobalSettings(des.getProp(data, 'tooltip'), styleList);

    if (this.value_ && !isNaN(this.value_)) this.strValue_ = this.value_.toString();
};

anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.rotateGradient = function (style) {
    if (style.getNormal().initializeRotation) {
        style.getNormal().initializeRotation(this.gauge_);
        style.getHover().initializeRotation(this.gauge_);
        style.getPushed().initializeRotation(this.gauge_);
        style.getSelectedNormal().initializeRotation(this.gauge_);
        style.getSelectedHover().initializeRotation(this.gauge_);
    }
};
//------------------------------------------------------------------------------
//                  Interactivity.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @protected
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.initListeners = function (sprite) {
    if (this.useHandCursor_) this.getSVGManager().setCursorPointer(sprite.getElement());
    this.setupListeners(sprite);
};
/**
 * ND: Needs doc!
 * @protected
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setupListeners = function (sprite) {
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEOVER, this.onMouseOver_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEOUT, this.onMouseOut_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEDOWN, this.onMouseDown_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.CLICK, this.onMouseClick_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this);

};
/**
 * Pointer mouse over handler
 * @param {goog.events.Event} event
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.onMouseOver_ = function (event) {
    this.isOver_ = true;
    this.currentStateString_ = this.selected_ ? 'SelectedHover' : 'Hover';
    this.currentState_ = this.getCurrentStyleState(this.style_);
    this.styleShapes_.update(this.currentState_, false);

    if (this.tooltipSprite_) {
        var tooltipState = this.getCurrentStyleState(this.tooltip_.getStyle());
        this.tooltipSprite_.update(tooltipState);
        this.tooltipSprite_.setVisibility(true);
    }
    /*if (event != null)
     this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OVER);*/
};
/**
 * Pointer mouse out handler
 * @param {goog.events.Event} event
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.onMouseOut_ = function (event) {
    this.isOver_ = false;
    this.currentStateString_ = this.selected_ ? 'SelectedNormal' : 'Normal';
    this.currentState_ = this.getCurrentStyleState(this.style_);
    this.styleShapes_.update(this.currentState_, false);

    if (this.tooltipSprite_) this.tooltipSprite_.setVisible(false);
    /* this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OUT);*/
};
/**
 * Pointer mouse down handler
 * @param {goog.events.Event} event
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.onMouseDown_ = function (event) {
    if (this.actions_) this.actions_.execute(this);
    if (!this.selected_) {
        this.currentStateString_ = 'Pushed';
        this.currentState_ = this.getCurrentStyleState(this.style_);
        this.styleShapes_.update(this.currentState_, false);
    }
    /*
     /*if (this.editable) {
     this.startValueChange();
     this.container.mouseEnabled = false;
     this.container.stage.addEventListener(MouseEvent.MOUSE_MOVE, stageMouseMoveHandler);
     this.container.stage.addEventListener(MouseEvent.MOUSE_UP, stageMouseClickHandler);
     }
     this.dispatchAnyChartPointEvent(event, PointEvent.POINT_CLICK);*/
};
/**
 * Pointer mouse click handler
 * @param {goog.events.Event} event
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.onMouseClick_ = function (event) {
    if (this.selectable_ && !this.selected_) {
        if (this.gauge_.selectedPointer) this.gauge_.selectedPointer.deselect();
        this.gauge_.selectedPointer = this;
        this.selected_ = true;
        this.onMouseOver_(event);
    }
};
/**
 * Pointer mouse move handler
 * @param {goog.events.Event} event
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.onMouseMove_ = function (event) {
    if (this.isOver_ && this.tooltipSprite_)
        this.tooltip_.execMoveTooltip(
            event,
            this,
            this.getCurrentStyleState(this.tooltip_.getStyle()),
            this.tooltipSprite_);
};
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.deselect = function () {
    this.selected_ = false;
    this.onMouseOut_(null)
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.initialize = function (svgManager) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.initListeners(this.sprite_);
    this.selected_ = this.selectable_ && this.selected_;

    this.currentStateString_ = (this.selected_) ?
        'SelectedNormal' :
        'Normal';

    this.currentState_ = this.getCurrentStyleState(this.style_);

    this.styleShapes_ = this.style_.initialize(this);

    if (this.label_) {
        this.labelSprite_ = this.label_.getStyle().initialize(this, this.label_);
        if (this.labelSprite_) this.initListeners(this.labelSprite_);
    }

    if (this.tooltip_)
        this.tooltipSprite_ = this.tooltip_.getStyle().initialize(this, this.tooltip_);


    return  this.sprite_;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Update pointer style and path
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.draw = function () {
    this.styleShapes_.update(this.currentState_, true);
    if (this.label_)
        this.drawElement(
            this.label_,
            this.labelSprite_
        );
};
/**
 * @protected
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.elements.ElementsSprite} elementSprite
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.drawElement = function (element, elementSprite) {
    if (elementSprite && element)
        elementSprite.update(this.getCurrentStyleState(element.getStyle()));
};
/**
 * @param {anychart.styles.Style} style
 * @return {anychart.styles.StyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getCurrentStyleState = function (style) {
    return this.getStateByName(style, this.currentStateString_);

};
/**
 * @private
 * @param {anychart.styles.Style} style
 * @param {String} stateName
 * @return {anychart.styles.StyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getStateByName = function (style, stateName) {
    switch (stateName) {
        default:
        case 'Normal':
            return style.getNormal();
        case 'Hover':
            return style.getHover();
        case 'Pushed':
            return style.getPushed();
        case 'SelectedNormal':
            return style.getSelectedNormal();
        case 'SelectedHover':
            return style.getSelectedHover();
        case 'Missing':
            return style.getMissing();
    }
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Update pointer path
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.resize = function () {
    this.styleShapes_.update(null, true);
    this.resizeElement(
        this.label_,
        this.labelSprite_
    );
};
/**
 * @protected
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.elements.ElementsSprite} elementSprite
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.resizeElement = function (element, elementSprite) {
    if (element && elementSprite)
        elementSprite.update(this.getCurrentStyleState(element.getStyle()));
};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getTokenValue = function (token) {
    if (token == '%Value') return this.strValue_;
    if (token == '%Name') return this.name_;
    if (this.customAttributes_[token])
        return this.customAttributes_[token];
    return '';
};

anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getTokenType = function (token) {
    if (token == '%Value') return anychart.formatting.TextFormatTokenType.NUMBER;
    return anychart.formatting.TextFormatTokenType.TEXT;
};
//------------------------------------------------------------------------------
//                  Elements position.
//------------------------------------------------------------------------------
/**
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.styles.StyleState} state
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getElementPosition = function (element, state, bounds) {
    if (!element || !state || !bounds || (this.isMissing_ && !this.plot_.ignoreMissingPoints_)) return null;
    var res = new anychart.utils.geom.Point();
    this.getAnchorPoint(res, state.getAnchor());
    if (state.getAnchor() != anychart.layout.Anchor.X_AXIS) {
        this.setElementHorizontalAlign(res, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
        this.setElementVerticalAlign(res, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
    }
    return res;
};
/**
 *
 * @param {anychart.styles.StyleState} state
 * @param {anychart.svg.SVGSprite} sprite
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {Number} x
 * @param {Number} y
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getFixedElementPosition = function (state, sprite, bounds, x, y) {
    if (state) return null;
    var pos = new anychart.utils.geom.Point();
    pos.x = this.gauge_.getBounds().x + this.gauge_.getBounds().width * x;
    pos.y = this.gauge_.getBounds().y + this.gauge_.getBounds().height * y;
    this.setElementHorizontalAlign(pos, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
    this.setElementVerticalAlign(pos, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
    return pos;
}
/**
 * ND: Needs doc!
 * @private
 * @param {anychart.utils.geom.Point} pos
 * @param {anychart.layout.HorizontalAlign} hAlign
 * @param {anychart.utils.geom.Rectangle} elementSize
 * @param {Number} padding
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setElementHorizontalAlign = function (point, anchor, hAlign, vAlign, bounds, padding) {
    switch (hAlign) {
        case anychart.layout.HorizontalAlign.LEFT:
            point.x -= bounds.width + padding;
            break;
        case anychart.layout.HorizontalAlign.RIGHT:
            point.x += padding;
            break;
        case anychart.layout.HorizontalAlign.CENTER:
            point.x -= bounds.width / 2;
            break;
    }
};


/**
 * ND: Needs doc!
 * @private
 * @param {anychart.utils.geom.Point} pos
 * @param {uint} vAlign
 * @param {anychart.utils.geom.Rectangle} elementSize
 * @param {Number} padding
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.setElementVerticalAlign = function (point, anchor, hAlign, vAlign, bounds, padding) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP:
            point.y -= bounds.height + padding;
            break;
        case anychart.layout.VerticalAlign.BOTTOM:
            point.y += padding;
            break;
        case anychart.layout.VerticalAlign.CENTER:
            point.y -= bounds.height / 2;
            break;
    }
};
/**
 * ND: Needs doc!
 * @param {anychart.layout.Anchor} anchor
 * @param {anychart.utils.geom.Point} pos
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer.prototype.getAnchorPoint = function (anchor, pos) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER:
            pos.x = this.bounds_.x + this.bounds_.width / 2;
            pos.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_BOTTOM:
            pos.x = this.bounds_.x + this.bounds_.width / 2;
            pos.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.CENTER_LEFT:
            pos.x = this.bounds_.x;
            pos.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_RIGHT:
            pos.x = this.bounds_.x + this.bounds_.width;
            pos.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_TOP:
            pos.x = this.bounds_.x + this.bounds_.width / 2;
            pos.y = this.bounds_.y;
            break;
        case anychart.layout.Anchor.LEFT_BOTTOM:
            pos.x = this.bounds_.x;
            pos.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.LEFT_TOP:
            pos.x = this.bounds_.x;
            pos.y = this.bounds_.y;
            break;
        case anychart.layout.Anchor.RIGHT_BOTTOM:
            pos.x = this.bounds_.x + this.bounds_.width;
            pos.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.RIGHT_TOP:
            pos.x = this.bounds_.x + this.bounds_.width;
            pos.y = this.bounds_.y;
            break;
    }
};





