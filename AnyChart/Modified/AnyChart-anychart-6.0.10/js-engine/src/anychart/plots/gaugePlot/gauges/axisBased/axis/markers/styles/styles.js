/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles');

goog.require('anychart.styles.background');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.pointer.styles');
//------------------------------------------------------------------------------
//
//                          GaugeAxisMarkerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState = function (style, opt_stateType) {
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.call(style, opt_stateType);
};

goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);
//------------------------------------------------------------------------------
//
//                          GaugeAxisMarkerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.labelSptite_ = null;
//------------------------------------------------------------------------------
//                  Update.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.calculateBounds();
    this.updateLabel();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
//------------------------------------------------------------------------------
//                  Marker label.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.updateLabel = function () {
    var marker = this.point_; //todo: upgrade style logic
    var label = marker.getStyle().getNormal().getLabel();
    var p = new anychart.utils.geom.Point(0, 0);
    if (label) {
        var textElement = label.getTextElement();

        //update font size
        if (label.getFontSize()) {
            var size = (label.isFontSizeAxisDepend() ? marker.getAxis().getSize() : 1) * label.getFontSize();
            textElement.setSize(size);
        }
        textElement.initSize(marker.getSVGManager(), textElement.getText());

        var val = marker.getAxis().getScale().transformValueToPixel(isNaN(label.getValue()) ?
            this.getDefaultValue() :
            label.getValue());

        var pos = new anychart.utils.geom.Point();
        this.getLabelPosition(pos, val, label.getAlign(), label.getPadding());

        if (!this.labelSprite_) { //todo: remove lazy after upgrade style logic
            this.labelSprite_ = label.getTextElement().createSVGText(marker.getSVGManager());
            marker.getSprite().appendSprite(this.labelSprite_);
        }

        p = textElement.update(
            marker.getSVGManager(),
            this.labelSprite_,
            0,
            0,
            marker.getAxis().getGauge().getBounds().width,
            marker.getAxis().getGauge().getBounds().height, marker.getColor());

    }

    if (this.labelSprite_) this.labelSprite_.setPosition(pos.x + p.x, pos.y + p.y);
};
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.getDefaultValue = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @protected
 * @param {anychart.utils.geom.Point} pos
 * @param {Number} pixVal
 * @param {anychart.plots.gaugePlot.layout.Align} align
 * @param {Number} padding
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.getLabelPosition = function (pos, pixVal, align, padding) {
    var axis = this.point_.getAxis();
    if (axis instanceof anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis) {
        var r = axis.getRadius(align, padding, 0);
        axis.setPixelPoint(r, pixVal, pos);
    } else {
        if (axis.getGauge().isHorizontal()) {
            pos.x = pixVal;
            pos.y = axis.getPosition(align, padding, 0);
        } else {
            pos.y = pixVal;
            pos.x = axis.getPosition(align, padding, 0);
        }
    }
};
//------------------------------------------------------------------------------
//
//                          ColorRangeStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState = function (style, opt_stateType) {
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState.call(style, opt_stateType);
    this.startWidth_ = .5;
    this.endWidth_ = .2;
    this.isEndSizeDepend_ = false;
    this.isStartSizeDepend_ = false
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.startWidth_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.getStartWidth = function () {
    return this.startWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.endWidth_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.getEndWidth = function () {
    return this.endWidth_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.isStartSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.isStartSizeDepend = function () {
    return this.isStartSizeDepend_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.isEndSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.isEndSizeDepend = function () {
    return this.isEndSizeDepend_;
};
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.label_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.getLabel = function () {
    return this.label_;
};
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;
    var size;

    if (des.hasProp(data, 'start_size')) {
        size = des.getStringProp(data, 'start_size');
        this.isStartSizeDepend_ = gaugeDes.isAxisSizeDepend(size);
        this.startWidth_ = this.isStartSizeDepend_ ? gaugeDes.getAxisSizeFactor(size) : des.getNum(size) / 100;
    }

    if (des.hasProp(data, 'end_size')) {
        size = des.getStringProp(data, 'end_size');
        this.isEndSizeDepend_ = gaugeDes.isAxisSizeDepend(size);
        this.endWidth_ = this.isEndSizeDepend_ ? gaugeDes.getAxisSizeFactor(size) : des.getNum(size) / 100;
    }

    if (des.isEnabledProp(data, 'label')) {
        this.label_ = new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel();
        this.label_.deserialize(des.getProp(data, 'label'));
    }
    return data;
};
//------------------------------------------------------------------------------
//
//                       ColorRangeStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes);
//------------------------------------------------------------------------------
//                       Bounds.
//------------------------------------------------------------------------------
/**
 * Calculate marker bounds.
 * @protected
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes.prototype.calculateBounds = function () {
    var marker = this.point_; //todo: upgrade style logic
    var axis = marker.getAxis();
    var scale = axis.getScale();
    var state = marker.getCurrentStyleState();
    var bounds = marker.getBounds();

    var startValue = scale.transformValueToPixel(marker.getStart());
    var endValue = scale.transformValueToPixel(marker.getEnd());

    this.startInnerPt_ = new anychart.utils.geom.Point();
    this.endInnerPt_ = new anychart.utils.geom.Point();
    this.startOuterPt_ = new anychart.utils.geom.Point();
    this.endOuterPt_ = new anychart.utils.geom.Point();

    var startPercentW = (state.isStartSizeDepend() ? axis.getSize() : 1) * state.getStartWidth();
    var endPercentW = (state.isEndSizeDepend() ? axis.getSize() : 1) * state.getEndWidth();

    axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), startPercentW, true), startValue, this.startInnerPt_);
    axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), endPercentW, true), endValue, this.endInnerPt_);
    axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), startPercentW, false), startValue, this.startOuterPt_);
    axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), endPercentW, false), endValue, this.endOuterPt_);

    bounds.x = Math.min(this.startInnerPt_.x, this.endInnerPt_.x, this.startOuterPt_.x, this.endOuterPt_.x);
    bounds.y = Math.min(this.startInnerPt_.y, this.endInnerPt_.y, this.startOuterPt_.y, this.endOuterPt_.y);
    bounds.width = Math.max(this.startInnerPt_.x, this.endInnerPt_.x, this.startOuterPt_.x, this.endOuterPt_.x) - bounds.x;
    bounds.height = Math.max(this.startInnerPt_.y, this.endInnerPt_.y, this.startOuterPt_.y, this.endOuterPt_.y) - bounds.y;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes.prototype.getDefaultValue = function () {
    var marker = this.point_;
    return (marker.getStart() + marker.getEnd()) / 2;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes.prototype.updatePath = function (element) {
    var marker = this.point_; //todo: upgrade style logic
    var svgManager = marker.getSVGManager();

    var pathData = svgManager.pMove(this.startInnerPt_.x, this.startInnerPt_.y);
    pathData += svgManager.pLine(this.endInnerPt_.x, this.endInnerPt_.y);
    pathData += svgManager.pLine(this.endOuterPt_.x, this.endOuterPt_.y);
    pathData += svgManager.pLine(this.startOuterPt_.x, this.startOuterPt_.y);
    pathData += svgManager.pLine(this.startInnerPt_.x, this.startInnerPt_.y);

    element.setAttribute('d', pathData);
};
//------------------------------------------------------------------------------
//
//                          ColorRangeStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyleShapes();
};
//------------------------------------------------------------------------------
//
//                          CircularColorRangeStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes);
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes.prototype.calculateBounds = function () {
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes.prototype.updatePath = function (element) {
    var marker = this.point_; //todo: upgrade style logic
    var axis = marker.getAxis();
    var scale = axis.getScale();
    var state = marker.getCurrentStyleState();
    var bounds = marker.getBounds();

    var startPercentW = (state.isStartSizeDepend() ? axis.size : 1) * state.getStartWidth();
    var endPercentW = (state.isEndSizeDepend() ? axis.size : 1) * state.getEndWidth();

    var baseStartR = axis.getComplexRadius(state.getAlign(), state.getPadding(), startPercentW, true),
        baseEndR = axis.getComplexRadius(state.getAlign(), state.getPadding(), endPercentW, true),
        startR = axis.getComplexRadius(state.getAlign(), state.getPadding(), startPercentW, false),
        endR = axis.getComplexRadius(state.getAlign(), state.getPadding(), endPercentW, false);

    var startAngle = scale.transformValueToPixel(marker.getStart()),
        endAngle = scale.transformValueToPixel(marker.getEnd()),
        path = this.doDrawRangeMarker_(marker.getSVGManager(), baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle);

    element.setAttribute('d', path);
};
/**
 * ND: Needs doc!
 * @param {*} baseStartR
 * @param {*} baseEndR
 * @param {*} startR
 * @param {*} endR
 * @param {anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis} axis
 * @param {*} startAngle
 * @param {*} endAngle
 * @private
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes.prototype.doDrawRangeMarker_ = function (svgManager, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle) {
    var sweepAngle = Math.abs(startAngle - endAngle);
    var center = axis.getGauge().getPixPivotPoint(),
        path = '';


    var largeArc = 1, sweepArc = (axis.getScale().isInverted() ? 0 : 1);
    if (sweepAngle < 180) {
        largeArc = 0;
    }
    if (baseStartR == baseEndR) {
        var startX = center.x + baseStartR * Math.cos(startAngle * Math.PI / 180),
            startY = center.y + baseStartR * Math.sin(startAngle * Math.PI / 180),
            endX = center.x + baseStartR * Math.cos(endAngle * Math.PI / 180),
            endY = center.y + baseStartR * Math.sin(endAngle * Math.PI / 180);

        path += svgManager.pMove(startX, startY);
        path += svgManager.pCircleArc(baseStartR, largeArc, sweepArc, endX, endY, baseStartR);
    } else {
        var pt = new anychart.utils.geom.Point();
        axis.setPixelPoint(baseStartR, startAngle, pt);
        path += svgManager.pMove(pt.x, pt.y);
        path += this.drawDynamicArc_(svgManager, axis, startAngle, endAngle, baseStartR, baseEndR);
    }
    if (endR == startR) {
        startX = center.x + endR * Math.cos(startAngle * Math.PI / 180);
        startY = center.y + endR * Math.sin(startAngle * Math.PI / 180);
        endX = center.x + endR * Math.cos(endAngle * Math.PI / 180);
        endY = center.y + endR * Math.sin(endAngle * Math.PI / 180);
        path += svgManager.pLine(endX, endY);
        path += svgManager.pCircleArc(endR, largeArc, !sweepArc, startX, startY, endR);
//        DrawingUtils.drawArc(g, center.x, center.y, endAngle, startAngle, endR, endR, 0, false);
    } else {
        path += this.drawDynamicArc_(svgManager, axis, endAngle, startAngle, endR, startR);
    }
    return path;
};
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes.prototype.drawDynamicArc_ = function (svgManager, axis, startAngle, endAngle, startR, endR) {
    var path = '';
    var angle, r,
        dR = (endR - startR) / (endAngle - startAngle),
        pt = new anychart.utils.geom.Point();
    if (endAngle > startAngle) {
        for (angle = startAngle; angle <= endAngle; angle++) {
            r = startR + dR * (angle - startAngle);
            axis.setPixelPoint(r, angle, pt);
            path += svgManager.pLine(pt.x, pt.y);
        }
    } else {
        for (angle = startAngle; angle >= endAngle; angle--) {
            r = startR + dR * (angle - startAngle);
            axis.setPixelPoint(r, angle, pt);
            path += svgManager.pLine(pt.x, pt.y);
        }
    }
    axis.setPixelPoint(endR, endAngle, pt);
    path += svgManager.pLine(pt.x, pt.y);
    pt = null;
    return path;
};
//------------------------------------------------------------------------------
//
//                          CircularColorRangeStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle,
    anychart.styles.background.BackgroundStyle);
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.ColorRangeStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyleShapes();
};
//------------------------------------------------------------------------------
//
//                       TrendlineStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 *
 * @param {*} style
 * @param {*} opt_stateType
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState = function (style, opt_stateType) {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.label_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.getLabel = function () {
    return this.label_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.size_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.getSize = function () {
    return this.size_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.isSizeAxisSizeDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.isSizeAxisSizeDepend = function () {
    return this.isSizeAxisSizeDepend_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    if (des.hasProp(data, 'size')) {
        var strSize = des.getStringProp(data, 'size');
        this.isSizeAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(strSize);
        this.size_ = (this.isSizeAxisSizeDepend_) ?
            gaugeDes.getAxisSizeFactor(strSize) :
            des.getSimplePercent(strSize);
    }

    if (des.isEnabledProp(data, 'line')) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(des.getProp(data, 'line'));
    }

    if (des.hasProp(data, 'label')) {
        this.label_ = new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel();
        this.label_.deserialize(des.getProp(data, 'label'));
    }

    this.fill_ = null;
    this.hatchFill_ = null;
};
//------------------------------------------------------------------------------
//
//                     TrendlineStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.start_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.end_ = null;
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.calculateBounds = function () {
    var marker = this.point_; //todo: upgrade style logic
    var axis = marker.getAxis();
    var scale = axis.getScale();

    this.start_ = new anychart.utils.geom.Point();
    this.end_ = new anychart.utils.geom.Point();

    var value = scale.transformValueToPixel(marker.getValue());

    var state = marker.getCurrentStyleState();
    var size = (state.isSizeAxisSizeDepend() ? axis.getSize() : 1) * state.getSize();

    if (axis instanceof anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis) {
        var startR = axis.getComplexRadius(state.getAlign(), state.getPadding(), size, true);
        var endR = axis.getComplexRadius(state.getAlign(), state.getPadding(), size, false);

        axis.setPixelPoint(startR, value, this.start_);
        axis.setPixelPoint(endR, value, this.end_);
    } else {
        axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), size, true), value, this.start_);
        axis.setPoint(axis.getComplexPosition(state.getAlign(), state.getPadding(), size, false), value, this.end_);
    }
    var bounds = marker.getBounds();
    bounds.x = Math.min(this.start_.x, this.end_.x);
    bounds.y = Math.min(this.start_.y, this.end_.y);
    bounds.width = Math.max(this.start_.x, this.end_.x) - bounds.x;
    bounds.height = Math.max(this.start_.y, this.end_.y) - bounds.y;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes.prototype.getDefaultValue = function () {
    var marker = this.point_;
    return marker.getValue();
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.update = function (opt_styleState, opt_updatePath) {
    if (opt_updatePath) this.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updatePath);
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes.prototype.updatePath = function (element) {
    var marker = this.point_; //todo: upgrade style logic
    var svgManager = marker.getSVGManager();

    var path = svgManager.pMove(this.start_.x, this.start_.y);
    path += svgManager.pLine(this.end_.x, this.end_.y);
    element.setAttribute('d', path);
};
//------------------------------------------------------------------------------
//
//                       TrendlineStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.line.LineStyle}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyleShapes();
};
//------------------------------------------------------------------------------
//
//                      CustomLabelStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState.prototype.label_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState.prototype.getLabel = function () {
    return this.label_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState.prototype.tickmark_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState.prototype.getTickmark = function () {
    return this.tickmark_;
};
//------------------------------------------------------------------------------
//                        Deserialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'label')) {
        this.label_ = new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel();
        this.label_.deserialize(des.getProp(data, 'label'));
    }

    if (des.hasProp(data, 'tickmark')) {
        this.tickmark_ = new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark();
        this.tickmark_.deserialize(des.getProp(data, 'tickmark'));
    }
    return data;
};
//------------------------------------------------------------------------------
//
//                     CustomLabelStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.GaugeAxisMarkerStyleShapes);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.tickmarkSptite_ = null;
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.createShapes = function () {
    var marker = this.point_;
    if (marker.getStyle().getNormal().getTickmark())
        this.tickmarkSptite_ = new anychart.svg.SVGSprite(marker.getSVGManager());
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.placeShapes = function () {
    if (this.tickmarkSptite_) this.point_.getSprite().appendSprite(this.tickmarkSptite_);
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    this.updateLabel();
    this.updateTickmark();
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.getDefaultValue = function () {
    var marker = this.point_;
    return marker.getValue();
};
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes.prototype.updateTickmark = function () {
    if (this.tickmarkSptite_) {
        this.tickmarkSptite_.clear();
        var tickmark = this.point_.getAxis().drawTickmark(
            this.point_.getStyle().getNormal().getTickmark(),
            this.point_.getAxis().getScale().transformValueToPixel(this.point_.getValue()),
            this.point_.getColor().getColor()
        );
        this.tickmarkSptite_.appendSprite(tickmark);
    }
};
//------------------------------------------------------------------------------
//
//                     CustomLabelStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyleShapes();
};
















