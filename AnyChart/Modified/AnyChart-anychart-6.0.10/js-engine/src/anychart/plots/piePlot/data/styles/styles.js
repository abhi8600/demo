/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.data.styles.PieFillStyleShape}</li>
 *  <li>@class {anychart.plots.piePlot.data.styles.PieHatchFillStyleShape}</li>
 *  <li>@class {anychart.plots.piePlot.data.styles.PieStyleShapes}</li>
 *  <li>@class {anychart.plots.piePlot.data.styles.PieStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.data.styles');
goog.require('anychart.styles.background');
goog.require('anychart.styles.aqua');
//------------------------------------------------------------------------------
//
//                          PieFillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillWithStrokeStyleShape}
 */
anychart.plots.piePlot.data.styles.PieFillStyleShape = function () {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieFillStyleShape,
    anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.styles.PieFillStyleShape.prototype.updatePath = function () {
    var path = this.point_.getPathData();
    this.element_.setAttribute(
        'd',
        path);
};
//------------------------------------------------------------------------------
//
//                          PieHatchFillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.HatchFillStyleShape}
 */
anychart.plots.piePlot.data.styles.PieHatchFillStyleShape = function () {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieHatchFillStyleShape,
    anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.styles.PieHatchFillStyleShape.prototype.updatePath = function () {
    var path = this.point_.getPathData();
    this.element_.setAttribute(
        'd',
        path);
};
//------------------------------------------------------------------------------
//
//                          PieStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.piePlot.data.styles.PieStyleShapes = function () {
    anychart.styles.background.BackgroundStyleShapes.call(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);

//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.styles.PieStyleShapes.prototype.createShapes = function () {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.piePlot.data.styles.PieFillStyleShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.piePlot.data.styles.PieHatchFillStyleShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
/**@inheritDoc*/
anychart.plots.piePlot.data.styles.PieStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calcBounds();

    if (this.fillShape_)
        this.fillShape_.update(opt_styleState, opt_updateData);

    if (this.hatchFillShape_)
        this.hatchFillShape_.update(opt_styleState, opt_updateData);
};
/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.styles.PieStyleShapes.prototype.updateGradients = function() {
    return;
};

//------------------------------------------------------------------------------
//
//                         PieAquaShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.aqua.CircleFillShape}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaFillShape = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaFillShape,
    anychart.styles.aqua.CircleFillShape);

//------------------------------------------------------------------------------
//
//                         PieAquaOuterBorderShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.aqua.CircleFillShape}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaOuterBorderShape = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaOuterBorderShape,
    anychart.styles.aqua.CircleFillShape);

/**
 * ND: Needs doc!
 * @param {*} styleState
 */
anychart.plots.piePlot.data.styles.PieAquaOuterBorderShape.prototype.updateStyle = function(styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getBounds();

    style += styleState.getOuterBorder().getSVGFill(svgManager, bounds, color, true);
    this.currentStyle_ = style;

    this.element_.setAttribute('style', style);
};

//------------------------------------------------------------------------------
//
//                         PieAquaInnerBorderShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.aqua.CircleFillShape}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape,
    anychart.styles.aqua.CircleFillShape);
/**
 * ND: Needs doc!
 * @private
 */
anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape.prototype.makeConfig_ = function(innerRadius, outerRadius, index, opacity) {
    var n = innerRadius / outerRadius;

    var fillData = {};
    var gradientData = {};
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};

    key1['position'] = n;
    key1['opacity'] = opacity * 100.0 / 255.0;
    key1['color'] = 'Black';
    keys.push(key1);

    if (index == 0)
        key2['position'] = n + .1;
    else
        key2['position'] = n + .05;
    key2['opacity'] = '0';
    key2['color'] = 'Black';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = '0';
    key3['color'] = 'Black';
    keys.push(key3);

    gradientData['type'] = 'radial';
    gradientData['angle'] = '0';
    gradientData['focal_point'] = '0';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;
    return fillData;
};
/**
 * ND: Needs doc!
 * @param {*} styleState
 */
anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape.prototype.updateStyle = function(styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getBounds();

    var series = this.point_.getSeries();

    var innerRadius = series.getInnerRadius();
    var outerRadius = series.getOuterRadius();
    var index = series.getIndex();

    if (innerRadius > 0) {
        var fillData = this.makeConfig_(innerRadius, outerRadius, index, 1);
        styleState.getInnerBorder().deserialize(fillData);
        style += styleState.getInnerBorder().getSVGFill(svgManager, bounds, color, true);
    } else {
        style += 'fill:none;';
    }

    this.currentStyle_ = style;

    this.element_.setAttribute('style', style);
};

//------------------------------------------------------------------------------
//
//                    PieAquaStyleShapes
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaStyleShapes,
    anychart.styles.StyleShapes);
/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.getSprite = function() {
    return this.point_.getSprite();
};

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.piePlot.data.styles.PieAquaFillShape}
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.circleShape_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.piePlot.data.styles.PieAquaOuterBorderShape}
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.outerBorderShape_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape}
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.innerBorderShape_ = null;
/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.createShapes = function() {
    this.circleShape_ = new anychart.plots.piePlot.data.styles.PieAquaFillShape();
    this.circleShape_.initialize(this.point_, this);

    this.outerBorderShape_ = new anychart.plots.piePlot.data.styles.PieAquaOuterBorderShape();
    this.outerBorderShape_.initialize(this.point_, this);

    this.innerBorderShape_ = new anychart.plots.piePlot.data.styles.PieAquaInnerBorderShape();
    this.innerBorderShape_.initialize(this.point_, this);
};
/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.placeShapes = function() {
    var sprite = this.getSprite();
    if (this.circleShape_) sprite.appendChild(this.circleShape_.getElement());
    if (this.outerBorderShape_) sprite.appendChild(this.outerBorderShape_.getElement());
    if (this.innerBorderShape_) sprite.appendChild(this.innerBorderShape_.getElement());
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.createElement = function() {
    return this.point_.getSVGManager().createPath();
};



/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calcBounds();

    if (this.circleShape_)
        this.circleShape_.update(opt_styleState, opt_updateData);

    if (this.outerBorderShape_)
        this.outerBorderShape_.update(opt_styleState, opt_updateData);

    if (this.innerBorderShape_)
        this.innerBorderShape_.update(opt_styleState, opt_updateData);
};

/**
 * ND: Needs doc!
 * @param {*} styleState
 */
anychart.plots.piePlot.data.styles.PieAquaStyleShapes.prototype.updateGradients = function(styleState) {
    if (this.circleShape_)
        this.circleShape_.updateGradients(styleState);
    if (this.outerBorderShape_)
        this.outerBorderShape_.updateGradients(styleState);
    if (this.innerBorderShape_)
        this.innerBorderShape_.updateGradients(styleState);
};


/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.aqua.CircleAquaStyleState}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaStyleState = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaStyleState,
    anychart.styles.aqua.CircleAquaStyleState);

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.piePlot.data.styles.PieAquaStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieAquaStyle,
    anychart.styles.background.BackgroundStyle);
anychart.plots.piePlot.data.styles.PieAquaStyle.prototype.initialize = function (point) {
    return goog.base(this, 'initialize', point);
};
/**@inheritDoc*/
anychart.plots.piePlot.data.styles.PieAquaStyle.prototype.createStyleShapes = function (point) {
    return new anychart.plots.piePlot.data.styles.PieAquaStyleShapes();
};
/** @inheritDoc */
anychart.plots.piePlot.data.styles.PieAquaStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.piePlot.data.styles.PieAquaStyleState;
};
//------------------------------------------------------------------------------
//
//                          PieStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.piePlot.data.styles.PieStyle = function () {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.PieStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                  StyleShapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.styles.PieStyle.prototype.createStyleShapes = function (point) {
    return new anychart.plots.piePlot.data.styles.PieStyleShapes();
};
//------------------------------------------------------------------------------
//                  Style state.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.styles.PieStyle.prototype.getStyleStateClass = function () {
    return anychart.styles.background.BackgroundStyleState;
};
//------------------------------------------------------------------------------
//
//                         Pie3DFrontSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DFrontSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DFrontSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DFrontSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getFrontSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DFrontSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DFrontSideShape.prototype.updatePath = function () {
    var path = this.point_.getFrontSidePath();
    if (path != null && path != ' Z' && path != '')
        this.element_.setAttribute('d', path);
    else this.element_.removeAttribute('d');
};

//------------------------------------------------------------------------------
//
//                         Pie3DFrontSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DBackSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DBackSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DBackSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getBackSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DBackSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DBackSideShape.prototype.updatePath = function () {
    if (this.point_.backSideDrawer_) {
        var path = this.point_.getBackSidePath();
        if (path != null && path != ' Z')
            this.element_.setAttribute('d', path);
        else this.element_.removeAttribute('d');
    }
};

//------------------------------------------------------------------------------
//
//                         Pie3DTopSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DTopSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DTopSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DTopSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getTopSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DTopSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DTopSideShape.prototype.updatePath = function () {
    var path = this.point_.getTopSidePath();
    if (path != null && path != ' Z' && path != '')
        this.element_.setAttribute('d', path);
    else this.element_.removeAttribute('d');
};

//------------------------------------------------------------------------------
//
//                         Pie3DSmileSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DSmileSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DSmileSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DSmileSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getSmileSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DSmileSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DSmileSideShape.prototype.updatePath = function () {
    if (this.point_.getFrontSideDrawer() != null) {
        var path = this.point_.getSmilePath();
        if (path != null && path != ' Z' && path != '')
            this.element_.setAttribute('d', path);
        else this.element_.removeAttribute('d');
    }
};

//------------------------------------------------------------------------------
//
//                         Pie3DStartSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DStartSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DStartSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStartSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getStartSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStartSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStartSideShape.prototype.updatePath = function () {
    var path = this.point_.getStartSidePath();
    if (path != null && path != ' Z' && path != '')
        this.element_.setAttribute('d', path);
    else this.element_.removeAttribute('d');
};

//------------------------------------------------------------------------------
//
//                         Pie3DEndSideShape class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.BaseFillShape}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DEndSideShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DEndSideShape,
    anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DEndSideShape.prototype.getFill = function (styleContainer) {
    return styleContainer.getEndSideFill();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DEndSideShape.prototype.needUpdateFill = function (styleContainer) {
    return true;
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DEndSideShape.prototype.updatePath = function () {
    var path = this.point_.getEndSidePath();
    if (path != null && path != ' Z' && path != '')
        this.element_.setAttribute('d', path);
    else this.element_.removeAttribute('d');
};

//------------------------------------------------------------------------------
//
//                         Pie3DStyleShapes class
//
//------------------------------------------------------------------------------

/**
 * Pie3D Style Shapes class.
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DStyleShapes,
    anychart.styles.StyleShapes);

//------------------------------------------------------------------------------
//                             Properties
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.frontSideShape_ = null;

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.backSideShape_ = null;

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.startSideShape_ = null;

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.endSideShape_ = null;

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.topSideShape_ = null;

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.styles.BaseFillShape}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.smileSideShape_ = null;


/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.createShapes = function () {
    this.frontSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DFrontSideShape();
    this.frontSideShape_.initialize(this.point_, this);

    this.backSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DBackSideShape();
    this.backSideShape_.initialize(this.point_, this);

    this.startSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DStartSideShape();
    this.startSideShape_.initialize(this.point_, this);

    this.endSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DEndSideShape();
    this.endSideShape_.initialize(this.point_, this);

    this.topSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DTopSideShape();
    this.topSideShape_.initialize(this.point_, this);

    this.smileSideShape_ = new anychart.plots.piePlot.data.styles.Pie3DSmileSideShape();
    this.smileSideShape_.initialize(this.point_, this);
    this.smileSideShape_.getElement().setAttribute('filter', 'url(#smileBlur)');
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.placeShapes = function () {
    var point = this.point_;

    if (point.topSideShape_) {
        var topSideShapeSprite = new anychart.svg.SVGSprite(point.getSVGManager());
        topSideShapeSprite.appendChild(this.topSideShape_.getElement());

        var smileSideShapeSprite = new anychart.svg.SVGSprite(point.getSVGManager());
        smileSideShapeSprite.appendChild(this.smileSideShape_.getElement());

        point.topSideShape_.appendChild(topSideShapeSprite.getElement());
        point.topSideShape_.appendChild(smileSideShapeSprite.getElement());
    }

    if (point.frontSideShape_) {
        point.frontSideShape_.appendChild(this.frontSideShape_.getElement());
    }

    if (point.backSideShape_) {
        point.backSideShape_.appendChild(this.backSideShape_.getElement());
    }

    if (point.startSideShape_) {
        point.startSideShape_.appendChild(this.startSideShape_.getElement());
    }

    if (point.endSideShape_) {
        point.endSideShape_.appendChild(this.endSideShape_.getElement());
    }

};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData) this.point_.calcBounds();

    this.topSideShape_.update(opt_styleState, opt_updateData);
    this.smileSideShape_.update(opt_styleState, opt_updateData);

    if (this.frontSideShape_)
        this.frontSideShape_.update(opt_styleState, opt_updateData);

    if (this.backSideShape_)
        this.backSideShape_.update(opt_styleState, opt_updateData);

    this.startSideShape_.update(opt_styleState, opt_updateData);
    this.endSideShape_.update(opt_styleState, opt_updateData);
};
/**
 * @inheritDoc
 */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.updateGradients = function(opt_styleState) {
    this.topSideShape_.updateGradients(opt_styleState);
    this.smileSideShape_.updateGradients(opt_styleState);

    if (this.frontSideShape_)
        this.frontSideShape_.updateGradients(opt_styleState);

    if (this.backSideShape_)
        this.backSideShape_.updateGradients(opt_styleState);

    this.startSideShape_.updateGradients(opt_styleState);
    this.endSideShape_.updateGradients(opt_styleState);
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.updatePath = function (element) {
};


/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};

//------------------------------------------------------------------------------
//
//                         Pie3DStyleState class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DStyleState,
    anychart.styles.background.BackgroundStyleState);

//------------------------------------------------------------------------------
//                    Properties
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.frontSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getFrontSideFill = function () {
    return this.frontSideFill_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.backSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getBackSideFill = function () {
    return this.backSideFill_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.startSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getStartSideFill = function () {
    return this.startSideFill_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.endSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getEndSideFill = function () {
    return this.endSideFill_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.topSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getTopSideFill = function () {
    return this.topSideFill_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.smileSideFill_ = null;
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.getSmileSideFill = function () {
    return this.smileSideFill_;
};

//------------------------------------------------------------------------------
//                    deserialization
//------------------------------------------------------------------------------

/**
 * Settings for top side fill.
 * @private
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.deserializeTopSideFill_ = function (color) {
    var keys = [];
    var key1 = {};
    var key2 = {};
    var gradientData = {};
    var fillData = {};

    key1['position'] = '0';
    key1['opacity'] = '1';
    key1['color'] = color;
    keys.push(key1);

    key2['position'] = 1;
    key2['opacity'] = '1';
    key2['color'] = 'Blend(' + color + ', DarkColor(' + color + '),0.7)';
    keys.push(key2);

    gradientData['type'] = 'linear';
    gradientData['angle'] = '-50';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.topSideFill_ = new anychart.visual.fill.Fill();
    this.topSideFill_.deserialize(fillData);
};

/**
 * Setting for front side fill.
 * @private
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.deserializeFrontSideFill_ = function (color) {
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};
    var gradientData = {};
    var fillData = {};

    keys = [];
    key1['position'] = '0';
    key1['opacity'] = 1;
    key1['color'] = color;
    keys.push(key1);

    key2['position'] = 0.19;
    key2['opacity'] = 1;
    key2['color'] = 'Blend(DarkColor(' + color + '),LightColor(' + color + '),0.3)';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = 1;
    key3['color'] = 'Blend(' + color + ',DarkColor(' + color + '),0.3)';
    keys.push(key3);

    gradientData['type'] = 'linear';
    gradientData['angle'] = '45';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.frontSideFill_ = new anychart.visual.fill.Fill();
    this.frontSideFill_.deserialize(fillData);
};

/**
 * Settings for smile side fill.
 * @private
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.deserializeSmileSideFill_ = function (color) {
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};
    var key4 = {};
    var gradientData = {};
    var fillData = {};

    key1['position'] = 0;
    key1['opacity'] = 0.29;
    key1['color'] = 'white';
    keys.push(key1);

    key2['position'] = 0.28;
    key2['opacity'] = 0.8;
    key2['color'] = 'white';
    keys.push(key2);

    key3['position'] = 0.72;
    key3['opacity'] = 0.16;
    key3['color'] = 'white';
    keys.push(key3);

    key4['position'] = 1;
    key4['opacity'] = '0';
    key4['color'] = 'white';
    keys.push(key4);

    gradientData['type'] = 'linear';
    gradientData['angle'] = '0';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.smileSideFill_ = new anychart.visual.fill.Fill();
    this.smileSideFill_.deserialize(fillData);
};

/**
 * Settings for sides fill.
 * @private
 */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.deserializeSidesFill_ = function (color) {
    var fillData = {};
    fillData['enabled'] = true;
    fillData['type'] = 'solid';
    fillData['color'] = 'Blend(' + color + ', DarkColor(' + color + '), 0.35)';

    this.backSideFill_ = new anychart.visual.fill.Fill();
    this.backSideFill_.deserialize(fillData);

    this.startSideFill_ = new anychart.visual.fill.Fill();
    this.startSideFill_.deserialize(fillData);

    this.endSideFill_ = new anychart.visual.fill.Fill();
    this.endSideFill_.deserialize(fillData);
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyleState.prototype.deserialize = function (config) {
    config = goog.base(this, 'deserialize', config);
    var des = anychart.utils.deserialization;
    var color = des.getProp(des.getProp(config, 'fill'), 'color');
    this.deserializeTopSideFill_(color);
    this.deserializeFrontSideFill_(color);
    this.deserializeSmileSideFill_(color);
    this.deserializeSidesFill_(color);
    return config;
};

//------------------------------------------------------------------------------
//
//                          Pie3DStyle.
//
//------------------------------------------------------------------------------

/**
 * 3D pie style.
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.piePlot.data.styles.Pie3DStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.piePlot.data.styles.Pie3DStyle,
    anychart.styles.background.BackgroundStyle);

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyle.prototype.initialize = function (point) {
    return goog.base(this, 'initialize', point);
};

//------------------------------------------------------------------------------
//                    StyleShapes
//------------------------------------------------------------------------------

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.piePlot.data.styles.Pie3DStyleShapes();
};

/** @inheritDoc */
anychart.plots.piePlot.data.styles.Pie3DStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.piePlot.data.styles.Pie3DStyleState;
};


