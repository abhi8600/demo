/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>{anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE}</li>
 * <ul>
 */

goog.provide('anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE');

/**
 * Contains default template for all series plots. 
 *
 * @public
 * @const
 * @type {Object.<*>} - JSON
 */
anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE = {
	"chart": {
		"palettes": {
			"hatch_palette": {
				"item": [
					{
						"type": "backwardDiagonal"
					},
					{
						"type": "forwardDiagonal"
					},
					{
						"type": "horizontal"
					},
					{
						"type": "vertical"
					},
					{
						"type": "dashedBackwardDiagonal"
					},
					{
						"type": "grid"
					},
					{
						"type": "dashedForwardDiagonal"
					},
					{
						"type": "dashedHorizontal"
					},
					{
						"type": "dashedVertical"
					},
					{
						"type": "diagonalCross"
					},
					{
						"type": "diagonalBrick"
					},
					{
						"type": "divot"
					},
					{
						"type": "horizontalBrick"
					},
					{
						"type": "verticalBrick"
					},
					{
						"type": "checkerboard"
					},
					{
						"type": "confetti"
					},
					{
						"type": "plaid"
					},
					{
						"type": "solidDiamond"
					},
					{
						"type": "zigzag"
					},
					{
						"type": "weave"
					},
					{
						"type": "percent05"
					},
					{
						"type": "percent10"
					},
					{
						"type": "percent20"
					},
					{
						"type": "percent25"
					},
					{
						"type": "percent30"
					},
					{
						"type": "percent40"
					},
					{
						"type": "percent50"
					},
					{
						"type": "percent60"
					},
					{
						"type": "percent70"
					},
					{
						"type": "percent75"
					},
					{
						"type": "percent80"
					},
					{
						"type": "percent90"
					}
				],
				"name": "default"
			},
			"marker_palette": {
				"item": [
					{
						"type": "Circle"
					},
					{
						"type": "Square"
					},
					{
						"type": "Diamond"
					},
					{
						"type": "Star4"
					},
					{
						"type": "Star5"
					},
					{
						"type": "Star7"
					},
					{
						"type": "TriangleUp"
					},
					{
						"type": "TriangleDown"
					},
					{
						"type": "Cross"
					},
					{
						"type": "DiagonalCross"
					},
					{
						"type": "HLine"
					},
					{
						"type": "VLine"
					},
					{
						"type": "Star6"
					},
					{
						"type": "Star10"
					}
				],
				"name": "default"
			},
			"palette": {
				"item": [
					{
						"color": "#1D8BD1"
					},
					{
						"color": "#F1683C"
					},
					{
						"color": "#2AD62A"
					},
					{
						"color": "#DBDC25"
					},
					{
						"color": "#8FBC8B"
					},
					{
						"color": "#D2B48C"
					},
					{
						"color": "#FAF0E6"
					},
					{
						"color": "#20B2AA"
					},
					{
						"color": "#B0C4DE"
					},
					{
						"color": "#DDA0DD"
					},
					{
						"color": "#9C9AFF"
					},
					{
						"color": "#9C3063"
					},
					{
						"color": "#FFFFCE"
					},
					{
						"color": "#CEFFFF"
					},
					{
						"color": "#630063"
					},
					{
						"color": "#FF8284"
					},
					{
						"color": "#0065CE"
					},
					{
						"color": "#CECFFF"
					},
					{
						"color": "#000084"
					},
					{
						"color": "#FF00FF"
					},
					{
						"color": "#FFFF00"
					},
					{
						"color": "#00FFFF"
					},
					{
						"color": "#840084"
					},
					{
						"color": "#840000"
					},
					{
						"color": "#008284"
					},
					{
						"color": "#0000FF"
					},
					{
						"color": "#00CFFF"
					},
					{
						"color": "#CEFFFF"
					},
					{
						"color": "#CEFFCE"
					},
					{
						"color": "#FFFF9C"
					},
					{
						"color": "#9CCFFF"
					},
					{
						"color": "#FF9ACE"
					},
					{
						"color": "#CE9AFF"
					},
					{
						"color": "#FFCF9C"
					},
					{
						"color": "#3165FF"
					},
					{
						"color": "#31CFCE"
					},
					{
						"color": "#9CCF00"
					},
					{
						"color": "#FFCF00"
					},
					{
						"color": "#FF9A00"
					},
					{
						"color": "#FF6500"
					}
				],
				"name": "default"
			}
		},
		"styles": {
			"animation_style": [
				{
					"name": "anychart_default",
					"enabled": "true",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultLabel",
					"enabled": "True",
					"start_time": "1.2",
					"duration": "0.5",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultMarker",
					"enabled": "True",
					"start_time": "1.2",
					"duration": "0.5",
					"type": "Appear",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultScaleYBottom",
					"enabled": "True",
					"start_time": "0.2",
					"type": "ScaleYBottom",
					"duration": "1",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultSideYBottom",
					"enabled": "True",
					"start_time": "0.2",
					"type": "SideFromBottom",
					"duration": "1",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultScaleYCenter",
					"enabled": "True",
					"start_time": "0.2",
					"type": "ScaleYCenter",
					"duration": "1",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultScaleXYCenter",
					"enabled": "True",
					"start_time": "0.2",
					"type": "ScaleXYCenter",
					"duration": "1",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultPie",
					"enabled": "True",
					"start_time": "0.2",
					"type": "OutSide",
					"duration": "2",
					"show_mode": "Smoothed",
					"smoothed_delay": "0.5",
					"interpolation_type": "Cubic"
				},
				{
					"name": "defaultChart",
					"enabled": "False",
					"type": "Appear",
					"interpolation_type": "Cubic",
					"start_time": "0",
					"duration": "0.5"
				}
			],
			"marker_style": {
				"marker": {
					"type": "%MarkerType",
					"size": "6",
					"anchor": "top"
				},
				"fill": {
					"color": "LightColor(%Color)"
				},
				"border": {
					"color": "DarkColor(%Color)",
					"thickness": "1"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "false",
						"distance": "1.5",
						"opacity": "0.15"
					}
				},
				"states": {
					"hover": {
						"marker": {
							"size": "10"
						},
						"fill": {
							"color": "LightColor(%Color)"
						},
						"effects": {
							"glow": {
								"enabled": "true",
								"blur_x": "2",
								"blur_y": "2",
								"opacity": "0.8"
							}
						}
					},
					"missing": {
						"fill": {
							"type": "Solid",
							"color": "White",
							"opacity": "0"
						},
						"border": {
							"thickness": "1",
							"color": "White",
							"opacity": "1"
						}
					}
				},
				"name": "anychart_default"
			},
			"label_style": [
				{
					"format": {
						"value": "{%Value}"
					},
					"position": {
						"anchor": "Top",
						"halign": "Center",
						"valign": "Top",
						"padding": "5"
					},
					"font": {
						"family": "Verdana",
						"size": "10",
						"color": "rgb(35,35,35)",
						"bold": "True"
					},
					"background": {
						"border": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(221,221,221)"
									},
									{
										"position": "1",
										"color": "rgb(208,208,208)"
									}
								],
								"angle": "90"
							},
							"enabled": "True",
							"type": "Gradient"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(255,255,255)"
									},
									{
										"position": "0.5",
										"color": "rgb(243,243,243)"
									},
									{
										"position": "1",
										"color": "rgb(255,255,255)"
									}
								],
								"angle": "90"
							},
							"type": "Gradient"
						},
						"inside_margin": {
							"all": "3"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "True",
								"distance": "1",
								"opacity": "0.1"
							},
							"enabled": "True"
						},
						"enabled": "false"
					},
					"name": "anychart_default"
				},
				{
					"format": {
						"value": "{%Value}"
					},
					"position": {
						"anchor": "Top",
						"halign": "Center",
						"valign": "Top",
						"padding": "5"
					},
					"font": {
						"family": "Verdana",
						"size": "10",
						"color": "DarkColor(%Color)",
						"bold": "True"
					},
					"background": {
						"border": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "DarkColor(%Color)"
									},
									{
										"position": "1",
										"color": "DarkColor(%Color)"
									}
								],
								"angle": "90"
							},
							"enabled": "True",
							"type": "Gradient"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(255,255,255)"
									},
									{
										"position": "0.5",
										"color": "rgb(243,243,243)"
									},
									{
										"position": "1",
										"color": "rgb(255,255,255)"
									}
								],
								"angle": "90"
							},
							"enabled": "true",
							"type": "Gradient"
						},
						"inside_margin": {
							"all": "3"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "True",
								"distance": "1",
								"opacity": "0.1"
							},
							"enabled": "True"
						},
						"enabled": "true"
					},
					"name": "Default01"
				},
				{
					"font": {
						"bold": "false"
					},
					"name": "Default02",
					"parent": "Default01"
				},
				{
					"font": {
						"color": "rgb(35,35,35)"
					},
					"name": "Default03",
					"parent": "Default01"
				},
				{
					"font": {
						"color": "rgb(35,35,35)",
						"bold": "false"
					},
					"name": "Default04",
					"parent": "Default01"
				},
				{
					"font": {
						"color": "White",
						"bold": "false"
					},
					"background": {
						"border": {
							"enabled": "false"
						},
						"fill": {
							"enabled": "true",
							"type": "Solid",
							"color": "DarkColor(%Color)",
							"opacity": "0.7"
						},
						"effects": {
							"enabled": "false"
						},
						"inside_margin": {
							"top": "1",
							"bottom": "2"
						},
						"corners": {
							"type": "Rounded",
							"all": "2"
						},
						"enabled": "true"
					},
					"name": "Default05"
				}
			],
			"tooltip_style": [
				{
					"format": {
						"value": "{%Value}{numDecimals:2}"
					},
					"position": {
						"anchor": "Float",
						"halign": "Center",
						"valign": "Top",
						"padding": "5"
					},
					"font": {
						"family": "Verdana",
						"size": "10",
						"color": "rgb(35,35,35)",
						"bold": "True",
						"italic": "False",
						"underline": "False"
					},
					"background": {
						"border": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(221,221,221)",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "rgb(208,208,208)",
										"opacity": "1"
									}
								],
								"angle": "90",
								"type": "Linear",
								"focal_point": "0"
							},
							"enabled": "True",
							"thickness": "1",
							"type": "Gradient",
							"opacity": "1",
							"color": "rgb(0,0,0)",
							"dashed": "False",
							"dash_length": "5",
							"space_length": "3",
							"caps": "Round",
							"joints": "Round"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(255,255,255)",
										"opacity": "1"
									},
									{
										"position": "0.5",
										"color": "rgb(243,243,243)",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "rgb(255,255,255)",
										"opacity": "1"
									}
								],
								"angle": "90",
								"type": "Linear",
								"focal_point": "0"
							},
							"enabled": "True",
							"type": "Gradient",
							"color": "rgb(255,255,255)",
							"opacity": "1",
							"image_mode": "Stretch",
							"image_url": ""
						},
						"hatch_fill": {
							"enabled": "False",
							"type": "BackwardDiagonal",
							"color": "rgb(0,0,0)",
							"opacity": "1",
							"thickness": "1",
							"pattern_size": "10"
						},
						"inside_margin": {
							"left": "10",
							"top": "5",
							"right": "10",
							"bottom": "5"
						},
						"corners": {
							"type": "Square",
							"all": "10"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "True",
								"opacity": "0.3",
								"angle": "45",
								"color": "Black",
								"distance": "2",
								"blur_x": "2",
								"blur_y": "2"
							},
							"enabled": "True"
						},
						"enabled": "True"
					},
					"name": "anychart_default",
					"enabled": "true"
				},
				{
					"background": {
						"enabled": "false"
					},
					"name": "Default01",
					"enabled": "true"
				},
				{
					"position": {
						"halign": "Center",
						"valign": "Top",
						"padding": "5"
					},
					"font": {
						"color": "#575757"
					},
					"background": {
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "#FFFFFF"
									},
									{
										"position": "0.5",
										"color": "#E4E5F0"
									}
								],
								"angle": "90"
							},
							"type": "gradient"
						},
						"inside_margin": {
							"left": "4",
							"right": "4",
							"top": "3",
							"bottom": "3"
						},
						"border": {
							"color": "#767676"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "true",
								"distance": "2"
							}
						}
					},
					"name": "Default02",
					"enabled": "true"
				}
			]
		},
		"data_plot_settings": {
		},
		"chart_settings": {
			"data_plot_background": {
				"border": {
					"color": "#B7B7B7"
				},
				"fill": {
					"color": "#FFFFFF"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "True",
						"distance": "2",
						"opacity": "0.2"
					}
				}
			}
		}
	},
	"defaults": {
		"color_swatch": {
			"labels": {
				"format": {
					"value": "{%Value}{numDecimals:2}"
				},
				"font": {
					"family": "Verdana",
					"size": "10",
					"bold": "false",
					"color": "#232323"
				},
				"padding": "0"
			},
			"background": {
				"border": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "rgb(221,221,221)"
							},
							{
								"position": "1",
								"color": "rgb(208,208,208)"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"fill": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "#FFFFFF"
							},
							{
								"position": "0.5",
								"color": "rgb(243,243,243)"
							},
							{
								"position": "1",
								"color": "#FFFFFF"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"inside_margin": {
					"all": "5"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "True",
						"distance": "1",
						"opacity": "0.1"
					},
					"enabled": "True"
				},
				"enabled": "true"
			},
			"tickmark": {
				"thickness": "1",
				"size": "4",
				"color": "#232323"
			},
			"range_item": {
				"background": {
					"fill": {
						"color": "%Color"
					},
					"states": {
						"hover": {
							"fill": {
								"color": "LightColor(%Color)"
							}
						}
					}
				}
			},
			"title": {
				"text": {
					"value": "Color Swatch"
				},
				"enabled": "false"
			},
			"title_separator": {
				"gradient": {
					"key": [
						{
							"position": "0",
							"color": "#333333",
							"opacity": "0"
						},
						{
							"position": "0.5",
							"color": "#333333",
							"opacity": "1"
						},
						{
							"position": "1",
							"color": "#333333",
							"opacity": "0"
						}
					]
				},
				"enabled": "true",
				"type": "Gradient"
			},
			"orientation": "Horizontal",
			"position": "bottom",
			"align": "near",
			"inside_dataplot": "false",
			"align_by": "dataplot",
			"padding": "5"
		}
	}
};