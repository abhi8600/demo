//---CONFIGURATION
var PATH_TO_JS_FOLDER = '../../../../out/trial/';
var PATH_TO_SWF_FILE = '../../../../out/trial/AnyChart.swf';
var SLEEP_TIMER = 1000;
//-------------------------
var IS_SVG_COMPONENT = true;
var chart = {};
//----Page Manipulations
addScript(PATH_TO_JS_FOLDER+'AnyChart.js')
addScript(PATH_TO_JS_FOLDER+'AnyChartHTML5.js');
var s = addStyleSheet();
addStyle(s, 'div.descr', 'background-color:#FFF8DC; width: 700px; left: 40px; top: 600px; position:absolute; margins:0px; padding: 10px;');
addStyle(s, 'div.container', 'width: 800px; height:600px; left: 0px; top:0px; position:absolute; margins: 0px; padding: 0px');
//-------------------------

//--------------**************--------------
function addScript(src){
	var Script = document.createElement('script');
	Script.src = src;
	Script.type = "text/javascript";
	document.getElementsByTagName('head')[0].appendChild(Script);
}
function addStyleSheet() {
	var style = document.createElement('style');
	style.type = 'text/css';
	document.getElementsByTagName('head')[0].appendChild(style);
	return document.styleSheets[document.styleSheets.length - 1];
}
function addStyle(ss, sel, rule) {
	if (ss.addRule) { ss.addRule(sel, rule); }
	else { if (ss.insertRule) { ss.insertRule(sel + ' {' + rule + '}', ss.cssRules.length); } }
}
function getChartInstance(xmlFile){
	if (IS_SVG_COMPONENT){
		AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
		chart = new AnyChart();
	}else{
		AnyChart.renderingType = anychart.RenderingType.FLASH_ONLY;
		chart = new AnyChart(PATH_TO_SWF_FILE);
	}	
	chart.width='100%';
	chart.height='100%';
	chart.addEventListener("error", function(e) {console.log("chart error:" + e.errorCode);});
	if (xmlFile) chart.xmlFile=xmlFile;
	return chart;
}
function chartError(){
}
//--------------**************--------------
