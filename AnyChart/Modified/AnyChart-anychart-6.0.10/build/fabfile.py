from __future__ import with_statement
from fabric.api import local, settings, abort, run, cd, env
from fabric.operations import get

env.hosts = ['root@dev.anychart.com']
env.no_keys = True

def server():
	branch_name = local('git branch | grep "*"', True)[2:]
	print("Branch name: '"+branch_name+"'")
	project_dir = "/data/anychart-building/anychart"
	with cd(project_dir):
		run("git checkout -- *")
		run("git pull")
		run("git checkout %s" % branch_name)
		run("rm -rf out/*")
		run("python build/build.py")
		get("out", "../out")

def tests():
	branch_name = local('git branch | grep "*"', True)[2:]
	print("Branch name: '"+branch_name+"'")
	project_dir = "/data/anychart-building/anychart"
	with cd(project_dir):
		run("git checkout -- *")
		run("git pull")
		run("git checkout %s" % branch_name)
		run("rm -rf out/*")
		run("python build/build.py -b jstests-develop --buildKey AC-JSTESTSDEVELOP-JOB1 --buildNumber 204")
		get("out", "../out")
