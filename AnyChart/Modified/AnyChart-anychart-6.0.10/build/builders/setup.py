import build_utils
import os
import shutil

BASE_DIR = build_utils.get_root()
BIN_DIR = os.path.join(BASE_DIR, "build")
SRC_DIR = os.path.join(BASE_DIR, "install")

def main():
    __check_inno_setup()
    __update_version()
    __check_structure()
    __build_install("bundle")
    __build_install("charts")
    __build_install("maps")
    __build_install("trial")

def __check_structure():
    src = os.path.join(SRC_DIR, "src")

    if not os.path.exists(os.path.join(src, "binaries")):
        os.mkdir(os.path.join(src, "binaries"))

    if not os.path.exists(os.path.join(src, "binaries", "swf")):
        os.mkdir(os.path.join(src, "binaries", "swf"))
    if not os.path.exists(os.path.join(src, "binaries", "flex")):
        os.mkdir(os.path.join(src, "binaries", "flex"))
    if not os.path.exists(os.path.join(src, "binaries", "js")):
        os.mkdir(os.path.join(src, "binaries", "js"))

    if not os.path.exists(os.path.join(src, "basic-sample", "swf")):
        os.mkdir(os.path.join(src, "basic-sample", "swf"))
    if not os.path.exists(os.path.join(src, "basic-sample", "js")):
        os.mkdir(os.path.join(src, "basic-sample", "js"))

def __build_install(folder):
    print "# building %s install..." % folder
    binaries = os.path.join(BASE_DIR, "out", folder)
    preloader = os.path.join(BASE_DIR, "out", "Preloader.swf")
    src = os.path.join(SRC_DIR, "src")
    swf_dir = os.path.join(src, "binaries", "swf")
    swc_dir = os.path.join(src, "binaries", "flex")
    js_dir = os.path.join(src, "binaries", "js")

    shutil.copy(preloader, swf_dir)
    shutil.copy(os.path.join(binaries,"AnyChart.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"Chart.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"Funnel.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"Gauge.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"HeatMap.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"Map.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"Pie.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"TreeMap.swf"), swf_dir)
    shutil.copy(os.path.join(binaries,"RadarPolar.swf"), swf_dir)

    shutil.copy(os.path.join(binaries,"AnyChartFlexComponent.swc"), swc_dir)

    shutil.copy(os.path.join(binaries,"AnyChart.js"), js_dir)
    if os.path.exists(os.path.join(binaries, "AnyChartHTML5.js")):
        shutil.copy(os.path.join(binaries,"AnyChartHTML5.js"), js_dir)

    sample_dir = os.path.join(src, "basic-sample")

    shutil.copy(os.path.join(binaries, "AnyChart.swf"), os.path.join(sample_dir, "swf"))
    shutil.copy(preloader, os.path.join(sample_dir, "swf"))
    shutil.copy(os.path.join(binaries, "AnyChart.js"), os.path.join(sample_dir, "js"))
    if os.path.exists(os.path.join(binaries, "AnyChartHTML5.js")):
        shutil.copy(os.path.join(binaries, "AnyChartHTML5.js"), os.path.join(sample_dir, "js"))

    build_utils.zipdir(src, os.path.join(binaries,"AnyChart_plain.zip"), ("anychart.bmp", ".svn", "logo.bmp", ".DS_Store", "Thumbs.db", "anychart.url",\
                                                                          "homepage.url", "purchase.url", "sales.url", "support.url"))

    __run_inno_setup(os.path.join(binaries,"AnyChart.exe"))

def __run_inno_setup(output):
    pass
        """subprocess.call(["wine %s/bin/utils/inno-setup/ISCC.exe",\
                         "%s/install/setup-script/install.iss"])"""

def __check_inno_setup():
    print "## checking inno setup..."
    innoSetupPath = os.path.join(BIN_DIR, "utils", "inno-setup")

    if not os.path.exists(innoSetupPath):
        print "# extracting inno setup..."
        build_utils.extract_zip(os.path.join(BIN_DIR, "utils", "inno-setup.zip"), os.path.join(BIN_DIR, "utils"))
    else:
        print "# inno setup exists"

def __update_version():
    print "# updating version to %s" % build_utils.get_version_string()
    v = build_utils.get_version()

    script = os.path.join(SRC_DIR, "setup-script", "install.iss")

    version_dot = "%s.%s.%s" % (v['global'], v['major'], v['minor'])
    version_dash = "%s-%s-%s" % (v['global'], v['major'], v['minor'])

    build_utils.replace_in_file(script, "AnyChart [0-9]+\.[0-9]+\.[0-9]+","AnyChart %s" % version_dot)
    build_utils.replace_in_file(script, "Component [0-9]+\.[0-9]+\.[0-9]+","Component %s" % version_dot)
    build_utils.replace_in_file(script, "=[0-9]+\.[0-9]+\.[0-9]+","=%s" % version_dot)
    build_utils.replace_in_file(script, "AnyChart-[0-9]+-[0-9]+-[0-9]+","AnyChart-%s" % version_dash)

if __name__ == "__main__":
    main()
