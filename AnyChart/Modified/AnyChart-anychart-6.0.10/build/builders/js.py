# coding=utf8
import os
import sys
import re
import build_utils
import copy
import shutil
import ConfigParser

#root paths
BASE_PATH = build_utils.get_root()
SRC_PATH = os.path.join(BASE_PATH, 'js-engine', 'src')
OUT_PATH = os.path.join(BASE_PATH, 'out')
BUILD_PATH = os.path.join(BASE_PATH, 'build')

#build paths
BUILD_UTILS_PATH = os.path.join(BUILD_PATH, 'utils')
BUILDERS_PATH = os.path.join(BUILD_PATH, 'builders')

#deploy paths
DEPLOY_DEBUG_PATH = os.path.join(BUILD_UTILS_PATH, 'deploy_debug')
DEPLOY_DEBUG_CONFIG_PATH = os.path.join(DEPLOY_DEBUG_PATH, 'deploy_debug.cfg')

#compiler paths
CLOSURE_PATH = os.path.join(BUILD_UTILS_PATH, 'google_closure')
CLOSURE_COMPILER_PATH = os.path.join(CLOSURE_PATH, 'compiler.jar')

PROP_MAP_OUT = os.path.join(OUT_PATH, 'prop_out')
VAR_MAP_OUT = os.path.join(OUT_PATH, 'var_out')
PROP_MAP_IN = os.path.join(OUT_PATH, 'prop_in')
VAR_MAP_IN = os.path.join(OUT_PATH, 'var_in')

#import closure ??????? ???? ???, ?? ?????? ??????? ?????????? ??????
sys.path.append(CLOSURE_PATH)
import calcdeps

#source paths
ANYCHART_SOURCE = os.path.join(SRC_PATH, 'anychart')
GOOGLE_SOURCE = os.path.join(SRC_PATH, 'goog')
SOURCES = [ANYCHART_SOURCE, GOOGLE_SOURCE]

ANYCHART_MODULE_NAME = 'AnyChart.js'
HTML5_MODULE_NAME = 'AnyChartHTML5.js'

#modules
ANYCHART = os.path.join(SRC_PATH, 'anychart', ANYCHART_MODULE_NAME)
ANYCHART_HTML5 = os.path.join(SRC_PATH, 'anychart', 'render', 'svg', 'svg.js')
INPUTS = [ANYCHART, ANYCHART_HTML5]

#compile level
ADVANCED_OPTIMIZATIONS, SIMPLE_OPTIMIZATIONS, WHITESPACE_ONLY = (0, 1, 2)

#build types
DEBUG, TRIAL, BUNDLE, EVALUATION_VERSION = (0, 1, 2, 3)

PRETTY_PRINT = 1

#default compiler flags
DEFAULT_COMPILER_FLAGS = [
    "--module='AnyChart:36'",
    "--module='AnyChartHTML5:169:AnyChart'",
    "--generate_exports",
    "--module_wrapper='AnyChart:(function(){%s})()'",
    "--module_wrapper='AnyChartHTML5:(function(){%s})()'",
    "--property_map_output_file='%s'" % PROP_MAP_OUT,
    "--variable_map_output_file='%s'" % VAR_MAP_OUT,
    "--define='AnyChart.VERSION=\'%s\''" % build_utils.get_version_string()
]

def build():
    #Build js engine of AnyChart component (deps, debug, bundle, trial, evaluation version)
    build_deps()
    build_debug()
    build_bundle()
    #build_trial()
    #build_evaluation()
    #build_maps()
    build_charts()


def build_deps(path_to_deps_file=GOOGLE_SOURCE, path_to_source=SOURCES):
    print 'Calculating dependencies'
    out = open(os.path.join(path_to_deps_file, 'deps.js'), 'w')
    search_paths = calcdeps.ExpandDirectories(path_to_source)
    print 'Write in ' + out.name
    calcdeps.PrintDeps(search_paths, [], out)
    print 'Dependencies calculated \n'


def build_debug():
    #Build js engine debug version
    __build_project('Build debug')


def build_bundle():
    #Build js engine bundle version
    __build_project('Build bundle', BUNDLE)


def build_trial():
    #Build js engine trial version
    __build_project('Build trial', TRIAL)


def build_evaluation():
    #Build js engine evaluation version version
    __build_project('Build evaluation version', EVALUATION_VERSION)


def build_maps():
    print 'Copy trial/AnyChart.js to maps/AnyChart.js'
    if not os.path.exists(os.path.join(OUT_PATH, 'maps')):
        os.mkdir(os.path.join(OUT_PATH, 'maps'))
    shutil.copy(os.path.join(OUT_PATH, 'trial', 'AnyChart.js'), os.path.join(OUT_PATH, 'maps', 'AnyChart.js'))
    shutil.copy(os.path.join(OUT_PATH, 'trial', 'AnyChartHTML5.js'), os.path.join(OUT_PATH, 'maps', 'AnyChartHTML5.js'))


def build_charts():
    print 'Copy bundle/AnyChart.js and AnyChartHTML5.js to charts/'
    if not os.path.exists(os.path.join(OUT_PATH, 'charts')):
        os.mkdir(os.path.join(OUT_PATH, 'charts'))
    shutil.copy(os.path.join(OUT_PATH, 'bundle', 'AnyChart.js'), os.path.join(OUT_PATH, 'charts', 'AnyChart.js'))
    shutil.copy(os.path.join(OUT_PATH, 'bundle', 'AnyChartHTML5.js'),
        os.path.join(OUT_PATH, 'charts', 'AnyChartHTML5.js'))


def __build_project(buildMsg, buildType=DEBUG, compileLevel=ADVANCED_OPTIMIZATIONS, isPrettyPrint=PRETTY_PRINT):
    # Build js engine of AnyChart component
    if calcdeps.distutils and not (
        calcdeps.distutils.version.LooseVersion(calcdeps.GetJavaVersion()) > calcdeps.distutils.version.LooseVersion(
            '1.6')):
        print 'Closure Compiler requires Java 1.6 or higher.'
        print 'Please visit http://www.java.com/getjava'
        sys.exit(1)

    flags = copy.copy(DEFAULT_COMPILER_FLAGS)
    output_path = __set_output_path(flags, buildType)

    __set_watermark_text(flags, buildType)
    __set_debug(flags, buildType == DEBUG)
    __set_trial(flags, buildType == TRIAL)
    __set_compile_level_flag(flags, compileLevel)

    __set_pretty_print(flags, isPrettyPrint)

    print buildMsg + ' with the following flags:'

    for flag in flags:
        print flag

    search_paths = calcdeps.ExpandDirectories(SOURCES)
    deps = calcdeps.CalculateDependencies(search_paths, INPUTS)
    calcdeps.Compile(CLOSURE_COMPILER_PATH, deps, sys.stdout, flags)

    __copy_anonymous_functions(output_path)

    print 'Build success \n'

def __set_compile_level_flag(flags, level):
    #set compiler level
    if level == WHITESPACE_ONLY:
        levelValue = 'WHITESPACE_ONLY'
    elif level == SIMPLE_OPTIMIZATIONS:
        levelValue = 'SIMPLE_OPTIMIZATIONS'
    else: levelValue = 'ADVANCED_OPTIMIZATIONS'

    __add_compiler_flag(flags, 'compilation_level', levelValue)

#add to compiler flags output path for modules
def __set_output_path(flags, buildType):
    #set output path for engine modules
    if buildType == TRIAL:
        path = os.path.join(OUT_PATH, 'trial', '')
    elif buildType == BUNDLE:
        path = os.path.join(OUT_PATH, 'bundle', '')
    elif buildType == EVALUATION_VERSION:
        path = os.path.join(OUT_PATH, 'custom', 'Evaluation version', '')
    else: path = os.path.join(OUT_PATH, 'debug', '')

    __add_compiler_flag(flags, 'module_output_path_prefix', path)

    return path

#add to compiler flags encrypted watermark text
def __set_watermark_text(flags, buildType):
    #set watermark text for build
    if buildType == EVALUATION_VERSION:
        __add_define(flags, "WATERMARK_TEXT=" + "\'" + __encrypt_watermark_text('Evaluation Version') + "\'")
    else: __add_define(flags, "WATERMARK_TEXT=" + "\'" + __encrypt_watermark_text('AnyChart Trial Version') + "\'")


def __set_debug(flags, isDebug):
    # set build debug
    debug = __get_bool_string_value(isDebug)

    __add_define(flags, "IS_ANYCHART_DEBUG_MOD=" + debug)
    __add_define(flags, "goog.DEBUG=" + debug)


def __set_trial(flags, isTrial):
    #set built trial
    trial = __get_bool_string_value(isTrial)

    __add_define(flags, "IS_ANYCHART_TRIAL_VERSION=" + trial)


def __set_pretty_print(flags, isPrettyPrint):
    # set pretty print
    if isPrettyPrint:
        __add_compiler_flag(flags, 'formatting', 'PRETTY_PRINT')


def __add_define(flags, define):
    __add_compiler_flag(flags, 'define', define)


def __add_compiler_flag(flags, flagName, flagValue):
    flags.append("--" + flagName + "='" + flagValue + "'")


def __get_bool_string_value(value):
    if value: return 'true'
    else: return 'false'


def __encrypt_watermark_text(text):
    res = ''
    i = 0
    charCount = len(text)
    for char in text:
        res += str(ord(char))
        i += 1
        if i != charCount: res += ','
    return res

########################################################################################################################
#
#                            Deploy to debug server.
#
########################################################################################################################
def deploy_to_debug_server_use_cfg(rebuild_js=False, rebuild_flash=False):
    """
    1. ????? ????????? ??? ?????? ? ????? deploy_debug.cfg.
    2. ??????? ?????????? anychart-debug ? ?????? ??????????
    3. ???????? ? anychart-debug/src ????????? ??????? (??? ??????? not compiled version)
    4. ???????? ? anychart-debug/bin ????????? ?? ./out/dubug (???? ??? ?? ???????? ??)
    5. ???????? ? anychart-debug ????? ??? ??????? compoled ? not compiled ?????? index.html ? index-compiled.html

    ???? ??????? ?????? ???? ??????? ? Unix shell-like mini-languages ? ????????? ?????? deploy ? ?????? path.

    ??????:
    [deploy]
    path: path_to_deploy_directory
    """
    #read config file
    if not os.path.exists(DEPLOY_DEBUG_CONFIG_PATH) or not os.path.isfile(DEPLOY_DEBUG_CONFIG_PATH):
        msg_error = 'Required %s file, see docs in source code for more information' % DEPLOY_DEBUG_CONFIG_PATH
        raise Exception(msg_error)

    config_file = open(DEPLOY_DEBUG_CONFIG_PATH)
    config = ConfigParser.ConfigParser()
    config.readfp(config_file)
    config_file.close()

    #check for required section and options
    try:
        path_to_deploy_dir = config.get('deploy', 'path')
    except ConfigParser.NoSectionError:
        error_msg = 'Required deploy section in %s, see docs in source code for more information' % DEPLOY_DEBUG_CONFIG_PATH
        raise Exception(error_msg)

    except ConfigParser.NoOptionError:
        error_msg = 'Required path option for deploy section in %s, see docs in source code for more information' % DEPLOY_DEBUG_CONFIG_PATH
        raise Exception(error_msg)


    #execute deploy
    print 'Start deploy to %s' % path_to_deploy_dir
    deploy_to_debug_server(path_to_deploy_dir, rebuild_js, rebuild_flash)


def deploy_to_debug_server(path_to_deploy_dir, rebuild_js=False, rebuild_flash=False):
    """
    1. ??????? ?????????? anychart-debug ? ?????? ?????????? path_to_deploy_dir
    2. ???????? ? anychart-debug/src ????????? ??????? (??? ??????? not compiled version)
    3. ???????? ? anychart-debug/bin ????????? ?? ./out/dubug (???? ??? ?? ???????? ??)
    4. ???????? ? anychart-debug ????? ??? ??????? compoled ? not compiled ?????? index.html ? index-compiled.html

    ???? ??????? ?????? ???? ??????? ? Unix shell-like mini-languages ? ????????? ?????? deploy ? ?????? path.

    ??????:
    [deploy]
    path: path_to_deploy_directory
    """

    #expand on ~ for unix-like systems (if exists)
    path_to_deploy_dir = os.path.expanduser(path_to_deploy_dir)
    #expand system environment variables (if exists)
    path_to_deploy_dir = os.path.expandvars(path_to_deploy_dir)

    #create anychart-debug directory and sub-directories
    if not os.path.exists(path_to_deploy_dir) or not os.path.isdir(path_to_deploy_dir):
        msg = 'Specified directory %s not exists' % path_to_deploy_dir
        raise Exception(msg)

    debug_path = __create_directory(path_to_deploy_dir, 'anychart-debug')
    bin_path = __create_directory(debug_path, 'bin')
    src_path = os.path.join(debug_path, 'src')

    #anychart source
    print 'Deploy source files'
    if os.path.exists(src_path): shutil.rmtree(src_path)
    shutil.copytree(SRC_PATH, src_path)
    build_deps(os.path.join(src_path, 'goog'), [os.path.join(src_path, 'goog'), os.path.join(src_path, 'anychart')])

    #anychart binaries
    print 'Deploy binaries files'
    binaries_path = os.path.join(OUT_PATH, 'debug')
    anychart_path = os.path.join(binaries_path, 'AnyChart.js')
    anychart_html5_path = os.path.join(binaries_path, 'AnyChartHTML5.js')
    anychart_flash = os.path.join(OUT_PATH, 'trial', 'AnyChart.swf')
    #build js if not exists or specified
    if not os.path.exists(anychart_path) or not os.path.exists(anychart_html5_path) or rebuild_js:
        print 'Deploy js binaries'
        build_debug()
    #exec copy
    shutil.copy(anychart_path, os.path.join(bin_path, 'AnyChart.js'))
    shutil.copy(anychart_html5_path, os.path.join(bin_path, 'AnyChartHTML5.js'))
    #build flash if not exists or specified
    if not os.path.exists(anychart_flash) or rebuild_flash:
        print 'Deploy flash binaries'
        import flash

        flash.build_trial()
        #exec copy
    shutil.copy(anychart_flash, os.path.join(bin_path, 'AnyChart.swf'))

    #run files
    print 'Deploy run files'
    shutil.copy(os.path.join(DEPLOY_DEBUG_PATH, 'index.html'), os.path.join(debug_path, 'index.html'))
    shutil.copy(os.path.join(DEPLOY_DEBUG_PATH, 'index-optimized.html'),
        os.path.join(debug_path, 'index-optimized.html'))
    config_path = os.path.join(SRC_PATH, '..', 'debug', 'chart.xml')
    if os.path.exists(config_path):
        shutil.copy(config_path, os.path.join(debug_path, 'chart.xml'))

    print 'Deploy successful complete'


def __create_directory(path, name):
    dir_path = os.path.join(path, name)
    if not os.path.exists(dir_path):
        try:
            os.mkdir(dir_path)
        except Exception as e:
            raise Exception("Can't create %s directory in %s, error:%s") % (name, path, str(e))
    return dir_path


def __copy_anonymous_functions(path):
    #get string to replace
    anychart = open(os.path.join(path, ANYCHART_MODULE_NAME), 'r').read()
    r = 'var[ \\n\\r]*[\w+][ \\n\\r]*,[ \\n\\r]*[\w+][ \\n\\r]*=[ \\n\\r]*[\w+][ \\n\\r]*\|\|[ \\n\\r]*\{\};'
    p = re.compile(r)

    result_str = re.split(p, anychart, 1)[0]

    #replace in file
    html5_file = open(os.path.join(path, HTML5_MODULE_NAME), 'r+')
    html5_text = html5_file.read()
    html5_text = html5_text.lstrip('(function(){')
    html5_text = result_str + html5_text
    html5_file.seek(0)
    html5_file.write(html5_text)
    html5_file.close()

########################################################################################################################
#
#                            Main.
#
########################################################################################################################
def print_help():
    print 'Possible arguments:'
    print 'trial, deps, debug, deploy-debug, deploy-debug flash, deploy-debug js, deploy-debug all, bundle, eval, maps, charts'

#build all by default
if __name__ == "__main__":
    args_count = len(sys.argv)
    if args_count == 1: build()
    elif args_count == 2:
        if sys.argv[1] == 'all': build()
        elif sys.argv[1] == 'trial': build_trial()
        elif sys.argv[1] == 'deps': build_deps()
        elif sys.argv[1] == 'debug': build_debug()
        elif sys.argv[1] == 'deploy-debug': deploy_to_debug_server_use_cfg()
        elif sys.argv[1] == 'bundle': build_bundle()
        elif sys.argv[1] == 'eval': build_evaluation()
        elif sys.argv[1] == 'maps': build_maps()
        elif sys.argv[1] == 'charts': build_charts()
    elif args_count == 3:
        if sys.argv[1] == 'deploy-debug':
            if sys.argv[2] == 'flash':
                deploy_to_debug_server_use_cfg(rebuild_flash=True)
            elif sys.argv[2] == 'js':
                deploy_to_debug_server_use_cfg(rebuild_js=True)
            elif sys.argv[2] == 'all':
                deploy_to_debug_server_use_cfg(True, True)
    else: print_help()








