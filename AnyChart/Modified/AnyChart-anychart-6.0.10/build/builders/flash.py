import build_utils
import os
import xml.dom.minidom
import string
import shutil
import subprocess
import platform

FLEX_SDK_DOWNLOAD = "http://fpdownload.adobe.com/pub/flex/sdk/builds/flex3/flex_sdk_3.0.0.477.zip"
BASE_DIR = build_utils.get_root()
BUILD_UTILS_DIR = os.path.join(BASE_DIR, 'build', 'utils')
FLEX_SDK_DIR = os.path.join(BUILD_UTILS_DIR, 'flex')
OUT_DIR = os.path.join(BASE_DIR, 'out')
SRC_DIR = os.path.join(BASE_DIR, 'flash-engine', 'src')
if platform.system() == 'Windows':
    OS_POSTFIX = '.exe'
else:
    OS_POSTFIX = ''

def main():
    build()

def __prebuild():
    clean()
    __check_flex_sdk()
    __update_version()
    __engine_set_error_reporting(True)
    __build_base_libs()

def build():
    __prebuild()
    __build_preloader()
    #__build_general_binaries(is_trial = True, output = "trial", license_type = "BUNDLE", main_class = "AnyChartSite")
    __build_general_binaries(is_trial = False, output = "bundle", license_type = "BUNDLE", main_class = "AnyChartRunnerBase")
    #__build_general_binaries(is_trial = False, output = "maps", license_type = "MAPS", main_class = "AnyChartRunnerBase")
    __build_general_binaries(is_trial = False, output = "charts", license_type = "CHARTS", main_class = "AnyChartRunnerBase")
    #__build_general_binaries(is_trial = False, output = "nonSecureCharts", license_type = "CHARTS", main_class = "AnyChartNonSecure")
    #__build_custom_trial(msg = 'Evaluation version', output = os.path.join("custom", "Evaluation version"))
    #__build_oracle()

def build_trial():
    __prebuild()
    __build_preloader()
    __build_general_binaries(is_trial = True, output = "trial", license_type = "BUNDLE", main_class = "AnyChartSite")

def clean():
    print "# cleaning..."
    for (name,path) in [(o,os.path.join(SRC_DIR, o)) for o in os.listdir(SRC_DIR) if os.path.isdir(os.path.join(SRC_DIR,o))]:
        if os.path.exists(os.path.join(path, "bin")) and os.path.exists(os.path.join(path, "bin","%s.swc" % name)):
            os.remove(os.path.join(path, "bin", "%s.swc" % name))

def __build_custom_trial(msg, output):
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart51HTMLRunner","src","AnyChartCustom.as"),
                                "const CUSTOM_TEXT:String = '.*';",
                                "const CUSTOM_TEXT:String = '%s';" % msg)
    __build_general_binaries(is_trial = True, output = output, license_type = "BUNDLE", main_class="AnyChartCustom")

def __build_oracle():
    __engine_set_error_reporting(True)
    __engine_set_oracle(True)
    __engine_set_license_type('BUNDLE')
    __set_plot_factory("OraclePlotFactory")

    __build_main_swf("AnyChartRunnerBase", os.path.join(OUT_DIR, "oracle", "AnyChart.swf"))

    __engine_set_error_reporting(False)
    __engine_set_oracle(False)
    __engine_set_license_type('BUNDLE')
    __set_plot_factory("BundlePlotFactory", False)

def __check_flex_sdk():
    print "# checking flex sdk..."
    archive = os.path.join(BUILD_UTILS_DIR, 'flex-sdk.zip')

    if os.path.exists(FLEX_SDK_DIR):
        print "flex SDK exists"
        return

    print "extracting flex sdk..."
    build_utils.extract_zip(archive, FLEX_SDK_DIR)

    print "updating chmods..."
    os.chmod(os.path.join(FLEX_SDK_DIR,'bin','mxmlc'), 0777)
    os.chmod(os.path.join(FLEX_SDK_DIR,'bin','compc'), 0777)

def __build_preloader():
    print "# building Preloader..."
    __build_swf(("AnyChart51HTMLPreloader","src","AnyChart51HTMLPreloader.as"),
                deps = ("AnyChart5CoreLib",),
                out = os.path.join(OUT_DIR,"Preloader.swf"),
                meta = "AnyChart preloader swf file")

def __build_general_binaries(is_trial, main_class, output, license_type):
    print "# building swfs... Trial: %s, Main class: %s, output: %s, license: %s" % (is_trial, main_class, output, license_type)
    __engine_set_error_reporting(True)
    __engine_set_license_type(license_type)
    __engine_set_flex_trial(is_trial)
    __engine_set_oracle(False)

    # Full
    __set_plot_factory("BundlePlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "AnyChart.swf"))
    __build_flex_swc("AnyChartFlex", os.path.join(OUT_DIR, output, "AnyChartFlexComponent.swc"))

    # Chart
    __set_plot_factory("ChartPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "Chart.swf"))
    __build_flex_swc("Chart", os.path.join(OUT_DIR, output, "AnyChartFlexChartComponent.swc"))

    # Funnel
    __set_plot_factory("FunnelPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "Funnel.swf"))

    # Gauge
    __set_plot_factory("GaugePlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "Gauge.swf"))
    __build_flex_swc("Gauge", os.path.join(OUT_DIR, output, "AnyChartGaugeFlexComponent.swc"))

    # Heat map
    __set_plot_factory("HeatMapPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "HeatMap.swf"))

    # Map
    __set_plot_factory("MapPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "Map.swf"))
    __build_flex_swc("Map", os.path.join(OUT_DIR, output, "AnyChartMapFlexComponent.swc"));

    # Pie
    __set_plot_factory("PiePlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "Pie.swf"))

    # Radar/Polar
    __set_plot_factory("RadarPolarPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "RadarPolar.swf"))

    # Treemap
    __set_plot_factory("TreeMapPlotFactory")
    __build_main_swf(main_class, os.path.join(OUT_DIR, output, "TreeMap.swf"))

    __set_plot_factory("BundlePlotFactory", False)
    __engine_set_flex_trial(False)
    __engine_set_license_type("BUNDLE")
    __engine_set_error_reporting(False)

def __build_main_swf(main_class, output):
    __build_swf(("AnyChart51HTMLRunner","src",main_class+".as"),
                deps = ("AnyChart5Core", "AnyChart5CoreLib"),
                out = output,
                meta = "AnyChart swf file")

def __build_flex_swc(class_name, output):
    print "# building flex engine: %s" % class_name
    __engine_set_flex_class_name(class_name)
    __build_swc("AnyChart51Flex", output, True)
    __engine_set_flex_class_name("AnyChartFlex")

def __set_plot_factory(name, need_rebuild = True):
    print "setting plot factory to %s" % name
    plotFactoryDestFile = os.path.join(SRC_DIR,"AnyChart5Core","src","com","anychart","plot","PlotFactory.as")
    plotFactorySrcFile = os.path.join(BUILD_UTILS_DIR, "plotFactories", name+".as")
    shutil.copyfile(plotFactorySrcFile, plotFactoryDestFile)
    if need_rebuild:
        __build_swc("AnyChart5Core")

def __build_base_libs():
    print "# bulding base libs:"
    __build_swc("AnyChart5CoreLib")
    __build_swc("SeriesPlot")
    __build_swc("PiePlot")
    __build_swc("AxesPlotBase")
    __build_swc("AxesPlot")
    __build_swc("FunnelPlot")
    __build_swc("GaugePlot")
    __build_swc("MapPlot")
    __build_swc("TreeMapPlot")
    __build_swc("TablePlot")
    __build_swc("RadarPolarPlot")
    __build_swc("MapSeriesTypeBubble")
    __build_swc("MapSeriesTypeConnector")
    __build_swc("MapSeriesTypeLine")
    __build_swc("MapSeriesTypeMarker")
    __build_swc("RadarPolarLineSeries")
    __build_swc("RadarPolarAreaSeries")
    __build_swc("RadarPolarMarkerSeries")
    __build_swc("RadarPolarBubbleSeries")
    __build_swc("SeriesTypeBar")
    __build_swc("SeriesTypeBar3D")
    __build_swc("SeriesTypeBubble")
    __build_swc("SeriesTypeHeatMap")
    __build_swc("SeriesTypeLine")
    __build_swc("SeriesTypeMarker")
    __build_swc("SeriesTypeStock")
    __build_swc("SeriesTypeArea")

def __build_swf(src, deps, out, meta):
    print "building swf: %s" % out
    params = [os.path.join(FLEX_SDK_DIR,"bin","mxmlc" + OS_POSTFIX),\
                "-keep-generated-actionscript=false", \
                "-output=%s" % out, \
                "-as3=true", \
                "-strict=true", \
                "-optimize=true", \
                "-use-network=false", \
                "-show-binding-warnings=false", \
                "-show-actionscript-warnings=false", \
                "-show-deprecation-warnings=false", \
                "-show-unused-type-selector-warnings=false", \
                "-warnings=false", \
                "-keep-as3-metadata=false", \
                "-load-config=%s/frameworks/flex-config.xml" % FLEX_SDK_DIR, \
                "-source-path+=%s/frameworks" % FLEX_SDK_DIR, \
                "-metadata.title='%s'" % meta, \
                "-metadata.description='%s'" % meta]
    for path in deps:
        params.append("-library-path+=%s" % os.path.join(SRC_DIR,path,"bin"))
    params.append(os.path.join(SRC_DIR,"/".join(src)))
    if not subprocess.call(params) == 0:
        raise Exception("Compilation error")

def __build_swc(project, out=None, is_flex_component = False):
    if out == None:
        out = os.path.join(SRC_DIR,project,"bin",project+".swc")
    print "building swc: %s..." % project
    project = os.path.join(SRC_DIR, project)
    classes = [el.getAttribute("path").encode("utf-8") for el in xml.dom.minidom.parse(os.path.join(project, ".flexLibProperties")).getElementsByTagName("classEntry")]
    embeds = [SRC_DIR+el.getAttribute("path").encode("utf-8") for el in xml.dom.minidom.parse(os.path.join(project,
        ".actionScriptProperties")).getElementsByTagName("libraryPathEntry") if el.getAttribute("kind") == "3" and el.getAttribute("linkType") == "1"]

    externals = [SRC_DIR+el.getAttribute("path").encode("utf-8") for el in xml.dom.minidom.parse(os.path.join(project,
        ".actionScriptProperties")).getElementsByTagName("libraryPathEntry") if el.getAttribute("kind") == "3" and el.getAttribute("linkType") == "2"]

    externals.append(os.path.join(FLEX_SDK_DIR,"frameworks","libs"))

    params = [os.path.join(FLEX_SDK_DIR,"bin","compc" + OS_POSTFIX),\
              "-load-config=%s" % os.path.join(FLEX_SDK_DIR,"frameworks","flex-config.xml"), \
              "-compiler.optimize=true", \
              "-source-path=%s" % os.path.join(SRC_DIR,project,"src"), \
              "-include-classes=%s" % ",".join(classes), \
              "-library-path=%s" % ",".join(embeds), \
              "-compiler.external-library-path+=%s" % ",".join(externals), \
              "-output=%s" % out]

    if not subprocess.call(params) == 0:
        raise Exception("Compilation error")

def __update_version():
    print "# updating version to %s" % build_utils.get_version_string()
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart5Core","src","com","anychart","context","VersionInfo.as"),
                                "Version [0-9\.a-zA-Z-]+ \(build #[0-9]+\)",
                                "Version %s" % build_utils.get_version_string())
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart51Flex","src","com","anychart","AnyChartFlex.as"),
                                "Version [0-9\.a-zA-Z-]+ \(build #[0-9]+\)",
                                "Version %s" % build_utils.get_version_string())

def __engine_set_flex_trial(is_trial):
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart51Flex","src","com","anychart","AnyChartFlex.as"),
                                "const IS_TRIAL:Boolean = (true|false);",
                                "const IS_TRIAL:Boolean = %s;" % (is_trial and 'true' or 'false'))

def __engine_set_oracle(is_oracle):
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart5Core","src","com","anychart","plot","PlotFactoryBase.as"),
                                "const IS_ORACLE:Boolean = (true|false);",
                                "const IS_ORACLE:Boolean = %s;" % (is_oracle and 'true' or 'false'))

def __engine_set_license_type(license_type):
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart5Core","src","com","anychart","plot","LicenseType.as"),
                                "var currentType:LicenseType = (BUNDLE|CHARTS|MAPS);",
                                "var currentType:LicenseType = %s;" % (license_type))

def __engine_set_error_reporting(enable):
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart5Core","src","com","anychart","AnyChart.as"),
                                "const ENABLE_ERROR_REPORTING:Boolean = (true|false);",
                                "const ENABLE_ERROR_REPORTING:Boolean = %s;" % (enable and 'true' or 'false'))

def __engine_set_flex_class_name(class_name):

    base_dir = os.path.join(SRC_DIR,"AnyChart51Flex","src","com","anychart")
    main_file = os.path.join(base_dir,class_name+".as")

    try:
        shutil.move(os.path.join(base_dir, "AnyChartFlex.as"), main_file)
    except:
        pass

    try:
        shutil.move(os.path.join(base_dir, "Chart.as"), main_file)
    except:
        pass

    try:
        shutil.move(os.path.join(base_dir, "Map.as"), main_file)
    except:
        pass
    try:
        shutil.move(os.path.join(base_dir, "Gauge.as"), main_file)
    except:
        pass

    build_utils.replace_in_file(main_file,
                                "public class (AnyChartFlex|Chart|Map|Gauge) extends UIComponent",
                                "public class %s extends UIComponent" % class_name)
    build_utils.replace_in_file(main_file,
                                "public function (AnyChartFlex|Chart|Map|Gauge)\(\)",
                                "public function %s()" % class_name)
    build_utils.replace_in_file(os.path.join(SRC_DIR,"AnyChart51Flex",".flexLibProperties"),
                                'classEntry path="com\.anychart\.(AnyChartFlex|Chart|Map|Gauge)',
                                'classEntry path="com.anychart.%s' % class_name)

if __name__ == "__main__":
    main()
