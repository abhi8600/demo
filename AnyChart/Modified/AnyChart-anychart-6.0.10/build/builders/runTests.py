#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from js_screenshots import *
import argparse
import build_utils
now = datetime.datetime.now()

ROOT = build_utils.get_root()
PATH_TO_OUT = os.path.join(ROOT, "out", "js_screenshots")
PATH_TO_TESTS = os.path.join(ROOT, "tests", 'automatic')
TRIAL_PATH = os.path.join(ROOT, "out", "trial")
UTILS_PATH = os.path.join(ROOT, "build", "utils", "js_screenshots")
PATH_TO_SELECTED = os.path.join(ROOT, "out", "js_screenshots", "selectedImages")

DATA = {'docs': os.path.join(ROOT, '../site/deploy/docs/users-guide/Samples'), 
        'gallery': os.path.join(ROOT, '../site/deploy/gallery/samples'), 
        'tests': PATH_TO_TESTS}

parser = argparse.ArgumentParser(description='''Generating screenshots and compare them with images from last release.''')
parser.add_argument('-e', '--example', type=str, action='store', help='Name of example. IMPORTANT: should be with group name: docs/chart-with-smth.', default = None)
parser.add_argument('-a', '--alias', type=str, action='store', help='Group name. Use it if you want to run test only for one group.', default = None)
parser.add_argument('-g', '--generate', action="store_true", help='flag to generate screenshots only (default = False).', default=False)
parser.add_argument('-c', '--compare', action="store_true", help='flag to compare screenshots only (default = False).', default=False)
parser.add_argument('-n', type=int, help='number of theards, for generating screenshots only (default = 5).', default = 3)
parser.add_argument('-tf', '--target_folder', type=str, action='store', help='folder to generate images in (default = "' + PATH_TO_OUT + '/images" ).', default = PATH_TO_OUT +"/images")
parser.add_argument('-sf', '--src_folder', type=str, action='store', help='folder to compare images with (default = "' + PATH_TO_SELECTED + '" ).', default = PATH_TO_SELECTED)
parser.add_argument('-js', '--js_folder', type=str, action='store', help='folder where js files are (default = "' + TRIAL_PATH + '").', default = TRIAL_PATH)
args = parser.parse_args() 
output_folder = args.target_folder
comparing_folder = args.src_folder
data = DataIn()

if not os.path.exists(PATH_TO_OUT):
    os.mkdir(PATH_TO_OUT)
if not os.path.exists(os.path.join(PATH_TO_OUT, 'images')):
    os.mkdir(os.path.join(PATH_TO_OUT, 'images'))    
#for d in DATA:
#    data.add(InputFolder(d, DATA[d]))

data.add(InputFolder('docs', os.path.join(ROOT, '../site/deploy/docs/users-guide/Samples')))

passedTests, failedTests = ImagesGenerator().generate(args.n, output_folder, args.js_folder, data)
all_generated = len(passedTests + failedTests)
print 'GENERATION passed : ' + str(len(passedTests))+ '  failed : ' +str(len(failedTests))

passedTests, suspected, failed = ImagesComparer().compareVersion(passedTests, os.path.join(PATH_TO_OUT, 'release'))
all_comparedVersion = len(passedTests + suspected + failed)
failedTests += failed
print 'RELEASE passed : ' + str(len(passedTests))+ '  failed : ' +str(len(failedTests))

passedTests, failed = ImagesComparer().compareSelected(passedTests, suspected, comparing_folder)
all_comparedSelected = len(passedTests + failed)
failedTests += failed
print 'SELECTED passed : ' + str(len(passedTests))+ '  failed : ' +str(len(failedTests))

passed, failed = ImagesGenerator().generateResized(args.n, os.path.join(PATH_TO_OUT, 'tmp'), passedTests)
all_generatedResized = len(passed + failed)
failedTests += failed
print 'RESIZED passed : ' + str(len(passed)) + '  failed : ' + str(len(failed))
passed, failed = ImagesComparer().compareResized(passed, os.path.join(PATH_TO_OUT, 'images'), 'Comparing resized')
all_comparedResized = len(passed + failed)
failedTests += failed
passedTests += passed
print 'COMPARING RESIZED passed : ' + str(len(passed))+ '  failed : ' +str(len(failed))

report = ImageGenerationResults('AC-JSTESTSDEVELOP', '00043', passedTests, failedTests)
report.generateXMLReport()
report.generateHTMLReport(all_generated, all_comparedVersion, all_comparedSelected, all_generatedResized, all_comparedResized)
report.generateFailedSamples()

print "---\n %d tests done, %d failed, %s min passed. \n " %(len(passedTests)+len(failedTests), len(failedTests), str(round((time.time() - start)/60, 2)))
 

