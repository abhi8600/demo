package com.anychart.plot {
	
	import com.anychart.Chart;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.viewController.ChartView;
	import flash.utils.getDefinitionByName;
	
	public final class PlotFactory {
		
		include "_heatmap_inc.as"
		
		public static function getChartXML(source:String):XML { return PlotFactoryBase.getChartXML(source); }
		public static function createSinglePlot(view:ChartView, chart:Chart, xmlData:XML):void { PlotFactoryBase.createSinglePlot(view, chart, xmlData); }
		public static function createPlot(view:ChartView, chart:Chart, plotNode:XML):void { PlotFactoryBase.createPlot(view, chart, plotNode); }
	}
}