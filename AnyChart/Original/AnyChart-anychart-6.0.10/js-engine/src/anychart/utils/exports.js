goog.provide('anychart.utils.exports');
//------------------------------------------------------------------------------
//                  Exports from AnyChart.js.
//------------------------------------------------------------------------------
var export_scope = window['anychart']['scope'];

var goog = goog || export_scope['goog'];
goog.events.Event = goog.events.Event || export_scope['Event'];
goog.events.EventTarget = goog.events.EventTarget || export_scope['EventTarget'];
goog.events.EventType = goog.events.EventType || export_scope['EventType'];
goog.net.XhrIo = goog.net.XhrIo || export_scope['XhrIo'];
