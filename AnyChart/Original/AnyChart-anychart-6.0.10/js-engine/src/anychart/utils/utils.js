/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.utils.stringUtils}</li>
 *  <li>@class {anychart.utils.deserialization}</li>
 *  <li>@class {anychart.utils.deserialization.XmlNodeType_}</li>
 *  <li>@class {anychart.utils.disposeUtils}</li>
 *  <li>@class {anychart.utils.JSONUtils}</li>
 *  <li>@class {anychart.utils.serialization}</li>
 *  <li>@class {anychart.utils.StatisticUtils}</li>
 *  <li>@class {anychart.utils.Base64Utils}</li>
 * <ul>.
 */
goog.provide('anychart.utils');
goog.require('anychart.visual.color');
//------------------------------------------------------------------------------
//
//                          stringUtils class.
//
//------------------------------------------------------------------------------

/**
 * String utils package.
 */
anychart.utils.stringUtils = {};

/**
 * Trims all whitespace from the left of the string.
 * @param {String} str source string.
 * @return {String} left trimmed string.
 */
anychart.utils.stringUtils.ltrim = function (str) {
    return str.replace(/^\s*/, '');
};

/**
 * Trims all whitespace from the right of the string.
 * @param {String} str source string.
 * @return {String} right trimmed string.
 */
anychart.utils.stringUtils.rtrim = function (str) {
    var ws = /\s/;
    var i = str.length;
    while (ws.test(str.charAt(--i))) {
    }
    return str.slice(0, i + 1);
};

/**
 * Trims all whitespace from the both sides of the string.
 * @param {String} str source string.
 * @return {String} trimmed string.
 */
anychart.utils.stringUtils.trim = function (str) {
    var res = str.replace(/^\s*/, '');
    var ws = /\s/;
    var i = res.length;
    while (ws.test(res.charAt(--i))) {
    }
    return res.slice(0, i + 1);
};

//------------------------------------------------------------------------------
//
//                          deserialization class.
//
//------------------------------------------------------------------------------

/**
 * Deserialization utils.
 */
anychart.utils.deserialization = {};

/**
 * Built-in XML node types enumeration.
 *
 * @private
 * @enum {Number}
 */
anychart.utils.deserialization.XmlNodeType_ = {
    ELEMENT_NODE:1,
    ATTRIBUTE_NODE:2,
    TEXT_NODE:3,
    CDATA_SECTION_NODE:4,
    ENTITY_REFERENCE_NODE:5,
    ENTITY_NODE:6,
    PROCESSING_INSTRUCTION_NODE:7,
    COMMENT_NODE:8,
    DOCUMENT_NODE:9,
    DOCUMENT_TYPE_NODE:10,
    DOCUMENT_FRAGMENT_NODE:11,
    NOTATION_NODE:12
};

/**
 * Public function that parses XML string and returns corresponding JSON.
 *
 * @see anychart.utils.deserialization#parseXML()
 * @param {String} xmlString xml source string.
 * @return {object} node transformation result (may by null).
 */
anychart.utils.deserialization.parseXMLString = function (xmlString) {
    return anychart.utils.deserialization.parseXML(
        new DOMParser().parseFromString(xmlString, 'text/xml')
    );
};

/**
 * Public function that parses XML Document from the XmlHttpRequest.responseXML
 * result and returns corresponding JSON. It hides all errors in Document
 * structure, so the result may be not as strict as expected.
 *
 * The function converts XML node to an object with attributes introduced as
 * same named string properties. All subnodes are also converted to
 * correspondingly named properties by the next rules:
 * - if there is only one child with this name, it is converted into an object
 *   an added just as an object property to the parent object.
 * - if there are multiple children with the same name, they are also converted
 *   to a separate objects and then added to the parent object as an array.
 * - if there is an attribute named just like the node(s), the node(s) will
 *   completely replace the attribute.
 * - if there is any text value or a CDATA node or any their combination in the
 *   node, that value is stored as a string in a property named "value" even in
 *   cases when there is a subnode named "value" (but the node with this name
 *   still can be found in "#children#" array mentioned below).
 *
 * Also some service properties are added an object representing a node:
 * - "#name#" property - the node name
 * - "#children#" array property - all node children listed in order they appear
 *   in the node
 * - mentioned above "value" string property - contains textual value of the
 *   node
 *
 * It is recommended to use hasProp, getProp and getPropArray methods with JSON
 * objects.
 *
 * Usage sample:
 * <code>
 *   var a = new XMLHttpRequest();
 *   a.open('GET', 'http://sample.com', false, "", "");
 *   a.send(null);
 *   // for example there was the next XML:
 *   // <root a="123">
 *   //   <child />
 *   //   <child>some text value<![CDATA[   and a CDATA   ]]></child>
 *   //   <child_other/>
 *   // </root>
 *   var json = anychart.utils.deserialization.parseXML(a.responseXML);
 * </code>
 * json variable will have the following structure:
 * {
 *   #name#: "root",
 *   #children#: [
 *      {#name#: "child", #children#: []},
 *      {#name#: "child", #children#: [], value: "some text value   and a CDATA   "}
 *      {#name#: "child_other", #children#: [vbox, hbox, vbox]}
 *   ],
 *   a: "123",
 *   child: [
 *      {#name#: "child", #children#: []},
 *      {#name#: "child", #children#: [], value: "some text value   and a CDATA   "}
 *   ],
 *   child_other: {#name#: "child_other", #children#: []}
 * }
 *
 * @param {Node} node  - node to be transformed (Node is a base class for all
 * built-in XML entities).
 * @return {object} - node transformation result (may by null);.
 */
anychart.utils.deserialization.parseXML = function (node) {
    if (!node) {
        return null;
    }
    // aliases
    var XmlNodeType_ = anychart.utils.deserialization.XmlNodeType_;
    var trim = anychart.utils.stringUtils.trim;
    var parseXML = anychart.utils.deserialization.parseXML;

    // parsing node type
    switch (node.nodeType) {
        case XmlNodeType_.ELEMENT_NODE:
//            var result = {'#name#': anychart.utils.stringUtils.trim(
//                    node.nodeName), '#children#': []};
            var result = {};
            var i;

            var len = node.childNodes.length;
            // collecting subnodes
            for (i = 0; i < len; i++) {
                var subnode = parseXML(node.childNodes[i]);
                if (subnode == null) {
                    //continue;
                } else {
                    var subNodeName = node.childNodes[i].nodeName;
                    if (subNodeName.charAt(0) == '#') {
                        if (result['value'] == undefined ||
                            !(typeof result['value'] == 'string')) {
                            result['value'] = subnode['value'];
                        } else {
                            result['value'] += subnode['value'];
                        }
                    } else if (result[subNodeName] == undefined) {
                        result[subNodeName] = subnode;
                    } else if (result[subNodeName] instanceof Array) {
                        result[subNodeName].push(subnode);
                    } else if (subNodeName != 'value' ||
                        (typeof result['value'] != 'string')) {
                        result[subNodeName] = [result[subNodeName], subnode];
                    }
                }
            }

            len = (node.attributes == null) ? 0 : node.attributes.length;
            // collecting attributes
            for (i = 0; i < len; i++) {
                /** @type {Attr} */
                var attr = node.attributes[i];
                // assertTrue(attr.nodeType == NodeType.ATTRIBUTE_NODE);
                if (result[attr.nodeName] == undefined)
                    result[attr.nodeName] = attr.nodeValue;
            }
            return result;
        case XmlNodeType_.TEXT_NODE:
            var value = trim(node.nodeValue);
            return (value == '') ? null : {value:value};
        case XmlNodeType_.CDATA_SECTION_NODE:
            return {value:node.nodeValue};
        case XmlNodeType_.DOCUMENT_NODE:
            return parseXML(node.documentElement);
        default:
            return null;
    }
};

/**
 * Tests whether the JSON object has a property named name.
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {boolean} Property represented in JSON object.
 */
anychart.utils.deserialization.hasProp = function (obj, name) {
    return obj && obj[name] != undefined;
};

/**
 * Returns property value. If there is an array property for this name - returns
 * the first one. If there is no such property - returns null (not undefined)
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of the property.
 * @return {object} Property value or first element of array if property is an
 * array.
 */
anychart.utils.deserialization.getProp = function (obj, name) {
    if (!obj) return null;
    var prop = obj[name];
    if (prop == null || prop == undefined) return null;
    return (prop instanceof Array) ? prop[0] : prop;
};

/**
 * Sets the property of JSON object.
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @param {*} value Value of property.
 */
anychart.utils.deserialization.setProp = function (obj, name, value) {
    if (!obj) return;
    obj[name] = value;
};
anychart.utils.deserialization.deleteProp = function (obj, name) {
    if (obj && obj[name]) delete obj[name];
};

/**
 * Returns property as an array. If there is only one property with this name -
 * boxes it into an array.
 * If there is no such prop returns an undefined property as array with one
 * undefined element.
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {Array.<Object>} Array of fetched/created object.
 */
anychart.utils.deserialization.getPropArray = function (obj, name) {
    if (!obj || !name) return [];
    if (goog.isDef(obj[name])) {
        if (!(obj[name] instanceof Array)) obj[name] = [obj[name]];
    } else obj[name] = [];
    return obj[name];
};

/**
 * Retrieves string value from first or single property with this name.
 * There can be the next cases:
 * 1) there is no property with such name in the object - return null;
 * 2) there is one string-type property with this name - return property;
 * 3) there is one object-type property with this name - retrieve "value" prop
 *    from the property;
 * 4) there is an array of object-type properties - retrieve "value" prop from
 *    0-th element;
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {String} String value of property.
 */
anychart.utils.deserialization.getStringProp = function (obj, name) {
    if (!obj) return null;
    var prop = obj[name];
    if (!prop) return null;
    prop = (prop instanceof Array) ? prop[0] : prop;
    if (typeof prop == 'string')
        return prop;
    else
        return (prop.value == undefined) ? null : prop.value;
};

/**
 * Tests whether the value is a percent value string.
 *
 * @param {*} val Value expected as percent-like string XX%.
 * @return {boolean} Is value a percent.
 */
anychart.utils.deserialization.isPercent = function (val) {
    if (!val) return NaN;
    var tmp = anychart.utils.stringUtils.trim(val.toString());
    return tmp.charAt(tmp.length - 1) == '%';
};

/**
 * Gets percent value as ratio (0..1)
 *
 * @param {*} val - a string of "XX%" format, where XX - is a percent value
 * number.
 * @return {Number} Ratio of percent (may be NaN).
 */
anychart.utils.deserialization.getPercent = function (val) {
    if (anychart.utils.deserialization.isPercent(val)) {
        var strVal = anychart.utils.stringUtils.trim(val.toString());
        strVal = strVal.substr(0, strVal.length - 1);
        return anychart.utils.deserialization.getNum(strVal) / 100;
    } else return NaN;
};

/**
 * TODO: TEST IT
 */
anychart.utils.deserialization.getSimplePercent = function (val) {
    return anychart.utils.deserialization.getNum(val) / 100;
};

/**
 * TODO: TEST IT
 */
anychart.utils.deserialization.getSimplePercentProp = function (config, propName) {
    return anychart.utils.deserialization.getSimplePercent(
        anychart.utils.deserialization.getProp(config, propName)
    );
};


/**
 * Gets value as a boolean (input expects a string like "true", "false",
 * "1" or "0").
 *
 * @param {*} val Value.
 * @return {boolean} Boolean value.
 */
anychart.utils.deserialization.getBool = function (val) {
    if (val == undefined) return false;
    return (val == '1') || (val.toString().toLowerCase() == 'true');
};

/**
 * Gets property value as a boolean.
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {boolean} Is property value is boolean.
 */
anychart.utils.deserialization.getBoolProp = function (obj, name) {
    return anychart.utils.deserialization.getBool(
        anychart.utils.deserialization.getProp(obj, name));
};

/**
 * Gets value as a number.
 *
 * @param {*} val Value.
 * @return {Number} Number (may be NaN).
 */
anychart.utils.deserialization.getNum = function (val) {
    if (goog.isString(val)) {
        if (!anychart.utils.stringUtils.trim(val)) return NaN;
    } else if (goog.isNumber(val)) {
        return val
    } else if (val == null) {
        return NaN;
    }
    return Number(val);
};

/**
 * Get property value as a number.
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {Number} Number (may be NaN).
 */
anychart.utils.deserialization.getNumProp = function (obj, name) {
    return anychart.utils.deserialization.getNum(anychart.utils.deserialization.getProp(obj, name));
};


/**
 * Gets value as a number normalized by [0..1].
 *
 * @param {*} val Value.
 * @return {Number} Number normalized by [0..1] (may be NaN).
 */
anychart.utils.deserialization.getRatio = function (val) {
    var tmp = Number(val);
    if (tmp > 1) tmp = 1;
    else if (tmp < 0) tmp = 0;
    return tmp;
};

/**
 * Gets property value as a number nrmalized by [0..1]
 *
 * @param {Object} obj JSON object.
 * @param {String} key Name of property.
 * @return {Number} Number normalized by [0..1] (may be NaN).
 */
anychart.utils.deserialization.getRatioProp = function (obj, key) {
    return anychart.utils.deserialization.getRatio(
        anychart.utils.deserialization.getProp(obj, key));
};

/**
 * Gets value as a number normalized by [-1..1]
 *
 * @param {*} val Value.
 * @return {Number} Number normalized by [-1..1] (may be NaN).
 */
anychart.utils.deserialization.getExtendedRatio = function (val) {
    var tmp = Number(val);
    if (tmp > 1) tmp = 1;
    else if (tmp < -1) tmp = -1;
    return tmp;
};

/**
 * Gets value as an integer
 *
 * @param {*} val Value.
 * @return {int} Integer part of value (may be NaN).
 */
anychart.utils.deserialization.getInt = function (val) {
    return Math.floor(Number(val));
};

/**
 * Checks whether an object is not null as has a property named "enabled" that
 * is set to boolean true.
 * If there is no such property, but an object is not null, returns opt_default.
 *
 * @param {Object.<*>} node Object.
 * @param {boolean=} opt_default Value returning by default (true by def).
 * @return {boolean} Boolean value of 'enabled' property or opt_default.
 */
anychart.utils.deserialization.isEnabled = function (node, opt_default) {
    if (!node) return false;
    var prop = anychart.utils.deserialization.getProp(node, 'enabled');
    if (prop == null || prop == undefined) return (opt_default == undefined ||
        opt_default == null) ? true : opt_default;
    return anychart.utils.deserialization.getBool(prop);
};

anychart.utils.deserialization.isEnabledProp = function (node, name) {
    return anychart.utils.deserialization.isEnabled(anychart.utils.deserialization.getProp(node, name));
};

/**
 * Gets value as a lowercase string.
 *
 * @param {*} val Value.
 * @return {String} Lowercase string.
 */
anychart.utils.deserialization.getLString = function (val) {
    return (val) ? val.toString().toLowerCase() : '';
};

/**
 * Gets property value as a lowercase string.
 *
 * @param {Object} obj JSON object.
 * @param {String} name Name of property.
 * @return {String} Lowercase string.
 */
anychart.utils.deserialization.getLStringProp = function (obj, name) {
    return anychart.utils.deserialization.getLString(
        anychart.utils.deserialization.getProp(obj, name));
};

/**
 * Gets value as a lowercase string just as getLString. Backward readability
 * issue.
 *
 * @param {*} val Value.
 * @return {String} Lowercase string.
 */
anychart.utils.deserialization.getEnumItem = function (val) {
    return (val) ? val.toString().toLowerCase() : '';
};

/**
 * Gets property value as lowercase string just as getLStringProp. Backward
 * readability issue.
 *
 * @param {Object} obj JSON object.
 * @param {String} key Name of property.
 * @return {String} Lowercase string.
 */
anychart.utils.deserialization.getEnumProp = function (obj, key) {
    return anychart.utils.deserialization.getEnumItem(
        anychart.utils.deserialization.getProp(obj, key));
};

/**
 * Test the value to be an "auto" string.
 *
 * @param {*} val Value.
 * @return {boolean} Is value an "auto" string.
 */
anychart.utils.deserialization.isAuto = function (val) {
    return (val != undefined &&
        anychart.utils.deserialization.getLString(val) == 'auto');
};

/**
 * Parses a value as a color definition string. Returns colorContainer that
 * handles static and dynamic colors.
 *
 * @param {*} val Value as a color definition string.
 * @param {Number=} opt_opacity Opacity.
 * @return {anychart.visual.color.ColorContainer} ColorContainer than handles
 * static and dynamic colors.
 */
anychart.utils.deserialization.getColor = function (val, opt_opacity) {
    return anychart.visual.color.colorParser.parse(val, opt_opacity);
};

/**
 * Gets value from prop property, represents it as a color definition string.
 * Returns colorContainer that handles static and dynamic colors.
 * @see {anychart.utils.deserialization.getColor}
 *
 * @param {Object} config JSON object.
 * @param {String} prop Name of property.
 * @return {anychart.visual.color.ColorContainer} ColorContainer than handles
 * static and dynamic colors.
 */
anychart.utils.deserialization.getColorProp = function (config, prop) {
    return anychart.utils.deserialization.getColor(
        anychart.utils.deserialization.getProp(config, prop));
};
/**
 *
 * @param {*} value
 * @param {anychart.locale.DateTimeLocale} locale
 * @return {Number}
 */
anychart.utils.deserialization.getDateTime = function (value, locale) {
    return locale.getTimeStamp(String(value));
};

/**
 * Copies full passed object structure to a new object and returns it.
 * All functions and value types (not objects) are assigned as is, objects are
 * also copied.
 *
 * @param {Object.<*>} obj JSON Object, workability to class instances is not
 * guaranteed.
 * @return {Object.<*>} Copy of object.
 */
anychart.utils.deserialization.copy = function (obj) {
    var res;
    if (obj instanceof Function) {
        res = obj;
    } else if (obj instanceof Array) {
        res = [];
        for (var i = 0, len = obj.length; i < len; i++)
            res[i] = anychart.utils.deserialization.copy(obj[i]);

    } else if (obj instanceof Object) {
        res = {};
        for (var prop in obj)
            res[prop] = anychart.utils.deserialization.copy(obj[prop]);
    } else {
        res = obj;
    }
    return res;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          disposeUtils class.
//
//----------------------------------------------------------------------------------------------------------------------

anychart.utils.disposeUtils = {};

/**
 * Dispose object.
 *
 * @param {object} target Object to dispose.
 */
anychart.utils.disposeUtils.dispose = function (target) {
    if (target == undefined) return;
    if (target['dispose'] != undefined) target.dispose();
    target = null;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          JSONUtils class.
//
//----------------------------------------------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.utils.JSONUtils = {};

/**
 * Merge node and all internal properties.
 * @param {object} template Template object.
 * @param {object} actual  Actual object.
 * @param {String} opt_nodeName Merged node name, used to merge special node's,
 * like gradient, format, text, save_as_image_item_text, save_as_pdf_item_text,
 * print_chart_item_text and action.
 * @param {String} opt_excludeNode Exclude node name, use if you want to exclude
 * node from merging process.
 * @return {*}
 * <p>
 *  Usage sample:
 *  <code>
 *      var template = {
 *          a : 10,
 *          b : 'text',
 *          c: '20',
 *          d: function(){},
 *          x : {a:10, b:20}
 *      };
 *      var actual = {
 *          b :  10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, c : 40}
 *      };
 *      var res = anychart.utils.JSONUtils.mergeProperty(template, actual);
 *  </code>
 *  Expected result:
 *  <code>
 *      res = {
 *          a : 10,
 *          b : 10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, b : 20, c : 40}
 *      };
 *  </code>
 *  Field 'a' - is remains unchanged, coincidence b, c, d - replaced, e = added.
 * </p>
 */
anychart.utils.JSONUtils.merge = function (template, actual, opt_nodeName, opt_excludeNode) {
    if (template == null && actual == null) return null;
    if (template == null) return actual;
    if (actual == null) return (template instanceof Array) ?
        template[template.length - 1] : template;
    // alias
    var des = anychart.utils.deserialization;
    // clone
    template = des.copy(template);
    actual = des.copy(actual);

    var res = {};
    res = anychart.utils.JSONUtils.mergeSimpleProperty(template, actual);
    if (opt_nodeName) {
        if (opt_nodeName == 'gradient') {
            if (des.getPropArray(actual, 'key').length != 0) {
                res['key'] = actual['key'];
            } else {
                res['key'] = des.getPropArray(template, 'key');
            }
        } else if (opt_nodeName == 'format' || opt_nodeName == 'text' ||
            opt_nodeName == 'save_as_image_item_text' ||
            opt_nodeName == 'save_as_pdf_item_text' ||
            opt_nodeName == 'print_chart_item_text') {
            return actual;
        } else if (opt_nodeName == 'action') {
            if (des.getPropArray(actual, 'arg').length == 0)
                anychart.utils.JSONUtils.mergeSimpleProperty(template, actual, res);
            else res = anychart.utils.JSONUtils.mergeProperty_(template, actual, opt_excludeNode);
            return res;
        } else res = anychart.utils.JSONUtils.mergeProperty_(template, actual, opt_excludeNode);
    } else res = anychart.utils.JSONUtils.mergeProperty_(template, actual, opt_excludeNode);
    return res;
};

/**
 * Merge properties with override template values.
 * @param {Object} template Template object.
 * @param {Object} actual Actual object.
 * @return {Object} Merged object.
 * <p>
 *  Usage sample:
 *  <code>
 *      var template = {
 *          a : 10,
 *          b : 'text',
 *          c: '20',
 *          d: function(){},
 *          x : {a:10, b:20}
 *      };
 *      var actual = {
 *          b :  10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, c : 40}
 *      };
 *      var res = anychart.utils.JSONUtils.mergeProperty(template, actual);
 *  </code>
 *  Expected result:
 *  <code>
 *      res = {
 *          a : 10,
 *          b : 10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, c : 40}
 *      };
 *  </code>
 *  All coincidence properties was overridden.
 * </p>
 */
anychart.utils.JSONUtils.mergeWithOverride = function (template, actual) {
    if (template == null && actual == null) return null;
    if (template == null) return actual;
    if (actual == null) return template;

    if (!anychart.utils.JSONUtils.isSimpleProperty(template)) {
        var res = anychart.utils.deserialization.copy(template);
        for (var i in actual) {
            var node = anychart.utils.deserialization.getPropArray(res, i);
            if (node.length == 0) res[i] = actual[i];
            else {
                if (goog.isArray(actual[i])) {
                    for (var j = 0; j < actual[i].length; j++) {
                        anychart.utils.JSONUtils.mergeNodeWithOverride_(res[i],
                            actual[i][j]);
                    }
                } else anychart.utils.JSONUtils.mergeNodeWithOverride_(res[i],
                    actual[i]);
            }
        }
    }
    return res;
};

/**
 * Searches the "node" node in the array of nodes to be merged. If there is no
 * occurrence than adds the node to the array, otherwise replaces the node
 * coinciding by name.
 *
 * @private
 * @param {Array.<Object>} nodes Array of nodes to be merged.
 * @param {Object} node Merging node.
 */
anychart.utils.JSONUtils.mergeNodeWithOverride_ = function (nodes, node) {
    var nodeName = anychart.utils.deserialization.getProp(node, 'name');
    var subNode = anychart.utils.JSONUtils.getNodeByName(nodes, nodeName);
    if (subNode) {
        var subNodeIndex = anychart.utils.JSONUtils.getNodeIndexInArrayByName(
            nodes, nodeName);
        nodes[subNodeIndex] = node;
    } else nodes.push(node);
};

/**
 * Merge property with override template values if name of properties coincided
 * with passed name property.
 * @param {object} template Template value.
 * @param {object} actual Actual value.
 * @param {String} name Property.
 * @param {boolean} opt_ignoreOther Flag to merge only node specified by name.
 * @param {String} opt_excludeNode Exclude node name, use if you want to exclude
 * node from merging process.
 * @return {Object} Merged object.
 */
anychart.utils.JSONUtils.mergeWithOverrideByName = function (template, actual, name, opt_ignoreOther, opt_excludeNode) {
    if (template == null && actual == null) return null;
    if (template == null) return actual;
    if (actual == null) return template;
    var deserialization = anychart.utils.deserialization;
    var res = anychart.utils.deserialization.copy(template);
    for (var i in actual) {
        if (!res[i]) {
            res[i] = actual[i];
        } else {
            if (i == name) {
                if (deserialization.hasProp(res[i], 'name') &&
                    deserialization.hasProp(actual[i], 'name') &&
                    anychart.utils.JSONUtils.getNodeByName(res[i],
                        deserialization.getProp(res[i], 'name')) ==
                        deserialization.getProp(actual[i], 'name')) {
                    delete res[i];
                }
                res[i] = actual[i];
            } else if (!opt_ignoreOther) {
                res[i] = anychart.utils.JSONUtils.merge(actual[i], res[i], i,
                    opt_excludeNode);
            }
        }
    }
    return res;
};

/**
 * Merge property.
 * <p>
 *  Cases:
 *  <ul>
 *      <li>If template and actual has a different types, actual will be
 *          assigned without merge.</li>
 *      <li>If actual and template is a simple property, actual will be assigned
 *          without merge.</li>
 *  </ul>
 * </p>
 * @see anychart.utils.JSONUtils.isSimpleProperty
 * @private
 * @param {*} template Template value.
 * @param {*} actual Actual value.
 * @param {String} opt_excludeNode Exclude node name, use if you want to exclude
 * node from merging process.
 * @return {*}
 * <p>
 *  Usage sample:
 *  <code>
 *      var template = {
 *          a : 10,
 *          b : 'text',
 *          c: '20',
 *          d: function(){},
 *          x : {a:10, b:20}
 *      };
 *      var actual = {
 *          b :  10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, c : 40}
 *      };
 *      var res = anychart.utils.JSONUtils.mergeProperty(template, actual);
 *  </code>
 *  Expected result:
 *  <code>
 *      res = {
 *          a : 10,
 *          b : 10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20},
 *          x : {a : 20, b : 20, c : 40}
 *      };
 *  </code>
 *  Field 'a' - is remains unchanged, coincidence b, c, d - replaced, e = added.
 * </p>
 */
anychart.utils.JSONUtils.mergeProperty_ = function (template, actual, opt_excludeNode) {
    if (actual instanceof Function) return actual;
    if ((template instanceof Array) && (actual instanceof Object) && !(actual instanceof Array)) {
        var lastInTemplate = template[template.length - 1];
        return anychart.utils.JSONUtils.merge(lastInTemplate, actual);
    } else if (template instanceof Object && actual instanceof Object) {
        var i;
        var res;
        res = {};
        if ((actual instanceof Array) && (template instanceof Array)) res = [];
        for (i in template) {
            if (i != opt_excludeNode) {
                res[i] = anychart.utils.JSONUtils.merge(template[i], actual[i]);
                delete actual[i];
            } else {
                res[i] = actual[i];
            }
        }
        for (i in actual) {
            res[i] = actual[i];
        }
    } else res = actual;
    return res;
};

/**
 * Merge simple property's like number's, string's function's in object,
 * ignore Array's and object's.
 * @param {Object} template Template object.
 * @param {Object} actual Actual object.
 * @param {Object} opt_res Target object to merge result.
 * @return {Object} Object with merged simple properties.
 * <p>
 *  Usage sample:
 *  <code>
 *      var template = {
 *          a : 10,
 *          b : 'text',
 *          c : '20',
 *          d : function(){}
 *      };
 *      var actual = {
 *          b :  10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){},
 *          f : {a:10, b:20}
 *      };
 *      var res = {};
 *      anychart.utils.JSONUtils.mergeSimpleProperty(template, actual, res);
 *  </code>
 *  Expected result:
 *  <code>
 *      res = {
 *          a : 10,
 *          b : 10,
 *          c : 'text',
 *          d : 100,
 *          e : function(){}
 *      };
 *  </code>
 *  Field 'a' - is remains unchanged, f - ignored, coincidence b, c, d -
 *  replaced, e = added.
 * </p>
 */
anychart.utils.JSONUtils.mergeSimpleProperty = function (template, actual, opt_res) {
    if (!opt_res) opt_res = {};
    var i;
    for (i in template) {
        if (anychart.utils.JSONUtils.isSimpleProperty(template[i]))
            opt_res[i] = template[i];
    }

    for (i in actual) {
        if (anychart.utils.JSONUtils.isSimpleProperty(actual[i]))
            opt_res[i] = actual[i];
    }
    return opt_res;
};

/**
 * Indicates,  is property simple (not array or object).
 *
 * @param {*} value Indicated value.
 * @return {boolean} Boolean.
 */
anychart.utils.JSONUtils.isSimpleProperty = function (value) {
    return Boolean(value instanceof Function || !(value instanceof Array ||
        value instanceof Object));
};

/**
 * Indicate, is specified value an Object.
 *
 * @param {*} value Target value.
 * @return {boolean} Object.
 */
anychart.utils.JSONUtils.isObject = function (value) {
    return Boolean(value instanceof Object && !(value instanceof Function ||
        value instanceof Array));
};

/**
 * Search in targetNode node with specified value of name param.
 *
 * @param {Object} targetNode Target node to search.
 * @param {String} name Node name.
 * @return {Object} Node with specified name.
 * <p>
 *  Usage sample:
 *  <code>
 *      var root = {
 *          node1 = {
 *              name : 'nodeName1',
 *              someParam : 'value'
 *          }
 *          node2 = {
 *              name : 'nodeName2',
 *              someParam : 'someParam2'
 *          }
 *      }
 *   var result = anychart.utils.JSONUtils.getNodeByName(root, 'nodeName1);
 *   alert.(root.node1 == result); //return true.
 *  </code>
 * </p>
 */
anychart.utils.JSONUtils.getNodeByName = function (targetNode, name) {
    return anychart.utils.JSONUtils.getNodeByPropNameAndValue(targetNode, 'name', name)
};

anychart.utils.JSONUtils.getNodeById = function (targetNode, id) {
    return anychart.utils.JSONUtils.getNodeByPropNameAndValue(targetNode, 'id', id)
};

anychart.utils.JSONUtils.getNodeByPropNameAndValue = function (targetNode, propName, value) {
    var deserialization = anychart.utils.deserialization;
    value = deserialization.getLString(value);
    if (!anychart.utils.JSONUtils.isSimpleProperty(targetNode)) {
        for (var i in targetNode) {
            if (goog.isArray(targetNode[i])) {
                var arrayVal = anychart.utils.JSONUtils.getNodeByName(
                    targetNode[i], value);
                if (arrayVal) return arrayVal;
            }
            if (deserialization.hasProp(targetNode[i], propName) &&
                deserialization.getLStringProp(targetNode[i], propName) == value)
                return targetNode[i];
        }
    }
    return null;
};

/**
 * Searches the node by name in array and return its index. Returns first
 * occurrence.
 *
 * @param {Array.<Object>} targetNode Array of nodes.
 * @param {String} name Name of node to find.
 * @return {Number} Index of node.
 */
anychart.utils.JSONUtils.getNodeIndexInArrayByName = function (targetNode, name) {
    var deserialization = anychart.utils.deserialization;
    name = deserialization.getLString(name);
    if (!anychart.utils.JSONUtils.isSimpleProperty(targetNode)) {
        for (var i in targetNode) {
            if (deserialization.hasProp(targetNode[i], 'name') &&
                deserialization.getLStringProp(targetNode[i], 'name') == name)
                return i;
        }
    }
    return null;
};
anychart.utils.JSONUtils.insert = function (array, index, data) {
    var count = array.length;
    if (index > count) {
        array.push(data);
        return array
    }
    var res = [];
    var i;

    for (i = 0; i < index; i++)
        res[i] = array[i];

    res[index] = data;

    for (i = (index + 1); i <= count; i++)
        res[i] = array[i - 1];

    return res;
};
anychart.utils.JSONUtils.removeById = function (array, id) {
    var elementCount = array.length;
    var deleteTargets = [];
    var deleteTargetIds = [];
    var i;
    var des = anychart.utils.deserialization;

    for (i = 0; i < elementCount; i++) {
        if (des.hasProp(array[i], 'id')) {
            var elementId = des.getProp(array[i], 'id');
            if (elementId == id) {
                deleteTargets.push(i);
                deleteTargetIds.push(elementId);
            }
        }
    }

    var deleteTargetsCount = deleteTargets.length;
    for (i = 0; i < deleteTargetsCount; i++) array.splice(deleteTargets[i], 1);

    return deleteTargetIds;
};

anychart.utils.JSONUtils.mergeDataNode = function (template, actual) {
    var des = anychart.utils.deserialization;
    des.deleteProp(actual, 'id');

    anychart.utils.JSONUtils.mergeSimpleProperty(template, actual, template);

    for (var i in actual) {
        if (i == null || i == undefined) continue;
        if (i == 'actions' || i == 'extra_labels' || i == 'extra_markers' || i == 'extra_tooltips')
            template[i] = actual[i];
        else
            template[i] = anychart.utils.JSONUtils.merge(template[i], actual[i], i);
    }
    return template;
};
/**
 * Apply chart config replaces.
 * @param {Object} data
 * @param {Array.<Array.<String>>} replaces
 */
anychart.utils.JSONUtils.applyReplaces = function (data, replaces) {
    if (!data) return null;
    var replaceCount = replaces.length;
    if (!replaces || replaceCount == 0) return data;

    return anychart.utils.JSONUtils.execApplyReplaces_(data, replaces, replaceCount);
};
/**
 * @param {Object} data
 * @param replaces
 */
anychart.utils.JSONUtils.execApplyReplaces_ = function (data, replaces, replacesCount) {
    var res;
    if (data instanceof Function) {
        res = data;
    } else if (data instanceof Array) {
        res = [];
        for (var i = 0, len = data.length; i < len; i++)
            res[i] = anychart.utils.JSONUtils.execApplyReplaces_(data[i], replaces, replacesCount);

    } else if (data instanceof Object) {
        res = {};
        for (var prop in data)
            res[prop] = anychart.utils.JSONUtils.execApplyReplaces_(data[prop], replaces, replacesCount);
    } else {
        res = data;
        for (var j = 0; j < replacesCount; j++) {
            if (data.indexOf(replaces[j][0].toString()) != -1) {
                res = data.split(replaces[j][0]).join(replaces[j][1]);
            }
        }
    }
    return res;
};
//------------------------------------------------------------------------------
//
//                          serialization class.
//
//------------------------------------------------------------------------------

anychart.utils.serialization = {};

/**
 * Serializes passed JSON object to a human-readable string.
 *
 * @public
 * @param {Object} obj - JSON object expected, but actually it can handle value of any type.
 * @return {String}
 */
anychart.utils.serialization.serialize = function (obj) {
    return anychart.utils.serialization.serializeItem_(obj, '');
};

/**
 * Serializes passed object with passed indent. Used internally by public serialize function
 *
 * @param {*} obj - any object or primitive value, may be null or undefined.
 * @param {String} indent - a string of "\t", that specifies an indention.
 * @return {String}
 */
anychart.utils.serialization.serializeItem_ = function (obj, indent) {
    var objType = typeof obj;
    if (objType != 'object' || obj === null) {
        // simple data type
        if (objType == 'string') obj = "'" + anychart.utils.serialization.escapeString_(obj) + "'";
        return String(obj);
    } else {
        // recurse array or object
        var isArray = (obj && obj.constructor == Array);
        var res = isArray ? '[' : '{';
        var propIndent = '\n\t' + indent;
        var first = true;
        for (var prop in obj) {
            var propVal = obj[prop];
            var subObjType = typeof propVal;
            if (subObjType == 'function' || prop.charAt(0) == '#') continue;
            propVal = anychart.utils.serialization.serializeItem_(propVal, indent + '\t');
            res += propIndent + (isArray ? '' : "'" + prop + "': ") + String(propVal);
            if (first) {
                first = false;
                propIndent = ',' + propIndent;
            }
        }
        return res + '\n' + indent + (isArray ? ']' : '}');
    }
};

/**
 * Creates a printable representation of the string (replaces new line, carriage
 * return, tabulation, form feed
 * characters with special symbols).
 *
 * @param {String} str
 * @return {String}
 */
anychart.utils.serialization.escapeString_ = function (str) {
    var res = '';
    var start = 0;
    var leng = 0;
    for (var i = 0, len = str.length; i < len; i++) {
        var c = str.charAt(i);
        switch (c) {
            case '\n':
                res += str.substr(start, leng) + '\\n';
                start = i + 1;
                leng = 0;
                break;
            case '\t':
                res += str.substr(start, leng) + '\\t';
                start = i + 1;
                leng = 0;
                break;
            case '\r':
                res += str.substr(start, leng) + '\\r';
                start = i + 1;
                leng = 0;
                break;
            case '\f':
                res += str.substr(start, leng) + '\\f';
                start = i + 1;
                leng = 0;
                break;
            default:
                leng++;
                break;
        }
    }
    return res + str.substr(start, leng);
};
//------------------------------------------------------------------------------
//
//                          StatisticUtils class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.utils.StatisticUtils = function () {
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.utils.StatisticUtils.fieldName_ = null;
//------------------------------------------------------------------------------
//                          Median
//------------------------------------------------------------------------------
anychart.utils.StatisticUtils.prototype.getMedian = function (points, field) {
    var sortedPoints = this.getSortedPoints(points, field);
    var point, point1, point2;

    if (sortedPoints.length % 2 == 0) {
        point1 = sortedPoints[Math.floor(sortedPoints.length / 2) - 1];
        point2 = sortedPoints[Math.floor(sortedPoints.length / 2) + 1];
        if (point1 == null || point2 == null) return NaN;
    } else {
        point = sortedPoints[Math.ceil(sortedPoints.length / 2)];
        if (point == null) return NaN;
    }

    return point ?
        point.getTokenValue(anychart.utils.StatisticUtils.fieldName_) :
        ((point1.getTokenValue(anychart.utils.StatisticUtils.fieldName_) +
            point2.getTokenValue(anychart.utils.StatisticUtils.fieldName_)) / 2);
};
//------------------------------------------------------------------------------
//                          Mode
//------------------------------------------------------------------------------
anychart.utils.StatisticUtils.prototype.getMode = function (points, field) {
    var cTable = {};
    var i;
    var mode = 0;
    var maxCnt = 0;
    for (i = 0; i < points.length; i++) {
        var value = Number(points[i].getTokenValue(field));
        if (!value) continue;
        if (cTable[value] != null)
            cTable[value]++;
        else
            cTable[value] = 1;

        if (cTable[value] > maxCnt) {
            maxCnt = cTable[value];
            mode = value;
        }
    }

    return mode;
};
//------------------------------------------------------------------------------
//                          Sort function
//------------------------------------------------------------------------------
anychart.utils.StatisticUtils.prototype.getSortedPoints = function (points, field) {
    anychart.utils.StatisticUtils.fieldName_ = field;
    var newPoints = [];
    var i = 0;
    for (i = 0; i < points.length; i++)
        newPoints.push(points[i]);


    return newPoints.sort(function (a, b) {
        return Number(a.getTokenValue(anychart.utils.StatisticUtils.fieldName_)) -
            Number(b.getTokenValue(anychart.utils.StatisticUtils.fieldName_));
    });
};
//------------------------------------------------------------------------------
//
//                          DateUtils class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.utils.DateUtils = function () {
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatYear = function (date, offset) {
    var intOffset = Math.floor(offset);
    date.setUTCFullYear(date.getUTCFullYear() + intOffset);

    if (offset != intOffset)
        anychart.utils.DateUtils.addFloatMonths(date, (offset - intOffset) * 12);
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatMonths = function (date, offset) {
    var intOffset = Math.floor(offset);
    date.setUTCMonth(date.getUTCMonth() + intOffset);

    if (offset != intOffset)
        anychart.utils.DateUtils.addFloatDays(date, (offset - intOffset) * 30);
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatDays = function (date, offset) {
    var intOffset = Math.floor(offset);
    date.setUTCDate(date.getUTCDate() + intOffset);

    if (offset != intOffset)
        anychart.utils.DateUtils.addFloatHours(date, (offset - intOffset) * 24);
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatHours = function (date, offset) {
    var intOffset = Math.floor(offset);
    date.setUTCHours(date.getUTCHours() + intOffset);

    if (offset != intOffset)
        anychart.utils.DateUtils.addFloatMinutes(date, (offset - intOffset) * 60);
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatMinutes = function (date, offset) {
    var intOffset = Math.floor(offset);
    date.setUTCMinutes(date.getUTCMinutes() + intOffset);

    if (offset != intOffset)
        anychart.utils.DateUtils.addFloatSeconds(date, (offset - intOffset) * 60);
};
/**
 * @param {Date} date
 * @param {Number} offset
 */
anychart.utils.DateUtils.addFloatSeconds = function (date, offset) {
    date.setUTCSeconds(date.getUTCSeconds() + offset);
};
//------------------------------------------------------------------------------
//
//                          Base64Utils class.
//
//------------------------------------------------------------------------------

/**
 * Utils for working with base64 coding/encoding.
 * @constructor
 */
anychart.utils.Base64Utils = function () {
};

/**
 * The alphabet.
 * @private
 * @const
 * @type {String}
 */
anychart.utils.Base64Utils.keyStr_ = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

/**
 * Getter for key string.
 * @return {String} Key string
 */
anychart.utils.Base64Utils.getKeyStr = function () {
    return anychart.utils.Base64Utils.keyStr_;
};

/**
 * Method for utf8 encoding.
 * @private
 * @param {String} input String to encode.
 */
anychart.utils.Base64Utils.utf8_encode_ = function (input) {
    input = input.replace(/\r\n/g, '\n');

    var utftext = '';

    for (var i = 0; i < input.length; i++) {
        var c = input.charCodeAt(i);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if ((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }
    }

    return utftext;
};

/**
 * Method for utf8 decoding.
 * @private
 * @param {String} utftext String to decode.
 */
anychart.utils.Base64Utils.utf8_decode_ = function (utftext) {
    var output = "";
    var i = 0;
    var c, c1, c2, c3;
    c = c1 = c2 = c3 = 0;

    while (i < utftext.length) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            output += String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i + 1);
            output += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i + 1);
            c3 = utftext.charCodeAt(i + 2);
            output += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return output;
};

/**
 * Encode input string to base64-string.
 * @param {String} input String to encode.
 */
anychart.utils.Base64Utils.encode = function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = anychart.utils.Base64Utils.utf8_encode_(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
            anychart.utils.Base64Utils.getKeyStr().charAt(enc1) +
            anychart.utils.Base64Utils.getKeyStr().charAt(enc2) +
            anychart.utils.Base64Utils.getKeyStr().charAt(enc3) +
            anychart.utils.Base64Utils.getKeyStr().charAt(enc4);

    }

    return output;
};

/**
 * Decode base64-string to string.
 * @param {String} input base64-string.
 */
anychart.utils.Base64Utils.decode = function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

    while (i < input.length) {
        enc1 = anychart.utils.Base64Utils.getKeyStr().indexOf(input.charAt(i++));
        enc2 = anychart.utils.Base64Utils.getKeyStr().indexOf(input.charAt(i++));
        enc3 = anychart.utils.Base64Utils.getKeyStr().indexOf(input.charAt(i++));
        enc4 = anychart.utils.Base64Utils.getKeyStr().indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = anychart.utils.Base64Utils.utf8_decode_(output);

    return output;
};
