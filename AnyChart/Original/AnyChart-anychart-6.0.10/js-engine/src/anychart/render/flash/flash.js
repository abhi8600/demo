/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.render.flash.JSConverter};</li>
 *  <li>@class {anychart.render.flash.FlashRenderer};</li>
 *  <li>@class {anychart.render.flash.FFPrintFix};.</li>
 * <ul>
 */
goog.provide('anychart.render.flash');

//------------------------------------------------------------------------------
//
//                  JSConverter class
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.render.flash.JSConverter = {
    isAttribute: function(prop) {
        var type = typeof prop;
        return type == 'string' || type == 'number' || type == 'boolean';
    },

    isArray: function(prop) {
        return typeof prop != 'string' && typeof prop.length != 'undefined';
    },

    createNode: function(nodeName, data) {
        var res = '<' + nodeName;

        if (typeof data['functionName'] != 'undefined') {
            data['function'] = data['functionName'];
            delete data['functionName'];
        }

        for (var j in data) {
            if (j != 'format' && j != 'text' && j != 'custom_attribute_value' && j != 'attr' && anychart.render.flash.JSConverter.isAttribute(data[j])) {
                res += ' ' + j + '=\"' + data[j] + '\"';
            }
        }
        res += '>';
        for (var j in data) {
            if (j == 'arg' && anychart.render.flash.JSConverter.isArray(data[j])) {
                var args = data[j];
                for (var i = 0; i < args.length; i++) {
                    res += '<arg><![CDATA[' + args[i] + ']]></arg>';
                }
            } else if (j == 'custom_attribute_value' || j == 'attr') {
                res += '<![CDATA[' + data[j] + ']]>';
            } else if (j == 'format' || j == 'text') {
                res += '<' + j + '><![CDATA[' + data[j] + ']]></' + j + '>';
            } else if (anychart.render.flash.JSConverter.isArray(data[j])) {
                var nodes = data[j];
                for (var i = 0; i < nodes.length; i++) {
                    res += anychart.render.flash.JSConverter.createNode(j, nodes[i]);
                }
            } else if (!anychart.render.flash.JSConverter.isAttribute(data[j])) {
                res += anychart.render.flash.JSConverter.createNode(j, data[j]);
            }
        }
        res += '</' + nodeName + '>';
        return res;
    },

    convert: function(obj) {
        return anychart.render.flash.JSConverter.createNode('anychart', obj);
    }
};

//------------------------------------------------------------------------------
//
//              FlashRenderer class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @implements {anychart.render.IChartRenderer}
 */
anychart.render.flash.FlashRenderer = function() {

};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.render.flash.FlashRenderer.prototype.anychartBase_ = null;
anychart.render.flash.FlashRenderer.prototype.flashObject = null;
anychart.render.flash.FlashRenderer.prototype.flashObjectContainer_ = null;

/**
 * @private
 * @type {Boolean}
 */
anychart.render.flash.FlashRenderer.prototype.needSetXMLFileAfterCreation_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.render.flash.FlashRenderer.prototype.needSetXMLDataAfterCreation_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.render.flash.FlashRenderer.prototype.needSetJSONFileAfterCreation_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.render.flash.FlashRenderer.prototype.needSetJSONDataAfterCreation_ = null;
//------------------------------------------------------------------------------
//                          FF print fix properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.render.flash.FFPrintFix}
 */
anychart.render.flash.FlashRenderer.prototype.ffPrintFix_ = null;
//------------------------------------------------------------------------------
//                          Internal
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.render.flash.FlashRenderer.prototype.init = function(chart) {
    this.anychartBase_ = chart;
    this.isChartCreated_ = false;
    this.needSetXMLFileAfterCreation_ = false;
    this.needSetXMLDataAfterCreation_ = false;
    this.needSetJSONFileAfterCreation_ = false;
    this.needSetJSONDataAfterCreation_ = false;
    this.enableFirefoxPrintPreviewFix = this.anychartBase_['enableFirefoxPrintPreviewFix'];
};
/**
 * @private
 * @type {Boolean}
 */
anychart.render.flash.FlashRenderer.prototype.isChartCreated_ = false;
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.onBeforeChartCreate = function() {
    this.isChartCreated_ = true;
    if (this.needSetXMLFileAfterCreation_) this.setXMLFile(this.anychartBase_['xmlFile']);
    if (this.needSetXMLDataAfterCreation_) this.setXMLData(this.anychartBase_['xmlData']);
    if (this.needSetJSONFileAfterCreation_) this.setJSONFile(this.anychartBase_['jsonFile']);
    if (this.needSetJSONDataAfterCreation_) this.setJSONData(this.anychartBase_['jsonData']);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.onBeforeChartDraw = function() {
    if (this.enableFirefoxPrintPreviewFix) this.createFFPrintFixObjects_();
};
//------------------------------------------------------------------------------
//                  Getting SVG as base64.
//------------------------------------------------------------------------------
/**
 * Gets chart SVG image as string or base64 encoded string.
 * @param {Boolean} base64 If set to true then base64 encoded string is returned, if set to false - plain string.
 * @return {String} null.
 */
anychart.render.flash.FlashRenderer.prototype.getSVG = function (base64) {
    return null;
};

//------------------------------------------------------------------------------
//                  Show/Hide.
//------------------------------------------------------------------------------

/**
 * Hide chart.
 */
anychart.render.flash.FlashRenderer.prototype.hide = function() {
    this.anychartBase_['visible'] = false;
    if (this.flashObject) {
        this.flashObject['style']['visibility'] = "hidden";
        this.flashObject.setAttribute('width', '1px');
        this.flashObject.setAttribute('height', '1px');
    }
};
/**
 * Show chart
 */
anychart.render.flash.FlashRenderer.prototype.show = function() {
    this.anychartBase_['visible'] = true;
    if (this.flashObject) {
        this.flashObject['style']['visibility'] = 'visible';
        this.flashObject.setAttribute('width', this.width_ + "");
        this.flashObject.setAttribute('height', this.height_ + "");
    }
};
//------------------------------------------------------------------------------
//                          Dispose.
//------------------------------------------------------------------------------
anychart.render.flash.FlashRenderer.prototype.disposeVisual = function(obj, id) {
    if (obj && obj['nodeName'] == 'OBJECT') {
        if (AnyChart.platform.isIE && AnyChart.platform.isWin) {
            obj['style']['display'] = 'none';
            (function() {
                if (obj['readyState'] == 4) {
                    if (AnyChart.platform.needFormFix && id != null)
                        AnyChart.disposeFlashObjectInIE(window[id]);

                    AnyChart.disposeFlashObjectInIE(obj);
                }
                else {
                    setTimeout(arguments['callee'], 10);
                }
            })();
        } else {
            obj['parentNode']['removeChild'](obj);
        }
    }
};
/**
 * Dispose flash object in IE
 * @param {Object} obj
 */
anychart.render.flash.FlashRenderer.prototype.disposeFlashObjectInIE = function(obj) {
    for (var j in obj) {
        if (typeof obj[j] == 'function') {
            obj[j] = null;
        }
    }
    if (obj['parentNode']) obj['parentNode']['removeChild'](obj);
};
//------------------------------------------------------------------------------
//                          Embed
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.canWrite = function() {
    return AnyChart.platform.hasRequiredVersion && this.anychartBase_['swfFile'] != null;
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.write = function(target) {
    var width = this.anychartBase_['width'] + '';
    var height = this.anychartBase_['height'] + '';
    var path = this.anychartBase_['preloaderSWFFile'] ? this.anychartBase_['preloaderSWFFile'] : this.anychartBase_['swfFile'];

    //ffPrintFix
    this.enableFirefoxPrintPreviewFix = this.enableFirefoxPrintPreviewFix && AnyChart.platform.isFirefox;
    if (this.enableFirefoxPrintPreviewFix) this.ffPrintFix_ = new anychart.render.flash.FFPrintFix();

    if (AnyChart.platform.isIE && AnyChart.platform.isWin) {
        var htmlCode = '<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\"';
        htmlCode += ' id=\"' + this.anychartBase_['id'] + '\"';
        htmlCode += ' width=\"' + width + '\"';
        htmlCode += ' height=\"' + height + '\"';
        htmlCode += ' style=\"visibility:' + (this.anychartBase_['visible'] ? 'visible' : 'hidden') + '\"';
        htmlCode += ' codebase=\"' + AnyChart.platform.protocol + '//fpdownload.macromedia.com/get/flashplayer/current/swflash.cab\">';

        htmlCode += this.generateStringParam_('movie', path);
        htmlCode += this.generateStringParam_('bgcolor', this.anychartBase_['bgColor']);
        htmlCode += this.generateStringParam_('allowScriptAccess', 'always');
        htmlCode += this.generateStringParam_('flashvars', this.buildFlashVars_());

        if (this.anychartBase_['wMode'] != null) htmlCode += this.generateStringParam_('wmode', this.anychartBase_['wMode']);

        htmlCode += '</object>';

        if (AnyChart.platform.needFormFix) {

            var targetForm = null;
            var tmp = target;
            while (tmp) {
                if (tmp.nodeName != null && tmp.nodeName.toLowerCase() == 'form') {
                    targetForm = tmp;
                    break;
                }
                tmp = tmp.parentNode;
            }
            if (targetForm != null) {

                window[this.anychartBase_['id']] = {};
                window[this.anychartBase_['id']].SetReturnValue = function() {
                };
                target.innerHTML = htmlCode;

                window[this.anychartBase_['id']].SetReturnValue = null;
                var fncts = {};
                for (var j in window[this.anychartBase_['id']]) {
                    if (typeof (window[this.anychartBase_['id']][j]) == 'function')
                        fncts[j] = window[this.anychartBase_['id']][j];
                }
                this.flashObject = window[this.anychartBase_['id']] = targetForm[this.anychartBase_['id']];
                this.anychartBase_['flashObject'] = this.flashObject;

                for (var j in fncts) {
                    this.rebuildExternalInterfaceFunctionForFormFix_(this.flashObject, j);
                }

            } else {
                target.innerHTML = htmlCode;
                this.flashObject = document.getElementById(this.anychartBase_['id']);
                this.anychartBase_['flashObject'] = this.flashObject;
            }

        } else {
            target.innerHTML = htmlCode;
            this.flashObject = document.getElementById(this.anychartBase_['id']);
            this.anychartBase_['flashObject'] = this.flashObject;
        }

    } else {
        var obj = document.createElement('object');
        obj.setAttribute('type', 'application/x-shockwave-flash');
        obj.setAttribute('id', this.anychartBase_['id']);
        obj.setAttribute('width', width);
        obj.setAttribute('height', height);
        obj.setAttribute('data', path);
        obj.setAttribute('style', 'visibility: ' + (this.anychartBase_['visible'] ? 'visible' : 'hidden'));

        this.addParam_(obj, 'movie', path);
        this.addParam_(obj, 'bgcolor', this.anychartBase_['bgColor']);
        this.addParam_(obj, 'allowScriptAccess', 'always');
        this.addParam_(obj, 'flashvars', this.buildFlashVars_());

        if (this.anychartBase_['wMode'] != null) this.addParam_(obj, 'wmode', this.anychartBase_['wMode']);

        if (target.hasChildNodes()) {
            while (target.childNodes.length > 0) {
                target.removeChild(target.firstChild);
            }
        }

        this.flashObject = target.appendChild(obj);
        this.anychartBase_['flashObject'] = this.flashObject;
        this.flashObjectContainer_ = target;
    }
    return this.flashObject != null;
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.generateStringParam_ = function(paramName, paramValue) {
    return '<param name=\"' + paramName + '\" value=\"' + paramValue + '\" />';
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.addParam_ = function(target, paramName, paramValue) {
    var node = document.createElement('param');
    node.setAttribute('name', paramName);
    node.setAttribute('value', paramValue);
    target.appendChild(node);
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.rebuildExternalInterfaceFunctionForFormFix_ = function(obj, functionName) {
    eval('obj[functionName] = function(){return eval(this.CallFunction("<invoke name=\\"' + functionName + '\\" returntype=\\"javascript\\">" + __flash__argumentsToXML(arguments,0) + "</invoke>"));}');
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.buildFlashVars_ = function() {
    var res = '__externalobjid=' + this.anychartBase_['id'];
    if (this.anychartBase_['preloaderSWFFile']) res += '&swffile=' + this.anychartBase_['swfFile'];
    if (this.anychartBase_['xmlFile']) res += '&xmlfile=' + this.anychartBase_['xmlFile'];
    if (this.anychartBase_['enabledChartEvents']) res += '&__enableevents=1';
    if (this.anychartBase_['enabledChartMouseEvents']) res += '&__enablechartmouseevents=1';

    //messages
    res += "&preloaderInit=" + this.anychartBase_['messages']['preloaderInit'];
    res += "&preloaderLoading=" + this.anychartBase_['messages']['preloaderLoading'];
    res += "&init=" + this.anychartBase_['messages']['init'];
    res += "&loadingConfig=" + this.anychartBase_['messages']['loadingConfig'];
    res += "&loadingTemplates=" + this.anychartBase_['messages']['loadingTemplates'];
    res += "&loadingResources=" + this.anychartBase_['messages']['loadingResources'];
    res += "&noData=" + this.anychartBase_['messages']['noData'];
    res += "&waitingForData=" + this.anychartBase_['messages']['waitingForData'];

    return res;
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.canSetConfigAfterWrite = function() {
    return true;
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updatePrintForFirefox = function() {
    this.disposeFFPrintFixObjects_();
    this.createFFPrintFixObjects_();
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setSize = function(width, height) {
    if (this.flashObject) {
        this.flashObject.setAttribute('width', width + '');
        this.flashObject.setAttribute('height', height + '');
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.remove = function() {
    this.removeFlashObject_();
    if (AnyChart && AnyChart.hasCharts()) {
        var numCharts = AnyChart.getNumCharts();
        for (var i = 0; i < numCharts; i++) {
            var chart = AnyChart.getCharts()[i];
            if (chart == this) {
                chart = null;
                AnyChart.getCharts().splice(i, 1);
                break;
            }
        }
    }
    if (AnyChart && AnyChart.getChartsMap() && this.id_ != null)
        AnyChart.getChartsMap()[this.id_] = null;

};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.clear = function() {
    if (this.flashObject && this.flashObject['Clear'])
        this.flashObject['Clear']();
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.refresh = function() {
    if (this.flashObject != null && this.flashObject['Refresh'] != null)
        this.flashObject['Refresh']();
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updateData = function(path, data) {
    if (this.flashObject != null && this.flashObject['UpdateData'] != null)
        this.flashObject['UpdateData'](path, data);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setLoading = function(opt_message) {
    if (this.flashObject && this.flashObject['SetLoading'])
        this.flashObject['SetLoading'](opt_message);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getInformation = function() {
    return this.flashObject['GetInformation']();
};
//------------------------------------------------------------------------------
//                          Config setters
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setXMLFile = function(path) {
    if (this.flashObject && this.isChartCreated_) {
        this.flashObject['SetXMLDataFromURL'](path)
    } else {
        this.needSetXMLFileAfterCreation_ = true;
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setXMLData = function(data) {
    if (this.flashObject && this.isChartCreated_) {
        this.flashObject['SetXMLDataFromString'](data)
    } else {
        this.needSetXMLDataAfterCreation_ = true;
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setJSONFile = function(path) {
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setJSONData = function(data) {
    if (this.flashObject && this.isChartCreated_) {
        this.setXMLData(anychart.render.flash.JSConverter.convert(data))
    } else {
        this.needSetJSONDataAfterCreation_ = true;
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setXMLDataFromString = function(data) {
    if (this.flashObject && this.isChartCreated_) {
        this.flashObject['SetXMLDataFromString'](data)
    } else {
        this.needSetXMLDataAfterCreation_ = true;
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setXMLDataFromURL = function(data) {
    if (this.flashObject && this.isChartCreated_) {
        this.flashObject['SetXMLDataFromURL'](data)
    } else {
        this.needSetXMLFileAfterCreation_ = true;
    }
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setViewXMLFile = function(viewId, url) {
    if (this.flashObject && this.isChartCreated_)
        this.flashObject['UpdateViewFromURL'](viewId, url);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setViewData = function(viewId, data) {
    if (this.flashObject && this.isChartCreated_)
        this.flashObject['UpdateViewFromString'](viewId, data);
};
//------------------------------------------------------------------------------
//                          Point
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.addPoint = function(seriesId, pointData) {
    if (this.flashObject != null && this.flashObject['AddPoint'] != null)
        this.flashObject['AddPoint'](seriesId, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.addPointAt = function(seriesId, pointIndex, pointData) {
    if (this.flashObject != null && this.flashObject['AddPointAt'] != null)
        this.flashObject['AddPointAt'](seriesId, pointIndex, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.removePoint = function(seriesId, pointId) {
    if (this.flashObject != null && this.flashObject['RemovePoint'] != null)
        this.flashObject['RemovePoint'](seriesId, pointId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updatePoint = function(seriesId, pointId, pointData) {
    if (this.flashObject != null && this.flashObject['UpdatePoint'] != null)
        this.flashObject['UpdatePoint'](seriesId, pointId, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updatePointData = function(groupName, pointName, data) {
    if (this.flashObject != null && this.flashObject['UpdatePointData'] != null)
        this.flashObject['UpdatePointData'](groupName, pointName, data);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.highlightPoint = function(seriesId, pointId, highlight) {
    if (this.flashObject != null && this.flashObject['HighlightPoint'] != null)
        this.flashObject['HighlightPoint'](seriesId, pointId, highlight);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.selectPoint = function(seriesId, pointId, select) {
    if (this.flashObject != null && this.flashObject['SelectPoint'] != null)
        this.flashObject['SelectPoint'](seriesId, pointId, select);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updatePointWithAnimation = function(seriesId, pointId, newValue, animationSettings) {
    if (this.flashObject && this.flashObject['updatePointWithAnimation'] != null)
        this.flashObject['updatePointWithAnimation'](seriesId, pointId, newValue, animationSettings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.startPointsAnimation = function() {
    if (this.flashObject && this.flashObject['startPointsUpdate'] != null)
        this.flashObject['startPointsUpdate']();
};
//------------------------------------------------------------------------------
//                          Series
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.addSeries = function(seriesData) {
    if (this.flashObject != null && this.flashObject['AddSeries'] != null)
        this.flashObject['AddSeries'](seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.addSeriesAt = function(index, seriesData) {
    if (this.flashObject != null && this.flashObject['AddSeriesAt'] != null)
        this.flashObject['AddSeriesAt'](index, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.removeSeries = function(seriesId) {
    if (this.flashObject != null && this.flashObject['RemoveSeries'] != null)
        this.flashObject['RemoveSeries'](seriesId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updateSeries = function(seriesId, seriesData) {
    if (this.flashObject != null && this.flashObject['UpdateSeries'] != null)
        this.flashObject['UpdateSeries'](seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.showSeries = function(seriesId, seriesData) {
    if (this.flashObject != null && this.flashObject['ShowSeries'] != null) //TODO WTF
        this.flashObject['ShowSeries'](seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.highlightSeries = function(seriesId, highlight) {
    if (this.flashObject != null && this.flashObject['HighlightSeries'] != null)
        this.flashObject['HighlightSeries'](seriesId, highlight);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.highlightCategory = function(categoryName, highlight) {
    if (this.flashObject != null && this.flashObject['HighlightCategory'] != null)
        this.flashObject['HighlightCategory'](categoryName, highlight);
};
//------------------------------------------------------------------------------
//                          Axes
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.showAxisMarker = function(markerId, seriesData) {
    if (this.flashObject != null && this.flashObject['ShowAxisMarker'] != null)
        this.flashObject['ShowAxisMarker'](markerId, seriesData);
};
//------------------------------------------------------------------------------
//                          Custom attributes
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setPlotCustomAttribute = function(attributeName, attributeValue) {
    if (this.flashObject != null && this.flashObject['SetPlotCustomAttribute'] != null)
        this.flashObject['SetPlotCustomAttribute'](attributeName, attributeValue);
};
//------------------------------------------------------------------------------
//                     Dashboard external methods
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_setPlotCustomAttribute = function(viewId, attributeName, attributeValue) {
    if (this.flashObject != null && this.flashObject['View_SetPlotCustomAttribute'] != null)
        this.flashObject['View_SetPlotCustomAttribute'](viewId, attributeName, attributeValue);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_addSeries = function(viewId, seriesData) {
    if (this.flashObject != null && this.flashObject['View_AddSeries'] != null)
        this.flashObject['View_AddSeries'](viewId, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_removeSeries = function(viewId, seriesId) {
    if (this.flashObject != null && this.flashObject['View_RemoveSeries'] != null)
        this.flashObject['View_RemoveSeries'](viewId, seriesId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_addSeriesAt = function(viewId, index, seriesData) {
    if (this.flashObject != null && this.flashObject['View_AddSeriesAt'] != null)
        this.flashObject['View_AddSeriesAt'](viewId, index, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_updateSeries = function(viewId, seriesId, seriesData) {
    if (this.flashObject != null && this.flashObject['View_UpdateSeries'] != null)
        this.flashObject['View_UpdateSeries'](viewId, seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_showSeries = function(viewId, seriesId, seriesData) {
    if (this.flashObject != null && this.flashObject['View_ShowSeries'] != null)
        this.flashObject['View_ShowSeries'](viewId, seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_addPoint = function(viewId, seriesId, pointData) {
    if (this.flashObject != null && this.flashObject['View_AddPoint'] != null)
        this.flashObject['View_AddPoint'](viewId, seriesId, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_addPointAt = function(viewId, seriesId, pointIndex, pointData) {
    if (this.flashObject != null && this.flashObject['View_AddPointAt'] != null)
        this.flashObject['View_AddPointAt'](viewId, seriesId, pointIndex, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_removePoint = function(viewId, seriesId, pointId) {
    if (this.flashObject != null && this.flashObject['View_RemovePoint'] != null)
        this.flashObject['View_RemovePoint'](viewId, seriesId, pointId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_updatePoint = function(viewId, seriesId, pointId, pointData) {
    if (this.flashObject != null && this.flashObject['View_UpdatePoint'] != null)
        this.flashObject['View_UpdatePoint'](viewId, seriesId, pointId, pointData);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_clear = function(viewId) {
    if (this.flashObject != null && this.flashObject['View_Clear'] != null)
        this.flashObject['View_Clear'](viewId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_refresh = function(viewId) {
    if (this.flashObject != null && this.flashObject['View_Refresh'] != null)
        this.flashObject['View_Refresh'](viewId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_highlightSeries = function(viewId, seriesId, highlight) {
    if (this.flashObject != null && this.flashObject['View_HighlightSeries'] != null)
        this.flashObject['View_HighlightSeries'](viewId, seriesId, highlight);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_highlightPoint = function(viewId, seriesId, pointId, highlight) {
    if (this.flashObject != null && this.flashObject['View_HighlightPoint'] != null)
        this.flashObject['View_HighlightPoint'](viewId, seriesId, pointId, highlight);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_highlightCategory = function(viewId, categoryName, highlight) {
    if (this.flashObject != null && this.flashObject['View_HighlightCategory'] != null)
        this.flashObject['View_HighlightCategory'](viewId, categoryName, highlight);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.view_selectPoint = function(viewId, seriesId, pointId, select) {
    if (this.flashObject != null && this.flashObject['View_SelectPoint'] != null)
        this.flashObject['View_SelectPoint'](viewId, seriesId, pointId, select);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.updateViewPointData = function(viewId, groupName, pointName, data) {
    if (this.flashObject != null && this.flashObject['UpdateViewPointData'] != null)
        this.flashObject['UpdateViewPointData'](groupName, pointName, data);
};
//------------------------------------------------------------------------------
//                          Export (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getPNG = function(opt_width, opt_height) {
    return this.flashObject['GetPngScreen'](opt_width, opt_height);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getPDF = function(opt_width, opt_height) {
    return this.flashObject['GetPdfScreen'](opt_width, opt_height);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.printChart = function() {
    this.flashObject['PrintChart']();
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.saveAsImage = function(opt_width, opt_height) {
    this.flashObject['SaveAsImage'](opt_width, opt_height);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.saveAsPDF = function(opt_width, opt_height) {
    this.flashObject['SaveAsPDF'](opt_width, opt_height);
};
//------------------------------------------------------------------------------
//                          Scroll (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.scrollXTo = function(value) {
    if (this.flashObject != null && this.flashObject['ScrollXTo'] != null)
        this.flashObject['ScrollXTo'](value);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.scrollYTo = function(value) {
    if (this.flashObject != null && this.flashObject['ScrollYTo'] != null)
        this.flashObject['ScrollYTo'](value);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.scrollTo = function(xValue, yValue) {
    if (this.flashObject != null && this.flashObject['ScrollTo'] != null)
        this.flashObject['ScrollTo'](xValue, yValue);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.viewScrollXTo = function(viewId, value) {
    if (this.flashObject != null && this.flashObject['ViewScrollXTo'] != null)
        this.flashObject['ViewScrollXTo'](viewId, value);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.viewScrollYTo = function(viewId, value) {
    if (this.flashObject != null && this.flashObject['ViewScrollYTo'] != null)
        this.flashObject['ViewScrollYTo'](viewId, value);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.viewScrollTo = function(viewId, xValue, yValue) {
    if (this.flashObject != null && this.flashObject['ViewScrollTo'] != null)
        this.flashObject['ViewScrollTo'](viewId, xValue, yValue);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getXScrollInfo = function() {
    if (this.flashObject != null && this.flashObject['GetXScrollInfo'] != null)
        return this.flashObject['GetXScrollInfo']();
    return null;
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getYScrollInfo = function() {
    if (this.flashObject != null && this.flashObject['GetYScrollInfo'] != null)
        return this.flashObject['GetYScrollInfo']();
    return null;
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getViewXScrollInfo = function(viewId) {
    if (this.flashObject != null && this.flashObject['GetViewXScrollInfo'] != null)
        return this.flashObject['GetViewXScrollInfo'](viewId);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.getViewYScrollInfo = function(viewId) {
    if (this.flashObject != null && this.flashObject['GetViewYScrollInfo'] != null)
        return this.flashObject['GetViewYScrollInfo'](viewId);
};
//------------------------------------------------------------------------------
//                          Zoom (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setXZoom = function(settings) {
    if (this.flashObject != null && this.flashObject['SetXZoom'] != null)
        this.flashObject['SetXZoom'](settings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setYZoom = function(settings) {
    if (this.flashObject != null && this.flashObject['SetYZoom'] != null)
        this.flashObject['SetYZoom'](settings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setZoom = function(xSettings, ySettings) {
    if (this.flashObject != null && this.flashObject['SetZoom'] != null)
        this.flashObject['SetZoom'](xSettings, ySettings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setViewXZoom = function(viewId, settings) {
    if (this.flashObject != null && this.flashObject['SetViewXZoom'] != null)
        this.flashObject['SetViewXZoom'](viewId, settings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setViewYZoom = function(viewId, settings) {
    if (this.flashObject != null && this.flashObject['SetViewYZoom'] != null)
        this.flashObject['SetViewYZoom'](viewId, settings);
};
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.setViewZoom = function(viewId, xSettings, ySettings) {
    if (this.flashObject != null && this.flashObject['SetViewZoom'] != null)
        this.flashObject['SetViewZoom'](viewId, xSettings, ySettings);
};
//------------------------------------------------------------------------------
//                          Animation (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.flash.FlashRenderer.prototype.animate = function() {
    if (this.flashObject != null && this.flashObject['RepeatAnimation'] != null)
        this.flashObject['RepeatAnimation']();
};
//------------------------------------------------------------------------------
//                          Dispose
//------------------------------------------------------------------------------
/**@private*/
anychart.render.flash.FlashRenderer.prototype.removeFlashObject_ = function() {
    if (this.flashObject) {
        if (this.flashObject['Dispose']) this.flashObject['Dispose']();
        this.disposeFlashObject_(this.flashObject, this.id_);
        if (this.enableFirefoxPrintPreviewFix) this.disposeFFPrintFixObjects_();
    }
    this.flashObject = null;
    this.anychartBase_['flashObject'] = this.flashObject;
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.disposeFlashObject_ = function(obj, id) {
    if (obj && obj['nodeName'] == 'OBJECT') {
        if (AnyChart.platform.isIE && AnyChart.platform.isWin) {
            obj['style']['display'] = 'none';
            (function() {
                if (obj['readyState'] == 4) {
                    if (AnyChart.platform.needFormFix && id)
                        this.disposeFlashObjectInIE(window[id]);

                    AnyChart.disposeFlashObjectInIE(obj);
                }
                else {
                    setTimeout(arguments.callee, 10);
                }
            })();
        } else obj['parentNode']['removeChild'](obj);

    }
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.disposeFlashObjectInIE = function(obj) {
    for (var j in obj)
        if (typeof obj[j] == 'function') obj[j] = null;

    if (obj['parentNode']) obj['parentNode']['removeChild'](obj);
};
//------------------------------------------------------------------------------
//                          FF print fix
//------------------------------------------------------------------------------
/**@private*/
anychart.render.flash.FlashRenderer.prototype.createFFPrintFixObjects_ = function() {
    if (!this.enableFirefoxPrintPreviewFix || !this.flashObjectContainer_) return;

    var imgData = this.getPNG();
    if (!imgData || imgData.length == 0) return;

    var targetId = this.flashObjectContainer_.getAttribute('id');
    if (!targetId) {
        targetId = '__chart_generated_container__' + this.id_;
        this.flashObjectContainer_.setAttribute('id', targetId);
    }

    this.ffPrintFix_.fix(
            this,
            this.flashObjectContainer_,
            imgData,
            this.width_ + '',
            this.height_ + '',
            this.flashObjectContainer_['nodeName'],
            targetId);
};
/**@private*/
anychart.render.flash.FlashRenderer.prototype.disposeFFPrintFixObjects_ = function() {
    if (this.enableFirefoxPrintPreviewFix || this.flashObjectContainer_)
        this.ffPrintFix_.dispose();
};
//------------------------------------------------------------------------------
//
//                          FFPrintFix class
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.render.flash.FFPrintFix = function() {
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.render.flash.FlashRenderer.prototype.ffPrintScreenStyle_ = null;
/**
 * @private
 * @type {String}
 */
anychart.render.flash.FlashRenderer.prototype.ffPrintStyle_ = null;
/**
 * @private
 * @type {Object}
 */
anychart.render.flash.FlashRenderer.prototype.ffPrintFixImg_ = null;
//------------------------------------------------------------------------------
//                          Fix creation
//------------------------------------------------------------------------------
/**
 * @param targetChart
 * @param targetNode
 * @param pngData
 * @param w
 * @param h
 * @param targetNodeName
 * @param targetId
 */
anychart.render.flash.FFPrintFix.prototype.fix = function(targetChart, targetNode, pngData, w, h, targetNodeName, targetId) {
    var head = document.getElementsByTagName('head');
    head = (head.length > 0) ? head[0] : null;
    if (!head) return;

    this.ffPrintScreenStyle_ = this.createDisplayStyle_(head, w, h, targetNodeName, targetId);
    this.ffPrintStyle_ = this.createPrintStyle_(head, w, h, targetNodeName, targetId);
    this.ffPrintFixImg_ = this.createImage_(targetNode, pngData);
};
/**
 * @private
 * @param head
 * @param w
 * @param h
 * @param targetNodeName
 * @param targetId
 */
anychart.render.flash.FFPrintFix.prototype.createDisplayStyle_ = function(head, w, h, targetNodeName, targetId) {
    //crete style node
    var style = document.createElement('style');
    style.setAttribute('type', 'text/css');
    style.setAttribute('media', 'screen');

    //write style.
    var objDescriptor = targetNodeName + '#' + targetId;
    var imgDescriptor = objDescriptor + ' img';
    var objRule = ' object { width:' + w + '; height:' + h + ';padding:0; margin:0; }\n';
    var imgRule = ' { display: none; }';

    style.appendChild(document.createTextNode(objDescriptor + objRule));
    style.appendChild(document.createTextNode(imgDescriptor + imgRule));

    //add style to head
    return head.appendChild(style);
};
/**
 * @private
 * @param head
 * @param w
 * @param h
 * @param targetNodeName
 * @param targetId
 */
anychart.render.flash.FFPrintFix.prototype.createPrintStyle_ = function(head, w, h, targetNodeName, targetId) {
    //crete style node
    var style = document.createElement('style');
    style.setAttribute('type', 'text/css');
    style.setAttribute('media', 'print');

    //write style.
    var objDescriptor = targetNodeName + '#' + targetId;
    var imgDescriptor = objDescriptor + ' img';
    var objRule = ' object { display: none; }\n';
    var imgRule = ' { display: block; width: ' + w + '; height: ' + h + '; }';

    style.appendChild(document.createTextNode(objDescriptor + objRule));
    style.appendChild(document.createTextNode(imgDescriptor + imgRule));

    //add style to head
    return head.appendChild(style);
};
/**
 * @private
 * @param targetNode
 * @param pngData
 */
anychart.render.flash.FFPrintFix.prototype.createImage_ = function(targetNode, pngData) {
    var img = document.createElement('img');
    img = targetNode.appendChild(img);
    img['src'] = 'data:image/png;base64,' + pngData;

    return img;
};

anychart.render.flash.FFPrintFix.prototype.dispose = function() {
    if (this.ffPrintFixImg_ && this.ffPrintFixImg_['parentNode']) {
        this.ffPrintFixImg_['parentNode']['removeChild'](this.ffPrintFixImg_);
        this.ffPrintFixImg = null;
    }

    if (this.ffPrintScreenStyle_) {
        this.ffPrintScreenStyle_['parentNode']['removeChild'](this.ffPrintScreenStyle_);
        this.ffPrintScreenStyle_ = null;
    }

    if (this.ffPrintStyle_) {
        this.ffPrintStyle_['parentNode']['removeChild'](this.ffPrintStyle_);
        this.ffPrintStyle_ = null;
    }
};


