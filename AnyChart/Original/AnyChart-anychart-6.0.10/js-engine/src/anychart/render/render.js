/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains interfaces:
 * <ul>
 *  <li>@class {anychart.render.IChartRenderer};.</li>
 * <ul>
 */
goog.provide('anychart.render');

/**
 * @interface
 */
anychart.render.IChartRenderer = function() { };
//------------------------------------------------------------------------------
//                          Internal
//------------------------------------------------------------------------------
/**
 * @param {anychart.AnyChart} chart Chart.
 */
anychart.render.IChartRenderer.prototype.init = function(chart) {};
anychart.render.IChartRenderer.prototype.onBeforeChartCreate = function() {};
anychart.render.IChartRenderer.prototype.onBeforeChartDraw = function() {};
//------------------------------------------------------------------------------
//                          Embed
//------------------------------------------------------------------------------
/**
 * @return {Boolean} Can write or not.
 */
anychart.render.IChartRenderer.prototype.canWrite = function() {};
anychart.render.IChartRenderer.prototype.write = function(opt_target) {};
anychart.render.IChartRenderer.prototype.canSetConfigAfterWrite = function() {};
anychart.render.IChartRenderer.prototype.updatePrintForFirefox = function() {};
anychart.render.IChartRenderer.prototype.setSize = function(width, height) {};
anychart.render.IChartRenderer.prototype.remove = function() {};
anychart.render.IChartRenderer.prototype.clear = function() {};
anychart.render.IChartRenderer.prototype.refresh = function() {};
anychart.render.IChartRenderer.prototype.updateData = function(path, data) {};
anychart.render.IChartRenderer.prototype.setLoading = function(opt_message) {};
anychart.render.IChartRenderer.prototype.getInformation = function() {};
//------------------------------------------------------------------------------
//                          Config setters
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.setXMLFile = function(path) {};
anychart.render.IChartRenderer.prototype.setXMLData = function(data) {};
anychart.render.IChartRenderer.prototype.setJSONData = function(data) {};
/**
 * @deprecated
 */
anychart.render.IChartRenderer.prototype.setXMLDataFromString =
        function(data) {};
/**
 * @deprecated
 */
anychart.render.IChartRenderer.prototype.setXMLDataFromURL = function(data) {};
anychart.render.IChartRenderer.prototype.setViewXMLFile = function(viewId,
                                                                   url) {};
anychart.render.IChartRenderer.prototype.setViewData = function(viewId,
                                                                data) {};
//------------------------------------------------------------------------------
//                          Point
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.addPoint = function(seriesId,
                                                             pointData) {};
anychart.render.IChartRenderer.prototype.addPointAt = function(seriesId,
                                                               pointIndex,
                                                               pointData) {};
anychart.render.IChartRenderer.prototype.removePoint = function(seriesId,
                                                                pointId) {};
anychart.render.IChartRenderer.prototype.updatePoint = function(seriesId,
                                                                pointId,
                                                                pointData) {};
anychart.render.IChartRenderer.prototype.updatePointData = function(groupName,
                                                                    pointName,
                                                                    data) {};
anychart.render.IChartRenderer.prototype.highlightPoint =
        function(seriesId, pointId, highlight) {};
anychart.render.IChartRenderer.prototype.selectPoint = function(seriesId,
                                                                pointId,
                                                                select) {};
anychart.render.IChartRenderer.prototype.updatePointWithAnimation = function(seriesId, pointId, newValue, animationSettings) {
};
anychart.render.IChartRenderer.prototype.startPointsAnimation = function() {
};
//------------------------------------------------------------------------------
//                          Series
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.addSeries = function(seriesData) {};
anychart.render.IChartRenderer.prototype.addSeriesAt = function(index,
                                                                seriesData) {};
anychart.render.IChartRenderer.prototype.removeSeries = function(seriesId) {};
anychart.render.IChartRenderer.prototype.updateSeries = function(seriesId,
                                                                 seriesData) {};
anychart.render.IChartRenderer.prototype.showSeries = function(seriesId,
                                                               seriesData) {};
anychart.render.IChartRenderer.prototype.highlightSeries =
        function(seriesId, highlight) {};
anychart.render.IChartRenderer.prototype.highlightCategory =
        function(categoryName, highlight) {};
//------------------------------------------------------------------------------
//                          Axes
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.showAxisMarker =
        function(markerId, seriesDate) {};
//------------------------------------------------------------------------------
//                          Custom attributes
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.setPlotCustomAttribute =
        function(attributeName, attributeValue) {};
//------------------------------------------------------------------------------
//                          View (ignored)
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.view_updatePointData =
        function(viewId, groupName, pointName, data) {};
anychart.render.IChartRenderer.prototype.view_setPlotCustomAttribute =
        function(viewId, attributeName, attributeValue) {};
anychart.render.IChartRenderer.prototype.view_addSeries =
        function(viewId, seriesData) {};
anychart.render.IChartRenderer.prototype.view_removeSeries =
        function(viewId, seriesId) {};
anychart.render.IChartRenderer.prototype.view_addSeriesAt =
        function(viewId, index, seriesData) {};
anychart.render.IChartRenderer.prototype.view_updateSeries =
        function(viewId, seriesId, seriesData) {};
anychart.render.IChartRenderer.prototype.view_showSeries =
        function(viewId, seriesId, seriesData) {};
anychart.render.IChartRenderer.prototype.view_addPoint =
        function(viewId, seriesId, pointData) {};
anychart.render.IChartRenderer.prototype.view_addPointAt =
        function(viewId, seriesId, pointIndex, pointData) {};
anychart.render.IChartRenderer.prototype.view_removePoint =
        function(viewId, seriesId, pointId) {};
anychart.render.IChartRenderer.prototype.view_updatePoint =
        function(viewId, seriesId, pointId, pointData) {};
anychart.render.IChartRenderer.prototype.view_clear = function(viewId) {};
anychart.render.IChartRenderer.prototype.view_refresh = function(viewId) {};
anychart.render.IChartRenderer.prototype.view_highlightSeries =
        function(viewId, seriesId, highlight) {};
anychart.render.IChartRenderer.prototype.view_highlightPoint =
        function(viewId, seriesId, pointId, highlight) {};
anychart.render.IChartRenderer.prototype.view_highlightCategory =
        function(viewId, categoryName, highlight) {};
anychart.render.IChartRenderer.prototype.view_selectPoint =
        function(viewId, seriesId, pointId, select) {};
anychart.render.IChartRenderer.prototype.updateViewPointData =
        function(viewId, groupName, pointName, data) {};
//------------------------------------------------------------------------------
//                          Export (ignored)
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.getPNG = function(opt_width, opt_height) {};
anychart.render.IChartRenderer.prototype.getPDF = function(opt_width, opt_height) {};

anychart.render.IChartRenderer.prototype.printChart = function() {};

anychart.render.IChartRenderer.prototype.saveAsImage = function(opt_width, opt_height) {};
anychart.render.IChartRenderer.prototype.saveAsPDF = function(opt_width, opt_height) {};
//------------------------------------------------------------------------------
//                          Scroll (ignored)
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.scrollXTo = function(value) {};
anychart.render.IChartRenderer.prototype.scrollYTo = function(value) {};
anychart.render.IChartRenderer.prototype.scrollTo = function(xValue,
                                                             yValue) {};
anychart.render.IChartRenderer.prototype.viewScrollXTo = function(viewId,
                                                                  value) {};
anychart.render.IChartRenderer.prototype.viewScrollYTo = function(viewId,
                                                                  value) {};
anychart.render.IChartRenderer.prototype.viewScrollTo = function(viewId,
                                                                 xValue,
                                                                 yValue) {};
anychart.render.IChartRenderer.prototype.getXScrollInfo = function() {};
anychart.render.IChartRenderer.prototype.getYScrollInfo = function() {};
anychart.render.IChartRenderer.prototype.getViewXScrollInfo =
        function(viewId) {};
anychart.render.IChartRenderer.prototype.getViewYScrollInfo =
        function(viewId) {};
//------------------------------------------------------------------------------
//                          Zoom (ignored)
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.setXZoom = function(settings) {};
anychart.render.IChartRenderer.prototype.setYZoom = function(settings) {};
anychart.render.IChartRenderer.prototype.setZoom = function(xSettings,
                                                            ySettings) {};
anychart.render.IChartRenderer.prototype.setViewXZoom = function(viewId,
                                                                 settings) {};
anychart.render.IChartRenderer.prototype.setViewYZoom = function(viewId,
                                                                 settings) {};
anychart.render.IChartRenderer.prototype.setViewZoom = function(viewId,
                                                                xSettings,
                                                                ySettings) {};
//------------------------------------------------------------------------------
//                          Animation (ignored)
//------------------------------------------------------------------------------
anychart.render.IChartRenderer.prototype.animate = function() {};
