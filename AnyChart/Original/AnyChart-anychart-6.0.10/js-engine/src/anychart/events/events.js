/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.events.EngineEvent};</li>
 *  <li>@class {anychart.events.PointEvent};</li>
 *  <li>@class {anychart.events.MultiplePointsEvent}.</li>
 * <ul>
 */
goog.provide('anychart.events');
goog.require('goog.events');
//------------------------------------------------------------------------------
//
//                          EngineEvents class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 * @param {String} type Type of event.
 */
anychart.events.EngineEvent = function(type) {
    goog.events.Event.call(this, type);
};
goog.inherits(anychart.events.EngineEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                          Event types
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.events.EngineEvent.ANYCHART_CREATE = 'anychartCreate';
/**
 * @type {String}
 */
anychart.events.EngineEvent.ANYCHART_DRAW = 'anychartDraw';
/**
 * @type {String}
 */
anychart.events.EngineEvent.ANYCHART_RENDER = 'anychartRender';
/**
 * @type {String}
 */
anychart.events.EngineEvent.ANYCHART_REFRESH = 'anychartRefresh';
//------------------------------------------------------------------------------
//                  DashboardEvent class.
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 * @param {String} type Type of event.
 * @param {String} viewId Dashboard view id.
 */
anychart.events.DashboardEvent = function(type, viewId) {
    goog.events.Event.call(this, type);
    this.viewId_ = viewId;
};
goog.inherits(anychart.events.DashboardEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                          Event types.
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.events.DashboardEvent.DASHBOARD_VIEW_RENDER = 'dashboardViewRender';
/**
 * @type {String}
 */
anychart.events.DashboardEvent.DASHBOARD_VIEW_DRAW = 'dashboardViewDraw';
/**
 * @type {String}
 */
anychart.events.DashboardEvent.DASHBOARD_VIEW_REFRESH = 'dashboardViewRefresh';
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.events.DashboardEvent.prototype.viewId_ = null;
/**
 * @return {String}
 */
anychart.events.DashboardEvent.prototype.getViewId = function() {
    return this.viewId_;
};
//------------------------------------------------------------------------------
//
//                          PointEvent class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 * @param {String} type Type of event.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point.
 * @param {Number} mouseX X coordinate of mouse.
 * @param {Number} mouseY Y coordinate of mouse.
 */
anychart.events.PointEvent = function(type, point, mouseX, mouseY) {
    goog.events.Event.call(this, type);
    this.point_ = point;
    this.mouseX_ = mouseX;
    this.mouseY_ = mouseY;
};
goog.inherits(anychart.events.PointEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                          Event types
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_CLICK = 'pointClick';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_MOUSE_OVER = 'pointMouseOver';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_MOUSE_OUT = 'pointMouseOut';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_MOUSE_DOWN = 'pointMouseDown';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_MOUSE_UP = 'pointMouseUp';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_SELECT = 'pointSelect';
/**
 * @type {String}
 */
anychart.events.PointEvent.POINT_DESELECT = 'pointDeselect';
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Getter for type.
 * @return {String} Type of event.
 */
anychart.events.PointEvent.prototype.getType = function() {
    return this.type;
};

/**
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.events.PointEvent.prototype.point_ = null;

/**
 * Getter for point.
 * @return {anychart.plots.seriesPlot.data.BasePoint} Point.
 */
anychart.events.PointEvent.prototype.getPoint = function() {
    return this.point_;
};

/**
 * @private
 * @type {Number}
 */
anychart.events.PointEvent.prototype.mouseX_ = null;

/**
 * Getter for mouseX.
 * @return {Number} X coordinate of mouse.
 */
anychart.events.PointEvent.prototype.getMouseX = function() {
    return this.mouseX_;
};

/**
 * @private
 * @type {Number}
 */
anychart.events.PointEvent.prototype.mouseY_ = null;

/**
 * Getter for mouseY.
 * @return {Number} Y coordinate of mouse.
 */
anychart.events.PointEvent.prototype.getMouseY = function() {
    return this.mouseY_;
};
//------------------------------------------------------------------------------
//
//                          MultiplePointsEvent class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 * @param {String} type Type of event.
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} points Points.
 */
anychart.events.MultiplePointsEvent = function(type, points) {
    goog.events.Event.call(this, type);
    this.points_ = points;
};
goog.inherits(anychart.events.MultiplePointsEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                          Event types
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.events.MultiplePointsEvent.
        MULTIPLE_POINTS_SELECT = 'multiplePointsSelect';
/**
 * @type {String}
 */
anychart.events.MultiplePointsEvent.
        MULTIPLE_POINTS_DESELECT = 'multiplePointsDeselect';
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Getter for type.
 * @return {String} Type of event.
 */
anychart.events.MultiplePointsEvent.prototype.getType = function() {
    return this.type;
};

/**
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.events.MultiplePointsEvent.prototype.points_ = null;

/**
 * Getter for points.
 * @return {Array.<anychart.plots.seriesPlot.data.BasePoint>} Points.
 */
anychart.events.MultiplePointsEvent.prototype.getPoints = function() {
    return this.points_;
};

/**
 * Returns number of points.
 * @return {Number} Number of points.
 */
anychart.events.MultiplePointsEvent.prototype.getNumPoints = function() {
    return this.points_.length;
};

/**
 * Return point by index.
 * @param {int} index Index.
 * @return {anychart.plots.seriesPlot.data.BasePoint} Point.
 */
anychart.events.MultiplePointsEvent.prototype.getPointAt = function(index) {
    return this.points_[index];
};
//------------------------------------------------------------------------------
//
//                          DragEvent class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 */
anychart.events.DragEvent = function(type) {
    goog.events.Event.call(this, type);
};
goog.inherits(anychart.events.DragEvent, goog.events.Event);

anychart.events.DragEvent.ON_DRAG_PROCESS = 'onDragProcess';