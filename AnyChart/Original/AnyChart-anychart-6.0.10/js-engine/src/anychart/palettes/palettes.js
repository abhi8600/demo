/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.palettes.BasePalette}</li>
 *  <li>@class {anychart.palettes.PalettesCollection}</li>
 *  <li>@class {anychart.palettes.ColorPalette}</li>
 *  <li>@class {anychart.palettes.MarkerTypePalette}</li>
 *  <li>@class {anychart.palettes.HatchTypePalette}.</li>
 * <ul>
 */
goog.provide('anychart.palettes');
goog.require('anychart.elements.marker');
goog.require('anychart.utils');
goog.require('anychart.visual.hatchFill');

//------------------------------------------------------------------------------
//
//                          BasePalette class
//
//------------------------------------------------------------------------------
/**
 * Base class for all palettes.
 * @constructor
 * <p>
 *  Note: It's a base class for all palettes, you should not create instance of
 * this class.
 * </p>
 * <p>
 *  var palette = new anychart.palettes.HatchTypePalette();
 *  palette.deserialize(data);
 *  var item = palette.getItemAt(0);
 * </p>
 */
anychart.palettes.BasePalette = function() {
};

/**
 * Palette items.
 * @protected
 * @type {Array.<object>}
 */
anychart.palettes.BasePalette.prototype.items = null;

/**
 * Get palette item by index, if index more then items count, return modulo.
 * @param {int} index Palette item index.
 * @return {object} palette item.
 */
anychart.palettes.BasePalette.prototype.getItemAt = function(index) {
    if (index >= this.items.length)
        index = index % this.items.length;
    return this.items[index];
};

/**
 * Base deserialize method.
 * @param {object} data Palette configuration.
 */
anychart.palettes.BasePalette.prototype.deserialize = function(data) {
    this.items = [];
};

/**
 * Create and return base palette clone.
 * @param {anychart.palettes.BasePalette}} opt_palette Target object to clone.
 * @return {anychart.palettes.BasePalette} cloned palette.
 */
anychart.palettes.BasePalette.prototype.clone = function(opt_palette) {
    if (opt_palette == undefined)
        opt_palette = new anychart.palettes.BasePalette();
    opt_palette.items = this.items;
    return opt_palette;
};
//------------------------------------------------------------------------------
//
//                          PalettesCollection class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.palettes.PalettesCollection = function() {
};

/**
 * @param {String} type Palette type.
 * @param {String} name Palette name.
 * @return {Object} Palette configuration.
 */
anychart.palettes.PalettesCollection.prototype.getPalette = function(type,
                                                                     name) {
    if (!this.config_) return null;
    name = anychart.utils.deserialization.getLString(name);
    var palettesList = anychart.utils.deserialization.
                           getPropArray(this.config_, type);
    var len = palettesList.length;
    for (var i = 0; i < len; i++) {
        if (anychart.utils.deserialization.hasProp(palettesList[i], 'name') &&
                anychart.utils.deserialization.getLStringProp(
                        palettesList[i], 'name') == name)
            return palettesList[i];
    }
    return null;
};

/**
 * @private
 * @type {Object}
 */
anychart.palettes.PalettesCollection.prototype.config_ = null;

/**
 * @param {Object} config Palettes list config.
 */
anychart.palettes.PalettesCollection.prototype.deserialize = function(config) {
    this.config_ = config;
};

//------------------------------------------------------------------------------
//
//                          ColorPalette class
//
//------------------------------------------------------------------------------
/**
 * Color palette class.
 * @constructor
 * @extends {anychart.palettes.BasePalette}
 * <p>
 * Sample:
 *  <code>
 *      var palette = new anychart.palette.ColorPalette();
 *      palette.deserialize(data);
 *      var realPalette = palette.checkAuto(10, false);
 *  </code>
 * </p>
 */
anychart.palettes.ColorPalette = function() {
    anychart.palettes.BasePalette.apply(this);
};

//inheritance
goog.inherits(anychart.palettes.ColorPalette, anychart.palettes.BasePalette);

/**
 * Describes possible color palette types.
 * @enum {int}
 */
anychart.palettes.ColorPalette.ColorPaletteType = {
    DISTINCT_COLORS: 0,
    COLOR_RANGE: 1
};

/**
 * Deserialize palette type from config.
 * @param {*} type palette type from config.
 * @return {anychart.palettes.ColorPalette.ColorPaletteType} type.
 */
anychart.palettes.ColorPalette.ColorPaletteType.deserialize = function(type) {
    var paletteType = anychart.palettes.ColorPalette.ColorPaletteType;
    switch (type) {
        case 'colorrange':
            return paletteType.COLOR_RANGE;
        default:
            return paletteType.DISTINCT_COLORS;
    }
};

/**
 * Gradient instance.
 * @private
 * @type {anychart.visual.gradient.Gradient}
 */
anychart.palettes.ColorPalette.prototype.gradient_ = null;

/**
 * Flag, define is auto gradient.
 * @private
 * @type {boolean}
 */
anychart.palettes.ColorPalette.prototype.isAutoGradient_ = false;

/**
 * Color palette name.
 * @private
 * @type {string}
 */
anychart.palettes.ColorPalette.prototype.name_ = '';

/**
 * Color palette type.
 * @private
 * @type {anychart.palettes.ColorPalette.ColorPaletteType}
 */
anychart.palettes.ColorPalette.prototype.type_ =
    anychart.palettes.ColorPalette.ColorPaletteType.DISTINCT_COLORS;

/**
 * Color palette count, can be 'auto' or int value.
 * @private
 * @type {int || string}
 */
anychart.palettes.ColorPalette.prototype.colorCount_ = 'auto';

/**
 * Define is color count auto
 * @private
 * @type {Boolean}
 */
anychart.palettes.ColorPalette.prototype.isColorCountAuto_ = false;


/**
 * Deserialize color palette settings.
 * @param {object} data JSON object with color palette settings.
 */
anychart.palettes.ColorPalette.prototype.deserialize = function(data) {
    //aliases
    var des = anychart.utils.deserialization;
    //super class deserialization
    goog.base(this, 'deserialize', data);

    if (des.hasProp(data, 'name'))
        this.name_ = des.getLStringProp(data, 'name');

    if (des.hasProp(data, 'color_count')) {
        this.colorCount_ = des.getProp(data, 'color_count');
        if (des.isAuto(this.colorCount_)) this.isColorCountAuto_ = true;
        else this.colorCount_ = des.getNum(this.colorCount_);
    }


    var type;
    if (des.hasProp(data, 'type'))
         this.type_ = anychart.palettes.ColorPalette.ColorPaletteType.
                         deserialize(
                             des.getLStringProp(data, 'type'));


    switch (this.type_) {
        case anychart.palettes.ColorPalette.ColorPaletteType.COLOR_RANGE:
            this.isAutoGradient_ = true;
            if (this.colorCount_ && !this.isColorCountAuto_) {
                var grad = new anychart.visual.gradient.Gradient(1);
                grad.deserialize(des.getProp(data, 'gradient'));
                this.initRangedPalette_(grad, this.colorCount_);
                this.isAutoGradient_ = false;
            }
            this.gradient_ = new anychart.visual.gradient.Gradient(1);
            this.gradient_.deserialize(
                des.getProp(data, 'gradient'));
            break;
        case anychart.palettes.ColorPalette.ColorPaletteType.DISTINCT_COLORS:
        default:
            var children = des.getPropArray(data, 'item');
            var itemCount = children.length;
            for (var i = 0; i < itemCount; i++)
                this.items.push(
                    des.getColorProp(children[i], 'color'));
            break;
    }
};

/**
 * Check to auto gradient settings and return real ColorPalette instance.
 * @param {int} itemsCount Palette items count.
 * @param {boolean} opt_ignoreDefaults Flag, ignore default settings,
 * if true, return ColorPalette instance with specified itemsCount.
 * @return {anychart.palettes.ColorPalette} Color palette.
 */
anychart.palettes.ColorPalette.prototype.checkAuto = function(itemsCount,
                                                           opt_ignoreDefaults) {
    var res = this;
    if ((this.isAutoGradient_ && !opt_ignoreDefaults) ||
        (this.gradient_ && opt_ignoreDefaults)) {
        res = new anychart.palettes.ColorPalette();
        res.items = [];
        res.initRangedPalette_(this.gradient_, itemsCount);
    }
    return res;
};

/**
 * Initialize ranged palette, depending on count define to blend color or not.
 * @private
 * @param {anychart.visual.gradient.Gradient} grad Gradient instance.
 * @param {int} count Palette items count.
 */
anychart.palettes.ColorPalette.prototype.initRangedPalette_ = function(grad,
                                                                       count) {
    var i;
    if (count < 2) {
        for (i = 0; i < count; i++) {
            if (grad.getKeyAt(i))
                this.items.push(grad.getKeyAt(i).getColor());
        }
    }
    for (i = 0; i < count; i++)
        this.items.push(this.getColorItem_(grad, i / (count - 1)));
};

/**
 * Depending on fPlace, bled gradient colors.
 * @private
 * @param {anychart.visual.gradient.Gradient} grad Gradient instance.
 * @param {int} fPlace Ratio palette items index to palette items count.
 * @return {anychart.visual.color.Color} Color.
 */
anychart.palettes.ColorPalette.prototype.getColorItem_ = function(grad,
                                                                  fPlace) {
    var idx_start = -1;
    var idx_end = -1;
    var place_end = 10000000;
    var place_start = -1;
    var keyCount = grad.keyCount();
    for (var i = 0; i < keyCount; i++) {
        var key = grad.getKeyAt(i);
        var rt = key.getBytePosition() / 255.0;
        if (rt == fPlace) return key.getColor();

        if (rt < fPlace) {
            if (rt > place_start) {
                idx_start = i;
                place_start = rt;
            }
        }

        if (rt > fPlace) {
            if (rt < place_end) {
                idx_end = i;
                place_end = rt;
            }
        }
    }

    if (idx_start == -1) {
        if (idx_end == -1)
            return anychart.utils.deserialization.getColor(null, 1);
        else
            return grad.getKeyAt(idx_end).getColor();
    }else if (idx_end == -1)
        return grad.getKeyAt(idx_start).getColor();

    if (place_start == place_end) return grad.getKeyAt(idx_start).getColor();

    var startKey = grad.getKeyAt(idx_start);
    var srt = grad.getKeyAt(idx_start).getBytePosition() / 255.0;
    var endKey = grad.getKeyAt(idx_end);
    var ert = grad.getKeyAt(idx_end).getBytePosition() / 255.0;
    var pos = 1 - (fPlace - srt) / (ert - srt);
    var blend = new anychart.visual.color.colorOperations.Blend();
    return blend.blendColors(startKey.getColor().getColor(),
            endKey.getColor().getColor(), pos);
};

/**
 * Return palette clone.
 * @param {*} opt_targetPalette Target to clone.
 * @return {anychart.palettes.ColorPalette} Cloned palette.
 */
anychart.palettes.ColorPalette.prototype.clone = function(opt_targetPalette) {
    if (opt_targetPalette == undefined)
        opt_targetPalette = new anychart.palettes.ColorPalette();

    goog.base(this, 'clone', opt_targetPalette);

    opt_targetPalette.name_ = this.name_;
    opt_targetPalette.colorCount_ = this.colorCount_;
    opt_targetPalette.gradient_ = this.gradient_;
    opt_targetPalette.type_ = this.type_;
    opt_targetPalette.isAutoGradient_ = this.isAutoGradient_;
    return opt_targetPalette;
};

//------------------------------------------------------------------------------
//
//                          HatchTypePalette class
//
//------------------------------------------------------------------------------

/**
 * HatchType palette class.
 * @constructor
 * @extends {anychart.palettes.BasePalette}
 * <p>
 *      var palette = new anychart.palettes.HatchTypePalette();
 *      palette.deserialize(data);
 *      palette.getItemAt(0);
 * </p>
 */
anychart.palettes.HatchTypePalette = function() {
    anychart.palettes.BasePalette.apply(this);
};

//inherits
goog.inherits(anychart.palettes.HatchTypePalette,
              anychart.palettes.BasePalette);

/**
 * Deserialize Hatch type palette settings.
 * @param {object} data JSON object with hatch type palette settings.
 */
anychart.palettes.HatchTypePalette.prototype.deserialize = function(data) {
    //aliases
    var deserialization = anychart.utils.deserialization;
    goog.base(this, 'deserialize', data);
    var item = deserialization.getPropArray(data, 'item');
    var itemCount = item.length;
    for (var i = 0; i < itemCount; i++) {
        this.items.push(
            anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(
                deserialization.getLStringProp(item[i], 'type')));
    }
};

/**
 * Create and return hatch type palette clone.
 * @param {anychart.palettes.HatchTypePalette} opt_palette Target palette.
 * @return {anychart.palettes.HatchTypePalette} Cloned palette.
 */
anychart.palettes.HatchTypePalette.prototype.clone = function(opt_palette) {
    if (opt_palette == undefined)
        opt_palette = new anychart.palettes.HatchTypePalette();
    goog.base(this, 'clone', opt_palette);
    return opt_palette;
};

//------------------------------------------------------------------------------
//
//                          MarkerTypePalette class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.palettes.MarkerTypePalette = function() {
    anychart.palettes.BasePalette.apply(this);
};

//inherits
goog.inherits(anychart.palettes.MarkerTypePalette,
              anychart.palettes.BasePalette);

/** @inheritDoc */
anychart.palettes.MarkerTypePalette.prototype.deserialize = function(data) {
    //aliases
    var deserialization = anychart.utils.deserialization;
    goog.base(this, 'deserialize', data);
    var item = deserialization.getPropArray(data, 'item');
    var itemCount = item.length;
    for (var i = 0; i < itemCount; i++) {
        this.items.push(
            anychart.elements.marker.MarkerType.deserialize(
                deserialization.getLStringProp(item[i], 'type')));
    }
};

/** @inheritDoc */
anychart.palettes.MarkerTypePalette.prototype.clone = function(opt_palette) {
    if (opt_palette == undefined)
        opt_palette = new anychart.palettes.MarkerTypePalette();
    goog.base(this, 'clone', opt_palette);
    return opt_palette;
};
