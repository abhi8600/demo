/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.layouters.ControlsCollection};</li>
 * <ul>.
 */
goog.provide('anychart.controls.layouters');
goog.require('anychart.controls.controlsBase');
//------------------------------------------------------------------------------
//
//                           ControlsCollection class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.controls.layouters.ControlsCollection = function() {};

/**
 * Adding control.
 * @param {anychart.controls.controlsBase.Control} control Control.
 */
anychart.controls.layouters.ControlsCollection.prototype.addControl =
    function(control) {
        goog.abstractMethod();
    };

/**
 * Finalize layout.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 */
anychart.controls.layouters.ControlsCollection.prototype.finalizeLayout =
    function(plotBounds, chartBounds) {
        goog.abstractMethod();
    };
