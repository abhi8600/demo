/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonState}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButton}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonWithArrow}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonArrowBackground}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonDown}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonLeft}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonRight}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarButtonUp}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarThumb}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarThumbV}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarThumbH}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarControl}</li>
 *  <li>@class {anychart.controls.scrollBar.ScrollBarVertical}</li>
 * <ul>
 */
goog.provide('anychart.controls.scrollBar');

goog.require('anychart.svg');
goog.require('anychart.visual.background');
goog.require('goog.events');
//------------------------------------------------------------------------------
//
//                       ScrollBarButtonState class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.controls.scrollBar.ScrollBarButtonState = {
    NORMAL : 0,
    HOVER : 1,
    PUSHED : 2
};
//------------------------------------------------------------------------------
//
//                          ScrollBarEvent class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 */
anychart.controls.scrollBar.ScrollBarEvent = function(type, value) {
    goog.events.Event.call(this, type);
    this.value_ = value;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                          Event types.
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.controls.scrollBar.ScrollBarEvent.CHANGE = 'ScrollBarValueChange';
//------------------------------------------------------------------------------
//                         Event properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarEvent.prototype.value_ = null;
/**
 * @return {Number}
 */
anychart.controls.scrollBar.ScrollBarEvent.prototype.getValue = function() {
    return this.value_
};
//------------------------------------------------------------------------------
//
//                          ScrollBarButton class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 * @param {anychart.svg.SVGManager}
    */
anychart.controls.scrollBar.ScrollBarButton = function(svgManager) {
    anychart.svg.SVGDraggableSprite.call(this, svgManager);
    this.dx_ = 0;
    this.dy_ = 0;
    this.dw_ = 0;
    this.dh_ = 0;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButton,
    anychart.svg.SVGDraggableSprite);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.currentState_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.getBounds = function() {
    return this.bounds_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.isOver_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.isPushed_ = null;
//------------------------------------------------------------------------------
//                        Backgrounds.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.normalBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.hoverBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.pushedBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.backgroundSprite_ = null;
//------------------------------------------------------------------------------
//                          Position.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.dx_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.dy_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.dw_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.dh_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize scroll button background states.
 * @param {Object} normalData
 * @param {Object} hoverData
 * @param {Object} pushedData
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.deserialize = function(normalData, hoverData, pushedData) {
    var des = anychart.utils.deserialization;

    this.normalBackground_ = new anychart.visual.background.RectBackground();
    this.normalBackground_.deserialize(des.getProp(normalData, 'background'));

    this.hoverBackground_ = new anychart.visual.background.RectBackground();
    this.hoverBackground_.deserialize(des.getProp(hoverData, 'background'));

    this.pushedBackground_ = new anychart.visual.background.RectBackground();
    this.pushedBackground_.deserialize(des.getProp(pushedData, 'background'));
};
//------------------------------------------------------------------------------
//                       Initialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.initialize = function(bounds) {
    this.bounds_ = bounds.clone();

    //button events
    goog.events.listen(this.element_, goog.events.EventType.MOUSEOVER, this.mouseOverHandler_, false, this);
    goog.events.listen(this.element_, goog.events.EventType.MOUSEOUT, this.mouseOutHandler_, false, this);
    goog.events.listen(this.element_, goog.events.EventType.MOUSEDOWN, this.mouseDownHandler_, false, this);
    //svg events
    goog.events.listen(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.containerStopMove_, false, this);


    this.currentState_ = anychart.controls.scrollBar.ScrollBarButtonState.NORMAL;
    if (this.needCreateBackground_()) {
        var bg = this.getEnabledBackground_();
        this.backgroundSprite_ = bg.initialize(this.svgManager_);
        this.appendSprite(this.backgroundSprite_);
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Update scroll bar button.
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.draw = function() {
    var bg = this.getActualBackground_();
    if (bg && bg.isEnabled())
        bg.draw(
            this.backgroundSprite_,
            new anychart.utils.geom.Rectangle(
                0,
                0,
                this.bounds_.width,
                this.bounds_.height));
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Resize scroll bar button.
 * @param {anychart.utils.geom.Rectangle}
    */
anychart.controls.scrollBar.ScrollBarButton.prototype.resize = function(bounds) {
    this.setPosition(bounds.x + this.dx_, bounds.y + this.dy_);
    this.bounds_.width = bounds.width - this.dw_;
    this.bounds_.height = bounds.height - this.dh_;

    if (this.backgroundSprite_) {
        var bg = this.getActualBackground_();
        bg.resize(
            this.backgroundSprite_,
            new anychart.utils.geom.Rectangle(
                0,
                0,
                this.bounds_.width,
                this.bounds_.height));
    }
};
//------------------------------------------------------------------------------
//                      Background utils.
//------------------------------------------------------------------------------
/**
 * @private
 * @return {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.getActualBackground_ = function() {
    switch (this.currentState_) {
        case anychart.controls.scrollBar.ScrollBarButtonState.NORMAL:
            return this.normalBackground_;

        case anychart.controls.scrollBar.ScrollBarButtonState.PUSHED:
            return this.pushedBackground_;

        case anychart.controls.scrollBar.ScrollBarButtonState.HOVER:
            return this.hoverBackground_;
    }
    return null;
};
/**
 * Define, need create background sprite.
 * @private
 * @return {Boolean}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.needCreateBackground_ = function() {
    return  (this.normalBackground_ && this.normalBackground_.isEnabled()) ||
        (this.pushedBackground_ && this.pushedBackground_.isEnabled()) ||
        (this.hoverBackground_ && this.hoverBackground_.isEnabled());
};
/**
 * @private
 * @return {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.getEnabledBackground_ = function() {
    if (this.normalBackground_ && this.normalBackground_.isEnabled()) return this.normalBackground_;
    if (this.pushedBackground_ && this.pushedBackground_.isEnabled()) return this.pushedBackground_;
    if (this.hoverBackground_ && this.hoverBackground_.isEnabled()) return this.hoverBackground_;
    return null;
};
//------------------------------------------------------------------------------
//                      Events handlers.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.mouseOverHandler_ = function(event) {
    this.isOver_ = true;
    if (!this.isPushed_) {
        this.currentState_ = anychart.controls.scrollBar.ScrollBarButtonState.HOVER;
        this.draw();
    }
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.mouseOutHandler_ = function(event) {
    this.isOver_ = false;
    if (!this.isPushed_) {
        this.currentState_ = anychart.controls.scrollBar.ScrollBarButtonState.NORMAL;
        this.draw();
    }
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.mouseDownHandler_ = function(event) {
    this.currentState_ = anychart.controls.scrollBar.ScrollBarButtonState.PUSHED;
    this.isPushed_ = true;
    this.draw();
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.mouseUpHandler_ = function(event) {
    this.isPushed_ = false;
    this.currentState_ = this.isOver_ ?
        anychart.controls.scrollBar.ScrollBarButtonState.HOVER :
        anychart.controls.scrollBar.ScrollBarButtonState.NORMAL;
    this.draw();
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarButton.prototype.containerStopMove_ = function(event) {
    if (this.isPushed_) this.mouseUpHandler_(event);
};
//------------------------------------------------------------------------------
//
//                  ScrollBarButtonWithArrow class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButton}
 * @param {anychart.svg.SVGManager}
    */
anychart.controls.scrollBar.ScrollBarButtonWithArrow = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButton.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonWithArrow,
    anychart.controls.scrollBar.ScrollBarButton);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.background.ShapeBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.arrowNormalBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.ShapeBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.arrowHoverBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.ShapeBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.arrowPushedBackground_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.arrowBackgroundSprite_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.deserialize = function(normalData, hoverData, pushedData) {
    goog.base(this, 'deserialize', normalData, hoverData, pushedData);
    var des = anychart.utils.deserialization;

    this.arrowNormalBackground_ = new anychart.controls.scrollBar.ScrollBarButtonArrowBackground(this);
    this.arrowNormalBackground_.deserialize(des.getProp(normalData, 'arrow'));

    this.arrowHoverBackground_ = new anychart.controls.scrollBar.ScrollBarButtonArrowBackground(this);
    this.arrowHoverBackground_.deserialize(des.getProp(hoverData, 'arrow'));

    this.arrowPushedBackground_ = new anychart.controls.scrollBar.ScrollBarButtonArrowBackground(this);
    this.arrowPushedBackground_.deserialize(des.getProp(pushedData, 'arrow'));
};
//------------------------------------------------------------------------------
//                         Initialization.
//------------------------------------------------------------------------------
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.initialize = function(bounds) {
    goog.base(this, 'initialize', bounds);

    if (this.needCreateArrowBackground_()) {
        var bg = this.getEnabledBackground_();
        this.arrowBackgroundSprite_ = bg.initialize(this.svgManager_);
        this.appendSprite(this.arrowBackgroundSprite_);
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.draw = function() {
    goog.base(this, 'draw');
    var bg = this.getActualArrowBackground_();
    if (bg && bg.isEnabled())
        bg.draw(this.arrowBackgroundSprite_, this.getArrowBounds());
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.resize = function(bounds) {
    goog.base(this, 'resize', bounds);
    if (this.arrowBackgroundSprite_) {
        var bg = this.getActualArrowBackground_();
        bg.resize(this.arrowBackgroundSprite_, this.getArrowBounds());
    }
};
//------------------------------------------------------------------------------
//                          Background utils.
//------------------------------------------------------------------------------
/**
 * @param {Function} class
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.getArrowPath = goog.abstractMethod;
/**
 * Return scroll bar arrow button bounds.
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.getArrowBounds = goog.abstractMethod;
/**
 * @return {anychart.visual.background.ShapeBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.getActualArrowBackground_ = function() {
    switch (this.currentState_) {
        case anychart.controls.scrollBar.ScrollBarButtonState.NORMAL:
            return this.arrowNormalBackground_;

        case anychart.controls.scrollBar.ScrollBarButtonState.PUSHED:
            return this.arrowPushedBackground_;

        case anychart.controls.scrollBar.ScrollBarButtonState.HOVER:
            return this.arrowHoverBackground_;
    }
    return null;
};
/**
 * Define, need create arrow background sprite.
 * @private
 * @return {Boolean}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.needCreateArrowBackground_ = function() {
    return  (this.arrowNormalBackground_ && this.arrowNormalBackground_.isEnabled()) ||
        (this.arrowPushedBackground_ && this.arrowPushedBackground_.isEnabled()) ||
        (this.arrowHoverBackground_ && this.arrowHoverBackground_.isEnabled());
};
/**
 * @private
 * @return {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonWithArrow.prototype.getEnabledBackground_ = function() {
    if (this.arrowNormalBackground_ && this.arrowNormalBackground_.isEnabled()) return this.arrowNormalBackground_;
    if (this.arrowPushedBackground_ && this.arrowPushedBackground_.isEnabled()) return this.arrowPushedBackground_;
    if (this.arrowHoverBackground_ && this.arrowHoverBackground_.isEnabled()) return this.arrowHoverBackground_;
    return null;
};
//------------------------------------------------------------------------------
//
//                    ScrollBarButtonArrowBackground class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.background.ShapeBackground}
 */
anychart.controls.scrollBar.ScrollBarButtonArrowBackground = function(scrollBarButton) {
    anychart.visual.background.ShapeBackground.call(this);
    this.scrollBarButton_ = scrollBarButton;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonArrowBackground,
    anychart.visual.background.ShapeBackground);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 */
anychart.controls.scrollBar.ScrollBarButtonArrowBackground.prototype.scrollBarButton_ = null;
//------------------------------------------------------------------------------
//                          Override.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonArrowBackground.prototype.createElement = function(svgManager) {
    return svgManager.createPath();
};
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonArrowBackground.prototype.getElementPath = function(svgManager, bounds) {
    return this.scrollBarButton_.getElementPath(svgManager, bounds);
};
//------------------------------------------------------------------------------
//
//                   ScrollBarButtonDown class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.scrollBar.ScrollBarButtonDown = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButtonWithArrow.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonDown,
    anychart.controls.scrollBar.ScrollBarButtonWithArrow);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonDown.prototype.getElementPath = function(svgManager, bounds) {
    var path = svgManager.pMove(bounds.x, bounds.y);
    path += svgManager.pLine(bounds.x + bounds.width, bounds.y);
    path += svgManager.pLine(bounds.x + bounds.width / 2, bounds.y + bounds.height);
    path += svgManager.pLine(bounds.x, bounds.y);
    return path;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonDown.prototype.getArrowBounds = function() {
    var iconSize = Math.min(this.bounds_.width, this.bounds_.height) / 2.5;

    return new anychart.utils.geom.Rectangle(this.bounds_.width / 2 - iconSize / 2,
        this.bounds_.height / 2 - iconSize / 4,
        iconSize,
        iconSize / 2);
};
//------------------------------------------------------------------------------
//
//                   ScrollBarButtonLeft class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.scrollBar.ScrollBarButtonLeft = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButtonWithArrow.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonLeft,
    anychart.controls.scrollBar.ScrollBarButtonWithArrow);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonLeft.prototype.getElementPath = function(svgManager, bounds) {
    var path = svgManager.pMove(bounds.x, bounds.y + bounds.height * .5);
    path += svgManager.pLine(bounds.x + bounds.width, bounds.y);
    path += svgManager.pLine(bounds.x + bounds.width, bounds.y + bounds.height);
    path += svgManager.pMove(bounds.x, bounds.y + bounds.height * .5);
    return path;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonLeft.prototype.getArrowBounds = function() {
    var iconSize = Math.min(this.bounds_.width, this.bounds_.height) * .4;

    return new anychart.utils.geom.Rectangle(
        this.bounds_.width * .5 - iconSize * .25,
        (this.bounds_.height - iconSize) * .5,
        iconSize * .5,
        iconSize);
};
//------------------------------------------------------------------------------
//
//                   ScrollBarButtonRight class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.scrollBar.ScrollBarButtonRight = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButtonWithArrow.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonRight,
    anychart.controls.scrollBar.ScrollBarButtonWithArrow);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonRight.prototype.getElementPath = function(svgManager, bounds) {
    var path = svgManager.pMove(bounds.x, bounds.y);
    path += svgManager.pLine(bounds.x + bounds.width, bounds.y + bounds.height / 2);
    path += svgManager.pLine(bounds.x, bounds.y + bounds.height);
    path += svgManager.pMove(bounds.x, bounds.y);
    return path;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonRight.prototype.getArrowBounds = function() {
    var iconSize = Math.min(this.bounds_.width, this.bounds_.height) * 0.4;

    return new anychart.utils.geom.Rectangle(this.bounds_.width / 2 - iconSize / 4,
        this.bounds_.height / 2 - iconSize / 2,
        iconSize / 2,
        iconSize);
};
//------------------------------------------------------------------------------
//
//                       ScrollBarButtonUp class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.scrollBar.ScrollBarButtonUp = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButtonWithArrow.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarButtonUp,
    anychart.controls.scrollBar.ScrollBarButtonWithArrow);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonUp.prototype.getElementPath = function(svgManager, bounds) {
    var path = svgManager.pMove(bounds.x, bounds.y + bounds.height);
    path += svgManager.pLine(bounds.x + bounds.width, bounds.y + bounds.height);
    path += svgManager.pLine(bounds.x + bounds.width / 2, bounds.y);
    path += svgManager.pLine(bounds.x, bounds.y + bounds.height);
    return path;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarButtonUp.prototype.getArrowBounds = function() {
    var iconSize = Math.min(this.bounds_.width, this.bounds_.height) / 2.5;

    return new anychart.utils.geom.Rectangle(this.bounds_.width / 2 - iconSize / 2,
        this.bounds_.height / 2 - iconSize / 4,
        iconSize,
        iconSize / 2);
};
//------------------------------------------------------------------------------
//
//                          ScrollBarThumb class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButton}
 * @param {anychart.svg.SVGManager}
    */
anychart.controls.scrollBar.ScrollBarThumb = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarButton.call(this, svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarThumb,
    anychart.controls.scrollBar.ScrollBarButton);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.controls.scrollBar.ScrollBarThumb.prototype.thumbPath_ = null;
//------------------------------------------------------------------------------
//                         Initialization.
//------------------------------------------------------------------------------
anychart.controls.scrollBar.ScrollBarThumb.prototype.initialize = function(bounds) {
    goog.base(this, 'initialize', bounds);
    this.thumbPath_ = this.svgManager_.createPath();
    this.thumbPath_.setAttribute('fill', 'none');
    this.thumbPath_.setAttribute('stroke-thickness', '1');
    this.thumbPath_.setAttribute('stroke', '#494949');
    this.thumbPath_.setAttribute('stroke-opacity', '0.5');
    this.appendChild(this.thumbPath_);
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarThumb.prototype.draw = function() {
    goog.base(this, 'draw');
    this.thumbPath_.setAttribute('d', this.getThumbPathData())
};
/**
 * Return thumb path data.
 * @protected
 * @return {String}
 */
anychart.controls.scrollBar.ScrollBarThumb.prototype.getThumbPathData = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                          ScrollBarThumbV class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButton}
 * @param {anychart.svg.SVGManager}
    */
anychart.controls.scrollBar.ScrollBarThumbV = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarThumb.call(this, svgManager);
    this.dw_ = 2;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarThumbV,
    anychart.controls.scrollBar.ScrollBarThumb);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarThumbV.prototype.getThumbPathData = function() {
    var cX = this.bounds_.width * 0.25;
    var cY = this.bounds_.height * 0.5;

    var pathData = this.svgManager_.pMove(cX, cY - 4);
    pathData += this.svgManager_.pLine(this.bounds_.width - cX, cY - 4);
    pathData += this.svgManager_.pMove(cX, cY - 2);
    pathData += this.svgManager_.pLine(this.bounds_.width - cX, cY - 2);
    pathData += this.svgManager_.pMove(cX, cY);
    pathData += this.svgManager_.pLine(this.bounds_.width - cX, cY);
    pathData += this.svgManager_.pMove(cX, cY + 2);
    pathData += this.svgManager_.pLine(this.bounds_.width - cX, cY + 2);
    pathData += this.svgManager_.pMove(cX, cY + 4);
    pathData += this.svgManager_.pLine(this.bounds_.width - cX, cY + 4);

    return pathData;
};
//------------------------------------------------------------------------------
//
//                          ScrollBarThumbH class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarButton}
 * @param {anychart.svg.SVGManager}
    */
anychart.controls.scrollBar.ScrollBarThumbH = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarThumb.call(this, svgManager);
    this.dh_ = 2;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarThumbH,
    anychart.controls.scrollBar.ScrollBarThumb);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarThumbH.prototype.getThumbPathData = function() {
    var cX = this.bounds_.width * 0.5;
    var cY = this.bounds_.height * 0.25;

    var pathData = this.svgManager_.pMove(cX - 4, cY);
    pathData += this.svgManager_.pLine(cX - 4, this.bounds_.height - cY);
    pathData += this.svgManager_.pMove(cX - 2, cY);
    pathData += this.svgManager_.pLine(cX - 2, this.bounds_.height - cY);
    pathData += this.svgManager_.pMove(cX, cY);
    pathData += this.svgManager_.pLine(cX, this.bounds_.height - cY);
    pathData += this.svgManager_.pMove(cX + 2, cY);
    pathData += this.svgManager_.pLine(cX + 2, this.bounds_.height - cY);
    pathData += this.svgManager_.pMove(cX + 4, cY);
    pathData += this.svgManager_.pLine(cX + 4, this.bounds_.height - cY);

    return pathData;
};
//------------------------------------------------------------------------------
//
//                         ScrollBarControl class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 */
anychart.controls.scrollBar.ScrollBarControl = function(svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
    this.minValue_ = 0;
    this.maxValue_ = 100;
    this.visibleValue_ = 1;
    this.scrollBarSize_ = 20;
    this.currentButtonSize_ = 20;
    this.thumbBonusSize_ = 0;
    this.thumbStep_ = 5;
    this.isPercentThumbStep_ = false;
};
goog.inherits(anychart.controls.scrollBar.ScrollBarControl,
    anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                              Constants.
//------------------------------------------------------------------------------
/**
 *
 * @const
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.MIN_THUMB_SIZE = 16;
/**
 * @private
 * @const
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.THUMB_MOVE_DELAY = 300;
/**
 * @private
 * @const
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.THUMB_MOVE_INTERVAL = 40;
//------------------------------------------------------------------------------
//                     Scroll bar background.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.background_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.backgroundSprite_ = null;
//------------------------------------------------------------------------------
//                          Scroll bar controls.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.buttonAdd_ = null;
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarButtonWithArrow}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.buttonSub_ = null;
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarButton}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumb_ = null;
//------------------------------------------------------------------------------
//                          Size properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.scrollBarSize_ = null;
/**
 * @return {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.getScrollBarSize = function() {
    return this.scrollBarSize_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.buttonSize_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.currentButtonSize_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumbSize_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumbBonusSize_ = null;
//------------------------------------------------------------------------------
//                       ScrollBar value properties.
//------------------------------------------------------------------------------
/**
 * Start coordinate of scrollable container (x for horizontal, y for vertical)
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.minValue_ = null;
/**
 * @param {Number} value
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setMinValue = function(value) {
    this.minValue_ = value;
};
/**
 * Lenght of scrollable container (x + width for horizontal, y + height for vertical)
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.maxValue_ = null;
/**
 * @param {Number} value
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setMaxValue = function(value) {
    this.maxValue_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.currentValue_ = null;
/**
 * @param {Number} value
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setCurrentValue = function(value) {
    this.currentValue_ = value;
    this.setThumbPosition();
    this.checkThumb();
    this.dispatchChangeEvent_();
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.visibleValue_ = null;
/**
 * @param {Number} value
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setVisibleValue = function(value) {
    this.visibleValue_ = value;
    this.checkControlsSize_();
    this.setThumbPosition();
    this.checkThumb();
    this.dispatchChangeEvent_();
};
//------------------------------------------------------------------------------
//                            Bounds.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.bounds_ = null;
/**
 * @protected
 * @param {anychart.utils.geom.Rectangle}
    */
anychart.controls.scrollBar.ScrollBarControl.prototype.setScrollBarBounds = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Thumb move properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.lastThumbPosition_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumbTimeoutId_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumbIntervalId_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.thumbStep_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.isPercentThumbStep_ = null;
//------------------------------------------------------------------------------
//                           Deserialize.
//------------------------------------------------------------------------------
/**
 * Deserialize scroll bar settings
 * @param {Object} data
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    var utils = anychart.utils.JSONUtils;

    //scroll bar background
    this.background_ = new anychart.visual.background.RectBackground();
    this.background_.deserialize(data);

    //scroll bar size
    if (des.hasProp(data, 'size'))
        this.scrollBarSize_ = des.getNumProp(data, 'size');
    this.buttonSize_ = this.scrollBarSize_;
    this.thumbSize_ = this.scrollBarSize_;

    var normalData, hoverData, pushedData;

    //deserialize buttons background
    if (des.hasProp(data, 'buttons')) {
        var buttons = des.getProp(data, 'buttons');
        normalData = hoverData = pushedData = buttons;

        if (des.hasProp(buttons, 'states')) {
            var states = des.getProp(buttons, 'states');
            normalData = utils.merge(normalData, des.getProp(states, 'normal'));
            hoverData = utils.merge(hoverData, des.getProp(states, 'hover'));
            pushedData = utils.merge(pushedData, des.getProp(states, 'pushed'));
        }
        this.buttonAdd_.deserialize(normalData, hoverData, pushedData);
        this.buttonSub_.deserialize(normalData, hoverData, pushedData);
    }

    //deserialize thumb background
    if (des.hasProp(data, 'thumb')) {
        var thumb = des.getProp(data, 'thumb');
        normalData = hoverData = pushedData = thumb;

        if (des.hasProp(thumb, 'states')) {
            states = des.getProp(thumb, 'states');
            normalData = utils.merge(normalData, des.getProp(states, 'normal'));
            hoverData = utils.merge(normalData, des.getProp(states, 'hover'));
            pushedData = utils.merge(normalData, des.getProp(states, 'pushed'));
        }
        this.thumb_.deserialize(normalData, hoverData, pushedData);
    }
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize scroll bar
 * @param {anychart.utils.geom.Rectangle} bounds scrollable sprite bounds.
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.initialize = function(bounds) {
    //bounds
    this.setScrollBarBounds(bounds);
    this.bounds_ = bounds.clone();

    //background
    if (this.background_ && this.background_.isEnabled()) {
        this.backgroundSprite_ = this.background_.initialize(this.svgManager_);
        goog.events.listen(this.backgroundSprite_.getElement(), goog.events.EventType.MOUSEDOWN, this.onScrollBarBackgroundMouseDown, false, this);
    }


    //controls
    this.thumb_.initialize(bounds);
    this.buttonAdd_.initialize(bounds);
    this.buttonSub_.initialize(bounds);

    //appending controls
    if (this.backgroundSprite_)
        this.appendSprite(this.backgroundSprite_);
    this.appendSprite(this.buttonSub_);
    this.appendSprite(this.thumb_);
    this.appendSprite(this.buttonAdd_);

    //mouse down listeners
    goog.events.listen(this.buttonAdd_.getElement(), goog.events.EventType.MOUSEDOWN, this.buttonAddMouseDownHandler, false, this);
    goog.events.listen(this.buttonSub_.getElement(), goog.events.EventType.MOUSEDOWN, this.buttonSubMouseDownHandler, false, this);
    goog.events.listen(this.thumb_, anychart.events.DragEvent.ON_DRAG_PROCESS, this.onDragProcess_, false, this);

};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw scroll bar
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.draw = function() {
    if (this.backgroundSprite_)
        this.background_.draw(this.backgroundSprite_,
            new anychart.utils.geom.Rectangle(
                0,
                0,
                this.bounds_.width,
                this.bounds_.height));

    this.buttonAdd_.draw();
    this.buttonSub_.draw();
    this.thumb_.draw();
    this.thumb_.initializeDrag(this.getThumbDragBounds());
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.resize = function(bounds) {
    this.setScrollBarBounds(bounds);
    this.bounds_ = bounds.clone();
    this.setPosition(bounds.x, bounds.y);
    this.checkControlsSize_();
    this.resizeControls();
    this.checkThumb();
};
/**
 * В зависимости от размеров сколл бара:
 * 1.Cкрываем тумбу если не влазит
 * 2. Уменьшаем размер кнопок, это единственный метод, который может выставить currentButtonSize_ меньше заданного.
 * @private
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.checkControlsSize_ = function() {
    this.currentButtonSize_ = this.buttonSize_;
    var w = this.getLength() - 2 * this.buttonSize_;
    var pixPerValue = w / (this.maxValue_ - this.minValue_);
    this.thumbSize_ = pixPerValue * this.visibleValue_;

    if (w < this.MIN_THUMB_SIZE) {
        this.thumb_.setVisibility(false && this.visible_);
        if (w < 0) this.currentButtonSize_ = this.getLength() * 0.5;
    } else {
        this.thumb_.setVisibility(true && this.visible_);
        if (this.thumbSize_ < this.MIN_THUMB_SIZE) {
            this.thumbBonus_ = this.MIN_THUMB_SIZE - this.thumbSize_;
            this.thumbSize_ = this.MIN_THUMB_SIZE;
        } else {
            this.thumbBonus_ = 0;
        }
    }
};
//------------------------------------------------------------------------------
//                  Thumb move by press arrow buttons.
//------------------------------------------------------------------------------
/**
 * Move thumb.
 * @param {Boolean} isIncrease
 * @param {Number} step
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.moveThumb_ = function(isIncrease, step) {
    this.execMoveThumb_(this, isIncrease, step);
    this.thumbTimeoutId_ = setTimeout(this.startMoveThumb_, this.THUMB_MOVE_DELAY, this, isIncrease, step);
};
/**
 * @param {anychart.controls.scrollBar.ScrollBarControl} scope
 * @param {Boolean} isIncrease
 * @param {Number} step
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.execMoveThumb_ = function(scope, isIncrease, step) {
    if (Math.abs(step) > 100 && scope.isPercentThumbStep_) step = step > 0 ? 12 : -12;
    step = scope.isPercentThumbStep_ ? ((step / 100) * Math.abs(scope.maxValue_ - scope.minValue_)) : step;
    scope.currentValue_ = isIncrease ? (scope.currentValue_ + step) : (scope.currentValue_ - step);

    scope.setThumbPosition();
    scope.checkThumb();
    scope.checkThumbMove_(isIncrease);
    scope.dispatchChangeEvent_();
};
/**
 * Не дает тебме выйти за пределы скол бара
 * @protected
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.checkThumb = function() {
    if (!this.thumb_.isVisible()) return;
    var w = this.getLength() - this.buttonSize_ - this.thumbSize_;
    var p = this.getThumbSpritePosition();

    if (p < this.buttonSize_) {
        this.currentValue_ = this.minValue_;
        this.setSpriteThumbPosition(this.buttonSize_);
    } else if (p > w || w - p < 1) {
        this.currentValue_ = this.maxValue_ - this.visibleValue_;
        this.setSpriteThumbPosition(w - 2);
    }
};
/**
 * Проверяем, если thumb вышел за пределы скрола, остонавливаем движение
 * @param {Boolean} isIncrease
 * @return {Boolean}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.checkThumbMove_ = function(isIncrease) {
    var p = this.getThumbSpritePosition();
    var isThumbVisable = this.thumb_.isVisible();
    var isLeaveBounds = this.currentValue_ < this.minValue_ || this.currentValue_ > this.maxValue_ - this.visibleValue_;
    var isLeaveIncrease = isThumbVisable && isIncrease && p + this.thumbSize_ >= this.lastThumbPosition_;
    var isLeaveDecrease = isThumbVisable && !isIncrease && p <= this.lastThumbPosition_;
    var isLeave = isLeaveBounds || isLeaveIncrease || isLeaveDecrease;
    if (isLeave) this.stopMoveThumb_(null);
    return !isLeave;
};
//------------------------------------------------------------------------------
//                  Thumb move by hold arrow buttons.
//------------------------------------------------------------------------------
anychart.controls.scrollBar.ScrollBarControl.prototype.startMoveThumb_ = function(scope, isIncrease, step) {
    clearTimeout(scope.thumbTimeoutId_);
    if (scope.checkThumbMove_(isIncrease))
        scope.thumbIntervalId_ = setInterval(scope.execMoveThumb_, scope.THUMB_MOVE_INTERVAL, scope, isIncrease, step);
};
anychart.controls.scrollBar.ScrollBarControl.prototype.stopMoveThumb_ = function(isIncrease, step) {
    clearTimeout(this.thumbTimeoutId_);
    clearInterval(this.thumbIntervalId_);
    goog.events.unlisten(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.stopMoveThumb_, false, this);
};
//------------------------------------------------------------------------------
//                      Events dispatching.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.scrollBarChangeTimoutId_ = null;
/**
 * Dispatch scroll bar change event by 1 millisecond delay.
 * @private
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.dispatchChangeEvent_ = function() {
    clearTimeout(this.scrollBarChangeTimoutId_);
    this.scrollBarChangeTimoutId_ = setTimeout(this.execDispatchChangeEvent_(this), 1);
};
/**
 * Create and dispatch scroll bar change event.
 * @param scope
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.execDispatchChangeEvent_ = function(scope) {
    goog.events.dispatchEvent(
        scope,
        new anychart.controls.scrollBar.ScrollBarEvent(
            anychart.controls.scrollBar.ScrollBarEvent.CHANGE,
            scope.bounds_.y - scope.currentValue_));
};
//------------------------------------------------------------------------------
//                          Thumb drag.
//------------------------------------------------------------------------------

anychart.controls.scrollBar.ScrollBarControl.prototype.onDragProcess_ = function() {
    this.currentValue_ = this.getCurrentThumbValue();

    this.checkThumb();
    this.dispatchChangeEvent_();
};
/**
 * @protected
 * @return {Number}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.getCurrentThumbValue = function() {
    var w = this.getLength() - 2 * this.buttonSize_ - this.thumbBonus_;
    var d = this.maxValue_ - this.minValue_;
    var p = this.getThumbSpritePosition() - this.buttonSize_;
    return this.minValue_ + p * d / w;
};
/**
 * Set thumb position.
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setThumbPosition = function() {
    if (!this.thumb_.isVisible() || !this.bounds_) return;
    var w = this.getLength() - 2 * this.currentButtonSize_ - this.thumbBonus_;
    var d = this.maxValue_ - this.minValue_;
    this.setSpriteThumbPosition(this.currentButtonSize_ + (this.currentValue_ - this.minValue_) * w / d);
};
//------------------------------------------------------------------------------
//                      Mouse down handlers
//------------------------------------------------------------------------------
/**
 * @protected
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.buttonAddMouseDownHandler = function(event) {
    this.lastThumbPosition_ = this.getLength() - this.currentButtonSize_;
    this.moveThumb_(true, this.thumbStep_);
    goog.events.listen(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.stopMoveThumb_, false, this);
};
/**
 * @protected
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.buttonSubMouseDownHandler = function(event) {
    this.lastThumbPosition_ = this.currentButtonSize_;
    this.moveThumb_(false, this.thumbStep_);
    goog.events.listen(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.stopMoveThumb_, false, this);
};
/**
 * @protected
 * @param {goog.events.Event} event
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.onScrollBarBackgroundMouseDown = function(event) {
    this.thumbMoveLastPoint_ = this.getMouseCoord(event);
    var v = this.thumbMoveLastPoint_;
    var t = this.getThumbSpritePosition();
    if (v < t) this.moveThumb_(false, this.visibleValue_);
    if (v > t + this.thumbSize_) this.moveThumb_(true, this.visibleValue_);
};
//------------------------------------------------------------------------------
//                          Abstract.
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.getLength = goog.abstractMethod;
/**
 * @protected
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.resizeControls = goog.abstractMethod;
/**
 * @protected
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.getThumbSpritePosition = goog.abstractMethod;
/**
 * @protected
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.setSpriteThumbPosition = goog.abstractMethod;
/**
 * @protected
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.scrollBar.ScrollBarControl.prototype.getThumbDragBounds = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                      ScrollBarVertical class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.scrollBar.ScrollBarControl}
 */
anychart.controls.scrollBar.ScrollBarVertical = function(svgManager) {
    anychart.controls.scrollBar.ScrollBarControl.call(this, svgManager);
    this.buttonSub_ = new anychart.controls.scrollBar.ScrollBarButtonUp(svgManager);
    this.buttonAdd_ = new anychart.controls.scrollBar.ScrollBarButtonDown(svgManager);
    this.thumb_ = new anychart.controls.scrollBar.ScrollBarThumbV(svgManager);
};
goog.inherits(anychart.controls.scrollBar.ScrollBarVertical,
    anychart.controls.scrollBar.ScrollBarControl);
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.setScrollBarBounds = function(bounds) {
    bounds.width = this.scrollBarSize_;
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.resizeControls = function() {
    //button sub bounds
    var tempBounds = new anychart.utils.geom.Rectangle(
        0,
        0,
        this.bounds_.width,
        this.currentButtonSize_);
    this.buttonSub_.resize(tempBounds);

    //button add bounds
    tempBounds.y = this.bounds_.height - tempBounds.height;
    this.buttonAdd_.resize(tempBounds);

    //thumb
    var w = this.bounds_.height - 2 * this.currentButtonSize_;
    var d = this.maxValue_ - this.minValue_;
    tempBounds.y = this.buttonSize_ + (this.currentValue_ - this.minValue_) * w / d;
    tempBounds.height = this.thumbSize_;
    this.thumb_.resize(tempBounds);
};
//------------------------------------------------------------------------------
//                           Thumb.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.getThumbDragBounds = function() {
    return new anychart.utils.geom.Rectangle(
        0,
        this.buttonSize_,
        this.bounds_.width - 2,
        this.bounds_.height - 2 * this.buttonSize_);
};
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.setSpriteThumbPosition = function(value) {
    this.thumb_.setY(value);
};
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.getThumbSpritePosition = function() {
    return this.thumb_.getY();
};
//------------------------------------------------------------------------------
//                         Other override.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.getLength = function() {
    return this.bounds_ ? this.bounds_.height : 0;
};
/**
 * @inheritDoc
 */
anychart.controls.scrollBar.ScrollBarVertical.prototype.getMouseCoord = function(event) {
    return event.offsetY - this.getClientPosition().y;
};

