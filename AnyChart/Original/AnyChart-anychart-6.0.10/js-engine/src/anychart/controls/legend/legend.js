/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains interfaces:
 * <ul>
 *  <li>@class {anychart.controls.legend.ILegendItem};</li>
 *  <li>@class {anychart.controls.legend.ILegendItemsContainer};</li>
 *  <li>@class {anychart.controls.legend.ILegendTag};</li>
 * <ul>
 *
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.legend.LegendAdaptiveItem};</li>
 *  <li>@class {anychart.controls.legend.LegendAdapter};</li>
 *  <li>@class {anychart.controls.legend.BaseIconSetting};</li>
 *  <li>@class {anychart.controls.legend.LegendSeparatorLine};</li>
 *  <li>@class {anychart.controls.legend.LegendItem};</li>
 *  <li>@class {anychart.controls.legend.LegendItemsGroup};</li>
 *  <li>@class {anychart.controls.legend.LegendControl};</li>
 *  <li>@class {anychart.controls.legend.ColumnBasedLegend};</li>
 *  <li>@class {anychart.controls.legend.LegendRow};</li>
 *  <li>@class {anychart.controls.legend.RowBasedLegend};</li>
 * <ul>
 */
goog.provide('anychart.controls.legend');
goog.require('anychart.controls.layouters.anchored');
goog.require('anychart.controls.layouters.groupable');
goog.require('anychart.controls.scrollBar');
//------------------------------------------------------------------------------
//
//                           ILegendItem interface.
//
//------------------------------------------------------------------------------
anychart.controls.legend.ILegendItem = function() {
};
anychart.controls.legend.ILegendItem.prototype.getColor = function() {
};
anychart.controls.legend.ILegendItem.prototype.getHatchType = function() {
};
anychart.controls.legend.ILegendItem.prototype.getMarkerType = function() {
};
anychart.controls.legend.ILegendItem.prototype.iconCanDrawMarker = function() {
};
anychart.controls.legend.ILegendItem.prototype.getTag = function() {
};
anychart.controls.legend.ILegendItem.prototype.setTag = function(value) {
};
//------------------------------------------------------------------------------
//
//                           ILegendItemsContainer interface.
//
//------------------------------------------------------------------------------
anychart.controls.legend.ILegendItemsContainer = function() {
};
anychart.controls.legend.ILegendItemsContainer.prototype.addItem = function(control) {
    goog.abstractMethod(control)
};
//------------------------------------------------------------------------------
//
//                           ILegendTag interface.
//
//------------------------------------------------------------------------------
anychart.controls.legend.ILegendTag = function() {
};
anychart.controls.legend.ILegendTag.prototype.setHover = function() {
};
anychart.controls.legend.ILegendTag.prototype.setNormal = function() {
};
//------------------------------------------------------------------------------
//
//                           LegendAdaptiveItem class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendAdaptiveItem = function(opt_color, opt_hatchType, opt_markerType, opt_iconCanDrawMarker, opt_tag) {
    if (opt_color) this.color_ = opt_color;
    if (opt_hatchType)this.hatchType_ = opt_hatchType;
    if (opt_markerType != null && opt_markerType != undefined)this.markerType_ = opt_markerType;
    if (opt_iconCanDrawMarker)this.iconCanDrawMarker_ = opt_iconCanDrawMarker;
    if (opt_tag)this.tag_ = opt_tag;
};
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.color_ = null;
/**
 * @return {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getColor = function() {
    return this.color_;
};
/**
 * @param {anychart.visual.color.ColorContainer}
    */
anychart.controls.legend.LegendAdaptiveItem.prototype.setColor = function(value) {
    this.color_ = value;
};
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.hatchType_ = null;
/**
 * @return {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getHatchType = function() {
    return this.hatchType_;
};
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.markerType_ = null;
/**
 * @return {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getMarkerType = function() {
    return this.markerType_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.iconCanDrawMarker_ = null;
/**
 * @return {Boolean}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.iconCanDrawMarker = function() {
    return this.iconCanDrawMarker_;
};
/**
 * @private
 * @type {anychart.controls.legend.ILegendTag}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.tag_ = null;
/**
 * @param {anychart.controls.legend.ILegendTag} value
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.setTag = function(value) {
    this.tag_ = value;
};
/**
 * @return {anychart.controls.legend.ILegendTag}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getTag = function() {
    return this.tag_;
};
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.seriesType_ = 0;
/**
 * @return {int}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getSeriesType = function() {
    return this.seriesType_;
};
/**
 *
 * @param {int} value
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.setSeriesType = function(value) {
    this.seriesType_ = value;
};
/**
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.styleState_ = null;
/**
 * @return {anychart.styles.StyleState}
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.getStyleState = function() {
    return this.styleState_;
};
/**
 * @param {anychart.styles.StyleState} value
 */
anychart.controls.legend.LegendAdaptiveItem.prototype.setStyleState = function(value) {
    this.styleState_ = value;
};
//------------------------------------------------------------------------------
//
//                           LegendAdapter class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendAdapter = function(plot) {
    this.plot_ = plot;
};
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
anychart.controls.legend.LegendAdapter.prototype.plot_ = null;
//------------------------------------------------------------------------------
//                           LegendData
//------------------------------------------------------------------------------
/**
 * @param {string} source
 * @param {Object} item
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.controls.legend.LegendAdapter.prototype.getLegendDataBySource = function(source, item, container) {
    switch (source) {
        case 'series':
            this.getLegendSeriesData(item, container);
            break;
        case 'points':
            this.getLegendPointData(item, container);
            break;
        case 'thresholds':
            this.getLegendThresholdsData(item, container);
            break;
        case 'auto':
            this.getLegendAutoItem(item, container);
            break;
    }
};
//------------------------------------------------------------------------------
//                           Series
//------------------------------------------------------------------------------
/**
 * @protected
 * @param {Object} item
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.controls.legend.LegendAdapter.prototype.getLegendSeriesData = function(item, container) {
    var seriesName = this.getSeriesName_(item);
    var seriesCount = this.plot_.getNumSeries();
    for (var i = 0; i < seriesCount; i++) {
        var series = this.plot_.getSeriesAt(i);
        if (!seriesName || series.getName() == seriesName) {
            //marker type
            var markerType;
            if (series.getMarker().getStyle().getNormal().isDynamicType()) {
                markerType = series.getMarker().getType() == -1 ?
                    series.getMarkerType() :
                    series.getMarker().getType();
            } else
                markerType = series.getMarker().getStyle().getNormal().getType();

            //legend item
            var legendItem = new anychart.controls.legend.LegendAdaptiveItem(
                series.getColor(),
                series.getHatchType(),
                markerType,
                series.getGlobalSettings().iconCanDrawMarker(),
                series);

            legendItem.setSeriesType(series.getType());
            legendItem.setStyleState(series.getGlobalSettings().getStyle().getNormal());

            container.addItem(legendItem);
        }
    }

};
//------------------------------------------------------------------------------
//                           Point
//------------------------------------------------------------------------------
/**
 * @protected
 * @param {Object} item
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.controls.legend.LegendAdapter.prototype.getLegendPointData = function(item, container) {
    var des = anychart.utils.deserialization;
    var pointName = des.hasProp(item, 'name') ? des.getStringProp(item, 'name') : null;
    var seriesName = this.getSeriesName_(item);

    this.plot_.resetSeriesIterator();
    while (this.plot_.hasSeries()) {
        var series = this.plot_.getNextSeries();
        if (seriesName && series.getName() != seriesName) continue;
        var pointsLength = series.getPoints().length;
        for (var j = 0; j < pointsLength; j++) {
            var point = series.getPointAt(j);
            if (point && (!pointName || pointName == point.getName())) {
                if (point.getThreshold()) point.getThreshold().checkBeforeDraw(point);

                var markerType;
                if (point.getMarker().getStyle().getNormal().isDynamicType()) {
                    if (point.getMarker().getType == -1)
                        markerType = point.getMarkerType();
                    else
                        markerType = point.getMarker().getType();

                } else
                    markerType = series.getMarker().getStyle().getNormal().getMarkerType();


                var lItem = new anychart.controls.legend.LegendAdaptiveItem(
                    point.getColor(),
                    point.getHatchType(),
                    markerType,
                    point.getGlobalSettings().iconCanDrawMarker(),
                    point);

                lItem.setSeriesType(series.getType());
                lItem.setStyleState(point.getGlobalSettings().getStyle().getNormal());
                container.addItem(lItem);
            }
        }
    }

};
//------------------------------------------------------------------------------
//                           Thresholds
//------------------------------------------------------------------------------
/**
 * @protected
 * @param {Object} item
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.controls.legend.LegendAdapter.prototype.getLegendThresholdsData = function(item, container) {
    var des = anychart.utils.deserialization;
    var thresholdName = des.hasProp(item, 'threshold') ? des.getLStringProp(item, 'threshold') : null;
    var thresholds = this.plot_.getThresholdsList().getParsedThresholdList();
    var thresholdsCount = thresholds.length;
    for (var i = 0; i < thresholdsCount; i++) {
        var threshold = thresholds[i];
        if (!thresholdName || thresholdName == threshold.getName().toLowerCase())
            threshold.getData(container);
    }
};
//------------------------------------------------------------------------------
//                           Auto
//------------------------------------------------------------------------------
/**
 * @protected
 * @param {Object} item
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.controls.legend.LegendAdapter.prototype.getLegendAutoItem = function(item, container) {
    this.getLegendSeriesData(item, container);
    this.getLegendThresholdsData(item, container);
};
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
/**
 * @private
 * @param {Object} item
 * @return {String}
 */
anychart.controls.legend.LegendAdapter.prototype.getSeriesName_ = function(item) {
    var des = anychart.utils.deserialization;
    return des.hasProp(item, 'series') ? des.getStringProp(item, 'series') : null;
};
//------------------------------------------------------------------------------
//
//                           LegendIconSetting class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.BaseIconSetting = function() {
    this.markerType_ = anychart.elements.marker.MarkerType.NONE;
};
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.width_ = 20;
/**
 * @return {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.getWidth = function() {
    return this.width_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.height_ = 10;
/**
 * @return {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.getHeight = function() {
    return this.height_;
};
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.BaseIconSetting.prototype.color_ = null;
/**
 * @return {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.BaseIconSetting.prototype.getColor = function() {
    return this.color_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.BaseIconSetting.prototype.markerEnabled_ = false;
/**
 * @return {Boolean}
 */
anychart.controls.legend.BaseIconSetting.prototype.isMarkerEnabled = function() {
    return this.markerEnabled_
};
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.BaseIconSetting.prototype.markerType_ = null;
/**
 * @return {int}
 */
anychart.controls.legend.BaseIconSetting.prototype.getMarkerType = function() {
    return this.markerType_;
};
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.BaseIconSetting.prototype.markerColor_ = null;
/**
 * @return {anychart.visual.color.ColorContainer}
 */
anychart.controls.legend.BaseIconSetting.prototype.getMarkerColor = function() {
    return this.markerColor_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.markerSize_ = null;
/**
 * @return {Number}
 */
anychart.controls.legend.BaseIconSetting.prototype.getMarkerSize = function() {
    return (this.markerSize_) ? this.markerSize_ : (Math.min(this.width_, this.height_) * .5);
};
//------------------------------------------------------------------------------
//                           Deserialize
//------------------------------------------------------------------------------
anychart.controls.legend.BaseIconSetting.prototype.deserialize = function(data) {
    if (!data) return;
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'width')) this.width_ = des.getProp(data, 'width');
    if (des.hasProp(data, 'height')) this.height_ = des.getProp(data, 'height');
    if (des.hasProp(data, 'color')) {
        if (des.getStringProp(data, 'color') != '%color')
            this.color = des.getColorProp(data, 'color');
    }
    if (des.hasProp(data, 'marker')) {
        var markerData = des.getProp(data, 'marker');
        if (des.hasProp(markerData, 'enabled'))
            this.markerEnabled_ = des.getBoolProp(markerData, 'enabled');
        if (des.hasProp(markerData, 'type')) {
            if (des.getLStringProp(markerData, 'type') != '%markertype')
                this.markerType_ = anychart.elements.marker.MarkerType.deserialize(
                    des.getEnumProp(markerData, 'type'));
        }
        if (des.hasProp(markerData, 'color'))
            this.markerColor_ = des.getColorProp(markerData, 'color');
        if (des.hasProp(markerData, 'size'))
            this.markerSize_ = des.getNumProp(markerData, 'size');
    }
};
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
anychart.controls.legend.BaseIconSetting.prototype.copy = function(opt_target) {
    if (opt_target)
        opt_target = new anychart.controls.legend.BaseIconSetting();
    opt_target.width_ = this.width_;
    opt_target.height_ = this.height_;
    opt_target.markerEnabled_ = this.markerEnabled_;
    opt_target.markerType_ = this.markerType_;
    opt_target.markerSize_ = this.markerSize_;
    opt_target.markerColor_ = this.markerColor_;
    opt_target.color_ = this.color_;
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                           SeriesIconSettings class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.SeriesIconSetting = function(plot) {
    anychart.controls.legend.BaseIconSetting.call(this);
    this.plot_ = plot;
    this.isSimpleBox_ = false;
    this.isDynamicSeriesType_ = true;
};
goog.inherits(anychart.controls.legend.SeriesIconSetting,
    anychart.controls.legend.BaseIconSetting);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.SeriesIconSetting.prototype.isSimpleBox_ = null;
/**
 * @return {Boolean}
 */
anychart.controls.legend.SeriesIconSetting.prototype.isSimpleBox = function() {
    return this.isSimpleBox_;
};
/**
 * @private
 * @type {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.controls.legend.SeriesIconSetting.prototype.plot_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.SeriesIconSetting.prototype.isDynamicSeriesType_ = null;
/**
 * @return {Boolean}
 */
anychart.controls.legend.SeriesIconSetting.prototype.isDynamicSeriesType = function() {
    return this.isDynamicSeriesType_;
};
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.SeriesIconSetting.prototype.seriesType_ = null;
/**
 * @return {int}
 */
anychart.controls.legend.SeriesIconSetting.prototype.getSeriesType = function() {
    return this.seriesType_;
};
//------------------------------------------------------------------------------
//                           Deserialization
//------------------------------------------------------------------------------
anychart.controls.legend.SeriesIconSetting.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'type'))
        this.isSimpleBox_ = des.getEnumProp(data, 'type') == 'box';

    if (des.hasProp(data, 'series_type')) {
        var seriesType = des.getEnumProp(data, 'series_type');
        if (seriesType != '%SeriesType')
            this.isDynamicSeriesType_ = false;
        this.seriesType_ = seriesType;
    }
};
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
anychart.controls.legend.SeriesIconSetting.prototype.copy = function(settings) {
    if (!settings)
        settings = new anychart.controls.legend.SeriesIconSetting(this.plot_);
    goog.base(this, 'copy', settings);

    settings.isSimpleBox_ = this.isSimpleBox_;
    settings.isDynamicSeriesType_ = this.isDynamicSeriesType_;
    settings.seriesType_ = this.seriesType_;
    return settings;
};
//------------------------------------------------------------------------------
//
//                           LegendSeparatorLine class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendSeparatorLine = function() {
    anychart.visual.stroke.Stroke.apply(this);
};
goog.inherits(anychart.controls.legend.LegendSeparatorLine,
    anychart.visual.stroke.Stroke);
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendSeparatorLine.prototype.padding_ = 1;
anychart.controls.legend.LegendSeparatorLine.prototype.getPadding = function() {
    return this.padding_;
};
//------------------------------------------------------------------------------
//                           Deserialize
//------------------------------------------------------------------------------
anychart.controls.legend.LegendSeparatorLine.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    if (anychart.utils.deserialization.hasProp(data, 'padding'))
        this.padding_ = anychart.utils.deserialization.getNumProp(data, 'padding');
};
//------------------------------------------------------------------------------
//
//                           LegendItem class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendItem = function(svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
    this.contentBounds_ = new anychart.utils.geom.Rectangle();
};
goog.inherits(anychart.controls.legend.LegendItem, anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.legend.LegendItem.prototype.contentBounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.legend.LegendItem.prototype.getContentBounds = function() {
    return this.contentBounds_;
};

/**
 * @private
 * @type {anychart.controls.legend.ILegendTag}
 */
anychart.controls.legend.LegendItem.prototype.tag_ = null;
//------------------------------------------------------------------------------
//                           Tag
//------------------------------------------------------------------------------
anychart.controls.legend.LegendItem.prototype.initTag = function(tag) {
    this.tag_ = tag;
    //todo:implement tag events
};
//------------------------------------------------------------------------------
//                           Events
//------------------------------------------------------------------------------
//todo:implement
//------------------------------------------------------------------------------
//
//                           LegendItemsGroup class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendItemsGroup = function(plot) {
    this.plot_ = plot;
    this.fixedWidth_ = NaN;
    this.fixedHeight_ = NaN;
};
//------------------------------------------------------------------------------
//                           Legend text properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.controls.legend.LegendItemsGroup.prototype.text_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendItemsGroup.prototype.hasLeftText_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendItemsGroup.prototype.leftTextBounds_ = null;
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.controls.legend.LegendItemsGroup.prototype.leftTextFormater_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendItemsGroup.prototype.hasRightText_ = null;

/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.controls.legend.LegendItemsGroup.prototype.rightTextFormater_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendItemsGroup.prototype.rightTextBounds_ = null;
//------------------------------------------------------------------------------
//                           Legend icon properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.legend.LegendItemsGroup.prototype.hasIcon_ = null;
/**
 * @private
 * @type {anychart.controls.legend.SeriesIconSetting}
 */
anychart.controls.legend.LegendItemsGroup.prototype.icon_ = null;
//------------------------------------------------------------------------------
//                           Legend other properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.IPlot}
 */
anychart.controls.legend.LegendItemsGroup.prototype.plot_ = null;
/**
 * @param {anychart.plots.IPlot} value
 */
anychart.controls.legend.LegendItemsGroup.prototype.setPlot = function(value) {
    this.plot_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendItemsGroup.prototype.fixedWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendItemsGroup.prototype.fixedHeight_ = null;
//------------------------------------------------------------------------------
//                           Deserialization
//------------------------------------------------------------------------------
anychart.controls.legend.LegendItemsGroup.prototype.deserializeDefault = function(data) {
    this.text_ = new anychart.visual.text.TextElement();
    this.text_.deserialize(data);
    this.text_.setBackground(null);

    this.deserialize(data);
    if (!this.plot_) return;
    this.icon_ = this.plot_.createIconSettings();
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'icon'))
        this.icon_.deserialize(des.getProp(data, 'icon'));
    if (des.hasProp(data, 'fixed_item_width'))
        this.fixedWidth_ = des.getNumProp(data, 'fixed_item_width');
    if (des.hasProp(data, 'fixed_item_height'))
        this.fixedHeight_ = des.getNumProp(data, 'fixed_item_height');
};

anychart.controls.legend.LegendItemsGroup.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'format'))
        this.processFormat(des.getStringProp(data, 'format'));
    else if (des.hasProp(data, 'text'))
        this.processFormat(des.getStringProp(data, 'text'));
    if (this.icon_) {
        this.icon_ = this.icon_.copy();
        if (des.hasProp(data, 'icon'))
            this.icon_.deserialize(des.getProp(data, 'icon'));
    }
};
//------------------------------------------------------------------------------
//                           Drawing
//------------------------------------------------------------------------------
anychart.controls.legend.LegendItemsGroup.prototype.createItem = function(source) {
    //!!!!!!!!!!!!!!!!!ATTENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //"4" svg определяет высоту текста в среднем на 3-4 пикселя меньше чем флеш
    // Поэтому 4 пикселя в декоративных целях добавлены к расстоянию между иконкой и текстом а так же к размерам занимаемым елементом легенды
    //todo: разобраться с метриками текста в svg, возможно удасться уровнять значение вменяемым способом.
    var item = new anychart.controls.legend.LegendItem(this.plot_.getSVGManager());

    //bounds calculation
    if (this.hasLeftText_) {
        this.setLeftText_(source);
        item.getContentBounds().width += this.leftTextBounds_.width + 4;
        item.getContentBounds().height = Math.max(this.leftTextBounds_.height, item.getContentBounds().height);
    }
    if (this.hasRightText_) {
        this.setRightText_(source);
        item.getContentBounds().width += this.rightTextBounds_.width;
        item.getContentBounds().height = Math.max(this.rightTextBounds_.height, item.getContentBounds().height);
    }
    if (this.hasIcon_) {
        item.getContentBounds().width += this.icon_.getWidth() + 4;
        item.getContentBounds().height = Math.max(this.icon_.getHeight(), item.getContentBounds().height);
    }

    //item drawing
    var x = 0;
    var y, textSprite;
    if (this.hasLeftText_) {
        y = (item.getContentBounds().height - this.leftTextBounds_.height) / 2;
        textSprite = this.text_.createSVGText(this.plot_.getSVGManager(), source.getColor());
        textSprite.setPosition(x, y);
        item.appendSprite(textSprite);
        x += this.leftTextBounds_.width + 4;
    }
    if (this.hasIcon_) {
        var iconSprite = new anychart.svg.SVGSprite(this.plot_.getSVGManager());
        y = (item.getContentBounds().height - this.icon_.getHeight()) / 2;
        this.plot_.drawDataIcon(this.icon_, source, iconSprite, 0, 0);

        item.appendSprite(iconSprite);

        if (source.iconCanDrawMarker() && this.icon_.isMarkerEnabled()) {
            var type = (this.icon_.getMarkerType() == anychart.elements.marker.MarkerType.NONE) ?
                source.getMarkerType() : this.icon_.getMarkerType();
            if (type != anychart.elements.marker.MarkerType.NONE)
                this.drawMarker_(item, source, type);

        }
        iconSprite.setX(x);
        iconSprite.setY(y);

        x += this.icon_.getWidth();
    }

    if (this.hasRightText_) {
        y = (item.getContentBounds().height - this.rightTextBounds_.height) / 2;
        textSprite = this.text_.createSVGText(this.plot_.getSVGManager(), source.getColor());
        textSprite.setPosition(x + 4, y);
        item.appendSprite(textSprite);
    }

    item.initTag(source.getTag());

    if (!isNaN(this.fixedWidth_)) item.getContentBounds().width = this.fixedWidth_;
    if (!isNaN(this.fixedHeight_)) item.getContentBounds().height = this.fixedHeight_;

    return item;
};
/**@private*/
anychart.controls.legend.LegendItemsGroup.prototype.setLeftText_ = function(source) {
    this.text_.initSize(
        this.plot_.getSVGManager(),
        this.leftTextFormater_.getValue(source.getTag()));
    this.leftTextBounds_ = this.text_.getBounds()
};
/**@private*/
anychart.controls.legend.LegendItemsGroup.prototype.setRightText_ = function(source) {
    this.text_.initSize(
        this.plot_.getSVGManager(),
        this.rightTextFormater_.getValue(source.getTag()));
    this.rightTextBounds_ = this.text_.getBounds();
};
/**@private*/
anychart.controls.legend.LegendItemsGroup.prototype.getMarkerStyle_ = function(color) {
    return 'fill:' + color.getColor().toRGB() +
        ';fill-opacity:1; stroke-thickness:1; stroke-opacity:1;' +
        'stroke:' + anychart.utils.deserialization.getColor('darkcolor(' + color.getColor().toRGB() + ')');
};
/**@private*/
anychart.controls.legend.LegendItemsGroup.prototype.drawMarker_ = function(item, source, type) {
    var markerSize = this.icon_.getMarkerSize();
    var markerSprite = new anychart.elements.marker.MarkerSprite(this.plot_.getSVGManager());

    var color = this.icon_.getMarkerColor() ?
        this.icon_.getMarkerColor() : source.getColor();

    var drawer = anychart.elements.marker.MarkerDrawer;
    var path = this.plot_.getSVGManager().createPath();
    var pathData = drawer.getSVGPathData(type, this.plot_.getSVGManager(), markerSize, 0, 0);
    path.setAttribute('style', this.getMarkerStyle_(color));
    path.setAttribute('d', pathData);
    markerSprite.appendChild(path);

    markerSprite.setX((this.icon_.getWidth() - markerSize) / 2);
    markerSprite.setY((this.icon_.getHeight() - markerSize) / 2);

    item.appendSprite(markerSprite);
};
//------------------------------------------------------------------------------
//                           Format
//------------------------------------------------------------------------------
/**@private*/
anychart.controls.legend.LegendItemsGroup.prototype.processFormat = function(format) {
    this.hasLeftText_ = false;
    this.hasRightText_ = false;

    var iconIndex = format.indexOf('{%Icon}');
    this.hasIcon_ = iconIndex != -1;

    if (!this.hasIcon_) {
        this.hasLeftText_ = true;
        this.leftTextFormat_ = format;
        this.leftTextFormater_ = new anychart.formatting.TextFormatter(this.leftTextFormat_);
    } else {
        if (iconIndex > 0) {
            this.hasLeftText_ = true;
            this.leftTextFormat_ = format.substring(0, iconIndex);
            this.leftTextFormater_ = new anychart.formatting.TextFormatter(this.leftTextFormat_);
        }
        if (iconIndex < format.length - 7) {
            this.hasRightText_ = true;
            this.rightTextFormat_ = format.substring(iconIndex + 7);
            this.rightTextFormater_ = new anychart.formatting.TextFormatter(this.rightTextFormat_);
        }
    }
};
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
anychart.controls.legend.LegendItemsGroup.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target = new anychart.controls.legend.LegendItemsGroup();
    opt_target.icon_ = this.icon_;
    opt_target.hasIcon_ = this.hasIcon_;

    opt_target.hasLeftText_ = this.hasLeftText_;
    opt_target.leftTextFormater_ = this.leftTextFormater_;
    opt_target.leftTextBounds_ = this.leftTextBounds_;

    opt_target.hasRightText_ = this.hasRightText_;
    opt_target.rightTextBounds_ = this.rightTextBounds_;
    opt_target.rightTextFormater_ = this.rightTextFormater_;

    opt_target.text_ = this.text_;
    opt_target.plot_ = this.plot_;

    opt_target.fixedWidth_ = this.fixedWidth_;
    opt_target.fixedHeight_ = this.fixedHeight_;

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                           LegendControl class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendControl = function() {
    anychart.controls.controlsBase.ControlWithTitle.call(this);
    this.columnsPadding_ = 2;
    this.rowsPadding_ = 2;
    this.scrollBarSpace_ = 0;
    this.elementsAlign_ = anychart.layout.HorizontalAlign.LEFT;
    this.format_ = "{%Icon} {%Name}";
    this.columnSpace_ = this.columnsPadding_;
    this.rowSpace_ = this.rowsPadding_;
    this.scrollBarBounds_ = new anychart.utils.geom.Rectangle();
    this.scrollBarClipPathBounds_ = new anychart.utils.geom.Rectangle();
};
goog.inherits(anychart.controls.legend.LegendControl,
    anychart.controls.controlsBase.ControlWithTitle);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.columnsPadding_ = null;
anychart.controls.legend.LegendControl.prototype.rowsPadding_ = null;
anychart.controls.legend.LegendControl.prototype.elementsAlign_ = null;
anychart.controls.legend.LegendControl.prototype.realElementsAlign_ = null;

anychart.controls.legend.LegendControl.prototype.format_ = null;

anychart.controls.legend.LegendControl.prototype.columnsSeparator_ = null;
anychart.controls.legend.LegendControl.prototype.rowsSeparator_ = null;


anychart.controls.legend.LegendControl.prototype.columnSpace_ = null;
anychart.controls.legend.LegendControl.prototype.rowSpace_ = null;

anychart.controls.legend.LegendControl.prototype.realContentHeight_ = null;
//------------------------------------------------------------------------------
//                      Visual properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.legend.LegendControl.prototype.itemsSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.legend.LegendControl.prototype.separatorsSprite_ = null;
//------------------------------------------------------------------------------
//                           Deserialization properties
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.legendDataFilter_ = null;
anychart.controls.legend.LegendControl.prototype.ignoreAutoItems_ = null;
//------------------------------------------------------------------------------
//                           Bounds properties
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.visualItems_ = null;
anychart.controls.legend.LegendControl.prototype.isItemsInitialized_ = false;
//------------------------------------------------------------------------------
//                      Legend items properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.legend.LegendItemsGroup}
 */
anychart.controls.legend.LegendControl.prototype.currentGroup_ = null;
/**
 * @private
 * @type {Array.<anychart.controls.legend.LegendItemsGroup>}
 */
anychart.controls.legend.LegendControl.prototype.groups_ = null;
/**
 * @private
 * @type {anychart.controls.legend.LegendItemsGroup}
 */
anychart.controls.legend.LegendControl.prototype.defaultGroup_ = null;
//------------------------------------------------------------------------------
//                          ScrollBar.
//-----------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarVertical}
 */
anychart.controls.legend.LegendControl.prototype.SCROLL_BAR_PADDING = 4;
/**
 * @private
 * @type {anychart.controls.scrollBar.ScrollBarVertical}
 */
anychart.controls.legend.LegendControl.prototype.scrollBar_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.legend.LegendControl.prototype.scrollBarBounds_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.legend.LegendControl.prototype.scrollBarClipPathBounds_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendControl.prototype.scrollBarSpace_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.legend.LegendControl.prototype.scrollBarSprite_ = null;
/**
 * @private
 */
anychart.controls.legend.LegendControl.prototype.setScrollBarBounds_ = function() {
    this.scrollBarClipPathBounds_.x = 0;
    this.scrollBarClipPathBounds_.y = 0;
    this.scrollBarClipPathBounds_.width = this.contentBounds_.width + this.scrollBar_.getScrollBarSize();
    this.scrollBarClipPathBounds_.height = this.contentBounds_.height;

    this.scrollBarBounds_.x = this.contentBounds_.x + this.contentBounds_.width - this.scrollBar_.getScrollBarSize();
    this.scrollBarBounds_.y = this.contentBounds_.y;
    this.scrollBarBounds_.height = this.contentBounds_.height;

    if (this.realContentHeight_ > this.contentBounds_.height) {
        this.scrollBar_.setMinValue(0);
        this.scrollBar_.setMaxValue(this.realContentHeight_);
        this.scrollBar_.setVisibleValue(this.contentBounds_.height);
    }

    this.scrollBar_.resize(this.scrollBarBounds_);
};
/**
 * Show legend scroll bar.
 * @protected
 */
anychart.controls.legend.LegendControl.prototype.showScrollBar = function() {
    this.scrollBarSpace_ = this.scrollBar_.getScrollBarSize() + this.SCROLL_BAR_PADDING;
    this.scrollBar_.setVisibility(true);
};
/**
 * Hide legend scroll bar.
 * @protected
 */
anychart.controls.legend.LegendControl.prototype.hideScrollBar = function() {
    this.scrollBarSpace_ = 0;
    this.scrollBar_.setCurrentValue(0);
    this.scrollBar_.setVisibility(false);
};
/**
 * @private
 * @event {anychart.controls.scrollBar.ScrollBarEvent}
 */
anychart.controls.legend.LegendControl.prototype.scrollBarChangeHandler_ = function(event) {
    this.itemsSprite_.scrollY(event.getValue());
};
//------------------------------------------------------------------------------
//                           Deserialize
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;
    data = des.copy(data);
    if (des.hasProp(data, 'width')) delete data['width'];
    if (des.hasProp(data, 'height')) delete data['height'];
    if (des.hasProp(data, 'columns_padding'))
        this.columnsPadding_ = des.getNumProp(data, 'columns_padding');
    if (des.hasProp(data, 'rows_padding'))
        this.rowsPadding_ = des.getNumProp(data, 'rows_padding');
    if (des.hasProp(data, 'elements_align'))
        this.elementsAlign_ = anychart.layout.HorizontalAlign.deserialize(
            des.getEnumProp(data, 'elements_align'));
    if (des.hasProp(data, 'format'))
        this.format_ = des.getStringProp(data, 'format');

    if (des.isEnabledProp(data, 'columns_separator')) {
        this.columnsSeparator_ = new anychart.controls.legend.LegendSeparatorLine();
        this.columnsSeparator_.deserialize(des.getProp(data, 'columns_separator'));
    }

    if (des.isEnabledProp(data, 'rows_separator')) {
        this.rowsSeparator_ = new anychart.controls.legend.LegendSeparatorLine();
        this.rowsSeparator_.deserialize(des.getProp(data, 'rows_separator'));
    }

    if (des.hasProp(data, 'ignore_auto_item'))
        this.ignoreAutoItems_ = des.getBoolProp(data, 'ignore_auto_item');

    this.createDataFilter_(data);

    this.columnSpace_ = this.columnsPadding_;
    this.rowSpace_ = this.rowsPadding_;

    if (this.columnsSeparator_)
        this.columnSpace_ += this.columnsSeparator_.getPadding();
    if (this.rowsSeparator_)
        this.rowSpace_ += this.rowsSeparator_.getPadding();

    this.realElementsAlign_ = this.elementsAlign_;

    //this.scrollBar_ = new anychart.controls.scrollBar.ScrollBarVertical(this.plot_.getSVGManager());
    //if (des.hasProp(data, 'scroll_bar'))
    //    this.scrollBar_.deserialize(des.getProp(data, 'scroll_bar'));
};
//------------------------------------------------------------------------------
//                           Initialize
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.initialize = function(svgManager) {
    goog.base(this, 'initialize', svgManager);
    this.separatorsSprite_ = new anychart.svg.SVGSprite(this.itemsSprite_.getSVGManager());
    this.itemsSprite_.appendSprite(this.separatorsSprite_);

    //goog.events.listen(this.scrollBar_, anychart.controls.scrollBar.ScrollBarEvent.CHANGE, this.scrollBarChangeHandler_, false, this);
    //this.scrollBar_.initialize(this.scrollBarBounds_);
    //this.sprite_.appendSprite(this.scrollBar_);

    return this.sprite_;
};

//------------------------------------------------------------------------------
//                           Bounds
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.controls.legend.LegendControl.prototype.initializeItems = function() {
    if (this.isItemsInitialized_) return;
    this.visualItems_ = [];

    this.getFilteredData_();

    this.isItemsInitialized_ = true;
};
anychart.controls.legend.LegendControl.prototype.setContentBounds = function() {
    goog.base(this, 'setContentBounds');
    //this.setScrollBarBounds_();
};
//------------------------------------------------------------------------------
//                           Drawing
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.draw = function() {
    goog.base(this, 'draw');
    this.itemsSprite_.setPosition(this.contentBounds_.x, this.contentBounds_.y);
    this.distributeItems();
    //this.itemsSprite_.initializeClipPath(this.scrollBarClipPathBounds_);
    //this.scrollBar_.draw();
    this.sprite_.appendSprite(this.itemsSprite_);
};
/**
 * @protected
 * Distribute legend items.
 */
anychart.controls.legend.LegendControl.prototype.distributeItems = goog.abstractMethod;
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
anychart.controls.legend.LegendControl.prototype.resize = function() {
    goog.base(this, 'resize');
    this.itemsSprite_.setPosition(this.contentBounds_.x, this.contentBounds_.y);
    if (this.separatorsSprite_) this.separatorsSprite_.clear();
    this.distributeItems();
    //this.itemsSprite_.updateClipPath(this.scrollBarClipPathBounds_);
    //this.scrollBar_.draw();
};
//------------------------------------------------------------------------------
//                           Items creation
//------------------------------------------------------------------------------
/**@private*/
anychart.controls.legend.LegendControl.prototype.createDataFilter_ = function(data) {
    this.groups_ = [];
    this.legendDataFilter_ = [];
    this.defaultGroup_ = new anychart.controls.legend.LegendItemsGroup(this.plot_);
    this.defaultGroup_.deserializeDefault(data);
    var des = anychart.utils.deserialization;

    if (!this.ignoreAutoItems_) {
        this.legendDataFilter_.push({'#name#':'item', '#children#' : [],'source': 'auto'});
        this.groups_.push(this.defaultGroup_);
    }

    //parse items
    if (des.hasProp(data, 'items')) {
        var itemsData = des.getPropArray(des.getProp(data, 'items'), 'item');
        var itemsLength = itemsData.length;
        for (var i = 0; i < itemsLength; i++) {
            var item = itemsData[i];
            var group = this.defaultGroup_.copy();
            group.deserialize(item);
            this.groups_.push(group);
            this.legendDataFilter_.push(item);
        }
    }
};
/**@private*/

anychart.controls.legend.LegendControl.prototype.getFilteredData_ = function() {
    var itemsCnt = this.legendDataFilter_.length;
    for (var i = 0; i < itemsCnt; i++) {
        this.currentGroup_ = this.groups_[i];
        this.currentGroup_.setPlot(this.plot_);
        this.plot_.setLegendData(this.legendDataFilter_[i], this);
    }
    this.defaultGroup_ = null;
    this.currentGroup_ = null;
    this.groups_ = null;
};
/**
 * @param {anychart.controls.legend.ILegendItem}
    */
anychart.controls.legend.LegendControl.prototype.addItem = function(item) {
    if (!item.getTag()) item.setTag(this.plot_);

    var visualItem = this.currentGroup_.createItem(item);
    this.visualItems_.push(visualItem);
    this.itemsSprite_.appendSprite(visualItem);
};
//------------------------------------------------------------------------------
//
//                           ColumnBasedLegend class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.ColumnBasedLegend = function() {
    anychart.controls.legend.LegendControl.call(this);
    this.columns_ = 1;
};
goog.inherits(anychart.controls.legend.ColumnBasedLegend,
    anychart.controls.legend.LegendControl);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.columns_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.itemsInColumn_ = null;
/**
 * максимальная высота колонок
 * @private
 * @type {Number}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.columnsHeight_ = null;
/**
 * суммарная ширина колонок
 * @private
 * @type {Number}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.summColumnsWidth_ = null;
/**
 * суммарные длины по колонкам (включая padding-и)
 * @private
 * @type {Array.<Number>}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.columnsWidths_ = null;
/**
 * максимальная длинна элементов в колонке
 * @private
 * @type {Array.<Number>}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.columnsMaxItemWidths_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.left_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.top_ = null;
//------------------------------------------------------------------------------
//                           Deserialization
//------------------------------------------------------------------------------
anychart.controls.legend.ColumnBasedLegend.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'columns')) this.columns_ = des.getNumProp(data, 'columns');
};
//------------------------------------------------------------------------------
//                           Bounds preparing
//------------------------------------------------------------------------------
anychart.controls.legend.ColumnBasedLegend.prototype.initializeItems = function() {
    goog.base(this, 'initializeItems');
    this.initializeLayoutBase();
};
//calculate items in columns and columns sizes
anychart.controls.legend.ColumnBasedLegend.prototype.initializeLayoutBase = function() {
    var legendItemsCount = this.visualItems_.length;
    this.itemsInColumn_ = Math.ceil(legendItemsCount / this.columns_);
    this.columns_ = Math.ceil(legendItemsCount / this.itemsInColumn_);

    this.columnsWidths_ = [];
    this.columnsMaxItemWidths_ = [];
    this.summColumnsWidth_ = 0;
    this.columnsHeight_ = 0;


    var columnWidth = 0;
    var columnHeight = 0;

    for (var i = 0; i < legendItemsCount; i++) {

        var item = this.visualItems_[i];

        columnWidth = Math.max(columnWidth, item.getContentBounds().width);
        columnHeight += item.getContentBounds().height;

        if (this.isLastInColumn_(i)) {

            this.columnsMaxItemWidths_.push(columnWidth);

            if (!this.isLastColumn_(this.columnsWidths_.length))
                columnWidth += this.columnSpace_;

            this.columnsWidths_.push(columnWidth);
            this.summColumnsWidth_ += columnWidth;
            this.columnsHeight_ = Math.max(columnHeight, this.columnsHeight_);

            columnWidth = 0;
            columnHeight = 0;
        } else {
            columnHeight += this.rowSpace_;
        }
    }
};
/**
 * @return {Boolean}
 */
anychart.controls.legend.ColumnBasedLegend.prototype.isLastInColumn_ = function(index) {
    return index == (this.itemsInColumn_ - 1) ||
        index == (this.visualItems_.length - 1) ||
        (index + 1) % this.itemsInColumn_ == 0;
};

anychart.controls.legend.ColumnBasedLegend.prototype.isLastColumn_ = function(index) {
    return (index + 1) == this.columns_;
};
//------------------------------------------------------------------------------
//                           Bounds calculation
//------------------------------------------------------------------------------
anychart.controls.legend.ColumnBasedLegend.prototype.checkControlBounds = function() {
    this.initializeItems();

    this.left_ = 0;
    this.top_ = 0;

    this.elementsAlign_ = this.realElementsAlign_;

    this.scrollBarSpace_ = 0;

    if (!this.layout_.isHeightSetted())
        this.bounds_.height = this.columnsHeight_;

    this.checkHeight_();

    if (!this.layout_.isWidthSetted())
        this.bounds_.width = this.summColumnsWidth_ + this.scrollBarSpace_;
    else
        this.calculateLeftOffset_();

    goog.base(this, 'checkControlBounds');
};

anychart.controls.legend.ColumnBasedLegend.prototype.calculateLeftOffset_ = function() {
    this.left_ = 0;
    var w = this.getContentWidth() - this.scrollBarSpace_;
    if (this.summColumnsWidth_ < w) {
        switch (this.realElementsAlign_) {
            case anychart.layout.HorizontalAlign.CENTER:
                this.left_ = (w - this.summColumnsWidth_) / 2;
                break;
            case anychart.layout.HorizontalAlign.RIGHT:
                this.left_ = w - this.summColumnsWidth_;
                break;
        }
    } else {
        this.elementsAlign_ = anychart.layout.HorizontalAlign.LEFT;
    }
};

anychart.controls.legend.ColumnBasedLegend.prototype.checkHeight_ = function() {
    var maxHeight = this.getMaxContentHeight();
    if (!this.layout_.isHeightSetted()) {
        if (this.bounds_.height > maxHeight) {
            this.bounds_.height = maxHeight;
            //this.createScrollBar(); todo : implement
        } else {
            //this.removeScrollBar(); todo : implement
        }
    } else {
        if (this.columnsHeight_ > this.getContentHeight())
            var m; //мусор, дабы не убивать ифы
        //this.createScrollBar(); todo : implement
        else
            var g; //мусор, дабы не убивать ифы
        //this.removeScrollBar(); todo : implement
    }
    this.realContentHeight_ = this.columnsHeight_;
};
//------------------------------------------------------------------------------
//                           Drawing
//------------------------------------------------------------------------------
anychart.controls.legend.ColumnBasedLegend.prototype.distributeItems = function() {
    if (this.columnsSeparator_)
        var columnSeparatorBounds = new anychart.utils.geom.Rectangle(
            0,
            0,
            this.columnsSeparator_.getThickness(),
            this.columnsHeight_);

    if (this.rowsSeparator_)
        var rowsSeparatorBounds = new anychart.utils.geom.Rectangle(
            0,
            0,
            0,
            this.rowsSeparator_.getThickness());

    var svgManager = this.itemsSprite_.getSVGManager();
    var path, pathData, style, sX, sY;
    var x = this.left_;
    var y = this.top_;

    var cnt = this.visualItems_.length;
    var columnIndex = 0;

    var xSpace = this.columnSpace_ / 2;
    var xSpace1 = xSpace;

    if (this.columnsSeparator_) {
        xSpace += this.columnsSeparator_.getPadding() / 2;
        xSpace1 -= this.columnsSeparator_.getPadding() / 2;
    }

    for (var i = 0; i < cnt; i++) {
        var item = this.visualItems_[i];
        item.setX(x);
        item.setY(y);
        switch (this.elementsAlign_) {
            case anychart.layout.HorizontalAlign.CENTER:
                item.incrementX((this.columnsMaxItemWidths_[columnIndex] - item.getContentBounds().width) / 2);
                break;
            case anychart.layout.HorizontalAlign.RIGHT:
                item.incrementX((this.columnsMaxItemWidths_[columnIndex] - item.getContentBounds().width));
                break;
        }

        if (this.isLastInColumn_(i)) {
            x += this.columnsWidths_[columnIndex];
            y = 0;

            if (this.columnsSeparator_ && !this.isLastColumn_(columnIndex)) {
                //draw columns separator
                sX = x - this.columnSpace_ / 2;
                if (columnSeparatorBounds)
                    columnSeparatorBounds.x = sX - this.columnsSeparator_.getThickness() / 2;

                path = svgManager.createPath();
                pathData = svgManager.pMove(sX, 0);
                pathData += svgManager.pLine(sX, this.columnsHeight_);
                style = this.columnsSeparator_.getSVGStroke(svgManager, columnSeparatorBounds);
                path.setAttribute('d', pathData);
                path.setAttribute('style', style);
                this.separatorsSprite_.appendChild(path);
            }

            columnIndex++;
        } else {
            y += item.getContentBounds().height + this.rowSpace_;

            if (this.rowsSeparator_) {
                //draw row separator
                sY = y - this.rowSpace_ / 2;
                sX = x;
                var w = this.columnsWidths_[columnIndex] - xSpace;
                if (columnIndex > 0) {
                    sX -= xSpace1;
                    w += xSpace1;
                    if (this.isLastColumn_(columnIndex))
                        w += xSpace;
                } else if (this.isLastColumn_(columnIndex)) {
                    w += xSpace;
                }

                if (rowsSeparatorBounds) {
                    rowsSeparatorBounds.width = w;
                    rowsSeparatorBounds.x = sX;
                    rowsSeparatorBounds.y = sY - this.rowsSeparator_.getThickness() / 2;
                }

                path = svgManager.createPath();
                pathData = svgManager.pMove(sX, sY);
                pathData += svgManager.pLine(sX + w, sY);
                style = this.rowsSeparator_.getSVGStroke(svgManager, rowsSeparatorBounds);
                path.setAttribute('d', pathData);
                path.setAttribute('style', style);
                this.separatorsSprite_.appendChild(path);
            }
        }
    }
};
//------------------------------------------------------------------------------
//
//                           LegendRow class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.LegendRow = function() {
    this.items_ = [];
    this.width_ = 0;
    this.height_ = 0;
    this.y_ = 0;
};
//------------------------------------------------------------------------------
//                       Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<anychart.controls.legend.LegendItem>}
 */
anychart.controls.legend.LegendRow.prototype.items_ = null;
/**
 * @return {Array.<anychart.controls.legend.LegendItem>}
 */
anychart.controls.legend.LegendRow.prototype.getRowItems = function() {
    return this.items_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendRow.prototype.width_ = null;
/**
 * @return {Number}
 */
anychart.controls.legend.LegendRow.prototype.getWidth = function() {
    return this.width_
};
/**
 * @param {Number} value
 */
anychart.controls.legend.LegendRow.prototype.setWidth = function(value) {
    this.width_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendRow.prototype.height_ = null;
/**
 * @return {Number}
 */
anychart.controls.legend.LegendRow.prototype.getHeight = function() {
    return this.height_;
};
/**
 * @param {Number} value
 */
anychart.controls.legend.LegendRow.prototype.setHeight = function(value) {
    this.height_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.LegendRow.prototype.y_ = null;
/**
 * @param {Number} value
 */
anychart.controls.legend.LegendRow.prototype.setY = function(value) {
    this.y_ = value;
};
/**
 * @return {Number}
 */
anychart.controls.legend.LegendRow.prototype.getY = function() {
    return this.y_;
};
//------------------------------------------------------------------------------
//                       Items
//------------------------------------------------------------------------------
anychart.controls.legend.LegendRow.prototype.offsetItems = function(xOffset) {
    var itemsCount = this.items_.length;
    for (var i = 0; i < itemsCount; i++) {
        this.items_[i].incrementX(xOffset);
    }
};

anychart.controls.legend.LegendRow.prototype.spreadItems = function(maxWidth) {
    if (this.width_ >= maxWidth) return;
    var itemsCnt = this.items_.length;
    var itemSpace = maxWidth / itemsCnt;

    for (var i = 0; i < itemsCnt; i++) {
        var item = this.items_[i];
        item.setX(itemSpace * i + (itemSpace - item.getContentBounds().width) / 2);
    }
};
//------------------------------------------------------------------------------
//
//                           RowBasedLegend class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.legend.RowBasedLegend = function() {
    anychart.controls.legend.LegendControl.call(this);
};
goog.inherits(anychart.controls.legend.RowBasedLegend,
    anychart.controls.legend.LegendControl);
//------------------------------------------------------------------------------
//                       Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.legend.RowBasedLegend.prototype.maxRowWidth_ = null;
/**
 * @private
 * @type {Array.<anychart.controls.legend.RowBasedLegend>}
 */
anychart.controls.legend.RowBasedLegend.prototype.rows_ = null;
//------------------------------------------------------------------------------
//                       Bounds
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.legend.RowBasedLegend.prototype.checkControlBounds = function() {
    this.initializeItems();
    this.elementsAlign_ = this.realElementsAlign_;
    //this.hideScrollBar();
    this.initializeLayoutBase();
    goog.base(this, 'checkControlBounds');
};

/**@private*/
anychart.controls.legend.RowBasedLegend.prototype.initializeLayoutBase = function() {

    var maxWidth = this.layout_.isWidthSetted() ? this.getContentWidth() : this.getMaxContentWidth();
    var maxHeight = this.layout_.isHeightSetted() ? this.getContentHeight() : this.getMaxContentHeight();

    this.calcLayout_(maxWidth, maxHeight);

    if (!this.layout_.isWidthSetted())
        this.bounds_.width = this.maxRowWidth_ + this.scrollBarSpace_;

    if (!this.layout_.isHeightSetted())
        this.bounds_.height = Math.min(this.realContentHeight_, maxHeight);

    if (this.realContentHeight_ > maxHeight) {
        //this.showScrollBar();
        this.calcLayout_(maxWidth - this.scrollBarSpace_, maxHeight);

        if (!this.layout_.isWidthSetted())
            this.bounds_.width = this.maxRowWidth_ + this.scrollBarSpace_;

        if (!this.layout_.isHeightSetted())
            this.bounds_.height = Math.min(this.realContentHeight_, maxHeight);
    }
};
/**
 * @private
 * @param {Number} maxWidth
 */
anychart.controls.legend.RowBasedLegend.prototype.calcLayout_ = function(maxWidth) {

    this.maxRowWidth_ = 0;
    this.rows_ = [];

    var cnt = this.visualItems_.length;

    this.realContentHeight_ = 0;

    var x = 0;
    var y = 0;

    var isInNextRow = false;
    var row = new anychart.controls.legend.LegendRow();
    row.setY(y);

    for (var i = 0; i < cnt; i++) {

        var item = this.visualItems_[i];

        isInNextRow = this.isNotInCurrentRow_(item, x, maxWidth);
        if (isInNextRow) {
            row.setWidth(x - this.columnSpace_);
            this.maxRowWidth_ = Math.max(row.getWidth(), this.maxRowWidth_);
            this.rows_.push(row);

            y += row.getHeight() + this.rowSpace_;
            x = 0;

            row = new anychart.controls.legend.LegendRow();
            row.setY(y);
        }

        item.setX(x);
        item.setY(y);

        row.setHeight(Math.max(row.getHeight(), item.contentBounds_.height));
        x += item.getContentBounds().width + this.columnSpace_;
        row.getRowItems().push(item);
    }

    //add last row
    row.setWidth(x - this.columnSpace_);
    this.rows_.push(row);
    this.maxRowWidth_ = Math.max(row.getWidth(), this.maxRowWidth_);
    y += row.getHeight();
    this.realContentHeight_ = y;
};
/**
 * @private
 * @param {anychart.controls.legend.LegendItem} item
 * @param {Number} x
 * @param {Number} maxWidth
 */
anychart.controls.legend.RowBasedLegend.prototype.isNotInCurrentRow_ = function(item, x, maxWidth) {
    return x + item.getContentBounds().width > maxWidth;
};
//------------------------------------------------------------------------------
//                       Drawing
//------------------------------------------------------------------------------
anychart.controls.legend.RowBasedLegend.prototype.distributeItems = function() {
    if (this.columnsSeparator_)
        var columnSeparatorBounds = new anychart.utils.geom.Rectangle(
            0,
            0,
            this.columnsSeparator_.getThickness(),
            0);

    if (this.rowsSeparator_)
        var rowsSeparatorBounds = new anychart.utils.geom.Rectangle(0,
            0,
            this.maxRowWidth_,
            this.rowsSeparator_.getThickness());

    var path, pathData, style;
    var svgManager = this.itemsSprite_.getSVGManager();
    var rowsCount = this.rows_.length;

    for (var i = 0; i < rowsCount; i++) {
        var row = this.rows_[i];
        var rowItemsCount = row.getRowItems().length;

        var offset = 0;

        if (this.scrollBarSpace_ == 0) {

            if (this.layout_.getAlign() == anychart.controls.layout.ControlAlign.SPREAD) {
                row.spreadItems(this.getContentWidth());
            } else {
                switch (this.elementsAlign_) {
                    case anychart.layout.HorizontalAlign.CENTER:
                        offset = (this.getContentWidth() - this.maxRowWidth_) / 2;
                        row.offsetItems((this.getContentWidth() - row.getWidth()) / 2);
                        break;
                    case anychart.layout.HorizontalAlign.RIGHT:
                        offset = this.getContentWidth() - this.maxRowWidth_;
                        row.offsetItems(this.getContentWidth() - row.getWidth());
                        break;
                }
            }
        }

        if (this.columnsSeparator_) {
            var columnsSeparatorY = row.getY();
            var columnsSeparatorHeight = row.getHeight();

            if (i == 0 && rowsCount > 0) {
                columnsSeparatorHeight += this.rowSpace_ / 2;
                if (this.rowsSeparator_)
                    columnsSeparatorHeight -= this.rowsSeparator_.getPadding() / 2;
            } else if (i > 0 && (i == rowsCount - 1)) {
                var topSpace = this.rowSpace_ / 2;
                if (this.rowsSeparator_)
                    topSpace -= this.rowsSeparator_.getPadding() / 2;
                columnsSeparatorHeight += topSpace;
                columnsSeparatorY -= topSpace;
            } else {
                var space = this.rowSpace_;
                if (this.rowsSeparator_)
                    space -= this.rowsSeparator_.getPadding();
                columnsSeparatorHeight += space;
                columnsSeparatorY -= space / 2;
            }

            for (var j = 0; j < rowItemsCount; j++) {
                if (j < (rowItemsCount - 1)) {
                    var item = row.getRowItems()[j];
                    var separatorX = item.getX() + item.getContentBounds().width + this.columnSpace_ / 2;
                    columnSeparatorBounds.x = separatorX - this.columnsSeparator_.thickness / 2;
                    columnSeparatorBounds.y = columnsSeparatorY;
                    columnSeparatorBounds.height = columnsSeparatorHeight;
                    path = svgManager.createPath();
                    pathData = svgManager.pMove(separatorX, columnsSeparatorY);
                    pathData += svgManager.pLine(separatorX, columnsSeparatorY + columnsSeparatorHeight);
                    style = this.columnsSeparator_.getSVGStroke(svgManager, columnSeparatorBounds);
                    path.setAttribute('d', pathData);
                    path.setAttribute('style', style);
                    this.separatorsSprite_.appendChild(path);
                }
            }
        }

        if (this.rowsSeparator_ && i < (rowsCount - 1)) {
            var rowsSeparatorY = row.getY() + row.getHeight() + this.rowSpace_ / 2;
            rowsSeparatorBounds.y = rowsSeparatorY - this.rowsSeparator_.getThickness() / 2;

            path = svgManager.createPath();
            pathData = svgManager.pMove(offset, rowsSeparatorY);
            pathData += svgManager.pLine(offset + this.maxRowWidth_, rowsSeparatorY);
            style = this.rowsSeparator_.getSVGStroke(svgManager, rowsSeparatorBounds);
            path.setAttribute('d', pathData);
            path.setAttribute('style', style);
            this.separatorsSprite_.appendChild(path);
        }
    }
};
