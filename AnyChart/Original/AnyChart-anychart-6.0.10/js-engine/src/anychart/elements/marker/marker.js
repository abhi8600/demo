/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.marker.MarkerType}</li>
 *  <li>@class {anychart.elements.marker.BaseMarkerElement}</li>
 *  <li>@class {anychart.elements.marker.MarkerDrawer}.</li>
 * <ul>
 */
goog.provide('anychart.elements.marker');
goog.require('anychart.elements');
goog.require('anychart.elements.marker.styles');
//------------------------------------------------------------------------------
//
//                           MarkerType class.
//
//------------------------------------------------------------------------------
/**
 * Describe possible marker types.
 * @enum {int}
 * @return {int} Marker type.
 */
anychart.elements.marker.MarkerType = {
    NONE: -1,
    CIRCLE: 0,
    SQUARE: 1,
    DIAMOND: 2,
    CROSS: 3,
    DIAGONAL_CROSS: 4,
    H_LINE: 5,
    V_LINE: 6,
    STAR_4: 7,
    STAR_5: 8,
    STAR_6: 9,
    STAR_7: 10,
    STAR_10: 11,
    TRIANGLE_UP: 12,
    TRIANGLE_DOWN: 13,
    IMAGE: 14,
    deserialize: function(type) {
        switch (type) {
            default:
            case 'none': return anychart.elements.marker.MarkerType.NONE;
            case 'circle': return anychart.elements.marker.MarkerType.CIRCLE;
            case 'square': return anychart.elements.marker.MarkerType.SQUARE;
            case 'diamond': return anychart.elements.marker.MarkerType.DIAMOND;
            case 'cross': return anychart.elements.marker.MarkerType.CROSS;
            case 'diagonalcross':
                return anychart.elements.marker.MarkerType.DIAGONAL_CROSS;
            case 'hline': return anychart.elements.marker.MarkerType.H_LINE;
            case 'vline': return anychart.elements.marker.MarkerType.V_LINE;
            case 'star4': return anychart.elements.marker.MarkerType.STAR_4;
            case 'star5': return anychart.elements.marker.MarkerType.STAR_5;
            case 'star6': return anychart.elements.marker.MarkerType.STAR_6;
            case 'star7': return anychart.elements.marker.MarkerType.STAR_7;
            case 'star10': return anychart.elements.marker.MarkerType.STAR_10;
            case 'triangleup':
                return anychart.elements.marker.MarkerType.TRIANGLE_UP;
            case 'triangledown':
                return anychart.elements.marker.MarkerType.TRIANGLE_DOWN;
            case 'image': return anychart.elements.marker.MarkerType.IMAGE;
        }
    }
};
//------------------------------------------------------------------------------
//
//                           BaseMarkerElement class.
//
//------------------------------------------------------------------------------
/**
 * Class represent base marker element.
 * @constructor
 * @extends {anychart.elements.BaseElement}
 */
anychart.elements.marker.BaseMarkerElement = function() {
    anychart.elements.BaseElement.apply(this);
    this.type_ = anychart.elements.marker.MarkerType.NONE;
    this.markerDrawer_ = new anychart.elements.marker.MarkerDrawer();
};
//inheritance
goog.inherits(anychart.elements.marker.BaseMarkerElement,
        anychart.elements.BaseElement);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Marker type.
 * @private
 * @type {anychart.elements.marker.MarkerType} Marker type.
 */
anychart.elements.marker.BaseMarkerElement.prototype.type_ = 0;
/**
 * @return {int} Type.
 */
anychart.elements.marker.BaseMarkerElement.prototype.getType = function() {
    return this.type_;
};
/**
 * @private
 * @type {anychart.elements.marker.MarkerDrawer}
 */
anychart.elements.marker.BaseMarkerElement.prototype.markerDrawer_ = null;
//------------------------------------------------------------------------------
//                          Override
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.getStyleNodeName =
    function() {
        return 'marker_style';
    };
/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.createStyle = function() {
    return new anychart.elements.marker.styles.MarkerElementStyle();
};
/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.getElementContainer =
    function(container) {
        return container.getPlot().getMarkersSprite();
    };
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.deserializeSeriesSettings =
    function(markerNode, stylesList) {
        goog.base(this, 'deserializeSeriesSettings', markerNode, stylesList);
        this.deserializeMarkerType_(markerNode);
    };

/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.deserializePointSettings =
    function(markerNode, stylesList) {
        goog.base(this, 'deserializePointSettings', markerNode, stylesList);
        this.deserializeMarkerType_(markerNode);
    };

/**
 * Deserialize marker type.
 * @private
 * @param {Object} markerNode Marker node.
 */
anychart.elements.marker.BaseMarkerElement.prototype.deserializeMarkerType_ =
    function(markerNode) {
        if (anychart.utils.deserialization.hasProp(markerNode, 'type'))
            this.type_ = anychart.elements.marker.MarkerType.deserialize(
                    anychart.utils.deserialization.getLStringProp(
                            markerNode,
                            'type'));
    };

//------------------------------------------------------------------------------
//
//                          Visual
//
//------------------------------------------------------------------------------
/**
 * @return {String} Marker path.
 * @param {int} type Type.
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {anychart.elements.marker.styles.MarkerElementStyleState} state State.
 */
anychart.elements.marker.BaseMarkerElement.prototype.getMarkerPath =
    function(type, svgManager, state) {
        return this.markerDrawer_.getSVGPathData(
                type,
                svgManager,
                state.getSize(),
                0,
                0,
                state.getWidth(),
                state.getHeight());
    };

/**
 * @inheritDoc
 */
anychart.elements.marker.BaseMarkerElement.prototype.getBounds =
    function(container, state, sprite) {
        return state.getBounds();
    };
//------------------------------------------------------------------------------
//
//                          Utils
//
//------------------------------------------------------------------------------
/**
 * Return copy of base marker element.
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.elements.marker.BaseMarkerElement} Marker element.
 */
anychart.elements.marker.BaseMarkerElement.prototype.copy =
    function(opt_target) {
        if (!opt_target) opt_target =
                new anychart.elements.marker.BaseMarkerElement();
        opt_target.type_ = this.type_;
        return goog.base(this, 'copy', opt_target);
    };

//------------------------------------------------------------------------------
//
//                          Marker Drawer
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.elements.marker.MarkerDrawer = function() {
};
/**
 * @param {int} type Marker type.
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @param {Number} width Width.
 * @param {Number} height Height.
 * @return {String} SVG path data.
 */
anychart.elements.marker.MarkerDrawer.prototype.getSVGPathData =
    function(type, svgManager, size, dx, dy, width, height) {
        switch (type) {
            default:
            case anychart.elements.marker.MarkerType.NONE:
                return null;
            case anychart.elements.marker.MarkerType.CIRCLE:
                return this.drawCircle_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.SQUARE:
                if (!height) height = size;
                if (!width) width = size;
                return this.drawSquare_(svgManager, width, height, dx, dy);
            case anychart.elements.marker.MarkerType.DIAMOND:
                return this.drawDiamond_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.CROSS:
                return this.drawCross_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.DIAGONAL_CROSS:
                return this.drawDiagonalCross_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.H_LINE:
                return this.drawHLine_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.V_LINE:
                return this.drawVLine_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.TRIANGLE_DOWN:
                return this.drawTriangleDown_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.TRIANGLE_UP:
                return this.drawTriangleUp_(svgManager, size, dx, dy);
            case anychart.elements.marker.MarkerType.STAR_4:
                return this.drawPrettyStar_(svgManager, size, dx, dy, 4);
            case anychart.elements.marker.MarkerType.STAR_5:
                return this.drawPrettyStar_(svgManager, size, dx, dy, 5);
            case anychart.elements.marker.MarkerType.STAR_6:
                return this.drawStar_(svgManager, size, dx, dy, 6);
            case anychart.elements.marker.MarkerType.STAR_7:
                return this.drawPrettyStar_(svgManager, size, dx, dy, 7);
            case anychart.elements.marker.MarkerType.STAR_10:
                return this.drawStar_(svgManager, size, dx, dy, 10);
        }
    };
/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawCircle_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx, dy + size / 2);
        path += svgManager.pCircleArc(size / 2, false, true,
                dx + size / 2, dy);
        path += svgManager.pCircleArc(size / 2, false, true,
                dx + size, dy + size / 2);
        path += svgManager.pCircleArc(size / 2, false, true,
                dx + size / 2, dy + size);
        path += svgManager.pCircleArc(size / 2, false, true,
                dx, dy + size / 2);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} width Width.
 * @param {Number} heigth Height.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawSquare_ =
    function(svgManager, width, heigth, dx, dy) {
        dx = dx + 1;
        dy = dy + 1;
        width = width - 2;
        heigth = heigth - 2;
        var path = svgManager.pMove(dx, dy);
        path += svgManager.pLine(dx + width, dy);
        path += svgManager.pLine(dx + width, dy + heigth);
        path += svgManager.pLine(dx, dy + heigth);
        path += svgManager.pLine(dx, dy);
        path += svgManager.pFinalize();
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawDiamond_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx + size / 2, dy);
        path += svgManager.pLine(dx + size, dy + size / 2);
        path += svgManager.pLine(dx + size / 2, dy + size);
        path += svgManager.pLine(dx, dy + size / 2);
        path += svgManager.pLine(dx + size / 2, dy);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawCross_ =
    function(svgManager, size, dx, dy) {
        var w = size / 4;
        var path = svgManager.pMove(dx + size / 2 - w / 2, dy);
        path += svgManager.pLine(dx + size / 2 - w / 2, dy + size / 2 - w / 2);
        path += svgManager.pLine(dx, dy + size / 2 - w / 2);
        path += svgManager.pLine(dx, dy + size / 2 + w / 2);
        path += svgManager.pLine(dx + size / 2 - w / 2, dy + size / 2 + w / 2);
        path += svgManager.pLine(dx + size / 2 - w / 2, dy + size);
        path += svgManager.pLine(dx + size / 2 + w / 2, dy + size);
        path += svgManager.pLine(dx + size / 2 + w / 2, dy + size / 2 + w / 2);
        path += svgManager.pLine(dx + size, dy + size / 2 + w / 2);
        path += svgManager.pLine(dx + size, dy + size / 2 - w / 2);
        path += svgManager.pLine(dx + size / 2 + w / 2, dy + size / 2 - w / 2);
        path += svgManager.pLine(dx + size / 2 + w / 2, dy);
        path += svgManager.pLine(dx + size / 2 - w / 2, dy);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawDiagonalCross_ =
    function(svgManager, size, dx, dy) {
        var dw = size / 4;
        var d = dw * Math.SQRT2 / 2;
        var path = svgManager.pMove(dx + d, dy);
        path += svgManager.pLine(dx + size / 2, dy + size / 2 - d);
        path += svgManager.pLine(dx + size - d, dy);
        path += svgManager.pLine(dx + size, dy + d);
        path += svgManager.pLine(dx + size / 2 + d, dy + size / 2);
        path += svgManager.pLine(dx + size, dy + size - d);
        path += svgManager.pLine(dx + size - d, dy + size);
        path += svgManager.pLine(dx + size / 2, dy + size / 2 + d);
        path += svgManager.pLine(dx + d, dy + size);
        path += svgManager.pLine(dx, dy + size - d);
        path += svgManager.pLine(dx + size / 2 - d, dy + size / 2);
        path += svgManager.pLine(dx, dy + d);
        path += svgManager.pLine(dx + d, dy);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawHLine_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx, dy + (size - size / 4) / 2);
        path += svgManager.pLine(dx + size, dy + (size - size / 4) / 2);
        path += svgManager.pLine(dx + size,
                dy + (size - size / 4) / 2 + size / 4);
        path += svgManager.pLine(dx, dy + (size - size / 4) / 2 + size / 4);
        path += svgManager.pLine(dx, dy + (size - size / 4) / 2);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawVLine_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx + (size - size / 4) / 2, dy);
        path += svgManager.pLine(dx + (size - size / 4) / 2 + size / 4, dy);
        path += svgManager.pLine(dx + (size - size / 4) / 2 + size / 4,
                dy + size);
        path += svgManager.pLine(dx + (size - size / 4) / 2, dy + size);
        path += svgManager.pLine(dx + (size - size / 4) / 2, dy);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @param {int} n Number.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawPrettyStar_ =
    function(svgManager, size, dx, dy, n) {
        var path = '';
        var R = size / 2;
        var r = R / 2;
        var da = (Math.PI * 2) / n;
        var outA = -Math.PI / 2;
        var inA = outA + da / 2;

        for (var i = 0; i <= n; i++) {
            var outX = R * Math.cos(outA);
            var outY = R * Math.sin(outA);
            var inX = r * Math.cos(inA);
            var inY = r * Math.sin(inA);

            outX += R;
            outY += R;
            inX += R;
            inY += R;

            if (i == 0) path += svgManager.pMove(dx + outX, dy + outY);
            else path += svgManager.pLine(dx + outX, dy + outY);
            path += svgManager.pLine(dx + inX, dy + inY);

            outA += da;
            inA += da;
        }
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @param {int} n Number.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawStar_ = function(svgManager,
                                                                     size, dx,
                                                                     dy, n) {
    var R = size / 2;
    var alpha = Math.PI * 2 / n;
    var pointsOut = [];
    var i;
    var path = '';
    for (i = 0; i <= n; i++)
        pointsOut.push(new anychart.utils.geom.Point(-R * Math.sin(i * alpha),
                -R * Math.cos(i * alpha)));

    //get inner radius
    var aP = pointsOut[0];
    var bP = pointsOut[1];
    var cP = pointsOut[2];

    var dP = new anychart.utils.geom.Point();
    dP.x = (aP.x - cP.x) * (bP.y - cP.y) / (aP.y - cP.y) + cP.x;
    dP.y = bP.y;

    var pOut;
    var pIn;

    var r = Math.sqrt(Math.pow(dP.x, 2) + Math.pow(dP.y, 2));
    var pointsIn = [];

    for (i = 0; i <= n; i++)
        pointsIn.push(
            new anychart.utils.geom.Point(-r * Math.sin(i * alpha + alpha / 2),
                    -r * Math.cos(i * alpha + alpha / 2)));

    path = svgManager.pMove(dx + aP.x + R, dy + aP.y + R);
    var k = 0;
    for (i = 0; i <= 2 * n; i += 2) {
        pOut = pointsOut[k];
        pIn = pointsIn[k];
        path += svgManager.pLine(dx + pOut.x + R, dy + pOut.y + R);
        path += svgManager.pLine(dx + pIn.x + R, dy + pIn.y + R);
        k++;
    }
    return path;
};
/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawTriangleDown_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx + size / 2, dy + size);
        path += svgManager.pLine(dx + size, dy);
        path += svgManager.pLine(dx, dy);
        path += svgManager.pLine(dx + size / 2, dy + size);
        return path;
    };

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @param {Number} size Size.
 * @param {Number} dx dx.
 * @param {Number} dy dy.
 * @return {String} Path.
 */
anychart.elements.marker.MarkerDrawer.prototype.drawTriangleUp_ =
    function(svgManager, size, dx, dy) {
        var path = svgManager.pMove(dx + size / 2, dy);
        path += svgManager.pLine(dx + size, dy + size);
        path += svgManager.pLine(dx, dy + size);
        path += svgManager.pLine(dx + size / 2, dy);
        return path;
    };
