goog.provide('anychart.plots.gaugePlot.gauges.indicator');

goog.require('anychart.plots.gaugePlot.frames.linear');

/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge = function() {
    anychart.plots.gaugePlot.gauges.GaugeBase.call(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge,
              anychart.plots.gaugePlot.gauges.GaugeBase);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.createFrame = function() {
    return new anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame(this);
};
