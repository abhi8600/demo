/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.CircularGauge}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.circular');

goog.require('anychart.plots.gaugePlot.frames.circular');
goog.require('anychart.plots.gaugePlot.gauges.circular.markers');
goog.require('anychart.plots.gaugePlot.gauges.circular.pointers');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.circular.CircularGauge class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge = function () {
    anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.call(this);
    this.pivotPoint_ = new anychart.utils.geom.Point(.5, .5);
    this.pixPivotPoint_ = new anychart.utils.geom.Point();
    this.maxRadius = 0;
    this.minAngle = 72000;
    this.maxAngle = -72000;
    this.maxCapRadius = 0;
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.CircularGauge,
    anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge);

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.maxPixelRadius_ = null;
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getMaxPixelRadius = function () {
    return this.maxPixelRadius_;
};
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.setMaxPixelRadius = function (pixRadius) {
    this.maxPixelRadius_ = pixRadius;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.frameRadius_ = null;
/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getFrameRadius = function () {
    return this.frameRadius_;
};

/**
 * @param {number} radius
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.setFrameRadius = function (radius) {
    this.frameRadius_ = radius;
};

/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.pivotPoint_ = null;

/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getPivotPoint = function () {
    return this.pivotPoint_;
};

/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.pixPivotPoint_ = null;

/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getPixPivotPoint = function () {
    return this.pixPivotPoint_;
};
/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.maxRadius = null;
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.setMaxRadius = function(value) {
    this.maxRadius = value;
};

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.minAngle = null;
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getMinAngle = function () {
    return this.minAngle;
};

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.maxAngle = null;
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getMaxAngle = function () {
    return this.maxAngle;
};

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.maxCapRadius = null;
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getMaxCapRadius = function () {
    return this.maxCapRadius
};

/**
 * ND: Needs doc!
 * @param {*} svgManager
 * @param {*} bounds
 * @param {*} tooltipsContainer
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.initialize = function (svgManager, bounds, tooltipsContainer) {
    goog.base(this, 'initialize', svgManager, bounds, tooltipsContainer);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite.setId('CircularGauge');
    return this.sprite;
};
/**
 * ND: Needs doc!
 * @param {Object} config
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.deserialize = function (config) {
    goog.base(this, 'deserialize', config);
    var des = anychart.utils.deserialization;
    if (des.getProp(config, 'pivot_point')) {
        var p = des.getProp(config, 'pivot_point');
        this.pivotPoint_.x = des.getSimplePercent(p.x);
        this.pivotPoint_.y = des.getSimplePercent(p.y);
    }
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.createPointerByType = function (type) {
    switch (type) {
        case 'bar':
            return new anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer();
        case 'rangebar':
            return new anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer();
        case 'marker':
            return new anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer();
        case 'knob':
            return new anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer();
        default:
            return new anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer();
    }
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.checkPointer = function (pointer) {
    this.checkStateCap_(pointer, pointer.getStyle().getNormal());
    this.checkStateCap_(pointer, pointer.getStyle().getHover());
    this.checkStateCap_(pointer, pointer.getStyle().getPushed());
    this.checkStateCap_(pointer, pointer.getStyle().getSelectedNormal());
    this.checkStateCap_(pointer, pointer.getStyle().getSelectedHover());
};
/**
 * @private
 * @param {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer} pointer
 * @param {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState} state
 */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.checkStateCap_ = function (pointer, state) {
    var axis = pointer.getAxis();
    if (state.getCapStyle() != null)
        this.maxCapRadius = Math.max(this.getMaxCapRadius(),
            state.getCapStyle().getPercentRadius(axis));
    var r;
    if (state.getNeedle) {
        this.maxCapRadius = Math.max(this.getMaxCapRadius(), state.getNeedle().getPercentThickness(axis) / 2);
        r = state.getNeedle().getPercentBaseRadius(axis);
        if (r < 0)
            this.maxCapRadius = Math.max(-r, this.getMaxCapRadius());
    }
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.createFrame = function (config) {
    var des = anychart.utils.deserialization;
    if (config != null && des.hasProp(config, 'type')) {
        switch (des.getEnumProp(config, 'type')) {
            case "auto":
                return new anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto(this);
            case "rectangular":
                return new anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect(this);
            case "autorectangular":
                return new anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect(this);
        }
    }
    return new anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.createAxis = function (config, stylesList) {
    var axis = new anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis(this);
    axis.deserialize(config, stylesList);
    this.maxRadius = Math.max(axis.radius, this.maxRadius);
    this.minAngle = Math.min(axis.startAngle, this.minAngle);
    this.maxAngle = Math.max(axis.startAngle + axis.sweepAngle, this.maxAngle);
    return axis;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.getGaugeType = function () {
    return anychart.plots.gaugePlot.gauges.GaugeType.CIRCULAR;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGauge.prototype.setBounds = function (newBounds) {
    goog.base(this, 'setBounds', newBounds);

    if (this.frame.allowAutoFit()) {
        this.frame.autoFit(this.bounds)
    } else {
        var w = Math.min(this.bounds.width, this.bounds.height);
        this.maxPixelRadius_ = w;
        this.frameRadius_ = this.maxRadius * w;
    }
    this.pixPivotPoint_.x = this.bounds.x + this.pivotPoint_.x * this.bounds.width;
    this.pixPivotPoint_.y = this.bounds.y + this.pivotPoint_.y * this.bounds.height;
};


//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis = function (gauge) {
    goog.base(this, gauge);
    this.radius = .6;
    this.startAngle = 90;
    this.sweepAngle = 90;
    this.endAngle = this.startAngle + this.sweepAngle;
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis,
    anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis);

/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getLabelPosition = function (pixVal) {
    var pos = new anychart.utils.geom.Point();
    var r = this.getRadius(this.labels.getAlign(), this.labels.getPadding(), 0, false);
    var angle = pixVal * Math.PI / 180;

    var cos = Math.round(Math.cos(angle) * 1000) / 1000;
    var sin = Math.round(Math.sin(angle) * 1000) / 1000;

    var mAlign = anychart.plots.gaugePlot.layout.Align;
    var labelBounds = this.labels.getTextElement().getBounds();

    var dx = labelBounds.width / 2;
    var dy = labelBounds.height / 2;

    switch (this.labels.getAlign()) {
        case mAlign.OUTSIDE:
            r += dx * Math.abs(cos) + dy * Math.abs(sin);
            break;
        case mAlign.INSIDE:
            r -= dx * Math.abs(cos) + dy * Math.abs(sin);
            break;
    }

    pos.x = this.getGauge().getPixPivotPoint().x + r * cos - dx;
    pos.y = this.getGauge().getPixPivotPoint().y + r * sin - dy;

    return pos;
};


//------------------------------------------------------------------------------
//                      Methods.
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @param {*} config
 * @param {*} localStyles
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.deserialize = function (config, localStyles) {
    goog.base(this, 'deserialize', config, localStyles);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'radius')) this.radius = des.getSimplePercentProp(config, 'radius');
    if (des.hasProp(config, 'start_angle')) this.startAngle = des.getNumProp(config, 'start_angle')+90;
    if (des.hasProp(config, 'sweep_angle')) this.sweepAngle = des.getNumProp(config, 'sweep_angle');
    this.endAngle = this.startAngle + this.sweepAngle;
};

/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.initializeSize = function () {
    goog.base(this, 'initializeSize');

    this.pixelRadius_ = this.gauge.getMaxPixelRadius() * this.radius;
    this.pixelSize = this.pixelRadius_ * this.size;

    this.scale.setPixelRange(this.startAngle, this.endAngle);
    this.scale.optimalMajorSteps = (this.sweepAngle) / 10;
    this.scale.calculate();
};

/**
 * ND: Needs doc!
 * @param {Number} pixelR
 * @param {Number} gradAngle
 * @param {anychart.utils.geom.Point} point
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.setPixelPoint = function (pixelR, gradAngle, point) {
    var center = this.gauge.getPixPivotPoint();
    var radAngle = gradAngle * Math.PI / 180;
    point.x = center.x + pixelR * Math.cos(radAngle);
    point.y = center.y + pixelR * Math.sin(radAngle);
};

/**
 * ND: Needs doc!
 * @param {int} align
 * @param {Number} padding
 * @param {Number} width
 * @param {Boolean} isStartPoint
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getComplexRadius = function (align, padding, width, isStartPoint) {
    var mAlign = anychart.plots.gaugePlot.layout.Align;
    switch (align) {
        case mAlign.INSIDE:
            return this.pixelRadius_ * (1 - padding - (isStartPoint ? 0 : width)) - this.pixelSize / 2;
        case mAlign.OUTSIDE:
            return this.pixelRadius_ * (1 + padding + (isStartPoint ? 0 : width)) + this.pixelSize / 2;
        case mAlign.CENTER:
        default:
            return this.pixelRadius_ * (1 + (isStartPoint ? (-.5) : +(.5)) * width - padding);
    }
};

/**
 * ND: Needs doc!
 * @param {int} align
 * @param {Number} padding
 * @param {Number} width
 * @param {Boolean} isPixelWidth Default: false
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getRadius = function (align, padding, width, isPixelWidth) {
    var mAlign = anychart.plots.gaugePlot.layout.Align;
    switch (align) {
        case mAlign.INSIDE:
            return this.pixelRadius_ * (1 - (isPixelWidth ? 0 : width) - padding) - (isPixelWidth ? width : 0) - this.pixelSize / 2;
        case mAlign.OUTSIDE:
            return this.pixelRadius_ * (1 + padding) + this.pixelSize / 2;
        case mAlign.CENTER:
        default:
            return this.pixelRadius_ * (1 - (isPixelWidth ? 0 : width / 2)) - (isPixelWidth ? width / 2 : 0);
    }
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.drawTickmark = function (tickmark, value, color) {
    var svgManager = this.sprite.getSVGManager();
    var w = tickmark.getWidth() * this.pixelRadius_;
    var h = tickmark.getLength() * this.pixelRadius_;
    var size = Math.min(w, h);
    var mAlign = anychart.plots.gaugePlot.layout.Align;
    var r = this.getRadius(tickmark.getAlign(), tickmark.getPadding(), 0, false);
    switch (tickmark.getAlign()) {
        case mAlign.INSIDE:
            r -= h / 2;
            break;
        case mAlign.OUTSIDE:
            r += h / 2;
            break;
    }
    var du = anychart.utils.geom.DrawingUtils;
    var center = this.gauge.getPixPivotPoint();
    var x = center.x + du.getPointX(r, value) - w / 2;
    var y = center.y + du.getPointY(r, value) - h / 2;
    var res = tickmark.drawTickmark(svgManager, x, y, size, w, h, color);
    if (tickmark.autoRotate_) {
        res.setRotation(value - 90 + tickmark.getRotation(), x + w / 2, y + h / 2);
    } else {
        res.setRotation(tickmark.getRotation())
    }
    return res;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getScaleBarBounds = function (scaleBar) {
    var bounds = new anychart.utils.geom.Rectangle();
    var center = this.gauge.getPixPivotPoint();
    var isBarShape = scaleBar.getShapeType() == anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;
    var innerRadius = 0,
        outerRadius = 0;
    if (isBarShape) {
        innerRadius = this.getRadius(scaleBar.getAlign(), scaleBar.getPadding(), scaleBar.getSize(), false);
        outerRadius = innerRadius + this.getPixelRadius(1) * scaleBar.getSize();
    }
    bounds.x = center.x - outerRadius;
    bounds.y = center.y - outerRadius;
    bounds.width = outerRadius * 2;
    bounds.height = outerRadius * 2;
    return bounds;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getScaleBarPath = function (svgManager, scaleBar) {
    var center = this.gauge.getPixPivotPoint();
    var isBarShape = scaleBar.getShapeType() == anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;
    var innerRadius;
    if (isBarShape) {
        innerRadius = this.getRadius(scaleBar.getAlign(), scaleBar.getPadding(), scaleBar.getSize(), false);
        var outerRadius = innerRadius + this.pixelRadius_ * scaleBar.getSize();
    } else {
        innerRadius = this.getRadius(scaleBar.getAlign(), scaleBar.getPadding(), 0, false);
    }

    var startAngle;
    var endAngle;
    var min;
    if (this.endAngle % 360 != this.startAngle % 360) {
        endAngle = (this.endAngle) * Math.PI / 180;
        startAngle = this.startAngle * Math.PI / 180;
    } else {
        // В случае одной точки - криво рассчитываются координаты при изменении start_angle
        // для корректного рассчета сделано чтобы точка рисовалась по координатам как будто бы
        // она рисуется от угла=0 до угла=360
        min = Math.min(this.startAngle, this.endAngle);
        endAngle = (this.endAngle - min - 0.001) * Math.PI / 180;
        startAngle = (this.startAngle - min) * Math.PI / 180
    }

    var innerStartX = center.x + innerRadius * Math.cos(startAngle);
    var innerStartY = center.y + innerRadius * Math.sin(startAngle);
    var innerEndX = center.x + innerRadius * Math.cos(endAngle);
    var innerEndY = center.y + innerRadius * Math.sin(endAngle);

    var outerStartX = center.x + outerRadius * Math.cos(startAngle);
    var outerStartY = center.y + outerRadius * Math.sin(startAngle);
    var outerEndX = center.x + outerRadius * Math.cos(endAngle);
    var outerEndY = center.y + outerRadius * Math.sin(endAngle);

    var largeArc = 1, sweepArc = 1;
    if (this.sweepAngle < 180) {
        largeArc = 0;
    }

    var pathData = '';
    pathData += svgManager.pMove(innerStartX, innerStartY);
    pathData += svgManager.pCircleArc(innerRadius, largeArc, sweepArc, innerEndX, innerEndY, innerRadius);
    if (isBarShape) {
        pathData += svgManager.pLine(outerEndX, outerEndY);
        pathData += svgManager.pCircleArc(outerRadius, largeArc, !sweepArc, outerStartX, outerStartY, outerRadius);
        pathData += svgManager.pLine(innerStartX, innerStartY);
    }
    pathData += svgManager.pCircleArc(innerRadius, largeArc, !sweepArc, innerStartX, innerStartY, innerRadius);
    pathData += svgManager.pFinalize();
    return pathData;
};


//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.radius = null;

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.pixelRadius_ = null;

/**
 * ND: Needs doc!
 * @private
 * @param {Number} r
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.getPixelRadius = function (r) {
    return this.pixelRadius_ * r;
};

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.startAngle = null;

/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.sweepAngle = null;

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis.prototype.endAngle_ = null;
