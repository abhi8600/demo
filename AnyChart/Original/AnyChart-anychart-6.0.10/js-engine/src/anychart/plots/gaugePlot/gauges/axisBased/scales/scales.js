goog.provide('anychart.plots.gaugePlot.gauges.axisBased.scales');

goog.require('anychart.plots.axesPlot.scales');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale = function () {
    anychart.plots.axesPlot.scales.LinearScale.call(this);
    this.needCalcLabels = false;
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale,
    anychart.plots.axesPlot.scales.LinearScale);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale.prototype.checkMajorIntervalLabels = function () {
};

/**
 * @param {number} value
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale.prototype.linearize = function (value) {
    return value;
};
/**
 * @param {number} value
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale.prototype.delinearize = function (value) {
    return value;
};
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale = function () {
    goog.base(this);
    this.logBase_ = 10;
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale,
    anychart.scales.BaseScale);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Minor logarithmic values.
 * @public
 * @static
 * @type {Array.<number>}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.MINOR_LOG_VALUES = [
    0,0.301029995663981, 0.477121254719662, 0.602059991327962, 0.698970004336019,
    0.778151250383644, 0.845098040014257, 0.903089986991944, 0.954242509439325, 1
];

/**
 * ??
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.transformedMin_ = null;

/**
 * ??
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.transformedMax_ = null;

/**
 * Logarithm base value. Effective when scale type is set to logarithmic.
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.logBase_ = 10;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;

    if (!data) return;
    goog.base(this, 'deserialize', data);

    if (des.hasProp(data, 'log_base')) {
        var strLogBase = des.getLStringProp(data, 'log_base');
        this.logBase_ = strLogBase == 'e' ? Math.E : des.getNum(strLogBase);
    }
};
//------------------------------------------------------------------------------
//                      Calculation.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.calculate = function() {
    //minimum and maximum auto calculation
    if (isNaN(this.dataRangeMinimum_) && isNaN(this.dataRangeMaximum_) && this.isMinimumAuto_ && this.isMaximumAuto_) {
        this.isMinimumAuto_ = false;
        this.minimum_ = 1;
        this.isMaximumAuto_ = false;
        this.maximum_ = 10;
    }
    //calculate base property's
    goog.base(this, 'calculate');

    //auto calculate major interval
    if (this.isMajorIntervalAuto_)
        this.majorInterval_ = 1;

    //check minimum and maximum values
    if (this.minimum_ <= 0 && this.maximum_ <= 0) {
        this.minimum_ = 1;
        this.maximum_ = this.logBase_;
    } else if (this.minimum_ <= 0) {
        this.minimum_ = this.maximum_ / this.logBase_;
    } else if (this.maximum_ <= 0.0) {
        this.maximum_ = this.minimum_ * this.logBase_;
    }
    if (this.maximum_ - this.minimum_ < 1.0e-20) {
        if (this.isMaximumAuto_)
            this.maximum_ *= 2;
        if (this.isMinimumAuto_)
            this.minimum_ /= 2;
    }
    if (this.isMinimumAuto_)
        this.minimum_ = Math.pow(this.logBase_, Math.floor(Math.log(this.minimum_) / Math.log(this.logBase_)));
    if (this.isMaximumAuto_)
        this.maximum_ = Math.pow(this.logBase_, Math.ceil(Math.log(this.maximum_) / Math.log(this.logBase_)));

    this.transformedMin_ = Math.round(this.safeLog_(this.minimum_) * 1000) / 1000;
    this.transformedMax_ = Math.round(this.safeLog_(this.maximum_) * 1000) / 1000;

    //calculate other property's
    this.calculateBaseValue_();
    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};

/**
 * Calculate minor and minor interval count.
 * @protected
 * @override
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.calculateIntervalCount_ = function() {
    this.majorIntervalCount_ = parseInt(Math.floor(this.safeLog_(this.maximum_) + 1.0e-12)) - parseInt(this.baseValue_);
    if (this.getMajorIntervalValue(this.majorIntervalCount_) > this.safeLog_(this.maximum_))
        this.majorIntervalCount_--;
    if (this.majorIntervalCount_ < 1)
        this.majorIntervalCount_ = 1;
    this.minorIntervalCount_ = 9;
};
/**
 * Sets base value to 1, if not auto.
 * @protected
 * @override
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.calculateBaseValue_ = function() {
    if (!this.isBaseValueAuto_) return;
    this.baseValue_ = Math.ceil(this.safeLog_(this.minimum_) - 0.00000001);
};
/**
 * Calculate minor start interval.
 * @protected
 * @override
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.calculateMinorStartInterval_ = function() {
    this.minorStartInterval_ = -9;
};
/**
 * Calculate and return current major interval value.
 * <p>
 *  Note: multiplication and division by 10 necessary to fix bug with overload float values (2.00000000001)
 * <p>
 * @public
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.getMajorIntervalValue = function(index) {
    return this.baseValue_ +index;
};
/**
 * Calculate and return current minor interval value.
 * @public
 * @override
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.getMinorIntervalValue = function(baseValue, index) {
    return baseValue + Math.floor(Number(index) / 9.0) + anychart.plots.axesPlot.scales.LogScale.MINOR_LOG_VALUES[(index + 9) % 9];
};
//------------------------------------------------------------------------------
//                  Transfarmations.
//------------------------------------------------------------------------------
/**
 * Transform and return value in pixels to internal scale value.
 * @public
 * @override
 * @param {Number} pixValue value in pixels.
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.transformPixelToValue = function(pixValue) {
    var ratio = (pixValue - this.pixelRangeMinimum_) / (this.pixelRangeMaximum_ - this.pixelRangeMinimum_);
    var offset = (this.transformedMax_ - this.transformedMin_) * ratio;

    var pixMax = this.transformedMax_;
    var pixMin = this.transformedMin_;

    var res = this.inverted_ ? (pixMax - offset) : (pixMin + offset);

    if (res < this.transformedMin_) res = this.transformedMin_;
    else if (res > this.transformedMax_) res = this.transformedMax_;

    return this.delinearize(res);
};

/**
 * Transform from internal scale value to pixel value.
 * @public
 * @override
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.transformValueToPixel = function(value) {
    var ratio = (this.safeLog_(value) - this.transformedMin_) / (this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;
    if (this.inverted_) return this.pixelRangeMaximum_ - pixelOffset;
    return this.pixelRangeMinimum_ + pixelOffset;
};

/**
 * @public
 * @override
 * @param value
 * @param isZeroAtAxisZero
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.localTransform = function(value, isZeroAtAxisZero) {
    var ratio = (value - this.transformedMin_) / ( this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;

    var pixMax = isZeroAtAxisZero ? (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) : this.pixelRangeMaximum_;
    var pixMin = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum_;

    return (this.inverted_) ? (pixMax - pixelOffset) : (pixMin + pixelOffset);
};
//------------------------------------------------------------------------------
//                  Linearize.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.logBase_ = null;

anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.linearize = function (value) {
    return this.safeLog_(value);
};

/**
 * Safe log.
 * @private
 * @param {Number} value Value.
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.safeLog_ = function(value) {
    return (value > 1.0e-20) ? Math.log(value) / Math.log(this.logBase_) : 0;
};

/**
 * Delinearize and return passed value.
 * @public
 * @param {Number} value Target value.
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale.prototype.delinearize = function(value) {
    return Math.pow(this.logBase_, value);
};