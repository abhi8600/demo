/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class{anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.categorization.axes.labels');

//------------------------------------------------------------------------------
//
//                          CategorizedAxisLabels class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels = function(axis, viewPort) {
    anychart.plots.axesPlot.axes.elements.labels.AxisLabels.call(this, axis, viewPort, true);
};

goog.inherits(anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels,
        anychart.plots.axesPlot.axes.elements.labels.AxisLabels);

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.tmpCategory_ = null;
anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.getValueTokenType = function() {
    return anychart.formatting.TextFormatTokenType.TEXT;
};

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.getValueTokenValue = function() {
    return this.axis_.getScale().getCategoryName(this.tmpCategory_);
};

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.addCategory = function(category, svgManager) {
    this.tmpCategory_ = category;
    this.tmpCategory_.setAxisLabelFormattedText(this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale()));
    this.initSize(svgManager, this.tmpCategory_.getAxisLabelFormattedText());

    this.tmpCategory_.setAxisLabelBounds(this.bounds_.clone());
    this.tmpCategory_.setAxisLabelNonRotatedBounds(this.nonRotatedBounds_.clone());
    this.tmpCategory_ = null;
};

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.initMaxLabelSize = function(svgManager) {
    this.maxNonRotatedLabelSize_ = new anychart.utils.geom.Rectangle();
    this.maxLabelSize_ = new anychart.utils.geom.Rectangle();

    var scale = this.axis_.getScale();

    var majorIntervalsCount = scale.getMajorIntervalCount();

    for (var i = 0; i <= majorIntervalsCount; i++) {

        var categoryIndex = scale.getMajorIntervalValue(i);
        var category = this.axis_.getCategoryByIndex(categoryIndex + .5);

        if (!category) continue;

        var rotatedBounds = category.getAxisLabelBounds();
        var nonRotatedBounds = category.getAxisLabelNonRotatedBounds();

        if (rotatedBounds.width > this.maxLabelSize_.width) {
            this.maxLabelSize_.width = rotatedBounds.width;
            this.maxNonRotatedLabelSize_.width = nonRotatedBounds.width;
        }
        if (rotatedBounds.height > this.maxLabelSize_.height) {
            this.maxLabelSize_.height = rotatedBounds.height;
            this.maxNonRotatedLabelSize_.height = nonRotatedBounds.height;
        }
    }
};

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.drawLabels = function(container) {
    var pos = new anychart.utils.geom.Point();

    var scale = this.viewPort_.getMainViewPort().getAxis().getScale();

    var checkLabels = !this.allowOverlap_;
    var isInvertedLabelsCheck = checkLabels && this.viewPort_.isInvertedLabelsCheck(scale.isInverted());
    var lastX = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;

    var isT1Overlap;
    if (!this.rotation_.is90C)
        isT1Overlap = this.viewPort_.isFirstTypeLabelsOverlap(this.maxNonRotatedLabelSize_, this.rotation_);

    for (var i = 0; i < scale.getMajorIntervalCount(); i++) {
        var categoryIndex = scale.getMajorIntervalValue(i);
        var category = this.axis_.getCategoryByIndex(categoryIndex + .5);

        if (!category) continue;

        var rotatedBounds = category.getAxisLabelBounds();
        var nonRotatedBounds = category.getAxisLabelNonRotatedBounds();

        this.viewPort_.setLabelPosition(this, pos, rotatedBounds, nonRotatedBounds, scale.localTransform(categoryIndex + .5));

        if (checkLabels) {
            if (this.viewPort_.isLabelsOverlaped(isInvertedLabelsCheck, this.rotation_, pos, lastX, isT1Overlap, rotatedBounds, nonRotatedBounds))
                continue;

            lastX = this.viewPort_.getLabelLastPosition(isInvertedLabelsCheck, this.rotation_, pos, isT1Overlap, rotatedBounds, nonRotatedBounds);
        }
        this.moveLabel_(pos, rotatedBounds, nonRotatedBounds, this.rotation_);

        this.initSize(container.getSVGManager(), category.getAxisLabelFormattedText());
        var el = this.createSVGText(container.getSVGManager());
        el.setPosition(pos.x, pos.y);

        container.appendSprite(el);
    }
};

anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.prototype.getLabelBounds = function(svgManager, majorIntervalIndex) {
    var pos = new anychart.utils.geom.Point();
    var categoryIndex = this.axis_.getScale().getMajorIntervalValue(majorIntervalIndex) + .5;
    var category = this.axis_.getCategoryByIndex(categoryIndex);
    if (!category)
        return null;

    this.viewPort_.setLabelPosition(this, pos, category.getAxisLabelBounds(), category.getAxisLabelNonRotatedBounds(), this.axis_.getScale().localTransform(categoryIndex));
    this.moveLabel_(pos, category.getAxisLabelBounds(), category.getAxisLabelNonRotatedBounds(), this.rotation_);

    var bounds = category.getAxisLabelBounds().clone();
    bounds.x = pos.x;
    bounds.y = pos.y;

    return bounds;
};
//------------------------------------------------------------------------------
//
//                          StagerCategorizedAxisLabels class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels = function(axis, viewPort) {
    anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels.call(this, axis, viewPort, true);
};
goog.inherits(
        anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels,
        anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels);

/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.extraMaxLabelsSize_ = null;

/**
 * @override
 */
anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.initMaxLabelSize = function(svgManager) {
    this.maxLabelSize_ = new anychart.utils.geom.Rectangle();
    this.extraMaxLabelSize_ = new anychart.utils.geom.Rectangle();

    var scale = this.axis_.getScale();
    var majorIntervalsCount = scale.getMajorIntervalCount();

    for (var i = 0; i <= majorIntervalsCount; i++) {
        var categoryIndex = scale.getMajorIntervalValue(i);
        var category = this.axis_.getCategoryByIndex(categoryIndex + .5);
        if (!category) continue;

        var rotatedBounds = category.getAxisLabelBounds();
        var targetRect = (i % 2 == 0) ? this.maxLabelSize_ : this.extraMaxLabelSize_;

        if (rotatedBounds.width > targetRect.width)
            targetRect.width = rotatedBounds.width;
        if (rotatedBounds.height > targetRect.height)
            targetRect.height = rotatedBounds.height;
    }
};

anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.calcMaxLabelsCount = function(svgManager) {
    this.initMaxLabelSize(svgManager);
    var maxFirstLineLabels = this.viewPort_.getMaxRotatedLabelsCount(this.maxLabelSize_, this.maxLabelSize_, this.rotation_);
    var maxSecondLineLabels = this.viewPort_.getMaxRotatedLabelsCount(this.extraMaxLabelSize_, this.extraMaxLabelSize_, this.rotation_);
    var res = Math.ceil(maxFirstLineLabels + maxSecondLineLabels);
    return (res < 1) ? 1 : res;
};

anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.calcSpace = function() {
    this.space_ = this.viewPort_.getTextSpace(this.maxLabelSize_) + this.viewPort_.getTextSpace(this.extraMaxLabelSize_) + this.padding_;
    this.totalSpace_ = this.space_ + this.padding_;
};

anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.deserialize = function(config) {
    goog.base(this, 'deserialize', config);
    this.setRotationAngle(0);
};

anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels.prototype.drawLabels = function(container) {
    var pos = new anychart.utils.geom.Point();
    var checkLabels = !this.allowOverlap();
    var scale = this.axis_.getScale();
    var isInvertedLabelsCheck = checkLabels && this.viewPort_.isInvertedLabelsCheck(scale.isInverted());
    var lastX1 = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
    var lastX2 = lastX1;

    var space1 = this.viewPort_.getTextSpace(this.maxLabelSize_);
    var space2 = this.viewPort_.getTextSpace(this.extraMaxLabelSize_);

    var offset,space, even;

    for (var i = 0; i < scale.getMajorIntervalCount(); i++) {
        var categoryIndex = scale.getMajorIntervalValue(i);
        var category = this.axis_.getCategoryByIndex(categoryIndex + .5);
        if (!category) continue;

        even = i % 2 == 0;

        if (even) {
            space = space1;
            offset = 0;
        } else {
            space = space2;
            offset = space1;
        }

        var bounds = this.bounds_;
        this.viewPort_.setStagerLabelPosition(this, pos, bounds, scale.localTransform(categoryIndex + .5), offset, space);

        if (checkLabels) {
            if (even) {
                if (this.viewPort_.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, bounds, lastX1))
                    continue;
                lastX1 = this.viewPort_.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, bounds);
            } else {
                if (this.viewPort_.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, bounds, lastX2))
                    continue;
                lastX2 = this.viewPort_.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, bounds);
            }
        }

        this.initSize(container.getSVGManager(), category.getAxisLabelFormattedText());
        var el = this.createSVGText(container.getSVGManager());
        el.setPosition(pos.x, pos.y);
        container.appendSprite(el);
    }
};