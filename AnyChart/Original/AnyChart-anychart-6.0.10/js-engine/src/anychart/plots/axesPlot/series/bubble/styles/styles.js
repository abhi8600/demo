/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.bubble.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                          BubbleFillShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bubble.styles.BubbleFillShape = function() {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.styles.BubbleFillShape,
        anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.styles.BubbleFillShape.prototype.updatePath = function() {
    this.point_.updateBubbleShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                          BubbleHatchFillShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bubble.styles.BubbleHatchFillShape = function() {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.styles.BubbleHatchFillShape,
        anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.styles.BubbleHatchFillShape.prototype.updatePath = function() {
    this.point_.updateBubbleShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                          Bubble style shapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes = function() {
    anychart.styles.background.BackgroundStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes,
        anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.axesPlot.series.bubble.styles.BubbleFillShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.bubble.styles.BubbleHatchFillShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes.prototype.createElement = function() {
    return this.point_.createBubbleShape(this.point_.getSVGManager());
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes.prototype.updatePath = function() {
    this.point_.updateBubbleShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                          BubbleStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.bubble.styles.BubbleStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bubble.styles.BubbleStyle,
        anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                  Style shapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bubble.styles.BubbleStyle.prototype.createStyleShapes = function() {
    return new anychart.plots.axesPlot.series.bubble.styles.BubbleStyleShapes();
};



