goog.provide('anychart.plots.axesPlot.axes.viewPorts.drawing');

//-----------------------------------------------------------------------------------
//
//                          AxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort = function (axisViewPort) {
    this.axisViewPort_ = axisViewPort;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.axisViewPort_ = null;

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.svg_createParallelLine = function (svgManager, pixStart, pixEnd, position, thickness) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.svg_createPerpendicularLine = function (svgManager, pixValue) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.svg_createPerpendicularRectangle = function (svgManager, bounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.setGradientRotation = function (gradient) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.revertGradientRotation = function (gradient) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, pixStart, pixEnd, position, lineBounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.setPerpendicularLineBounds = function (thickness, pixValue, lineBounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.prototype.setPerpendicularRectangle = function (start, end, bounds) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.setGradientRotation = function () {
};

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.revertGradientRotation = function () {
};


anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.setPerpendicularLineBounds = function (thickness, axisValue, bounds) {
    bounds.x = this.axisViewPort_.getBounds().x + axisValue - thickness / 2;
    bounds.width = thickness;
    bounds.y = this.axisViewPort_.getBounds().y;
    bounds.height = this.axisViewPort_.getBounds().height;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.setPerpendicularRectangle = function (start, end, bounds) {
    bounds.x = this.axisViewPort_.getBounds().x + start;
    bounds.width = end - start;
    bounds.y = this.axisViewPort_.getBounds().y;
    bounds.height = this.axisViewPort_.getBounds().height;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    lineBounds.y = this.axisViewPort_.getBounds().y;
    lineBounds.x = this.axisViewPort_.getBounds().x + start;
    lineBounds.width = end - start;
    lineBounds.height = thickness;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.svg_createPerpendicularLine = function (svgManager, pixValue, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(pixValue, this.axisViewPort_.getBounds().y + .5, pixValue, this.axisViewPort_.getBounds().getBottom(), thickness));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.prototype.svg_createPerpendicularRectangle = function (svgManager, bounds) {
    return svgManager.createRect(bounds.x, bounds.y, bounds.width, bounds.height);
};


//-----------------------------------------------------------------------------------
//
//                          BottomAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.BottomAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.BottomAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.BottomAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    goog.base(this, 'setParallelLineBounds', thickness, start, end, position, lineBounds);
    lineBounds.y += this.axisViewPort_.getBounds().getBottom() + position - thickness / 2;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.BottomAxisDrawingViewPort.prototype.svg_createParallelLine = function (svgManager, start, end, position, thickness) {
    var el = svgManager.createPath();
    var y = this.axisViewPort_.getBounds().getBottom() + position;
    el.setAttribute('d', svgManager.pRLine(start + .5, y, end, y, thickness));
    return el;
};

//-----------------------------------------------------------------------------------
//
//                          TopAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.HorizontalAxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    goog.base(this, 'setParallelLineBounds', thickness, start, end, position, lineBounds);
    lineBounds.y += this.axisViewPort_.getBounds().y - position - thickness / 2;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort.prototype.svg_createParallelLine = function (svgManager, start, end, position, thickness) {
    var el = svgManager.createPath();
    var y = this.axisViewPort_.getBounds().y - position;
    el.setAttribute('d', svgManager.pRLine(start, y, end + 1, y, thickness));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort.prototype.setGradientRotation = function (gradient) {
    gradient.setAngle(-gradient.getAngle());
};

anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort.prototype.revertGradientRotation = function (gradient) {
    gradient.setAngle(-gradient.getAngle());
};


//-----------------------------------------------------------------------------------
//
//                          VerticalAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.AxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.prototype.setPerpendicularLineBounds = function (thickness, axisValue, bounds) {
    bounds.y = this.axisViewPort_.getBounds().getBottom() - axisValue - thickness / 2;
    bounds.height = thickness;
    bounds.x = this.axisViewPort_.getBounds().x;
    bounds.width = this.axisViewPort_.getBounds().width;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.prototype.setPerpendicularRectangle = function (start, end, bounds) {
    start = this.axisViewPort_.getBounds().getBottom() - start;
    end = this.axisViewPort_.getBounds().getBottom() - end;
    bounds.y = Math.min(start, end);
    bounds.height = Math.max(start, end) - bounds.y;
    bounds.x = this.axisViewPort_.getBounds().x;
    bounds.width = this.axisViewPort_.getBounds().width;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    lineBounds.x = this.axisViewPort_.getBounds().x;
    lineBounds.y = start;
    lineBounds.height = end - start;
    lineBounds.width = thickness;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.prototype.svg_createPerpendicularLine = function (svgManager, pixValue, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(this.axisViewPort_.getBounds().x + .5, pixValue, this.axisViewPort_.getBounds().getRight(), pixValue, thickness));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.prototype.svg_createPerpendicularRectangle = function (svgManager, bounds) {
    return svgManager.createRect(bounds.x, bounds.y, bounds.width, bounds.height);
};

//-----------------------------------------------------------------------------------
//
//                          LeftAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    goog.base(this, 'setParallelLineBounds', thickness, start, end, position, lineBounds);
    lineBounds.x += this.axisViewPort_.getBounds().x - position - thickness / 2;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort.prototype.svg_createParallelLine = function (svgManager, start, end, position, thickness) {
    var el = svgManager.createPath();
    var x = this.axisViewPort_.getBounds().x - position;
    el.setAttribute('d', svgManager.pRLine(x, start + 1, x, end, thickness));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort.prototype.setGradientRotation = function (gradient) {
    var angle = 180 - (gradient.getAngle() - 90);
    gradient.setAngle(angle);
};

anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort.prototype.revertGradientRotation = function (gradient) {
    var angle = 180 - (gradient.getAngle() - 90);
    gradient.setAngle(angle);
};


//-----------------------------------------------------------------------------------
//
//                          RightAxisDrawingViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort,
    anychart.plots.axesPlot.axes.viewPorts.drawing.VerticalAxisDrawingViewPort);

anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort.prototype.setParallelLineBounds = function (thickness, start, end, position, lineBounds) {
    goog.base(this, "setParallelLineBounds", thickness, start, end, position, lineBounds);
    lineBounds.x += this.axisViewPort_.getBounds().getRight() + position - thickness / 2;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort.prototype.svg_createParallelLine = function (svgManager, start, end, position, thickness) {
    var el = svgManager.createPath();
    var x = this.axisViewPort_.getBounds().getRight() + position;
    el.setAttribute('d', svgManager.pRLine(x, start + .5, x, end, thickness));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort.prototype.setGradientRotation = function (gradient) {
    var angle = -(gradient.getAngle() + 90);
    gradient.setAngle(angle);
};

anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort.prototype.revertGradientRotation = function (gradient) {
    var angle = -(gradient.getAngle() + 90);
    gradient.setAngle(angle);
};