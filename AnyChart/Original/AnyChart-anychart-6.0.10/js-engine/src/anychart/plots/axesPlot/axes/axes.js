/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.Axis};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.ValueAxis};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.AxisType}.</li>
 * <ul>
 */

//namespace
goog.provide('anychart.plots.axesPlot.axes');

//require

goog.require('anychart.layout');
goog.require('anychart.utils');
goog.require('anychart.visual.stroke');
goog.require('anychart.utils.geom');
goog.require('anychart.plots.axesPlot.axes.viewPorts');
goog.require('anychart.plots.axesPlot.axes.elements.labels');
goog.require('anychart.plots.axesPlot.axes.markers');
goog.require('anychart.plots.axesPlot.scales');
goog.require('anychart.plots.axesPlot.axes.elements');

//-----------------------------------------------------------------------------------
//
//                          Axis type
//
//-----------------------------------------------------------------------------------

anychart.plots.axesPlot.axes.AxisType = {
    UNKNOWN: 0,
    VALUE: 1,
    CATEGORIZED: 2,
    CATEGORIZED_BY_SERIES: 3
};

//-----------------------------------------------------------------------------------
//
//                          Axis class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent axis.
 * @public
 * @constructor
 */
anychart.plots.axesPlot.axes.Axis = function() {
    this.elements_ = [];
    this.points_ = [];
    this.initFormatting_();
    this.gridBounds_ = new anychart.utils.geom.Rectangle();
    this.statisticUtils_ = new anychart.utils.StatisticUtils();
};
//-----------------------------------------------------------------------------------
//
//                          Axis common public properties
//
//-----------------------------------------------------------------------------------
/**
 * Sets whether the axis is enabled.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.enabled_ = true;
anychart.plots.axesPlot.axes.Axis.prototype.isEnabled = function() {
    return this.enabled_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setEnabled = function(value) {
    this.enabled_ = value;
};
anychart.plots.axesPlot.axes.Axis.prototype.getType = function() {
    return anychart.plots.axesPlot.axes.AxisType.UNKNOWN;
};
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getSVGManager = function() {
    return this.plot_.getSVGManager();
};
/**
 * Axis position
 * @private
 * @type {anychart.layout.Position}
 */
anychart.plots.axesPlot.axes.Axis.prototype.position_ = NaN;
anychart.plots.axesPlot.axes.Axis.prototype.getPosition = function() {
    return this.position_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setPosition = function(value) {
    this.position_ = value;
};

/**
 * Axis name.
 * @private
 * @type {string}
 */
anychart.plots.axesPlot.axes.Axis.prototype.name_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getName = function() {
    return this.title_ ?
        this.title_.getTextFormatter().getValue(this) :
        this.name_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setName = function(value) {
    this.name_ = value;
};

/**
 * Linkage to plot.
 * @private
 * @type {anychart.plots.axesPlot.AxesPlot}
 */
anychart.plots.axesPlot.axes.Axis.prototype.plot_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getPlot = function() {
    return this.plot_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setPlot = function(value) {
    this.plot_ = value;
};

anychart.plots.axesPlot.axes.Axis.prototype.crossAxis_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.setCrossAxis = function(value) {
    this.crossAxis_ = value;
};

/**
 * TODO: describe
 * @public
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.isArgument_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.isArgument = function() {
    return this.isArgument_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setIsArgument = function(value) {
    this.isArgument_ = value;
};

/**
 * TODO: describe
 * @private
 * @type {Array}
 */
anychart.plots.axesPlot.axes.Axis.prototype.points_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getPoints = function() {
    return this.points_;
};

/**
 * Plot fixed space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.fixedSpace_ = 0;
anychart.plots.axesPlot.axes.Axis.prototype.getFixedSpace = function() {
    return this.fixedSpace_;
};

/**
 * Plot title space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.titleSpace_ = 0;
anychart.plots.axesPlot.axes.Axis.prototype.getTitleSpace = function() {
    return this.titleSpace_;
};

/**
 * Plot space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.space_ = 0;
anychart.plots.axesPlot.axes.Axis.prototype.getSpace = function() {
    return this.space_;
};

/**
 * Tickmarks space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.tickmarksSpace_ = 0;

/**
 * Tickmarks inside space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.insideTickmakrsSpace_ = 0;

/**
 * Tickmarks outside space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.outsideTickmarkSpace_ = 0;

/**
 * Markers fixed space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.markersFixedSpace_ = 0;

/**
 * Markers labels space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.markersLabelsSpace_ = 0;

/**
 * Markers before space.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.markersBeforeSpace_ = 0;

/**
 * Axis markers list.
 * @private
 * @type {anychart.plots.axesPlot.axes.markers.AxisMarkersList}
 */
anychart.plots.axesPlot.axes.Axis.prototype.markers_ = null;

/**
 * Axis view port.
 * @private
 * @type {anychart.plots.axesPlot.axes.viewPorts.AxisViewPort}
 */
anychart.plots.axesPlot.axes.Axis.prototype.viewPort_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getViewPort = function() {
    return this.viewPort_;
};

/**
 * Axis scale.
 * @private
 * @type {anychart.plots.axesPlot.scales.ValueScale}
 */
anychart.plots.axesPlot.axes.Axis.prototype.scale_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getScale = function() {
    return this.scale_;
};

/**
 * Create axis view port.
 * @public
 * @param {Function} viewPortFactory View port factory class.
 */
anychart.plots.axesPlot.axes.Axis.prototype.createViewPort = function(viewPortFactory) {
    this.viewPort_ = viewPortFactory.createViewPort(this);
};

/**
 * Check has axis valid scale or not.
 * @return {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.hasData = function() {
    return !isNaN(this.scale_.getDataRangeMinimum()) && !isNaN(this.scale_.getDataRangeMaximum());
};

/**
 * Copy scale from another axis
 * @param {anychart.plots.axesPlot.axes.Axis} axis
 */
anychart.plots.axesPlot.axes.Axis.prototype.copyScale = function(axis) {
    this.checkScaleValue(axis.getScale().getDataRangeMinimum());
    this.checkScaleValue(axis.getScale().getDataRangeMaximum());
};

/**
 * Indicates, is opposite.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.isOpposite_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.isOpposite = function() {
    return this.isOpposite_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setIsOpposite = function(value) {
    this.isOpposite_ = value;
};
anychart.plots.axesPlot.axes.Axis.prototype.isCategorized = function() {
    return false;
};
/**
 * Indicates, is first axis in axis list.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.isFirstInList_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.isFirstInList = function() {
    return this.isFirstInList_;
};
anychart.plots.axesPlot.axes.Axis.prototype.setIsFirstInList = function(value) {
    this.isFirstInList_ = value;
};

/**
 * Axis padding.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.padding_ = 0;

/**
 * Axis offset.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.offset_ = 0;

anychart.plots.axesPlot.axes.Axis.prototype.setOffset = function(value) {
    this.offset_ = value;
};

/**
 * Axis extraAdditionalOffset.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.extraAdditionalOffset_ = 0;
anychart.plots.axesPlot.axes.Axis.prototype.getExtraAdditionalOffset = function() {
    return this.extraAdditionalOffset_;
};

/**
 * Axis major grid.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.AxisGrid}
 */
anychart.plots.axesPlot.axes.Axis.prototype.majorGrid_ = null;

/**
 * Axis minor grid.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.AxisGrid}
 */
anychart.plots.axesPlot.axes.Axis.prototype.minorGrid_ = null;

/**
 * Axis major tickmark.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.AxisTickmark}
 */
anychart.plots.axesPlot.axes.Axis.prototype.majorTickmark_ = null;

/**
 * Axis minor tickmark.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.AxisTickmark}
 */
anychart.plots.axesPlot.axes.Axis.prototype.minorTickmark_ = null;

/**
 * Axis line
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.Axis.prototype.line_ = null;

/**
 * Axis zero line
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.Axis.prototype.zeroLine_ = null;

/**
 * Axis title.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.AxisTitle}
 */
anychart.plots.axesPlot.axes.Axis.prototype.title_ = null;

/**
 * Axis title.
 * @public
 * @return {anychart.plots.axesPlot.axes.elements.AxisTitle}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getTitle = function() {
    return this.title_;
};

/**
 * Axis labels.
 * @private
 * @type {anychart.plots.axesPlot.axes.elements.labels.AxisLabels}
 */
anychart.plots.axesPlot.axes.Axis.prototype.labels_ = null;
/**
 * @return {anychart.plots.axesPlot.axes.elements.labels.AxisLabels}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getLabels = function() {
    return this.labels_;
};

/**
 * Indicates, is center tickmark.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.axes.Axis.prototype.isCenterTickmarks_ = false;
anychart.plots.axesPlot.axes.Axis.prototype.isCenterTickmarks = function() {
    return this.isCenterTickmarks_;
};

/**
 * @private
 */
anychart.plots.axesPlot.axes.Axis.prototype.statisticUtils_ = null;

//todo : translate comments
/**
 * Сумарное пространство занимаемое елементами точек.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.pointElementsSpace_ = 0;

/**
 * Елементы точек, размер которых нужно учитвать при зажатии плота осями. Например лейблы точек с anchor = XAis.
 * @private
 * @type {Array}
 */
anychart.plots.axesPlot.axes.Axis.prototype.pointElements_ = null;
//------------------------------------------------------------------------------
//                              Locale.
//------------------------------------------------------------------------------
/**
 * @return {anychart.locale.DateTimeLocale}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getDateTimeLocale = function() {
    return this.plot_.getDateTimeLocale();
};
/**
 * @return {anychart.locale.NumberLocale}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getNumberLocale = function() {
    return this.plot_.getNumberLocale();
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.deserializeValue = function(val) {
    return this.scale_.deserializeValue(val);
};

anychart.plots.axesPlot.axes.Axis.prototype.checkScaleValue = function(val) {
    this.scale_.checkValue(val);
};

anychart.plots.axesPlot.axes.Axis.prototype.transform = function(point, value, pixPoint, opt_pixOffset) {
    if (isNaN(opt_pixOffset)) opt_pixOffset = 0;
    var pixValue = this.scale_.transformValueToPixel(value) + opt_pixOffset;
    this.viewPort_.setScaleValue(pixValue, pixPoint);
};

anychart.plots.axesPlot.axes.Axis.prototype.simpleTransform = function(point, value) {
    return this.scale_.transformValueToPixel(value);
};


anychart.plots.axesPlot.axes.Axis.prototype.onAfterDeserializeData = function(drawingPoints) {

};

anychart.plots.axesPlot.axes.Axis.prototype.checkSeries = function(series) {

};

anychart.plots.axesPlot.axes.Axis.prototype.isValueAxis = function() {
    return false;
};

/**
 * Deserialize axis settings.
 * @public
 * @param {object} data JSON object with axis settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.axesPlot.axes.Axis.prototype.deserialize = function(data, stylesList) {
    this.fixedSpace_ = 0;
    this.offset_ = 0;
    this.padding_ = 10;
    this.extraAdditionalOffset_ = this.padding_;

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'enabled'))
        this.enabled_ = des.getBoolProp(data, 'enabled');

    if (!this.enabled_) return;

    if (des.isEnabledProp(data, 'zoom'))
        anychart.errors.featureNotSupported('Zoom');

    var majorGridSettings = des.getProp(data, 'major_grid');
    if (des.isEnabled(majorGridSettings)) {
        this.majorGrid_ = new anychart.plots.axesPlot.axes.elements.AxisGrid();
        this.majorGrid_.deserialize(majorGridSettings);
        this.setGridGradientsRotation_(this.majorGrid_);
    }

    var minorGridSettings = des.getProp(data, 'minor_grid');
    if (des.isEnabled(minorGridSettings)) {
        this.minorGrid_ = new anychart.plots.axesPlot.axes.elements.AxisGrid();
        this.minorGrid_.deserialize(minorGridSettings);
        this.setGridGradientsRotation_(this.minorGrid_);
    }

    this.tickmarksSpace_ = 0;
    this.insideTickmakrsSpace_ = 0;
    this.outsideTickmarkSpace_ = 0;

    var majorTickmarkSettings = des.getProp(data, 'major_tickmark');
    if (des.isEnabled(majorTickmarkSettings)) {
        this.majorTickmark_ = new anychart.plots.axesPlot.axes.elements.AxisTickmark();
        this.majorTickmark_.deserialize(majorTickmarkSettings);
        if (this.majorTickmark_.hasStroke()) {
            if (this.majorTickmark_.getStroke().isGradient())
                this.viewPort_.getDrawingViewPort().setGradientRotation(this.majorTickmark_.getStroke().getGradient());

            if (this.majorTickmark_.isOutside()) {
                this.tickmarksSpace_ += this.majorTickmark_.getSize();
                this.outsideTickmarkSpace_ = this.majorTickmark_.getSize();
            }

            if (this.majorTickmark_.isInside() && !this.isFirstInList_) {
                this.tickmarksSpace_ += this.majorTickmark_.getSize();
                this.insideTickmakrsSpace_ = this.majorTickmark_.getSize();
            }
        }
    }

    var minorTickmarkSettings = des.getProp(data, 'minor_tickmark');
    if (des.isEnabled(minorTickmarkSettings)) {
        this.minorTickmark_ = new anychart.plots.axesPlot.axes.elements.AxisTickmark();
        this.minorTickmark_.deserialize(minorTickmarkSettings);
        if (this.minorTickmark_.hasStroke()) {
            if (this.minorTickmark_.getStroke().isGradient())
                this.viewPort_.getDrawingViewPort().setGradientRotation(this.minorTickmark_.getStroke().getGradient());

            var minorTickmarkSpace = 0;
            if (this.minorTickmark_.isOutside()) {
                minorTickmarkSpace += this.minorTickmark_.getSize();
                if (this.minorTickmark_.getSize() > this.outsideTickmarkSpace_)
                    this.outsideTickmarkSpace_ = this.minorTickmark_.getSize();
            }

            if (this.minorTickmark_.isInside() && !this.isFirstInList_) {
                minorTickmarkSpace += this.minorTickmark_.getSize();
                if (this.minorTickmark_.getSize() > this.insideTickmakrsSpace_)
                    this.insideTickmakrsSpace_ = this.minorTickmark_.getSize();
            }

            if (minorTickmarkSpace > this.tickmarksSpace_)
                this.tickmarksSpace_ = minorTickmarkSpace;
        }
    }

    var lineSettings = des.getProp(data, 'line');
    if (des.isEnabled(lineSettings)) {
        this.line_ = new anychart.visual.stroke.Stroke();
        this.line_.deserialize(lineSettings);
        if (this.line_.isGradient()) {
            this.lineBounds_ = new anychart.utils.geom.Rectangle();
            this.viewPort_.getDrawingViewPort().setGradientRotation(this.line_.getGradient());
        }
    }

    var zeroLineSettings = des.getProp(data, 'zero_line');
    if (des.isEnabled(zeroLineSettings)) {
        this.zeroLine_ = new anychart.visual.stroke.Stroke();
        this.zeroLine_.deserialize(zeroLineSettings);
        if (this.zeroLine_.isGradient()) {
            this.zeroLineBounds_ = new anychart.utils.geom.Rectangle();
            this.viewPort_.getDrawingViewPort().setGradientRotation(this.zeroLine_.getGradient());
        }
    }

    this.fixedSpace_ = this.tickmarksSpace_;
    this.extraAdditionalOffset_ += this.insideTickmakrsSpace_;

    if (!this.isFirstInList_)
        this.fixedSpace_ += this.padding_;

    var titleSettings = des.getProp(data, 'title');
    if (des.isEnabled(titleSettings)) {
        this.title_ = new anychart.plots.axesPlot.axes.elements.AxisTitle(this, this.viewPort_);
        this.title_.deserialize(titleSettings);
    }

    var labelsSettings = des.getProp(data, 'labels');
    if (des.isEnabled(labelsSettings)) {
        this.labels_ = this.createLabels_(labelsSettings);
        if (this.labels_)
            this.labels_.deserialize(labelsSettings);
    }

    if (des.isEnabledProp(data, 'axis_markers')) {
        this.markers_ = new anychart.plots.axesPlot.axes.markers.AxisMarkersList();
        this.markers_.setAxis(this);
        this.markers_.deserialize(des.getProp(data, 'axis_markers'), stylesList);
        this.markers_.setSprite(this.outsideContainer_);
    }

    this.space_ = this.fixedSpace_;
};

/**
 * Deserialize axis scale settings.
 * @public
 * @param {object} data JSON object with axis scale settings.
 */
anychart.plots.axesPlot.axes.Axis.prototype.deserializeScale = function(data) {
    this.scale_ = this.createScale_(anychart.utils.deserialization.getProp(data, 'scale'));
    if (anychart.utils.deserialization.hasProp(data, 'scale'))
        this.scale_.deserialize(anychart.utils.deserialization.getProp(data, 'scale'));
};

/**
 * Create axis scale.
 * @protected
 * @param {object} scaleNode JSON object with scale settings.
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.axesPlot.axes.Axis.prototype.createScale_ = goog.abstractMethod;

anychart.plots.axesPlot.axes.Axis.prototype.isDateTimeAxis = function() {
    return this.scale_.getType() == anychart.plots.axesPlot.scales.ScaleType.TEXT_DATE_TIME ||
        this.scale_.getType() == anychart.plots.axesPlot.scales.ScaleType.DATE_TIME;
};

/**
 * Create axis labels.
 * @protected
 * @param {object} labelsNode JSON object with labels settings.
 * @return {anychart.plots.axesPlot.axes.elements.labels.AxisLabels}
 */
anychart.plots.axesPlot.axes.Axis.prototype.createLabels_ = function(labelsNode) {
    goog.abstractMethod();
};

/**
 * @private
 */
anychart.plots.axesPlot.axes.Axis.prototype.setGridGradientsRotation_ = function(grid) {
    if (grid.hasEvenFill() && grid.getEvenFill().isGradient())
        this.viewPort_.getDrawingViewPort().setGradientRotation(grid.getEvenFill().getGradient());

    if (grid.hasOddFill() && grid.getOddFill().isGradient())
        this.viewPort_.getDrawingViewPort().setGradientRotation(grid.getOddFill().getGradient());

    if (grid.hasLine() && grid.getLine().isGradient())
        this.viewPort_.getDrawingViewPort().setGradientRotation(grid.getLine().getGradient());
};

//------------------------------------------------------------------------------
//					   Formatting
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.plots.axesPlot.axes.Axis.prototype.tokensHash_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.getTokensHash = function() {
    return  this.tokensHash_;
};

anychart.plots.axesPlot.axes.Axis.prototype.initFormatting_ = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};

anychart.plots.axesPlot.axes.Axis.prototype.setDefaultTokenValues = function() {
    this.tokensHash_.add('%AxisName', '');

    this.tokensHash_.add('%AxisValue', 0);
    this.tokensHash_.add('%AxisMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%AxisMin', Number.MAX_VALUE);
    this.tokensHash_.add('%AxisBubbleSizeMin', Number.MAX_VALUE);
    this.tokensHash_.add('%AxisBubbleSizeMax', -Number.MAX_VALUE);

};


anychart.plots.axesPlot.axes.Axis.prototype.getTokenValue = function(token) {
    if ((token == '%AxisValue' || token == '%Value') && this.labels_)
        return this.labels_.getValueTokenValue();

    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenValue(token);

    if (token == '%AxisScaleMax')
        this.tokensHash_.add('%AxisScaleMax', this.scale_.getMaximum());

    if (token == '%AxisScaleMin')
        this.tokensHash_.add('%AxisScaleMin', this.scale_.getMinimum());

    if (!this.tokensHash_.has('%AxisRange'))
        this.tokensHash_.add('%AxisRange',
            this.tokensHash_.get('%AxisMax') -
                this.tokensHash_.get('%AxisMin'));

    if (!this.tokensHash_.has('%AxisAverage'))
        this.tokensHash_.add('%AxisAverage',
            this.tokensHash_.get('%AxisSum') / this.points_.length);

    if (!this.tokensHash_.has('%AxisMedian'))
        this.tokensHash_.add(
            '%AxisMedian',
            this.statisticUtils_.getMedian(
                this.points_,
                this.isArgument() ?
                    '%XValue' :
                    '%YValue'));

    if (!this.tokensHash_.has('%AxisMode'))
        this.tokensHash_.add(
            '%AxisMode', this.statisticUtils_.getMode(
                this.points_,
                this.isArgument() ?
                    '%XValue' :
                    '%YValue'
            ));

    return this.tokensHash_.get(token);
};

anychart.plots.axesPlot.axes.Axis.prototype.getTokenType = function(token) {
    if(this.isDateTimeAxis() && token == '%Value')
        return anychart.formatting.TextFormatTokenType.DATE;

    switch (token) {
        case '%AxisName' :
        default :
            return anychart.formatting.TextFormatTokenType.TEXT;

        case '%Value' :
        case '%AxisValue' :
        case '%AxisSum' :
        case '%AxisMax' :
        case '%AxisMin' :
        case '%AxisRange' :
        case '%AxisScaleMax' :
        case '%AxisScaleMin' :
        case '%AxisAverage' :
        case '%AxisBubbleSizeSum' :
        case '%AxisBubbleSizeMin' :
        case '%AxisBubbleSizeMax' :
        case '%AxisMode' :
        case '%AxisMedian' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.calculateAxisTokenValue = function() {
    this.tokensHash_.add('%AxisName', this.getName());
};
//------------------------------------------------------------------------------
//                  Markers initialization.
//------------------------------------------------------------------------------
/**
 * Initialize axis markers.
 */
anychart.plots.axesPlot.axes.Axis.prototype.initMarkers = function() {
    this.markersFixedSpace_ = 0;
    this.markersLabelsSpace_ = 0;
    this.markersBeforeSpace_ = 0;

    if (this.markers_ != null) {
        this.markers_.initialize(this.plot_.getSVGManager());
        this.markersBeforeSpace_ = this.markers_.getBeforeAxisLabelsSpace();
        this.markersFixedSpace_ = this.markers_.getAfterAxisLabelsSpace() + this.markers_.getBeforeAxisLabelsSpace();
        this.markersLabelsSpace_ = this.markers_.getAxisLabelsSpace();
    }
};
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getMarkersAxisSpace = function() {
    var offset = this.isFirstInList_ ? (this.offset_ + this.tickmarksSpace_) : (this.offset_ + this.outsideTickmarkSpace_);
    offset += this.pointElementsSpace_;
    offset += this.markersBeforeSpace_;
    return offset;
};
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getMarkersBeforeLabelsSpace = function() {
    var offset = this.isFirstInList_ ? (this.offset_ + this.tickmarksSpace_) : (this.offset_ + this.outsideTickmarkSpace_);
    offset += this.pointElementsSpace_;
    return offset;
};
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.Axis.prototype.getMarkersAfterLabelsSpace = function() {
    var offset = this.isFirstInList_ ? (this.offset_ + this.tickmarksSpace_) : (this.offset_ + this.outsideTickmarkSpace_);
    if (this.labels_ != null)
        offset += Math.max(this.markersLabelsSpace_, this.getOutsideLabelsSpace_());
    else
        offset += this.markersLabelsSpace_;
    offset += this.markersBeforeSpace_;
    offset += this.pointElementsSpace_;
    return offset;
};
//------------------------------------------------------------------------------
//					   Point elements
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.Axis.prototype.applyPointElementsSpace = function() {
    this.pointElementsSpace_ = 0;
    if (!this.pointElements_) return;
    for (var i = 0; i < this.pointElements_.length; i++) {
        var element = this.pointElements_[i][0];
        var container = this.pointElements_[i][1];
        this.pointElementsSpace_ = Math.max(
            this.pointElementsSpace_,
            this.viewPort_.getSpace(
                element.getBounds(
                    container,
                    element.getStyle().getNormal(),
                    this.plot_.getSprite())
            ) + element.getStyle().getNormal().getPadding());
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.addPointAxisElement = function(container, element) {
    if (!this.pointElements_) this.pointElements_ = [];
    this.pointElements_.push(new Array(element, container));
};

anychart.plots.axesPlot.axes.Axis.prototype.setElementPosition = function(dataPoint, bounds, padding, position) {
    this.transform(dataPoint, dataPoint.getX(), position);
    this.viewPort_.setOffset(padding, bounds, position);
};

anychart.plots.axesPlot.axes.Axis.prototype.getOutsideLabelsSpace_ = function() {
    return (this.labels_ && !this.labels_.isInside()) ? this.labels_.getTotalSpace() : 0;
};

anychart.plots.axesPlot.axes.Axis.prototype.calculateWidthDefaultSize = function(bounds) {
    if (!this.enabled_) return;

    this.viewPort_.setScaleBounds(bounds);
    this.scale_.calculate();

    if (this.labels_)
        this.labels_.calcSpace();
};

anychart.plots.axesPlot.axes.Axis.prototype.recalculateWithDynamicSize = function(bounds) {
    this.calculateWidthDefaultSize(bounds);
    if (this.enabled_ && this.labels_)
        this.space_ = this.markersFixedSpace_ + this.fixedSpace_ + this.titleSpace_ + Math.max(this.getOutsideLabelsSpace_(), this.markersLabelsSpace_);
    else
        this.space_ = this.markersFixedSpace_ + this.fixedSpace_ + this.titleSpace_ + this.markersLabelsSpace_;
};

anychart.plots.axesPlot.axes.Axis.prototype.applyFixedOffset = function(bounds) {
    if (!this.enabled_ || this.scale_.isCrossingEnabled()) return;
    this.viewPort_.applyOffset(bounds, this.fixedSpace_ + this.titleSpace_ + this.pointElementsSpace_ + this.markersFixedSpace_);
};

anychart.plots.axesPlot.axes.Axis.prototype.applyDynamicOffset = function(bounds) {
    if (!this.enabled_ || this.scale_.isCrossingEnabled()) return;

    if (this.labels_)
        this.viewPort_.applyOffset(bounds, Math.max(this.getOutsideLabelsSpace_(), this.markersLabelsSpace_));
    else
        this.viewPort_.applyOffset(bounds, this.markersLabelsSpace_);
};

anychart.plots.axesPlot.axes.Axis.prototype.setBounds = function(bounds) {
    this.viewPort_.setScaleBounds(bounds);
    this.scale_.calculate();
    this.initTitle();
};

anychart.plots.axesPlot.axes.Axis.prototype.setLabelsSpace = function(bounds, axisBounds) {
    this.applyLabelsSpace_(bounds, axisBounds, this.scale_.getMajorIntervalCount());
};

anychart.plots.axesPlot.axes.Axis.prototype.applyLabelsSpace_ = function(bounds, axisBounds, lastInterval) {
    if (!this.enabled_ || this.labels_ == null) return;

    var firstLabel = this.labels_.getLabelBounds(this.plot_.getSVGManager(), 0);
    var lastLabel = this.labels_.getLabelBounds(this.plot_.getSVGManager(), lastInterval);

    this.viewPort_.setScaleBounds(axisBounds);

    if (firstLabel && lastLabel) {
        //switch if first is last
        if (firstLabel.x > bounds.width)
            firstLabel = lastLabel;
        else if (lastLabel.x > bounds.width)
            lastLabel = firstLabel;

        if (firstLabel.y > bounds.height)
            firstLabel = lastLabel;
        else if (lastLabel.y > bounds.height)
            lastLabel = firstLabel;

        this.viewPort_.getLabelsViewPort().applyLabelsOffsets(bounds, firstLabel, lastLabel);
    }

};

anychart.plots.axesPlot.axes.Axis.prototype.hasLabels = function() {
    return this.labels_ && this.labels_.isEnabled();
};
anychart.plots.axesPlot.axes.Axis.prototype.allowLabelsOverlap = function() {
    return this.labels_.allowOverlap();
};

anychart.plots.axesPlot.axes.Axis.prototype.calculateMaxLabelSize = function() {
    this.labels_.initMaxLabelSize(this.plot_.getSVGManager());
};

anychart.plots.axesPlot.axes.Axis.prototype.getMaxLabelsCount = function() {
    return this.labels_.calcMaxLabelsCount(this.plot_.getSVGManager());
};

//------------------------------------------------------------------------------
//					   Visual
//------------------------------------------------------------------------------

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.Axis.prototype.fixedContainer_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.Axis.prototype.plotBottomContainer_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.Axis.prototype.outsideContainer_ = null;

anychart.plots.axesPlot.axes.Axis.prototype.setContainers = function(fixedContainer, outsideContainer, plotBottomContainer) {
    this.fixedContainer_ = fixedContainer;
    this.outsideContainer_ = outsideContainer;
    this.plotBottomContainer_ = plotBottomContainer;
};

anychart.plots.axesPlot.axes.Axis.prototype.getOffset_ = function(offset, extraOffsetInCross) {
    var position = offset;
    if (this.scale_.isCrossingEnabled()) {
        position = this.crossAxis_.getScale().transform(this.getScale().getCrossValue());
        var pt = new anychart.utils.geom.Point();
        this.crossAxis_.getViewPort().setScaleValue(position, pt);

        position = pt.x != 0 ? pt.x : pt.y;
        position = -position + extraOffsetInCross;
    }
    return position;
};

//----------------------------------------------------------------
//					   Drawing: Axis line, zero line
//----------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.lineBounds_ = null;
anychart.plots.axesPlot.axes.Axis.prototype.zeroLineBounds_ = null;

anychart.plots.axesPlot.axes.Axis.prototype.drawAxisLine = function() {
    if (!this.enabled_ || this.line_ == null) return;

    var position = this.getOffset_(this.offset_, 0);

    var pixStart = this.viewPort_.getVisibleStart();
    var pixEnd = this.viewPort_.getVisibleEnd();

    if (this.line_.isGradient())
        this.viewPort_.getDrawingViewPort().setParallelLineBounds(this.line_.getThickness(), pixStart, pixEnd, position, this.lineBounds_);

    var style = this.line_.getSVGStroke(this.plot_.getSVGManager(), this.lineBounds_);
    if (style) {
        var el = this.viewPort_.getDrawingViewPort().svg_createParallelLine(this.fixedContainer_.getSVGManager(), pixStart, pixEnd, position,this.line_.getThickness());
        el.setAttribute('style', style);

        this.fixedContainer_.appendChild(el);
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawZeroLine = function() {

    if (!this.enabled_ || this.zeroLine_ == null || !this.scale_.contains(0) || this.scale_.getMinimum() == 0) return;

    var pixValue = this.scale_.transformValueToPixel(0);
    if (this.zeroLine_.isGradient())
        this.viewPort_.getDrawingViewPort().setPerpendicularLineBounds(this.zeroLine_.getThickness(), pixValue, this.zeroLineBounds_);

    var style = this.zeroLine_.getSVGStroke(this.plotBottomContainer_.getSVGManager(), this.zeroLineBounds_);
    if (style) {
        var el = this.viewPort_.getDrawingViewPort().svg_createPerpendicularLine(this.plotBottomContainer_.getSVGManager(), pixValue, this.zeroLine_.getThickness());
        el.setAttribute('style', style);

        this.plotBottomContainer_.appendChild(el);
    }
};

//----------------------------------------------------------------
//					   Drawing: Title
//----------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.initTitle = function() {
    if (this.enabled_ && this.title_) {
        this.title_.initialize(this.plotBottomContainer_.getSVGManager());
        this.titleSpace_ = this.title_.getSpace();
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawTitle = function() {
    if (!this.enabled_ || !this.title_) return;

    var alwaysOffset = this.isFirstInList_ ? this.tickmarksSpace_ : this.outsideTickmarkSpace_;

    var titleOffset = this.offset_;

    if (this.labels_ && this.labels_.isEnabled())
        alwaysOffset += Math.max(this.markersLabelsSpace_, this.getOutsideLabelsSpace_());
    else
        titleOffset += this.markersLabelsSpace_;

    titleOffset += this.pointElementsSpace_ + this.markersFixedSpace_;
    titleOffset += alwaysOffset;

    this.title_.setOffset(this.getOffset_(titleOffset, alwaysOffset));
    this.title_.drawAxisTitle(this.fixedContainer_);
};

//----------------------------------------------------------------
//					   Drawing: Labels
//----------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.drawLabels = function() {
    if (!this.enabled_ || this.labels_ == null) return;

    if (this.labels_.isInside())
        this.setInsideLabelsOffset_();
    else
        this.setOutsideLabelsOffset_();

    this.labels_.drawLabels(this.outsideContainer_);
};

anychart.plots.axesPlot.axes.Axis.prototype.setInsideLabelsOffset_ = function() {
    this.labels_.setOffset(this.offset_ - this.insideTickmakrsSpace_ - this.labels_.getPadding() * 2
        - this.labels_.getTotalSpace());
};

anychart.plots.axesPlot.axes.Axis.prototype.setOutsideLabelsOffset_ = function() {
    var alwaysOffset = this.isFirstInList_ ? this.tickmarksSpace_ : this.outsideTickmarkSpace_;
    var offset = this.offset_;
    alwaysOffset += this.labels_.getPadding();
    offset += this.pointElementsSpace_;
    offset += this.markersBeforeSpace_;
    offset += alwaysOffset;

    this.labels_.setOffset(this.getOffset_(offset, alwaysOffset));
};

//----------------------------------------------------------------
//					   Drawing: Tickmarks
//----------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.drawTickmarks = function() {
    if (!this.enabled_) return;

    if (this.minorTickmark_ != null && this.minorTickmark_.hasStroke())
        this.drawMinorTickmarks_();

    if (this.majorTickmark_ != null && this.majorTickmark_.hasStroke())
        this.drawMajorTickmarks_();
};

anychart.plots.axesPlot.axes.Axis.prototype.drawMajorTickmarks_ = function() {
    var isGradientTickmarks = this.majorTickmark_.getStroke().isGradient();

    var style = null;

    if (!isGradientTickmarks)
        style = this.majorTickmark_.getStroke().getSVGStroke(this.outsideContainer_.getSVGManager());

    var position = this.getOffset_(this.offset_, 0);

    var cnt = this.scale_.getMajorIntervalCount();
    var valueOffset = 0;
    if (this.isCenterTickmarks_) {
        cnt--;
        valueOffset = .5;
    }

    for (var i = 0; i <= cnt; i++) {
        var value = this.scale_.getMajorIntervalValue(i) + valueOffset;
        if (value >= this.scale_.getMinimum() && value <= this.scale_.getMaximum())
            this.majorTickmark_.draw(this.outsideContainer_,
                isGradientTickmarks,
                this.scale_.localTransform(value),
                position,
                this.viewPort_.getTickmarksViewPort(),
                style);
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawMinorTickmarks_ = function() {
    var isGradientTickmarks = this.minorTickmark_.getStroke().isGradient();

    var style = null;

    if (!isGradientTickmarks)
        style = this.minorTickmark_.getStroke().getSVGStroke(this.outsideContainer_.getSVGManager());

    var position = this.getOffset_(this.offset_, 0);

    var valueOffset = 0;
    if (this.isCenterTickmarks_)
        valueOffset = .5;

    var lastIndex = this.getScale().getMajorIntervalCount() - 1;

    var chekMax = false;
    if (this.getScale().getMajorIntervalValue(lastIndex) < this.getScale().getMaximum()) {
        lastIndex++;
        chekMax = true;
    }

    var minorIntervalsCount = this.getScale().getMinorIntervalCount();

    for (var i = 0; i <= lastIndex; i++) {
        var majorValue = this.getScale().getMajorIntervalValue(i);

        for (var j = 1; j < minorIntervalsCount; j++) {
            if (this.checkCenterTickmarkLast_(valueOffset, i, j))
                continue;

            var minorValue = this.getScale().getMinorIntervalValue(majorValue, j) + valueOffset;
            if (chekMax && this.notInAxisBounds_(minorValue))
                break;

            this.minorTickmark_.draw(this.outsideContainer_,
                isGradientTickmarks,
                this.scale_.localTransform(minorValue),
                position,
                this.viewPort_.getTickmarksViewPort(),
                style);
        }
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.checkCenterTickmarkLast_ = function(valueOffset, majorIndex, minorIndex) {
    return valueOffset != 0 &&
        (majorIndex + 1) == this.scale_.getMajorIntervalCount() &&
        minorIndex == this.scale_.getMinorIntervalCount();
};

anychart.plots.axesPlot.axes.Axis.prototype.notInAxisBounds_ = function(value) {
    return !this.scale_.linearizedContains(value);
};
//----------------------------------------------------------------
//					   Drawing: Grid lines
//----------------------------------------------------------------
anychart.plots.axesPlot.axes.Axis.prototype.drawMajorGridLines = function() {
    if (!this.enabled_ ||!this.majorGrid_ || !this.majorGrid_.hasLine()) return;

    var isGradientLines = this.majorGrid_.getLine().isGradient();

    var style = null;

    if (!isGradientLines)
        style = this.majorGrid_.getLine().getSVGStroke(this.plotBottomContainer_.getSVGManager());

    var cnt = this.scale_.getMajorIntervalCount();

    var valueOffset = 0;
    if (this.isCenterTickmarks_) {
        cnt--;
        valueOffset = .5;
    }

    for (var i = 0; i <= cnt; i++) {
        var pixValue = this.scale_.localTransform(this.scale_.getMajorIntervalValue(i) + valueOffset);

        if (isGradientLines) {
            this.viewPort_.getDrawingViewPort().setPerpendicularLineBounds(this.majorGrid_.getLine().getThickness(), pixValue, this.gridBounds_);
            style = this.majorGrid_.getLine().getSVGStroke(this.plotBottomContainer_.getSVGManager(), this.gridBounds_);
        }

        if (style) {
            var el = this.viewPort_.getDrawingViewPort().svg_createPerpendicularLine(this.plotBottomContainer_.getSVGManager(), pixValue, this.majorGrid_.getLine().getThickness());
            el.setAttribute('style', style);
            this.plotBottomContainer_.appendChild(el);
        }
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawMinorGridLines = function() {
    if (!this.enabled_ ||!this.minorGrid_ || !this.minorGrid_.hasLine()) return;

    var isGradientLines = this.minorGrid_.getLine().isGradient();

    var style = null;
    if (!isGradientLines)
        style = this.minorGrid_.getLine().getSVGStroke(this.plotBottomContainer_.getSVGManager());

    var lastIndex = this.scale_.getMajorIntervalCount() - 1;
    var chekMax = false;
    if (this.scale_.getMajorIntervalValue(lastIndex) < this.getScale().getMaximum()) {
        lastIndex++;
        chekMax = true;
    }

    var valueOffset = 0;
    if (this.isCenterTickmarks_) {
        valueOffset = .5;
    }

    for (var i = 0; i <= lastIndex; i++) {
        var majorValue = this.scale_.getMajorIntervalValue(i);

        for (var j = 1; j < this.scale_.getMinorIntervalCount(); j++) {

            if (this.checkCenterTickmarkLast_(valueOffset, i, j))
                continue;

            var minorValue = this.scale_.getMinorIntervalValue(majorValue, j) + valueOffset;
            if (chekMax && this.notInAxisBounds_(minorValue))
                break;

            var pixValue = this.scale_.localTransform(minorValue);

            if (isGradientLines) {
                this.viewPort_.getDrawingViewPort().setPerpendicularLineBounds(this.minorGrid_.getLine().getThickness(), pixValue, this.gridBounds_);
                style = style = this.minorGrid_.getLine().getSVGStroke(this.plotBottomContainer_.getSVGManager(), this.gridBounds_);
            }

            if (style) {
                var el = this.viewPort_.getDrawingViewPort().svg_createPerpendicularLine(this.plotBottomContainer_.getSVGManager(), pixValue, this.minorGrid_.getLine().getThickness());
                el.setAttribute('style', style);

                this.plotBottomContainer_.appendChild(el);
            }
        }
    }
};

//----------------------------------------------------------------
//					   Drawing: Grid interlaces
//----------------------------------------------------------------

anychart.plots.axesPlot.axes.Axis.prototype.drawGridInterlaces = function() {
    if (!this.enabled_) return;

    var drawEven;
    var drawOdd;

    if (this.minorGrid_ != null) {
        drawEven = this.minorGrid_.hasEvenFill() != null || this.minorGrid_.hasEvenHatchFill() != null;
        drawOdd = this.minorGrid_.hasOddFill() != null || this.minorGrid_.hasOddHatchFill() != null;
        if (drawEven || drawOdd)
            this.drawMinorGridInterlaces_(drawEven, drawOdd);
    }

    if (this.majorGrid_ != null) {
        drawEven = this.majorGrid_.hasEvenFill() != null || this.majorGrid_.hasEvenHatchFill() != null;
        drawOdd = this.majorGrid_.hasOddFill() != null || this.majorGrid_.hasOddHatchFill() != null;
        if (drawEven || drawOdd)
            this.drawMajorGridInterlaces_(drawEven, drawOdd);
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawMajorGridInterlaces_ = function(drawEven, drawOdd) {
    var isOdd = false;
    var cnt = this.scale_.getMajorIntervalCount();
    var valueOffset = 0;

    if (this.isCenterTickmarks_) {
        cnt--;
        valueOffset = .5;
    }

    for (var i = 0; i < cnt; i++) {
        if ((isOdd && drawOdd) || (!isOdd && drawEven)) {
            var start = this.scale_.getMajorIntervalValue(i) + valueOffset;
            var end = this.scale_.getMajorIntervalValue(i + 1) + valueOffset;
            this.drawInterlace_(start, end, this.majorGrid_, isOdd);
        }
        isOdd = !isOdd;
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawMinorGridInterlaces_ = function(drawEven, drawOdd) {
    var isOdd = false;
    var valueOffset = 0;
    if (this.isCenterTickmarks_)
        valueOffset = .5;

    for (var i = 0; i < this.scale_.getMajorIntervalCount(); i++) {
        var majorValue = this.scale_.getMajorIntervalValue(i);

        for (var j = 0; j < this.scale_.getMinorIntervalCount(); j++) {

            if (this.checkCenterTickmarkLast_(valueOffset, i, j))
                continue;

            if ((isOdd && drawOdd) || (!isOdd && drawEven)) {
                var start = this.scale_.getMinorIntervalValue(majorValue, j) + valueOffset;
                var end = this.scale_.getMinorIntervalValue(majorValue, j + 1) + valueOffset;
                this.drawInterlace_(start, end, this.minorGrid_, isOdd);
            }

            isOdd = !isOdd;
        }
    }
};

anychart.plots.axesPlot.axes.Axis.prototype.drawInterlace_ = function(start, end, grid, isOdd) {
    start = this.scale_.localTransform(start);
    end = this.scale_.localTransform(end);
    this.viewPort_.getDrawingViewPort().setPerpendicularRectangle(start, end, this.gridBounds_);
    var fill;
    var hatchFill;

    if (isOdd) {
        fill = grid.getOddFill();
        hatchFill = grid.getOddHatchFill();
    } else {
        fill = grid.getEvenFill();
        hatchFill = grid.getEvenHatchFill();
    }

    var container = this.plotBottomContainer_;

    var el;
    var style = null;

    if (fill != null) {
        style = fill.getSVGFill(container.getSVGManager(), this.gridBounds_);
        if (style) {
            el = this.viewPort_.getDrawingViewPort().svg_createPerpendicularRectangle(container.getSVGManager(), this.gridBounds_);
            el.setAttribute('style', style);
            container.appendChild(el);
        }
    }

    if (hatchFill != null) {
        style = hatchFill.getSVGHatchFill(container.getSVGManager());
        if (style) {
            el = this.viewPort_.getDrawingViewPort().svg_createPerpendicularRectangle(container.getSVGManager(), this.gridBounds_);
            el.setAttribute('style', style);
            container.appendChild(el);
        }
    }
};
//------------------------------------------------------------------------------
//                  Draw markers.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.Axis.prototype.drawAxisMarkers = function(svgManager) {
    if (!this.enabled_ || !this.markers_) return;
    this.markers_.draw(svgManager);
};
//------------------------------------------------------------------------------
//					   Serialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.Axis.prototype.serialize = function(opt_target) {
    if (opt_target == undefined || opt_target == null) opt_target = {};
    opt_target['Key'] = this.name_;
    opt_target['Name'] = this.tokensHash_.get("%AxisName");
    opt_target['Sum'] = this.tokensHash_.get("%AxisSum");
    opt_target['Max'] = this.tokensHash_.get("%AxisMax");
    opt_target['Min'] = this.tokensHash_.get("%AxisMin");
    opt_target['BubbleSizeMax'] = this.tokensHash_.get("%AxisBubbleMax");
    opt_target['BubbleSizeMin'] = this.tokensHash_.get("%AxisBubbleMin");
    opt_target['BubbleSizeSum'] = this.tokensHash_.get("%AxisBubbleSum");
    opt_target['Average'] = this.tokensHash_.get("%AxisAverage");
    opt_target['Median'] = this.tokensHash_.get("%AxisMedian");
    opt_target['Mode'] = this.tokensHash_.get("%AxisMode");
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          ValueAxis class
//
//------------------------------------------------------------------------------

/**
 * @public
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.axes.ValueAxis = function() {
    anychart.plots.axesPlot.axes.Axis.apply(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.axes.ValueAxis, anychart.plots.axesPlot.axes.Axis);

anychart.plots.axesPlot.axes.ValueAxis.prototype.getType = function() {
    return anychart.plots.axesPlot.axes.AxisType.VALUE;
};

/**
 * @override
 */
anychart.plots.axesPlot.axes.ValueAxis.prototype.isValueAxis = function() {
    return true;
};

anychart.plots.axesPlot.axes.ValueAxis.prototype.createScale_ = function(scaleNode) {
    var scale;
    switch (anychart.utils.deserialization.getEnumItem(anychart.utils.deserialization.getProp(scaleNode, 'type'))) {
        default:
        case "linear":
            scale = new anychart.plots.axesPlot.scales.LinearScale(this);
            break;

        case "logarithmic":
            scale = new anychart.plots.axesPlot.scales.LogScale(this);
            break;

        case "datetime":
            scale = new anychart.plots.axesPlot.scales.DateTimeScale(this);
            break;
    }
    return scale;
};

anychart.plots.axesPlot.axes.ValueAxis.prototype.createLabels_ = function(labelsNode) {
    if (anychart.utils.deserialization.hasProp(labelsNode, 'display_mode')) {
        var mode = anychart.utils.deserialization.getLStringProp(labelsNode, 'display_mode');
        if (mode == 'stager')
            return new anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels(this, this.viewPort_.getLabelsViewPort(), false);
    }
    return new anychart.plots.axesPlot.axes.elements.labels.AxisLabels(this, this.viewPort_.getLabelsViewPort(), false);
};