/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class{anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.elements.labels.AxisLabels}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.elements.labels');
goog.require('anychart.utils');
goog.require('anychart.formatting');
//-----------------------------------------------------------------------------------
//
//                          AxisLabelsAlign class
//
//-----------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign = {
    INSIDE: 0,
    OUTSIDE: 1,
    CENTER: 2
};
//-----------------------------------------------------------------------------------
//
//                          AxisLabels class
//
//-----------------------------------------------------------------------------------

/**
 * @public
 * @constructor
 * @param {anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort} viewPort
 * @param {Boolean} isTextScale
 * @extends {anychart.visual.text.TextElement}
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabels = function(axis, viewPort, isTextScale) {
    this.axis_ = axis;
    this.viewPort_ = viewPort;
    this.isTextScale_ = isTextScale;
    anychart.visual.text.TextElement.apply(this);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.axes.elements.labels.AxisLabels, anychart.visual.text.TextElement);

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.createRotationUtils_ = function() {
    return new anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils(this.viewPort_);
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.axis_ = null;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.padding_ = 5;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getPadding = function() {
    return this.padding_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.align_ = anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.INSIDE;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getAlign = function() {
    return this.align_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.inside_ = false;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.isInside = function() {
    return this.inside_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.viewPort_ = null;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.isTextScale_ = false;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.showFirst_ = true;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.showLast_ = true;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.textFormatter_ = null;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.allowOverlap_ = false;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.allowOverlap = function() {
    return this.allowOverlap_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.tmpValue_ = NaN;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getValueTokenValue = function() {
    return this.tmpValue_;
};
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getValueTokenType = function() {
    return anychart.formatting.TextFormatTokenType.NUMBER;
};
/**
 * Number of decimals.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.numDecimals_ = null;
/**
 * Getter for number of decimals.
 * @return {Number} Number of decimals.
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getNumDecimals = function() {
    return this.numDecimals_;
};
/**
 * Method for parsing numDecimals value.
 * @private
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.parseNumDecimals_ = function() {
    var tf = this.textFormatter_;
    tf.getValue(this.axis_);
    var term = null;
    for (var i in tf.termsQueue_) {
        if (tf.termsQueue_[i].tokenName_ == '%Value') {
            term = tf.termsQueue_[i];
            break;
        }
    }
    if (term != null && term.numDecimals_ != undefined) this.numDecimals_ = term.numDecimals_;
    else this.numDecimals_ = 2;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.deserialize = function(config) {
    goog.base(this, 'deserialize', config);
    if (!this.enabled_) return;

    var des = anychart.utils.deserialization;
    
    if (des.hasProp(config, 'format')) {
        var format = des.getStringProp(config, 'format');
        this.textFormatter_ = new anychart.formatting.TextFormatter(format);
        if (this.axis_.getScale().isSmartMode()) {
            this.parseNumDecimals_();
            this.axis_.getScale().enableSmartMode(this.getNumDecimals());
        }
    }

    if (des.hasProp(config, 'padding'))
        this.padding_ = des.getNumProp(config, 'padding');

    if (des.hasProp(config, 'align')) {
        switch (des.getEnumProp(config, 'align')) {
            case 'inside':
                this.align_ = anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.INSIDE;
                break;
            case 'outside':
                this.align_ = anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE;
                break;
            case 'center':
                this.align_ = anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER;
                break;
        }
    }

    if (des.hasProp(config, 'position'))
        this.inside_ = des.getLStringProp(config, 'position') == 'inside';

    if (des.hasProp(config, 'show_first_label'))
        this.showFirst_ = des.getBoolProp(config, 'show_first_label');

    if (des.hasProp(config, 'show_last_label'))
        this.showLast_ = des.getBoolProp(config, 'show_last_label');

    if (des.hasProp(config, 'allow_overlap'))
        this.allowOverlap_ = des.getBoolProp(config, 'allow_overlap');
};

//----------------------------------------------------------
//		Max size calculation
//----------------------------------------------------------

/**
 * @protected
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.maxLabelSize_ = null;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.maxNonRotatedLabelSize_ = null;

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.initSize = function(svgManager, text) {
    this.rotation_.calculate();
    goog.base(this, 'initSize', svgManager, text);
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.initMaxLabelSize = function(svgManager) {
    this.maxNonRotatedLabelSize_ = new anychart.utils.geom.Rectangle();
    this.maxLabelSize_ = new anychart.utils.geom.Rectangle();

    var scale = this.viewPort_.getMainViewPort().getAxis().getScale();
    var majorIntervalsCount = scale.getMajorIntervalCount();

    for (var i = 0; i <= majorIntervalsCount; i++) {

        this.tmpValue_ = scale.getAxisValue(i);

        var formattedText = this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale());

        this.initSize(svgManager, formattedText);

        var rotatedBounds = this.bounds_;
        var nonRotatedBounds = this.nonRotatedBounds_;

        if (rotatedBounds.width > this.maxLabelSize_.width) {
            this.maxLabelSize_.width = rotatedBounds.width;
            this.maxNonRotatedLabelSize_.width = nonRotatedBounds.width;
        }

        if (rotatedBounds.height > this.maxLabelSize_.height) {
            this.maxLabelSize_.height = rotatedBounds.height;
            this.maxNonRotatedLabelSize_.height = nonRotatedBounds.height;
        }
    }
};

//----------------------------------------------------------
//		Max labels calculation
//----------------------------------------------------------

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.calcMaxLabelsCount = function(svgManager) {
    this.initMaxLabelSize(svgManager);
    var maxLabels = this.viewPort_.getMaxRotatedLabelsCount(this.maxNonRotatedLabelSize_, this.maxLabelSize_, this.rotation_);
    maxLabels = Math.ceil(maxLabels);
    return (maxLabels < 1) ? 1 : maxLabels;
};

//----------------------------------------------------------
//		Labels space
//----------------------------------------------------------

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.space_ = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getSpace = function() {
    return this.space_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.totalSpace_ = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getTotalSpace = function() {
    return this.totalSpace_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.calcSpace = function() {
    this.space_ = this.viewPort_.getTextSpace(this.maxLabelSize_);
    this.totalSpace_ = this.space_ + this.padding_;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getLabelBounds = function(svgManager, majorIntervalIndex) {

    var pos = new anychart.utils.geom.Point();

    var scale = this.viewPort_.getMainViewPort().getAxis().getScale();

    var tmpValue = scale.getMajorIntervalValue(majorIntervalIndex);

    this.tmpValue_ = scale.getAxisValue(majorIntervalIndex);
    
    this.initSize(svgManager, this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale()));

    this.viewPort_.setLabelPosition(this, pos, this.bounds_, this.nonRotatedBounds_, scale.localTransform(tmpValue));
    this.moveLabel_(pos, this.bounds_, this.nonRotatedBounds_, this.rotation_);

    var bounds = this.bounds_.clone();
    bounds.x = pos.x;
    bounds.y = pos.y;

    return bounds;
};

//----------------------------------------------------------
//		Labels drawing
//----------------------------------------------------------

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.offset_ = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.getOffset = function() {
    return this.offset_;
};
anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.setOffset = function(value) {
    this.offset_ = value;
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.drawLabels = function(container) {
    var pos = new anychart.utils.geom.Point();

    var scale = this.viewPort_.getMainViewPort().getAxis().getScale();

    var checkLabels = !this.allowOverlap_;
    var isInvertedLabelsCheck = checkLabels && this.viewPort_.isInvertedLabelsCheck(scale.isInverted());
    var lastX = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;

    var isT1Overlap;
    if (!this.rotation_.is90C)
        isT1Overlap = this.viewPort_.isFirstTypeLabelsOverlap(this.maxNonRotatedLabelSize_, this.rotation_);

    var startIndex = this.showFirst_ ? 0 : 1;
    var endIndex = scale.getMajorIntervalCount();
    if (!this.showLast_)
        endIndex--;

    for (var i = startIndex; i <= endIndex; i++) {
        var tmpValue = scale.getMajorIntervalValue(i);
        this.tmpValue_ = scale.getAxisValue(i);

        if (!scale.linearizedContains(tmpValue)) return;

        var formattedText = this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale());
        this.initSize(container.getSVGManager(), formattedText);

        var rotatedBounds = this.bounds_;
        var nonRotatedBounds = this.nonRotatedBounds_;

        this.viewPort_.setLabelPosition(this, pos, rotatedBounds, nonRotatedBounds, scale.localTransform(tmpValue));

        if (checkLabels) {
            if (this.viewPort_.isLabelsOverlaped(isInvertedLabelsCheck, this.rotation_, pos, lastX, isT1Overlap, rotatedBounds, nonRotatedBounds))
                continue;

            lastX = this.viewPort_.getLabelLastPosition(isInvertedLabelsCheck, this.rotation_, pos, isT1Overlap, rotatedBounds, nonRotatedBounds);
        }
        this.moveLabel_(pos, rotatedBounds, nonRotatedBounds, this.rotation_);

        var el = this.createSVGText(container.getSVGManager());
        el.setPosition(pos.x, pos.y);

        container.appendSprite(el);
    }
};

anychart.plots.axesPlot.axes.elements.labels.AxisLabels.prototype.moveLabel_ = function(position, rotatedBounds, nonRotatedBounds, rotation) {
    if (this.rotation_.is180C) {
        position.x += rotatedBounds.width * rotation.dxw;
        position.y += rotatedBounds.height * rotation.dyh;
    } else {
        position.x += nonRotatedBounds.width * rotation.dxw + nonRotatedBounds.height * rotation.dxh;
        position.y += nonRotatedBounds.width * rotation.dyw + nonRotatedBounds.height * rotation.dyh;
    }
};

//-----------------------------------------------------------------------------------
//
//                          StagerAxisLabels class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels = function(axis, viewPort, isTextScale) {
    anychart.plots.axesPlot.axes.elements.labels.AxisLabels.call(this, axis, viewPort, isTextScale);
};

goog.inherits(anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels,
        anychart.plots.axesPlot.axes.elements.labels.AxisLabels);

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.extraMaxLabelSize_ = null;

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.initMaxLabelSize = function(svgManager) {
    this.maxLabelSize_ = new anychart.utils.geom.Rectangle();
    this.extraMaxLabelSize_ = new anychart.utils.geom.Rectangle();

    var scale = this.axis_.getScale();

    var majorIntervalsCount = scale.getMajorIntervalCount();
    for (var i = 0; i <= majorIntervalsCount; i++) {

        var tmpValue = scale.getMajorIntervalValue(i);

        this.tmpValue_ = scale.getAxisValue(i);
        var formattedText = this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale());

        this.initSize(svgManager, formattedText);

        var targetRect = (i % 2 == 0) ? this.maxLabelSize_ : this.extraMaxLabelSize_;

        if (this.bounds_.width > targetRect.width)
            targetRect.width = this.bounds_.width;
        if (this.bounds_.height > targetRect.height)
            targetRect.height = this.bounds_.height;
    }
};

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.calcMaxLabelsCount = function(svgManager) {
    this.initMaxLabelSize(svgManager);
    var maxFirstLineLabels = this.viewPort_.getMaxRotatedLabelsCount(this.maxLabelSize_, this.maxLabelSize_, this.rotation_);
    var maxSecondLineLabels = this.viewPort_.getMaxRotatedLabelsCount(this.extraMaxLabelSize_, this.extraMaxLabelSize_, this.rotation_);

    if (maxFirstLineLabels < 1)
        maxFirstLineLabels = 1;

    if (maxSecondLineLabels < 1)
        maxSecondLineLabels = 1;

    return maxFirstLineLabels + maxSecondLineLabels;
};

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.calcSpace = function() {
    this.space_ = this.viewPort_.getTextSpace(this.maxLabelSize_) + this.viewPort_.getTextSpace(this.extraMaxLabelSize_) + this.padding_;
    this.totalSpace_ = this.space_ + this.padding_;
};

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.drawLabels = function(container) {
    var pos = new anychart.utils.geom.Point();

    var scale = this.viewPort_.getMainViewPort().getAxis().getScale();

    var checkLabels = !this.allowOverlap_;
    var isInvertedLabelsCheck = checkLabels && this.viewPort_.isInvertedLabelsCheck(scale.isInverted());
    var lastX1 = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
    var lastX2 = lastX1;

    var space1 = this.viewPort_.getTextSpace(this.maxLabelSize_);
    var space2 = this.viewPort_.getTextSpace(this.extraMaxLabelSize_);

    var offset,space;

    for (var i = 0; i <= scale.getMajorIntervalCount(); i++) {

        var even = i % 2 == 0;

        var tmpValue = scale.getMajorIntervalValue(i);
        this.tmpValue_ = scale.getAxisValue(i);
        var formattedText = this.textFormatter_.getValue(this.axis_, this.axis_.getDateTimeLocale());
        this.initSize(container.getSVGManager(), formattedText);

        if (even) {
            space = space1;
            offset = 0;
        } else {
            space = space2;
            offset = space1;
        }

        var bounds = this.bounds_;

        this.viewPort_.setStagerLabelPosition(this, pos, bounds, scale.localTransform(tmpValue), offset, space);

        if (checkLabels) {
            if (even) {
                if (this.viewPort_.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, bounds, lastX1))
                    continue;
                lastX1 = this.viewPort_.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, bounds);
            } else {
                if (this.viewPort_.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, bounds, lastX2))
                    continue;
                lastX2 = this.viewPort_.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, bounds);
            }
        }

        var el = this.createSVGText(container.getSVGManager());
        el.setPosition(pos.x, pos.y);

        container.appendSprite(el);
    }
};

anychart.plots.axesPlot.axes.elements.labels.StagerAxisLabels.prototype.deserialize = function(config) {
    goog.base(this, 'deserialize', config);
    this.setRotationAngle(0);
};


//-----------------------------------------------------------------------------------
//
//                          AxisLabelsRotationUtils class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils = function(viewPort) {
    this.viewPort = viewPort;
    anychart.utils.geom.RotationUtils.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils, anychart.utils.geom.RotationUtils);

anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.tan = 1;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.absTan = 1;

anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.dxw = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.dyw = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.dxh = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.dyh = 0;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.is180C = false;
anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.is90C = false;

anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.viewPort = null;

anychart.plots.axesPlot.axes.elements.labels.AxisLabelsRotationUtils.prototype.calculate = function() {
    goog.base(this, 'calculate');

    this.tan = Math.tan(this.radRotation);
    this.absTan = this.tan;
    if (this.absTan < 0)
        this.absTan = -this.absTan;

    this.is180C = this.radRotation % 180 == 0;
    this.is90C = this.radRotation % 90 == 0;

    var qIndex = 0;
    if (this.radRotation < 90) qIndex = 0;
    else if (this.radRotation < 180) qIndex = 1;
    else if (this.radRotation < 270) qIndex = 2;
    else qIndex = 3;

    this.dxw = this.viewPort.getLabelXOffsetWidthMult(qIndex, this);
    this.dxh = this.viewPort.getLabelXOffsetHeightMult(qIndex, this);
    this.dyw = this.viewPort.getLabelYOffsetWidthMult(qIndex, this);
    this.dyh = this.viewPort.getLabelYOffsetHeightMult(qIndex, this);
};