goog.provide('anychart.plots.axesPlot.series.heatMap.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.heatMap.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"heat_map_style": {
				"border": {
					"type": "Solid",
					"thickness": "1",
					"color": "DarkColor(%Color)"
				},
				"fill": {
					"type": "Solid",
					"color": "%Color"
				},
				"name": "anychart_default"
			}
		},
		"data_plot_settings": {
			"heat_map": {
				"label_settings": {
				},
				"tooltip_settings": {
				},
				"marker_settings": {
				}
			}
		}
	}
};