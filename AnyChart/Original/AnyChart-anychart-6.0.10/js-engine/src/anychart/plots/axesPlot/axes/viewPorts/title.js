goog.provide('anychart.plots.axesPlot.axes.viewPorts.title');

goog.require('anychart.layout');
//-----------------------------------------------------------------------------------
//
//                          TitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort = function(axisViewPort) {
    this.axisViewPort_ = axisViewPort;
};

anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort.prototype.axisViewPort_ = null;

anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort.prototype.setTitlePosition = function(title, position) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort.prototype.setTitleRotation = function(title) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort.prototype.setTitleRotation = function(title) {
};

anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    switch (title.getAlign()) {
        case anychart.layout.AbstractAlign.NEAR:
            position.x = this.axisViewPort_.getBounds().x;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            position.x = this.axisViewPort_.getBounds().x + (this.axisViewPort_.getBounds().width - title.getNonRotatedBounds().width) / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            position.x = this.axisViewPort_.getBounds().getRight() - title.getNonRotatedBounds().width;
            break;
    }
};

//-----------------------------------------------------------------------------------
//
//                          BottomAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.BottomAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.BottomAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.BottomAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    goog.base(this, 'setTitlePosition', title, position);
    position.y = this.axisViewPort_.getBounds().getBottom() + title.getOffset() + title.getPadding();
};

//-----------------------------------------------------------------------------------
//
//                          TopAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.TopAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.TopAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.HorizontalAxisTitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.TopAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    goog.base(this, 'setTitlePosition', title, position);
    position.y = this.axisViewPort_.getBounds().getTop() - title.getOffset() - title.getPadding() - title.getNonRotatedBounds().height;
};

//-----------------------------------------------------------------------------------
//
//                          VerticalAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.TitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort.prototype.setTitleRotation = function(title) {
    title.setRotationAngle(title.getRotationAngle() + 90);
};

anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    switch (title.getAlign()) {
        case anychart.layout.AbstractAlign.NEAR:
            position.y = this.axisViewPort_.getBounds().getBottom() - title.getBounds().height;//костыль, выпинывает жуткий косяк с ротейтом
            break;
        case anychart.layout.AbstractAlign.CENTER:
            position.y = this.axisViewPort_.getBounds().getTop() + (this.axisViewPort_.getBounds().height - title.getBounds().height) / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            position.y = this.axisViewPort_.getBounds().getTop();//костыль, выпинывает жуткий косяк с ротейтом
            break;
    }
};

//-----------------------------------------------------------------------------------
//
//                          LeftAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.LeftAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.LeftAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.LeftAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    goog.base(this, 'setTitlePosition', title, position);
    position.x = this.axisViewPort_.getBounds().x - title.getOffset() - title.getPadding() - title.getBounds().width;
};
//-----------------------------------------------------------------------------------
//
//                          RightAxisTitleViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.title.RightAxisTitleViewPort = function(axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.title.RightAxisTitleViewPort,
        anychart.plots.axesPlot.axes.viewPorts.title.VerticalAxisTitleViewPort);

anychart.plots.axesPlot.axes.viewPorts.title.RightAxisTitleViewPort.prototype.setTitlePosition = function(title, position) {
    goog.base(this, 'setTitlePosition', title, position);
    position.x = this.axisViewPort_.getBounds().getRight() + title.getOffset() + title.getPadding();
};