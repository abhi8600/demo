/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.scales.dateTime.DateTimeUnit}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.dateTime.DateTimeRange}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.scales.dateTime');
//------------------------------------------------------------------------------
//
//                          DateTimeUnit class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit = function() {
};
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR = 0;
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH = 1;
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY = 2;
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR = 3;
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE = 4;
/**
 * @type {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND = 5;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_SECOND = 1000;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_MINUTE = 60 * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_SECOND;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_HOUR = 60 * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_MINUTE;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_DAY = 24 * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_HOUR;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_YEAR = 365 * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_DAY;
//------------------------------------------------------------------------------
//                          Date.
//------------------------------------------------------------------------------
/**
 * Increase date value.
 * @param {Number} dateValue
 * @param {int} unit
 * @param {Number} step
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.increaseDate = function(dateValue, unit, step) {
    var dateUtils = anychart.utils.DateUtils;
    var date = new Date(dateValue);
    switch (unit) {
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR :
            dateUtils.addFloatYear(date, step);
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH :
            dateUtils.addFloatMonths(date, step);
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY :
            dateUtils.addFloatDays(date, step);
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR :
            dateUtils.addFloatHours(date, step);
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTES :
            dateUtils.addFloatMinutes(date, step);
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND :
            dateUtils.addFloatSeconds(date, step);
            break;
    }
    return date.getTime();
};
/**
 * @param {int} unit
 * @param {Number} dateValue
 * @param {Boolean} direction
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.getEvenStepDate = function(unit, dateValue, direction) {
    var date = new Date(dateValue);
    var offset = (direction) ? 1 : 0;

    var year = date.getUTCFullYear();
    var month = date.getUTCMonth();
    var day = date.getUTCDate();
    var hour = date.getUTCHours();
    var minute = date.getUTCMinutes();
    var second = date.getUTCSeconds();

    var isFirstMonth = month == 0;
    var isFirstDay = day == 1;
    var isFirstHour = hour == 0;
    var isFirstMinute = minute == 0;
    var isFirstSecond = second == 0;

    switch (unit) {
        default:
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR:
            if (direction && isFirstMonth && isFirstDay && isFirstHour && isFirstMinute && isFirstSecond)
                return dateValue;
            return new Date(Date.UTC(year + offset, 0)).getTime();

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH:
            if (direction && isFirstDay && isFirstHour && isFirstMinute && isFirstSecond)
                return dateValue;
            return new Date(Date.UTC(year, month + offset)).getTime();

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY:
            if (direction && isFirstHour && isFirstMinute && isFirstSecond)
                return dateValue;
            return new Date(Date.UTC(year, month, day + offset)).getTime();

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR:
            if (direction && isFirstMinute && isFirstSecond)
                return dateValue;
            return new Date(Date.UTC(year, month, day, hour + offset)).getTime();

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE:
            if (direction && isFirstSecond)
                return dateValue;
            return new Date(Date.UTC(year, month, day, hour, minute + offset)).getTime();

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND:
            return new Date(Date.UTC(year, month, day, hour, minute, second + offset)).getTime();

    }
};
//------------------------------------------------------------------------------
//
//                          DateTimeRange class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange = function() {

};
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER = 24 * 60 * 60 * 1000;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.YEAR_YEAR = 1825 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.YEAR_MONTH = 365 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MONTH_MONTH = 90 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.DAY_DAY = 10 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.DAY_HOUR = 3 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.HOUR_HOUR = 0.4167 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.HOUR_MINUTE = 0.125 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MINUTE_MINUTE = 6.94e-3 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
/**
 * @type {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MINUTE_SECOND = 2.083e-3 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER;
//------------------------------------------------------------------------------
//                            Intervals.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calculateIntervals = function(scale, targetSteps) {
    var range = scale.getMaximum() - scale.getMinimum();
    var step;

    if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.YEAR_YEAR)
        step = this.calcYearYear_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.YEAR_MONTH)
        step = this.calcYearMonth_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MONTH_MONTH)
        step = this.calcMonthMonth_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.DAY_DAY)
        step = this.calcDayDay_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.DAY_HOUR)
        step = this.calcDayHour_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.HOUR_HOUR)
        step = this.calcHourHour_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.HOUR_MINUTE)
        step = this.calcHourMinute_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MINUTE_MINUTE)
        step = this.calcMinuteMinute_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MINUTE_SECOND)
        step = this.calcMinuteSecond_(scale, range, targetSteps);
    else if (range > anychart.plots.axesPlot.scales.dateTime.DateTimeRange.DAY_HOUR)
        step = this.calcSecondSecond_(scale, range, targetSteps);

    return step;
};
//------------------------------------------------------------------------------
//                                  Utils.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getYearStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR);
    tempStep = Math.ceil(tempStep / (365 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER));
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getMonthStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH);
    tempStep = Math.ceil(tempStep / (30 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER));
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getDayStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY);
    tempStep = Math.ceil(tempStep / anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER);
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getHourStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR);
    tempStep = Math.ceil((tempStep) / anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER);
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getMinuteStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE);
    tempStep = Math.ceil((tempStep * 60 * 24) / anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER);
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 * @return {Number}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.getSecondStep_ = function(scale, range, targetSteps) {
    var tempStep = range / targetSteps;
    scale.setMajorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND);
    tempStep = Math.ceil((tempStep * 60 * 60 * 24) / anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER);
    return tempStep;
};
/**
 * @private
 * @param {number} step
 * @return {int}
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.boundMinuteOrSecondStep_ = function(step) {
    if (step > 15)
        return 30;
    if (step > 5)
        return 15;
    if (step > 1)
        return 5;
    else
        return 1;
};
//------------------------------------------------------------------------------
//                              Calculation.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcYearYear_ = function(scale, range, targetSteps) {
    var tempStep = this.getYearStep_(scale, range, targetSteps);

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR);
        (tempStep == 1) ? scale.setMinorInterval(.25) : scale.setMinorInterval(scale.calcStepSize(tempStep, 7));
    }

    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcYearMonth_ = function(scale, range, targetSteps) {
    var tempStep = this.getYearStep_(scale, range, targetSteps);
    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH);
        scale.setMinorInterval(Math.ceil(range / (targetSteps * 3) / (30 * anychart.plots.axesPlot.scales.dateTime.DateTimeRange.MULTIPLIER)));
        if (scale.getMinorInterval() > 6) scale.setMinorInterval(12);
        else if (scale.getMinorInterval() > 3) scale.setMinorInterval(6);
    }
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcMonthMonth_ = function(scale, range, targetSteps) {
    var tempStep = this.getMonthStep_(scale, range, targetSteps);
    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY);
        scale.setMinorInterval(tempStep * (30 / 4));
    }

    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcDayDay_ = function(scale, range, targetSteps) {
    var tempStep = this.getDayStep_(scale, range, targetSteps);

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY);
        var minorInterval = tempStep / 4;
        if (minorInterval > 12) minorInterval = 12;
        else if (minorInterval > 6) minorInterval = 6;
        else if (minorInterval > 3) minorInterval = 3;
        else if (minorInterval > 2) minorInterval = 2;
        else minorInterval = 1;
        scale.setMinorInterval(minorInterval);
    }
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcDayHour_ = function(scale, range, targetSteps) {
    var tempStep = this.getDayStep_(scale, range, targetSteps);
    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR);
        var minorInterval = Math.ceil(range / (targetSteps * 3) * 24);
        if (minorInterval > 6) minorInterval = 12;
        else if (minorInterval > 3) minorInterval = 6;
        else minorInterval = 1;
        scale.setMinorInterval(minorInterval);
    }
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcHourHour_ = function(scale, range, targetSteps) {
    var tempStep = this.getHourStep_(scale, range, targetSteps);
    if (tempStep > 12) tempStep = 24;
    else if (tempStep > 6) tempStep = 12;
    else if (tempStep > 2) tempStep = 6;
    else if (tempStep > 1) tempStep = 2;
    else tempStep = 1;

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR);
        var minorInterval;
        if (tempStep <= 1) minorInterval = .25;
        else if (tempStep <= 6) minorInterval = 1;
        else if (tempStep <= 12) minorInterval = 2;
        else minorInterval = 4;
        scale.setMinorInterval(minorInterval);
    }

    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcHourMinute_ = function(scale, range, targetSteps) {
    var tempStep = this.getHourStep_(scale, range, targetSteps);
    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE);
        var minorInterval = Math.ceil(range / (targetSteps * 3) * 60);
        scale.setMinorInterval(this.boundMinuteOrSecondStep_(minorInterval));
    }
    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcMinuteMinute_ = function(scale, range, targetSteps) {
    var tempStep = this.getMinuteStep_(scale, range, targetSteps);
    tempStep = this.boundMinuteOrSecondStep_(tempStep);

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE);
        if (tempStep <= 1) scale.setMinorInterval(.25);
        else if (tempStep <= 5) scale.setMinorInterval(1);
        else scale.setMinorInterval(5);
    }

    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcMinuteSecond_ = function(scale, range, targetSteps) {
    var tempStep = this.getMinuteStep_(scale, range, targetSteps);

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND);
        var minorInterval = Math.ceil(range / (targetSteps * 3) * (60 * 60 * 24));
        minorInterval = this.boundMinuteOrSecondStep_(minorInterval);
        scale.setMinorInterval(minorInterval);
    }

    return tempStep;
};
/**
 * @private
 * @param {anychart.plots.axesPlot.scales.DateTimeScale} scale
 * @param {Number} range
 * @param {int} targetSteps
 */
anychart.plots.axesPlot.scales.dateTime.DateTimeRange.prototype.calcSecondSecond_ = function(scale, range, targetSteps) {
    var tempStep = this.getSecondStep_(scale, range, targetSteps);
    tempStep = this.boundMinuteOrSecondStep_(tempStep);

    if (scale.isMinorIntervalAuto()) {
        scale.setMinorIntervalUnit(anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND);
        if (tempStep <= 1) scale.setMinorInterval(.25);
        else if (tempStep <= 5) scale.setMinorInterval(1);
        else scale.setMinorInterval(5);
    }

    return tempStep;
};
