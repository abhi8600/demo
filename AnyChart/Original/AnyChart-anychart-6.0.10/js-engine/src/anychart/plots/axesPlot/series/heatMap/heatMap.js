/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.heatMap.HeatMapPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.heatMap.HeatMapSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.heatMap');
goog.require('anychart.plots.axesPlot.series.heatMap.styles');
goog.require('anychart.plots.axesPlot.data');
//------------------------------------------------------------------------------
//
//                          HeatMapPoint.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.SingleValuePoint}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapPoint = function() {
    anychart.plots.axesPlot.data.SingleValuePoint.call(this);
    this.leftTop_ = new anychart.utils.geom.Point();
    this.rightBottom_ = new anychart.utils.geom.Point();
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.HeatMapPoint,
    anychart.plots.axesPlot.data.SingleValuePoint);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.leftTop_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.rightBottom_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.row_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.colomn_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.deserializeArgument_ = function(data, svgManager) {
    var des = anychart.utils.deserialization;

    var argumentAxis = this.series_.getArgumentAxis();
    var valueAxis = this.series_.getValueAxis();


    var columnName = des.getStringProp(data, 'column');
    var rowName = des.getStringProp(data, 'row');

    this.isHorizontal_ =
        argumentAxis.getPosition() === anychart.layout.Position.LEFT ||
            argumentAxis.getPosition() === anychart.layout.Position.RIGHT;

    this.category_ = argumentAxis.getCategory(columnName, svgManager);
    this.yCategory_ = valueAxis.getCategory(rowName, svgManager);

    this.column_ = this.category_.getIndex();
    this.row_ = this.yCategory_.getIndex();

    this.clusterSettings_ = this.series_.getClusterSettings();
    this.paletteIndex_ = this.category_.getIndex();

    if (!this.isMissing_) this.category_.checkPoint(this);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.checkMissing_ = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'y')) {
        this.isMissing_ = isNaN(this.deserializeValueFromConfig_(des.getProp(data, 'y')));
    }
    this.isMissing_ = this.isMissing_ || (!des.hasProp(data, 'column') || !des.hasProp(data, 'row'));
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.deserializeValue_ = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'y')) this.y_ = des.getNumProp(data, 'y');
};
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.initialize = function(svgManager) {
    return goog.base(this, 'initialize', svgManager);
};
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.calculateBounds = function() {
    var argumentAxis = this.series_.getArgumentAxis();
    var valueAxis = this.series_.getValueAxis();

    argumentAxis.transform(this, this.column_ - .5, this.leftTop_);
    argumentAxis.transform(this, this.column_ + .5, this.rightBottom_);
    valueAxis.transform(this, this.row_ - .5, this.leftTop_);
    valueAxis.transform(this, this.row_ + .5, this.rightBottom_);

    var x = Math.min(this.leftTop_.x, this.rightBottom_.x);
    var y = Math.min(this.leftTop_.y, this.rightBottom_.y);
    this.bounds_ = new anychart.utils.geom.Rectangle(
        x,
        y,
        Math.max(this.leftTop_.x, this.rightBottom_.x) - x,
        Math.max(this.leftTop_.y, this.rightBottom_.y) - y);
};
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------

anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.setDefaultTokenValues = function() {
    this.tokensHash_.add('%Column', 0);
    this.tokensHash_.add('%Row', 0);
};
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.getTokenType = function(token) {
    switch (token) {
        case '%Column':
        case '%Row':
            return anychart.formatting.TextFormatTokenType.NUMBER;

    }
    return goog.base(this, 'getTokenType', token);
};
anychart.plots.axesPlot.series.heatMap.HeatMapPoint.prototype.calculatePointTokenValues = function() {
    this.tokensHash_.add('%Column', this.category_.getName());
    this.tokensHash_.add('%Row', this.yCategory_.getName());
    goog.base(this, 'calculatePointTokenValues');
};

//------------------------------------------------------------------------------
//
//                          HeatMapSeries.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.SingleValueSeries}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapSeries = function() {
    anychart.plots.axesPlot.data.SingleValueSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.HeatMapSeries,
    anychart.plots.axesPlot.data.SingleValueSeries);
//------------------------------------------------------------------------------
//                               Point.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.heatMap.HeatMapPoint();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return false;
};
//------------------------------------------------------------------------------
//
//                          HeatMapGlobalSeriesSetting.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting,
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                              Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting.prototype.createSeries = function(seriesType) {
    var series = new anychart.plots.axesPlot.series.heatMap.HeatMapSeries();
    series.setType(anychart.series.SeriesType.HEAT_MAP);
    series.setClusterKey(anychart.series.SeriesType.HEAT_MAP);
    return series;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting.prototype.getSettingsNodeName = function() {
    return 'heat_map';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting.prototype.getStyleNodeName = function() {
    return 'heat_map_style';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyle();
};
