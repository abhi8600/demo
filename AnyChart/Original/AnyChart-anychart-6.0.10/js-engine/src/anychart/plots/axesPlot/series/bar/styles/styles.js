/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BarShapeType};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BarStyleState};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BarStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BoxFillWithStrokeStyleShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BoxHatchFillStyleShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.BarStyle};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyle};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.VerticalConeStyle};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.styles.RangeBarStyle};</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.bar.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                          BarShapeType class
//
//------------------------------------------------------------------------------
/**
 * Describe possible bar types.
 * @enum {int}
 */
anychart.plots.axesPlot.series.bar.styles.BarShapeType = {
    BOX:0,
    CYLINDER:1,
    PYRAMID:2,
    CONE:3,

    deserialize:function (type) {
        switch (type) {
            case 'cylinder':
                return anychart.plots.axesPlot.series.bar.styles.BarShapeType.CYLINDER;
            case 'pyramid':
                return anychart.plots.axesPlot.series.bar.styles.BarShapeType.PYRAMID;
            case 'cone':
                return anychart.plots.axesPlot.series.bar.styles.BarShapeType.CONE;
            default:
                return anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX;
        }
    }
};
//------------------------------------------------------------------------------
//
//                          BarStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleState = function (style, opt_stateType) {
    anychart.styles.background.BackgroundStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BarStyleState,
    anychart.styles.background.BackgroundStyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.corners.Corners}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleState.prototype.corners_ = null;
/**
 * @return {anychart.visual.corners.Corners}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleState.prototype.getCorners = function () {
    return this.corners_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BarStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    if (anychart.utils.deserialization.hasProp(data, 'corners')) {
        this.corners_ = new anychart.visual.corners.Corners();
        this.corners_.deserialize(anychart.utils.deserialization.getProp(data, 'corners'));
    }
};

//------------------------------------------------------------------------------
//
//                          HorizontalConeStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState,
    anychart.styles.background.BackgroundStyleState);

anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    var color = des.getProp(des.getProp(data, 'fill'), 'color');
    var fillNode = des.getProp(data, 'fill');
    var opacity = des.hasProp(fillNode, 'opacity') ? des.getProp(fillNode, 'opacity') : 1;

    this.deserializeFill_(color, opacity);
};
anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState.prototype.deserializeFill_ = function (color, opacity) {
    var fillData = {};
    var gradientData = {};
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};
    key1['position'] = '0';
    key1['opacity'] = opacity;
    key1['color'] = 'DarkColor(' + color + ')';
    keys.push(key1);

    key2['position'] = 0.41;
    key2['opacity'] = opacity;
    key2['color'] = 'LightColor(' + color + ')';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = opacity;
    key3['color'] = 'DarkColor(' + color + ')';
    keys.push(key3);

    gradientData['type'] = 'linear';
    gradientData['key'] = keys;
    gradientData['angle'] = 90;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.fill_ = new anychart.visual.fill.Fill();
    this.fill_.deserialize(fillData);

    this.stroke_ = new anychart.visual.stroke.Stroke();
    this.stroke_.deserialize(fillData);
};

//------------------------------------------------------------------------------
//
//                          VerticalConeStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState,
    anychart.styles.background.BackgroundStyleState);

anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    var color = des.getProp(des.getProp(data, 'fill'), 'color');
    var fillNode = des.getProp(data, 'fill');
    var opacity = des.hasProp(fillNode, 'opacity') ? des.getProp(fillNode, 'opacity') : 1;

    this.deserializeFill_(color, opacity);
};
anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState.prototype.deserializeFill_ = function (color, opacity) {
    var fillData = {};
    var gradientData = {};
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};
    key1['position'] = '0';
    key1['opacity'] = opacity;
    key1['color'] = 'DarkColor(' + color + ')';
    keys.push(key1);

    key2['position'] = 0.41;
    key2['opacity'] = opacity;
    key2['color'] = 'LightColor(' + color + ')';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = opacity;
    key3['color'] = 'DarkColor(' + color + ')';
    keys.push(key3);

    gradientData['type'] = 'linear';
    gradientData['key'] = keys;
    gradientData['angle'] = '0';

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.fill_ = new anychart.visual.fill.Fill();
    this.fill_.deserialize(fillData);

    this.stroke_ = new anychart.visual.stroke.Stroke();
    this.stroke_.deserialize(fillData);
};

//------------------------------------------------------------------------------
//
//                       BarStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes = function () {
    anychart.styles.background.BackgroundStyleShapes.call(this);
    this.leftTop_ = new anychart.utils.geom.Point();
    this.rightBottom_ = new anychart.utils.geom.Point();
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BarStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * left top bar point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.leftTop_ = null;
/**
 * Right top bar point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.rightBottom_ = null;
/**
 * Indicates, is stacked enabled.
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.isStacked_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.startValue_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.endValue_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.calculateBounds = function () {
    var series = this.point_.getSeries();
    this.startValue_ = isNaN(this.point_.getStartStackValue()) ?
        series.getValueAxisStart() : this.point_.getStartStackValue();

    this.endValue_ = this.point_.getStackValue();

    var w = this.point_.getGlobalSettings().getBarWidth(this.point_) / 2;

    this.isStacked_ = this.point_.getCategory() != null &&
        this.point_.getCategory().isStackSettingsExists(
            series.getValueAxis().getName(),
            series.getType());

    series.getArgumentAxis().transform(this.point_, this.point_.getX(), this.leftTop_, -w);
    series.getValueAxis().transform(this.point_, this.startValue_, this.leftTop_);
    series.getArgumentAxis().transform(this.point_, this.point_.getX(), this.rightBottom_, w);
    series.getValueAxis().transform(this.point_, this.endValue_, this.rightBottom_);

    this.point_.getBounds().x = Math.min(this.leftTop_.x, this.rightBottom_.x);
    this.point_.getBounds().y = Math.min(this.leftTop_.y, this.rightBottom_.y);
    this.point_.getBounds().width = Math.max(this.leftTop_.x, this.rightBottom_.x) - this.point_.getBounds().x;
    this.point_.getBounds().height = Math.max(this.leftTop_.y, this.rightBottom_.y) - this.point_.getBounds().y;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.getAnchorPoint = function (anchor, position, bounds, padding) {
    goog.abstractMethod();
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.isCornersVisible = function () {
    return false;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.prototype.isBottomGradientVisible = function () {
    return true;
};
//------------------------------------------------------------------------------
//
//                     BoxFillWithStrokeStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bar.styles.BoxFillWithStrokeStyleShape = function () {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BoxFillWithStrokeStyleShape,
    anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                             Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BoxFillWithStrokeStyleShape.prototype.updatePath = function () {
    this.styleShapes_.updateBarPath(this.element_, this.point_.getBounds());
};
//------------------------------------------------------------------------------
//
//                     BoxHatchFillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bar.styles.BoxHatchFillStyleShape = function () {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BoxHatchFillStyleShape,
    anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                             Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BoxHatchFillStyleShape.prototype.updatePath = function () {
    this.styleShapes_.updateBarPath(this.element_, this.point_.getBounds());
};
//------------------------------------------------------------------------------
//
//                       BoxStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BarStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.createShapes = function () {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.axesPlot.series.bar.styles.BoxFillWithStrokeStyleShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.bar.styles.BoxHatchFillStyleShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData === true) this.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
//------------------------------------------------------------------------------
//                  .
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.updateBarPath = function (element, bounds) {
    element.setAttribute('d', this.point_.getSVGManager().pRectByBounds(bounds));
};
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.setAnchorPoint = function (position, anchor, bounds, padding) {
    if (this.leftTop_.x > this.rightBottom_.x) {
        var tmp = this.leftTop_.x;
        this.leftTop_.x = this.rightBottom_.x;
        this.rightBottom_.x = tmp;
    }
    if (this.leftTop_.y < 0) this.leftTop_.y = 0;
    if (this.leftTop_.y > this.rightBottom_.y) {
        var tmp1 = this.leftTop_.y;
        this.leftTop_.y = this.rightBottom_.y;
        this.rightBottom_.y = tmp1;
    }
    switch (anchor) {
        case anychart.layout.Anchor.CENTER:
        {
            position.x = (this.leftTop_.x + this.rightBottom_.x) / 2;
            position.y = (this.leftTop_.y + this.rightBottom_.y) / 2;
            break;
        }

        case anychart.layout.Anchor.CENTER_LEFT:
        {
            position.x = this.leftTop_.x;
            position.y = (this.leftTop_.y + this.rightBottom_.y) / 2;
            break;
        }
        case anychart.layout.Anchor.CENTER_RIGHT:
        {
            position.x = this.rightBottom_.x;
            position.y = (this.leftTop_.y + this.rightBottom_.y) / 2;
            break;
        }
        case anychart.layout.Anchor.CENTER_BOTTOM:
        {
            position.x = (this.leftTop_.x + this.rightBottom_.x) / 2;
            position.y = this.rightBottom_.y;
            break;
        }
        case anychart.layout.Anchor.CENTER_TOP:
        {
            position.x = (this.leftTop_.x + this.rightBottom_.x) / 2;
            position.y = this.leftTop_.y;
            break;
        }
        case anychart.layout.Anchor.LEFT_TOP:
        {
            position.x = this.leftTop_.x;
            position.y = this.leftTop_.y;
            break;
        }
        case anychart.layout.Anchor.RIGHT_TOP:
        {
            position.x = this.rightBottom_.x;
            position.y = this.leftTop_.y;
            break;
        }
        case anychart.layout.Anchor.LEFT_BOTTOM:
        {
            position.x = this.leftTop_.x;
            position.y = this.rightBottom_.y;
            break;
        }
        case anychart.layout.Anchor.RIGHT_BOTTOM:
        {
            position.x = this.rightBottom_.x;
            position.y = this.rightBottom_.y;
            break;
        }
    }
};
anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.isCornersVisible = function () {
    if (this.isStacked_) return this.point_.getStackSettings().isLastInStack(this.point_);
    return true;
};

anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.prototype.isBottomGradientVisible = function () {
    return !this.isStacked_;
};
//------------------------------------------------------------------------------
//
//                          ConePyramidFillShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidFillShape = function () {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.ConePyramidFillShape,
    anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.ConePyramidFillShape.prototype.updatePath = function () {
    this.element_.setAttribute('d', this.styleShapes_.getElementPath(this.point_.getSVGManager()));
};
//------------------------------------------------------------------------------
//
//                          ConePyramidFillShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidHatchFillShape = function () {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.ConePyramidHatchFillShape,
    anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.ConePyramidHatchFillShape.prototype.updatePath = function () {
    this.element_.setAttribute('d', this.styleShapes_.getElementPath(this.point_.getSVGManager()));
};
//------------------------------------------------------------------------------
//
//                       ConePyramidStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BarStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.call(this);
    this.rightTop_ = new anychart.utils.geom.Point();
    this.leftBottom_ = new anychart.utils.geom.Point();
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.rightTop_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.leftBottom_ = null;
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.createShapes = function () {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.axesPlot.series.bar.styles.ConePyramidFillShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.bar.styles.ConePyramidHatchFillShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.calculateBounds = function () {
    goog.base(this, 'calculateBounds');

    if (this.isStacked_)
        this.initializeStacked(
            this.isInverted(),
            this.point_.getBounds(),
            this.startValue_,
            this.endValue_);
    else
        this.initializeNormal(this.isInverted(), this.point_.getBounds());
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------

anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData === true) this.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**
 * @return {String}
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.getElementPath = function (svgManager) {
    var pathData = svgManager.pMove(this.leftBottom_.x, this.leftBottom_.y);
    pathData += svgManager.pLine(this.leftTop_.x, this.leftTop_.y);
    pathData += svgManager.pLine(this.rightTop_.x, this.rightTop_.y);
    pathData += svgManager.pLine(this.rightBottom_.x, this.rightBottom_.y);
    pathData += svgManager.pFinalize();
    return pathData;
};
//------------------------------------------------------------------------------
//                          Utils.
//------------------------------------------------------------------------------
/**
 * Инициализация Drawer'a когда график не стекирован
 * @param inverted
 * @param bounds
 *
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.initializeNormal = function (inverted, bounds) {
    goog.abstractMethod();
};
/**
 * Инициализация Drawer'a когда график стекирован
 * @param inverted
 * @param bounds
 * @param startValue
 * @param endValue
 *
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.initializeStacked = function (inverted, bounds, startValue, endValue) {
    goog.abstractMethod();
};
/**
 * Define, is value axis inverted.
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.isInverted = function () {
    return false;
};
anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.prototype.setAnchorPoint = function (position, anchor, bounds, padding) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER:
            position.x = (this.leftTop_.x + this.rightTop_.x + this.rightBottom_.x + this.leftBottom_.x) / 4;
            position.y = (this.leftTop_.y + this.rightTop_.y + this.rightBottom_.y + this.leftBottom_.y) / 4;
            break;

        case anychart.layout.Anchor.CENTER_LEFT:
            position.x = (this.leftTop_.x + this.leftBottom_.x) / 2;
            position.y = (this.leftTop_.y + this.rightTop_.y + this.rightBottom_.y + this.leftBottom_.y) / 4;
            break;

        case anychart.layout.Anchor.CENTER_RIGHT:
            position.x = (this.rightTop_.x + this.rightBottom_.x) / 2;
            position.y = (this.leftTop_.y + this.rightTop_.y + this.rightBottom_.y + this.leftBottom_.y) / 4;
            break;

        case anychart.layout.Anchor.CENTER_BOTTOM:
            position.x = (this.leftTop_.x + this.rightTop_.x + this.rightBottom_.x + this.leftBottom_.x) / 4;
            position.y = (this.rightBottom_.y + this.leftBottom_.y) / 2;
            break;

        case anychart.layout.Anchor.CENTER_TOP:
            position.x = (this.leftTop_.x + this.rightTop_.x + this.rightBottom_.x + this.leftBottom_.x) / 4;
            position.y = (this.leftTop_.y + this.rightTop_.y) / 2;
            break;

        case anychart.layout.Anchor.LEFT_TOP:
            position.x = this.leftTop_.x;
            position.y = this.leftTop_.y;
            break;

        case anychart.layout.Anchor.RIGHT_TOP:
            position.x = this.rightTop_.x;
            position.y = this.rightTop_.y;
            break;

        case anychart.layout.Anchor.LEFT_BOTTOM:
            position.x = this.leftBottom_.x;
            position.y = this.leftBottom_.y;
            break;

        case anychart.layout.Anchor.RIGHT_BOTTOM:
            position.x = this.rightBottom_.x;
            position.y = this.rightBottom_.y;
            break;
    }
};
//------------------------------------------------------------------------------
//
//                       HorizontalConePyramidStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes);
//------------------------------------------------------------------------------
//                          Inverted.
//------------------------------------------------------------------------------

/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes.prototype.getInverted = function () {
    return this.leftTop_.x > this.rightBottom_.x;
};
//------------------------------------------------------------------------------
//                          Normal.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes.prototype.initializeNormal = function (inverted, bounds) {
    this.rightTop_.x = this.rightBottom_.x = bounds.getRight();
    this.rightBottom_.y = inverted ? bounds.getBottom() : bounds.y + bounds.height / 2;
    this.rightTop_.y = inverted ? bounds.getTop() : bounds.y + bounds.height / 2;

    this.leftTop_.x = this.leftBottom_.x = bounds.getLeft();
    this.leftBottom_.y = !inverted ? bounds.getBottom() : bounds.y + bounds.height / 2;
    this.leftTop_.y = !inverted ? bounds.getTop() : bounds.y + bounds.height / 2;
};
//------------------------------------------------------------------------------
//                          Stacked.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes.prototype.initializeStacked = function (inverted, bounds, startValue, endValue) {
    var series = this.point_.getSeries();
    var stackSettings = this.point_.getCategory().getStackSettings(
        series.getValueAxis().getName(),
        series.getType(),
        series.getValueAxis().getScale().isPercentStacked());

    var stackSumm = stackSettings.getStackSum(this.point_.getY());
    var bottomWidth = bounds.height * (stackSumm - startValue) / stackSumm;
    var topWidth = bounds.height * (stackSumm - endValue) / stackSumm;

    this.rightBottom_.x = this.rightTop_.x = bounds.getRight();
    this.leftBottom_.x = this.leftTop_.x = bounds.getLeft();
    inverted ? bounds.getLeft() : bounds.getRight();

    this.rightTop_.y = bounds.y + (!inverted ? (bounds.height - topWidth) / 2 : (bounds.height - bottomWidth) / 2);
    this.rightBottom_.y = bounds.y + (!inverted ? (bounds.height + topWidth) / 2 : (bounds.height + bottomWidth) / 2);
    this.leftTop_.y = bounds.y + (inverted ? (bounds.height - topWidth) / 2 : (bounds.height - bottomWidth) / 2);
    this.leftBottom_.y = bounds.y + (inverted ? (bounds.height + topWidth) / 2 : (bounds.height + bottomWidth) / 2);
};
//------------------------------------------------------------------------------
//
//                       VerticalConePyramidStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.ConePyramidStyleShapes);
//------------------------------------------------------------------------------
//                          Inverted.
//------------------------------------------------------------------------------

/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes.prototype.getInverted = function () {
    return this.leftTop_.y < this.rightBottom_.y;
};
//------------------------------------------------------------------------------
//                          Normal.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes.prototype.initializeNormal = function (inverted, bounds) {
    this.rightTop_.y = this.leftTop_.y = bounds.getTop();
    this.rightBottom_.y = this.leftBottom_.y = bounds.getBottom();

    this.rightTop_.x = inverted ? bounds.getRight() : bounds.x + bounds.width / 2;
    this.leftTop_.x = inverted ? bounds.getLeft() : bounds.x + bounds.width / 2;
    this.leftBottom_.x = inverted ? bounds.x + bounds.width / 2 : bounds.getLeft();
    this.rightBottom_.x = inverted ? bounds.x + bounds.width / 2 : bounds.getRight();
};
//------------------------------------------------------------------------------
//                          Stacked.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes.prototype.initializeStacked = function (inverted, bounds, startValue, endValue) {
    var series = this.point_.getSeries();
    var stackSettings = this.point_.getCategory().getStackSettings(
        series.getValueAxis().getName(),
        series.getType(),
        series.getValueAxis().getScale().isPercentStacked());
    var stackSumm = stackSettings.getStackSum(this.point_.getY());
    var bottomWidth = bounds.width * (stackSumm - startValue) / stackSumm;
    var topWidth = bounds.width * (stackSumm - endValue) / stackSumm;

    this.leftBottom_.y = this.rightBottom_.y = bounds.getBottom();
    this.leftTop_.y = this.rightTop_.y = bounds.getTop();

    this.rightBottom_.x = bounds.x + (!inverted ? (bounds.width + bottomWidth) / 2 : (bounds.width + topWidth) / 2);
    this.leftBottom_.x = bounds.x + (!inverted ? (bounds.width - bottomWidth) / 2 : (bounds.width - topWidth) / 2);
    this.rightTop_.x = bounds.x + (inverted ? (bounds.width + bottomWidth) / 2 : (bounds.width + topWidth) / 2);
    this.leftTop_.x = bounds.x + (inverted ? (bounds.width - bottomWidth) / 2 : (bounds.width - topWidth) / 2);
};
//------------------------------------------------------------------------------
//
//                       CylinderStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BarStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.BarStyleShapes);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes.prototype.calculateBounds = function () {
    console.log('calculateBounds');
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createRect(200, 100, 100, 100);
};
//------------------------------------------------------------------------------
//
//                       BarStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.bar.styles.BarStyle = function () {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.BarStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BarStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.axesPlot.series.bar.styles.BarStyleState;
};

//------------------------------------------------------------------------------
//
//                       HorizontalConeStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BarStyle}
 */
anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyle,
    anychart.plots.axesPlot.series.bar.styles.BarStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyleState;
};

//------------------------------------------------------------------------------
//
//                       VerticalConeStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BarStyle}
 */
anychart.plots.axesPlot.series.bar.styles.VerticalConeStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.VerticalConeStyle,
    anychart.plots.axesPlot.series.bar.styles.BarStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.VerticalConeStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.axesPlot.series.bar.styles.VerticalConeStyleState;
};

//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.BarStyle.prototype.createStyleShapes = function (point) {
    switch (point.getShapeType()) {
        default:
        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX:
            return new anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes();

        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.PYRAMID:
        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.CONE:
            if (point.getSeries().getPlot().isHorizontal())
                return new anychart.plots.axesPlot.series.bar.styles.HorizontalConePyramidStyleShapes();
            else
                return new anychart.plots.axesPlot.series.bar.styles.VerticalConePyramidStyleShapes();

        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.CYLINDER:
            //return new anychart.plots.axesPlot.series.bar.styles.CylinderStyleShapes();
            return new anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes();

    }
};
//------------------------------------------------------------------------------
//
//                       RangeBarStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes}
 */
anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes = function () {
    anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes,
    anychart.plots.axesPlot.series.bar.styles.BoxStyleShapes);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes.prototype.calculateBounds = function () {
    this.point_.calculateBounds();
};
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_updateData === true) this.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
//------------------------------------------------------------------------------
//
//                       RangeBarStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.bar.styles.RangeBarStyle = function () {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.styles.RangeBarStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.RangeBarStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.axesPlot.series.bar.styles.BarStyleState;
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.styles.RangeBarStyle.prototype.createStyleShapes = function (point) {
    return new anychart.plots.axesPlot.series.bar.styles.RangeBarStyleShapes();
};

