goog.provide('anychart.plots.axesPlot.series.marker.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.marker.DEFAULT_TEMPLATE = {
    "chart": {
        "styles": {
        },
        "data_plot_settings": {
            "marker_series": {
                "animation": {
                    "style": "defaultScaleXYCenter"
                },
                "label_settings": {
                    "animation": {
                        "style": "defaultLabel"
                    },
                    "enabled": "False"
                },
                "tooltip_settings": {
                }
            }
        }
    }
};