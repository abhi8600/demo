goog.provide('anychart.plots.axesPlot.axes.viewPorts');

goog.require('anychart.layout');
goog.require('anychart.plots.axesPlot.axes.viewPorts.tickmarks');
goog.require('anychart.plots.axesPlot.axes.viewPorts.drawing');
goog.require('anychart.plots.axesPlot.axes.viewPorts.layout');
goog.require('anychart.plots.axesPlot.axes.viewPorts.title');
goog.require('anychart.plots.axesPlot.axes.viewPorts.labels');
goog.require('anychart.plots.axesPlot.axes.viewPorts.markers');

//-----------------------------------------------------------------------------------
//
//                          ViewPortFactory class
//
//-----------------------------------------------------------------------------------

anychart.plots.axesPlot.axes.viewPorts.ViewPortFactory = function() {
};

/**
 * @public
 * @static
 * @param {anychart.plots.axesPlot.axes.Axis} axis
 * @return {anychart.plots.axesPlot.axes.viewPorts.AxisViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.ViewPortFactory.createViewPort = function(axis) {
    switch (axis.getPosition()) {
        case anychart.layout.Position.LEFT:
            return new anychart.plots.axesPlot.axes.viewPorts.LeftAxisViewPort(axis);
        case anychart.layout.Position.RIGHT:
            return new anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort(axis);
        case anychart.layout.Position.TOP:
            return new anychart.plots.axesPlot.axes.viewPorts.TopAxisViewPort(axis);
        case anychart.layout.Position.BOTTOM:
            return new anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort(axis);
    }
    return null;
};


//-----------------------------------------------------------------------------------
//
//                          AxisViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent base view port.
 * @public
 * @constructor
 * @param {anychart.plots.axesPlot.axes.Axis} axis Axis.
 */
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort = function(axis) {
    this.axis_ = axis;
};

/** @protected */
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.bounds_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getBounds = function() {
    return this.bounds_;
};

/** @protected */
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.axis_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getAxis = function() {
    return this.axis_;
};

/** @protected */
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.tickmarksViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getTickmarksViewPort = function() {
    return this.tickmarksViewPort_;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.drawingViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getDrawingViewPort = function() {
    return this.drawingViewPort_;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.layoutViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getLayoutViewPort = function() {
    return this.layoutViewPort_;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.titleViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getTitleViewPort = function() {
    return this.titleViewPort_;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.labelsViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getLabelsViewPort = function() {
    return this.labelsViewPort_;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.markersViewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getMarkersViewPort = function() {
    return this.markersViewPort_;
};


anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.setScaleBounds = function(bounds) {
    this.bounds_ = bounds;
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.applyOffset = function(bounds, offset) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getVisibleStart = function() {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getVisibleEnd = function() {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.setScaleValue = function(value, point) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getScaleValue = function(point) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.setOffset = function(padding, bounds, point) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.setStartValue = function(value, point) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.setEndValue = function(value, point) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.prototype.getSpace = function(bounds) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalAxisViewPort_ class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_ = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.call(this, axis);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_, anychart.plots.axesPlot.axes.viewPorts.AxisViewPort);

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setScaleValue = function(value, point) {
    point.x = this.bounds_.x + value;
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.getScaleValue = function(point) {
    return point.x;
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setScaleBounds = function(bounds) {
    goog.base(this, 'setScaleBounds', bounds);
    this.axis_.getScale().setPixelRange(0, bounds.width);
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setStartValue = function(value, point) {
    point.x = value;
    point.y = this.bounds_.y;
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setEndValue = function(value, point) {
    point.x = value;
    point.y = this.bounds_.getBottom();
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.getVisibleStart = function() {
    return this.bounds_.getLeft();
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.getVisibleEnd = function() {
    return this.bounds_.getRight();
};

anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.getSpace = function(bounds) {
    return bounds.height;
};

//-----------------------------------------------------------------------------------
//
//                          VerticalAxisViewPort_ class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_ = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.AxisViewPort.call(this, axis);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_, anychart.plots.axesPlot.axes.viewPorts.AxisViewPort);

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setScaleBounds = function(bounds) {
    goog.base(this, 'setScaleBounds', bounds);
    this.axis_.getScale().setPixelRange(0, bounds.height);
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setScaleValue = function(value, point) {
    point.y = this.bounds_.getBottom() - value;
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.getScaleValue = function(point) {
    return this.axis_.getScale().getPixelRangeMaximum() - point.y;
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setStartValue = function(value, point) {
    point.y = value;
    point.x = this.bounds_.x;
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setEndValue = function(value, point) {
    point.y = value;
    point.x = this.bounds_.getRight();
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.getVisibleStart = function() {
    return this.bounds_.y;
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.getVisibleEnd = function() {
    return this.bounds_.getBottom();
};

anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.getSpace = function(bounds) {
    return bounds.width;
};

//-----------------------------------------------------------------------------------
//
//                          TopAxisViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.TopAxisViewPort = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.call(this, axis);

    this.tickmarksViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort(this);
    this.drawingViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.drawing.TopAxisDrawingViewPort(this);
    this.layoutViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.layout.TopLayoutViewPort();
    this.titleViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.title.TopAxisTitleViewPort(this);
    this.labelsViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort(this);
    this.markersViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.TopAxisViewPort, anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_);

anychart.plots.axesPlot.axes.viewPorts.TopAxisViewPort.prototype.applyOffset = function(bounds, offset) {
    bounds.height -= offset;
    bounds.y += offset;
};

anychart.plots.axesPlot.axes.viewPorts.TopAxisViewPort.prototype.setOffset = function(offset, bounds, pos) {
    pos.y = this.bounds_.y - offset - bounds.height;
    pos.x -= bounds.width / 2;
};

//-----------------------------------------------------------------------------------
//
//                          BottomAxisViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.call(this, axis);

    this.tickmarksViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort(this);
    this.drawingViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.drawing.BottomAxisDrawingViewPort(this);
    this.layoutViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.layout.BottomLayoutViewPort();
    this.titleViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.title.BottomAxisTitleViewPort(this);
    this.labelsViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort(this);
    this.markersViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort, anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_);

anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort.prototype.applyOffset = function(bounds, offset) {
    bounds.height -= offset;
};

anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort.prototype.setStartValue = function(value, pt) {
    //goog.base(this, 'setEndValue',value, pt);
    anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setEndValue.call(this, value, pt);
};

anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort.prototype.setEndValue = function(value, pt) {
    //goog.base(this, 'setStartValue',value, pt);
    anychart.plots.axesPlot.axes.viewPorts.HorizontalAxisViewPort_.prototype.setStartValue.call(this, value, pt);
};

anychart.plots.axesPlot.axes.viewPorts.BottomAxisViewPort.prototype.setOffset = function(offset, bounds, pos) {
    pos.y = this.bounds_.getBottom() + offset;
    pos.x -= bounds.width / 2;
};

//-----------------------------------------------------------------------------------
//
//                          LeftAxisViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.LeftAxisViewPort = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.call(this, axis);

    this.tickmarksViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort(this);
    this.drawingViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.drawing.LeftAxisDrawingViewPort(this);
    this.layoutViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.layout.LeftLayoutViewPort();
    this.titleViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.title.LeftAxisTitleViewPort(this);
    this.labelsViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort(this);
    this.markersViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.LeftAxisViewPort, anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_);

anychart.plots.axesPlot.axes.viewPorts.LeftAxisViewPort.prototype.applyOffset = function(bounds, offset) {
    bounds.width -= offset;
    bounds.x += offset;
};

anychart.plots.axesPlot.axes.viewPorts.LeftAxisViewPort.prototype.setOffset = function(offset, bounds, pos) {
    pos.x = this.bounds_.x - offset - bounds.width;
    pos.y -= bounds.height / 2;
};

//-----------------------------------------------------------------------------------
//
//                          RightAxisViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort = function(axis) {
    anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.call(this, axis);

    this.tickmarksViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort(this);
    this.drawingViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.drawing.RightAxisDrawingViewPort(this);
    this.layoutViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.layout.RightLayoutViewPort();
    this.titleViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.title.RightAxisTitleViewPort(this);
    this.labelsViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort(this);
    this.markersViewPort_ = new anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort, anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_);

anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort.prototype.applyOffset = function(bounds, offset) {
    bounds.width -= offset;
};

anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort.prototype.setOffset = function(offset, bounds, pos) {
    pos.x = this.bounds_.getRight() + offset;
    pos.y -= bounds.height / 2;
};

anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort.prototype.setStartValue = function(value, pt) {
    //goog.base(this, 'setEndValue', value, pt);
    anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setEndValue.call(this, value, pt);
};

anychart.plots.axesPlot.axes.viewPorts.RightAxisViewPort.prototype.setEndValue = function(value, pt) {
    //goog.base(this, 'setStartValue', value, pt);
    anychart.plots.axesPlot.axes.viewPorts.VerticalAxisViewPort_.prototype.setStartValue.call(this, value, pt);
};