/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.line.LinePoint};</li>
 *  <li>@class {anychart.plots.axesPlot.series.line.LineSeries};</li>
 *  <li>@class {anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings};</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.line');
goog.require('anychart.plots.axesPlot.series.line.styles');
//------------------------------------------------------------------------------
//
//                          LinePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.StackablePoint}
 */
anychart.plots.axesPlot.series.line.LinePoint = function() {
    anychart.plots.axesPlot.data.StackablePoint.call(this);
    this.pixValuePt_ = new anychart.utils.geom.Point();
};
goog.inherits(anychart.plots.axesPlot.series.line.LinePoint,
        anychart.plots.axesPlot.data.StackablePoint);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.isPointInitialized_ = false;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.needCalcPixValue_ = true;
anychart.plots.axesPlot.series.line.LinePoint.prototype.setPixValueCalculated = function() {
    this.needCalcPixValue_ = false;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.hasPrev_ = false;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.hasNext_ = false;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.isLineDrawable_ = false;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.isLineDrawable = function() {
    return this.isLineDrawable_;
};
/**
 * @private
 * @type {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.drawingPoints_ = null;
/**
 * @return {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.getDrawingPoints = function() {
    return this.drawingPoints_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.pixValuePt_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.getPixValuePoint = function() {
    return this.pixValuePt_;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LinePoint.prototype.initialize = function(svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};
anychart.plots.axesPlot.series.line.LinePoint.prototype.calcBounds = function() {
    if(!this.isInitialized_) this.initializePixValue_();

    this.bounds_ = new anychart.utils.geom.Rectangle(
            this.pixValuePt_.x,
            this.pixValuePt_.y,
            0,
            0);

    if (!this.isLineDrawable_) return;
    this.createDrawingPoints();
};
/**
 * Initialize point shapes pix value.
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.initializePixValue_ = function() {
    if (!this.isPointInitialized_) {
        
        this.hasPrev_ = (this.index_ > 0) &&
                (this.series_.getPointAt(this.index_ - 1)) &&
                (!this.series_.getPointAt(this.index_ - 1).isMissing() || !this.getPlot().ignoreMissingPoints_);

        this.hasNext_ = (this.index_ < (this.series_.getNumPoints() - 1)) &&
                (this.series_.getPointAt(this.index_ + 1)) &&
                (!this.series_.getPointAt(this.index_ + 1).isMissing() || !this.getPlot().ignoreMissingPoints_);

        this.isLineDrawable_ = this.hasPrev_ || this.hasNext_;

        this.isPointInitialized_ = true;
    }

    this.transformArgument();
    this.series_.getValueAxis().transform(this, this.getStackValue(), this.pixValuePt_);
};
/**
 * @protected
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.transformArgument = function() {
    this.series_.getArgumentAxis().transform(this, this.x_, this.pixValuePt_);
};
/**
 * @protected
 */
anychart.plots.axesPlot.series.line.LinePoint.prototype.createDrawingPoints = function() {
    this.drawingPoints_ = [];

    var pointPosition, res, roundPoint;
    if (this.hasPrev_) {
        roundPoint = this.series_.getPointAt(this.index_ - 1);
        pointPosition = roundPoint.getPixValuePoint();
        res = new anychart.utils.geom.Point();
        res.x = (pointPosition.x + this.pixValuePt_.x) / 2;
        res.y = (pointPosition.y + this.pixValuePt_.y) / 2;
        this.drawingPoints_.push(res);
    }

    this.drawingPoints_.push(this.pixValuePt_);

    if (this.hasNext_) {
        roundPoint = this.series_.getPointAt(this.index_ + 1);
        roundPoint.initializePixValue_();
        roundPoint.setPixValueCalculated();
        pointPosition = roundPoint.getPixValuePoint();
        res = new anychart.utils.geom.Point();
        res.x = (pointPosition.x + this.pixValuePt_.x) / 2;
        res.y = (pointPosition.y + this.pixValuePt_.y) / 2;
        this.drawingPoints_.push(res);
    }
};
//------------------------------------------------------------------------------
//
//                          LineSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.SingleValueSeries}
 */
anychart.plots.axesPlot.series.line.LineSeries = function() {
    anychart.plots.axesPlot.data.SingleValueSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.LineSeries,
        anychart.plots.axesPlot.data.SingleValueSeries);
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.line.LinePoint();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return false;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineSeries.prototype.needCallOnBeforeDraw = function() {
  return true;
};
//------------------------------------------------------------------------------
//
//                          LineGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings,
        anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.shiftStepLine_ = false;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.shiftStepLine = function() {
    return this.shiftStepLine_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.deserialize = function(config) {
    goog.base(this, 'deserialize', config);
    if (anychart.utils.deserialization.hasProp(config, 'shift_step_line'))
        this.shiftStepLine_ = anychart.utils.deserialization.getBoolProp(config, 'shift_step_line');
};
//------------------------------------------------------------------------------
//                          IGlobalSettings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series;
    switch (seriesType) {
        case anychart.series.SeriesType.STEP_LINE_BACKWARD:
            series = new anychart.plots.axesPlot.series.line.StepBackwardLineSeries();
            break;
        case anychart.series.SeriesType.STEP_LINE_FORWARD:
            series = new anychart.plots.axesPlot.series.line.StepForwardLineSeries();
            break;
        default :
        case anychart.series.SeriesType.LINE:
            series = new anychart.plots.axesPlot.series.line.LineSeries();
            break;
    }
    series.setType(seriesType);
    series.setClusterKey(anychart.series.SeriesType.LINE);
    return series;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.series.line.styles.LineStyle();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return "line_series";
};

anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return "line_style";
};
//------------------------------------------------------------------------------
//
//                          StepBackwardLineSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineSeries}
 */
anychart.plots.axesPlot.series.line.StepBackwardLineSeries = function() {
    anychart.plots.axesPlot.series.line.LineSeries.call(this);
};

goog.inherits(anychart.plots.axesPlot.series.line.StepBackwardLineSeries,
        anychart.plots.axesPlot.series.line.LineSeries);
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.StepBackwardLineSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.line.StepBackwardLinePoint();
};
//------------------------------------------------------------------------------
//
//                          StepBackwardLinePoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineSeries}
 */
anychart.plots.axesPlot.series.line.StepBackwardLinePoint = function() {
    anychart.plots.axesPlot.series.line.LinePoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.StepBackwardLinePoint,
        anychart.plots.axesPlot.series.line.LinePoint);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.StepBackwardLinePoint.prototype.createDrawingPoints = function() {
    this.drawingPoints_ = [];

    var tmp;
    var tmp1;
    if (this.hasPrev_) {
        tmp = this.series_.getPointAt(this.index_ - 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(this.pixValuePt_.x, (tmp.y + this.pixValuePt_.y) / 2)
                : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, this.pixValuePt_.y);
        this.drawingPoints_.push(tmp1);
    }
    this.drawingPoints_.push(this.pixValuePt_);

    if (this.hasNext_) {
        this.series_.getPointAt(this.index_ + 1).initializePixValue_();
        this.series_.getPointAt(this.index_ + 1).needCalcPixValue_ = false;
        tmp = this.series_.getPointAt(this.index_ + 1).pixValuePt_;

        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, this.pixValuePt_.y) : new anychart.utils.geom.Point(this.pixValuePt_.x, tmp.y);
        this.drawingPoints_.push(tmp1);

        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, (tmp.y + this.pixValuePt_.y) / 2)
                : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, tmp.y);
        this.drawingPoints_.push(tmp1);
    }
};

anychart.plots.axesPlot.series.line.StepBackwardLinePoint.prototype.transformArgument = function() {
    goog.base(this, 'transformArgument');

    if (this.category_ && this.getGlobalSettings().shiftStepLine()) {
        var axis = this.series_.getArgumentAxis();
        var offset = axis.getScale().transformValueToPixel(1) - axis.getScale().transformValueToPixel(.5);
        axis.transform(this, this.x_, this.pixValuePt_, -offset);
    }
};

//------------------------------------------------------------------------------
//
//                          StepForwardLineSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineSeries}
 */
anychart.plots.axesPlot.series.line.StepForwardLineSeries = function() {
    anychart.plots.axesPlot.series.line.LineSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.StepForwardLineSeries,
        anychart.plots.axesPlot.series.line.LineSeries);
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.StepForwardLineSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.line.StepForwardLinePoint();
};
//------------------------------------------------------------------------------
//
//                          StepForwardLinePoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LinePoint}
 */
anychart.plots.axesPlot.series.line.StepForwardLinePoint = function() {
    anychart.plots.axesPlot.series.line.LinePoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.line.StepForwardLinePoint,
        anychart.plots.axesPlot.series.line.LinePoint);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.line.StepForwardLinePoint.prototype.createDrawingPoints = function() {
    this.drawingPoints_ = [];

    var tmp;
    var tmp1;
    if (this.hasPrev_) {
        tmp = this.series_.getPointAt(this.index_ - 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, (tmp.y + this.pixValuePt_.y) / 2) : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, tmp.y);
        this.drawingPoints_.push(tmp1);

        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, this.pixValuePt_.y) : new anychart.utils.geom.Point(this.pixValuePt_.x, tmp.y);
        this.drawingPoints_.push(tmp1);
    }
    this.drawingPoints_.push(this.pixValuePt_);

    if (this.hasNext_) {
        this.series_.getPointAt(this.index_ + 1).initializePixValue_();
        this.series_.getPointAt(this.index_ + 1).needCalcPixValue = false;
        tmp = this.series_.getPointAt(this.index_ + 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(this.pixValuePt_.x, (tmp.y + this.pixValuePt_.y) / 2)
                : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, this.pixValuePt_.y);
        this.drawingPoints_.push(tmp1);
    }
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.line.StepForwardLinePoint.prototype.transformArgument = function() {
    goog.base(this, 'transformArgument');

    if (this.category_ && this.getGlobalSettings().shiftStepLine()) {
        var axis = this.series_.getArgumentAxis();
        var offset = axis.getScale().transformValueToPixel(1) - axis.getScale().transformValueToPixel(.5);
        axis.transform(this, this.x_, this.pixValuePt_, -offset);
    }

};
