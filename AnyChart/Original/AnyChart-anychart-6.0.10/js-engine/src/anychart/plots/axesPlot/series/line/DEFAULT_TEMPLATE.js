goog.provide('anychart.plots.axesPlot.series.line.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.line.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"line_style": {
				"line": {
					"thickness": "2",
					"color": "%Color"
				},
				"states": {
					"hover": {
						"line": {
							"thickness": "3",
							"color": "LightColor(%Color)"
						},
						"effects": {
							"glow": {
								"enabled": "true"
							}
						}
					},
					"missing": {
						"line": {
							"thickness": "3",
							"color": "%Color",
							"opacity": "0.2"
						}
					}
				},
				"name": "anychart_default"
			},
			"area_style": [
				{
					"line": {
						"enabled": "True",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"color": "%Color",
						"opacity": "1"
					},
					"states": {
						"hover": {
							"fill": {
								"type": "solid",
								"color": "LightColor(%Color)",
								"opacity": "1"
							}
						},
						"pushed": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.7)",
								"opacity": "1"
							}
						},
						"selected_normal": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.9)",
								"opacity": "1"
							},
							"hatch_fill": {
								"type": "ForwardDiagonal",
								"enabled": "True",
								"thickness": "3",
								"opacity": "0.3",
								"pattern_size": "6"
							}
						},
						"selected_hover": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.95)",
								"opacity": "1"
							},
							"hatch_fill": {
								"type": "ForwardDiagonal",
								"enabled": "True",
								"thickness": "3",
								"opacity": "0.2",
								"pattern_size": "6"
							}
						}
					},
					"name": "anychart_default"
				},
				{
					"line": {
						"enabled": "True",
						"type": "Solid",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "%Color",
									"opacity": "1"
								},
								{
									"position": "1",
									"color": "Blend(DarkColor(%Color),%Color,0.7)",
									"opacity": "1"
								}
							],
							"angle": "90"
						},
						"type": "Gradient",
						"color": "%Color",
						"opacity": "0.9"
					},
					"effects": {
						"bevel": {
							"enabled": "true",
							"distance": "1"
						}
					},
					"states": {
						"hover": {
							"line": {
								"enabled": "True",
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),%White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							}
						},
						"pushed": {
							"line": {
								"enabled": "True",
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							},
							"effects": {
								"bevel": {
									"enabled": "true",
									"distance": "2",
									"angle": "225"
								}
							}
						},
						"selected_normal": {
							"line": {
								"enabled": "True",
								"thickness": "2",
								"color": "DarkColor(%Color)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"opacity": "0.3"
							}
						},
						"selected_hover": {
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							},
							"line": {
								"enabled": "True",
								"thickness": "2",
								"color": "DarkColor(White)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"color": "DarkColor(White)",
								"opacity": "0.3"
							}
						},
						"missing": {
							"line": {
								"enabled": "True",
								"type": "Solid",
								"color": "DarkColor(White)",
								"thickness": "1",
								"opacity": "0.4"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "Dark"
				},
				{
					"line": {
						"enabled": "True",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"color": "%Color",
						"opacity": "0.5"
					},
					"states": {
						"hover": {
							"fill": {
								"type": "solid",
								"color": "LightColor(%Color)",
								"opacity": "0.5"
							}
						},
						"pushed": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.7)",
								"opacity": "0.5"
							}
						},
						"selected_normal": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.9)",
								"opacity": "0.5"
							},
							"hatch_fill": {
								"type": "ForwardDiagonal",
								"enabled": "True",
								"thickness": "3",
								"opacity": "0.3",
								"pattern_size": "6"
							}
						},
						"selected_hover": {
							"fill": {
								"type": "solid",
								"color": "Blend(LightColor(%Color),Black,0.95)",
								"opacity": "0.5"
							},
							"hatch_fill": {
								"type": "ForwardDiagonal",
								"enabled": "True",
								"thickness": "3",
								"opacity": "0.2",
								"pattern_size": "6"
							}
						}
					},
					"name": "Transparent"
				}
			],
			"range_area_style": {
				"start_line": {
					"color": "DarkColor(%Color)",
					"thickness": "2"
				},
				"end_line": {
					"color": "DarkColor(%Color)",
					"thickness": "2"
				},
				"fill": {
					"type": "Solid",
					"color": "%Color"
				},
				"states": {
					"hover": {
						"start_line": {
							"color": "%Color",
							"thickness": "2"
						},
						"end_line": {
							"color": "%Color",
							"thickness": "2"
						},
						"fill": {
							"type": "Solid",
							"color": "LightColor(%Color)"
						}
					}
				},
				"name": "anychart_default"
			}
		},
		"data_plot_settings": {
			"line_series": {
				"animation": {
					"style": "defaultScaleYCenter"
				},
				"marker_settings": {
					"animation": {
						"style": "defaultMarker"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "LightColor(%Color)",
									"opacity": "1"
								},
								{
									"position": "0.7",
									"color": "%Color",
									"opacity": "1"
								},
								{
									"position": "1",
									"color": "LightColor(%Color)",
									"opacity": "1"
								}
							],
							"angle": "45",
							"type": "radial",
							"focal_point": "-0.7"
						},
						"type": "gradient",
						"opacity": "1"
					},
					"states": {
						"missing": {
							"marker": {
								"type": "None"
							},
							"fill": {
								"opacity": "0"
							},
							"border": {
								"thickness": "1",
								"color": "%Color",
								"opacity": "0.3"
							}
						}
					},
					"enabled": "true"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "true",
						"distance": "1.5",
						"opacity": "0.25"
					}
				},
				"tooltip_settings": {
					"enabled": "False"
				},
				"label_settings": {
					"animation": {
						"style": "defaultLabel"
					},
					"enabled": "False"
				}
			},
			"area_series": {
				"animation": {
					"style": "defaultScaleYBottom"
				},
				"tooltip_settings": {
					"enabled": "False"
				},
				"label_settings": {
					"animation": {
						"style": "defaultLabel"
					},
					"enabled": "False"
				},
				"marker_settings": {
					"animation": {
						"style": "defaultMarker"
					},
					"enabled": "False"
				}
			},
			"range_area_series": {
				"animation": {
					"style": "defaultScaleYCenter"
				},
				"start_point": {
					"tooltip_settings": {
						"format": {
							"value": "{%YRangeStart}"
						},
						"position": {
							"anchor": "centerBottom",
							"valign": "bottom"
						},
						"enabled": "False"
					},
					"marker_settings": {
						"animation": {
							"style": "defaultMarker"
						},
						"marker": {
							"anchor": "centerBottom"
						},
						"enabled": "False"
					},
					"label_settings": {
						"animation": {
							"style": "defaultLabel"
						},
						"format": {
							"value": "{%YRangeStart}"
						},
						"position": {
							"anchor": "centerBottom",
							"valign": "bottom"
						},
						"enabled": "False"
					}
				},
				"end_point": {
					"tooltip_settings": {
						"format": {
							"value": "{%YRangeEnd}"
						},
						"position": {
							"anchor": "centerTop",
							"valign": "top"
						},
						"enabled": "False"
					},
					"marker_settings": {
						"animation": {
							"style": "defaultMarker"
						},
						"marker": {
							"anchor": "centerTop"
						},
						"enabled": "False"
					},
					"label_settings": {
						"animation": {
							"style": "defaultLabel"
						},
						"format": {
							"value": "{%YRangeEnd}"
						},
						"position": {
							"anchor": "centerTop",
							"valign": "top"
						},
						"enabled": "False"
					}
				}
			}
		}
	}
};