goog.provide('anychart.plots.axesPlot.axes.viewPorts.tickmarks');

//-----------------------------------------------------------------------------------
//
//                          TickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort = function (axisViewPort) {
    this.axisViewPort_ = axisViewPort;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.axisViewPort_ = null;

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.svg_createInsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.svg_createOutsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.svg_createOppositeAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.setInsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.setOutsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.prototype.setOpposideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort);

/**
 * @protected
 * @param thickness
 * @param axisValue
 * @param size
 * @param bounds
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort.prototype.setTickmarkBounds_ = function (thickness, axisValue, size, bounds) {
    bounds.x = this.axisViewPort_.getBounds().x + axisValue - thickness / 2;
    bounds.width = thickness;
    bounds.height = size;
};


//-----------------------------------------------------------------------------------
//
//                         BottomAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort);


anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.setInsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().getBottom() - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.setOutsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().getBottom();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.setOppositeAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().y;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.svg_createInsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset - size,
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.svg_createOutsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset,
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset + size,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.BottomAxisTickmarksViewPort.prototype.svg_createOppositeAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue, this.axisViewPort_.getBounds().y - offset,
        pixValue, this.axisViewPort_.getBounds().y - offset + size,
        thickness
    ));
    return el;
};

//-----------------------------------------------------------------------------------
//
//                         TopAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.HorizontalAxisTickmarksViewPort);


anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.setInsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().y;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.setOutsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().y - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.setOppositeAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.y = this.axisViewPort_.getBounds().getBottom() - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.svg_createInsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue,
        this.axisViewPort_.getBounds().y,
        pixValue,
        this.axisViewPort_.getBounds().y + size,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.svg_createOutsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue,
        this.axisViewPort_.getBounds().y - size - offset,
        pixValue,
        this.axisViewPort_.getBounds().y - offset,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.TopAxisTickmarksViewPort.prototype.svg_createOppositeAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    var el = svgManager.createPath();
    pixValue += this.axisViewPort_.getBounds().x;
    el.setAttribute('d', svgManager.pRLine(
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset - size,
        pixValue,
        this.axisViewPort_.getBounds().getBottom() + offset,
        thickness
    ));
    return el;
};

//-----------------------------------------------------------------------------------
//
//                          VerticalAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.TickmarksViewPort);

/**
 * @protected
 * @param thickness
 * @param axisValue
 * @param size
 * @param bounds
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort.prototype.setTickmarkBounds_ = function (thickness, axisValue, size, bounds) {
    axisValue = this.axisViewPort_.getBounds().getBottom() - axisValue;

    bounds.y = axisValue - thickness / 2;
    bounds.height = thickness;
    bounds.width = size;
};

//-----------------------------------------------------------------------------------
//
//                         LeftAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort);

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.setInsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().x;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.setOutsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().x - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.setOppositeAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().getRight() - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.svg_createInsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().x, pixValue,
        this.axisViewPort_.getBounds().x + size, pixValue,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.svg_createOutsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().x - size - offset,
        pixValue,
        this.axisViewPort_.getBounds().x - offset,
        pixValue,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.LeftAxisTickmarksViewPort.prototype.svg_createOppositeAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().getRight() - size,
        pixValue,
        this.axisViewPort_.getBounds().getRight(),
        pixValue,
        thickness
    ));
    return el;
};

//-----------------------------------------------------------------------------------
//
//                         RightAxisTickmarksViewPort class
//
//-----------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort = function (axisViewPort) {
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort.call(this, axisViewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort,
    anychart.plots.axesPlot.axes.viewPorts.tickmarks.VerticalAxisTickmarksViewPort);

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.setInsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().getRight() - size;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.setOutsideAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().getRight();
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.setOppositeAxisTickmarkBounds = function (thickness, pixValue, size, bounds) {
    this.setTickmarkBounds_(thickness, pixValue, size, bounds);
    bounds.x = this.axisViewPort_.getBounds().x;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.svg_createInsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().getRight() - size,
        pixValue,
        this.axisViewPort_.getBounds().getRight(),
        pixValue,
        thickness
    ));
    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.svg_createOutsideAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().getRight() + offset,
        pixValue,
        this.axisViewPort_.getBounds().getRight() + size + offset,
        pixValue,
        thickness
    ));

    return el;
};

anychart.plots.axesPlot.axes.viewPorts.tickmarks.RightAxisTickmarksViewPort.prototype.svg_createOppositeAxisTickmark = function (svgManager, pixValue, offset, size, thickness) {
    pixValue = this.axisViewPort_.getBounds().getBottom() - pixValue;
    var el = svgManager.createPath();
    el.setAttribute('d', svgManager.pRLine(
        this.axisViewPort_.getBounds().x, pixValue,
        this.axisViewPort_.getBounds().x + size, pixValue,
        thickness
    ));
    return el;
};