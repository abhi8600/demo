/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains interfaces:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.data.ITreeMapItem}</li>
 * <ul>
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.data.TreeMapDesProp}</li>
 *  <li>@class {anychart.plots.treeMapPlot.data.TreeMapPoint}</li>
 *  <li>@class {anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint}</li>
 *  <li>@class {anychart.plots.treeMapPlot.data.TreeMapSeries}</li>
 *  <li>@class {anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.data');
//------------------------------------------------------------------------------
//
//                          ITreeMapItem interface.
//
//------------------------------------------------------------------------------
/**
 * @interface
 */
anychart.plots.treeMapPlot.data.ITreeMapItem = function () {
};
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.treeMapPlot.data.TreeMapDesProp} desProp
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.deserializeData = function (desProp) {
};
//------------------------------------------------------------------------------
//                           Tree.
//------------------------------------------------------------------------------
/**
 * @return {anychart.plots.treeMapPlot.data.TreeMapSeries}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getParent = function () {
};
/**
 * @param {anychart.plots.treeMapPlot.data.TreeMapSeries} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setParent = function (value) {
};
//------------------------------------------------------------------------------
//                          Value.
//------------------------------------------------------------------------------
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getValue = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setValue = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getValueScale = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setValueScale = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getWidthScale = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setWidthScale = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getHeightScale = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setHeightScale = function (value) {
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getWidth = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setWidth = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getHeight = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setHeight = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getX = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setX = function (value) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getY = function () {
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setY = function (value) {
};
//------------------------------------------------------------------------------
//                          Visibility.
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.getVisible = function () {
};
/**
 * @param {Boolean} value
 */
anychart.plots.treeMapPlot.data.ITreeMapItem.prototype.setVisible = function (value) {
};
//------------------------------------------------------------------------------
//
//                        TreeMapDesProp class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp = function (opt_index, opt_data, opt_stylesList, opt_palettes, opt_threshold) {
    if (opt_index != null && opt_index != undefined) this.index = opt_index;
    if (opt_data) this.data = opt_data;
    if (opt_stylesList) this.stylesList = opt_stylesList;
    if (opt_palettes) this.palettes = opt_palettes;
    if (opt_threshold) this.threshold = opt_threshold;
};
/**
 * @type {Object}
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.data = null;
/**
 * @type {anychart.styles.StylesList}
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.stylesList = null;
/**
 * @type {anychart.palettes.PalettesCollection}
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.palettes = null;
/**
 * @type {int}
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.index = null;
/**
 * @type {anychart.thresholds.Threshold}
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.threshold = null;
/**
 * Create copy of TreeMapDesProp
 * @param {*} opt_target
 */
anychart.plots.treeMapPlot.data.TreeMapDesProp.prototype.copy = function (opt_target) {
    if (opt_target == null || opt_target == undefined)
        opt_target = new anychart.plots.treeMapPlot.data.TreeMapDesProp();

    opt_target.index = this.index;
    opt_target.data = this.data;
    opt_target.stylesList = this.stylesList;
    opt_target.palettes = this.palettes;
    opt_target.threshold = this.threshold;

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                      BaseTreeMapPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BasePoint}
 * @implements {anychart.plots.treeMapPlot.data.ITreeMapItem}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint = function () {
    anychart.plots.seriesPlot.data.BasePoint.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.data.BaseTreeMapPoint,
    anychart.plots.seriesPlot.data.BasePoint);
//------------------------------------------------------------------------------
//               ITreeMapItem tree implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.TreeMapSeries}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.parent_ = null;
/**
 * @return {anychart.plots.treeMapPlot.data.TreeMapSeries}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getParent = function () {
    return this.parent_;
};
/**
 * @param {anychart.plots.treeMapPlot.data.TreeMapSeries} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setParent = function (value) {
    this.parent_ = value;
};
//------------------------------------------------------------------------------
//                ITreeMapItem value implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.value_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getValue = function () {
    return this.value_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setValue = function (value) {
    this.value_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.valueScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getValueScale = function () {
    return this.valueScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setValueScale = function (value) {
    this.valueScale_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.widthScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getWidthScale = function () {
    return this.widthScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setWidthScale = function (value) {
    this.widthScale_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.heightScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getHeightScale = function () {
    return this.heightScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setHeightScale = function (value) {
    this.heightScale_ = value;
};
//------------------------------------------------------------------------------
//                ITreeMapItem bounds implementation.
//------------------------------------------------------------------------------
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getWidth = function () {
    return this.bounds_.width;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setWidth = function (value) {
    this.bounds_.width = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getHeight = function () {
    return this.bounds_.height;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setHeight = function (value) {
    this.bounds_.height = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getX = function () {
    return this.bounds_.x;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setX = function (value) {
    this.bounds_.x = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getY = function () {
    return this.bounds_.y;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setY = function (value) {
    this.bounds_.y = value;
};
//------------------------------------------------------------------------------
//                ITreeMapItem visibility implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.visible_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.isVisible = function () {
    return this.visible_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.setVisibility = function (value) {
    this.visible_ = value;
    if (this.sprite_) this.sprite_.setVisibility(value);
    if (this.labelSprite_) this.labelSprite_.setVisible(value);
    if (this.markerSprite_) this.markerSprite_.setVisible(value);
    if (this.tooltipSprite_) this.tooltipSprite_.setVisible(value);
};
//------------------------------------------------------------------------------
//             ITreeMapItem deserialization implementation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.deserializeData = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.calculatePointTokenValues = function () {
    goog.base(this, 'calculatePointTokenValues');
    this.tokensHash_.add('%Value', this.value_);
    this.tokensHash_.add('%YValue', this.value_);
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.BaseTreeMapPoint.prototype.getTokenType = function (token) {
    if (token == '%Value' || token == '%YValue')
        return anychart.formatting.TextFormatTokenType.NUMBER;

    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//
//                          TreeMapPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.treeMapPlot.data.BaseTreeMapPoint}
 * @implements {anychart.plots.treeMapPlot.data.ITreeMapItem}
 */
anychart.plots.treeMapPlot.data.TreeMapPoint = function () {
    anychart.plots.treeMapPlot.data.BaseTreeMapPoint.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.data.TreeMapPoint,
    anychart.plots.treeMapPlot.data.BaseTreeMapPoint);
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapPoint.prototype.deserialize = function (data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'y'))
        this.value_ = des.getNumProp(data, 'y');
    else if (des.hasProp(data, 'value'))
        this.value_ = des.getNumProp(data, 'value');
    else this.value_ = 0;


    if (this.value_ < 0 || isNaN(this.value_)) this.value_ = 0;
};
/**
 *
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapPoint.prototype.deserializeData = function (desProp) {
    this.series_ = this.parent_;

    this.index_ = desProp.index;
    this.name_ = this.index_.toString();
    this.deserializeName(desProp.data);

    this.setGlobalSettings(this.parent_.getGlobalSettings());

    this.setActionsList(this.parent_.getActionsList());
    this.deserializeActions(desProp.data);

    this.setInteractivitySettings(this.parent_);
    this.deserializeInteractivity(desProp.data);

    this.setStyle(this.parent_.getStyle());
    this.deserializeStyle(desProp.data, desProp.stylesList);

    this.deserialize(desProp.data);

    this.parent_.setElements(this);
    this.deserializeElements(desProp.data, desProp.stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(desProp.data, 'color'))
        this.color_ = des.getColorProp(desProp.data, 'color');
    else this.color_ = this.parent_.getColor();


    if (des.hasProp(desProp.data, 'hatch_type'))
        this.hatchType_ = des.getNumProp(desProp.data, 'hatch_type');
    else this.hatchType_ = this.parent_.getHatchType();


    this.markerType_ = this.parent_.getMarkerType();

    this.threshold_ = this.parent_.getThreshold();
    if (des.hasProp(desProp.data, 'threshold'))
        this.threshold_ = this.getPlot().getThresholdsList().getThreshold(
            des.getLStringProp(desProp.data, 'threshold'),
            desProp.palettes);

    this.parent_.checkPoint(this);

    if (this.threshold_)
        this.threshold_.checkAfterDeserialize(this);

    if (this.value_ >= 0)
        this.getPlot().getDrawingPoints().push(this);

};
//------------------------------------------------------------------------------
//                        Drawing.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {SVGPathElement}
 */
anychart.plots.treeMapPlot.data.TreeMapPoint.prototype.createElement = function (svgManager) {
    return svgManager.createPath();
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.treeMapPlot.data.TreeMapPoint.prototype.getPathData = function (svgManager) {
    return svgManager.pRectByBounds(this.bounds_);
};
//------------------------------------------------------------------------------
//
//                    TreeMapSeriesSelfPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.treeMapPlot.data.BaseTreeMapPoint}
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint = function () {
    anychart.plots.treeMapPlot.data.BaseTreeMapPoint.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint,
    anychart.plots.treeMapPlot.data.BaseTreeMapPoint);
//------------------------------------------------------------------------------
//                        Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.headerSprite_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.backgroundSprite_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.headerBounds_ = null;
//------------------------------------------------------------------------------
//               ITreeMapItem tree implementation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.deserializeData = function (desProp) {
    this.series_ = this.parent_;
    this.index_ = this.parent_.index;
    this.name_ = this.parent_.getName();
    this.color_ = this.parent_.getColor();
    this.hatchType_ = this.parent_.getHatchType();
    this.markerType_ = this.parent_.getMarkerType();
    this.threshold_ = this.parent_.getThreshold();

    this.setStyle(this.parent_.getStyle());
    this.setGlobalSettings(this.parent_.getGlobalSettings());
    this.setInteractivitySettings(this.parent_);
    this.setActionsList(this.parent_.getActionsList());

    this.parent_.checkPoint(this);
};
//------------------------------------------------------------------------------
//                         Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    this.initBackground_();
    var header = this.initHeader_();
    this.headerSprite_ = header.createSVGText(
        svgManager,
        this.color_,
        this.hatchType_);

    this.sprite_.appendSprite(this.headerSprite_);

    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.update = function () {
    goog.base(this, 'update');
    this.updateHeader();
    this.updateBackground();
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.resize = function () {
    goog.base(this, 'resize');
    this.updateHeader();
    this.updateBackground();
};
//------------------------------------------------------------------------------
//                      Header.
//------------------------------------------------------------------------------
/**
 * Init header
 * @private
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.initHeader_ = function () {
    if (this.style_.needCreateHeader()) {
        var header = this.style_.getEnabledHeader();
        header.initSize(this.getSVGManager(), header.getTextFormatter().getValue(this));
        this.headerBounds_ = header.getBounds();
        return header;
    }
};
/**
 * Update header
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.updateHeader = function () {
    if (this.headerSprite_) {
        if (this.currentState_.needUpdateHeader()) {
            var header = this.currentState_.getHeader();
            header.initSize(this.getSVGManager(), header.getTextFormatter().getValue(this));
            header.update(this.headerSprite_, this.bounds_);
            var background = header.getBackground();
            if (background && background.isEnabled()) {
                var bgBounds = new anychart.utils.geom.Rectangle(
                    0,
                    0,
                    this.bounds_.width,
                    Math.min(this.getHeaderHeight(), this.bounds_.height));
                background.draw(this.headerSprite_.getBackgroundSprite(), bgBounds)
            }

        } else this.headerSprite_.setVisibility(false);
    }
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.getHeaderHeight = function () {
    if (!this.currentState_.needUpdateHeader()) return 0;
    var header = this.currentState_.getHeader();
    if (header.getHeight()) return header.getHeight();
    this.initHeader_();
    return this.headerBounds_ ? this.headerBounds_.height : 0;
};
//------------------------------------------------------------------------------
//                          Background.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.initBackground_ = function () {
    if (this.style_.needCreateBackground()) {
        var background = this.style_.getEnabledBackground();
        this.backgroundSprite_ = background.initialize(this.getSVGManager());
        this.sprite_.appendSprite(this.backgroundSprite_);
    }
};
/**
 * Update background
 */
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.updateBackground = function () {
    if (this.backgroundSprite_ && this.currentState_.hasBackground()) {
        this.currentState_.getBackground().draw(this.backgroundSprite_, this.bounds_, this.color_);
    }
};
//------------------------------------------------------------------------------
//                           Mouse events.
//------------------------------------------------------------------------------
anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint.prototype.onMouseClick_ = function (event) {
    goog.base(this, 'onMouseClick_', event);
    if (this.getGlobalSettings().isEnabledDrillDown())
        this.getPlot().drillDown(this.parent_);
};
//------------------------------------------------------------------------------
//
//                          TreeMapSeries.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSeries}
 * @implements {anychart.plots.treeMapPlot.data.ITreeMapItem}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries = function () {
    anychart.plots.seriesPlot.data.BaseSeries.call(this);
    this.bounds_ = new anychart.utils.geom.Rectangle();
    this.drawSelfHeader_ = true;
};
goog.inherits(anychart.plots.treeMapPlot.data.TreeMapSeries,
    anychart.plots.seriesPlot.data.BaseSeries);
//------------------------------------------------------------------------------
//               ITreeMapItem tree implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.TreeMapSeries}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.parent_ = null;
/**
 * @return {anychart.plots.treeMapPlot.data.TreeMapSeries}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getParent = function () {
    return this.parent_;
};
/**
 * @param {anychart.plots.treeMapPlot.data.TreeMapSeries} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setParent = function (value) {
    this.parent_ = value;
};
//------------------------------------------------------------------------------
//                ITreeMapItem value implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.value_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getValue = function () {
    return this.value_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setValue = function (value) {
    this.value_ = value;
};
/**
 * Increment series value
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.incValue = function (value) {
    this.value_ += value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.valueScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getValueScale = function () {
    return this.valueScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setValueScale = function (value) {
    this.valueScale_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.widthScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getWidthScale = function () {
    return this.widthScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setWidthScale = function (value) {
    this.widthScale_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.heightScale_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getHeightScale = function () {
    return this.heightScale_;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setHeightScale = function (value) {
    this.heightScale_ = value;
};
//------------------------------------------------------------------------------
//                  Tree map series bounds properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.bounds_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.tmp_dWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.tmp_dHeight_ = null;
//------------------------------------------------------------------------------
//                ITreeMapItem bounds implementation.
//------------------------------------------------------------------------------
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getWidth = function () {
    return this.bounds_.width;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setWidth = function (value) {
    this.bounds_.width = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getHeight = function () {
    return this.bounds_.height;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setHeight = function (value) {
    this.bounds_.height = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getX = function () {
    return this.bounds_.x;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setX = function (value) {
    this.bounds_.x = value;
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getY = function () {
    return this.bounds_.y;
};
/**
 * @param {Number} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setY = function (value) {
    this.bounds_.y = value;
};
//------------------------------------------------------------------------------
//                ITreeMapItem visibility implementation.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.visible_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.isVisible = function () {
    return this.visible_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setVisibility = function (value) {
    var pointsCount = this.getNumPoints();

    if (this.selfPoint_) this.selfPoint_.setVisibility(value);
    for (var i = 0; i < pointsCount; i++)
        this.getPointAt(i).setVisibility(value);
};
//------------------------------------------------------------------------------
//                          Series properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.selfPoint_ = null;
/**
 * Set value to series self point.
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.sendValueToPoint = function () {
    if (this.selfPoint_) this.selfPoint_.setValue(this.value_);
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.drawSelfHeader_ = null;
/**
 * @param {Boolean} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setDrawSelfHeader = function (value) {
    this.drawSelfHeader_ = value;
};
/**
 * @param {Array.<anychart.plots.treeMapPlot.data.ITreeMapItem>} value
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.setPoints = function (value) {
    this.points_ = value;
};
//------------------------------------------------------------------------------
//                          Series margins.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.insideMargin_ = null;
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.outsideMargin_ = null;
//------------------------------------------------------------------------------
//             ITreeMapItem deserialization implementation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.deserializeData = function (desProp) {
    this.index_ = desProp.index;
    this.name_ = this.index_.toString();
    var data = desProp.data;
    this.deserializeName(data);

    var des = anychart.utils.deserialization;
    //color
    if (des.hasProp(data, 'color'))
        this.color_ = des.getColorProp(data, 'color');
    else this.color_ = des.getColor('0xCCCCCC');

    //hatch type
    if (des.hasProp(data, 'hatch_type'))
        this.hatchType_ = anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(
            des.getEnumProp(data, 'hatch_type'));
    else this.hatchType_ = anychart.visual.hatchFill.HatchFill.HatchFillType.BACKWARD_DIAGONAL;

    //markers
    this.markerType_ = anychart.elements.marker.MarkerType.CIRCLE;

    //parent settings
    if (this.parent_) {
        this.globalSettings_ = this.parent_.getGlobalSettings();

        this.actionsList_ = this.parent_.getActionsList();
        this.deserializeActions(data);

        this.setInteractivitySettings(this.globalSettings_);
        this.deserializeInteractivity(data);

        this.style_ = this.parent_.getStyle();
        this.deserializeStyle(data, desProp.stylesList);
    }

    //thresholds
    if (des.hasProp(data, 'threshold'))
        this.threshold_ = this.getPlot().getThresholdsList().getThreshold(
            des.getLStringProp(data, 'threshold'),
            desProp.palettes);

    if (this.threshold_) desProp.threshold = this.threshold_;
    else this.threshold_ = desProp.threshold;


    this.deserializeElements(data, desProp.stylesList);
    this.deserialize(data, desProp.stylesList);


    this.insideMargin_ = this.style_.getNormal().getInsideMargin();
    this.outsideMargin_ = this.style_.getNormal().getOutsideMargin();

    this.getPlot().pushSeries(this);

    if (this.drawSelfHeader_) {
        this.selfPoint_ = new anychart.plots.treeMapPlot.data.TreeMapSeriesSelfPoint();
        this.selfPoint_.setParent(this);
        var dataCache = des.copy(data);
        desProp.data = {'#name#':'point'};
        this.selfPoint_.deserializeData(desProp);
        desProp.data = dataCache;
        this.getPlot().getDrawingPoints().push(this.selfPoint_);
    }
};
//------------------------------------------------------------------------------
//                  On Before draw/resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.onBeforeDraw = function () {
    this.calcMap();
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.onBeforeResize = function () {
    this.calcMap();
};
//------------------------------------------------------------------------------
//                  Tree map calculation.
//------------------------------------------------------------------------------
/**
 * Алгоритм перенесен хер победи откуда, вычисляет баунды области рисования для каждой точки в map'e.
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.calcMap = function () {
    if (this.value_ == 0) return;

    var width = this.bounds_.width;
    var height = this.bounds_.height;
    var x = this.bounds_.x;
    var y = this.bounds_.y;

    if (this.drawSelfHeader_) {
        if (this.outsideMargin_) {
            width -= this.outsideMargin_.getLeft() + this.outsideMargin_.getRight();
            height -= this.outsideMargin_.getBottom() + this.outsideMargin_.getTop();
            x += this.outsideMargin_.getLeft();
            y += this.outsideMargin_.getTop()
        }

        this.selfPoint_.getBounds().height = height;
        this.selfPoint_.getBounds().width = width;
        this.selfPoint_.getBounds().x = x;
        this.selfPoint_.getBounds().y = y;

        var headerHeight = this.selfPoint_.getHeaderHeight();
        height -= headerHeight;
        y += headerHeight;

        if (this.insideMargin_) {
            width -= this.insideMargin_.getLeft() + this.insideMargin_.getLeft();
            height -= this.insideMargin_.getBottom() + this.insideMargin_.getTop();
            x += this.insideMargin_.getLeft();
            y += this.insideMargin_.getTop();
        }
    }

    var i;
    var len = this.points_.length;
    var breakCalc = false;

    if (width <= 1) {
        breakCalc = true;
        for (i = 0; i < len; i++)
            this.points_[i].getBounds().width = 0;
    }

    if (height <= 1) {
        breakCalc = true;
        for (i = 0; i < len; i++)
            this.points_[i].getBounds().height = 0;
    }
    if (breakCalc) return;


    this.tmp_dWidth_ = width;
    this.tmp_dHeight_ = height;

    var scale = 0;
    var pt;

    scale = (width * height) / this.value_;  // определение масштаба

    for (i = 0; i < len; i++)
        this.points_[i].setValueScale(this.points_[i].getValue() * scale);

    var start = 0;
    var end = 0;
    var vert = this.goingToDrawVertically_(this.tmp_dWidth_, this.tmp_dHeight_);
    var aspectCurr = Number.MAX_VALUE;
    var aspectLast;

    var offsetX = 0;
    var offsetY = 0;

    while (end < len) {
        aspectLast = this.getAspect_(start, end, vert);

        if (aspectLast > aspectCurr) {
            var currX = 0;
            var currY = 0;

            for (i = start; i < end; i++) {
                pt = this.points_[i];
                pt.setX(x + offsetX + currX);
                pt.setY(y + offsetY + currY);

                if (vert) {
                    currY += pt.getHeight();
                } else {
                    currX += pt.getWidth();
                }
            }

            if (vert) {
                offsetX += this.points_[start].getWidth();
            } else {
                offsetY += this.points_[start].getHeight();
            }

            this.tmp_dWidth_ = width - offsetX;
            this.tmp_dHeight_ = height - offsetY;

            vert = this.goingToDrawVertically_(this.tmp_dWidth_, this.tmp_dHeight_);

            start = end;
            end = start;

            aspectCurr = Number.MAX_VALUE;
        } else {
            for (i = start; i <= end; i++) {
                pt = this.points_[i];
                pt.setWidth((pt.getWidthScale() < 1 || isNaN(pt.getWidthScale())) ? 1 : pt.getWidthScale());
                pt.setHeight((pt.getHeightScale() < 1 || isNaN(pt.getHeightScale())) ? 1 : pt.getHeightScale());
            }
            aspectCurr = aspectLast;
            end++;
        }
    }

    var currX1 = 0;
    var currY1 = 0;

    for (i = start; i < end; i++) {
        pt = this.points_[i];
        pt.setX(x + offsetX + currX1);
        pt.setY(y + offsetY + currY1);

        if (vert) {
            currY1 += pt.getHeight();
        } else {
            currX1 += pt.getWidth();
        }
    }

    for (i = 0; i < len; i++) {
        pt = this.points_[i];
        if (pt.calcMap) pt.calcMap();
    }
};
/**
 * @private
 * @param {Number} width
 * @param {Number} height
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.goingToDrawVertically_ = function (width, height) {
    return width > height;
};
/**
 * Метод, вычисляющий коэффициент aspect.
 * @param {int} start начальная граница
 * @param {int} end конечная граница
 * @param {Boolean} vert перемееная, которую возвращает метод drawVert
 * @return {Number} коэффициент aspect
 */
anychart.plots.treeMapPlot.data.TreeMapSeries.prototype.getAspect_ = function (start, end, vert) {
    var total = 0;
    var aspect = 0;
    var localWidth, localHeight, pt, i;

    for (i = start; i <= end; i++)
        total += this.points_[i].getValueScale();

    if (vert) {
        localHeight = this.tmp_dHeight_ / total;
        localWidth = total / this.tmp_dHeight_;
    } else {
        localWidth = this.tmp_dWidth_ / total;
        localHeight = total / this.tmp_dWidth_;
    }

    for (i = start; i <= end; i++) {
        pt = this.points_[i];

        if (vert) {
            pt.setHeightScale(localHeight * pt.getValueScale());
            pt.setWidthScale(localWidth);
        } else {
            pt.setWidthScale(localWidth * pt.getValueScale());
            pt.setHeightScale(localHeight);
        }
    }

    pt = this.points_[end];
    aspect = Math.max(pt.getHeightScale() / pt.getWidthScale(), pt.getWidthScale() / pt.getHeightScale());

    return aspect;
};

//------------------------------------------------------------------------------
//
//                   TreeMapGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.GlobalSeriesSettings}
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings = function () {
    anychart.plots.seriesPlot.data.GlobalSeriesSettings.call(this);
    this.applyDataPaletteToSeries_ = true;
    this.elementsDrawingMod_ = anychart.plots.treeMapPlot.elements.ElementsDrawingMod.CROP;
    this.enabledDrillDown_ = true;
    this.isSortingEnabled = true;
};
goog.inherits(anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.GlobalSeriesSettings);
//------------------------------------------------------------------------------
//                            Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.treeMapPlot.elements.ElementsDrawingMod}
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.elementsDrawingMod_ = null;
/**
 * @return {anychart.plots.treeMapPlot.elements.ElementsDrawingMod}
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.getElementsDrawingMod = function () {
    return this.elementsDrawingMod_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.enabledDrillDown_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.isEnabledDrillDown = function () {
    return this.enabledDrillDown_;
};
//------------------------------------------------------------------------------
//                  Sort.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.sortDescending = true;
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.canSortPoints = function () {
    return true;
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.execSortPoints = function (points, sortDescending) {
    sortDescending ?
        points.sort(function (a, b) {
            return b.getValue() - a.getValue();
        }) :
        points.sort(function (a, b) {
            return a.getValue() - b.getValue();
        });
};
//------------------------------------------------------------------------------
//                          Series settings.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'tree_map';
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'tree_map_style';
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.createStyle = function () {
    switch (this.elementsDrawingMod_) {
        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.DISPLAY_ALL:
            return new anychart.plots.treeMapPlot.styles.TreeMapStyle();

        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.HIDE:
            return new anychart.plots.treeMapPlot.styles.TreeMapHiddenStyle();

        default:
        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.CROP:
            return new anychart.plots.treeMapPlot.styles.TreeMapCroppedStyle();
    }
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.createLabelElement = function () {
    return new anychart.plots.treeMapPlot.elements.TreeMapLabel();
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.data.TreeMapGlobalSeriesSettings.prototype.deserialize = function (data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'enable_drilldown'))
        this.enabledDrillDown_ = des.getProp(data, 'enable_drilldown');

    if (des.hasProp(data, 'label_settings')) {
        var labelSettings = des.getProp(data, 'label_settings');
        if (des.hasProp(labelSettings, 'drawing_mode')) {
            this.elementsDrawingMod_ = anychart.plots.treeMapPlot.elements.ElementsDrawingMod.deserialize(
                des.getEnumProp(labelSettings, 'drawing_mode'));
        }
    }
};

