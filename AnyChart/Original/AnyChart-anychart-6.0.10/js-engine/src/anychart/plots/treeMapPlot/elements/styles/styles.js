/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes}</li>
 *  <li>@class {anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes}</li>
 *  <li>@class {anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.elements.styles');
//------------------------------------------------------------------------------
//
//                       TreeMapCroppedLabelStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyleShapes}
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes = function () {
    anychart.elements.label.styles.LabelElementStyleShapes.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes,
    anychart.elements.label.styles.LabelElementStyleShapes);
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes.prototype.createShapes = function (point, element) {
    goog.base(this, 'createShapes', point, element);
    if (this.textSprite_)
        this.textSprite_.initializeClipPath(point.getBounds());
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes.prototype.update = function (state) {
    goog.base(this, 'update', state);
    this.updateByDrawingMode(state); //todo:can bi optimized by passing point into element sprite update method
};
/**
 * Update label clip path.
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes.prototype.updateByDrawingMode = function (state) {
        if (this.textSprite_)
            this.textSprite_.updateClipPath(new anychart.utils.geom.Rectangle(
                this.point_.getBounds().x - this.textSprite_.getParent().getX(),
                this.point_.getBounds().y - this.textSprite_.getParent().getY(),
                this.point_.getBounds().width,
                this.point_.getBounds().height));
};
//------------------------------------------------------------------------------
//
//                       TreeMapHiddenLabelStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyleShapes}
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes = function () {
    anychart.elements.label.styles.LabelElementStyleShapes.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes,
    anychart.elements.label.styles.LabelElementStyleShapes);
//------------------------------------------------------------------------------
//                      Create shapes.
//------------------------------------------------------------------------------

anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes.prototype.update = function (state) {
    goog.base(this, 'update', state);
    this.updateByDrawingMode(state); //todo:can bi optimized by passing point into element sprite update method
};
/**
 * Update hidden label visibility.
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes.prototype.updateByDrawingMode = function(state) {
    var pointBounds = new anychart.utils.geom.Rectangle(
            this.point_.getBounds().x - this.textSprite_.getParent().getX(),
            this.point_.getBounds().y - this.textSprite_.getParent().getY(),
            this.point_.getBounds().width,
            this.point_.getBounds().height);
        var bounds = this.point_.getLabel().getBounds(this.point_, state, this.textSprite_);

        pointBounds.containsRect(bounds) ? this.textSprite_.setVisibility(true) : this.textSprite_.setVisibility(false)
};
//------------------------------------------------------------------------------
//
//                          TreeMapLabelStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyle}
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle = function () {
    anychart.elements.label.styles.LabelElementStyle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle,
    anychart.elements.label.styles.LabelElementStyle);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Function}
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle.prototype.styleShapesClass_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle.prototype.initialize = function (point, element) {
    switch (point.getGlobalSettings().getElementsDrawingMod()) {
        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.DISPLAY_ALL:
            this.styleShapesClass_ = anychart.elements.label.styles.LabelElementStyleShapes;
            break;

        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.HIDE:
            this.styleShapesClass_ = anychart.plots.treeMapPlot.elements.styles.TreeMapHiddenLabelStyleShapes;
            break;

        default:
        case anychart.plots.treeMapPlot.elements.ElementsDrawingMod.CROP:
            this.styleShapesClass_ = anychart.plots.treeMapPlot.elements.styles.TreeMapCroppedLabelStyleShapes;
            break;
    }
    return goog.base(this, 'initialize', point, element);
};
//------------------------------------------------------------------------------
//                              Style shapes.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle.prototype.createStyleShapes = function () {
    return new this.styleShapesClass_();
};


