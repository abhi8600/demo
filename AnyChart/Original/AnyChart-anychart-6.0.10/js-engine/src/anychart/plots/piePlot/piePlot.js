/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.PiePlot3DDepthManager}.</li>
 *  <li>@class {anychart.plots.piePlot.PiePlot}.</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot');

goog.require('anychart.series');
goog.require('anychart.plots.seriesPlot');
goog.require('anychart.plots.piePlot.data');
goog.require('anychart.plots.piePlot.templates');
goog.require('anychart.plots.piePlot.labels');
goog.require('anychart.utils.geom');
//------------------------------------------------------------------------------
//
//                           PiePlot3DDepthManager class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param mainSprite
 */
anychart.plots.piePlot.PiePlot3DDepthManager = function(mainSprite) {
    this.svgManager_ = mainSprite.getSVGManager();
    this.frontSides_ = [];
    this.backSides_ = [];
    this.sides_ = [];

    if (IS_ANYCHART_DEBUG_MOD) mainSprite.setId('PiePlot__MainSprite');

    this.topSidesContainer_ = new anychart.svg.SVGSprite(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.topSidesContainer_.setId('PiePlot__TopSidesContainer');

    this.sidesContainer_ = new anychart.svg.SVGSprite(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.sidesContainer_.setId('PiePlot__SidesContainer');

    this.frontSideConnectorsContainer_ = new anychart.svg.SVGSprite(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.frontSideConnectorsContainer_.setId('PiePlot__FrontSideConnectorsContainer');

    this.backSideConnectorsContainers_ = new anychart.svg.SVGSprite(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.backSideConnectorsContainers_.setId('PiePlot__BackSideConnectorsContainer');

    mainSprite.appendChild(this.backSideConnectorsContainers_.getElement());
    mainSprite.appendChild(this.sidesContainer_.getElement());
    mainSprite.appendChild(this.topSidesContainer_.getElement());
    mainSprite.appendChild(this.frontSideConnectorsContainer_.getElement());
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.topSidesContainer_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.sidesContainer_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.frontSideConnectorsContainer_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.backSideConnectorsContainers_ = null;
/**
 * @private
 * @type {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.frontSides_ = null;
/**
 * @private
 * @type {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.backSides_ = null;
/**
 * @private
 * @type {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.sides_ = null;
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.svgManager_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.sidesInitialized_ = false;
//------------------------------------------------------------------------------
//                          Side adding
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGSprite} side
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addTopSide = function(side) {
    this.topSidesContainer_.appendSprite(side);
};
/**
 * @param {anychart.svg.SVGSprite} side
 * @param {int} index
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addFrontSide = function(side, index) {
    this.frontSides_.push({'d':side, 'i':index});
};
/**
 * @param {anychart.svg.SVGSprite} side
 * @param {int} index
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addBackSide = function(side, index) {
    this.backSides_.push({'d':side, 'i':index});
};
/**
 *
 * @param {Number} angle
 * @param {anychart.svg.SVGSprite} side
 * @param {Number} index
 */
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addSide = function(angle, side, index) {
    this.sides_.push({'s': Math.sin(angle * Math.PI / 180), 'c':side, 'i':index});
};

anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addFrontSideConnector = function(connector) {
    this.frontSideConnectorsContainer_.appendChild(connector);
};

anychart.plots.piePlot.PiePlot3DDepthManager.prototype.addBackSideConnector = function(connector) {
    this.backSideConnectorsContainers_.appendChild(connector);
};
//------------------------------------------------------------------------------
//                          Initialized
//------------------------------------------------------------------------------
//todo : desc and change c,s,i to normal object or array
anychart.plots.piePlot.PiePlot3DDepthManager.prototype.initSides = function() {
    if (this.sidesInitialized_) return;
    var i, index, j, k;

    //front side
    var length = this.frontSides_.length;
    for (i = 0; i < length; i++) {
        if (!this.frontSides_[i]) continue;
        var fInfo = this.frontSides_[i]['d'];
        index = this.frontSides_[i]['i'];
        k = fInfo.getLength();
        for (j = 0; j < k; j++)
            this.sides_.push(
                    {'c': fInfo.getSprite(j),
                        's': (fInfo.isInFront() ?
                                1 :
                                Math.sin(fInfo.getCenterAngle(j) * Math.PI / 180)),
                        'i': index});
    }

    //back side
    length = this.backSides_.length;
    for (i = 0; i < length; i++) {
        if (!this.backSides_[i]) continue;
        var bInfo = this.backSides_[i]['d'];
        index = this.backSides_[i]['i'];
        k = bInfo.getLength();
        for (j = 0; j < k; j++)
            this.sides_.push(
                    {'c': bInfo.getSprite(j),
                        's': (bInfo.isInFront() ?
                                -1
                                : Math.sin(bInfo.getCenterAngle(j) * Math.PI / 180)
                                ),
                        'i': index});
    }

    //sides
    length = this.sides_.length;
    for (i = 0; i < length; i++) {
        if (this.sides_[i]['s'] >= 0)
            this.sides_[i]['s'] += this.sides_[i]['i'] * 2;
        else
            this.sides_[i]['s'] -= this.sides_[i]['i'] * 2;
    }

    this.sides_.sort(function(a, b) {
        return a['s'] - b['s'];
    });

    for (i = 0; i < length; i++)
        this.sidesContainer_.appendChild(this.sides_[i]['c'].getElement());


    this.sidesInitialized_ = true;
};
//------------------------------------------------------------------------------
//
//                           PiePlot class.
//
//------------------------------------------------------------------------------
/**
 * Class represent pie plot.
 * @constructor
 * @extends {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.piePlot.PiePlot = function() {
    anychart.plots.seriesPlot.SeriesPlot.call(this);
    this.centerPoint_ = new anychart.utils.geom.Point();
    this.isDoughnut_ = false;
    this.is3D_ = false;

    this.categoriesMap_ = {};
    this.categoriesList_ = [];
};
//inheritance
goog.inherits(anychart.plots.piePlot.PiePlot,
        anychart.plots.seriesPlot.SeriesPlot);
//------------------------------------------------------------------------------
//                          Common properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.piePlot.data.PieGlobalSeriesSettings}
 */
anychart.plots.piePlot.PiePlot.prototype.globalSeriesSettings_ = null;
/**
 * Indicates, is plot a single series plot.
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.PiePlot.prototype.isSingleSeries_ = false;
//------------------------------------------------------------------------------
//                          2D properties
//------------------------------------------------------------------------------
/**
 * Indicates, is plot doughnut.
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.PiePlot.prototype.isDoughnut_ = false;
/**
 * Is plot type is doughnut.
 * @return {Boolean} Is doughnut.
 */
anychart.plots.piePlot.PiePlot.prototype.isDoughnut = function() {
    return this.isDoughnut_;
};

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.PiePlot.prototype.connectorsSprite_ = null;
//------------------------------------------------------------------------------
//                          3D properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.PiePlot.prototype.is3D_ = false;
/**
 * @return {Boolean}
 */
anychart.plots.piePlot.PiePlot.prototype.is3D = function() {
    return this.is3D_;
};
/**
 * @const
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.HEIGHT = .2;
/**
 * @const
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.ASPECT = .45;
/**
 * @private
 * @type {anychart.plots.piePlot.PiePlot3DDepthManager}
 */
anychart.plots.piePlot.PiePlot.prototype.depthManager_ = null;
/**
 * @return {anychart.plots.piePlot.PiePlot3DDepthManager}
 */
anychart.plots.piePlot.PiePlot.prototype.getDepthManager = function() {
    return this.depthManager_;
};
//------------------------------------------------------------------------------
//                   Categorization properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.plots.piePlot.PiePlot.prototype.categoriesMap_ = null;
/**
 * @return {Object}
 */
anychart.plots.piePlot.PiePlot.prototype.getCategoriesMap = function() {
    return this.categoriesMap_;
};
/**
 * @private
 * @type {Array}
 */
anychart.plots.piePlot.PiePlot.prototype.categoriesList_ = null;
/**
 * @return {Array}
 */
anychart.plots.piePlot.PiePlot.prototype.getCategoriesList = function() {
    return this.categoriesList_;
};
/**
 * @return {anychart.plots.seriesPlot.categorization.BaseCategoryInfo}
 */
anychart.plots.piePlot.PiePlot.prototype.createCategory = function(name) {
    if (!this.categoriesMap_[name]) {
        var info = new anychart.plots.seriesPlot.categorization.BaseCategoryInfo(this);
        info.setName(name);
        info.setIndex(this.categoriesList_.length);
        this.categoriesList_.push(info);
        this.categoriesMap_[name] = info;
    }
    return this.categoriesMap_[name];
};
//------------------------------------------------------------------------------
//                      Radius properties
//------------------------------------------------------------------------------
/**
 * Outer plot radius.
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.outerRadius_ = NaN;
/**
 * Getter for plot outer radius.
 * @return {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.getOuterRadius = function() {
    return this.outerRadius_;
};
/**
 * Inner plot radius.
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.innerRadius_ = NaN;
/**
 * Pie plot center point
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.piePlot.PiePlot.prototype.centerPoint_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.piePlot.PiePlot.prototype.getCenterPoint = function() {
    return this.centerPoint_;
};
/**
 * Outer plot y radius.
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.outerYRadius_ = NaN;
/**
 * Inner plot y radius.
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.innerYRadius_ = NaN;
//------------------------------------------------------------------------------
//                          Explode properties
//------------------------------------------------------------------------------
/**
 * Current explode point.
 * @private
 * @type {anychart.plots.piePlot.data.PiePoint}
 */
anychart.plots.piePlot.PiePlot.prototype.explodedPoint_ = null;
/**
 * @return {anychart.plots.piePlot.data.PiePoint}
 */
anychart.plots.piePlot.PiePlot.prototype.getExplodedPoint = function() {
    return this.explodedPoint_;
};
/**
 *
 * @param {anychart.plots.piePlot.data.PiePoint} value
 */
anychart.plots.piePlot.PiePlot.prototype.setExplodedPoint = function(value) {
    this.explodedPoint_ = value;
};
/**
 * Return pie plot explode radius.
 * @return {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.getExplodeRadius = function() {
    return this.outerRadius_ * this.getExplodeRadiusPercent();
};
/**
 * Return percent value of explode radius.
 * @return {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.getExplodeRadiusPercent = function() {
    return this.globalSeriesSettings_.getExplodeRadius();
};
//------------------------------------------------------------------------------
//                          Labels properties
//------------------------------------------------------------------------------
/**
 * Pie plot critical angle value.
 * <p>
 *     Note: default value is illegal.
 * </p>
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.criticalAngle_ = -10;
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.PiePlot.prototype.labelRadius_ = NaN;
/**
 * @private
 * @type {Array.<anychart.plots.piePlot.labels.LabelInfo>}
 */
anychart.plots.piePlot.PiePlot.prototype.basicLabelsInfo_ = null;
/**
 * Getter for basic labels info.
 * @return {Array.<anychart.plots.piePlot.labels.LabelInfo>}
 */
anychart.plots.piePlot.PiePlot.prototype.getBasicLabelsInfo = function() {
    return this.basicLabelsInfo_;
};
/**
 * Indicates, allow overlap for labels.
 */
anychart.plots.piePlot.PiePlot.prototype.isLabelsOverlapAllowed = function() {
    return this.globalSeriesSettings_.allowLabelsOverlap();
};
//------------------------------------------------------------------------------
//                   IPlot interface implementation
//------------------------------------------------------------------------------
/**
 * @param {String} type
 */
anychart.plots.piePlot.PiePlot.prototype.setPlotType = function(type) {
    goog.base(this, 'setPlotType', type);
    if (type == 'doughnut') this.isDoughnut_ = true;
};
/**
 * Return template merger.
 * @return {anychart.plots.piePlot.templates.PiePlotTemplatesMerger}
 */
anychart.plots.piePlot.PiePlot.prototype.getTemplatesMerger = function() {
    return new anychart.plots.piePlot.templates.PiePlotTemplatesMerger();
};
/**
 * Return plot global series settings object.
 * @param {anychart.series.SeriesType} seriesType Series type.
 * @return {anychart.plots.piePlot.data.PieGlobalSeriesSettings}
 */
anychart.plots.piePlot.PiePlot.prototype.createGlobalSeriesSettings = function(seriesType) {
    return new anychart.plots.piePlot.data.PieGlobalSeriesSettings();
};
/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.getGlobalSeriesSettings_ = function (dataPlotSettings, dataNode, seriesType, stylesList) {
    if (this.globalSeriesSettings_) return this.globalSeriesSettings_;
    this.globalSeriesSettings_ = goog.base(this, 'getGlobalSeriesSettings_', dataPlotSettings, dataNode, seriesType, stylesList);
    return this.globalSeriesSettings_;
};
/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.deserializeSeriesArguments_ = function(series, seriesNode) {
};
/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.onAfterDeserializeData = function(config) {
};
anychart.plots.piePlot.PiePlot.prototype.beforeSeriesPointsDeserialize = function(series, points) {
    return this.globalSeriesSettings_.sortJSONPoints(points);
};
/**
 * @inheritDoc
 */
anychart.plots.piePlot.PiePlot.prototype.deserializeDataPlotSettings_ = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'enable_3d_mode')) {
        this.is3D_ = des.getBoolProp(data, 'enable_3d_mode');   
    }
};

//------------------------------------------------------------------------------
//                   Series
//------------------------------------------------------------------------------

/**
 * Return series type.
 * @protected
 * @return {int}
 */
anychart.plots.piePlot.PiePlot.prototype.getSeriesType = function() {
    return anychart.series.SeriesType.PIE;
};
anychart.plots.piePlot.PiePlot.prototype.defaultSeriesType_ = anychart.series.SeriesType.PIE;
/**
 * Return series radius.
 * @param {Number} seriesIndex
 * @param {Boolean} isInner
 */
anychart.plots.piePlot.PiePlot.prototype.getSeriesRadius = function(seriesIndex, isInner) {
    var dR = (this.isDoughnut_) ? (this.outerRadius_ - this.innerRadius_) : this.outerRadius_;
    var radius = dR * (isInner ? seriesIndex : (seriesIndex + 1)) / this.getNumSeries();
    return (this.isDoughnut_) ? (this.innerRadius_ + radius) : radius;
};
anychart.plots.piePlot.PiePlot.prototype.getYRadius = function(xRadius) {
    return xRadius * this.ASPECT;
};
anychart.plots.piePlot.PiePlot.prototype.get3DHeight = function() {
    return this.outerRadius_ * this.HEIGHT;
};
anychart.plots.piePlot.PiePlot.prototype.getRadiusByFactor = function(factorValue) {
    return this.outerRadius_ * factorValue;
};
anychart.plots.piePlot.PiePlot.prototype.getExplodeRadius = function() {
    return this.outerRadius_ * this.globalSeriesSettings_.getExplodeRadius();
};
anychart.plots.piePlot.PiePlot.prototype.getExplodeRadiusPercent = function() {
    return this.globalSeriesSettings_.getExplodeRadius();
};
anychart.plots.piePlot.PiePlot.prototype.getAllowOverlap = function() {
    return this.globalSeriesSettings_.allowLabelsOverlap();
};
anychart.plots.piePlot.PiePlot.prototype.getExplodeYRadius = function() {
    return this.getExplodeRadius() * this.ASPECT;
};
//------------------------------------------------------------------------------
//                      Drawing
//------------------------------------------------------------------------------
anychart.plots.piePlot.PiePlot.prototype.draw = function() {
    goog.base(this, 'draw');
    if (this.depthManager_)
        this.depthManager_.initSides();
};
//------------------------------------------------------------------------------
//                       Resize.
//------------------------------------------------------------------------------
anychart.plots.piePlot.PiePlot.prototype.calculateResize = function(bounds) {
    this.calculateBounds_(bounds);
    goog.base(this, 'calculateResize', bounds);
    if(this.globalSeriesSettings_.isOutsideLabels()) this.calculateLabelsPositions_(bounds);
};
/*
 override protected function beforeSeriesPointsDeserialization(series:BaseSeries, points:XMLList):XMLList {
 // сортировка точек
 return series.global.sortXMLPoints(points);
 }*/
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.getBackgroundBounds = function() {
    return new anychart.utils.geom.Rectangle(0, 0, this.bounds_.width, this.bounds_.height);
};

/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.initializeBottomPlotContainers_ = function() {
};

/**
 * Creates feGaussianFilter filter in defs node.
 * @param {String} stdDeviation Blur factor.
 * @private
 */
anychart.plots.piePlot.PiePlot.prototype.createBlurFilter_ = function(stdDeviation) {
    var svgManager = this.getSVGManager();
    var defs = svgManager.defs_;
    var filter = svgManager.createSVGElement('filter');
    filter.setAttribute('id','smileBlur');
    var blur = svgManager.createSVGElement('feGaussianBlur');
    blur.setAttribute('in', 'SourceGraphic');
    blur.setAttribute('stdDeviation', stdDeviation);
    filter.appendChild(blur);
    defs.appendChild(filter);
};


/**@inheritDoc*/
anychart.plots.piePlot.PiePlot.prototype.initialize = function(bounds, tooltipContainer, opt_drawBackground) {
    this.connectorsSprite_ = new anychart.svg.SVGSprite(this.getSVGManager());
    this.afterDataSprite_.appendSprite(this.connectorsSprite_);
    if (IS_ANYCHART_DEBUG_MOD) this.afterDataSprite_.setId('PiePlot__AfterDataSprite');
    var seriesCount = this.getNumSeries();

    //3D support
    if (this.is3D_) {
        this.depthManager_ = new anychart.plots.piePlot.PiePlot3DDepthManager(this.dataSprite_);
        this.createBlurFilter_('1.1');
    }

    goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);

    //point angles calculation
    var i,j;
    for (i = 0; i < seriesCount; i++) {
        var pointsCount = this.series_[i].getNumPoints();
        for (j = 0; j < pointsCount; j++)
            this.getSeriesAt(i).getPointAt(j).calculateAngles();
    }

    //multiple series settings
    this.isSingleSeries_ = seriesCount == 1;

    //bounds and position calculation
    this.calculateBounds_(bounds);

    if (!this.isSingleSeries_) {
        this.globalSeriesSettings_.setExplodeOnClick(false);
    }

    if (this.globalSeriesSettings_.isOutsideLabels()) {
        this.series_[seriesCount - 1].isOutsideLabels_ = true;
        this.calculateLabelsPositions_(bounds);
    }

    return this.sprite_;
};

/**
 * Calculate pie plot bounds.
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.piePlot.PiePlot.prototype.calculateBounds_ = function(bounds) {
    this.bounds_ = this.is3D_ ?
            this.calculateBounds3D_(bounds) :
            this.calculateBounds2D_(bounds);
};
/**
 * Calculate 2D pie plot bounds.
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.piePlot.PiePlot.prototype.calculateBounds2D_ = function(bounds) {
    var baseBounds = bounds;
    bounds = bounds.clone();
    //pie bounds
    var maxSize = Math.min(bounds.width, bounds.height);
    bounds.x = bounds.x + (bounds.width - maxSize) / 2;
    bounds.y = bounds.y + (bounds.height - maxSize) / 2;
    bounds.width = bounds.height = maxSize;
    //pie center point
    var r = maxSize / 2;
    this.centerPoint_.x = baseBounds.width / 2;
    this.centerPoint_.y = baseBounds.height / 2;

    //pie inner/outer radius calculation
    if (this.getNumSeries() <= 0) return;

    if (this.isSingleSeries_ && (this.globalSeriesSettings_.hasExplodedItems() || this.globalSeriesSettings_.isExplodeOnClick())) {
        var c = (this.globalSeriesSettings_.getRadius() + this.globalSeriesSettings_.getExplodeRadius());
        var totalR = r / c;
        this.outerRadius_ = totalR * this.globalSeriesSettings_.getRadius();
        this.innerRadius_ = totalR * this.globalSeriesSettings_.getInnerRadius();
    } else {
        this.outerRadius_ = this.globalSeriesSettings_.getRadius() * r;
        this.innerRadius_ = this.globalSeriesSettings_.getInnerRadius() * r;
    }
    return bounds;
};
/**
 * Calculate 3D pie plot bounds.
 * @private
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.piePlot.PiePlot.prototype.calculateBounds3D_ = function(bounds) {
    var baseBounds = bounds;
    bounds = bounds.clone();

    //radius calculation
    var r = Math.min(bounds.width / 2, (bounds.height) / (2 * this.ASPECT + this.HEIGHT));
    bounds.x = bounds.x + bounds.width / 2 - r;
    bounds.y = bounds.y + bounds.height / 2 - r * (this.ASPECT + this.HEIGHT / 2);
    bounds.width = r * 2;
    bounds.height = 2 * r * this.ASPECT + r * this.HEIGHT;
    this.centerPoint_.x = baseBounds.width / 2;
    this.centerPoint_.y = baseBounds.height / 2 - r * this.HEIGHT / 2;

    //multiple series
    var seriesCount = this.getNumSeries();
    if (seriesCount > 0) {

        if (seriesCount == 1 && (this.globalSeriesSettings_.hasExplodedItems() || this.globalSeriesSettings_.isExplodeOnClick())) {
            var c = (this.globalSeriesSettings_.getRadius() + this.globalSeriesSettings_.getExplodeRadius());
            var totalR = r / c;
            this.outerRadius_ = totalR * this.globalSeriesSettings_.getRadius();
            this.innerRadius_ = totalR * this.globalSeriesSettings_.getInnerRadius();
        } else {
            this.outerRadius_ = this.globalSeriesSettings_.getRadius() * r;
            this.innerRadius_ = this.globalSeriesSettings_.getInnerRadius() * r;
        }

        this.outerYRadius_ = this.outerRadius_ * this.ASPECT;
        this.innerYRadius_ = this.innerRadius_ * this.ASPECT;
    }
    return bounds;
};
//------------------------------------------------------------------------------
//                          Point explode
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.piePlot.data.PiePoint} point
 */
anychart.plots.piePlot.PiePlot.prototype.checkExplode = function(point) {
    if (this.globalSeriesSettings_.isExplodeOnClick()) {
        if (!this.globalSeriesSettings_.isMultiExplodeAllowed()) {
            if (this.explodedPoint_ && this.explodedPoint_ != point) {
                this.explodedPoint_.setExploded(false);
                this.explodedPoint_.updateExplode();
            }
            this.setExplodedPoint(point);
        }
        point.setExploded(!point.isExploded());
    }
    return false;
};
//------------------------------------------------------------------------------
//                          Labels position
//------------------------------------------------------------------------------
anychart.plots.piePlot.PiePlot.prototype.calculateLabelsPositions_ = function(bounds) {
    this.adjustRadiusByLabels_(bounds);
    if (!this.globalSeriesSettings_.isLabelsEnabled()) return;
    if (this.globalSeriesSettings_.isSmartLabelsEnabled()) {
        //New labels position logic
        this.logicMethodOfCalculateLabels();
        this.calculateLabelsOverlapLogic();
    } else {
        //Old labels position logic
        if (this.checkOverlap_())
            if (this.calculateLabelsOverlapAnglesDown())
                (this.calculateLabelsOverlapAnglesUp());
        this.calculateLabelsOverlap();
    }
};
//------------------------------------------------------------------------------
//                   Old labels position logic.
//------------------------------------------------------------------------------
/**
 * @param bounds
 */
anychart.plots.piePlot.PiePlot.prototype.adjustRadiusByLabels_ = function(bounds) {
    //labels padding
    var labelPadding = this.globalSeriesSettings_.isPercentLabelsPadding() ?
            (this.globalSeriesSettings_.getLabelsPadding() * this.outerRadius_) :
            (this.globalSeriesSettings_.getLabelsPadding());
    //connector padding
    var connectorPadding = this.globalSeriesSettings_.isConnectorEnabled() ?
            this.globalSeriesSettings_.getConnectorPadding() : 0;
    this.basicLabelsInfo_ = [];

    var ji = this.series_.length - 1;

    for(var i = 0; i<this.getSeriesAt(ji).getNumPoints(); i++) {
            var point = this.getSeriesAt(ji).getPointAt(i);
            if (!point.hasLabel()) continue;

            var angle = (point.getStartAngle() + point.getEndAngle()) * Math.PI / 360;
            point.getLabel().getStyle().getNormal().setPadding(labelPadding);
            point.getLabel().getStyle().getHover().setPadding(labelPadding);
            point.getLabel().getStyle().getPushed().setPadding(labelPadding);
            point.getLabel().getStyle().getSelectedNormal().setPadding(labelPadding);
            point.getLabel().getStyle().getSelectedHover().setPadding(labelPadding);
            point.getLabel().getStyle().getMissing().setPadding(labelPadding);

            var labelBounds = point.getLabel().getBounds(
                    point,
                    point.getLabel().getStyle().getNormal(),
                    point.getSprite());
            
            this.outerRadius_ = Math.min(
                    this.outerRadius_,
                    Math.abs((bounds.width - (labelBounds.width + connectorPadding) * 2) /
                            (2 * (1 + this.getExplodeRadiusPercent() + labelPadding / this.outerRadius_) * Math.cos(angle))));

            if (this.is3D_)
                this.outerRadius_ = Math.min(
                        this.outerRadius_,
                        Math.abs((bounds.height * this.ASPECT - labelBounds.height * 2) /
                                (2 * (1 + this.getExplodeRadiusPercent() + labelPadding / this.outerRadius_) * this.ASPECT * Math.sin(angle))
                        ) / this.ASPECT
                );
            else
                this.outerRadius_ = Math.min(this.outerRadius_,
                        Math.abs(
                                (bounds.height - labelBounds.height * 2) /
                                        (2 * (1 + this.getExplodeRadiusPercent() + labelPadding / this.outerRadius_) * Math.sin(angle))
                        )
                );
            var labelInfo = new anychart.plots.piePlot.labels.LabelInfo(
                    (point.getStartAngle() + point.getEndAngle()) / 2,
                    labelBounds,
                    point,
                    this.basicLabelsInfo_.length);

            this.basicLabelsInfo_.push(labelInfo);
            point.setLabelIndex(this.basicLabelsInfo_.length - 1);
            if (this.criticalAngle_ == -10)
                this.criticalAngle_ = Math.acos(1 / (1 + labelPadding / this.outerRadius_));
        }

    this.labelRadius_ = labelPadding + this.outerRadius_;
    this.innerRadius_ = this.outerRadius_ * this.globalSeriesSettings_.getInnerRadius();
};
anychart.plots.piePlot.PiePlot.prototype.getLabelPositionByIndex = function(index, needAlign) {
    return this.getLabelPosition_(this.basicLabelsInfo_[index], needAlign);
};
anychart.plots.piePlot.PiePlot.prototype.getLabelPosition_ = function(labelInfo, needAlign) {
    var pos = labelInfo.getPoint().getCenterPoint();

    //label position
    if (this.is3D_) {
        var yRadius = this.getYRadius(this.outerRadius_);
        var labelPadding = this.labelRadius_ - this.outerRadius_;
        pos.x += (this.outerRadius_ + labelPadding) * Math.cos(labelInfo.getAngle() * Math.PI / 180);
        pos.y += (yRadius + labelPadding) * Math.sin(labelInfo.getAngle() * Math.PI / 180);
        pos.y += this.get3DHeight() / 2;
    } else {
        pos.x += (this.labelRadius_) * Math.cos(labelInfo.getAngle() * Math.PI / 180);
        pos.y += (this.labelRadius_) * Math.sin(labelInfo.getAngle() * Math.PI / 180);
    }

    var labelRect = new anychart.utils.geom.Rectangle(
            0,
            0,
            labelInfo.getWidth(),
            labelInfo.getHeight());

    //connector padding
    if (this.globalSeriesSettings_.isConnectorEnabled()) {
        if (labelInfo.getAngle() > 90 && labelInfo.getAngle() < 270)
            pos.x -= this.globalSeriesSettings_.getConnectorPadding();
        else
            pos.x += this.globalSeriesSettings_.getConnectorPadding();
    }
    //align
    if (needAlign) {
        if (Math.cos(labelInfo.getAngle() * Math.PI / 180) < 0)
            anychart.layout.HorizontalAlign.apply(pos, anychart.layout.HorizontalAlign.LEFT, labelRect, 0);
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER, labelRect, 0);
    }

    return pos;
};
/**
 * TODO: docs
 */
anychart.plots.piePlot.PiePlot.prototype.getLabelAngleInRightHalf = function(labelInfo, y) {
    y -= this.getCenterPoint().y;
    var res;
    if (this.is3D()) {
        var yRadius = this.getYRadius(this.outerRadius_);
        var labelPadding = this.labelRadius_ - this.outerRadius_;
        y -= this.get3DHeight() / 2;
        res = Math.asin(y / (yRadius + labelPadding)) * 180 / Math.PI;
    } else {
        res = Math.asin(y / (this.labelRadius_)) * 180 / Math.PI;
    }
    if (isNaN(res))
        return (y > 0) ? (90.01) : (-89.99);
    return res;
};

//------------------------------------------------------------------------------
//                    New labels position logic
//------------------------------------------------------------------------------
anychart.plots.piePlot.PiePlot.prototype.logicMethodOfCalculateLabels = function() {
    var clustersPool = [];
    var i;
    var l = this.basicLabelsInfo_.length;

    for (i = 0; i < l; i++)
        clustersPool.push(
                new anychart.plots.piePlot.labels.Cluster(
                        this.basicLabelsInfo_[i],
                        Math.acos(this.outerRadius_ / this.labelRadius_) * 180 / Math.PI,
                        this));

    clustersPool.sort(function(a, b) {
        return a.getInitialAngle() - b.getInitialAngle();
    });

    var clusters = [];

    l = clustersPool.length;
    for (i = 0; i < l; i++) {
        var currCluster = clustersPool[i];
        var topCluster = clusters.pop();
        while (topCluster != null) {
            if (topCluster.checkOverlap(currCluster)) {
                topCluster.merge(currCluster);
                currCluster = topCluster;
                topCluster = clusters.pop();
            } else {
                clusters.push(topCluster);
                break;
            }
        }
        clusters.push(currCluster);
    }

    l = clusters.length;
    for (i = 0; i < l; i++) clusters[i].applyPositioning();
};
anychart.plots.piePlot.PiePlot.prototype.calculateLabelsOverlapLogic = function() {
    var lbl, lastVisible;
    if (!this.getAllowOverlap() && this.basicLabelsInfo_.length > 0) {

        for (var key in this.basicLabelsInfo_) {
            lbl = this.basicLabelsInfo_[key];
            lastVisible = this.enabledLabel_(lbl, false);
        }
        lastVisible = this.enabledLabel_(this.basicLabelsInfo_[this.basicLabelsInfo_.length - 1]);

        for (key in this.basicLabelsInfo_) {
            lbl = this.basicLabelsInfo_[key];
            if (lbl != lastVisible && !this.calcOverlapLogic_(lbl, lastVisible))
                lastVisible = this.enabledLabel_(lbl);
        }
    }
    else
        for (key in this.basicLabelsInfo_) {
            lbl = this.basicLabelsInfo_[key];
            lastVisible = this.enabledLabel_(lbl);
        }
};
/**
 * TODO: Docs
 * @private
 */
anychart.plots.piePlot.PiePlot.prototype.calcOverlapLogic_ = function(currLabel, visibleVisible) {
    var geom = anychart.utils.geom;
    var currentPos = this.getLabelPosition_(currLabel, true);
    var labelRect = new geom.Rectangle(currentPos.x, currentPos.y, currLabel.width_, currLabel.height_);
    var visiblePos = this.getLabelPosition_(visibleVisible, true);
    var rect = new geom.Rectangle(visiblePos.x, visiblePos.y, visibleVisible.width_, visibleVisible.height_);
    return labelRect.intersects(rect);
};

/**
 * Change enabled value to label info and related point label.
 * @private
 * @param {anychart.plots.piePlot.labels.LabelInfo} labelInfo
 * @param {Boolean} value
 * @return {anychart.plots.piePlot.labels.LabelInfo}
 */
anychart.plots.piePlot.PiePlot.prototype.enabledLabel_ = function(labelInfo, value) {
    if (value == null || value == undefined) value = true;
    labelInfo.setEnabled(value);
    labelInfo.getPoint().enableLabel(value);
    return value ? labelInfo : null;
};
//------------------------------------------------------------------------------
//                    Old labels position logic
//------------------------------------------------------------------------------
anychart.plots.piePlot.PiePlot.prototype.checkOverlap_ = function() {
    var labelsCount = this.basicLabelsInfo_.length;
    for (var i = 0; i < labelsCount; i++)
        if (!this.calcOverlap_(this.basicLabelsInfo_[i])) return true;
    return false;
};
anychart.plots.piePlot.PiePlot.prototype.calcOverlap_ = function(labelInfo) {
    var position = this.getLabelPosition_(labelInfo, true);
    var labelRect = new anychart.utils.geom.Rectangle(
            position.x,
            position.y,
            labelInfo.width,
            labelInfo.height);

    for (var i = 0; i < this.basicLabelsInfo_.length; i++) {
        if (i != labelInfo.getIndex()) {
            var currInfo = this.basicLabelsInfo_[i];
            if (!currInfo.isEnabled()) continue;
            var currPos = this.getLabelPosition_(currInfo, true);
            var rect = new anychart.utils.geom.Rectangle(
                    currPos.x,
                    currPos.y,
                    currInfo.width,
                    currInfo.height);

            if (labelRect.intersects(rect)) return false;
        }
    }
    return true;
};
anychart.plots.piePlot.PiePlot.prototype.calculateLabelsOverlapAnglesDown = function() {
    var l = this.basicLabelsInfo_.length - 1;
    var currentBounds, nextBounds, currentPos, nextPos, currentLabelInfo, nextLabelInfo;

    for (var i = 0; i <= l; i++) {
        currentLabelInfo = this.basicLabelsInfo_[i];
        nextLabelInfo = (i == l) ? (this.basicLabelsInfo_[0]) : (this.basicLabelsInfo_[i + 1]);
        currentPos = this.getLabelPosition_(currentLabelInfo, true);
        nextPos = this.getLabelPosition_(nextLabelInfo, true);
        currentBounds = new anychart.utils.geom.Rectangle(
                currentPos.x,
                currentPos.y,
                currentLabelInfo.getWidth(),
                currentLabelInfo.getHeight());

        nextBounds = new anychart.utils.geom.Rectangle(
                nextPos.x,
                nextPos.y,
                nextLabelInfo.getWidth(),
                nextLabelInfo.getWidth());

        if (!currentBounds.intersects(nextBounds) && currentLabelInfo.getAngle() > nextLabelInfo.getAngle()) continue;

        var tmpAngle = Math.ceil(currentLabelInfo.getAngle() - this.criticalAngle_ * 180 / Math.PI);
        for (var angle = currentLabelInfo.getAngle(); angle > tmpAngle; angle--) {
            currentLabelInfo.setAngle(angle);

            var pos = this.getLabelPosition_(currentLabelInfo, true);
            currentBounds.x = pos.x;
            currentBounds.y = pos.y;
            if (!currentBounds.intersects(nextBounds)){
                currentLabelInfo.setAngle(angle);
                break;
            }
        }
    }
    return this.checkOverlap();
};
anychart.plots.piePlot.PiePlot.prototype.calculateLabelsOverlapAnglesUp = function() {
    var l = this.basicLabelsInfo_.length - 1;

    var currentBounds, previousBounds, currentPos, previousPos, currentLabelInfo, previousLabelInfo;
    var geom = anychart.utils.geom;
    for (var i = 0; i <= l; i++) {
        currentLabelInfo = this.basicLabelsInfo_[i];
        previousLabelInfo = (i == 0) ? (this.basicLabelsInfo_[l]) : (this.basicLabelsInfo_[i - 1]);
        
        currentPos = this.getLabelPosition_(currentLabelInfo, true);
        previousPos = this.getLabelPosition_(previousLabelInfo, true);

        currentBounds = new geom.Rectangle(
                currentPos.x,
                currentPos.y,
                currentLabelInfo.getWidth(),
                currentLabelInfo.getHeight());

        previousBounds = new geom.Rectangle(
                previousPos.x,
                previousPos.y,
                previousLabelInfo.getWidth(),
                previousLabelInfo.getHeight());

        if (!currentBounds.intersects(previousBounds) && currentLabelInfo.getAngle() > previousLabelInfo.getAngle())
            continue;

        var tmpAngle = Math.ceil(currentLabelInfo.getAngle() + this.criticalAngle_ * 180 / Math.PI);
        for (var angle = currentLabelInfo.getAngle(); angle < tmpAngle; angle++) {
            currentLabelInfo.setAngle(angle);

            var pos = this.getLabelPosition_(currentLabelInfo, true);
            currentBounds.x = pos.x;
            currentBounds.y = pos.y;

            if (!currentBounds.intersects(previousBounds)) {
                currentLabelInfo.setAngle(angle);
                break
            }
        }

    }
    return this.checkOverlap();
};
anychart.plots.piePlot.PiePlot.prototype.calculateLabelsOverlap = function() {
    for (var i = 0; i < this.basicLabelsInfo_.length; i++) {
        var labelInfo = this.basicLabelsInfo_[i];
        if (labelInfo.isEnabled() && !this.calcOverlap_(labelInfo)) {
            if (!this.getAllowOverlap()) {
                labelInfo.setEnabled(false);
                labelInfo.getPoint().enableLabel(false);
            } else {
                labelInfo.setEnabled(true);
                labelInfo.getPoint().enableLabel(true);
            }
        } else {
            labelInfo.setEnabled(true);
            labelInfo.getPoint().enableLabel(true);
        }
    }
};
