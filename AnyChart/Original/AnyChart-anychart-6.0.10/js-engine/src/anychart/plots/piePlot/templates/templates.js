/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.templates.PiePlotTemplatesMerger}.</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.templates');
goog.require('anychart.plots.piePlot.templates.DEFAULT_TEMPLATE');
goog.require('anychart.plots.seriesPlot.templates');
//----------------------------------------------------------------------------------------------------------------------
//
//                           PiePlotTemplatesMerger class.
//
//----------------------------------------------------------------------------------------------------------------------
/**
 * Creates PiePlotTemplatesMerger - a redundant merger class, that exists only
 * for strictness and AnyChart source compatibility. This class inherits everything
 * from SeriesPlotTemplatesMerger and doesn't add or override any properties. 
 *
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.piePlot.templates.PiePlotTemplatesMerger = function(){
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.apply(this);
};

goog.inherits(anychart.plots.piePlot.templates.PiePlotTemplatesMerger, anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger);

/**
 * Returns plot default template.
 *
 * @public
 * @override
 * @return {object}
 */
anychart.plots.piePlot.templates.PiePlotTemplatesMerger.prototype.getPlotDefaultTemplate = function(){
    return this.mergeTemplates(goog.base(this, 'getPlotDefaultTemplate'), anychart.plots.piePlot.templates.DEFAULT_TEMPLATE);
};