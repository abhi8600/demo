/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.data.drawers.PiePoint3DDrawer};</li>
 *  <li>@class {anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer};</li>
 *  <li>@class {anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer};</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.data.drawers');
goog.require('anychart.utils.geom');
//------------------------------------------------------------------------------
//
//                           PiePoint3DDrawer class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param svgManager
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer = function(svgManager) {
    this.startAngles_ = [];
    this.endAngles_ = [];
    this.shapes_ = [];
    this.svgManager_ = svgManager;
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<Number>}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.startAngles_ = null;
/**
 * @param {int} index
 * @return {Number}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getStartAngleAt = function(index) {
    return this.startAngles_[index];
};
/**
 * @private
 * @type {Array.<Number>}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.endAngles_ = null;
/**
 * @param {int} index
 * @return {Number}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getEndAngle = function(index) {
    return this.endAngles_[index];
};
/**
 * @private
 * @type {anychart.plots.piePlot.data.Pie3DPoint}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.point_ = null;
/**
 * @private
 * @type {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.shapes_ = null;
/**
 * Gets the count of shapes.
 * @return {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getLength = function() {
    return this.shapes_.length;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.isInFrontFlag_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.isInFront = function() {
    return this.isInFrontFlag_;
};
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.svgManager_ = null;
//------------------------------------------------------------------------------
//                          Initialization
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.piePlot.data.Pie3DPoint} point
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.initialize = function(point) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * @param {int} index
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.draw = function(index) {
    goog.abstractMethod();
};
/**
 * @private
 * @param {anychart.utils.geom.Point} centerPoint
 * @param {Number} startAngle
 * @param {Number} endAngle
 * @param {Number} innerYR
 * @param {Number} innerXR
 * @param {Number} height
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getDrawingPath_ =
        function(centerPoint, startAngle, endAngle, innerYR, innerXR, height) {
            goog.abstractMethod();
        };
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**
 * @return {int}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getLength = function() {
    return this.shapes_.length;
};
/**
 * @param {int} index
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getSprite = function(index) {
    return this.shapes_[index];
};
/**
 * @param {int} index
 * @return {Number}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getCenterAngle = function(index) {
    var start = this.startAngles_[index];
    var end = this.endAngles_[index];

    if (end < start)
        end += 360;

    return (start + end) / 2;
};
/**
 * @param {Number} startAngle
 * @param {Number} endAngle
 * @return {Boolean}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.isEnabled = function(startAngle, endAngle) {
    goog.abstractMethod();
};
/**
 * Return angle fourth by angle sin and cos.
 * @private
 * @param {Number} cos
 * @param {Number} sin
 * @return {Number}
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.getAngleFourth_ = function(cos, sin) {
    if (cos >= 0 && sin >= 0)
        return 1;
    if (cos <= 0 && sin >= 0)
        return 2;
    if (cos <= 0 && sin < 0)
        return 3;
    return 4;
};
/**
 * @private
 * @param {Number} startAngle
 * @param {Number} endAngle
 */
anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.prototype.normalizeAngles = function(startAngle, endAngle) {
    if (endAngle < startAngle) endAngle += 360;
};
//------------------------------------------------------------------------------
//
//                           PiePoint3DBackSideDrawer class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.piePlot.data.drawers.PiePoint3DDrawer}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer = function(svgManager) {
    anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.call(this, svgManager);
};
goog.inherits(anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer,
        anychart.plots.piePlot.data.drawers.PiePoint3DDrawer);
//------------------------------------------------------------------------------
//                          Initialize
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer.prototype.initialize = function(point) {
    this.point_ = point;

    var startAngle = point.getStartAngle();
    var endAngle = point.getEndAngle();

    var startSin = Math.sin(startAngle * Math.PI / 180);
    var endSin = Math.sin(endAngle * Math.PI / 180);

    var startCos = Math.cos(startAngle * Math.PI / 180);
    var endCos = Math.cos(endAngle * Math.PI / 180);

    var startFourth = this.getAngleFourth_(startCos, startSin);
    var endFourth = this.getAngleFourth_(endCos, endSin);

    if (startFourth == 1) {
        switch (endFourth) {
            case 1:
                if (startCos <= endCos) {
                    this.startAngles_.push(180);
                    this.endAngles_.push(360);
                }
                break;
            case 3:
                this.startAngles_.push(180);
                this.endAngles_.push(endAngle);
                break;
            case 4:
                this.startAngles_.push(180);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
        }
    } else if (startFourth == 2) {
        switch (endFourth) {
            case 1:
                this.startAngles_.push(180);
                this.endAngles_.push(360);
                this.isInFrontFlag_ = true;
                break;
            case 2:
                if (startCos <= endCos) {
                    this.startAngles_.push(180);
                    this.endAngles_.push(360);
                    this.isInFrontFlag_ = true;
                }
                break;
            case 3:
                this.startAngles_.push(180);
                this.endAngles_.push(endAngle);
                break;
            case 4:
                this.startAngles_.push(180);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
        }
    } else if (startFourth == 3) {
        switch (endFourth) {
            case 1:
            case 2:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(360);
                this.isInFrontFlag_ = true;
                break;
            case 3:
                if (startCos >= endCos) {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(360);
                    this.isInFrontFlag_ = true;
                    this.startAngles_.push(180);
                    this.endAngles_.push(endAngle);
                } else {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(endAngle);
                }
                break;
            case 4:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
        }
    } else if (startFourth == 4) {
        switch (endFourth) {
            case 1:
            case 2:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(360);
                break;
            case 3:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(360);
                this.startAngles_.push(180);
                this.endAngles_.push(endAngle);
                break;
            case 4:
                if (startCos >= endCos) {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(360);
                    this.startAngles_.push(180);
                    this.endAngles_.push(endAngle);
                    this.isInFrontFlag_ = true;
                } else {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(endAngle);
                }
                break;
        }
    }

    var anglesCount = this.startAngles_.length;
    var sprite;
    for (var i = 0; i < anglesCount; i++) {
        sprite = this.point_.backSideShape_;
        this.shapes_.push(sprite);
    }

};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer.prototype.draw = function(index) {
    var sprite = this.getSprite(index);

    var innerXR = this.point_.getSeries().getInnerRadius();
    var innerYR = this.point_.getSeries().getInnerYRadius();
    var center = this.point_.getCenterPoint();

    var h = this.point_.getPlot().get3DHeight();

    var startAngle = this.startAngles_[index] * Math.PI / 180;
    var endAngle = this.endAngles_[index] * Math.PI / 180;

    this.normalizeAngles(startAngle, endAngle);

    var innerStartX = center.x + innerXR * Math.cos(startAngle);
    var innerStartY = center.y + innerYR * Math.sin(startAngle);

    var innerEndX = center.x + innerXR * Math.cos(endAngle);
    var innerEndY = center.y + innerYR * Math.sin(endAngle);

    var largeArc1 = false;
    var sweepArc1 = true;

    if (this.sweepAngle_ > 180) {
        largeArc1 = !largeArc1;
        sweepArc1 = !sweepArc1;
    }

    var svgManager = sprite.getSVGManager();
    var pathData = '';
    pathData += svgManager.pMove(innerStartX, innerStartY);
    pathData += svgManager.pCircleArc(innerXR, largeArc1, sweepArc1, innerEndX, innerEndY, innerYR);
    pathData += svgManager.pLine(innerEndX, innerEndY + h);

    pathData += svgManager.pCircleArc(innerXR, 0, 0, innerStartX, innerStartY + h, innerYR);
    pathData += svgManager.pLine(innerStartX, innerStartY);

    return pathData;
};
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer.prototype.getDrawingPath_ =
        function(centerPoint, startAngle, endAngle, innerYR, innerXR, height) {
            /*DrawingUtils.drawArc(g, centerPoint.x, centerPoint.y,
             startAngle, endAngle,
             innerYR, innerXR,0, true);

             DrawingUtils.drawArc(g, centerPoint.x, centerPoint.y+height,
             endAngle, startAngle,
             innerYR, innerXR,0, false);
             g.lineTo(centerPoint.x+DrawingUtils.getPointX(innerXR, startAngle),
             centerPoint.y+DrawingUtils.getPointY(innerYR, startAngle));*/
        };
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer.prototype.isEnabled = function(startAngle, endAngle) {
    startAngle *= Math.PI / 180;
    endAngle *= Math.PI / 180;

    var startCos = Math.cos(startAngle);
    var endCos = Math.cos(endAngle);

    var startF = this.getAngleFourth_(startCos, Math.sin(startAngle));
    var endF = this.getAngleFourth_(endCos, Math.sin(endAngle));

    if (startF == 3 || startF == 4) return true;

    if (startF == 1) {
        if (endF == 3 || endF == 4) return true;
        if (endF == 1) return (startCos <= endCos);
        return false;
    }

    if (startF == 2) {
        if (endF == 2) return (startCos <= endCos);
        return true;
    }
    return false;
};
//------------------------------------------------------------------------------
//
//                           PiePoint3DFrontSideDrawer class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.piePlot.data.drawers.PiePoint3DDrawer}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer = function(svgManager) {
    anychart.plots.piePlot.data.drawers.PiePoint3DDrawer.call(this, svgManager);
};
goog.inherits(anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer, anychart.plots.piePlot.data.drawers.PiePoint3DDrawer);
//------------------------------------------------------------------------------
//                          Initialize
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer.prototype.initialize = function(point) {
    this.point_ = point;

    var startAngle = point.getStartAngle();
    var endAngle = point.getEndAngle();

    var startSin = Math.sin(startAngle * Math.PI / 180);
    var endSin = Math.sin(endAngle * Math.PI / 180);

    var startCos = Math.cos(startAngle * Math.PI / 180);
    var endCos = Math.cos(endAngle * Math.PI / 180);

    var startFourth = this.getAngleFourth_(startCos, startSin);
    var endFourth = this.getAngleFourth_(endCos, endSin);

    if (startFourth == 1) {
        switch (endFourth) {
            case 1:
                if (startCos >= endCos) {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(endAngle);
                } else {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(180);

                    this.startAngles_.push(360);
                    this.endAngles_.push(endAngle);

                    this.isInFrontFlag_ = true;
                }
                break;
            case 2:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
            case 3:
            case 4:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(180);
                this.isInFrontFlag_ = true;
                break;
        }
    } else if (startFourth == 2) {
        switch (endFourth) {
            case 1:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(180);

                this.startAngles_.push(360);
                this.endAngles_.push(endAngle);
                break;
            case 2:
                if (startCos >= endCos) {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(endAngle);
                } else {
                    this.startAngles_.push(startAngle);
                    this.endAngles_.push(180);

                    this.startAngles_.push(360);
                    this.endAngles_.push(endAngle);
                    this.isInFrontFlag_ = true;
                }
                break;
            case 3:
            case 4:
                this.startAngles_.push(startAngle);
                this.endAngles_.push(180);
                break;
        }
    } else if (startFourth == 3) {
        switch (endFourth) {
            case 1:
                this.startAngles_.push(360);
                this.endAngles_.push(endAngle);
                break;
            case 2:
                this.startAngles_.push(360);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
            case 3:
                if (startCos >= endCos) {
                    this.startAngles_.push(0);
                    this.endAngles_.push(180);
                    this.isInFrontFlag_ = true;
                }
                break;
        }
    } else if (startFourth == 4) {
        switch (endFourth) {
            case 1:
                this.startAngles_.push(360);
                this.endAngles_.push(endAngle);
                break;
            case 2:
                this.startAngles_.push(360);
                this.endAngles_.push(endAngle);
                this.isInFrontFlag_ = true;
                break;
            case 3:
                this.startAngles_.push(360);
                this.endAngles_.push(180);
                this.isInFrontFlag_ = true;
                break;
            case 4:
                if (startCos >= endCos) {
                    this.startAngles_.push(0);
                    this.endAngles_.push(180);
                    this.isInFrontFlag_ = true;
                }
                break;
        }
    }

    var anglesCount = this.startAngles_.length;
    var sprite;
    for (var i = 0; i < anglesCount; i++) {
        sprite = this.point_.frontSideShape_;
        this.shapes_.push(sprite);
    }
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer.prototype.draw = function(index) {
    var sprite = this.getSprite(index);

    var outerXR = this.point_.getSeries().getOuterRadius();
    var outerYR = this.point_.getSeries().getOuterYRadius();
    var center = this.point_.getCenterPoint();

    var h = this.point_.getPlot().get3DHeight();

    var startAngle = this.startAngles_[index] * Math.PI / 180;
    var endAngle = this.endAngles_[index] * Math.PI / 180;

    this.normalizeAngles(startAngle, endAngle);

    var outerStartX = center.x + outerXR * Math.cos(startAngle);
    var outerStartY = center.y + outerYR * Math.sin(startAngle);

    var outerEndX = center.x + outerXR * Math.cos(endAngle);
    var outerEndY = center.y + outerYR * Math.sin(endAngle);

    var largeArc1 = false;
    var sweepArc1 = true;

    if (this.sweepAngle_ > 180) {
        largeArc1 = !largeArc1;
        sweepArc1 = !sweepArc1;
    }

    var svgManager = sprite.getSVGManager();
    var pathData = '';

    pathData += svgManager.pMove(outerStartX, outerStartY);
    pathData += svgManager.pCircleArc(outerXR, largeArc1, sweepArc1, outerEndX, outerEndY, outerYR);
    pathData += svgManager.pLine(outerEndX, outerEndY + h);
    pathData += svgManager.pCircleArc(outerXR, 0, 0, outerStartX, outerStartY + h, outerYR);
    pathData += svgManager.pLine(outerStartX, outerStartY);

    return pathData;
};
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer.prototype.getDrawingPath_ = function(centerPoint, startAngle, endAngle, innerYR, innerXR, height) {
/*        var svgManager = this.svgManager_;
        svgManager.pCircleArc()
        du.drawArc(g, center.x, center.y,
         start, end,
         outerYR, outerXR,0, true);

         DrawingUtils.drawArc(g, center.x, center.y+h,
         end, start,
         outerYR, outerXR,0, false);
         g.lineTo(center.x+DrawingUtils.getPointX(outerXR, start),
         center.y+DrawingUtils.getPointY(outerYR, start));*/
};
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer.prototype.isEnabled = function(startAngle, endAngle) {
    if (startAngle == endAngle) return false;

    startAngle *= Math.PI / 180;
    endAngle *= Math.PI / 180;

    var startCos = Math.cos(startAngle);
    var endCos = Math.cos(endAngle);

    var startFourth = this.getAngleFourth_(startCos, Math.sin(startAngle));
    var endFourth = this.getAngleFourth_(endCos, Math.sin(endAngle));

    if (startFourth == 1 || startFourth == 2) return true;

    if (startFourth == 3) {
        if (endFourth == 1 || endFourth == 2) return true;
        if (endFourth == 3) return (startCos >= endCos);
        return false;
    }

    if (startFourth == 4) {
        if (endFourth == 4) return (startCos >= endCos);
        return true;
    }

    return false;
};