/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.data.BaseDataElement}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.BaseDynamicElementsContainer}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.BasePoint}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.BaseSeries}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.GlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings}</li>
 * <ul>
 */

goog.provide('anychart.plots.seriesPlot.data');

goog.require('goog.events');
goog.require('anychart.actions');
goog.require('anychart.utils');
goog.require('anychart.styles');
goog.require('anychart.elements');
goog.require('anychart.layout');
goog.require('anychart.interpolation');
goog.require('anychart.plots.seriesPlot.elements');
//------------------------------------------------------------------------------
//
//                          BaseDataElement class
//
//------------------------------------------------------------------------------

/**
 * Class represent base element for all chart data points and series.
 * @public
 * @constructor
 */
anychart.plots.seriesPlot.data.BaseDataElement = function () {
};
//------------------------------------------------------------------------------
//                           MainChartView.
//------------------------------------------------------------------------------
/**
 * @return {anychart.chartView.ChartView}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.getMainChartView = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Interactivity
//------------------------------------------------------------------------------
/**
 * Is data point selectable
 * @private
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.isSelectable_ = true;
/**
 * @return {boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.isSelectable = function () {
    return this.isSelectable_;
};
/**
 * Is data point hoverable
 * @private
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.isHoverable_ = true;
/**
 * @return {boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.isHoverable = function () {
    return this.isHoverable_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.useHandCursor_ = true;
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.useHandCursor = function () {
    return this.useHandCursor_;
};
/**
 * @param {anychart.plots.seriesPlot.data.BaseDataElement} container
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.setInteractivitySettings = function (container) {
    this.isSelectable_ = container.isSelectable();
    this.isHoverable_ = container.isHoverable();
    this.useHandCursor_ = container.useHandCursor();
};
/**
 * Deserialize element interactivity settings.
 * @param {Object} data JSON object with interactivity settings.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeInteractivity = function (data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'allow_select'))
        this.isSelectable_ = des.getBoolProp(data, 'allow_select');

    if (des.hasProp(data, 'hoverable'))
        this.isHoverable_ = des.getBoolProp(data, 'hoverable');

    if (des.hasProp(data, 'use_hand_cursor'))
        this.useHandCursor_ = des.getBoolProp(data, 'use_hand_cursor');
};
//------------------------------------------------------------------------------
//                          Styles.
//------------------------------------------------------------------------------
/**
 * Data element style object.
 * @private
 * @type {anychart.styles.Style}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.style_ = null;
/**
 * @return {anychart.styles.Style}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.getStyle = function () {
    return this.style_;
};
/**
 * @param {anychart.styles.Style} value
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.setStyle = function (value) {
    this.style_ = value;
};
//------------------------------------------------------------------------------
//                          Elements
//------------------------------------------------------------------------------
/**
 * Point label
 * @private
 * @type {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.label_ = null;

anychart.plots.seriesPlot.data.BaseDataElement.prototype.getLabel = function () {
    return this.label_;
};

/**
 * Point marker
 * @private
 * @type {anychart.plots.seriesPlot.elements.MarkerElement}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.marker_ = null;

anychart.plots.seriesPlot.data.BaseDataElement.prototype.getMarker = function () {
    return this.marker_;
};

/**
 * Point tooltip
 * @private
 * @type {anychart.plots.seriesPlot.elements.TooltipElement}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.tooltip_ = null;
/**
 * @return {anychart.plots.seriesPlot.elements.TooltipElement}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.getTooltip = function () {
    return  this.tooltip_;
};
/**
 * Indicates, can point draw tooltips.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.canDrawTooltips_ = true;
/**
 * Extra labels array.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.elements.LabelElement>}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.extraLabels_ = null;

/**
 * Extra markers array.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.elements.MarkerElement>}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.extraMarkers_ = null;

/**
 * Extra tooltips array.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.elements.TooltipElement>}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.extraTooltips_ = null;
//------------------------------------------------------------------------------
//                    Elements deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize elements settings.
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeElements = function (data, stylesList) {
    this.deserializeLabelElement_(data, stylesList);
    this.deserializeMarkerElement_(data, stylesList);
    this.deserializeTooltipElement_(data, stylesList);
    this.deserializeExtraLabelsElements_(data, stylesList);
    this.deserializeExtraMarkersElements_(data, stylesList);
    this.deserializeExtraTooltipsElements_(data, stylesList);
};

/**
 * Deserialize label element.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeLabelElement_ = function (data, stylesList) {
    goog.abstractMethod();
};

/**
 * Deserialize marker element.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    goog.abstractMethod();
};

/**
 * Deserialize tooltip element.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeTooltipElement_ = function (data, stylesList) {
    goog.abstractMethod();
};

/**
 * Deserialize extra labels elements.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeExtraLabelsElements_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'extra_labels')) {
        this.extraLabels_ = [];
        var labelsList = des.getPropArray(
            des.getProp(
                data,
                'extra_labels'),
            'label');

        var labelsCount = labelsList.length;
        for (var i = 0; i < labelsCount; i++) {
            var label = this.globalSettings_.createLabelElement();
            label.deserializeGlobalSettings(labelsList[i], stylesList);
            this.extraLabels_.push(label);
        }
    }
};

/**
 * Deserialize extra markers elements.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeExtraMarkersElements_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'extra_markers')) {
        this.extraMarkers_ = [];
        var markersList = des.getPropArray(
            des.getProp(
                data,
                'extra_markers'),
            'marker');

        var markersCount = markersList.length;
        for (var i = 0; i < markersCount; i++) {
            var marker = this.globalSettings_.createMarkerElement();
            marker.deserializeGlobalSettings(markersList[i], stylesList);
            this.extraMarkers_.push(marker);
        }
    }
};

/**
 * Deserialize extra tooltips elements.
 * @protected
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeExtraTooltipsElements_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'extra_tooltips')) {
        this.extraTooltips_ = [];
        var tooltipsList = des.getPropArray(
            des.getProp(
                data,
                'extra_tooltips'),
            'tooltip');

        var tooltipsCount = tooltipsList.length;
        for (var i = 0; i < tooltipsCount; i++) {
            var tooltip = this.globalSettings_.createTooltipElement();
            tooltip.deserializeGlobalSettings(tooltipsList[i], stylesList);
            this.extraTooltips_.push(tooltip);
        }
    }
};

/**
 * Override passed data elements.
 * @param {anychart.plots.seriesPlot.data.BaseDataElement} target Data element.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.setElements = function (target) {
    target.label_ = this.label_;
    target.marker_ = this.marker_;
    target.tooltip_ = this.tooltip_;
    target.extraLabels_ = this.extraLabels_;
    target.extraMarkers_ = this.extraMarkers_;
    target.extraTooltips_ = this.extraTooltips_;
};
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize data element settings.
 * @public
 * @param config
 * @param stylesList
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserialize = function (config, stylesList) {
};
//------------------------------------------------------------------------------
//                          Actions.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.actions.ActionsList}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.actionsList_ = null;
/**
 * @return {anychart.actions.ActionsList}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.getActionsList = function () {
    return this.actionsList_
};
/**
 * @param {anychart.actions.ActionsList} value
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.setActionsList = function (value) {
    if (value) this.actionsList_ = value;
};
/**
 * Deserialize element actions list.
 * @param {Object} data JSON object with element actions settings.
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeActions = function (data) {
    var des = anychart.utils.deserialization;
    if (data && des.hasProp(data, 'actions')) {
        this.actionsList_ = new anychart.actions.ActionsList(this.getMainChartView());
        this.actionsList_.deserialize(des.getProp(data, 'actions'));
    }
};

//------------------------------------------------------------------------------
//
//               BaseDynamicElementsContainer class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer = function () {
    anychart.plots.seriesPlot.data.BaseDataElement.call(this);
    this.initFormatting();
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.BaseDynamicElementsContainer, anychart.plots.seriesPlot.data.BaseDataElement);
//------------------------------------------------------------------------------
//                          Common settings
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.id_ = null;
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getId = function () {
    return this.id_;
};

anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.index_ = null;
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setIndex = function (value) {
    this.index_ = value;
};
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getIndex = function () {
    return this.index_;
};

anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.name_ = null;
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setName = function (value) {
    this.name_ = value;
};
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getName = function () {
    return this.name_;
};

anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.customAttributes_ = null;

/**
 * Data element point.
 * @protected
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.color_ = null;

/**
 * Set new color for data element.
 * @public
 * @param {anychart.visual.color.ColorContainer} value New color value.
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setColor = function (value) {
    this.color_ = value;
};

/**
 * Return data element color value.
 * @public
 * @return {anychart.visual.color.ColorContainer}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getColor = function () {
    return this.color_;
};


anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.hatchType_ = null;
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setHatchType = function (value) {
    this.hatchType_ = value;
};
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getHatchType = function () {
    return this.hatchType_;
};

anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.markerType_ = null;
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setMarkerType = function (value) {
    this.markerType_ = value;
};
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getMarkerType = function () {
    return this.markerType_;
};

/**
 * Global series settings object.
 * @protected
 * @type {anychart.plots.seriesPlot.data.GlobalSeriesSettings}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.globalSettings_ = null;

anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setGlobalSettings = function (value) {
    this.globalSettings_ = value;
};
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getGlobalSettings = function () {
    return this.globalSettings_;
};
/**
 * @return {anychart.chartView.MainChartView}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getMainChartView = function () {
    return this.globalSettings_.getMainChartView();
};
/**
 * Return plot.
 * @public
 * @return {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getPlot = function () {
    return this.globalSettings_.getPlot();
};
//------------------------------------------------------------------------------
//                          Locale.
//------------------------------------------------------------------------------
/**
 * @return {anychart.locale.DateTimeLocale}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getDateTimeLocale = function () {
    return this.getPlot().getDateTimeLocale();
};
/**
 * @return {anychart.locale.NumberLocale}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getNumberLocale = function () {
    return this.getPlot().getNumberLocale();
};
//------------------------------------------------------------------------------
//                          Threshold
//------------------------------------------------------------------------------
/**
 * Element threshold.
 * @private
 * @type {anychart.thresholds.IThreshold}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.threshold_ = null;

/**
 * Set new value of Element threshold.
 * @private
 * @param {anychart.thresholds.IThreshold} value New element threshold value.
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setThreshold = function (value) {
    this.threshold_ = value;
};

/**
 * Define, is element have threshold.
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.hasThreshold = function () {
    return this.threshold_ != null;
};

/**
 * Return element threshold value.
 * @return {anychart.thresholds.IThreshold}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getThreshold = function () {
    return this.threshold_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'id')) this.id_ = des.getStringProp(config, 'id');
    if (des.hasProp(config, 'attributes')) {
        this.customAttributes_ = {};
        var attributes = des.getPropArray(des.getProp(config, 'attributes'), 'attribute');
        var attributesCount = attributes.length;

        for (var i = 0; i < attributesCount; i++) {
            var attribute = attributes[i];
            if (des.hasProp(attribute, 'name')) {
                //old version support
                var tempVal;
                if (des.hasProp(attribute, 'custom_attribute_value'))
                    tempVal = des.getProp(attribute, 'custom_attribute_value');
                else if (des.hasProp(attribute, 'value'))
                    tempVal = des.getProp(attribute, 'value');

                if(tempVal) des.setProp(this.customAttributes_, '%' + des.getProp(attribute, 'name'), tempVal);
            }
        }
    }
};

anychart.plots.seriesPlot.data.BaseDataElement.prototype.deserializeName = function (config) {
    if (anychart.utils.deserialization.hasProp(config, 'name')) {
        this.name_ = anychart.utils.deserialization.getStringProp(config, 'name');
    }
};


anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.deserializeStyle = function (config, stylesList) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(config, 'style')) return;
    var styleName = des.getStringProp(config, 'style');
    var styleNodeName = this.getGlobalSettings().getStyleNodeName();
    var cashedStyle = stylesList.getStyle(styleNodeName, styleName);
    //if (cashedStyle) { todo
//        this.style_ = cashedStyle;
//    } else {
    var actualStyleXML = stylesList.getStyleByApplicator(styleNodeName, styleName);
    if (actualStyleXML) {
        this.style_ = this.getGlobalSettings().createStyle();
        this.style_.deserialize(actualStyleXML);
        //stylesList.addStyle(styleNodeName, styleName, this.style_);
        //}
    }
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * Cashed tokes map, key is token name.
 * @protected
 * @type {anychart.formatting.TokensHash}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.tokensHash_ = null;
/**
 * @return {anychart.formatting.TokensHash}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getTokensHash = function () {
    return this.tokensHash_
};
/**
 * @param {String} token
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.hasCustomAttribute = function (token) {
    return Boolean(this.customAttributes_ && this.customAttributes_[token]);
};
/**
 * Initialize element formatting.
 * @protected
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.initFormatting = function () {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
/**
 * Set defaults value for all element tokens.
 * @protected
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.setDefaultTokenValues = function () {
    goog.abstractMethod();
};
/**
 * Return token type by token name.
 * @return {anychart.formatting.TextFormatTokenType}
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.getTokenType = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
/**
 * Serialize element data to passed object.
 * @param {*} opt_target
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.serialize = function (opt_target) {
    if (opt_target == null || opt_target == undefined) opt_target = {};
    opt_target['Name'] = this.name_;
    opt_target['Attributes'] = this.serializeCustomAttributes(this.customAttributes_);
    opt_target['Color'] = this.color_.getColor().toRGBA();
    opt_target['HatchType'] = this.hatchType_;
    opt_target['MarkerType'] = this.markerType_;
    if (this.id_) opt_target['ID'] = this.id_;
    return opt_target;
};
/**
 * Удаляет первый символ из строки с кастомным аттрибутом который всегда должен быть '%'.
 * @see http://dev.anychart.com/jira/browse/SUP-337
 * @param {Object} customAttributes
 */
anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.prototype.serializeCustomAttributes = function(customAttributes) {
    if (!customAttributes) return;

    var res = {};
    for (var atrName in customAttributes)
        if(customAttributes.hasOwnProperty(atrName))
            res[/^%(.*)$/.exec(atrName)[1]] = customAttributes[atrName];

    return  res;
};
//------------------------------------------------------------------------------
//
//                          BasePoint class
//
//------------------------------------------------------------------------------
/**
 * Class represent base point for all data points.
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseDynamicElementsContainer}
 */
anychart.plots.seriesPlot.data.BasePoint = function () {
    anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.call(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.BasePoint,
    anychart.plots.seriesPlot.data.BaseDynamicElementsContainer);
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize base point settings.
 * @public
 * @override
 * @param {object} data JSON object with point settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserialize = function (data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    this.paletteIndex_ = this.index_;
    if (anychart.utils.deserialization.hasProp(data, 'selected'))
        this.isSelected_ = anychart.utils.deserialization.getBoolProp(data, 'selected');
};
//------------------------------------------------------------------------------
//                          Missing.
//------------------------------------------------------------------------------
/**
 * Defines, is point missing.
 * @protected
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.isMissing_ = false;
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.isMissing = function () {
    return this.isMissing_;
};
//------------------------------------------------------------------------------
//                          Series.
//------------------------------------------------------------------------------
/**
 * Link to series.
 * @private
 * @type {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.series_ = null;
/**
 * @param {anychart.plots.seriesPlot.data.BaseSeries} value
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.setSeries = function (value) {
    this.series_ = value;
};
/**
 * @return {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getSeries = function () {
    return this.series_;
};
//------------------------------------------------------------------------------
//                          Palettes.
//------------------------------------------------------------------------------
/**
 * Index in series palette. Usually it equals to #getIndex()
 * @type {number}
 * @private
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.paletteIndex_ = 0;
/**
 * Get index in series color palette
 * @return {number}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getPaletteIndex = function () {
    return this.paletteIndex_;
};
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getBounds = function () {
    return this.bounds_;
};
//------------------------------------------------------------------------------
//                          Threshold
//------------------------------------------------------------------------------
/**
 * Threshold instance.
 * @private
 * @type {anychart.thresholds.IFormattableThresholdItem}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.thresholdItem_ = null;

/**
 * Return point threshold item.
 * @return {anychart.thresholds.IFormattableThresholdItem}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getThresholdItem = function () {
    return this.thresholdItem_;
};

/**
 * Set new threshold item value.
 * @param {anychart.thresholds.IFormattableThresholdItem} value New threshold item value.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.setThresholdItem = function (value) {
    this.thresholdItem_ = value;
};

anychart.plots.seriesPlot.data.BasePoint.prototype.checkThreshold = function () {
    if (this.threshold_) this.threshold_.checkBeforeDraw(this);
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getSprite = function () {
    return this.sprite_;
};
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getSVGManager = function () {
    return this.sprite_.getSVGManager();
};
/**
 * @private
 * @type {anychart.styles.StyleShapes}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.styleShapes_ = null;
/**
 * @return {anychart.styles.StyleShapes}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getShapes = function () {
    return this.styleShapes_;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.initialize = function (svgManager) {
    if (!this.bounds_) this.bounds_ = new anychart.utils.geom.Rectangle();
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);

    this.initStyleState();
    this.initializeStyle();
    this.initializeElements();

    this.addEventHandlers(this.sprite_);
    return this.sprite_;
};
/**
 * Initialize point styles, override in marker series.
 * @protected
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.initializeStyle = function () {
    this.styleShapes_ = this.style_.initialize(this);
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.initializeAfterInterpolate = function () {
};
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**
 * Update point and point elements.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.update = function () {
    this.styleShapes_.update(this.currentState_, true);
    this.updateElements();
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Resize point and point elements.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.resize = function () {
    this.styleShapes_.update(null, true);
    this.resizeElements();
};
//------------------------------------------------------------------------------
//                          Styles.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.currentState_ = null;

/**
 * @param {anychart.styles.StyleState} value
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.setCurrentState = function (value) {
    this.currentState_ = value;
};
/**
 * @private
 * @type {String}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.currentStateString_ = null;
/**
 * Set first style state.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.initStyleState = function () {
    if (this.missing_) {
        this.currentState_ = this.style_.getMissing();
        this.currentStateString_ = 'Missing';
    } else if (this.isSelectable_ && this.isSelected_) {

        if (!this.getPlot().isAllowMultipleSelect()) {
            var previousSelectedPoint = this.getPlot().getSelectedPoint();
            if (previousSelectedPoint) {
                previousSelectedPoint.setSelected(false);
                previousSelectedPoint.setCurrentState(previousSelectedPoint.getStyle().getNormal());
            }
        }

        this.currentState_ = this.style_.getSelectedNormal();
        this.currentStateString_ = 'SelectedNormal';
        this.getPlot().selectPoint(this, false);
    } else {
        this.currentState_ = this.style_.getNormal();
        this.currentStateString_ = 'Normal';
    }
};
/**
 * @param {anychart.styles.Style} style
 * @return {anychart.styles.StyleState}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getCurrentStyleState = function (style) {
    return this.getStateByName(style, this.currentStateString_);
};
/**
 * @private
 * @param {anychart.styles.Style} style
 * @param {String} stateName
 * @return {anychart.styles.StyleState}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getStateByName = function (style, stateName) {
    switch (stateName) {
        default:
        case 'Normal':
            return style.getNormal();
        case 'Hover':
            return style.getHover();
        case 'Pushed':
            return style.getPushed();
        case 'SelectedNormal':
            return style.getSelectedNormal();
        case 'SelectedHover':
            return style.getSelectedHover();
        case 'Missing':
            return style.getMissing();
    }
};
//------------------------------------------------------------------------------
//                          Events
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.isOver = false;
/**
 * Add event listeners to passed sprite.
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.addEventHandlers = function (sprite) {
    if (sprite == null) return;
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEOVER, this.onMouseOver_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEOUT, this.onMouseOut_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEDOWN, this.ouMouseDown_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.CLICK, this.onMouseClick_, false, this);
    goog.events.listen(sprite.getElement(), goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this);
    if (this.useHandCursor_) this.getSVGManager().setCursorPointer(sprite.getElement());
};
/**
 * Mouse over handler.
 * @param {goog.events.Event} event
 * @param {Boolean} opt_noDispatchEvent
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.onMouseOver_ = function (event, opt_noDispatchEvent) {
//todo : opt_noDispatchEvent - костыль. Используется при програмном изменении состояния, таких как extermal methods. убрать.
    if (this.isHoverable_) {
        if (this.isSelected_) {
            this.currentState_ = this.style_.getSelectedHover();
            this.currentStateString_ = 'SelectedHover';
        } else {
            this.currentState_ = this.style_.getHover();
            this.currentStateString_ = 'Hover';
        }
        this.update();
    }
    this.updateTooltips(event);
    this.isOver = true;

    if (opt_noDispatchEvent != true)
        this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_MOUSE_OVER);
};
/**
 * Mouse out handler.
 * @param {goog.events.Event} event
 * @param {Boolean} opt_noDispatchEvent
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.onMouseOut_ = function (event, opt_noDispatchEvent) {
    //todo : opt_noDispatchEvent - костыль. Используется при програмном изменении состояния, таких как extermal methods. убрать.
    this.isOver = false;

    if (this.isHoverable_) {
        if (this.isSelected_) {
            this.currentState_ = this.style_.getSelectedNormal();
            this.currentStateString_ = 'SelectedNormal';
        } else {
            this.currentState_ = this.style_.getNormal();
            this.currentStateString_ = 'Normal';
        }
        this.update();

    }
    this.hideTooltips();


    if (opt_noDispatchEvent != true)
        this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_MOUSE_OUT);
};
/**
 *
 * @param event
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.onMouseMove_ = function (event) {
    if (this.isOver)
        this.moveTooltips(event);
};
/**
 * Mouse down handler
 * @param {goog.events.Event} event
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.ouMouseDown_ = function (event) {
    var isActionExecuted = false;

    if (this.actionsList_)
        isActionExecuted = this.actionsList_.execute(this);

    if (!isActionExecuted && this.isSelectable_ && !this.isSelected_) {
        this.currentState_ = this.style_.getPushed();
        this.currentStateString_ = 'Pushed';
        this.update();
        this.updateElements();
    }

    this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_MOUSE_DOWN);
};
/**
 * Mouse click handler
 * @param {goog.events.Event} event
 * @param {Boolean} opt_noDispatchEvent
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.onMouseClick_ = function (event, opt_noDispatchEvent) {
//todo : opt_noDispatchEvent - костыль. Используется при програмном изменении состояния, таких как extermal methods. убрать.
    if (this.isSelectable_) {
        if (this.getPlot().isAllowMultipleSelect()) {
            this.isSelected_ = !this.isSelected_;
            if (this.isHoverable_) {
                this.onMouseOver_(event, true);
            } else {
                if (this.isSelected_) {
                    this.currentState_ = this.style_.getSelectedNormal();
                    this.currentStateString_ = 'SelectedNormal';
                } else {
                    this.currentState_ = this.style_.getSelectedHover();
                    this.currentStateString_ = 'SelectedHover';
                }
                this.update();
                this.updateElements();
            }

            if (this.isSelected_) {
                this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_SELECT);
            } else {
                this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_DESELECT);
            }
            this.getPlot().selectPoint(this, true);
        } else {
            if (!this.isSelected_) {
                this.isSelected_ = true;
                this.getPlot().selectPoint(this, true);
                this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_SELECT);

                if (this.isHoverable_) {
                    this.onMouseOver_(event, true);
                } else {
                    this.currentState_ = this.style_.getSelectedNormal();
                    this.currentStateString_ = 'SelectedNormal';
                }
            }
        }
    }

    this.update();
    this.updateElements();

    if (opt_noDispatchEvent) return;
    this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_MOUSE_UP);
    this.dispatchPointEvent(event, anychart.events.PointEvent.POINT_CLICK);

};
/**
 * Dispatch point event
 * @param {goog.events.Event} event
 * @param {anychart.events.PointEvent} type
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.dispatchPointEvent = function (event, type) {
    var x = NaN;
    var y = NaN;
    if (event) {
        x = event.clientX;
        y = event.clientY;
    }

    this.getGlobalSettings().getMainChartView().dispatchEvent(
        new anychart.events.PointEvent(type, this, x, y));
};
//------------------------------------------------------------------------------
//                          Select/Deselect
//------------------------------------------------------------------------------
/**
 * Indicates, is point selected.
 * @protected
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.isSelected_ = false;
/**
 * @param {Boolean} value
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.setSelected = function (value) {
    this.isSelected_ = value;
};
anychart.plots.seriesPlot.data.BasePoint.prototype.deselect = function (opt_redraw) {
    this.isSelected_ = false;
    this.currentState_ = this.style_.getNormal();
    this.currentStateString_ = 'Normal';
    if (opt_redraw != undefined && opt_redraw) {
        this.update();
    }
    this.dispatchPointEvent(null, anychart.events.PointEvent.POINT_DESELECT);
};

anychart.plots.seriesPlot.data.BasePoint.prototype.select = function (opt_redraw) {
    this.isSelected_ = true;
    this.currentState_ = this.style_.getSelectedNormal();
    this.currentStateString_ = 'SelectedNormal';
    if (opt_redraw != undefined && opt_redraw) {
        this.update();
    }
    this.dispatchPointEvent(null, anychart.events.PointEvent.POINT_SELECT);
};
//------------------------------------------------------------------------------
//                          Hover/Normal
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BasePoint.prototype.setHover = function () {
    this.canDrawTooltips_ = false;
    this.onMouseOver_(null, true);
    this.canDrawTooltips_ = true;
};
anychart.plots.seriesPlot.data.BasePoint.prototype.setNormal = function () {
    this.onMouseOut_(null, true);
};
//------------------------------------------------------------------------------
//                          Elements deserialization
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeLabelElement_ = function (data, stylesList) {
    if (this.label_) {
        if (anychart.utils.deserialization.hasProp(data, 'label')) {
            if (this.label_.canDeserializeForPoint(
                anychart.utils.deserialization.getProp(data, 'label'))) {
                this.label_ = this.label_.copy();
                this.label_.deserializePointSettings(
                    anychart.utils.deserialization.getProp(data, 'label'),
                    stylesList);
            }
        }
        this.checkElementPosition(this.label_);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    if (this.marker_) {
        if (anychart.utils.deserialization.hasProp(data, 'marker')) {
            if (this.marker_.canDeserializeForPoint(
                anychart.utils.deserialization.getProp(data, 'marker'))) {
                this.marker_ = this.marker_.copy();
                this.marker_.deserializePointSettings(
                    anychart.utils.deserialization.getProp(data, 'marker'),
                    stylesList);
            }
        }
        this.checkElementPosition(this.marker_);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeTooltipElement_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    if (this.tooltip_ && this.tooltip_.canDeserializeForPoint(des.getProp(data, 'tooltip'))) {
        this.tooltip_ = this.tooltip_.copy();
        this.tooltip_.deserializePointSettings(
            des.getProp(data, 'tooltip'),
            stylesList);
    }
    this.checkElementPosition(this.tooltip_);
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeExtraLabelsElements_ = function (data, stylesList) {
    goog.base(this, 'deserializeExtraLabelsElements_', data, stylesList);
    if (this.extraLabels_)
        for (var i = 0; i < this.extraLabels_.length; i++)
            this.checkElementPosition(this.extraLabels_[i]);
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeExtraMarkersElements_ = function (data, stylesList) {
    goog.base(this, 'deserializeExtraMarkersElements_', data, stylesList);
    if (this.extraMarkers_)
        for (var i = 0; i < this.extraMarkers_.length; i++)
            this.checkElementPosition(this.extraMarkers_[i]);

};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.deserializeExtraTooltipsElements_ = function (data, stylesList) {
    goog.base(this, 'deserializeExtraTooltipsElements_', data, stylesList);
    if (this.extraTooltips_)
        for (var i = 0; i < this.extraTooltips_.length; i++)
            this.checkElementPosition(this.extraTooltips_[i]);
};

//------------------------------------------------------------------------------
//                     Elements initialization
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.labelSprite_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.markerSprite_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.tooltipSprite_ = null;
/**
 * @private
 * @type {Array.<anychart.elements.ElementsSprite>}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.extraLabelsSprites_ = null;
/**
 * @private
 * @type {Array.<anychart.elements.ElementsSprite>}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.extraMarkersSprites_ = null;
/**
 * @private
 * @type {Array.<anychart.elements.ElementsSprite>}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.extraTooltipsSprites_ = null;
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.initializeElements = function () {
    //labels
    this.labelSprite_ = this.initElement(this.label_, true);
    this.initExtraLabelsVisual();
    //markers
    this.markerSprite_ = this.initElement(this.marker_, true);
    this.initExtraMarkersVisual();
    //tooltips
    this.tooltipSprite_ = this.initElement(this.tooltip_, false);
    this.initExtraTooltipsVisual();
};
//------------------------------------------------------------------------------
//                         Extra labels initialization
//------------------------------------------------------------------------------
/** @protected */
anychart.plots.seriesPlot.data.BasePoint.prototype.initExtraLabelsVisual = function () {
    if (!this.extraLabels_) return;

    this.extraLabelsSprites_ = [];
    var labelsCount = this.extraLabels_.length;

    for (var i = 0; i < labelsCount; i++)
        this.extraLabelsSprites_[i] = this.initElement(
            this.extraLabels_[i],
            true);
};
//------------------------------------------------------------------------------
//                          Extra markers initialization
//------------------------------------------------------------------------------
/** @protected */
anychart.plots.seriesPlot.data.BasePoint.prototype.initExtraMarkersVisual = function () {
    if (!this.extraMarkers_) return;

    this.extraMarkersSprites_ = [];
    var markerCount = this.extraMarkers_.length;

    for (var i = 0; i < markerCount; i++)
        this.extraMarkersSprites_[i] = this.initElement(
            this.extraMarkers_[i],
            true);
};
//------------------------------------------------------------------------------
//                   Extra tooltips initialization
//------------------------------------------------------------------------------
/** @protected */
anychart.plots.seriesPlot.data.BasePoint.prototype.initExtraTooltipsVisual = function () {
    if (!this.extraTooltips_) return;

    this.extraTooltipsSprites_ = [];
    var tooltipCount = this.extraTooltips_.length;

    for (var i = 0; i < tooltipCount; i++)
        this.extraTooltipsSprites_[i] = this.initElement(
            this.extraTooltips_[i],
            false);
};
/**
 * Initialize passed element.
 * @param {anychart.elements.BaseElement} element
 * @param {Boolean} needAddEvents
 * @return {anychart.elements.ElementsSprite}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.initElement = function (element, needAddEvents) {
    if (element) {
        var elementSprite = element.getStyle().initialize(this, element);
        //if element disabled, sprite == null
        if (elementSprite && needAddEvents && !this.missing_)
            this.addEventHandlers(elementSprite);
        return elementSprite;
    }
    return null;
};
//------------------------------------------------------------------------------
//                           Update elements
//------------------------------------------------------------------------------\
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateElements = function () {
    //labels
    this.updateElement(this.label_, this.labelSprite_);
    this.updateExtraLabels();
    //markers
    this.updateElement(this.marker_, this.markerSprite_);
    this.updateExtraMarkers();
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateTooltips = function (event) {
    if (!this.canDrawTooltips_) return;
    this.updateTooltip(event, this.tooltip_, this.tooltipSprite_);
    this.updateExtraTooltips(event);
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.hideTooltips = function () {
    this.hideElement(this.tooltipSprite_);
    this.hideExtraTooltips();
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.moveTooltips = function (event) {
    this.moveTooltip(event, this.tooltip_, this.tooltipSprite_);
    this.moveExtraTooltips(event);
};
//------------------------------------------------------------------------------
//                        Update extra labels
//------------------------------------------------------------------------------
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateExtraLabels = function () {
    if (!this.extraLabelsSprites_) return;

    var count = this.extraLabelsSprites_.length;

    for (var i = 0; i < count; i++)
        this.updateElement(this.extraLabels_[i], this.extraLabelsSprites_[i]);

};
//------------------------------------------------------------------------------
//                      Update extra markers
//------------------------------------------------------------------------------
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateExtraMarkers = function () {
    if (!this.extraMarkersSprites_) return;

    var count = this.extraMarkersSprites_.length;

    for (var i = 0; i < count; i++)
        this.updateElement(this.extraMarkers_[i], this.extraMarkersSprites_[i]);
};
//------------------------------------------------------------------------------
//                       Update tooltips
//------------------------------------------------------------------------------
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateExtraTooltips = function (event) {
    if (!this.extraTooltipsSprites_) return;

    var count = this.extraTooltipsSprites_.length;

    for (var i = 0; i < count; i++) {
        this.updateElement(
            this.extraTooltips_[i],
            this.extraTooltipsSprites_[i]);

        this.moveTooltip(
            event,
            this.extraTooltips_[i],
            this.extraTooltipsSprites_[i]);
    }
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.hideExtraTooltips = function () {
    if (this.extraTooltipsSprites_) {
        var tooltipsCount = this.extraTooltipsSprites_.length;
        for (var i = 0; i < tooltipsCount; i++)
            this.hideElement(this.extraTooltipsSprites_[i]);
    }
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.moveExtraTooltips = function (event) {
    if (this.extraTooltipsSprites_) {
        var tooltipsCount = this.extraTooltipsSprites_.length;
        for (var i = 0; i < tooltipsCount; i++)
            this.moveTooltip(
                event,
                this.extraTooltips_[i],
                this.extraTooltipsSprites_[i]);
    }
};
/**
 * @protected
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.elements.ElementsSprite} elementSprite
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.updateElement = function (element, elementSprite) {
    if (elementSprite)
        elementSprite.update(this.getCurrentStyleState(element.getStyle()));
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.updateTooltip = function (event, tooltip, tooltipSprite) {
    this.updateElement(tooltip, tooltipSprite);
    this.moveTooltip(event, tooltip, tooltipSprite);
};
/**
 * @protected
 * @param {goog.events.Event} event
 * @param {anychart.elements.tooltip.BaseTooltipElement} tooltip
 * @param {anychart.elements.ElementsSprite} tooltipSprite
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.moveTooltip = function (event, tooltip, tooltipSprite) {
    if (tooltipSprite)
        tooltip.execMoveTooltip(
            event,
            this,
            this.getCurrentStyleState(tooltip.getStyle()),
            tooltipSprite);
};
/**
 * @protected
 * @param {anychart.elements.ElementsSprite} elementSprite
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.hideElement = function (elementSprite) {
    if (elementSprite) {
        elementSprite.setPosition(0, 0);
        elementSprite.setVisible(false);
    }
};
//------------------------------------------------------------------------------
//                          Elements resize.
//------------------------------------------------------------------------------
/**
 * Resize point elements
 * @protected
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.resizeElements = function () {
    this.resizeLabel(this.label_, this.labelSprite_);

    this.resizeExtraLabels();

    if (this.markerSprite_)
        this.resizeElement(this.marker_, this.markerSprite_);

    this.resizeExtraMarkers();

    this.hideTooltips();
};
/**
 * Resize point label, override in axes plot.
 * @protected
 * @param {anychart.elements.label.BaseLabelElement}
    * @param {anychart.elements.ElementsSprite}
    */
anychart.plots.seriesPlot.data.BasePoint.prototype.resizeLabel = function (label, labelSprite) {
    if (labelSprite) this.resizeElement(label, labelSprite);
};
/**
 * Resize point extra labels elements
 * @protected
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.resizeExtraLabels = function () {
    if (!this.extraLabelsSprites_) return;

    var count = this.extraLabelsSprites_.length;
    for (var i = 0; i < count; i++)
        this.resizeLabel(
            this.extraLabels_[i],
            this.extraLabelsSprites_[i]);
};
/**
 * Resize point extra markers elements
 * @protected
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.resizeExtraMarkers = function () {
    if (!this.extraMarkersSprites_) return;

    var count = this.extraMarkersSprites_.length;
    for (var i = 0; i < count; i++)
        this.resizeElement(
            this.extraMarkers_[i],
            this.extraMarkersSprites_[i]);
};
/**
 * Resize passed element.
 * @protected
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.elements.ElementsSprite} sprite
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.resizeElement = function (element, sprite) {
    if (element && sprite)
        element.resize(
            this,
            this.getCurrentStyleState(element.getStyle()),
            sprite);
};
//------------------------------------------------------------------------------
//                        Elements position
//------------------------------------------------------------------------------
/**
 * Check element position.
 * @protected
 * @param {anychart.elements.BaseElement} element Point element.
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.checkElementPosition = function (element) {
};
/**
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.styles.StyleState} state
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getElementPosition = function (element, state, bounds) {
    if (!element || !state || !bounds || (this.isMissing_ && !this.getPlot().needIgnoreMissingPoints())) return null;
    var res = new anychart.utils.geom.Point();
    this.setAnchorPoint(res, state.getAnchor(), bounds, state.getPadding());
    if (state.getAnchor() != anychart.layout.Anchor.X_AXIS) {
        this.setElementHorizontalAlign(res, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
        this.setElementVerticalAlign(res, state.getAnchor(), state.getHAlign(), state.getVAlign(), bounds, state.getPadding());
    }
    return res;
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.setAnchorPoint = function (point, anchor, bounds, padding) {
    if (this.bounds_ == null) return;
    switch (anchor) {
        default:
        case anychart.layout.Anchor.CENTER:
            point.x = this.bounds_.x + this.bounds_.width / 2;
            point.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_BOTTOM:
            point.x = this.bounds_.x + this.bounds_.width / 2;
            point.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.CENTER_LEFT:
            point.x = this.bounds_.x;
            point.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_RIGHT:
            point.x = this.bounds_.x + this.bounds_.width;
            point.y = this.bounds_.y + this.bounds_.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_TOP:
            point.x = this.bounds_.x + this.bounds_.width / 2;
            point.y = this.bounds_.y;
            break;
        case anychart.layout.Anchor.LEFT_BOTTOM:
            point.x = this.bounds_.x;
            point.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.LEFT_TOP:
            point.x = this.bounds_.x;
            point.y = this.bounds_.y;
            break;
        case anychart.layout.Anchor.RIGHT_BOTTOM:
            point.x = this.bounds_.x + this.bounds_.width;
            point.y = this.bounds_.y + this.bounds_.height;
            break;
        case anychart.layout.Anchor.RIGHT_TOP:
            point.x = this.bounds_.x + this.bounds_.width;
            point.y = this.bounds_.y;
            break;
    }
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.setElementHorizontalAlign = function (point, anchor, hAlign, vAlign, bounds, padding) {
    switch (hAlign) {
        case anychart.layout.HorizontalAlign.LEFT:
            point.x -= bounds.width + padding;
            break;
        case anychart.layout.HorizontalAlign.RIGHT:
            point.x += padding;
            break;
        default:
        case anychart.layout.HorizontalAlign.CENTER:
            point.x -= bounds.width / 2;
            break;
    }
};
/**@protected*/
anychart.plots.seriesPlot.data.BasePoint.prototype.setElementVerticalAlign = function (point, anchor, hAlign, vAlign, bounds, padding) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP:
            point.y -= bounds.height + padding;
            break;
        case anychart.layout.VerticalAlign.BOTTOM:
            point.y += padding;
            break;
        default:
        case anychart.layout.VerticalAlign.CENTER:
            point.y -= bounds.height / 2;
            break;
    }
};
//------------------------------------------------------------------------------
//                    Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.setDefaultTokenValues = function () {
    this.tokensHash_.add('%Index', 0);
    this.tokensHash_.add('%Name', '');
    this.tokensHash_.add('%Color', '0xFFFFFF');
};
/**@inheritDoc*/
anychart.plots.seriesPlot.data.BasePoint.prototype.calculatePointTokenValues = function () {
    this.tokensHash_.add('%Index', this.index_);
    this.tokensHash_.add('%Name', this.name_);
    if (this.color_)
        this.tokensHash_.add(
            '%Color',
            this.tokensHash_.formatColorValue(this.color_.getColor()));

};
/**@inheritDoc*/
anychart.plots.seriesPlot.data.BasePoint.prototype.recalculatePointTokenValues = function () {
};
//------------------------------------------------------------------------------
//                    Formatting token value getters
//------------------------------------------------------------------------------
/**
 * @param {String} token
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getTokenValue = function (token) {
    //point custom attributes
    if (this.hasCustomAttribute(token))
        return this.customAttributes_[token];

    //series token or series custom attributes
    if (this.tokensHash_.isSeriesToken(token, this.series_))
        return this.series_.getSeriesTokenValue(token);

    //data plot token or data plot custom attributes
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenValue(token);

    //threshold item token or threshold item custom attributes
    if (this.tokensHash_.isThresholdItemToken(token, this.thresholdItem_)) {
        return this.thresholdItem_.getFormattedTokenValue(this, token);
    }

    //threshold token
    if (this.tokensHash_.isThresholdToken(token, this.threshold_))
        return this.threshold_.getTokenValue(token);

    return this.getPointTokenValue(token);
};
/**
 * @param token
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getPointTokenValue = function (token) {
    return this.tokensHash_.get(token);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.getTokenType = function (token) {
    if (this.hasCustomAttribute(token)) return anychart.formatting.TextFormatTokenType.TEXT;
    if (this.tokensHash_.isSeriesToken(token, this.series_)) return this.series_.getTokenType(token);
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot())) return this.getPlot().getTokenType(token);

    if (this.tokensHash_.isThresholdToken(token, this.threshold_))
        return this.threshold_.getTokenType(token);

    switch (token) {
        case '%Index':
            return anychart.formatting.TextFormatTokenType.NUMBER;
        case '%Name':
        case '%Color':
        case '%text':
            return anychart.formatting.TextFormatTokenType.TEXT;
        default:
            return anychart.formatting.TextFormatTokenType.UNKNOWN;
    }
};
//------------------------------------------------------------------------------
//                    Serialization
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.canSerializeForEvent = function () {
    return true;
};
/**
 * @param {Object} opt_target
 * @return {Object}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.serialize = function (opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['SeriesPointCount'] = this.getSeries().getNumPoints();
    opt_target['Index'] = this.index_;
    return opt_target;
};
/**
 * @return {Object}
 */
anychart.plots.seriesPlot.data.BasePoint.prototype.serializeForEvent = function () {
    var res = this.serialize();
    res['Series'] = this.getSeries().serialize();
    return res;
};
//------------------------------------------------------------------------------
//
//                          BaseSeries class
//
//------------------------------------------------------------------------------
/**
 * Class represent base series for all data series.
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseDynamicElementsContainer}
 * @implements {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.plots.seriesPlot.data.BaseSeries = function () {
    anychart.plots.seriesPlot.data.BaseDynamicElementsContainer.call(this);
    this.points_ = [];
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.BaseSeries, anychart.plots.seriesPlot.data.BaseDynamicElementsContainer);

/**
 * Array with contains series points.
 * @protected
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.points_ = null;

/**
 * Return points array.
 * @public
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.getPoints = function () {
    return this.points_;
};

/**
 * Get total count of points
 * @return {number}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.getNumPoints = function () {
    return this.points_ ? this.points_.length : 0;
};

/**
 * Get data point at specific index
 * @param {number} index point index
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.getPointAt = function (index) {
    return this.points_[index];
};

/**
 * Create series point.
 * @public
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.createPoint = function () {
    goog.abstractMethod();
};

/**
 * Add point to points array.
 * <p>
 *  Note: If at passed index already exist some point, it will be override.
 * </p>
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Base series boint.
 * @param {int} index Index to add point
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.addPoint = function (point, index) {
    this.points_[index] = point;
};


anychart.plots.seriesPlot.data.BaseSeries.prototype.type_ = NaN;
anychart.plots.seriesPlot.data.BaseSeries.prototype.getType = function () {
    return this.type_;
};
anychart.plots.seriesPlot.data.BaseSeries.prototype.setType = function (value) {
    this.type_ = value;
};

anychart.plots.seriesPlot.data.BaseSeries.prototype.isSortable = function () {
    return false;
};

//------------------------------------------------------------------------------
//                          Elements
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.deserializeLabelElement_ = function (data, stylesList) {
    if (this.label_ && this.label_.canDeserializeForSeries(anychart.utils.deserialization.getProp(data, 'label'))) {
        this.label_ = this.label_.copy();
        this.label_.deserializeSeriesSettings(anychart.utils.deserialization.getProp(data, 'label'), stylesList);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    if (this.marker_ && this.marker_.canDeserializeForSeries(anychart.utils.deserialization.getProp(data, 'marker'))) {
        this.marker_ = this.marker_.copy();
        this.marker_.deserializeSeriesSettings(anychart.utils.deserialization.getProp(data, 'marker'), stylesList);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.deserializeTooltipElement_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    if (this.tooltip_ && this.tooltip_.canDeserializeForSeries(des.getProp(data, 'tooltip'))) {
        this.tooltip_ = this.tooltip_.copy();
        this.tooltip_.deserializeSeriesSettings(des.getProp(data, 'tooltip'), stylesList);
    }
};

//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------

anychart.plots.seriesPlot.data.BaseSeries.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
};
//------------------------------------------------------------------------------
//                          Hover/Normal
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BaseSeries.prototype.setHover = function () {
    var pointCount = this.getNumPoints();
    for (var i = 0; i < pointCount; i++) this.getPointAt(i).setHover();
};
anychart.plots.seriesPlot.data.BaseSeries.prototype.setNormal = function () {
    var pointCount = this.getNumPoints();
    for (var i = 0; i < pointCount; i++) this.getPointAt(i).setNormal();
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.setDefaultTokenValues = function () {
    this.tokensHash_.add('%SeriesPointCount', 0);
    this.tokensHash_.add('%Name', '');
    this.tokensHash_.add('%SeriesName', '');
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.getTokenType = function (token) {
    if (this.hasCustomAttribute(token)) return anychart.formatting.TextFormatTokenType.TEXT;
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot())) return this.getPlot().getTokenType(token);
    switch (token) {
        case '%Name':
        case '%SeriesName':
        default:
            return anychart.formatting.TextFormatTokenType.TEXT;
        case '%SeriesPointCount' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
};
//------------------------------------------------------------------------------
//                          Formatting value calculation
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BaseSeries.prototype.checkPoint = function (dataPoint) {
    dataPoint.calculatePointTokenValues();
};

anychart.plots.seriesPlot.data.BaseSeries.prototype.checkPlot = function (plot) {
    plot.getTokensHash().increment('%DataPlotPointCount', this.points_.length);
    this.calculateSeriesTokenValues();
};

anychart.plots.seriesPlot.data.BaseSeries.prototype.calculateSeriesTokenValues = function () {
    this.tokensHash_.add('%SeriesName', this.name_);
    this.tokensHash_.add('%Name', this.name_);
    this.tokensHash_.add('%SeriesPointCount', this.points_.length);
};
anychart.plots.seriesPlot.data.BaseSeries.prototype.recalculateSeriesTokenValues = function () {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Formatting value getters
//------------------------------------------------------------------------------
/**
 * Return series token value by token name.
 * @param {String} token Token name.
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.getSeriesTokenValue = function (token) {
    if (this.customAttributes_ && this.customAttributes_[token])
        return this.customAttributes_[token];

    return this.tokensHash_.get(token);
};

anychart.plots.seriesPlot.data.BaseSeries.prototype.getTokenValue = function (token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenValue(token);

    return this.getSeriesTokenValue(token);
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * Indicates, need call on before draw method for this series.
 * @protected
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.needCallOnBeforeDraw = function () {
    return false;
};
anychart.plots.seriesPlot.data.BaseSeries.prototype.onBeforeDraw = function () {
};
anychart.plots.seriesPlot.data.BaseSeries.prototype.onBeforeResize = function () {
};
/**
 * Add points to drawing or not.
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.BaseSeries.prototype.addPointsToDrawing = function () {
    return true;
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.data.BaseSeries.prototype.serialize = function (opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['Points'] = [];
    var pointCount = this.points_.length;
    for (var i = 0; i < pointCount; i++)
        if (this.points_[i])
            opt_target['Points'].push(this.points_[i].serialize());
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          GlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * Class represent global series settings.
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseDataElement}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings = function () {
    //anychart.plots.seriesPlot.data.BaseDataElement.call(this);
    this.globalSettings_ = this;
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.GlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseDataElement);
//------------------------------------------------------------------------------
//                         Common properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.IPlot}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.plot_ = null;
/**
 * @param {anychart.plots.IPlot} value
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.setPlot = function (value) {
    this.plot_ = value;
};
/**
 * @return {anychart.plots.IPlot}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.getPlot = function () {
    return this.plot_;
};
//------------------------------------------------------------------------------
//                           MainChartView.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.mainChartView_ = null;
/**
 * @param {anychart.chartView.ChartView} value
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.setMainChartView = function (value) {
    this.mainChartView_ = value;
};
/**
 * @return {anychart.chartView.ChartView}
 */
anychart.plots.seriesPlot.data.BaseDataElement.prototype.getMainChartView = function () {
    return this.mainChartView_;
};
//------------------------------------------------------------------------------
//                         Legend
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.hasIconDrawer = function () {
    return false;
};
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.iconCanDrawMarker = function () {
    return false;
};
/**
 * @param {int} seriesType
 * @param {anychart.controls.legend.ILegendItem} item
 * @param {anychart.svg.SVGSprite} sprite
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.drawIcon = function (seriesType, item, sprite, bounds) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                         Palettes
//------------------------------------------------------------------------------
/**
 * Indicates, need apply palette to series.
 * @protected
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.applyDataPaletteToSeries_ = false;

/**
 * Indicates, need apply palette to series.
 * @public
 * @type {boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.isApplyDataPaletteToSeries = function () {
    return this.applyDataPaletteToSeries_;
};

//------------------------------------------------------------------------------
//                         Padding
//------------------------------------------------------------------------------
//TODO MOVE TO ANOTHER CLASS
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.groupPadding_ = .1;
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.getGroupPadding = function () {
    return this.groupPadding_;
};
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.pointPadding_ = .1;
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.getPointPadding = function () {
    return this.pointPadding_;
};
//------------------------------------------------------------------------------
//                         Styles
//------------------------------------------------------------------------------
/**
 * Return series style node name.
 * @public
 * @return {string}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.getStyleNodeName = function () {
    goog.abstractMethod();
};
/**
 * @protected
 * @param {Object} config
 * @return {anychart.styles.Style}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createStyle = function (config) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize global series settings.
 * @public
 * @override
 * @param {object} config JSON object with global series settings.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(config, 'apply_palettes_to'))
        this.applyDataPaletteToSeries_ = des.getLString(des.getProp(config, 'apply_palettes_to')) == 'points';

    if (des.hasProp(config, 'group_padding'))
        this.groupPadding_ = des.getNumProp(config, 'group_padding');

    if (des.hasProp(config, 'point_padding'))
        this.pointPadding_ = des.getNumProp(config, 'point_padding');

    if (this.canSortPoints() || this.canPreSortPoints()) {
        if (des.hasProp(config, 'sort')) {
            switch (des.getEnumProp(config, 'sort')) {
                case "asc":
                    this.isSortingEnabled = true;
                    this.sortDescending = false;
                    break;
                case "desc":
                    this.isSortingEnabled = true;
                    this.sortDescending = true;
                    break;
                default:
                case "none":
                    this.isSortingEnabled = false;
                    break;
            }
        }
    }
};
/**
 * Deserialize style for series
 * @param {Object} settingsNode
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeStyle = function (settingsNode, stylesList) {
    var styleNodeName = this.getStyleNodeName();
    if (!styleNodeName) return;
    var des = anychart.utils.deserialization;

    //getting parent node name
    var parentStyleName;
    var styleNode;
    if (des.hasProp(settingsNode, 'style')) parentStyleName = des.getLStringProp(settingsNode, 'style');
    if (des.hasProp(settingsNode, styleNodeName)) {
        styleNode = des.getProp(settingsNode, styleNodeName);
        if (des.hasProp(styleNode, 'parent'))  parentStyleName = des.getProp(styleNode, 'parent');
    }

    //merge global style
    var styleConfig = stylesList.getStyleByMerge(styleNodeName, styleNode, parentStyleName);
    styleConfig.name = parentStyleName;
    this.style_ = this.createStyle(styleConfig);
    this.style_.deserialize(styleConfig);
};

anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeInteractivity = function (data) {
    var des = anychart.utils.deserialization;
    goog.base(this, 'deserializeInteractivity', data);
    if (!des.hasProp(data, 'interactivity')) return;
    goog.base(this, 'deserializeInteractivity', des.getProp(data, 'interactivity')); //old config support
};
//------------------------------------------------------------------------------
//                          Series
//------------------------------------------------------------------------------
/**
 * Create series.
 * @public
 * @param {string} type Series type.
 * @return {object}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createSeries = goog.abstractMethod;
/**
 * Return series settings node name.
 * @public
 * @return {string}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.getSettingsNodeName = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Sorting.
//------------------------------------------------------------------------------
/**
 * Define sorting method.
 * @protected
 * @type {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.sortDescending = false;
/**
 * Define need or not sort points (can be set by user ).
 * @protected
 * @type {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.isSortingEnabled = false;
/**
 * Define need or not sort points.
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.needSortPoints = function () {
    return this.isSortingEnabled && this.canSortPoints();
};
/**
 * Define can point be sort for this series type.
 * @protected
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.canSortPoints = function () {
    return false;
};
/**
 * Define can point be presort for this series type.
 * @protected
 * @return {Boolean}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.canPreSortPoints = function () {
    return false;
};
/**
 * Sort deserialized points.
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} points
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.sortPoints = function (points) {
    if (!this.canPreSortPoints() && this.needSortPoints())
        this.execSortPoints(points, this.sortDescending);
};
/**
 * Contain sorting algorithm.
 * @protected
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} points
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.execSortPoints = function (points, sortDescending) {

};
/**
 * Sort non deserialized points.
 * @param {Array.<Object>} points
 * @return {Array.<Object>} Sorted points.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.sortJSONPoints = function (points) {
    if (this.canPreSortPoints() && this.isSortingEnabled)
        return this.execSortJSONPoints(points, this.sortDescending);
    return points;
};
/**
 * Contain sorting algorithm.
 * @param {Array.<Object>} points
 * @param {Boolean} sortDescending
 * @return {Array.<Object>} Sorted points.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.execSortJSONPoints = function (points, sortDescending) {
    return null;
};
//------------------------------------------------------------------------------
//                         Elements creation
//------------------------------------------------------------------------------
/**
 * Create label element.
 * @public
 * @return {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createLabelElement = function () {
    return new anychart.plots.seriesPlot.elements.LabelElement();
};
/**
 * Create marker element.
 * @public
 * @return {anychart.plots.seriesPlot.elements.MarkerElement}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createMarkerElement = function () {
    return new anychart.plots.seriesPlot.elements.MarkerElement();
};
/**
 * Create tooltip element.
 * @public
 * @return {anychart.plots.seriesPlot.elements.TooltipElement}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createTooltipElement = function () {
    return new anychart.plots.seriesPlot.elements.TooltipElement();
};
//------------------------------------------------------------------------------
//                         Elements deserialization
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeLabelElement_ = function (data, stylesList) {
    this.label_ = this.createLabelElement();
    if (!this.label_) return;
    if (data && anychart.utils.deserialization.hasProp(data, 'label_settings'))
        this.label_.deserializeGlobalSettings(
            anychart.utils.deserialization.getProp(data, 'label_settings'),
            stylesList);
    else this.label_.setStyle(new anychart.styles.Style(
        this.label_.getStyleStateClass()));
};
/**@inheritDoc*/
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    this.marker_ = this.createMarkerElement();
    if (!this.marker_) return;
    if (data && anychart.utils.deserialization.hasProp(data, 'marker_settings'))
        this.marker_.deserializeGlobalSettings(
            anychart.utils.deserialization.getProp(data, 'marker_settings'),
            stylesList);
    else this.marker_.setStyle(new anychart.styles.Style(
        this.marker_.getStyleStateClass()));
};
/**@inheritDoc*/
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.deserializeTooltipElement_ = function (data, stylesList) {
    this.tooltip_ = this.createTooltipElement();
    if (!this.tooltip_) return;
    var des = anychart.utils.deserialization;
    if (data && des.hasProp(data, 'tooltip_settings'))
        this.tooltip_.deserializeGlobalSettings(
            des.getProp(data, 'tooltip_settings'),
            stylesList);
    else
        this.tooltip_.setStyle(new anychart.styles.Style(this.tooltip_.getStyleStateClass()));
};
//------------------------------------------------------------------------------
//                          Interpolation.
//------------------------------------------------------------------------------
/**
 * Indicate, need add missing points to drawing points.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.canDrawMissingPoints = function () {
    return true;
};
/**
 * @protected
 * @return {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.createMissingInterpolator = function () {
    return new anychart.interpolation.SimpleMissingInterpolator();
};
/**
 * Initialize missing interpolators.
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.initializeMissingInterpolators = goog.abstractMethod;
/**
 * @protected
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.checkMissingPointDuringDeserialize = goog.abstractMethod;
/**
 * @param {anychart.plots.seriesPlot.data.BaseSeries} series
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.interpolateMissingPoints = goog.abstractMethod;
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.missingInitializeAfterInterpolate = function (point) {
    point.initializeAfterInterpolate();
};
//------------------------------------------------------------------------------
//
//                  BaseSingleValueGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings = function () {
    anychart.plots.seriesPlot.data.GlobalSeriesSettings.apply(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.GlobalSeriesSettings);
//------------------------------------------------------------------------------
//                              Missing.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.prototype.yInterpolator = null;
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.prototype.initializeMissingInterpolators = function () {
    this.yInterpolator = this.createMissingInterpolator();
    this.yInterpolator.initialize(
        anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.getY,
        anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.setY
    );
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.prototype.checkMissingPointDuringDeserialize = function (point) {
    this.yInterpolator.checkDuringDeserialize(point, point.getIndex());
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.GlobalSeriesSettings.prototype.interpolateMissingPoints = function (series) {
    var points = this.yInterpolator.interpolate(series.getPoints());
    var pointsCount = points.length;

    for (var i = 0; i < pointsCount; i++)
        this.missingInitializeAfterInterpolate(points[i]);

    this.yInterpolator.clear();
};