/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.categorization.BaseCategoryInfo}</li>
 * <ul>
 */

goog.provide("anychart.plots.seriesPlot.categorization");

/**
 * @constructor
 * @param {anychart.plots.seriesPlot.SeriesPlot} plot
 */
anychart.plots.seriesPlot.categorization.BaseCategoryInfo = function(plot) {
    this.plot_ = plot;
    this.points_ = [];
    this.initFormatting();
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.name_ = null;
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.setName = function(value) {
    this.name_ = value;
};
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getName = function() {
    return this.name_;
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.index_ = NaN;
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.setIndex = function(value) {
    this.index_ = value;
};
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getIndex = function() {
    return this.index_;
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.color_ = 0;

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.plot_ = null;
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getPlot = function() {
    return this.plot_;
};
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.points_ = null;

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getPoints = function() {
    return this.points_;
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.checkPoint = function(point) {
    point.checkCategory(this);
    this.calculateCategoryTokenValues();
    this.points_.push(point);
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.initColor = function() {
    this.color = this.points_.length > 0 ? this.points_[0].color : 0;
};
/**
 * Category tokens hash.
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.tokensHash_ = null;
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getTokensHash = function() {
    return this.tokensHash_;
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.initFormatting = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.setDefaultTokenValues = function() {
    this.tokensHash_.add('%CategoryName', '');
    this.tokensHash_.add('%Name', '');
    this.tokensHash_.add('%CategoryIndex', '');
    this.tokensHash_.add('%CategoryPointCount', 0);
    this.tokensHash_.add('%CategoryYSum', 0);
    this.tokensHash_.add('%CategoryYMax', 0);
    this.tokensHash_.add('%CategoryYMin', 0);
    this.tokensHash_.add('%CategoryYRangeMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%CategoryYRangeMin', Number.MAX_VALUE);
    this.tokensHash_.add('%CategoryYRangeSum', 0);
    this.tokensHash_.add('%CategoryYPercentOfTotal', 0);
    this.tokensHash_.add('%CategoryYAverage', 0);
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getTokenType = function(token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenType(token);
    switch (token) {
        case '%CategoryPointCount':
        case '%CategoryYSum':
        case '%CategoryYMax':
        case '%CategoryYMin':
        case '%CategoryYRangeMax':
        case '%CategoryYRangeMin':
        case '%CategoryYRangeSum':
        case '%CategoryYPercentOfTotal':
        case '%CategoryYAverage':
            return anychart.formatting.TextFormatTokenType.NUMBER;
        default:
        case '%CategoryName':
        case '%Name':
            return anychart.formatting.TextFormatTokenType.TEXT;

    }
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.calculateCategoryTokenValues = function() {
    this.tokensHash_.add('%CategoryName', this.name_);
    this.tokensHash_.add('%Name', this.name_);
    this.tokensHash_.add('%CategoryIndex', this.index_);
};

anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.getTokenValue = function(token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokensHash().get(token);

    if (!this.tokensHash_.has('%CategoryYPercentOfTotal'))
        this.tokensHash_.add(
                '%CategoryYPercentOfTotal',
                this.tokensHash_.get('%CategoryYSum') /
                        this.getPlot().getTokenValue('%DataPlotYSum') * 100);

    if (!this.tokensHash_.has('%CategoryYAverage'))
        this.tokensHash_.add(
                '%CategoryYAverage',
                this.tokensHash_.get('%CategoryYSum') /
                        this.tokensHash_.get('%CategoryPointCount'));

    return this.tokensHash_.get(token);
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.serialize = function(opt_target) {
    if (opt_target == null || opt_target == undefined) opt_target = {};
    opt_target['Name'] = this.name_;
    opt_target['XSum'] = this.tokensHash_.get('%CategoryXSum');
    opt_target['YSum'] = this.tokensHash_.get('%CategoryYSum');
    opt_target['BubbleSizeSum'] = this.tokensHash_.get('%CategoryBubbleSizeSum');
    opt_target['YRangeMax'] = this.tokensHash_.get('%CategoryRangeMax');
    opt_target['YRangeMin'] = this.tokensHash_.get('%CategoryRangeMin');
    opt_target['YRangeSum'] = this.tokensHash_.get('%CategoryRangeSum');
    opt_target['YPercentOfTotal'] = this.tokensHash_.get('%CategoryYPercentOfTotal');
    opt_target['YMedian'] = this.tokensHash_.get('%CategoryYMedian');
    opt_target['YMode'] = this.tokensHash_.get('%CategoryYMode');
    opt_target['YAverage'] = this.tokensHash_.get('%CategoryYAverage');
    return opt_target;
};
//------------------------------------------------------------------------------
//                          Hover/Normal
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.setHover = function() {
    var pointCount = this.points_.length;
    for (var i = 0; i < pointCount; i++) this.points_[i].setHover();
};
anychart.plots.seriesPlot.categorization.BaseCategoryInfo.prototype.setNormal = function() {
    var pointCount = this.points_.length;
    for (var i = 0; i < pointCount; i++) this.points_[i].setNormal();
};