/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}</li>
 * <ul>
 */

goog.provide('anychart.plots.seriesPlot.templates');

goog.require("anychart.utils");
goog.require("anychart.templates");
goog.require("anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE");

//-----------------------------------------------------------------------------------
//
//                          SeriesPlotTemplatesMerger class
//
//-----------------------------------------------------------------------------------

/**
 * Represents base type for all series plot template mergers. Used in Chart deserialization by its
 * descendants.
 *
 * @public
 * @constructor
 * @extends {anychart.templates.BaseTemplatesMerger}
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger = function() {
    anychart.templates.BaseTemplatesMerger.apply(this);
};

goog.inherits(
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger,
    anychart.templates.BaseTemplatesMerger
);

/**
 * Returns chart type-dependent chart node name.
 *
 * @return {string}
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.getBaseNodeName = function() {
    return "chart";
};

/**
 * Returns plot default template.
 *
 * @public
 * @override
 * @return {object}
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.getPlotDefaultTemplate = function() {
    return this.mergeTemplates(
        goog.base(this, 'getPlotDefaultTemplate'),
        anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE);
};

/**
 * Processes template to template merging. Merges elements that are relevant to series plot.
 *
 * @public
 * @override
 * @param {Object.<*>} source
 * @param {Object.<*>} override
 * @return {Object.<*>}
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.mergeTemplates = function(source, override) {
    var res = goog.base(this, 'mergeTemplates', source, override);

    var des = anychart.utils.deserialization;

    var nodeName = this.getBaseNodeName();
    var templateData = des.getProp(des.getProp(source, nodeName), "data");
    var chartData = des.getProp(des.getProp(override, nodeName), "data");

    if (templateData != null || chartData != null) {
        var data = anychart.utils.JSONUtils.mergeWithOverrideByName(templateData, chartData, "series");
        if (data)
            des.setProp(des.getProp(res, nodeName), 'data', data);
    }

    return res;
};

/**
 * Processes template to chart merging. Merges elements that are relevant to series plot.
 *
 * @public
 * @override
 * @param {Object.<*>} template
 * @param {Object.<*>} chart
 * @return {Object.<*>}
 *
 * // TODO: убрать положенный на детей хуй
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.mergeWithChart = function(template, chart) {
    var res = goog.base(this, 'mergeWithChart', template, chart);
    var nodeName = this.getBaseNodeName();

    var des = anychart.utils.deserialization;
    var jsonUtils = anychart.utils.JSONUtils;

    var templateData = des.getProp(des.getProp(template, nodeName), 'data');
    var chartData = des.getProp(chart, 'data');

    if (chartData) {
        if (templateData) {
            //merge data
            var resData = jsonUtils.merge(templateData, chartData, 'data', 'series');
            des.setProp(res, 'data', resData);

            //merge data series
            var chartSeries = des.getPropArray(chartData, 'series');
            var chartSeriesCount = chartSeries.length;
            var resSeries = [];
            des.setProp(resData, 'series', resSeries);

            for (var i = 0; i < chartSeriesCount; i++) {
                var seriesNode = chartSeries[i];
                if (des.hasProp(seriesNode, 'name')) {

                    var templateSeries = jsonUtils.getNodeByName(
                        des.getPropArray(templateData, 'series'),
                        des.getProp(seriesNode, 'name'));

                    if (templateSeries)
                        resSeries[i] = jsonUtils.merge(templateSeries, seriesNode, 'series', 'point');
                } else resSeries[i] = seriesNode;

            }
        } else {
            res.data = chartData;
        }
    }
    return res;
};

/**
 * Processes base merging for chart node. Should be overridden in
 * descendants if they need any other <chart> subnodes to be merged.
 *
 * @override
 * @protected
 * @param {Object.<*>} template
 * @param {Object.<*>} chart
 * @param {Object.<*>} res - out result
 */
anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.mergeChartNode = function(template, chart, res) {
    goog.base(this, 'mergeChartNode', template, chart, res);

    var des = anychart.utils.deserialization;

    var dataPlotSettings = anychart.utils.JSONUtils.merge(
        des.getProp(template, 'data_plot_settings'),
        des.getProp(chart, 'data_plot_settings')
    );
    if (dataPlotSettings != null)
        res['data_plot_settings'] = dataPlotSettings;

    var child;

    var palettes = anychart.utils.JSONUtils.mergeWithOverride(
        des.getProp(template, 'palettes'),
        des.getProp(chart, 'palettes')
    );
    if (palettes != null) {
        res['palettes'] = palettes;
        for (child in res['palettes'])
            if (des.hasProp(res['palettes'][child], 'name'))
                res['palettes'][child]['name'] = des.getLString(des.getProp(res['palettes'][child], 'name'));
    }

    var thresholds = anychart.utils.JSONUtils.mergeWithOverride(
        des.getProp(template, 'thresholds'),
        des.getProp(chart, 'thresholds'));
    if (thresholds != null) {
        res['thresholds'] = thresholds;
        for (child in res['thresholds'])
            if (des.hasProp(res['thresholds'][child], 'name'))
                res['thresholds'][child]['name'] = des.getLString(des.getProp(res['thresholds'][child], 'name'));
    }
};

anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.prototype.applyDefaults = function(res, defaults) {
    goog.base(this, 'applyDefaults', res, defaults);
    if (!res || !defaults) return;

    var des = anychart.utils.deserialization;

    if (des.hasProp(defaults, 'color_swatch')) {
        var defaultColorSwatch = des.getProp(defaults, 'color_swatch');
        if (des.hasProp(res, 'chart_settings')) {
            var chartSettings = des.getProp(res, 'chart_settings');
            if (des.hasProp(chartSettings, 'controls')) {
                var controls = des.getProp(chartSettings, 'controls');
                if (des.hasProp(controls, 'color_swatch')) {
                    des.setProp(
                        controls,
                        'color_swatch',
                        anychart.utils.JSONUtils.merge(
                            defaultColorSwatch,
                            des.getProp(controls, 'color_swatch'),
                            'color_swatch'));
                }
            }
        }
    }
};
