//Copyright 2011 AnyChart. All rights reserved.
/**
 * @fileoverview
 * Файл объединяет все пакеты visual.
 */
goog.provide('anychart.visual');
goog.require('anychart.svg');
goog.require('anychart.visual.background');
goog.require('anychart.visual.color');
goog.require('anychart.visual.corners');
goog.require('anychart.visual.fill');
goog.require('anychart.visual.hatchFill');
goog.require('anychart.visual.stroke');
goog.require('anychart.visual.text');
