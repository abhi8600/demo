/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.corners.Corners}</li>
 *     <li>@class {anychart.visual.corners.CornersType}</li>
 * </ul>.
 */

goog.provide('anychart.visual.corners');

goog.require('anychart.utils');

/**
 * Corners type.
 * @enum {int}
 * <p>
 * Corners type description:
 * case:CornerType.SQUARE
 *      Draw square with right angle.
 * case:CornerType.ROUNDED
 *      Draw square with outside rounded angle. In this case, method
 *      quadraticCurveTo draw a Bezier curve with control point at edge of right
 *      angle square.
 * case:CornerType.ROUNDED_INNER
 *      Draw square with inside rounded angle. In this case, method
 *      quadraticCurveTo draw a Bezier curve with control point that placed
 *      inside a square.
 * case:CornerType.CUT
 *      Draw octagon.
 * </p>
 */
anychart.visual.corners.CornersType = {
    SQUARE: 0,
    ROUNDED: 1,
    ROUNDED_INNER: 2,
    CUT: 3
};

/**
 * Corners settings.
 * @constructor
 * <p>
 * Sample:
 * <code>
 *  var corners = new anychart.visual.corners.Corners();
 *  corners.deserialize(data);
 *  corners.drawRect(canvasContext, bounds)
 * </code>
 * </p>
 */
anychart.visual.corners.Corners = function() {
};

/**
 * Corners type.
 * @private
 * @type {anychart.visual.corners.CornersType}
 */
anychart.visual.corners.Corners.prototype.type_ = anychart.visual.corners.CornersType.SQUARE;
/**
 * @return {anychart.visual.corners.CornersType}
 */
anychart.visual.corners.Corners.prototype.getType = function() {
    return this.type_;
};
/**
 * @param {anychart.visual.corners.CornersType} value
 */
anychart.visual.corners.Corners.prototype.setType = function(value) {
    this.type_ = value;
};
/**
 * Left top corner.
 * @private
 * @type {Number}
 */
anychart.visual.corners.Corners.prototype.leftTop_ = 0;

/**
 * Sets left top corner.
 * @param {Number} value Value.
 */
anychart.visual.corners.Corners.prototype.setLeftTop = function(value) {
    this.leftTop_ = value;
};
/**
 * @return {Number}
 */
anychart.visual.corners.Corners.prototype.getLeftTop = function() {
    return this.leftTop_;
};
/**
 * Right top corner.
 * @private
 * @type {Number}
 */
anychart.visual.corners.Corners.prototype.rightTop_ = 0;

/**
 * Sets right top corner.
 * @param {Number} value Value.
 */
anychart.visual.corners.Corners.prototype.setRightTop = function(value) {
    this.rightTop_ = value;
};
/**
 * @return {Number}
 */
anychart.visual.corners.Corners.prototype.getRightTop = function() {
    return this.rightTop_;
};
/**
 * Left bottom corner.
 * @private
 * @type {Number}
 */
anychart.visual.corners.Corners.prototype.leftBottom_ = 0;

/**
 * Sets left bottom corner.
 * @param {Number} value Value.
 */
anychart.visual.corners.Corners.prototype.setLeftBottom = function(value) {
    this.leftBottom_ = value;
};
/**
 * @return {Number}
 */
anychart.visual.corners.Corners.prototype.getLeftBottom = function() {
    return this.leftBottom_;
};
/**
 * Right bottom corner.
 * @private
 * @type {Number}
 */
anychart.visual.corners.Corners.prototype.rightBottom_ = 0;

/**
 * Sets right bottom corner.
 * @param {Number} value Value.
 */
anychart.visual.corners.Corners.prototype.setRightBottom = function(value) {
    this.rightBottom_ = value;
};
/**
 * @return {Number}
 */
anychart.visual.corners.Corners.prototype.getRightBottom = function() {
    return this.rightBottom_;
};
/**
 * Deserialize corner settings.
 * @param {Object} data JSON object.
 * <p>
 * Sample:
 * <code>
 *  var corners = new anychart.visual.corners.Corners();
 *  corners.deserialize(data);
 * </code>
 * </p>.
 */
anychart.visual.corners.Corners.prototype.deserialize = function(data) {
    //aliases
    var deserialization = anychart.utils.deserialization;

    //deserializing type
    if (deserialization.hasProp(data, 'type')) {
        var type = deserialization.getLString(deserialization.getProp(
                data, 'type'));
        switch (type) {
            case 'square':
                this.type_ = anychart.visual.corners.CornersType.SQUARE;
                break;
            case 'rounded':
                this.type_ = anychart.visual.corners.CornersType.ROUNDED;
                break;
            case 'roundedinner':
                this.type_ = anychart.visual.corners.CornersType.ROUNDED_INNER;
                break;
            case 'cut':
                this.type_ = anychart.visual.corners.CornersType.CUT;
                break;
        }
    }

    if (this.type_ == anychart.visual.corners.CornersType.SQUARE) return;

    //deserialize sides if need
    if (deserialization.hasProp(data, 'all'))
        this.setAll(deserialization.getNum(
                deserialization.getProp(data, 'all')));
    if (deserialization.hasProp(data, 'left_top'))
        this.leftTop_ = deserialization.getNum(
                deserialization.getProp(data, 'left_top'));
    if (deserialization.hasProp(data, 'right_top'))
        this.rightTop_ = deserialization.getNum(
                deserialization.getProp(data, 'right_top'));
    if (deserialization.hasProp(data, 'right_bottom'))
        this.rightBottom_ = deserialization.getNum(
                deserialization.getProp(data, 'right_bottom'));
    if (deserialization.hasProp(data, 'left_bottom'))
        this.leftBottom_ = deserialization.getNum(
                deserialization.getProp(data, 'left_bottom'));
};

/**
 * Creates polygon.
 *
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.utils.geom.Rectangle} rect Bounds.
 * @return {SVGElement} SVG path element with corresponding attributes.
 */
anychart.visual.corners.Corners.prototype.createSVGPolygon = function(svgManager, rect) {
            var el = svgManager.createPath();
            el.setAttribute('d', this.createSVGPolygonData(svgManager, rect));
            return el;
        };

/**
 * Creates data to create polygon.
 *
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.utils.geom.Rectangle} rect Bounds.
 * @return {String} Polygon data.
 */
anychart.visual.corners.Corners.prototype.createSVGPolygonData = function(svgManager, rect) {
            var points;
            switch (this.type_) {
                default:
                case anychart.visual.corners.CornersType.SQUARE:
                    points = svgManager.pMove(rect.x, rect.y);
                    points += svgManager.pLine(rect.x + rect.width, rect.y);
                    points += svgManager.pLine(
                            rect.x + rect.width, rect.y + rect.height);
                    points += svgManager.pLine(rect.x, rect.y + rect.height);
                    points += svgManager.pLine(rect.x, rect.y);
                    points += svgManager.pFinalize();
                    break;

                case anychart.visual.corners.CornersType.ROUNDED:
                    points = svgManager.pMove(rect.x + this.leftTop_, rect.y);
                    points += svgManager.pLine(
                            rect.x + rect.width - this.rightTop_, rect.y);
                    points += svgManager.pCircleArc(
                            this.rightTop_, false, true, rect.x + rect.width,
                            rect.y + this.rightTop_);
                    points += svgManager.pLine(
                            rect.x + rect.width,
                            rect.y + rect.height - this.rightBottom_);
                    points += svgManager.pCircleArc(
                            this.rightBottom_, false, true,
                            rect.x + rect.width - this.rightBottom_,
                            rect.y + rect.height);
                    points += svgManager.pLine(
                            rect.x + this.leftBottom_, rect.y + rect.height);
                    points += svgManager.pCircleArc(
                            this.leftBottom_, false, true,
                            rect.x, rect.y + rect.height - this.leftBottom_);
                    if (this.leftTop_ != 0) {
                        points += svgManager.pLine(
                                rect.x, rect.y + this.leftTop_);
                        points += svgManager.pCircleArc(
                                this.leftTop_, false, true,
                                rect.x + this.leftTop_, rect.y);
                    }
                    points += svgManager.pFinalize();
                    break;

                case anychart.visual.corners.CornersType.ROUNDED_INNER:
                    points = svgManager.pMove(rect.x + this.leftTop_, rect.y);
                    points += svgManager.pLine(
                            rect.x + rect.width - this.rightTop_, rect.y);
                    points += svgManager.pCircleArc(
                            this.rightTop_, false, false,
                            rect.x + rect.width, rect.y + this.rightTop_);
                    points += svgManager.pLine(
                            rect.x + rect.width,
                            rect.y + rect.height - this.rightBottom_);
                    points += svgManager.pCircleArc(
                            this.rightBottom_, false, false,
                            rect.x + rect.width - this.rightBottom_,
                            rect.y + rect.height);
                    points += svgManager.pLine(
                            rect.x + this.leftBottom_, rect.y + rect.height);
                    points += svgManager.pCircleArc(
                            this.leftBottom_, false, false,
                            rect.x, rect.y + rect.height - this.leftBottom_);
                    if (this.leftTop_ != 0) {
                        points += svgManager.pLine(
                                rect.x, rect.y + this.leftTop_);
                        points += svgManager.pCircleArc(
                                this.leftTop_, false, false,
                                rect.x + this.leftTop_, rect.y);
                    }
                    points += svgManager.pFinalize();
                    break;

                case anychart.visual.corners.CornersType.CUT:
                    points = svgManager.pMove(rect.x + this.leftTop_, rect.y);
                    points += svgManager.pLine(
                            rect.x + rect.width - this.rightTop_, rect.y);
                    points += svgManager.pLine(
                            rect.x + rect.width, rect.y + this.rightTop_);
                    points += svgManager.pLine(
                            rect.x + rect.width,
                            rect.y + rect.height - this.rightBottom_);
                    points += svgManager.pLine(
                            rect.x + rect.width - this.rightBottom_,
                            rect.y + rect.height);
                    points += svgManager.pLine(
                            rect.x + this.leftBottom_, rect.y + rect.height);
                    points += svgManager.pLine(
                            rect.x, rect.y + rect.height - this.leftBottom_);
                    points += svgManager.pLine(
                            rect.x, rect.y + this.leftTop_);
                    points += svgManager.pFinalize();
                    break;
            }
            return points;
        };

/**
 * Set all corners.
 * @param {Number} value Corner value.
 * <p>
 * Sample:
 * <code>
 *  corners.setAll(10);
 * </code>
 * </p>.
 */
anychart.visual.corners.Corners.prototype.setAll = function(value) {
    this.leftTop_ =
            this.rightTop_ =
                    this.rightBottom_ =
                            this.leftBottom_ = value;
};

/**
 * @param {*=} opt_target
 * @return {anychart.visual.corners.Corners}
 */
anychart.visual.corners.Corners.prototype.clone = function(opt_target) {
    if (!opt_target) opt_target = new anychart.visual.corners.Corners();
    opt_target.setType(this.type_);
    opt_target.setLeftBottom(this.leftBottom_);
    opt_target.setRightBottom(this.rightBottom_);
    opt_target.setLeftTop(this.leftTop_);
    opt_target.setRightTop(this.rightTop_);
    return opt_target;
};