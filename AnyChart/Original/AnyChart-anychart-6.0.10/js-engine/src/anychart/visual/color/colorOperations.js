/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.color.colorOperations.Blend}</li>
 *     <li>@class {anychart.visual.color.colorOperations.DarkColor}</li>
 *     <li>@class {anychart.visual.color.colorOperations.HSB}</li>
 *     <li>@class {anychart.visual.color.colorOperations.LightColor}</li>
 *     <li>@class {anychart.visual.color.colorOperations.RGB}</li>
 *     <li>@class {anychart.visual.color.colorOperations.Factory}</li>
 * </ul>
 * Interfaces:
 * <ul>
 *     <li>@interface {anychart.visual.color.colorOperations.IColorOperation}
 *     </li>.
 * </ul>
 */

goog.provide('anychart.visual.color.colorOperations');
//------------------------------------------------------------------------------
//
//                          IColorOperation interface
//
//------------------------------------------------------------------------------


/**
 * Represents color transform operation. Color operation has the next workflow:
 * <code>
 *  // create operation with factory
 *  var op = anychart.visual.color.colorOperations.Factory.getOperation(opID);
 *
 *  // push some args to it
 *  op.arguments.push(arg1);
 *  op.arguments.push(arg2);
 *
 *  // check for arguments compliance
 *  var errmsg = op.checkArguments();
 *  if (errmsg != null) throw new Error(errmsg);
 *
 *  // check if we can execute operation without %color value
 *  // if true, op variable will now contain Color instance, not an operation
 *  if (op.canBeExecuted()) op = op.execute();
 *
 *  // aggregate operation into ColorContainer to provide common interface
 *  var container = new anychart.visual.color.ColorContainer(op, 1);
 *
 *  //set color value to a fill style for example
 *  canvasContext.fillStyle = container.getColor(colorVarValue).toRGBA();
 * </code>
 * That is the way how color parser handles color operations
 *
 * @interface
 */
anychart.visual.color.colorOperations.IColorOperation = function() {};

/**
 * Function arguments. These arguments are parsed from color definition string
 * and pushed into this array by color parser. Parser can push any number of
 * arguments of any type, so type and count checking is required before function
 * calculation.
 * Possible argument types: number (for example for blend op), int (for example
 * for RGB op), Color (for example for LightColor op), "%color" string (treated
 * as Color type) or other operation for nested operations calculation
 * (operation is also treated as Color type).
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.IColorOperation.prototype.arguments =
        null;

/**
 * Determines whether the function can be executed without %Color argument by
 * testing all function arguments for dependency on %Color token. If any
 * argument depends on %Color token or is %Color token itself, this operation
 * cannot be implicitly calculated and needs to be stored until concrete %Color
 * value is determined.
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.IColorOperation.prototype.canBeExecuted =
        function() {};

/**
 * Executes current function on it's arguments. If opt_color is passed it is
 * treated as %Color value.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color} Color instance.
 */
anychart.visual.color.colorOperations.IColorOperation.prototype.execute =
        function(opt_color) {};

/**
 * Checks passed arguments for compliance and returns error message if there is
 * any, null otherwise.
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.IColorOperation.prototype.checkArguments =
        function() {};

//------------------------------------------------------------------------------
//
//                          Blend class
//
//------------------------------------------------------------------------------

/**
 * Represents Blend function
 *
 * @constructor
 * @implements {anychart.visual.color.colorOperations.IColorOperation}
 */
anychart.visual.color.colorOperations.Blend = function() {
    this.arguments = [];
};

/**
 * Function arguments
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.Blend.prototype.arguments = null;

/**
 * Determines whether the function can be executed without %Color argument
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.Blend.prototype.canBeExecuted =
        function() {
            return (this.arguments[0].toRGBA != undefined &&
                    this.arguments[1].toRGBA != undefined);
        };

/**
 * Blend two color with amount.
 * @param {anychart.visual.color.ColorContainer} colorA First color.
 * @param {anychart.visual.color.Color} colorB Second solor.
 * @param {Number} amount Amount.
 * @return {anychart.visual.color.ColorContainer} Color container.
 */
anychart.visual.color.colorOperations.Blend.prototype.blendColors =
        function(colorA, colorB, amount) {
            this.arguments[0] = colorA;
            this.arguments[1] = colorB;
            this.arguments[2] = amount;
            return new anychart.visual.color.ColorContainer(this.execute());
        };

/**
 * Executes current function on it's arguments.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color} Color instance.
 */
anychart.visual.color.colorOperations.Blend.prototype.execute =
        function(opt_color) {
            var arg1 = this.arguments[0];
            if (arg1.execute != undefined)
                arg1 = arg1.execute(opt_color);
            else if (arg1 == '%color')
                arg1 = new anychart.visual.color.Color(
                        opt_color.getRed(),
                        opt_color.getGreen(),
                        opt_color.getBlue(),
                        opt_color.getAlpha(),
                        false);

            var arg2 = this.arguments[1];
            if (arg2.execute != undefined)
                arg2 = arg2.execute(opt_color);
            else if (arg2 == '%color')
                arg2 = new anychart.visual.color.Color(
                        opt_color.getRed(),
                        opt_color.getGreen(),
                        opt_color.getBlue(),
                        opt_color.getAlpha(), false);

            if (arg1.toRGBA == undefined ||
                    arg2.toRGBA == undefined) return opt_color;

            var K = this.arguments[2];
            var K1 = 1 - K;

            return new anychart.visual.color.Color(
                    Math.floor((arg1.getRed() * K +
                            arg2.getRed() * K1) % 0x100),
                    Math.floor((arg1.getGreen() * K +
                            arg2.getGreen() * K1) % 0x100),
                    Math.floor((arg1.getBlue() * K +
                            arg2.getBlue() * K1) % 0x100)
            );
        };

/**
 * Checks passed arguments for consistency and returns error message if there is
 * any, null otherwise.
 * Blend function expects three values to be passed as arguments: Color, Color
 * and number [0..1](color proportion)
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.Blend.prototype.checkArguments =
        function() {
            if (this.arguments.length != 3)
                return 'Blend function expects 3 arguments, ' +
                        this.arguments.length + ' passed';
            if (this.arguments[0].execute == undefined &&
                    this.arguments[0].toRGBA == undefined &&
                    this.arguments[0] != '%color') {
                return 'Blend function requires a color, a function ' +
                        'returning a color or a %color variable as it\'s ' +
                        'first argument';
            }
            if (this.arguments[1].execute == undefined &&
                    this.arguments[1].toRGBA == undefined &&
                    this.arguments[1] != '%color') {
                return 'Blend function requires a color, a function ' +
                        'returning a color or a %color variable as it\'s ' +
                        'second argument';
            }
            if (this.arguments[2].execute != undefined ||
                    this.arguments[2].toRGBA != undefined ||
                    this.arguments[2] == '%color') {
                return 'Blend function requires a number as it\'s ' +
                        'third argument';
            }
            if (this.arguments[2] > 1) this.arguments[2] = 1;
            if (this.arguments[2] < 0) this.arguments[2] = 0;
            return null;
        };

//------------------------------------------------------------------------------
//
//                          DarkColor class
//
//------------------------------------------------------------------------------

/**
 * Represents DarkColor function.
 *
 * @constructor
 * @implements {anychart.visual.color.colorOperations.IColorOperation}
 */
anychart.visual.color.colorOperations.DarkColor = function() {
    this.arguments = [];
};

/**
 * Function arguments.
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.DarkColor.prototype.arguments = null;

/**
 * Determines whether the function can be executed without %Color argument.
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.DarkColor.prototype.canBeExecuted =
        function() {
            return this.arguments[0].toRGBA != undefined;
        };

/**
 * Executes current function on it's arguments.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation} Color instance.
 */
anychart.visual.color.colorOperations.DarkColor.prototype.execute =
        function(opt_color) {
            var arg = this.arguments[0];
            if (arg.execute != undefined)
                arg = arg.execute(opt_color);
            else if (arg == '%color')
                arg = new anychart.visual.color.Color(
                        opt_color.getRed(),
                        opt_color.getGreen(),
                        opt_color.getBlue());

            if (arg.toRGBA == undefined) return opt_color;

            var hls = arg.toHLS();
            var n = (hls.l * 100);
            hls.l -= 0.007 * (n * n) / 100;
            if (hls.l < 0) hls.l = 0;

            return anychart.visual.color.Color.fromHLS(hls.h, hls.l, hls.s);
        };

/**
 * Checks passed arguments for consistency and returns error message if there is
 * any, null otherwise DarkColor function expects one Color value as an
 * argument.
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.DarkColor.prototype.checkArguments =
        function() {
            if (this.arguments.length != 1)
                return 'DarkColor function expects 1 argument, ' +
                        this.arguments.length + ' passed';
            if (this.arguments[0].execute == undefined &&
                    this.arguments[0].toRGBA == undefined &&
                    this.arguments[0] != '%color') {
                return 'DarkColor function requires a color, a function ' +
                        'returning a color or a %color variable as it\'s ' +
                        'argument';
            }
            return null;
        };

//------------------------------------------------------------------------------
//
//                          HSB class
//
//------------------------------------------------------------------------------

/**
 * Represents HSB function.
 *
 * @constructor
 * @implements {anychart.visual.color.colorOperations.IColorOperation}
 */
anychart.visual.color.colorOperations.HSB = function() {
    this.arguments = [];
};

/**
 * Function arguments.
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.HSB.prototype.arguments = null;

/**
 * Determines whether the function can be executed without %Color argument.
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.HSB.prototype.canBeExecuted = function() {
    return true;
};

/**
 * Executes current function on it's arguments.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation} Color instance.
 */
anychart.visual.color.colorOperations.HSB.prototype.execute =
        function(opt_color) {
            return anychart.visual.color.Color.fromHSB(this.arguments[0],
                    this.arguments[1], this.arguments[2]);
        };

/**
 * Checks passed arguments for consistency and returns error message if there is
 * any, null otherwise HSB function expects three number values to be passed as
 * arguments: [0..360](hue), [0..100](saturation) and [0..100](brightness).
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.HSB.prototype.checkArguments =
        function() {
            if (this.arguments.length != 3)
                return 'HSB function expects 3 arguments, ' +
                        this.arguments.length + ' passed';
            if (this.arguments[0].execute != undefined ||
                    this.arguments[0].toRGBA != undefined ||
                    this.arguments[0] == '%color') {
                return "HSB function requires a number as it's first argument";
            }
            if (this.arguments[1].execute != undefined ||
                    this.arguments[1].toRGBA != undefined ||
                    this.arguments[1] == '%color') {
                return "HSB function requires a number as it's second argument";
            }
            if (this.arguments[2].execute != undefined ||
                    this.arguments[2].toRGBA != undefined ||
                    this.arguments[2] == '%color') {
                return "HSB function requires a number as it's third argument";
            }
            this.arguments[0] = Math.round(this.arguments[0]);
            if (this.arguments[0] > 360) this.arguments[0] = 360;
            else if (this.arguments[0] < 0) this.arguments[0] = 0;
            for (var i = 1; i < 3; i++) {
                this.arguments[i] = Math.round(this.arguments[i]);
                if (this.arguments[i] > 100) this.arguments[i] = 100;
                else if (this.arguments[i] < 0) this.arguments[i] = 0;
            }
            return null;
        };

//------------------------------------------------------------------------------
//
//                          LightColor
//
//------------------------------------------------------------------------------

/**
 * Represents LightColor function.
 *
 * @constructor
 * @implements {anychart.visual.color.colorOperations.IColorOperation}
 */
anychart.visual.color.colorOperations.LightColor = function() {
    this.arguments = [];
};

/**
 * Function arguments.
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.LightColor.prototype.arguments = null;

/**
 * Determines whether the function can be executed without %Color argument.
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.LightColor.prototype.canBeExecuted =
        function() {
            return this.arguments[0].toRGBA != undefined;
        };

/**
 * Executes current function on it's arguments.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation} Color instance.
 */
anychart.visual.color.colorOperations.LightColor.prototype.execute =
        function(opt_color) {
            var arg = this.arguments[0];
            if (arg.execute != undefined)
                arg = arg.execute(opt_color);
            else if (arg == '%color')
                arg = new anychart.visual.color.Color(
                        opt_color.getRed(),
                        opt_color.getGreen(),
                        opt_color.getBlue());

            if (arg.toRGBA == undefined) return opt_color;

            var hls = arg.toHLS();
            var n = (hls.l * 100) - 100;
            hls.l += 0.007 * (n * n) / 100;
            if (hls.l > 1) hls.l = 1;

            return anychart.visual.color.Color.fromHLS(hls.h, hls.l, hls.s);
        };

/**
 * Checks passed arguments for consistency and returns error message if there is
 * any, null otherwise LightColor function expects one Color value as an
 * argument.
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.LightColor.prototype.checkArguments =
        function() {
            if (this.arguments.length != 1)
                return 'LightColor function expects 1 argument, ' +
                        this.arguments.length + ' passed';
            if (this.arguments[0].execute == undefined &&
                    this.arguments[0].toRGBA == undefined &&
                    this.arguments[0] != '%color') {
                return 'LightColor function requires a color, a function ' +
                        'returning a color or a %color variable as it\'s ' +
                        'argument';
            }
            return null;
        };

//------------------------------------------------------------------------------
//
//                          RGB
//
//------------------------------------------------------------------------------

/**
 * Represents RGB function.
 *
 * @constructor
 * @implements {anychart.visual.color.colorOperations.IColorOperation}
 */
anychart.visual.color.colorOperations.RGB = function() {
    this.arguments = [];
};

/**
 * Function arguments.
 *
 * @type {Array.<*>}
 */
anychart.visual.color.colorOperations.RGB.prototype.arguments = null;

/**
 * Determines whether the function can be executed without %Color argument.
 *
 * @return {boolean} Is function can be executed.
 */
anychart.visual.color.colorOperations.RGB.prototype.canBeExecuted = function() {
    return true;
};

/**
 * Executes current function on it's arguments.
 *
 * @param {anychart.visual.color.Color} opt_color (as %Color if needed).
 * @return {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation} Color instance.
 */
anychart.visual.color.colorOperations.RGB.prototype.execute =
        function(opt_color) {
            return new anychart.visual.color.Color(this.arguments[0],
                    this.arguments[1], this.arguments[2]);
        };

/**
 * Checks passed arguments for consistency and returns error message if there is
 * any, null otherwise RGB function expects three integer [0..255] (red, green
 * and blue) values to be passed as arguments.
 *
 * @return {String} Error message.
 */
anychart.visual.color.colorOperations.RGB.prototype.checkArguments =
        function() {
            if (this.arguments.length != 3)
                return 'RGB function expects 3 arguments, ' +
                        this.arguments.length + ' passed';
            if (this.arguments[0].execute != undefined ||
                    this.arguments[0].toRGBA != undefined ||
                    this.arguments[0] == '%color') {
                return "RGB function requires a number as it's first argument";
            }
            if (this.arguments[1].execute != undefined ||
                    this.arguments[1].toRGBA != undefined ||
                    this.arguments[1] == '%color') {
                return "RGB function requires a number as it's second argument";
            }
            if (this.arguments[2].execute != undefined ||
                    this.arguments[2].toRGBA != undefined ||
                    this.arguments[2] == '%color') {
                return "RGB function requires a number as it's third argument";
            }
            for (var i = 0; i < 3; i++) {
                this.arguments[i] = Math.round(this.arguments[i]);
                if (this.arguments[i] > 255) this.arguments[i] = 255;
                else if (this.arguments[i] < 0) this.arguments[i] = 0;
            }
            return null;
        };

//------------------------------------------------------------------------------
//
//                          Factory class
//
//------------------------------------------------------------------------------


/**
 * Factory class for all color operations. Do not call this constructor.
 *
 */
anychart.visual.color.colorOperations.Factory = function() {};

/**
 * Creates operation of requested type. Possible operation types are defined in
 * anychart.visual.color.colorOperations.Lexer_.FUNCTION_IDS enumeration.
 *
 * @param {int} funcType
 * (anychart.visual.color.colorOperations.Lexer_.FUNCTION_IDS enum members).
 * @return {anychart.visual.color.colorOperations.IColorOperation} Operation.
 */
anychart.visual.color.colorOperations.Factory.getOperation =
    function(funcType) {
        switch (funcType) {
            case anychart.visual.color.colorParser.FUNCTION_IDS['blend']:
                return new anychart.visual.color.colorOperations.Blend();
            case anychart.visual.color.colorParser.FUNCTION_IDS['lightcolor']:
                return new anychart.visual.color.colorOperations.LightColor();
            case anychart.visual.color.colorParser.FUNCTION_IDS['darkcolor']:
                return new anychart.visual.color.colorOperations.DarkColor();
            case anychart.visual.color.colorParser.FUNCTION_IDS['rgb']:
                return new anychart.visual.color.colorOperations.RGB();
            case anychart.visual.color.colorParser.FUNCTION_IDS['hsb']:
                return new anychart.visual.color.colorOperations.HSB();
            default:
                throw new Error('Unknown color operation identifier passed');
    }
};
