goog.require('anychart.visual.background');

$(document).ready(function() {

    module('visual/background/Background');

    test('Empty background', function() {
        var background = new anychart.visual.background.Background();
        ok(background != null, 'object exists');
        ok(background.getFill() == null, 'fill');
        ok(background.getBorder() == null, 'border');
        ok(background.getHatchFill() == null, 'hatch fill');
        ok(background.isEnabled(), 'isEnabled()');
    });

    test('Advanced Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/background/RectBackground');

    test('Empty rect background', function() {
        var background = new anychart.visual.background.RectBackground();
        ok(background != null, 'object exists');
        ok(background.getFill() == null, 'fill');
        ok(background.getBorder() == null, 'border');
        ok(background.getHatchFill() == null, 'hatch fill');
        ok(background.getCorners() == null, 'corners');
        ok(background.getMargin() == null, 'margin');
        ok(background.getBounds() == null, 'bounds');

        ok(background.isEnabled(), 'isEnabled()');
    });

    test('Advanced Test exists', function() {
        ok(false, 'no tests implemented');
    });
    
});

