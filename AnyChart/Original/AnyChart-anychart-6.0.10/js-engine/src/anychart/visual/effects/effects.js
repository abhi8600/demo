/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.visual.effects.EffectBase}</li>
 *  <li>@class {anychart.visual.effects.BevelEffect}</li>
 *  <li>@class {anychart.visual.effects.GlowEffect}</li>
 *  <li>@class {anychart.visual.effects.ShadowEffect}</li>
 *  <li>@class {anychart.visual.effects.BlurEffect}</li>
 *  <li>@class {anychart.visual.effects.EffectsList}</li>
 * <ul>
 */
goog.provide('anychart.visual.effects');
//------------------------------------------------------------------------------
//
//                          EffectBase class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.visual.effects.EffectBase = function() {
    this.enabled_ = false;
    this.blurX_ = 4;
    this.blurY_ = 4;
    this.strength_ = 1;
};
//------------------------------------------------------------------------------
//                           Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.effects.EffectBase.prototype.enabled_ = false;
/**
 * @return {Boolean}
 */
anychart.visual.effects.EffectBase.prototype.isEnabled = function() {
    return this.enabled_;
};
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.EffectBase.prototype.blurX_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.EffectBase.prototype.blurY_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.EffectBase.prototype.strength_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize effect settings
 * @param {Object} data JSON object with effect settings.
 */
anychart.visual.effects.EffectBase.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'enabled'))
        this.enabled_ = des.getBoolProp(data, 'enabled');

    if (!this.enabled_) return;

    if (des.hasProp(data, 'strength'))
        this.strength_ = des.getNumProp(data, 'strength');

    if (des.hasProp(data, 'blur_x'))
        this.blurX_ = des.getNumProp(data, 'blur_x');

    if (des.hasProp(data, 'blur_y'))
        this.blurY_ = des.getNumProp(data, 'blur_y');
};
/**
 * @return {String}
 */
anychart.visual.effects.EffectBase.prototype.toString = function() {
    return 'x' + this.blurX_ + 'y' + this.blurY_ + 'str' + this.strength_;
};
//------------------------------------------------------------------------------
//                          SVG filter.
//------------------------------------------------------------------------------
/**
 * Create effect filter.
 * @param {anychart.svg.SVGManager} svgManager
 * @param {SVGFilter} filter
 */
anychart.visual.effects.EffectBase.prototype.createFilter = function(filter, svgManager) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//
//                          Bevel lass.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.effects.EffectBase}
 */
anychart.visual.effects.BevelEffect = function() {
    anychart.visual.effects.EffectBase.call(this);
    this.angle_ = 45;
    this.distance_ = 3;
    this.highlightOpacity_ = 1;
    this.highlightColorContainer_ = anychart.utils.deserialization.getColor(null);
    this.shadowOpacity_ = 1;
    this.shadowColorContainer_ = anychart.utils.deserialization.getColor(null);
};
goog.inherits(anychart.visual.effects.BevelEffect,
        anychart.visual.effects.EffectBase);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.BevelEffect.prototype.angle_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.BevelEffect.prototype.distance_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.BevelEffect.prototype.highlightOpacity_ = null;
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.effects.BevelEffect.prototype.highlightColorContainer_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.BevelEffect.prototype.shadowOpacity_ = null;
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.effects.BevelEffect.prototype.shadowColorContainer_ = null;
//------------------------------------------------------------------------------
//                        Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.BevelEffect.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);

    if (!this.enabled_) return;

    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'highlight_opacity'))
        this.highlightOpacity_ = des.getRatioProp(data, 'highlight_opacity');

    if (des.hasProp(data, 'highlight_color'))
        this.highlightColorContainer_ = des.getColorProp(data, 'highlight_color');

    if (des.hasProp(data, 'shadow_opacity'))
        this.shadowOpacity_ = des.getRatioProp(data, 'shadow_opacity');

    if (des.hasProp(data, 'shadow_color'))
        this.shadowColorContainer_ = des.getColorProp(data, 'shadow_color');

    if (des.hasProp(data, 'distance'))
        this.distance_ = des.getNumProp(data, 'distance');

    if (des.hasProp(data, 'angle'))
        this.angle_ = des.getNumProp(data, 'angle');
};
//------------------------------------------------------------------------------
//                          SVG filter.
//------------------------------------------------------------------------------
/**
 * Create
 */
anychart.visual.effects.BevelEffect.prototype.createFilter = function(filter, svgManager) {
    var merge = svgManager.createSVGElement('feMerge');
    var mergeNode1 = svgManager.createSVGElement('feMergeNode');
    mergeNode1.setAttribute('in', 'SourceGraphics');
    merge.appendChild(mergeNode1);
    filter.appendChild(merge);
};
//------------------------------------------------------------------------------
//
//                          GlowEffect.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.effects.EffectBase}
 */
anychart.visual.effects.GlowEffect = function() {
    anychart.visual.effects.EffectBase.call(this);
    this.opacity_ = 1;
    this.inner_ = false;
    this.colorContainer_ = anychart.utils.deserialization.getColor(null);
};
goog.inherits(anychart.visual.effects.GlowEffect,
        anychart.visual.effects.EffectBase);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.GlowEffect.prototype.opacity_ = null;
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.effects.GlowEffect.prototype.colorContainer_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.effects.GlowEffect.prototype.inner_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.GlowEffect.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);

    if (!this.enabled_) return;

    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'opacity'))
        this.opacity_ = des.getRatioProp(data, 'opacity');

    if (des.hasProp(data, 'color'))
        this.colorContainer_ = des.getColorProp(data, 'color');
};
//------------------------------------------------------------------------------
//                          SVG filter.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.GlowEffect.prototype.createFilter = function(filter, svgManager) {
    var merge = svgManager.createSVGElement('feMerge');
    var mergeNode1 = svgManager.createSVGElement('feMergeNode');
    mergeNode1.setAttribute('in', 'SourceGraphics');
    merge.appendChild(mergeNode1);
    filter.appendChild(merge);
};
//------------------------------------------------------------------------------
//
//                          ShadowEffect class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.effects.EffectBase}
 */
anychart.visual.effects.ShadowEffect = function() {
    anychart.visual.effects.EffectBase.call(this);
};
goog.inherits(anychart.visual.effects.ShadowEffect,
        anychart.visual.effects.EffectBase);
//------------------------------------------------------------------------------
//                           Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.ShadowEffect.prototype.angle_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.ShadowEffect.prototype.opacity_ = null;
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.effects.ShadowEffect.prototype.colorContainer_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.visual.effects.ShadowEffect.prototype.distance_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.effects.ShadowEffect.prototype.inner_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.ShadowEffect.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);

    if (!this.enabled_) return;

    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'opacity'))
        this.opacity_ = des.getRatioProp(data, 'opacity');

    if (des.hasProp(data, 'color'))
        this.colorContainer_ = des.getColorProp(data, 'color');

    if (des.hasProp(data, 'distance'))
        this.distance_ = des.getNumProp(data, 'distance');

    if (des.hasProp(data, 'angle'))
        this.angle_ = des.getNumProp(data, 'angle');
};
//------------------------------------------------------------------------------
//                          SVG filter.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.ShadowEffect.prototype.createFilter = function(filter, svgManager) {
    var blur = svgManager.createSVGElement('feGaussianBlur');
    blur.setAttribute('in', 'SourceAlpha');
    blur.setAttribute('stdDeviation', this.blurX_ + ' ' + this.blurY_);
    blur.setAttribute('flood-opacity', 1);
    blur.setAttribute('flood-color', 'red');
    blur.setAttribute('result', 'blur');

    var offset = svgManager.createSVGElement('feOffset');
    offset.setAttribute('in', 'blur');
    offset.setAttribute('dx', this.distance_ * Math.cos(this.angle_));
    offset.setAttribute('dy', this.distance_ * Math.sin(this.angle_));
    offset.setAttribute('result', 'offsetBlur');

    var merge = svgManager.createSVGElement('feMerge');
    var mergeNode1 = svgManager.createSVGElement('feMergeNode');
    mergeNode1.setAttribute('in', 'offsetBlur');
    var mergeNode2 = svgManager.createSVGElement('feMergeNode');
    mergeNode2.setAttribute('in', 'SourceGraphic');

    merge.appendChild(mergeNode1);
    merge.appendChild(mergeNode2);

    filter.appendChild(blur);
    filter.appendChild(offset);
    filter.appendChild(merge);
};
//------------------------------------------------------------------------------
//
//                          BlurEffect.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.effects.EffectBase}
 */
anychart.visual.effects.BlurEffect = function() {
    anychart.visual.effects.EffectBase.call(this);
};
goog.inherits(anychart.visual.effects.BlurEffect,
        anychart.visual.effects.EffectBase);
//------------------------------------------------------------------------------
//                        SVG filter.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.visual.effects.BlurEffect.prototype.createFilter = function(filter, svgManager) {
    var merge = svgManager.createSVGElement('feMerge');
    var mergeNode1 = svgManager.createSVGElement('feMergeNode');
    mergeNode1.setAttribute('in', 'SourceGraphics');
    merge.appendChild(mergeNode1);
    filter.appendChild(merge);
};
/**
 * @inheritDoc
 */
anychart.visual.effects.BlurEffect.prototype.toString = function() {
    return 'blur' + goog.base(this, 'toString');
};
//------------------------------------------------------------------------------
//
//                          EffectsList class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.visual.effects.EffectsList = function() {
    this.enabled_ = true;
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.effects.EffectsList.prototype.enabled_ = null;
/**
 * @return {Boolean}
 */
anychart.visual.effects.EffectsList.prototype.isEnabled = function() {
    return Boolean(this.enabled_ && this.effectsList_ && this.effectsList_.length > 0);
};
/**
 * @private
 * @type {anychart.visual.effects.EffectBase}
 */
anychart.visual.effects.EffectsList.prototype.blur_ = null;
/**
 * @private
 * @type {anychart.visual.effects.ShadowEffect}
 */
anychart.visual.effects.EffectsList.prototype.innerShadow_ = null;
/**
 * @private
 * @type {anychart.visual.effects.ShadowEffect}
 */
anychart.visual.effects.EffectsList.prototype.dropShadow_ = null;
/**
 * @private
 * @type {anychart.visual.effects.GlowEffect}
 */
anychart.visual.effects.EffectsList.prototype.glow_ = null;
/**
 * @private
 * @type {anychart.visual.effects.GlowEffect}
 */
anychart.visual.effects.EffectsList.prototype.innerGlow_ = null;
/**
 * @private
 * @type {Array.<anychart.visual.effects.EffectBase>}
 */
anychart.visual.effects.EffectsList.prototype.effectsList_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize effects
 * @param {Object} data JSON object with effects settings.
 */
anychart.visual.effects.EffectsList.prototype.deserialize = function(data) {
    //todo: Т.к браузеры не держат нужные фильтры, отложено до лучших времен.

    /*var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'enabled'))
        this.enabled_ = des.getBoolProp(data, 'enabled');

    if (!this.enabled_) return;

    this.effectsList_ = [];

    if (des.isEnabledProp(data, 'drop_shadow')) {
        this.dropShadow_ = new anychart.visual.effects.ShadowEffect();
        this.dropShadow_.deserialize(des.getProp(data, 'drop_shadow'));
        this.effectsList_.push(this.dropShadow_);
    }

    if (des.isEnabledProp(data, 'inner_shadow')) {
        this.innerShadow_ = new anychart.visual.effects.ShadowEffect();
        this.innerShadow_.deserialize(des.getProp(data, 'inner_shadow'));
        this.effectsList_.push(this.innerShadow_);
    }*/
/*    if (des.isEnabledProp(data, 'glow')) {
        this.glow_ = new anychart.visual.effects.GlowEffect();
        this.glow_.deserialize(des.getProp(data, 'glow'));
        this.effectsList_.push(this.glow_);
    }

    if (des.isEnabledProp(data, 'inner_glow')) {
        this.innerGlow_ = new anychart.visual.effects.GlowEffect();
        this.innerGlow_.deserialize(des.getProp(data, 'inner_glow'));
        this.effectsList_.push(this.innerGlow_);
    }

    if (des.isEnabledProp(data, 'blur')) {
        this.blur_ = new anychart.visual.effects.BlurEffect();
        this.blur_.deserialize(des.getProp(data, 'blur'));
        this.effectsList_.push(this.blur_);
    }

    if (des.isEnabledProp(data, 'bevel')) {
        this.bevel_ = new anychart.visual.effects.BevelEffect();
        this.bevel_.deserialize(des.getProp(data, 'bevel'));
        this.effectsList_.push(this.bevel_);
    }*/
};
//------------------------------------------------------------------------------
//                           Visual.
//------------------------------------------------------------------------------
/**
 *
 * @param {anychart.svg.SVGManager} svgManager
 * @return {SVGFilter}
 */
anychart.visual.effects.EffectsList.prototype.processEffects = function(svgManager) {
    if (!this.enabled_) return null;
    var filter = svgManager.createSVGElement('filter');
    var effectsCount = this.effectsList_.length;
    if(effectsCount == 0) return null;
    for (var i = 0; i < effectsCount; i++) {
        var effect = this.effectsList_[i];
        if (effect && effect.isEnabled())
            effect.createFilter(filter, svgManager);
    }
    return filter;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.visual.effects.EffectsList.prototype.createEffects = function(svgManager) {
    return svgManager.getEffectsManager().getSVGFilter(this);
};
/**
 * @return {String}
 */
anychart.visual.effects.EffectsList.prototype.toString = function() {
    var effectsCount = this.effectsList_.length;
    var res = '';
    for (var i = 0; i < effectsCount; i++) {
        var effect = this.effectsList_[i];
        if (effect && effect.isEnabled()) res += effect.toString();
    }
    return res;
};
//------------------------------------------------------------------------------
//
//                          SVGEffectsManager.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.visual.effects.SVGEffectsManager = function(svgManager) {
    this.svgManager_ = svgManager;
    this.clear();
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.visual.effects.SVGEffectsManager.prototype.lastId_ = null;
/**
 * @private
 * @type {Object.<String>}
 */
anychart.visual.effects.SVGEffectsManager.prototype.effectMap_ = null;
//------------------------------------------------------------------------------
//                              Visual
//------------------------------------------------------------------------------
/**
 * @param {anychart.visual.effects.EffectsList} effectList
 * @return {String}
 */
anychart.visual.effects.SVGEffectsManager.prototype.getSVGFilter = function(effectList) {
    var id = effectList.toString();
    if (this.effectMap_[id]) return this.effectMap_[id];

    var filter = effectList.processEffects(this.svgManager_);
    if(!filter) return this.svgManager_.getEmptySVGFilter();

    var uid = 'effect_' + String(this.lastId_++);
    filter.setAttribute('id', uid);
    filter.setAttribute('filterUnits', 'userSpaceOnUse');


    this.svgManager_.addDef(filter);

    uid = 'url(#' + uid + ')';
    this.effectMap_[id] = uid;
    return uid;
};
//------------------------------------------------------------------------------
//                              Clear.
//------------------------------------------------------------------------------
anychart.visual.effects.SVGEffectsManager.prototype.clear = function() {
    this.lastId_ = 0;
    this.effectMap_ = {};
};
