[Setup]
AppName              =AnyChart Flash Chart Component 6.0.7
AppVerName           =AnyChart 6.0.7
AppCopyright         =Anychart.Com
DefaultDirName       ={pf}\AnyChart 6.0.7\
DefaultGroupName     =AnyChart 6.0.7
OutputBaseFilename   =AnyChart
LicenseFile          =../src/license.rtf
DisableStartupPrompt =true
AppPublisher         =AnyChart.Com Software
AppSupportURL        =support@anychart.com
AppUpdatesURL        =www.anychart.com
AppVersion           =6.0.7 
UninstallDisplayIcon =../src/anychart.ico
AppPublisherURL      =www.anychart.com
WizardImageFile      =../src/logo.bmp
WizardSmallImageFile =../src/anychart.bmp
UninstallDisplayName =AnyChart Flash Chart Component
AppID                =AnyChart-6.0.7
AppMutex             =AnyChart-6.0.7
;PrivilegesRequired   = admin

Uninstallable             =yes
CreateUninstallRegKey     =yes
UpdateUninstallLogAppName =yes

MinVersion=6.0.7,4

; To make setup dialogs look better added:
UninstallStyle  =modern
WizardStyle     =modern
Compression=bzip


[Files]
;-----------------------------------------------------------------------------------------
;                                 CONTENT
;-----------------------------------------------------------------------------------------

;root
Source: ../src/*.txt; DestDir: {app}; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/*.html; DestDir: {app}; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/*.ico; DestDir: {app}; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/*.url; DestDir: {app}; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/*.rtf; DestDir: {app}; Flags: restartreplace ignoreversion overwritereadonly

;basic-sample
Source: ../src/basic-sample/*.html; DestDir: {app}\basic-sample; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/basic-sample/*.bat; DestDir: {app}\basic-sample; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/basic-sample/*.sh; DestDir: {app}\basic-sample; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/basic-sample/*.xml; DestDir: {app}\basic-sample; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/js/*.js; DestDir: {app}\basic-sample\js; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/swf/AnyChart.swf; DestDir: {app}\basic-sample\swf; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/swf/Preloader.swf; DestDir: {app}\basic-sample\swf; Flags: restartreplace ignoreversion overwritereadonly

;binaries
Source: ../src/binaries/flex/*.swc; DestDir: {app}\binaries\flex; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/swf/*.swf; DestDir: {app}\binaries\swf; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/js/*.js; DestDir: {app}\binaries\js; Flags: restartreplace ignoreversion overwritereadonly

;maps and fonts
Source: ../src/binaries/fonts/*.swf; DestDir: {app}\binaries\fonts; Flags: restartreplace ignoreversion overwritereadonly

Source: ../src/binaries/maps/africa/*.amap; DestDir: {app}\binaries\maps\africa; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/asia/*.amap; DestDir: {app}\binaries\maps\asia; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/custom/*.amap; DestDir: {app}\binaries\maps\custom; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/europe/*.amap; DestDir: {app}\binaries\maps\europe; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/north_america/*.amap; DestDir: {app}\binaries\maps\north_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/oceania/*.amap; DestDir: {app}\binaries\maps\oceania; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/south_america/*.amap; DestDir: {app}\binaries\maps\south_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/south_america/*.amap; DestDir: {app}\binaries\maps\south_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/world/*.amap; DestDir: {app}\binaries\maps\world; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/country/*.amap; DestDir: {app}\binaries\maps\usa\country; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/regions/states/*.amap; DestDir: {app}\binaries\maps\usa\regions\states; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/regions/zip3/*.amap; DestDir: {app}\binaries\maps\usa\regions\zip3; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/states/counties/*.amap; DestDir: {app}\binaries\maps\usa\states\counties; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/states/zip3/*.amap; DestDir: {app}\binaries\maps\usa\states\zip3; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/counties/*.amap; DestDir: {app}\binaries\maps\usa\sub_regions\counties; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/states/*.amap; DestDir: {app}\binaries\maps\usa\sub_regions\states; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/zip3/*.amap; DestDir: {app}\binaries\maps\usa\sub_regions\zip3; Flags: restartreplace ignoreversion overwritereadonly

Source: ../src/binaries/maps/africa/*.xml; DestDir: {app}\binaries\maps\africa; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/asia/*.xml; DestDir: {app}\binaries\maps\asia; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/custom/*.xml; DestDir: {app}\binaries\maps\custom; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/europe/*.xml; DestDir: {app}\binaries\maps\europe; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/north_america/*.xml; DestDir: {app}\binaries\maps\north_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/oceania/*.xml; DestDir: {app}\binaries\maps\oceania; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/south_america/*.xml; DestDir: {app}\binaries\maps\south_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/south_america/*.xml; DestDir: {app}\binaries\maps\south_america; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/world/*.xml; DestDir: {app}\binaries\maps\world; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/country/*.xml; DestDir: {app}\binaries\maps\usa\country; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/regions/states/*.xml; DestDir: {app}\binaries\maps\usa\regions\states; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/regions/zip3/*.xml; DestDir: {app}\binaries\maps\usa\regions\zip3; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/states/counties/*.xml; DestDir: {app}\binaries\maps\usa\states\counties; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/states/zip3/*.xml; DestDir: {app}\binaries\maps\usa\states\zip3; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/counties/*.xml; DestDir: {app}\binaries\maps\usa\sub_regions\counties; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/states/*.xml; DestDir: {app}\binaries\maps\usa\sub_regions\states; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/binaries/maps/usa/sub_regions/zip3/*.xml; DestDir: {app}\binaries\maps\usa\sub_regions\zip3; Flags: restartreplace ignoreversion overwritereadonly


;img
Source: ../src/img/*.gif; DestDir: {app}\img; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/img/*.jpg; DestDir: {app}\img; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/img/*.png; DestDir: {app}\img; Flags: restartreplace ignoreversion overwritereadonly

;register
Source: ../src/register/*.bat; DestDir: {app}\register; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/register/*.sh; DestDir: {app}\register; Flags: restartreplace ignoreversion overwritereadonly

;styles
Source: ../src/styles/*.css; DestDir: {app}\styles; Flags: restartreplace ignoreversion overwritereadonly
Source: ../src/styles/style.css; DestDir: {app}\styles;  AfterInstall: AfterInstallHandler(''); Flags: restartreplace ignoreversion overwritereadonly

;-----------------------------------------------------------------------------------------
;                                 SMTH
;-----------------------------------------------------------------------------------------

[Icons]
Name: {group}\AnyChart; Filename: {app}\anychart.html; WorkingDir: {app}; IconFilename: {app}\anychart.ico; IconIndex: 0
Name: {group}\User's Guide; Filename: {app}\documentation.html; WorkingDir: {app}; IconFilename: {app}\anychart.ico; IconIndex: 0

Name: {userdesktop}\AnyChart 6.0.7; Filename: {app}\anychart.html; MinVersion: 4,4; Tasks: desktopicon; WorkingDir: {app}; IconIndex: 0; IconFilename: {app}\anychart.ico
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\AnyChart; Filename: {app}\anychart.html; MinVersion: 4,4; Tasks: quicklaunchicon; WorkingDir: {app}; IconIndex: 0; IconFilename: {app}\anychart.ico

Name: {group}\Uninstall; Filename: {uninstallexe}; WorkingDir: {app}
Name: {group}\Online Resources\AnyChart WebSite; Filename: {app}\homepage.url; WorkingDir: {app}
Name: {group}\Online Resources\Contact Sales; Filename: {app}\sales.url; WorkingDir: {app}
Name: {group}\Online Resources\Purchase Online; Filename: {app}\purchase.url; WorkingDir: {app}
Name: {group}\Online Resources\Contact Support; Filename: {app}\support.url; WorkingDir: {app}

[Tasks]
Name: desktopicon; Description: Create a &desktop icon; GroupDescription: Additional icons:; MinVersion: 4,4
Name: quicklaunchicon; Description: Create a &Quick Launch icon; GroupDescription: Additional icons:; MinVersion: 4,4; Flags: unchecked

[Run]
Filename: {app}\AnyChart.url; Description: Run AnyChart; WorkingDir: {app}; Flags: nowait postinstall shellexec

[INI]
Filename: {app}\AnyChart.url; Section: InternetShortcut; Key: URL; String: {app}/anychart.html

[Code]
var
  text1: string;
  text2: string;
  tryNum: integer;

procedure TryCreateAnyChartCfg(prefix: String);
var
 path:String;
 f: TFileStream;
begin
  path := ExpandConstant('{userappdata}')+'\Macromedia\Flash Player\#Security\FlashPlayerTrust\AnyChart'+prefix+'.cfg';
  if (FileOrDirExists(path)) then
   begin
    tryNum:= tryNum + 1;
    TryCreateAnyChartCfg(IntToStr(tryNum));
   end
  else
   begin
    try
      f := TFileStream.Create(path, fmShareDenyWrite or fmCreate);
      f.Write(text1, Length(text1));
      f.Write(text2, Length(text2));
      f.Free;
    except
      MsgBox('Error', mbInformation, MB_OK);
    end;
   end;
end;

procedure AfterInstallHandler(S: String);
var
  text: string;
  path: string;
  dirPath: string;
  errorText: string;
  f: TFileStream;
begin

  text := ExpandConstant('{app}');
  text1 := text + #13 + #10;
  text2 := text + '\swf';

  if (not FileOrDirExists(ExpandConstant('{userappdata}')+'\Macromedia')) then
      CreateDir(ExpandConstant('{userappdata}')+'\Macromedia');

  if (not FileOrDirExists(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player')) then
      CreateDir(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player');

  if (not FileOrDirExists(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player\#Security')) then
      CreateDir(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player\#Security');

  if (not FileOrDirExists(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player\#Security\FlashPlayerTrust')) then
      CreateDir(ExpandConstant('{userappdata}')+'\Macromedia\Flash Player\#Security\FlashPlayerTrust');
  TryCreateAnyChartCfg('');

  MsgBox('Please restart your web browser if it''s launched.', mbInformation, MB_OK);
end;
