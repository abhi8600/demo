package com.anychart {
	import com.anychart.animation.AnimationManager;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	
	import flash.geom.Rectangle;
	
	public interface IChart extends IObjectSerializable {
		function getChartTotalBounds():Rectangle;
		function getAnimation():AnimationManager;
		function showNoData(data:XML):void;
	}
}