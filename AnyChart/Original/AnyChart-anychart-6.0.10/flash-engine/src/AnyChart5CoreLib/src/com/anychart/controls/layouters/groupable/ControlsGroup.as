package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	
	internal final class ControlsGroup {
		public var controls:Array;
		public var alignByDataPlot:Boolean;
		
		public function ControlsGroup() {
			this.controls = [];
			this.alignByDataPlot = true;
			this.isAlignSetted = false;
		}
		
		public function addControl(control:Control):void {
			if (!control.layout.alignByDataPlot)
				this.alignByDataPlot = false;
			this.controls.push(control);
		}
	
		private var isAlignSetted:Boolean;
		public function setAlign():void {
			if (this.isAlignSetted || this.alignByDataPlot) return;
			this.isAlignSetted = true;
			for each (var control:Control in this.controls) {
				control.layout.alignByDataPlot = false;
			}
		}
	}
}