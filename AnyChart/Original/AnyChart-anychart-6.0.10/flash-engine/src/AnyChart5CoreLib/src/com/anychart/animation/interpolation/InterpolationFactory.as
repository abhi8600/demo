package com.anychart.animation.interpolation {
	
	public final class InterpolationFactory {
		public static function create(type:*):Interpolator {
			switch (String(type).toLowerCase()) {
				case "linear": return new Linear();
				case "back": return new Back();
				case "bounce": return new Bounce();
				case "circular": return new Circular();
				case "cubic": return new Cubic();
				case "elastic": return new Elastic();
				case "exponential": return new Exponential();
				case "quadratic": return new Quadratic();
				case "quartic": return new Quartic();
				case "quintic": return new Quintic();
				case "sine": return new Sine();
				case "downturn": return new FC();
			}
			return new Linear();
		}
	}
}

import com.anychart.animation.interpolation.Interpolator;

class Linear extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return totalChange * time/duration + initialValue;
	}
}

class Back extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		if (!extraObj)
			extraObj = 1.70158;
		return totalChange * ((time = time / duration - 1) * time * ((extraObj + 1) * time + extraObj) + 1) + initialValue;
	}
}

class Bounce extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		if ((time /= duration) < (1 / 2.75))
			return totalChange * (7.5625 * time * time) + initialValue;
		
		else if (time < (2 / 2.75))
			return totalChange * (7.5625 * (time -= (1.5 / 2.75)) * time + 0.75) + initialValue;
		
		else if (time < (2.5 / 2.75))
			return totalChange * (7.5625 * (time -= (2.25 / 2.75)) * time + 0.9375) + initialValue;
		
		else
			return totalChange * (7.5625 * (time -= (2.625 / 2.75)) * time + 0.984375) + initialValue;
	}
}

class Circular extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return totalChange * Math.sqrt(1 - (time = time/duration - 1) * time) + initialValue;
	}
}

class Cubic extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return totalChange * ((time = time / duration - 1) * time * time + 1) + initialValue;
	}
}

class Elastic extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 0):Number {
		if (time == 0)
			return initialValue;
			
		if ((time /= duration) == 1)
			return initialValue + totalChange;
		
		if (!extraObj2)
			extraObj2 = duration * 0.3;

		var s:Number;
		if (!extraObj || extraObj < Math.abs(totalChange)) {
			extraObj = totalChange;
			s = extraObj2 / 4;
		}else {
			s = extraObj2 / (2 * Math.PI) * Math.asin(totalChange / extraObj);
		}

		return extraObj * Math.pow(2, -10 * time) *
			   Math.sin((time * duration - s) * (2 * Math.PI) / extraObj2) + totalChange + initialValue;
	}
}

class Exponential extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return time == duration ? initialValue + totalChange : totalChange * (-Math.pow(2, -10 * time / duration) + 1) + initialValue;
	}
}

class Quadratic extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return -totalChange * (time /= duration) * (time - 2) + initialValue;
	}
}

class Quartic extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return -totalChange * ((time = time / duration - 1) * time * time * time - 1) + initialValue;
	}
}

class Quintic extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return totalChange * ((time = time / duration - 1) * time * time * time * time + 1) + initialValue;
	}
}

class Sine extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		return totalChange * Math.sin(time / duration * (Math.PI / 2)) + initialValue;
	}
}

class FC extends Interpolator {
	override public function interpolate(time:Number, initialValue:Number, totalChange:Number, duration:Number, extraObj:Number=0, extraObj2:Number = 1):Number {
		if (time < (duration*2/3))
			return totalChange*Math.sqrt(time/duration)*3 + initialValue;
		else
			return totalChange * Math.sin(time / duration * (Math.PI / 2)) + initialValue;
	}
}