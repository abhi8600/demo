package com.anychart.selector {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class MouseSelectionManager {
		
		private var provider:ISelectionProvider;
		public var enabled:Boolean;
		
		private var border:Stroke;
		private var fill:Fill;
		
		private var deselectAllOnPointClick:Boolean;
		
		private var _selectLineOnlyMarkers:Boolean;
		public function get selectLineOnlyMarkers():Boolean { return this._selectLineOnlyMarkers; }
		
		public function MouseSelectionManager(provider:ISelectionProvider) {
			this.provider = provider;
			this.enabled = false;
			
			this.border = new Stroke();
			this.border.enabled = false;
			
			this._selectLineOnlyMarkers = false;
			
			this.fill = new Fill();
			this.fill.deserialize(<fill color="0x0000FF" opacity="0.1" />, null);
			
			this.deselectAllOnPointClick = false;
		}
		
		public function deserializeSettings(node:XML):void {
			if (node.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(node.@enabled);
			
			if (node.border[0] != null) this.border.deserialize(node.border[0]);
			if (node.fill[0] != null) this.fill.deserialize(node.fill[0], null);
			
			if (node.@deselect_all_on_plot_click != undefined)
				this.deselectAllOnPointClick = SerializerBase.getBoolean(node.@deselect_all_on_plot_click);
			
			if (node.@line_series_selection != undefined && SerializerBase.getLString(node.@line_series_selection) == "markersonly") this._selectLineOnlyMarkers = true; 
		}
		
		public function init():void {
			if (this.enabled) {
				if (this.provider.selectionEventsProvider) 
					this.provider.selectionEventsProvider.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
				if (this.provider.extraSelectionEventsProvider)
					this.provider.extraSelectionEventsProvider.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
				if (this.provider.lockSelectionEventsProvider)
					this.provider.lockSelectionEventsProvider.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			}
		}
		
		private var baseData:Point;
		private var baseDraw:Point;
		
		private function mouseDownHandler(e:MouseEvent):void {
			if (this.enabled) {
				
				if (deselectAllOnPointClick) 
					this.provider.deselectSelectedPoints();
				
				this.baseData = this.provider.getSelectionBasePointCoords();
				this.baseDraw = new Point(this.provider.lockSelectionEventsProvider.mouseX, this.provider.lockSelectionEventsProvider.mouseY);
				
				this.provider.lockSelectionEventsProvider.mouseEnabled = true;
				
				this.provider.lockSelectionEventsProvider.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
				this.provider.drawingContainer.stage.addEventListener(MouseEvent.MOUSE_UP, this.mouseUpHandler);
				
				this.provider.selectionEventsProvider["mouseChildren"] = false;
				e.stopPropagation();
				e.stopImmediatePropagation();
			}
		}
		
		private function mouseMoveHandler(e:MouseEvent):void {
			var actual:Point = this.provider.getSelectionActualPointCoords();
			this.drawSelectionArea();
			this.provider.selectPointsInArea(new Rectangle(this.baseData.x, this.baseData.y, actual.x - this.baseData.x, actual.y - this.baseData.y));
			
			e.stopPropagation();
			e.stopImmediatePropagation();
		}
		
		private function mouseUpHandler(e:MouseEvent):void {
			
			this.provider.lockSelectionEventsProvider.mouseEnabled = false;
			
			var actual:Point = this.provider.getSelectionActualPointCoords();
			this.provider.drawingContainer.graphics.clear();
			this.provider.finalizePointsSelection(new Rectangle(this.baseData.x, this.baseData.y, actual.x - this.baseData.x, actual.y - this.baseData.y));
			
			this.provider.selectionEventsProvider.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
			this.provider.drawingContainer.stage.removeEventListener(MouseEvent.MOUSE_UP, this.mouseUpHandler);
			this.provider.selectionEventsProvider["mouseChildren"] = true;
			e.stopPropagation();
			e.stopImmediatePropagation();
		}
		
		private function drawSelectionArea():void {
			var base:Point = this.baseDraw;
			var actual:Point = new Point(this.provider.lockSelectionEventsProvider.mouseX, this.provider.lockSelectionEventsProvider.mouseY);
			
			var g:Graphics = this.provider.drawingContainer.graphics;
			g.clear();
			this.border.apply(g);
			this.fill.begin(g, new Rectangle(base.x, base.y, actual.x - base.x, actual.y - base.y));
			g.drawRect(base.x, base.y, actual.x - base.x, actual.y - base.y);
			g.endFill();
		}
	}
}