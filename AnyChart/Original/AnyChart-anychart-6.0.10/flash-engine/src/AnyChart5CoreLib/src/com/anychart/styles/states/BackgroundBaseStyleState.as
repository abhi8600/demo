package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IRotationManager;
	import com.anychart.styles.Style;
	import com.anychart.utils.StringUtils;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.fill.FillType;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.stroke.StrokeType;
	
	public class BackgroundBaseStyleState extends StyleStateWithEffects {
		
		public var fill:Fill;
		public var hatchFill:HatchFill;
		public var stroke:Stroke;
		
		public function BackgroundBaseStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			data = super.deserialize(data, resources);
			if (data.@hatch_type != undefined) {
				var hatchType:String = data.@hatch_type;
				if (data.hatch_fill[0] != null && data.hatch_fill[0].@type != undefined) {
					var childNode:XML = data.hatch_fill[0];
					childNode.@type = StringUtils.replace(String(childNode.@type).toLowerCase(),"%hatchtype",hatchType);
				}
			}
			
			if (SerializerBase.isEnabled(data.fill[0])) {
				this.fill = new Fill();
				this.fill.deserialize(data.fill[0],resources,this.style);
			}
			
			if (SerializerBase.isEnabled(data.hatch_fill[0])) {
				this.hatchFill = new HatchFill();
				this.hatchFill.deserialize(data.hatch_fill[0],this.style);
			}
			
			this.deserializeStroke(data);
			return data;
		}
		
		protected function deserializeStroke(data:XML):void {
			if (data.border[0] != null) {
				if (SerializerBase.isEnabled(data.border[0])) {
					if (this.stroke == null) this.stroke = new Stroke();
					this.stroke.deserialize(data.border[0],this.style);
				}else {
					this.stroke = null;
				}
			}
		}
		
		override public function initializeRotations(manager:IRotationManager, baseScaleInvert:Boolean):void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.setGradientAngles(this.stroke.gradient, baseScaleInvert, manager);
			
			if (this.fill != null && this.fill.gradient != null)
				this.setGradientAngles(this.fill.gradient, baseScaleInvert, manager);
		}
		
		override public function setNormalRotation():void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.stroke.gradient.angle = this.stroke.gradient.normalAngle;
			if (this.fill != null && this.fill.gradient != null)
				this.fill.gradient.angle = this.fill.gradient.normalAngle;
		}
		
		override public function setInvertedRotation():void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.stroke.gradient.angle = this.stroke.gradient.invertedAngle;
			if (this.fill != null && this.fill.gradient != null)
				this.fill.gradient.angle = this.fill.gradient.invertedAngle;
		}
		
		public function hasGradient():Boolean {
			return (this.fill && this.fill.type == FillType.GRADIENT) || (this.stroke && this.stroke.type == StrokeType.GRADIENT); 
		}
	}
}