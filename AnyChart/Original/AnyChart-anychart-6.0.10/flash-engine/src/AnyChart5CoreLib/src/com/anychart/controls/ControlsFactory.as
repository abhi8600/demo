package com.anychart.controls {
	import com.anychart.controls.image.ImageControl;
	import com.anychart.controls.label.LabelControl;
	
	public final class ControlsFactory {
		
		private static var types:Array = [];
		
		public static function register(classRef:Class):void {
			types.push(classRef);
		}
		
		public static function create(data:XML):Control {
			for (var i:uint = 0;i<types.length;i++) {
				if (types[i].check(data))
					return new types[i];
			}
			return null;
		}
	}
}