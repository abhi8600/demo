package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	
	public final class BackgroundBaseStyleState3DEntry extends BackgroundBaseStyleState {
		
		public function BackgroundBaseStyleState3DEntry(style:Style) {
			super(style);
		}
		
		private var data:XML;
		private var resourcesLink:ResourcesLoader;
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.data = data.copy();
			return data;
		}
		
		public function createCopy(state:BackgroundBaseStyleState3DEntry = null):BackgroundBaseStyleState3DEntry {
			if (state == null)
				state = new BackgroundBaseStyleState3DEntry(this.style);
			state.deserialize(this.data, this.resourcesLink);
			return state;
		}
		
		public function clear():void {
			this.data = null;
			this.resourcesLink = null;
		}

	}
}