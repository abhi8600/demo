package com.anychart.visual.effects {
	
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	import flash.filters.BlurFilter;
	
	public class EffectsList implements ISerializable {
		
		// Properties.
		public var enabled:Boolean=true;
		
		private var _blur:EffectBase=null;
		public function get blur():EffectBase {
			if (this._blur==null) this._blur=new EffectBase();
			return this._blur;
		}
		public function set blur(value:EffectBase):void { this._blur=value; }
		
		private var _innerShadow:Shadow=null;
		public function get innerShadow():Shadow {
			if(this._innerShadow==null) {
				this._innerShadow=new Shadow();
				this._innerShadow.inner = true;
			}
			return _innerShadow;
		}
		public function set innerShadow(value:Shadow):void { 
			this._innerShadow=value;
			this._innerShadow.inner = true; 
		}
		
		private var _dropShadow:Shadow=null;
		public function get dropShadow():Shadow {
			if(this._dropShadow==null) this._dropShadow=new Shadow();
			return this._dropShadow;
		}
		public function set dropShadow(value:Shadow):void { this._dropShadow=value; }
		
		private var _glow:Glow=null;
		public function get glow():Glow {
			if(this._glow==null) this._glow=new Glow();
			return this._glow;
		}
		public function set glow(value:Glow):void { this._glow=value; }
		
		private var _innerGlow:Glow=null;
		public function get innerGlow():Glow {
			if(this._innerGlow==null) this._innerGlow=new Glow();
			_innerGlow.inner=true;
			return this._innerGlow;
		}
		public function set innerGlow(value:Glow):void { this._innerGlow=value; _innerGlow.inner=true; }
		
		private var _bevel:Bevel=null;
		public function get bevel():Bevel {
			if(this._bevel==null) this._bevel=new Bevel();
			return this._bevel;
		}
		public function set bevel(value:Bevel):void { this._bevel=value; }
		
		private var _list:Array=null;
		public function get list():Array { return this._list; }
		
		// Methods.
		private function processEffects():void {
			if(!this.enabled) return;
			this._list = [];
			
			if (this._blur!=null && this._blur.enabled) this._list.push(new BlurFilter(this._blur.blurX,this._blur.blurY));
			if (this._dropShadow!=null && this._dropShadow.enabled) this._list.push(this._dropShadow.createFilter());
			if (this._innerShadow!=null && this._innerShadow.enabled) this._list.push(this._innerShadow.createFilter());
			if (this._glow!=null && this._glow.enabled) this._list.push(this._glow.createFilter());
			if (this._innerGlow!=null && this._innerGlow.enabled) this._list.push(this._innerGlow.createFilter());
			if (this._bevel!=null && this._bevel.enabled) this._list.push(this._bevel.createFilter());
			
			if (this._list.length == 0) {
				this.enabled = false;
				this._list = null;
			}
		}
		
		public function deserialize(data:XML):void {
			if(data.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(data.@enabled);
			if(!this.enabled) return;
			
			if(SerializerBase.isEnabled(data.drop_shadow[0])) this.dropShadow.deserialize(data.drop_shadow[0])
			if(SerializerBase.isEnabled(data.inner_shadow[0])) this.innerShadow.deserialize(data.inner_shadow[0]);
			if(SerializerBase.isEnabled(data.glow[0])) this.glow.deserialize(data.glow[0]);
			if(SerializerBase.isEnabled(data.inner_glow[0])) this.innerGlow.deserialize(data.inner_glow[0]);
			if(SerializerBase.isEnabled(data.blur[0])) this.blur.deserialize(data.blur[0]);
			if(SerializerBase.isEnabled(data.bevel[0])) this.bevel.deserialize(data.bevel[0]);
			
			this.processEffects();
		}

	}
}