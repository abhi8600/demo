package com.anychart.dispose {
	public final class DisposableStaticList {
		
		private static var disposeTargets:Array = [];
		
		public static function register(classRef:Class):void {
			disposeTargets.push(classRef);
		} 
		
		public static function clear():void {
			for each (var item:Object in disposeTargets) { 
				if (item != null)
					item.clear();
			}
		}
	}
}