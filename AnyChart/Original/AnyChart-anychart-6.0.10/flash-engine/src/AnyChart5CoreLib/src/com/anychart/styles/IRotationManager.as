package com.anychart.styles {
	
	public interface IRotationManager {
		function getNormalAngle(angle:Number, isBaseInverted:Boolean):Number;
		function getInvertedAngle(angle:Number, isBaseInverted:Boolean):Number;
		function getNormalAnchor(anchor:uint):uint;
		function getNormalHAlign(hAlign:uint, vAlign:uint):uint;
		function getNormalVAlign(hAlign:uint, vAlign:uint):uint;
		function getInvertedAnchor(anchor:uint):uint;
		function getInvertedHAlign(hAlign:uint, vAlign:uint):uint;
		function getInvertedVAlign(hAlign:uint, vAlign:uint):uint;
	}
}