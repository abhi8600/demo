package com.anychart.formatters.terms
{
	public class TermTokenOneScale extends TermToken implements ITerm
	{
		private var scale:Array;

		public function TermTokenOneScale(tokenName:String, parentCall:ITerm, scale:Array) {
			super(tokenName, parentCall);
			this.scale = scale;
		}

		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return parentCall.getValue(getTokenFunction, isDateTimeToken) + execFormating(getTokenFunction(tokenName));
		}

		private function execFormating(value:String):String {
			var numValue:Number = Number(value);
			if (isNaN(numValue)) return String(value);
			var isNegative:Boolean = numValue < 0;
			if (isNegative) numValue = -numValue;
			
			var unitValue:Number = Number(scale[0]);
			var newValue:Number = Math.floor(numValue/unitValue);
			newValue += (numValue-newValue*unitValue)/unitValue;
			var res:String = formatNumber(newValue, isNegative) + scale[1];
			return formatString(res);
		}

	}
}