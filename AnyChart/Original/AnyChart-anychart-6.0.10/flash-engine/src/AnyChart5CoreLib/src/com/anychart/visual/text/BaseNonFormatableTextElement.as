package com.anychart.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.RectBackground;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	
	public class BaseNonFormatableTextElement implements IStyleSerializableRes {
		
		public var wrap:Boolean;
		public var width:Number;
		
		public var enabled:Boolean = true;
		public var font:FontSettings;
		public var background:RectBackground;
		public var multiLineAlign:uint = MultiLineAlign.CENTER;
		
		protected var field:TextField;
		protected var bg:Sprite;
		
		public var rotation:Number = 0;
		protected var radRotation:Number;
		
		public var text:String;
		
		protected var sin:Number;
		protected var cos:Number;
		
		protected var absSin:Number;
		protected var absCos:Number;
		
		private var useEmbed:Boolean = false;
		
		public function BaseNonFormatableTextElement() {
			this.text = "";
		}
		
		protected function createFontSettings():FontSettings { return new FontSettings(); }
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			
			this.font = this.createFontSettings();
			if (!this.enabled) {
				this.font.createFormat();
				return;
			}
			
			if (data.@rotation != undefined) this.rotation = -SerializerBase.getNumber(data.@rotation);
			if (data.@width != undefined) {
				this.width = SerializerBase.getNumber(data.@width);
				this.wrap = true;
			}
			
			if (data.font[0] != null) {
				this.font.deserialize(data.font[0], resources, style);
				if (data.font[0].@embed != undefined) this.useEmbed = SerializerBase.getBoolean(data.font[0].@embed);
			}else
				this.font.createFormat();
				
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new RectBackground();
				this.background.deserialize(data.background[0], resources, style);
			}
			
			var alignAttrName:String = (data.@text_align != undefined) ? "text_align" : "multi_line_align";
			
			if (data.@[alignAttrName] != undefined) {
				switch (SerializerBase.getEnumItem(data.@[alignAttrName])) {
					case 'left':
						this.multiLineAlign = MultiLineAlign.LEFT;
						if (this.font.format)
							this.font.format.align = TextFormatAlign.LEFT;
						break;
					case 'center':
						this.multiLineAlign = MultiLineAlign.CENTER;
						if (this.font.format)
							this.font.format.align = TextFormatAlign.CENTER;
						break;
					case 'right':
						this.multiLineAlign = MultiLineAlign.RIGHT;
						if (this.font.format)
							this.font.format.align = TextFormatAlign.RIGHT;
						break;
				}
			}
			
			this.field = this.createTextField();
				
			if (this.background != null)
				this.bg = this.createBackgroundSprite(this.field); 
		}
		
		internal function createTextField():TextField {
			var field:TextField = new TextField();
			field.multiline = true;
			field.autoSize = TextFieldAutoSize.LEFT;
			field.wordWrap = false;
			
			if (this.wrap) {
				field.wordWrap = true;
				field.width = this.width;
			}
			if (!this.font.renderAsHTML)
				field.defaultTextFormat = this.font.format;
			field.embedFonts = this.useEmbed;
			if (this.rotation % 90 == 0 && this.font.hasEffects())
				field.filters = this.font.effects.list;
			return field;
		}
		
		internal final function createBackgroundSprite(field:TextField):Sprite {
			var bg:Sprite = new Sprite();
			bg.addChild(field);
			if (this.background != null) {
				this.background.moveContent(field,true);
				if (this.rotation % 90 == 0 && this.background.effects != null)
					bg.filters = this.background.effects.list;
			}
			return bg; 
		}
		
		public function createInformation():TextElementInformation {
			return new TextElementInformation();
		}
		
		public function getBounds(info:TextElementInformation):void {
			this.initRotation();
			if (this.font.renderAsHTML)
				this.field.htmlText = info.formattedText;
			else
				this.field.text = info.formattedText;
				
			this.getNormalBounds(info);
			this.getRotatedBounds(info);
		}
		
		protected function getNormalBounds(info:TextElementInformation):void {
			var res:Rectangle = new Rectangle(0,0,this.field.width,this.field.height);
			if (this.background != null) {
				this.background.applyMargins(res);
				res.width += 4;
				res.height += 4;
			}
				
			info.nonRotatedBounds = res;
		}
		
		protected function getRotatedBounds(info:TextElementInformation):void {
			var res:Rectangle = info.nonRotatedBounds;
			if (this.rotation != 0) {
				res = info.nonRotatedBounds.clone();
			
				var w:Number = res.width;
				var h:Number = res.height;
				
				res.width = w*this.absCos + h*this.absSin;
				res.height = w*this.absSin + h*this.absCos;
			}
			info.rotatedBounds = res;
		}
		
		internal final function setFieldProperties(field:TextField, info:TextElementInformation, color:uint):void {
			
			if (this.font.isDynamicTextColor && !this.font.renderAsHTML)
				field.defaultTextFormat = this.font.getFormat(color);
			
			if (this.font.renderAsHTML)
				field.htmlText = info.formattedText;
			else
				field.text = info.formattedText;
			
			if (this.font.effects != null)
				field.filters = this.font.effects.list;
		}
		
		protected function drawTextBackground(g:Graphics, bounds:Rectangle, color:uint, hatchType:uint):void {
			bounds = bounds.clone();
			bounds.x = 1;
			bounds.y = 1;
			bounds.width -= 2;
			bounds.height -= 2;
			this.background.draw(g, bounds, color, hatchType);
		}
		
		public function draw(g:Graphics, x:Number, y:Number, info:TextElementInformation, color:uint, hatchType:uint):void {
			this.setFieldProperties(this.field, info, color);
			
			var data:BitmapData = new BitmapData(info.nonRotatedBounds.width, info.nonRotatedBounds.height, true,0);
			if (this.background == null) {
				data.draw(this.field);
			}else {
				this.bg.graphics.clear();
				this.drawTextBackground(this.bg.graphics, info.nonRotatedBounds, color, hatchType);
				data.fillRect(info.nonRotatedBounds,0x00ffffff);
				data.draw(this.bg);
			}
			
			var m:Matrix = this.getMatrix(x,y,
										  info.nonRotatedBounds.width,info.nonRotatedBounds.height,
										  info.rotatedBounds.width,info.rotatedBounds.height);
			g.beginBitmapFill(data, m, false, this.rotation % 90 != 0);
			g.drawRect(x,y,info.rotatedBounds.width,info.rotatedBounds.height);
			g.endFill();
		}
		
		private function getMatrix(x:Number, 
								   y:Number, 
								   nonRotatedW:Number, 
								   nonRotatedH:Number,
								   realW:Number,
								   realH:Number):Matrix {
			var m:Matrix = new Matrix();
			var tx:Number = 0;
			var ty:Number = 0;
			if (this.rotation != 0) {
				m.rotate(this.radRotation);
				if (this.rotation <= 90) {
					tx = nonRotatedH*this.absSin;
				}else if (this.rotation <= 180) {
					tx = realW;
					ty = nonRotatedH*this.absCos;
				}else if (this.rotation <= 270) {
					tx = realW - nonRotatedH*this.absSin;
					ty = realH;
				}else {
					ty = realH - nonRotatedH*this.absCos;				
				}
			}
			m.translate(x+tx, y+ty);
				
			return m;
		}
		
		protected function initRotation():void {
			/* if (this.rotation >= 360 || this.rotation <= 360) 
				this.rotation = this.rotation - Math.floor(this.rotation/360)*360;
				
			if (this.rotation < 0)
				this.rotation = 360 - this.rotation; */
				
			if (Math.abs(this.rotation) >= 360) 
				this.rotation %= 360;
				
			if (this.rotation < 0)
				this.rotation += 360;
			
			this.radRotation = this.rotation * Math.PI / 180;
			this.sin = Math.sin(this.radRotation);
			this.cos = Math.cos(this.radRotation);
			this.absSin = this.sin < 0 ? -this.sin : this.sin;
			this.absCos = this.cos < 0 ? -this.cos : this.cos;
		}
		
		protected function getCDATAString(value:XML):String {
			var str:String = SerializerBase.getCDATAString(value);
			if (this.font.renderAsHTML) {
				//TODO get colors from html text
			}
			return str;
		}
	}
}