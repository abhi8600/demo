package com.anychart.resources {
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	/**
	 * Класс для работы с относительными путями и получения абсолютных путей ресурсов
	 * @see ResourcesLoader
	 * @see ResourcesLoader#deserializeSettings()
	 * @see ResourcesLoader#initialize()
	 */
	public final class ResourcesPathSettings implements ISerializable {
		
		/**
		 * Путь указывается относительно html
		 * 
		 * @see ResourcesPathSettings#pathType
		 */
		public static const PATH_RELATIVE_TO_HTML:uint = 0;
		
		/**
		 * Путь указывается относительно swf
		 * 
		 * @see ResourcesPathSettings#pathType
		 */
		public static const PATH_RELATIVE_TO_SWF:uint = 1;
		
		/**
		 * Путь указывается абсолютно
		 * 
		 * @see ResourcesPathSettings#pathType
		 */
		public static const PATH_ABSOLUTE:uint = 2;
		
		/**
		 * Базовый путь, полученый из xml
		 * @see deserialize()
		 */
		private var path:String;
		
		/**
		 * Тип пути
		 * @see #PATH_RELATIVE_TO_HTML
		 * @see #PATH_RELATIVE_TO_SWF
		 * @see #PATH_ABSOLUTE
		 */
		private var pathType:uint;
		
		/**
		 * Префикс для путей всех ресурсов, которые используют этот ResourcesPathSettings
		 * @see #initialize()
		 */
		private var prefix:String;
		
		/**
		 * Конструктор
		 * @param pathType тип пути
		 * @see #pathType
		 * @param path базовый путь
		 * 
		 */
		public function ResourcesPathSettings(pathType:int = PATH_RELATIVE_TO_HTML, path:String = null) {
			this.path = path;
			this.pathType = pathType;
		}
		
		/**
		 * Десериализовать настройки
		 * @param data XML с настройками вида <nodeName path="" path_type="relativeToHtml|relativeToSwf|absolute" />
		 */
		public function deserialize(data:XML):void {
			if (data.@path != undefined) this.path = SerializerBase.getString(data.@path);
			if (data.@path_type != undefined) {
				switch (SerializerBase.getEnumItem(data.@path_type)) {
					case "relativetohtml": this.pathType = PATH_RELATIVE_TO_HTML; break;
					case "relativetoswf": this.pathType = PATH_RELATIVE_TO_SWF; break;
					case "absolute": this.pathType = PATH_ABSOLUTE; break;
				}
			}
		}
		
		/**
		 * Инициализация
		 * Устанавливает значение #prefix
		 * @param swfUrl путь к swf
		 */
		public function initialize(swfUrl:String):void {
			var path:String = this.path == null ? "" : this.path;
			if (swfUrl == null) this.pathType = PATH_ABSOLUTE;
			switch (this.pathType) {
				case PATH_RELATIVE_TO_HTML: 
					this.prefix = "./"+path;
					break;
				case PATH_RELATIVE_TO_SWF:
					var lastIndex:int = swfUrl.lastIndexOf("/");
					lastIndex = Math.max(lastIndex,swfUrl.lastIndexOf("\\"));
					this.prefix = swfUrl.substring(0, lastIndex+1)+path;
					break;
				case PATH_ABSOLUTE:
					this.prefix = path;
					break; 
			}
		}
		
		/**
		 * Применить настройки к конкретному URL
		 * @param url 
		 * @return абсолютный путь
		 */
		public function apply(url:String):String {
			return this.prefix+url;
		}

	}
}