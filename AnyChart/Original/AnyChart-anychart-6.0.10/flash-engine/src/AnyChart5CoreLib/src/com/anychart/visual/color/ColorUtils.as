package com.anychart.visual.color
{
	public final class ColorUtils
	{
		// Global properties.
		public static var r:uint;
		public static var g:uint;
		public static var b:uint;
		
		public static var hue:Number;
		public static var sat:Number;
		public static var br:Number;
		
		private static var _trHelper:ColorTransformHelper=new ColorTransformHelper();
		
		// Methods.
		public static function getDarkColor(source:uint):uint {
			initColorTransform(source);
			
			 var num:Number = (((_trHelper.b() * 100) * 0.27272727272727271) + 0.36) / 100;
            num = 0.007 * Math.pow(_trHelper.b() * 100, 2);
            num /= 100;
            _trHelper.b_ex(_trHelper.b() - num);
            
			return _trHelper.aColor();
		}
		
		public static function getLightColor(source:uint):uint {
			initColorTransform(source);
			
			var n:Number=(_trHelper.b() * 100) - 100;
			var num:Number=0.007*(n*n);
            num /= 100;
            _trHelper.b_ex(_trHelper.cValue + num);
            
			return _trHelper.aColor();
		}
		
		public static function initColorTransform(color:uint):void {
			toHLS(color);
			
            _trHelper.a_ex(hue);
            _trHelper.b_ex(br);
            _trHelper.c_ex(sat);
		} 
		
		public static function blendColor(colorA:uint,colorB:uint,amount:Number):uint {
			toRGB(colorA);
			var ra:uint=r;
			var ga:uint=g;
			var ba:uint=b;
			
			toRGB(colorB);
			var	K:int=Math.min(256,Math.max(0,int(256*amount)));
			var	K1:int=256-K;
		
			var or:uint=(ra*K + r*K1)>>8;
			var og:uint=(ga*K + g*K1)>>8;
			var ob:uint=(ba*K + b*K1)>>8;
			
			return fromRGB(or,og,ob);
		}
		
		
		public static function toHLS(color:uint):void {
			toRGB(color);
			
			var rf:Number=Number(r)/255;
			var gf:Number=Number(g)/255;
			var bf:Number=Number(b)/255;
			
			var minval:Number=Math.min(rf,Math.min(gf,bf));
			var maxval:Number=Math.max(rf,Math.max(gf,bf));
			
			if(minval==maxval)
			{
				hue=0;
				br=(maxval+minval)/2
				sat=0;
				return;
			}
			
			var mdiff:Number=maxval-minval;
			var msum:Number=maxval+minval;
			
			var m_luminance:Number=msum/2;
			var m_saturation:Number=0;
			var m_hue:Number=0;
			
			m_saturation =	(m_luminance <= 0.5) ? (mdiff / msum) : (mdiff / (2.0 - msum));
			
			/*			
			var rnorm:Number=(maxval-rf)/mdiff;
			var gnorm:Number=(maxval-gf)/mdiff;
			var bnorm:Number=(maxval-bf)/mdiff;
			
			if (r == maxval) m_hue =0.1666*(6.0+bnorm-gnorm);
				else if (g == maxval) m_hue=0.1666*(2.0+rnorm-bnorm);
					else m_hue =0.1666*(4.0+gnorm-rnorm);

			if (m_hue > 1) m_hue = m_hue - 1;
			*/
			
			hue=m_hue;
			br=m_luminance;
			sat=m_saturation;
			
			
			{
				var min:Number=minval;
				var max:Number=maxval;
				var delta:Number=mdiff;
				var h:Number=0;
			
				if( rf == max )			h =	6 + ( gf - bf ) / delta;
					else if( gf == max )	h = 2 + ( bf - rf ) / delta;
						else			h = 4 + ( rf - gf ) / delta;
			
				h *= 0.1666;
				if( h >= 1.0) h-=1.0;
			
				if( rf == max )			h =	6 + ( gf - bf ) / delta;
					else if( gf == max )	h = 2 + ( bf - rf ) / delta;
						else			h = 4 + ( rf - gf ) / delta;
			
				h *= 0.1666;
				if( h >= 1.0) h-=1.0;
				
				hue=h;
			}
		}
		
		public static function toRGB(color:uint):void
		{
			 r = color >> 16;
			 var temp:uint = color ^ r << 16;
			 g = temp >> 8;
			 b = temp ^ g << 8;
		}
		
		public static function fromRGB(red:uint,green:uint,blue:uint):uint
		{
			var c:uint=0;
			c=blue|green<<8|red<<16;
			return c;
		}
		
		// hc - 0..360, sc - 0..100, bc - 0..100.
		public static function fromHSB(hc:uint, sc:uint, bc:uint):uint {
			var r:Number=Number(hc/360.0);
			var g:Number=Number(bc/100.0);
			var b:Number=Number(sc/100.0);
		
			if( b == 0 ) return fromRGB(uint(g*255),uint(g*255),uint(g*255));
		
			var h:Number=r*6;
			var i:int=int(Math.floor(h));
			var f:Number=h-i;
			var p:Number=g*(1-b);
			var q:Number=0;
		
			if(i&1)		q = g * ( 1 - b * f );
			else		q = g * ( 1 - b * ( 1 - f ) );

			switch( i ) {
				case 0:		return	fromRGB(uint(g*255),uint(q*255),uint(p*255));
				case 1:		return	fromRGB(uint(q*255),uint(g*255),uint(p*255));
				case 2:		return	fromRGB(uint(p*255),uint(g*255),uint(q*255));
				case 3:		return	fromRGB(uint(p*255),uint(q*255),uint(g*255));
				case 4:		return	fromRGB(uint(q*255),uint(p*255),uint(g*255));
				default:	return	fromRGB(uint(g*255),uint(p*255),uint(q*255));
			}
			
			return 0;
		}
	}
}