package com.anychart.templates {
	import flash.utils.ByteArray;
	
	public final class GlobalTemplateItems {
		
		[Embed(source="../../../../presets/GlobalTemplateItems.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		private static var items:XMLList = buildGlobal(new XMLContent());
		private static function buildGlobal(content:*):XMLList {
			var b:ByteArray = content as ByteArray;			
			var data:XML = new XML(b.readUTFBytes(b.length));
			return data.item;
		}
		
		public static function createTemplateXML(strData:String):XML {
			var res:XML = new XML(strData);
			var includes:XMLList = res..includeGlobal;
			var cnt:uint = includes.length();
			for (var i:uint = 0;i<cnt;i++) {
				var item:XML = includes[0];
				var parent:XML = item.parent();
				var source:String = item.@key;
				var newItem:XML = GlobalTemplateItems.items.(@key == source)[0];
				parent.appendChild(newItem.children());
				delete includes[i];
			}
			return res;
		}
	}
}