package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlLayout;
	
	import flash.geom.Rectangle;
	
	
	internal final class VerticalInsideControlsLine extends VerticalControlsLine {
		
		public function VerticalInsideControlsLine(isOpposite:Boolean) {
			super(isOpposite);
		}
		
		override protected function finalizeNearOrFarLayout(controls:Array, space:Number):void {
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite)
					control.bounds.x = basePositionRect.right - control.bounds.width;
				else
					control.bounds.x = basePositionRect.left;
			}
		}
		
		override protected function finalizeCenterLayout(controls:Array, space:Number):void {
			var x:Number = this.isOpposite ? (basePositionRect.right) : (basePositionRect.left);
			
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite) {
					control.bounds.x = x - control.bounds.width;
					x -= control.bounds.width;
				}else {
					control.bounds.x = x;
					x += control.bounds.width;
				}
			}
		}
		
		override protected function setMaxBounds(layout:ControlLayout, chartRect:Rectangle, plotRect:Rectangle):void {
			layout.maxHeight = layout.alignByDataPlot ? plotRect.height : chartRect.height;
			layout.maxWidth = chartRect.width;
		}
	}
}