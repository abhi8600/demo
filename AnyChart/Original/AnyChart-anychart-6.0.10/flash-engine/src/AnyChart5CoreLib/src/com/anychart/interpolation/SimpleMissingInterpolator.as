package com.anychart.interpolation {
	public final class SimpleMissingInterpolator implements IMissingInterpolator {
		
		private var nonMissingPointsCount:uint;
		private var nonMissingPointsIndexes:Array;
		private var fieldName:String;
		
		private var interpolatedPoints:Array;
		
		public function initialize(fieldName:String):void {
			this.fieldName = fieldName;
			this.clear();
		}
		
		public function clear():void {
			this.interpolatedPoints = [];
			this.nonMissingPointsCount = 0;
			this.nonMissingPointsIndexes = [];
		}
		
		public function checkDuringDeserialize(point:Object, index:int):void {
			if (!this.isMissing(point)) {
				this.nonMissingPointsCount++;
				this.nonMissingPointsIndexes.push(index);
			}
		}
		
		public function interpolate(points:Array):Array {
			if (points.length == this.nonMissingPointsIndexes.length) return [];
			this.interpolatedPoints = [];
			if (this.nonMissingPointsCount == 0)
				this.interpolateNoValues(points);
			else if (this.nonMissingPointsCount == 1)
				this.interpolateSingleValue(points);
			else
				this.interpolateMulti(points);
				
			return this.interpolatedPoints;
		}
		
		private function isMissing(pt:Object):Boolean {
			return isNaN(pt[this.fieldName]);
		}
		
		private function interpolateNoValues(points:Array):void {
			for each (var pt:Object in points) {
				this.setPointValue(pt, 1);
			}
		}
		
		private function interpolateSingleValue(points:Array):void {
			var value:Number = points[this.nonMissingPointsIndexes[0]][fieldName];
			for each (var pt:Object in points) {
				this.setPointValue(pt, value);
			}
		}
		
		private function interpolateMulti(points:Array):void {
			
			this.nonMissingPointsIndexes.sort(Array.NUMERIC);
			
			//interpolate all bounded blocks [value, missing, ..., value]
			var i:uint;
			var startNonMissingIndex:int = this.nonMissingPointsIndexes[0];
			var endNonMissingIndex:int = this.nonMissingPointsIndexes[this.nonMissingPointsIndexes.length-1];
			for (i = 0;i<(this.nonMissingPointsIndexes.length - 1);i++) {
				var startIndex:int = this.nonMissingPointsIndexes[i];
				var endIndex:int = this.nonMissingPointsIndexes[i+1];
				if ((endIndex - startIndex) > 1)
					this.interpolateBoundedBlock(points, startIndex, endIndex);
			}
			
			if (startNonMissingIndex > 0)
				this.interpolateBoundedRight(points, startNonMissingIndex);
			if (endNonMissingIndex < (points.length - 1))
				this.interpolateBoundedLeft(points, endNonMissingIndex);
		}
		
		private function interpolateBoundedBlock(points:Array, startIndex:int, endIndex:int):void {
			var startValue:Number = points[startIndex][this.fieldName];
			var dValue:Number = points[endIndex][this.fieldName] - startValue;
			var dArgument:Number = endIndex - startIndex;
			
			var d:Number = dValue/dArgument;
			
			var start:int = startIndex+1;
			for (var i:int = start;i < endIndex;i++)
				this.setPointValue(points[i], startValue+(i-startIndex)*d);
		}
		
		private function interpolateBoundedRight(points:Array, startValueIndex:int):void {
			var value1:Number = points[startValueIndex][this.fieldName];
			var value2:Number = points[startValueIndex+1][this.fieldName];
			var d:Number = (value2 - value1);
			for (var i:int = (startValueIndex-1);i>=0;i--)
				this.setPointValue(points[i],value1 - d*(startValueIndex-i));
		}
		
		private function interpolateBoundedLeft(points:Array, endValueIndex:int):void {
			var value1:Number = points[endValueIndex][this.fieldName];
			var value2:Number = points[endValueIndex-1][this.fieldName];
			var d:Number = (value2 - value1);
			for (var i:int = (endValueIndex+1);i<points.length;i++)
				this.setPointValue(points[i], value2 - d*(i-endValueIndex+1));
		}
		
		private function setPointValue(point:Object, value:Number):void {
			if (point != null){
				point[this.fieldName] = value;
				this.interpolatedPoints.push(point);
			}
		}
	}
}