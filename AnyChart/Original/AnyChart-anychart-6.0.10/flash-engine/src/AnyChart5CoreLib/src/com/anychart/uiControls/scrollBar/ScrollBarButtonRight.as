package com.anychart.uiControls.scrollBar {
	import flash.geom.Rectangle;
	
	internal final class ScrollBarButtonRight extends ScrollBarButtonWithArrow {
		
		override protected function getArrowBounds():Rectangle {
			var iconSize:Number = Math.min(size.width, size.height) * 0.4;
			
			return new Rectangle(size.width/2-iconSize/4,
								 size.height/2-iconSize/2,
								 iconSize/2,
								 iconSize);
		}
		
		override protected function drawArrow():void {
			var s:Rectangle = size;
			var iconSize:Number = Math.min(s.width, s.height) * 0.4;
			
			var pX:Number=s.width/2-iconSize/4;
			var pY:Number=s.height/2-iconSize/2;
			
			var w:Number=iconSize/2;
			var h:Number=iconSize;
			
			graphics.moveTo(pX,pY);
			graphics.lineTo(pX+w,pY+h/2);
			graphics.lineTo(pX,pY+h);
			graphics.moveTo(pX,pY);
		}
	}
}