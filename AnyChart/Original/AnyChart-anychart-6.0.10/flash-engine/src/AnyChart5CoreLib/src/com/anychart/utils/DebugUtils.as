package com.anychart.utils {
	
	import flash.external.ExternalInterface;
	
	public class DebugUtils {
		public static function log(...msgs):void {
			ExternalInterface.call("console.log", "Flash: "+msgs.join(", ").toString());
		}
	}
}