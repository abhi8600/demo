package com.anychart.animation {
	import com.anychart.animation.interpolation.Interpolator;
	
	
	public final class AnimatableProperty {
		public var start:Number;
		public var end:Number;
		public var enabled:Boolean;
		public var d:Number;
		
		public var tempValue:Number;
		public var tempValue1:Number;
		
		public function AnimatableProperty(tempValue:uint) {
			this.tempValue = tempValue;
		}
		
		public function initialize(startValue:Number, endValue:Number):void {
			this.start = startValue;
			this.end = endValue;
			this.enabled = this.start != this.end;
			this.d = this.end - this.start;
		}
		
		public function getValue(i:Interpolator, time:Number, duration:Number):Number {
			return i.interpolate(time, this.start, this.d, duration);
		}
		
		public function isAnimated():Boolean {
			return this.start != this.end;
		}
	}
}