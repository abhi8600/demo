package com.anychart.uiControls.scrollBar {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.background.RectBackground;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.InteractiveObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	public class ScrollBarControl extends Sprite implements IScrollBar {
		
		private var container:InteractiveObject;
		private var background:RectBackground;
		
		public function ScrollBarControl(container:InteractiveObject) {
			this.container = container;
			this.minValue = 0;
			this.maxValue = 100;
			this.visValue = 1;
			this.scrollBarSize = 20;
		}
		
		public function destroy(e:Event = null):void {
			if (thumb) thumb.removeEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			if (buttonAdd) buttonAdd.removeEventListener(MouseEvent.MOUSE_DOWN, buttonAdd_onMouseDown);
			if (buttonSub) buttonSub.removeEventListener(MouseEvent.MOUSE_DOWN, buttonSub_onMouseDown);
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			
			if (container) {
				container.removeEventListener(MouseEvent.MOUSE_UP, breakThumbMove);
				container.removeEventListener(Event.MOUSE_LEAVE, breakThumbMove);
				container.removeEventListener(MouseEvent.MOUSE_MOVE, thumb_onMouseMove, false);
				container.removeEventListener(MouseEvent.MOUSE_UP, thumb_onMouseUp);
				container.removeEventListener(Event.MOUSE_LEAVE, thumb_onMouseLeave);
				if (thumb) thumb.removeEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			}
			
			clearInterval(thumbMoveIntervalId);
			clearInterval(thumbMoveTimeoutId);
			clearInterval(timeoutId);
			
			this.container = null;
			this.background = null;
			this.size = null;
			this.buttonSizeRect = null;
			
			if (this.buttonAdd)
				this.buttonAdd.destroy();
			
			if (this.buttonSub)
				this.buttonSub.destroy();
			
			if (this.thumb)
				this.thumb.destroy();
			
			if (this.bgShape && this.bgShape.parent)
				removeChild(bgShape);
			if (this.buttonSub && this.buttonSub.parent)
				removeChild(buttonSub);
			if (this.thumb && this.thumb.parent)
				removeChild(thumb);
			if (this.buttonAdd && this.buttonAdd.parent)
				removeChild(buttonAdd);
				
			this.bgShape = null;
			this.buttonAdd = null;
			this.buttonSub = null;
			this.thumb = null;
		}
		
		protected var state:uint;
		public var size:Rectangle;
		protected var _scrollBarSize:Number;
		public function get scrollBarSize():Number { return this._scrollBarSize; }
		public function set scrollBarSize(value:Number):void { this._scrollBarSize = value; }
		protected var buttonSizeRect:Rectangle;

		public function initialize(bounds:Rectangle):DisplayObject {
			this.setScrollBarSize(bounds);
			size = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
			
			this.bgShape = new Shape();
			
			if (this.buttonAdd)
				this.buttonAdd.container = this.container;
			if (this.buttonSub)
				this.buttonSub.container = this.container;
			if (this.thumb)
				this.thumb.container = this.container;
			
			if (thumb)
				thumb.initialize(bounds);
			if (buttonAdd)
				buttonAdd.initialize(bounds);
			if (buttonSub)
				buttonSub.initialize(bounds);
			resize(bounds);
			
			if (thumb)
				thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			if (buttonAdd)
				buttonAdd.addEventListener(MouseEvent.MOUSE_DOWN, buttonAdd_onMouseDown);
			if (buttonSub)
				buttonSub.addEventListener(MouseEvent.MOUSE_DOWN, buttonSub_onMouseDown);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(Event.REMOVED_FROM_STAGE, this.destroy);
			return this; 
		}
		
		private function onMouseDown(e:MouseEvent):void {
			if (thumb == null || !thumb.visible || e.target != this) return;
			thumbMoveLastPoint = getMouseCoord(e);
			var v:Number = thumbMoveLastPoint;
			var t:Number = getThumbPos();
			if (v < t) moveThumb(false, visValue);
			if (v > t + thumbSize) moveThumb(true, visValue);
			this.startListenContainer();
		}
		
		private function startListenContainer():void {
			container.addEventListener(MouseEvent.MOUSE_UP, breakThumbMove);
			container.addEventListener(Event.MOUSE_LEAVE, breakThumbMove);
		}
		
		private function startListenThumb():void {
			container.addEventListener(MouseEvent.MOUSE_MOVE, thumb_onMouseMove, false, 1000);
			container.addEventListener(MouseEvent.MOUSE_UP, thumb_onMouseUp);
			container.addEventListener(Event.MOUSE_LEAVE, thumb_onMouseLeave);
		}
		
		public var step:Number = 5;
		public var isPercentStep:Boolean = false;
		
		protected function invertStep():void 
		{
			// для инвертированных шкал
			step = (step < 0) ? step : step*(-1);  
		}
		
		
		protected function buttonAdd_onMouseDown(e:MouseEvent):void {
			thumbMoveLastPoint = getLength()- currentButtonSize;
			moveThumb(true, step);
			this.startListenContainer();
		}

		protected function buttonSub_onMouseDown(e:MouseEvent):void {
			thumbMoveLastPoint = currentButtonSize;
			moveThumb(false, step);
			this.startListenContainer();
		}
		
		private var thumbMoveLastPoint:Number;
		private var thumbMoveTimeoutId:uint;
		private var thumbMoveIntervalId:uint;
		
		private function breakThumbMove(e:Event):void {
			clearTimeout(thumbMoveTimeoutId);
			clearInterval(thumbMoveIntervalId);
			removeEventListener(MouseEvent.MOUSE_UP, breakThumbMove);
			container.removeEventListener(MouseEvent.MOUSE_UP, breakThumbMove);
			container.removeEventListener(Event.MOUSE_LEAVE, breakThumbMove);
		}

		private static const DELAY_THUMB_MOVE_START:Number = 300;
		private static const DELAY_THUMB_MOVE_INTERVAL:Number = 40;
		
		private function moveThumb(isIncrease:Boolean, step:Number):void {
			thumbMoveStep(isIncrease, step);
			thumbMoveTimeoutId = setTimeout(startThumbMove, DELAY_THUMB_MOVE_START, isIncrease, step);
		}
		
		private function startThumbMove(isIncrease:Boolean, step:Number):void {
			clearTimeout(thumbMoveTimeoutId);
			if (checkThumbMove(isIncrease))
				thumbMoveIntervalId = setInterval(thumbMoveStep, DELAY_THUMB_MOVE_INTERVAL, isIncrease, step);
		}
		
		private function thumbMoveStep(isIncrease:Boolean, step:Number):void {
			var startValue:Number = currentValue;
			if (Math.abs(step)>100 && isPercentStep) step= step > 0 ? 12 : -12;
			step = isPercentStep ? ((step/100)*Math.abs(this.maxValue - this.minValue)) : step;
			currentValue = isIncrease ? (currentValue + step) : (currentValue - step);
			
			setThumbPosition();
			checkThumb();
			dispatch();
			checkThumbMove(isIncrease);
		}
		
		private function checkThumbMove(isIncrease:Boolean):Boolean {
			var p:Number = getThumbPos();
			var isThumbVisable:Boolean = thumb && thumb.visible;
			var isLeaveBounds:Boolean = currentValue < minValue || currentValue > maxValue - visValue;
			var isLeaveIncrease:Boolean = isThumbVisable && isIncrease && p + thumbSize >= thumbMoveLastPoint;
			var isLeaveDecrease:Boolean = isThumbVisable && !isIncrease && p <= thumbMoveLastPoint;
			var isLeave:Boolean = isLeaveBounds || isLeaveIncrease || isLeaveDecrease;
			if (isLeave) breakThumbMove(null);
			return !isLeave;
		}
		
		
		public function thumb_onMouseDown(e:MouseEvent):void {
			if (!thumb) return;
			thumb.startDrag(false, getThumbDragRect());
			thumb.removeEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			this.startListenThumb();
			onDragProcess();
			
		}
		
		protected function thumb_onMouseMove(e:MouseEvent):void {
			e.stopImmediatePropagation();
			e.stopPropagation();
			onDragProcess();
		}
		
		protected function thumb_onMouseUp(e:MouseEvent):void {
			if (!thumb) return;
			thumb.stopDrag();
			onDragProcess();
			container.removeEventListener(MouseEvent.MOUSE_MOVE, thumb_onMouseMove);
			container.removeEventListener(MouseEvent.MOUSE_UP, thumb_onMouseUp);
			container.removeEventListener(Event.MOUSE_LEAVE, thumb_onMouseLeave);
			thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
		}

		private function thumb_onMouseLeave(e:Event):void {
			thumb_onMouseUp(null);
		}
		
		private function onDragProcess():void {
			currentValue = getCurrentValue();
			checkThumb();
			dispatch();
		}

		private var timeoutId:uint = 0;
		private static const DISPATH_TIMEOUT:Number = 1;
		
		protected var currentValue:Number = 0;

		private function dispatch():void {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(createEvent, DISPATH_TIMEOUT);
		}
		
		private function createEvent():void {
			dispatchEvent(new ScrollBarEvent(ScrollBarEvent.CHANGE, currentValue));
		}
		
		protected var buttonAdd:ScrollBarButtonWithArrow;
		protected var buttonSub:ScrollBarButtonWithArrow;
		protected var thumb:ScrollBarButton;
		
		private var bgShape:Shape;

		public function draw():void {
			while (numChildren > 0) removeChildAt(0);
			
			var g:Graphics = this.graphics;
			g.clear();
			bgShape.graphics.clear();
			
			if (this.background != null) {
				this.background.draw(bgShape.graphics, new Rectangle(0,0,size.width,size.height));
				this.bgShape.filters = this.background.effects == null ? null : this.background.effects.list;
			}
				
			buttonAdd.draw();
			buttonSub.draw();
			thumb.draw();
			
			addChild(bgShape);
			addChild(buttonSub);
			addChild(thumb);
			addChild(buttonAdd);
		}
		
		protected var currentButtonSize:Number = 20;
		protected var buttonSize:Number = 20;
		protected var thumbSize:Number = 40;
		protected var thumbBonus:Number = 0;

		public function resize(newBounds:Rectangle):void {
			this.setScrollBarSize(newBounds);
			var s:Rectangle = size;
			s.x = newBounds.x;
			s.y = newBounds.y;
			s.width = newBounds.width;
			s.height = newBounds.height;
			this.x = s.x;
			this.y = s.y;
			refresh();
			resizeButtons();
			checkThumb();
			draw();
		}
		
		protected var minValue:Number;
		protected var maxValue:Number;
		protected var visValue:Number;
		
		public function set min(value:Number):void { minValue = value; }
		public function get min():Number { return minValue; }
		public function set max(value:Number):void { maxValue = value; }
		public function get max():Number { return maxValue; }
		public function set visibleRange(value:Number):void {
			visValue = value;
			refresh();
			setThumbPosition();
			checkThumb();
			dispatch();
		}
		public function get visibleRange():Number { return visValue; }
		public function get value():Number { return currentValue; }
		public function set value(value:Number):void {
			currentValue = value;
			setThumbPosition();
			checkThumb();
			dispatch();
		}
		
		protected var pixelsPerValue:Number;
		protected static const MIN_THUMB_SIZE:Number = 16;

		protected var dragRect:Rectangle;
		
		protected function refresh():void {
			currentButtonSize = buttonSize;
			var w:Number = getLength() - 2 * buttonSize;
			pixelsPerValue = w / (maxValue - minValue);
			thumbSize = pixelsPerValue * visValue;
			if (w < MIN_THUMB_SIZE) {
				if (thumb) thumb.visible = false;
				if (w < 0) currentButtonSize = getLength() * 0.5; 
			} else {
				if (thumb) thumb.visible = true;
				if (thumbSize < MIN_THUMB_SIZE) {
					thumbBonus = MIN_THUMB_SIZE - thumbSize;
					thumbSize = MIN_THUMB_SIZE;
				} else {
					thumbBonus = 0;
				}
			}
		}

		protected function getCurrentValue():Number {
			var w:Number = getLength() - 2 * buttonSize - thumbBonus;
			var d:Number = maxValue - minValue;
			var p:Number = getThumbPos() - buttonSize;
			return minValue + p * d / w;
		}
		
		protected function checkThumb():void {
			if (thumb == null || !thumb.visible) return;
			var w:Number = getLength() - buttonSize - thumbSize;// - thumbBonus;
			var p:Number = getThumbPos();
			if (p < buttonSize) {
				currentValue = minValue;
				setThumbPos(buttonSize);
			} else if (p > w || w - p < 1) {
				currentValue = maxValue - visValue;
				setThumbPos(w);
			}
		}

		protected function setThumbPosition():void {
			if (!thumb || !thumb.visible || size == null) return;
			var w:Number = getLength() - 2 * currentButtonSize - thumbBonus;
			var d:Number = maxValue - minValue;
			setThumbPos(currentButtonSize + (currentValue - minValue) * w / d);
		}

		protected function getMouseCoord(e:MouseEvent):Number { throw new Error("Call an abstruct method()"); }
		protected function getLength():Number { throw new Error("Call an abstruct method()"); }
		protected function setThumbPos(value:Number):void { throw new Error("Call an abstruct method()"); }
		protected function getThumbPos():Number { throw new Error("Call an abstruct method()"); }
		protected function getThumbDragRect():Rectangle { throw new Error("Call an abstruct method()"); }
		protected function resizeButtons():void { throw new Error("Call an abstruct method()"); }
		protected function setScrollBarSize(bounds:Rectangle):void {}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			this.background = new RectBackground();
			this.background.deserialize(data, resources);
			
			if (data.@size != undefined) this.scrollBarSize = Number(data.@size);
			this.buttonSize = this.scrollBarSize;
			this.thumbSize = this.scrollBarSize;
			
			//deserialize buttons background
			var buttonsNormalBackground:RectBackground = new RectBackground();
			var buttonsNormalArrowBackground:BackgroundBase = new BackgroundBase();
			var buttonsHoverBackground:RectBackground = new RectBackground();
			var buttonsHoverArrowBackground:BackgroundBase = new BackgroundBase();
			var buttonsPushedBackground:RectBackground = new RectBackground();
			var buttonsPushedArrowBackground:BackgroundBase = new BackgroundBase();
			
			var normalXML:XML;
			var hoverXML:XML;
			var pushedXML:XML;
			if (data.buttons[0] != null) {
				normalXML = data.buttons[0];
				hoverXML = data.buttons[0];
				pushedXML = data.buttons[0];
				
				if (data.buttons[0].states[0] != null) {
					normalXML = XMLUtils.merge(normalXML, data.buttons[0].states[0].normal[0]);
					hoverXML = XMLUtils.merge(hoverXML, data.buttons[0].states[0].hover[0]);
					pushedXML = XMLUtils.merge(pushedXML, data.buttons[0].states[0].pushed[0]);
				} 
				
				buttonsNormalBackground.deserialize(normalXML.background[0], resources);
				buttonsNormalArrowBackground.deserialize(normalXML.arrow[0], resources);
				
				buttonsHoverBackground.deserialize(hoverXML.background[0], resources);
				buttonsHoverArrowBackground.deserialize(hoverXML.arrow[0], resources);
				
				buttonsPushedBackground.deserialize(pushedXML.background[0], resources);
				buttonsPushedArrowBackground.deserialize(pushedXML.arrow[0], resources);
			}
			
			var thumbNormalBackground:RectBackground = new RectBackground();
			var thumbHoverBackground:RectBackground = new RectBackground();
			var thumbPushedBackground:RectBackground = new RectBackground();
			
			var thumbXML:XML = data.thumb[0];
			if (thumbXML != null) {
				
				normalXML = thumbXML;
				hoverXML = thumbXML;
				pushedXML = thumbXML;
				
				if (thumbXML.states[0] != null) {
					normalXML = XMLUtils.merge(normalXML, thumbXML.states[0].normal[0]);
					hoverXML = XMLUtils.merge(hoverXML, thumbXML.states[0].hover[0]);
					pushedXML = XMLUtils.merge(pushedXML, thumbXML.states[0].pushed[0]);
				}
				
				if (normalXML.background[0] != null)
					thumbNormalBackground.deserialize(normalXML.background[0], resources);
				if (hoverXML.background[0] != null)
					thumbHoverBackground.deserialize(hoverXML.background[0], resources);
				if (pushedXML.background[0] != null)
					thumbPushedBackground.deserialize(pushedXML.background[0], resources);
			}
			
			this.buttonSub.normalBackground = this.buttonAdd.normalBackground = buttonsNormalBackground;
			this.buttonSub.hoverBackground = this.buttonAdd.hoverBackground = buttonsHoverBackground;
			this.buttonSub.pushedBackground = this.buttonAdd.pushedBackground = buttonsPushedBackground;
			this.thumb.normalBackground = thumbNormalBackground;
			this.thumb.hoverBackground = thumbHoverBackground;
			this.thumb.pushedBackground = thumbPushedBackground;
			
			this.buttonSub.arrowNormalBackground = this.buttonAdd.arrowNormalBackground = buttonsNormalArrowBackground;
			this.buttonSub.arrowHoverBackground = this.buttonAdd.arrowHoverBackground = buttonsHoverArrowBackground; 
			this.buttonSub.arrowPushedBackground = this.buttonAdd.arrowPushedBackground = buttonsPushedArrowBackground;
		}
		
	}
}