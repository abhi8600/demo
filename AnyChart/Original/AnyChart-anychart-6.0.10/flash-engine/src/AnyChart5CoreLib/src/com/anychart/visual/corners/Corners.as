package com.anychart.visual.corners {
	
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class Corners implements ISerializable {
		
		private var type:uint=CornersType.SQUARE;
		
		public var leftTop:Number=0;
		public var rightTop:Number=0;
		public var rightBottom:Number=0;
		public var leftBottom:Number=0;
		
		public function set all(value:Number):void {
			this.leftTop = this.rightTop = this.rightBottom = this.leftBottom = value;
		}
		
		public function createCopy(res:Corners = null):Corners {
			if (res == null)
				res = new Corners();
			res.leftTop = this.leftTop;
			res.rightTop = this.rightTop;
			res.leftBottom = this.leftBottom;
			res.rightBottom = this.rightBottom;
			res.type = this.type;
			return res;
		}
		
		public function deserialize(data:XML):void {
			
			if (data.@type!=undefined) {
				var type:String = SerializerBase.getLString(data.@type);
				switch (type) {
					case "square": this.type = CornersType.SQUARE; break;
					case "rounded": this.type = CornersType.ROUNDED; break;
					case "roundedinner": this.type = CornersType.ROUNDED_INNER; break;
					case "cut": this.type = CornersType.CUT; break;
				}
			}
			if (this.type==CornersType.SQUARE) return;
			
			if (data.@all!=undefined) this.leftTop = this.rightTop = this.rightBottom = this.leftBottom = SerializerBase.getNumber(data.@all);
			if (data.@left_top!=undefined) this.leftTop=SerializerBase.getNumber(data.@left_top);
			if (data.@right_top!=undefined) this.rightTop=SerializerBase.getNumber(data.@right_top);
			if (data.@right_bottom!=undefined) this.rightBottom=SerializerBase.getNumber(data.@right_bottom);
			if (data.@left_bottom!=undefined) this.leftBottom=SerializerBase.getNumber(data.@left_bottom);
		}
		
		public function drawRect(g:Graphics, rc:Rectangle):void {
			switch(this.type) {
				case CornersType.SQUARE:
					g.drawRect(rc.x,rc.y,rc.width,rc.height);
					break;
				case CornersType.ROUNDED:
					g.drawRoundRectComplex(rc.x,rc.y,rc.width,rc.height,this.leftTop,this.rightTop,this.leftBottom,this.rightBottom);
					break;
						
				case CornersType.CUT:
					g.moveTo(rc.x + this.leftTop, rc.y);
					g.lineTo(rc.x + rc.width - this.rightTop, rc.y);
					g.lineTo(rc.x + rc.width, rc.y + this.rightTop);
					g.lineTo(rc.x + rc.width, rc.y + rc.height - this.rightBottom);
					g.lineTo(rc.x + rc.width - this.rightBottom, rc.y + rc.height);
					g.lineTo(rc.x + this.leftBottom,rc.y + rc.height);
					g.lineTo(rc.x,rc.y + rc.height - this.leftBottom);
					g.lineTo(rc.x,rc.y + this.leftTop);
					g.lineTo(rc.x + this.leftTop, rc.y);
					break;
					
				case CornersType.ROUNDED_INNER:
					g.moveTo(rc.x, rc.y + this.leftTop);
					DrawingUtils.drawCircleSector(g,rc.x,rc.y + rc.height, this.leftBottom, 3);
					DrawingUtils.drawCircleSector(g,rc.x + rc.width,rc.y + rc.height, this.rightBottom, 2);
					DrawingUtils.drawCircleSector(g,rc.x + rc.width, rc.y,this.rightTop,1);
					DrawingUtils.drawCircleSector(g,rc.x, rc.y,this.leftTop,0);
					g.lineTo(rc.x,rc.y + this.leftTop);
					break;
			}	
		}

	}
}