package com.anychart.visual.fill
{
	public final class FillType
	{
		public static const SOLID:uint=0;
		public static const GRADIENT:uint=1;
		public static const IMAGE:uint=2;

	}
}