package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	
	internal final class UpdateChartAction extends ChartAction {
		
		public function UpdateChartAction(chart:IAnyChart) {
			super(chart);
		}
		
		override public function execute(formatter:IFormatable):Boolean {
			if (this.sourceMode == SOURCE_EXTERNAL)
				chart.setXMLFile(this.getSource(formatter), this.getReplace(formatter), false);
			else
				chart.setSource(this.getSource(formatter), this.getReplace(formatter), false);
			return true;
		}

	}
}