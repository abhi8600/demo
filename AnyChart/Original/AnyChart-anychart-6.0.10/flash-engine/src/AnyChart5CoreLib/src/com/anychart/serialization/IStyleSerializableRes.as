package com.anychart.serialization {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	
	public interface IStyleSerializableRes {
		function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void; 
	}
}