package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	import flash.events.SecurityErrorEvent;
	import flash.external.ExternalInterface;
	
	
	internal final class CallAction extends Action {
		
		private var isDynamicFunctionName:Boolean;
		private var functionName:String;
		private var functionNameFormater:TextFormater;
		
		private var args:Array;
		private var hasDynamicArguments:Boolean;
		
		public function CallAction(chart:IAnyChart) {
			super(chart);
		}
		
		override public function execute(formater:IFormatable):Boolean {
			
			var actualArgs:Array = this.args;
			if (this.hasDynamicArguments) {
				actualArgs = [];
				for (var i:uint = 0;i<this.args.length;i++) {
					if (this.args[i] is TextFormater) {
						actualArgs.push(TextFormater(this.args[i]).getValue(formater.getTokenValue, formater.isDateTimeToken));
					}else {
						actualArgs.push(this.args[i]);
					}
				}
			}
			
			var functionName:String = this.functionName;
			if (this.isDynamicFunctionName) {
				functionName = this.functionNameFormater.getValue(formater.getTokenValue, formater.isDateTimeToken);
			}
			
			try {
				if (ExternalInterface.available) {
					ExternalInterface.call.apply(null,[functionName].concat(actualArgs));
				}
			}catch (e:SecurityError) {
				this.chart.showSecurityError(new SecurityErrorEvent(SecurityErrorEvent.SECURITY_ERROR));
			}
			
			return false;
		}
		
		override public function deserialize(data:XML):void {
			if (data['@function'] != undefined)
				this.functionName = SerializerBase.getString(data['@function']);
			this.isValidXML = this.functionName != null;
			if (!this.isValidXML) return;
			
			this.isDynamicFunctionName = FormatsParser.isDynamic(this.functionName);
			if (this.isDynamicFunctionName)
				this.functionNameFormater = FormatsParser.parse(this.functionName);
			
			this.hasDynamicArguments = false;
			if (data.arg != null && data.arg.length() > 0) {
				this.args = [];
				var argCnt:int = data.arg.length();
				for (var i:int = 0;i<argCnt;i++) {
					var act:String = SerializerBase.getCDATAString(data.arg[i]);
					if (FormatsParser.isDynamic(act)) {
						this.hasDynamicArguments = true;
						this.args.push(FormatsParser.parse(act));
					}else {
						this.args.push(act);
					}
				}
			}
		}

	}
}