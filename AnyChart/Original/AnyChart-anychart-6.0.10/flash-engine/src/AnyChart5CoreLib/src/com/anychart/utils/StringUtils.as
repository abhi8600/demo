package com.anychart.utils {
	
	public final class StringUtils {
		
		public static var LINE_BREAK:String = "\n";
		
		/**
		 * Получает количество столбцов и строчек в строке
		 * @param text String
		 * @return [width, height]
		 */
		public static function getSymbolMetrics(text:String):Array {
			if (text == null) return [0,0];
			var w:uint = 0;
			var h:uint = 0;
			var lineWidth:int;
			
			var prevIndex:int = 0;
			var index:int = text.indexOf(LINE_BREAK);
			while (index != -1) {
				lineWidth = index - prevIndex;
				if (lineWidth > w) w = lineWidth;
				prevIndex = index+1;
				h++;
				index = text.indexOf(LINE_BREAK, prevIndex);
			}
			
			return [w,h];
		}
		
		/*
		Это самый быстрый replace
		на 1 000 000 вызовов:
		почти на 10 секунд быстрее чем StringUtil.substitue
		на 2 секунды быстрее чем String.replace
		 */
		public static function replace(string:String, what:String, repl:String):String {
			return string.split(what).join(repl);
		}
	}
}