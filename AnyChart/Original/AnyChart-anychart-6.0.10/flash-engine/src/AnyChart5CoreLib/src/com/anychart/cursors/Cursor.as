package com.anychart.cursors {
	import flash.display.Sprite;
	
	public final class Cursor {
		private static var height:Number = 14;
		public static function get realHeight():Number { return height; }
		private static var width:Number = 14;
		public static function get realWidth():Number { return width; }
		
		private static var heightMultiplier:Number = 1;
		private static var widthMultipliter:Number = 1;
		
		[Embed(source="./../../../../assets/cursor/lapka_drag.png")]
		private static var Cursor2DDrag:Class;
		
		[Embed(source="./../../../../assets/cursor/lapka_normal.png")]
		private static var Cursor2D:Class;
		
		public static function drawMoveHorizontal(container:Sprite, isClicked:Boolean):void {
			while(container.numChildren>0) container.removeChildAt(0);
			container.addChild(isClicked ? new Cursor2DDrag() : new Cursor2D());
		}
		
		public static function drawMoveVertical(container:Sprite, isClicked:Boolean):void {
			while(container.numChildren>0) container.removeChildAt(0);
			container.addChild(isClicked ? new Cursor2DDrag() : new Cursor2D());
		}
		
		public static function drawMove2D(container:Sprite, isClicked:Boolean):void {
			while(container.numChildren>0) container.removeChildAt(0);
			container.addChild(isClicked ? new Cursor2DDrag() : new Cursor2D());
		}		
	}
}