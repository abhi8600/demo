package com.anychart {
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	
	public interface IResizable {
		function initialize(bounds:Rectangle):DisplayObject;
		function draw():void;
		function resize(newBounds:Rectangle):void;
	}
}