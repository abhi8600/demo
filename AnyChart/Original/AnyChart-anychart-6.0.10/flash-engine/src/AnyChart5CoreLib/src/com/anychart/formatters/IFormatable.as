package com.anychart.formatters {
	
	public interface IFormatable {
		function getTokenValue(token:String):*;
		function isDateTimeToken(token:String):Boolean;
	}
}