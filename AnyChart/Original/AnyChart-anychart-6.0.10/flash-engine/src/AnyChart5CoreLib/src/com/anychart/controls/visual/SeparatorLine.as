package com.anychart.controls.visual {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.stroke.Stroke;
	
	public final class SeparatorLine extends Stroke {
		public var padding:Number;
		
		public function SeparatorLine() {
			super();
			this.padding = 1;
		}
		
		override public function deserialize(data:XML, style:IStyle=null):void {
			super.deserialize(data, style);
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
		}
	}
}