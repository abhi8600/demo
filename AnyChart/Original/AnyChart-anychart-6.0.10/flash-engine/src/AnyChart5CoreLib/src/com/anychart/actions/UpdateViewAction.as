package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	internal final class UpdateViewAction extends ChartAction {
		
		private var isDynamicViewId:Boolean;
		private var viewIdFormater:TextFormater;
		private var viewId:String;
		
		public function UpdateViewAction(chart:IAnyChart) {
			super(chart);
		}
		
		override public function execute(formater:IFormatable):Boolean {
			
			var view:String = this.isDynamicViewId ? this.viewIdFormater.getValue(formater.getTokenValue, formater.isDateTimeToken) : this.viewId;
			
			if (this.sourceMode == SOURCE_EXTERNAL)
				this.chart.setViewXMLFile(view, this.getSource(formater), this.getReplace(formater));
			else
				this.chart.setViewInternalSource(view, this.getSource(formater), this.getReplace(formater));
			return false;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.isValidXML = this.isValidXML && (data.@view != undefined || data.@view_id != undefined);
			if (this.isValidXML) {
				this.viewId = data.@view_id != undefined ? SerializerBase.getString(data.@view_id) : SerializerBase.getString(data.@view);
				this.isDynamicViewId = FormatsParser.isDynamic(this.viewId);
				if (this.isDynamicViewId)
					this.viewIdFormater = FormatsParser.parse(this.viewId);
			}
		}

	}
}