package com.anychart.animation {
	import com.anychart.animation.event.AnimationEvent;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	[Event(name="animationFinish", type="com.anychart.animation.event.AnimationEvent")]
	
	public final class AnimationManager extends EventDispatcher {
		
		public var enabled:Boolean;
		public var targets:Array;
		
		public var startTime:Number;
		private var endTime:Number;
		
		public var loop:Boolean;
		public var loopDelay:Number;
		private var isInLoop:Boolean;
		
		public function AnimationManager() {
			this.targets = [];
			this.enabled = true;
			this.startTime = 0;
			this.endTime = 0;
			this.loop = false;
			this.loopDelay = 5;
			this.isInLoop = false;
		}
		
		public function isAnimated():Boolean {
			return this.enabled && this.targets && this.targets.length > 0;
		}
		
		public function destroy():void {
			for each (var target:AnimationTarget in this.targets)
				target.destroy();
			this.targets = null;
			for each (var timeoutId:int in this.timeouts)
				clearTimeout(timeoutId);
			this.timeouts = null;
			for each (var intervalObj:Object in this.intervals){
				 if (intervalObj)
				 	clearInterval(intervalObj.interval);
			}
				 
			this.intervals = null;
		}
		
		public function registerCallBack(targetFunction:Function, startValue:Number, endValue:Number, animation:Animation):void {
			if (!this.enabled) return;
			var t:AnimationFunctionTarget = new AnimationFunctionTarget();
			t.start = startValue;
			t.end = endValue;
			t.callBack = targetFunction;
			t.animation = animation;
			
			this.addTargetToTimeMap(t);
		}
		
		public function registerDisplayObject(container:DisplayObject, mainContainer:DisplayObject, animation:Animation, applyTimeOffset:Boolean = true, useRect:Boolean = true):void {
			if (!this.enabled) return;
			if (!applyTimeOffset) animation.isMain = true;
			this.addTargetToTimeMap(this.createDisplayObjectTarget(container, mainContainer, animation, useRect));
		}
		
		public function registerCustomDisplayObject(container:DisplayObject, mainContainer:DisplayObject, animation:Animation, target:AnimationDisplayObjectComplexTarget):void {
			if (!this.enabled) return;
			this.configTarget(container, mainContainer, animation, target);
			this.addTargetToTimeMap(target);
		}
		
		private function configTarget(container:DisplayObject, mainContainer:DisplayObject, animation:Animation, target:AnimationDisplayObjectComplexTarget):void {
			target.animation = animation;
			target.container = container;
			target.mainContainer = mainContainer;
		}
		
		private function createDisplayObjectTarget(container:DisplayObject, mainContainer:DisplayObject, animation:Animation, useRect:Boolean):AnimationDisplayObjectComplexTarget {
			
			var a:AnimationDisplayObjectComplexTarget = new AnimationDisplayObjectComplexTarget();
			
			a.useRect = useRect;
			this.configTarget(container, mainContainer, animation, a);
			
			switch (animation.type) {
				case AnimationType.SIDE_FROM_LEFT: 
					a.setX(HorizontalAlign.LEFT);
					break;
					
				case AnimationType.SIDE_FROM_RIGHT: 
					a.setX(HorizontalAlign.RIGHT);
					break;
					
				case AnimationType.SIDE_FROM_TOP: 
					a.setY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SIDE_FROM_BOTTOM: 
					a.setY(VerticalAlign.BOTTOM);
					break;
				
				case AnimationType.SIDE_FROM_LEFT_TOP: 
					a.setX(HorizontalAlign.LEFT);
					a.setY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_TOP: 
					a.setX(HorizontalAlign.RIGHT);
					a.setY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_BOTTOM: 
					a.setX(HorizontalAlign.RIGHT);
					a.setY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SIDE_FROM_LEFT_BOTTOM: 
					a.setX(HorizontalAlign.LEFT);
					a.setY(VerticalAlign.BOTTOM);
					break;
					
				
				case AnimationType.SIDE_FROM_LEFT_CENTER: 
					a.setX(HorizontalAlign.LEFT);
					a.setY(VerticalAlign.CENTER);
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_CENTER: 
					a.setX(HorizontalAlign.RIGHT);
					a.setY(VerticalAlign.CENTER);
					break;
					
				case AnimationType.SIDE_FROM_TOP_CENTER: 
					a.setX(HorizontalAlign.CENTER);
					a.setY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SIDE_FROM_BOTTOM_CENTER: 
					a.setX(HorizontalAlign.CENTER);
					a.setY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SCALE_X_CENTER:
					a.setScaleX(HorizontalAlign.CENTER);
					break;
					
				case AnimationType.SCALE_X_LEFT:
					a.setScaleX(HorizontalAlign.LEFT);
					break;
					
				case AnimationType.SCALE_X_RIGHT:
					a.setScaleX(HorizontalAlign.RIGHT);
					break;
					
				case AnimationType.SCALE_XY_BOTTOM:
					a.setScaleX(HorizontalAlign.CENTER);
					a.setScaleY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SCALE_XY_CENTER:
					a.setScaleX(HorizontalAlign.CENTER);
					a.setScaleY(VerticalAlign.CENTER);
					break;
					
				case AnimationType.SCALE_XY_LEFT:
					a.setScaleX(HorizontalAlign.LEFT);
					a.setScaleY(VerticalAlign.CENTER);
					break;
					
				case AnimationType.SCALE_XY_LEFT_BOTTOM:
					a.setScaleX(HorizontalAlign.LEFT);
					a.setScaleY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SCALE_XY_LEFT_TOP:
					a.setScaleX(HorizontalAlign.LEFT);
					a.setScaleY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SCALE_XY_RIGHT:
					a.setScaleX(HorizontalAlign.RIGHT);
					a.setScaleY(VerticalAlign.CENTER);
					break;
					
				case AnimationType.SCALE_XY_RIGHT_BOTTOM:
					a.setScaleX(HorizontalAlign.RIGHT);
					a.setScaleY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SCALE_XY_RIGHT_TOP:
					a.setScaleX(HorizontalAlign.RIGHT);
					a.setScaleY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SCALE_XY_TOP:
					a.setScaleX(HorizontalAlign.CENTER);
					a.setScaleY(VerticalAlign.TOP);
					break;
					
				case AnimationType.SCALE_Y_BOTTOM:
					a.setScaleY(VerticalAlign.BOTTOM);
					break;
					
				case AnimationType.SCALE_Y_CENTER:
					a.setScaleY(VerticalAlign.CENTER);
					break;
				
				case AnimationType.SCALE_Y_TOP:
					a.setScaleY(VerticalAlign.TOP);
					break;
				
				case AnimationType.SHOW:
				case AnimationType.APPEAR:
					a.setOpacity();
					break;
			}
			
			if (animation.animateOpacity)
				a.setOpacity();
			
			return a;
		}
		
		private function addTargetToTimeMap(target:AnimationTarget):void {
			
			var startTime:int = target.animation.startTime;
			var endTime:int = target.animation.startTime + target.animation.duration;
			this.endTime = Math.max(this.endTime, endTime);
			
			this.targets.push(target);
		}
		
		private var baseTime:Number;
		private var time:int;
		private var intervals:Array;
		private var timeouts:Array;
		public var allowrepeat:Boolean;
		
		private var bounds:Rectangle;
		public function animate(bounds:Rectangle):void {
			this.bounds = bounds;
			if (!this.enabled) return;
			this.intervals = new Array(this.targets.length);
			this.timeouts = new Array(this.targets.length);
			this.allowrepeat=false;
			
			var item:AnimationTarget;
			for each (item in this.targets) {
				item.initialize();
				if (item is AnimationDisplayObjectComplexTarget && AnimationDisplayObjectComplexTarget(item).useRect) {
					AnimationDisplayObjectComplexTarget(item).mainContainerWidth = bounds.width;
					AnimationDisplayObjectComplexTarget(item).mainContainerHeight = bounds.height;
				}
			}
			
			for (var i:uint = 0;i<this.targets.length;i++) {
				item = this.targets[i];
				item.setInitial();
				this.timeouts[i] = setTimeout(this.startAnimation, item.animation.startTime + (item.animation.isMain ? 0 : this.startTime), item, i);
			}
			this.timeouts.push(setTimeout(this.finalizeAll, this.endTime + this.startTime));
		}
		
		private function startAnimation(item:AnimationTarget, intervalIndex:uint):void {
			this.intervals[intervalIndex] = {
				startTime: new Date().getTime(), 
				item: item,
				interval: setInterval(execItemAnimation, 1, item, intervalIndex) 
			};
		}
		
		private function execItemAnimation(item:AnimationTarget, intervalIndex:uint):void {
			if (!item) {
				clearInterval(intervals[intervalIndex].interval);
				return;
			}
			var time:Number = 0;
			if (intervalIndex>-1 && this.intervals[intervalIndex]!=null) time=new Date().getTime()- this.intervals[intervalIndex].startTime;
			if (time >= item.animation.duration) {
				item.setFinal();
				clearInterval(this.intervals[intervalIndex].interval);
				this.intervals[intervalIndex] = null;
			}else {
				item.animate(time);
			}
		}
		
		private function finalizeAll():void {
			for each (var interval:Object in this.intervals) {
				if (interval != null) {
					clearInterval(interval.interval);
					interval.item.setFinal();
				}
			}
			this.intervals = null;
			this.timeouts=null;
			this.allowrepeat=true;
			/*if (!this.loop)
				this.targets = null;
			*/
			if (!this.isInLoop)
				this.dispatchEvent(new AnimationEvent(AnimationEvent.ANIMATION_FINISH));
			
			if (this.loop) {
				this.isInLoop = true;
				setTimeout(this.animate, this.loopDelay*1000, this.bounds);
			}
		}
		
		public function reanimate():void
		{
			if(this.allowrepeat)
				this.animate(this.bounds);
		}
	}
}