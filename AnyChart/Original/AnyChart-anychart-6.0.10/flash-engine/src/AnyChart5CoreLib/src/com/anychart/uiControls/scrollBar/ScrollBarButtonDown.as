package com.anychart.uiControls.scrollBar
{
	import flash.geom.Rectangle;
	
	internal final class ScrollBarButtonDown extends ScrollBarButtonWithArrow {
		
		override protected function getArrowBounds():Rectangle {
			var iconSize:Number = Math.min(size.width, size.height)/2.5;
			
			return new Rectangle(size.width/2-iconSize/2, 
								 size.height/2-iconSize/4,
								 iconSize,
								 iconSize/2);
		}
		
		override protected function drawArrow():void {
			var s:Rectangle = size;
			
			var iconSize:Number = Math.min(s.width, s.height)/2.5;
			
			var pX:Number = s.width/2-iconSize/2;
			var pY:Number = s.height/2-iconSize/4;
			
			var w:Number = iconSize;
			var h:Number = iconSize/2;
			
			graphics.moveTo(pX,pY);
			graphics.lineTo(pX+w,pY);
			graphics.lineTo(pX+w/2,pY+h);
			graphics.lineTo(pX,pY);
		}
	}
}