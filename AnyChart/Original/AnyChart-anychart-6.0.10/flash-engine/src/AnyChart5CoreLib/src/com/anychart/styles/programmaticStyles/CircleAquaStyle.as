package com.anychart.styles.programmaticStyles {
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorTransformHelper;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class CircleAquaStyle extends ProgrammaticStyle {
		
		public static function createColorTransform(color:uint):ColorTransformHelper {
			var th:ColorTransformHelper=new ColorTransformHelper();
			ColorUtils.toHLS(color);
			
            th.a_ex(ColorUtils.hue);
            th.b_ex(ColorUtils.br);
            th.c_ex(ColorUtils.sat);
            
            return th;
		} 
		
		protected static function ai_e2(A_0:uint):uint {
            var a:ColorTransformHelper = createColorTransform(A_0);
            var num:Number = 0.005 * Math.pow((a.b() * 100) - 100, 2);
            num /= 100;
            a.b_ex(a.b() + num);
            return a.aColor();
        }

        protected static function ai_d2(A_0:uint):uint {
            var a:ColorTransformHelper = createColorTransform(A_0);
            var num:Number = 0.005 * Math.pow(a.b() * 100, 2);
            num /= 100;
            a.b_ex(a.b() - num);
            return a.aColor();
        }
		
		protected var fillGrd:Gradient;
		protected var borderGrd:Gradient;
		
		override public function initialize(state:StyleState):void {
			initDynamicIndexes(state,ai_e2,ai_d2);
			
			// Init main fill gradient.
			{
				var opacity:Number=1;
            	if (state is BackgroundBaseStyleState && BackgroundBaseStyleState(state).fill != null)
            		opacity = BackgroundBaseStyleState(state).fill.opacity;
            		
				this.fillGrd = new Gradient(1);
   				this.fillGrd.type = GradientType.RADIAL;
   				this.fillGrd.focalPoint = 0.5;
            	this.fillGrd.angle = -145;
            	
            	var ents:Array = [];

            	ents.push(createKey(0,darkIndex,opacity,true));
            	ents.push(createKey(0.95,lightIndex,opacity,true));
            	ents.push(createKey(1,lightIndex,opacity,true));
            	
            	createDynamicGradient(state,this.fillGrd,ents);
			}
			
			// Init dark border gradient.
			{
				this.borderGrd = new Gradient(1);
	        	this.borderGrd.type = GradientType.RADIAL;
    		    this.borderGrd.focalPoint=0;
	        	this.borderGrd.angle=0;

	        	ents = [];
	        	opacity=1;
	        	
	        	ents.push(createKey(0,0,0));
	        	ents.push(createKey(1-0.12,0,0));
	        	ents.push(createKey(1,0,100.0/255.0*opacity));
	        	
	        	this.borderGrd.entries=ents;
   			}
		}
		
		override public function draw(data:Object):void {
			var point:ICircleAquaStyleContainer = ICircleAquaStyleContainer(data);
			var state:BackgroundBaseStyleState = point.getCurrentState();
			
			var ptColor:uint = point.getActualColor();
			var opacity:Number = state.fill != null ? state.fill.opacity : 1;
			var color:uint = state.fill.color;
			var bounds:Rectangle = point.getBounds();
			
			if (state.fill.isDynamic) {
				color = ColorParser.getInstance().getDynamicColor(color,ptColor);
			}
			
            var g:Graphics = point.getGraphics();
            
            ///draw fill
        	this.fillGrd.beginGradientFill(g,bounds,color);
            g.lineStyle(0,0,0);
            point.drawShape();
            g.endFill();
	        
	        // Draw dark border.
        	this.borderGrd.beginGradientFill(g,bounds,color);
            g.lineStyle(0,0,0);
            point.drawShape();          
            g.endFill();  
            
            // Draw hatch fill
            if (state.hatchFill != null && state.hatchFill.enabled) {
            	state.hatchFill.beginFill(g, point.getActualHatchType(), color);
            	point.drawShape();
            	g.endFill();
            }
		}
	}
}