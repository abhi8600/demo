package com.anychart.formatters {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.formatters.terms.ITerm;
	import com.anychart.formatters.terms.TermEmpty;
	import com.anychart.formatters.terms.TermStringConst;
	import com.anychart.formatters.terms.TermToken;
	import com.anychart.formatters.terms.TermTokenMultiScale;
	import com.anychart.formatters.terms.TermTokenNoScale;
	import com.anychart.formatters.terms.TermTokenOneScale;
	import com.anychart.formatters.terms.TermTokenString;
	
	
	public final class FormatsParser {

		private static var formats:Object = {};

		public static function isDynamic(format:String):Boolean {
			return format.indexOf('{%') != -1;  
		}
		
		private static const STATE_DEFAULT:uint = 0;
		private static const STATE_START_TOKEN:uint = 1;
		private static const STATE_TOKEN:uint = 2;
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(FormatsParser);
			return true;
		}
		
		public static function clear():void {
			formats = {};
		}

		public static function parse(format:String):TextFormater {
			if (formats[format] != undefined) return formats[format];
			var length:int = format.length
			var i:uint;
			var c:String;
			var tokenName:String = "";
			var tmpValue:String = "";
			var afterToken:String = "";
			var buffer:String = "";
			
			var curentTerm:ITerm = new TermEmpty();
			
			var state:uint = STATE_DEFAULT;
			for (i = 0; i < length; i++) {
				c = format.charAt(i);
				tmpValue += c;
				
				if (c == '{') {
					buffer = buffer + tmpValue;
					tmpValue = "";
					state = STATE_START_TOKEN;
					continue;
				}
				
				if (state == STATE_START_TOKEN) {
					if (c == '%') {
						tokenName = "";
						state = STATE_TOKEN;
					}
					else {
						tmpValue = buffer + tmpValue;
						buffer = "";
						state = STATE_DEFAULT
					}
					continue;
				}
				
				if (state == STATE_TOKEN) {
					if (c == '}') {
						if (tokenName.length > 0) {
							curentTerm = new TermStringConst(buffer.substr(0,buffer.length-1), curentTerm);
							tmpValue = "";
							buffer = "";
							var lastI:uint = i;
							if (i < length && format.charAt(i+1) == '{' && format.charAt(i+2) != '%') {
								c = format.charAt(++i);
								while (i < length) {
									c = format.charAt(++i);
									if (c == '}') {
										curentTerm = createTermToken('%'+tokenName, curentTerm, afterToken);
										afterToken = "";
										break;
									}
									afterToken += c;
								}
								if (i == length) {
									i = lastI;
									curentTerm = createTermToken('%'+tokenName, curentTerm, null);
								}
							} else {
								curentTerm = createTermToken('%'+tokenName, curentTerm, null);
							}
							tokenName = "";
							state = STATE_DEFAULT;
						} else {
							tmpValue = buffer + tmpValue;
							state = STATE_DEFAULT;
						}
					} else tokenName += c;
					continue;
				}

			}
			
			var lastStr:String = buffer + tmpValue;
			if (lastStr.length > 0) curentTerm = new TermStringConst(buffer+tmpValue, curentTerm);
			
			formats[format] = new TextFormater(curentTerm);
			return formats[format];
		}
		
		private static function createTermToken(tokenName:String, parentCall:ITerm, formatExpr:String):ITerm {
			var token:ITerm;
			var enabled:Boolean = true;
			var formatObj:Object = {};
			if (formatExpr == null) token = new TermTokenNoScale(tokenName, parentCall);
			else {
				var isName:Boolean = true;
				var name:String = '';
				var value:String = '';
				
				var i:uint;
				var c:String;
				
				for (i = 0;i<formatExpr.length;i++) {
					c = formatExpr.charAt(i);
					if (i == formatExpr.length - 1) {
						value = (isName && c == ":") ? "" : (value + c);
						formatObj[name] = value;
					} else {
						if (c == '\\') {
							c = formatExpr.charAt(++i);
							if (isName) name += c;
							else value += c;
							continue;
						}
						if (isName) {
							if (c == ':') {
								isName = false;
								continue;
							} else name += c;
						}else {
							if (c == ',') {
								isName = true;
								formatObj[name] = value;
								value = '';
								name = '';
								continue;
							} else value += c;
						}
					}
				}
				
				if (formatObj.enabled != undefined) {
					enabled = formatObj.enabled.toLowerCase() == 'true';
					if (!enabled) return new TermTokenString(tokenName, parentCall);
				}
				
				var scale:Array;
				if (formatObj.scale != null) {
					scale = [];
					var scaleStr:String = formatObj.scale;
					var isValuesParsing:Boolean = true;
					var index:uint = 0;
					for (i = 0;i<scaleStr.length;i++) {
						c = scaleStr.charAt(i);
						if (c == "|") {
							isValuesParsing = false;
							index = 0;
						}else if (c == ")") {
							scale[index][0] = Number(scale[index][0]);
							scale[index][2] = scale[index][0];
							if (index > 0)
								scale[index][2] *= scale[index-1][2];
							index++;
						}else if (c == "(") {
							if (isValuesParsing) scale.push(["","",1]);
						} else {
							scale[index][isValuesParsing ? 0 : 1] += c;
						}
					}
					if (scale != null) {
						if (scale.length == 1)
							token = new TermTokenOneScale(tokenName, parentCall, scale[0]);
						if (scale.length > 1)
							token = new TermTokenMultiScale(tokenName, parentCall, scale);
					}
				}
			}
			if (token == null) token = new TermTokenNoScale(tokenName, parentCall);
			if (token is TermTokenOneScale || token is TermTokenMultiScale || token is TermTokenNoScale) {
				TermToken(token).setFormatters(
					formatObj.useNegativeSign != undefined ? (formatObj.useNegativeSign.toLowerCase() == 'true') : true,
					formatObj.trailingZeros != undefined ? (formatObj.trailingZeros.toLowerCase() == "true") : true,
					formatObj.leadingZeros != undefined ? int(formatObj.leadingZeros) : 1,
					formatObj.numDecimals != undefined ? int(formatObj.numDecimals) : 2,
					formatObj.thousandsSeparator != undefined ? formatObj.thousandsSeparator : ',',
					formatObj.decimalSeparator != undefined ? formatObj.decimalSeparator : '.',
					formatObj.dateTimeFormat != undefined ? formatObj.dateTimeFormat : null,
					formatObj.maxChar != undefined ? formatObj.maxChar : -1,
					formatObj.maxCharFinalChars != undefined ? formatObj.maxCharFinalChars : '...'
				);
				TermToken(token).setEnabled(enabled);
			}
			return ITerm(token);
		}
		
	}
}