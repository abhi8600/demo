package com.anychart.preloader.light {
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public final class ErrorMessage {
		
		public static const SECURITY_ERROR:String = "Flash Security Error: <br />"+
"AnyChart can not be launched due to Flash Security Settings violation.<br />"+
"Please refer to Security Error Article in AnyChart Documentation to fix this issue.<br />" + 
"<u>Click for more information...</u>";
		public static const SECURITY_ERROR_URL:String = "http://www.anychart.com/products/anychart/docs/users-guide/index.html?security-error.html";

		public static const IO_ERROR:String = "I/O Error";
		
		public static function getXMLErrorMessage(error:Error):String {
			var message:String = "XML Parser failure: \n";
			switch (error.errorID) {
				case 1085: message += "The element type must be terminated by the matching end-tag."; break;
				case 1088: message += "The markup in the document following the root element must be well-formed."; break;
				case 1090: message += "element is malformed."; break;
				case 1091: message += "Unterminated CDATA section."; break;
				case 1092: message += "Unterminated XML declaration."; break;
				case 1093: message += "Unterminated DOCTYPE declaration."; break;
				case 1094: message += "Unterminated comment."; break;
				case 1095: message += "Unterminated attribute."; break;
				case 1096: message += "Unterminated element."; break;
				case 1097: message += "Unterminated processing instruction."; break; 
				case 1104: message += "Attribute was already specified for element."; break;
				default: message += "Error #"+error.errorID.toString(); break;
			}
			return message;
		}
		
		public static function showErrorMessage(bounds:Rectangle, message:String, errorURL:String = null):DisplayObject {
			var tf:TextField = new TextField();
			tf.autoSize = TextFieldAutoSize.CENTER;
			tf.htmlText = message;
			var fmt:TextFormat = new TextFormat();
			if (errorURL != null)
				fmt.url = errorURL;
			
			tf.x = bounds.x + (bounds.width - tf.width)/2;
			tf.y = bounds.y + (bounds.height - tf.height)/2;
			return tf;
		}
	}
}