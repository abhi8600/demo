package com.anychart.elements.tooltip {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class ItemTooltipElement extends ItemLabelElement {
		
		override protected function getStyleNodeName():String { return 'tooltip_style'; }
		override public function getStyleStateClass():Class { return TooltipElementStyleState; }
		override protected function getContainer(container:IMainElementsContainer):Sprite { return Sprite(container.tooltipsContainer); }
		
		override public function initialize(container:IElementContainer, elementIndex:uint):Sprite {
			if (!container.hasPersonalContainer) return null;
			var sp:Sprite = super.initialize(container, elementIndex);
			if (!sp) return null;
			sp.mouseEnabled = false;
			sp.mouseChildren = false;
			return sp;
		}
		
		override protected function getPosition(container:IElementContainer, state:BaseElementStyleState, sprite:Sprite, elementIndex:uint):Point {
			var pos:Point = super.getPosition(container, state, sprite, elementIndex);
			
			if (state.anchor == Anchor.FLOAT) return pos;

			if (container && container.getMainContainer() && 
				container.getMainContainer().root && 
				container.getMainContainer().root.getContainer()) {
				var rootSprite:Sprite = container.getMainContainer().root.getContainer();
				
				var pt:Point = new Point();
				pt = rootSprite.localToGlobal(pt);
				
				pos.x += pt.x;
				pos.y += pt.y;
			}
			
			
			
			return pos;
		}
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new ItemTooltipElement();
			return super.createCopy(element);
		}
		
		public function hide(sprite:Sprite):void {
			if (sprite != null)
				sprite.graphics.clear();
		} 
		
		public function move(state:StyleState, container:IElementContainer, elementIndex:uint, sprite:Sprite):void {
			var s:BaseElementStyleState = BaseElementStyleState(state);
			if (s.anchor != Anchor.FLOAT || !sprite) return;
			
			var pos:Point = new Point(sprite.parent.mouseX, sprite.parent.mouseY);
			
			var info:TextElementInformation = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			if (info == null) return;
			
			switch (s.hAlign) {
				case HorizontalAlign.LEFT:
					pos.x -= info.rotatedBounds.width + s.padding;
					break;
				case HorizontalAlign.RIGHT:
					pos.x += s.padding;
					break;
				case HorizontalAlign.CENTER:
					pos.x -= info.rotatedBounds.width/2;
					break;
			}
			
			switch (s.vAlign) {
				case VerticalAlign.TOP:
					pos.y -= s.padding+info.rotatedBounds.height;
					break;
				case VerticalAlign.BOTTOM:
					pos.y += s.padding;
					break;
				case VerticalAlign.CENTER:
					pos.y -= info.rotatedBounds.height/2;
					break;
			}
			
			var checkPt:Point = sprite.parent.localToGlobal(pos);
			var dx:Number = 0;
			var dy:Number = 0;
			if (sprite.stage) {
				if (checkPt.x < 0) {
					dx = Math.abs(checkPt.x);
				}else if (checkPt.x + info.rotatedBounds.width > sprite.stage.stageWidth) {
					dx = sprite.stage.stageWidth - checkPt.x - info.rotatedBounds.width;
				}
				if (checkPt.y < 0) {
					dy = Math.abs(checkPt.y);
				}else if (checkPt.y + info.rotatedBounds.height > sprite.stage.stageHeight) {
					dy = sprite.stage.stageHeight - checkPt.y - info.rotatedBounds.height;
				}
			}
			sprite.x = pos.x+dx;
			sprite.y = pos.y+dy;
		}
	}
}