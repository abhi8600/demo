package com.anychart.resources {
	import flash.events.Event;
	
	internal final class ResourceEntryEvent extends Event {
		
		public static const GET_SIZE:String = "getresourcesizeevent";
		public static const COMPLETE:String = "resourceloadingcompleteevent";
		public var bytesTotal:int;
		public var data:Object;
		
		public function ResourceEntryEvent(type:String) {
			super(type);
		}

	}
}