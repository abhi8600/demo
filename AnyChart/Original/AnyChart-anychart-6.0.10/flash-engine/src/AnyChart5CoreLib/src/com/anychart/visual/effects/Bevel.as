package com.anychart.visual.effects
{
	import com.anychart.serialization.SerializerBase;
	
	import flash.filters.BevelFilter;
	
	public class Bevel extends EffectBase
	{
		// Properties.
		private var angle:Number=45;
		private var distance:Number=3;
		private var highlightOpacity:Number=1;
		private var highlightColor:uint=0xFFFFFF;
		private var shadowOpacity:Number=1;
		private var shadowColor:uint=0x0;
		private var strength:Number=1;
		
		// Constructors.
		public function Bevel()
		{
			super();
		}
		
		
		// Methods
		override public function deserialize(data:XML):void
		{
			super.deserialize(data);
			if(data.@highlight_opacity!=undefined) this.highlightOpacity=SerializerBase.getRatio(data.@highlight_opacity);
			if(data.@highlight_color!=undefined) this.highlightColor=SerializerBase.getColor(data.@highlight_color);
			if(data.@shadow_opacity!=undefined) this.shadowOpacity=SerializerBase.getRatio(data.@shadow_opacity);
			if(data.@shadow_color!=undefined) this.shadowColor=SerializerBase.getColor(data.@shadow_color);
			if(data.@distance!=undefined) this.distance=SerializerBase.getNumber(data.@distance);
			if(data.@strength!=undefined) this.strength=SerializerBase.getNumber(data.@strength);
			if(data.@angle!=undefined) this.angle=SerializerBase.getNumber(data.@angle);
		}
		
		public function createFilter():BevelFilter
		{
			return new BevelFilter(distance,angle,highlightColor,highlightOpacity,shadowColor,shadowOpacity,blurX,blurY,strength);
		}
		
	}
}