package com.anychart.animation {
	public final class AnimationFunctionTarget extends AnimationTarget {
		public var callBack:Function;
		public var start:Number;
		public var end:Number;
		
		override public function destroy():void {
			this.callBack = null;
		}
		
		override public function setInitial():void {
			this.callBack(start);
		}
		
		override public function setFinal():void {
			this.callBack(end);
		}
		
		override public function animate(time:Number):void {
			this.callBack(this.animation.interpolation.interpolate(time, this.start, this.end - this.start, this.animation.duration)); 
		}
	}
}