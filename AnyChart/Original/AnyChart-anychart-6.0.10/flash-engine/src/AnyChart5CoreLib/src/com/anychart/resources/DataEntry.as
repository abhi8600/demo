package com.anychart.resources {
	import flash.net.URLLoader;
	
	internal final class DataEntry extends ResourceEntry {
		private var loader:URLLoader;
		
		public function DataEntry(dataFormat:String) {
			this.loader = new URLLoader();
			this.loader.dataFormat = dataFormat;
			this.initListeners(this.loader);
			super();
		}
		
		override public function stopLoading():void {
			if (this.isLoading) {
				this.loader.close();
				this.isLoading = false;
			}
		}
		
		override protected function execLoading():void {
			this.loader.load(this.request);
		}
		
		override protected function setData(event:ResourceEntryEvent):void {
			event.data = this.loader.data;
		}
	}
}