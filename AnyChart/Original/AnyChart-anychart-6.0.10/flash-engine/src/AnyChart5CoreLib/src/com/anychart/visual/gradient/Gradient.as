package com.anychart.visual.gradient {
	
	import com.anychart.serialization.IStyleSerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.color.ColorParser;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class Gradient implements IStyleSerializable {
		
		private var opacity:Number;
		
		public var isDynamic:Boolean;
		public var colorsIndex:uint; 
		
		private var colors:Array;
		
		public var ratios:Array;
		public var alphas:Array;
		
		private var _entries:Array;
		
		private var matrix:Matrix;
		
		public function Gradient(opacity:Number) {
			this.opacity = opacity;
			this.isDynamic = false;
			this.colors = [];
			this.ratios = [];
			this.alphas = [];
			this._entries = [];
			this.matrix = new Matrix();
			this._rotation = 0;
		}
		
		public function get entries():Array { return this._entries; }
		public function set entries(value:Array):void {
			this._entries = value;
			this.processEntries();
		}
		
		public var normalAngle:Number;
		public var invertedAngle:Number;
		
		private var _rotation:Number;
		public function get angle():Number {
			return this._rotation / Math.PI * 180;
		}
		public function set angle(value:Number):void {
			this._rotation = value / 180 * Math.PI;
		}
		
		public var type:String=GradientType.LINEAR;
		public var focalPoint:Number = 0;
		public var isLinearRGBInterpolationMode:Boolean = false;
		
		private function processEntries():void {
			if (!this.isDynamic) this.colors = [];
			this.ratios = [];
			this.alphas = [];
	
			if (!this._entries || this._entries.length == 0)
				return;
	
			var ratioConvert:Number = 255;
	
			var i:int;
			
			var n:int = this._entries.length;
			for (i = 0; i < n; i++) {
				var e:GradientEntry = this._entries[i];
				if (!this.isDynamic) this.colors.push(e.color);
				this.alphas.push(e.alpha);
				this.ratios.push(e.ratio * ratioConvert);
			}
			
			if (isNaN(ratios[0]))
				ratios[0] = 0;
				
			if (isNaN(ratios[n - 1]))
				ratios[n - 1] = 255;
			
			i = 1;
	
			while (true) {
				
				while (i < n && !isNaN(this.ratios[i]))
					i++;
	
				if (i == n)
					break;
					
				var start:int = i - 1;
				
				while (i < n && isNaN(this.ratios[i]))
					i++;
				
				var br:Number = this.ratios[start];
				var tr:Number = this.ratios[i];
				
				for (var j:int = 1; j < i - start; j++) 
					this.ratios[j] = br + j * (tr - br) / (i - start);
			}
		}
		
		public function beginGradientFill(target:Graphics,rc:Rectangle,color:uint = 0):void {
			this.matrix.createGradientBox(rc.width, rc.height, _rotation,rc.left, rc.top);
			target.beginGradientFill(this.type,
									 this.isDynamic ? ColorParser.getInstance().getDynamicGradient(this.colorsIndex,color) :
									 				  this.colors,
									 this.alphas, this.ratios,this.matrix,"pad",this.isLinearRGBInterpolationMode ? "linearRGB" : "rgb",this.focalPoint);	
		}
		
		public function lineGradientStyle(target:Graphics,rc:Rectangle,color:uint = 0):void {
			this.matrix.createGradientBox(rc.width, rc.height, _rotation,rc.left, rc.top);
			target.lineGradientStyle(this.type,
									 this.isDynamic ? ColorParser.getInstance().getDynamicGradient(this.colorsIndex,color) :
									 				  this.colors,
									 this.alphas,this.ratios,this.matrix,"pad",this.isLinearRGBInterpolationMode ? "linearRGB" : "rgb",this.focalPoint);
		}
		 
		public function deserialize(data:XML,style:IStyle = null):void {
			if(data.@type!=undefined) this.type=SerializerBase.getLString(data.@type);
			if(data.@angle!=undefined) this.angle=SerializerBase.getNumber(data.@angle);
			if(data.@focal_point != undefined) this.focalPoint=SerializerBase.getNumber(data.@focal_point);
			if(data.@interpolation_method != undefined) 
				this.isLinearRGBInterpolationMode = SerializerBase.getEnumItem(data.@interpolation_method) == "linearrgb";
				
			var ents:Array=[];
			
			var keysCount:uint = data.key.length();
			
			this.isDynamic = false;
			for (var i:uint = 0;i<keysCount;i++) {
				var entry:GradientEntry=new GradientEntry();
				entry.deserialize(data.key[i],style);
				entry.alpha *= this.opacity;
				ents.push(entry);
				if (entry.isDynamic)
					this.isDynamic = true;
			}
			if (this.isDynamic) {
				this.colorsIndex = ColorParser.getInstance().setDynamicGradient(ents,data.key);
				style.addDynamicGradient(this.colorsIndex);
			}
			
			if (ents.length != 0) {
				this.entries=ents;
			}
		}
	}
}