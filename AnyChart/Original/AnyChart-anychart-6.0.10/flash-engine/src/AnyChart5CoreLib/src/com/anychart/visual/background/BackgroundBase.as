package com.anychart.visual.background {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.effects.EffectsList;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	public class BackgroundBase implements IStyleSerializableRes {
		
		//-------------------------------------------------------
		// 
		//						PROPERTIES
		//
		//-------------------------------------------------------
		
		public var enabled:Boolean=true;
		
		public var fill:Fill=null;
		public var border:Stroke=null;
		public var hatchFill:HatchFill=null;
		public var effects:EffectsList=null;
		
		//-------------------------------------------------------
		// 
		//						Methods
		//
		//-------------------------------------------------------
		
		//------------------------------------
		// ISerializable
		//------------------------------------
		public function deserialize(node:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if(node.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(node.@enabled);
			if(!this.enabled) return;
			if(SerializerBase.isEnabled(node.border[0])) {
				this.border = new Stroke();
				this.border.deserialize(node.border[0], style);
				if (!this.border.enabled) this.border = null;
			}
			if(SerializerBase.isEnabled(node.fill[0])) {
				this.fill = new Fill();
				this.fill.deserialize(node.fill[0], resources, style);
				if (!this.fill.enabled) this.fill = null;
			}
			if(SerializerBase.isEnabled(node.hatch_fill[0])) {
				this.hatchFill = new HatchFill();
				this.hatchFill.deserialize(node.hatch_fill[0], style);
				if (!this.hatchFill.enabled) this.hatchFill = null;
			}
			if(SerializerBase.isEnabled(node.effects[0])) {
				this.effects = new EffectsList();
				this.effects.deserialize(node.effects[0])
				if (!this.effects.enabled) this.effects = null;
			}
		}

	}
}