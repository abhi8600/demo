package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public class Action implements ISerializable {
		
		protected var chart:IAnyChart;
		
		protected var isValidXML:Boolean;
		
		public function Action(chart:IAnyChart) {
			this.chart = chart;
			this.isValidXML = false;
		}
		
		public function execute(formatter:IFormatable):Boolean { return false; }
		public function deserialize(data:XML):void {}
		
		private static var classes:Object = {};
		public static function register(type:String, classRef:Class):void {
			classes[type] = classRef;
		}
		
		public static function create(chart:IAnyChart, node:XML):Action {
			if (node.@type == undefined) return null;
			var act:Action = null;
			var type:String = String(node.@type).toLowerCase();
			if (classes[type])
				act = new classes[type](chart);
			
			if (act != null)
				act.deserialize(node);
			return (act != null && act.isValidXML) ? act : null;
		}
		
	}
}