package com.anychart.styles {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.utils.XMLUtils;
	
	
	public final class StylesList {
		
		private static var instance:StylesList;
		public static function getInstance():StylesList { 
			if (instance == null)
				instance = new StylesList();
			return instance;
		}
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(StylesList);
			return true;
		}
		
		public static function clear():void {
			instance = null;
		}
		
		private var _styles:Object;
		private var _stylesList:Array;
		
		public static function get stylesList():Array { return getInstance()._stylesList; }
		public static function set stylesList(value:Array):void { getInstance()._stylesList = value; }
		
		public static function initializeParsing():void {
			StylesList.getInstance().initializeParsing();
		}
		
		public static function getStyle(nodeName:String, styleName:String):Style {
			return getInstance().getStyle(nodeName, styleName);
		}
		
		public static function addStyle(nodeName:String, styleName:String, style:Style):void {
			getInstance().addStyle(nodeName, styleName, style);
		}
		
		public function initializeParsing():void {
			this._styles = {};
			this._stylesList = [];
		}
		
		public function getStyle(nodeName:String, styleName:String):Style {
			if (this._styles[nodeName] == null) return null;
			return this._styles[nodeName][styleName];
		}
		
		public function addStyle(nodeName:String, styleName:String, style:Style):void {
			if (this._styles[nodeName] == null) 
				this._styles[nodeName] = {};
			this._styles[nodeName][styleName] = style;
		}
		
		public static function getStyleXMLByMerge(stylesList:XML,
												  styleNodeName:String,
												  styleData:XML,
												  parentStyleName:*, needBuildStates:Boolean = true):XML {
			var styles:Array = new Array();
				
			styles.push(styleData);
			
			if (parentStyleName != null)
				parentStyleName = String(parentStyleName).toLowerCase();
				
			if (parentStyleName == undefined || stylesList[styleNodeName].(@name == parentStyleName).length() == 0) {
				styles.push(stylesList[styleNodeName].(@name == "anychart_default")[0]);
			}else {
				var styleNode:XML = stylesList[styleNodeName].(@name == parentStyleName)[0];
				styles.push(styleNode);
				
				while (styleNode.@name != "anychart_default") {
					if (styleNode.@parent == undefined || stylesList[styleNodeName].(@name == String(styleNode.@parent).toLowerCase()).length() == 0) {
						styleNode = stylesList[styleNodeName].(@name == "anychart_default")[0];
					}else {
						styleNode = stylesList[styleNodeName].(@name == String(styleNode.@parent).toLowerCase())[0];
					}
					styles.push(styleNode);
				}
			}
			
			var actualStyleXML:XML = buildStyle(styles, needBuildStates);
			
			styles = null;
			
			return actualStyleXML;
	    }
		
		public static function getStyleXMLByApplicator(stylesList:XML, 
											  	       styleNodeName:String,
												       styleName:String,
												       localList:StylesList = null):XML {
			if (localList == null)
				localList = StylesList.getInstance();
				
			styleName = styleName.toLowerCase();
												    	
			var cashedStyle:Style = localList.getStyle(styleNodeName, styleName);
			if (cashedStyle != null) 
				return null;
			
			var styleNode:XML = stylesList[styleNodeName].(String(@name).toLowerCase() == styleName)[0];
			if (styleNode == null) 
				return (styleName == "anychart_default") ? null : getStyleXMLByApplicator(stylesList, styleNodeName, "anychart_default");
			var styles:Array = new Array();
			styles.push(styleNode);
			
			while (styleNode.@name != "anychart_default") {
				if (styleNode.@parent == undefined || stylesList[styleNodeName].(@name == String(styleNode.@parent).toLowerCase()).length() == 0) {
					styleNode = stylesList[styleNodeName].(@name == "anychart_default")[0];
				}else {
					styleNode = stylesList[styleNodeName].(@name == String(styleNode.@parent).toLowerCase())[0];
				}
				styles.push(styleNode);
			}
			
			var actualStyleXML:XML = buildStyle(styles);
			
			styles = null;
			
			return actualStyleXML;
	    }
	    
	    private static function buildStyle(styles:Array, needBuildStates:Boolean = true):XML {
	    	var i:int;
	    	var stylesCnt:int = styles.length;
	    	
	    	if (needBuildStates) {
		    	for (i = 0;i<stylesCnt;i++)
		    		styles[i] = buildStyleXML(styles[i]);
	    	}
	    	
	    	var actualStyleXML:XML = styles[stylesCnt-1];
			actualStyleXML.@inherits = actualStyleXML.@name != undefined ? String(actualStyleXML.@name) : "";
			for (i = stylesCnt-1;i>0;i--) {
				actualStyleXML = XMLUtils.merge(actualStyleXML, styles[i-1]);
				if (styles[i-1].@name != undefined)
					actualStyleXML.@inherits = String(actualStyleXML.@inherits)+","+String(styles[i-1].@name);
			}
			return actualStyleXML;
	    }
	    
	    //---------------------------------------------------------------------------------
	    //					ANYCHART 4 BUG REPRODUCING
	    //---------------------------------------------------------------------------------
	    
	    private static const IGNORED:Array = ["animation_style", 
	    "animation", "line_axis_marker_style", "range_axis_marker_style", "custom_label_style", 
	    "trendline_style", "color_range_style", "tooltip_style"];
	    
	    private static function buildStyleXML(data:XML):XML {
	    	var nodeName:String = data.name().toString();
	    	if (IGNORED.indexOf(nodeName) != -1)
	    		return data;
	    		
	    	var newData:XML = new XML(<{nodeName}>
	    		<states>
	    			<normal />
	    			<hover />
	    			<pushed />
	    			<selected_normal />
	    			<selected_hover />
	    			<missing />
	    		</states>
	    	</{nodeName}>);
	    	
	    	XMLUtils.mergeAttributes(data, newData, newData);
	    	
	    	var states:XML = newData.states[0];
	    	newData.states[0].normal[0] = getMergedState(data, "normal");
	    	newData.states[0].hover[0] = getMergedState(data, "hover");
	    	newData.states[0].pushed[0] = getMergedState(data, "pushed");
	    	newData.states[0].selected_normal[0] = getMergedState(data, "selected_normal");
	    	newData.states[0].selected_hover[0] = getMergedState(data, "selected_hover");
	    	newData.states[0].missing[0] = getMergedState(data, "missing");
	    	return newData;
	    }
	    
	    private static function getMergedState(data:XML, stateName:String):XML {
	    	if (data.states[0] == null) data.states[0] = <states><{stateName} /></states>;
	    	if (data.states[0][stateName][0] == null) data.states[0][stateName][0] = <{stateName} />;
	    	return XMLUtils.merge(data, data.states[0][stateName][0], stateName, "states");
	    }
	}
}