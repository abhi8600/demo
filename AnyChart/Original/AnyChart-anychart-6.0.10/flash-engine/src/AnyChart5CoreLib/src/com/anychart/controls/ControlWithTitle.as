package com.anychart.controls {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	
	public class ControlWithTitle extends Control {
		
		private var title:ControlTitle;
		private var noTitleBounds:Rectangle;
		
		public function ControlWithTitle() {
			super();
			this.noTitleBounds = new Rectangle();
		}
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			if (SerializerBase.isEnabled(data.title[0])) {
				this.title = new ControlTitle();
				this.title.deserialize(data, resources);
			}
		}
		
		override public function initialize():DisplayObject {
			super.initialize();
			if (this.title != null) {
				this.title.initialize(this.container);
				this.title.calculateSpace(this.getTokenValue, this.isDateTimeToken);
			}
			return this.container;
		}
		
		private function getTokenValue(token:String):* {
			return this.plot.getTokenValue(token);
		}
		
		private function isDateTimeToken(token:String):Boolean {
			return false;
		}
		
		override public function draw():void {
			super.draw();
			if (this.title != null)
				this.title.drawTitle(this.noTitleBounds);
		}
		
		override public function resize():void {
			super.resize();
			if (this.title != null)
				this.title.drawTitle(this.noTitleBounds);
		}
		
		override public function setBounds(chartRect:Rectangle, plotRect:Rectangle):void {
			super.setBounds(chartRect, plotRect);
		}
		
		override protected function getMaxContentHeight():Number {
			var h:Number = super.getMaxContentHeight();
			if (this.title != null)
				h -= this.title.space;
			return h;
		}
		
		override protected function getContentHeight():Number {
			var h:Number = super.getContentHeight();
			if (this.title != null)
				h -= this.title.space;
			return h;				 
		}
		
		override protected function checkContentBounds():void {
			if (this.title != null) {
				if (!this.layout.isWidthSetted)
					this.bounds.width = Math.max(this.bounds.width, this.title.minWidth);
				if (!this.layout.isHeightSetted)
					this.bounds.height += this.title.space;
			}
			super.checkContentBounds();
		}
		
		override protected function setContentBounds():void {
			
			super.setContentBounds();
			
			this.noTitleBounds.x = this.contentBounds.x;
			this.noTitleBounds.y = this.contentBounds.y;
			this.noTitleBounds.width = this.contentBounds.width;
			this.noTitleBounds.height = this.contentBounds.height;
			
			if (this.title != null)
				this.title.applySpace(this.contentBounds);
		}
	}
}