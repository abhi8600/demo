package com.anychart.elements.tooltip {
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IRotationManager;
	import com.anychart.styles.Style;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.VerticalAlign;

	internal final class TooltipElementStyleState extends LabelElementStyleState {
		
		public function TooltipElementStyleState(style:Style) {
			super(style);
		}
		
		override public function initializeRotations(manager:IRotationManager, baseScaleInverted:Boolean):void {
			
			this.normalAnchor = this.invertedAnchor = this.anchor;
			this.normalHAlign = this.invertedHAlign = this.hAlign;
			this.normalVAlign = this.invertedVAlign = this.vAlign; 
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (this.anchor == Anchor.FLOAT && this.vAlign == VerticalAlign.BOTTOM)
				this.padding += 20;
			return data;
		}
	}
}