package com.anychart.elements {
	import com.anychart.animation.Animation;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	public class AnimatableElement extends BaseElement {
		public var animation:Animation;
		public var enableAnimation:Boolean;
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new AnimatableElement();
			super.createCopy(element);
			if (this.animation)
				AnimatableElement(element).animation = this.animation.createCopy();
			AnimatableElement(element).enableAnimation = this.enableAnimation;
			return element;
		}
		
		override public function needCreateElementForContainer(elementNode:XML):Boolean {
			if (super.needCreateElementForContainer(elementNode)) return true;
			if (this.enableAnimation && elementNode.animation[0] != null) return true;
			return false; 
		}
		
		override public function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
			this.deserializeAnimation(data, stylesList);
		}
		
		public function deserializeAnimation(data:XML, stylesList:XML):void {
			if (this.enableAnimation && data.animation[0] != null) {
				if (SerializerBase.isEnabled(data.animation[0])) {
					this.animation = (this.animation) ? this.animation.createCopy() : new Animation();
					this.animation.deserialize(data.animation[0], stylesList);
				}else {
					this.animation = null;
				}
			}
		}
	}
}