package com.anychart.uiControls.scrollBar {
	import flash.geom.Rectangle;
	
	internal final class ScrollBarButtonLeft extends ScrollBarButtonWithArrow {
		
		override protected function getArrowBounds():Rectangle {
			var iconSize:Number = Math.min(size.width, size.height) * 0.4;
			
			return new Rectangle(size.width * 0.5 - iconSize * 0.25,
							  	 (size.height - iconSize) * 0.5,
							  	 iconSize * 0.5,
							  	 iconSize);
		}
		
		override protected function drawArrow():void {
			var s:Rectangle = size;
			var iconSize:Number = Math.min(s.width, s.height) * 0.4;
			
			var pX:Number = s.width * 0.5 - iconSize * 0.25;
			var pY:Number = (s.height - iconSize) * 0.5;
			
			var w:Number = iconSize * 0.5;
			var h:Number = iconSize;
			
			graphics.moveTo(pX, pY + h* 0.5);
			graphics.lineTo(pX + w, pY);
			graphics.lineTo(pX + w, pY + h);
			graphics.moveTo(pX, pY + h * 0.5);
		}
	}
}