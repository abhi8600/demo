package com.anychart.selector {
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class HitTestUtil {
		
		public static function hitTest(sp1:DisplayObject, sp2:DisplayObject):Boolean {
			if (sp1.hitTestObject(sp2)) {
				
				var p1:Point = sp1.getBounds(sp1.parent).topLeft;
				var p2:Point = sp2.getBounds(sp2.parent).topLeft;
				
				var bmp1:BitmapData = getBitmap(sp1);
				var bmp2:BitmapData = getBitmap(sp2);
				
				if (bmp1 == null || bmp2 == null) return false;
				var res:Boolean = bmp1.hitTest(p1, 255, bmp2, p2, 255);
				bmp1.dispose();
				bmp2.dispose();
				return res;
			}
			return false;
		}
		
		private static function getBitmap(sp:DisplayObject):BitmapData {
			var bounds:Rectangle = sp.getBounds(sp.parent);
			
			if (bounds.width == 0 || bounds.height == 0) return null;
			
			var m:Matrix = new Matrix();
			m.tx = sp.x - bounds.x;
			m.ty = sp.y - bounds.y;
			
			var bmpData:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0);
			bmpData.draw(sp, m);
			
			return bmpData;
		}
	}
}