package com.anychart.controls.legend {
	import flash.display.Graphics;
	
	public final class RectangleIconDrawer {
		public static function draw(g:Graphics, 
									x:Number, y:Number, width:Number, height:Number,
									color:uint):void {
			g.beginFill(color, 1);
			g.drawRect(x,y,width,height);
			g.endFill();
		} 
	}
}