package com.anychart.actions{
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	import flash.geom.Point;
	
	
	public class ScrollAction extends Action{
		
		protected var x:String;
		protected var isDinamicX:Boolean;
		protected var xFormater:TextFormater;
		
		protected var y:String;
		protected var isDinamicY:Boolean;
		protected var yFormater:TextFormater;
		
		public function ScrollAction(chart:IAnyChart) {
			super(chart);
		}
		
		override public function deserialize(data:XML):void {
			this.isValidXML = data.@x != undefined || data.@y != undefined;
			
			if (!this.isValidXML) return;
			
			if (data.@x != undefined) {
				this.x = SerializerBase.getString(data.@x);
				this.isDinamicX = FormatsParser.isDynamic(this.x);
				if (this.isDinamicX) this.xFormater = FormatsParser.parse(this.x);
			}
			
			if (data.@y != undefined) {
				this.y = SerializerBase.getString(data.@y);
				this.isDinamicY = FormatsParser.isDynamic(this.y);
				if (this.isDinamicY) this.yFormater = FormatsParser.parse(this.y);
			}
		}
		
		
		override public function execute(formatter:IFormatable):Boolean{
			var pt:Point = this.doExecute(formatter);
			this.chart.scrollTo(pt.x.toString(),pt.y.toString());
			return false;
		}
		
		protected function doExecute (formatter:IFormatable):Point {
			var pt:Point = new Point();
			pt.x = this.x == null ? 
				NaN 
				: Number(
					this.isDinamicX ? 
						this.xFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)
						 : this.x);
						 
			pt.y = this.y == null ? NaN : Number(this.isDinamicY ? this.yFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken) : this.y);
			
			return pt;
		}
	}
}