package com.anychart.resources {
	import flash.display.Bitmap;
	import flash.display.Loader;
	
	internal final class ImageEntry extends ResourceEntry {
		private var loader:Loader;
		
		public function ImageEntry() {
			this.loader = new Loader();			
			this.initListeners(this.loader.contentLoaderInfo);
			super();
		}
		
		override public function stopLoading():void {
			if (this.isLoading) {
				this.loader.close();
				this.isLoading = false;
			}
		}
		
		override protected function execLoading():void {
			this.loader.load(this.request);
		}
		
		override protected function setData(event:ResourceEntryEvent):void {
			event.data = Bitmap(this.loader.content).bitmapData.clone();
			this.loader.unload();
			this.loader = null;
		}
	}
}