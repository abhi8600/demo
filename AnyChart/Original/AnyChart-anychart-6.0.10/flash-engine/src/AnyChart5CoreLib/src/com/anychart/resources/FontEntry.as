package com.anychart.resources {
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.text.Font;
	
	internal final class FontEntry extends ResourceEntry {
		private var loader:Loader;
		
		public function FontEntry() {
			this.loader = new Loader();			
			this.initListeners(this.loader.contentLoaderInfo);
			super();
		}
		
		override public function stopLoading():void {
			if (this.isLoading) {
				this.loader.close();
				this.isLoading = false;
			}
		}
		
		override protected function execLoading():void {
			this.loader.load(this.request);
		}
		
		override protected function setData(event:ResourceEntryEvent):void {
			
			var loaderInfo:LoaderInfo = this.loader.contentLoaderInfo;
			
			var fontClass:Class;
			fontClass = loaderInfo.content['getFontClass']();
			Font.registerFont(fontClass);
			
			this.loader.unload();
			this.loader = null;
			
			event.data = "FONT";
		}
	}
}