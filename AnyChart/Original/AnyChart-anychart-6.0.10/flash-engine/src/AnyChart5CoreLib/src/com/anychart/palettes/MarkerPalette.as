package com.anychart.palettes {
	import com.anychart.serialization.SerializerBase;
	
	
	public class MarkerPalette extends BasePalette {
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			var itemsCount:uint = data.item.length();
			for (var i:uint = 0;i<itemsCount;i++) {
				this.items.push(SerializerBase.getMarkerType(data.item[i].@type));
			}
		}
	}
}