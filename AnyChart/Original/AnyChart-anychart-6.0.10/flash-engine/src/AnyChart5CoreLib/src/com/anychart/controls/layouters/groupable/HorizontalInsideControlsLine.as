package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlLayout;
	
	import flash.geom.Rectangle;
	
	internal final class HorizontalInsideControlsLine extends HorizontalControlsLine {
		
		public function HorizontalInsideControlsLine(isOpposite:Boolean) {
			super(isOpposite);
		}
		
		override protected function finalizeNearOrFarLayout(controls:Array, space:Number):void {
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite)
					control.bounds.y = basePositionRect.bottom - control.bounds.height;
				else 
					control.bounds.y = basePositionRect.top;
			}
		}
		
		override protected function finalizeCenterLayout(controls:Array, space:Number):void {
			var y:Number = this.isOpposite ? (basePositionRect.bottom) : (basePositionRect.top);
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite) {
					control.bounds.y = y - control.bounds.height;
					y -= control.bounds.height;
				}else {
					control.bounds.y = y;
					y += control.bounds.height;
				}
			}
		}
		
		override protected function setMaxBounds(layout:ControlLayout, chartRect:Rectangle, plotRect:Rectangle):void {
			layout.maxWidth = layout.alignByDataPlot ? plotRect.width : chartRect.width;
			layout.maxHeight = chartRect.height;
		}
		
	}
}