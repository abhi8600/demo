package com.anychart.dashboard {
	import com.anychart.Chart;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.background.RectBackground;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	internal class Panel extends DashboardElementContainer {
		
		protected var title:PanelTitle;
		private var background:RectBackground;
		
		protected var panelTotalBounds:Rectangle;
		private var contentTotalBounds:Rectangle;
		
		public function Panel(dashboard:Dashboard) {
			super(dashboard);
		}
		
		override protected function setLayout(newBounds:Rectangle):void {
			//get total bounds and position
			super.setPosition(newBounds);
			this.panelTotalBounds = this.bounds.clone();
			//get content total bounds (with title)
			if (this.background != null)
				this.background.applyInsideMargins(this.bounds);
			this.contentTotalBounds = this.bounds.clone();
			
			//this.bounds is bounds without title
			if (this.title != null) {
				this.bounds.y += this.title.space;
				this.bounds.height -= this.title.space;
			}
		}
		
		override public function initialize(newBounds:Rectangle):DisplayObject {
			super.initialize(newBounds);
			if (this.title != null)
				this.container.addChild(this.title.initialize(this.contentTotalBounds));
			return this.container;
		}
		
		override public function draw():void {
			super.draw();
			if (this.title != null)
				this.title.drawTitle();
			if (this.background != null)
				this.background.draw(this.container.graphics, this.panelTotalBounds, 0, 0);
		}
		
		override public function resize(newBounds:Rectangle):void {
			super.resize(newBounds);
			if (this.title != null)
				this.title.resize(this.contentTotalBounds);
			if (this.background != null) {
				this.container.graphics.clear();
				this.background.draw(this.container.graphics, this.panelTotalBounds, 0, 0);
			}
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			data = this.mergeDashboardTemplate(Chart.getDefaultTemplate().defaults[0].panel[0].copy(), data);
			this.deresializeWithoutTemplate(data, resources);
		}
		
		protected final function deresializeWithoutTemplate(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data,resources);
			if (SerializerBase.isEnabled(data.title[0]) && data.title[0].text[0] != null) {
				this.title = new PanelTitle(this.dashboard.base);
				this.title.deserialize(data.title[0], resources);
			}
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new RectBackground();
				this.background.deserialize(data.background[0], resources);
			}
		}
		
		protected final function mergeDashboardTemplate(template:XML, data:XML):XML {
			var res:XML = data.copy();
			XMLUtils.mergeAttributes(template, data, res);
			
			var templateChildren:XMLList = template.children();
			var cnt:int = templateChildren.length();
			for (var i:int = 0;i<cnt;i++) {
				var c:XML = templateChildren[i];
				if (res[c.name()][0] == null)
					res[c.name()] += c;
				else
					res[c.name()][0] = XMLUtils.merge(c, res[c.name()][0]);
			}
			return res;
		}
	}
}