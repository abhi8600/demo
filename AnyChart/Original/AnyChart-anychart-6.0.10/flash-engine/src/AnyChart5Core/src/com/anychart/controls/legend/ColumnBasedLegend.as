package com.anychart.controls.legend {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public final class ColumnBasedLegend extends LegendControl {
		public static function check(data:XML):Boolean {
			if (data.name().toString() != "legend") return false;
			if (data.@elements_layout != undefined && String(data.@elements_layout).toLowerCase() != "horizontal") return true;
			if (data.@position != undefined) {
				var pos:String = String(data.@position).toLowerCase();
				return pos == "float" || pos == "fixed" || pos == "left" || pos == "right";
			}
			return true;
		}

		private var columns:uint;
		private var itemsInColumn:uint;
		
		//------------------------------------------------------------------------------
		//		Constructor
		//------------------------------------------------------------------------------
		
		public function ColumnBasedLegend() {
			this.columns = 1;
		}
		
		//------------------------------------------------------------------------------
		//		XML
		//------------------------------------------------------------------------------
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			if (data.@columns != undefined) this.columns = SerializerBase.getNumber(data.@columns);
		}
		
		//------------------------------------------------------------------------------
		//		Bounds Prepearing
		//------------------------------------------------------------------------------
		
		//максимальная высота колонок
		private var columnsHeight:Number;
		
		//суммарная ширина колонок
		private var summColumnsWidth:Number;
		
		//суммарные длины по колонкам (включая padding-и)
		private var columnsWidths:Array;
		
		//максимальная длинна элементов в колонке
		private var columnsMaxItemWidths:Array;
		
		//calculate items in columns and columns sizes
		override protected function initializeLayoutBase():void {
			
			this.itemsInColumn = Math.ceil(this.visualItems.length/this.columns);
			this.columns = Math.ceil(this.visualItems.length/this.itemsInColumn);
			
			this.columnsWidths = new Array();
			this.columnsMaxItemWidths = new Array();
			this.summColumnsWidth = 0;
			this.columnsHeight = 0;
			
			var cnt:uint = this.visualItems.length;
			
			var columnWidth:Number = 0;
			var columnHeight:Number = 0;
			
			for (var i:uint = 0;i<cnt;i++) {
				
				var item:LegendItem = LegendItem(this.visualItems[i]);
				
				columnWidth = Math.max(columnWidth, item.contentBounds.width);
				columnHeight += item.contentBounds.height;
				
				if (this.isLastInColumn(i)) {
					
					this.columnsMaxItemWidths.push(columnWidth);
					
					if (!this.isLastColumn(this.columnsWidths.length))
						columnWidth += this.columnSpace;
					
					this.columnsWidths.push(columnWidth);
					this.summColumnsWidth += columnWidth;
					this.columnsHeight = Math.max(columnHeight, this.columnsHeight);
					
					columnWidth = 0;
					columnHeight = 0;
				}else {
					columnHeight += this.rowSpace;
				}
			}
		}
		
		private function isLastInColumn(index:uint):Boolean {
			if (index == (this.itemsInColumn -1)) return true;
			if (index == (this.visualItems.length - 1)) return true;
			if ((index + 1) % this.itemsInColumn == 0) return true;
			return false;
		}
		
		private function isLastColumn(index:uint):Boolean {
			return (index+1) == this.columns; 
		}
		
		//------------------------------------------------------------------------------
		//		Bounds Calculation
		//------------------------------------------------------------------------------
		
		override protected function checkContentBounds():void {
			this.initializeItems();
			
			this.left = 0;
			this.top = 0;
			
			this.elementsAlign = this.realElementsAlign;
			
			this.scrollBarSpace = 0;
			
			if (!this.layout.isHeightSetted)
				this.bounds.height = this.columnsHeight;
			
			this.checkHeight();
			
			if (!this.layout.isWidthSetted)
				this.bounds.width = this.summColumnsWidth + this.scrollBarSpace;
			else
				this.calculateLeftOffset();
				
			super.checkContentBounds();
		}
		
		private function calculateLeftOffset():void {
			this.left = 0;
			var w:Number = this.getContentWidth() - this.scrollBarSpace;
			if (this.summColumnsWidth < w) {
				switch (this.realElementsAlign) {
					case HorizontalAlign.CENTER:
						this.left = (w - this.summColumnsWidth)/2;
						break;
					case HorizontalAlign.RIGHT:
						this.left = w - this.summColumnsWidth;
						break;
				}
			}else {
				this.elementsAlign = HorizontalAlign.LEFT;
			}
		}
		
		private function checkHeight():void {
			var maxHeight:Number = this.getMaxContentHeight();
			if (!this.layout.isHeightSetted) {
				if (this.bounds.height > maxHeight) {
					this.bounds.height = maxHeight;
					this.createScrollBar();
				}else {
					this.removeScrollBar();
				}
			}else {
				if (this.columnsHeight > this.getContentHeight())
					this.createScrollBar();
				else
					this.removeScrollBar();
			}
			this.realContentHeight = this.columnsHeight;
		}
		
		//------------------------------------------------------------------------------
		//		Items Placement
		//------------------------------------------------------------------------------
		
		private var left:Number;
		private var top:Number;
		
		override protected function updateLayout():void {
			this.initializeLayoutBase();
		}
		
		override protected function distributeItems():void {
			
			var columnSeparatorBounds:Rectangle;
			if (this.columnsSeparator != null && this.columnsSeparator.type == StrokeType.GRADIENT)
				columnSeparatorBounds = new Rectangle(0,0,this.columnsSeparator.thickness,this.columnsHeight);
			
			var rowsSeparatorBounds:Rectangle;
			if (this.rowsSeparator != null && this.rowsSeparator.type == StrokeType.GRADIENT) 
				rowsSeparatorBounds = new Rectangle(0,0,0,this.rowsSeparator.thickness);
				
			var g:Graphics = this.itemsContainer.graphics;
			
			var x:Number = this.left;
			var y:Number = this.top;
			
			var cnt:uint = this.visualItems.length;
			var lastIndex:int = cnt-1;
			var columnIndex:int = 0;
			
			var sX:Number;
			var xSpace:Number = this.columnSpace/2;
			var xSpace1:Number = xSpace;
			
			if (this.columnsSeparator != null) {
				xSpace += this.columnsSeparator.padding/2;
				xSpace1 -= this.columnsSeparator.padding/2;
			}
			
			for (var i:int = 0;i<cnt;i++) {
				var item:LegendItem = this.visualItems[i];
				item.x = x;
				item.y = y;
				
				switch (this.elementsAlign) {
					case HorizontalAlign.CENTER:
						item.x += (this.columnsMaxItemWidths[columnIndex] - item.contentBounds.width)/2;
						break;
					case HorizontalAlign.RIGHT:
						item.x += (this.columnsMaxItemWidths[columnIndex] - item.contentBounds.width);
						break;
				}
				
				if (this.isLastInColumn(i)) {
					x += this.columnsWidths[columnIndex];
					y = 0;
					
					if (this.columnsSeparator != null && !this.isLastColumn(columnIndex)) {
						//draw columns separator
						sX = x-this.columnSpace/2;
						if (columnSeparatorBounds != null)
							columnSeparatorBounds.x = sX-this.columnsSeparator.thickness/2;
						this.columnsSeparator.apply(g, columnSeparatorBounds);
						g.moveTo(sX, 0);
						g.lineTo(sX, this.columnsHeight);
						g.lineStyle();
					}
					
					columnIndex++;
				}else {
					y += item.contentBounds.height + this.rowSpace;
					
					if (this.rowsSeparator != null) {
						//draw row separator
						var sY:Number = y - this.rowSpace/2;
						sX = x;
						var w:Number = this.columnsWidths[columnIndex] - xSpace;
						if (columnIndex > 0) {
							sX -= xSpace1;
							w += xSpace1;
							if (this.isLastColumn(columnIndex))
								w += xSpace;
						}else if (this.isLastColumn(columnIndex)) {
							w += xSpace;
						}
						
						if (rowsSeparatorBounds != null) {
							rowsSeparatorBounds.width = w;
							rowsSeparatorBounds.x = sX;
							rowsSeparatorBounds.y = sY - this.rowsSeparator.thickness/2; 
						}
						
						this.rowsSeparator.apply(g, rowsSeparatorBounds);
						g.moveTo(sX, sY);
						g.lineTo(sX + w, sY);
						g.lineStyle();
					}
				}
			}
		}
	}
}