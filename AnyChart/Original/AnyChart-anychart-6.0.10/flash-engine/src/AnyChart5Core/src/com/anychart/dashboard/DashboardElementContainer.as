package com.anychart.dashboard {
	import com.anychart.resources.ResourcesLoader;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	
	internal class DashboardElementContainer extends DashboardElement {
		
		protected var children:Array;
		
		public function DashboardElementContainer(dashboard:Dashboard) {
			super(dashboard);
			this.children = [];
		}
		
		//-------------------------------------------------------------
		//		INITIALIZE
		//-------------------------------------------------------------
		
		override public function initialize(newBounds:Rectangle):DisplayObject {
			this.initializeContainer(newBounds);
			this.initializeChildren();
			return this.container;
		}
		
		protected function initializeContainer(newBounds:Rectangle):void {
			super.initialize(newBounds);
		}
		
		protected function initializeChildren():void {
			for (var i:uint = 0;i<this.children.length;i++)
				this.container.addChild(this.children[i].initialize(this.bounds));
		}
		
		//-------------------------------------------------------------
		//		DRAW
		//-------------------------------------------------------------
		
		override public function draw():void {
			for (var i:uint = 0;i<this.children.length;i++)
				this.children[i].draw();
		}
		
		//-------------------------------------------------------------
		//		RESIZE
		//-------------------------------------------------------------
		
		override public function resize(newBounds:Rectangle):void {
			this.resizeContainer(newBounds);
			this.resizeChildren();
		}
		
		protected function resizeContainer(newBounds:Rectangle):void {
			super.resize(newBounds);
		}
		
		protected function resizeChildren():void {
			for (var i:uint = 0;i<this.children.length;i++)
				this.children[i].resize(this.bounds);
		}
		
		//-------------------------------------------------------------
		//		DESERIALIZE
		//-------------------------------------------------------------
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			var childNodes:XMLList = data.children();
			var childCnt:int = childNodes.length();
			for (var i:uint = 0;i<childCnt;i++) {
				var node:XML = childNodes[i];
				var element:DashboardElement = DashboardElementFactory.createElement(node, this.dashboard);
				if (element != null) {
					element.deserialize(node, resources);
					this.children.push(element);
				}
			}
		}
		
	}
}