package com.anychart.dashboard {
	import flash.geom.Rectangle;
	
	internal final class DashboardHBox extends DashboardElementContainer {
		
		public function DashboardHBox(dashboard:Dashboard) {
			super(dashboard);
		}
		
		override protected function setLayout(newBounds:Rectangle):void {
			super.setLayout(newBounds);
			var tmpX:Number = this.bounds.x;
			for (var i:uint = 0;i<this.children.length;i++) {
				var child:DashboardElement = this.children[i];
				child.calculatedX = tmpX;
				child.calculatedWidth = NaN;
				child.calculatedWidth = child.getWidth(newBounds);
				tmpX += child.calculatedWidth;
			}
		}
		
	}
}