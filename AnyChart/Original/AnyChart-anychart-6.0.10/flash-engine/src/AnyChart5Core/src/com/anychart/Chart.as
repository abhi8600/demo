package com.anychart {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.PlotControlsList;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.noData.NoDataMessage;
	import com.anychart.plot.IPlot;
	import com.anychart.plot.PlotFactoryBase;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.titles.ChartTitle;
	import com.anychart.viewController.ChartView;
	import com.anychart.viewController.IViewElement;
	import com.anychart.visual.background.RectBackground;
	import com.anychart.visual.background.RectBackgroundSprite;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	public class Chart extends RectBackgroundSprite implements IViewElement, IChart {
		
		private var hasData:Boolean;
			
		private var chartView:ChartView;
		
		private var base:IAnyChart;
		private var controls:PlotControlsList;
		
		private var animation:AnimationManager;
		private var chartAnimation:Animation;
		
		private var content:Sprite;
		private var chartBackground:Sprite;
		
		public function Chart(base:IAnyChart, view:ChartView) {
			this.chartView = view;
			this.base = base;			
			this.afterPlotContainer = new Sprite();
			this.hasData = true;
			this._isInitialized = false;
			super();
		}
		
		public function destroy():void {
			if (this.animation != null)
				this.animation.destroy();
				
			if (this.controls)
				this.controls.destroy();
				
			if (this._plot)
				this._plot.destroy();
				
			this._plot = null;
			this._plotContainer = null;
		}
		
		override protected function getDrawingTarget():Sprite {
			return this.chartBackground;
		}
		
		public function getMainSprite():Sprite { return this.sprite; }
		
		private var _isInitialized:Boolean;
		public function get isInitialized():Boolean { return this._isInitialized; }
		
		//-----------------------------------------------------------
		//	IViewElement
		//-----------------------------------------------------------
		
		private var margin:Margin;
		public function setMargin(margin:Margin):void { this.margin = margin; }
		
		public function getTitle():String {
			return (this.title != null) ? this.title.info.formattedText : null;
		}
		
		public function getBounds():Rectangle {
			return this.bounds;
		}
		
		public function getContainer():Sprite {
			return this.content;
		}
		
		public function getAnimation():AnimationManager { return this.animation; }
		//-------------------------------------------------------
		// DEFAULT TEMPLATE
		//-------------------------------------------------------
		
		[Embed(source="../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var stringData:String = b.readUTFBytes(b.length);
			var data:XML = new XML(GlobalTemplateItems.createTemplateXML(stringData));
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		//-------------------------------------------------------
		// Container
		//-------------------------------------------------------
		
		/**
		 * Plot container
		 */
		private var _plotContainer:DisplayObject;
		
		//-------------------------------------------------------
		// Crop chart for animation
		//-------------------------------------------------------
		
		private function cropChart(bounds:Rectangle):void {
			var scrollRect:Rectangle = new Rectangle();
			this.getCropBounds(scrollRect, bounds);
			this.sprite.scrollRect = scrollRect;
		}
		
		private function recropChart(bounds:Rectangle):void {
			var scrollRect:Rectangle = this.sprite.scrollRect.clone();
			this.getCropBounds(scrollRect, bounds);
			this.sprite.scrollRect = scrollRect;
		}
		
		private function getCropBounds(scrollRect:Rectangle, bounds:Rectangle):void {
			scrollRect.width = bounds.width;
			scrollRect.height = bounds.height;
			scrollRect.x = 0;
			scrollRect.y = 0;
			this.sprite.x = bounds.x;
			this.sprite.y = bounds.y;
		}
		
		private var noDataText:NoDataMessage;
		
		//-------------------------------------------------------
		// Plot
		//-------------------------------------------------------
		
		private var _plot:IPlot;
		public function get plot():IPlot { return this._plot; }
		public function set plot(value:IPlot):void { 
			this._plot = value;
			this._plot.setContainer(this); 
		}
		
		public var selectionDrawingContainer:Shape;
		public var selectionEventsProviderUnderChart:Sprite;
		public var selectionEventsProviderAfterChart:Sprite;
		
		public var afterPlotContainer:Sprite;
		public var drawAfterCaller:Function;
		public var resizeAfterCaller:Function;
		
		//-------------------------------------------------------
		// Chart size
		//-------------------------------------------------------
		
		private var _chartRectangle:Rectangle;
		private var _plotRectangle:Rectangle;
		
		//-------------------------------------------------------
		// IResizable
		//-------------------------------------------------------		
		
		private var titleContainer:DisplayObject;
		private var subtitleContainer:DisplayObject;
		private var footerContainer:DisplayObject;
		
		public function getChartTotalBounds():Rectangle {
			return this.bounds;
		}
		
		override public function initialize(fullBounds:Rectangle):DisplayObject {
			this._isInitialized = true;
			var bounds:Rectangle = fullBounds.clone();
			if (this.margin != null)
				this.margin.applyInside(bounds);
			this.chartBackground = new Sprite();
			this.content = new Sprite();
			
			this.selectionDrawingContainer = new Shape();
			this.selectionEventsProviderUnderChart = new Sprite();
			
			this.selectionEventsProviderAfterChart = new Sprite();
			this.selectionEventsProviderAfterChart.mouseEnabled = false;
			
			this.content.addChild(this.selectionEventsProviderUnderChart);
			
			this.content.addChild(this.chartBackground);
			super.initialize(bounds);
			
			this.sprite.addChild(this.content);
			
			if (!this.hasData) {
				this.content.addChild(this.noDataText.initialize(bounds));
				return this.sprite;
			}
			
			this.cropChart(fullBounds);
			
			if (this.chartAnimation)
				this.animation.registerDisplayObject(this.content, this.sprite, this.chartAnimation, false, false);
			
			if (this.title != null) {
				this.titleContainer = this.title.initialize(null);
				this.content.addChild(this.titleContainer);
			}
			if (this.subtitle != null) {
				this.subtitleContainer = this.subtitle.initialize(null);
				this.content.addChild(this.subtitleContainer);
			}
			if (this.footer != null) {
				this.footerContainer = this.footer.initialize(null);
				this.content.addChild(this.footerContainer);
			}
			
			if (this.controls != null)
				this.controls.initialize();
			
			//plot initialization
			if (this._plot != null) {
				
				var innerBounds:Rectangle = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
				var nonMarginInnerBounds:Rectangle = innerBounds.clone();
				if (this.background != null) 
					this.background.applyInsideMargins(innerBounds);
				
				this._chartRectangle = innerBounds.clone();
				this.initTitles();
				this.cropTitles(innerBounds);
				
				var chartRectangleWithoutTitlesAndWithoutMargin:Rectangle = nonMarginInnerBounds.clone();
				var chartRectangleWithoutTitles:Rectangle = innerBounds.clone();
				
				this._plot.preInitialize();
				this.controls.setLayout(innerBounds, chartRectangleWithoutTitles);
				
				this._plotContainer = this._plot.initialize(innerBounds);
				this._plotRectangle = this._plot.getBounds();
				
				this.cropPlot(chartRectangleWithoutTitles, chartRectangleWithoutTitlesAndWithoutMargin);
				
				if (this._plotContainer != null) 
					this.content.addChild(this._plotContainer);
					
				this.content.addChild(this.controls.container);
				
				//tooltip container
				this.initTooltipSprite();
				
				if (this.drawAfterCaller != null)
					this.content.addChild(this.afterPlotContainer);
			}
			
			this.sprite.addChild(this.selectionEventsProviderAfterChart);
			this.sprite.addChild(this.selectionDrawingContainer);
			
			return this.sprite;
		}
		
		public function clearTooltips():void {
			if (this._plot.tooltipsContainer) {
				while (this._plot.tooltipsContainer.numChildren > 0)
					this._plot.tooltipsContainer.removeChildAt(0);
			}
		}
		
		private function initTooltipSprite():void {
			var obj:Object = null;
			try {
				obj = getDefinitionByName("mx.managers::SystemManager");
				if (obj && obj["getSWFRoot"] != undefined) {
					obj = obj.getSWFRoot(this);
					if (obj) obj = obj.topLevelSystemManager;
					if (obj) obj = obj.toolTipChildren;
				}
			}catch (e:Error){}
			
			if (obj) {
				obj.addChild(this._plot.tooltipsContainer);
			}else {
				this.content.addChild(this._plot.tooltipsContainer);
			}
		}
		
		public function postInitialize():void {
			if (this.hasData && this._plot != null)
				this._plot.postInitialize();
		}
		
		private function cropPlot(chartRectangleWithoutTitles:Rectangle, chartRectangleWithoutTitlesAndMargins:Rectangle):void {
			var finalized:Boolean = this.controls.finalizeLayout(this._plot.getBounds(), chartRectangleWithoutTitles, chartRectangleWithoutTitlesAndMargins);
			var cnt:int = 0;
			while (!finalized) {
				cnt ++;
				if (cnt == 10) {
					//this.base.showFatalError(new Error());
					return;
				}
				var innerBounds:Rectangle = chartRectangleWithoutTitles.clone();
				this.controls.cropPlot(innerBounds);
				this._plot.calculateResize(innerBounds);
				this._plotRectangle = this._plot.getBounds();
				finalized = this.controls.finalizeLayout(this._plot.getBounds(), chartRectangleWithoutTitles, chartRectangleWithoutTitlesAndMargins);
			}
		}
		
		override public function resize(bounds:Rectangle):void {
			var newBounds:Rectangle;
			if (!this.hasData) {
				newBounds = bounds.clone();
				if (this.margin != null)
					this.margin.applyInside(newBounds);
				
				super.resize(newBounds);
				this.noDataText.resize(newBounds);
				return;
			}
			
			this.recropChart(bounds);
			newBounds = bounds.clone();
			if (this.margin != null)
				this.margin.applyInside(newBounds);
				
			super.resize(newBounds);
			if (this._plot != null) {
				var innerBounds:Rectangle = newBounds.clone();
				var nonMarginInnerBounds:Rectangle = innerBounds.clone();
				
				if (this.background != null) 
					this.background.applyInsideMargins(innerBounds);
					
				this._chartRectangle = innerBounds.clone();
				this.cropTitles(innerBounds);
				
				var chartRectangleWithoutTitlesAndWithoutMargin:Rectangle = nonMarginInnerBounds.clone();
				var chartRectangleWithoutTitles:Rectangle = innerBounds.clone();
				
				this.controls.setLayout(innerBounds, chartRectangleWithoutTitles);
				
				this._plot.calculateResize(innerBounds);
				this._plotRectangle = this._plot.getBounds();
				
				this.cropPlot(chartRectangleWithoutTitles, chartRectangleWithoutTitlesAndWithoutMargin);
				
				this._plot.execResize();
				
				this.controls.resize();
				
				this.drawTitles();
			}
			if (this.resizeAfterCaller != null)
				this.resizeAfterCaller(this.bounds);
			
			this.selectionEventsProviderUnderChart.graphics.clear();
			this.selectionEventsProviderUnderChart.graphics.beginFill(0xFFFFFF,0.01);
			this.selectionEventsProviderUnderChart.graphics.drawRect(0,0,this.bounds.width,this.bounds.height);
			this.selectionEventsProviderUnderChart.graphics.endFill();
			
			this.selectionEventsProviderAfterChart.graphics.clear();
			this.selectionEventsProviderAfterChart.graphics.beginFill(0xFFFFFF,0.01);
			this.selectionEventsProviderAfterChart.graphics.drawRect(0,0,this.bounds.width,this.bounds.height);
			this.selectionEventsProviderAfterChart.graphics.endFill();
		}
		
		override public function draw():void {
			super.draw();
			if (!this.hasData) {
				this.noDataText.drawTitle();
				return;
			}
			if (this._plot != null) {
				this._plot.draw();
				this.controls.draw();
			}
			this.drawTitles();
			
			this.selectionEventsProviderUnderChart.graphics.clear();
			this.selectionEventsProviderUnderChart.graphics.beginFill(0xFFFFFF,0.01);
			this.selectionEventsProviderUnderChart.graphics.drawRect(0,0,this.bounds.width,this.bounds.height);
			this.selectionEventsProviderUnderChart.graphics.endFill();
			
			this.selectionEventsProviderAfterChart.graphics.clear();
			this.selectionEventsProviderAfterChart.graphics.beginFill(0xFFFFFF,0.01);
			this.selectionEventsProviderAfterChart.graphics.drawRect(0,0,this.bounds.width,this.bounds.height);
			this.selectionEventsProviderAfterChart.graphics.endFill();
			
			if (this.drawAfterCaller != null)
				this.drawAfterCaller(this.bounds);
			this.startAnimation();			
		}
		
		public function startAnimation():void
		{
			if (this.animation)
				this.animation.animate(this.plot ? this.plot.getBounds() : this.bounds);	
		}
		
		//-------------------------------------------------------
		// Titles
		//-------------------------------------------------------
		
		private var title:ChartTitle;
		private var subtitle:ChartTitle;
		private var footer:ChartTitle;
		
		private function initTitles():void {
			if (this.title != null) this.title.initTitleText(this._plot);
			if (this.subtitle != null) this.subtitle.initTitleText(this._plot);
			if (this.footer != null) this.footer.initTitleText(this._plot);
		}
		
		private function cropTitles(rect:Rectangle):void {
			if (this.footer != null) this.footer.crop(rect);
			if (this.title != null) this.title.crop(rect);
			if (this.subtitle != null) this.subtitle.crop(rect);
		}
		
		private function drawTitles():void {
			if (this.footer != null) this.footer.setPosition(this._chartRectangle, this._plotRectangle);
			if (this.title != null) this.title.setPosition(this._chartRectangle, this._plotRectangle);
			if (this.subtitle != null) this.subtitle.setPosition(this._chartRectangle, this._plotRectangle);
		}
		
		//-------------------------------------------------------
		// ISerializable
		//-------------------------------------------------------
		
		public function processXMLData(data:XML):void {
			//PlotFactory.createSinglePlot(this._chartBase, this, data);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			
			this.hasData = false;
			
			if (data == null) {
				this.showNoData(data);
				return;
			}else if (this.plot.checkNoData(data)) {
				this.deserialzieChartBackground(data, resources);
				this.showNoData(data);
				return;
			}
			
			this.hasData = true;
			
			this.animation = new AnimationManager();
			this.animation.loop = this.base.loopAnimation;
			this.animation.loopDelay = this.base.loopDelay;
			this.animation.enabled = this.base.enableAnimation;
			
			StylesList.initializeParsing();
			
			var styles:XML = data.styles[0];
			
			if (data.@animate != undefined)
				this.animation.enabled = SerializerBase.getBoolean(data.@animate);
				
			if (data.chart_settings[0] != null) {
				if (SerializerBase.isEnabled(data.chart_settings[0].chart_animation[0])) {
					this.chartAnimation = new Animation();
					this.chartAnimation.deserialize(data.chart_settings[0].chart_animation[0], styles);
					this.animation.startTime = this.chartAnimation.startTime + this.chartAnimation.duration;
				}
			}
			
			this.controls = new PlotControlsList(this.base, this.plot, this);
			
			// data plot
			if (this._plot != null) {
				this._plot.setAnimationManager(this.animation);
				this._plot.setControlsList(controls);
				try {
					this._plot.deserialize(data,resources);
				}catch (e:FeatureNotSupportedError) {
					if (PlotFactoryBase.IS_ORACLE)
						this.chartView.showErrorMessage(e.failMessage, e.url);
					else
						throw e;
				}
			}
			
			// Chart settings.
			if (data.chart_settings[0] != null) {
				
				this.deserialzieChartBackground(data, resources);
				
				if (data.chart_settings[0].controls[0] != null)
					this.controls.deserialize(data.chart_settings[0].controls[0]);
				
				if (SerializerBase.isEnabled(data.chart_settings[0].title[0])) {
					this.title = new ChartTitle(this.base);
					this.title.deserialize(data.chart_settings[0].title[0], resources);
				}
				if (SerializerBase.isEnabled(data.chart_settings[0].subtitle[0])) {
					this.subtitle = new ChartTitle(this.base);
					this.subtitle.deserialize(data.chart_settings[0].subtitle[0], resources);
				}
				if (SerializerBase.isEnabled(data.chart_settings[0].footer[0])) {
					this.footer = new ChartTitle(this.base);
					this.footer.deserialize(data.chart_settings[0].footer[0], resources);
				}
				if (SerializerBase.isEnabled(data.chart_settings[0].legend[0]) && this._plot != null)
					this.controls.deserializeControl(data.chart_settings[0].legend[0], styles, resources);
				if (data.chart_settings[0].controls[0] != null) {
					var controlsList:XMLList = data.chart_settings[0].controls[0].children();
					var controlsCnt:int = controlsList.length();
					for (var i:int = 0;i<controlsCnt;i++) {
						var controlXML:XML = controlsList[i];
						if (SerializerBase.isEnabled(controlXML))
							this.controls.deserializeControl(controlXML, styles, resources);
					}
				}
			}
		}
		
		public function showNoData(data:XML):void {
			this.hasData = false;
			
			this.noDataText = new NoDataMessage(this.base);
			this.noDataText.deserialize(AnyChart(this.base).settingsNoData, null, null);
			if (this.noDataText.noDataText == null || this.noDataText.noDataText.length == 0) 
				this.noDataText.noDataText = AnyChart(this.base).messageNoData;
		}
		
		
		private function deserialzieChartBackground(data:XML, resources:ResourcesLoader):void {
			if (data != null && data.chart_settings[0] != null) {
				if (data.chart_settings[0].chart_background[0] != null) {
					this.background = new RectBackground();
					var enabled:Boolean = true;
					if (data.chart_settings[0].chart_background[0].@enabled != undefined) {
						enabled = SerializerBase.getBoolean(data.chart_settings[0].chart_background[0].@enabled);
						data.chart_settings[0].chart_background[0].@enabled = "true";
					}
					this.background.deserialize(data.chart_settings[0].chart_background[0], resources);
					if (!enabled) {
						this.background.fill = null;
						this.background.hatchFill = null;
						this.background.border = null;
						this.background.effects = null;
						this.background.corners = null
					}
				}
			}
		}
		
		//-------------------------------------------------------
		// IObjectSerializable
		//-------------------------------------------------------
		
		public function serialize(res:Object = null):Object {
			if (res == null)
				res = {};
				
			if (this.title) res.Title = this.title.info.formattedText;
			if (this.subtitle) res.SubTitle = this.subtitle.info.formattedText;
			if (this.footer) res.Footer = this.footer.info.formattedText;
			
			if (this.hasData && this.plot)
				this.plot.serialize(res);
			
			return res;
		}
	}
}