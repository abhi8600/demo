package com.anychart.viewController {
	public final class ChartViewStatus {
		public static const ELEMENT:uint = 0;
		public static const PRELOADER:uint = 1;
		public static const ERROR:uint = 2;
	}
}