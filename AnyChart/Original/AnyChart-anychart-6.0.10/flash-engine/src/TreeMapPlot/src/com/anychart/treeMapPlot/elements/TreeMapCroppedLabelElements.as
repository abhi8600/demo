package com.anychart.treeMapPlot.elements {
	import com.anychart.elements.IElementContainer;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public final class TreeMapCroppedLabelElements extends LabelElement {
		
		override protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			var info:TextElementInformation = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			var pointBounds:Rectangle = BasePoint(container).bounds;
			if (pointBounds.width <= 5) return;
			
			var bounds:Rectangle = new Rectangle(sprite.x, sprite.y, info.rotatedBounds.width, info.rotatedBounds.height);
			if (!pointBounds.containsRect(bounds)) {
				var rc:Rectangle = pointBounds.clone();
				rc.x = 0;
				rc.y = 0;
				if (pointBounds.width < bounds.width) {
					sprite.x = pointBounds.x;
					rc.x = pointBounds.x - bounds.x; 
				}
				if (pointBounds.height < bounds.height) {
					sprite.y = pointBounds.y;
					rc.y = pointBounds.y - bounds.y;
				}
				sprite.scrollRect = rc;
			} else {
				sprite.scrollRect = null;
			}
			
			super.execDrawing(container, state, sprite, elementIndex);
				
		}		
	}
}