package com.anychart.treeMapPlot.data {
	
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;

	internal class TreeMapBasePoint extends BasePoint implements ITreeMapItem {
		
		//--------------------------------------------
		// ITreeMapItem
		//--------------------------------------------
		protected var _parent:TreeMapSeries;
		
		public function get parent():TreeMapSeries { return this._parent; }
		public function set parent(value:TreeMapSeries):void{ this._parent = value; }
		
		protected var _value:Number;
		
		public function get value():Number { return this._value; }
		public function set value(value:Number):void { this._value = value; }
		
		protected var _valueScale:Number;
		
		public function get valueScale():Number { return this._valueScale; }
		public function set valueScale(value:Number):void { this._valueScale = value; }
		
		protected var _widthScale:Number;
		
		public function get widthScale():Number { return this._widthScale; }
		public function set widthScale(value:Number):void { this._widthScale = value; }
		
		protected var _heightScale:Number;	
		
		public function get heightScale():Number { return this._heightScale; }
		public function set heightScale(value:Number):void { this._heightScale = value; }
		
		public function get width():Number { return this.bounds.width; }
		public function set width(value:Number):void { this.bounds.width = value; }
		
		public function get height():Number { return this.bounds.height; }
		public function set height(value:Number):void { this.bounds.height = value; }

		public function get x():Number { return this.bounds.x; }
		public function set x(value:Number):void { this.bounds.x = value; }
		
		public function get y():Number { return this.bounds.y; }
		public function set y(value:Number):void { this.bounds.y = value; }
		
		public function deserializeData(deserParams:TreeMapDeserializationParams):void { }
		
		protected var _visible:Boolean;
		public function get visible():Boolean { return this._visible; }
		public function set visible(value:Boolean):void { 
			this._visible = value;
			if (this.hasPersonalContainer) 
				this.container.visible = value;
			if (this.labelSprite) this.labelSprite.visible = value;
			if (this.markerSprite) this.markerSprite.visible = value;
			if (this.tooltipSprite) this.tooltipSprite.visible = value;
			if (!this.elementsInfo)
				this.elementsInfo = {};
			if (!this.elementsInfo[LABEL_INDEX])
				this.elementsInfo[LABEL_INDEX] = {};
			this.elementsInfo[LABEL_INDEX].enabled = value;
			if (!this.elementsInfo[MARKER_INDEX])
				this.elementsInfo[MARKER_INDEX] = {};
			this.elementsInfo[MARKER_INDEX].enabled = value;
			if (!this.elementsInfo[TOOLTIP_INDEX])
				this.elementsInfo[TOOLTIP_INDEX] = {};
			this.elementsInfo[TOOLTIP_INDEX].enabled = value;
		}
		
		//-------------------------------------
		// DRAWING
		//-------------------------------------
		override protected function execDrawing():void {
			if (this._visible){
				super.execDrawing();
				this.drawBox();
			}
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == "%Value" || token == "%YValue") return this._value;
			return super.getPointTokenValue(token); 
		}
		
		override protected function redraw():void {
			if (this._visible){
				super.redraw();
				this.drawBox();
			}
		}
		
		protected function drawBox():void {}
	}
}