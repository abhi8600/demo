package com.anychart.treeMapPlot {
	
	import com.anychart.actions.Action;
	import com.anychart.data.SeriesType;
	import com.anychart.palettes.BasePalette;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.treeMapPlot.actions.TreeMapSetRootAction;
	import com.anychart.treeMapPlot.data.ITreeMapItem;
	import com.anychart.treeMapPlot.data.TreeMapDeserializationParams;
	import com.anychart.treeMapPlot.data.TreeMapFactory;
	import com.anychart.treeMapPlot.data.TreeMapGlobalSeriesSettings;
	import com.anychart.treeMapPlot.data.TreeMapSeries;
	import com.anychart.utils.XMLUtils;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	public final class TreeMapPlot extends SeriesPlot	{
		
		//--------------------------------
		// TEMPLATES
		//--------------------------------
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		//-----------------------------------
		// DATA DESERIALIZATION
		//-----------------------------------
		private var rootTreeMapItem:ITreeMapItem;
		private var currRoot:ITreeMapItem;
		public function get globalSettings():TreeMapGlobalSeriesSettings { return this.settings[SeriesType.TREE_MAP]; }
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings { return new TreeMapGlobalSeriesSettings(); }
		
		override protected function deserializeData(data:XML, resources:ResourcesLoader):void {
			var xmlData:XML = data.data[0];
			
			if (xmlData == null)
				return;
				
			var xmlStyles:XML = data.styles[0];
			var xmlPalletes:XML = data.palettes[0];
			var xmlDataPlotSettings:XML = data.data_plot_settings[0];
			
			this.getSeriesSettings(SeriesType.TREE_MAP, xmlDataPlotSettings, xmlStyles, resources, xmlData); 
			
			var deserParams:TreeMapDeserializationParams = new TreeMapDeserializationParams();
			deserParams.xmlData = xmlData;
			deserParams.xmlStyles = xmlStyles;
			deserParams.xmlPalletes = xmlPalletes;
			deserParams.resources = resources;
			deserParams.index = 0;
			deserParams.threshold = null;
			
			TreeMapFactory.initFactory(xmlData);
			this.rootTreeMapItem = TreeMapFactory.createTree(deserParams, this.globalSettings);
			this.setRoot(BaseSeries(this.rootTreeMapItem));
			
			this.checkTresholdsAfterDeserialize();
		}
		
		private function setRoot(newRoot: BaseSeries):void {
			if (this.currRoot != null)
				this.currRoot.visible = false;
			this.currRoot = ITreeMapItem(newRoot);
			/* var len:int = this.onBeforeDrawSeries.length;
			if (len > 1){
				for (var i:int = 0; i < len; i++){
					this.onBeforeDrawSeries[i] = null;
					delete this.onBeforeDrawSeries[i];
				}
				this.onBeforeDrawSeries.length = 1;
			} */
			this.onBeforeDrawSeries[0] = this.currRoot;
			
			this.currRoot.visible = true;
			
			if (this.bounds != null)
				this.resize(this.bounds);
		}
		
		public function drillDown(newRoot:BaseSeries):void {
			if (newRoot == this.currRoot){
				if (newRoot != this.rootTreeMapItem)
					this.setRoot(BaseSeries(ITreeMapItem(newRoot).parent));
			} else  
				this.setRoot(newRoot); 
		}
		public function drillUp():void { this.setRoot(BaseSeries(this.rootTreeMapItem)); }
		
		public function getSeriesPaletteWrapper(paletteClass:Class, dataPalette:BasePalette, 
			globalSettings:GlobalSeriesSettings, seriesNode:XML, palettes:XML, paletteType:String):BasePalette {
			return this.getSeriesPalette(paletteClass, dataPalette, globalSettings, seriesNode, palettes, paletteType);	
		}
		
		
		//-----------------------------------
		// DATA VISUALISATION
		//-----------------------------------

		private static var actsInitialized:Boolean = initActs();
		private static function initActs():Boolean {
			Action.register("setroot", TreeMapSetRootAction);
			return true; 
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject{
			var res:DisplayObject = super.initialize(bounds);
			this.sendBounds();
			return res;
		}
		
		override protected function resizeSeries():void {
			this.sendBounds();
			super.resizeSeries();
		}
		
		private function sendBounds():void {
			if (!this.rootTreeMapItem) return;
			this.currRoot.x = 0;
			this.currRoot.y = 0;
			var tmp:Number;
			this.currRoot.width = (tmp = this.getBounds().width) < 1 ? 1 : tmp - 1;
			this.currRoot.height = (tmp = this.getBounds().height) < 1 ? 1 : tmp - 1;
		}
		
		override public function checkNoData(data:XML):Boolean {
			if (data.data[0] == null) return true;
			if (data.data[0]..point.length() == 0) return true;
			return false;
		}
		
		//---------------------------------------
		// UPDATING
		//---------------------------------------
		
		override public function addPoint(seriesId:String, ...pointXML):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0]..point, seriesId);
			
			if (seriesNode != null) {
				var pointsCnt:uint = pointXML.length;
				for (var i:uint = 0;i<pointsCnt;i++)
					seriesNode.point += new XML(pointXML[i]);
			}
		}
		
		override public function addPointAt(seriesId:String, pointIndex:uint, pointData:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0]..point, seriesId);
			if (seriesNode != null) {
				seriesNode.point = XMLUtils.insert(seriesNode.point, pointIndex, new XML(pointData));
			}
		}
		
		override public function removePoint(seriesId:String, pointId:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0]..point, pointId);
			if (seriesNode == null) return;
			seriesNode = seriesNode.parent();
			if (seriesNode != null)
				XMLUtils.removeById(seriesNode.point, pointId);
		}
		
		override public function updatePoint(seriesId:String, pointId:String, pointXML:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0]..point, pointId);
			if (seriesNode == null) return;
			seriesNode = seriesNode.parent();
			if (seriesNode != null) {
				var pointIndex:int = XMLUtils.findById(seriesNode.point, pointId);
				if (pointIndex != -1)
					seriesNode.point[pointIndex] = XMLUtils.mergeDataNode(seriesNode.point[pointIndex], new XML(pointXML));
			}
		}
		
		//---------------------------------------
		// HIGHLIGHT && SELECT
		//---------------------------------------
		
		private function getItemById(pointId:String, root:TreeMapSeries = null):ITreeMapItem {
			if (root == null) 
				root = TreeMapSeries(this.rootTreeMapItem);
			
			if (root.id == pointId) return root;
			for each (var pt:ITreeMapItem in root.points){
				if (pt.id == pointId) return pt;
			}
			
			for each (pt in root.points){
				if (pt is TreeMapSeries) {
					var res:ITreeMapItem = this.getItemById(pointId, TreeMapSeries(pt));
					if (res != null) return res;
				}
			}
			
			return null;
		}
		
		override public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {
			if (pointId == null) return;
			var pt:ITreeMapItem = this.getItemById(pointId, null);
			
			if (pt) {
				if (highlighted)
					pt.setHover();
				else
					pt.setNormal();
			}
		}
		
		override public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {
			if (pointId == null) return;
			var pt:ITreeMapItem = this.getItemById(pointId, null);
			
			if (pt) {
				if (pt is TreeMapSeries) {
					if (selected) {
						this.drillDown(TreeMapSeries(pt));
					}else {
						this.drillUp();
					}
				}else {
					var basePoint:BasePoint = BasePoint(pt);
					basePoint.setSelected(selected);
				}
			}
		}
	}
}