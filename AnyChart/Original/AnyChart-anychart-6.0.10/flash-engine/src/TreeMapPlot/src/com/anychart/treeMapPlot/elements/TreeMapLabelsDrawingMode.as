package com.anychart.treeMapPlot.elements {
	
	public final class TreeMapLabelsDrawingMode {
		
		public static const DISPLAY_ALL:uint = 0;
		public static const CROP:uint = 1;
		public static const HIDE:uint = 2; 		
	}
}