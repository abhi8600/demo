package com.anychart.treeMapPlot.data {
	
	import com.anychart.elements.marker.MarkerType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.treeMapPlot.style.TreeMapBaseStyleState;
	import com.anychart.visual.hatchFill.HatchFillType;
	import com.anychart.visual.layout.Margin;
	
	import flash.geom.Rectangle;

	public class TreeMapSeries extends BaseSeries implements ITreeMapItem {
		//--------------------------------------------
		// Constructor
		//--------------------------------------------
		
		public function TreeMapSeries(){
			super();
			this.bounds = new Rectangle();
		}
		
		//--------------------------------------------
		// ITreeMapItem
		//--------------------------------------------
		protected var _parent:TreeMapSeries;
		
		public function get parent():TreeMapSeries { return this._parent; }
		public function set parent(value:TreeMapSeries):void{ this._parent = value; }
		
		protected var _value:Number;
		
		public function get value():Number { return this._value; }
		public function set value(val:Number):void { this._value = val; }
		
		protected var _valueScale:Number;
		
		public function get valueScale():Number { return this._valueScale; }
		public function set valueScale(value:Number):void { this._valueScale = value; }
		
		protected var _widthScale:Number;
		
		public function get widthScale():Number { return this._widthScale; }
		public function set widthScale(value:Number):void { this._widthScale = value; }
		
		protected var _heightScale:Number;	
		
		public function get heightScale():Number { return this._heightScale; }
		public function set heightScale(value:Number):void { this._heightScale = value; }
		
		public function get width():Number { return this.bounds.width; }
		public function set width(value:Number):void { this.bounds.width = value; }
		
		public function get height():Number { return this.bounds.height; }
		public function set height(value:Number):void { this.bounds.height = value; }
		
		public function get x():Number { return this.bounds.x; }
		public function set x(value:Number):void { this.bounds.x = value; }
		
		public function get y():Number { return this.bounds.y; }
		public function set y(value:Number):void { this.bounds.y = value; }
		
		public function get visible():Boolean { return this.selfPoint.visible; }
		public function set visible(value:Boolean):void {
			var len:int = this.points.length;
			if (this.selfPoint != null)
				this.selfPoint.visible = value; 
			for (var i:int = 0; i < len; i++)
				ITreeMapItem(this.points[i]).visible = value; 
		}
		
		//------------------------------------------
		// DESERIALIZATION
		//-----------------------------------------
		
		private var selfPoint:TreeMapSeriesPoint;
		public function sendValueToPoint():void {if (this.selfPoint) this.selfPoint.value = this._value;}
		private var insideMargin:Margin;
		private var outsideMargin:Margin;
		public function deserializeData(deserParams:TreeMapDeserializationParams):void {
			this.index = deserParams.index;
			this.name = deserParams.index.toString();
			
			this.color = (deserParams.xmlData.@color == undefined) 
				? 0xCCCCCC 
				: SerializerBase.getColor(deserParams.xmlData.@color);
			this.hatchType = (deserParams.xmlData.@hatch_type == undefined) 
				? HatchFillType.BACKWARD_DIAGONAL 
				: SerializerBase.getHatchType(deserParams.xmlData.@hatch_type);																  
			this.markerType = MarkerType.CIRCLE;
			
			if (this._parent) {
				var parentSettings:BaseSeriesSettings = this._parent.settings;
				if (parentSettings.seriesContainsUniqueSettings(deserParams.xmlData)) {
					this.settings = parentSettings.createCopy(this.settings);
					this.settings.deserialiseSeries(deserParams.xmlData, deserParams.xmlStyles, deserParams.resources);
				}else {
					this.settings = parentSettings;
				}
				this._parent.setElements(this);
			}else {
				this.settings.global.setElements(this);
			}
							
			this.deserializeThreshold(this.plot.thresholdsList, deserParams.xmlData, deserParams.xmlPalletes);
			if (this.threshold == null)
				this.threshold = deserParams.threshold;
			else
				deserParams.threshold = this.threshold;
			
			this.deserializeElements(deserParams.xmlData, deserParams.xmlStyles, deserParams.resources);
			this.deserialize(deserParams.xmlData);
			
			this.initializeStyle();
			this.initializeElementsStyle();
			
			this.insideMargin = TreeMapBaseStyleState(this.settings.style.normal).insideMargin;
			this.outsideMargin = TreeMapBaseStyleState(this.settings.style.normal).outsideMargin;
			
			this.plot.series.push(this);
			
			if (this._drawSelfHeader){
				this.selfPoint = new TreeMapSeriesPoint();
				this.selfPoint.parent = this;
				var xmlCache:XML = deserParams.xmlData;
				deserParams.xmlData = <point />;
				this.selfPoint.deserializeData(deserParams);
				deserParams.xmlData = xmlCache;
				this.plot.drawingPoints.push(this.selfPoint);
			}
		}
		
		private var _drawSelfHeader:Boolean = true;
		public function set drawSelfHeader(value:Boolean):void { this._drawSelfHeader = value; } 
		public function get drawSelfHeader():Boolean { return this._drawSelfHeader; }
		
		//---------------------------
		// TREE MAP CALCULATION	
		//---------------------------
		override public function onBeforeDraw():void {
			this.calcMap();	
		}
		
		override public function onBeforeResize():void {
			this.calcMap();
		}
				
		private var tmp_dWidth:Number;
		private var tmp_dHeight:Number;
		private var bounds:Rectangle;
		public function calcMap():void {									
			
			if (this.value == 0)
				return;
				
			var width:Number = this.bounds.width;
			var height:Number = this.bounds.height;
			var x:Number = this.bounds.x;
			var y:Number = this.bounds.y;
			if (this._drawSelfHeader){
				if (this.outsideMargin != null){
					width -= this.outsideMargin.left + this.outsideMargin.right; 
					height -= this.outsideMargin.bottom + this.outsideMargin.top;
					x += this.outsideMargin.left;
					y += this.outsideMargin.top; 
				}
				this.selfPoint.bounds.height = height;
				this.selfPoint.bounds.width = width;
				this.selfPoint.bounds.x = x;
				this.selfPoint.bounds.y = y;
				var headerHeight:Number = this.selfPoint.getHeaderHeight(); 
				height -= headerHeight;
				y += headerHeight;
				if (this.insideMargin != null){
					width -= this.insideMargin.left + this.insideMargin.right;
					height -= this.insideMargin.bottom + this.insideMargin.top;
					x += this.insideMargin.left;
					y += this.insideMargin.top;			 
				}
			}
			
			var i:int;	
			var len:uint = this.points.length;
			var b:Boolean = false;
			if (width <= 1) {
				b = true;
				for (i=0; i < len; i++) 
					this.points[i].bounds.width = 0;
			}
			if (height <= 1) {
				b = true;
				for (i=0; i < len; i++) 
					this.points[i].bounds.height = 0;
			}
			if (b) { return; }
			
			this.tmp_dWidth = width;
			this.tmp_dHeight = height;
			
			var scale:Number = 0;
			var pt:ITreeMapItem;
			
			scale = (width * height) / this.value;  // определение масштаба
			
			for (i=0; i < len; i++) 
				this.points[i].valueScale = this.points[i].value * scale;																
			
			var start:int=0;
			var end:int=0;
			var vert:Boolean = goingToDrawVertically(tmp_dWidth, tmp_dHeight);
			var aspectCurr:Number = Number.MAX_VALUE;
			var aspectLast:Number;
			
			var offsetX:Number=0;
			var offsetY:Number=0;
			
			while (end < len) {							  	
			  	aspectLast = this.getAspect(start, end, vert);
			  	
			  	if (aspectLast>aspectCurr) {
			  		var currX:Number=0;
			  		var currY:Number=0;
			  		
			  		for (i=start; i<end; i++) {
			  			pt = points[i];	  			
			  			pt.x = x + offsetX + currX;
			  			pt.y = y + offsetY + currY;			  			
			  			
			  			if (vert) {
			  				currY += pt.height;
			  			} else {
			  				currX += pt.width;
			  			}			  		
			  		}
			  		
			  		if (vert) {
			  			offsetX += ITreeMapItem(points[start]).width;
			  		} else {
			  			offsetY += ITreeMapItem(points[start]).height;
			  		}
			  		
			  		this.tmp_dWidth = width - offsetX;
			  		this.tmp_dHeight = height - offsetY;
			  		
			  		vert = goingToDrawVertically(tmp_dWidth, tmp_dHeight);
			  		
			  		start = end;
			  		end = start;
			  		
			  		aspectCurr = Number.MAX_VALUE;			  		 
			  	} else {
			  		for (i=start; i<=end; i++) {
			  			pt=points[i];			  			
			  			pt.width = pt.widthScale < 1 ? 1 : pt.widthScale;
			  			pt.height = pt.heightScale < 1 ? 1 : pt.heightScale;			  			
			  		}
			  		aspectCurr = aspectLast;
			  		end++;
			  	}
			}
			
			var currX1:Number=0;
			var currY1:Number=0;
			
			for (i=start; i<end; i++) {
				pt = this.points[i];
				pt.x = x + offsetX + currX1;
				pt.y = y + offsetY + currY1;
								
				if (vert) {
					currY1 += pt.height;					
				} else {
					currX1 += pt.width;
				}		
			}
			
			for (i=0; i < len; i++){
				pt = this.points[i];
				if (pt is TreeMapSeries)
					TreeMapSeries(pt).calcMap();
			} 
			
			/* if (this.insideMargin == null){
				var pointsWidth:Number = this.points[0].width;
				var pointsX:Number = this.points[0].x;
				for (i=1; i < len; i++){
					pt = this.points[i];
					if (pointsX > pt.x){
						pointsWidth += pointsX - pt.x;
						pointsX = pt.x;
					}
					var tmp:Number = pt.x - pointsX + pt.width;
					if (tmp > pointsWidth)
						pointsWidth = tmp;
				}
				this.selfPoint.bounds.width = pointsWidth;
				this.selfPoint.bounds.x = pointsX;
			} */
		}
		
		/**
		 * Метод для сравнения ширины и высоты области рисования
		 * @param width ширина области рисования
		 * @param height высота области рисования
		 * @return <code>true</code>, если ширина больше высоты, иначе <code>false</code> 	 		 		  
		 */
		private function goingToDrawVertically(width:Number, height:Number): Boolean {
			return width > height;
		} 
		
		/**
		 * Метод, вычисляющий коэффициент aspect.
		 * @param start начальная граница
		 * @param end конечная граница
		 * @param vert перемееная, которую возвращает метод drawVert
		 * @see TreeMap#drawVert
		 * @return коэффициент aspect  	 		 		  
		 */
		private function getAspect(start:Number, end:Number, vert:Boolean):Number {
			var total:Number = 0;
			var aspect:Number = 0;
			var localWidth:Number;
			var localHeight:Number;
			
			var pt:ITreeMapItem;
			var i:int;
			
			for (i=start; i <= end; i++) 
				total += this.points[i].valueScale;				
			
			if (vert) {
				localHeight = this.tmp_dHeight/total;
				localWidth = total/this.tmp_dHeight;
			} else {
				localWidth = this.tmp_dWidth/total;
				localHeight = total/this.tmp_dWidth;
			}
			
			for (i=start; i <= end; i++) {
				pt = this.points[i];
				
				if (vert) {
					pt.heightScale = localHeight*pt.valueScale;
					pt.widthScale = localWidth;
				} else {
					pt.widthScale = localWidth*pt.valueScale;
					pt.heightScale = localHeight;
				}
			}
			
			pt = this.points[end];
			aspect = Math.max(pt.heightScale/pt.widthScale, pt.widthScale/pt.heightScale);
			
			return aspect;
		}
			
	}
}