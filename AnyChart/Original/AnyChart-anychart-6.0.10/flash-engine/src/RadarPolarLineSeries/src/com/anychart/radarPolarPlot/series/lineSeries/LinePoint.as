package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class LinePoint extends RadarPolarPlotPoint {
		
		protected var prev:int;
		protected var next:int;
		
		protected var hasPrev:Boolean;
		protected var hasNext:Boolean;
		
		protected var isValuePointsInitialized:Boolean;
		public var isLineDrawable:Boolean;
		//public var needCalcPixValue:Boolean;
		
		public var drawingPoints:Array;
		protected var pixValuePt:Point;
		
		protected function isGradientState(state:StyleState):Boolean {
			var l:LineStyleState = LineStyleState(state);
			return (l.stroke != null && l.stroke.enabled && l.stroke.type == StrokeType.GRADIENT);
		}
		
		public function LinePoint() {
			super();
			this.pixValuePt = new Point();
			this.isValuePointsInitialized = false;
			this.isLineDrawable = false;
			this.prev = -1;
			this.next = -1;
			//this.needCalcPixValue = true;
		}
		
		override protected function addContainerToSeriesContainer():void {
			LineSeries(this.series).container.addChild(this.container);
		}
		
		protected function initializePixValues():void {
			if (!this.isValuePointsInitialized) {
				var isClosed:Boolean = true;
				if (this.series is LineSeries) 
					isClosed = LineSeries(this.series).isClosed;
				
				var pointsCount:int = int(this.series.points.length);
				var closed:Boolean = LineSeries(this.series).isClosed;
				
				if (this.index == 0)
					this.hasPrev = isClosed && !this.series.points[this.series.points.length-1].isMissing;
				else
					this.hasPrev = (this.index > 0) && 
						(this.series.points[this.index - 1] != null) &&
						(!this.series.points[this.index - 1].isMissing || this.plot.interpolateMissings);
				
				if (this.index == this.series.points.length-1)
					this.hasNext = isClosed && this.series.points[0] != null && !this.series.points[0].isMissing;
				else
					this.hasNext = (this.index < (this.series.points.length-1) ) &&
						(this.series.points[this.index + 1] != null) &&
						(!this.series.points[this.index + 1].isMissing || this.plot.interpolateMissings);
				
				this.isLineDrawable = this.hasPrev || this.hasNext;
				
				this.isValuePointsInitialized = true;
			}
			
			RadarPolarPlot(this.plot).transform(this.x, this.stackValue, this.pixValuePt);
		}
		
		protected function createDrawingPoints():void {}
		
		override protected final function execDrawing():void {
			//if (this.needCalcPixValue)
			this.initializePixValues();
			if (this.bounds == null) this.bounds = new Rectangle();
			this.bounds.x = this.pixValuePt.x;
			this.bounds.y = this.pixValuePt.y;
			this.bounds.width = 0;
			this.bounds.height = 0;
			
			if (this.isLineDrawable) {
				this.createDrawingPoints();
				
				super.execDrawing();
				this.drawSegment(this.container.graphics);
			}
		}
		
		override protected function redraw():void {
			if (this.isLineDrawable) {
				super.redraw();
				this.drawSegment(this.container.graphics);
			}
		} 
		
		protected function drawSegment(g:Graphics):void {}
	}
}