package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class LineSeries extends RadarPolarPlotSeries {
		
		public var isClosed:Boolean;
		public var gradientBounds:Rectangle;
		public var needCreateGradientRect:Boolean;
		
		public var valueAxisStart:Number;
		public var minimum:Number;
		public var maximum:Number;
		
		
		public function LineSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
			this.needCreateGradientRect = false;
			this.container = new Sprite();
			this.isClosed = true;
		}
		
		override public function onBeforeResize():void {
			this.doGradientBounds();
		}
		
		
		override public function onBeforeDraw():void {
			this.global.container.addChild(this.container);
			
			var scaleMinimum:Number = RadarPolarPlot(this.plot).getYScaleMinimum();
			this.valueAxisStart = scaleMinimum;
			this.doGradientBounds();
		}

		private var lt:Point;
		private var rb:Point;
		
		private function doGradientBounds():void {
			if (!this.needCreateGradientRect)
				return;
			
			this.lt = new Point (Number.MAX_VALUE,Number.MAX_VALUE);
			this.rb = new Point (-Number.MAX_VALUE,-Number.MAX_VALUE);
			
			var point:RadarLinePoint;
			var point1:PolarLinePoint;
			if (this.plot is RadarPlot)
				for (var j:int=0; j<this.points.length; j++){
					point = RadarLinePoint(this.points[j]);
					if (point == null) continue;
					if (!point.isMissing){
						point.needCreateDrawingPoints = true;
						point.needInitializePixValues = true;
						
						point.doInitializePixValues();
						
						if (point.isLineDrawable)
							point.doCreateDrawingPoints();
						
						this.getRectPoints(point);
						this.lt.x = Math.min(point.bottomEnd.x,point.bottomStart.x,lt.x);
						this.lt.y = Math.min(point.bottomEnd.y,point.bottomStart.y,lt.y);
						
						rb.x = Math.max(point.bottomEnd.x,point.bottomStart.x,rb.x);
						rb.y = Math.max(point.bottomEnd.y,point.bottomStart.y,rb.y);
					}
				}
			else
				for (var ij:int=0; ij<this.points.length; ij++){
					point1 = PolarLinePoint(this.points[ij]);
					if (!point1.isMissing){
						point1.needCreateDrawingPoints = true;
						point1.needInitializePixValues = true;
						
						point1.doInitializePixValues();
						
						if (point1.isLineDrawable)
							point1.doCreateDrawingPoints();
						
						this.getRectPoints(point1);
						this.lt.x = Math.min(point1.bottomEnd.x,point1.bottomStart.x,lt.x);
						this.lt.y = Math.min(point1.bottomEnd.y,point1.bottomStart.y,lt.y);
						
						rb.x = Math.max(point1.bottomEnd.x,point1.bottomStart.x,rb.x);
						rb.y = Math.max(point1.bottomEnd.y,point1.bottomStart.y,rb.y);
					}
				}
			
			
			this.gradientBounds = new Rectangle (lt.x,lt.y,rb.x-lt.x,rb.y-lt.y);
		}	
		
		
		private function getRectPoints (point:RadarPolarPlotPoint):void {
			var drawPoints:Array = LinePoint(point).drawingPoints;
			for (var i:int=0; i<drawPoints.length;i++){
				this.lt.x = Math.min(drawPoints[i].x,lt.x);
				this.lt.y = Math.min(drawPoints[i].y,lt.y);
				
				this.rb.x = Math.max(drawPoints[i].x,rb.x);
				this.rb.y = Math.max(drawPoints[i].y,rb.y);
			}
		}
		
		override public function setAnimation(animation:Animation, manager:AnimationManager):void {
			manager.registerDisplayObject(this.container, this.plot.scrollableContainer, animation);
		}
		
		override public function createPoint():BasePoint {
			return (RadarPolarPlot(this.plot).usePolarCoords) ? new PolarLinePoint() : new RadarLinePoint();
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.deserializeClose(data);
		}
		
		protected function deserializeClose(data:XML):void {
			if (data.@close_contour != undefined)
				this.isClosed = SerializerBase.getBoolean(data.@close_contour);
		}
	}
}