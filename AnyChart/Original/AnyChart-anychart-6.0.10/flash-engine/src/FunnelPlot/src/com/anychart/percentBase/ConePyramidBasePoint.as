package com.anychart.percentBase{
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Базовый класс рисования 2D пирамиды и конуса
	 * ибо они одинаковые.
	 */
	public class ConePyramidBasePoint extends PercentBasePoint{
		
		public function ConePyramidBasePoint(){
			this.drawingInfo = new PercentBaseDrawingInfo();
			super();
		}
		
		/**
		 * метод для рисования точки с заливкой, границами и т.д.
		 */
		override protected function drawSector():void{
			this.setBounds();
			this.getFill();
			this.drawShape();
			this.getEndFill();
			if (BackgroundBaseStyleState(this.styleState).hatchFill != null) {
				BackgroundBaseStyleState(this.styleState).hatchFill.beginFill(this.container.graphics, this.hatchType, this.color);
				this.drawShape();
				this.container.graphics.endFill();
			 }
			 this.drawConnectors();
		}
		
		/**
		 * метод для рисования линий точки
		 */
		override protected function drawShape():void{
			var g:Graphics = this.container.graphics;
			g.moveTo(drawingInfo.leftTop.x,drawingInfo.leftTop.y);
			g.lineTo(drawingInfo.leftBottom.x,drawingInfo.leftBottom.y);
			g.lineTo(drawingInfo.rightBottom.x,drawingInfo.rightBottom.y);
			g.lineTo(drawingInfo.rightTop.x,drawingInfo.rightTop.y);
			g.lineTo(drawingInfo.leftTop.x,drawingInfo.leftTop.y);
		}
		
		/**
		 * выставляем прямоугольник в котором рисутся точка
		 */
		override protected function setBounds():void{
			var h:Number = PercentBaseSeries(this.series).getHeight();
			this.bounds.x = Math.min(this.drawingInfo.leftTop.x, this.drawingInfo.leftBottom.x);
			this.bounds.y = Math.min(this.drawingInfo.leftBottom.y, this.drawingInfo.leftBottom.y);
			this.bounds.width = Math.max(this.drawingInfo.rightTop.x - this.drawingInfo.leftTop.x,this.drawingInfo.rightBottom.x - this.drawingInfo.leftBottom.x);
			this.bounds.height = this.percentHeight*h;
		}
		
		/**
		 * выстваление X координат сектора.
		 */
		override public function setSectorXValue():void{
			this.bottomWidth = this.bottomHeight;
			this.topWidth = this.topHeight;	
		} 
		
		/**
		 * Поучаем позиции лейблов
		 */
		override public function getLabelPosition(state: BaseElementStyleState, sprite: Sprite, bounds: Rectangle):Point{
				var placement:uint = PercentBaseGlobalSeriesSettings(this.global).labelsPlacementMode;
				var pos:Point = new Point;

				if (placement == LabelsPlacementMode.INSIDE){ return this.getElementPosition(state, sprite, bounds);}
				
				if (placement == LabelsPlacementMode.OUTSIDE_LEFT) {
					pos.x = (PercentBaseDrawingInfo(this.drawingInfo).leftTop.x + PercentBaseDrawingInfo(this.drawingInfo).leftBottom.x)/2;
					pos.y = (PercentBaseDrawingInfo(this.drawingInfo).leftTop.y + PercentBaseDrawingInfo(this.drawingInfo).leftBottom.y)/2;
					this.applyHAlign(pos, HorizontalAlign.LEFT, bounds, state.padding);
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}else if (placement == LabelsPlacementMode.OUTSIDE_RIGHT) {
					pos.x = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.x + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.x)/2;
					pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}else if (placement == LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN && this.label != null){
					pos.x = PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht() - this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width;
					pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}else if (placement == LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN && this.label != null){
					pos.x = PercentBaseSeries(this.series).seriesBound.x;
					pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}	
					return pos;
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void{
			this.drawingInfo.setAnchorPoint(anchor, pos, PercentBaseGlobalSeriesSettings(this.global).inverted);
		}  
	}
}