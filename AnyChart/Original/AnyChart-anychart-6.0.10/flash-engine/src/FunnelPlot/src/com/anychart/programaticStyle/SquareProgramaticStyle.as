package com.anychart.programaticStyle{
	
	import com.anychart.funnelPlot.Funnel3DPoint;
	import com.anychart.funnelPlot.FunnelDrawingInfo;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.Graphics;

	public class SquareProgramaticStyle extends ProgrammaticStyle{
		
		protected var frontGrd:Gradient;
		protected var backGrd:Gradient;
		protected var leftGrd:Gradient;
		protected var rightGrd:Gradient;
		protected var topGrd:Gradient;
		protected var bottomGrd:Gradient;
		
		protected var entries:Array;
		protected var lightColor:uint;
		protected var darkColor:uint;
		protected var blendIndex1:uint;
		protected var blendIndex2:uint;
		
		
		protected function execCreateGradient(grd:Gradient,state:BackgroundBaseStyleState,opacity:Number,color1:String,color2:String):void {
			if (!color1 && !color2) return;
			if (color1 && color2)
				grd.deserialize(<gradient>
						<key color={color1.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
						<key color={color2.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
					</gradient>);
		}
		
		
		override public function initialize(style:StyleState):void {
			this.initDynamicIndexes(style,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(style);
			var opacity:Number= state.fill ? state.fill.opacity : 1;
			var isDynamic:Boolean = state.fill.isDynamic;
			this.entries = [];
			var color:uint;
			opacity*=0.85;
			
			
			{//front
				blendIndex1 = ColorParser.getInstance().parse("Blend(%Color, LightColor(%Color), 0.7)");
				blendIndex2 = ColorParser.getInstance().parse("Blend(%Color, DarkColor(%Color), 0)");
				this.frontGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.frontGrd,this.entries);	
				}else {
					this.execCreateGradient(this.frontGrd,state,opacity,"Blend(%Color, LightColor(%Color), 0.7)","Blend(%Color, DarkColor(%Color), 0)");
				}
				
				this.entries = [];
			}
			{//right
				blendIndex1 = ColorParser.getInstance().parse("Blend(%Color,  #0x333333, 0.7)");
				blendIndex2 = ColorParser.getInstance().parse("Blend(DarkColor(%Color), 0, 0.7)");
				this.rightGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.rightGrd,this.entries);	
				}else {
					this.execCreateGradient(this.rightGrd,state,opacity,"Blend(%Color,  #0x333333, 0.7)","Blend(DarkColor(%Color), 0, 0.7)");
				}
				this.entries = [];
			}
			{//top
				blendIndex1 = ColorParser.getInstance().parse("Blend( %Color, DarkColor(%Color), 0.7)");
				blendIndex2 = ColorParser.getInstance().parse("Blend( %Color, DarkColor(%Color), 0.6)");
				this.topGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.topGrd,this.entries);	
				}else{
					this.execCreateGradient(this.topGrd,state,opacity,"Blend( %Color, DarkColor(%Color), 0.7)","Blend( %Color, DarkColor(%Color), 0.6)");
				}
				
				this.entries = [];
			}
			{//bottom
				blendIndex1 = ColorParser.getInstance().parse("Blend( %Color, DarkColor(%Color), 0.6)");
				blendIndex2 = ColorParser.getInstance().parse("Blend( %Color, DarkColor(%Color), 0.6)");
				this.bottomGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.bottomGrd,this.entries);
				}else{
					this.execCreateGradient(this.topGrd,state,opacity,"Blend( %Color, DarkColor(%Color), 0.6)","Blend( %Color, DarkColor(%Color), 0.6)");
				}
				this.entries = [];
			}
			{//left
				blendIndex1 = ColorParser.getInstance().parse("Blend(%Color, #0x333333, 0.7)");
				blendIndex2 = ColorParser.getInstance().parse("Blend(DarkColor(%Color), 0, 0.7)");
				this.leftGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.leftGrd,this.entries);	
				}else {
					this.execCreateGradient(this.topGrd,state,opacity,"Blend(%Color, #0x333333, 0.7)","Blend(DarkColor(%Color), 0, 0.7)");
				}
				
				this.entries = [];
			}
			{//back
				blendIndex1 = ColorParser.getInstance().parse("Blend(%Color, DarkColor(%Color), 0)");
				blendIndex2 = ColorParser.getInstance().parse("Blend(%Color, DarkColor(%Color), 0)");
				this.backGrd = new Gradient(1);
				this.entries.push(this.createKey(0,blendIndex1,opacity,true));
				this.entries.push(this.createKey(1,blendIndex2,opacity,true));
				if (isDynamic){
					this.createDynamicGradient(state,this.backGrd,this.entries);	
				}else {
					this.execCreateGradient(this.topGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
				
				this.entries = [];
			}
			
		}
		
		
		override public function draw(data:Object):void {
			var point:Funnel3DPoint = Funnel3DPoint(data);
			var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var color:uint = point.color;
			
			this.drawBottomSide(point);
			this.drawLeftSide(point);
			this.drawBackSide(point);
			this.drawFrontSide(point);
			this.drawRightSide(point); 
			this.drawTopSide(point);
			this.drawLine(point);   
		}
		
		protected function drawTopSide(point:Funnel3DPoint):void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			this.topGrd.beginGradientFill(g, drawInfo.topBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.topBounds, point.color);

			drawInfo.drawTopSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawTopSide(g,state);
				g.endFill();
			}
		}
		
		protected function drawBottomSide(point:Funnel3DPoint):void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			this.bottomGrd.beginGradientFill(g, drawInfo.bottomBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.bottomBounds, point.color);

			drawInfo.drawBottomSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawBottomSide(g,state);
				g.endFill();
			}
			/* g.beginFill(0x333333,1);
			g.drawRect(drawInfo.bottomBounds.x,drawInfo.bottomBounds.y,drawInfo.bottomBounds.width,drawInfo.bottomBounds.height); */
		}
		protected function drawLeftSide(point:Funnel3DPoint):void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			this.leftGrd.beginGradientFill(g, drawInfo.leftBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.leftBounds, point.color);

			drawInfo.drawLeftSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawLeftSide(g,state);
				g.endFill();
			}
		}
		
		protected function drawRightSide(point:Funnel3DPoint):void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			this.rightGrd.beginGradientFill(g, drawInfo.rightBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.rightBounds, point.color);

			drawInfo.drawRightSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawRightSide(g,state);
				g.endFill();
			}
			
		}
		
		protected function drawFrontSide(point:Funnel3DPoint):void{
			var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			
			this.frontGrd.beginGradientFill(g, drawInfo.frontBounds, point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.frontBounds, point.color);

			drawInfo.drawFrontSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawFrontSide(g,state);
				g.endFill();
			}
		}
		
		protected function drawBackSide(point:Funnel3DPoint):void{
			var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			
			this.backGrd.beginGradientFill(g, drawInfo.backBounds, point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.backBounds, point.color);

			drawInfo.drawBackSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawBackSide(g,state);
				g.endFill();
			}
		
		}
		protected function drawLine(point:Funnel3DPoint):void{
		 	var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
				
			if (BackgroundBaseStyleState(point.styleState).stroke != null)
				BackgroundBaseStyleState(point.styleState).stroke.apply(g, point.bounds, point.color);
			
			g.moveTo(drawInfo.leftCenter.x,drawInfo.leftCenter.y);
			g.lineTo(drawInfo.centerCenterFront.x,drawInfo.centerCenterFront.y);
			g.lineTo(drawInfo.rightCenter.x,drawInfo.rightCenter.y);
			
			g.lineStyle(); 
		} 
		
	}
}