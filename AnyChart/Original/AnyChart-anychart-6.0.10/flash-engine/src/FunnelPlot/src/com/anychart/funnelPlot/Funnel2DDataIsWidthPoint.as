package com.anychart.funnelPlot{
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.percentBase.PercentBaseGlobalSeriesSettings;
	import com.anychart.percentBase.PercentBasePoint;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Funnel2DDataIsWidthPoint extends FunnelPoint{
		
		public var baseWidth:Number;		
		
		public function Funnel2DDataIsWidthPoint(){
			this.is3D = false;
			super();
			this.drawingInfo = new FunnelDIWDrawingInfo();
		}
		
		override public function setSectorYValue(dataIsWidth:Boolean, minValue:Number, padding:Number, realSummHeight:Number, size3D:Number):void{
			this.baseWidth = this.series.cashedTokens['YMin']/this.series.cashedTokens['YMax'];
			var size3D:Number;
			var firstPoint:PercentBasePoint = this.series.points[0];
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			var previousPoint:PercentBasePoint = this.series.points[this.index-1];
			
			if (inverted){
				size3D = firstPoint.calculationValue/this.series.cashedTokens['YMax']*PercentBaseGlobalSeriesSettings(this.global).size3D;
			}else size3D=0;
			if (!this.is3D) size3D=0;
			
			this.percentHeight = 1/(PercentBaseSeries(this.series).points.length-1);
			this.percentHeight *= this.getRealPercentHeight(padding,size3D);
			this.bottomHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY-=this.percentHeight;
			this.topHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY -= padding; 
			this.topHeight-=size3D*2;
			this.bottomHeight-=size3D*2;
			if (this.index == (PercentBaseSeries(this.series).points.length-1)){
				this.topHeight = this.bottomHeight = previousPoint.topHeight;
				this.percentHeight = 0;
			}
		}
		
		override protected function getRealPercentHeight(padding:Number, size3D:Number):Number{
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			var size3D:Number;
			var firstPoint:PercentBasePoint = this.series.points[0];
			var lastPoint:PercentBasePoint = this.series.points[this.series.points.length-1];
			if (inverted){
				size3D = firstPoint.calculationValue/this.series.cashedTokens['YMax']*PercentBaseGlobalSeriesSettings(this.global).size3D;
			}else size3D =lastPoint.calculationValue/this.series.cashedTokens['YMax']*PercentBaseGlobalSeriesSettings(this.global).size3D;
			if (!this.is3D) size3D=0;
			return (1-padding*(PercentBaseSeries(this.series).points.length-2)-size3D*2);
		}
		
		override public function setSectorXValue():void{
			var index:Number = this.index;
			var nextPoint:FunnelPoint = FunnelSeries(this.series).points[index+1];
			this.bottomWidth = this.calculationValue/this.series.cashedTokens['YMax'];
			if (nextPoint != null){
				this.topWidth = nextPoint.calculationValue/this.series.cashedTokens['YMax'];
			}else this.topWidth = this.bottomWidth;
		}
		
		override protected function drawSector():void{
			this.setBounds();
			this.getFill();
			this.drawShape();
			this.getEndFill();
			if (BackgroundBaseStyleState(this.styleState).hatchFill != null) {
				BackgroundBaseStyleState(this.styleState).hatchFill.beginFill(this.container.graphics, this.hatchType, this.color);
				this.drawShape();
				this.container.graphics.endFill();
			 }
			 this.drawConnectors();
		}
		
		override protected function drawShape():void{
			var g:Graphics = this.container.graphics;
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var nextPoint:Funnel2DDataIsWidthPoint = PercentBaseSeries(this.series).points[this.index+1];
			
			g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
			g.lineTo(drawInfo.rightBottom.x, drawInfo.rightBottom.y);
			if (nextPoint != null){
				g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
				g.lineTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
			}
		}
		override protected function setBounds():void {
			var h:Number = PercentBaseSeries(this.series).getHeight();
			
			this.bounds.x = Math.min(this.drawingInfo.leftTop.x,this.drawingInfo.leftBottom.x);
			this.bounds.width = Math.max(this.drawingInfo.rightTop.x-this.drawingInfo.leftTop.x,this.drawingInfo.rightBottom.x- this.drawingInfo.leftBottom.x);
			this.bounds.y = Math.min(this.drawingInfo.leftBottom.y,this.drawingInfo.leftTop.y);
			this.bounds.height = h* this.percentHeight;
		}
		
		override public function getLabelPosition(state: BaseElementStyleState, sprite: Sprite, bounds: Rectangle):Point{
				var placement:uint = FunnelGlobalSeriesSettings(this.global).labelsPlacementMode;
				var pos:Point = new Point();
				var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
					
				if (placement == LabelsPlacementMode.INSIDE){return this.getElementPosition(state, sprite, bounds);}
				
				if (placement == LabelsPlacementMode.OUTSIDE_LEFT) {
					pos.x = (FunnelDIWDrawingInfo(this.drawingInfo).leftTop.x + FunnelDIWDrawingInfo(this.drawingInfo).leftBottom.x)/2;
					pos.y = (FunnelDIWDrawingInfo(this.drawingInfo).leftTop.y + FunnelDIWDrawingInfo(this.drawingInfo).leftBottom.y)/2;
					this.applyHAlign(pos, HorizontalAlign.LEFT, bounds, state.padding);
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
					if (pos.x<FunnelSeries(this.series).seriesBound.x){
						pos.x = FunnelSeries(this.series).seriesBound.x;
					}
				}
				if (placement == LabelsPlacementMode.OUTSIDE_RIGHT) {
					pos.x = (FunnelDIWDrawingInfo(this.drawingInfo).rightTop.x + FunnelDIWDrawingInfo(this.drawingInfo).rightBottom.x)/2;
					pos.y = (FunnelDIWDrawingInfo(this.drawingInfo).rightTop.y + FunnelDIWDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
					if (pos.x+this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width>PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht()){
						pos.x = PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht() - this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width;
					}
				}
				if (placement == LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN && this.label != null){
					pos.x = PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht() - this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width;
					pos.y = (FunnelDIWDrawingInfo(this.drawingInfo).rightTop.y + FunnelDIWDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}
				if (placement == LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN && this.label != null){
					pos.x = PercentBaseSeries(this.series).seriesBound.x;
					pos.y = (FunnelDIWDrawingInfo(this.drawingInfo).rightTop.y + FunnelDIWDrawingInfo(this.drawingInfo).rightBottom.y)/2;
					this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
				}
				if (this.index == PercentBaseSeries(this.series).points.length-1)
					pos.y= inverted ? (pos.y-bounds.height/2) : (pos.y + bounds.height/2);	
					return pos;
		} 
	}
}