package com.anychart.funnelPlot{
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.Style;
	
	
	internal final class FunnelSeriesSettings extends BaseSeriesSettings{
		public var mode:uint;
		
		override public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void{
			super.deserializeGlobal(data, stylesList, resources);
			if (data.@mode != undefined){
				switch (SerializerBase.getEnumItem(data.@mode)){
					case "square": this.mode = FunnelMode.SQUARE;break;
					case "circular": this.mode = FunnelMode.CIRCULAR;break;
				}	
			}
			this.chekModeStyle(); 
		}
		
		private function chekModeStyle():void {
			if (this.mode == FunnelMode.SQUARE){this.setProgramaticStyle("SquareProgramaticStyle");}
			if (this.mode == FunnelMode.CIRCULAR){this.setProgramaticStyle("CircularProgramaticStyle");}
		}
		
		private function setProgramaticStyle(styleName:String):void {
			this.style = this.style.createCopy();
			var s:Style = this.style;
			
			s.normal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.normal); 
			s.hover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.hover);
			s.pushed.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.pushed);
			s.missing.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.missing);
			s.selectedNormal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedNormal);
			s.selectedHover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedHover);
		}
	}
}