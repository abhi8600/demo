package {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.LoaderInfo;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class AnyChartTrial extends AnyChartRunnerBase {
		
		private var trialBitmap:Bitmap;
		private var mainContainer:DisplayObjectContainer;
		
		private var trialWidth:Number;
		private var trialHeight:Number;
		
		public function AnyChartTrial() {
			var fmt:TextFormat = new TextFormat();
			fmt.font = 'Verdana';
			fmt.size = 60;
			
			var trialTextField:TextField = new TextField();
			trialTextField.text = "AnyChart Trial Version";
			trialTextField.autoSize = TextFieldAutoSize.LEFT;
			trialTextField.setTextFormat(fmt);
			
			var d:BitmapData = new BitmapData(trialTextField.width, trialTextField.height, true, 0);
			d.draw(trialTextField);
			
			this.trialBitmap = new Bitmap(d);
			this.trialBitmap.alpha = 0.15;
			this.xmlFile = null;
			
			this.trialWidth = trialTextField.width;
			this.trialHeight = trialTextField.height;
			
			super();
		}
		
		override public function createEngine(mainContainer:DisplayObjectContainer, paramsContainer:LoaderInfo):void {
			super.createEngine(mainContainer, paramsContainer);
			this.mainContainer = mainContainer;
			this.setBitmapSize();
			mainContainer.addChild(this.trialBitmap);
		}
		
		override public function resizeHandler(event:Event):void {
			this.setBitmapSize();
			super.resizeHandler(event);
			this.setBitmapSize();
		}
		
		private function setBitmapSize():void {
			if (mainContainer == null) return;
			var w:Number = stage.stageWidth;
			var h:Number = stage.stageHeight;
			
			this.trialBitmap.scaleX = 1; 
			this.trialBitmap.scaleY = 1;
			
			var scale:Number
			if (w/h < this.trialWidth/this.trialHeight)
				scale = w / this.trialWidth;
			else
				scale = h / this.trialHeight;
			
			this.trialBitmap.scaleX = scale; 
			this.trialBitmap.scaleY = scale;
			
			this.trialBitmap.x = (w - this.trialBitmap.width)/2;
			this.trialBitmap.y = (h - this.trialBitmap.height)/2;
		}
	}
}