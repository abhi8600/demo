package com.anychart.mapPlot.markerSeries {
	import com.anychart.mapPlot.data.GeoBasedPoint;
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	internal final class MarkerPoint extends GeoBasedPoint {
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, elementIndex:int, resources:ResourcesLoader):MarkerElement {
			if (data.@style != undefined)
				if (data.marker[0] == null)
					data.marker[0] = <marker style={data.@style} />;
				else 
					data.marker[0].@style = data.@style;
				
			if (data.marker[0] != null) {
				if (marker.needCreateElementForPoint(data.marker[0]))
					marker = MarkerElement(marker.createCopy());
				marker.deserializePoint(data.marker[0], this, styles, elementIndex,resources);
			}
			marker.enabled = true;
			this.checkPointElementPosition(marker, elementIndex);
			return marker;
		}
	}
}