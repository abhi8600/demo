package com.anychart.axesPlot.series.heatMap {
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	public class HeatMapGlobalSeriesSettings extends GlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function deserialize(data:XML):void {
			if (data == null) return;
			super.deserialize(data);
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:HeatMapSeries = new HeatMapSeries();
			series.type = SeriesType.HEAT_MAP;
			series.clusterKey = SeriesType.HEAT_MAP;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "heat_map";
		}
		
		override public function getStyleNodeName():String {
			return "heat_map_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
		
	}
}