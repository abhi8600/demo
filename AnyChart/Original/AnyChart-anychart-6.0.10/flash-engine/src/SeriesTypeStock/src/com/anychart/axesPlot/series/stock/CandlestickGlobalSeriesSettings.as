package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.series.stock.styles.CandlestickStyleState;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;

	public class CandlestickGlobalSeriesSettings extends StockGlobalSeriesSettings {
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:CandlestickSeries = new CandlestickSeries();
			series.type = seriesType;
			series.clusterKey = seriesType;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new StockSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "candlestick_series";
		}
		
		override public function getStyleNodeName():String {
			return "candlestick_style";
		}
		
		override public function getStyleStateClass():Class {
			return CandlestickStyleState;
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { return true; }
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			
			g.beginFill(0,0);
			g.lineStyle(1, item.color, 1);
			g.moveTo(bounds.x + bounds.width/2, bounds.top);
			g.lineTo(bounds.x + bounds.width/2, bounds.bottom);
			g.lineStyle();
			g.endFill();
		
			g.beginFill(item.color, 1);
			g.lineStyle(1, item.color, 1);
			g.drawRect(bounds.x+bounds.width/4, bounds.y+bounds.height/4, bounds.width/2, bounds.height/2);
			g.endFill();
			g.lineStyle();
		}
	}
}