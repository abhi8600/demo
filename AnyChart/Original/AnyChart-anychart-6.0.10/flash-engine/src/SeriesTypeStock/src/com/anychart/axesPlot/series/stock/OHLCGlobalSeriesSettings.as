package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.series.stock.styles.OHLCStyleState;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	
	public class OHLCGlobalSeriesSettings extends StockGlobalSeriesSettings {
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:OHLCSeries = new OHLCSeries();
			series.type = seriesType;
			series.clusterKey = seriesType;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new StockSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "ohlc_series";
		}
		
		override public function getStyleNodeName():String {
			return "ohlc_style";
		}
		
		override public function getStyleStateClass():Class {
			return OHLCStyleState;
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean {
			return true;
		}
		
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			g.lineStyle(2, item.color, 1);
			g.moveTo(bounds.x + bounds.width/2, bounds.top);
			g.lineTo(bounds.x + bounds.width/2, bounds.bottom);
			g.moveTo(bounds.x, bounds.y + bounds.height/4);
			g.lineTo(bounds.x + bounds.width/2, bounds.y + bounds.height/4);
			g.moveTo(bounds.x + bounds.width/2, bounds.bottom - bounds.height/4);
			g.lineTo(bounds.right, bounds.bottom - bounds.height/4);
			g.lineStyle();
		}
	}
}