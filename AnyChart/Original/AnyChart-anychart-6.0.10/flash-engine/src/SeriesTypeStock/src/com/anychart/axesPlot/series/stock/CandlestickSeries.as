package com.anychart.axesPlot.series.stock {
	import com.anychart.seriesPlot.data.BasePoint;
	
	
	internal final class CandlestickSeries extends StockSeries {
		
		override public function createPoint():BasePoint {
			return new CandlestickPoint();
		}
	}
}