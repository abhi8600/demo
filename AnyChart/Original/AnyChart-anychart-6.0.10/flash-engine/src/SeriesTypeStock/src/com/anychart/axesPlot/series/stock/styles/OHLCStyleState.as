package com.anychart.axesPlot.series.stock.styles {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.XMLUtils;
	
	public class OHLCStyleState extends StyleState {
		
		public var up:OHLCStyleStateEntry;
		public var down:OHLCStyleStateEntry;
		
		public function OHLCStyleState(style:Style) {
			super(style);
			this.up = new OHLCStyleStateEntry(style);
			this.down = new OHLCStyleStateEntry(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			
			var upXML:XML = XMLUtils.merge(data, data.up[0], "up");
			var downXML:XML = XMLUtils.merge(data, data.down[0], "down");
			this.up.deserialize(upXML, resources);
			this.down.deserialize(downXML, resources);
			
			return data;
		}
		
	}
}