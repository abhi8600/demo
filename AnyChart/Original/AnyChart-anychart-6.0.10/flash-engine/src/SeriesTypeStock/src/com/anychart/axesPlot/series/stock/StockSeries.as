package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.data.AxesPlotSeries;
	
	
	internal class StockSeries extends AxesPlotSeries {
		public function StockSeries() {
			super();
			this.isClusterizableOnCategorizedAxis = true;
			this.clusterContainsOnlyOnePoint = true;
			this.isSortable = false;
			this.needCallingOnBeforeDraw = false;
		}
	}
}