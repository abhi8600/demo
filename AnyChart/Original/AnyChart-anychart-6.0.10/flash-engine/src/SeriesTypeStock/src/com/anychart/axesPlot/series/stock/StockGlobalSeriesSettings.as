package com.anychart.axesPlot.series.stock {
	import com.anychart.interpolation.IMissingInterpolator;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	
	internal class StockGlobalSeriesSettings extends GlobalSeriesSettings {
		override public function createSettings():BaseSeriesSettings {
			return new StockSeriesSettings();
		}
		
		private var highInterpolator:IMissingInterpolator;
		private var lowInterpolator:IMissingInterpolator;
		private var openInterpolator:IMissingInterpolator;
		private var closeInterpolator:IMissingInterpolator;
		
		override public function missingInitializeInterpolators():void {
			this.highInterpolator = this.createMissingInterpolator();
			this.lowInterpolator = this.createMissingInterpolator();
			this.openInterpolator = this.createMissingInterpolator();
			this.closeInterpolator = this.createMissingInterpolator();
			
			this.highInterpolator.initialize("high");
			this.lowInterpolator.initialize("low");
			this.openInterpolator.initialize("open");
			this.closeInterpolator.initialize("close");
		}
		
		override public function missingCheckPointDuringDeserialize(point:BasePoint):void {
			this.highInterpolator.checkDuringDeserialize(point, point.index);
			this.lowInterpolator.checkDuringDeserialize(point, point.index);
			this.openInterpolator.checkDuringDeserialize(point, point.index);
			this.closeInterpolator.checkDuringDeserialize(point, point.index);
		}
		
		override public function missingInterpolate(points:Array):void {
			this.highInterpolator.interpolate(points);
			this.lowInterpolator.interpolate(points);
			this.openInterpolator.interpolate(points);
			this.closeInterpolator.interpolate(points);
		}
	}
}