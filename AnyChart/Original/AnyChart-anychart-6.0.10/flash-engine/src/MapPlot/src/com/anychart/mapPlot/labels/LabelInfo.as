package com.anychart.mapPlot.labels
{
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	internal final class LabelInfo
	{
		internal var labelContainer:DisplayObject;
		internal var labelBounds:Rectangle;
		internal var regionPixBounds:Rectangle;
		
		public function LabelInfo(labelContainer:DisplayObject, labelBounds:Rectangle, regionPixBounds:Rectangle) {
			this.labelContainer = labelContainer;
			this.labelBounds = labelBounds;
			this.regionPixBounds = regionPixBounds
		}
	}
}