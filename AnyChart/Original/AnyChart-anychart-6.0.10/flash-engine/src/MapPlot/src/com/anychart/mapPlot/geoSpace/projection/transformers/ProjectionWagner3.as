package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionWagner3 extends TopAndBottomFixedProjection {
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
			var num4:Number = (x * Math.PI) / 180;
    		var num5:Number = (num2 * Math.PI) / 180;
    		pt.x = ((Math.cos(0) / Math.cos(0)) * num4) * Math.cos((2 * num5) / 3);
    		pt.x *= 57.295779513082323;
	     	pt.y = num2;
		}
	}
}