package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionOrthographic extends ProjectionTransformer {
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
    		
			var num27:Number = (this.a(x, -90, 90) * Math.PI) / 180;
	        var num28:Number = (num2 * Math.PI) / 180;
	        var num29:Number = (centerY * Math.PI) / 180;
	        x = Math.cos(num28) * Math.sin(num27);
	        num2 = (Math.cos(num29) * Math.sin(num28)) - ((Math.sin(num29) * Math.cos(num28)) * Math.cos(num27));
	        x *= 57.295779513082323;
	        num2 *= 57.295779513082323;
	        
	        pt.x = x;
	        pt.y = num2;
		}
	}
}