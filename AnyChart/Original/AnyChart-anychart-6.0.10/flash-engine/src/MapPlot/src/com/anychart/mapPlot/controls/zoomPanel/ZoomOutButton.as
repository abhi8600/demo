package com.anychart.mapPlot.controls.zoomPanel {
	import flash.display.Graphics;
	import flash.geom.Rectangle;

	internal final class ZoomOutButton extends ZoomButton {
		
		override protected function drawPointer():void {
			var g:Graphics = this.graphics;
			var rc:Rectangle = this.pointerBounds;
			
			var opacity:Number=1;
			
			g.beginFill(0,0);
			g.lineStyle(2,0,opacity);
			g.moveTo(rc.left,rc.top+rc.height/2);
			g.lineTo(rc.left+rc.width,rc.top+rc.height/2);
			g.lineStyle(0,0,0);
			g.endFill();
		}
	}
}