package com.anychart.mapPlot {
	import com.anychart.mapPlot.grid.MapGrid;
	import com.anychart.resources.MapResourceEntry;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.effects.EffectsList;
	import com.anychart.visual.stroke.Stroke;
	
	public class MapSettings implements ISerializableRes {
		
		public var regionsBorder:Stroke;
		public var source:String;
		public var keyColumn:String;
		public var plot:MapPlot;
		public var effects:EffectsList;
		public var grid:MapGrid; 
		public var smoothing:Boolean;
		
		public function MapSettings(plot:MapPlot) {
			this.plot = plot;
			this.keyColumn = "REGION_ID";
			this.smoothing = true;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.map_series[0] != null) {
				var settings:XML = data.map_series[0];
				if (settings.@source != undefined) {
					this.source = SerializerBase.getString(settings.@source);
					this.source = resources.getMapPath(this.source);
					var mapEntry:MapResourceEntry = new MapResourceEntry();
					mapEntry.initialize(this.source, true);
					resources.addCustomEntry(mapEntry);
				}
				if (settings.@smoothing != undefined) this.smoothing = SerializerBase.getBoolean(settings.@smoothing);
				if (settings.@id_column != undefined) this.keyColumn = SerializerBase.getString(settings.@id_column);
				
				if (SerializerBase.isEnabled(settings.border[0])) {
					this.regionsBorder = new Stroke();
					this.regionsBorder.deserialize(settings.border[0]);
				}
				
				if (settings.projection[0] != null)
					this.plot.space.projection.deserialize(settings.projection[0]);
					
				if (SerializerBase.isEnabled(settings.effects[0])) {
					this.effects = new EffectsList();
					this.effects.deserialize(settings.effects[0]);
				}
				
				if (SerializerBase.isEnabled(settings.grid[0])) {
					this.grid = new MapGrid(this.plot.space);
					this.grid.deserialize(settings.grid[0], resources);
				}
				
				if (settings.zoom[0] != null) {
					plot.zoomManager.deserialize(settings.zoom[0]);
				}
			}
		}
		
		//-------------------------------------------------------
		//	Data Columns
		//-------------------------------------------------------
		
		public var columns:Array;
		public var columnsCount:uint;
	}
}