package com.anychart.mapPlot.grid.meridians {
	import com.anychart.mapPlot.coordinates.Coordinates;
	import com.anychart.mapPlot.grid.Axis;
	import com.anychart.mapPlot.grid.AxisLabelsPosition;
	import com.anychart.mapPlot.grid.MapGrid;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class MeridiansAxis extends Axis {
		
		override public function initialize(grid:MapGrid):void {
			super.initialize(grid);
			this.scale = this.space.geoXScale;
		}
		
		override public function drawLine(g:Graphics, value:Number, moveTo:Boolean=false, back:Boolean=false):void {
			var start:Point = new Point(value, this.space.geoBounds.top);
			var end:Point = new Point(value, this.space.geoBounds.bottom);
			if (back) this.space.drawLine(g, end,start, moveTo);
			else this.space.drawLine(g, start,end, moveTo);
		}
		
		override protected function getLabelSpace(labelSize:Rectangle):Number {
			return labelSize.height;
		}
		
		override protected function getPerpendicularSpace(labelSize:Rectangle):Number {
			return labelSize.width;
		}
		
		override protected function getStringGeoCoords(value:Number):String {
			return Coordinates.getStrLongitude(value);
		}
		
		override protected function setLabelPosition(value:Number, info:TextElementInformation, pos:Point):void {
			var y:Number = 0;
			var dy:Number = 0;
			switch (this.labels.position) {
				case AxisLabelsPosition.NEAR:
					y = this.space.geoBounds.top;
					dy = -info.rotatedBounds.height-this.labels.padding;
					break;
				case AxisLabelsPosition.CENTER:
					y = (this.space.geoBounds.top + this.space.geoBounds.bottom)/2;
					dy = -info.rotatedBounds.height/2; 
					break;
				case AxisLabelsPosition.FAR:
					y = this.space.geoBounds.bottom;
					dy = this.labels.padding;
					break;
				case AxisLabelsPosition.ONE_QUARTER:
					y = this.space.geoBounds.y + this.space.geoBounds.height/3;
					dy = -info.rotatedBounds.height/2-this.labels.padding;
					break;
				case AxisLabelsPosition.THREE_QUARTERS:
					y = this.space.geoBounds.y + this.space.geoBounds.height*2/3;
					dy = -info.rotatedBounds.height/2-this.labels.padding;
					break;
			}
			this.space.transform(value,y,pos);
			pos.x -= info.rotatedBounds.width/2;
			pos.y += dy;
		}
	}
}