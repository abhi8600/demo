package com.anychart.mapPlot.controls {
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class MapControlButton extends Sprite {
		
		public var bounds:Rectangle;
		
		protected var pointerBounds:Rectangle;
		
		private var state:BackgroundBaseStyleState;
		
		public function MapControlButton() {
			this.mouseChildren = false;
			this.buttonMode = true;
			this.useHandCursor = true;
			
			this.bounds = new Rectangle();
			this.pointerBounds = new Rectangle();
			this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			this.addEventListener(MouseEvent.CLICK, this.mouseClickHandler);
		}
		
		private function mouseOverHandler(event:MouseEvent):void {
			this.state = BackgroundBaseStyleState(style.hover);
			this.execDrawing();
		}
		
		private function mouseOutHandler(event:MouseEvent):void {
			this.state = BackgroundBaseStyleState(style.normal);
			this.execDrawing();
		}
		
		private function mouseDownHandler(event:MouseEvent):void {
			this.state = BackgroundBaseStyleState(style.pushed);
			this.execDrawing();
		}
		
		private function mouseClickHandler(event:MouseEvent):void {
			this.state = BackgroundBaseStyleState(style.normal);
			this.execDrawing();
		}
		
		public function initialize(w:Number, h:Number):void {
			this.bounds.width = w;
			this.bounds.height = h;
			this.state = BackgroundBaseStyleState(style.normal);
			this.pointerBounds = new Rectangle(this.bounds.width/2-(this.bounds.width/3)/2,
							   this.bounds.height/2-(this.bounds.height/3)/2,
							   this.bounds.width/3,
							   this.bounds.height/3);
		}
		
		public function draw():void {
			this.execDrawing();
		}
		
		protected function drawPointer():void {}
		
		private function execDrawing():void {
			var g:Graphics = this.graphics;
			g.clear();
			if (this.state.fill != null)
				this.state.fill.begin(g, this.bounds);
			if (this.state.stroke != null)
				this.state.stroke.apply(g, this.bounds);
			g.drawRoundRect(0,0,this.bounds.width,this.bounds.height, 6, 6);
			g.endFill();
			g.lineStyle();
			
			this.drawPointer();
			
			this.filters = (this.state.effects != null) ? (this.state.effects.list) : null;
		}

		public static var style:Style = createButtonStyle();
		private static function createButtonStyle():Style {
			style = new Style(BackgroundBaseStyleState);
			style.deserialize(
				<style>
					<fill opacity="1" type="Gradient">
						<gradient angle="90">
							<key position="0" color="#FEFEFE" />
							<key position=".5" color="#F3F3F3" />
							<key position="1" color="#EBEBEB" />
						</gradient>
					</fill>
					<border opacity="1" type="gradient" pixel_hinting="true">
						<gradient angle="90">
							<key position="0" color="#B5B8BA" />
							<key position="0.5" color="#8C8F90" />
							<key position="0.8" color="#6A6C6D" />
							<key position="1" color="#858788" />
						</gradient>
					</border>
					<effects>
						<bevel enabled="true" distance="2" shadow_opacity=".2" highlight_opacity=".2" />
						<drop_shadow enabled="true" opacity=".2" distance="1" blur_x="2" blur_y="2" />
					</effects>
					
					<states>
						<hover>
							<fill opacity="1" type="Gradient">
								<gradient angle="90">
									<key position="0" color="#FDFBFB" />
									<key position=".5" color="#F7F0F0" />
									<key position="1" color="#F0E2E2" />
								</gradient>
							</fill>
							<border type="Solid" color="#FF4D4D" />
							<effects>
								<bevel enabled="true" distance="2" shadow_opacity=".2" highlight_opacity=".2" />
								<drop_shadow enabled="true" opacity=".2" distance="1" blur_x="2" blur_y="2" />
							</effects>
						</hover>
						
						<pushed>
							<fill opacity="1" type="Gradient">
								<gradient angle="90">
									<key position="0" color="#DFDDDD" />
									<key position=".5" color="#DAD3D3" />
									<key position="1" color="#D5C5C5" />
								</gradient>
							</fill>
							<border opacity="1" type="solid" color="#DB4242"/>
							<effects>
								<bevel enabled="true" distance="2" shadow_opacity=".2" highlight_opacity=".2" />
								<drop_shadow enabled="true" opacity=".2" distance="1" blur_x="2" blur_y="2" />
								<inner_shadow enabled="true" angle="-45" opacity=".2" distance="1" blur_x="2" blur_y="2" />
							</effects>
						</pushed>
						
						<selected_normal>
							<fill type="Gradient">
								<gradient angle="90">
									<key position="0" color="#FEFEFE" />
									<key position=".5" color="#F3F3F3" />
									<key position="1" color="#EBEBEB" />
								</gradient>
							</fill>
							
							<border type="gradient">
								<gradient angle="90">
									<key position="0" color="#B5B8BA" />
									<key position="0.5" color="#8C8F90" />
									<key position="0.8" color="#6A6C6D" />
									<key position="1" color="#858788" />
								</gradient>
							</border>
							
							<effects>
								<bevel enabled="true" distance="2" shadow_opacity=".2" highlight_opacity=".2" />
							</effects>
							
						</selected_normal>
						
					</states>
				</style>
			, null);
			return style;
		}
	}
}