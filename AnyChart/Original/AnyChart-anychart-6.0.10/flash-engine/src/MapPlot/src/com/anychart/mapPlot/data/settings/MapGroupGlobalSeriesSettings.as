package com.anychart.mapPlot.data.settings {
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.data.MapSeries;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	public class MapGroupGlobalSeriesSettings extends GlobalSeriesSettings {
		
		override public function initialize(plot:SeriesPlot):void {
			this.container = MapPlot(plot).mapContainer;
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			return new MapSeries();
		}
		
		override public function getSettingsNodeName():String {
			return "map_series";
		}
		
		override public function getStyleNodeName():String {
			return "map_region_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
	}
}