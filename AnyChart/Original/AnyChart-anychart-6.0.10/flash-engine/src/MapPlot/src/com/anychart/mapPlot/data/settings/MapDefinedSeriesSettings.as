package com.anychart.mapPlot.data.settings {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	internal final class MapDefinedSeriesSettings extends MapSeriesSettings {
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new MapDefinedSeriesSettings();
			super.createCopy(settings);
			return settings;
		}
		
		override public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeGlobal(data, stylesList, resources);
			if (data.defined_map_region[0] != null)
				super.deserializeGlobal(data.defined_map_region[0], stylesList, resources);
		}
	}
}