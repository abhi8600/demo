package com.anychart.mapPlot.grid.parallels {
	import com.anychart.mapPlot.coordinates.Coordinates;
	import com.anychart.mapPlot.grid.Axis;
	import com.anychart.mapPlot.grid.AxisLabelsPosition;
	import com.anychart.mapPlot.grid.MapGrid;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class ParallelsAxis extends Axis {
		
		override public function initialize(grid:MapGrid):void {
			super.initialize(grid);
			this.scale = this.space.geoYScale;
		}
		
		override public function drawLine(g:Graphics, value:Number, moveTo:Boolean = false, back:Boolean = false):void {
			var start:Point = new Point(this.space.geoBounds.left, value);
			var end:Point = new Point(this.space.geoBounds.right, value);
			if (back) this.space.drawLine(g, end,start, moveTo);
			else this.space.drawLine(g, start,end, moveTo);
		}
		
		override protected function getLabelSpace(labelSize:Rectangle):Number {
			return labelSize.width;
		}
		
		override protected function getPerpendicularSpace(labelSize:Rectangle):Number {
			return labelSize.height;
		}
		
		override protected function getStringGeoCoords(value:Number):String {
			return Coordinates.getStrLatitude(value);
		}
		
		override protected function setLabelPosition(value:Number, info:TextElementInformation, pos:Point):void {
			var x:Number = 0;
			var dx:Number = 0;
			switch (this.labels.position) {
				case AxisLabelsPosition.NEAR:
					x = this.space.geoBounds.left;
					dx = -info.rotatedBounds.width-this.labels.padding;
					break;
				case AxisLabelsPosition.CENTER:
					x = (this.space.geoBounds.left + this.space.geoBounds.right)/2;
					dx = -info.rotatedBounds.width/2; 
					break;
				case AxisLabelsPosition.FAR:
					x = this.space.geoBounds.right;
					dx = this.labels.padding;
					break;
				case AxisLabelsPosition.ONE_QUARTER:
					x = this.space.geoBounds.x + this.space.geoBounds.width/3;
					dx = -info.rotatedBounds.width/2+this.labels.padding;
					break;
				case AxisLabelsPosition.THREE_QUARTERS:
					x = this.space.geoBounds.x + this.space.geoBounds.width*2/3;
					dx = -info.rotatedBounds.width/2+this.labels.padding;
					break;
			}
			this.space.transform(x,value,pos);
			pos.y -= info.rotatedBounds.height/2;
			pos.x += dx;
		}
	}
}