package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionEckert1 extends ProjectionTransformer {
		
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
    		
			var num10:Number = (x * Math.PI) / 180;
	        var num11:Number = (num2 * Math.PI) / 180;
	        var num12:Number = 2 * Math.sqrt(0.21220659078919379);
	        x = (num12 * num10) * (1 - (Math.abs(num11) / Math.PI));
	        num2 = num12 * num11;
	        x *= 57.295779513082323;
	        num2 *= 57.295779513082323;
	        
	        pt.x = x;
	        pt.y = num2;
		}
	}
}