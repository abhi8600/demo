package com.anychart.mapPlot.reader {
	import com.anychart.mapPlot.MapCentroidBounder;
	import com.anychart.mapPlot.MapSettings;
	import com.anychart.mapPlot.data.MapRegion;
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.mapPlot.geoSpace.TransformedGeoSpace;
	import com.anychart.mapPlot.geoSpace.projection.ProjectionType;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	public class MapReader {
		
		private var keyColumnIndex:int;
		
		public function read(settings:MapSettings):void {
			var archive:FZip = FZip(ResourcesHashMap.getObject(settings.source)); 
			if (archive == null) return;
			//projection: settings.projection
			//get type from string: settings.projection.deserializeType(type):int
			
			//create region:
			//settings.plot.createRegion(regionName)
			
			var source:FZipFile = FZipFile(archive.getFileByName("map.bin"));
			var mapContent:ByteArray = source.content;
			mapContent.endian = Endian.LITTLE_ENDIAN;
			mapContent.position = 0;
			
			var versionName:String = mapContent.readUTFBytes(mapContent.readUnsignedInt());
			
			if (versionName != "1.0")
				return;
			
			var space:GeoSpace = settings.plot.space;
			
			space.isWorldMap = mapContent.readByte() == 1;
			
			var defaultProjection:String = mapContent.readUTFBytes(mapContent.readUnsignedInt());
			
			if (space.projection.type == -1) {
				space.projection.type = space.projection.deserializeType(defaultProjection);
				if (space.projection.type == -1)
					space.projection.type = ProjectionType.EQUIRECTANGULAR;
			}
			
			space.projection.createTransformer();
			
			var columnsCount:int = mapContent.readByte();
			settings.columnsCount = columnsCount;
			settings.columns = new Array(columnsCount);
			
			for (var column:int = 0;column<columnsCount;column++) {
				var isKey:Boolean = mapContent.readUnsignedByte() == 1;
				var len:uint = mapContent.readUnsignedInt();
				var columnName:String = mapContent.readUTFBytes(len);
				settings.columns[column] = columnName;
				if (isKey && columnName == settings.keyColumn)
					this.keyColumnIndex = column;
			}
			
			space.geoBounds = new Rectangle(mapContent.readDouble(), mapContent.readDouble());
			space.geoBounds.width = mapContent.readDouble() - space.geoBounds.x;
			space.geoBounds.height = mapContent.readDouble() - space.geoBounds.y;
			
			this.maxLength = Math.min(space.geoBounds.width, space.geoBounds.height)*.1;
			
			MapCentroidBounder.setCustomBounds(space);
			
			if (space.isWorldMap) {
				space.geoBounds.top = -90;
				space.geoBounds.bottom = 90;
				space.geoBounds.left = -180;
				space.geoBounds.right = 180;
				
				if (!isNaN(space.projection.centerY)) {
					var y:Number = space.projection.centerY;
					if (y < 0)
						space.geoBounds.height += y;
					else
						space.geoBounds.top += y;
				}
				if (!isNaN(space.projection.centerX)) {
					var x:Number = space.projection.centerX;
					if (x < 0) {
						space.geoBounds.x += x;
					}else {
						space.geoBounds.left += x;
					}
				}
				/* space.geoBounds.top = Math.max(-90, );
				space.geoBounds.bottom = Math.min(90, space.geoBounds.bottom);
				space.geoBounds.left = Math.min(-180, space.geoBounds.left);
				space.geoBounds.right = Math.max(180, space.geoBounds.right); */
			}
			
			space.calculateProjection(settings.grid == null ? false : settings.grid.hasLabels);
			MapCentroidBounder.setLinearBounds(space);
			
			var i:uint;
			var regionCount:uint = mapContent.readUnsignedInt();
			for (i = 0; i < regionCount; i++)
				this.readRegion(space, mapContent, settings);
				
			//connectors
			//Known bug: key column not supported
 			
 			var connectorsCount:uint = mapContent.readInt();
 			for (i = 0; i < connectorsCount; i++) {
 				var name:String = mapContent.readUTFBytes(mapContent.readUnsignedInt());
 				if (settings.plot.regionsMap[name] == null)
					return;
					
				var connectorPoint:Point = this.readGeoPoint(space, mapContent);
				var hAlign:int = -1;
				var vAlign:int = -1;
				switch (mapContent.readUnsignedByte()) {
					case 0: case 2: hAlign = HorizontalAlign.CENTER; break;
					case 1: hAlign = HorizontalAlign.LEFT; break;
					case 2: hAlign = HorizontalAlign.RIGHT; break;
				}
				switch (mapContent.readUnsignedByte()) {
					case 0: case 2: vAlign = VerticalAlign.CENTER; break;
					case 1: vAlign = VerticalAlign.TOP; break;
					case 2: vAlign = VerticalAlign.BOTTOM; break;
				}
				var padding:Number = mapContent.readInt();
 			}
		}
		
		private function readRegion(space:GeoSpace, mapContent:ByteArray, settings:MapSettings):void {
			var name:String = "";
			var data:Object = {};
			var ln:uint = settings.columnsCount;
			for (var column:int = 0; column < ln; column++) {
				var value:String = mapContent.readUTFBytes(mapContent.readUnsignedInt());
				data[settings.columns[column]] = value;
				if (column == this.keyColumnIndex) name = value;
			}

			
			if (name == "")
				name = data[0];
				
			var region:MapRegion = settings.plot.createRegion(name, data);

			if (mapContent.readUnsignedByte() == 1)
				this.readTransformedRegion(region, space, mapContent, settings);
			else
				this.readNormalRegion(region, space, mapContent, settings);
			region.name = name;
		}
		
		private function readNormalRegion(region:MapRegion, space:GeoSpace, mapContent:ByteArray, settings:MapSettings):void {
			region.space = space;

			var min:Point = new Point(Number.MAX_VALUE, Number.MAX_VALUE);
			var max:Point = new Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
			
			region.geoCentroid = new Point(mapContent.readDouble(), mapContent.readDouble());
			region.linearGeoCentroid = new Point();
			space.linearize(region.geoCentroid.x, region.geoCentroid.y, region.linearGeoCentroid);
			
			region.geoBounds = new Rectangle();
			region.polygons = [];
			
			var polygonsCount:uint = mapContent.readUnsignedInt();
			
			for (var i:uint=0; i<polygonsCount; i++)
				this.readPolygon(space, mapContent,region,min,max, settings);
			
			region.geoBounds.x = min.x;
			region.geoBounds.y = min.y;
			region.geoBounds.width = max.x - min.x;
			region.geoBounds.height = max.y - min.y;
		}
		
		private function readTransformedRegion(region:MapRegion, space:GeoSpace, mapContent:ByteArray, settings:MapSettings):void {
			var trSpace:TransformedGeoSpace = new TransformedGeoSpace(mapContent.readDouble(), mapContent.readDouble(),mapContent.readDouble(),mapContent.readDouble()); 
			region.space = trSpace;
			trSpace.projection = space.projection.createCopy();

			var min:Point = new Point(Number.MAX_VALUE, Number.MAX_VALUE);
			var max:Point = new Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
			var linearMin:Point = new Point(Number.MAX_VALUE, Number.MAX_VALUE);
			var linearMax:Point = new Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
			
			region.geoCentroid = new Point(mapContent.readDouble(), mapContent.readDouble());
			region.linearGeoCentroid = new Point();
			
			region.geoBounds = new Rectangle();
			region.polygons = [];
			
			var i:uint;
			
			var polygonsCount:uint = mapContent.readUnsignedInt();
			for (i=0; i<polygonsCount; i++)
				this.readTransformedPolygon(mapContent,region,min,max, settings, space);
				
			//устанавливаем trSpace.geoBounds нелинейные географические размеры региона			
			trSpace.geoBounds = new Rectangle();
			trSpace.geoBounds.x = min.x;
			trSpace.geoBounds.y = min.y;
			trSpace.geoBounds.width = max.x - min.x;
			trSpace.geoBounds.height = max.y - min.y;
			
			trSpace.projection.centerX = NaN;
			trSpace.projection.centerY = NaN;
			trSpace.calculateProjection();
			
			//высчитываем центроид региона в линейных координатах
			trSpace.linearize(region.geoCentroid.x, region.geoCentroid.y, region.linearGeoCentroid);
			
			for (i = 0;i<region.polygons.length;i++) {
				var poly:Array = region.polygons[i];
				for (var j:uint = 0;j<poly.length;j++) {
					
					var vPoint:Point = poly[j];
					
					trSpace.linearize(vPoint.x, vPoint.y, vPoint);
					
					linearMax.x = Math.max(vPoint.x, linearMax.x);
					linearMax.y = Math.max(vPoint.y, linearMax.y);
					linearMin.x = Math.min(vPoint.x, linearMin.x);
					linearMin.y = Math.min(vPoint.y, linearMin.y);
				}
			}
			
			region.geoBounds.x = linearMin.x;
			region.geoBounds.y = linearMin.y;
			region.geoBounds.width = linearMax.x - linearMin.x;
			region.geoBounds.height = linearMax.y - linearMin.y;
			
			trSpace.linearLeftTop.x = min.x + trSpace.tx;
			trSpace.linearLeftTop.y = min.y + trSpace.ty;
			trSpace.linearRightBottom.x = min.x + trSpace.tx + (max.x - min.x)*trSpace.scaleX;
			trSpace.linearRightBottom.y = min.y + trSpace.ty + (max.y - min.y)*trSpace.scaleY;
			
			space.linearize(trSpace.linearLeftTop.x, trSpace.linearLeftTop.y, trSpace.linearLeftTop);
			space.linearize(trSpace.linearRightBottom.x, trSpace.linearRightBottom.y, trSpace.linearRightBottom);
			
			space.transformedRegions.push(region);
		}
		
		private var maxLength:Number;
		
		private function readPolygon(space:GeoSpace, mapContent:ByteArray, region:MapRegion, min:Point, max:Point, settings:MapSettings):void {
			var poly:Array = new Array();
			var pointsCount:uint = mapContent.readUnsignedInt();
			var j:uint;
			var vPoint:Point;
			
			var tmpP:Point = new Point();
			var tmp1:Point;
			if (settings.smoothing && space.projection.needExtraPoints) {
				var prevPoint:Point;
				for (j=0; j<pointsCount; j++) {
					vPoint = new Point(mapContent.readDouble(), mapContent.readDouble());
					if (prevPoint == null) {
						prevPoint = vPoint.clone();
						space.linearize(vPoint.x, vPoint.y, vPoint);
						addPoint(poly, vPoint, min, max);
					}else {
						tmpP.x = vPoint.x - prevPoint.x;
						tmpP.y = vPoint.y - prevPoint.y;
						var len:Number = Math.abs(tmpP.length);
						if (len > this.maxLength) {
							var cnt:int = Math.ceil(len/this.maxLength);
							var dx:Number = tmpP.x/cnt;
							var dy:Number = tmpP.y/cnt; 
							
							for (var k:int = 0;k<cnt;k++) {
								tmp1 = new Point(prevPoint.x + dx*k, prevPoint.y + dy*k);
								space.linearize(tmp1.x, tmp1.y, tmp1);
								poly.push(tmp1);
							}
						}
						prevPoint = vPoint.clone();
						space.linearize(vPoint.x, vPoint.y, vPoint);
						this.addPoint(poly, vPoint, min, max);
					}
				}
			}else {
				for (j=0; j<pointsCount; j++) {
					vPoint = new Point(mapContent.readDouble(), mapContent.readDouble());
					space.linearize(vPoint.x, vPoint.y, vPoint);
					addPoint(poly, vPoint, min, max);
				}
			}
			
			region.polygons.push(poly);
		}
		
		private function addPoint(poly:Array, point:Point, min:Point, max:Point):void {
			max.x = Math.max(point.x, max.x);
			max.y = Math.max(point.y, max.y);
			min.x = Math.min(point.x, min.x);
			min.y = Math.min(point.y, min.y);
			poly.push(point);
		}
		
		private function readTransformedPolygon(mapContent:ByteArray, region:MapRegion, min:Point, max:Point, settings:MapSettings, space:GeoSpace):void {
			var poly:Array = new Array();
			var pointsCount:uint = mapContent.readUnsignedInt();
			var j:uint;
			var vPoint:Point;
			var tmpP:Point = new Point();
			var tmp1:Point;
			if (settings.smoothing && space.projection.needExtraPoints) {
				var prevPoint:Point;
				for (j=0; j<pointsCount; j++) {
					vPoint = new Point(mapContent.readDouble(), mapContent.readDouble());
					if (prevPoint == null) {
						prevPoint = vPoint.clone();
						addPoint(poly, vPoint, min, max);
					}else {
						tmpP.x = vPoint.x - prevPoint.x;
						tmpP.y = vPoint.y - prevPoint.y;
						var len:Number = Math.abs(tmpP.length);
						if (len > this.maxLength) {
							var cnt:int = Math.ceil(len/this.maxLength);
							var dx:Number = tmpP.x/cnt;
							var dy:Number = tmpP.y/cnt; 
							
							for (var k:int = 0;k<cnt;k++) {
								tmp1 = new Point(prevPoint.x + dx*k, prevPoint.y + dy*k);
								poly.push(tmp1);
							}
						}
						prevPoint = vPoint.clone();
						this.addPoint(poly, vPoint, min, max);
					}
				}
			}else {
				for (j=0; j<pointsCount; j++) {
					vPoint = new Point(mapContent.readDouble(), mapContent.readDouble());
					addPoint(poly, vPoint, min, max);
				}
			}
			
			region.polygons.push(poly);
		}
		
		private function readGeoPoint(space:GeoSpace, mapContent:ByteArray):Point {
			var pt:Point = new Point(mapContent.readDouble(), mapContent.readDouble());
			space.linearize(pt.x, pt.y, pt);
			return pt;
		}
	}
}