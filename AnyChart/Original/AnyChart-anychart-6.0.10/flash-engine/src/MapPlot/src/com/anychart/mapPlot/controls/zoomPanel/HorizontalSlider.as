package com.anychart.mapPlot.controls.zoomPanel {
	internal final class HorizontalSlider extends Slider {
		override protected function setSliderBounds():void {
			this.sliderBounds.y = this.bounds.height/2 - 3;
			this.sliderBounds.height = 6;
			this.sliderBounds.width = this.bounds.width;
			
			this.thumb.bounds.height = this.bounds.height;
			this.thumb.bounds.width = 9;
			
			this.dragBounds.x = 0;
			this.dragBounds.height = 0;
			this.dragBounds.y = 0;
			this.dragBounds.width = this.bounds.width - 9;
		}
		
		override public function get value():Number {
			return (this.thumb.x/(this.bounds.width - 9));
		}
		
		override public function set value(newValue:Number):void {
			this.thumb.x = newValue*(this.bounds.width-9);
		}
	}
}