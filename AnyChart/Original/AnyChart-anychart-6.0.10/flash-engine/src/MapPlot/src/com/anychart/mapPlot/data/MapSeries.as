package com.anychart.mapPlot.data {
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	
	public final class MapSeries extends BaseSeries {
		
		public function MapSeries() {
			super();
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = '';
			this.cashedTokens['MinYValuePointName'] = '';
		}
		
		override public function resetTokens():void {
			super.resetTokens();
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = '';
			this.cashedTokens['MinYValuePointName'] = '';
		}
		
		override public function createPoint():BasePoint {
			return new MapRegion();
		}
		
		override public function addPointsToDrawing():Boolean {
			return false;
		}
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var pt:MapRegion = MapRegion(point);
			if (!isNaN(pt.value)) {
				this.cashedTokens['YSum'] += pt.value;
				if (pt.value > this.cashedTokens['YMax']) {
					this.cashedTokens['YMax'] = pt.value;
					this.cashedTokens['MaxYValuePointName'] = pt.name;
				}
				if (pt.value < this.cashedTokens['YMin']) {
					this.cashedTokens['YMin'] = pt.value;
					this.cashedTokens['MinYValuePointName'] = pt.name;
				}
			}
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			if (plotTokensCash['YSum'] == null)
				plotTokensCash['YSum'] = 0;
			if (plotTokensCash['YBasedPointsCount'] == null)
				plotTokensCash['YBasedPointsCount'] = 0;
				
			plotTokensCash['YSum'] += this.cashedTokens['YSum'];
			plotTokensCash['YBasedPointsCount'] += this.points.length;
			
			if (plotTokensCash['YMax'] == null || this.cashedTokens['YMax'] > plotTokensCash['YMax']) {
				plotTokensCash['YMax'] = this.cashedTokens['YMax'];
				plotTokensCash['MaxYValuePointName'] = this.cashedTokens['MaxYValuePointName'];
				plotTokensCash['MaxYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['YMin'] == null || this.cashedTokens['YMin'] < plotTokensCash['YMin']) {
				plotTokensCash['YMin'] = this.cashedTokens['YMin'];
				plotTokensCash['MinYValuePointName'] = this.cashedTokens['MinYValuePointName'];
				plotTokensCash['MinYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['MaxYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MaxYSumSeries']) {
				plotTokensCash['MaxYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MaxYSumSeriesName'] = this.name;
			}
			if (plotTokensCash['MinYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MinYSumSeries']) {
				plotTokensCash['MinYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MinYSumSeriesName'] = this.name;
			}
			
			super.checkPlot(plotTokensCash);
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesFirstYValue') return this.points.length > 0 ? this.points[0].value : '';
			if (token == '%SeriesLastYValue') return this.points.length > 0 ? this.points[this.points.length-1].value : '';
			if (token == '%SeriesYSum') return this.cashedTokens['YSum'];
			if (token == '%SeriesYMax') return this.cashedTokens['YMax'];
			if (token == '%SeriesYMin') return this.cashedTokens['YMin'];
			if (token == '%SeriesYAverage') {
				if (this.cashedTokens['YAverage'] == null)
					this.cashedTokens['YAverage'] = this.cashedTokens['YSum']/this.points.length;
				return this.cashedTokens['YAverage'];
			}
			return super.getSeriesTokenValue(token);
		}
	}
}