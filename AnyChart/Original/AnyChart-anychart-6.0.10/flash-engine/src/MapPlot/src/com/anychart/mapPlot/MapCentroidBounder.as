package com.anychart.mapPlot {
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	
	import flash.geom.Point;
	
	
	public final class MapCentroidBounder {
		
		public static function setCustomBounds(space:GeoSpace):void {
			if (!isNaN(space.projection.centerX)) {
				var hSpaceInfo:BoundCustomInfo = new BoundCustomInfo(space.projection.centerX, space.geoBounds.x, space.geoBounds.width);
				space.geoBounds.width = hSpaceInfo.calcNewRange();
				space.geoBounds.x = hSpaceInfo.calcNewStart();
			}
			
			if (!isNaN(space.projection.centerY)) {
				var vSpaceInfo:BoundCustomInfo = new BoundCustomInfo(space.projection.centerY, space.geoBounds.y, space.geoBounds.height);
				space.geoBounds.height = vSpaceInfo.calcNewRange();
				space.geoBounds.y = vSpaceInfo.calcNewStart();
			} 
		}
		
		public static function setLinearBounds(space:GeoSpace):void {
		}
	}
}

class BoundCustomInfo {
	public var center:Number;
	public var range:Number;
	public var start:Number;
	
	public function BoundCustomInfo(center:Number, start:Number, range:Number) {
		this.center = center;
		this.start = start;
		this.range = range;
	}
	
	public function calcNewRange():Number {
		return 2*((this.center >= (this.start + this.range/2)) ?
					(this.center - this.start) : 
					(this.range - this.center + this.start)
				 );
	}
	
	public function calcNewStart():Number {
		if (this.center < (this.start + this.range/2))
			return 2*this.center - this.range - this.start;
		return this.start;
	}
}