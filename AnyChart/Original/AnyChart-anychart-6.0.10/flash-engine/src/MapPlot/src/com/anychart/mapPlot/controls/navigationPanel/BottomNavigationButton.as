package com.anychart.mapPlot.controls.navigationPanel {
	import com.anychart.mapPlot.controls.MapControlButton;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class BottomNavigationButton extends MapControlButton {
		
		override protected function drawPointer():void {
			var g:Graphics = this.graphics;
			var rc:Rectangle = this.pointerBounds;
			
			var opacity:Number = 1;
			
			g.beginFill(0,0);
			g.lineStyle(1,0,opacity);
			g.moveTo(rc.left+rc.width/2,rc.top+rc.height);
			g.lineTo(rc.left,rc.top);
			g.lineStyle(0,0,0);
			g.endFill();
			
			g.lineStyle(1,0,opacity);
			g.moveTo(rc.left+rc.width/2,rc.top+rc.height);
			g.lineTo(rc.left+rc.width,rc.top);
			g.lineStyle(0,0,0);
			g.endFill();
		}
	}
}