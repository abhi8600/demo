package com.anychart.mapPlot.geoSpace.projection.transformers {
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	import com.anychart.mapPlot.geoSpace.projection.Projection;
	import com.anychart.mapPlot.geoSpace.projection.ProjectionType;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	//Abstract factory
	public class ProjectionTransformer {
		
		private var projection:Projection;
		protected final function get centerX():Number { return this.projection.centerX; }
		protected final function get centerY():Number { return this.projection.centerY; }
		
		public function get needExtraPoints():Boolean { return true; }
		
		protected final function sign(value:Number):Number {
			if(value==0) return 0;
			return (value<0) ? -1 : 1;
		}
		
		protected final function a(num1:Number, num2:Number, num3:Number):Number {
		    if (num1 < num2)
		        return num2;
		    if (num1 > num3)
		        return num3;
		    return num1;
		}
		
		public function transform(geoX:Number, geoY:Number, pt:Point):void {}
		public function setLinearBounds(geoBounds:Rectangle, linearGeoBounds:Rectangle):void {
			var minX:Number = Number.POSITIVE_INFINITY;
			var maxX:Number = Number.NEGATIVE_INFINITY;
			var minY:Number = Number.POSITIVE_INFINITY;
			var maxY:Number = Number.NEGATIVE_INFINITY;
			
			var pt:Point = new Point();
			
			var geoX:Number;
			var geoY:Number;
			for (var i:uint = 0;i<300;i++) {
				geoY = geoBounds.top + geoBounds.height*i/300;
				geoX = geoBounds.left;
				this.transform(geoX, geoY, pt);
				minY = Math.min(pt.y, minY);
				maxY = Math.max(pt.y, maxY);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
				
				geoX = geoBounds.right;
				this.transform(geoX, geoY, pt);
				minY = Math.min(pt.y, minY);
				maxY = Math.max(pt.y, maxY);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
				
				geoX = geoBounds.left + geoBounds.width*i/300;
				geoY = geoBounds.top;
				this.transform(geoX, geoY, pt);
				minY = Math.min(pt.y, minY);
				maxY = Math.max(pt.y, maxY);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
				
				geoY = geoBounds.bottom;
				this.transform(geoX, geoY, pt);
				minY = Math.min(pt.y, minY);
				maxY = Math.max(pt.y, maxY);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
			}
			
			linearGeoBounds.x = minX;
			linearGeoBounds.y = minY;
			linearGeoBounds.width = maxX - minX;
			linearGeoBounds.height = maxY - minY;
		}
		
		/**
		 * Draw line
		 * start and end points contains non-linear geo coordinates
		 */
		public function drawLine(g:Graphics, 
								 space:BaseGeoSpace,
								 start:Point, end:Point,
								 moveTo:Boolean):void {
			
			var dx:Number = (end.x - start.x)/50;
			var dy:Number = (end.y - start.y)/50;
			
			var pixPt:Point = new Point();
			space.transform(start.x, start.y, pixPt);
			if (moveTo)
				g.moveTo(pixPt.x, pixPt.y);
			else
				g.lineTo(pixPt.x, pixPt.y);
				
			for (var i:uint = 1;i<50;i++) {
				space.transform(start.x + dx*i, start.y + dy*i, pixPt);
				g.lineTo(pixPt.x, pixPt.y);
			}
			
			space.transform(end.x, end.y, pixPt);
			g.lineTo(pixPt.x, pixPt.y);
		}
		
		public function interpolateLine(start:Point, end:Point, linearPts:Array):void {
			var dx:Number = (end.x - start.x)/50;
			var dy:Number = (end.y - start.y)/50;
			
			var pixPt:Point = new Point();
			
			var lastX:Number = start.x;
			var lastY:Number = start.y;
			for (var i:int = 0;i<50;i++) {
				this.transform(lastX, lastY, pixPt);
				lastX += dx;
				lastY += dy;
				linearPts.push(pixPt.clone());
			}
			this.transform(end.x, end.y, pixPt);
			linearPts.push(pixPt.clone());
		}
		
		public static function create(projection:Projection):ProjectionTransformer {
			var p:ProjectionTransformer;
			switch (projection.type) {
				case ProjectionType.EQUIRECTANGULAR:
					p = new ProjectionEquirectangular();
					break;
				case ProjectionType.MERCATOR:
					p = new ProjectionMercator();
					break;
				case ProjectionType.WAGNER_3:
					p = new ProjectionWagner3();
					break;
				case ProjectionType.FAHEY:
					p = new ProjectionFahey();
					break;
				case ProjectionType.ECKERT_1:
					p = new ProjectionEckert1();
					break;
				case ProjectionType.ECKERT_3:
					p = new ProjectionEckert3();
					break;
				case ProjectionType.HAMMER_AITOFF:
					p = new ProjectionHammerAitoff();
					break;
				case ProjectionType.ROBINSON:
					p = new ProjectionRobinson();
					break;
				case ProjectionType.ORTHOGRAPHIC:
					p = new ProjectionOrthographic();
					break;
				case ProjectionType.BONNE:
					p = new ProjectionBonne();
					break;
			}
			p.projection = projection;
			return p;
		}
	}
}