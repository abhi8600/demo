package com.anychart.mapPlot.data {
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class MapRegion extends BasePoint {
		
		public var space:BaseGeoSpace;
		public var value:Number;
		
		public var data:Object;
		public var polygons:Array;
		
		public var geoBounds:Rectangle;
		public var linearGeoCentroid:Point;
		public var geoCentroid:Point;
		
		private var pixBounds:Rectangle;
		private var pixCentroid:Point;
		
		private var labelBound:Rectangle;
		
		public function MapRegion() {
			super();
			this.pixBounds = new Rectangle();
			this.labelBound = new Rectangle();
			this.labelNotAdded = true;
			this.stopPropagation = false;
		}
		
		public function initializeSelection():void {
			super.initializeStyleState();
		}
		
		override public function initialize():void {
			super.initializeContainer();
		}
		
		override public function getMainContainer():IMainElementsContainer { 
			return MapPlot(super.getMainContainer()).regionElementsContainer; 
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@id != undefined) this.name = SerializerBase.getString(data.@id);
			
			if (data.@y != undefined) this.value = SerializerBase.getNumber(data.@y);
			else if (data.@value != undefined) this.value = SerializerBase.getNumber(data.@value);
			
			this._id = this.name;
			
			if (this.name != null)
				MapPlot(this.plot).registerDefinedRegion(this);
		}
		
		private function calcPixBounds():void {
			var pt1:Point = new Point();
			this.space.transformLinearized(this.geoBounds.topLeft, pt1);
			this.pixBounds.x = pt1.x;
			this.pixBounds.y = pt1.y;
			
			this.space.transformLinearized(this.geoBounds.bottomRight, pt1);
			this.pixBounds.width = pt1.x - this.pixBounds.x;
			this.pixBounds.height = pt1.y - this.pixBounds.y;
			
			this.pixCentroid = new Point();
			this.space.transformLinearized(this.linearGeoCentroid, this.pixCentroid);
		}
		
		private var labelNotAdded:Boolean;
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			this.calcPixBounds();
			
			if (this.labelSprite != null) {
				var newLabelBounds:Rectangle = this.label.getBounds(this, this.label.style.normal, LABEL_INDEX);
				var pos:Point = this.getElementPosition(BaseElementStyleState(this.label.style.normal), this.labelSprite, newLabelBounds);
				this.labelBound.x = pos.x;
				this.labelBound.y = pos.y;
				this.labelBound.width = newLabelBounds.width;
				this.labelBound.height = newLabelBounds.height;
				if (this.labelNotAdded) {
					MapPlot(this.plot).regionLabelsManager.registerLabel(this.labelSprite, this.labelBound, this.pixBounds);
					this.labelNotAdded = false;
				}else {
					MapPlot(this.plot).regionLabelsManager.recheckLabel(this.labelSprite, this.labelBound, this.pixBounds);
				}
			}
			
			this.drawRegion();
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawRegion();
		}
		
		private function drawRegion():void {
			var g:Graphics = this.container.graphics;
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);

			var borderG:Graphics; 
			
			if (this.polygons != null)
			{
				if (state.stroke == null) {
					MapPlot(this.plot).removeBorderContainer(this);
				}else {
					borderG = MapPlot(this.plot).createBorderContainer(this);
					borderG.clear();
				}
				
				if (state.fill == null && state.hatchFill == null && state.stroke == null) return;
				
				var pixPt:Point = new Point(); 
			
				for (var i:uint = 0;i<this.polygons.length;i++) {
					if (state.fill != null) {
						state.fill.begin(g, this.pixBounds, this.color);
						this.drawPolygon(g, pixPt, i);
						g.endFill();
					}
					
					if (state.stroke != null) {
						state.stroke.apply(borderG, this.pixBounds, this.color);
						this.drawPolygon(borderG, pixPt, i);
						borderG.lineStyle();
					}
					
					if (state.hatchFill != null) {
						state.hatchFill.beginFill(g, this.hatchType, this.color);
						this.drawPolygon(g, pixPt, i);
						g.endFill();
					}
				}
			}
		}
		
		public function drawBorder(stroke:Stroke, g:Graphics):void {
			var pixPt:Point = new Point(); 
			
			stroke.apply(g, this.pixBounds, this.color);
			for (var i:uint = 0;i<this.polygons.length;i++) {
				this.drawPolygon(g, pixPt, i);
			}
			g.lineStyle();
		}
		
		private function drawPolygon(g:Graphics, pixPt:Point, index:uint):void {
			var polygon:Array = this.polygons[index];
			this.space.transformLinearized(polygon[0], pixPt);
			g.moveTo(pixPt.x, pixPt.y);
			for (var j:uint = 1;j<polygon.length;j++) {
				this.space.transformLinearized(polygon[j], pixPt);
				g.lineTo(pixPt.x, pixPt.y);
			}
			this.space.transformLinearized(polygon[0], pixPt);
			g.lineTo(pixPt.x, pixPt.y);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			pos.x = (this.pixCentroid !=null) ? this.pixCentroid.x : NaN;
			pos.y = (this.pixCentroid !=null) ? this.pixCentroid.y : NaN;
		}
		
		override public function resetTokens():void {
			super.resetTokens();
			this.cashedTokens['YPercentOfSeries'] == null;
			this.cashedTokens['YPercentOfTotal'] == null;
		}
		
		override public function getPointTokenValue(token:String):* {
			var dataToken:String = token.substr(1);
			if (this.data==null) return null;
			if (this.data[dataToken] != null)
				return this.data[dataToken];
			if (token == '%Value' || token == '%YValue') return this.value;
			if (token == '%YPercentOfSeries') {
				if (this.cashedTokens['YPercentOfSeries'] == null) {
					if (!isNaN(this.value) && !isNaN(Number(this.series.getSeriesTokenValue("%SeriesYSum"))) )
					{
						this.cashedTokens['YPercentOfSeries'] = (this.series.getSeriesTokenValue("%SeriesYSum")!=0) ? this.value/this.series.getSeriesTokenValue("%SeriesYSum")*100 :0;
					}
				}
				return this.cashedTokens['YPercentOfSeries'];
			}
			if (token == '%YPercentOfTotal') {
				if (this.cashedTokens['YPercentOfTotal'] == null) {
					if (!isNaN(this.value) && !isNaN(Number(this.series.getSeriesTokenValue("%DataPlotYSum"))) )
					{
						this.cashedTokens['YPercentOfTotal'] = (this.plot.getTokenValue('%DataPlotYSum')!=0)? this.value/this.plot.getTokenValue('%DataPlotYSum')*100 :0;
					}
				}
				return this.cashedTokens['YPercentOfTotal'];
			}
			return super.getPointTokenValue(token);
		}
		
		override public function serialize(res:Object=null):Object {
			res = super.serialize(res);
			for (var attr:String in this.data)
				res.Attributes[attr] = this.data[attr];
			return res;
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "y" || field == "value";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return  {
				value: this.value,
				y: this.value
			}
		}
		
		override public function updateValue(newValue:Object):void {
			MapPlot(this.plot).needRedrawMap = true;
			if (newValue.y != undefined) this.value = newValue.y;
			else if (newValue.value != undefined) this.value = newValue.value;
		}
	}
}