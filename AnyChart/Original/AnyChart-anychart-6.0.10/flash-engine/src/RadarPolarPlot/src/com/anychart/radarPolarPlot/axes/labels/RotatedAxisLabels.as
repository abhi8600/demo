package com.anychart.radarPolarPlot.axes.labels {
	import com.anychart.radarPolarPlot.axes.Axis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	public class RotatedAxisLabels extends AxisLabels {
		
		private var rotateCircular_:Boolean;
		public function get rotateCircular():Boolean { return this.rotateCircular_; }
		
		private var autoOrientation:Boolean;
		
		private var baseRotation:Number;
		
		public function RotatedAxisLabels(axis:Axis) {
			super(axis);
			this.rotateCircular_ = true;
			this.autoOrientation = false;
			this.baseRotation = 0;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources, style);
			if (data.@circular_labels_style != undefined) {
				switch (SerializerBase.getLString(data.@circular_labels_style)) {
					default:
					case "horizontal":
						this.rotation = 0;
						this.rotateCircular_ = false;
						this.autoOrientation = false;
						break;
					case "circular":
						this.rotation = 0;
						this.rotateCircular_ = true;
						this.autoOrientation = true;
						break;
					case "radial":
						this.rotation = 90;
						this.rotateCircular_ = true;
						this.autoOrientation = true;
						break;
						
				}
			}else {
				this.rotation = 0;
				this.rotateCircular_ = false;
				this.autoOrientation = false;
			}
			//if (data.@rotate_circular != undefined) this.rotateCircular_ = SerializerBase.getBoolean(data.@rotate_circular);
			//if (data.@auto_orientation != undefined) this.autoOrientation = SerializerBase.getBoolean(data.@auto_orientation);
			
			this.baseRotation = this.rotation;
		}
		
		public function setAngle(value:Number):void {
			if (this.rotateCircular_) {
				var invert:Boolean = this.autoOrientation && Math.sin(value*Math.PI/180) > 0;
				this.rotation = (!invert) ? (this.baseRotation+value+90) : (this.baseRotation+value-90);
			}
		}
		
		public function clearAngle(value:Number):void {
			if (this.rotateCircular_) {
				this.rotation = this.baseRotation;
			}
		}
		
		public function getPosition(info:TextElementInformation, pixPos:Number):Point {
			return this.rotateCircular_ ? 
								this.getRotateCircularPosition(info, pixPos) :
								this.getFixedPosition(info, pixPos);
		}
		
		override public function drawLabel(g:Graphics, x:Number, y:Number, info:TextElementInformation, pixValue:Number):void {
			var anchorAngle:Number = (pixValue + this.axis.plot.getYAxisCrossing());
			
			this.setAngle(anchorAngle);
			this.getBounds(info);
			var pos:Point = this.getPosition(info, pixValue);
			this.draw(g, pos.x, pos.y, info, 0, 0);
			this.clearAngle(pixValue);
		}
		
		private function getRotateCircularPosition(info:TextElementInformation, 
												   pixValue:Number):Point {
			var anchorAngle:Number = (pixValue + this.axis.plot.getYAxisCrossing());
			
			var pos:Point = new Point();
			pos.x = this.axis.center.x;
			pos.y = this.axis.center.y;
			
			this.clearAngle(anchorAngle);
			this.getBounds(info);
			var size:Number = info.rotatedBounds.height;
			this.setAngle(anchorAngle);
			this.getBounds(info);
			
			var insideTickmarksSpace:Number = (this.axis.majorTickmark != null && this.axis.majorTickmark.enabled && this.axis.majorTickmark.inside) ?
				this.axis.majorTickmark.size : 0;
			
			var outsideTickmarksSpace:Number = (this.axis.majorTickmark != null && this.axis.majorTickmark.enabled && this.axis.majorTickmark.outside) ? 
				this.axis.majorTickmark.size : 0;
			
			var r:Number = Math.min(this.axis.bounds.width, this.axis.bounds.height) / 2;
			
			if (this.isInsideLabels) {
				r -= this.padding + size/2 + insideTickmarksSpace;
			}else {
				r += this.padding + size/2 + outsideTickmarksSpace;
			}
			
			var angle:Number = anchorAngle*Math.PI/180;
			var cos:Number = Math.round(Math.cos(angle)*1000)/1000;
			var sin:Number = Math.round(Math.sin(angle)*1000)/1000;
			pos.x += r * cos;
			pos.y += r * sin;
			
			pos.x -= info.rotatedBounds.width/2;
			pos.y -= info.rotatedBounds.height/2;
			
			return pos;
		}
		
		private function getFixedPosition(info:TextElementInformation, 
										  pixValue:Number):Point {
			
			var anchorAngle:Number = (pixValue + this.axis.plot.getYAxisCrossing()) * Math.PI/180;
			var r:Number = Math.min(this.axis.bounds.height, this.axis.bounds.width) / 2;
			
			var insideTickmarksSpace:Number = (this.axis.majorTickmark != null && this.axis.majorTickmark.enabled && this.axis.majorTickmark.inside) ?
				this.axis.majorTickmark.size : 0;
			
			var outsideTickmarksSpace:Number = (this.axis.majorTickmark != null && this.axis.majorTickmark.enabled && this.axis.majorTickmark.outside) ? 
				this.axis.majorTickmark.size : 0;
			
			var w:Number = info.rotatedBounds.width;
			var h:Number = info.rotatedBounds.height;
			
			var cos:Number = Math.round(Math.cos(anchorAngle)*100)/100;
			var sin:Number = Math.round(Math.sin(anchorAngle)*100)/100;
			
			var dx:Number = -w/2;
			var dy:Number = -h/2;
			
			var dr:Number = 0;
			
			var criticalSin:Number = Math.abs(h) / ((2/Math.sqrt(2))*Math.sqrt(Math.pow(w,2) + Math.pow(h,2)));
			criticalSin = Math.round(criticalSin*100)/100;
			
			if (Math.abs(sin) < criticalSin) {
				if (cos != 0)
					dr += w/(2*Math.abs(cos));
			}else {
				if (sin != 0)
					dr += h/(2*Math.abs(sin));
			}
			
			if (this.isInsideLabels) { 
				r -= this.padding + dr + insideTickmarksSpace;
			}else {
				var r0:Number = r + this.padding;
				var r1:Number = r + this.padding + outsideTickmarksSpace;
				var r2:Number = r + this.padding + dr + outsideTickmarksSpace;
				
				var r0p2:Number = Math.pow(r0, 2);
				var r1p2:Number = Math.pow(r1, 2);
				
				var xOffset:Number = r2*Math.abs(cos) - Math.sqrt(r0p2 - r1p2 + r1p2*Math.pow(cos, 2));
				
				var labelXOffset:Number;
				if (cos > 0) {
					labelXOffset = w/2 - xOffset;
					if (labelXOffset < w/2 && labelXOffset > -w/2) {
						dx += labelXOffset;
					} 
				}else {
					labelXOffset = w/2 - xOffset;
					if (labelXOffset < w/2 && labelXOffset > -w/2) {
						dx -= labelXOffset;
					} 
				}
				
				r += this.padding + dr + outsideTickmarksSpace;
			}
			
			var pos:Point = new Point();
			pos.x = this.axis.center.x + r*cos + dx;
			pos.y = this.axis.center.y + r*sin + dy;
			return pos;
		}
	}
}