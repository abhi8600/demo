package com.anychart.radarPolarPlot.utils
{
	import flash.geom.Point;

	public class PolarPoint
	{
		private var _center:Point;
		
		public var r:Number=0;
		public var phi:Number=0;
		
		public function get x():Number{
			return r*Math.cos(phi*Math.PI/180)+_center.x;
		}
		
		public function get y():Number{
			return r*Math.sin(phi*Math.PI/180)+_center.y;
		}
		
		public function PolarPoint(centerPoint:Point){
			this._center = centerPoint || new Point(0,0);
		}
		
		public function getCenter():Point {return _center;}
	}
}