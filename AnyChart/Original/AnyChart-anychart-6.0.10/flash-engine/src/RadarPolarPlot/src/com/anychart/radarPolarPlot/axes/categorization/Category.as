package com.anychart.radarPolarPlot.axes.categorization {
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.Axis;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.visual.text.TextElementInformation;
	
	public final class Category extends CategoryInfo {
		
		public var clusterSettings:ClusterSettings;
		
		private var sortedOverlays:Object;
		private var sortedOverlaysList:Array;
		
		private var stackSettings:Object;
		private var percentStackSettingsList:Array;
		
		private var overlaySettings:Object;
		
		public var info:TextElementInformation;
		
		public function Category(plot:RadarPolarPlot, index:int, name:String, axis:Axis) {
			this.axis = axis;
			this.index = index;
			this.name = name;
			super(plot);
		}
		
		//-------------------------------------------
		//				Points ordering
		//-------------------------------------------
		
		public function getSortedOverlay(seriesType:int):Array {
			if (this.sortedOverlays == null) {
				this.sortedOverlays = {};
				this.sortedOverlaysList = [];
			}
			if (this.sortedOverlays[seriesType] == null) {
				this.sortedOverlays[seriesType] = [];
				this.sortedOverlaysList.push(this.sortedOverlays[seriesType]);
			}
			return this.sortedOverlays[seriesType];
		}
		
		public function isStackSettingsExists(seriesType:int):Boolean {
			return this.stackSettings != null && this.stackSettings[seriesType] != null;
		}
		
		public function getStackSettings(seriesType:int, isPercent:Boolean):StackSettings {
			if (this.stackSettings == null) {
				this.stackSettings = {};
				if (isPercent)
					this.percentStackSettingsList = [];
			}
			
			if (this.stackSettings[seriesType] == null) {
				this.stackSettings[seriesType] = new StackSettings(isPercent);
				if (isPercent)
					this.percentStackSettingsList.push(this.stackSettings[seriesType]);
			}
			
			return this.stackSettings[seriesType];
		}
		
		public function getOverlaysCount(axisName:String, seriesType:int):uint {
			this.checkOverlays(axisName, seriesType);
			return this.overlaySettings[axisName][seriesType];
		}
		
		public function setOverlaysCount(axisName:String, seriesType:int, value:uint):void {
			this.checkOverlays(axisName, seriesType);
			this.overlaySettings[axisName][seriesType] = value;
		}
		
		private function checkOverlays(axisName:String, seriesType:int):void {
			if (this.overlaySettings == null)
				this.overlaySettings = {};
			
			if (this.overlaySettings[axisName] == null)
				this.overlaySettings[axisName] = {};
				
			if (this.overlaySettings[axisName][seriesType] == undefined)
				this.overlaySettings[axisName][seriesType] = 0;
		}
		
		public function onAfterDeserializeData(plotDrawingPoints:Array):void {
			var i:uint;
			if (this.sortedOverlaysList != null) {
				for (i=0;i<this.sortedOverlaysList.length;i++) {
					var list:Array = this.sortedOverlaysList[i];
					list.sortOn("sortedOverlayValue",Array.NUMERIC | Array.DESCENDING);
					
					var initialize:Boolean = true;
					if (list.length > 0)
						 initialize = RadarPolarPlotPoint(list[0]).global.initializeIfSortedOVerlay();
					
					for (var j:uint = 0;j<list.length;j++) {
						if (initialize) {
							list[j].initialize();
							plotDrawingPoints.push(list[j]);
						}
					}
					list = null;
				}
				this.sortedOverlays = null;
				this.sortedOverlaysList = null;
			}
			if (this.percentStackSettingsList != null) {
				for (i<0;i<this.percentStackSettingsList.length;i++)
					this.percentStackSettingsList[i].setPercent();
				this.percentStackSettingsList = null;
			}
		}
	}
}