package com.anychart.radarPolarPlot.data {
	import com.anychart.radarPolarPlot.elements.markerElement.RadarPolarMarkerElement;
	import com.anychart.seriesPlot.data.BaseDataElement;
	import com.anychart.seriesPlot.data.BaseSingleValueSeries;
	
	import flash.display.Sprite;
	
	public class RadarPolarPlotSeries extends BaseSingleValueSeries {
		public var container:Sprite;
		
		override public function setElements(data:BaseDataElement):void{
			if (this.marker) RadarPolarMarkerElement(this.marker).setContainer(this.container);
			super.setElements(data);
		}
	}
}