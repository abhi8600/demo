package com.anychart.radarPolarPlot.axes{
	
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.grids.RadarPolarGrid;
	import com.anychart.radarPolarPlot.axes.labels.AxisLabels;
	import com.anychart.radarPolarPlot.axes.tickmarks.Tickmark;
	import com.anychart.radarPolarPlot.scales.LinearScale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.utils.LineEquation;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Axis implements ISerializableRes, IObjectSerializable{

		public var enabled:Boolean = true;
		
		
		public var majorTickmark:Tickmark;
		public var minorTickmark:Tickmark;
		public var plot:RadarPolarPlot;
		public var scale:BaseScale;
		public var line:Stroke;
		public var majorGrid:RadarPolarGrid;
		public var minorGrid:RadarPolarGrid;
		public var labels:AxisLabels;
		
		public var bounds:Rectangle;
		public var center:Point;
		
		protected var gridInterlacesFillingContainer:Shape;
		protected var gridLinesContainer:Shape;
		protected var tickmarksContainer:Sprite;
		protected var labelsContainer:Sprite;
		protected var axisLinesContainer:Shape;
		
		public function Axis(plot:RadarPolarPlot) {
			this.plot = plot;
			this.center = new Point();
		}
		
		
		
		public function initialize():void {	}
		
		public function setContainers(gridInterlacesFillingContainer:Shape, gridLinesContainer:Shape, 
			tickmarksContainer:Sprite, labelsContainer:Sprite, axisLinesContainer:Shape):void {
				
			this.gridInterlacesFillingContainer = gridInterlacesFillingContainer;
			this.gridLinesContainer = gridLinesContainer;
			this.tickmarksContainer = tickmarksContainer;
			this.labelsContainer = labelsContainer;
			this.axisLinesContainer = axisLinesContainer;
		}
		
		public function calcBounds(bounds:Rectangle):void {
			this.bounds = bounds.clone();
			var thickness:Number = this.plot.getPlotOuterThickness();
			bounds.x += thickness;
			bounds.y += thickness;
			bounds.width -= thickness + thickness;
			bounds.height -= thickness + thickness; 
			//this.bounds.x = 0;
			//this.bounds.y = 0;
			this.calcCenter();
		}
		
		private function calcCenter():void {
			this.center.x = this.bounds.x + this.bounds.width / 2;
			this.center.y = this.bounds.y + this.bounds.height / 2;
		}
		
		//---------------------------------
		// DRAWING
		//---------------------------------
		
		protected function getMaxMajorIntervalValue():int { return this.scale.majorIntervalsCount - 1; }
		
		protected function showLabel(index:int):Boolean {
			if (this.labels == null || !this.labels.enabled) return false;
			if (index == 0) return this.labels.showFirst;
			return true;
		}
		
		public function draw():void {
			this.drawLine();
			
			if (this.majorGrid == null && this.minorGrid == null && this.majorTickmark == null && this.minorTickmark == null && this.labels == null)
				return;
			var majIntervals:uint = this.getMaxMajorIntervalValue();
			var minIntervals:uint = this.scale.minorIntervalsCount - 1;
			//if ((this.minorGrid && this.majorGrid) || (this.minorTickmark && this.majorTickmark))
			//	minIntervals -= 1; 
			var majorOdd:Boolean = true;
			
			for (var i:uint = 0; i<=majIntervals; i++){
				
				var majorInterval:Number = this.scale.getMajorIntervalValue(i);
				var pixValue:Number = this.scale.localTransform(majorInterval);
				var pixValueNext:Number = this.scale.localTransform(this.scale.getMajorIntervalValue(i+1));
				
				
				if (i != majIntervals && (this.minorGrid != null || this.minorTickmark != null)){
					var minorOdd:Boolean = true;
					for (var j:uint = 0; j <= minIntervals; j++){
						
						var minorPixValue:Number = this.scale.localTransform(this.scale.getMinorIntervalValue(majorInterval, j));
						var minorPixValueNext:Number = this.scale.localTransform(this.scale.getMinorIntervalValue(majorInterval, j+1));
						
						if (this.minorGrid != null && this.minorGrid.enabled){
							minorOdd = !minorOdd;
							this.drawGridLine(this.minorGrid, minorPixValue, minorPixValueNext, minorOdd, 
								(j == minIntervals && this.minorGrid.drawLastLine) || (j != minIntervals),
								pixValue, pixValueNext);
						}
						
						if (this.minorTickmark != null && this.minorTickmark.enabled)
							this.drawTickmark(minorPixValue, this.minorTickmark);
					}
				}
				
				if (this.majorGrid != null && this.majorGrid.enabled){
					majorOdd = !majorOdd;
					var b:Boolean = (i == majIntervals && this.majorGrid.drawLastLine) || (i != majIntervals);
					if (this.scale.getMajorIntervalValue(i+1) > this.scale.maximum)
						pixValueNext = pixValue;
					this.drawGridLine(this.majorGrid, pixValue, pixValueNext, majorOdd, b);
				}
				
				if (this.majorTickmark != null && this.majorTickmark.enabled)
					this.drawTickmark(pixValue, this.majorTickmark);
				
				if (this.showLabel(i))
					this.drawLabel(i);
					
			}
			
			if (this.labels != null && this.labels.enabled && this.labels.showLast && this.execDrawLastLabelHack())
				this.drawLabel(majIntervals);
		}
		
		protected function execDrawLastLabelHack():Boolean { return true; }
		
		private function drawLabel(majIntervalIndex:Number):void {
			var info:TextElementInformation = this.getLabelInfo(majIntervalIndex);
			var pos:Point = new Point();
			var pixValue:Number = this.scale.localTransform(this.scale.getMajorIntervalValue(majIntervalIndex));
			var leftTopCornerPos:Point = new Point();
			this.setLabelPosition(info, pos, pixValue, leftTopCornerPos);
			if (this.labels.allowOverlap || !this.checkOverlap(info, pos, pixValue, leftTopCornerPos))
				this.doLabelDrawing(pos.x, pos.y, info, pixValue);
		}
		
		private var lastNonRotatedLines:Array;
		private var lastRotatedBounds:Rectangle;
		protected function checkOverlap(info:TextElementInformation, pos:Point, pixValue:Number, leftTopCornerPos:Point):Boolean {
			var labelRotationAngle:Number = this.getLabelRotation(pixValue);
			var corners:Array = this.getAllPoints(info.nonRotatedBounds, labelRotationAngle, new Point());
			for (var i:int = 0; i < 4; i++){
				corners[i].x += leftTopCornerPos.x;
				corners[i].y += leftTopCornerPos.y;
				
			}
			var bounds:Rectangle = new Rectangle(pos.x, pos.y, info.rotatedBounds.width, info.rotatedBounds.height);
			
			if (this.lastRotatedBounds != null && this.lastNonRotatedLines != null && 
				this.lastRotatedBounds.intersects(bounds) && this.intersects(this.lastNonRotatedLines, corners))
				return true;
			else {
				this.lastRotatedBounds = bounds;
				this.lastNonRotatedLines = [
					LineEquation.getLineFrom2Points(corners[0], corners[1]),
					LineEquation.getLineFrom2Points(corners[1], corners[2]),
					LineEquation.getLineFrom2Points(corners[2], corners[3]),
					LineEquation.getLineFrom2Points(corners[3], corners[0])
				];
				return false; 
			}
		}
		
		protected function getAllPoints(nonRotatedBounds:Rectangle, rotationAngle:Number, anchor:Point):Array{
			var res:Array = new Array(4);
			var dx:Number = nonRotatedBounds.width;
			var dy:Number = nonRotatedBounds.height;
			res[0] = new Point(anchor.x, anchor.y);
			res[1] = new Point(rotationAngle, dx);
			res[2] = new Point(rotationAngle + Math.atan2(dy, dx) * 180 / Math.PI, Math.sqrt(dx*dx + dy*dy));
			res[3] = new Point(rotationAngle + 90, dy);
			for (var i:int = 1; i < 4; i++){
				var r:Number = res[i].y;
				var a:Number = res[i].x;
				res[i].x = DrawingUtils.getPointX(r, a) + anchor.x;
				res[i].y = DrawingUtils.getPointY(r, a) + anchor.y;
			}
			return res;
		}
		
		private function intersects(lines:Array, rc2:Array):Boolean {
			var line1:LineEquation;
			var line2:LineEquation;
			var lookingForLeft:Boolean;
			var fail:Boolean;
			for (var i:int = 0; i < 2; i++){
				line1 = lines[i];
				line2 = lines[i+2];
				var b1:Boolean = line1.isLeftPoint(rc2[0]);
				var b2:Boolean = line2.isLeftPoint(rc2[0]);
				var b:Boolean = b1;
				fail = false;
				for (var j:int = 1; j < 4; j++){
					if (((b1 && b2) || (!b1 && !b2)) && b == b1){
						b1 = line1.isLeftPoint(rc2[j]);
						b2 = line2.isLeftPoint(rc2[j]);
					} else {
						fail = true;
						break;
					}
				}
				if (!fail)
					return false;
			}
			return fail;
		}
		
		/**
		 * points order:
		 * 0(0)---1---2(1)
		 * |             |
		 * 7             3
		 * |             |
		 * 6(3)---5---4(2)
		*/
		protected function getNearestPoint(line:LineEquation, points:Array, sin:Number, cos:Number):Point {
			var findMax:Boolean;
			if (sin == 0)
				findMax = (cos > 0);
			else 
				findMax = (sin < 0);
			var nearest:Array = [];
			var foundDistance:Number; 
			if (findMax)
				foundDistance = -Number.MAX_VALUE;
			else
				foundDistance = Number.MAX_VALUE;
			for (var i:int = 0; i < 4; i++){
				var distance:Number = line.getDistance(points[i]);
				if (Math.abs(distance - foundDistance) < 1){
					nearest.push(i);
				} else if ((findMax && distance > foundDistance) || ((!findMax) && distance < foundDistance)){
					nearest = [i];
					foundDistance = distance;
				}
			}
			if (nearest.length == 0)
				return null;
			else if (nearest.length == 1)
				return points[nearest[0]];
			else 
				return this.getCenter(points[nearest[0]], points[nearest[1]]);
		}
		
		private function getCenter(p1:Point, p2:Point):Point{
			return new Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
		}
		
		protected function getLabelRotation(pixValue:Number):Number { return this.labels.rotation; }
		
		protected function doLabelDrawing(x:Number, y:Number, info:TextElementInformation, pixValue:Number):void { }
		
		//------------------------------------
		// UTILS
		//------------------------------------
		
		protected function countLineBounds(x1:Number, y1:Number, x2:Number, y2:Number, thickness:Number, cos:Number, sin:Number):Rectangle {
			var dx:Number = x2 - x1;
			var dy:Number = y2 - y1;
			var xThicknessHalf:Number = Math.abs(thickness * cos / 2);
			var yThicknessHalf:Number = Math.abs(thickness * sin / 2);
			return new Rectangle(Math.min(x1, x2) - xThicknessHalf, Math.min(y1, y2) - yThicknessHalf, 
				Math.abs(x2 - x1) + xThicknessHalf + xThicknessHalf, Math.abs(y2 - y1) + yThicknessHalf + yThicknessHalf);
		}
		
		//------------------------------------
		// ABSTRACT
		//------------------------------------
		
		protected function drawLine():void { }
		protected function drawGridLine(gridLine:RadarPolarGrid, pixValue:Number, nextValue:Number, odd:Boolean, drawLine:Boolean, majPixValue:Number = 0, majPixValueNext:Number = 0):void {	}
		protected function drawTickmark(pixvalue:Number, tickmark:Tickmark):void { }
		protected function setLabelPosition(info:TextElementInformation, pos:Point, pixPos:Number, leftTopCornerPos:Point):void { }
		protected function getLabelInfo(pixValue:Number):TextElementInformation { return null; }
		
		//------------------------------------
		// GETTING LABEL INFO
		//------------------------------------
		
		public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return token == '%Value' ? scaleValue : '';
		}
		
		public function isDateTimeToken(token:String):Boolean {
			return false;
			//if (token.indexOf('%DataPlot') == 0)
			//	return false;
			//if (!this.isDateTimeAxis()) return false;
			//return (token == "%Value");
		}
		
		//------------------------------------
		// DESERIALIZATION
		//------------------------------------
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			
			if (this.enabled && SerializerBase.isEnabled(data.line[0])){
				this.line = new Stroke();
				this.line.deserialize(data.line[0], null);
			}
			if (this.enabled && SerializerBase.isEnabled(data.major_grid[0])){
				this.majorGrid = new RadarPolarGrid();
				this.majorGrid.isMinor = false;
				this.majorGrid.deserialize(data.major_grid[0], null);
			}
			if (this.enabled && SerializerBase.isEnabled(data.major_tickmark[0])){
				this.majorTickmark = new Tickmark();
				this.majorTickmark.deserialize(data.major_tickmark[0], null);
			}
			if (this.enabled && SerializerBase.isEnabled(data.minor_grid[0])){
				this.minorGrid = new RadarPolarGrid();
				this.minorGrid.isMinor = true;
				this.minorGrid.deserialize(data.minor_grid[0], null);
			}
			if (this.enabled && SerializerBase.isEnabled(data.minor_tickmark[0])){
				this.minorTickmark = new Tickmark();
				this.minorTickmark.deserialize(data.minor_tickmark[0], null);
			}
			this.scale = this.createScale(data.scale[0] != null ? data.scale[0].@type : null);
			if (SerializerBase.isEnabled(data.scale[0]))
				this.scale.deserialize(data.scale[0]);
			if (this.enabled && SerializerBase.isEnabled(data.labels[0])){
				this.labels = this.createAxisLabels();
				this.labels.deserialize(data.labels[0], resources);
			}
		}
		
		protected function createAxisLabels():AxisLabels { return new AxisLabels(this); }
		
		public function onAfterDataDeserialize():void {}
		
		protected function createScale(scaleType:*):BaseScale {
			return new LinearScale();
		} 
		
		public function serialize(res:Object=null):Object { return null; }
	}
}