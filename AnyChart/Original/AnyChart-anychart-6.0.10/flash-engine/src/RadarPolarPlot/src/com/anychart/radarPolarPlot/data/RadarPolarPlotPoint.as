package com.anychart.radarPolarPlot.data {
	import com.anychart.radarPolarPlot.PolarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.CategorizedAxis;
	import com.anychart.radarPolarPlot.axes.ValueAxis;
	import com.anychart.radarPolarPlot.axes.categorization.Category;
	import com.anychart.radarPolarPlot.axes.categorization.ClusterSettings;
	import com.anychart.radarPolarPlot.elements.markerElement.RadarPolarMarkerElement;
	import com.anychart.scales.ScaleMode;
	import com.anychart.seriesPlot.data.BaseSingleValuePoint;

	public class RadarPolarPlotPoint extends BaseSingleValuePoint {
		public var x:Number;
		override public function deserialize(data:XML):void {
			this.checkMissing(data);
			super.deserialize(data);
			this.deserializeArgument(data);
			this.deserializeValue(data);
			RadarPolarPlot(this.plot).checkPoint(this);
			this.checkYValue();
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == "%XValue") return this.x;
			return super.getPointTokenValue(token);
		}
		
		public var stackValue:Number;
		
		protected function checkYValue():void {
			RadarPolarPlot(this.plot).checkYValue(this.stackValue);
		}
		
		override protected function createElementsSprites():void {
			if (this.marker) RadarPolarMarkerElement(this.marker).setContainer(this.container);
			super.createElementsSprites();
		}
		
		protected var category:Category;
		private var clusterSettings:ClusterSettings;
		
		private function deserializeArgument(data:XML):void {
			
			if (this.plot is PolarPlot) {
				this.x = Number(RadarPolarPlot(this.plot).getXScale().deserializeValue(data.@x));
			}else {
				this.category = CategorizedAxis(RadarPolarPlot(this.plot).getXAxis()).getCategory(this.name);
				this.x = this.category.index;
				this.clusterSettings = this.category.clusterSettings;
				this.index = this.paletteIndex = this.category.index;
			}
			/*
			this.dataCategory = this.category = CategorizedAxis(argumentAxis).getCategory(this.name);
					this.x = this.category.index;
					this.clusterSettings = axesSeries.clusterSettings;
					this.paletteIndex = this.category.index;
					this.index = this.category.index;*/
		}
		
		override protected function deserializeYValue(data:XML):void { }
		
		protected function deserializeValue(data:XML):void {
			this.y = Number(RadarPolarPlot(this.plot).getYScale().deserializeValue(data.@y));
			if (!this.isMissing && this.category) {
				
				var axis:ValueAxis = RadarPolarPlot(this.plot).yAxis; 
				
				if (axis.scale.mode == ScaleMode.STACKED) {
					this.stackValue = this.category.getStackSettings(this.series.type, false).addPoint(this, this.y);
				}else if (axis.scale.mode == ScaleMode.PERCENT_STACKED) {
					this.stackValue = this.category.getStackSettings(this.series.type, true).addPoint(this, this.y);
				}else {
					this.stackValue = this.y;
				}
			}else {
				this.stackValue = this.y;
			}
		}
		
		protected function checkMissing(data:XML):void {
			this.isMissing = (data.@y == undefined) || (isNaN(Number(data.@y))) || (String(data.@y).replace(/^\s+|\s+$/g, "")=="");
		}
				
		//-----------------------------------
		// CATEGORIZATION
		//-----------------------------------
		
		public function checkCategory(categoryTokens:Object):void {
			
		}
		
		public var previousStackPoint:RadarPolarPlotPoint;
		public function setStackMax(value:Number):void {
			if (value != 0)
				this.stackValue *= 100/value;
			else
				this.stackValue = 0;
		}

	}
}