package com.anychart.mapPlot.areaSeries {
	import com.anychart.data.SeriesType;
	import com.anychart.mapPlot.multiPointSeries.TemplateUtils;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	public class MapAreaGlobalSeriesSettings extends GlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:AreaSeries = new AreaSeries();
			series.type = SeriesType.AREA;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "area_series";
		}
		
		override public function getStyleNodeName():String {
			return "area_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
		
	}
}