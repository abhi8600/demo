package com.anychart.mapPlot.connectorSeries {
	import com.anychart.mapPlot.multiPointSeries.MultiSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class ConnectorSeries extends MultiSeries {
		
		override public function createPoint():BasePoint {
			return new ConnectorPoint();
		}
	}
}