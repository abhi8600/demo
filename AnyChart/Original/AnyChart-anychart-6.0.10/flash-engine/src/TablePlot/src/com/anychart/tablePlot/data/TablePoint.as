package com.anychart.tablePlot.data {
	import com.anychart.seriesPlot.data.BaseSingleValuePoint;
	import com.anychart.tablePlot.ITableCell;
	import com.anychart.tablePlot.style.TableCellStyle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;

	internal final class TablePoint extends BaseSingleValuePoint 
		implements ITableCell {

		private var info:TextElementInformation;
		
		//-------------------------------------------------
		//				ITableCell
		//-------------------------------------------------
		public function getMinBounds():Rectangle {
			return this.info.rotatedBounds;
		}
		
		//-------------------------------------------------
		//				Drawing
		//-------------------------------------------------
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.drawSelf();
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawSelf();
		}
		
		private static const E_WIDTH:uint = 10;
		
		private function drawSelf():void {
			var s:TableCellStyle = TableCellStyle(this.styleState);
			var g:Graphics = this.container.graphics;
			if (s.fill != null) 
				s.fill.begin(g, this.bounds, this.color);
			if (s.stroke != null)
				s.stroke.apply(g, this.bounds, this.color);
			g.drawRoundRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height, E_WIDTH);
			g.endFill();
			g.lineStyle();
			if (s.hatchFill != null){
				s.hatchFill.beginFill(g, this.hatchType, this.color);
				g.drawRoundRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height, E_WIDTH);
				g.endFill();
			}
			s.label.draw(g, this.bounds.x, this.bounds.y, this.info, this.color, this.hatchType);
		}
		
		override public function initialize():void {
			super.initialize();
			
			this.info = TableCellStyle(this.styleState).label.createInformation();
			
			var s:TableCellStyle = TableCellStyle(this.styleState);
			if (s.label.isDynamicText)
				this.info.formattedText = s.label.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken);
			else
				this.info.formattedText = s.label.text;
			s.label.getBounds(this.info);
		} 
	}
}