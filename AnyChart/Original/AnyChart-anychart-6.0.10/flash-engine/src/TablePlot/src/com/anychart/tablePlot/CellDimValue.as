package com.anychart.tablePlot {
	internal final class CellDimValue {
		public var value:Number;
		public var num:uint;
		public function CellDimValue(value:Number, num:uint) {
			this.value = value;
			this.num = num;
		}
	}
}