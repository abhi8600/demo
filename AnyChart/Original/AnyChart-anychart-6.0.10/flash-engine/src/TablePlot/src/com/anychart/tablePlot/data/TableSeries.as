package com.anychart.tablePlot.data {
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSingleValueSeries;

	internal final class TableSeries extends BaseSingleValueSeries {
		public function TableSeries() {
			super();
		}
		
		override public function createPoint():BasePoint {
			return new TablePoint();
		}
	}
}	