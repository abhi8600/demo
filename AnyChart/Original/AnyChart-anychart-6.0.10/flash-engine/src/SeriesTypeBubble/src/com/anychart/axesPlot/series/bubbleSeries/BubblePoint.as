package com.anychart.axesPlot.series.bubbleSeries {
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.programmaticStyles.ICircleAquaStyleContainer;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class BubblePoint extends StackablePoint implements ICircleAquaStyleContainer {
		
		public var bubbleSize:Number;
		public var pixelRadius:Number;
		public var centerX:Number;
		public var centerY:Number;
		public var dRadius:Number;
		
		override public function get sortedOverlayValue():Number { return this.bubbleSize; }
		
		override public function updateValue(newValue:Object):void {
			super.updateValue(newValue);
			if (newValue.y != undefined) this.stackValue = this.deserializeValueFromXML(newValue.y);
			if (newValue.size != undefined) this.bubbleSize = SerializerBase.getNumber(newValue.size);
			
			if (isNaN(this.bubbleSize)) 
				this.isMissing = true;
		}
		
		override public function getCurrentValuesAsObject():Object {
			var res:Object = super.getCurrentValuesAsObject();
			res.size = this.bubbleSize;
			return res;
		}
		
		override public function canAnimate(field:String):Boolean {
			return super.canAnimate(field) || field == "size";
		}
		
		override public function checkScaleValue():void {
			
			if (!this.isMissing) {
				var settings:BubbleGlobalSeriesSettings =  BubbleGlobalSeriesSettings(this.global);
				if (this.bubbleSize > settings.maximumValueBubbleSize)
					settings.maximumValueBubbleSize = this.bubbleSize;
				if (this.bubbleSize < settings.minimumValueBubbleSize)
					settings.minimumValueBubbleSize = this.bubbleSize;
			}
			super.checkScaleValue();
		}
		
		override protected function deserializeValue(data:XML):void {
			super.deserializeValue(data);
			this.bubbleSize = SerializerBase.getNumber(data.@size);
			if (isNaN(this.bubbleSize)) {
				this.isMissing = true;
			}else {
				var settings:BubbleGlobalSeriesSettings =  BubbleGlobalSeriesSettings(this.global);
				if (this.bubbleSize > settings.maximumValueBubbleSize)
					settings.maximumValueBubbleSize = this.bubbleSize;
				if (this.bubbleSize < settings.minimumValueBubbleSize)
					settings.minimumValueBubbleSize = this.bubbleSize;
			}
		}
		
		override protected function checkMissing(data:XML):void {
			super.checkMissing(data);
			
			this.isMissing = this.isMissing || data.@size == undefined || isNaN(SerializerBase.getNumber(data.@size));
			if (!BubbleGlobalSeriesSettings(this.global).displayNegative && SerializerBase.getNumber(data.@size) < 0)
				this.isMissing = true;
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%BubbleSize') return this.bubbleSize;
			if (token == '%BubbleSizePercentOfSeries') {
				if (this.cashedTokens['BubbleSizePercentOfSeries'] == null) {
					this.cashedTokens['BubbleSizePercentOfSeries'] = this.bubbleSize/this.series.getSeriesTokenValue("%SeriesBubbleSizeSum")*100;
				}
				return this.cashedTokens['BubbleSizePercentOfSeries'];
			}
			if (token == '%BubbleSizePercentOfTotal') {
				if (this.cashedTokens['BubbleSizePercentOfTotal'] == null) {
					this.cashedTokens['BubbleSizePercentOfTotal'] = this.bubbleSize/this.plot.getTokenValue('%DataPlotBubbleSizeSum')*100;
				}
				return this.cashedTokens['BubbleSizePercentOfTotal'];
			}
			if (token == '%BubbleSizePercentOfCategory') {
				if (this.dataCategory == null) return '';
				if (this.cashedTokens['BubbleSizePercentOfCategory'] == null) {
					this.cashedTokens['BubbleSizePercentOfCategory'] = this.bubbleSize/this.dataCategory.getCategoryTokenValue('%CategoryBubbleSizeSum')*100;
				}
				return this.cashedTokens['BubbleSizePercentOfCategory'];
			}
			return super.getPointTokenValue(token);
		}
		
		private function get stateStyle():BackgroundBaseStyleState {
			return BackgroundBaseStyleState(this.styleState);
		}
		
		private static var centerPt:Point = new Point();
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			AxesPlotSeries(this.series).argumentAxis.transform(this, this.x, centerPt);
			AxesPlotSeries(this.series).valueAxis.transform(this, this.stackValue, centerPt);
			
			if (this.z) this.z.transform(centerPt);
			
			this.centerX = centerPt.x;
			this.centerY = centerPt.y;
			
			this.pixelRadius = BubbleGlobalSeriesSettings(this.global).getBubbleSize(this.bubbleSize) / 2;
			
			this.dRadius = 0;
			if (this.stateStyle.stroke != null && this.stateStyle.stroke.enabled) {
				this.dRadius = this.stateStyle.stroke.thickness/2;
			}
			this.bounds.x = this.centerX - this.pixelRadius;
			this.bounds.y = this.centerY - this.pixelRadius;
			this.bounds.width = this.pixelRadius*2;
			this.bounds.height = this.pixelRadius*2;
			
			this.drawBubble(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawBubble(this.container.graphics);
		} 
		
		private function drawBubble(g:Graphics):void {
			
			if (this.styleState.programmaticStyle != null) {
				this.styleState.programmaticStyle.draw(this);
			}else {
				if (this.stateStyle.fill != null && this.stateStyle.fill.enabled) {
					this.bounds.x += this.dRadius;
					this.bounds.y += this.dRadius;
					this.bounds.width -= this.dRadius*2;
					this.bounds.height -= this.dRadius*2;
					
					this.stateStyle.fill.begin(g,this.bounds,this.color);
					this.drawShape();
					g.endFill();
					
					this.bounds.x -= this.dRadius;
					this.bounds.y -= this.dRadius;
					this.bounds.width += this.dRadius*2;
					this.bounds.height += this.dRadius*2;
				}
				
				if (this.stateStyle.hatchFill != null && this.stateStyle.hatchFill.enabled) {
					this.drawShape();
					this.stateStyle.hatchFill.beginFill(g,this.hatchType,this.color);
				}
				
				if (this.stateStyle.stroke != null && this.stateStyle.stroke.enabled) {
					this.stateStyle.stroke.apply(g, this.bounds,this.color);
					this.drawShape();
					g.lineStyle();
				}
			}
			if (this.hasPersonalContainer) {
				if (this.stateStyle.effects != null && this.stateStyle.effects.enabled) {
					this.container.filters = this.stateStyle.effects.list;
				}else {
					this.container.filters = null;
				}				
			}
		}
		
		public function drawShape():void {
			this.container.graphics.drawCircle(this.centerX,this.centerY,this.pixelRadius - this.dRadius);
		}
		
		public function getBounds():Rectangle { return this.bounds; }
		public function getActualColor():uint { return this.color; }
		public function getActualHatchType():uint { return this.hatchType; }
		public function getGraphics():Graphics { return this.container.graphics; }
		public function getCurrentState():BackgroundBaseStyleState { return BackgroundBaseStyleState(this.styleState); }
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.BubbleSize = this.bubbleSize;
			res.BubbleSizePercentOfSeries = this.getPointTokenValue("%BubbleSizePercentOfSeries");
			res.BubbleSizePercentOfTotal = this.getPointTokenValue("%BubbleSizePercentOfTotal");
			res.BubbleSizePercentOfCategory = this.getPointTokenValue("%BubbleSizePercentOfCategory");
			return res;
		}
	}
}