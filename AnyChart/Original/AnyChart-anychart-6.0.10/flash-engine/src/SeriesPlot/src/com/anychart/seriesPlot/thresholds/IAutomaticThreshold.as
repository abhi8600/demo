package com.anychart.seriesPlot.thresholds {
	import com.anychart.palettes.ColorPalette;
	
	public interface IAutomaticThreshold extends IThreshold {
		function initialize():void;
		function set palette(value:ColorPalette):void;
		function getRangeCount():int;
	}
}