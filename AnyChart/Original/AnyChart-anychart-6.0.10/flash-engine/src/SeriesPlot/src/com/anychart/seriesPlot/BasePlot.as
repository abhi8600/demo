package com.anychart.seriesPlot
{
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.ControlsFactory;
	import com.anychart.controls.PlotControlsList;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.controls.legend.IconSettings;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.selector.ISelectionProvider;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.seriesPlot.thresholds.ThresholdsList;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.visual.background.RectBackground;
	import com.anychart.visual.background.RectBackgroundSprite;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	public class BasePlot extends RectBackgroundSprite implements IPlot, IMainElementsContainer, ISelectionProvider
	{
		
		public static const IS_ORACLE:Boolean = true;
		
		//-----------------------------------------------------
		//				Controls
		//-----------------------------------------------------
		private static var initControls:Boolean = includeControls();
		private static function includeControls():Boolean {
			import com.anychart.seriesPlot.controls.colorMap.HorizontalColorMapControl;
			import com.anychart.seriesPlot.controls.colorMap.VerticalColorMapControl;
			ControlsFactory.register(HorizontalColorMapControl);
			ControlsFactory.register(VerticalColorMapControl);
			return true;
		}
		
		//-----------------------------------------------------
		//				CONTAINERS
		//-----------------------------------------------------
		
		public var scrollableContainer:Sprite;
		public var draggableContainer:Sprite;
		protected var scrollableMain:Sprite;
		public var afterDataContainer:Sprite;
		public var dataContainer:Sprite;
		public var beforeDataContainer:Sprite;
		
		private var _markersContainer:Sprite;
		private var _labelsContainer:Sprite;
		private var _tooltipsContainer:Sprite;
		
		public function get markersContainer():Sprite { return this._markersContainer; }
		public function getLabelsContainer(label:ItemLabelElement):Sprite { return this._labelsContainer; }
		public function get tooltipsContainer():DisplayObjectContainer { return this._tooltipsContainer; }
		
		private var _tooltipsSprites:Array; 
		public function getTooltipSprite(index:uint):Sprite {
			 if (this._tooltipsSprites.length <= index)
			 	this._tooltipsSprites.length = index + 1;
			 if (this._tooltipsSprites[index] == null) {
			 	this._tooltipsSprites[index] = new Sprite();
			 	this._tooltipsContainer.addChild(this._tooltipsSprites[index]);
			 }
			 return this._tooltipsSprites[index];
		}
		
		public function get root():IAnyChart { return this.chart; }
		
		//-----------------------------------------------------
		//				TEMPLATES
		//-----------------------------------------------------
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		//-----------------------------------------------------
		//				Custom fieldss
		//-----------------------------------------------------
		
		protected var customAttributes:Object;
		
		//-----------------------------------------------------
		//				Constructor
		//-----------------------------------------------------
		
		public function BasePlot() {
			
			this.cashedTokens = {};
			this.cashedTokens['PointCount'] = 0;
			
			this.scrollableContainer = new Sprite();
			this.draggableContainer = new Sprite();
			this.scrollableMain = new Sprite();
			this.dataContainer = new Sprite();
			this._labelsContainer = new Sprite();
			this._markersContainer = new Sprite();
			this.afterDataContainer = new Sprite();
			this.beforeDataContainer = new Sprite();
			this._tooltipsContainer = new Sprite();
			this._tooltipsContainer.mouseChildren = false;
			this._tooltipsContainer.mouseEnabled = false;
			
			this._tooltipsSprites = [];
		}
		
		public function destroy():void {
			this.chart = null;
			this.chartContainer = null;
			if (this.sprite) {
				this.destroyDisplayObjectContainer(this.sprite);
				this.sprite = null;
			}
			if (this.selectedPoint != null) {
				this.selectedPoint.destroy();
				this.selectedPoint = null;
			}
			if (this.selectedPoints != null) {
				for each (var pt:BasePoint in this.selectedPoints) {
					pt.destroy();
				}
				this.selectedPoints = null;
			}
			if (this._tooltipsSprites) {
				for each (var s:Sprite in this._tooltipsSprites)
					this.destroyDisplayObjectContainer(s);
			}
		}
		
		private function destroyDisplayObjectContainer(container:DisplayObjectContainer):void {
			while (container.numChildren > 0) {
				try {
					var obj:DisplayObject = container.getChildAt(0);
					if (obj is DisplayObjectContainer) this.destroyDisplayObjectContainer(DisplayObjectContainer(obj));
					container.removeChildAt(0);
					obj = null;
				}catch (e:Error) {}
			}
			if (container.parent) container.parent.removeChild(container);
			container = null;
		}
		
		//-----------------------------------------------------
		//				IPlot
		//-----------------------------------------------------
		
		public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void {}
		
		public function startPointsUpdate():void {}
		
		public function createIconSettings():IconSettings { return null; }
		
		public function getLegendData(item:XML, container:ILegendItemsContainer):void {}
		
		protected var animationManager:AnimationManager;
		
		public var enableAnimation:Boolean;
		
		public final function updateData(...params:*):void {}
		public final function setAnimationManager(manager:AnimationManager):void {
			this.animationManager = manager;
		}
		
		protected var chart:IAnyChart;
		protected var controls:PlotControlsList;
		protected var chartContainer:IChart;
		protected var view:Object;
		public final function setContainer(chart:IChart):void { this.chartContainer = chart; }
		public final function setBase(chart:IAnyChart):void {
			this.chart = chart;
		}
		public final function setControlsList(controls:PlotControlsList):void {
			this.controls = controls;
		}
		
		public final function setView(view:Object):void { this.view = view; }
		
		protected var plotType:String;
		
		public function setPlotType(value:String):void {
			this.plotType = value;
		}
		
		public final function getBounds():Rectangle {
			return this.bounds;
		}
		
		protected static var templatesManager:BaseTemplatesManager;
		
		public function getTemplatesManager():BaseTemplatesManager {
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			return templatesManager;
		}
		
		public final function setHover():void {}
		public final function setNormal():void {}
		
		public function drawDataIcon(iconSettings:IconSettings, src:ILegendItem, g:Graphics, dx:Number, dy:Number):void {}
		
		public function checkNoData(data:XML):Boolean {
			return false;
		}
		
		public function setXZoom(settings:Object):void {}
		public function setYZoom(settings:Object):void {}
		public function setZoom(xZoomSettings:Object, yZoomSettings:Object):void {}
		public function scrollXTo(xValue:String):void {}
		public function scrollYTo(yValue:String):void {}
		public function scrollTo(xValue:String, yValue:String):void {}
		public function getXScrollInfo():Object { return null; }
		public function getYScrollInfo():Object { return null; }
		
		//-----------------------------------------------------
		//				INTERACTIVITY
		//-----------------------------------------------------
		
		protected var selectionManager:MouseSelectionManager;
		
		protected var selectedPoint:BasePoint;
		protected var selectedPoints:Array;
		public var allowMultipleSelect:Boolean;
		
		public function setSelectedPoint(point:BasePoint):void {
			if (this.allowMultipleSelect) {
				if (this.selectedPoints.indexOf(point) == -1)
					this.selectedPoints.push(point);
			}else {
				this.selectedPoint = point;
			}
		}
		
		public function deselectPoint():void{
			this.selectedPoint = null;
		}
		
		public function getSelectedPoint():BasePoint {
			return (this.allowMultipleSelect) ? null : this.selectedPoint;
		}
		
		//-----------------------------------------------------
		//				Deserialization
		//-----------------------------------------------------
		
		protected function onAfterDeserializeData(data:XML, resources:ResourcesLoader):void {}
		protected function deserializeSeriesArguments(series:BaseSeries, seriesNode:XML):void {}
		
		protected var cashedData:XML;
		
		public function setCashedData(data:XML):void {
			this.cashedData = data.copy();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data != null) {
				
				this.thresholdsList = new ThresholdsList(data.thresholds[0] == null ? null : data.thresholds[0].threshold);
				
				if (data.chart_settings[0] != null)
					this.deserializeChartSettings(data.chart_settings[0], data.styles[0], resources);
				
				if (data.data_plot_settings[0] != null)
					this.deserializeDataPlotSettings(data.data_plot_settings[0], resources);
				
				this.deserializeData(data, resources);
				this.onAfterDeserializeData(data, resources);
			}
		}
		
		//<chart_settings />
		protected function deserializeChartSettings(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.data_plot_background[0] != null) {
				this.background = new RectBackground(); 
				var enabled:Boolean = true;
				if (data.data_plot_background[0].@enabled != undefined) {
					enabled = SerializerBase.getBoolean(data.data_plot_background[0].@enabled);
					data.data_plot_background[0].@enabled = "true";
				}
				this.background.deserialize(data.data_plot_background[0], resources);
				if (!enabled) {
					this.background.fill = null;
					this.background.hatchFill = null;
					this.background.border = null;
					this.background.effects = null;
					this.background.corners = null
				}
			}
		}
		
		//<data_plot_settings />
		protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {}
		
		public var thresholdsList:ThresholdsList;
		
		protected function deserializeData(data:XML, resources:ResourcesLoader):void {
			if (data == null) return;
			var xmlDataNode:XML = data.data[0];
			if (xmlDataNode != null && xmlDataNode.attributes[0] != null) {
				this.customAttributes = {};
				var attributesList:XMLList = xmlDataNode.attributes[0].attribute;
				var attributesCnt:int = attributesList.length();
				for (var i:int = 0;i<attributesCnt;i++) {
					var attr:XML = attributesList[i];
					if (attr.@name == undefined) continue;
					this.customAttributes[attr.@name] = SerializerBase.getCDATAString(attr);
				}
			}
		}
		
		//-----------------------------------------------------
		//				Formatting
		//-----------------------------------------------------
		
		protected var cashedTokens:Object;
		
		public function resetTokens():void {
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE; 
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = null;
			this.cashedTokens['MinYValuePointName'] = null;
			this.cashedTokens['MaxYValuePointSeriesName'] = null;
			this.cashedTokens['MinYValuePointSeriesName'] = null;
			this.cashedTokens['MaxYSumSeriesName'] = null;
			this.cashedTokens['MinYSumSeriesName'] = null;
			this.cashedTokens['BubbleSizeSum'] = 0;
			this.cashedTokens['BubbleSizeMax'] = -Number.MAX_VALUE;
			this.cashedTokens['BubbleSizeMin'] = Number.MAX_VALUE;
			this.cashedTokens['BubblePointCount'] = 0;
			this.cashedTokens['PointCount'] = 0;
		}
		
		public function getTokenValue(token:String):* {
			
			if (this.customAttributes != null) {
				var attrName:String = token.substr(1);
				if (this.customAttributes[attrName] != undefined)
					return this.customAttributes[attrName];
			}
			
			if (token == '%DataPlotPointCount') return this.cashedTokens['PointCount'];
			
			if (token == '%DataPlotYSum') return this.cashedTokens['YSum'];
			if (token == '%DataPlotYMax') return this.cashedTokens['YMax'];
			if (token == '%DataPlotYMin') return this.cashedTokens['YMin'];
			if (token == '%DataPlotYAverage') return this.cashedTokens['YSum']/this.cashedTokens['YBasedPointsCount'];
			
			if (token == '%DataPlotMaxYValuePointName') return this.cashedTokens['MaxYValuePointName'];
			if (token == '%DataPlotMinYValuePointName') return this.cashedTokens['MinYValuePointName'];
			if (token == '%DataPlotMaxYValuePointSeriesName') return this.cashedTokens['MaxYValuePointSeriesName'];
			if (token == '%DataPlotMinYValuePointSeriesName') return this.cashedTokens['MinYValuePointSeriesName'];
			if (token == '%DataPlotMaxYSumSeriesName') return this.cashedTokens['MaxYSumSeriesName'];
			if (token == '%DataPlotMinYSumSeriesName') return this.cashedTokens['MinYSumSeriesName'];
			
			if (token == '%DataPlotBubbleSizeSum') return this.cashedTokens['BubbleSizeSum'];
			if (token == '%DataPlotBubbleMaxSize') return this.cashedTokens['BubbleSizeMax'];
			if (token == '%DataPlotBubbleMinSize') return this.cashedTokens['BubbleSizeMin'];
			if (token == '%DataPlotBubbleSizeAverage') return this.cashedTokens['BubbleSizeSum']/this.cashedTokens['BubblePointCount'];
			
			return '';
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		//-----------------------------------------------------
		//				IResizable
		//-----------------------------------------------------

		public function preInitialize():void {}
		
		protected var initialized:Boolean = false;
		private var postInitialized:Boolean = false;
		
		private var bgSprite:Sprite;
		
		override protected function getDrawingTarget():Sprite {
			return this.bgSprite;
		}

		override public function initialize(bounds:Rectangle):DisplayObject {
			this.bgSprite = new Sprite();
			super.initialize(bounds);
			this.sprite.addChild(this.bgSprite);
			
			this.addBottomExtraContainers();
			
			this.draggableContainer.addChild(this.beforeDataContainer);
			this.draggableContainer.addChild(this.dataContainer);
			this.draggableContainer.addChild(this._markersContainer);
			this.draggableContainer.addChild(this._labelsContainer);
			this.draggableContainer.addChild(this.afterDataContainer);
			
			this.scrollableContainer.addChild(this.draggableContainer);
			
			this.scrollableMain.addChild(this.scrollableContainer);
			this.sprite.addChild(this.scrollableMain);
			
			this.setContainersPositions(bounds);
			
			if (this.selectionManager)
				this.selectionManager.init();
			
			this.initialized = true;
			
			return this.sprite;
		}
		
		protected function addBottomExtraContainers():void {}
		
		public function postInitialize():void {
			if (!this.initialized) return;
			if (this.postInitialized) return;
			this.postInitialized = true;
			
			if (this.selectedPoint != null && this.selectedPoint.settings.actions != null)
				this.selectedPoint.settings.actions.execute(this.selectedPoint);
			for each (var pt:BasePoint in this.selectedPoints) {
				if (pt.settings.actions != null)
					pt.settings.actions.execute(pt);
			}
				
		}
		
		override public function calculateResize(bounds:Rectangle):void {
			super.calculateResize(bounds);
			this.setContainersPositions(bounds);
		}
		
		override public function execResize():void {
			super.execResize();
			
			this.scrollableContainer.graphics.clear();
			this._markersContainer.graphics.clear();
			this._labelsContainer.graphics.clear();
			this._tooltipsContainer.graphics.clear();
		}
		
		protected function setContainersPositions(newBounds:Rectangle):void {
			this.scrollableContainer.x = newBounds.x;
			this.scrollableContainer.y = newBounds.y;
			this._tooltipsContainer.x = newBounds.x;
			this._tooltipsContainer.y = newBounds.y;
			
			this.setScrollRect(newBounds);
		}
		
		protected function setScrollRect(newBounds:Rectangle):void {
			var scrollRect:Rectangle = newBounds.clone();
			scrollRect.x = 0;
			scrollRect.y = 0;
			this.scrollableContainer.scrollRect = scrollRect;
		}
		
		public function setTooltipPosition(pos:Point):void { }
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		public function serialize(res:Object = null):Object {
			if (!res) res = {}; 
			
			if (this.customAttributes) {
				for (var attrName:String in this.customAttributes) {
					res[attrName] = this.customAttributes[attrName];
				}
			}
			
			res.YSum = this.cashedTokens['YSum'];
			res.YMax = this.cashedTokens['YMax'];
			res.YMin = this.cashedTokens['YMin'];
			res.YAverage = this.cashedTokens['YSum']/this.cashedTokens['YBasedPointsCount'];
			
			res.BubbleSizeSum = this.cashedTokens['BubbleSizeSum'];
			res.BubbleMaxSize = this.cashedTokens['BubbleSizeMax'];
			res.BubbleMinSize = this.cashedTokens['BubbleSizeMin'];
			res.BubbleSizeAverage = this.cashedTokens['BubbleSizeSum']/this.cashedTokens['BubblePointCount'];
			
			res.MaxYValuePointName = this.cashedTokens['MaxYValuePointName'];
			res.MaxYValuePointSeriesName = this.cashedTokens['MaxYValuePointSeriesName'];
			res.MinYValuePointName = this.cashedTokens['MinYValuePointName'];
			res.MinYValuePointSeriesName = this.cashedTokens['MinYValuePointSeriesName'];
			res.MaxYSumSeriesName = this.cashedTokens['MaxYSumSeriesName'];
			res.MinYSumSeriesName = this.cashedTokens['MinYSumSeriesName'];
			res.PointCount = this.cashedTokens['PointCount'];
			
			return res;
		}
		
		//---------------------------------------------------------------
		//				Value Change
		//---------------------------------------------------------------
		
		public final function setPlotCustomAttribute(attributeName:String, attributeValue:String):void {
			if (this.customAttributes == null)
				this.customAttributes = {};
			if (attributeName.charAt(0) == "%") attributeName = attributeName.substr(1);
			this.customAttributes[attributeName] = attributeValue;
			if (this.cashedData != null && this.cashedData.data[0] != null) {
				if (this.cashedData.data[0].attributes[0] == null)
					this.cashedData.data[0].attributes += <attributes />;
				if (this.cashedData.data[0].attributes[0].attribute.(@name == attributeName).length() == 0)
					this.cashedData.data[0].attributes[0].attribute += <attribute name={attributeName}>{attributeValue}</attribute>;
				else
					this.cashedData.data[0].attributes[0].attribute.(@name == attributeName)[0] = <attribute name={attributeName}>{attributeValue}</attribute>;
			}
		}
		
		public function addSeries(...seriesData):void {}
		public function removeSeries(sereisId:String):void {}
		public function addSeriesAt(index:uint, seriesData:String):void {}
		public function updateSeries(seriesId:String, seriesData:String):void {}
		public function showSeries(seriesId:String, isVisible:Boolean):void {}
		
		public function showAxisMarker(markerId:String, isVisible:Boolean):void {}
		
		public function addPoint(seriesId:String, ...pointXML):void {}
		public function removePoint(seriesId:String, pointId:String):void {}
		public function addPointAt(seriesId:String, pointIndex:uint, pointData:String):void {}
		public function updatePoint(seriesId:String, pointId:String, pointXML:String):void {}
		
		public function refresh():void {}
		public function clear():void {}
		
		public function highlightSeries(seriesId:String, highlighted:Boolean):void {}
		public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {}
		public function highlightCategory(categoryName:String, highlighted:Boolean):void {}
		public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {}
		
		// ----------------			SELECTION		---------------------
		// --------------------------------------------------------------
		
		public function get drawingContainer():Shape { return this.chartContainer["selectionDrawingContainer"]; }
		public function get selectionEventsProvider():Sprite { return this.scrollableContainer; }
		public function get extraSelectionEventsProvider():Sprite { return this.chartContainer["getMainSprite"](); }
		public function get lockSelectionEventsProvider():Sprite { return this.chartContainer["selectionEventsProviderAfterChart"]; }
		
		public function finalizePointsSelection(area:Rectangle):void { }
		
		public function selectPointsInArea(area:Rectangle):void {
		}
		
		public function getSelectionActualPointCoords():Point { 
			return new Point(this.scrollableContainer.mouseX, this.scrollableContainer.mouseY); 
		}
		
		public function getSelectionBasePointCoords():Point { 
			return new Point(this.scrollableContainer.mouseX, this.scrollableContainer.mouseY);
		}
		
		public function deselectSelectedPoints():void {
			
		}
		
		//}
	}
}