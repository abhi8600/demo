package com.anychart.seriesPlot.thresholds {
	
	internal final class OptimalThreshold extends CollectionBasedThreshold {
		
		override public function initialize():void {
			super.initialize();
			this.collectedPoints.sortOn("pointValue",Array.NUMERIC);
			if (this.collectedPoints.length == 0) {
				this.buildBoundsArray();
			}else {
				var rangeCount:uint = Math.min(this.rangeCount, this.collectedPoints.length);
				if (rangeCount <= 4) {
					this.rangeCount = Math.max(this.rangeCount, 1);
					this.buildBoundsArray();
				}else {
					if (rangeCount > (this.collectedPoints.length - 3))
					{
						this.rangeCount = this.collectedPoints.length;
						/*
						Эксперимент в трешхолде...
						пока активна старая версия...
						это не удалять!!!
						if (this.intervals.length < this.rangeCount)
							for (var ij:int = this.intervals.length ; ij < this.rangeCount ; ij ++)
								this.intervals[ij]=0;
						*/
					}
					if (rangeCount < 4) {
						this.rangeCount = Math.max(this.rangeCount, 1);
						this.buildBoundsArray();
					}else {
						var list:Array = new Array();
		                for (var i:int = 0; i < this.collectedPoints.length; i++)
		                    list.push(this.collectedPoints[i].pointValue);
		                
		                var numArray:Array = this.helper1(list, this.rangeCount);
		                var r:AutomaticThresholdRange = new AutomaticThresholdRange();
		                r.autoValue = this.autoValue;
		                var tmp1:Number = this.collectedPoints[0].pointValue;
		                r.start = this.doUnknownAction(tmp1, tmp1, true);
		                
		                tmp1 = this.collectedPoints[numArray[0]-1].pointValue;
		               	var tmp2:Number = this.collectedPoints[numArray[0]].pointValue;
		                
		                var obj2:Number = this.doUnknownAction(tmp1, tmp2, true);
		                r.end = obj2;
		                
		                this.intervals[0] = r;
		                
		                for (var j:int = 1;j<Math.min(this.rangeCount-1,numArray.length);j++) {
		                	r = new AutomaticThresholdRange();
		                	r.autoValue = this.autoValue;
		                	r.start = obj2;
		                	tmp1 = this.collectedPoints[numArray[j] - 1].pointValue;
		                	tmp2 = this.collectedPoints[numArray[j]].pointValue;
		                	obj2 = this.doUnknownAction(tmp1, tmp2, true);
		                	r.end = obj2;
		                	this.intervals[j] = r;
		                }
		             
						r = new AutomaticThresholdRange();
		                r.autoValue = this.autoValue;
		                r.start = obj2;
		                r.end = this.doUnknownAction(this.collectedPoints[this.collectedPoints.length-1].pointValue,this.collectedPoints[this.collectedPoints.length-1].pointValue,false);
						this.intervals[this.rangeCount-1] = r;
						
						/*
						Эксперимент в трешхолде...
						пока активна старая версия...
						это не удалять!!!
						if (numArray.length < this.rangeCount-1)
						{
							this.intervals[numArray.length] = r;
							for (var il:int = numArray.length; il < this.rangeCount-1 ; il ++)
								this.intervals.pop();
							this.rangeCount = this.intervals.length;
						}
						*/
					}
				}
			}
		}
		
		private function helper1(listPoints:Array,RangeCountVar:int):Array {
		    var numArray:Array=new Array();
		   	var numArray2:Array=new Array();
	   		for(var i:int=0;i<listPoints.length+1;i++)
	   		{	
	   			numArray.push(new Array(RangeCountVar+1));
		   		numArray2.push(new Array(RangeCountVar+1));
			}
		    for (i = 1; i <= RangeCountVar; i++) {
		    	numArray[1][i] = 1;
		    	numArray2[1][i] = 0;
		        for (var m:int = 2; m <= listPoints.length; m++)
		            numArray2[m][i] = Number.POSITIVE_INFINITY;
		    }
		    
		    var expression:Number = 0;
		    for (var j:int = 2; j <= listPoints.length; j++) {
		        var Summ:Number = 0;
		        var SummSqrt:Number = 0;
		        var count:Number = 0;
		        for (var n:int = 1; n <= j; n++) {
		            var indxR:int = j - n;
		            var point:Number = Number(listPoints[indxR]);
		            SummSqrt += point * point;
		            Summ += point;
		            count++;
		            expression = SummSqrt - ((Summ * Summ) / count);
		            var indx:int = indxR;
		            if (indx != 0) {
		                for (var ii:int = 2; ii <= RangeCountVar; ii++) {
		                    if (numArray2[j][ii] >= (expression + numArray2[indx][ii - 1])) {
		                        numArray[j][ii] = indxR+1;
		                        numArray2[j][ii] = expression + numArray2[indx][ii - 1];
		                    }
		                }
		            }
		        }
		        numArray[j][1] = 1;
		        numArray2[j][1] = expression;
		    }
		    
		    var list:Array = new Array();
		    
		    indx = listPoints.length;
		    for (var k:int = RangeCountVar; k >= 2; k--) {
		        indxR = (int(numArray[indx][k])) - 2;
		        if (indxR > 0) list.splice(0,0,indxR);
		        indx = Math.max((int(numArray[indx][k])) - 1, 0);
		    }
		    
		    list.push(listPoints.length - 1);
		    
		    return list;
		}
	}
}