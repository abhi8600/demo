package com.anychart.seriesPlot.thresholds {
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.formatters.IFormatable;
	import com.anychart.serialization.ISerializable;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public interface IThreshold extends ISerializable,IFormatable {
		function getName():String;
		function checkAfterDeserialize(pt:BasePoint):void;
		function checkBeforeDraw(pt:BasePoint):void;
		function isAutomatic():Boolean;
		function getData(container:ILegendItemsContainer):void;
		function reset():void;
	}
}