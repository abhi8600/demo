package com.anychart.seriesPlot.data {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	
	public class BaseSeries extends BaseDynamicElementsContainer implements ILegendTag,IObjectSerializable {
		[ArrayElementType("BasePoint")]
		public var points:Array;
		
		public var type:uint;
		public var needCallingOnBeforeDraw:Boolean = false;
		
		public var index:uint;
		
		public function addPointsToDrawing():Boolean {
			return true;
		}
		
		public function BaseSeries() {
			this.cashedTokens = {};
		}
		
		public function destroy():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				if (this.points[i] is BasePoint) {
					BasePoint(this.points[i]).destroy();
				}
			}
			this.points = null;
		}
		
		public function createPoint():BasePoint { return null; }
		public function onBeforeDraw():void { }
		public function onBeforeResize():void { }
		public function setAnimation(animation:Animation, manager:AnimationManager):void { }
		
		override public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
			this.marker = this.deserializeMarkerElement(data, styles, this.marker, resources);
			this.label = this.deserializeLabelElement(data, styles, this.label, resources);
			this.tooltip = this.deserializeTooltipElement(data, styles, this.tooltip, resources);
			this.deserializeExtraLabels(data, styles, resources);
			this.deserializeExtraMarkers(data, styles, resources);
			this.deserializeExtraTooltips(data, styles, resources);
		}
		
		protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (marker.needCreateElementForSeries(data.marker[0])) {
				marker = MarkerElement(marker.createCopy());
				marker.deserializeSeries(data.marker[0], styles, resources);
			}
			return marker;
		}
		
		protected final function deserializeLabelElement(data:XML, styles:XML, label:LabelElement, resources:ResourcesLoader):LabelElement {
			if (label.needCreateElementForSeries(data.label[0])) {
				label = LabelElement(label.createCopy());
				label.deserializeSeries(data.label[0], styles, resources);
			}
			return label;
		}
		
		protected final function deserializeTooltipElement(data:XML, styles:XML, tooltip:TooltipElement, resources:ResourcesLoader):TooltipElement {
			if (tooltip.needCreateElementForSeries(data.tooltip[0])) {
				tooltip = TooltipElement(tooltip.createCopy());
				tooltip.deserializeSeries(data.tooltip[0], styles, resources);
			}
			return tooltip;
		}
		
		private function deserializeExtraLabels(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_labels[0] != null) {
				this.extraLabels = [];
				var extraLabelsList:XMLList = data.extra_labels[0].label;
				var extraLabelsCnt:uint = extraLabelsList.length();
				for (var i:uint = 0;i<extraLabelsCnt;i++) {
					this.extraLabels.push(this.deserializeExtraLabel(extraLabelsList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraLabel(data:XML, styles:XML, resources:ResourcesLoader):LabelElement {
			var label:LabelElement = this.global.createLabelElement(true);			
			if (data != null) 
				label.deserializeGlobal(data, styles, resources);
			return label;
		}
		
		private function deserializeExtraMarkers(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_markers[0] != null) {
				this.extraMarkers = [];
				var extraMarkersList:XMLList = data.extra_labels[0].marker;
				var extraMarkersCnt:uint = extraMarkersList.length();
				for (var i:uint = 0;i<extraMarkersCnt;i++) {
					this.extraMarkers.push(this.deserializeExtraMarker(extraMarkersList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraMarker(data:XML, styles:XML, resources:ResourcesLoader):MarkerElement {
			var marker:MarkerElement = this.global.createMarkerElement();			
			if (data != null) 
				marker.deserializeGlobal(data, styles, resources);
			return marker;
		}
		
		private function deserializeExtraTooltips(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_tooltips[0] != null) {
				this.extraTooltips = [];
				var extraTooltipsList:XMLList = data.extra_tooltips[0].tooltip;
				var extraTooltipsCnt:uint = extraTooltipsList.length();
				for (var i:uint = 0;i<extraTooltipsCnt;i++) {
					this.extraTooltips.push(this.deserializeExtraTooltip(extraTooltipsList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraTooltip(data:XML, styles:XML, resources:ResourcesLoader):TooltipElement {
			var tooltip:TooltipElement = new TooltipElement();			
			if (data != null) 
				tooltip.deserializeGlobal(data, styles, resources);
			return tooltip;
		}
		
		//-----------------------------------------------------
		//					Interactivity
		//-----------------------------------------------------
		
		public function setHover():void {
			for (var i:int = 0;i<this.points.length;i++) {
				if (this.points[i])
					ILegendTag(this.points[i]).setHover();
			}
		}
		
		public function setNormal():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				if (this.points[i])
					ILegendTag(this.points[i]).setNormal();
			}
		}
		
		//-----------------------------------------------------
		//					Formatting
		//-----------------------------------------------------
		
		public var cashedTokens:Object;
		
		public function checkPoint(point:BasePoint):void {
		}
		
		public function checkPlot(plotTokensCash:Object):void {
			plotTokensCash['PointCount'] += this.points.length;
		}
		
		public function getSeriesTokenValue(token:String):* {
			if (token == "%SeriesName" || token == "%Name") return this.name;
			if (token == "%SeriesPointCount") return this.points.length;
			if (this.customAttributes != null && this.customAttributes[token.substr(1)] != null) return this.customAttributes[token.substr(1)];			
			return "";
		}
		
		public function resetTokens():void {}
		
		public function getTokenValue(token:String):* {
			if (token.indexOf("%DataPlot") == 0)
				return this.plot.getTokenValue(token);
			if (token == "%Name") return this.name;
			return this.getSeriesTokenValue(token);
		}
		
		public function isDateTimeToken(token:String):Boolean {
			if (token.indexOf("%DataPlot") == 0)
				return this.plot.isDateTimeToken(token);
			return this.isDateTimeSeriesToken(token);
		}
		
		public function isDateTimeSeriesToken(token:String):Boolean {
			return false;
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.Points = new Array();
			for (var i:uint = 0;i<this.points.length;i++) {
				if (this.points[i])
					res.Points.push(BasePoint(this.points[i]).serialize());
			}
			return res;
		}
	}
}