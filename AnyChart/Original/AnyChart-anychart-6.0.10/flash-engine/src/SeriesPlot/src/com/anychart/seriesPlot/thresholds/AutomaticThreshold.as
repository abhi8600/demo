package com.anychart.seriesPlot.thresholds {
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.palettes.ColorPalette;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.controls.legend.LegendAdaptiveItem;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class AutomaticThreshold extends Threshold implements IAutomaticThreshold {
		
		protected var colorPalette:ColorPalette;
		public function set palette(value:ColorPalette):void { this.colorPalette = value; }
		
		protected var rangeCount:int;
		protected var autoValue:String;
		
		protected var minValue:Number;
		protected var maxValue:Number;
		protected var range:Number;
		
		protected var intervals:Array;
		
		public function getRangeCount():int { return this.rangeCount; }
		public function getRangeColor(rangeIndex:int):uint { return this.colorPalette.getItemAt(rangeIndex); }
		public function getRangeStart(rangeIndex:int):Number { return AutomaticThresholdRange(this.intervals[rangeIndex]).start; }
		public function getRangeEnd(rangeIndex:int):Number { return AutomaticThresholdRange(this.intervals[rangeIndex]).end; }
		public function getRangeValue(rangeIndex:int):Number { return this.getRangeEnd(rangeIndex) + this.getRangeStart(rangeIndex); }
		public function getRangePoints(rangeIndex:int):Array { return this.intervals[rangeIndex].points; } 
		
		override public function getData(container:ILegendItemsContainer):void {
			for (var i:uint = 0;i<this.intervals.length;i++) {
				container.addItem(new LegendAdaptiveItem(this.getRangeColor(i), 0, -1, true, this.intervals[i]));
			}
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.minValue = Number.MAX_VALUE;
			this.maxValue = -Number.MAX_VALUE;
			
			this.rangeCount = data.@range_count != undefined ? SerializerBase.getNumber(data.@range_count) : 5;
			this.autoValue = data.@auto_value != undefined ? SerializerBase.getString(data.@auto_value) : "%Value";
			if (this.autoValue.indexOf("{") != -1)
				this.autoValue = this.autoValue.substr(1, this.autoValue.length-2);
		}
		
		override public function checkAfterDeserialize(point:BasePoint):void {
			var valString:String = point.getPointTokenValue(this.autoValue);
			if (valString == "") return;
			var val:Number = Number(valString);
			if (isNaN(val)) return;
			if (val < this.minValue)
				this.minValue = val;
			if (val > this.maxValue)
				this.maxValue = val;
		}
		
		public function initialize():void {
			if (this.minValue == Number.MAX_VALUE)
				this.minValue = 0;
			if (this.maxValue == -Number.MAX_VALUE)
				this.maxValue = 10;
			this.intervals = new Array(this.rangeCount);
		}
		
		override public function reset():void {
			this.minValue = Number.MAX_VALUE;
			this.maxValue = -Number.MAX_VALUE;
			this.intervals = new Array(this.rangeCount);
			super.reset();
		}
		
		protected final function buildBoundsArray():void {
			var r:Number = (this.maxValue - this.minValue)/this.rangeCount;
			for (var i:uint = 0;i<this.rangeCount;i++) {
				var range:AutomaticThresholdRange = new AutomaticThresholdRange();
				range.start = i == 0 ? (this.minValue + r*i) : (this.intervals[i-1].end);
				range.end = range.start + r;
				range.autoValue = this.autoValue;
				this.intervals[i] = range;
			}
		}
		
		protected final function findBounds(point:BasePoint):int {
			var valString:String = point.getPointTokenValue(this.autoValue);
			if (valString == "") return -1;
			var val:Number = Number(valString);
			if (isNaN(val)) return -1;
			if (val < this.minValue || val > this.maxValue)
				return -1;
			for (var i:uint = 0;i<this.rangeCount;i++) {
				if (this.intervals[i]!= undefined && this.intervals[i].contains(val))
					return i;
			}
			if (val == this.intervals[this.rangeCount-1].end)
				return this.rangeCount - 1;
			return -1;
		}
	}
}