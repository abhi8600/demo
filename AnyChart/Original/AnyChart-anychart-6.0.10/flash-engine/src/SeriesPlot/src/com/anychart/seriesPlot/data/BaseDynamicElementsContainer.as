package com.anychart.seriesPlot.data {
	import com.anychart.elements.BaseElement;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	import com.anychart.seriesPlot.thresholds.IThreshold;
	import com.anychart.seriesPlot.thresholds.ThresholdsList;
	
	public class BaseDynamicElementsContainer extends BaseDataElement {
		
		public var name:String;
		public var customAttributes:Object;
		
		protected var _id:String;
		public function get id():String { return this._id; }
		
		protected var _color:int;
		protected var _hatchType:int;
		protected var _markerType:int;
		
		public function get color():int { return this._color; }
		public function set color(value:int):void { this._color = value; }
		
		public function get hatchType():int { return this._hatchType; }
		public function set hatchType(value:int):void { this._hatchType = value; }
		
		public function get markerType():int { return this._markerType; }
		public function set markerType(value:int):void { this._markerType = value; }
		
		public var threshold:IThreshold;
		
		public final function deserializeName(data:XML):void {
			if (data.@name != undefined)
				this.name = SerializerBase.getString(data.@name);
			if (data.name[0] != null)
				this.name = SerializerBase.getCDATAString(data.name[0]);
		}
		
		override public function deserialize(data:XML):void {
			if (data.@id != undefined)
				this._id = SerializerBase.getString(data.@id);
			this.deserializeName(data);
			if (data.attributes[0] != null) {
				this.customAttributes = {};
				var attributesList:XMLList = data.attributes[0].attribute;
				var attributesCnt:int = attributesList.length();
				for (var i:int = 0;i<attributesCnt;i++) {
					var attr:XML = attributesList[i];
					if (attr.@name == undefined) continue;
					this.customAttributes[attr.@name] = SerializerBase.getCDATAString(attr);
				}
			}
		}
		
		public final function deserializeThreshold(thresholdsList:ThresholdsList, data:XML, palettes:XML):void {
			if (data.@threshold != undefined)
				this.threshold = thresholdsList.getThreshold(palettes, data.@threshold);
		}
		
		public final function initializeStyle():void {
			if (this.settings.style == null) return;
			this.initializeColor(this._color);
			this.settings.style.setDynamicHatchTypes(this._hatchType);
		}
		
		public final function initializeColor(color:uint):void {
			if (settings.style == null) return;
			this.settings.style.setDynamicColors(color);
			this.settings.style.setDynamicGradients(color);
		}
		
		public function initializeElementsStyle():void {
			this.initializeElementsStyleColor(this._color);
			
			this.initializeElementStyleHatchType(this.label, this._hatchType);
			this.initializeElementStyleHatchType(this.marker, this._hatchType);
			this.initializeElementStyleHatchType(this.tooltip, this._hatchType);
			
			if (this.extraLabels) {
				for each (var label:LabelElement in this.extraLabels) {
					this.initializeElementStyleHatchType(label, this._hatchType);
				}
			}
			
			if (this.extraMarkers) {
				for each (var marker:MarkerElement in this.extraMarkers) {
					this.initializeElementStyleHatchType(marker, this._hatchType);
				}
			}
			
			if (this.extraTooltips) {
				for each (var tooltip:TooltipElement in this.extraTooltips) {
					this.initializeElementStyleHatchType(tooltip, this._hatchType);
				}
			}
		}
		
		public function initializeElementsStyleColor(color:uint):void {
			this.initializeElementStyleColor(this.label, color);
			this.initializeElementStyleColor(this.marker, color);
			this.initializeElementStyleColor(this.tooltip, color);
			
			if (this.extraLabels) {
				for each (var label:LabelElement in this.extraLabels) {
					this.initializeElementStyleColor(label, this._color);
				}
			}
			
			if (this.extraMarkers) {
				for each (var marker:MarkerElement in this.extraMarkers) {
					this.initializeElementStyleColor(marker, this._color);
				}
			}
			
			if (this.extraTooltips) {
				for each (var tooltip:TooltipElement in this.extraTooltips) {
					this.initializeElementStyleColor(tooltip, this._color);
				}
			}
		}
		
		protected final function initializeElementStyleColor(element:BaseElement, color:uint):void {
			if (element != null && element.style != null) {
				element.style.setDynamicColors(this.color);
				element.style.setDynamicGradients(this.color);
			}
		}
		
		protected final function initializeElementStyleHatchType(element:BaseElement, hatchType:uint):void {
			if (element != null && element.style != null)
				element.style.setDynamicHatchTypes(hatchType);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		public function serialize(res:Object = null):Object {
			if (!res) res = {};
			
			res.Name = this.name;
			res.Attributes = {};
			for (var attr:String in this.customAttributes)
				res.Attributes[attr] = this.customAttributes[attr];
				
			res.Color = this.color;
			res.HatchType = this.hatchType;
			res.MarkerType = this.markerType;
			
			if (this._id != null)
				res.ID = this._id;
			
			return res;
		}
	}
}