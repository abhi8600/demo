package com.anychart.seriesPlot.thresholds {
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	
	
	public class CustomThresholdCondition implements IThresholdItemFormatable,ILegendTag {
		private var value1:String;
		private var isDynamicValue1:Boolean;
		
		private var value2:String;
		private var isDynamicValue2:Boolean;
		
		private var value3:String;
		private var isDynamicValue3:Boolean;
		
		private var name:String;
		internal var color:uint;
		
		internal var threshold:CustomThreshold;
		private var points:Array;
		public var attributes:Object;
		
		public function CustomThresholdCondition() {
			this.isDynamicValue1 = false;
			this.isDynamicValue2 = false;
			this.isDynamicValue3 = false;
			this.color = 0xFF0000;
			this.name = "condition";
			this.threshold = threshold;
			this.points = new Array();
		}
		
		public final function check(point:BasePoint):Boolean {
			 var v1:* = this.isDynamicValue1 ? point.getTokenValue(this.value1) : this.value1;
			 var v2:* = this.isDynamicValue2 ? point.getTokenValue(this.value2) : this.value2;
			 var v3:* = this.isDynamicValue3 ? point.getTokenValue(this.value3) : this.value3;
			 if (this.isTrue(isNaN(Number(v1)) ? v1 : Number(v1),
			 				 isNaN(Number(v2)) ? v2 : Number(v2),
			 				 isNaN(Number(v3)) ? v3 : Number(v3))) {
				this.threshold.setPointColor(point, this.color);
				point.thresholdItem = this;
				this.points.push(point);
				return true;
			}
			return false;
		}
		
		public function reset():void {
			this.points = [];
		}
		
		protected function isTrue(value1:*, value2:*, value3:*):Boolean {
			return false;
		}
		
		internal final function deserialize(data:XML):void {
			if (data.@color != undefined) this.color = SerializerBase.getColor(data.@color);
			if (data.@name != undefined) this.name = SerializerBase.getString(data.@name);
			if (data.@value_1 != undefined) {
				this.isDynamicValue1 = this.threshold.isToken(data.@value_1);
				this.value1 = this.isDynamicValue1 ? this.threshold.getToken(data.@value_1) : SerializerBase.getString(data.@value_1);
			}
			if (data.@value_2 != undefined) {
				this.isDynamicValue2 = this.threshold.isToken(data.@value_2);
				this.value2 = this.isDynamicValue1 ? this.threshold.getToken(data.@value_2) : SerializerBase.getString(data.@value_2);
			}
			if (data.@value_3 != undefined) {
				this.isDynamicValue3 = this.threshold.isToken(data.@value_3);
				this.value3 = this.isDynamicValue1 ? this.threshold.getToken(data.@value_3) : SerializerBase.getString(data.@value_3);
			}
			if (data.attributes[0] != null) {
				this.attributes = {};
				var attrCnt:int = data.attributes[0].attribute.length();
				for (var i:int = 0;i<attrCnt;i++) {
					var attr:XML = data.attributes[0].attribute[i];
					if (attr.@name != undefined) {
						this.attributes["%"+SerializerBase.getString(attr.@name)] = SerializerBase.getCDATAString(attr);
						this.attributes["%Condition"+SerializerBase.getString(attr.@name)] = SerializerBase.getCDATAString(attr);
					}
				}
			}
		}
		
		internal static function create(data:XML, threshold:CustomThreshold):CustomThresholdCondition {
			if (data.@type == undefined) return null;
			var condition:CustomThresholdCondition;
			switch (String(data.@type).toLowerCase()) {
				case "between": condition = new BetweenCondition(); break;
				case "notbetween": condition = new NotBetweenCondition(); break;
				case "equalto": condition = new EqualCondition(); break;
				case "notequalto": condition = new NotEqualToCondition(); break;
				case "greaterthan": condition = new GreaterThanCondition(); break;
				case "greaterthanorequalto": condition = new GreaterThanOrEqualToCondition(); break;
				case "lessthan": condition = new LessThanCondition(); break;
				case "lessthanorequalto": condition = new LessThanOrEqualToCondition(); break;
			}
			if (condition == null)
				return null;
			condition.threshold = threshold;
			condition.deserialize(data);
			return condition;
		}
		
		public final function getCustomAttribute(token:String):* {
			return this.attributes[token];
		}
		
		public final function isCustomAttribute(token:String):Boolean {
			return this.attributes != null && this.attributes[token] != null;
		}
		
		public final function getTokenValue(token:String):* {
			if (token == "%ConditionName" || token == "%Name") return this.name;
			if (this.isCustomAttribute(token)) return this.attributes[token]; 
			return "";
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		public final function getFormatedTokenValue(point:BasePoint, token:String):* {
			if (token == "%ConditionName" || token == "%Name") return this.name;
			if (token == "%ConditionValue1") return this.isDynamicValue1 ? point.getTokenValue(this.value1) : this.value1;
			if (token == "%ConditionValue2") return this.isDynamicValue2 ? point.getTokenValue(this.value2) : this.value2;
			if (token == "%ConditionValue3") return this.isDynamicValue3 ? point.getTokenValue(this.value3) : this.value3;
			return ""; 
		}
		
		public final function hasToken(token:String):Boolean {
			return token.indexOf("%Condition") == 0;
		}
		
		public final function setHover():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setHover();
			}
		}
		
		public final function setNormal():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setNormal();
			}
		}
	}
}
import com.anychart.seriesPlot.thresholds.CustomThresholdCondition;

final class BetweenCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 >= value2 && value1 <= value3;
	}
}

final class NotBetweenCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 < value2 || value1 > value3;
	}
}

final class EqualCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 == value2;
	}
}

final class NotEqualToCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 != value2;
	}
}

final class GreaterThanCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 > value2;
	}
}

final class GreaterThanOrEqualToCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 >= value2;
	}
}

final class LessThanCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 < value2;
	}
}

final class LessThanOrEqualToCondition extends CustomThresholdCondition {
	override protected function isTrue(value1:*, value2:*, value3:*):Boolean {
		return value1 <= value2;
	}
}