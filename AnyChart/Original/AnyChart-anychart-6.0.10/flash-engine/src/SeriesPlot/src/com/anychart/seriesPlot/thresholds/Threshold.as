package com.anychart.seriesPlot.thresholds {
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal class Threshold implements IThreshold {
		
		public var name:String;
		
		public function deserialize(data:XML):void {
			if (data.@name != undefined)
				this.name = SerializerBase.getString(data.@name); 
		}
		public function checkBeforeDraw(point:BasePoint):void {}
		public function checkAfterDeserialize(point:BasePoint):void {}
		
		public function isAutomatic():Boolean { return this is AutomaticThreshold; }
		
		public function reset():void {}
		
		internal final function setPointColor(point:BasePoint, color:uint):void {
			point.color = color;
			point.initializeColor(color);
			point.initializeElementsStyleColor(color);
		}
		
		internal final function isToken(value:*):Boolean {
			var index:int = String(value).indexOf('%');
			return index == 0 || index == 1;
		}
		
		internal final function getToken(value:*):String {
			var res:String = String(value);
			if (res.charAt(0) == '{')
				res = res.substr(1,res.length-2);
			return res;
		}
		
		public function getTokenValue(token:String):* {
			if (token == "%ThresholdName") return this.name;
			return "";
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		public function getData(container:ILegendItemsContainer):void {}
		public final function getName():String { return this.name; }
	}
}