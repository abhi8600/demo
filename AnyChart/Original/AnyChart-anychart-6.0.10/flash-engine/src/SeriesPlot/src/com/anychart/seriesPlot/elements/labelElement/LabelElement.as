package com.anychart.seriesPlot.elements.labelElement {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class LabelElement extends ItemLabelElement {
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new LabelElement();
			super.createCopy(element);
			return element;
		}
		
		public function needCreateElementForSeries(elementNode:XML):Boolean {
			if (elementNode == null) return false;
			if (elementNode.@style != undefined) return true;
			if (elementNode.@enabled != undefined && SerializerBase.getBoolean(elementNode.@enabled) != this.enabled) return true;
			if (elementNode.format[0] != null) return true;
			return super.needCreateElementForContainer(elementNode); 
		}
		
		public function needCreateElementForPoint(elementNode:XML):Boolean {
			return super.needCreateElementForContainer(elementNode);
		}
		
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			delete data.@enabled;
			this.deserializeStyleFromNode(stylesList, data, resources);
		}
		
		public function deserializeSeries(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
		}
		
		public function deserializePoint(data:XML, point:BasePoint, stylesList:XML, elementIndex:uint, resources:ResourcesLoader):void {
			super.deserializeElementForContainer(data, point, stylesList, elementIndex, resources);
		}
		
	}
}