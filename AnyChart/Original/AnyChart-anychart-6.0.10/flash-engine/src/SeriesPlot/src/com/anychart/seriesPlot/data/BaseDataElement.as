package com.anychart.seriesPlot.data {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	public class BaseDataElement implements ISerializable {
		public var settings:BaseSeriesSettings;
		
		public var marker:MarkerElement;
		public var label:LabelElement;
		public var tooltip:TooltipElement;
		
		public var extraLabels:Array;
		public var extraMarkers:Array;
		public var extraTooltips:Array;
		
		public function get plot():SeriesPlot { return this.settings.global.plot; }
		public function get global():GlobalSeriesSettings { return this.settings.global; }
		
		public function deserialize(data:XML):void {
		}
		
		public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
		}
		
		public function setElements(data:BaseDataElement):void {
			data.marker = this.marker;
			data.label = this.label;
			data.tooltip = this.tooltip;
			data.extraLabels = this.extraLabels;
			data.extraMarkers = this.extraMarkers;
			data.extraTooltips = this.extraTooltips;
		}
	}
}