package com.anychart.seriesPlot.categories {
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.formatters.IFormatable;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.seriesPlot.SeriesPlot;
	
	
	public class BaseCategoryInfo implements IFormatable,ILegendTag,IObjectSerializable {
		public var name:String;
		public var index:int;
		public var color:uint;
		
		protected var plot:SeriesPlot;
		protected var points:Array;
		
		public function BaseCategoryInfo(plot:SeriesPlot) {
			this.plot = plot;
			this.points = [];
			this.cashedTokens = {};
		}
		
		public function destroy():void {
			this.plot = null;
			this.points = null;
			this.singleValuePoints = null;
		}
		
		public function getPoints():Array { return this.points; }
		
		public function initColor():void {
			this.color = this.points.length > 0 ? this.points[0].color : 0;
		}
		
		public function recalculate():void {
			this.cashedTokens = {};
			for (var i:int = 0;i<this.points.length;i++) 
				this.points[i].checkCategory(this.cashedTokens);
		}
		
		public function checkPoint(point:ICategorizedPoint):void {
			point.checkCategory(this.cashedTokens);
			this.points.push(point);
		}
		
		//-------------------------------------------
		//				Formatting
		//-------------------------------------------
		
		private var singleValuePoints:Array;
		private var cashedTokens:Object;
		
		public function resetTokens():void {
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['RangeMax'] = -Number.MAX_VALUE;
			this.cashedTokens['RangeMin'] = Number.MAX_VALUE;
			this.cashedTokens['RangeSum'] = 0;
			this.cashedTokens['CategoryYPercentOfTotal'] = null;
			this.cashedTokens['PointCount'] = 0;
		}
		
		public function getCategoryTokenValue(token:String):* {
			if (token == '%CategoryName' || token == "%Name") return this.name;
			if (token == '%CategoryPointCount') return this.cashedTokens['PointCount'];
			if (token == '%CategoryYSum') return this.cashedTokens['YSum'];
			if (token == '%CategoryYMax') return this.cashedTokens['YMax'];
			if (token == '%CategoryYMin') return this.cashedTokens['YMin'];
			if (token == '%CategoryYRangeMax') return this.cashedTokens['RangeMax'];
			if (token == '%CategoryYRangeMin') return this.cashedTokens['RangeMin'];
			if (token == '%CategoryYRangeSum') return this.cashedTokens['RangeSum'];
			
			if (token == '%CategoryYPercentOfTotal') {
				if (this.cashedTokens['%CategoryYPercentOfTotal'] == null) {
					this.cashedTokens['%CategoryYPercentOfTotal'] = this.cashedTokens['YSum']/this.plot.getTokenValue('%DataPlotYSum')*100;
				}
				return this.cashedTokens['%CategoryYPercentOfTotal'];
			}
			if (token == '%CategoryYAverage') return this.cashedTokens['YSum']/this.cashedTokens['PointCount'];
			return '';
		}
		
		public function getTokenValue(token:String):* {
			if (token.indexOf('%DataPlot') == 0) return this.plot.getTokenValue(token);
			return this.getCategoryTokenValue(token);
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		//-------------------------------------------
		//				Interactivity
		//-------------------------------------------
		
		public function setHover():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setHover();
			}
		}
		
		public function setNormal():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setNormal();
			}
		}
		
		//-------------------------------------------
		//				IObjectSerializable
		//-------------------------------------------
		
		public function serialize(res:Object = null):Object {
			if (!res) res = {};
			res.Name = this.name;
			res.XSum = this.cashedTokens['XSum'];
			res.YSum = this.cashedTokens['YSum'];
			res.BubbleSizeSum = this.cashedTokens['BubbleSizeSum'];;
			res.YRangeMax = this.cashedTokens['RangeMax'];
			res.YRangeMin = this.cashedTokens['RangeMin'];
			res.YRangeSum = this.cashedTokens['RangeSum'];
			res.YPercentOfTotal = this.getCategoryTokenValue("%CategoryYPercentOfTotal");
			res.YMedian = this.getCategoryTokenValue("%CategoryYMedian");
			res.YMode = this.getCategoryTokenValue("%CategoryYMode");
			res.YAverage = this.getCategoryTokenValue("%CategoryYAverage");
			return res;
		}
	}
}