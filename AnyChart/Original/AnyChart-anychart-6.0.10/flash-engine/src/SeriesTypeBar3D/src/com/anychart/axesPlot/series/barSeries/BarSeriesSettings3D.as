package com.anychart.axesPlot.series.barSeries {
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.Style;
	
	internal final class BarSeriesSettings3D extends BarSeriesSettings {
		
		override protected function checkShapeTypeStyle():void {
			if (this.shapeType == BarShapeType.BOX){this.setProgramaticStyle("box3dprogramaticstyle");}
			if (this.shapeType == BarShapeType.PYRAMID){this.setProgramaticStyle("pyramid3dprogramaticstyle");}
			if (this.shapeType == BarShapeType.CYLINDER){this.setProgramaticStyle("cylinder3dprogramaticstyle");}
			if (this.shapeType == BarShapeType.CONE){this.setProgramaticStyle("cone3dprogramaticstyle");}
		}
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new BarSeriesSettings3D();
			super.createCopy(settings);
			BarSeriesSettings3D(settings).shapeType = this.shapeType;
			return settings;
		}
		
		private function setProgramaticStyle(styleName:String):void {
			this.style = this.style.createCopy();
			var s:Style = this.style;
			
			s.normal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.normal); 
			s.hover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.hover);
			s.pushed.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.pushed);
			s.missing.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.missing);
			s.selectedNormal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedNormal);
			s.selectedHover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedHover);
		}
	}
}