package com.anychart.axesPlot.series.barSeries.programmaticStyles{
	
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.axesPlot.series.barSeries.drawing3D.Bar3DDrawerBase;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.geom.Rectangle;

	public final class Cone3DProgramaticStyle extends Base3DProgramaticStyle{
		private var frontGrd:Gradient;
		private var frontGrdHor:Gradient;
		private var backGrd:Gradient;
		
		private var border:Stroke;
		
		private var leftGrd:Gradient;
		private var leftHorGrd:Gradient;
		private var leftGrdInv:Gradient;
		private var leftHorGrdInv:Gradient;  
		
		private var rightGrd:Gradient;
		private var rightHorGrd:Gradient;
		private var rightGrdInv:Gradient;
		private var rightHorGrdInv:Gradient;
		
		private var topGrd:Gradient;
		private var topHorGrd:Gradient;
		private var topGrdInv:Gradient;
		private var topHorGrdInv:Gradient;
		
		private var bottomGrd:Gradient;
		private var bottomHorGrd:Gradient;
		private var bottomGrdInv:Gradient;
		private var bottomHorGrdInv:Gradient;
		
		private var blend1Index:uint;
		private var blend2Index:uint;
		private var blend3Index:uint;
		private var entries:Array;    
		
		override public function initialize(stateBase:StyleState):void {
			this.initDynamicIndexes(stateBase,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(stateBase);
			var opacity:Number= state.fill ? state.fill.opacity : 1;

			var isDynamic:Boolean = state.fill.isDynamic;
			this.entries = [];
			if (isDynamic) {//front
				this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
				this.frontGrd = new Gradient(1);
				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.frontGrd,state,opacity);
				
				this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
				this.frontGrdHor = new Gradient(1);
				this.frontGrdHor.angle = 90;
				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.frontGrdHor,state,opacity);
			}else {
				this.frontGrd = new Gradient(1);
				this.frontGrdHor = new Gradient(1);
				this.frontGrdHor.angle = 90;
				this.execCreateGradient(this.frontGrdHor,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0)");
				this.execCreateGradient(this.frontGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0)");
			}
			
			{//back
				if (isDynamic) {
				this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
				this.frontGrd = new Gradient(1);
				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.frontGrd,state,opacity);
				
				this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
				this.frontGrdHor = new Gradient(1);
				this.frontGrdHor.angle = 90;
				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.frontGrdHor,state,opacity);
			}else {
				this.frontGrd = new Gradient(1);
				this.frontGrdHor = new Gradient(1);
				this.frontGrdHor.angle = 90;
				this.execCreateGradient(this.frontGrdHor,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0)");
				this.execCreateGradient(this.frontGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0)");
			}
				
			}
			
			{//left
				if (isDynamic){
					this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0)",
										 "Blend(%Color, DarkColor(%Color), 0)",	
										 "Blend(%Color, DarkColor(%Color), 0)");
				
					this.leftGrd = new Gradient(1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.leftGrd,state,1);
					//this.leftGrd  = this.frontGrd;
				}else {
					this.leftGrd = new Gradient(1);
					this.execCreateGradient(this.leftGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
			}
			
			{//right
				if (isDynamic){
					this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0)",
										 "Blend(%Color, DarkColor(%Color), 0)",	
										 "Blend(%Color, DarkColor(%Color), 0)");
				
					this.rightGrd = new Gradient(1);
					
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.rightGrd,state,1);
					//this.rightGrd = this.frontGrd;
				}else {
					this.rightGrd = new Gradient(1);
					this.execCreateGradient(this.rightGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
			}
			
			{//top
				if (isDynamic){
					this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0)",
										 "Blend(%Color, DarkColor(%Color), 0)",	
										 "Blend(%Color, DarkColor(%Color), 0)");
					
					this.topGrd = new Gradient(1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.topGrd,state,opacity);
				}else {
					this.topGrd = new Gradient(1);
					this.execCreateGradient(this.topGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
			}
			
			{//bottom
				if (isDynamic){
					this.getBlendIndexes("Blend(%Color, DarkColor(%Color), 0)",
										 "Blend(%Color, DarkColor(%Color), 0)",	
										 "Blend(%Color, DarkColor(%Color), 0)");
	
					this.bottomGrd = new Gradient(1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.blend3Index,this.bottomGrd,state,opacity*0.3);
				}else{
					this.bottomGrd = new Gradient(1);
					this.execCreateGradient(this.bottomGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
			}
		}
		
		private function getBlendIndexes(a1:String = null,a2:String = null,a3:String = null):void{
			if (!a1 && !a2 && !a3) return;
			
			if (a1)this.blend1Index = ColorParser.getInstance().parse(a1);
			if (a2)this.blend2Index = ColorParser.getInstance().parse(a2);
			if (a3)this.blend3Index = ColorParser.getInstance().parse(a3);
		}
		
		private function execCreateDinamicGradient(index1:uint,index2:uint,index3:uint,grd:Gradient,state:BackgroundBaseStyleState,opacity:Number):void{
				this.entries.push(createKey(0,index1,opacity,true));
				this.entries.push(createKey(0.35,index2,opacity,true));
        		this.entries.push(createKey(1,index3,opacity,true));
        		
       			this.createDynamicGradient(state,grd,entries);
				this.entries=[];
		}
		
		private function execCreateGradient(grd:Gradient,state:BackgroundBaseStyleState,opacity:Number,color1:String,color2:String,color3:String):void {
			if (!color1 && !color2) return;
			if (color1 && color2)
				grd.deserialize(<gradient>
						<key position="0" color={color1.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
						<key position="0.35" color={color2.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
						<key position="1" color={color3.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
					</gradient>);
		} 
		
		override protected function drawFrontSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).frontBounds;
			if (point.isHorizontal){
				this.frontGrdHor.beginGradientFill(point.container.graphics, rect, point.color);
				this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
				point.container.graphics.lineStyle(1,this.lineColor,0.9);
				this.execDrawFrontSide(point);
				/* point.container.graphics.beginFill(0,1);
				point.container.graphics.drawRect(rect.x,rect.y,rect.width,rect.height); */
				point.container.graphics.endFill();
			}else{
				this.frontGrd.beginGradientFill(point.container.graphics, rect, point.color);
				this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
				point.container.graphics.lineStyle(1,this.lineColor,0.9);
				this.execDrawFrontSide(point);
				point.container.graphics.endFill();
			}
			super.drawFrontSide(point);
		}
		
		override protected function drawBackSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).backBounds;
			if (point.isHorizontal){
				this.frontGrdHor.beginGradientFill(point.container.graphics, rect, point.color);
				this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
				point.container.graphics.lineStyle(1,this.lineColor,0.9);
				this.execDrawBackSide(point);
				/* point.container.graphics.beginFill(0xFF0000,1);
				point.container.graphics.drawRect(rect.x,rect.y,rect.width,rect.height); */
				point.container.graphics.endFill();
			}else{
				this.frontGrd.beginGradientFill(point.container.graphics, rect, point.color);
				this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
				point.container.graphics.lineStyle(1,this.lineColor,0.9);
				this.execDrawBackSide(point);
				point.container.graphics.endFill();
			}
			super.drawBackSide(point);
		}
		
		private var lineColor:uint;
		
		private function initLineColor(state:BackgroundBaseStyleState, pointColor:uint):void {
			lineColor = (state.stroke && !state.stroke.isDynamic) ? 
								ColorUtils.getDarkColor(state.stroke.color)
								: ColorParser.getInstance().getDynamicColor(this.darkIndex, pointColor);
		} 
		
		override protected function drawLeftSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).leftBounds;
		//	this.leftGrd.angle = !I3DObject(point).isLeftSideVisible ? 0:180; 
			this.leftGrd.beginGradientFill(point.container.graphics,rect, point.color);
			this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
			point.container.graphics.lineStyle(1,this.lineColor,0.9);
			this.execDrawLeftSide(point);
			point.container.graphics.endFill();
			super.drawLeftSide(point);
				
		}
		
		override protected function drawRightSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).rightBounds;
			//this.rightGrd.angle = I3DObject(point).isLeftSideVisible ? 90:0; 
			this.rightGrd.beginGradientFill(point.container.graphics,rect, point.color);
			this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
			point.container.graphics.lineStyle(1,this.lineColor,0.9);
			this.execDrawRightSide(point);
			point.container.graphics.endFill();
			super.drawRightSide(point);
		}
		
		override protected function drawTopSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).topBounds;
			this.frontGrd.beginGradientFill(point.container.graphics,rect,point.color);
			this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
			point.container.graphics.lineStyle(1,this.lineColor,0.9);
			this.execDrawTopSide(point);
			point.container.graphics.endFill();
			super.drawTopSide(point);
		}
		override protected function drawBottomSide(point:AxesPlotPoint):void {
			var rect:Rectangle = Bar3DDrawerBase(I3DObject(point).getDrawer()).bottomBounds;
			this.topGrd.beginGradientFill(point.container.graphics,rect,point.color);
			this.initLineColor(BackgroundBaseStyleState(point.styleState),point.color);
			point.container.graphics.lineStyle(1,this.lineColor,0.9);
			this.execDrawBottomSide(point);
			point.container.graphics.endFill();
			super.drawBottomSide(point);
		}
	}
}