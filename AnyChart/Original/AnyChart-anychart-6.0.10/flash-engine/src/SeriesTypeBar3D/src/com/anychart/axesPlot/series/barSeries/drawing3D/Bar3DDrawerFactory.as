package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.axesPlot.series.barSeries.BarShapeType;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawerBase;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawingFactory;
	import com.anychart.errors.FeatureNotSupportedError;
	
	import flash.utils.getDefinitionByName;
	
	public final class Bar3DDrawerFactory implements IBarDrawingFactory{
		
		private var coneDrawerClass:Class;
		private var cylinderDrawerClass:Class;
		private var pyramidDrawerClass:Class;
		
		public function Bar3DDrawerFactory(isHorizontal:Boolean){
			cylinderDrawerClass = isHorizontal ? Cylinder3DDrawerHorizontal : Cylinder3DDrawer;
			coneDrawerClass = isHorizontal ? Cone3DDrawerHorizontal : Cone3DDrawer;
			pyramidDrawerClass = isHorizontal ? Pyramid3DDrawerHorizontal : Pyramid3DDrawer;
		}
		
		public function create(shapeType:uint):IBarDrawerBase {
			
			switch (shapeType) {
				case BarShapeType.CYLINDER:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Cylinder chart type not supported in this swf version");
				 
					return new cylinderDrawerClass();
				case BarShapeType.CONE:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Cone chart type not supported in this swf version");  
					return new coneDrawerClass();
					
				case BarShapeType.PYRAMID:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Pyramid chart type not supported in this swf version"); 
					return new pyramidDrawerClass();
			}
			
			return new Box3DDrawer();
		}
		
	}
}