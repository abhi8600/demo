package com.anychart.axesPlot.series.barSeries.drawing3D{
	import com.anychart.styles.states.BackgroundBaseStyleState3D;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	
	internal final class Cylinder3DDrawerHorizontal extends ElipseBased3DDrawerHorizontal implements IBar3DDrawer{
		
		public function Cylinder3DDrawerHorizontal(){
			super();
		}
	}
}