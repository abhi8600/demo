package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawerBase;
	import com.anychart.styles.states.BackgroundBaseStyleState3D;
	
	import flash.display.Graphics;
	
	public interface IBar3DDrawer extends IBarDrawerBase{
		function initialize(point:AxesPlotPoint):void;
		
		function initializePixels(startValue:Number, endValue:Number):void;
		
		function drawLeftSide(g:Graphics):void;
		function drawRightSide(g:Graphics):void;
		function drawTopSide(g:Graphics):void;
		function drawBottomSide(g:Graphics):void;
		function drawFrontSide(g:Graphics):void;
		function drawBackSide(g:Graphics):void;
	}
}