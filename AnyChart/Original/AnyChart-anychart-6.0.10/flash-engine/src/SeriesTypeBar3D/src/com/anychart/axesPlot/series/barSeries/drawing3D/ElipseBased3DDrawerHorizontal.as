package com.anychart.axesPlot.series.barSeries.drawing3D{
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.styles.states.BackgroundBaseStyleState3D;
	import com.anychart.styles.states.BackgroundBaseStyleState3DEntry;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	internal class ElipseBased3DDrawerHorizontal extends ElipseBased3DDrawer{
		
		override protected function initializeSides():void {
			//front side
			var frontLeftTop:Point = this.leftTop.clone();
			var frontRightBottom:Point = this.rightBottom.clone();
			
			//back side
			var backLeftTop:Point = this.leftTop.clone();
			var backRightBottom:Point = this.rightBottom.clone();
			
			this.point.z.transform(frontLeftTop);
			this.point.z.transform(frontRightBottom)
			
			this.point.z.transform(backLeftTop, 0, 1);
			this.point.z.transform(backRightBottom, 0, 1);
			
			/* this.checkGeom(frontLeftTop, frontRightBottom);
			this.checkGeom(backLeftTop, backRightBottom); */
 
			this.topCenter.x = (frontLeftTop.x + backLeftTop.x)/2;
			this.bottomCenter.x = (frontRightBottom.x + backRightBottom.x)/2;
			
			this.topCenter.y = this.bottomCenter.y = (frontLeftTop.y + backRightBottom.y)/2;
			
			this.topXRadius = this.bottomXRadius = Math.abs(backLeftTop.x - frontLeftTop.x)/2;
			this.topYRadius = this.bottomYRadius = Math.abs(frontRightBottom.y - frontLeftTop.y)/2;
			
			this.topBounds.x = this.topCenter.x - this.topXRadius;
			this.topBounds.width = this.topXRadius*2;
			this.topBounds.y = this.topCenter.y - this.topYRadius;
			this.topBounds.height = this.topYRadius*2;
			
			this.bottomBounds.x = this.bottomCenter.x - this.bottomXRadius;
			this.bottomBounds.width = this.bottomXRadius*2;
			this.bottomBounds.y = this.bottomCenter.y - this.bottomYRadius;
			this.bottomBounds.height = this.bottomYRadius*2;
			
			this.frontBounds.x = this.topBounds.x;
			this.frontBounds.y = this.topBounds.y;
			this.frontBounds.width = this.topBounds.width;
			this.frontBounds.height = this.bottomBounds.bottom - this.frontBounds.y;
			
			this.backBounds.x = backLeftTop.x;
			this.backBounds.y = backLeftTop.y;
			this.backBounds.width = backRightBottom.x - backLeftTop.x;
			this.backBounds.height = backRightBottom.y - backLeftTop.y;
			
			this.leftBounds.x = Math.min(frontLeftTop.x,backLeftTop.x); 
			this.leftBounds.y = Math.min(frontLeftTop.y,backLeftTop.y); 
			this.leftBounds.width = Math.abs(backRightBottom.x-frontRightBottom.x);
			this.leftBounds.height = Math.max(Math.abs(backRightBottom.y-frontLeftTop.y),Math.abs(frontRightBottom.y - backLeftTop.y));
			
			this.rightBounds.x = Math.min(backRightBottom.x,frontRightBottom.x); 
			this.rightBounds.y = Math.min(frontLeftTop.y,backLeftTop.y); 
			this.rightBounds.width = Math.abs(backRightBottom.x-frontRightBottom.x);
			this.rightBounds.height = Math.max(Math.abs(backRightBottom.y-frontLeftTop.y),Math.abs(frontRightBottom.y - backLeftTop.y));
			
		}
		
		
		override public function drawRightSide(g:Graphics):void {
			
			//да, я еблан. не нравится - переписывайте
			var center:Point;
			var xRadius:Number;
			var yRadius:Number;
			var bounds:Rectangle;
			
				 center = this.bottomCenter;
				xRadius = this.bottomXRadius;
				yRadius = this.bottomYRadius;
				bounds = this.bottomBounds; 
				/* center = this.topCenter;
				xRadius = this.topXRadius;
				yRadius = this.topYRadius;
				bounds = this.topBounds; */
			
			
			this.drawEllipseSide(g, center, xRadius, yRadius, bounds);
		}
		
		override public function drawLeftSide(g:Graphics):void {
			
			//да, я еблан. не нравится - переписывайте
			var center:Point;
			var xRadius:Number;
			var yRadius:Number;
			var bounds:Rectangle;
			
			
				/* center = this.bottomCenter;
				xRadius = this.bottomXRadius;
				yRadius = this.bottomYRadius;
				bounds = this.bottomBounds; */
			
				 center = this.topCenter;
				xRadius = this.topXRadius;
				yRadius = this.topYRadius;
				bounds = this.topBounds; 
			   
			
			this.drawEllipseSide(g, center, xRadius, yRadius, bounds);
		} 
		
		override public function drawTopSide(g:Graphics):void {}
		override public function drawBottomSide(g:Graphics):void {}
		
		override public function drawFrontSide(g:Graphics):void {
			var startAngle:Number =!I3DObject(this.point).isLeftSideVisible ? 90:270; 
			this.drawSide(g, startAngle, startAngle+180,this.frontBounds );
		}
		
		 override public function drawBackSide(g:Graphics):void {
		 	var startAngle:Number =!I3DObject(this.point).isLeftSideVisible ? 270:90; 
			this.drawSide(g, startAngle, startAngle +180, this.backBounds);
		}  
	}
}