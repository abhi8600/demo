package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.series.barSeries.pointsOrdering.IPointsOrderer;
	import com.anychart.axesPlot.series.barSeries.pointsOrdering.PointsOrderingFactory;
	
	public final class Bar3DPointsDistributor {
		
		public static function distribute(plot:AxesPlot3D):void {
			var orderer:IPointsOrderer = PointsOrderingFactory.create(plot.getPlotType());
			if (orderer) orderer.order(plot);
		}
	}
}