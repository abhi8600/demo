package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.series.barSeries.drawing3D.IBar3DDrawer;
	
	public interface I3DObject {
		function get isLeftSideVisible():Boolean;
		function set isLeftSideVisible(vaue:Boolean):void;
		
		function get isTopSideVisible():Boolean;
		function set isTopSideVisible(vaue:Boolean):void;
		
		function getDrawer():IBar3DDrawer;
		
		function initialize():void;
	}
}