package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.scales.BaseScale;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class BarSeries3D extends BarSeries {
		
		override public function createPoint():BasePoint {
			return new BarPoint3D();
		}
		
		override public function addPointsToDrawing():Boolean { return false; }
	}
}