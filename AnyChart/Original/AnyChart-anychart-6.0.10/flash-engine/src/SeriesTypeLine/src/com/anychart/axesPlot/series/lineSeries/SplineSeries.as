package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal class SplineSeries extends LineSeries {
		
		override public function createPoint():BasePoint {
			return new SplinePoint();
		}
		
		override public function onBeforeDraw():void {
			super.onBeforeDraw();
			createControls();
		}
		
		override public function onBeforeResize():void {
			createControls();
		}
	
//////// DO NOT DELETE !!!	
/*
		override public function onBeforeResize():void {
			var pt:SplinePoint;
			var length:uint = this.points.length;
			for (var i:uint = 0; i < length; i++) {
				pt = SplinePoint(this.points[i]);
				if (pt.isMissing) continue;
				pt.initValuePoint();
				pt.initConrolPoints();
			}
		}
//*/		
		
		private function getNextNonMissing(ptNum:int):uint {
			var interpolate:Boolean = this.plot.interpolateMissings;
			for (var i:uint = ptNum + 1; i < length; i++)
				if (points[i] != null && (!BasePoint(points[i]).isMissing || interpolate)) return i;
			return i;
		}
		
		private var length:int;
		private function createControls():void {
			length = this.points.length;
			var pt:SplinePoint; 
			var ptPrev:SplinePoint; 
			var numPoint:uint = getNextNonMissing(-1);
			var numPointPrev:uint = numPoint;
			try
			{
				pt = SplinePoint(this.points[numPoint]);
				pt.prev = null;
				pt.firstInit();
				ptPrev = pt;
				for (numPoint = getNextNonMissing(numPoint); numPoint < length; numPoint = getNextNonMissing(numPoint)) {
					pt = SplinePoint(this.points[numPoint]);
	
					if (numPoint - numPointPrev == 1) {
						ptPrev.next = pt;
						pt.prev = ptPrev;
					} else {
						ptPrev.next = null;
						pt.prev = null;
					}
					pt.firstInit();
						 
					ptPrev.initDelegates();
					ptPrev.createControlPoints();
					//ptPrev.initConrolPoints();
					numPointPrev = numPoint;
					ptPrev = pt;
				}
				ptPrev.initDelegates();
				ptPrev.createControlPoints();
				//ptPrev.initConrolPoints();
			}
			catch (error:Error)
			{
				
			}
			
		}
	}
}