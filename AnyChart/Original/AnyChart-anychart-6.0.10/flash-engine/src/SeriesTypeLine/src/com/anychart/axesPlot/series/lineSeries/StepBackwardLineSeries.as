package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class StepBackwardLineSeries extends LineSeries {
		override public function createPoint():BasePoint {
			return new StepBackwardLinePoint();
		}
	}
}