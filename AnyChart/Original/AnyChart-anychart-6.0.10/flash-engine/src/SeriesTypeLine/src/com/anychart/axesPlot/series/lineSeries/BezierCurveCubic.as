package com.anychart.axesPlot.series.lineSeries
{
	import com.anychart.utils.LineEquation;
	import com.anychart.utils.MathUtils;
	
	import flash.geom.Point;
	
	internal final class BezierCurveCubic
	{
		public var pointFirst:Point;
		public var pointSecond:Point;
		public var controlFirst:Point;
		public var controlSecond:Point;
		
		/*
		public function toString():String {
			return "[f="+pointFirst+"; s="+pointSecond+"; cf="+controlFirst+"; cs="+controlSecond+"]";
		}
		//*/

		public function getBezierValue(t:Number):Point {
			var tSq:Number = t * t;
			var tCb:Number = t * tSq;
			var r1:Number = (1 - 3 * t + 3 * tSq - tCb);
			var r2:Number = (3 * t - 6 * tSq + 3 * tCb);
			var r3:Number = (3 * tSq - 3 * tCb);
			var r4:Number = tCb;
			
			return new Point(
/*
				r1 * pointFirst.valuePointX + 
				r2 * controlFirst.valuePointX + 
				r3 * controlSecond.valuePointX + 
				r4 * pointSecond.valuePointX,
				
				r1 * pointFirst.valuePointY + 
				r2 * controlFirst.valuePointY + 
				r3 * controlSecond.valuePointY + 
				r4 * pointSecond.valuePointY);
//*/
				r1 * pointFirst.x + 
				r2 * controlFirst.x + 
				r3 * controlSecond.x + 
				r4 * pointSecond.x,
				
				r1 * pointFirst.y + 
				r2 * controlFirst.y + 
				r3 * controlSecond.y + 
				r4 * pointSecond.y);
		}

		public function getCubicTangent(t:Number):Tangent {
			var g:Number = 3 * (controlFirst.x - pointFirst.x);
			var b:Number = (3 * (controlSecond.x - controlFirst.x)) - g;
			var a:Number = pointSecond.x - pointFirst.x - b - g;
			var dx:Number = (3*a*t*t + 2*b*t + g);
			g = 3 * (controlFirst.y - pointFirst.y);
			b = (3 * (controlSecond.y - controlFirst.y)) - g;
			a = pointSecond.y - pointFirst.y - b - g;
			var dy:Number = (3*a*t*t + 2*b*t + g);
			var tg:Tangent = new Tangent();
			tg.point = getBezierValue(t);
			tg.line = LineEquation.getLineFrom2Points(tg.point, 
				new Point(tg.point.x + dx, tg.point.y + dy));
			return tg;
		}

		public const SEGMENTS:Number = 4;
		public const STEP:Number = 1/SEGMENTS;
		
		[ArrayElementType("com.anychart.valuePlot.series.splineSeries.bezierCurves.BezierCurveQuadratic")]
		public var quadraticCurvesLeft:Array;
		[ArrayElementType("com.anychart.valuePlot.series.splineSeries.bezierCurves.BezierCurveQuadratic")]
		public var quadraticCurvesRight:Array;
		
		private var pushDelegate:Function;
		
		private function pushToLeft(q:BezierCurveQuadratic, x:Number):void {
			if (q.containsX(x)) {
				quadraticCurvesRight.push(q.divide(x));
				pushDelegate = pushToRight;
			} else if (x == q.start.x) {
				pushDelegate = pushToRight;
				quadraticCurvesRight.push(q);
				return;
			} else if (x == q.end.x) {
				pushDelegate = pushToRight;
				quadraticCurvesLeft.push(q);
				return;
			}
			quadraticCurvesLeft.push(q);
		}
		
		private function pushToRight(q:BezierCurveQuadratic, x:Number):void {
			quadraticCurvesRight.push(q);
		}
		
		private static const tolerance:Number = 0.01;
		
		public function buildQuadraticBezier(divideX:Number):void {
			quadraticCurvesLeft = [];
			quadraticCurvesRight = [];
			pushDelegate = pushToLeft;

			var nextTangent:Tangent;
			var curentTangent:Tangent = new Tangent();
			curentTangent.point = pointFirst;
			curentTangent.line = LineEquation.getLineFrom2Points(pointFirst, controlFirst);

			var testLine:LineEquation = LineEquation.getLineFrom2Points(pointFirst, pointSecond);
			if (testLine.containsPoint(controlFirst, tolerance) 
					&& testLine.containsPoint(controlSecond, tolerance)) {
				var q:BezierCurveQuadratic = new BezierCurveQuadratic();
				//*
				q.start = new Point(pointFirst.x, pointFirst.y);
				q.end = new Point(pointSecond.x, pointSecond.y);
				/*/
				q.end = pointSecond;
				q.start = pointFirst;
				//*/
				q.control = new Point((q.start.x + q.end.x)/2, (q.start.y + q.end.y)/2);
				pushDelegate(q, divideX);
			} else {
				for (var i:Number = 1; i < SEGMENTS + 1; i++) {
					nextTangent = getCubicTangent(i*STEP);
					getQuadraticSlice((i-1)*STEP, i*STEP, curentTangent, nextTangent, 0, divideX);
					curentTangent = nextTangent;
				}
			}
		}
		
		private static const RECURSE_NUM_MAX:uint = 9;
		
		private function getQuadraticSlice(t0:Number, t1:Number, curentTangent:Tangent, nextTangent:Tangent, recurseNum:Number, divideX:Number):void {
			if (LineEquation.isEqualsLines(curentTangent.line, nextTangent.line)) return;
			if (recurseNum > RECURSE_NUM_MAX)  {
				var quadraticLine:BezierCurveQuadratic = new BezierCurveQuadratic();
				quadraticLine.start = curentTangent.point;
				quadraticLine.end = nextTangent.point;
				quadraticLine.control = new Point(
					(curentTangent.point.x + nextTangent.point.x) / 2,
					(curentTangent.point.y + nextTangent.point.y) / 2);
				pushDelegate(quadraticLine, divideX);
				return;
			}
			var controllPoint:Point = LineEquation.getCrossPoint(curentTangent.line, nextTangent.line);
			var d:Number = MathUtils.getDistance(curentTangent.point, nextTangent.point);
			if ((controllPoint == null) || 
					(MathUtils.getDistance(controllPoint, curentTangent.point) > d) ||
					(MathUtils.getDistance(controllPoint, nextTangent.point) > d)) {
				var tMid:Number = (t0 + t1) / 2;
				var midleTangent:Tangent = getCubicTangent(tMid);
				getQuadraticSlice(t0, tMid, curentTangent, midleTangent, recurseNum + 1, divideX);
				getQuadraticSlice(tMid, t1, midleTangent, nextTangent, recurseNum + 1, divideX);
			} else {
				var quadraticCurve:BezierCurveQuadratic = new BezierCurveQuadratic();
				quadraticCurve.start = curentTangent.point;
				quadraticCurve.end = nextTangent.point;
				quadraticCurve.control = controllPoint;
				pushDelegate(quadraticCurve, divideX);
			}
		}

	}
}