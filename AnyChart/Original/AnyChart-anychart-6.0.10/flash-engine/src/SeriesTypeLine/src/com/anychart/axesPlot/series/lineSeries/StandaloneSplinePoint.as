package com.anychart.axesPlot.series.lineSeries
{
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.utils.LineEquation;
	import com.anychart.utils.MathUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	public class StandaloneSplinePoint extends Point
	{
		public var prev:StandaloneSplinePoint;
		public var next:StandaloneSplinePoint;

		public var controlFirst:Point = new Point();
		public var controlSecond:Point = new Point();
		
		private var bezierPrev:BezierCurveCubic;
		private var createConrolPointsDelegate:Function;

		public function initDelegates():void {
			createConrolPointsDelegate = getCreateConrolPointsDelegate();
		}
		
		private function getCreateConrolPointsDelegate():Function {
			if (prev == null && next == null) return createControlPointsByNothing;
			if (prev == null && next != null) return createControlPointsByPointNext;
			bezierPrev = new BezierCurveCubic();
			bezierPrev.pointFirst = prev;
			bezierPrev.pointSecond = this;
			bezierPrev.controlFirst = prev.controlSecond;
			bezierPrev.controlSecond = controlFirst;
			if (next == null) return createControlPointsByPointPrev;
			return createControlPointsNormal;
		}
		
		private static const KR:Number = 0.4;

		private static const GRAD_TO_RAD:Number = Math.PI / 180;
		private static const RAD_TO_GRAD:Number = 180 / Math.PI;
		private static const RAD_90:Number = 90 * GRAD_TO_RAD;
		private static const RAD_180:Number = 180 * GRAD_TO_RAD;
		private static const RAD_270:Number = 270 * GRAD_TO_RAD;
		private static const RAD_360:Number = 360 * GRAD_TO_RAD;
		
		private function createControlPointsNormal():void {
			var l1:Number;
			var l2:Number;
			var angle:Number;

			var minY:Number = Math.min(prev.y, next.y);
			var maxY:Number = Math.max(prev.y, next.y);
			var minX:Number = Math.min(prev.x, next.x);
			var maxX:Number = Math.max(prev.x, next.x);
			
			if ((y >= maxY || y <= minY) && x > minX && x < maxX) {
				l1 = Math.abs(x - prev.x);
				l2 = Math.abs(x - next.x);
				angle = RAD_270;
			} else if ((x >= maxX || x <= minX) && y > minY && y < maxY) {
				l1 = Math.abs(y - prev.y);
				l2 = Math.abs(y - next.y);
				angle = 0;
			} else if (y > minY && y < maxY && x > minX && x < maxX) {
				var line:LineEquation = LineEquation.getLineFrom2Points(prev, next);
				var lineParallel:LineEquation = line.getParallelLine(this);
				angle = Math.atan2(prev.x - next.x, prev.y - next.y);
				if (line.isLeftPoint(this) == (maxY == next.y)) {
					l1 = MathUtils.getDistance(this, lineParallel.getPointByX(prev.x));
					l2 = MathUtils.getDistance(this, lineParallel.getPointByY(next.y));
				} else {
					l1 = MathUtils.getDistance(this, lineParallel.getPointByY(prev.y));
					l2 = MathUtils.getDistance(this, lineParallel.getPointByX(next.x));
				}
			} else {
				angle = Math.atan2(prev.x - next.x, prev.y - next.y);
				var l:LineEquation = LineEquation.getLineFrom2Points(prev, next);
				var d:Number = l.getDistance(this);
				d *= d;
				l1 = MathUtils.getDistance(prev, this);
				l1 = Math.sqrt(l1 * l1 - d);

				l2 = MathUtils.getDistance(next, this);
				l2 = Math.sqrt(l2 * l2 - d);
				/////////////////
				// TO DO !!
				//if (MathUtils.getDistance(pointPrev.pointValue, pointNext.pointValue) < l1 + l2) { }
				////////////////
			}
			
			l1 *= KR;
			l2 *= KR;

			controlFirst.x = x + l1 * Math.sin(angle);
			controlFirst.y = y + l1 * Math.cos(angle);
			controlSecond.x = x + l2 * Math.sin(angle + RAD_180);
			controlSecond.y = y + l2 * Math.cos(angle + RAD_180);
		}
		
		private function createControlPointsByPointNext():void {
			controlFirst.x = x;
			controlFirst.y = y;
			controlSecond.x = x + (next.x - x) * KR;
			controlSecond.y = y;
			leftPoint.x = x;
			leftPoint.y = y;
		}

		private function createControlPointsByPointPrev():void {
			controlFirst.x = x - (x - prev.x) * KR;
			controlFirst.y = y;
			controlSecond.x = x;
			controlSecond.y = y;
			rightPoint.x = x;
			rightPoint.y = y;
		}

		private function createControlPointsByNothing():void {
			controlFirst.x = x;
			controlFirst.y = y;
			controlSecond = controlFirst;
			rightPoint.x = x;
			rightPoint.y = y;
		}
		
		public function createControlPoints():void {
			createConrolPointsDelegate();
			createCurves();
		}

		[ArrayElementType("com.anychart.axesPlot.series.lineSeries.BezierCurveQuadratic")]
		internal var curvesLeft:Array = [];
		[ArrayElementType("com.anychart.axesPlot.series.lineSeries.BezierCurveQuadratic")]
		internal var curvesRight:Array = [];
		
		internal var leftPoint:Point = new Point();
		internal var rightPoint:Point = new Point();

		private function createCurves():void {
			if (bezierPrev == null || prev == null) return;
//trace(x+") Left:", (prev.x + x) * 0.5);
//var st:String = bezierPrev.toString();
			bezierPrev.buildQuadraticBezier((prev.x + x) * 0.5);
			curvesLeft = bezierPrev.quadraticCurvesRight;
			prev.curvesRight = bezierPrev.quadraticCurvesLeft;
			if (curvesLeft.length > 0) {
				var curve:BezierCurveQuadratic = BezierCurveQuadratic(curvesLeft[0]);
				leftPoint.x = curve.start.x;
				leftPoint.y = curve.start.y;
			} else {
				leftPoint.x = x;
				leftPoint.y = y;
			}
			if (prev.curvesRight.length > 0) {
				prev.rightPoint.x = leftPoint.x;
				prev.rightPoint.y = leftPoint.y;
			}
//st != bezierPrev.toString() ? trace("XYU!!") : null;
		}
		
		private static var pt:Point = new Point();
		private static var pt1:Point = new Point();

		private function drawCurves(g:Graphics, curves:Array, hasLeft:Boolean, xAxis:Axis, yAxis:Axis):void {
			var i:uint;
			var curve:BezierCurveQuadratic;
			var ln:uint = curves.length;
			if (ln > 0) {
				curve = BezierCurveQuadratic(curves[0]);
				xAxis.rotateValue(curve.start.x, pt);
				yAxis.rotateValue(curve.start.y, pt);

				hasLeft ? g.lineTo(pt.x, pt.y) : g.moveTo(pt.x, pt.y);
				
				for (i = 0; i < ln; i++) {
					curve = BezierCurveQuadratic(curves[i]);
					xAxis.rotateValue(curve.control.x, pt);
					yAxis.rotateValue(curve.control.y, pt);
	
					xAxis.rotateValue(curve.end.x, pt1);
					yAxis.rotateValue(curve.end.y, pt1); 

					g.curveTo(pt.x, pt.y, pt1.x, pt1.y);
				}
			}
		}
		
		public function draw(g:Graphics, xAxis:Axis, yAxis:Axis):void {
			//drawBoundPoint(g, xAxis, yAxis);
//trace(x+")", curvesLeft.length, curvesLeft);
			drawCurves(g, curvesLeft, false, xAxis, yAxis);
			drawCurves(g, curvesRight, curvesLeft.length > 0, xAxis, yAxis);
			//drawControlLines(g, xAxis, yAxis);
		}

		
		public function drawBackward(g:Graphics, xAxis:Axis, yAxis:Axis, lineToOnStart:Boolean = false):void {
			 if (prev == null && next == null) return;
			//drawBoundPoint(g, xAxis, yAxis);
			//trace(curvesLeft.length, curvesRight.length);
			//g.lineStyle(1, 0xFF0000);
			
			var i:int;
			var curve:BezierCurveQuadratic;
			var curves:Array = curvesRight;
			var ln:uint = curves.length;
			var hasRight:Boolean = ln > 0; 
			// Почему тут было  ln>1 ? без этого работает, с ним - нет..
			if (ln > 0) {
				curve = BezierCurveQuadratic(curves[ln-1]);
				xAxis.rotateValue(curve.end.x, pt);
				yAxis.rotateValue(curve.end.y, pt);

				lineToOnStart ?  g.lineTo(pt.x, pt.y)  : g.moveTo(pt.x, pt.y) ;
				for (i = ln - 1; i >= 0; i--) {
					curve = BezierCurveQuadratic(curves[i]);
					xAxis.rotateValue(curve.control.x, pt);
					yAxis.rotateValue(curve.control.y, pt);
	
					xAxis.rotateValue(curve.start.x, pt1);
					yAxis.rotateValue(curve.start.y, pt1); 

					g.curveTo(pt.x, pt.y, pt1.x, pt1.y);
				} 
			}
			
			curves = curvesLeft;
			ln = curves.length;
			if (ln > 0) {
				curve = BezierCurveQuadratic(curves[ln-1]);
				xAxis.rotateValue(curve.end.x, pt);
				yAxis.rotateValue(curve.end.y, pt);
				
				lineToOnStart  ?   g.lineTo(pt.x, pt.y)  : g.moveTo(pt.x, pt.y) ;
				g.lineTo(pt.x, pt.y);
				for (i = ln - 1; i >= 0; i--) {
					curve = BezierCurveQuadratic(curves[i]);

					xAxis.rotateValue(curve.control.x, pt);
					yAxis.rotateValue(curve.control.y, pt);
	
					xAxis.rotateValue(curve.start.x, pt1);
					yAxis.rotateValue(curve.start.y, pt1); 

					g.curveTo(pt.x, pt.y, pt1.x, pt1.y);
				}
			}
			g.lineStyle();
		}

/*
		private function drawBoundPoint(g:Graphics, xAxis:Axis, yAxis:Axis):void {
//trace(leftPoint, rightPoint);
			var p2:Point = new Point();
			xAxis.rotateValue(leftPoint.x, p2);
			yAxis.rotateValue(leftPoint.y, p2);
			g.lineStyle(2, 0xFF0000);
			g.drawCircle(p2.x, p2.y, 2);

			xAxis.rotateValue(rightPoint.x, p2);
			yAxis.rotateValue(rightPoint.y, p2);
			g.lineStyle(2, 0x0000FF);
			g.drawCircle(p2.x, p2.y, 4);
		}

		private function drawControlLines(g:Graphics, xAxis:Axis, yAxis:Axis):void {
			var p1:Point = new Point();
			var p2:Point = new Point();

			xAxis.rotateValue(x, p1);
			yAxis.rotateValue(y, p1);
		
			xAxis.rotateValue(controlFirst.x, p2);
			yAxis.rotateValue(controlFirst.y, p2);
			g.lineStyle(1, 0x880000);
			g.moveTo(p1.x, p1.y);
			g.lineTo(p2.x, p2.y);
			g.drawCircle(p2.x, p2.y, 2);

			xAxis.rotateValue(controlSecond.x, p2);
			yAxis.rotateValue(controlSecond.y, p2);
			g.lineStyle(1, 0xFF8800);
			g.moveTo(p1.x, p1.y);
			g.lineTo(p2.x, p2.y);
			g.drawCircle(p2.x, p2.y, 2);
			
			
			g.lineStyle(1, 0x000088);
			g.drawCircle(p1.x, p1.y, 3);
			g.endFill();
		}
//*/
	}
}