package com.anychart.axesPlot.series.barSeries.programmaticStyles
{
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.IBarAquaContainer;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class LightAquaBarStyle extends ProgrammaticStyle {
		// Properties.
		protected var fillGrd:Gradient;
		protected var topShadeGrd:Gradient;
		protected var bottomShadeGrd:Gradient;
		protected var blikGradient:Gradient;
		
		override public function initialize(state:StyleState):void 
		{
			initDynamicIndexes(state,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			
			// Initialize main background gradient.
			{
				this.fillGrd=new Gradient(1);
				var entries:Array=[];
 				var opacity:Number=1;
				entries.push(createKey(0, darkIndex,opacity,true));
            	entries.push(createKey(0.05,darkIndex,opacity,true));
            	entries.push(createKey(0.85,lightIndex,opacity,true));
            	entries.push(createKey(0.85,lightIndex,opacity,true));
            	entries.push(createKey(1,darkIndex,opacity,true));
	            createDynamicGradient(state,this.fillGrd,entries);
	  		}
	  		
	  		// Initialize top shade gradient.
	  		{
	  			this.topShadeGrd=new Gradient(1);
	  			this.topShadeGrd.angle=90;
	  			entries=[];
	  			opacity=1;
	  			entries.push(createKey(0,darkIndex,1*opacity,true));
				entries.push(createKey(1,darkIndex,0,true));
				createDynamicGradient(state,this.topShadeGrd,entries);
	  		}
	  		
	  		// Initialize bottom shade gradient.
	  		{
	  			this.bottomShadeGrd=new Gradient(1);
	  			this.bottomShadeGrd.angle=90;
	  			entries=[];
	  			opacity=1;
	  			entries.push(createKey(0,darkIndex,0,true));
				entries.push(createKey(1,darkIndex,1*opacity,true));
				createDynamicGradient(state,this.bottomShadeGrd,entries);
	  		}
	  		
	  		// Initialize blik gradient.
	  		{
	  			this.blikGradient=new Gradient(1);
	  			entries=[];
	  			opacity=1;
	  			
	  			entries.push(createKey(0,0xFFFFFF,0));
	            entries.push(createKey(0.2,0xFFFFFF,Number(160.0/255.0)*opacity));
	            entries.push(createKey(0.25,0xFFFFFF,Number(140.0/255.0)*opacity));
	            entries.push(createKey(0.3,0xFFFFFF,Number(30.0/255.0)*opacity));
	            entries.push(createKey(0.35,0xFFFFFF,0));
	            entries.push(createKey(1,0xFFFFFF,0));
	            
	            blikGradient.entries=entries;
	            
	  		}
		}
		
		override public function draw(data:Object):void {
			var point:AxesPlotPoint = AxesPlotPoint(data);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			var color:uint = state.fill.color;
			if (state.fill.isDynamic)
				color = ColorParser.getInstance().getDynamicColor(color,point.color);
			
			var rad:Number = 0;
			
			var topShadeBounds:Rectangle = point.bounds.clone();
			var bottomShadeBounds:Rectangle = point.bounds.clone();
			
			var isAllCornersVisible:Boolean = IBarAquaContainer(point).isAllCornersVisible();
			var isLeftTopCornerVisible:Boolean = isAllCornersVisible;
			var isRightTopCornerVisible:Boolean = isAllCornersVisible;
			var isLeftBottomCornerVisible:Boolean = isAllCornersVisible;
			var isRightBottomCornerVisible:Boolean = isAllCornersVisible;
			
			var isCornersVisible:Boolean = IBarAquaContainer(point).isCornersCanBeVisible();
            
            if (point.isHorizontal) {
            	this.fillGrd.angle = 90;
				this.topShadeGrd.angle = 0;
				this.bottomShadeGrd.angle = 0;
				this.blikGradient.angle = 90;
				
                topShadeBounds.width = point.bounds.height*.2;
                bottomShadeBounds.width = topShadeBounds.width;
                bottomShadeBounds.x = point.bounds.right - bottomShadeBounds.width;
                
                if (isCornersVisible) {
                	rad = point.bounds.height * 0.12;
                	if (point.bounds.width < rad) 
                		rad = point.bounds.width;
                	if (!isAllCornersVisible) {
                		if (point.isInverted) {
                		    isLeftTopCornerVisible = true;
	                		isLeftBottomCornerVisible = true;
	                	}else {
	                		isRightTopCornerVisible = true;
	                		isRightBottomCornerVisible = true;
	                	}
                	}
                }
            }else {
                topShadeBounds.height = topShadeBounds.width*.2;
                bottomShadeBounds.height = topShadeBounds.height;
                bottomShadeBounds.y = point.bounds.bottom - bottomShadeBounds.height;
                
                if (isCornersVisible) {
                	rad = point.bounds.width * 0.12;
                	if (point.bounds.height < rad) 
                		rad = point.bounds.height;
                	if (!isAllCornersVisible) {
                		if (point.isInverted) {
	                		isLeftBottomCornerVisible = true;
	                		isRightBottomCornerVisible = true;   
	                	}else {
	                		isLeftTopCornerVisible = true;
	                		isRightTopCornerVisible = true;
	                	}
                	}
                }
            }
            
            //rad = Math.max(rad,1);  

			var rc:Rectangle = point.bounds;
			
			g.lineStyle();
			
			// Draw main background.
			{
				this.fillGrd.beginGradientFill(g,point.bounds,color);
				if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
				g.endFill();
				
			}
			
			var isBottomGrdVisible:Boolean = IBarAquaContainer(point).isBottomGradientVisible();
			
			var isTopVisible:Boolean = (!point.isHorizontal) || (point.isInverted);
			var isBottomVisible:Boolean = (isBottomGrdVisible && !point.isHorizontal) || (point.isHorizontal && !point.isInverted);
			
			// Draw top shade.
			if (isTopVisible && (isCornersVisible || isAllCornersVisible)) {
				rc=topShadeBounds;
				this.topShadeGrd.beginGradientFill(g,rc,color);
				if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
				g.endFill();
			}
			
			// Draw bottom shade.
			if (isBottomVisible && (isCornersVisible || isAllCornersVisible)) {
				rc = bottomShadeBounds;
				this.bottomShadeGrd.beginGradientFill(g,rc,color);
				if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
				g.endFill();
			}
			
			rc = point.bounds;
			
			// Draw hatch fill
            if (state.hatchFill != null && state.hatchFill.enabled) {
            	state.hatchFill.beginFill(g, point.hatchType, point.color);
            	if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
            	g.endFill();
            }
			
			// Draw top blik.
			{
				this.blikGradient.beginGradientFill(g,point.bounds);
				if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
				g.endFill();
			}
		}
	}
}