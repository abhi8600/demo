package com.anychart.axesPlot.series.barSeries.drawing {
	public interface IBarDrawingFactory {
		function create(shapeType:uint):IBarDrawerBase;
	}
}