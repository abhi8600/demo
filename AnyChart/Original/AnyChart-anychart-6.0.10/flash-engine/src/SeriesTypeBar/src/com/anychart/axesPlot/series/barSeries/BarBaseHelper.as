package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.visual.corners.Corners;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class BarBaseHelper {
		
		private static var leftTop:Point = new Point();
		private static var rightBottom:Point = new Point();
		
		internal static function setPixelPosition(point:AxesPlotPoint, startValue:Number, endValue:Number):void {
			var series:AxesPlotSeries = AxesPlotSeries(point.series);
			
			var w:Number = IBarCustomWidthSupport(point.global).getBarWidth(point)/2;
					
			series.argumentAxis.transform(point, point.x, leftTop,-w);
			series.valueAxis.transform(point, startValue, leftTop);
			
			series.argumentAxis.transform(point, point.x, rightBottom,w);
			series.valueAxis.transform(point, endValue, rightBottom);
			
			if (point.z) {
				point.z.transform(leftTop);
				point.z.transform(rightBottom);
			}
			
			point.bounds.x = Math.min(leftTop.x, rightBottom.x);
			point.bounds.y = Math.min(leftTop.y, rightBottom.y);
			point.bounds.width = Math.max(leftTop.x, rightBottom.x) - point.bounds.x;
			point.bounds.height = Math.max(leftTop.y, rightBottom.y) - point.bounds.y;
		}
		
		internal static function drawBox(g:Graphics, point:AxesPlotPoint, state:BarStateStyle):void {
			if (point.styleState.programmaticStyle != null) {
				point.styleState.programmaticStyle.draw(point);
			}else {
				if (state.fill != null)
					state.fill.begin(g,point.bounds,point.color);
				
				if (state.hatchFill != null) {
					drawRect(g, point.bounds, state.corners);
					state.hatchFill.beginFill(g,point.hatchType,point.color);
				}
				
				if (state.stroke != null)
					state.stroke.apply(g, point.bounds,point.color);
					
				drawRect(g, point.bounds, state.corners);
				
				if (state.stroke != null)
					g.lineStyle();
			}
			if (point.hasPersonalContainer) 
				point.container.filters = (state.effects != null) ? state.effects.list : null;
		}
		
		private static function drawRect(g:Graphics, rect:Rectangle, corners:Corners):void {
			if (corners == null)
				g.drawRect(rect.x, rect.y, rect.width, rect.height);
			else
				corners.drawRect(g, rect);
		}
	}
}