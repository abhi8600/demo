package com.anychart.axesPlot.series.barSeries.programmaticStyles{
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.stroke.Stroke;
	
	
	public final class ConeProgramaticStyle extends CylinderProgrammaticStyle{
		
		override public function initialize(state:StyleState):void {
			this.initDynamicIndexes(state,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			
			var opacity:Number=BackgroundBaseStyleState(state).fill ? BackgroundBaseStyleState(state).fill.opacity : 1;
			
			this.fillGrd = new Gradient(opacity);
			var entries:Array=[];
 			
			entries.push(createKey(0, darkIndex,opacity,true));
        	entries.push(createKey(0.41,lightIndex,opacity,true));
        	entries.push(createKey(1,darkIndex,opacity,true));
            this.createDynamicGradient(state,this.fillGrd,entries);
            
            this.border = new Stroke();
            this.border.color = darkIndex;
            this.border.isDynamic = true; 
		}
	}
}