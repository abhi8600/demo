package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	
	public interface IBarCustomWidthSupport {
		function getBarWidth(point:AxesPlotPoint):Number;
	}
}