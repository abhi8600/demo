package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.data.RangePoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class RangeAreaPoint extends RangePoint {
		
		public var needCalcPixValue:Boolean;
		private var isValuePointsInitialized:Boolean;
		private var startPixValuePt:Point;
		private var endPixValuePt:Point;
		public var needCreateDrawingPoints:Boolean;
		public var needInitializePixValues :Boolean;
		
		public function RangeAreaPoint() {
			this.needCalcPixValue = true;
			this.isValuePointsInitialized = false;
			this.hasPrev = false;
			this.hasNext = false;
			this.startPixValuePt = new Point();
			this.endPixValuePt = new Point();
			this.needCreateDrawingPoints = true;
			this.needInitializePixValues = true;
		}
		
		override protected function addContainerToSeriesContainer():void {
			RangeAreaSeries(this.series).container.addChild(this.container);
		}
		
		override public function initialize():void {
			super.initialize();
			var s:RangeAreaSeries = RangeAreaSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		private function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}
		
		public var isDrawable:Boolean;
		public var startDrawingPoints:Array;
		public var endDrawingPoints:Array;
		
		private var hasPrev:Boolean;
		private var hasNext:Boolean;
		
		public function doCreateDrawingPoints ():void {
			this.createDrawingPoints();
			this.needCreateDrawingPoints = false;
		}
		
		private function createDrawingPoints():void {
			if (!this.needCreateDrawingPoints) return;
			
			this.startDrawingPoints = [];
			this.endDrawingPoints = [];
			
			var tmp:Point;
			var tmp1:Point;
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].startPixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.startPixValuePt.x)/2;
				tmp1.y = (tmp.y + this.startPixValuePt.y)/2;
				this.startDrawingPoints.push(tmp1);
			}
			
			this.startDrawingPoints.push(this.startPixValuePt);
			
			if (this.hasNext) {
				this.series.points[this.index+1].initializePixValues();
				this.series.points[this.index+1].needCalcPixValue = false;
				tmp = this.series.points[this.index+1].startPixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.startPixValuePt.x)/2;
				tmp1.y = (tmp.y + this.startPixValuePt.y)/2;
				this.startDrawingPoints.push(tmp1);
				
				tmp = this.series.points[this.index+1].endPixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.endPixValuePt.x)/2;
				tmp1.y = (tmp.y + this.endPixValuePt.y)/2;
				this.endDrawingPoints.push(tmp1);
			}
			
			this.endDrawingPoints.push(this.endPixValuePt);
			
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].endPixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.endPixValuePt.x)/2;
				tmp1.y = (tmp.y + this.endPixValuePt.y)/2;
				this.endDrawingPoints.push(tmp1);
			}
		}
		
		public function doInitializePixValues():void {
			this.initializePixValues();
			this.needInitializePixValues = false;
		}
		
		private function initializePixValues():void {
			if (!this.needInitializePixValues) return;
			
			var series:RangeAreaSeries = RangeAreaSeries(this.series);
			
			if (!this.isValuePointsInitialized) {
				this.hasPrev = (this.index > 0) && 
							  (this.series.points[this.index - 1] != null) &&
							  (!this.series.points[this.index - 1].isMissing);
				this.hasNext = (this.index < (this.series.points.length-1)) &&
							  (this.series.points[this.index + 1] != null) &&
							  (!this.series.points[this.index + 1].isMissing);
				this.isDrawable = this.hasPrev || this.hasNext;
				this.isValuePointsInitialized = true;
			}
			
			series.argumentAxis.transform(this, this.x, this.startPixValuePt);
			series.valueAxis.transform(this, this.start, this.startPixValuePt);
			
			series.argumentAxis.transform(this, this.x, this.endPixValuePt);
			series.valueAxis.transform(this, this.end, this.endPixValuePt);
			
			if (this.z) {
				this.z.transform(this.startPixValuePt);
				this.z.transform(this.endPixValuePt);
			}
		}
		
		override protected final function execDrawing():void {
			
			if (this.needCalcPixValue)
				this.initializePixValues();
				
			this.bounds.x = Math.min(this.startPixValuePt.x, this.endPixValuePt.x);
			this.bounds.y = Math.min(this.startPixValuePt.y, this.endPixValuePt.y);
			this.bounds.width = Math.max(this.startPixValuePt.x, this.endPixValuePt.x) - this.bounds.x;
			this.bounds.height = Math.max(this.startPixValuePt.y, this.endPixValuePt.y) - this.bounds.y;
			
			if (this.isDrawable) {
				this.createDrawingPoints();
				
				super.execDrawing();
				this.drawSegment(this.container.graphics);
			}
		}
		
		override protected function redraw():void {
			if (this.isDrawable) {
				super.redraw();
				this.drawSegment(this.container.graphics);
			}
		} 
		
		protected function drawSegment(g:Graphics):void {
			
			var s:RangeAreaStyleState = RangeAreaStyleState(this.styleState);
			
			var gradientBounds:Rectangle = new Rectangle();
			if (RangeAreaSeries(this.series).needCreateGradientRect)
				gradientBounds = RangeAreaSeries(this.series).gradientBounds;
			if (s.stroke != null)
				s.stroke.apply(g, gradientBounds, this.color);
			
			if (s.fill != null)
				s.fill.begin(g, gradientBounds, this.color);
			
			var i:uint = 1;
			g.moveTo(this.startDrawingPoints[0].x, this.startDrawingPoints[0].y);
			for (i = 1;i<this.startDrawingPoints.length;i++) 
				g.lineTo(this.startDrawingPoints[i].x, this.startDrawingPoints[i].y);
			
			g.lineStyle();
			g.lineTo(this.endDrawingPoints[0].x, this.endDrawingPoints[0].y);
			
			if (s.endStroke != null)
				s.endStroke.apply(g, null, this.color);
			
			for (i = 1;i<this.endDrawingPoints.length;i++)
				g.lineTo(this.endDrawingPoints[i].x, this.endDrawingPoints[i].y);
			g.lineStyle();
			g.endFill();
			
			if (s.hatchFill != null) {
				s.hatchFill.beginFill(g, this.hatchType, this.color);
				g.moveTo(this.startDrawingPoints[0].x, this.startDrawingPoints[0].y);
				for (i = 1;i<this.startDrawingPoints.length;i++) 
					g.lineTo(this.startDrawingPoints[i].x, this.startDrawingPoints[i].y);
				g.lineTo(this.endDrawingPoints[0].x, this.endDrawingPoints[0].y);
				for (i = 1;i<this.endDrawingPoints.length;i++)
					g.lineTo(this.endDrawingPoints[i].x, this.endDrawingPoints[i].y);
				g.endFill();
			}	
		} 
	}
}