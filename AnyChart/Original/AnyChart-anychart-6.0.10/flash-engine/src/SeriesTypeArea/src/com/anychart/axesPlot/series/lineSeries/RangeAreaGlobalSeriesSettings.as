package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.data.RangeSeries;
	import com.anychart.axesPlot.data.settings.RangeGlobalSeriesSettings;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class RangeAreaGlobalSeriesSettings extends RangeGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:RangeSeries;
			switch (seriesType) {
				case SeriesType.RANGE_AREA:
					series = new RangeAreaSeries();
					break;
				case SeriesType.RANGE_SPLINE_AREA:
					series = new RangeSplineAreaSeries();
					break;
			}
			series.type = seriesType;
			series.clusterKey = SeriesType.RANGE_AREA;
			return series;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSettings():BaseSeriesSettings {
			return new LineSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return RangeAreaStyleState;
		}
		
		override public function getSettingsNodeName():String {
			return "range_area_series";
		}
		
		override public function getStyleNodeName():String {
			return "range_area_style";
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { return true; }
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			var dc:uint = ColorUtils.getDarkColor(item.color);
			g.lineStyle(1, dc, 1);
			g.beginFill(item.color, 1);
			this.doDrawIconTop(seriesType, g, bounds);
			g.lineStyle();
			g.lineTo(bounds.right,bounds.bottom);
			g.lineStyle(1, dc, 1);
			this.doDrawIconBottom(seriesType, g, bounds);
			g.endFill();
		}
		
		private function doDrawIconTop(seriesType:uint, g:Graphics, bounds:Rectangle):void {
			var d:Number = bounds.height/4;
			g.moveTo(bounds.x, bounds.y);
			if (seriesType == SeriesType.RANGE_AREA) {
				g.lineTo(bounds.x + bounds.width/2, bounds.y + d);
				g.lineTo(bounds.right, bounds.y);
			}else {
				g.curveTo(bounds.x + bounds.width/2, bounds.y + bounds.height/2, bounds.right, bounds.y);
			}
		}
		
		private function doDrawIconBottom(seriesType:uint, g:Graphics, bounds:Rectangle):void {
			if (seriesType == SeriesType.RANGE_AREA) {
				var d:Number = bounds.height/4;
				g.lineTo(bounds.x + bounds.width/2, bounds.bottom - d);
				g.lineTo(bounds.x, bounds.bottom);
			}else {
				g.curveTo(bounds.x + bounds.width/2, bounds.y + bounds.height/2, bounds.x, bounds.bottom);
			}
			g.lineStyle();
			g.lineTo(bounds.x, bounds.y);
		}
		
		override public function pointsCanBeAnimated():Boolean { return false; }
		override public function seriesCanBeAnimated():Boolean { return true; }
	}
}