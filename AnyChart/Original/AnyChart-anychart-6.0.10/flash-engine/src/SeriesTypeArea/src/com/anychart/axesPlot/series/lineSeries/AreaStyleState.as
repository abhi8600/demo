package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.stroke.Stroke;
	
	internal final class AreaStyleState extends BackgroundBaseStyleState {
		
		public function AreaStyleState(style:Style) {
			super(style);
		}

		override protected function deserializeStroke(data:XML):void {
			if (SerializerBase.isEnabled(data.line[0])) {
				this.stroke = new Stroke();
				this.stroke.deserialize(data.line[0],this.style);
			}
		}
	}
}