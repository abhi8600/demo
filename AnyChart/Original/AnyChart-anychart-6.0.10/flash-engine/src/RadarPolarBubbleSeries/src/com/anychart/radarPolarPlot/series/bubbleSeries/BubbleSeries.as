package com.anychart.radarPolarPlot.series.bubbleSeries {
	
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class BubbleSeries extends RadarPolarPlotSeries {
		
		override public function createPoint():BasePoint {
			return new BubblePoint();
		}
	}
}