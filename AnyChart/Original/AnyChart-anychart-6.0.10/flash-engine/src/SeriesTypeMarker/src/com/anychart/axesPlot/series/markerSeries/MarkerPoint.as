package com.anychart.axesPlot.series.markerSeries {
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.selector.HitTestUtil;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class MarkerPoint extends StackablePoint {
		
		private var pixValuePt:Point;
		
		override protected function execDrawing():void {
			this.pixValuePt = new Point();
			var series:AxesPlotSeries = AxesPlotSeries(this.series);
			series.argumentAxis.transform(this, this.x, this.pixValuePt);
			series.valueAxis.transform(this, this.stackValue, this.pixValuePt);
			if (this.z) this.z.transform(this.pixValuePt);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			pos.x = pixValuePt.x;
			pos.y = pixValuePt.y;
		}
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, elementIndex:int, resources:ResourcesLoader):MarkerElement {
			if (data.@style != undefined)
				if (data.marker[0] == null)
					data.marker[0] = <marker style={data.@style} />;
				else 
					data.marker[0].@style = data.@style;
				
			if (data.marker[0] != null) {
				if (marker.needCreateElementForPoint(data.marker[0]))
					marker = MarkerElement(marker.createCopy());
				marker.deserializePoint(data.marker[0], this, styles, elementIndex,resources);
				
				if (data.marker[0].@style != undefined) {
					this.settings = this.settings.createCopy(this.settings);
					this.settings.style = marker.style;
				}
			}
			marker.enabled = true;
			this.checkPointElementPosition(marker, elementIndex);
			return marker;
		}
		
		//---------------------------------------------------------------
		//				Hit test
		//---------------------------------------------------------------
		
		override public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			var tmp:Shape = new Shape();
			tmp.graphics.beginFill(0xFF0000, 1);
			tmp.graphics.drawRect(0,0, area.width, area.height);
			tmp.x = area.x;
			tmp.y = area.y;
			this.container.parent.addChild(tmp);
			
			var res:Boolean = HitTestUtil.hitTest(this.markerSprite, tmp);
			
			this.container.parent.removeChild(tmp);
			tmp = null;
			return res;
		}
	}
}