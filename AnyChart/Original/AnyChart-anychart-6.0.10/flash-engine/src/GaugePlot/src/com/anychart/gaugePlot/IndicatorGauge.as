package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.frame.GaugeFrame;
	import com.anychart.gaugePlot.frame.LinearGaugeFrame;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	internal final class IndicatorGauge extends GaugeBase {
		
		private var pointer:IndicatorPointer;
		
		override public function serialize(res:Object=null):Object {
			res = super.serialize(res);
			if (this.pointer)
				res.pointer = this.pointer.serialize();
			return res;
		}
		
		override protected function createFrame(data:XML):GaugeFrame {
			return new LinearGaugeFrame();
		}
		
		override protected function setBounds(newBounds:Rectangle):void {
			super.setBounds(newBounds);
			this.frame.autoFit(this.bounds);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data,resources);
			if (SerializerBase.isEnabled(data.marker[0])) {
				
				this.pointer = new IndicatorPointer();
				this.pointer.gauge = this;
				if (this.pointer.label != null)
					this.pointer.label.gauge = this;
				this.pointer.deserialize(data.marker[0], resources, localStyles, data.styles[0], this.plot.chart);
			}
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			super.initialize(bounds);
			if (this.pointer != null)
				this.contentContainer.addChild(this.pointer.initialize());
			return this.container;
		}
		
		override public function draw():void {
			super.draw();
			if (this.pointer != null)
				this.pointer.draw();
		}
		
		override public function resize(newBounds:Rectangle, setBounds:Boolean=true):void {
			super.resize(newBounds, setBounds);
			if (this.pointer != null)
				this.pointer.resize();
		}
		
		override public function setPointerValue(value:Object, pointerName:String=null):void {
			if (this.pointer)
				this.pointer.udpateValue(value);
		}
	}
}