package com.anychart.gaugePlot.axis {
	import com.anychart.gaugePlot.AxisGaugeBase;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.labels.GaugeAxisLabels;
	import com.anychart.gaugePlot.axis.markers.AxisMarkersList;
	import com.anychart.gaugePlot.axis.scales.LinearGaugeScale;
	import com.anychart.gaugePlot.axis.scales.LogGaugeScale;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.circular.RangeBarCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.linear.RangeBarLinearGaugePointer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	public class GaugeAxis {
		
		public var enabled:Boolean;

		public var gauge:AxisGaugeBase;
		
		public var pixelWidth:Number;
		public var width:Number;
		
		public var scale:BaseScale;
		protected var scaleBar:ScaleBar;
		protected var extraScaleBar:ScaleBar;
		protected var minorTickmark:Tickmark;
		protected var majorTickmark:Tickmark;
		public var labels:GaugeAxisLabels;
		protected var markers:AxisMarkersList;
		
		private var tickmarksContainer:Sprite;
		private var markersContainer:Sprite;
		
		protected function createLabels():GaugeAxisLabels { return null; }
		protected function createMarkers():AxisMarkersList { return null; }
		
		public function GaugeAxis():void {
			this.width = 0;
			this.enabled = true;
		}
		
		public function deserialize(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			if (data.@size != undefined) this.width = GaugePlotSerializerBase.getSimplePercent(data.@size);
			
			if (data.scale[0] != null) {
				this.scale = (data.scale[0].@type != undefined && SerializerBase.getEnumItem(data.scale[0].@type) == "logarithmic") ?
								new LogGaugeScale() : new LinearGaugeScale();
				this.scale.deserialize(data.scale[0]);
			}else {
				this.scale = new LinearGaugeScale();
			}
			
			if (!this.enabled) return;
			
			if (SerializerBase.isEnabled(data.scale_bar[0])) {
				this.scaleBar = new ScaleBar(false);
				this.scaleBar.deserialize(data.scale_bar[0], this.width, resources);
				this.gauge.setBackgroundRotation(this.scaleBar.background);
			}
			if (SerializerBase.isEnabled(data.scale_line[0])) {
				this.extraScaleBar = new ScaleBar(true);
				this.extraScaleBar.deserialize(data.scale_line[0], this.width, resources);
				this.gauge.setBackgroundRotation(this.extraScaleBar.background);
			}
			
			if (SerializerBase.isEnabled(data.major_tickmark[0])) {
				this.majorTickmark = new Tickmark();
				this.majorTickmark.deserialize(data.major_tickmark[0], this.width, resources);
				this.gauge.setBackgroundRotation(this.majorTickmark.background);
			}
			
			if (SerializerBase.isEnabled(data.minor_tickmark[0])) {
				this.minorTickmark = new Tickmark();
				this.minorTickmark.deserialize(data.minor_tickmark[0], this.width, resources);
				this.gauge.setBackgroundRotation(this.minorTickmark.background);
			}
			
			this.labels = this.createLabels();
			this.labels.axis = this;
			if (SerializerBase.isEnabled(data.labels[0])) {
				this.labels.deserialize(data.labels[0], this.width, resources);
			}
			
			this.markers = this.createMarkers();
			this.markers.axis = this;
			this.markers.scale = this.scale;
			this.markers.deserialize(data, localStyles, styles, this.width, resources);
		}
		
		public function checkPoint(point:GaugePointer):void {
			this.checkScale(point.value);
			if (point is RangeBarCircularGaugePointer) {
				this.checkScale(RangeBarCircularGaugePointer(point).endValue);
			}else if (point is RangeBarLinearGaugePointer) { 
				this.checkScale(RangeBarLinearGaugePointer(point).endValue);
			}
		}
		
		internal final function checkScale(value:Number):void {
			if (isNaN(scale.dataRangeMinimum) || value < scale.dataRangeMinimum)
				scale.dataRangeMinimum = value;
			if (isNaN(scale.dataRangeMaximum) || value > scale.dataRangeMaximum)
				scale.dataRangeMaximum = value;
		}
		
		protected var container:Sprite;		
		public var beforeMarkersContainer:Sprite;
		public var afterMarkersContainer:Sprite;
		
		public function initialize():DisplayObject {
			this.container = new Sprite();
			
			if (this.scaleBar != null)
				this.container.addChild(this.scaleBar.initialize());
				
			if (this.extraScaleBar != null)
				this.container.addChild(this.extraScaleBar.initialize());
			
			this.beforeMarkersContainer = new Sprite();
			this.container.addChild(this.beforeMarkersContainer);
			
			if (this.majorTickmark != null)
				this.container.addChild(this.majorTickmark.initialize());
			if (this.minorTickmark != null)
				this.container.addChild(this.minorTickmark.initialize());
			if (this.labels != null && this.labels.enabled)
				this.container.addChild(this.labels.initialize(this));
			if (this.markers != null)
				this.container.addChild(this.markers.initialize());
			
			this.afterMarkersContainer = new Sprite();
			this.container.addChild(this.afterMarkersContainer);	
			
			return this.container;
		}
		
		public function initializeSize():void {}
		
		public function draw():void {
			if (!this.enabled) return;
			if (this.scaleBar != null)
				this.drawScaleBar(this.scaleBar);
			if (this.extraScaleBar != null)
				this.drawScaleBar(this.extraScaleBar); 
			if (this.minorTickmark != null)
				this.drawMinorTickmarks();
			if (this.majorTickmark != null)
				this.drawMajorTickmarks();
			if (this.labels.enabled) {
				this.labels.clear();
				this.labels.draw(this.scale);
			}
			if (this.markers != null) {
				this.markers.clear();
				this.markers.draw();
			}
		}
		
		public function resize():void {
			this.initializeSize();
			this.draw();
		}

		
		protected function drawScaleBar(scaleBar:ScaleBar):void {}
		
		private function drawMinorTickmarks():void {
			this.minorTickmark.clear();
			var min:Number = this.scale.linearize(this.scale.minimum);
			var max:Number = this.scale.linearize(this.scale.maximum);
			for (var i:uint = 0;i<=this.scale.majorIntervalsCount;i++) {
				var majorValue:Number = this.scale.getMajorIntervalValue(i);
				for (var j:uint = 1;j<this.scale.minorIntervalsCount;j++) {
					var minorValue:Number = this.scale.getMinorIntervalValue(majorValue, j);
					if (minorValue < min || minorValue > max) continue; 
					this.drawTickmark(this.minorTickmark, this.scale.localTransform(minorValue));
				}
			}
		}
		
		private function drawMajorTickmarks():void {
			this.majorTickmark.clear();
			var cnt:int = this.scale.majorIntervalsCount;
			for (var i:int = 0;i<=cnt;i++) {
				this.drawTickmark(this.majorTickmark,this.scale.localTransform(this.scale.getMajorIntervalValue(i))); 
			}
		}
		
		public function drawTickmark(tickmark:Tickmark, value:Number, color:uint = 0):void {}
	}
}