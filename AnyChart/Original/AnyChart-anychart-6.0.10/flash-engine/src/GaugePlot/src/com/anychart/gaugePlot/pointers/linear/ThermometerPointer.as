package com.anychart.gaugePlot.pointers.linear { 
	import com.anychart.gaugePlot.LinearGauge;
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.linear.ThermometerPointerStyleState;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class ThermometerPointer extends LinearGaugePointer {
		override protected function getStyleNodeName():String {
			return "thermometer_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return ThermometerPointerStyleState;
		}
		
		override protected function checkMargin():void {
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			var state:ThermometerPointerStyleState = ThermometerPointerStyleState(this.currentState.style.normal);
			this.checkStateMargin(ThermometerPointerStyleState(this.style.normal));
			this.checkStateMargin(ThermometerPointerStyleState(this.style.hover));
			this.checkStateMargin(ThermometerPointerStyleState(this.style.pushed));
			this.checkStateMargin(ThermometerPointerStyleState(this.style.selectedNormal));
			this.checkStateMargin(ThermometerPointerStyleState(this.style.selectedHover));
			var capSize:Number = (state.isCapRadiusAxisSideDepend ? axis.width : 1)*state.capRadius*2 + state.capPadding;
			
			if (axis.scale.inverted)
				LinearGauge(this.gauge).endMargin = Math.max(LinearGauge(this.gauge).endMargin, capSize);
			else
				LinearGauge(this.gauge).startMargin = Math.max(LinearGauge(this.gauge).startMargin, capSize);
		}
		
		private function checkStateMargin(state:ThermometerPointerStyleState):void {
			var percentW:Number = (state.isWidthAxisSizeDepend ? axis.width : 1)*state.width;
			var percentCapRadius:Number = (state.isCapRadiusAxisSideDepend ? axis.width : 1)*state.capRadius;
			if (percentCapRadius < (percentW/2)) {
				state.capRadius = (percentW/2)/(state.isCapRadiusAxisSideDepend ? axis.width : 1); 
			}
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var state:ThermometerPointerStyleState = ThermometerPointerStyleState(this.currentState);
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			
			var percentW:Number = (state.isWidthAxisSizeDepend ? axis.width : 1)*state.width;
			var percentCapRadius:Number = (state.isCapRadiusAxisSideDepend ? axis.width : 1)*state.capRadius;
			
			var start:Number = axis.getComplexPosition(state.align, state.padding, percentW, true);
			var end:Number = axis.getComplexPosition(state.align, state.padding, percentW, false);
			
			var valStart:Number = axis.scale.transform(axis.scale.minimum);
			var actualVal:Number = axis.scale.transform(this.fixValueScaleOut(this.value));
			
			var targetSize:Number = axis.getTargetSize();
			var r:Number = targetSize*percentCapRadius;
			var w:Number = targetSize*percentW;
			var padding:Number = targetSize*state.capPadding;
			
			var bounds:Rectangle = new Rectangle();
			this.setThermometerBounds(start, end, valStart, actualVal, r, padding, bounds);
			
			if (axis.scale.inverted)
				valStart += padding;
			else
				valStart -= padding;
			
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, bounds, this._color);
				
			if (state.stroke != null)
				state.stroke.apply(g, bounds, this._color);
				
			this.drawThermometer(g, start, end, valStart, actualVal, r,w);
			g.lineStyle();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, this._hatchType, this._color);
				this.drawThermometer(g, start, end, valStart, actualVal, r,w);
			}
			
			if (state.bulb != null) {
				var center:Point = this.getBublCenter(start, end, valStart, w, r, axis);
				bounds = new Rectangle();
				bounds.x = center.x - r;
				bounds.y = center.y - r;
				bounds.width = bounds.height = r*2;
				if (state.bulb.fill != null)
					state.bulb.fill.begin(g, bounds, this.color);
				if (state.bulb.border != null)
					state.bulb.border.apply(g, bounds, this.color);
				g.drawCircle(center.x, center.y, r);
				g.lineStyle();
				if (state.bulb.hatchFill != null) {
					state.bulb.hatchFill.beginFill(g, this.hatchType, this.color);
					g.drawCircle(center.x, center.y, r);
				}
			}
			
		}
		
		private function setThermometerBounds(start:Number, end:Number, valStart:Number, actualVal:Number, r:Number, capPadding:Number, bounds:Rectangle):void {
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			if (axis.isHorizontal) {
				bounds.y = (start+end)/2 - r;
				bounds.height = r*2;
				if (axis.scale.inverted) {
					bounds.x = actualVal;
					bounds.width = valStart + r*2 + capPadding - bounds.x;
				}else {
					bounds.x = valStart - r*2 - capPadding;
					bounds.width = actualVal - bounds.x;
				}
			}else {
				bounds.x = (start+end)/2 - r;
				bounds.width = r*2;
				if (axis.scale.inverted) {
					bounds.y = actualVal;
					bounds.height = valStart + r*2 - bounds.y;
				}else {
					bounds.y = valStart - r*2;
					bounds.height = actualVal - bounds.y;
				}
			}
		}
		
		private function drawThermometer(g:Graphics, start:Number, end:Number, valStart:Number, actualVal:Number, r:Number, w:Number):void {
			if (LinearGaugeAxis(this.axis).isHorizontal) {
				this.drawHorizontalThermometer(g, start, end, valStart, actualVal, r, w, this.axis.scale.inverted);
			}else {
				this.drawVerticalThermometer(g, start, end, valStart, actualVal, r, w, this.axis.scale.inverted);
			}
		}
		
		private function getBublCenter(start:Number, end:Number, valStart:Number, w:Number, r:Number, axis:LinearGaugeAxis):Point {
			var dA:Number = Math.abs(Math.asin(w/(r*2))*180/Math.PI);
			
			var center:Point = new Point();
			if (axis.isHorizontal) {
				center.y = (start+end)/2;
				center.x = axis.scale.inverted ? (valStart + (1/Math.tan(dA*Math.PI/180))*(end-start)/2) : 
												 (valStart - (1/Math.tan(dA*Math.PI/180))*(end-start)/2);
			}else {
				center.x = (start+end)/2;
				center.y = axis.scale.inverted ? (valStart + (1/Math.tan(dA*Math.PI/180))*(end-start)/2) :
												 ( valStart - (1/Math.tan(dA*Math.PI/180))*(end-start)/2);
			}
			
			return center;
		}
		
		private function drawHorizontalThermometer(g:Graphics, start:Number, end:Number, valStart:Number, actualVal:Number, r:Number, w:Number, inverted:Boolean):void {
			//sin(dA) = w/(2r) 
			var dA:Number = Math.abs(Math.asin(w/(r*2))*180/Math.PI);
			var angle1:Number;
			var angle2:Number;
			
			var center:Point = this.getBublCenter(start, end, valStart, w, r, LinearGaugeAxis(this.axis));
			
			if (inverted) {
				angle1 = 180 - dA+360;
				angle2 = 180 + dA;
			}else {
				angle1 = dA;
				angle2 = 360 - dA;
			}
			
			var pt1:Point = new Point(actualVal, start);
			var pt2:Point = new Point(actualVal, end);
			var pt3:Point = new Point(valStart, end);
			var pt4:Point = new Point(valStart, start);
			
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
			g.lineTo(pt3.x, pt3.y);
			DrawingUtils.drawArc(g, center.x, center.y, angle1, angle2, r, r, 0, false);
			g.lineTo(pt4.x, pt4.y);
			g.lineTo(pt1.x, pt1.y);
			
			this.bounds.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x);
			this.bounds.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y);
			this.bounds.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x) - this.bounds.x;
			this.bounds.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y) - this.bounds.y;
			
			g.endFill();
		}
		
		private function drawVerticalThermometer(g:Graphics, start:Number, end:Number, valStart:Number, actualVal:Number, r:Number, w:Number, inverted:Boolean):void {
			//sin(dA) = w/(2r) 
			var dA:Number = Math.abs(Math.asin(w/(r*2))*180/Math.PI);
			var angle1:Number;
			var angle2:Number;
			
			var center:Point = this.getBublCenter(start, end, valStart, w, r, LinearGaugeAxis(this.axis));
			if (inverted) {
				angle1 = 270 - dA + 360;
				angle2 = 270 + dA;
			}else {
				angle1 = 90 + dA;
				angle2 = 90 - dA + 360;
			}
			
			var pt1:Point = new Point(start, actualVal);
			var pt2:Point = new Point(end, actualVal);
			var pt3:Point = new Point(end,valStart);
			var pt4:Point = new Point(start,valStart);
			
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
			g.lineTo(pt3.x, pt3.y);
			DrawingUtils.drawArc(g, center.x, center.y, angle2, angle1, r, r, 0, false);
			
			g.lineTo(pt4.x, pt4.y);
			g.lineTo(pt1.x, pt1.y);
			
			this.bounds.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x);
			this.bounds.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y);
			this.bounds.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x) - this.bounds.x;
			this.bounds.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y) - this.bounds.y;
			
			g.endFill();
		}
	}
}