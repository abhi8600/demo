package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	internal final class GaugeAxisRangeMarker extends GaugeAxisMarker {
		internal var start:Number;
		internal var end:Number;
		
		public function GaugeAxisRangeMarker() {
			super();
		}
		
		override protected function getStyleNodeName():String { return "color_range_style"; }
		override protected function getStyleStateClass():Class { return ColorRangeStyleState; }
		
		override public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):void {
			super.deserialize(data, axisWidth, resources, stylesList, localStyles);
			if (data.@start != undefined) this.start = SerializerBase.getNumber(data.@start);
			if (data.@end != undefined) this.end = SerializerBase.getNumber(data.@end);
		}
		
		override public function checkScale(scale:BaseScale):void {
			this.checkScaleValue(this.start, scale);
			this.start=(start>scale.maximum)? scale.maximum : (start < scale.minimum) ? scale.minimum : this.start;
			this.checkScaleValue(this.end, scale);
			this.end=(end>scale.maximum)? scale.maximum : (end < scale.minimum) ? scale.minimum : this.end;
		}
	}
}