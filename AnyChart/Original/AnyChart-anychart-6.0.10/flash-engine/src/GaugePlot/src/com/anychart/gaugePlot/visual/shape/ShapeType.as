package com.anychart.gaugePlot.visual.shape {
	
	public final class ShapeType /* extends MarkerType */ {
		public static const RECTANGLE:uint = 17;
		public static const TRIANGLE:uint = 18;
		public static const TRAPEZOID:uint = 19;
		public static const PENTAGON:uint = 20;
		public static const LINE:uint = 21;
		public static const H_LINE:uint = 22;
	}
}