package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.fill.FillType;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.gradient.GradientEntry;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class CircularGaugeAxisMarkersList extends AxisMarkersList {
		
		override protected function drawLineMarker(marker:GaugeAxisLineMarker):void {
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			var state:TrendlineStyleState = TrendlineStyleState(marker.getActualState());
			
			var size:Number = (state.isSizeAxisSizeDepend ? axis.width : 1) * state.size; 
			
			var startR:Number = axis.getComplexRadius(state.align, state.padding, size, true);
			var endR:Number = axis.getComplexRadius(state.align, state.padding, size, false);
			
			var startP:Point = new Point();
			var endP:Point = new Point();
			
			var angle:Number = this.scale.transform(marker.value);
			
			axis.setPixelPoint(startR, angle, startP);
			axis.setPixelPoint(endR, angle, endP);
			
			marker.draw(startP, endP);
			
			if (state.label != null)
				this.drawMarkerLabel(marker, state.label, marker.value, 0);
		}
		
		override protected function drawRangeMarker(marker:GaugeAxisRangeMarker):void {
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			
			var state:ColorRangeStyleState = ColorRangeStyleState(marker.style.normal);
			
			var startPercentW:Number = (state.isStartSizeAxisSizeDepend ? axis.width : 1)*state.startWidth;
			var endPercentW:Number =  (state.isEndSizeAxisSizeDepend ? axis.width : 1)*state.endWidth;
			
			var baseStartR:Number = axis.getComplexRadius(state.align, state.padding, startPercentW, true);
			var baseEndR:Number = axis.getComplexRadius(state.align, state.padding, endPercentW, true);
			var startR:Number = axis.getComplexRadius(state.align, state.padding, startPercentW, false);
			var endR:Number = axis.getComplexRadius(state.align, state.padding, endPercentW, false);
			
			var startAngle:Number = this.scale.transform(marker.start);
			var endAngle:Number = this.scale.transform(marker.end);
			
			var g:Graphics = marker.container.graphics;
			
			if (state.fill != null) {
				if (state.fill.type != FillType.GRADIENT) {
					if (state.hatchFill == null && state.stroke != null)
						state.stroke.apply(g,null,marker.color);
					state.fill.begin(g,null,marker.color);
					this.doDrawRangeMarker(g, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle);
				}else {
					this.drawGradientRangeMarker(g, state.fill.gradient, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle, marker.color); 
					if (state.hatchFill == null && state.stroke != null) {
						state.stroke.apply(g,null,marker.color);
						this.doDrawRangeMarker(g, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle);
					}
				}
			}else if (state.hatchFill == null && state.stroke != null) {
				state.stroke.apply(g,null,marker.color);
				this.doDrawRangeMarker(g, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle);
			}
			g.lineStyle();
			if (state.hatchFill != null) {
				if (state.stroke != null)
					state.stroke.apply(g,null,marker.color);
				state.hatchFill.beginFill(g,0,marker.color);
				this.doDrawRangeMarker(g, baseStartR, baseEndR, startR, endR, axis, startAngle, endAngle);
				g.lineStyle();
			}
			
			if (state.label != null)
				this.drawMarkerLabel(marker, state.label, (marker.start+marker.end)/2, marker.color);
		}
		
		private function doDrawRangeMarker(g:Graphics,
										   baseStartR:Number,
										   baseEndR:Number,
										   startR:Number,
										   endR:Number, 
										   axis:CircularGaugeAxis,
										   startAngle:Number, 
										   endAngle:Number):void {
			var center:Point = CircularGauge(this.axis.gauge).pixPivotPoint;
			if (baseStartR == baseEndR) {
				DrawingUtils.drawArc(g, center.x, center.y, startAngle, endAngle, baseStartR, baseStartR, 0, true);
			}else {
				var pt:Point = new Point();
				axis.setPixelPoint(baseStartR, startAngle, pt);
				g.moveTo(pt.x, pt.y);
				this.drawDynamicArc(g, axis, startAngle, endAngle, baseStartR, baseEndR);
			}
			if (endR == startR) {
				DrawingUtils.drawArc(g, center.x, center.y, endAngle, startAngle, endR, endR, 0, false);
			}else {
				this.drawDynamicArc(g, axis, endAngle, startAngle, endR, startR);
			}
			g.endFill();
		}
		
		private function drawGradientRangeMarker(g:Graphics,
												 grd:Gradient, 
												 baseStartR:Number, baseEndR:Number, 
												 startR:Number, endR:Number,
												 axis:CircularGaugeAxis, 
												 startAngle:Number, endAngle:Number,
												 markerColor:uint):void {
			g.lineStyle();		
			g.beginFill(0,0);	
			
			var sweepAngle:Number = endAngle - startAngle;
			
			for (var i:int = 0;i<(grd.entries.length-1);i++) {
				
				var startRatio:Number = grd.ratios[i]/0xFF;
				var endRatio:Number = grd.ratios[i+1]/0xFF
				
				var currEntry:GradientEntry = GradientEntry(grd.entries[i]);
				var nextEntry:GradientEntry = GradientEntry(grd.entries[i+1]);
				
				var startColor:uint = currEntry.isDynamic ? ColorParser.getInstance().getDynamicColor(currEntry.color, markerColor) : currEntry.color;
				var endColor:uint = nextEntry.isDynamic ? ColorParser.getInstance().getDynamicColor(nextEntry.color, markerColor) : nextEntry.color;
				
				this.drawGradientBlock(g, axis, 
									   startAngle + sweepAngle * startRatio, 
									   startAngle + sweepAngle * endRatio,
									   baseStartR + (baseEndR - baseStartR)*startRatio,
									   baseStartR + (baseEndR - baseStartR)*endRatio,
									   startR + (endR - startR)*startRatio,
									   startR + (endR - startR)*endRatio,
									   startColor,
									   grd.entries[i].alpha,
									   endColor,
									   grd.entries[i+1].alpha);
			}
		}
		
		private function drawGradientBlock(g:Graphics, axis:CircularGaugeAxis, 
										   startAngle:Number, endAngle:Number,
										   startBaseR:Number, endBaseR:Number,
										   startR:Number, endR:Number,
										   color1:uint, alpha1:Number,
										   color2:uint, alpha2:Number):void {
			var centerAngle:Number = (startAngle + endAngle)/2;
			var centerBaseRadius:Number = (startBaseR + endBaseR)/2;
			var centerR:Number = (startR + endR)/2;
			
			var pt1:Point = new Point();
			var pt2:Point = new Point();
			var pt3:Point = new Point();
			var pt4:Point = new Point();
			
			axis.setPixelPoint(startBaseR, startAngle, pt1);
			axis.setPixelPoint(endBaseR, endAngle, pt2);
			axis.setPixelPoint(endR, endAngle, pt3);
			axis.setPixelPoint(startR, startAngle, pt4);
			
			var bounds:Rectangle = new Rectangle();
			bounds.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x);
			bounds.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y);
			bounds.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x) - bounds.x;
			bounds.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y) - bounds.y;
			
			var colors:Array = [color1, color2];
			var ratios:Array = [0, 0xFF];
			var alphas:Array = [alpha1, alpha2];
			
			var m:Matrix = new Matrix();
			m.createGradientBox(bounds.width, bounds.height, (centerAngle+90)*Math.PI/180, bounds.x, bounds.y);
			g.beginGradientFill(GradientType.LINEAR, colors, alphas, ratios, m);
			this.drawDynamicArc(g, axis, startAngle, centerAngle, startBaseR, centerBaseRadius, true);
			this.drawDynamicArc(g, axis, centerAngle, startAngle, centerR, startR, false);
			g.endFill();
			
			g.beginGradientFill(GradientType.LINEAR, colors, alphas, ratios, m);
			this.drawDynamicArc(g, axis, centerAngle, endAngle, centerBaseRadius, endBaseR, true);
			this.drawDynamicArc(g, axis, endAngle, centerAngle, endR, centerR, false);
			g.endFill();
	    }
		
		private function drawDynamicArc(g:Graphics, axis:CircularGaugeAxis, startAngle:Number, endAngle:Number, startR:Number, endR:Number,needMoveTo:Boolean = false):void {
			var angle:Number;
			var r:Number;
			var dR:Number = (endR - startR)/(endAngle-startAngle);
			var pt:Point = new Point();
			if (endAngle > startAngle) {
				for (angle = startAngle;angle<=endAngle;angle++) {
					r = startR + dR*(angle - startAngle);
					axis.setPixelPoint(r, angle, pt);
					if (needMoveTo && angle == startAngle)
						g.moveTo(pt.x, pt.y);
					else
						g.lineTo(pt.x, pt.y);
				}
			}else {
				for (angle = startAngle;angle>=endAngle;angle--) {
					r = startR + dR*(angle - startAngle);
					axis.setPixelPoint(r, angle, pt);
					if (needMoveTo && angle == startAngle)
						g.moveTo(pt.x, pt.y);
					else
						g.lineTo(pt.x, pt.y);
				}
			}
			axis.setPixelPoint(endR, endAngle, pt);
			g.lineTo(pt.x, pt.y);
			pt = null;
		}
		
		override protected function getLabelPosition(pos:Point, pixVal:Number, align:uint, padding:Number):void {
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis); 
			var r:Number = axis.getRadius(align, padding, 0);
			axis.setPixelPoint(r, pixVal, pos);
		}
		
	}
}