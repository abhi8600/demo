package com.anychart.gaugePlot.pointers {
	import com.anychart.IAnyChart;
	import com.anychart.actions.ActionsList;
	import com.anychart.animation.Animation;
	import com.anychart.animation.interpolation.InterpolationFactory;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.events.PointEvent;
	import com.anychart.events.ValueChangeEvent;
	import com.anychart.gaugePlot.AxisGaugeBase;
	import com.anychart.gaugePlot.GaugeBase;
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.elements.LabelElement;
	import com.anychart.gaugePlot.elements.TooltipElement;
	import com.anychart.gaugePlot.events.GaugePointerTransormation;
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.palettes.BasePalette;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	import com.anychart.styles.states.StyleState;
	import com.anychart.styles.states.StyleStateWithEffects;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class GaugePointer implements IElementContainer {
		
		public var value:Number;
		public var selected:Boolean;
		public var type:uint;
		public var name:String;
		public var editable:Boolean;
		public var selectable:Boolean;
		public var useHandCursor:Boolean;
		
		public var axis:GaugeAxis;
		
		private var _customAttributes:Object;
		private var actions:ActionsList;
		
		public function get customAttributes():Object
		{
			return _customAttributes;
		}
		
		public var style:Style;
		protected var currentState:StyleState;
		
		public var gauge:GaugeBase;
		
		public var animation:Animation;
		
		public var strValue:String;
		
		public function GaugePointer() {
			this.bounds = new Rectangle();
			this.selected = false;
			this._elementsInfo = {};
			this.isOver = false;
			this.container = new Sprite();
			this.editable = false;
			this.selectable = false;
		}
		
		public function serialize(res:Object = null):Object {
			if (res == null) res = {};
			res.name = this.name;
			res.value = this.value;
			return res;
		}
		
		protected var _color:int = 15820860;
		protected var _hatchType:int;
		
		public function get color():int { return this._color; }
		public function set color(value:int):void {this._color = value; }
		public function get hatchType():int { return this._hatchType; }
		public function set hatchType(value:int):void { this._hatchType = value; }
		public function get markerType():int { return 0; }
		
		//--------------------------------------------------------------
		//	Elements
		//--------------------------------------------------------------
		
		private static const LABEL_INDEX:uint = 0;
		private static const TOOLTIP_INDEX:uint = 1;
		
		public var label:LabelElement;
		public var tooltip:TooltipElement;
		
		private var _elementsInfo:Object;
		public function get elementsInfo():Object { return this._elementsInfo; }
		public function set elementsInfo(value:Object):void { this._elementsInfo = value; }
		
		public function get hasPersonalContainer():Boolean { return true; }
		
		public function getElementPosition(state:BaseElementStyleState, sprite:Sprite, bounds:Rectangle):Point {
			if (state == null) return null;
			var pos:Point = new Point();
			this.getAnchorPoint(state.anchor, pos);
			this.applyHAlign(pos, state.hAlign,bounds,state.padding);
			this.applyVAlign(pos, state.vAlign,bounds,state.padding);
			return pos;
		}
		
		public function getFixedElementPosition(state:BaseElementStyleState, sprite:Sprite, bounds:Rectangle, x:Number, y:Number ):Point {
			if (state == null) return null;
			var pos:Point = new Point();
			pos.x = this.gauge.bounds.x + this.gauge.bounds.width*x;
			pos.y = this.gauge.bounds.y + this.gauge.bounds.height*y;
			this.applyHAlign(pos, state.hAlign,bounds,state.padding);
			this.applyVAlign(pos, state.vAlign,bounds,state.padding);
			return pos;
		}
		
		private function applyHAlign(pos:Point, hAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (hAlign) {
				case HorizontalAlign.LEFT:
					pos.x -= elementSize.width + padding;
					break;
				case HorizontalAlign.RIGHT:
					pos.x += padding;
					break;
				case HorizontalAlign.CENTER:
					pos.x -= elementSize.width/2;
					break;
			}
		}
		
		private function applyVAlign(pos:Point, vAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (vAlign) {
				case VerticalAlign.TOP:
					pos.y -= elementSize.height + padding;
					break;
				case VerticalAlign.BOTTOM:
					pos.y += padding;
					break;
				case VerticalAlign.CENTER:
					pos.y -= elementSize.height/2;
					break;
			}
		}
		
		public function getAnchorPoint(anchor:uint, pos:Point):void {
			switch (anchor) {
				case Anchor.CENTER:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_BOTTOM:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.CENTER_LEFT:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_RIGHT:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_TOP:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y;
					break;
				case Anchor.LEFT_BOTTOM:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.LEFT_TOP:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y;
					break;
				case Anchor.RIGHT_BOTTOM:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.RIGHT_TOP:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y;
					break;
			}
		}
		
		public function getMainContainer():IMainElementsContainer { return this.gauge; }
		
		//--------------------------------------------------------------
		//	Deserialization
		//--------------------------------------------------------------
		
		protected function getStyleNodeName():String { return null; }
		protected function getStyleStateClass():Class { return null; }
		
		public function deserialize(data:XML, resources:ResourcesLoader, localStyles:StylesList, stylesList:XML, chart:IAnyChart):void {
			this.setStyle(data, resources, stylesList, localStyles);
			if (data.@name != undefined) this.name = SerializerBase.getString(data.@name);
			if (data.name[0] != null) this.name = SerializerBase.getCDATAString(data.name[0]);
			if (data.@value != undefined) this.value = SerializerBase.getNumber(data.@value);
			if (data.@selected != undefined) this.selected = SerializerBase.getBoolean(data.@selected);
			if (data.@editable != undefined) this.editable = SerializerBase.getBoolean(data.@editable);
			if (data.@use_hand_cursor != undefined) this.useHandCursor = SerializerBase.getBoolean(data.@use_hand_cursor);
			if (data.@allow_select != undefined) this.selectable = SerializerBase.getBoolean(data.@allow_select);
			if (data.attributes[0] != null) {
				this._customAttributes = {};
				var attributesList:XMLList = data.attributes[0].attribute;
				var attributesCnt:int = attributesList.length();
				for (var i:int = 0;i<attributesCnt;i++) {
					var attr:XML = attributesList[i];
					if (attr.@name == undefined) continue;
					this._customAttributes['%'+attr.@name] = SerializerBase.getCDATAString(attr);
				}
			}
			if (data.actions[0] != null) {
				this.actions = new ActionsList(chart, this);
				this.actions.deserialize(data.actions[0]);
			}
			if (data.@color != undefined) this._color = SerializerBase.getColor(data.@color);
			if (data.@hatch_type != undefined) this._hatchType = SerializerBase.getHatchType(data.@hatch_type);
			
			if (data.label[0] != null)
				this.label.deserializeGlobal(data.label[0],stylesList, resources);
			
			if (data.tooltip[0] != null)
				this.tooltip.deserializeGlobal(data.tooltip[0],stylesList, resources);
			
			this.initializeStyle();
			
			if (SerializerBase.isEnabled(data.animation[0])) {
				if (this.animation == null)
					this.animation = new Animation();
				this.animation.deserialize(data.animation[0]);
			}
			
			if (this.value) this.strValue=this.value.toString();
		}
		
		private function initializeStyle():void {
			
			this.setStyleDynamicData(this.style);
			
			if (this.label != null)
				this.setStyleDynamicData(this.label.style);
				
			if (this.tooltip != null)
				this.setStyleDynamicData(this.tooltip.style);
			
			if (this.gauge is AxisGaugeBase) {
				var gauge:AxisGaugeBase = AxisGaugeBase(this.gauge);
				GaugePointerStyleState(this.style.normal).initializeRotation(gauge);
				GaugePointerStyleState(this.style.hover).initializeRotation(gauge);
				GaugePointerStyleState(this.style.pushed).initializeRotation(gauge);
				GaugePointerStyleState(this.style.selectedNormal).initializeRotation(gauge);
				GaugePointerStyleState(this.style.selectedHover).initializeRotation(gauge);
			}
		}
		
		private function setStyleDynamicData(style:Style):void {
			style.setDynamicColors(this._color);
			style.setDynamicGradients(this._color);
			style.setDynamicHatchTypes(this._hatchType);
		}
		
		protected function getInlineStyleName():String { return this.getStyleNodeName(); }
		
		private function setStyle(data:XML, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):void {
			var styleName:String = data.@style == undefined ? "anychart_default" : String(data.@style).toLowerCase(); 
			
			var styleNodeName:String = this.getStyleNodeName();
			
			var styleData:XML = (this.getInlineStyleName() != null) ? data[this.getInlineStyleName()][0] : data;
			var actualStyleXML:XML;
			
			if (styleData != null) {
				actualStyleXML = StylesList.getStyleXMLByMerge(stylesList, styleNodeName, styleData ,styleName);
			}else {
				var cashedStyle:Style = localStyles.getStyle(styleNodeName, styleName);
				if (cashedStyle != null) {
					this.style = cashedStyle;
					return;
				}else {
					actualStyleXML = StylesList.getStyleXMLByApplicator(stylesList, styleNodeName, styleName, localStyles);
				}
			}
			
			if (actualStyleXML == null) {
				this.style = localStyles.getStyle(styleNodeName, "anychart_default");
			}else {
				this.style = new Style(this.getStyleStateClass());
				this.style.deserialize(actualStyleXML,resources);
				
				if (styleData == null)
					localStyles.addStyle(styleNodeName, styleName, style);
			}
		}
		
		//--------------------------------------------------------------
		//	Formatting
		//--------------------------------------------------------------
		
		public function getTokenValue(token:String):* {
			if (token == '%Value') return this.strValue;
			if (token == '%Name') return this.name;
			if (this._customAttributes[token] != null)
				return this._customAttributes[token];
			return '';
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		//--------------------------------------------------------------
		//	Interactivity
		//--------------------------------------------------------------
		
		private function startValueChange():void {
			this.container.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.container.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.container.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
			this.container.addEventListener(MouseEvent.MOUSE_OVER, this.valueChangeMouseOver);
			this.container.addEventListener(MouseEvent.MOUSE_OUT, this.valueChangeMouseOut);
			
			if (this.labelContainer != null) {
				this.labelContainer.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
				this.labelContainer.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
				this.labelContainer.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
				this.labelContainer.addEventListener(MouseEvent.MOUSE_OVER, this.valueChangeMouseOver);
				this.labelContainer.addEventListener(MouseEvent.MOUSE_OUT, this.valueChangeMouseOut);
			}
		}
		private function valueChangeMouseOver(event:MouseEvent):void {
			this.isOver = true;
		}
		private function valueChangeMouseOut(event:MouseEvent):void {
			this.isOver = false;
		}
		protected function valueChange(mouseX:Number, mouseY:Number):void { }
		private function stopValueChange():void {
			this.container.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.container.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.container.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
			this.container.removeEventListener(MouseEvent.MOUSE_OVER, this.valueChangeMouseOver);
			this.container.removeEventListener(MouseEvent.MOUSE_OUT, this.valueChangeMouseOut);
			
			if (this.labelContainer != null) {
				this.labelContainer.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
				this.labelContainer.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
				this.labelContainer.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
				this.labelContainer.removeEventListener(MouseEvent.MOUSE_OVER, this.valueChangeMouseOver);
				this.labelContainer.removeEventListener(MouseEvent.MOUSE_OUT, this.valueChangeMouseOut);
			}
			if (!this.isOver)
				this.mouseOutHandler(null);
			else
				this.mouseOverHandler(null);
		}
		
		private var isOver:Boolean;
		
		public function deselect():void {
			this.selected = false;
			this.mouseOutHandler(null);
		}
		
		protected final function initListeners(target:Sprite):void {
			target.mouseChildren = false;
			target.buttonMode = true;
			target.useHandCursor = this.useHandCursor;
			this.setListeners(target);
		}
		
		protected final function clearListeners(target:Sprite):void {
			target.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			target.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			target.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			target.removeEventListener(MouseEvent.CLICK, this.mouseClickHandler);
			target.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
		}
		
		protected final function setListeners(target:Sprite):void {
			target.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			target.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			target.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			target.addEventListener(MouseEvent.CLICK, this.mouseClickHandler);
			target.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
		}
		
		private function mouseOverHandler(event:MouseEvent):void {
			this.isOver = true;
			if (event != null)
				event.stopImmediatePropagation();
			this.currentState = this.selected ? this.style.selectedHover : this.style.hover;
			this.redraw();
			if (this.labelContainer != null)
				this.label.draw(this, this.selected ? this.label.style.selectedHover : this.label.style.hover, this.labelContainer, LABEL_INDEX);
			if (this.tooltipContainer != null) {
				this.tooltip.draw(this, this.selected ? this.tooltip.style.selectedNormal : this.tooltip.style.normal, this.tooltipContainer, TOOLTIP_INDEX);
				this.tooltip.move(this.selected ? this.tooltip.style.selectedNormal : this.tooltip.style.normal, this, TOOLTIP_INDEX, this.tooltipContainer);
			}
			if (event != null)
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OVER);
		}
		
		private function mouseOutHandler(event:MouseEvent):void {
			this.isOver = false;
			if (event != null) event.stopImmediatePropagation();
			this.currentState = this.selected ? this.style.selectedNormal : this.style.normal;
			this.redraw();
			if (this.labelContainer != null)
				this.label.draw(this, this.selected ? this.label.style.selectedNormal : this.label.style.normal, this.labelContainer, LABEL_INDEX);
			if (this.tooltipContainer != null)
				this.tooltip.hide(this.tooltipContainer);
			if (event != null)
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OUT);
		}
		
		private function mouseDownHandler(event:MouseEvent):void {
			event.stopImmediatePropagation();
			if (this.actions != null)
				this.actions.execute(this);
			if (!this.selected) {
				this.currentState = this.style.pushed;
				this.redraw();
				if (this.labelContainer != null)
					this.label.draw(this, this.label.style.pushed, this.labelContainer, LABEL_INDEX);
			}
			
			if (this.editable) {
				this.startValueChange();
				this.container.mouseEnabled = false;
				this.container.stage.addEventListener(MouseEvent.MOUSE_MOVE, stageMouseMoveHandler);
				this.container.stage.addEventListener(MouseEvent.MOUSE_UP, stageMouseClickHandler);
			}
			this.dispatchAnyChartPointEvent(event, PointEvent.POINT_CLICK);
		}
		
		private function mouseClickHandler(event:MouseEvent):void {
			event.stopImmediatePropagation();
			if (this.selectable && !this.selected) {
				if (this.gauge.selectedPointer != null)
					this.gauge.selectedPointer.deselect();
				this.gauge.selectedPointer = this;
				this.selected = true;
				this.mouseOverHandler(event);
			}
		}
		
		private function mouseMoveHandler(event:MouseEvent):void {
			event.stopImmediatePropagation();
			if (this.isOver && this.tooltipContainer != null)
				this.tooltip.move(this.selected ? this.tooltip.style.selectedNormal : this.tooltip.style.normal, this, TOOLTIP_INDEX, this.tooltipContainer);
		}
		
		private function stageMouseClickHandler(event:MouseEvent):void {
			this.container.mouseEnabled = true;
			this.stopValueChange();
			this.container.stage.removeEventListener(MouseEvent.MOUSE_MOVE, stageMouseMoveHandler);
			this.container.stage.removeEventListener(MouseEvent.MOUSE_UP, stageMouseClickHandler);
		}
		
		private function stageMouseMoveHandler(event:MouseEvent):void {
			this.changeValue(event,event.stageX, event.stageY);
			var params:Object = {
				pointerName: this.name,
				pointerValue: this.value,
				gaugeName: this.gauge.name
			};
			var valueChange:ValueChangeEvent = new ValueChangeEvent(ValueChangeEvent.VALUE_CHANGE, params);
			this.gauge.plot.chart.dispatchEvent(valueChange);
		}
		
		protected function changeValue(event:MouseEvent,stageMouseX:Number, stageMouseY:Number):void {
		}
		
		private var range:Number;
		private var previousMinimum:Number;
		
		public final function updateFixedValue(value:Number):void {
			this.value = value;
			if (this.label != null) {
				if (this.labelContainer != null)
					this.labelContainer.parent.removeChild(this.labelContainer);
				this.elementsInfo[LABEL_INDEX] = null;
				this.labelContainer = this.label.initialize(this, LABEL_INDEX);
				if (this.labelContainer != null)
					this.initListeners(this.labelContainer); 
			}
			if (this.tooltip != null) {
				if (this.tooltipContainer != null)
					this.tooltipContainer.parent.removeChild(this.tooltipContainer);
				this.elementsInfo[TOOLTIP_INDEX] = null;
				this.tooltipContainer = this.tooltip.initialize(this, TOOLTIP_INDEX);
			}
			this.container.graphics.clear();
			this.draw();
		}
		
		public final function udpateValue(data:Object):void {
			
			if (data.value != undefined) {
				var value:Number = data.value;
				this.strValue=data.value.toString();
			
				if (axis != null) {
					axis.scale.isMinimumAuto = false;
					axis.scale.isMaximumAuto = false;
				
					if (isNaN(this.range)) {
						this.range = this.axis.scale.maximum - this.axis.scale.minimum;
						this.previousMinimum = this.axis.scale.minimum;
					}
				
					if (value > this.axis.scale.maximum) {
						this.axis.scale.minimum = Math.ceil((value - range)/this.axis.scale.majorInterval)*this.axis.scale.majorInterval;
						this.axis.scale.maximum = this.axis.scale.minimum + this.range;
						this.axis.scale.calculate();
						this.axis.resize();
					}else if (value < this.axis.scale.minimum) {
						this.axis.scale.minimum = Math.max(Math.floor((value - range)/this.axis.scale.majorInterval)*this.axis.scale.majorInterval, this.previousMinimum);
						this.axis.scale.maximum = this.axis.scale.minimum + range;
						this.axis.scale.calculate();
						this.axis.resize();
					}
				}
				if ( (data.animate != undefined && Boolean(data.animate)) || data.animation != undefined){
					value=this.fixValueScaleOut(value);
					
					if (data.animation != undefined){
						if (this.animation==null) this.animation= new Animation();
						if (data.animation.start_time != undefined) this.animation.startTime = int(Number(data.animation.start_time))*1000;
						if (data.animation.duration != undefined) this.animation.duration = int(Number(data.animation.duration))*1000;
						this.animation.interpolation = InterpolationFactory.create(data.animation.interpolation_type != undefined ? data.animation.interpolation_type : "");
					}
					if (this.animation != null && this.gauge.plot.animation.enabled && this.gauge.plot.animation.allowrepeat)
					{
						this.gauge.plot.animation.targets = null;
						this.gauge.plot.animation.targets = [];
						this.gauge.plot.animation.registerCallBack(this.updateFixedValue, this.value, value, this.animation);
						this.value = value;
						var gaugeAxis:AxisGaugeBase = AxisGaugeBase(this.gauge);
						for each ( var ga:* in gaugeAxis.pointers)
						{
							if(ga.name!=this.name)
								ga.gauge.plot.animation.registerCallBack(ga.updateFixedValue, ga.value, ga.value, ga.animation);
						}
						this.gauge.plot.animation.reanimate();
						this.gauge.plot.animation.targets = null;
						this.gauge.plot.animation.targets = [];
						for each (var ttt:* in this.gauge.plot.gaugesList)
						{
							
						  if (ttt.hasOwnProperty('pointers') && ttt.pointers!=null)	
						  {
							for each (var ga2:* in ttt.pointers)
							{
								if(ga2.name!=this.name)
									ga2.gauge.plot.animation.registerCallBack(ga2.updateFixedValue, 0, ga2.value, ga2.animation);
								else
									this.gauge.plot.animation.registerCallBack(this.updateFixedValue, 0, value, this.animation);
							}
						  }
						}
					}
					else if(this.animation != null && !this.gauge.plot.animation.enabled){
						this.gauge.plot.animation.targets = null;
						this.gauge.plot.animation.targets = [];
						this.gauge.plot.animation.enabled=true;
						this.gauge.plot.animation.allowrepeat=true;
						this.gauge.plot.animation.registerCallBack(this.updateFixedValue, this.value, value, this.animation);
						this.value = value;
						this.gauge.plot.animation.reanimate();
						this.gauge.plot.animation.enabled=false;
						this.gauge.plot.animation.allowrepeat=false;
						//this.animation=null;
					}
				}else
					this.value = value;
			}
			if (data.color != undefined) {
				this.color = SerializerBase.getColor(data.color);
				this.setStyleDynamicData(this.style);
			}
			this.updateFixedValue(this.value);
			
			/*
			if (isNaN(this.range)) {
				this.range = this.axis.scale.maximum - this.axis.scale.minimum;
				this.previousMinimum = this.axis.scale.minimum;
			}
			
			if (value > this.axis.scale.maximum) {
				this.axis.scale.minimum = Math.ceil((value - range)/this.axis.scale.majorInterval)*this.axis.scale.majorInterval;
				this.axis.scale.maximum = this.axis.scale.minimum + this.range;
				this.axis.scale.calculate();
				this.axis.resize();
			}else if (value < this.axis.scale.minimum) {
				this.axis.scale.minimum = Math.max(Math.floor((value - range)/this.axis.scale.majorInterval)*this.axis.scale.majorInterval, this.previousMinimum);
				this.axis.scale.maximum = this.axis.scale.minimum + range;
				this.axis.scale.calculate();
				this.axis.resize();
			}
			this.updateFixedValue(value);
			*/
		}
		
		private function dispatchAnyChartPointEvent(e:MouseEvent, type:String):void {
			var x:Number = NaN;
			var y:Number = NaN;
			if (e != null) {
				x = e.stageX;
				y = e.stageY;
			}
			var point:GaugePointerTransormation = new GaugePointerTransormation(this);
			this.gauge.plot.chart.dispatchEvent(new PointEvent(type, point, x, y));
		}
		
		//--------------------------------------------------------------
		//	Drawing
		//--------------------------------------------------------------
		
		protected var bounds:Rectangle;
		protected var container:Sprite;
		
		private var labelContainer:Sprite;
		private var tooltipContainer:Sprite;
		
		public function initialize():DisplayObject {
			if (this.animation != null) {
				var start:Number = this.axis.scale.minimum > 0 ? this.axis.scale.minimum : 0;
				this.gauge.plot.animation.registerCallBack(this.updateFixedValue, start, this.value, this.animation);
			}
			this.initListeners(this.container);
			this.selected = this.selectable && this.selected;
			this.currentState = (this.selectable && this.selected) ? this.style.selectedNormal : this.style.normal;
			if (this.label != null) {
				this.labelContainer = this.label.initialize(this, LABEL_INDEX);
				if (this.labelContainer != null)
					this.initListeners(this.labelContainer);
			}
			if (this.tooltip != null) {
				this.tooltipContainer = this.tooltip.initialize(this, TOOLTIP_INDEX);
			} 
			return this.container;
		}
		
		public final function draw():void {
			this.execDrawing();
			if (this.labelContainer != null)
				this.label.draw(this, this.selected ? this.label.style.selectedNormal : this.label.style.normal, this.labelContainer, LABEL_INDEX);
		}
		
		private function redraw():void {
			this.container.graphics.clear();
			this.execDrawing();
		}
		
		public final function resize():void {
			this.container.graphics.clear();
			this.execDrawing();
			if (this.labelContainer != null)
				this.label.resize(this, this.selected ? this.label.style.selectedNormal : this.label.style.normal, this.labelContainer, LABEL_INDEX);
		}
		
		protected function execDrawing():void {
			this.setFiltersAndClear();
		}
		
		protected function setFiltersAndClear():void {
			this.container.graphics.clear();
			this.container.filters = StyleStateWithEffects(this.currentState).effects != null ? StyleStateWithEffects(this.currentState).effects.list : null;
		}
		
		// If vpointer's value is out of scale Min or Max value
		// needs to replace this value
		public function fixValueScaleOut(_value:Number):Number
		{
			if (!axis.scale.isMinimumAuto && _value<axis.scale.minimum) return axis.scale.minimum;
			if (!axis.scale.isMaximumAuto && _value>axis.scale.maximum) return axis.scale.maximum;
			return _value;	
		}
	}
}