package com.anychart.gaugePlot.pointers.styles.linear {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.color.ColorParser;
	
	public final class TankPointerStyleState extends GaugePointerStyleState {
		
		public var width:Number;
		public var isWidthAxisSizeDepend:Boolean;
		private var _color:int;
		private var _isDynamicColor:Boolean;
		
		public function TankPointerStyleState(style:Style) {
			super(style);
			this.width = .1;
			this.isWidthAxisSizeDepend = false;
			this._color = -1;
			this._isDynamicColor = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			this.deserializePosition(data);
			if (data.@width != undefined) {
				this.isWidthAxisSizeDepend = this.isAxisSizeDepend(data.@width);
				this.width = this.isWidthAxisSizeDepend ? this.getAxisSizeFactor(data.@width) : (SerializerBase.getNumber(data.@width)/100);
			}
			data = data.copy();
			if (data.@color != undefined) {
				this._isDynamicColor = ColorParser.getInstance().isDynamic(data.@color);
				this._color = SerializerBase.getColor(data.@color);
				if (this._isDynamicColor)
					this.style.addDynamicColor(this._color);
			}
			return data;
		}
		
		public function getTankColor(color:uint):uint {
			var clr:int = (this._isDynamicColor) ? ColorParser.getInstance().getDynamicColor(this._color, color) : this._color;
			if (clr == -1) return color;
			return clr;
		}
	}
}