package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.stroke.Stroke;
	
	internal final class TrendlineStyleState extends GaugeAxisMarkerStyleState {
		
		internal var label:AxisMarkerLabel;
		internal var size:Number;
		internal var isSizeAxisSizeDepend:Boolean;
		
		public function TrendlineStyleState(style:Style) {
			super(style);
			this.size = .1;
			this.isSizeAxisSizeDepend = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@size != undefined) {
				this.isSizeAxisSizeDepend = this.isAxisSizeDepend(data.@size);
				this.size = (this.isSizeAxisSizeDepend) ? this.getAxisSizeFactor(data.@size) : (SerializerBase.getNumber(data.@size)/100);
			}
			if (SerializerBase.isEnabled(data.line[0])) {
				this.stroke = new Stroke();
				this.stroke.deserialize(data.line[0], this.style);
			}
			if (SerializerBase.isEnabled(data.label[0])) {
				this.label = new AxisMarkerLabel();
				this.label.deserialize(data.label[0], resources, this.style);
			}
			return data;
		}

	}
}