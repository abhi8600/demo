package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.circular.NeedlePointerStyleState;
	
	import flash.geom.Point;
	
	public class NeedleCircularGaugePointer extends CircularGaugePointer {
		
		private var drawer:NeedleDrawer;
		
		public function NeedleCircularGaugePointer() {
			super();
			this.drawer = new NeedleDrawer();
		}
		
		override protected function getStyleNodeName():String {
			return "needle_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return NeedlePointerStyleState;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var state:NeedlePointerStyleState = NeedlePointerStyleState(this.currentState); 
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			this.drawer.draw(this.pointerShape, this, state.needle, axis, state.fill, state.hatchFill, state.stroke, state.effects);
		}
		
		override public function getAnchorPoint(anchor:uint, anchorPos:Point):void {
			
			var angle:Number = axis.scale.transform(this.fixValueScaleOut(this.value));
			
			var outerR:Number = Math.max(this.drawer.pixelBaseRadius, this.drawer.pixelPointRadius, this.drawer.pixelRadius);
			var innerR:Number = Math.min(this.drawer.pixelBaseRadius, this.drawer.pixelPointRadius, this.drawer.pixelRadius);
			var w:Number = Math.max(this.drawer.pixelPointThickness, this.drawer.pixelThickness); 
			
			this.getRectBasedAnchorPoint(anchor, anchorPos, w, (outerR - innerR), (innerR + outerR)/2, angle);
		}
	}
}