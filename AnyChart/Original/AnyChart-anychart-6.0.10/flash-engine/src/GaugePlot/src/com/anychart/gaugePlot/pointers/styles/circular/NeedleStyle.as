package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.BackgroundBase;
	
	public final class NeedleStyle implements IStyleSerializableRes {
		
		public var radius:Number;
		public var isRadiusAxisSizeDepend:Boolean;
		
		public var baseRadius:Number;
		public var isBaseRadiusAxisSizeDepend:Boolean;
		
		public var pointThickness:Number;
		public var isPointThicknessAxisSizeDepend:Boolean;
		
		public var pointRadius:Number;
		public var isPointRadiusAxisSizeDepend:Boolean;
		
		public var thickness:Number;
		public var isThicknessAxisSizeDepend:Boolean;
		
		public function getPercentThickness(axis:CircularGaugeAxis):Number {
			return (this.isThicknessAxisSizeDepend ? axis.width : 1)*this.thickness;
		}
		
		public function getPercentBaseRadius(axis:CircularGaugeAxis):Number {
			return (this.isBaseRadiusAxisSizeDepend ? axis.width : 1)*this.baseRadius;
		}
		
		public var background:BackgroundBase;
		
		public function NeedleStyle(useBackground:Boolean) {
			if (useBackground)
				this.background = new BackgroundBase();
			this.isBaseRadiusAxisSizeDepend = false;
			this.isPointRadiusAxisSizeDepend = false;
			this.isPointThicknessAxisSizeDepend = false;
			this.isRadiusAxisSizeDepend = false;
			this.isThicknessAxisSizeDepend = false;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@radius != undefined) {
				this.isRadiusAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@radius);
				this.radius = this.isRadiusAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@radius) : GaugePlotSerializerBase.getSimplePercent(data.@radius);
			}
			if (data.@thickness != undefined) {
				this.isThicknessAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@thickness);
				this.thickness = this.isThicknessAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@thickness) : GaugePlotSerializerBase.getSimplePercent(data.@thickness);
			}
			if (data.@base_radius != undefined) {
				this.isBaseRadiusAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@base_radius);
				this.baseRadius = this.isBaseRadiusAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@base_radius) : GaugePlotSerializerBase.getSimplePercent(data.@base_radius);
			}
			if (data.@point_thickness != undefined) {
				this.isPointThicknessAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@point_thickness);
				this.pointThickness = this.isPointThicknessAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@point_thickness) : GaugePlotSerializerBase.getSimplePercent(data.@point_thickness);
			}
			if (data.@point_radius != undefined) {
				this.isPointRadiusAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@point_radius);
				this.pointRadius = this.isPointRadiusAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@point_radius) : GaugePlotSerializerBase.getSimplePercent(data.@point_radius);
			}
			if (this.background != null)
				this.background.deserialize(data, resources, style);
		}

	}
}