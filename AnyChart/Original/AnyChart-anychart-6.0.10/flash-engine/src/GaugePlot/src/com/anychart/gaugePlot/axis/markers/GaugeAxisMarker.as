package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.AxisGaugeBase;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	internal class GaugeAxisMarker {
		
		internal var container:Sprite;
		internal var gauge:AxisGaugeBase;
		
		internal var color:uint;
		internal var style:Style;
		
		public function GaugeAxisMarker() {
			this.color = 0xCCCCCC;
		}
		
		public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):void {
			if (data.@color != undefined) this.color = SerializerBase.getColor(data.@color);
			delete data.@color;
			this.style = this.getStyle(data, resources, stylesList, localStyles);
			this.style.setDynamicColors(this.color);
			this.style.setDynamicGradients(this.color);
		}
		
		public function initialize():DisplayObject {
			this.container = new Sprite();
			if (this.getActualState().effects != null)
				this.container.filters = this.getActualState().effects.list;
			return this.container;
		}
		
		public function checkScale(scale:BaseScale):void {}
		
		protected final function checkScaleValue(value:Number, scale:BaseScale):void {
			if (isNaN(scale.dataRangeMinimum) || value < scale.dataRangeMinimum)
	    		scale.dataRangeMinimum = value;
	    	if (isNaN(scale.dataRangeMaximum) || value > scale.dataRangeMaximum)
	    		scale.dataRangeMaximum = value;
		}
		
		public function clear():void {
			this.container.graphics.clear();
			while (this.container.numChildren>0)
				this.container.removeChildAt(0);
		}
		
		protected function getStyleNodeName():String { return null; }
		protected function getStyleStateClass():Class { return null; }
		internal function getActualState():GaugeAxisMarkerStyleState { return GaugeAxisMarkerStyleState(this.style.normal); }
		protected function getStyle(data:XML, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):Style {
			var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, this.getStyleNodeName(), data, data.@style);
			var style:Style = new Style(this.getStyleStateClass());
			style.deserialize(actualStyleXML,resources);
			return style;
		}
	}
}