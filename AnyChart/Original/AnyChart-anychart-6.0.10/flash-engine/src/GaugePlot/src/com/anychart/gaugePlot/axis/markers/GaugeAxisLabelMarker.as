package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	import flash.display.DisplayObject;
	
	internal final class GaugeAxisLabelMarker extends GaugeAxisMarker {
		
		internal var value:Number;
		
		override protected function getStyleStateClass():Class { return CusomLabelStyleState; }
		override protected function getStyleNodeName():String { return "custom_label_style"; }
		
		override public function initialize():DisplayObject {
			super.initialize();
			if (CusomLabelStyleState(this.style.normal).tickmark != null)
				this.container.addChild(CusomLabelStyleState(this.style.normal).tickmark.initialize());
			return this.container;
		}
		
		override public function clear():void {
			this.container.graphics.clear();
			if (CusomLabelStyleState(this.style.normal).tickmark != null)
				CusomLabelStyleState(this.style.normal).tickmark.clear();
		}
		
		override public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):void {
			super.deserialize(data, axisWidth, resources, stylesList, localStyles);
			if (CusomLabelStyleState(this.style.normal).tickmark != null)
				gauge.setBackgroundRotation(CusomLabelStyleState(this.style.normal).tickmark.background);
			if (data.@value != undefined) this.value = SerializerBase.getNumber(data.@value);
		}
		
		override public function checkScale(scale:BaseScale):void {
			this.checkScaleValue(this.value, scale);
		}
	}
}