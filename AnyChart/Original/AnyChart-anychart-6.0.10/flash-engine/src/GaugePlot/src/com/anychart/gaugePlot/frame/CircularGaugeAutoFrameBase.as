package com.anychart.gaugePlot.frame {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.visual.layout.Margin;
	
	import flash.geom.Rectangle;
	
	internal class CircularGaugeAutoFrameBase extends CircularGaugeFrame {
		
		public var autoMargin:Margin;
		
		private function getAngleFourth(radAngle:Number):uint {
			var cos:Number = Math.cos(radAngle);
        	var sin:Number = Math.sin(radAngle);
			if (cos >= 0 && sin >= 0) return 0;
        	if (cos <= 0 && sin >= 0) return 1;
        	if (cos <= 0 && sin < 0) return 2;
        	return 3;
		}
		
		private function addExtraAngles(startRadAngle:Number, endRadAngle:Number):Array {
			var extraCheckAngles:Array = [];
			var startF:uint = this.getAngleFourth(startRadAngle);
            var endF:uint = this.getAngleFourth(endRadAngle);
            
			if (startF != endF) {
        		switch (startF) {
        			case 0:
        				if (endF == 1) {
        					extraCheckAngles.push(Math.PI/2);
        				}else if (endF == 2) {
        					extraCheckAngles.push(Math.PI/2);
        					extraCheckAngles.push(Math.PI);
        				}else if (endF == 3) {
        					extraCheckAngles.push(Math.PI/2);
        					extraCheckAngles.push(Math.PI);
        					extraCheckAngles.push(Math.PI*3/2);
        				}
        				break;
        			case 1:
        				if (endF == 0) {
        					extraCheckAngles.push(0);
        					extraCheckAngles.push(Math.PI);
        					extraCheckAngles.push(Math.PI*3/2);
        				}else if (endF == 2) {
        					extraCheckAngles.push(Math.PI);
        				}else if (endF == 3) {
        					extraCheckAngles.push(Math.PI);
        					extraCheckAngles.push(Math.PI*3/2);
        				}
        				break;
        			case 2:
        				if (endF == 1) {
        					extraCheckAngles.push(Math.PI/2);
        					extraCheckAngles.push(0);
        					extraCheckAngles.push(Math.PI*3/2);
        				}else if (endF == 3) {
        					extraCheckAngles.push(Math.PI*3/2);
        				}else if (endF == 0) {
        					extraCheckAngles.push(0);
        					extraCheckAngles.push(Math.PI*3/2);
        				}
        				break;
        			case 3:
        				if (endF == 0) {
        					extraCheckAngles.push(0);
        				}else if (endF == 1) {
        					extraCheckAngles.push(0);
        					extraCheckAngles.push(Math.PI/2);
        				}else if (endF == 2) {
        					extraCheckAngles.push(0);
        					extraCheckAngles.push(Math.PI/2);
        					extraCheckAngles.push(Math.PI);
        				}
        				break;
        		}
        	}else if ((endRadAngle - startRadAngle) > Math.PI) {
        		extraCheckAngles.push(0);
        		extraCheckAngles.push(Math.PI/2);
				extraCheckAngles.push(Math.PI);
				extraCheckAngles.push(Math.PI*3/2);
        	}
        	
        	return extraCheckAngles;
		}
		
		private function checkBoundAngle(radAngle:Number, r:Number, margin:Margin):void {
			var cos:Number = Math.cos(radAngle);
        	var sin:Number = Math.sin(radAngle);
        	
        	var tmpRc:Rectangle = new Rectangle(cos, sin, 0, 0);
        	tmpRc.inflate(r,r);
        	
        	if (tmpRc.left < 0)
        		margin.left = Math.min(margin.left, tmpRc.left);
        	else
        		margin.right = Math.max(margin.right, tmpRc.left);
        		
        	if (tmpRc.right < 0)
        		margin.left = Math.min(margin.left, tmpRc.right);
        	else
        		margin.right = Math.max(margin.right, tmpRc.right);
        		
        	if (tmpRc.top < 0)
        		margin.top = Math.min(margin.top, tmpRc.top);
        	else
        		margin.bottom = Math.max(margin.bottom, tmpRc.top);
        		
        	if (tmpRc.bottom < 0)
        		margin.top = Math.min(margin.top, tmpRc.top);
        	else
        		margin.bottom = Math.max(margin.bottom, tmpRc.bottom);
		}
		
		private function checkExtraAngle(radAngle:Number, r:Number, margin:Margin):void {
			var cos:Number = (1+r)*Math.cos(radAngle);
        	var sin:Number = (1+r)*Math.sin(radAngle);
        	
        	if (cos < 0)
        		margin.left = Math.min(margin.left, cos);
        	else
        		margin.right = Math.max(margin.right, cos);
        		
        	if (sin < 0)
        		margin.top = Math.min(margin.top, sin);
        	else
        		margin.bottom = Math.max(margin.bottom, sin);
		}
		
		private function checkCap(r:Number, margin:Margin):void {
			r += CircularGauge(this.gauge).maxCapRadius;
			margin.left = Math.max(Math.abs(margin.left), r);
            margin.right = Math.max(margin.right, r);
            margin.top = Math.max(Math.abs(margin.top), r);
            margin.bottom = Math.max(margin.bottom, r);
		}
		
		override public function autoFit(gaugeRect:Rectangle):void {
		 	var gauge:CircularGauge = CircularGauge(this.gauge);
            
            var thickness:Number = this.getThickness();
            	
            var r1:Number = 1;
            var r2:Number = gauge.maxCapRadius;
            var r3:Number = this.padding;
            
            var startAngle:Number = gauge.minAngle;
            var endAngle:Number = gauge.maxAngle;
            var sweepAngle:Number = endAngle - startAngle;
            
            var m:Margin = new Margin();
            m.all = 0;
            
            if (sweepAngle >= 320) {
            	m.all = r1+r3;
            }else {
	            var startRadAngle:Number = startAngle*Math.PI/180;
	            var endRadAngle:Number = endAngle*Math.PI/180;
	            
	            var checkAngles:Array = [];
	            checkAngles.push(startRadAngle);
	            checkAngles.push(endRadAngle);
	            
				var extraCheckAngles:Array = this.addExtraAngles(startRadAngle, endRadAngle);
            
	            var i:uint;
	            
	            for (i = 0;i<checkAngles.length;i++)
	            	this.checkBoundAngle(checkAngles[i], r3, m);
	            
	            for (i = 0;i<extraCheckAngles.length;i++)
	            	this.checkExtraAngle(extraCheckAngles[i], r3, m);
	            
	            this.checkCap(r3, m);
	        }
            
            m.left += thickness;
            m.right += thickness;
            m.top += thickness;
            m.bottom += thickness;
            
            this.autoMargin = m;
            
            var w:Number = m.left+m.right;
            var h:Number = m.top+m.bottom;
            
            var r:Number = Math.min(gauge.bounds.width/w,gauge.bounds.height/h);
            var newW:Number = w*r;
            var newH:Number = h*r;
            
            gauge.bounds.x = gauge.bounds.x + (gauge.bounds.width - newW)/2
            gauge.bounds.y = gauge.bounds.y + (gauge.bounds.height - newH)/2;
            gauge.bounds.width = newW;
            gauge.bounds.height =  newH;
            
            gauge.maxPixelRadius = r*2;
            gauge.frameRadius = r;
            
            gauge.pivotPoint.x = m.left/w;
            gauge.pivotPoint.y = m.top/h;
		 }
	}
}