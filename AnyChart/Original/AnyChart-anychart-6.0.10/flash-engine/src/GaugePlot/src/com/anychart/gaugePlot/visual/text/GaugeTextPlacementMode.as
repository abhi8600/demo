package com.anychart.gaugePlot.visual.text {
	public final class GaugeTextPlacementMode {
		public static const BY_POINT:uint = 0;
		public static const BY_RECTANGLE:uint = 1;
		public static const BY_ANCHOR:uint = 2;
	}
}