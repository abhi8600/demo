package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.text.EmbedableFontSettings;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	
	internal final class LabelGauge extends GaugeBase {
		
		private var text:GaugeBaseTextElement;
		private var info:TextElementInformation;
		
		public function LabelGauge() {
			super();
			this.text = new GaugeBaseTextElement();
		}
		
		override public function isValid(data:XML):Boolean {
			return (data.format[0] != null && SerializerBase.getCDATAString(data.format[0]).length > 0);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			this.text.deserialize(data, resources);
			this.info = this.text.createInformation();
			this.info.formattedText = this.text.text;
			
			var font:EmbedableFontSettings = EmbedableFontSettings(this.text.font);
			this.text.getBounds(this.info);
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			var c:Sprite = Sprite(super.initialize(bounds));
			return c;
		}
		
		override protected function setBounds(newBounds:Rectangle):void {
			super.setBounds(newBounds);
			this.bounds.x = newBounds.x;
			this.bounds.y = newBounds.y;
			this.bounds.width = newBounds.width;
			this.bounds.height = newBounds.height;
		}
		
		override public function draw():void {
			this.text.drawGaugeText(this.container, this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height, this.info, 0,0,true);
		}
		
		override public function resize(newBounds:Rectangle, setBounds:Boolean=true):void {
			super.resize(newBounds, setBounds);
			while (this.container.numChildren > 0)
				this.container.removeChildAt(0);
			this.container.graphics.clear();
			this.draw();
		}
	}
}