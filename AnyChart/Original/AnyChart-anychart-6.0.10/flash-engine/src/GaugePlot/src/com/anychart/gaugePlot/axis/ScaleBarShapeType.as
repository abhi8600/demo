package com.anychart.gaugePlot.axis {
	internal final class ScaleBarShapeType {
		public static const LINE:uint = 0;
		public static const BAR:uint = 1;
	}
}