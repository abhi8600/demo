package com.anychart.gaugePlot.axis {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.labels.CircularGaugeAxisLabels;
	import com.anychart.gaugePlot.axis.labels.GaugeAxisLabels;
	import com.anychart.gaugePlot.axis.markers.AxisMarkersList;
	import com.anychart.gaugePlot.axis.markers.CircularGaugeAxisMarkersList;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class CircularGaugeAxis extends GaugeAxis {
		public var radius:Number;
		private var pixelRadius:Number;
		
		public var startAngle:Number;
		public var sweepAngle:Number;
		private var endAngle:Number;
		
		public function CircularGaugeAxis() {
			super();
			this.radius = .6;
			this.startAngle = 90;
			this.sweepAngle = 90;
			this.endAngle = this.startAngle + this.sweepAngle;
		}
		
		override protected function createLabels():GaugeAxisLabels { return new CircularGaugeAxisLabels(); }
		override protected function createMarkers():AxisMarkersList { return new CircularGaugeAxisMarkersList(); }
		
		override public function deserialize(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, localStyles, styles, resources);
			if (data.@radius != undefined) this.radius = GaugePlotSerializerBase.getSimplePercent(data.@radius);
			if (data.@start_angle != undefined) this.startAngle = GaugePlotSerializerBase.getAngle(data.@start_angle)+90;
			if (data.@sweep_angle != undefined) this.sweepAngle = SerializerBase.getNumber(data.@sweep_angle);
			this.endAngle = this.startAngle + this.sweepAngle;
		}
		
		override public function initializeSize():void {
			super.initializeSize();

			this.pixelRadius = CircularGauge(this.gauge).maxPixelRadius*this.radius;
			this.pixelWidth = this.pixelRadius*this.width;

			this.scale.setPixelRange(this.startAngle, this.endAngle);
			this.scale.optimalMajorSteps = (this.sweepAngle)/10; 
			this.scale.calculate();
		}
		
		public function getPixelRadius(r:Number):Number {
			return this.pixelRadius*r;
		}
		
		public function setPixelPoint(pixelR:Number, gradAngle:Number, point:Point):void {
			var center:Point = CircularGauge(this.gauge).pixPivotPoint;
			var radAngle:Number = gradAngle*Math.PI/180;
			point.x = center.x + pixelR*Math.cos(radAngle);
			point.y = center.y + pixelR*Math.sin(radAngle);
		}
		
		public function getComplexRadius(align:uint, padding:Number, width:Number, isStartPoint:Boolean):Number {
			switch (align) {
				case GaugeItemAlign.INSIDE:
					return this.pixelRadius*(1 - padding - (isStartPoint ? 0 : width))-this.pixelWidth/2;
				case GaugeItemAlign.OUTSIDE:
					return this.pixelRadius*(1 + padding + (isStartPoint ? 0 : width))+this.pixelWidth/2;
				case GaugeItemAlign.CENTER:
				default:
					return this.pixelRadius*(1 + (isStartPoint ? (-.5) : +(.5))*width - padding);
			}
		}
		
		public function getRadius(align:uint, padding:Number, width:Number, isPixelWidth:Boolean = false):Number {
			switch (align) {
				case GaugeItemAlign.INSIDE:
					return this.pixelRadius*(1 - (isPixelWidth ? 0 : width) - padding)-(isPixelWidth ? width : 0)-this.pixelWidth/2;
				case GaugeItemAlign.OUTSIDE:
					return this.pixelRadius*(1 + padding)+this.pixelWidth/2;
				case GaugeItemAlign.CENTER:
				default:
					return this.pixelRadius*(1 - (isPixelWidth ? 0 : width/2)) - (isPixelWidth ? width/2 : 0);
			}
		}
		
		override protected function drawScaleBar(scaleBar:ScaleBar):void {
			var center:Point = CircularGauge(this.gauge).pixPivotPoint;
			var innerRadius:Number;
			if (scaleBar.shape == ScaleBarShapeType.BAR) {
				innerRadius = this.getRadius(scaleBar.align, scaleBar.padding, scaleBar.width);
				var outerRadius:Number = innerRadius + this.pixelRadius * scaleBar.width;
			}else {
				innerRadius = this.getRadius(scaleBar.align, scaleBar.padding, 0);
			}
			
			var g:Graphics = scaleBar.container.graphics;
			g.clear();
			
			var scaleBarBounds:Rectangle = new Rectangle();
			scaleBarBounds.x = center.x - outerRadius;
			scaleBarBounds.y = center.y - outerRadius;
			scaleBarBounds.width = outerRadius*2;
			scaleBarBounds.height = outerRadius*2;
			
			if (scaleBar.shape == ScaleBarShapeType.BAR) {
			
				if (scaleBar.background.fill != null)
					scaleBar.background.fill.begin(g, scaleBarBounds);
				
				if (scaleBar.background.border != null)
					scaleBar.background.border.apply(g, scaleBarBounds);
					
				DrawingUtils.drawArc(g, center.x, center.y, this.startAngle, this.endAngle, innerRadius, innerRadius, 1, true);
				DrawingUtils.drawArc(g, center.x, center.y, this.endAngle, this.startAngle, outerRadius, outerRadius, 1, false);
				
				if (scaleBar.background.hatchFill != null) {
					scaleBar.background.hatchFill.beginFill(g, 0, 0);
					
					DrawingUtils.drawArc(g, center.x, center.y, this.startAngle, this.endAngle, innerRadius, innerRadius, 1, true);
					DrawingUtils.drawArc(g, center.x, center.y, this.endAngle, this.startAngle, outerRadius, outerRadius, 1, false);
					g.endFill();
				}
				
				g.endFill();
			}else if (scaleBar.background.border != null) {
				scaleBar.background.border.apply(g, scaleBarBounds);
				DrawingUtils.drawArc(g, center.x, center.y, this.startAngle, this.endAngle, innerRadius, innerRadius, 1, true);
			}
			
			g.lineStyle();
			
			if (scaleBar.background.effects != null)
				scaleBar.container.filters = scaleBar.background.effects.list;
		}
		
		override public function drawTickmark(tickmark:Tickmark, value:Number, color:uint = 0):void {
			var w:Number = tickmark.height * this.pixelRadius;
			var h:Number = tickmark.width * this.pixelRadius;
			
			var r:Number = this.getRadius(tickmark.align, tickmark.padding, 0);
			switch (tickmark.align) {
				case GaugeItemAlign.INSIDE: r -= h/2; break;
				case GaugeItemAlign.OUTSIDE: r += h/2; break;
			}
			var center:Point = CircularGauge(this.gauge).pixPivotPoint;
			var x:Number = center.x + DrawingUtils.getPointX(r, value) - w/2;
			var y:Number = center.y + DrawingUtils.getPointY(r, value) - h/2;
			var td:DisplayObject = tickmark.draw(x, y, w, h,color, false);
			if (tickmark.autoRotate) {
				td.rotation = value-90+tickmark.rotation;
			}else {
				td.rotation = tickmark.rotation;
			}
		}
	}
}