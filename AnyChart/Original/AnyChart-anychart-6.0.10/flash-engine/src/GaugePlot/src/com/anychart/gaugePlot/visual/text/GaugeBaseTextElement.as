package com.anychart.gaugePlot.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.RectBackground;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	import com.anychart.visual.text.BaseTextElementWithEmbedFonts;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public final class GaugeBaseTextElement extends BaseTextElementWithEmbedFonts {
		 
		public var hAlign:uint;
		public var vAlign:uint;
		public var placemenetMode:uint;
		public var x:Number;
		public var y:Number;
		public var isPixXY:Boolean;
		public var percentWidth:Number;
		public var percentHeight:Number;
		public var spread:Boolean;
		public var padding:Number;
		
		public function GaugeBaseTextElement() {
			super();
			this.hAlign = HorizontalAlign.CENTER;
			this.vAlign = VerticalAlign.CENTER;
			this.placemenetMode = GaugeTextPlacementMode.BY_POINT;
			this.padding = 0;
			this.isPixXY = false;
		}
		 
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			if (data.position[0] != null) {
				var pos:XML = data.position[0];
				if (pos.@halign != undefined) this.hAlign = SerializerBase.getHorizontalAlign(pos.@halign);
				if (pos.@valign != undefined) this.vAlign = SerializerBase.getVerticalAlign(pos.@valign);
				if (pos.@placement_mode != undefined) {
					switch (SerializerBase.getEnumItem(pos.@placement_mode)) {
						case "bypoint": this.placemenetMode = GaugeTextPlacementMode.BY_POINT; break;
						case "byanchor": this.placemenetMode = GaugeTextPlacementMode.BY_ANCHOR; break;
						case "byrectangle": this.placemenetMode = GaugeTextPlacementMode.BY_RECTANGLE; break;
					}
				}
				if (pos.@x != undefined) this.x = SerializerBase.getNumber(pos.@x)/100;
				if (pos.@y != undefined) this.y = SerializerBase.getNumber(pos.@y)/100;
				if (pos.@width != undefined) this.percentWidth = SerializerBase.getNumber(pos.@width)/100;
				if (pos.@height != undefined) this.percentHeight = SerializerBase.getNumber(pos.@height)/100;
				if (pos.@spread != undefined) this.spread = SerializerBase.getBoolean(pos.@spread);
				if (pos.@padding != undefined) this.padding = SerializerBase.getNumber(pos.@padding);
			}
			data.@width = undefined;
			super.deserialize(data, resources, style);
			if (data.format[0] != null)
				this.text = this.getCDATAString(data.format[0]);
		}
		
		override public function getBounds(info:TextElementInformation):void {
			var bg:RectBackground = this.background;
			if (this.spread)
				this.background = null;
			super.getBounds(info);
			this.background = bg;
		}
		
		public function drawGaugeText(container:Sprite, x:Number, y:Number, wTargetSize:Number, hTargetSize:Number, info:TextElementInformation, color:uint, hatchType:uint, checkEmbed:Boolean = false):void {
			x = isNaN(this.x) ? x : ((this.isPixXY ? (this.x) : (this.x * wTargetSize)) + x);
			y = isNaN(this.y) ? y : ((this.isPixXY ? (this.y) : (this.y * hTargetSize)) + y);
			var bgX:Number = x;
			var bgY:Number = y;
			var width:Number = info.rotatedBounds.width;
			var height:Number = info.rotatedBounds.height;
			
			var containerW:Number = this.percentWidth*wTargetSize;
			var containerH:Number = this.percentHeight*hTargetSize;
			
			var bg:RectBackground = this.background;
			if (isNaN(containerW)) containerW = info.rotatedBounds.width;
			if (isNaN(containerH)) containerH = info.rotatedBounds.height;
			
			switch (this.placemenetMode) {
				case GaugeTextPlacementMode.BY_POINT:
				case GaugeTextPlacementMode.BY_ANCHOR:
					switch (this.hAlign) {
						case HorizontalAlign.CENTER:
							bgX -= containerW/2; 
							x -= width/2;
							break;
						case HorizontalAlign.LEFT:
							if (spread) {
								bgX -= containerW + this.padding;
								x += -containerW-this.padding+(containerW-width)/2; 
							}else { 
								x -= width+this.padding;
							}
							break;
						case HorizontalAlign.RIGHT:
							if (spread) {
								bgX += this.padding;
								x += this.padding+(containerW - width)/2;
							}else {
								x += this.padding;
							} 
							break;
					}
					
					switch (this.vAlign) {
						case VerticalAlign.TOP: 
							if (spread) {
								bgY -= containerH + this.padding;
								y += -containerH-this.padding+(containerH-height)/2; 
							}else {
								y -= height+this.padding;
							} 
							break;
						case VerticalAlign.CENTER:
							bgY -= (containerH)/2; 
							y -= height/2; 
							break;
						case VerticalAlign.BOTTOM:
							if (spread) {
								bgY += this.padding;
								y += this.padding+(containerH - height)/2;
							}else {
								y += this.padding;
							}  
							break;
					}
					break;
				case GaugeTextPlacementMode.BY_RECTANGLE:
					switch (this.hAlign) {
						case HorizontalAlign.LEFT: x += this.padding; break;
						case HorizontalAlign.CENTER: x += (containerW - width)/2; break;
						case HorizontalAlign.RIGHT: x += containerW - width - this.padding; break;
					}
					switch (this.vAlign) {
						case VerticalAlign.TOP: y += this.padding; break;
						case VerticalAlign.CENTER: y += (containerH - height)/2; break;
						case VerticalAlign.BOTTOM: y += containerH - height - this.padding; break;
					}
					break;
			}
			
			if (this.spread) {
				this.background = null;
				if (bg != null) {
					if (bg.margins != null) {
						containerW += bg.margins.left + bg.margins.right;
						containerH += bg.margins.top + bg.margins.bottom;
					}
					var bgRect:Rectangle = new Rectangle(bgX,bgY,containerW,containerH);
					this.background = bg;
					this.drawTextBackground(container.graphics, bgRect, color, hatchType);
					this.background = null;
				}
			}
			
			if (!checkEmbed)
				this.draw(container.graphics, x, y, info, color, hatchType);
			else
				this.drawEmbed(container, x, y, info, color, hatchType);
			this.background = bg;
		}
		
		override protected function drawTextBackground(g:Graphics, bounds:Rectangle, color:uint, hatchType:uint):void {
			bounds = bounds.clone();
			bounds.x += 1;
			bounds.y += 1;
			bounds.width -= 2;
			bounds.height -= 2;
			this.background.draw(g, bounds, color, hatchType);
		}
	}
}