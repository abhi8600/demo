package com.anychart.gaugePlot {
	import com.anychart.IAnyChart;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.gaugePlot.elements.LabelElement;
	import com.anychart.gaugePlot.frame.GaugeFrame;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class GaugeBase implements ISerializableRes, IMainElementsContainer {
		
		public var name:String;
		
		protected var x:Number;
		protected var y:Number;
		protected var width:Number;
		protected var height:Number;
		
		protected var frame:GaugeFrame;
		
		public var container:Sprite;
		protected var frameContainer:DisplayObject;
		protected var contentContainer:Sprite;
		protected var beforeContentContainer:Sprite;
		public var bounds:Rectangle;
		
		protected var children:Array;
		
		public var plot:GaugePlot;
		
		private var _labelsContainerBefore:Sprite;
		private var _labelsContainerAfter:Sprite;
		
		private var margin:Margin;
		private var labels:Array;
		
		public var selectedPointer:GaugePointer;
		
		public function getLabelsContainer(label:ItemLabelElement):Sprite {
			if (label is LabelElement)
				return LabelElement(label).underPointers ? this._labelsContainerBefore : this._labelsContainerAfter;
			return this._labelsContainerAfter; 
		}
		public function get markersContainer():Sprite { return null; }
		public function get tooltipsContainer():DisplayObjectContainer { return this.plot.tooltipsContainer; }
		
		public function get root():IAnyChart {
			return this.plot.chart;
		}
		
		public function GaugeBase() {
			this.children = [];
			this.x = 0;
			this.y = 0;
			this.width = 1;
			this.height = 1;
			this.bounds = new Rectangle();
			
			this._labelsContainerBefore = new Sprite();
			this._labelsContainerAfter = new Sprite();
			this.beforeContentContainer = new Sprite();
			this.margin = new Margin();
			this.margin.all = 0;
			
			this.labels = [];
		}
		
		public function addChild(gauge:GaugeBase):void {
			this.children.push(gauge);
		}
		
		public function numChildren():int {
			return this.children.length;
		}
		
		public function getChild(i:int):GaugeBase {
			return this.children[i];
		}
		
		public function serialize(res:Object = null):Object {
			if (res == null) res = {};
			res.children = [];
			for (var i:int = 0;i<this.children.length;i++) {
				res.children.push(this.children[i].serialize());
			}
			res.name = this.name;
			return res;
		}
		
		protected function createFrame(data:XML):GaugeFrame { return null; }
		
		protected var localStyles:StylesList;
		
		public function isValid(data:XML):Boolean { return true; }
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			this.localStyles = new StylesList();
			this.localStyles.initializeParsing();
			
			if (data.@name != undefined) this.name = SerializerBase.getString(data.@name);
			if (data.@x != undefined) this.x = GaugePlotSerializerBase.getSimplePercent(data.@x);
			if (data.@y != undefined) this.y = GaugePlotSerializerBase.getSimplePercent(data.@y);
			if (data.@width != undefined) this.width = GaugePlotSerializerBase.getSimplePercent(data.@width);
			if (data.@height != undefined) this.height = GaugePlotSerializerBase.getSimplePercent(data.@height);
			var styles:XML = data.styles[0];
			this.frame = this.createFrame(data.frame[0]);
			if (this.frame != null) {
				this.frame.gauge = this;
				if (data.frame[0] != null)
					this.frame.deserialize(data.frame[0], resources);
			}
			if (data.margin[0] != null)
				this.margin.deserialize(data.margin[0]);
			if (data.labels[0] != null) {
				var cnt:int = data.labels[0].label.length();
				for (var i:int = 0;i<cnt;i++) {
					var label:GaugeLabel = new GaugeLabel();
					label.deserialize(data.labels[0].label[i], styles, localStyles, resources);
					this.labels.push(label);
				}
			}
		}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.setBounds(bounds);
			this.container = new Sprite();
			if (this.frame != null) {
				this.frameContainer = this.frame.initialize(this.bounds);
				this.container.addChild(this.frameContainer);
			}
			this.contentContainer = new Sprite();
			this.contentContainer.addChild(this.beforeContentContainer);
			this.container.addChild(this._labelsContainerBefore);
			this.container.addChild(this.contentContainer);
			this.container.addChild(this._labelsContainerAfter);
			
			var i:uint;
			for (i = 0;i<this.labels.length;i++) {
				var label:GaugeLabel = GaugeLabel(this.labels[i]);
				if (label.underPointers)
					this._labelsContainerBefore.addChild(label.initialize(this.bounds));
				else
					this.container.addChild(label.initialize(this.bounds));
			}
			
			for (i = 0;i<this.children.length;i++)
				this.container.addChild(this.children[i].initialize(this.bounds));
			return this.container;
		}
		
		public function draw():void {
			if (this.frame != null)
				this.frame.draw();
			var i:uint;
			for (i = 0;i<this.labels.length;i++)
				this.labels[i].draw();
			for (i = 0;i<this.children.length;i++)
				this.children[i].draw();
		}
		
		public function resize(newBounds:Rectangle, setBounds:Boolean = true):void {
			if (setBounds)
				this.setBounds(newBounds);
			if (this.frame != null)
				this.frame.resize(this.bounds);
			this.contentContainer.graphics.clear();
			var i:uint;
			for (i = 0;i<this.labels.length;i++)
				this.labels[i].resize(this.bounds);
			for (i = 0;i<this.children.length;i++)
				this.children[i].resize(this.bounds);
		}
		
		protected function setBounds(newBounds:Rectangle):void {
			this.bounds.x = newBounds.x + this.x * newBounds.width;
			this.bounds.y = newBounds.y + this.y * newBounds.height;
			this.bounds.width = newBounds.width * this.width;
			this.bounds.height = newBounds.height * this.height;
			
			this.applyMargin(this.bounds);
		}
		
		protected final function applyMargin(bounds:Rectangle):void {
			
			var w:Number = bounds.width;
			var h:Number = bounds.height;
			
			bounds.left += margin.left*w/100;
			bounds.right -= margin.right*w/100;
			bounds.top += margin.top*h/100;
			bounds.bottom -= margin.bottom*h/100;
		}
		
		public function setPointerValue(value:Object, pointerName:String = null):void {
		}
	}
}