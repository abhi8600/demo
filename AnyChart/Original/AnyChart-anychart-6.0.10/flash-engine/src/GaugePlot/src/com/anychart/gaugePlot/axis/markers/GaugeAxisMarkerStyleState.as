package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.styles.Style;
	
	internal class GaugeAxisMarkerStyleState extends GaugePointerStyleState {
		public function GaugeAxisMarkerStyleState(style:Style) {
			super(style);
		}
	}
}