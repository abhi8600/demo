package com.anychart.gaugePlot {
	import com.anychart.animation.Animation;
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.elements.LabelElement;
	import com.anychart.gaugePlot.elements.TooltipElement;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public class AxisGaugeBase extends GaugeBase {
		
		protected var axes:Array;
		private var axesMap:Object;
		
		public var pointers:Array;
		public var pointersMap:Object;
		
		public var label:LabelElement;
		public var tooltip:TooltipElement;
		
		public function AxisGaugeBase() {
			super();
			this.axes = [];
			this.axesMap = {};
			this.pointers = [];
			this.pointersMap = {};
			this.label = new LabelElement();
			this.label.gauge = this;
			this.tooltip = new TooltipElement();
		}
		
		override public function serialize(res:Object=null):Object {
			var res:Object = super.serialize(res);
			res.pointers = [];
			for (var i:int = 0;i<this.pointers.length;i++) {
				res.pointers.push(this.pointers[i].serialize());
			}
			return res;
		}
		
		protected function createAxis(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):GaugeAxis {return null;}
		protected final function registerAxis(axisName:String, axis:GaugeAxis):void {
			axis.gauge = this;
			this.axes.push(axis);
			if (axisName != null)
				this.axesMap[axisName.toLowerCase()] = axis;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			
			var stylesList:XML = data.styles[0];
			
			var axis:GaugeAxis = this.createAxis(data.axis[0], localStyles, stylesList, resources);
			this.registerAxis("default",axis);
			
			var i:int;
			
			if (data.extra_axes[0] != null) {
				var extraAxesList:XMLList = data.extra_axes[0].axis;
				var cnt:int = extraAxesList.length();
				for (i = 0;i<cnt;i++) {
					axis = this.createAxis(extraAxesList[i], localStyles, stylesList, resources);
					this.registerAxis(extraAxesList[i].@name != undefined ? extraAxesList[i].@name : null, axis); 
				}
			}
			
			if (data.pointers[0] != null) {
				
				var color:int = data.pointers[0].@color != undefined ? SerializerBase.getColor(data.pointers[0].@color) : -1;
				var hatchType:int = data.pointers[0].@hatch_type != undefined ? SerializerBase.getHatchType(data.pointers[0].@hatch_type) : -1;
				
				var editable:Boolean = data.pointers[0].@editable != undefined ? SerializerBase.getBoolean(data.pointers[0].@editable) : false;
				var selectable:Boolean = data.pointers[0].@allow_select != undefined ? SerializerBase.getBoolean(data.pointers[0].@allow_select) : false;
				var handCursor:Boolean = data.pointers[0].@use_hand_cursor != undefined ? SerializerBase.getBoolean(data.pointers[0].@use_hand_cursor) : true;
				
				var animation:Animation;
				if (SerializerBase.isEnabled(data.pointers[0].animation[0])) {
					animation = new Animation();
					animation.deserialize(data.pointers[0].animation[0]);
				}
				
				this.label.deserializeGlobal(data.pointers[0].label[0],stylesList,resources);
				this.tooltip.deserializeGlobal(data.pointers[0].tooltip[0],stylesList,resources);
				
				var pointersList:XMLList = data.pointers[0].pointer;
				var pointersCnt:int = pointersList.length();
				for (i = 0;i<pointersCnt;i++) {
					if (pointersList[i].@value != undefined || (pointersList[i].@start != undefined && pointersList[i].@end != undefined)) {
						var pointer:GaugePointer = this.createPointerByType(pointersList[i].@type);
						if (animation != null)
							pointer.animation = animation.createCopy(); 
						pointer.gauge = this;
						pointer.editable = editable;
						pointer.selectable = selectable;
						pointer.useHandCursor = handCursor;
						pointer.label = LabelElement(this.label.createCopy());
						pointer.tooltip = TooltipElement(this.tooltip.createCopy());
						if (color != -1) 
							pointer.color = color;
						pointer.hatchType = hatchType;
						pointer.deserialize(pointersList[i], resources, localStyles, stylesList, this.plot.chart);
						if (pointersList[i].@axis != undefined)
							pointer.axis = this.axesMap[SerializerBase.getLString(pointersList[i].@axis)];
						if (pointer.axis == null) 
							pointer.axis = this.axesMap["default"];
						this.pointers.push(pointer);
						if (pointer.name != null)
							this.pointersMap[pointer.name.toLowerCase()] = pointer;
						this.checkPointer(pointer);
						pointer.axis.checkPoint(pointer);
					}
				}
			}
		}
		
		protected function checkPointer(pointer:GaugePointer):void {}
		
		protected function createPointerByType(type:*):GaugePointer { return null; }
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			super.initialize(bounds);
			var i:uint;
			for (i = 0;i<this.axes.length;i++)
				this.beforeContentContainer.addChild(this.axes[i].initialize());
			for (i = 0;i<this.pointers.length;i++)
				this.contentContainer.addChild(this.pointers[i].initialize());
			for (i = 0;i<this.axes.length;i++)
				this.axes[i].initializeSize();
			return this.container;
		}
		
		override public function draw():void {
			super.draw();
			var i:uint;
			for (i = 0;i<this.axes.length;i++)
				this.axes[i].draw();
			for (i = 0;i<this.pointers.length;i++)
				this.pointers[i].draw();
		}
		
		override public function resize(newBounds:Rectangle, setBounds:Boolean=true):void {
			super.resize(newBounds, setBounds);
			for (var i:uint = 0;i<this.axes.length;i++)
				this.axes[i].resize();
			for (i = 0;i<this.pointers.length;i++)
				this.pointers[i].resize();
		}
		
		override public function setPointerValue(value:Object, pointerName:String=null):void {
			var pointer:GaugePointer;
			if (pointerName != null)
				pointer = this.pointersMap[pointerName.toLowerCase()];
			if (pointer != null) {
				pointer.udpateValue(value);
			}
		}
		
		public function setGradientRotation(gradient:Gradient):void {}
		public function setBackgroundRotation(background:BackgroundBase):void {
			if (background != null) {
				if (background.fill != null && background.fill.gradient != null)
					this.setGradientRotation(background.fill.gradient);
				if (background.border != null && background.border.gradient != null)
					this.setGradientRotation(background.border.gradient);
			}
		}
	}
}