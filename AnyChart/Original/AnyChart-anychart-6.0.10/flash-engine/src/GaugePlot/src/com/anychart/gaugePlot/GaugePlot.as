package com.anychart.gaugePlot {
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.PlotControlsList;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.controls.legend.IconSettings;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.gaugePlot.templates.GaugePlotTemplatesManager;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.utils.XMLUtils;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	public final class GaugePlot implements IPlot {
		
		//-----------------------------------------------
		//	IPlot base
		//-----------------------------------------------
		
		public var chart:IAnyChart;
		
		public function setBase(chart:IAnyChart):void { this.chart = chart; }
		public function setView(view:Object):void {}
		public function setPlotType(value:String):void {}
		
		public final function setContainer(chart:IChart):void {}
		
		private var _tooltipsContainer:Sprite;
		
		public function GaugePlot() {
			super();
			this._tooltipsContainer = new Sprite();
			this._tooltipsContainer.mouseChildren = false;
			this._tooltipsContainer.mouseEnabled = false;
		}
		
		
		public function destroy():void {}
		
		public final function updateData(...params:*):void {
			var gaugeName:String = params[0];
			var pointerName:String = params[1];
			var newValue:Object = Object(params[2]);
			var gauge:GaugeBase;
			if (gaugeName != null) {
				gaugeName = String(gaugeName).toLowerCase();
				gauge = GaugeBase(this.gaugesMap[gaugeName]);
			}
			if (gauge == null)
				gauge = this.gaugesList[0];
			if (gauge != null)
				gauge.setPointerValue(newValue,pointerName);
		}
		
		public function setHover():void {}
		public function setNormal():void {}
		
		public function getLegendData(item:XML, container:ILegendItemsContainer):void {}
		public function drawDataIcon(iconSettings:IconSettings, source:ILegendItem, g:Graphics, dx:Number, dy:Number):void {}
		public function createIconSettings():IconSettings { return new IconSettings(); }
		public function get tooltipsContainer():DisplayObjectContainer { return this._tooltipsContainer; }
		
		public var animation:AnimationManager;
		public function setAnimationManager(manager:AnimationManager):void {
			this.animation = manager;
		}
		
		public function checkNoData(data:XML):Boolean { return false; }
		
		public function setXZoom(settings:Object):void {}
		public function setYZoom(settings:Object):void {}
		public function setZoom(xZoomSettings:Object, yZoomSettings:Object):void {}
		public function scrollXTo(xValue:String):void {}
		public function scrollYTo(yValue:String):void {}
		public function scrollTo(xValue:String, yValue:String):void {}
		
		public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void {};
		public function startPointsUpdate():void {}
		
		//-----------------------------------------------
		//	Controls
		//-----------------------------------------------
		
		public function setControlsList(controls:PlotControlsList):void { }
		
		//-----------------------------------------------
		//	Templates
		//-----------------------------------------------
		
		private static var templatesManager:BaseTemplatesManager = new GaugePlotTemplatesManager();
		
		public function getTemplatesManager():BaseTemplatesManager {
			return templatesManager;
		}
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		//-----------------------------------------------
		//	Tokens
		//-----------------------------------------------
		
		public function getTokenValue(token:String):* {
			return null;
		}
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		//-----------------------------------------------
		//	Layout
		//-----------------------------------------------
		
		private var baseGauge:GaugeBase;
		private var gaugesMap:Object;
		public var gaugesList:Array;
		
		private var container:Sprite;
		private var bounds:Rectangle;
		
		public function preInitialize():void {}
		public function postInitialize():void {}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.container = new Sprite();
			this.bounds = bounds;
			this.container.addChild(this.baseGauge.initialize(bounds));
			return this.container;
		}
		
		public function getBounds():Rectangle {
			return this.bounds;
		}
		
		//-----------------------------------------------
		//	Drawing
		//-----------------------------------------------
		
		public function draw():void {
			this.baseGauge.draw();
		}
		
		public function calculateResize(newBounds:Rectangle):void {
			this.bounds = newBounds;
		}
		
		public function execResize():void {
			this.baseGauge.resize(this.bounds);
		}
		
		//-----------------------------------------------
		//	Deserialization
		//-----------------------------------------------
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			
			this.baseGauge = new GaugeBase();
			this.gaugesMap = {};
			this.gaugesList = [];
			
			var circularGauges:XMLList = data.circular;
			var linearGauges:XMLList = data.linear;
			var labelGauges:XMLList = data.label;
			var imageGauges:XMLList = data.image;
			var indicatorGauges:XMLList = data.indicator;
			
			var gaugesXMLList:Array = [circularGauges, linearGauges, labelGauges, imageGauges, indicatorGauges];
			var templatesXMLList:Array = [data.circular_template, 
										 data.linear_template, 
										 data.label_template,
										 data.image_template,
										 data.indicator_template];
			
			var cnt:int;
			var i:int;
			if (circularGauges != null) {
				cnt = circularGauges.length();
				for (i = 0;i<cnt;i++)
					this.createGauge(circularGauges[i], gaugesXMLList, templatesXMLList, resources);
			}
			if (linearGauges != null) {
				cnt = linearGauges.length();
				if (cnt > 0 && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE) {
					throw new FeatureNotSupportedError("Linear gauge not supported in this swf version");
				}
				for (i = 0;i<cnt;i++)
					this.createGauge(linearGauges[i], gaugesXMLList, templatesXMLList, resources);
			}
			if (labelGauges != null) {
				cnt = labelGauges.length();
				if (cnt > 0 && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE) {
					throw new FeatureNotSupportedError("Label gauge not supported in this swf version");
				}

				for (i = 0;i<cnt;i++)
					this.createGauge(labelGauges[i], gaugesXMLList, templatesXMLList, resources);
			}
			if (imageGauges != null) {
				cnt = imageGauges.length();
				if (cnt > 0 && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE) {
					throw new FeatureNotSupportedError("Image gauge not supported in this swf version");
				}
				for (i = 0;i<cnt;i++)
					this.createGauge(imageGauges[i], gaugesXMLList, templatesXMLList, resources);
			}
			if (indicatorGauges != null) {
				cnt = indicatorGauges.length();
				if (cnt > 0 && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE) {
					throw new FeatureNotSupportedError("Indicator gauge not supported in this swf version");
				}
				for (i = 0;i<cnt;i++)
					this.createGauge(indicatorGauges[i], gaugesXMLList, templatesXMLList, resources);
			}
		}
		
		private function createGauge(data:XML, 
									 gaugesDataList:Array, 
									 templatesList:Array,
									 resources:ResourcesLoader):void {
			var gaugeName:String;
			var gauge:GaugeBase;
			if (data.@name != undefined) {
				gaugeName = SerializerBase.getLString(data.@name);
				if (this.gaugesMap[gaugeName] != null) return;
			}
			
			var targetTemplatesList:XMLList;
			
			switch (String(data.name())) {
				case "linear": 
					gauge = new LinearGauge();
					targetTemplatesList = templatesList[1];
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Linear gauge not supported in this swf version");
					break;
				case "circular":
					gauge = new CircularGauge();
					targetTemplatesList = templatesList[0];
					break;
				case "label":
					gauge = new LabelGauge();
					targetTemplatesList = templatesList[2];
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Label gauge not supported in this swf version");
					break;
				case "image":
					gauge = new ImageGauge();
					targetTemplatesList = templatesList[3];
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Image gauge not supported in this swf version");
					break;
				case "indicator":
					gauge = new IndicatorGauge();
					targetTemplatesList = templatesList[4];
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Indicator gauge not supported in this swf version");
					break;
				default:
					return;
			}
			if (!gauge.isValid(data))
				return;
			gauge.plot = this;
			
			if (data.@parent != undefined) {
				var parentName:String = SerializerBase.getLString(data.@parent);
				if (this.gaugesMap[parentName] != null) {
					GaugeBase(this.gaugesMap[parentName]).addChild(gauge);
				}else {
					var parentXML:XML;
					for (var i:uint = 0;i<gaugesDataList.length;i++) {
						if (gaugesDataList[i] != null) {
							for (var j:int = 0;j<gaugesDataList[i].length();j++) {
								if (gaugesDataList[i][j].@name != undefined && String(gaugesDataList[i][j].@name).toLowerCase() == parentName) {
									parentXML = gaugesDataList[i][j];
									break;
								}
							}
							if (parentXML != null)
								break;
						}
					}
					if (parentXML != null)
						this.createGauge(parentXML, gaugesDataList, templatesList, resources);
					if (this.gaugesMap[parentName] != null)
						GaugeBase(this.gaugesMap[parentName]).addChild(gauge);
					else
						this.baseGauge.addChild(gauge);
				}
			}else {
				this.baseGauge.addChild(gauge);
			}
			
			var pointers:XMLList = data.pointers[0] != null ? data.pointers[0].pointer : null;
			var actualData:XML = this.applyTemplate(data, targetTemplatesList);
			if (pointers != null) {
				actualData.pointers[0].pointer = pointers;
			}
			
			gauge.deserialize(actualData, resources);
			
			if (gaugeName != null) {
				this.gaugesMap[gaugeName] = gauge;
			}
			this.gaugesList.push(gauge);
		}
		
		protected function applyTemplate(data:XML, templatesList:XMLList):XML {
			
			var templatesParseList:Array = [];
			
			var actual:XML = templatesList.(@name == "default")[0];
			
			var templateName:String = null;
			if (data.@template != undefined) {
				templateName = SerializerBase.getLString(data.@template);
			}else {
				if (data.parent().@[data.name().toString()+"_default_template"] != undefined)
					templateName = SerializerBase.getLString(data.parent().@[data.name().toString()+"_default_template"]);
			}
			
			if (templateName != null) {
				var template:XML = templatesList.(@name == templateName)[0];
				while (template != null) {
					templatesParseList.push(template);
					template = (template.@parent == undefined || template.@parent == template.@name) ? null : templatesList.(@name == template.@parent)[0];
				}
				
				for (var i:int = templatesParseList.length-1;i>=0;i--)
					actual = this.mergeGauge(actual, templatesParseList[i]);
				templatesParseList = null;
			}
			
			var res:XML = this.mergeGauge(actual, data);
			res.setName(data.name().toString()); 
			return res;
		}
		
		private function mergeGauge(actual:XML, data:XML):XML {
			var res:XML = XMLUtils.merge(actual, data, actual.name().toString());
			var style:XML;
			if (actual.styles[0] != null) {
				for each (style in actual.styles[0].children())
					if (style.@name != undefined) style.@name = String(style.@name).toLowerCase();
			}
			if (data.styles[0] != null) {
				for each (style in data.styles[0].children())
					if (style.@name != undefined) style.@name = String(style.@name).toLowerCase();
			}
			var styles:XML = XMLUtils.mergeNamedChildren(actual.styles[0], data.styles[0]);
			if (styles != null)
				res.styles[0] = styles;
				
			if (res.defaults[0] != null) {
				
				var cnt:int;
				var i:int;
				
				if (res.defaults[0].axis[0] != null) {
					var defaultAxis:XML = res.defaults[0].axis[0];
					
					if (res.axis[0] != null) {
						res.axis[0] = this.mergeAxis(defaultAxis, actual.axis[0]);
						res.axis[0] = this.mergeAxis(res.axis[0], data.axis[0]);
					}
					if (res.extra_axes[0] != null) {
						cnt = res.extra_axes[0].axis.length();
						for (i = 0;i<cnt;i++) {
							res.extra_axes[0].axis[i] = this.mergeAxis(defaultAxis, res.extra_axes[0].axis[i]);
						}
					}
				}
			}
			
			return res;
		}
		
		private function mergeAxis(template:XML, data:XML):XML {
			if (data == null && template != null) return template.copy();
			if (data != null && template == null) return data.copy();
			var res:XML = XMLUtils.merge(template, data, data.name().toString());
			var colorRanges:XML = XMLUtils.mergeNamedChildrenByName(template.color_ranges[0], data.color_ranges[0], "color_range");
			if (colorRanges != null)
				res.color_ranges[0] = colorRanges;
			var customLabels:XML = XMLUtils.mergeNamedChildrenByName(template.custom_labels[0], data.custom_labels[0], "custom_label");
			if (customLabels)
				res.custom_labels[0] = customLabels;
			var trendlines:XML = XMLUtils.mergeNamedChildrenByName(template.trendlines[0], data.trendlines[0], "trendline");
			if (trendlines)
				res.trendlines[0] = trendlines;
			 
			return res;
		} 
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		public function serialize(res:Object = null):Object {
			if (res == null) res = {};
			res.gauges = [];
			for (var i:int = 0;i<this.baseGauge.numChildren();i++) {
				var gauge:GaugeBase = this.baseGauge.getChild(i);
				res.gauges.push(gauge.serialize());
			}
			return res;
		}
		
		//---------------------------------------------------------------
		//				Value Change
		//---------------------------------------------------------------
		
		public function setPlotCustomAttribute(attributeName:String, attributeValue:String):void {}
		
		public function addSeries(...seriesData):void {}
		public function removeSeries(sereisId:String):void {}
		public function addSeriesAt(index:uint, seriesData:String):void {}
		public function updateSeries(seriesId:String, seriesData:String):void {}
		public function showSeries(seriesId:String, isVisible:Boolean):void {}
		
		public function showAxisMarker(markerId:String, isVisible:Boolean):void {}
				
		public function addPoint(seriesId:String, ...pointXML):void {}
		public function removePoint(seriesId:String, pointId:String):void {}
		public function addPointAt(seriesId:String, pointIndex:uint, pointData:String):void {}
		public function updatePoint(seriesId:String, pointId:String, pointXML:String):void {}
		
		public function refresh():void {}
		public function clear():void {} 
		
		public function highlightSeries(seriesId:String, highlighted:Boolean):void {}
		public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {}
		public function highlightCategory(categoryName:String, highlighted:Boolean):void {}
		public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {}
		
		//---------------------------------------------------------------
		//				Scroll
		//---------------------------------------------------------------
		
		public function getXScrollInfo():Object { return null; }
		public function getYScrollInfo():Object { return null; }
		
		
	}
}