package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.radarPolarPlot.elements.markerElement.RadarPolarMarkerElement;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class AreaGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:RadarPolarPlotSeries = new AreaSeries();
			series.type = SeriesType.AREA;
			return series;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSettings():BaseSeriesSettings {
			return new LineSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return AreaStyleState;
		}
		
		override public function getSettingsNodeName():String {
			return "area_series";
		}
		
		override public function getStyleNodeName():String {
			return "area_style";
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { 
			return true;
		}
		
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			var color:uint = item.color;
			g.lineStyle(1, ColorUtils.getDarkColor(color), 1);
			g.beginFill(color, 1);
			this.doDrawIcon(seriesType, g, bounds);
			g.endFill();
			g.lineStyle();
		}
		
		protected function doDrawIcon(seriesType:uint, g:Graphics, bounds:Rectangle):void {
			this.drawAreaIcon(g, bounds);
			g.lineStyle();
			g.lineTo(bounds.right, bounds.bottom);
			g.lineTo(bounds.left, bounds.bottom);
		}
		
		private function drawAreaIcon(g:Graphics, bounds:Rectangle):void {
			var wStep:Number = bounds.width/4;
			var hStep:Number = bounds.height/4;
			g.moveTo(bounds.x, bounds.y + hStep*3);
			g.lineTo(bounds.x + wStep, bounds.y + hStep);
			g.lineTo(bounds.x + wStep*2, bounds.y + hStep*2);
			g.lineTo(bounds.right, bounds.y);
		}
		
		override public function seriesCanBeAnimated():Boolean { return true; }
		override public function pointsCanBeAnimated():Boolean { return false; }
		
		override public function createMarkerElement():MarkerElement
		{
			return new RadarPolarMarkerElement();
		}
	}
}