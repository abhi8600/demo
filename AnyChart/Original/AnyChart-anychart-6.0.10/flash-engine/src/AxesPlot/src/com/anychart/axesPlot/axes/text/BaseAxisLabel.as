package com.anychart.axesPlot.axes.text {
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.geom.Point;
	
	public class BaseAxisLabel extends BaseTextElement {
		
		public var padding:Number;
		public var align:uint; 
		public var isInsideLabels:Boolean;
		
		protected var viewPort:IAxisViewPort;
		
		protected var isTextScale:Boolean;
		
		public var showFirst:Boolean;
		public var showLast:Boolean;
		 
		public function BaseAxisLabel(viewPort:IAxisViewPort, isTextScale:Boolean) {
			this.isTextScale = isTextScale;
			this.padding = 5;
			this.align = AxisLabelsAlign.INSIDE;
			this.isInsideLabels = false;
			this.viewPort = viewPort;
			this.showFirst = true;
			this.showLast = true;
			super();
		}
		
		private var numDecimals_:int = 2;
		public function get numDecimals():int {
			return this.numDecimals_; 
		}
		private function parseNumDecimals_(format:String):Number {
			var l:int = 'numDecimals:'.length;
			var ndIndex:int = format.indexOf('numDecimals:');
			if (ndIndex == 0) return -1;
			var nd:String = '';
			for (var i:int = ndIndex + l; i < format.length; i++) {
				nd += format.charAt(i);
				if ((format.charAt(i+1)=='}')) break;
			}
			return Number(nd);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (!this.enabled) return;
			if (data.format[0] != null) { 
				this.text = this.getCDATAString(data.format[0]);
				this.text = this.checkFormat(this.text);
				this.isDynamicText = FormatsParser.isDynamic(this.text);
				if (this.isDynamicText)
					this.dynamicText = FormatsParser.parse(this.text);
				
				this.numDecimals_ = this.parseNumDecimals_(this.text);
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
			
			if (data.@align != undefined) {
				switch (SerializerBase.getEnumItem(data.@align)) {
					case "inside":
						this.align = AxisLabelsAlign.INSIDE;
						break;
					case "outside":
						this.align = AxisLabelsAlign.OUTSIDE;
						break;
					case "center":
						this.align = AxisLabelsAlign.CENTER;
						break;
				}
			}
			
			if (data.@position != undefined)
				this.isInsideLabels = SerializerBase.getEnumItem(data.@position) == "inside";
				
			if (data.@show_first_label != undefined)
				this.showFirst = SerializerBase.getBoolean(data.@show_first_label);
			if (data.@show_last_label != undefined)
				this.showLast = SerializerBase.getBoolean(data.@show_last_label);
		}
		
		protected function checkFormat(format:String):String { return format; }
		
		//----------------------------------------------------------
		//		Angle calculation
		//----------------------------------------------------------
		
		protected var tan:Number;
		protected var absTan:Number;
		
		protected var dxw:Number;
		protected var dyw:Number;
		protected var dxh:Number;
		protected var dyh:Number;
		protected var is180C:Boolean;
		protected var is90C:Boolean;
		
		override protected function initRotation():void {
			super.initRotation();
			this.tan = Math.tan(this.radRotation);
			this.absTan = this.tan;
			if (this.absTan < 0)
				this.absTan = -this.absTan;
				
			this.is180C = this.rotation % 180 == 0;
			this.is90C = this.rotation % 90 == 0;
			
			var qIndex:uint = 0;
			if (this.rotation < 90) 	  qIndex = 0;
			else if (this.rotation < 180) qIndex = 1;
			else if (this.rotation < 270) qIndex = 2;
			else						  qIndex = 3;
			
			this.dxw = this.viewPort.getLabelXOffsetWidthMult(qIndex, this.rotation, this.absSin, this.absCos);
			this.dxh = this.viewPort.getLabelXOffsetHeightMult(qIndex, this.rotation, this.absSin, this.absCos);
			this.dyw = this.viewPort.getLabelYOffsetWidthMult(qIndex, this.rotation, this.absSin, this.absCos);
			this.dyh = this.viewPort.getLabelYOffsetHeightMult(qIndex, this.rotation, this.absSin, this.absCos);
		}
		
		protected final function moveLabel(pos:Point, info:TextElementInformation):void {
			if (this.is180C) {
				pos.x += info.rotatedBounds.width*this.dxw;
				pos.y += info.rotatedBounds.height*this.dyh;
			}else {
				pos.x += info.nonRotatedBounds.width*this.dxw + info.nonRotatedBounds.height*this.dxh;
				pos.y += info.nonRotatedBounds.width*this.dyw + info.nonRotatedBounds.height*this.dyh;
			}
		}

	}
}