package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.styles.IRotationManager;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	public interface IAxisViewPort extends IRotationManager {
		
		/**
		 * Метод необходим для установки угла градиента в зависимости от позиции осей.
		 * Используется для установки градиентов Грида, мажорных и минорных Tickmark, axisLine,zeroLine
		 * @param gradient
		 * 
		 */
		function setGradientRotation(gradient:Gradient):void;
		
		/**
		 * Сохраняет ссылку на Rectangle плота, вызывает метод setPixelRange
		 * для последующей отрисовки Grid.
		 * @param bounds
		 * 
		 */
		function setScaleBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void;
		
		/**
		 * Выставляет пиксельную координату x или y
		 * @param pixValue
		 * @param point
		 * 
		 */
		function setScaleValue(pixValue:Number, point:Point):void;
		
		/**
		 * выставляет пиксельные координаты элементов с позицией X_AXIS (e.g. labels) 
		 * @param offset
		 * @param bounds
		 * @param point
		 * 
		 */
		function setOffset(offset:Number, bounds:Rectangle, point:Point):void;
		
		/**
		 * рисует перпендикулярные оси линии (мажорные или минорные Grid, ZeroLine)
		 * @param g
		 * @param axisValue
		 * 
		 */
		function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void;
		
		/**
		 * выставляет баунды для градиентов перпендикулярных к оси линий (мажорные или минорные Grid, ZeroLine)
		 * @param lineThickness
		 * @param axisValue
		 * @param bounds
		 * 
		 */
		function setPerpendicularLineBounds(lineThickness:Number, axisValue:Number, bounds:Rectangle):void;
		
		/**
		 * Рисует гриды
		 * @param axisStartValue
		 * @param axisEndValue
		 * @param bounds
		 * 
		 */
		function setPerpendicularRectangle(axisStartValue:Number, axisEndValue:Number, bounds:Rectangle):void;
		
		/**
		 * Рисует гриды
		 * @param g
		 * @param bounds
		 * 
		 */
		function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void;
		
		/**
		 * Рисует параллельные оси линии (AxisLine)
		 * @param g
		 * @param start
		 * @param end
		 * @param offset
		 * 
		 */
		function drawParallelLine(g:Graphics, start:Number, end:Number, offset:Number):void;
		
		/**
		 * выставляет баунды для градиентов AxisLine
		 * @param lineThickness
		 * @param start
		 * @param end
		 * @param offset
		 * @param bounds
		 * 
		 */
		function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void;
		
		/**
		 * Выставляет начальную координату AxisMarker
		 * @param value
		 * @param pt
		 * 
		 */
		function setStartValue(value:Number, pt:Point):void;
		
		/**
		 * Выставляет конечную координату AxisMarker
		 * @param value
		 * @param pt
		 * 
		 */
		function  setEndValue (value:Number, pt:Point):void;
		
		
		/**
		 * Выставляеяпт баунды для InsideAxisTickmark
		 *  
		 * @param lineThickness
		 * @param axisValue
		 * @param offset
		 * @param size
		 * @param bounds
		 * 
		 */
		function setInsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void;
		
		/**
		 * Выставляеяпт баунды для OutsideAxisTickmark
		 * @param lineThickness
		 * @param axisValue
		 * @param offset
		 * @param size
		 * @param bounds
		 * 
		 */
		function setOutsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void;
		
		/**
		 *Выставляеяпт баунды для OppositeAxisTickmark
		 *  
		 * @param lineThickness
		 * @param axisValue
		 * @param size
		 * @param bounds
		 * 
		 */
		function setOppositeAxisTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void;
		
		/**
		 *Рисует InsideAxisTickmark
		 *  
		 * @param g
		 * @param axisValue
		 * @param offset
		 * @param size
		 * 
		 */
		function drawInsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void;
		/**
		 *Рисует OutsideAxisTickmark
		 *  
		 * @param g
		 * @param axisValue
		 * @param offset
		 * @param size
		 * 
		 */
		function drawOutsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void;
		/**
		 * Рисует OppositeAxisTickmark
		 * 
		 * @param g
		 * @param axisValue
		 * @param size
		 * 
		 */
		function drawOppositeAxisTickmark(g:Graphics, axisValue:Number, size:Number):void;
		
		/**
		 * Возвращает пространство для лейблов исходя из максимального размера лейбла
		 * для х оси - из наибольшей высоты
		 * для у оси - из наибольшей ширины 
		 * @param bounds
		 * @return 
		 * 
		 */
		function getTextSpace(bounds:Rectangle):Number;
		
		
		/**
		 * Назначает координаты Title 
		 * @param title
		 * @param pos
		 * 
		 */
		function setTitlePosition(title:AxisTitle, pos:Point):void;
		/**
		 * Выставляет угол поворота Title в зависимости от положения оси
		 * @param title
		 * 
		 */
		function setTitleRotation(title:AxisTitle):void;
		
		
		/**
		 * Изменение размеров Плота 
		 * @param bounds
		 * @param offset
		 * 
		 */
		function applyOffset(bounds:Rectangle, offset:Number):void;
		
		/**
		 * Возвращает максимальное колличество лейблов вписывающихся в размер оси
		 * @param maxLabelSize
		 * @return 
		 * 
		 */
		function getMaxLabelsCount(maxLabelSize:Rectangle):int;
		
		/**
		 * Возвращает максимальное колличество повернутых лейблов вписывающихся в размер оси
		 * @param maxNonRotatedLabelSize
		 * @param maxLabelSize
		 * @param rotation
		 * @param absSin
		 * @param absCos
		 * @param absTan
		 * @return 
		 * 
		 */
		function getMaxRotatedLabelsCount(maxNonRotatedLabelSize:Rectangle, 
										  maxLabelSize:Rectangle,
										  rotation:Number, 
										  absSin:Number,
										  absCos:Number,
										  absTan:Number):int;
		
		function isInvertedLabelsCheck(isScaleInverted:Boolean):Boolean;
		function isFirstTypeLabelsOverlap(size:Rectangle, absTan:Number):Boolean;
		
		function getLabelXOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number;
		function getLabelXOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number;
		function getLabelYOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number;
		function getLabelYOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number;
		
		function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void;
		function setMarkerLabelPosition(pixPos:Number, padding:Number, offset:Number, bounds:Rectangle, pos:Point):void;
		function setInsideMarkerLabelPosition(startPixPos:Number, endPixPos:Number, align:uint, padding:Number, bounds:Rectangle, pos:Point):void;
		function isLabelsOverlaped(isInvertedCheck:Boolean,
								   is90C:Boolean, 
								   pos:Point, 
								   info:TextElementInformation, 
								   lastPosition:Number,
								   isT1Overlap:Boolean, 
								   absSin:Number, 
								   absCos:Number):Boolean;

		function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void;
		function isStagerLabelsOverlaped(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation, lastPosition:Number):Boolean;
		function getStagerLabelLastPosition(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation):Number;
								   
		function getLabelLastPosition(isInvertedCheck:Boolean,
								   	  is90C:Boolean, 
								   	  pos:Point, 
								  	  info:TextElementInformation, 
								  	  isT1Overlap:Boolean, 
								   	  absSin:Number, 
								   	  absCos:Number):Number;
		/**
		 * Уменьшает и сдвигает плот что бы вошли первый и последний лейблы 
		 * @param bounds
		 * @param firstLabel
		 * @param lastLabel
		 * 
		 */
		function applyLabelsOffsets(bounds:Rectangle,firstLabel:Rectangle, lastLabel:Rectangle):void;
		
		
		/**
		 * Возвращает ширину или высоту элемента в зависимоти от положения осей 
		 * для определения максимальной ширины/высоты 
		 * @param bounds
		 * @return 
		 * 
		 */
		function getSpace(bounds:Rectangle):Number;
		
		function drawMarkerLine(g:Graphics, startValue:Number, endValue:Number, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean = true, invertedDrawing:Boolean = false):void;
		function getRangeMarkerBounds(startMinValue:Number, endMinValue:Number, startMaxValue:Number, endMaxValue:Number):Rectangle;
		
		function getScrollBarPixPosition(scrollBarPosition:uint, offset:Number, size:Number):Point;
		function applyZoomFactor(bounds:Rectangle, factor:Number):void;
		
		function getVisibleStart():Number;
		function getVisibleEnd():Number;
		function isInverted(inverted:Boolean):Boolean;
		
		function getScaleValue(pixPt:Point):Number;
		function getMouseCoord(e:MouseEvent):Number;
		function getScrollRectStart(rect:Rectangle):Number;
		function checkPix(value:Number):Number;
	}
}