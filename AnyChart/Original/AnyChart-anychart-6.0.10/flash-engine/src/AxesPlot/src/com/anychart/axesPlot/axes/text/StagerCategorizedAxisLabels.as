package com.anychart.axesPlot.axes.text {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class StagerCategorizedAxisLabels extends CategorizedAxisLabels {
		
		public function StagerCategorizedAxisLabels(axis:Axis, viewPort:IAxisViewPort) {
			super(axis, viewPort);
		}
		
		//----------------------------------------------------------
		//		Max size calculation
		//----------------------------------------------------------
		
		private var extraMaxLabelSize:Rectangle;
		
		override public function initMaxLabelSize():void {
			this.maxLabelSize = new Rectangle();
			this.extraMaxLabelSize = new Rectangle();
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
				this.maxLabelSize.width = this.extraMaxLabelSize.width = info.rotatedBounds.width;
				this.maxLabelSize.height = this.extraMaxLabelSize.height = info.rotatedBounds.height;
			}else {
				var majorIntervalsCount:uint = this.axis.scale.majorIntervalsCount; 
				for (var i:uint = 0;i<majorIntervalsCount;i++) {
					if (this.isDynamicText) {
						var categoryIndex:uint = this.axis.scale.getMajorIntervalValue(i)+.5;
						info = BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex).info;
					}
					
					var targetRect:Rectangle = (i%2 == 0) ? this.maxLabelSize : this.extraMaxLabelSize;
					
					if (info.rotatedBounds.width > targetRect.width)
						targetRect.width = info.rotatedBounds.width;
					if (info.rotatedBounds.height > targetRect.height)
						targetRect.height = info.rotatedBounds.height;
				}
			}
		}
		
		//----------------------------------------------------------
		//		Max labels calculation
		//----------------------------------------------------------
		
		override public function calcMaxLabelsCount():uint {
			this.initMaxLabelSize();
			var maxFirstLineLabels:int = this.viewPort.getMaxLabelsCount(this.maxLabelSize);
			var maxSecondLineLabels:int = this.viewPort.getMaxLabelsCount(this.extraMaxLabelSize);
			
			if (maxFirstLineLabels < 1)
				maxFirstLineLabels = 1;
				
			if (maxSecondLineLabels < 1)
				maxSecondLineLabels = 1;
			
			return maxFirstLineLabels+maxSecondLineLabels;
		}
		
		//----------------------------------------------------------
		//		Labels space
		//----------------------------------------------------------
		
		override public function calcSpace():void {
			this.space = this.viewPort.getTextSpace(this.maxLabelSize) + this.viewPort.getTextSpace(this.extraMaxLabelSize) + this.padding;
			this.totalSpace = this.space + this.padding;
		}
		
		//----------------------------------------------------------
		//		Labels drawing
		//----------------------------------------------------------
		
		override public function drawLabels(g:Graphics):void {
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
			}
			
			var pos:Point = new Point();
			
			var checkLabels:Boolean = !this.allowOverlap;
			var isInvertedLabelsCheck:Boolean = checkLabels && this.viewPort.isInvertedLabelsCheck(this.axis.scale.inverted);
			var lastX1:Number = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
			var lastX2:Number = lastX1;
			
			var offset:Number;
			var space:Number;
			
			var space1:Number = this.viewPort.getTextSpace(this.maxLabelSize);
			var space2:Number = this.viewPort.getTextSpace(this.extraMaxLabelSize);
			var offset1:Number = 0;
			var offset2:Number = space1;
			
			var isEven:Boolean = false;
			
			for (var i:uint = 0;i<this.axis.scale.majorIntervalsCount;i++) {
				
				isEven = !isEven;
				
				var categoryIndex:uint = this.axis.scale.getMajorIntervalValue(i)+.5;
				if (this.isDynamicText) {
					info = BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex+.5).info;
				}
				
				if (isEven) {
					space = space1;
					offset = offset1;
				}else {
					space = space2;
					offset = offset2;
				}
				
				var pixPos:Number = this.axis.scale.localTransform(categoryIndex);
				this.viewPort.setStagerLabelPosition(this, info, pos, pixPos, offset, space);

				if (checkLabels) {
					if (isEven) {
						if (this.viewPort.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, info, lastX1))
							continue;
						lastX1 = this.viewPort.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, info);
					}else {
						if (this.viewPort.isStagerLabelsOverlaped(isInvertedLabelsCheck, pos, info, lastX2))
							continue;
						lastX2 = this.viewPort.getStagerLabelLastPosition(isInvertedLabelsCheck, pos, info);						
					}
				}
				
				this.draw(g, pos.x, pos.y, info, 0, 0);
			}
		}
		
		override protected function checkFormat(format:String):String {
			return (this.isTextScale && format.indexOf("{%Value}{numDecimals:2}") != -1) ? format.split("{%Value}{numDecimals:2}").join("{%Value}{enabled:false}") : format;
		}
	}
}