package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.VerticalAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class HorizontalAxisViewPort extends AxisViewPort {
		
		public function HorizontalAxisViewPort(axis:Axis) {
			super(axis);
		}
		
		public function getInvertedAngle(angle:Number, baseScaleInverted:Boolean):Number {
			return baseScaleInverted ? (angle - 180) : (-angle);
		}
		
		public function getNormalAngle(angle:Number, baseScaleInverted:Boolean):Number {
			return baseScaleInverted ? (180 - angle) : angle;
		}
		
		public function getNormalAnchor(anchor:uint):uint {
			return anchor;
		}
		
		public function getNormalHAlign(hAlign:uint, vAlign:uint):uint {
			return hAlign;
		}
		
		public function getNormalVAlign(hAlign:uint, vAlign:uint):uint {
			return vAlign;
		}
		
		public function getInvertedAnchor(anchor:uint):uint {
			switch (anchor) {
				case Anchor.CENTER_BOTTOM: return Anchor.CENTER_TOP;
				case Anchor.CENTER_TOP: return Anchor.CENTER_BOTTOM;
				case Anchor.LEFT_BOTTOM: return Anchor.LEFT_TOP;
				case Anchor.LEFT_TOP: return Anchor.LEFT_BOTTOM;
				case Anchor.RIGHT_BOTTOM: return Anchor.RIGHT_TOP;
				case Anchor.RIGHT_TOP: return Anchor.RIGHT_BOTTOM;
				default: return anchor;
			}
		}
		
		public function getInvertedHAlign(hAlign:uint, vAlign:uint):uint {
			return hAlign;
		}
		
		public function getInvertedVAlign(hAlign:uint, vAlign:uint):uint {
			switch (vAlign) {
				case VerticalAlign.TOP: return VerticalAlign.BOTTOM;
				case VerticalAlign.BOTTOM: return VerticalAlign.TOP;
				default: return vAlign;
			}
		}
		
		public function setScaleValue(pixValue:Number, point:Point):void {
			point.x = pixValue;
		}
		
		public function getScaleValue(pixPt:Point):Number { return pixPt.x; }
		
		override public function setScaleBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			super.setScaleBounds(totalBounds, visibleBounds);
			this.axis.scale.setPixelRange(0, totalBounds.width);
		}
		
		public function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {
			if (!dashed) {
				g.moveTo(axisValue, 0);
				g.lineTo(axisValue, this.bounds.height);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, axisValue, 0, axisValue, this.bounds.height);
			}
		}
		
		public function setStartValue(value:Number, pt:Point):void {
			pt.x = value;
			pt.y = 0;
		}
		
		public function setEndValue(value:Number, pt:Point):void {
			pt.x = value;
			pt.y = this.bounds.height;
		}
		
		public function setPerpendicularLineBounds(lineThickness:Number, axisValue:Number, bounds:Rectangle):void {
			bounds.x = axisValue-lineThickness/2;
			bounds.width = lineThickness;
			bounds.y = 0;
			bounds.height = this.bounds.height;
		}
		
		public function setPerpendicularRectangle(axisStartValue:Number, axisEndValue:Number, bounds:Rectangle):void {
			bounds.x = Math.min(axisStartValue, axisEndValue);
			bounds.width = Math.max(axisStartValue, axisEndValue) - bounds.x;
			bounds.y = 0;
			bounds.height = this.bounds.height;
		}
		
		public function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void {
			g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
		}
		
		public function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void {
			bounds.y = this.yOffset;
			bounds.x = start + this.xOffset;
			bounds.width = end - start;
			bounds.height = lineThickness;
		}
		
		protected final function setTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void {
			bounds.x = axisValue - lineThickness/2;
			bounds.width = lineThickness;
			bounds.height = size; 
		}
		
		public function getTextSpace(bounds:Rectangle):Number {
			return bounds.height;
		}
		
		public function setGradientRotation(g:Gradient):void {
		}
		
		public function setTitlePosition(title:AxisTitle, pos:Point):void {
			switch (title.align) {
				case AbstractAlign.NEAR:
					pos.x = this.visibleBounds.x;
					break;
				case AbstractAlign.CENTER:
					pos.x = this.visibleBounds.x + (this.visibleBounds.width - title.info.rotatedBounds.width)/2;
					break;
				case AbstractAlign.FAR:
					pos.x = this.visibleBounds.right - title.info.rotatedBounds.width;
					break;
			}
			pos.x += this.xOffset;
			pos.y = this.yOffset;
		}
		
		public function setTitleRotation(title:AxisTitle):void {
		}
		
		public function getMaxRotatedLabelsCount(maxNonRotatedLabelSize:Rectangle,
											     maxLabelSize:Rectangle, 
											     rotation:Number,
											     absSin:Number,
											     absCos:Number,
											     absTan:Number):int {
			var w:Number = this.bounds.width;
			var maxLabels:int = 0;
			var labelW:Number;
			if (rotation % 90 != 0) {
				var dl:Number = maxNonRotatedLabelSize.width*absTan;
				maxLabels = (dl < maxNonRotatedLabelSize.height) ? (Number((w*absCos)/maxNonRotatedLabelSize.width)) :
															  	   (Number((w*absSin)/maxNonRotatedLabelSize.height));
			}else if (rotation % 180 != 0) {
				maxLabels = w/maxNonRotatedLabelSize.height;
			}else {
				maxLabels = w/maxLabelSize.width;
			}
			return maxLabels;
	    }
	    
	    public function getMaxLabelsCount(size:Rectangle):int {
	    	return this.bounds.width/size.width;
	    }
	    
	    public function isInvertedLabelsCheck(isScaleInverted:Boolean):Boolean {
	    	return isScaleInverted;
	    }
	    
	    public function isFirstTypeLabelsOverlap(size:Rectangle, absTan:Number):Boolean {
	    	return (size.width*absTan) < size.height;
	    }
	    
		public function getLabelYOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			return 0;
		}
		
		public function getLabelYOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			return 0;
		}
		
		public function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void {
			pos.x = pixPos+this.xOffset;
			pos.y = this.yOffset;
		}
		
		public function setMarkerLabelPosition(pixPos:Number, 
											   padding:Number, 
											   offset:Number,
											   bounds:Rectangle,
											   pos:Point):void {
			pos.x = pixPos - bounds.width/2 + this.xOffset;
			pos.y = this.yOffset;
	    }
	    
		public function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void {
			pos.x = pixPos - info.rotatedBounds.width/2 + this.xOffset;
			pos.y = this.yOffset;
		}
		
		public function isLabelsOverlaped(isInvertedCheck:Boolean,
										   is90C:Boolean, 
								   		   pos:Point, 
										   info:TextElementInformation, 
										   lastPosition:Number,
										   isT1Overlap:Boolean, 
										   absSin:Number, 
										   absCos:Number):Boolean {
			
			if (isInvertedCheck) {
				if (is90C) {
					return (pos.x + info.rotatedBounds.width > lastPosition);
				}else {
					var dw:Number = (isT1Overlap ? (info.nonRotatedBounds.width/absCos) : (info.nonRotatedBounds.height/absSin));
					return (pos.x + dw > lastPosition);
				}
			}
			return (pos.x < lastPosition);
		}
		
		public function getLabelLastPosition(isInvertedCheck:Boolean,
										   	  is90C:Boolean, 
										   	  pos:Point, 
										  	  info:TextElementInformation, 
										  	  isT1Overlap:Boolean, 
										   	  absSin:Number, 
										   	  absCos:Number):Number {
			if (isInvertedCheck)
				return pos.x;
			return pos.x + (is90C ? (info.rotatedBounds.width) :
									 (isT1Overlap ? (info.nonRotatedBounds.width/absCos) : (info.nonRotatedBounds.height/absSin))
							);
 		}
 		
 		public function isStagerLabelsOverlaped(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation, lastPosition:Number):Boolean {
 			if (isInvertedCheck) 
				return (pos.x + info.rotatedBounds.width > lastPosition);
			return (pos.x < lastPosition);
 		}
 		
		public function getStagerLabelLastPosition(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation):Number {
			if (isInvertedCheck)
				return pos.x;
			return pos.x + info.rotatedBounds.width;
		}
		
		public function applyLabelsOffsets(bounds:Rectangle, firstLabel:Rectangle, lastLabel:Rectangle):void {
			var leftLabel:Rectangle = firstLabel;
			var rightLabel:Rectangle = lastLabel;
			if (this.axis.scale.inverted) {
				leftLabel = lastLabel;
				rightLabel = firstLabel;
			}
			if (leftLabel.x < 0) {
				bounds.x += -leftLabel.x;
				bounds.width += leftLabel.x; 
			}
			if (rightLabel.x + rightLabel.width > this.bounds.x + this.bounds.width) {
				bounds.width -= (rightLabel.x + rightLabel.width) - this.bounds.width - this.bounds.x;
			}
		}
		
		public function getSpace(bounds:Rectangle):Number {
			return bounds.height;
		}
		
		public function applyZoomFactor(bounds:Rectangle, factor:Number):void {
			bounds.width *= factor;
		}
		
		public function getVisibleStart():Number { return this.visibleBounds.left; }
		public function getVisibleEnd():Number { return this.visibleBounds.right; }
		
		public function getMouseCoord(e:MouseEvent):Number { return e.stageX; }
		public function getScrollRectStart(rect:Rectangle):Number { return rect.x; }
	}
}