package com.anychart.axesPlot.axes.markers {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class RangeAxisMarker extends AxisMarker {
		
		private var minimum:Number;
		private var maximum:Number;
		private var minimumStart:Number;
		private var minimumEnd:Number;
		private var maximumStart:Number;
		private var maximumEnd:Number;
		
		private var strMinimum:String;
		private var strMaximum:String;
		private var strMinimumStart:String;
		private var strMinimumEnd:String;
		private var strMaximumStart:String;
		private var strMaximumEnd:String;
		
		public function RangeAxisMarker(axis:Axis) {
			super(axis);
		}
		
		override protected function getStyleNodeName():String {
			return "range_axis_marker_style";
		}
		
		override protected function getStyleStateClass():Class {
			return RangeAxisMarkerStyleState;
		}
		
		override public function initialize():void {
			if (this.strMaximum != null) {
				this.maximumStart = this.maximumEnd = this.maximum = this.getValue(this.strMaximum);
			}
				
			if (this.strMaximumStart)
				this.maximumStart = this.getValue(this.strMaximumStart);
				
			if (this.strMaximumEnd)
				this.maximumEnd = this.getValue(this.strMaximumEnd);
			
			if (this.strMinimum != null) {
				this.minimumStart = this.minimumEnd = this.minimum = this.getValue(this.strMinimum);
			}
				
			if (this.strMinimumStart)
				this.minimumStart = this.getValue(this.strMinimumStart);
				
			if (this.strMinimumEnd)
				this.minimumEnd = this.getValue(this.strMinimumEnd);
			
			if (this.affectScaleRange && !(this.axis is BaseCategorizedAxis)) {
				this.checkScale(this.minimumStart);
				this.checkScale(this.minimumEnd);
				this.checkScale(this.maximumStart);
				this.checkScale(this.maximumEnd);
			}
			
			super.initialize();
		}
		
		override public function deserialize(collectionData:XML, data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(collectionData, data, styles, resources);
			if (data.@minimum != undefined)
				this.strMinimum = SerializerBase.getString(data.@minimum);
				
			if (data.@minimum_start != undefined)
				this.strMinimumStart = SerializerBase.getString(data.@minimum_start);
				
			if (data.@minimum_end != undefined) 
				this.strMinimumEnd = SerializerBase.getString(data.@minimum_end);
			
			if (data.@maximum != undefined)
				this.strMaximum = SerializerBase.getString(data.@maximum);
				
			if (data.@maximum_start != undefined)
				this.strMaximumStart = SerializerBase.getString(data.@maximum_start);
				
			if (data.@maximum_end != undefined)
				this.strMaximumEnd = SerializerBase.getString(data.@maximum_end);
		}
		
		override protected function execDrawing():void {
		  if (this.visible)
		  { 	
			super.execDrawing();
			var s:RangeAxisMarkerStyleState = RangeAxisMarkerStyleState(this.style.normal);
			
			var pixMinStart:Number = this.scale.transform(this.minimumStart);
			var pixMinEnd:Number = this.scale.transform(this.minimumEnd);
			var pixMaxStart:Number = this.scale.transform(this.maximumStart);
			var pixMaxEnd:Number = this.scale.transform(this.maximumEnd);
			
			var bounds:Rectangle = this.viewPort.getRangeMarkerBounds(pixMinStart, pixMinEnd, pixMaxStart, pixMaxEnd);
			var g:Graphics = this.container.graphics;
			
			if (s.fill != null)
				s.fill.begin(g, bounds);

			var dashed:Boolean = false;
			var dashOn:Number;
			var dashOff:Number;
			if (s.minimumLine != null) {
				s.minimumLine.apply(g);
				dashed = s.minimumLine.dashed;
				dashOn = s.minimumLine.dashLength;
				dashOff = s.minimumLine.spaceLength;
			}
			this.viewPort.drawMarkerLine(g, pixMinStart, pixMinEnd, dashed, dashOn, dashOff, true); 
			
			g.lineStyle();
			
			dashed = false;
			if (s.maximumLine != null) {
				s.maximumLine.apply(g);
				dashed = s.maximumLine.dashed;
				dashOn = s.maximumLine.dashLength;
				dashOff = s.maximumLine.spaceLength;
			}
			this.viewPort.drawMarkerLine(g, pixMaxStart, pixMaxEnd, dashed, dashOn, dashOff, false, true);
			
			g.lineStyle();
			g.endFill();
			
			if (s.hatchFill != null) {
				s.hatchFill.beginFill(g);
				this.viewPort.drawMarkerLine(g, pixMinStart, pixMinEnd, false, 0, 0, true);
				this.viewPort.drawMarkerLine(g, pixMaxStart, pixMaxEnd, false, 0, 0, false, true);
				g.endFill();
			}
			
			if (s.label != null)
				this.drawLabel(s.label, (this.minimumStart + this.maximumStart)/2, (this.minimumEnd + this.maximumEnd)/2);
		  }		
		}
		
		override public function getTokenValue(token:String):* {
			if (token == "%Value") return (this.minimumStart + this.minimumEnd + this.maximumStart + this.maximumEnd)/4;
			if (token == "%MinValue" || token == "%Min") return (this.minimumStart+this.minimumEnd)/2;
			if (token == "%MaxValue" || token == "%Max") return (this.maximumStart+this.maximumEnd)/2;
			if (token == "%MinStartValue" || token == "%MinStart") return this.minimumStart;
			if (token == "%MaxStartValue" || token == "%MaxStart") return this.maximumStart;
			if (token == "%MinEndValue" || token == "%MinEnd") return this.minimumEnd;
			if (token == "%MaxEndValue" || token == "%MaxEnd") return this.maximumEnd;
	    	return super.getTokenValue(token);
	    }
	}
}