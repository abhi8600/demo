package com.anychart.axesPlot.scales.dateTime {
	public final class DateTimeUnit {
		public static const YEAR:uint = 0;
		public static const MONTH:uint = 1;
		public static const DAY:uint = 2;
		public static const HOUR:uint = 3;
		public static const MINUTE:uint = 4;
		public static const SECOND:uint = 5;
		
		internal static const MILISECONDS_PER_SECOND:Number = 1000;
		internal static const MILISECONDS_PER_MINUTE:Number = 60*MILISECONDS_PER_SECOND;
		internal static const MILISECONDS_PER_HOUR:Number = 60*MILISECONDS_PER_MINUTE;
		internal static const MILISECONDS_PER_DAY:Number = 24*MILISECONDS_PER_HOUR;
		internal static const MILISECONDS_PER_YEAR:Number = 365*MILISECONDS_PER_DAY;
		
		internal static function increaseDate(dateValue:Number, unit:uint, step:Number):Number {
			var date:Date = new Date(dateValue);
			switch (unit) {
				case DateTimeUnit.YEAR:
					DateUtils.addFloatYear(date, step);
					break;
				case DateTimeUnit.MONTH:
					DateUtils.addFloatMonths(date, step);
					break;
				case DateTimeUnit.DAY:
					DateUtils.addFloatDays(date, step);
					break;
				case DateTimeUnit.HOUR:
					DateUtils.addFloatHours(date, step);
					break;
				case DateTimeUnit.MINUTE:
					DateUtils.addFloatMinutes(date, step);
					break;
				case DateTimeUnit.SECOND:
					DateUtils.addFloatSeconds(date, step);
					break;
			}
			
			return date.getTime();
		}
		
		internal static function getEvenStepDate(unit:uint, dateValue:Number, d:Boolean):Number {
			
			var date:Date = new Date(dateValue);
			var direction:int = (d) ? 1 : 0; 
			
			var year:int = date.getUTCFullYear();
			var month:int = date.getUTCMonth();
			var day:int = date.getUTCDate();
			var hour:int = date.getUTCHours();
			var minute:int = date.getUTCMinutes();
			var second:int = date.getUTCSeconds();
			
			var isFirstMonth:Boolean = month == 0; 
			var isFirstDay:Boolean = day == 1;
			var isFirstHour:Boolean = hour == 0;
			var isFirstMinute:Boolean = minute == 0;
			var isFirstSecond:Boolean = second == 0;
			
			switch (unit) {
				case YEAR:
				default:
					if (d && isFirstMonth && isFirstDay && isFirstHour && isFirstMinute && isFirstSecond) 
						return dateValue;
					return new Date(Date.UTC(year + direction, 0)).getTime();
					
				case MONTH:
					if (d && isFirstDay && isFirstHour && isFirstMinute && isFirstSecond)
						return dateValue;
					return new Date(Date.UTC(year, month + direction)).getTime();
					
				case DAY:
					if (d && isFirstHour && isFirstMinute && isFirstSecond)
						return dateValue;
					return new Date(Date.UTC(year, month, day + direction)).getTime();
					
				case HOUR:
					if (d && isFirstMinute && isFirstSecond)
						return dateValue;
					return new Date(Date.UTC(year, month, day, hour + direction)).getTime();
					
				case MINUTE:
					if (d && isFirstSecond)
						return dateValue;
					return new Date(Date.UTC(year, month, day, hour, minute+direction)).getTime();
					
				case SECOND:
					return new Date(Date.UTC(year, month, day, hour, minute, second + direction)).getTime();
			}
		}
	}
}