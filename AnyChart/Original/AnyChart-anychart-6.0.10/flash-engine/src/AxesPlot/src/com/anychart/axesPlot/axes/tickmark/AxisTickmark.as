package com.anychart.axesPlot.axes.tickmark {
	
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class AxisTickmark implements ISerializable {
		public var stroke:Stroke;
		public var size:Number = 10;
		public var inside:Boolean = false;
		public var outside:Boolean = true;
		public var opposite:Boolean = false;
		
		public function AxisTickmark() {
			this.stroke = new Stroke();
			this.stroke.pixelHinting = true;
			this.stroke.caps = CapsStyle.NONE;
		}
		
		public function deserialize(data:XML):void {
			this.stroke.deserialize(data);
			if (data.@size != undefined) this.size = SerializerBase.getNumber(data.@size);
			if (data.@inside != undefined) this.inside = SerializerBase.getBoolean(data.@inside);
			if (data.@outside != undefined) this.outside = SerializerBase.getBoolean(data.@outside);
			if (data.@opposite != undefined) this.opposite = SerializerBase.getBoolean(data.@opposite);
			if (!this.inside && !this.outside && !this.opposite)
				this.stroke = null;
			else
				this.bounds = new Rectangle();
		}
		
		private var bounds:Rectangle;
		
		public function draw(g:Graphics, isGradient:Boolean, pixValue:Number, offset:Number, viewPort:IAxisViewPort):void {
			var thickness:Number = this.stroke.thickness;
			
			if (this.inside) {
				if (isGradient) {
					viewPort.setInsideAxisTickmarkBounds(thickness, pixValue, offset, this.size, this.bounds);
					this.stroke.apply(g, this.bounds);
				}
				
				viewPort.drawInsideAxisTickmark(g, pixValue, offset, this.size);
				if (isGradient)
					g.lineStyle();
			}
			
			if (this.outside) {
				if (isGradient) {
					viewPort.setOutsideAxisTickmarkBounds(thickness, pixValue, offset, this.size, this.bounds);
					this.stroke.apply(g, this.bounds);
				}
				
				viewPort.drawOutsideAxisTickmark(g, pixValue, offset, this.size);
				if (isGradient)
					g.lineStyle();
			}
			
			if (this.opposite) {
				if (isGradient) {
					viewPort.setOppositeAxisTickmarkBounds(thickness, pixValue, this.size, this.bounds);
					this.stroke.apply(g, this.bounds);
				}
				
				viewPort.drawOppositeAxisTickmark(g, pixValue, this.size);
				if (isGradient)
					g.lineStyle();
			}
		}
	}
}