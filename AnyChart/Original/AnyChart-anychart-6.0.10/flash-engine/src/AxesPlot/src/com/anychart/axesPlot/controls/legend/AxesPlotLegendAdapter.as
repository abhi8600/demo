package com.anychart.axesPlot.controls.legend {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.axes.categorization.CategoryInfo;
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.controls.legend.LegendAdapter;
	import com.anychart.seriesPlot.controls.legend.LegendAdaptiveItem;

	public final class AxesPlotLegendAdapter extends LegendAdapter {
		
		public function AxesPlotLegendAdapter(plot:AxesPlot) {
			super(plot);
		}
		
		override public function getLegendDataBySource(source:String, item:XML, container:ILegendItemsContainer):void {
			if (source == "categories") {
				var categoryName:String = item.@category != undefined ? SerializerBase.getLString(item.@category) : null;
				var xCategories:Array = AxesPlot(plot).dataCategories;
				for (var i:uint = 0;i<xCategories.length;i++) {
					var category:CategoryInfo = xCategories[i];
					if (categoryName == null || categoryName == category.name.toLowerCase()) {
						category.initColor();
						container.addItem(new LegendAdaptiveItem(category.color, 0, -1, true, category));
					} 
				}
			}else {
				super.getLegendDataBySource(source, item, container);
			}
		}
	}
}