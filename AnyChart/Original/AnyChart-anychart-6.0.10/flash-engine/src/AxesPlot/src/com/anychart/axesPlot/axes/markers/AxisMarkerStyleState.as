package com.anychart.axesPlot.axes.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	
	internal class AxisMarkerStyleState extends StyleState {
		
		public var label:AxisMarkerLabel;
		
		public function AxisMarkerStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (SerializerBase.isEnabled(data.label[0])) {
				this.label = new AxisMarkerLabel();
				this.label.deserialize(data.label[0], resources, this.style);
			}
			return data;
		}
	}
}