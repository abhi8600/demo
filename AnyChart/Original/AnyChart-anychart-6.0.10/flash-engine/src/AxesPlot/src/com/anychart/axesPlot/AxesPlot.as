package com.anychart.axesPlot {
	
	import com.anychart.animation.Animation;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.CategorizedAxis;
	import com.anychart.axesPlot.axes.categorization.CategorizedBySeriesAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.categorization.CategoryInfo;
	import com.anychart.axesPlot.axes.viewPorts.ViewPortFactory;
	import com.anychart.axesPlot.controls.legend.AxesPlotLegendAdapter;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.scales.ValueScale;
	import com.anychart.axesPlot.templates.AxesPlotTemplatesManager;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.cursors.Cursor;
	import com.anychart.data.SeriesType;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.events.AxesPlotScrollEvent;
	import com.anychart.locale.DateTimeLocale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.Position;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Mouse;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	public class AxesPlot extends SeriesPlot implements IAxesPlot {
		
		private var axes:Array;
		private var argumentAxesList:Array;
		private var valueAxesList:Array;
		
		protected var argumentAxes:Object;
		protected var valueAxes:Object;
		
		public var outsideXAxesContainer:Sprite;
		public var outsideYAxesContainer:Sprite;
		private var axesFixedContainer:Sprite;
		
		private var _xCategories:Array;
		private var _yCategories:Array;
		private var _dataCategories:Array;
		public function get xCategories():Array { return this._xCategories; }
		public function get yCategories():Array { return this._yCategories; }
		
		//--------------------- categorised by series hack
		public function get dataCategories():Array { return this._dataCategories; }
		private var _dataCategoriesMap:Object;
		public function getDataCategory(axis:Axis, name:String):CategoryInfo {
			if (this._dataCategoriesMap[name]) return this._dataCategoriesMap[name];
			var c:CategoryInfo = new CategoryInfo(this);
			c.axis = axis;
			c.index = this._dataCategories.length;
			c.name = name;
			
			this._dataCategories.push(c);
			this._dataCategoriesMap[name] = c;
			return c;
		}
		
		public function getAfterDataContainer():Sprite { return this.afterDataContainer; }
		public function getBeforeDataContainer():Sprite { return this.beforeDataContainer; }
		public function getScrollableContainer():Sprite { return this.scrollableContainer; }
		
		public var interactiveScrollContainer:Sprite;
		public var topInteractiveScrollContainer:Sprite;
		
		public function AxesPlot() {
			super();
			this.defaultSeriesType = SeriesType.BAR;
			
			this.axes = [];
			this.argumentAxes = {};
			this.valueAxes = {};
			this.argumentAxesList = [];
			this.valueAxesList = [];
			this._xCategories = [];
			this._yCategories = [];
			this._dataCategories = [];
			this._dataCategoriesMap = {};
			
			this.outsideXAxesContainer = new Sprite();
			this.outsideYAxesContainer = new Sprite();
			this.axesFixedContainer = new Sprite();
			this.interactiveScrollContainer = new Sprite();
			this.topInteractiveScrollContainer = new Sprite();
		}
		
		override public function destroy():void {
			super.destroy();
			var cat:CategoryInfo;
			
			if (this._dataCategories) {
				for each (cat in this._dataCategories)
					cat.destroy();
			}
			this._dataCategories = null;
			
			if (this._dataCategoriesMap) {
				for each (cat in this._dataCategoriesMap)
					cat.destroy();
			}
			this._dataCategoriesMap = null;
			
			if (this._xCategories) {
				for each (cat in this._xCategories)
					cat.destroy();
			}
			this._xCategories = null;
			
			if (this._yCategories) {
				for each (cat in this._yCategories)
					cat.destroy();
			}
			this._yCategories = null;
			
			if (this.axes) {
				for each (var axis:Axis in this.axes)
					axis.destroy();
			}
			this.axes = null;
		}
		
		override public function setPlotType(value:String):void {
			super.setPlotType(value);
			if (this.plotType == "heatmap")
				this.defaultSeriesType = SeriesType.HEAT_MAP;
			this._isHorizontal = this.plotType.indexOf("horizontal") != -1;
			this._isScatter = this.plotType == "scatter";
		}
		
		public function get isHorizontal():Boolean { return this._isHorizontal; }
		public function get isScatter():Boolean { return this._isScatter; }
		private var _isHorizontal:Boolean;
		private var _isScatter:Boolean;
		
		//--------------------------------------------------------
		//					IPlot
		//--------------------------------------------------------
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new AxesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		protected static var templatesManager:BaseTemplatesManager;
		
		override public function getTemplatesManager():BaseTemplatesManager {
			if (templatesManager == null)
				templatesManager = new AxesPlotTemplatesManager();
			return templatesManager;
		}
		
		override protected function initLegendAdapter():void {
			this.legendAdapter = new AxesPlotLegendAdapter(this);
		}
		
		public function get dateTimeLocale():DateTimeLocale { return this.chart.dateTimeLocale; }
		
		override public function checkNoData(data:XML):Boolean {
			if (data.data[0] == null) return true;
			if (data.data[0].series.length() == 0) return true;
			//if (data.data[0].series..point.length() == 0) return true;
			return false;
		}
		
		//--------------------------------------------------------
		//					FORMATTINGS
		//--------------------------------------------------------
		
		override public function resetTokens():void {
			
			super.resetTokens();
			
			var i:int;
			for (i = 0;i<this.xCategories.length;i++) {
				Category(this.xCategories[i]).resetTokens();
			}
			for (i = 0;i<this.yCategories.length;i++) {
				Category(this.yCategories[i]).resetTokens();
			}
			for (i = 0;i<this.dataCategories.length;i++) {
				Category(this.dataCategories[i]).resetTokens();
			} 
			
			for each (var axis:Axis in this.axes) {
				axis.resetTokens();
			}
			
			this.cashedTokens['XSum'] = 0;
			this.cashedTokens['XMax'] = -Number.MAX_VALUE;
			this.cashedTokens['XMin'] = Number.MAX_VALUE;
			this.cashedTokens['RangeMax'] = -Number.MAX_VALUE;
			this.cashedTokens['RangeMin'] = Number.MAX_VALUE;
			this.cashedTokens['RangeSum'] = 0;
		}
		
		override public function getTokenValue(token:String):* {
			if (token == '%DataPlotXSum') return this.cashedTokens['XSum'];
			if (token == '%DataPlotXMax') return this.cashedTokens['XMax'];
			if (token == '%DataPlotXMin') return this.cashedTokens['XMin'];
			if (token == '%DataPlotXAverage') return this.cashedTokens['XSum']/this.cashedTokens['PointCount'];
			if (token == '%DataPlotYRangeMax') return this.cashedTokens['RangeMax'];
			if (token == '%DataPlotYRangeMin') return this.cashedTokens['RangeMin'];
			if (token == '%DataPlotYRangeSum') return this.cashedTokens['RangeSum'];
			return super.getTokenValue(token);
		}
		
		//--------------------------------------------------------
		//					Factory
		//--------------------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			var sClass:Class;
			switch (seriesType) {
				case SeriesType.BUBBLE:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Bubble series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.bubbleSeries::BubbleGlobalSeriesSettings"));
					break;
				case SeriesType.BAR:
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::BarGlobalSeriesSettings"));
					break;
				case SeriesType.RANGE_BAR:
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::RangeBarGlobalSeriesSettings"));
					break;
				case SeriesType.CANDLESTICK:
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.stock::CandlestickGlobalSeriesSettings"));
					break;
				case SeriesType.OHLC:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("OHLC series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.stock::OHLCGlobalSeriesSettings"));
					break;
				case SeriesType.LINE:
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::LineGlobalSeriesSettings"));
					break;
				case SeriesType.STEP_LINE_BACKWARD:
				case SeriesType.STEP_LINE_FORWARD:
				case SeriesType.SPLINE:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Step line and spline series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::LineGlobalSeriesSettings"));
					break;
				case SeriesType.AREA:
				case SeriesType.SPLINE_AREA:
				case SeriesType.STEP_AREA_BACKWARD:
				case SeriesType.STEP_AREA_FORWARD:
				case SeriesType.SPLINE_AREA:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Area series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::AreaGlobalSeriesSettings"));
					break;
				case SeriesType.RANGE_AREA:
				case SeriesType.RANGE_SPLINE_AREA:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Range area series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::RangeAreaGlobalSeriesSettings"));
					break;
					
				case SeriesType.MARKER:
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.markerSeries::MarkerGlobalSeriesSettings"));
					break;
				case SeriesType.HEAT_MAP:
					if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
						throw new FeatureNotSupportedError("Heat map series not supported in this swf version");
					sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.heatMap::HeatMapGlobalSeriesSettings"));
					break;
			}
			return new sClass();
		}
		
		override protected function registerSettings(type:uint, settings:GlobalSeriesSettings):void {
			switch (type) {
				case SeriesType.LINE:
				case SeriesType.STEP_LINE_BACKWARD:
				case SeriesType.STEP_LINE_FORWARD:
				case SeriesType.SPLINE:
					this.settings[SeriesType.LINE] = settings;
					this.settings[SeriesType.STEP_LINE_BACKWARD] = settings;
					this.settings[SeriesType.STEP_LINE_FORWARD] = settings;
					this.settings[SeriesType.SPLINE] = settings;
					break;
				case SeriesType.AREA:
				case SeriesType.SPLINE_AREA:
				case SeriesType.STEP_AREA_BACKWARD:
				case SeriesType.STEP_AREA_FORWARD:
				case SeriesType.SPLINE_AREA:
					this.settings[SeriesType.AREA] = settings;
					this.settings[SeriesType.SPLINE_AREA] = settings;
					this.settings[SeriesType.STEP_AREA_BACKWARD] = settings;
					this.settings[SeriesType.STEP_AREA_FORWARD] = settings;
					this.settings[SeriesType.SPLINE_AREA] = settings;
					break;
				case SeriesType.RANGE_AREA:
				case SeriesType.RANGE_SPLINE_AREA:
					this.settings[SeriesType.RANGE_AREA] = settings;
					this.settings[SeriesType.RANGE_SPLINE_AREA] = settings;
					break;
				default:
					super.registerSettings(type, settings);
					
			}
		}
		
		override public function getSeriesType(type:*):uint {
			if (this.plotType == "heatmap") return SeriesType.HEAT_MAP;
			if (type == null) return this.defaultSeriesType;
			switch (SerializerBase.getEnumItem(type)) {
				case "bubble": 		return SeriesType.BUBBLE;
				case "bar":    		return SeriesType.BAR;
				case "rangebar":	return SeriesType.RANGE_BAR;
				case "candlestick":	return SeriesType.CANDLESTICK;
				case "ohlc":		return SeriesType.OHLC;
				case "line":		return SeriesType.LINE;
				case "steplineforward": return SeriesType.STEP_LINE_FORWARD;
				case "steplinebackward": return SeriesType.STEP_LINE_BACKWARD;
				case "spline":		return SeriesType.SPLINE;
				case "area":		return SeriesType.AREA;
				case "splinearea":	return SeriesType.SPLINE_AREA;
				case "stepareaforward":
				case "steplineforwardarea":	
					return SeriesType.STEP_AREA_FORWARD;
				case "stepareabackward":
				case "steplinebackwardarea": 
					return SeriesType.STEP_AREA_BACKWARD;
				case "rangearea":	return SeriesType.RANGE_AREA;
				case "rangesplinearea": return SeriesType.RANGE_SPLINE_AREA;
				case "marker":		return SeriesType.MARKER;
				default:			return this.defaultSeriesType;
			}
		}
		
		//--------------------------------------------------------
		//					Deserialization
		//--------------------------------------------------------
		
		override protected function needCheckPointNames(series:BaseSeries):Boolean { return !this.isScatter; }
		
		override protected function deserializeChartSettings(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserializeChartSettings(data, styles,resources);
			this.hasLeftAxis = false;
			this.hasRightAxis = false;
			this.hasTopAxis = false;
			this.hasBottomAxis = false;
			
			this.createAxis(data.axes[0], data.axes[0].x_axis[0], true, styles, resources);
			this.createAxis(data.axes[0], data.axes[0].y_axis[0], true, styles, resources);
			
			var childrenNodes:XMLList = data.axes[0].children();
			for each (var child:XML in childrenNodes) {
				if (child.name().toString().indexOf("extra_y_axis_") == 0) {
					child.@name = child.name().toString();
					child.setName("y_axis");
					this.createAxis(data.axes[0], child, false, styles, resources);
				}
			}
			
			if (data.axes[0].extra[0] != null) {
				var extraAxes:XMLList = data.axes[0].extra[0].children();
				var extraAxesCnt:uint = extraAxes.length();
				for (var i:int = 0;i<extraAxesCnt;i++)
					this.createAxis(data.axes[0], extraAxes[i], false, styles, resources);
			}
		}
		
		override protected function deserializeSeriesArguments(series:BaseSeries, seriesNode:XML):void {
			var axSeries:AxesPlotSeries = AxesPlotSeries(series);
			var argumentAxisName:String = (seriesNode.@x_axis != undefined && this.argumentAxes[String(seriesNode.@x_axis).toLowerCase()] != null) ? String(seriesNode.@x_axis).toLowerCase() : "primary";
			var valueAxisName:String = (seriesNode.@y_axis != undefined && this.valueAxes[String(seriesNode.@y_axis).toLowerCase()] != null) ? String(seriesNode.@y_axis).toLowerCase() : "primary";
			
			axSeries.argumentAxis = this.argumentAxes[argumentAxisName];
			axSeries.valueAxis = this.valueAxes[valueAxisName];
			
			axSeries.argumentAxis.checkSeries(axSeries);
			axSeries.configureValueScale(axSeries.valueAxis.scale); 
		}
		
		override protected function onAfterDeserializeData(data:XML, resources:ResourcesLoader):void {
			var i:uint;
			for (i = 0;i<this.axes.length;i++)
				this.axes[i].onAfterDataDeserialize(this.drawingPoints);
				
			var baseAxis:Axis = this.argumentAxesList[0];
				
			for (i = 0;i<StylesList.stylesList.length;i++)
				Style(StylesList.stylesList[i]).initializeRotations(baseAxis.viewPort, baseAxis.scale.inverted);
		}
		
		private var hasLeftAxis:Boolean;
		private var hasRightAxis:Boolean;
		private var hasTopAxis:Boolean;
		private var hasBottomAxis:Boolean;
		
		protected function getViewPortFactoryClass():Class {
			return ViewPortFactory;
		}
		
		private function createAxis(axesNode:XML, axisNode:XML, isPrimary:Boolean, styles:XML, resources:ResourcesLoader):void {
			var axisNodeName:String = axisNode.name().toString();
			
			var isValue:Boolean = axisNode.scale[0] != null && SerializerBase.getEnumItem(axisNode.scale[0].@type) == "linear";
			
			if ((axisNodeName != "x_axis" && axisNodeName != "y_axis") || (!isPrimary && axisNode.@name == undefined)) return;
			
			var isScatter:Boolean = this._isScatter;
			var isHeatMap:Boolean = this.plotType == "heatmap";
			var isCategorizedBySeries:Boolean = this.plotType == "categorizedbyserieshorizontal" || this.plotType == "categorizedbyseriesvertical";
			var isCategorizedHorizontal:Boolean = this.plotType == "categorizedhorizontal" || this.plotType == "categorizedbyserieshorizontal";
			var isArgument:Boolean = axisNodeName == "x_axis"; 
			
			var axis:Axis;
			if (!isHeatMap && (isScatter || !isArgument || isValue))
				axis = new ValueAxis();
			else if (!isHeatMap && isCategorizedBySeries)
				axis = new CategorizedBySeriesAxis();
			else 
				axis = new CategorizedAxis();
			axis.plot = this;
			
			if (axisNode.@position != undefined) {
				if (SerializerBase.getEnumItem(axisNode.@position) == "bottom")
					axisNode.@position = "opposite";
				else if (SerializerBase.getEnumItem(axisNode.@position) == "right")
					axisNode.@position = "opposite";
			}
			
			var isOpposite:Boolean = (axisNode.@position != undefined && SerializerBase.getEnumItem(axisNode.@position) == "opposite") ||
									 (axisNode.@position == undefined && !isPrimary);
			
			var isXAxis:Boolean = (isArgument && (isScatter || !isCategorizedHorizontal)) || 
								  (!isArgument && isCategorizedHorizontal)								  
			
			if (isCategorizedHorizontal && isXAxis)
				isOpposite = !isOpposite;
			
			axis.isArgument = isArgument;
			
			axis.deserializeScale(axisNode);
			
			var isCrossAxis:Boolean = ValueScale(axis.scale).isCrossingEnabled;
			axis.isFirstInList = isCrossAxis;
			if (!isXAxis) {
				if (isOpposite) {
					axis.position = Position.RIGHT;
					if (!isCrossAxis && !this.hasRightAxis) {
						axis.isFirstInList = true;
						this.hasRightAxis = true;
					}
				}else {
					axis.position = Position.LEFT;
					if (!isCrossAxis && !this.hasLeftAxis) {
						axis.isFirstInList = true;
						this.hasLeftAxis = true;
					}
				}
			}else {
				if (isOpposite) {
					axis.position = Position.TOP;
					if (!isCrossAxis && !this.hasTopAxis) {
						axis.isFirstInList = true;
						this.hasTopAxis = true;
					}
				}else {
					axis.position = Position.BOTTOM;
					if (!isCrossAxis && !this.hasBottomAxis) {
						axis.isFirstInList = true;
						this.hasBottomAxis = true;
					}
				}
			}
			
			axis.isOpposite = !isPrimary && 
				(isArgument ? Axis(this.argumentAxes["primary"]) : 
							  Axis(this.valueAxes["primary"]))
				.position != axis.position;
			
			axis.setContainers(this.axesFixedContainer, isXAxis ? this.outsideXAxesContainer : this.outsideYAxesContainer);
			axis.createViewPort(this.getViewPortFactoryClass());
			axis.deserialize(axisNode,styles,resources);
			axis.deserializeScrollBar(axesNode, axisNode, resources);
			
			var axisKey:String = isPrimary ? "primary" : String(axisNode.@name).toLowerCase();
			axis.name = axisKey;
			if (isCategorizedHorizontal && isArgument)
				axis.scale.inverted = !axis.scale.inverted;
				
			if (isCategorizedHorizontal && !isArgument && Axis(this.argumentAxes["primary"]).position == Position.RIGHT)
				axis.scale.inverted = !axis.scale.inverted;
				
			if (isArgument) {
				this.argumentAxes[axisKey] = axis;
				this.argumentAxesList.push(axis);
			}else {
				this.valueAxes[axisKey] = axis;
				this.valueAxesList.push(axis);
			}
				
			this.axes.push(axis);
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			this.baseBounds = bounds.clone();
			this.initializeAxes();
			var sprite:DisplayObjectContainer = DisplayObjectContainer(super.initialize(bounds));
			this.checkOrder();
			sprite.addChild(this.outsideXAxesContainer);
			sprite.addChild(this.outsideYAxesContainer);
			sprite.addChild(this.axesFixedContainer);
			sprite.addChild(this.topInteractiveScrollContainer);
			this.topInteractiveScrollContainer.visible = false;
			
			this.beforeDataContainer.mouseChildren = false;
			this.beforeDataContainer.mouseEnabled = false;
			this.afterDataContainer.mouseChildren = false;
			this.afterDataContainer.mouseEnabled = false;
			this.initAxesElementsBounds();
			
			this.initializeAxesZoom();
			
			bounds = this.setAxesSize(bounds);
			this.setContainersPositions(bounds);
			this.initializeAxesScroll();
			
			for each (var axis:Axis in this.axes) {
				axis.initMarkers();
				axis.execZoom();
			}
			
			this.checkDrag();
			
			return sprite;
		}
		
		override public function recalculateAndRedraw():void {
			
			var i:int = 0;
			
			for each (var settings:GlobalSeriesSettings in this.settings) {
				settings.reset();
			}
			
			for (i = 0;i<this.valueAxesList.length;i++) 
				Axis(this.valueAxesList[i]).recalculateDataRange();
			
			for (i = 0;i<this.argumentAxesList.length;i++) {
				Axis(this.argumentAxesList[i]).recalculateDataRange();
			}
			
			super.recalculateAndRedraw();
		}
		
		override protected function addBottomExtraContainers():void {
			this.sprite.addChild(this.interactiveScrollContainer);
		}
		
		private function initializeAxes():void {
			for each (var axis:Axis in this.axes) {
				var scale:ValueScale = ValueScale(axis.scale);
				if (scale.isCrossingEnabled) {
					axis.crossAxis = axis.isArgument ? this.valueAxes["primary"] : this.argumentAxes["primary"];
					scale.crossValue = axis.crossAxis.scale.deserializeValue(scale.crossValueAsString);
				}
				
				if (!axis.hasData && !axis.isArgument && axis.name != "primary") {
					axis.copyScale(this.valueAxes["primary"]);
				}
			}
		}
		
		private function initializeAxesZoom():void {
			Axis(this.valueAxes["primary"]).initZoom();
			Axis(this.argumentAxes["primary"]).initZoom();
		}
		
		private function initializeAxesScroll():void {
			Axis(this.valueAxes["primary"]).initScrollBar(this.bounds.clone());
			Axis(this.argumentAxes["primary"]).initScrollBar(this.bounds.clone());
		}
		
		private var currentX:Number;
		private var currentY:Number;
		private var _isExternalCall_ScrollBarChangeX:Boolean = true; // should be FALSE, but on initial it'll be changing to FALSE
		public function get isExternalCall_ScrollBarChangeX ():Boolean { return this._isExternalCall_ScrollBarChangeX;}
		public function set isExternalCall_ScrollBarChangeX ( value:Boolean ):void { this._isExternalCall_ScrollBarChangeX = value;}
		private var _isExternalCall_ScrollBarChangeY:Boolean = true; // should be FALSE, but on initial it'll be changing to FALSE
		public function get isExternalCall_ScrollBarChangeY ():Boolean { return this._isExternalCall_ScrollBarChangeY;}
		public function set isExternalCall_ScrollBarChangeY ( value:Boolean ):void { this._isExternalCall_ScrollBarChangeY = value;}
		
		public function moveDataView(startX:Number, startY:Number, callFromScrollBar:Boolean = false):void {
			var scrollRect:Rectangle = this.scrollableContainer.scrollRect.clone();
			var pos:Point = this.getDataViewPixPosition(startX, startY, callFromScrollBar);
			
			scrollRect.x = pos.x - this.getXAxisOutsideContainerXOffset();
			scrollRect.y = pos.y - this.getXAxisOutsideContainerYOffset();
			this.scrollableContainer.scrollRect = scrollRect;
			
			if (this.outsideXAxesContainer.scrollRect) {
				scrollRect = this.outsideXAxesContainer.scrollRect.clone();
				scrollRect.x = pos.x;
				this.outsideXAxesContainer.scrollRect = scrollRect;
			}
			
			if (this.outsideYAxesContainer.scrollRect) {
				scrollRect = this.outsideYAxesContainer.scrollRect.clone();
				scrollRect.y = pos.y;
				this.outsideYAxesContainer.scrollRect = scrollRect;
			}
		}
		
		public function dispatchScrollerEvent(type:String, source:String, phase:String, startValue:Number, endValue:Number, valueRange:Number):void
		{
			if (type !="-" && phase != "-" && this.chart)
				this.chart.dispatchEvent(new AxesPlotScrollEvent(type, source, phase, startValue, endValue, valueRange));
		}
		
		private function validScrollData(scrollData:String, axis:Axis):Number
		{
			// проверка на то, что заданное значение не вываливается за границы шкалы
			var scrollToVal:Number= (isNaN(Number(scrollData))) ? axis.zoomManager.deserializeAxisValue(scrollData) : Number(scrollData);
			if (!isNaN(scrollToVal))
			{
				scrollToVal = (axis.scale.minimum > scrollToVal) ? axis.scale.minimum : scrollToVal;
				scrollToVal = (axis.scale.maximum - axis.zoomManager.realRange < scrollToVal) ? axis.scale.maximum - axis.zoomManager.realRange : scrollToVal;
				axis.zoomManager.changeScrollInfo(scrollToVal);
				return scrollToVal;
			}else return NaN;
		}
		
		override public function scrollTo(xValue:String, yValue:String):void {
			var xNum:Number;
			if (xValue != "-")
			{
				xNum=this.validScrollData(xValue, this.argumentAxes["primary"]);
				this._isExternalCall_ScrollBarChangeX=true;
			} 
			var yNum:Number;
			if (yValue != "-") {
				yNum = this.validScrollData(yValue, this.valueAxes["primary"]);
				this._isExternalCall_ScrollBarChangeY=true;
			} 
			this.moveDataView( xNum, yNum );
		}
		
		override public function scrollXTo(xValue:String):void {
			scrollTo(xValue, "-");
		}
		
		override public function scrollYTo(yValue:String):void {
			scrollTo("-", yValue);
		}
		
		override public function setXZoom(settings:Object):void {
			if (this.setAxisZoom(this.argumentAxes["primary"], settings))
				this.recalculateChartAfterZoom();
		}
		
		override public function setYZoom(settings:Object):void {
			if (this.setAxisZoom(this.valueAxes["primary"], settings))
				this.recalculateChartAfterZoom();
		}
		
		override public function setZoom(xZoomSettings:Object, yZoomSettings:Object):void {
			var isXZoomed:Boolean = xZoomSettings && this.setAxisZoom(this.argumentAxes["primary"], xZoomSettings);
			var isYZoomed:Boolean = yZoomSettings && this.setAxisZoom(this.valueAxes["primary"], yZoomSettings);
			if (isXZoomed || isYZoomed)
				this.recalculateChartAfterZoom();
		}
		
		private function setAxisZoom(axis:Axis, settings:Object):Boolean {
			if (!axis.zoomManager) return false;
			axis.zoomManager.deserializeFromObject(settings);
			axis.zoomManager.update();
			axis.zoomManager.setScrollBaseValue();
			return true;
		}
		
		private function recalculateChartAfterZoom():void {
			if (this.baseBounds) {
				
				this.calculateResize(this.baseBounds);
				this.execResize();
			}
		}
		
		private function getDataViewPixPosition(startX:Number, startY:Number, callFromScrollBar:Boolean = false):Point {
			
			var res:Point = new Point();
			var axis:Axis;
			var isDefaultStartX:Boolean = isNaN(startX);
			if (isNaN(startX) && !isNaN(currentX)) 
				startX = currentX;
			if (!isNaN(startX)) {
				axis = this.argumentAxes["primary"];
				if (!callFromScrollBar && !isDefaultStartX && axis.viewPort.isInverted(axis.scale.inverted)) 
					startX += axis.zoomManager.realRange;
				
				axis.rotateValue(axis.scale.transform(startX), res);
				currentX = startX;
			}
			
			var isDefaultStartY:Boolean = isNaN(startY);
			if (isNaN(startY) && !isNaN(currentY))
				startY = currentY;
			if (!isNaN(startY)) {
				axis = this.valueAxes["primary"];
				if (!callFromScrollBar && !isDefaultStartY && axis.viewPort.isInverted(axis.scale.inverted)) 
					startY += axis.zoomManager.realRange;
					
				axis.rotateValue(axis.scale.transform(startY), res);
				currentY = startY;
			}
			
			return res;
		}
		
		private function checkOrder():void {
			if (this.is3D() || (this.series.length > 0 && SeriesType.isLine(BaseSeries(this.series[0]).type))) {
				this.moveSeriesContainerToTop(this.settings[SeriesType.LINE]);
				this.moveSeriesContainerToTop(this.settings[SeriesType.SPLINE]);
				this.moveSeriesContainerToTop(this.settings[SeriesType.STEP_LINE_BACKWARD]);
				this.moveSeriesContainerToTop(this.settings[SeriesType.STEP_LINE_FORWARD]);
			}
		}
		
		private function moveSeriesContainerToTop(settings:GlobalSeriesSettings):void {
			if (settings && settings.container) {
				var newContainer:Sprite = new Sprite();
				this.dataContainer.addChild(newContainer);
				this.dataContainer.swapChildren(newContainer, settings.container);
			}
		}
		
		protected function initAxesElementsBounds():void {
			for each (var axis:Axis in this.axes) {
				axis.initElementsBounds();
				axis.initMarkers();
			}
		}
		
		override public function calculateResize(bounds:Rectangle):void {
			this.baseBounds = bounds.clone();
			bounds = this.setAxesSize(bounds);
			super.calculateResize(bounds);
		}
		
		override public function execResize():void {
			this.axesFixedContainer.graphics.clear();
			this.outsideXAxesContainer.graphics.clear();
			this.outsideYAxesContainer.graphics.clear();
			
			super.execResize();
			
			for each (var axis:Axis in this.axes) {
				axis.resizeScrollBar(this.bounds);
			}
			this.drawAxes();
		}
		
		protected function preCropBounds(bounds:Rectangle):void {}
		protected function cropBounds(bounds:Rectangle):void {}
		
		public var totalBounds:Rectangle;
		
		private function applyZoom(bounds:Rectangle):Rectangle {
			var totalBounds:Rectangle = bounds.clone();
			var axis:Axis = this.valueAxes["primary"];
			if (axis.zoomManager) axis.zoomManager.apply(totalBounds);
			axis = this.argumentAxes["primary"];
			if (axis.zoomManager) axis.zoomManager.apply(totalBounds);
			return totalBounds;
		}
		
		protected function setAxesSize(bounds:Rectangle):Rectangle {
			
			this.preCropBounds(bounds);
			
			var axis:Axis;
			
			var baseBounds:Rectangle = bounds.clone();
			//calculate major intervals by default size(75% of chart)
			bounds.width *= .75;
			bounds.height *= .75;
			
			var totalBounds:Rectangle = this.applyZoom(bounds);
			
			for each (axis in this.axes) {
				axis.calculateWidthDefaultSize(totalBounds, bounds);
				axis.initTitle();
			}
			
			bounds.width /= .75;
			bounds.height /= .75;

			//apply axes fixed offsets
			for each (axis in this.axes)
				axis.applyFixedOffset(bounds);
				
			var initBounds:Rectangle = bounds.clone();
			
			//apply dynamic offsets
			for each (axis in this.axes)
				axis.applyDynamicOffset(bounds);
			
			totalBounds = this.applyZoom(bounds);
			
			//recalculate axes	
			for each (axis in this.axes)
				axis.recalculateWithDynamicSize(totalBounds, bounds);
				
			bounds = initBounds;
			for each (axis in this.axes)
				axis.applyDynamicOffset(bounds);
				
			//labels
			var leftSpace:Number = bounds.x - baseBounds.x;
			var topSpace:Number = bounds.y - baseBounds.y;
			var rightSpace:Number = baseBounds.width - bounds.width - leftSpace;
			var bottomSpace:Number = baseBounds.height - bounds.height - topSpace;
			
			for each (axis in this.axes) {
				var tmpBounds:Rectangle = baseBounds.clone();
				axis.setLabelsSpace(tmpBounds, bounds, totalBounds);
				
				var newLeftSpace:Number = tmpBounds.x - baseBounds.x;
				var newTopSpace:Number = tmpBounds.y - baseBounds.y;
				var newRightSpace:Number = baseBounds.width - tmpBounds.width - newLeftSpace;
				var newBottomSpace:Number = baseBounds.height - tmpBounds.height - newTopSpace;
				
				if (newLeftSpace > leftSpace) leftSpace = newLeftSpace;
				if (newRightSpace > rightSpace) rightSpace = newRightSpace;
				if (newTopSpace > topSpace) topSpace = newTopSpace;
				if (newBottomSpace > bottomSpace) bottomSpace = newBottomSpace;
					
				tmpBounds = null;
			}
			
			bounds.x = baseBounds.x + leftSpace;
			bounds.y = baseBounds.y + topSpace;
			bounds.width = baseBounds.width - leftSpace - rightSpace;
			bounds.height = baseBounds.height - topSpace - bottomSpace;
			
			this.cropBounds(bounds);
			
			//set bounds and positions
			totalBounds = this.applyZoom(bounds);
				
			for each (axis in this.axes) {
				axis.setBounds(totalBounds, bounds);
				if (axis.zoomManager) axis.zoomManager.update();
			}
			
			var leftOffset:Number = 0;
			var rightOffset:Number = 0;
			var topOffset:Number = 0;
			var bottomOffset:Number = 0;
			
			totalBounds = this.applyZoom(bounds);
			
			for (var i:uint = 0;i<this.axes.length;i++) {
				axis = this.axes[i];
				switch (axis.position) {
					case Position.LEFT:
						axis.offset = leftOffset;
						leftOffset += axis.space;
						break;
					case Position.RIGHT:
						axis.offset = rightOffset;
						rightOffset += axis.space;
						break;
					case Position.TOP:
						axis.offset = topOffset;
						topOffset += axis.space;
						break;
					case Position.BOTTOM:
						axis.offset = bottomOffset;
						bottomOffset += axis.space;
						break;
				}
				if (!axis.isFirstInList) {
					axis.offset += axis.extraAdditionalOffset;
				}
				axis.setBounds(totalBounds, bounds);
			}
			
			this.bounds = bounds;
			this.totalBounds = totalBounds;
			return bounds;
		}
		
		override protected function setContainersPositions(newBounds:Rectangle):void {
			
			super.setContainersPositions(newBounds);
			
			this.setAxesPosition(this.scrollableContainer.x, this.scrollableContainer.y);
			
			var pos:Point = (!isNaN(currentX) || !isNaN(currentY)) ? this.getDataViewPixPosition(currentX, currentY) : new Point();
			
			this.cropXAxes(newBounds, pos);
			this.cropYAxes(newBounds, pos);
		}
		
		override protected function setScrollRect(newBounds:Rectangle):void {
			var pos:Point = (!isNaN(currentX) || !isNaN(currentY)) ? this.getDataViewPixPosition(currentX, currentY) : new Point();
			var scrollRect:Rectangle = newBounds.clone();
			scrollRect.x = pos.x - this.getXAxisOutsideContainerXOffset();
			scrollRect.y = pos.y;
			
			this.scrollableContainer.scrollRect = scrollRect;
		}
		
		override public function setTooltipPosition(pos:Point):void {
			if (this.scrollableContainer.scrollRect) {
				pos.x -= this.scrollableContainer.scrollRect.x;
				pos.y -= this.scrollableContainer.scrollRect.y;
			}
		}
		
		protected function getXAxisOutsideContainerXOffset():Number { return 0; }
		protected function getXAxisOutsideContainerYOffset():Number { return 0; }
		protected function getXAxisOutsideLabelsOffset():Number { return 0; }
		protected function getYAxisOutsideLabelsOffset():Number { return 0; }	
		
		private function cropXAxes(newBounds:Rectangle, pos:Point):void {
			
			var mainArg:Axis = this.argumentAxes["primary"];
			var mainVal:Axis = this.valueAxes["primary"];
			var axis:Axis = (mainArg.position == Position.TOP || mainArg.position == Position.BOTTOM) ? mainArg : mainVal;
			if (!axis.isZoomEnabled) return;
			
			var yOffset:Number = this.getXAxisOutsideLabelsOffset();
			
			var chartBounds:Rectangle = this.chart.getBounds();
			var scrollRect:Rectangle = newBounds.clone();		
			scrollRect.x = pos.x;
			scrollRect.y = -this.bounds.y;
			scrollRect.height = chartBounds.height;
			
			this.outsideXAxesContainer.y = -yOffset;
			this.outsideXAxesContainer.scrollRect = scrollRect;
		}
		
		private function cropYAxes(newBounds:Rectangle, pos:Point):void {
			var mainArg:Axis = this.argumentAxes["primary"];
			var mainVal:Axis = this.valueAxes["primary"];
			var axis:Axis = (mainArg.position == Position.LEFT || mainArg.position == Position.RIGHT) ? mainArg : mainVal;
			if (!axis.isZoomEnabled) return;
			
			var xOffset:Number = this.getYAxisOutsideLabelsOffset();
			
			var chartBounds:Rectangle = this.chart.getBounds();
			var scrollRect:Rectangle = newBounds.clone();		
			scrollRect.x = -this.bounds.x;
			scrollRect.y = pos.y;
			scrollRect.width = chartBounds.width;
			this.outsideYAxesContainer.x = xOffset;
			this.outsideYAxesContainer.scrollRect = scrollRect;
		}
		
		protected function setAxesPosition(x:Number, y:Number):void {
			this.outsideXAxesContainer.x = x;
			this.outsideXAxesContainer.y = y;
			this.outsideYAxesContainer.x = x;
			this.outsideYAxesContainer.y = y;
			this.axesFixedContainer.x = x;
			this.axesFixedContainer.y = y;
		}
		
		override public function draw():void {
			for each (var axis:Axis in this.axes) {
				axis.resizeScrollBar(this.bounds);
			}
			this.drawAxes();
			super.draw();
		}
		
		public function drawAxes():void {
			var i:uint;
			for (i = 0;i<this.valueAxesList.length;i++)
				this.valueAxesList[i].drawGridInterlaces();
			for (i = 0;i<this.argumentAxesList.length;i++)
				this.argumentAxesList[i].drawGridInterlaces();
			
			for (i = 0;i<this.valueAxesList.length;i++)
				this.valueAxesList[i].drawGridLines();
			for (i = 0;i<this.argumentAxesList.length;i++)
				this.argumentAxesList[i].drawGridLines();
				
			for (i = 0;i<this.valueAxesList.length;i++) {
				this.valueAxesList[i].drawTickmarks();
				this.valueAxesList[i].drawLabels();
			}
			for (i = 0;i<this.argumentAxesList.length;i++) {
				this.argumentAxesList[i].drawTickmarks();
				this.argumentAxesList[i].drawLabels();
			}
				
			for (i = 0;i<this.valueAxesList.length;i++) {
				this.valueAxesList[i].drawAxisLine();
				this.valueAxesList[i].drawZeroLine();
			}
			for (i = 0;i<this.argumentAxesList.length;i++) {
				this.argumentAxesList[i].drawAxisLine();
				this.argumentAxesList[i].drawZeroLine();
			}
			
			for (i = 0;i<this.axes.length;i++) {
				Axis(this.axes[i]).drawTitle();
				Axis(this.axes[i]).drawAxisMarkers();
				Axis(this.axes[i]).drawScrollBar();
			}
			
			
			this.drawHidden(this.topInteractiveScrollContainer);
			this.drawHidden(this.interactiveScrollContainer);
		}
		
		private function drawHidden(container:Sprite):void {
			container.x = this.bounds.x;
			container.y = this.bounds.y;
			container.graphics.clear();
			
			container.graphics.beginFill(0xFFFFFF,0.0);
			container.graphics.drawRect(0,0,this.bounds.width, this.bounds.height);
			container.graphics.endFill();
		}
		
		public function is3D():Boolean { return false; }
		
		override public function serialize(res:Object=null):Object {
			res = super.serialize(res);
			res.YRangeMax = this.cashedTokens['RangeMax'];
			res.YRangeMin = this.cashedTokens['RangeMin'];
			res.YRangeSum = this.cashedTokens['RangeSum'];
			res.XSum = this.cashedTokens['XSum'];
			res.XMax = this.cashedTokens['XMax'];
			res.XMin = this.cashedTokens['XMin'];
			res.XAverage = this.cashedTokens['XSum']/this.cashedTokens['PointCount'];
			
			res.Categories = [];
			
			var i:uint;
			for (i = 0;i<this._dataCategories.length;i++) {
				var category:CategoryInfo = this._dataCategories[i];
				if (category)
					res.Categories.push(category.serialize());
			}
			
			res.XAxes = {};
			res.YAxes = {};
			var axis:Axis;
			for (i = 0;i<this.argumentAxesList.length;i++) {
				axis = this.argumentAxesList[i];
				res.XAxes[axis.name] = axis.serialize();
			}
			for (i = 0;i<this.valueAxesList.length;i++) {
				axis = this.valueAxesList[i];
				res.YAxes[axis.name] = axis.serialize();
			}
			return res;
		}
		
		override public function getLabelsContainer(label:ItemLabelElement):Sprite {
			if (label.style == null || label.style.normal == null || LabelElementStyleState(label.style.normal).anchor != Anchor.X_AXIS)
				return super.getLabelsContainer(label);
			
			return this._isHorizontal ? this.outsideYAxesContainer : this.outsideXAxesContainer;
		}
		
		override protected function addPointAnimation(animation:Animation, dataPoint:BasePoint):void {
			if (this._isHorizontal || AxesPlotPoint(dataPoint).isInverted) {
				animation = animation.createCopy();
				animation.type = animation.getActualType(this._isHorizontal, AxesPlotPoint(dataPoint).isInverted);
			}
			super.addPointAnimation(animation, dataPoint);
		}
		
		override protected function addSeriesAnimation(animation:Animation, series:BaseSeries):void {
			if (this._isHorizontal) {
				animation = animation.createCopy();
				animation.type = animation.getActualType(true, false);
			}
			super.addSeriesAnimation(animation, series);
		}
		
		override public function getXScrollInfo():Object {
			return this.getScrollInfo(this.argumentAxes["primary"]);
		}
		
		override public function getYScrollInfo():Object {
			return this.getScrollInfo(this.valueAxes["primary"]);
		}
		
		private function getScrollInfo(axis:Axis):Object {
			return axis.zoomManager ? axis.zoomManager.getScrolllInfo() : null;
		}
		
		private var cursorShape:Sprite;
		
		public var canHideMouse:Boolean = true;
		
		private function checkDrag():void {
			var isXDragable:Boolean = Axis(this.argumentAxes["primary"]).zoomManager != null && Axis(this.argumentAxes["primary"]).zoomManager.isDragable();
			var isYDragable:Boolean = Axis(this.valueAxes["primary"]).zoomManager != null && Axis(this.valueAxes["primary"]).zoomManager.isDragable();
			
			if (isXDragable || isYDragable) {
				this.scrollableContainer.addEventListener(MouseEvent.MOUSE_OVER, this.plotMouseOverHandler);
				this.interactiveScrollContainer.addEventListener(MouseEvent.MOUSE_OVER, this.plotMouseOverHandler);
				this.topInteractiveScrollContainer.addEventListener(MouseEvent.MOUSE_OVER, this.plotMouseOverHandler);
				
				this.scrollableContainer.addEventListener(MouseEvent.MOUSE_OUT, this.plotMouseOutHandler);
				this.interactiveScrollContainer.addEventListener(MouseEvent.MOUSE_OUT, this.plotMouseOutHandler);
				this.topInteractiveScrollContainer.addEventListener(MouseEvent.MOUSE_OUT, this.plotMouseOutHandler);
				
				this.cursorShape = new Sprite();
				this.cursorShape.mouseChildren = false;
				this.cursorShape.mouseEnabled = false;
				this.drawNormalCursor();
			}
		}
		
		public function drawClickedCursor():void {
			var isXDragable:Boolean = Axis(this.argumentAxes["primary"]).zoomManager != null && Axis(this.argumentAxes["primary"]).zoomManager.isDragable();
			var isYDragable:Boolean = Axis(this.valueAxes["primary"]).zoomManager != null && Axis(this.valueAxes["primary"]).zoomManager.isDragable();
			
			if (isXDragable && isYDragable) {
				Cursor.drawMove2D(this.cursorShape, true);
			}else if (isXDragable) {
				this.drawAxisPositionBasedCursor(Axis(this.argumentAxes["primary"]), true);
			}else if (isYDragable) {
				this.drawAxisPositionBasedCursor(Axis(this.valueAxes["primary"]), true);
			}
		}
		
		public function drawNormalCursor():void {
			var isXDragable:Boolean = Axis(this.argumentAxes["primary"]).zoomManager != null && Axis(this.argumentAxes["primary"]).zoomManager.isDragable();
			var isYDragable:Boolean = Axis(this.valueAxes["primary"]).zoomManager != null && Axis(this.valueAxes["primary"]).zoomManager.isDragable();
			
			if (isXDragable && isYDragable) {
				Cursor.drawMove2D(this.cursorShape, false);
			}else if (isXDragable) {
				this.drawAxisPositionBasedCursor(Axis(this.argumentAxes["primary"]), false);
			}else if (isYDragable) {
				this.drawAxisPositionBasedCursor(Axis(this.valueAxes["primary"]), false);
			}
		}
		
		private function drawAxisPositionBasedCursor(axis:Axis, isClicked:Boolean):void {
			if (axis.position == Position.LEFT || axis.position == Position.RIGHT) {
				Cursor.drawMoveVertical(this.cursorShape, isClicked);
			}else {
				Cursor.drawMoveHorizontal(this.cursorShape, isClicked);
			}
		}
		
		private function plotMouseOverHandler(e:MouseEvent):void {
			if (!this.canHideMouse) return;
			Mouse.hide();
			this.root.getMainContainer().addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
			this.root.getMainContainer()["addChild"](this.cursorShape);
			this.cursorShape.x = e.stageX - Cursor.realWidth/2;
			this.cursorShape.y = e.stageY - Cursor.realHeight/2;
		}
		
		private function mouseMoveHandler(e:MouseEvent):void {
			this.cursorShape.x = e.stageX - Cursor.realWidth/2;
			this.cursorShape.y = e.stageY - Cursor.realHeight/2;
		}
		
		private function plotMouseOutHandler(e:MouseEvent):void {
			if (!this.canHideMouse) return;
			Mouse.show();
			this.root.getMainContainer().removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveHandler);
			if (this.cursorShape.parent)
				this.root.getMainContainer()["removeChild"](this.cursorShape);
		}
		
		public function disablePointEvents():void {
			this.scrollableContainer.mouseChildren = false;
			this.topInteractiveScrollContainer.visible = true;
		}
		
		public function enablePointEvents():void {
			this.scrollableContainer.mouseChildren = true;
			this.topInteractiveScrollContainer.visible = false;
		}
		
		override public function highlightCategory(categoryName:String, highlighted:Boolean):void {
			var category:ILegendTag;
			for each (var c:CategoryInfo in this._dataCategories) {
				if (c.name == categoryName) {
					category = c;
					break;
				}
					
			}
			if (category) {
				if (highlighted)
					category.setHover();
				else
					category.setNormal();
			}
		}
		
		
	}
}