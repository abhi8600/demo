package com.anychart.axesPlot.data {
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class SingleValueSeries extends AxesPlotSeries {
		
		public function SingleValueSeries() {
			super();
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = '';
			this.cashedTokens['MinYValuePointName'] = '';
		}
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var pt:SingleValuePoint = SingleValuePoint(point);
			this.cashedTokens['YSum'] += pt.y;
			if (pt.y > this.cashedTokens['YMax']) {
				this.cashedTokens['YMax'] = pt.y;
				this.cashedTokens['MaxYValuePointName'] = pt.name;
			}
			if (pt.y < this.cashedTokens['YMin']) {
				this.cashedTokens['YMin'] = pt.y;
				this.cashedTokens['MinYValuePointName'] = pt.name;
			}
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			if (plotTokensCash['YSum'] == null)
				plotTokensCash['YSum'] = 0;
			if (plotTokensCash['YBasedPointsCount'] == null)
				plotTokensCash['YBasedPointsCount'] = 0;
				
			plotTokensCash['YSum'] += this.cashedTokens['YSum'];
			plotTokensCash['YBasedPointsCount'] += this.points.length;
			
			if (plotTokensCash['YMax'] == null || this.cashedTokens['YMax'] > plotTokensCash['YMax']) {
				plotTokensCash['YMax'] = this.cashedTokens['YMax'];
				plotTokensCash['MaxYValuePointName'] = this.cashedTokens['MaxYValuePointName'];
				plotTokensCash['MaxYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['YMin'] == null || this.cashedTokens['YMin'] < plotTokensCash['YMin']) {
				plotTokensCash['YMin'] = this.cashedTokens['YMin'];
				plotTokensCash['MinYValuePointName'] = this.cashedTokens['MinYValuePointName'];
				plotTokensCash['MinYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['MaxYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MaxYSumSeries']) {
				plotTokensCash['MaxYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MaxYSumSeriesName'] = this.name;
			}
			if (plotTokensCash['MinYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MinYSumSeries']) {
				plotTokensCash['MinYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MinYSumSeriesName'] = this.name;
			}
			
			super.checkPlot(plotTokensCash);
		}
		
		override protected function checkAxesTokens():void {
			super.checkAxesTokens();
			var cashedTokens:Object = this.valueAxis.cashedTokens;
			cashedTokens['Sum'] += this.cashedTokens['YSum'];
			if (this.cashedTokens['YMax'] > cashedTokens['Max'])
				cashedTokens['Max'] = this.cashedTokens['YMax'];
			if (this.cashedTokens['YMin'] < cashedTokens['Min'])
				cashedTokens['Min'] = this.cashedTokens['YMin'];
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesFirstYValue') return (this.points.length > 0 && this.points[0]!=null)  ? this.points[0].y : '';
			if (token == '%SeriesLastYValue') return (this.points.length > 0 && this.points[this.points.length-1]!=null)  ? this.points[this.points.length-1].y : '';
			var pt:SingleValuePoint;
			if (token == '%SeriesYSum') return this.cashedTokens['YSum'];
			if (token == '%SeriesYMax') return this.cashedTokens['YMax'];
			if (token == '%SeriesYMin') return this.cashedTokens['YMin'];
			if (token == '%SeriesYAverage') {
				if (this.cashedTokens['YAverage'] == null)
					this.cashedTokens['YAverage'] = this.cashedTokens['YSum']/this.points.length;
				return this.cashedTokens['YAverage'];
			}
			if (token == '%Value') return this.cashedTokens['YSum'];
			if (token == '%YValue') return this.cashedTokens['YSum'];
			return super.getSeriesTokenValue(token);
		}
		
		override public function isDateTimeSeriesToken(token:String):Boolean {
			if (this.valueAxis.isDateTimeAxis()) {
				if (token == "%SeriesFirstYValue") return true;
				if (token == "%SeriesLastYValue") return true;
				if (token == "%SeriesYSum") return true;
				if (token == "%SeriesYMax") return true;
				if (token == "%SeriesYMin") return true;
				if (token == "%SeriesYAverage") return true;
				if (token == "%Value") return true;
				if (token == "%YValue") return true;
			}
			return super.isDateTimeSeriesToken(token);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.FirstYValue = this.getSeriesTokenValue("%SeriesFirstYValue");
			res.LastYValue = this.getSeriesTokenValue("%SeriesLastYValue");
			res.YSum = this.cashedTokens['YSum'];
			res.YMax = this.cashedTokens['YMax'];
			res.YMin = this.cashedTokens['YMin'];
			res.YAverage = this.getSeriesTokenValue("%SeriesYAverage");
			res.YMedian = this.getSeriesTokenValue("%SeriesYMedian");
			res.YMode = this.getSeriesTokenValue("%SeriesYMode");;
			return res;
		}
	}
}