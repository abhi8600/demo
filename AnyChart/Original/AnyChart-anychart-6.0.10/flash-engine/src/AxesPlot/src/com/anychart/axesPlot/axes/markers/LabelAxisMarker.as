package com.anychart.axesPlot.axes.markers {
	import com.anychart.axesPlot.axes.Axis;
	
	internal final class LabelAxisMarker extends AxisMarker {
		public function LabelAxisMarker(axis:Axis) {
			super(axis);
		}
	}
}