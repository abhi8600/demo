package com.anychart.axesPlot {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.viewPorts.ViewPortFactory3D;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.templates.AxesPlotTemplatesManager;
	import com.anychart.data.SeriesType;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.visual.layout.Position;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	public class AxesPlot3D extends AxesPlot {
		
		//------------------------------------------------------------------------------------------
		//							TEMPLATES
		//------------------------------------------------------------------------------------------
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new AxesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate().copy(), data);
			data = templatesManager.mergeTemplates(AxesPlot.getDefaultTemplate().copy(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		//------------------------------------------------------------------------------------------
		//							Constructor
		//------------------------------------------------------------------------------------------
		
		public function AxesPlot3D() {
			this.space3D = new Space3D();
			super();
		}
		
		//------------------------------------------------------------------------------------------
		//							Overrides
		//------------------------------------------------------------------------------------------
		
		override public function is3D():Boolean { return true; }
		
		override protected function getViewPortFactoryClass():Class {
			return ViewPortFactory3D;
		}
		
		public function getPlotType():String {
			return this.plotType;
		}
		
		//------------------------------------------------------------------------------------------
		//							3D series
		//------------------------------------------------------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			var sClass:Class;
			if (seriesType == SeriesType.BAR) {
				sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::BarGlobalSeriesSettings3D"));
				return new sClass;
			}else if (seriesType == SeriesType.RANGE_BAR){
				sClass = Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::RangeBarGlobalSeriesSettings3D"));
				return new sClass;
			} 
			return super.createGlobalSeriesSettings(seriesType);
		}
		
		//------------------------------------------------------------------------------------------
		//							3D settings
		//------------------------------------------------------------------------------------------
		
		public var zAxis:Axis;
		public var space3D:Space3D;
		
		private var xAxesPlane:AxisPlane;
		private var yAxesPlane:AxisPlane;
		
		//------------------------------------------------------------------------------------------
		//							Z Axis
		//------------------------------------------------------------------------------------------
		
		override protected function deserializeChartSettings(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserializeChartSettings(data, styles, resources);
			
			if (SerializerBase.isEnabled(data.data_plot_background[0])) {
				if (SerializerBase.isEnabled(data.data_plot_background[0].x_axis_plane[0])) {
					this.xAxesPlane = new AxisPlane();
					this.xAxesPlane.deserialize(data.data_plot_background[0].x_axis_plane[0], resources);
				}
				if (SerializerBase.isEnabled(data.data_plot_background[0].y_axis_plane[0])) {
					this.yAxesPlane = new AxisPlane();
					this.yAxesPlane.deserialize(data.data_plot_background[0].y_axis_plane[0], resources);
				}
			}
			
			this.space3D.deserialize(data.parent().data_plot_settings[0]);
			this.space3D.initialize(this.argumentAxes["primary"], this.valueAxes["primary"]);
			
			this.createZAxis(data.parent().chart_settings[0].axes[0].z_axis[0], styles, resources);
		}
		
		private function createZAxis(axisNode:XML, styles:XML, resources:ResourcesLoader):void {
			this.zAxis = new ValueAxis();
			this.zAxis.plot = this;
			this.zAxis.isArgument = false;
			this.zAxis.isFirstInList = true;

			this.zAxis.deserializeScale(axisNode);			
			this.zAxis.deserialize(axisNode,styles,resources);
			this.zAxis.position = 0xFF;
			this.zAxis.setContainers(null, null);
			this.zAxis.createViewPort(this.getViewPortFactoryClass());
			this.zAxis.name = "primary";
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			if (data != null)
				this.init3d();
		}
		
		private function init3d():void {
			if (this.zAxis.scale.isMinimumAuto) {
				this.zAxis.scale.isMinimumAuto = false;
				this.zAxis.scale.minimum = this.zAxis.scale.dataRangeMinimum;
			}
			if (this.zAxis.scale.isMaximumAuto) {
				this.zAxis.scale.isMaximumAuto = false;
				this.zAxis.scale.maximum = (this.zAxis.scale.dataRangeMaximum == this.zAxis.scale.dataRangeMinimum) ? (this.zAxis.scale.dataRangeMaximum+1) : this.zAxis.scale.dataRangeMaximum;
			}
			this.zAxis.scale.inverted = !this.zAxis.scale.inverted;
			this.space3D.zAxisRange = Math.abs(this.zAxis.scale.maximum - this.zAxis.scale.minimum);
			
			var categoryPercentWidth:Number = 1/this.xCategories.length;
			var w:Number = Number.MAX_VALUE;
			for each (var cat:Category in this.xCategories) {
				if (cat.clusterSettings) {
					cat.clusterSettings.calculate(categoryPercentWidth);
					w = Math.min(w,cat.clusterSettings.clusterWidth);
				}
			}
			if (w == Number.MAX_VALUE) {
				for each (var series:AxesPlotSeries in this.series) {
					if (series.clusterSettings) {
						series.clusterSettings.calculate(categoryPercentWidth);
						w = Math.min(w,series.clusterSettings.clusterWidth);
					}
				}
			}
			if (w != Number.MAX_VALUE) this.space3D.minClusterWidthInPercents = w;
			this.space3D.isHorizontal = this.isHorizontal;
			
			try {
				//injection
				//руки-бы-мне-оторвать-нахуй
				getDefinitionByName("com.anychart.axesPlot.series.barSeries::Bar3DPointsDistributor").distribute(this);
			}catch (e:Error) {
				if (e is FeatureNotSupportedError)
					throw e; 
			}
		}
		
		//------------------------------------------------------------------------------------------
		//							Plot bounds calculation
		//------------------------------------------------------------------------------------------
		
		override protected function setAxesSize(bounds:Rectangle):Rectangle {
			bounds = super.setAxesSize(bounds);
			this.space3D.initializeAxesPositions(bounds);
			super.setAxesPosition(this.space3D.axesX, this.space3D.axesY);
			this.space3D.movePlot(bounds);
			
			this.zAxis.setBounds(bounds,bounds);
			
			return bounds;
		}
		
		override protected function preCropBounds(bounds:Rectangle):void {
			this.space3D.preCropBounds(bounds);
		}
		
		override protected function cropBounds(bounds:Rectangle):void {
			this.space3D.cropBounds(bounds);
		}
		
		private function isXAxisScrollEnabled():Boolean {
			var mainArg:Axis = this.argumentAxes["primary"];
			var mainVal:Axis = this.valueAxes["primary"];
			var axis:Axis = (mainArg.position == Position.TOP || mainArg.position == Position.BOTTOM) ? mainArg : mainVal;
			return axis.isZoomEnabled;
		}
		
		private function isYAxisScrollEnabled():Boolean {
			var mainArg:Axis = this.argumentAxes["primary"];
			var mainVal:Axis = this.valueAxes["primary"];
			var axis:Axis = (mainArg.position == Position.LEFT || mainArg.position == Position.RIGHT) ? mainArg : mainVal;
			return axis.isZoomEnabled;
		}
		
		override protected function getXAxisOutsideContainerXOffset():Number {
			return this.isXAxisScrollEnabled() || this.isYAxisScrollEnabled() ? this.space3D.getXAxisOffset() : 0;
		}
		
		override protected function getXAxisOutsideContainerYOffset():Number {
			return this.isXAxisScrollEnabled() || this.isYAxisScrollEnabled() ? this.space3D.getYAxisOffset() : 0;
		}
		
		override protected function getXAxisOutsideLabelsOffset():Number {
			return this.isXAxisScrollEnabled() ? this.space3D.getPixYAspect() : 0;
		}
		
		override protected function getYAxisOutsideLabelsOffset():Number {
			return this.isYAxisScrollEnabled() ? -this.space3D.getPixXAspect() : 0;
		}
		
		//------------------------------------------------------------------------------------------
		//							Plot background
		//------------------------------------------------------------------------------------------
		
		override public function draw():void {
			super.draw();
			this.drawPlanes();
		}
		
		override public function execResize():void {
			super.execResize();
			this.drawPlanes();
		}
		
		private function drawPlanes():void {
			if (this.xAxesPlane) this.xAxesPlane.draw(this.getDrawingTarget().graphics, this.space3D.getXAxesPlanePoints(this));
			if (this.yAxesPlane) this.yAxesPlane.draw(this.getDrawingTarget().graphics, this.space3D.getYAxesPlanePoints(this));
		}
		
		//------------------------------------------------------------------------------------------
		//							Plot position
		//------------------------------------------------------------------------------------------
		
		override protected function setAxesPosition(x:Number, y:Number):void {
			//do nothing
		}
		
		public function offsetXToPlotFrontProjection(x:Number):Number {
			return this.space3D.getFrontX(x);
		}
		
		public function offsetYToPlotFrontProjection(y:Number):Number {
			return this.space3D.getFrontY(y);
		}
		
		override protected function setContainersPositions(newBounds:Rectangle):void {
			
			super.setContainersPositions(newBounds);

			newBounds = newBounds.clone();
			newBounds.x = 0;
			newBounds.y = 0;
			
			var frontRect:Rectangle = newBounds.clone();
			var backRect:Rectangle = newBounds.clone();
			frontRect.left = offsetXToPlotFrontProjection(frontRect.left);
			frontRect.right = offsetXToPlotFrontProjection(frontRect.right);
			frontRect.top = offsetYToPlotFrontProjection(frontRect.top);
			frontRect.bottom = offsetYToPlotFrontProjection(frontRect.bottom);
			
			var scrollRect:Rectangle = new Rectangle();
			scrollRect.x = Math.min(frontRect.x, backRect.x);
			scrollRect.y = Math.min(frontRect.y, backRect.y);
			scrollRect.width = Math.max(frontRect.right, backRect.right) - scrollRect.x;
			scrollRect.height = Math.max(frontRect.bottom, backRect.bottom) - scrollRect.y;
			
			this.scrollableContainer.scrollRect = scrollRect;
			this.scrollableContainer.x += scrollRect.x;
			this.scrollableContainer.y += scrollRect.y;
			
			this.setScrollMask(backRect, frontRect);
		}
		
		//------------------------------------------------------------------------------------------
		//							Scroll mask
		//------------------------------------------------------------------------------------------
		
		private var maskSprite:Sprite;
		
		private function setScrollMask(backRect:Rectangle, frontRect:Rectangle):void {
			//mask hack
			if (this.isXAxisScrollEnabled() || this.isYAxisScrollEnabled()) {
				if (maskSprite == null) { 
					maskSprite = new Sprite();
				}
				
				var g:Graphics = maskSprite.graphics;
				g.clear();
				
				var pts:Array = this.space3D.getMaskPoints(frontRect, backRect);
				
				g.beginFill(0, 1);
				g.moveTo(pts[0].x, pts[0].y);
				for (var i:int = 1; i < pts.length; i++) {
					g.lineTo(pts[i].x, pts[i].y);
				}
				g.lineTo(pts[0].x, pts[0].y);
				g.endFill();
				
				maskSprite.x = this.scrollableContainer.x + this.getXAxisOutsideContainerXOffset();
				maskSprite.y = this.scrollableContainer.y + this.getXAxisOutsideContainerYOffset();
				
				this.scrollableContainer.mask = maskSprite;
				//this.scrollableContainer.addChild(maskSprite);
			}
			//end of mask hack
		}
		
		
		//------------------------------------------------------------------------------------------
		//							Axes
		//------------------------------------------------------------------------------------------
		
		public function getPrimaryArgumentAxis():Axis { return this.argumentAxes["primary"]; }
		public function getPrimaryValueAxis():Axis { return this.valueAxes["primary"]; }
	}
}