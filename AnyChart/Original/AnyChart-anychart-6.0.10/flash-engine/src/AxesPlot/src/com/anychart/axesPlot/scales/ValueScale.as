package com.anychart.axesPlot.scales {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	
	public class ValueScale extends BaseScale {
		
		public var axis:Axis;
		
		private var _isCrossingEnabled:Boolean;
		private var _crossValueAsString:String;
		public var crossValue:Number;
		
		protected var _alwaysShowZero:Boolean;
		 
		public function get isCrossingEnabled():Boolean { return this._isCrossingEnabled; }
		public function get crossValueAsString():String { return this._crossValueAsString; }
		
		public function ValueScale(axis:Axis) {
			super();
			this.axis = axis;
			this._isCrossingEnabled = false;
			this._alwaysShowZero = false;
		}
		
		public function getCategoryName(category:Category):* {
			return category.name;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@crossing != undefined) {
				this._isCrossingEnabled = true;
				this._crossValueAsString = SerializerBase.getString(data.@crossing);
			}
			if (data.@always_show_zero != undefined){
				this._alwaysShowZero = SerializerBase.getBoolean(data.@always_show_zero);
			}
		}
		
		override protected function calculateOptimalStepSize(range:Number, steps:Number, isBounded:Boolean):Number {
			if (!isBounded && this.axis.zoomManager) {
				steps /= this.axis.zoomManager.valueMult;
			}
			return super.calculateOptimalStepSize(range, steps, isBounded);
		}
	}
}