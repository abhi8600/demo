package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.data.StackablePoint;
	
	public final class StackSettings {
		
		private var positiveStackSumm:Number;
		private var negativeStackSumm:Number;
		
		private var isPercent:Boolean;
		private var points:Array;
		
		public function StackSettings(isPercent:Boolean) {
			this.positiveStackSumm = 0;
			this.negativeStackSumm = 0;
			this.isPercent = isPercent;
			this.positivePoints = [];
			this.negativePoints = [];
			this.points = [];
		}
		
		public function recalculate():void {
			this.negativePoints = [];
			this.positivePoints = [];
			this.negativeStackSumm = this.positiveStackSumm = 0;
			
			for (var i:int = 0;i<this.points.length;i++) {
				var pt:StackablePoint = this.points[i];
				var value:Number = pt.y;
				if (value >= 0) {
					var prevPositiveValue:Number = this.positiveStackSumm;
					this.positiveStackSumm += value;
					pt.updateStackValue(prevPositiveValue, this.positiveStackSumm);
					this.positivePoints.push(pt);
				}else {
					var prevNegativeValue:Number = this.negativeStackSumm;
					this.negativeStackSumm += value;
					pt.updateStackValue(prevNegativeValue, this.negativeStackSumm);
					this.negativePoints.push(pt);
				}
			}
			
			if (this.isPercent) {
				this.setPercent();
			}
		}
		
		public function getPrevious(value:Number):Number {
			return (value >= 0) ? this.positiveStackSumm : this.negativeStackSumm;
		}
		
		public function addValue(value:Number):Number {
			if (value >= 0) {
				this.positiveStackSumm += value;
				return this.positiveStackSumm;
			}
			this.negativeStackSumm += value;
			return this.negativeStackSumm;
		}
		
		private var positivePoints:Array;
		private var negativePoints:Array;
		
		public function getPositivePoints():Array {
			return this.positivePoints;
		}
		
		public function getNegativePoints():Array {
			return this.negativePoints;
		}
		
		public function addPoint(pt:StackablePoint, value:Number):Number {
			var isPrevPositive:Boolean = true;
			if (value == 0 && this.points.length > 0) {
				var prevIndex:int = this.points.length - 1;
				var prevPt:StackablePoint = this.points[prevIndex];
				while (prevPt.stackValue == 0 && prevIndex > 0) {
					prevIndex--;
					prevPt = this.points[prevIndex];
				}
				isPrevPositive = (prevPt.stackValue >= 0);
			}
			this.points.push(pt);
			var isPositive:Boolean = value > 0;
			if (value == 0)
				isPositive = isPrevPositive;
			
			if (isPositive) {
				if (this.positivePoints.length > 0)
					pt.previousStackPoint = this.positivePoints[this.positivePoints.length-1];
				this.positivePoints.push(pt);
				this.positiveStackSumm += value;
				return this.positiveStackSumm;
			}
			if (this.negativePoints.length > 0)
				pt.previousStackPoint = this.negativePoints[this.negativePoints.length-1];

			this.negativePoints.push(pt);
			this.negativeStackSumm += value;
			return this.negativeStackSumm;
		}
		
		public function setPercent():void {
			var pt:StackablePoint;
			for each (pt in this.positivePoints)
				pt.setStackMax(this.positiveStackSumm);
			for each (pt in this.negativePoints)
				pt.setStackMax(Math.abs(this.negativeStackSumm));
		}
		
		public function isLastInStack (point:StackablePoint):Boolean {
			if (this.negativePoints.length > 0 && this.negativePoints[this.negativePoints.length-1]==point) return true;
			if (this.positivePoints.length > 0 && this.positivePoints[this.positivePoints.length-1]==point) return true;
			return false
		}
		
		public function getPercentofStack(value:Number):Number{
			return  (value > 0) ? (value/this.positiveStackSumm) : (value/this.negativeStackSumm);
		}
		
		public function getStackSumm(value:Number):Number {
			return this.isPercent ? 100 : ((value > 0) ? this.positiveStackSumm : this.negativeStackSumm);
		}

	}
}