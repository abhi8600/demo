package com.anychart.events
{
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.IFormatableSerializableObject;

	public final class AnyChartPointInfo {
		
		private var point:IFormatable;
		private var pointData:Object;
		
		public function AnyChartPointInfo(point:IFormatableSerializableObject) {
			this.point = point;
			this.pointData = point.serialize();
		}
		
		public function get pointName():String { return point.getTokenValue("%Name"); }
		public function get seriesName():String { return point.getTokenValue("%SeriesName"); }
		public function get x():Number { return Number(point.getTokenValue("%XValue")); }
		public function get y():Number { return Number(point.getTokenValue("%YValue")); }
		public function get high():Number { return Number(point.getTokenValue("%High")); }
		public function get low():Number { return Number(point.getTokenValue("%Low")); }
		public function get open():Number { return Number(point.getTokenValue("%Open")); }
		public function get close():Number { return Number(point.getTokenValue("%Close")); }
		
		public function get start():Number { return Number(point.getTokenValue("%Start")); }
		public function get end():Number { return Number(point.getTokenValue("%End")); }
		public function get size():Number { return Number(point.getTokenValue("%BubbleSize")); }
		
		public function get pointCustomAttributes():Object { return this.pointData.Attributes; }
		public function get seriesCustomAttributes():Object {
			try {
				if (this.point["series"])
					return this.point["series"].serialize().Attributes;
			}catch (e:Error) {}
			return null;
		}
		
		public function getTokenValue(name:String):String { 
			return point.getTokenValue("%"+name);
		}
	}
}