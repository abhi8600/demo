/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.color.Color}</li>
 *     <li>@class {anychart.visual.color.ColorContainer}</li>
 *     <li>@class {anychart.visual.color.colorParser}.</li>
 * </ul>
 */

// Linking namespace
goog.provide('anychart.visual.color');

goog.require('anychart.visual.color.colorOperations');

/**
 * Represents static color. This class is returned as a result of all
 * color operations. Allows to set separate channel values and then represent
 * all channels in canvas-friendly style (as rgba() string).
 * Usage sample:
 * <code>
 *  var color = new anychart.visual.color.Color(0xFF, 0xAA, 0xBB, 1, false);
 *  canvasContext.fillStyle = color.toRGBA(); // context fill color will now be
 *  0xFFAABB with 1 opacity;
 * </code>

 * <code>
 *  var colorContainer = anychart.utils.deserialization.getColor("blend(%Color,
 *  #000000, 0.5)", 0.5);
 *  var color = colorContainer.getColor(new anychart.visual.color.Color(0xff,
 *  0xff, 0xff));
 *  canvasContext.fillStyle = color.toRGBA(); // context fill color will now be
 *  0x7F7F7F with 0.5 opacity;
 * </code>
 *
 * @constructor
 * @param {int=} opt_red [0..255].
 * @param {int=} opt_green [0..255].
 * @param {int=} opt_blue [0..255].
 * @param {(int|Number)=} opt_alpha [0..255]|[0..1] interpretation manner is set
 * by the next param.
 * @param {boolean=} opt_alpha_as_byte (default true).
 */
anychart.visual.color.Color = function(opt_red, opt_green, opt_blue, opt_alpha,
                                       opt_alpha_as_byte) {
    if (opt_red != undefined) this.red_ = opt_red;
    if (opt_green != undefined) this.green_ = opt_green;
    if (opt_blue != undefined) this.blue_ = opt_blue;
    if (opt_alpha != undefined) this.alpha_ = opt_alpha;
    if (opt_alpha != undefined && (opt_alpha_as_byte == undefined ||
            opt_alpha_as_byte)) this.alpha_ /= 255;
};

/**
 * Creates color instance from HSB system color. Used in color operations.
 *
 * TODO: сейчас это плохо работает :)
 *
 * @param {int} hue [0..360].
 * @param {int} saturation [0..100].
 * @param {int} brightness [0..100].
 * @return {anychart.visual.color.Color} Color.
 */
anychart.visual.color.Color.fromHSB = function(hue, saturation, brightness) {
    var hueProp = Number(hue / 360.0);
    var satProp = Number(saturation / 100.0);
    var briProp = Number(brightness / 100.0);

    if (satProp == 0) return new anychart.visual.color.Color(
            Math.floor(briProp * 255),
            Math.floor(briProp * 255),
            Math.floor(briProp * 255));

    var tmp = hueProp * 6;
    var tmpFloor = Math.floor(tmp);
    var tmpDiff = tmp - tmpFloor;
    var color2Prop = briProp * (1 - satProp);
    var color3Prop = (tmpFloor & 1) ?
            (briProp * (1 - satProp * tmpDiff)) :
            (briProp * (1 - satProp * (1 - tmpDiff)));

    switch (tmpFloor) {
        case 0:
            return new anychart.visual.color.Color(
                    Math.floor(briProp * 255),
                    Math.floor(color3Prop * 255),
                    Math.floor(color2Prop * 255));
        case 1:
            return new anychart.visual.color.Color(
                    Math.floor(color3Prop * 255),
                    Math.floor(briProp * 255),
                    Math.floor(color2Prop * 255));
        case 2:
            return new anychart.visual.color.Color(
                    Math.floor(color2Prop * 255),
                    Math.floor(briProp * 255),
                    Math.floor(color3Prop * 255));
        case 3:
            return new anychart.visual.color.Color(
                    Math.floor(color2Prop * 255),
                    Math.floor(color3Prop * 255),
                    Math.floor(briProp * 255));
        case 4:
            return new anychart.visual.color.Color(
                    Math.floor(color3Prop * 255),
                    Math.floor(color2Prop * 255),
                    Math.floor(briProp * 255));
        default:
            return new anychart.visual.color.Color(
                    Math.floor(briProp * 255),
                    Math.floor(color2Prop * 255),
                    Math.floor(color3Prop * 255));
    }
};

/**
 * Creates Color from HLS system color. Used in internal color operations
 *
 * TODO: сейчас это плохо работает :)
 *
 * @param {Number} h [0..1].
 * @param {Number} l [0..1].
 * @param {Number} s [0..1].
 * @return {anychart.visual.color.Color} Color.
 */
anychart.visual.color.Color.fromHLS = function(h, l, s) {
    var r = 0;
    var g = 0;
    var b = 0;
    if (l == 0) {
        r = g = b = 0;
    }
    else if (s == 0) {
        r = g = b = l;
    }
    else {
        var diff = (l <= 0.5) ? (l * (1 + s)) : ((l + s) - (l * s));
        var summ = (2 * l) - diff;

        var huesArr = [h + 0.33333333333333331, h, h - 0.33333333333333331];
        var rgb = [];

        for (var i = 0; i < 3; i++) {
            if (huesArr[i] < 0) huesArr[i] += 1;
            else if (huesArr[i] > 1) huesArr[i] -= 1;

            if (huesArr[i] * 6 < 1) {
                rgb[i] = summ + (((diff - summ) * huesArr[i]) * 6);
            } else if (huesArr[i] * 2 < 1) {
                rgb[i] = diff;
            } else if (huesArr[i] * 3 < 2) {
                rgb[i] = summ + (((diff - summ) *
                        (0.66666666666666663 - huesArr[i])) * 6);
            } else {
                rgb[i] = summ;
            }
        }

        r = Math.max(0, Math.min(1, rgb[0]));
        g = Math.max(0, Math.min(1, rgb[1]));
        b = Math.max(0, Math.min(1, rgb[2]));
    }

    return new anychart.visual.color.Color(
            Math.floor(255 * r),
            Math.floor(255 * g),
            Math.floor(255 * b));
};

/**
 * Red channel value.
 * @private
 * @type {int} [0..255]
 */
anychart.visual.color.Color.prototype.red_ = 0;

/**
 * Return red channel value.
 * @return {int} [0..225].
 */
anychart.visual.color.Color.prototype.getRed = function() {
    return this.red_;
};

/**
 * Green channel value.
 * @private
 * @type {int} [0..255]
 */
anychart.visual.color.Color.prototype.green_ = 0;

/**
 * Return green channel value.
 * @return {int} [0..225].
 */
anychart.visual.color.Color.prototype.getGreen = function() {
    return this.green_;
};

/**
 * Blue channel
 * @private
 * @type {int} [0..255]
 */
anychart.visual.color.Color.prototype.blue_ = 0;

/**
 * Return blue channel value.
 * @return {int} [0..225].
 */
anychart.visual.color.Color.prototype.getBlue = function() {
    return this.blue_;
};

/**
 * Alpha channel value.
 * @private
 * @type {Number} [0..1]
 */
anychart.visual.color.Color.prototype.alpha_ = 1.0;

/**
 * Return alpha channel value.
 * @return {int} [0..1].
 */
anychart.visual.color.Color.prototype.getAlpha = function() {
    return this.alpha_;
};

/**
 * Returns alpha channel value normalized to [0..255]
 *
 * @return {int} [0..255].
 */
anychart.visual.color.Color.prototype.getAlphaAsByte = function() {
    return Math.round(this.alpha * 255);
};

/**
 * Returns RGBA string representing the color. The main method to get color
 * string.
 * Return string format: "rgba(red, green, blue, alpha)", where red, green,
 * blue and alpha are decimal values of corresponding channels.
 *
 * @return {String} rgba string.
 */
anychart.visual.color.Color.prototype.toRGBA = function() {
    return 'rgba(' + this.red_.toString() + ', ' +
            this.green_.toString() + ', ' +
            this.blue_.toString() + ', ' +
            this.getAlphaAsByte().toString() + ')';
};

/**
 * Returns RGB string representing the color. The main method to get color
 * string.
 * Return string format: "rgb(red, green, blue)", where red, green, blue are
 * decimal values of corresponding channels.
 *
 * @return {String} rgb string.
 */
anychart.visual.color.Color.prototype.toRGB = function() {
    return 'rgb(' + this.getRed().toString() + ', ' +
            this.getGreen().toString() + ', ' +
            this.getBlue().toString() + ')';
};


/**
 * Returns hex string with RGBA color, that can be used to make hashing.
 * String format: "0xRRGGBBAA", where RR, GG, BB, AA are hex values of
 * corresponding channels.
 *
 * @return {String} hex string with RGBA color.
 */
anychart.visual.color.Color.prototype.toRGBAHash = function() {
    return '0x' + ((this.red_ << 24) |
            (this.green_ << 16) |
            (this.blue_ << 8) |
            this.alpha_).toString(16);
};

/**
 * @override
 * @return {String} RGB string.
 */
anychart.visual.color.Color.prototype.toString = function() {
    return 'rgb(' + this.red_.toString() + ',' +
            this.green_.toString() + ',' +
            this.blue_.toString() + ')';
};

/**
 * Returns HLS color representation. Used in color operations.
 *
 * TODO: сейчас это плохо работает :)
 *
 * @return {anychart.visual.color.HLSColor} HLS color representation.
 */
anychart.visual.color.Color.prototype.toHLS = function() {
    var rProp = this.red_ / 255;
    var gProp = this.green_ / 255;
    var bProp = this.blue_ / 255;

    var minval = Math.min(rProp, Math.min(gProp, bProp));
    var maxval = Math.max(rProp, Math.max(gProp, bProp));

    if (minval == maxval)
        return new anychart.visual.color.HLSColor(0, (maxval + minval) / 2, 0);

    var mdiff = maxval - minval;
    var msum = maxval + minval;

    var hue = 0;

    if (rProp == maxval) hue = 6 + (gProp - bProp) / mdiff;
    else if (gProp == maxval) hue = 2 + (bProp - rProp) / mdiff;
    else hue = 4 + (rProp - gProp) / mdiff;

    hue *= 0.1666;
    if (hue >= 1.0) hue -= 1.0;

    if (rProp == maxval) hue = 6 + (gProp - bProp) / mdiff;
    else if (gProp == maxval) hue = 2 + (bProp - rProp) / mdiff;
    else hue = 4 + (rProp - gProp) / mdiff;

    hue *= 0.1666;
    if (hue >= 1.0) hue -= 1.0;

    return new anychart.visual.color.HLSColor(hue, msum / 2, (msum <= 1) ?
            (mdiff / msum) : (mdiff / (2 - msum)));
};

/**
 * Represents a color in HLS system. Used as temporary storage of a color -
 * transform it with anychart.visual.color.Color.fromHLS to use.
 * @constructor
 * @param {Number} hue [0..1].
 * @param {Number} luminance [0..1].
 * @param {Number} saturation [0..1].
 */
anychart.visual.color.HLSColor = function(hue, luminance, saturation) {
    this.h = hue;
    this.l = luminance;
    this.s = saturation;
};

/**
 * Represents hue.
 *
 * @type {Number} [0..1]
 */
anychart.visual.color.HLSColor.prototype.h = 0;

/**
 * Represents luminance.
 *
 * @type {Number} [0..1]
 */
anychart.visual.color.HLSColor.prototype.l = 0;

/**
 * Represents saturation.
 *
 * @type {Number} [0..1]
 */
anychart.visual.color.HLSColor.prototype.s = 0;

//------------------------------------------------------------------------------
//
//                          ColorContainer class
//
//------------------------------------------------------------------------------

/**
 * Aggregates any dynamic (color operation object or "%Color" string) or static
 * color (anychart.visual.color.Color instance) and provides common interface
 * for both. Also injects opacity to the color.
 * CAUTION: opacity passed to colorContainer will replace any opacity set in
 * source.
 * Instances of this class are returned by color parser.
 * To create default colorContainer that returns black color use
 * anychart.utils.deserialization.getColor() call with no arguments.
 * Usage sample:
 * <code>
 *  var colorContainer = anychart.utils.deserialization.getColor("blend(%Color,
 *  #000000, 0.5)", 0.5);
 *  var color;
 *  // test colorContainer source - if it is dynamic, it needs opt_color to be
 *  passed to replace %Color token
 *  if (colorContainer.isDynamic)
 *      color = colorContainer.getColor(new anychart.visual.color.Color(0xff,
 *          0xff, 0xff));
 *  else
 *      color = colorContainer.getColor();
 * </code>
 *
 * @constructor
 * @param {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation|
 * string} source Source.
 * @param {Number} opacity Opacity.
 */
anychart.visual.color.ColorContainer = function(source, opacity) {
    this.color_ = source;
    if (source.execute != undefined) {
        this.opacity_ = opacity;
        this.isDynamic = true;
    } else if (source == '%color') {
        this.isDynamic = true;
    } else {
        this.color_.alpha = opacity;
        this.isDynamic = false;
    }
};

/**
 * Contains color source.
 *
 * @private
 * @type {anychart.visual.color.Color|anychart.visual.color.colorOperations.IColorOperation|string}
 */
anychart.visual.color.ColorContainer.prototype.color_ = null;

/**
 * Contains color opacity if source type is dynamic.
 *
 * @private
 * @type {Number} [0..1]
 */
anychart.visual.color.ColorContainer.prototype.opacity_ = 1;

/**
 * Indicates whether the color is dynamic (needs %Color value to be calculated).
 *
 * @type {boolean}
 */
anychart.visual.color.ColorContainer.prototype.isDynamic = false;

/**
 * Returns stored color. If stored color is dynamic it needs opt_color to be
 * passed to replace %Color token.
 * If opt_color is not passed and the container source is dynamic, %color will
 * be replaced with black color.
 * If container source is not dynamic, opt_color may be omitted.
 *
 * @param {anychart.visual.color.Color} opt_color (replaces %Color value).
 * @return {anychart.visual.color.Color} Color instance.
 */
anychart.visual.color.ColorContainer.prototype.getColor = function(opt_color) {
    if (this.isDynamic) {
        if (!opt_color) opt_color = new anychart.visual.color.Color();
        if (this.color_.execute != undefined) {
            var color = this.color_.execute(opt_color);
            color.alpha = this.opacity_;
            return color;
        } else return opt_color;
    } else return this.color_;
};

//------------------------------------------------------------------------------
//
//                          ColorParser
//
//------------------------------------------------------------------------------

/**
 * Color parser package.
 *
 */
anychart.visual.color.colorParser = {};

/**
 * Parses color definition string and returns defined color container that has
 * all necessary information to calculate a color. This function is the only
 * public entity in this namespace.
 *
 * @param {String} color Color.
 * @param {number=} opt_opacity Opacity.
 * @return {anychart.visual.color.ColorContainer} Color container.
 */
anychart.visual.color.colorParser.parse = function(color, opt_opacity) {
    if (!color) return new anychart.visual.color.ColorContainer(
            new anychart.visual.color.Color(0, 0, 0), 1);
    var parser = new anychart.visual.color.colorParser.Parser_(color);
    try {
        return new anychart.visual.color.ColorContainer(parser.parse(),
                (opt_opacity == undefined) ? 1 : opt_opacity);
    } catch (e) {
        //throw new Error(e.message);
        return new anychart.visual.color.ColorContainer(
                new anychart.visual.color.Color(0, 0, 0), 1);
    }
};

/**
 * Color lexer constructor. Lexer parses color definition string and returns
 * found lexemes one per scan() method call.
 *
 * @private
 * @constructor
 * @param {String} colorString Color definition string.
 */
anychart.visual.color.colorParser.Lexer_ = function(colorString) {
    this.input = anychart.utils.deserialization.getLString(colorString);
    this.inputLength_ = colorString.length;
};

/**
 * Input color string.
 *
 * @type {String}
 */
anychart.visual.color.colorParser.Lexer_.prototype.input = '';

/**
 * Input length.
 *
 * @private
 * @type {int}
 */
anychart.visual.color.colorParser.Lexer_.prototype.inputLength_ = 0;

/**
 * Current lexer position.
 *
 * @private
 * @type {int}
 */
anychart.visual.color.colorParser.Lexer_.prototype.currPos_ = 0;

/**
 * Web colors array.
 *
 * @const
 * @type {Object.<int>}
 */
anychart.visual.color.colorParser.Lexer_.WEB_COLORS = {
    'steelblue': 0x4682B4,
    'royalblue': 0x041690,
    'cornflowerblue': 0x6495ED,
    'lightsteelblue': 0xB0C4DE,
    'mediumslateblue': 0x7B68EE,
    'slateblue': 0x6A5ACD,
    'darkslateblue': 0x483D8B,
    'midnightblue': 0x191970,
    'navy': 0x000080,
    'darkblue': 0x00008B,
    'mediumblue': 0x0000CD,
    'blue': 0x0000FF,
    'dodgerblue': 0x1E90FF,
    'deepskyblue': 0x00BFFF,
    'lightskyblue': 0x87CEFA,
    'skyblue': 0x87CEEB,
    'lightblue': 0xADD8E6,
    'powderblue': 0xB0E0E6,
    'azure': 0xF0FFFF,
    'lightcyan': 0xE0FFFF,
    'paleturquoise': 0xAFEEEE,
    'mediumturquoise': 0x48D1CC,
    'lightseagreen': 0x20B2AA,
    'darkcyan': 0x008B8B,
    'teal': 0x008080,
    'cadetblue': 0x5F9EA0,
    'darkturquoise': 0x00CED1,
    'aqua': 0x00FFFF,
    'cyan': 0x00FFFF,
    'turquoise': 0x40E0D0,
    'aquamarine': 0x7FFFD4,
    'mediumaquamarine': 0x66CDAA,
    'darkseagreen': 0x8FBC8F,
    'mediumseagreen': 0x3CB371,
    'seagreen': 0x2E8B57,
    'darkgreen': 0x006400,
    'green': 0x008000,
    'forestgreen': 0x228B22,
    'limegreen': 0x32CD32,
    'lime': 0x00FF00,
    'chartreuse': 0x7FFF00,
    'lawngreen': 0x7CFC00,
    'greenyellow': 0xADFF2F,
    'yellowgreen': 0x9ACD32,
    'palegreen': 0x98FB98,
    'lightgreen': 0x90EE90,
    'springgreen': 0x00FF7F,
    'mediumspringgreen': 0x00FA9A,
    'darkolivegreen': 0x556B2F,
    'olivedrab': 0x6B8E23,
    'olive': 0x808000,
    'darkkhaki': 0xBDB76B,
    'darkgoldenrod': 0xB8860B,
    'goldenrod': 0xDAA520,
    'gold': 0xFFD700,
    'yellow': 0xFFFF00,
    'khaki': 0xF0E68C,
    'palegoldenrod': 0xEEE8AA,
    'blanchedalmond': 0xFFEBCD,
    'moccasin': 0xFFE4B5,
    'wheat': 0xF5DEB3,
    'navajowhite': 0xFFDEAD,
    'burlywood': 0xDEB887,
    'tan': 0xD2B48C,
    'rosybrown': 0xBC8F8F,
    'sienna': 0xA0522D,
    'saddlebrown': 0x8B4513,
    'chocolate': 0xD2691E,
    'peru': 0xCD853F,
    'sandybrown': 0xF4A460,
    'darkred': 0x8B0000,
    'maroon': 0x800000,
    'brown': 0xA52A2A,
    'firebrick': 0xB22222,
    'indianred': 0xCD5C5C,
    'lightcoral': 0xF08080,
    'salmon': 0xFA8072,
    'darksalmon': 0xE9967A,
    'lightsalmon': 0xFFA07A,
    'coral': 0xFF7F50,
    'tomato': 0xFF6347,
    'darkorange': 0xFF8C00,
    'orange': 0xFFA500,
    'orangered': 0xFF4500,
    'crimson': 0xDC143C,
    'red': 0xFF0000,
    'deeppink': 0xFF1493,
    'fuchsia': 0xFF00FF,
    'magenta': 0xFF00FF,
    'hotpink': 0xFF69B4,
    'lightpink': 0xFFB6C1,
    'pink': 0xFFC0CB,
    'palevioletred': 0xDB7093,
    'mediumvioletred': 0xC71585,
    'purple': 0x800080,
    'darkmagenta': 0x8B008B,
    'mediumpurple': 0x9370DB,
    'blueviolet': 0x8A2BE2,
    'indigo': 0x4B0082,
    'darkviolet': 0x9400D3,
    'darkorchid': 0x9932CC,
    'mediumorchid': 0xBA55D3,
    'orchid': 0xDA70D6,
    'violet': 0xEE82EE,
    'plum': 0xDDA0DD,
    'thistle': 0xD8BFD8,
    'lavender': 0xE6E6FA,
    'ghostwhite': 0xF8F8FF,
    'aliceblue': 0xF0F8FF,
    'mintcream': 0xF5FFFA,
    'honeydew': 0xF0FFF0,
    'lightgoldenrodyellow': 0xFAFAD2,
    'lemonchiffon': 0xFFFACD,
    'ornsilk': 0xFFF8DC,
    'lightyellow': 0xFFFFE0,
    'ivory': 0xFFFFF0,
    'floralwhite': 0xFFFAF0,
    'linen': 0xFAF0E6,
    'oldlace': 0xFDF5E6,
    'antiquewhite': 0xFAEBD7,
    'bisque': 0xFFE4C4,
    'peachpuff': 0xFFDAB9,
    'papayawhip': 0xFFEFD5,
    'beige': 0xF5F5DC,
    'seashell': 0xFFF5EE,
    'lavenderblush': 0xFFF0F5,
    'istyrose': 0xFFE4E1,
    'snow': 0xFFFAFA,
    'white': 0xFFFFFF,
    'whitesmoke': 0xF5F5F5,
    'gainsboro': 0xDCDCDC,
    'lightgrey': 0xD3D3D3,
    'silver': 0xC0C0C0,
    'darkgray': 0xA9A9A9,
    'gray': 0x808080,
    'lightslategray': 0x778899,
    'slategray': 0x708090,
    'imgray': 0x696969,
    'darkslategray': 0x2F4F4F,
    'black': 0x000000
};

/**
 * Possible function names.
 *
 * @const
 * @type {Object.<int>}
 */
anychart.visual.color.colorParser.FUNCTION_IDS = {
    'blend': 1,
    'lightcolor': 2,
    'darkcolor': 3,
    'rgb': 4,
    'hsb': 5
};

/**
 * Possible lexeme types.
 *
 * @const
 * @enum {int}
 */
anychart.visual.color.colorParser.Lexer_.LEXEM_TYPES = {
    NONE: 0,
    FUNCTION: 1,
    VARIABLE: 2,
    COLOR: 3,
    NUMBER: 4,
    COMMA: 5,
    L_PAREN: 6,
    R_PAREN: 7
};

/**
 * Reflects parsed lexeme type.
 *
 * @type {int}
 */
anychart.visual.color.colorParser.Lexer_.prototype.lexemType =
        anychart.visual.color.colorParser.Lexer_.LEXEM_TYPES.NONE;

/**
 * Reflects parsed lexeme value.
 *
 * @type {(string|int|Number)}
 */
anychart.visual.color.colorParser.Lexer_.prototype.lexemValue = null;

/**
 * Current lexer start position.
 *
 * @type {int}
 */
anychart.visual.color.colorParser.Lexer_.prototype.lexemStart = 0;

/**
 * Scans input string for next lexeme. Ignores whitespace and unexpected
 * symbols. Returns true if a lexeme was successfully parsed, false otherwise.
 * Lexeme type is stored in lexemType property, lexeme value - in lexemValue
 * property, lexeme starting character column number - in lexemStart property.
 *
 * @return {boolean} Is lexeme was successfully parsed.
 */
anychart.visual.color.colorParser.Lexer_.prototype.scan = function() {
    var LEXEM_TYPES = anychart.visual.color.colorParser.Lexer_.LEXEM_TYPES;
    var done = false;
    var state = 0; // start state
    var currValue = '';
    var gotDot = false;
    this.lexemType = LEXEM_TYPES.NONE;
    this.lexemStart = this.currPos_;
    while (!done && this.currPos_ < this.inputLength_) {
        var currChar = this.input.charAt(this.currPos_);
        var foundLetter = currChar >= 'a' && currChar <= 'z';
        var foundNumber = currChar >= '0' && currChar <= '9';
        var foundHex = foundNumber || (currChar >= 'a' && currChar <= 'f');
        var foundWhiteSpace = currChar == ' ' || currChar == '\t';
        switch (state) {
            case 0: // start state, ignoring spaces, looking for lexem start
                if (!foundWhiteSpace) {
                    currValue = currChar;
                    if (foundLetter || currChar == '%') {
                        state = 1; // start identifier parsing
                    } else if (currChar == '0') {
                        state = 4; // decide what to parse - a number or a
                                   // 0x hex color
                    } else if (foundNumber) {
                        state = 2; // start number parsing
                    } else if (currChar == '#') {
                        currValue = '0x';
                        state = 3; // start hex parsing
                    } else if (currChar == '.') {
                        gotDot = true;
                        state = 2; // start number parsing with a dot
                    } else if (currChar == '(') {
                        this.lexemType = LEXEM_TYPES.L_PAREN;
                        this.lexemValue = currChar;
                        this.currPos_++;
                        done = true;
                    } else if (currChar == ')') {
                        this.lexemType = LEXEM_TYPES.R_PAREN;
                        this.lexemValue = currChar;
                        this.currPos_++;
                        done = true;
                    } else if (currChar == ',') {
                        this.lexemType = LEXEM_TYPES.COMMA;
                        this.lexemValue = currChar;
                        this.currPos_++;
                        done = true;
                    } else {
                        this.lexemStart = this.currPos_ + 1;
                        // do nothing
                    }
                } else {
                    this.lexemStart = this.currPos_ + 1;
                }
                break;
            case 1: // identifier parsing, only letters are allowed
                if (foundLetter) {
                    currValue += currChar;
                } else {
                    this.qualifyIdentifier(currValue);
                    done = true;
                }
                break;
            case 2: // number parsing
                if (foundNumber) {
                    currValue += currChar;
                } else if (currChar == '.') {
                    if (gotDot) {
                        this.lexemType = LEXEM_TYPES.NUMBER;
                        this.lexemValue = Number(currValue);
                        if (isNaN(this.lexemValue)) this.lexemValue = 0;
                        done = true;
                    } else {
                        currValue += currChar;
                        gotDot = true;
                    }
                } else {
                    this.lexemType = LEXEM_TYPES.NUMBER;
                    this.lexemValue = Number(currValue);
                    if (isNaN(this.lexemValue)) this.lexemValue = 0;
                    done = true;
                }
                break;
            case 3: // hex parsing
                if (foundHex) {
                    currValue += currChar;
                } else {
                    this.lexemType = LEXEM_TYPES.COLOR;
                    this.lexemValue = this.parseColor(Number(currValue));
                    done = true;
                }
                break;
            case 4: // deciding what to parse - a 0x hex color or a number
                if (currChar == 'x') {
                    currValue += currChar;
                    state = 3; // parse hex
                } else if (foundNumber) {
                    currValue += currChar;
                    state = 2; // parse number
                } else if (currChar == '.') {
                    currValue += currChar;
                    gotDot = true;
                    state = 2; // parse number
                } else {
                    this.lexemType = LEXEM_TYPES.NUMBER;
                    this.lexemValue = 0;
                    done = true;
                }
                break;
        }
        if (!done)
            this.currPos_++;
    }
    if (this.currPos_ < this.inputLength_)
        return done;
    switch (state) {
        case 0:
            return this.lexemType != LEXEM_TYPES.NONE;
        case 1:
            this.qualifyIdentifier(currValue);
            return true;
        case 2:
            this.lexemType = LEXEM_TYPES.NUMBER;
            this.lexemValue = Number(currValue);
            if (isNaN(this.lexemValue)) this.lexemValue = 0;
            return true;
        case 3:
            this.lexemType = LEXEM_TYPES.COLOR;
            this.lexemValue = this.parseColor(Number(currValue));
            return true;
        case 4:
            this.lexemType = LEXEM_TYPES.NUMBER;
            this.lexemValue = 0;
            return true;
    }
    return false;
};

/**
 * Recognizes identifier essence and sets lexemType and lexemValue properties
 * accordingly.
 *
 * @param {string} identifier Identifier.
 */
anychart.visual.color.colorParser.Lexer_.prototype.qualifyIdentifier =
        function(identifier) {
            var LEXEM_TYPES =
                    anychart.visual.color.colorParser.Lexer_.LEXEM_TYPES;
            var FUNCTION_IDS = anychart.visual.color.colorParser.FUNCTION_IDS;
            var WEB_COLORS =
                    anychart.visual.color.colorParser.Lexer_.WEB_COLORS;
            if (identifier == '%color') {
                this.lexemType = LEXEM_TYPES.VARIABLE;
                this.lexemValue = identifier;
            } else if (FUNCTION_IDS[identifier] != undefined) {
                this.lexemType = LEXEM_TYPES.FUNCTION;
                this.lexemValue = FUNCTION_IDS[identifier];
            } else {
                this.lexemType = LEXEM_TYPES.COLOR;
                this.lexemValue = this.parseColor(
                        (WEB_COLORS[identifier] != undefined) ?
                                WEB_COLORS[identifier] : 0xCCCCCC);
            }
        };

/**
 * Parses integer value and creates Color from it. An integer value is treated
 * as RGB value, one byte per channel.
 *
 * @param {!int} color Value to parse.
 * @return {anychart.visual.color.Color} Color instance.
 */
anychart.visual.color.colorParser.Lexer_.prototype.parseColor =
        function(color) {
            var g = color >> 8;
            var r = g >> 8;
            return new anychart.visual.color.Color(
                    r % 0x100, g % 0x100, color % 0x100);
        };

/**
 * Color string parser instance constructor. Creates internal lexer and passes
 * input string to it.
 *
 * @private
 * @constructor
 * @param {string} colorString Coloe definition string.
 */
anychart.visual.color.colorParser.Parser_ = function(colorString) {
    this.lexer_ = new anychart.visual.color.colorParser.Lexer_(colorString);
};

/**
 * Lexer instance.
 *
 * @private
 * @type {anychart.visual.color.colorParser.Lexer_}
 */
anychart.visual.color.colorParser.Parser_.prototype.lexer_ = null;

/**
 * Parses color string and calculates color value whenever possible. If parsed
 * color depends on %Color value, IColorOperaion aggregate is returned. If
 * parser color is exactly %Color, "%color" string is returned. In it's work
 * parser uses Lexer_ instance which scan() method is repeatedly called to
 * get next lexeme from input string. The parser is a based on pushdown
 * automation.
 *
 * @return {anychart.visual.color.Color|
 * anychart.visual.color.colorOperations.IColorOperation|
 * String} Parsed color.
 */
anychart.visual.color.colorParser.Parser_.prototype.parse = function() {
    var LEXEM_TYPES = anychart.visual.color.colorParser.Lexer_.LEXEM_TYPES;
    var Factory = anychart.visual.color.colorOperations.Factory;
    var state = 0; // scanning new construction
    var stack = [];
    var func;
    var errmsg;
    while (this.lexer_.scan()) {
        switch (state) {
            case 0: // start of parsing
                switch (this.lexer_.lexemType) {
                    case LEXEM_TYPES.FUNCTION:
                        state = 1; // expecting left paren
                        // when rparen will be found, parser whould pop stack
                        // until it find this state
                        stack.push(Factory.getOperation(
                                this.lexer_.lexemValue));
                        break;
                    case LEXEM_TYPES.COLOR:
                    case LEXEM_TYPES.VARIABLE:
                        state = 4; // expecting end of input
                        stack.push(this.lexer_.lexemValue);
                        break;
                    default:
                        throw new Error('Color parser error: ' +
                                'unexpected lexem at ' +
                                this.lexer_.lexemStart +
                                ' in "' + this.lexer_.input +
                                '". Function name, color or %Color expected.');
                }
                break;
            case 1: // expecting left paren
                if (this.lexer_.lexemType != LEXEM_TYPES.L_PAREN) {
                    throw new Error('Color parser error: unexpected left ' +
                            'parenthesis at ' + this.lexer_.lexemStart +
                                ' in "' + this.lexer_.input +
                            '". Left paren expected.');
                } else {
                    state = 2; // argument scan, expecting argument or rparen
                    break;
                }
            case 2: // argument scanning, expecting argument or rparen
                switch (this.lexer_.lexemType) {
                    case LEXEM_TYPES.FUNCTION:
                        state = 1; // expecting left paren
                        stack.push(Factory.getOperation(
                                this.lexer_.lexemValue));
                        break;
                    case LEXEM_TYPES.COLOR:
                    case LEXEM_TYPES.VARIABLE:
                    case LEXEM_TYPES.NUMBER:
                        state = 3; // expecting comma or right paren
                        stack[stack.length - 1].arguments.push(
                                this.lexer_.lexemValue);
                        break;
                    case LEXEM_TYPES.R_PAREN:
                        func = stack.pop();
                        errmsg = func.checkArguments();
                        if (errmsg != null) throw new Error(errmsg);
                        if (func.canBeExecuted()) func = func.execute();
                        if (stack.length == 0) {
                            state = 4; // expecting end of input
                            stack.push(func);
                        } else {
                            state = 3; // expecting comma or right paren
                            stack[stack.length - 1].arguments.push(func);
                        }
                        break;
                    default:
                        throw new Error('Color parser error: ' +
                                'unexpected lexem at ' +
                                this.lexer_.lexemStart +
                                ' in "' + this.lexer_.input +
                                '". Function name, color, %Color, ' +
                                'number or right paren expected.');
                }
                break;
            case 3: // expecting comma or right paren
                switch (this.lexer_.lexemType) {
                    case LEXEM_TYPES.R_PAREN:
                        func = stack.pop();
                        errmsg = func.checkArguments();
                        if (errmsg != null) throw new Error(errmsg);
                        if (func.canBeExecuted()) func = func.execute();
                        if (stack.length == 0) {
                            state = 4; // expecting end of input
                            stack.push(func);
                        } else {
                            state = 3; // expecting comma or right paren
                            stack[stack.length - 1].arguments.push(func);
                        }
                        break;
                    case LEXEM_TYPES.COMMA:
                        state = 2; // argument scan,
                                   // expecting argument or rparen
                        break;
                    default:
                        throw new Error('Color parser error: ' +
                                'unexpected lexem at ' +
                                this.lexer_.lexemStart +
                                ' in "' + this.lexer_.input +
                                '". Expected comma or right paren.');
                }
                break;
            case 4:
                throw new Error('Color parser error: ' +
                        'unexpected lexem at ' +
                        this.lexer_.lexemStart +
                        ' in "' + this.lexer_.input +
                        '". End of string expected.');
        }
    }
    if (state != 4) throw new Error('Color parser error: ' +
            'unexpected end of string in "' + this.lexer_.input + '"');
    return stack.pop();
};


