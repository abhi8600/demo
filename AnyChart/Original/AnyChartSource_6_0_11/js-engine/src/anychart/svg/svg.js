/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.svg.SVGManager}</li>
 *  <li>@class {anychart.svg.SVGSprite}</li>
 *  <li>@class {anychart.svg.SVGScrollableSprite}</li>
 *  <li>@class {anychart.svg.SVGDraggableSprite}</li>
 * <ul>
 */
goog.provide('anychart.svg');

goog.require('goog.events.EventTarget');
goog.require('anychart.visual.gradient');
goog.require('anychart.visual.hatchFill');
goog.require('anychart.visual.effects');
goog.require('anychart.utils.geom');

//------------------------------------------------------------------------------
//
//                          SVGManager class.
//
//------------------------------------------------------------------------------
/**
 * Anychart svg manager.
 * @constructor
 */
anychart.svg.SVGManager = function () {
    this.svg_ = document.createElementNS(this.svgNS_, 'svg');
    this.svg_.setAttribute('xmlns', this.svgNS_);
    this.svg_.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');

    this.svgBounds_ = new anychart.utils.geom.Rectangle();
    this.updateSVGBounds_();

    //svg element to text measure
    this.measureSVG_ = this.clone(this.svg_);
    this.measureSVG_.setAttribute('id', 'AnyChart SVG element for text measure');
    this.measureSVG_.setAttribute('width', '0');
    this.measureSVG_.setAttribute('height', '0');

    this.defs_ = this.createSVGElement('defs');
    this.svg_.appendChild(this.defs_);

    this.gradientManager_ = new anychart.visual.gradient.SVGGradientManager(this);
    this.textMeasurement_ = new anychart.visual.text.SVGTextMeasurement(this);
    this.hatchFillManager_ = new anychart.visual.hatchFill.SVGHatchFillManager(this);
    this.effectsManager_ = new anychart.visual.effects.SVGEffectsManager(this);

    if (IS_ANYCHART_DEBUG_MOD) {
        this.svg_.setAttribute('id', 'AnyChartSVG');
        this.measureSVG_.setAttribute('id', 'AnyChartSVGMeasure');
    }
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * SVG document namespace.
 * @private
 * @type {String}
 */
anychart.svg.SVGManager.prototype.svgNS_ = 'http://www.w3.org/2000/svg';
/**
 * Svg node of svg-document.
 * @private
 * @type {SVGElement}
 */
anychart.svg.SVGManager.prototype.svg_ = null;
/**
 * Getter for svg node element.
 * @return {SVGElement} Svg node.
 */
anychart.svg.SVGManager.prototype.getSVG = function () {
    return this.svg_;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.svg.SVGManager.prototype.svgBounds_ = null;
/**
 * Getter for svg node element.
 * @return {anychart.utils.geom.Rectangle} SVG bounds.
 */
anychart.svg.SVGManager.prototype.getSVGBounds = function () {
    this.updateSVGBounds_();
    return this.svgBounds_;
};
/**
 * Update svg bounds object from svg getBBox method
 * @private
 */
anychart.svg.SVGManager.prototype.updateSVGBounds_ = function () {
    try {
        var bBox = this.svg_['getBBox']();
        this.svgBounds_.x = bBox.x;
        this.svgBounds_.y = bBox.y;
        this.svgBounds_.width = bBox.width;
        this.svgBounds_.height = bBox.height;
    } catch (error) {

    }
};
/**
 * Svg defs element.
 * @private
 * @type {SVGElement}
 */
anychart.svg.SVGManager.prototype.defs_ = null;
/**
 * @private
 * @type {SVGElement}
 */
anychart.svg.SVGManager.prototype.measureSVG_ = null;
/**
 * @return {SVGElement}
 */
anychart.svg.SVGManager.prototype.getMeasureSVG = function () {
    return this.measureSVG_;
};
//------------------------------------------------------------------------------
//                          Managers.
//------------------------------------------------------------------------------
/**
 * Gradient manager variable
 * @type {anychart.visual.effects.SVGEffectsManager}
 * @private
 */
anychart.svg.SVGManager.prototype.effectsManager_ = null;
/**
 * Getter for gradient manager
 * @return {anychart.visual.gradient.SVGGradientManager} Gradient manager.
 */
anychart.svg.SVGManager.prototype.getEffectsManager = function () {
    return this.effectsManager_;
};
/**
 * Gradient manager variable
 * @type {anychart.visual.gradient.SVGGradientManager}
 * @private
 */
anychart.svg.SVGManager.prototype.gradientManager_ = null;
/**
 * Getter for gradient manager
 * @return {anychart.visual.gradient.SVGGradientManager} Gradient manager.
 */
anychart.svg.SVGManager.prototype.getGradientManager = function () {
    return this.gradientManager_;
};
/**
 * Test measurement variable.
 * @type {anychart.visual.text.SVGTextMeasurement}
 * @private
 */
anychart.svg.SVGManager.prototype.textMeasurement_ = null;
/**
 * Getter for text measurement.
 * @return {anychart.visual.text.SVGTextMeasurement} Text measurement.
 */
anychart.svg.SVGManager.prototype.getTextMeasurement = function () {
    return this.textMeasurement_;
};
/**
 * Hatch fill manager variable.
 * @type {anychart.visual.hatchFill.SVGHatchFillManager}
 * @private
 */
anychart.svg.SVGManager.prototype.hatchFillManager_ = null;
/**
 * Getter for hatch fill.
 * @return {anychart.visual.hatchFill.SVGHatchFillManager} SVGHatchFillManager.
 */
anychart.svg.SVGManager.prototype.getHatchFillManager = function () {
    return this.hatchFillManager_;
};
/**
 * Removes the child nodes for defs node.
 */
anychart.svg.SVGManager.prototype.clearDefs = function () {
    this.clearGroup(this.defs_);
    this.gradientManager_.clear();
    this.hatchFillManager_.clear();
    this.effectsManager_.clear();
};
//------------------------------------------------------------------------------
//                  Div for convert unicode and ascii symbols.
//                 http://dev.anychart.com/jira/browse/AFJ-170
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.svg.SVGManager.prototype.symbolsConverterDiv_ = null;
/**
 * @return {Object}
 */
anychart.svg.SVGManager.prototype.getSymbolsConverterDiv = function () {
    if (!this.symbolsConverterDiv_) this.createSymbolsConverterDiv_();
    return this.symbolsConverterDiv_;
};
/**
 * @private
 */
anychart.svg.SVGManager.prototype.createSymbolsConverterDiv_ = function () {
    this.symbolsConverterDiv_ = document.createElement('div');
    this.symbolsConverterDiv_.setAttribute('id', 'AnyChart hidden div for text measure');
    this.symbolsConverterDiv_.setAttribute('width', '0');
    this.symbolsConverterDiv_.setAttribute('height', '0');
    document['body'].appendChild(this.symbolsConverterDiv_);
};
//------------------------------------------------------------------------------
//                          Path element
//------------------------------------------------------------------------------
/**
 * Create <path> element.
 * @param {boolean=} opt_useCrispEdges use crisp edges or not.
 * @return {SVGElement} Path element.
 */
anychart.svg.SVGManager.prototype.createPath = function (opt_useCrispEdges) {
    var path = this.createSVGElement('path');
    if (opt_useCrispEdges)
        path.setAttribute('shape-rendering', 'crispEdges');
    return path;
};

/**
 * Moveto command.
 * @param {number} x x coordinate.
 * @param {number} y y coordinate.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {string} Path element attribute value for move point.
 */
anychart.svg.SVGManager.prototype.pMove = function (x, y, opt_useCrispEdges) {
    if (opt_useCrispEdges) {
        x = Math.round(x * 10) / 10;
        y = Math.round(y * 10) / 10;
    }

    return 'M' + x.toString() + ',' + y.toString();
};

/**
 * Lineto command.
 * @param {number} x x coordinate.
 * @param {number} y y coordinate.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {string} Path element attribute value for drawing line.
 */
anychart.svg.SVGManager.prototype.pLine = function (x, y, opt_useCrispEdges) {
    if (opt_useCrispEdges) {
        x = Math.round(x * 10) / 10;
        y = Math.round(y * 10) / 10;
    }

    return ' L' + x.toString() + ',' + y.toString();
};

/**
 * Draw line with round ti integer and normalize to crisp line
 * @param {Number} startX Start line x
 * @param {Number} startY Start line y.
 * @param {Number} endX End line x.
 * @param {Number} endY End line y.
 */
anychart.svg.SVGManager.prototype.pRLine = function (startX, startY, endX, endY, width) {
    if (startX === endX)  startX = endX = Math.round(startX) + (width % 2 / 2);
    if (startY === endY)  startY = endY = Math.round(startY) + (width % 2 / 2);
    return 'M' + startX.toString() + ',' + startY.toString() + ' L' + endX.toString() + ',' + endY.toString();
};

/**
 * Horizontal lineto command.
 * @param {number} x x coordinate.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {string} Path element attribute value for drawing horizontal line.
 */
anychart.svg.SVGManager.prototype.svgPHLine = function (x, opt_useCrispEdges) {
    if (opt_useCrispEdges) {
        x = Math.round(x * 10) / 10;
    }

    return ' H' + x.toString();
};

/**
 * Vertical lineto command.
 * @param {number} y y coordinate.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {string} Path element attribute value for drawing vertical line.
 */
anychart.svg.SVGManager.prototype.svgPVLine = function (y, opt_useCrispEdges) {
    if (opt_useCrispEdges) {
        y = Math.round(y * 10) / 10;
    }

    return ' V' + y.toString();
};

/**
 * Elliptical arc.
 * @see <a href="http://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands">
 *     The elliptical arc curve commands.</a>
 * @param {number} radiusX x radius.
 * @param {boolean} largeArc large-arc-flag.
 * @param {boolean} sweepArc sweep-arc-flag.
 * @param {number} endX end x coordinate.
 * @param {number} endY end y coordinate.
 * @param {number} radiusY y radius.
 * @return {string} Path element attribute value for drawing elliptical arc.
 */
anychart.svg.SVGManager.prototype.pCircleArc = function (radiusX, largeArc, sweepArc, endX, endY, radiusY) {
    if (arguments.length == 5) radiusY = radiusX;
    if ((radiusX == 0) || (radiusY == 0)) return '';
    return ' A' + radiusX.toString() + ',' + radiusY.toString() + ' 0 ' +
        (largeArc ? '1' : '0') + ' ' + (sweepArc ? '1' : '0') + ' ' +
        endX.toString() + ',' + endY.toString();
};
/**
 * ND: Needs doc!
 * @param {Number} x x coordinate.
 * @param {Number} y y coordinate.
 * @param {Number} r radius of circle.
 * @return {String} Path element attribute value for drawing circle.
 */
anychart.svg.SVGManager.prototype.pCircle = function(x, y, r) {
    var startX = x + r,
        endX = x + r * Math.cos(359.999 * Math.PI / 180),
        endY = y + r * Math.sin(359.999 * Math.PI / 180),
        path = '';
    path += this.pMove(startX, y);
    path += this.pCircleArc(r, 1, 1, endX, endY, r);
    return path;
};
/**
 * Draws a cubic Bezier curve from the current point to (cx,cy) using (cx1,cy1)
 * as the control point at the beginning of the curve and (cx2,cy2) as the
 * control point at the end of the curve. If using opt_shorthand it draws a
 * cubic Bezier curve from the current point to (opt_sx,opt_sy). The first
 * control point is assumed to be the reflection of the (cx2, cy2) point.
 * (opt_sx2,opt_sy2) is the second control point. If opt_shorthand is set it`s
 * necessary to set opt_sx2, opt_sy2, opt_sx and opt_sy variables, otherwise
 * opt_shorthand would be ignored.
 * @param {number} cx1 x coordinate of first control point.
 * @param {number} cy1 y coordinate of first control point.
 * @param {number} cx2 x coordinate of second control point.
 * @param {number} cy2 y coordinate of second control point.
 * @param {number} cx x coordinate of end point.
 * @param {number} cy y coordinate of end point.
 * @param {boolean=} opt_shorthand Use shorthand curve or not.
 * @param {number=} opt_sx2 x coordinate of second control point.
 * @param {number=} opt_sy2 y coordinate of second control point.
 * @param {number=} opt_sx x coordinate of end point of shorthand cubic Bezier
 * curve.
 * @param {number=} opt_sy y coordinate of end point of shorthand cubic Bezier
 * curve.
 * @return {string} Path element attribute value for drawing cubic bezier curve.
 */
anychart.svg.SVGManager.prototype.pCubicBezier = function (cx1, cy1, cx2, cy2, cx, cy, opt_shorthand, opt_sx2, opt_sy2, opt_sx, opt_sy) {
    if (opt_shorthand && arguments.length == 11) {
        return ' C' + cx1.toString() + ',' + cy1.toString() + ' ' +
            cx2.toString() + ',' + cy2.toString() + ' ' +
            cx.toString() + ',' + cy.toString() +
            ' S' + opt_sx2.toString() + ',' + opt_sy2.toString() + ' ' +
            opt_sx.toString() + ',' + opt_sy.toString();
    } else {
        return ' C' + cx1.toString() + ',' + cy1.toString() + ' ' +
            cx2.toString() + ',' + cy2.toString() + ' ' +
            cx.toString() + ',' + cy.toString();
    }
};

/**
 * Draws a quadratic Bezier curve from the current point to (qx,qy) using
 * (qx1,qy1) as the control point. If using opt_shorthand it draws a quadratic
 * Bezier curve from the current point to (opt_tx, opt_ty). The control point
 * is assumed to be the reflection of qx1, qx2 point. If opt_shorthand is set
 * it`s necessary to set opt_tx and opt_ty variables, otherwise opt_shorthand
 * would be ignored.
 * @param {number} qx1 x coordinate of control point.
 * @param {number} qy1 y coordinate of control point.
 * @param {number} qx x coordinate of end point.
 * @param {number} qy y coordinate of end point.
 * @param {boolean=} opt_shorthand Use shorthand curve or not.
 * @param {number=} opt_tx x coordinate of end point of shorthand quadratic
 * Bezier curve.
 * @param {number=} opt_ty y coordinate of end point of shorthand quadratic
 * Bezier curve.
 * @return {string} Path element attribute value for drawing quadratic bezier
 * curve.
 */
anychart.svg.SVGManager.prototype.pQuadraticBezier = function (qx1, qy1, qx, qy, opt_shorthand, opt_tx, opt_ty) {
    if (opt_shorthand && arguments.length == 7) {
        return ' Q' + qx1.toString() + ',' + qy1.toString() + ' ' +
            qx.toString() + ',' + qy.toString() +
            ' T' + opt_tx.toString() + ',' + opt_ty.toString();
    } else {
        return ' Q' + qx1.toString() + ',' + qy1.toString() + ' ' +
            qx.toString() + ',' + qy.toString();
    }
};

/**
 * The "closepath" command.
 * @return {string} ' Z'. Command to close the current subpath.
 */
anychart.svg.SVGManager.prototype.pFinalize = function () {
    return ' Z';
};
/**
 * Create rectangle path data.
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @return {String}
 */
anychart.svg.SVGManager.prototype.pRect = function (x, y, width, height) {
    var left = x + width;
    var bottom = y + height;

    var path = this.pMove(x, y);
    path += this.pLine(left, y);
    path += this.pLine(left, bottom);
    path += this.pLine(x, bottom);
    path += this.pLine(x, y);

    return path;
};
/**
 * Create rectangle path data by bounds.
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {String}
 */
anychart.svg.SVGManager.prototype.pRectByBounds = function (bounds) {
    return this.pRect(bounds.x, bounds.y, bounds.width, bounds.height);
};
//------------------------------------------------------------------------------
//                       Polyline element.
//------------------------------------------------------------------------------
/**
 * Create <polyline> element.
 * @param {string=} opt_points Initial points.
 * @return {SVGElement} Polyline element.
 */
anychart.svg.SVGManager.prototype.createPolyline = function (opt_points) {
    var polyLine = this.createSVGElement('polyline');
    if (opt_points) polyLine.setAttribute('points', opt_points);
    return polyLine;
};

/**
 * Draw polyline to (x,y).
 * @param {number=} x x coordinate.
 * @param {number=} y y coordinate.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {string} (x, y) pair separated by comma.
 */
anychart.svg.SVGManager.prototype.polyLineDraw = function (x, y, opt_useCrispEdges) {
    if (opt_useCrispEdges) {
        x = Math.round(x * 10) / 10;
        y = Math.round(y * 10) / 10;
    }
    return x.toString() + ',' + y.toString() + ' ';
};

//------------------------------------------------------------------------------
//                      Clip path element
//------------------------------------------------------------------------------
/**
 * Number of clipPath element.
 * @type {number}
 * @private
 */
anychart.svg.SVGManager.clipPathCount_ = 0;
/**
 * Create <clipPath> element.
 * @return {SVGElement} clipPath element.
 */
anychart.svg.SVGManager.prototype.createClipPath = function () {
    var el = this.createSVGElement('clipPath');
    el.setAttribute('id', 'anychart_clippath_' + (anychart.svg.SVGManager.clipPathCount_++).toString());
    el.setAttribute('clip-rule', 'nonzero');
    return el;
};
//------------------------------------------------------------------------------
//                      Elements creation.
//------------------------------------------------------------------------------
/**
 * Create SVG element by name.
 * @param {String} elementName The name of the element.
 * @return {SVGElement} Created svg element.
 */
anychart.svg.SVGManager.prototype.createSVGElement = function (elementName) {
    return this.svg_.ownerDocument.createElementNS(this.svgNS_, elementName);
};
/**
 * Creates g node element.
 * @type {SVGElement}
 * @return {SVGElement} G node.
 */
anychart.svg.SVGManager.prototype.createGroup = function () {
    return this.createSVGElement('g');
};
/**
 * Removes childs from the parent node.
 * @param {SvGElement} group Parent node.
 */
anychart.svg.SVGManager.prototype.clearGroup = function (group) {
    while (group.childNodes.length > 0)
        group.removeChild(group.childNodes[0]);
};
/**
 * Create text element.
 * @param {String} text Body of the text element.
 * @return {SVGElement} Svg text element.
 */
anychart.svg.SVGManager.prototype.createText = function (text) {
    return this.svg_.ownerDocument.createTextNode(text);
};
/**
 * Add child to defs node.
 * @param {SVGElement} element Element to add.
 */
anychart.svg.SVGManager.prototype.addDef = function (element) {
    this.defs_.appendChild(element);
};
/**
 * Create svg circle element.
 * @param {number} centerX x coordinate.
 * @param {number} centerY y coordinate.
 * @param {number} radius radius of circle.
 * @return {SVGElement} Circle svg element.
 */
anychart.svg.SVGManager.prototype.createCircle = function (centerX, centerY, radius) {
    var res = this.createSVGElement('circle');
    res.setAttribute('cx', centerX);
    res.setAttribute('cy', centerY);
    res.setAttribute('r', radius);
    return res;
};
/**
 * Create svg rectangle element.
 * @param {number} x x coordinate.
 * @param {number} y y coordinate.
 * @param {number} width width of rectangle.
 * @param {number} height height of rectangle.
 * @param {boolean=} opt_useCrispEdges Use crisp edges or not.
 * @return {SVGElement} Rectangle svg element.
 */
anychart.svg.SVGManager.prototype.createRect = function (x, y, width, height, opt_useCrispEdges) {
    if (width < 0) {
        width = Math.abs(width);
        x -= width;
    }
    if (height < 0) {
        height = Math.abs(height);
        y -= height;
    }

    var res = this.createSVGElement('rect');
    if (opt_useCrispEdges) {
        res.setAttribute('shape-rendering', 'crispEdges');
        x = Math.round(x * 10) / 10;
        y = Math.round(y * 10) / 10;
        width = Math.round(width * 10) / 10;
        height = Math.round(height * 10) / 10;
    }
    res.setAttribute('x', x);
    res.setAttribute('y', y);
    res.setAttribute('width', width);
    res.setAttribute('height', height);

    return res;
};
/**
 * Creates rectangle by bounds.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {Boolean} opt_useCrispEdges Use crisp edges or not.
 * @return {SVGElement} Rectangle SVG element.
 */
anychart.svg.SVGManager.prototype.createRectByBounds = function (bounds, opt_useCrispEdges) {
    return this.createRect(
        bounds.x,
        bounds.y,
        bounds.width,
        bounds.height,
        opt_useCrispEdges);
};
//------------------------------------------------------------------------------
//                  Element settings.
//------------------------------------------------------------------------------
anychart.svg.SVGManager.prototype.setCursorPointer = function (element) {
    element.setAttribute('cursor', 'pointer');
};
/**
 * @return {String}
 */
anychart.svg.SVGManager.prototype.getEmptySVGFilter = function (isProp) {
    return 'filter:none';
};
/**
 * @return {String} Empty SVG fill.
 */
anychart.svg.SVGManager.prototype.getEmptySVGFill = function (isProp) {
    return 'fill:none;';
};
/**
 * @return {String} Empty SVG stroke.
 */
anychart.svg.SVGManager.prototype.getEmptySVGStroke = function (isProp) {
    return 'stroke:none;';
};
/**
 * @return {String} Empty SVG style.
 */
anychart.svg.SVGManager.prototype.getEmptySVGStyle = function (isProp) {
    return this.getEmptySVGFill() + this.getEmptySVGStroke();
};
/**
 * @param {SVGElement} target Target element to clone.
 * @return {SVGElement} Cloned element.
 */
anychart.svg.SVGManager.prototype.clone = function (target) {
    return target['cloneNode'](false);
};
//------------------------------------------------------------------------------
//                  Elements workflow.
//------------------------------------------------------------------------------
anychart.svg.SVGManager.prototype.setTransform = function (element, opt_x, opt_y, opt_rotation, opt_rotationX, opt_rotationY) {
    opt_x = opt_x || 0;
    opt_y = opt_y || 0;
    opt_rotation = opt_rotation || 0;
    opt_rotationX = opt_rotationX || 0;
    opt_rotationY = opt_rotationY || 0;

    element.setAttribute('transform', 'translate(' +
        Number(opt_x).toString() + ',' +
        Number(opt_y).toString() +
        ') rotate(' + opt_rotation + ' ' + opt_rotationX + ' ' + opt_rotationY + ')');
};
//------------------------------------------------------------------------------
//
//                          SVGSprite class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @see anychart.svg.SVGManager
 * @extends {goog.events.EventTarget}
 * @param {anychart.svg.SVGManager} svgManager anychart svg manager.
 */
anychart.svg.SVGSprite = function (svgManager) {
    goog.events.EventTarget.call(this);
    this.element_ = svgManager.createSVGElement('g');
    this.svgManager_ = svgManager;
    this.childrens_ = [];
};
goog.inherits(anychart.svg.SVGSprite, goog.events.EventTarget);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @param {String} value
 */
anychart.svg.SVGSprite.prototype.setId = function (value) {
    this.element_.setAttribute('id', value);
};
/**
 * @return {String}
 */
anychart.svg.SVGSprite.prototype.getId = function () {
    return this.element_.getAttribute('id');
};
/**
 * SVGManager variable.
 * @type {anychart.svg.SVGManager}
 * @private
 */
anychart.svg.SVGSprite.prototype.svgManager_ = null;

/**
 * Getter for SVGManager.
 * @return {anychart.svg.SVGManager} SVGManager.
 */
anychart.svg.SVGSprite.prototype.getSVGManager = function () {
    return this.svgManager_;
};

/**
 * SVGSprite svg element variable.
 * @type {SVGElement}
 * @private
 */
anychart.svg.SVGSprite.prototype.element_ = null;

/**
 * X coordinate.
 * @type {Number}
 * @private
 */
anychart.svg.SVGSprite.prototype.x_ = 0;

/**
 * Y coordinate.
 * @type {Number}
 * @private
 */
anychart.svg.SVGSprite.prototype.y_ = 0;
/**
 * Rotation angle.
 * @type {Number}
 * @private
 */
anychart.svg.SVGSprite.prototype.rotation_ = 0;
/**
 * Rotation x.
 * @type {Number}
 * @private
 */
anychart.svg.SVGSprite.prototype.rotationX_ = 0;
/**
 * Rotation y.
 * @type {Number}
 * @private
 */
anychart.svg.SVGSprite.prototype.rotationY_ = 0;

/**
 * Array of child svg nodes.
 * @type {Array.<SVGElement>}
 * @private
 */
anychart.svg.SVGSprite.prototype.childrens_ = null;

/**
 * Parent svg node.
 * @type {anychart.svg.SVGSprite}
 * @private
 */
anychart.svg.SVGSprite.prototype.parent_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.svg.SVGSprite.prototype.getParent = function () {
    return this.parent_;
};
/**
 * Indicate, is sprite visible.
 * @private
 * @type {Boolean}
 */
anychart.svg.SVGSprite.prototype.visible_ = null;
/**
 * @return {Boolean}
 */
anychart.svg.SVGSprite.prototype.isVisible = function () {
    return this.visible_;
};
//------------------------------------------------------------------------------
//                          SVG elements workflow
//------------------------------------------------------------------------------

/**
 * Adds child node to SVGSprite svg element.
 * @param {SVGElement} child Child node to append.
 */
anychart.svg.SVGSprite.prototype.appendChild = function (child) {
    if (child) this.element_.appendChild(child);
};

/**
 * Getter for SVGSprite svg element.
 * @return {SVGElement} Svg element.
 */
anychart.svg.SVGSprite.prototype.getElement = function () {
    return this.element_;
};

/**
 * Gets child node in the SVGSprite svg element by index.
 * @param {number} index child position in child nodes.
 * @return {SVGElement} Child node.
 */
anychart.svg.SVGSprite.prototype.getChildAt = function (index) {
    return this.element_.childNodes[index];
};

/**
 * Removes child node in the SVGSprite svg element by index.
 * @param {number} index child position in child nodes.
 */
anychart.svg.SVGSprite.prototype.removeChildAt = function (index) {
    this.element_.removeChild(this.getChildAt(index));
};
/**
 * Remove passed sprite from children.
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.svg.SVGSprite.prototype.removeChild = function (sprite) {
    if (!sprite || !sprite.getElement()) return;

    var childCount;
    childCount = this.childrens_.length;
    for (var i = 0; i < childCount; i++) {
        if (this.childrens_[i] == sprite)
            this.childrens_.splice(i, 1);
    }

    childCount = this.numChildren();
    for (i = 0; i < childCount; i++) {
        var child = this.getChildAt(i);
        if (child == sprite.getElement())
            this.removeChildAt(i);
    }
};
/**
 * Gets the count of child elements.
 * @return {number} Count of child elemets.
 */
anychart.svg.SVGSprite.prototype.numChildren = function () {
    return this.element_.childElementCount;
};

/**
 * Setter for x coordinate.
 * @param {number} value new value of x coordinate.
 */
anychart.svg.SVGSprite.prototype.setX = function (value) {
    this.x_ = value;
    this.updateTransform_();
};

/**
 * Getter for x coordinate.
 * @return {number} x coordinate.
 */
anychart.svg.SVGSprite.prototype.getX = function () {
    return this.x_;
};
/**
 * Increments X.
 * @param {Number} value Increment value.
 */
anychart.svg.SVGSprite.prototype.incrementX = function (value) {
    this.x_ += value;
    this.updateTransform_();
};
/**
 * Decrement X.
 * @param {Number} value
 */
anychart.svg.SVGSprite.prototype.decrementX = function (value) {
    this.x_ -= value;
    this.updateTransform_();
};

/**
 * Setter for y coordinate.
 * @param {number} value new value of y coordinate.
 */
anychart.svg.SVGSprite.prototype.setY = function (value) {
    this.y_ = value;
    this.updateTransform_();
};

/**
 * Getter for y coordinate.
 * @return {number} y coordinate.
 */
anychart.svg.SVGSprite.prototype.getY = function () {
    return this.y_;
};
/**
 * Increments Y.
 * @param {Number} value Increment value.
 */
anychart.svg.SVGSprite.prototype.incrementY = function (value) {
    this.y_ += value;
    this.updateTransform_();
};
/**
 * Decrement Y.
 * @param {Number} value
 */
anychart.svg.SVGSprite.prototype.decrementY = function (value) {
    this.y_ -= value;
    this.updateTransform_();
};

/**
 * Setter for rotation.
 * @param {number} angle new value of rotation.
 */
anychart.svg.SVGSprite.prototype.setRotation = function (angle, rotationX, rotationY) {
    this.rotation_ = angle;
    if (rotationX != undefined) this.rotationX_ = rotationX;
    if (rotationY != undefined) this.rotationY_ = rotationY;

    this.updateTransform_();
};

/**
 * Getter for rotation.
 * @return {number} rotation.
 */
anychart.svg.SVGSprite.prototype.getRotation = function () {
    return this.rotation_;
};

/**
 * Set the position.
 * @param {number} x new value of x coordinate.
 * @param {number} y new value of y coordinate.
 */
anychart.svg.SVGSprite.prototype.setPosition = function (x, y) {
    this.x_ = x;
    this.y_ = y;
    this.updateTransform_();
};

/**
 * Set the 'transform' svg attribute to SVGSprite svg element using x, y
 * coordinates and rotation.
 * @private
 */
anychart.svg.SVGSprite.prototype.updateTransform_ = function () {
    this.element_.setAttribute('transform',
        'translate(' +
            Number(this.x_).toString() + ',' +
            Number(this.y_).toString() +
            ') rotate(' +
            Number(this.rotation_) + ' ' +
            Number(this.rotationX_).toString() + ' ' +
            Number(this.rotationY_).toString() + ')');
};
/**
 * Sprite visibility setter.
 * @param {Boolean} value
 */
anychart.svg.SVGSprite.prototype.setVisibility = function (value) {
    this.visible_ = value;
    if (value === true)
        this.element_.setAttribute('visibility', 'visible');
    else
        this.element_.setAttribute('visibility', 'hidden');
};

/**
 * Removes childs from SVGSprite svg element.
 */
anychart.svg.SVGSprite.prototype.clear = function () {
    this.svgManager_.clearGroup(this.element_);
};

//------------------------------------------------------------------------------
//                          SVG Sprites workflow
//------------------------------------------------------------------------------

/**
 * Adds child to SVGSprite svg element.
 * @see anychart.svg.SVGSprite
 * @param {anychart.svg.SVGSprite} sprite Anychart svg sprite.
 */
anychart.svg.SVGSprite.prototype.appendSprite = function (sprite) {
    this.childrens_.push(sprite);
    this.element_.appendChild(sprite.getElement());
    sprite.parent_ = this;
};

/**
 * @inheritDoc
 * @see anychart.utils.geom.Point
 * @return {anychart.utils.geom.Point} (x,y) coordinates in Point object.
 */
anychart.svg.SVGSprite.prototype.getClientPosition = function () {
    if (this.parent_) {
        var parentPos = this.parent_.getClientPosition();
        return new anychart.utils.geom.Point(
            this.parent_.getX() + parentPos.x,
            this.parent_.getY() + parentPos.y);
    }
    return new anychart.utils.geom.Point(0, 0);
};
//------------------------------------------------------------------------------
//                  Clip path.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.svg.SVGSprite.prototype.clipPathCircut_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.svg.SVGSprite.prototype.initializeClipPath = function (bounds) {
    if (!bounds) return;
    var clipPAth = this.svgManager_.createClipPath();
    this.clipPathCircut_ = this.svgManager_.createPath();
    clipPAth.appendChild(this.clipPathCircut_);
    this.appendChild(clipPAth);

    this.element_.setAttribute('clip-path', 'url(#' + clipPAth.getAttribute('id') + ')');
    this.element_.setAttribute('clipPathUnits', 'userSpaceOnUse');
};

/**
 * Update sprite clip path.
 * @param {anychart.utils.geom.Rectangle} bounds new clip path bounds.
 */
anychart.svg.SVGSprite.prototype.updateClipPath = function (bounds) {
    if (!bounds) return;
    if (!this.clipPathCircut_) this.initializeClipPath(bounds);
    this.clipPathCircut_.setAttribute('d', this.svgManager_.pRectByBounds(bounds));
};
//------------------------------------------------------------------------------
//
//                      SVGScrollableSprite class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 */
anychart.svg.SVGScrollableSprite = function (svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
    this.scrollabelSprite_ = new anychart.svg.SVGSprite(svgManager);
    this.childrens_.push(this.scrollabelSprite_);
    this.element_.appendChild(this.scrollabelSprite_.getElement());
    this.scrollabelSprite_.parent_ = this;
};
goog.inherits(anychart.svg.SVGScrollableSprite,
    anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------

/**
 * Current visible bounds.
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.svg.SVGScrollableSprite.prototype.visibleBounds_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.svg.SVGScrollableSprite.prototype.scrollabelSprite_ = null;
//------------------------------------------------------------------------------
//                       Override children.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.svg.SVGScrollableSprite.prototype.appendChild = function (child) {
    if (child) this.scrollabelSprite_.appendChild(child);
};
/**
 * @inheritDoc
 */
anychart.svg.SVGScrollableSprite.prototype.appendSprite = function (sprite) {
    if (sprite && sprite.getElement())
        this.scrollabelSprite_.appendSprite(sprite);
};
//------------------------------------------------------------------------------
//                            Scroll.
//------------------------------------------------------------------------------
anychart.svg.SVGScrollableSprite.prototype.scrollX = function (x) {
    if (x && !isNaN(x)) this.scrollabelSprite_.setX(x);
};
anychart.svg.SVGScrollableSprite.prototype.scrollY = function (y) {
    if (y && !isNaN(y)) this.scrollabelSprite_.setY(y);
};
anychart.svg.SVGScrollableSprite.prototype.scrollTo = function (x, y) {
    if (x && !isNaN(x) && y && !isNaN(y))
        this.scrollabelSprite_.setPosition(x, y);
};
//------------------------------------------------------------------------------
//
//                     SVGDraggableSprite class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 */
anychart.svg.SVGDraggableSprite = function (svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
};
goog.inherits(anychart.svg.SVGDraggableSprite, anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.svg.SVGDraggableSprite.prototype.dragBounds_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} value
 */
anychart.svg.SVGDraggableSprite.prototype.setDragBounds = function (value) {
    this.dragBounds_ = value.clone();
};
/**
 * @private
 * @type {Number}
 */
anychart.svg.SVGDraggableSprite.prototype.mouseOffsetX_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.svg.SVGDraggableSprite.prototype.mouseOffsetY_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 *
 * @param {anychart.utils.geom.Rectangle} opt_bounds
 */
anychart.svg.SVGDraggableSprite.prototype.initializeDrag = function (opt_bounds) {
    if (opt_bounds) this.dragBounds_ = opt_bounds.clone();
    if (this.dragBounds_)
        goog.events.listen(this.element_, goog.events.EventType.MOUSEDOWN, this.startDrag_, false, this);
};
//------------------------------------------------------------------------------
//                            Drag.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.svg.SVGDraggableSprite.prototype.moveElement_ = function (event) {
    //new position
    var clientPosition = this.getClientPosition();
    var x = event.offsetX - clientPosition.x - this.mouseOffsetX_;
    var y = event.offsetY - clientPosition.y - this.mouseOffsetY_;

    //check drag rect
    var bBox = this.element_['getBBox']();
    if (x < this.dragBounds_.getLeft())
        x = this.dragBounds_.getLeft();
    else if (x + bBox.width > this.dragBounds_.getRight())
        x = this.dragBounds_.getRight() - bBox.width;

    if (y < this.dragBounds_.getTop())
        y = this.dragBounds_.getTop();
    else if (y + bBox.height > this.dragBounds_.getBottom())
        y = this.dragBounds_.getBottom() - bBox.height;

    this.setPosition(x, y);
    this.checkOnMouseOutPosition(event);
    goog.events.dispatchEvent(this, new anychart.events.DragEvent(anychart.events.DragEvent.ON_DRAG_PROCESS));
};
/**
 * Stop drag if mouse is out svg bounds.
 * @param {goog.events.Event} event
 */
anychart.svg.SVGDraggableSprite.prototype.checkOnMouseOutPosition = function (event) {
    if (!this.svgManager_.getSVGBounds().hitTest(event.clientX, event.clientY))
        this.stopDrag_(event)
};
//------------------------------------------------------------------------------
//                          Handlers.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.svg.SVGDraggableSprite.prototype.startDrag_ = function (event) {
    var clientPosition = this.getClientPosition();
    this.mouseOffsetX_ = event.offsetX - (clientPosition.x + this.x_);
    this.mouseOffsetY_ = event.offsetY - (clientPosition.y + this.y_);
    goog.events.listen(this.svgManager_.getSVG(), goog.events.EventType.MOUSEMOVE, this.moveElement_, false, this);
    goog.events.listen(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.stopDrag_, false, this);
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.svg.SVGDraggableSprite.prototype.stopDrag_ = function (event) {
    goog.events.unlisten(this.svgManager_.getSVG(), goog.events.EventType.MOUSEMOVE, this.moveElement_, false, this);
    goog.events.unlisten(this.svgManager_.getSVG(), goog.events.EventType.MOUSEUP, this.stopDrag_, false, this);
};

