/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.styles.StyleShape}</li>
 *  <li>@class {anychart.styles.StyleShapes}</li>
 *  <li>@class {anychart.styles.StateType}</li>
 *  <li>@class {anychart.styles.StyleState}</li>
 *  <li>@class {anychart.styles.Style}</li>
 *  <li>@class {anychart.styles.StylesList}</li>
 * <ul>
 */
goog.provide('anychart.styles');
goog.require('anychart.utils');
goog.require('anychart.visual.fill');
goog.require('anychart.visual.stroke');
//------------------------------------------------------------------------------
//
//                          StyleShape.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.styles.StyleShape = function () {
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.styles.StyleShape.prototype.point_ = null;
/**
 * @private
 * @type {SVGElement}
 */
anychart.styles.StyleShape.prototype.element_ = null;
/**
 * @return {SVGElement}
 */
anychart.styles.StyleShape.prototype.getElement = function () {
    return this.element_;
};
/**
 * @private
 * @type {anychart.styles.StyleShapes}
 */
anychart.styles.StyleShape.prototype.styleShapes_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 * @param {anychart.styles.StyleShapes} styleShapes
 * @param {Function=} opt_drawingMethod
 */
anychart.styles.StyleShape.prototype.initialize = function (point, styleShapes, opt_drawingMethod) {
    this.point_ = point;
    this.styleShapes_ = styleShapes;
    if (opt_drawingMethod) this.element_ = opt_drawingMethod.call(styleShapes);
    else this.element_ = this.createElement();
};
/**
 * Create shape element.
 * @return {SVGElement}
 */
anychart.styles.StyleShape.prototype.createElement = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Update shape.
 * @param {anychart.styles.StyleState} opt_styleState Current style state,
 * if style state not passed, shape will not be updated.
 * @param {Boolean} opt_updateData Indicates, need update element path data.
 */
anychart.styles.StyleShape.prototype.update = function (opt_styleState, opt_updateData) {
    if (opt_styleState) {
        this.updateStyle(opt_styleState);
        this.updateEffects(opt_styleState);
    }
    if (opt_updateData === true) this.updatePath();
};
/**
 * @protected
 * @param {anychart.styles.StyleState} styleState Style state to update.
 */
anychart.styles.StyleShape.prototype.updateStyle = function (styleState) {
    goog.abstractMethod();
};
/**
 * Update element path.
 * @protected
 */
anychart.styles.StyleShape.prototype.updatePath = function () {
    goog.abstractMethod();
};
/**
 * Update element effects.
 * @protected
 * @param {anychart.styles.StyleState} styleState Style state to update.
 */
anychart.styles.StyleShape.prototype.updateEffects = function (styleState) {

};
//------------------------------------------------------------------------------
//
//                          StrokeStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.StrokeStyleShape = function () {
    goog.base(this);
};
goog.inherits(anychart.styles.StrokeStyleShape, anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                  Update style.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 */
anychart.styles.StrokeStyleShape.prototype.updateStyle = function (styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.getPercentColor();
    var bounds = this.getBounds();

    if (this.needUpdateStroke(styleState)) {
        style += this.getStrokeSettings(styleState).getSVGStroke(svgManager, bounds, color);
        style += svgManager.getEmptySVGFill();
    } else style += svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};

/**
 * ND: Needs doc!
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.styles.StrokeStyleShape.prototype.getBounds = function() {
    return this.point_.getBounds();
};

/**
 * ND: Needs doc!
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.StrokeStyleShape.prototype.getPercentColor = function() {
    return this.point_.getColor().getColor();
};

/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.StrokeStyleShape.prototype.getStrokeSettings = goog.abstractMethod;

/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 * @return {Boolean}
 */
anychart.styles.StrokeStyleShape.prototype.needUpdateStroke = function(styleState) {
    return true;
};
//------------------------------------------------------------------------------
//
//                          FillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.FillStyleShape = function () {
    goog.base(this);
};
goog.inherits(anychart.styles.FillStyleShape, anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                  Update style.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 */
anychart.styles.FillStyleShape.prototype.updateStyle = function (styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.getPercentColor();
    var bounds = this.getBounds();

    if (this.needUpdateFill(styleState)) {
        style += this.getFillSettings(styleState).getSVGFill(svgManager, bounds, color);
        style += svgManager.getEmptySVGStroke();
    } else style += svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};

/**
 * ND: Needs doc!
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.styles.FillStyleShape.prototype.getBounds = function() {
    return this.point_.getBounds();
};

/**
 * ND: Needs doc!
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.FillStyleShape.prototype.getPercentColor = function() {
    return this.point_.getColor().getColor();
};

/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.FillStyleShape.prototype.getFillSettings = goog.abstractMethod;

/**
 * ND: Needs doc!
 * @param {anychart.styles.StyleState} styleState
 * @return {Boolean}
 */
anychart.styles.FillStyleShape.prototype.needUpdateFill = function(styleState) {
    return true;
};
//------------------------------------------------------------------------------
//
//                         BaseFillShape class
//
//------------------------------------------------------------------------------

/**
 * Class for base fill shape.
 * todo:УБИТЬ это говнище
 * @constructor
 * @deprecated
 * @extends {anychart.styles.StyleShape}
 *
 */
anychart.styles.BaseFillShape = function () {
    goog.base(this);
};
goog.inherits(anychart.styles.BaseFillShape, anychart.styles.StyleShape);

/**
 * ND: Needs doc!
 * @param {Object} styleContainer
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.BaseFillShape.prototype.getFill = goog.abstractMethod;

/**
 * ND: Needs doc!
 * @param {Object} styleContainer
 * @return {Boolean}
 */
anychart.styles.BaseFillShape.prototype.needUpdateFill = goog.abstractMethod;
/**
 * Current style.
 * @private
 * @type {String}
 */
anychart.styles.BaseFillShape.prototype.currentStyle_ = null;
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.BaseFillShape.prototype.createElement = function () {
    return this.styleShapes_.createElement();
};
/**@inheritDoc*/
anychart.styles.BaseFillShape.prototype.updatePath = function () {
    this.styleShapes_.updatePath(this.element_);
};
anychart.styles.BaseFillShape.prototype.updateGradients = function (styleState) {
    var svgManager = this.point_.getSVGManager();

    if (this.currentStyle_) {
        var gm = svgManager.getGradientManager();
        gm.removeGradient(this.currentStyle_);
        this.currentStyle_ = '';
    }

    this.updateStyle(styleState);
};
/**@inheritDoc*/
anychart.styles.BaseFillShape.prototype.updateStyle = function (styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getPlot().getBounds();

    if (this.needUpdateFill(styleState)) {
        style += this.getFill(styleState).getSVGFill(svgManager, bounds, color, true);
        style += svgManager.getEmptySVGStroke();
        this.currentStyle_ = style;
    } else style = svgManager.getEmptySVGStyle();


    this.element_.setAttribute('style', style);
};
/** @inheritDoc */
anychart.styles.BaseFillShape.prototype.updateEffects = function () {
    return;
};

//------------------------------------------------------------------------------
//
//                          StyleShapes.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.styles.StyleShapes = function () {
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.styles.StyleShapes.prototype.point_ = null;
/**
 * Create shapes and add to container.
 * @param {anychart.plots.seriesPlot.data.BasePoint}
    */
anychart.styles.StyleShapes.prototype.initialize = function (point) {
    this.point_ = point;
    this.createShapes();
    this.placeShapes();
};
/**
 * Create style shapes.
 * @protected
 */
anychart.styles.StyleShapes.prototype.createShapes = goog.abstractMethod;
/**
 * Add shapes sprites to container.
 * @protected
 */
anychart.styles.StyleShapes.prototype.placeShapes = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Update all shapes.
 * @param {anychart.styles.StyleState} opt_styleState Current style state,
 * if current style state not passed, shapes style will not be updated.
 * @param {Boolean} opt_updateData Indicates, need update element path data.
 */
anychart.styles.StyleShapes.prototype.update = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                          StateType class
//
//------------------------------------------------------------------------------
/**
 * Describe possible styles state types.
 * @enum {int}
 */
anychart.styles.StateType = {
    NORMAL:0,
    HOVER:1,
    PUSHED:2,
    SELECTED_NORMAL:3,
    SELECTED_HOVER:4,
    MISSING:5
};
//------------------------------------------------------------------------------
//
//                          StyleState class
//
//------------------------------------------------------------------------------
/**
 * Class represent
 * @public
 * @constructor
 * @param {anychart.styles.Style} style Style for this state.
 * @param {?anychart.styles.StateType} opt_stateType State type, default is NORMAL.
 * <p>
 * Usage sample:
 *  <code>
 *      var styleState = new anychart.styles.StyleState(style, stateType);
 *      styleState.deserialize(data);
 *  </code>
 * </p>
 */
anychart.styles.StyleState = function (style, opt_stateType) {
    this.style_ = style;
    if (opt_stateType != undefined) this.stateType_ = opt_stateType;
};
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
/**
 * Link to style.
 * @private
 * @type {anychart.styles.Style}
 */
anychart.styles.StyleState.prototype.style_ = null;
anychart.styles.StyleState.prototype.getStyle = function () {
    return this.style_;
};
/**
 * State type, default NORMAL.
 * @private
 * @type {anychart.styles.StateType}
 */
anychart.styles.StyleState.prototype.stateType_ = 0;
anychart.styles.StyleState.prototype.getStateType = function () {
    return this.stateType_;
};
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
/**
 * Replace '%color' to style color and return data with replaces.
 * @public
 * @param {Object} data JSON object with styles or array with styles.
 * @return {Object}
 */
anychart.styles.StyleState.prototype.deserialize = function (data) {
    if (!data) return null;
    //aliases
    var des = anychart.utils.deserialization;
    data = des.copy(data);
    if (des.hasProp(data, 'color')) {
        var color = des.getProp(data, 'color');
        for (var i in data) {
            if (des.hasProp(data[i], 'color')) {
                var percentColor = des.getLStringProp(data[i], 'color');
                des.setProp(data[i],
                    'color',
                    percentColor.split('%color').join(color));
            }

        }
    }
    return data;
};
//------------------------------------------------------------------------------
//                         Utils
//------------------------------------------------------------------------------
/**
 * Copy style state object.
 * @param {*} opt_target Target to copy.
 * @return {anychart.styles.StyleState}
 */
anychart.styles.StyleState.prototype.copy = function (opt_target) {
    if (!opt_target) throw new Error('Unknown target');
    opt_target.style_ = this.style_;
    opt_target.stateType_ = this.stateType_;
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          StyleStateWithEffects class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.styles.StyleStateWithEffects = function (style, opt_stateType) {
    anychart.styles.StyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.styles.StyleStateWithEffects, anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.effects.EffectsList}
 */
anychart.styles.StyleStateWithEffects.prototype.effects_ = null;
/**
 * @return {anychart.visual.effects.EffectsList}
 */
anychart.styles.StyleStateWithEffects.prototype.getEffects = function () {
    return this.effects_;
};
/**
 * @return {Boolean}
 */
anychart.styles.StyleStateWithEffects.prototype.needUpdateEffects = function () {
    return Boolean(this.effects_ && this.effects_.isEnabled());
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.styles.StyleStateWithEffects.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);

    if (!data) return null;

    var des = anychart.utils.deserialization;

    if (des.isEnabledProp(data, 'effects')) {
        this.effects_ = new anychart.visual.effects.EffectsList();
        this.effects_.deserialize(des.getProp(data, 'effects'));
    }

    return data;
};
//------------------------------------------------------------------------------
//
//                          Style class
//
//------------------------------------------------------------------------------
/**
 * Class represent element style.
 * @public
 * @constructor
 * <p>
 *  Usage sample:
 *  <code>
 *      var style = new anychart.styles.Style(stateClass);
 *      style.deserialize(data);
 *  </code>
 * </p>
 */
anychart.styles.Style = function () {
    var styleStateClass = this.getStyleStateClass();
    this.normal_ = new styleStateClass(
        this,
        anychart.styles.StateType.NORMAL);

    this.hover_ = new styleStateClass(
        this,
        anychart.styles.StateType.HOVER);

    this.pushed_ = new styleStateClass(
        this,
        anychart.styles.StateType.PUSHED);

    this.selectedNormal_ = new styleStateClass(
        this,
        anychart.styles.StateType.SELECTED_NORMAL);

    this.selectedHover_ = new styleStateClass(
        this,
        anychart.styles.StateType.SELECTED_HOVER);

    this.missing_ = new styleStateClass(
        this,
        anychart.styles.StateType.MISSING);
};
//------------------------------------------------------------------------------
//		                    Style state
//------------------------------------------------------------------------------
/**
 * Style state class.
 * @private
 * @return {Function(style, opt_stateType)}
 */
anychart.styles.Style.prototype.getStyleStateClass = goog.abstractMethod;
//------------------------------------------------------------------------------
//		                Properties
//------------------------------------------------------------------------------
/**
 * Normal style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.normal_ = null;
anychart.styles.Style.prototype.getNormal = function () {
    return this.normal_;
};
anychart.styles.Style.prototype.setNormal = function (value) {
    this.normal_ = value;
};

/**
 * Hover style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.hover_ = null;
anychart.styles.Style.prototype.getHover = function () {
    return this.hover_;
};
anychart.styles.Style.prototype.setHover = function (value) {
    this.hover_ = value;
};


/**
 * Pushed style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.pushed_ = null;
anychart.styles.Style.prototype.getPushed = function () {
    return this.pushed_;
};
anychart.styles.Style.prototype.setPushed = function (value) {
    this.pushed_ = value;
};

/**
 * Selected normal style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.selectedNormal_ = null;
anychart.styles.Style.prototype.getSelectedNormal = function () {
    return this.selectedNormal_;
};
anychart.styles.Style.prototype.setSelectedNormal = function (value) {
    this.selectedNormal_ = value;
};

/**
 * Selected hover style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.selectedHover_ = null;
anychart.styles.Style.prototype.getSelectedHover = function () {
    return this.selectedHover_;
};
anychart.styles.Style.prototype.setSelectedHover = function (value) {
    this.selectedHover_ = value;
};

/**
 * Missing style state.
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.styles.Style.prototype.missing_ = null;
anychart.styles.Style.prototype.getMissing = function () {
    return this.missing_;
};
anychart.styles.Style.prototype.setMissing = function (value) {
    this.missing_ = value;
};
//------------------------------------------------------------------------------
//		                Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize style settings
 * @public
 * @param {Object} data JSON object with styles.
 * <p>
 *  Usage sample:
 *  <code>
 *      var style = new anychart.styles.Style(stateClass);
 *      style.deserialize(data);
 *  </code>
 * </p>
 */
anychart.styles.Style.prototype.deserialize = function (data) {
    if (!data) return;
    //aliases
    var deserialization = anychart.utils.deserialization;
    data = deserialization.copy(data);
    if (!deserialization.hasProp(data, 'states')) {
        this.normal_.deserialize(data);
        this.hover_.deserialize(data);
        this.pushed_.deserialize(data);
        this.selectedNormal_.deserialize(data);
        this.selectedHover_.deserialize(data);
        this.missing_.deserialize(data);
    } else {
        var states = deserialization.getProp(data, 'states');
        this.normal_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'normal')));
        this.hover_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'hover')));
        this.pushed_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'pushed')));
        this.selectedNormal_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'selected_normal')));
        this.selectedHover_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'selected_hover')));
        this.missing_.deserialize(this.mergeState_(data, deserialization.getProp(states, 'missing')));
    }
};
/**
 * Merge style with state.
 * @param {object} style Merged style.
 * @param {object} state Merged state
 * @return {object}
 */
anychart.styles.Style.prototype.mergeState_ = function (style, state) {
    if (!state) return style;
    return anychart.utils.JSONUtils.merge(style, state, null, 'states');
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
/**
 * Create shapes.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 */
anychart.styles.Style.prototype.initialize = function (point) {
    var shapes = this.createStyleShapes(point);
    shapes.initialize(point);
    return shapes;
};
/**
 * @protected
 * @param {anychart.plots.seriesPlot.data.BasePoint}
    * @return {anychart.styles.StyleShapes}
 */
anychart.styles.Style.prototype.createStyleShapes = goog.abstractMethod;
//------------------------------------------------------------------------------
//		                Utils
//------------------------------------------------------------------------------
/**
 * Create style copy.
 * @public
 * @param {*} opt_target Target to copy.
 * @return {anychart.styles.Style} */
anychart.styles.Style.prototype.copy = function (opt_target) {
    if (!opt_target) opt_target = new anychart.styles.Style(this.stateClass_);
    opt_target.setNormal(this.normal_.copy());
    opt_target.setHover(this.hover_.copy());
    opt_target.setPushed(this.pushed_.copy());
    opt_target.setSelectedNormal(this.selectedNormal_.copy());
    opt_target.setSelectedHover(this.selectedHover_.copy());
    opt_target.setMissing(this.missing_.copy());
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          StylesList class
//
//------------------------------------------------------------------------------
/**
 * Class represent Styles list.
 * <p>
 *  Description: Styles list store 'styles' node and provide methods to get and merge styles from 'styles' node.
 * </p>
 * @public
 * @constructor
 * @param {object} styles Styles list.
 * <p>
 *  Usage sample:
 *  <code>
 *      var styleList = new anychart.styles.StylesList(stylesNode);
 *      var style = stylesList.getNodeByMerge(nodeName, nodeParentName);
 *      stylesList.addStyleToCache(nodeName, styleName, style);
 *      alert(style == stylesList.getStyleFromCache(nodeName, styleName)); //alert true
 *  </code>
 * </p>
 */
anychart.styles.StylesList = function (styles) {
    this.stylesList_ = styles;
    this.internalCache_ = {};
    this.cachedStyles_ = {};
};
//------------------------------------------------------------------------------
//                          Styles storage.
//------------------------------------------------------------------------------
/**
 * Contains non merged styles ('styles' node).
 * @private
 * @type {object}
 */
anychart.styles.StylesList.prototype.stylesList_ = null;
anychart.styles.StylesList.prototype.getStylesList = function () {
    return this.stylesList_;
};


/**
 * Contains cached styles, keys is node name, style name.
 * @private
 * @type {object}
 */
anychart.styles.StylesList.prototype.cachedStyles_ = null;

/**
 * !!!!ATENTION!!!! В данный момент кеш архитектурно не юзаблелен, по всем вопросам к Роме
 * Add style to cache, keys is node name and style name.
 * @public
 * @param {string} nodeName Node name.
 * @param {string} styleName Style name.
 * @param {Object} style Style value.
 * <p>
 *  Usage sample:
 *  <code>
 *      var styleList = new anychart.styles.StylesList(stylesNode);
 *      var style = stylesList.getNodeByMerge(nodeName, nodeParentName);
 *      stylesList.addStyle(nodeName, styleName, style);
 *      alert(style == stylesList.getStyle(nodeName, styleName)); //alert true
 *  </code>
 * </p>
 */
anychart.styles.StylesList.prototype.addStyle = function (nodeName, styleName, style) {
    if (!this.cachedStyles_[nodeName]) this.cachedStyles_[nodeName] = {};
    this.cachedStyles_[nodeName][styleName] = style;
};

/**
 * * !!!!ATENTION!!!! В данный момент кеш архитектурно не юзаблелен, по всем вопросам к Роме
 * Return style from cache, keys is node name and style name.
 * @public
 * @param {string} nodeName Node name.
 * @param {string} styleName Style name.
 * @return {object}
 * <p>
 *  Usage sample:
 *  <code>
 *      var styleList = new anychart.styles.StylesList(stylesNode);
 *      var style = stylesList.getNodeByMerge(nodeName, nodeParentName);
 *      stylesList.addStyle(nodeName, styleName, style);
 *      alert(style == stylesList.getStyle(nodeName, styleName)); //alert true
 *  </code>
 * </p>
 */
anychart.styles.StylesList.prototype.getStyle = function (nodeName, styleName) {
    if (!this.cachedStyles_[nodeName]) return null;
    if (!this.cachedStyles_[nodeName][styleName]) return null;
    return this.cachedStyles_[nodeName][styleName];
};
//------------------------------------------------------------------------------
//                          Styles merging.
//------------------------------------------------------------------------------
/**
 * Merge and return style by node name.
 * @public
 * @param {string} nodeName Node name.
 * @param {object} opt_styleData Root style settings.
 * @param {string} opt_parentStyleName Style parent name.
 * @param {boolean=} opt_needBuildState Indicates, need rebuild style states, default is true.
 * @return {object}
 * <p>
 *  Usage sample:
 *  <code>
 *      var styleList = new anychart.styles.StylesList(stylesNode);
 *      var style = stylesList.getNodeByMerge(nodeName, nodeParentName);
 *      stylesList.addStyleToCache(nodeName, styleName, style);
 *      alert(style == stylesList.getStyle(nodeName, styleName)); //alert true
 *  </code>
 * </p>
 */
anychart.styles.StylesList.prototype.getStyleByMerge = function (nodeName, opt_styleData, opt_parentStyleName, opt_needBuildState) {
    //aliases
    var deserialization = anychart.utils.deserialization;
    //all style nodes with passed node name
    var nodeList = deserialization.getPropArray(this.stylesList_, nodeName);
    //merge sequence
    var mergeList = [];
    //parent node
    var parentNode;
    //root style
    if (opt_styleData) mergeList.push(opt_styleData);

    //search parent node
    if (opt_parentStyleName) {
        opt_parentStyleName = deserialization.getLString(opt_parentStyleName);
        parentNode = anychart.utils.JSONUtils.getNodeByName(nodeList, opt_parentStyleName);
    }

    //build merge list
    this.buildStylesParentTree_(mergeList, parentNode, nodeList);

    return this.buildStyle_(mergeList, nodeName, opt_needBuildState);
};

/**
 * Return merged style from cache if exist, else return null.
 * @public
 * @param nodeName
 * @param styleName
 * @return {object}
 */
anychart.styles.StylesList.prototype.getStyleByApplicator = function (nodeName, styleName) {
    if (!nodeName || this.getStyle(nodeName, styleName)) return null;

    var des = anychart.utils.deserialization;
    nodeName = des.getLString(nodeName);
    styleName = des.getLString(styleName);

    var nodeList = des.getPropArray(this.stylesList_, nodeName);
    var applicatorStyle = anychart.utils.JSONUtils.getNodeByName(nodeList, styleName);
    var mergeList = [];

    this.buildStylesParentTree_(mergeList, applicatorStyle, nodeList);
    return this.buildStyle_(mergeList, nodeName);
};

/**
 * @private
 */
anychart.styles.StylesList.prototype.buildStylesParentTree_ = function (mergeList, startNode, nodeList) {
    if (startNode) {
        var des = anychart.utils.deserialization;
        mergeList.push(des.copy(startNode));
        while (des.getStringProp(startNode, 'name') != 'anychart_default') {
            if (!des.hasProp(startNode, 'parent') || !anychart.utils.JSONUtils.getNodeByName(nodeList, des.getStringProp(startNode, 'parent'))) {
                startNode = anychart.utils.JSONUtils.getNodeByName(nodeList, 'anychart_default');
            } else {
                startNode = anychart.utils.JSONUtils.getNodeByName(nodeList, des.getProp(startNode, 'parent'));
            }
            mergeList.push(des.copy(startNode));
        }
    } else mergeList.push(anychart.utils.JSONUtils.getNodeByName(nodeList, 'anychart_default'));
};

/**
 * Build styles.
 * @private
 * @param {Array.<object>} mergeList Merge list.
 * @param {boolean} opt_needBuildState Indicates, need build style states, default is true.
 * @return {object}
 */
anychart.styles.StylesList.prototype.buildStyle_ = function (mergeList, opt_nodeName, opt_needBuildState) {
    //aliases
    var deserialization = anychart.utils.deserialization;
    //resources
    var i;
    var stylesCount = mergeList.length;
    //rebuild states
    //set default value
    if (opt_needBuildState == undefined || opt_needBuildState == null) opt_needBuildState = true;
    if (opt_needBuildState)
        for (i = 0; i < mergeList.length; i++) mergeList[i] = this.buildState_(mergeList[i], opt_nodeName);

    var buildResult = mergeList[stylesCount - 1];

    buildResult['inherits'] = deserialization.hasProp(buildResult, 'name') ? deserialization.getLString(deserialization.getProp(buildResult, 'name')) : '';
    for (i = stylesCount - 1; i > 0; i--) {
        buildResult = anychart.utils.JSONUtils.merge(buildResult, mergeList[i - 1]);
        if (deserialization.hasProp(mergeList[i - 1], 'name'))
            buildResult['inherits'] += ',' + deserialization.getLString(deserialization.getProp(mergeList[i - 1], 'name'));
    }
    return buildResult;
};

/**
 * Create new style with states.
 * @private
 * @return {object}
 */
anychart.styles.StylesList.prototype.buildState_ = function (styleData, opt_nodeName) {
    var styleName = opt_nodeName ? opt_nodeName : anychart.utils.deserialization.getStringProp(styleData, '#name#');

    //ANYCHART 4 BUG REPRODUCING
    if (styleName && anychart.styles.StylesList.IGNORED_.indexOf(styleName) != -1) return styleData;
    //build states
    var result = {};
    if (styleName) result['#name#'] = styleName;
    this.createStates_(result);
    this.addState_(result['states'], 'normal');
    this.addState_(result['states'], 'hover');
    this.addState_(result['states'], 'pushed');
    this.addState_(result['states'], 'selected_normal');
    this.addState_(result['states'], 'selected_hover');
    this.addState_(result['states'], 'missing');

    //merge attributes
    anychart.utils.JSONUtils.mergeSimpleProperty(styleData, result, result);

    //merge states
    result['states']['normal'] = this.mergeState_(styleData, 'normal');
    result['states']['hover'] = this.mergeState_(styleData, 'hover');
    result['states']['pushed'] = this.mergeState_(styleData, 'pushed');
    result['states']['selected_normal'] = this.mergeState_(styleData, 'selected_normal');
    result['states']['selected_hover'] = this.mergeState_(styleData, 'selected_hover');
    result['states']['missing'] = this.mergeState_(styleData, 'missing');
    return result;
};

/**
 *
 * @private
 * @param styleData {Object} Объект со стейтами.
 * @param {String} stateName Имя стейта.
 */
anychart.styles.StylesList.prototype.mergeState_ = function (styleData, stateName) {
    if (!styleData['states']) styleData['states'] = {};
    if (!styleData['states'][stateName]) styleData['states'][stateName] = {};
    return anychart.utils.JSONUtils.merge(styleData, styleData['states'][stateName], stateName, 'states');
};

/**
 * Create empty states node in passed object.
 * @private
 * @param {object} style Style object.
 */
anychart.styles.StylesList.prototype.createStates_ = function (style) {
    style['states'] = {'#name#':'states', '#children#':[]};
};

/**
 * Add in passed states object, empty state object.
 * @private
 * @param {object} states Target state object.
 * @param {string} stateName State name.
 */
anychart.styles.StylesList.prototype.addState_ = function (states, stateName) {
    states[stateName] = {'#name#':stateName, '#children#':[]};
};

//------------------------------------------------------------------------------
//					ANYCHART 4 BUG REPRODUCING
//------------------------------------------------------------------------------
/**
 * Ignored styles.
 * @private
 * @static
 * @const
 * @type {Array.<string>}
 */
anychart.styles.StylesList.IGNORED_ = [
    'animation_style', 'animation', 'line_axis_marker_style', 'range_axis_marker_style',
    'custom_label_style', 'trendline_style', 'color_range_style', 'tooltip_style'
];
//------------------------------------------------------------------------------
//                  Clear.
//------------------------------------------------------------------------------
anychart.styles.StylesList.prototype.clear = function () {
    this.stylesList_ = {};
    this.internalCache_ = {};
    this.cachedStyles_ = {};
};
