/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.styles.background.StrokeStyleShape};</li>
 *  <li>@class {anychart.styles.background.FillStyleShape};</li>
 *  <li>@class {anychart.styles.background.FillWithStrokeStyleShape};</li>
 *  <li>@class {anychart.styles.background.HatchFillStyleShape};</li>
 *  <li>@class {anychart.styles.background.BackgroundStyleShapes};</li>
 *  <li>@class {anychart.styles.background.BackgroundStyleState};</li>
 *  <li>@class {anychart.styles.background.BackgroundStyle}.</li>
 * <ul>
 */
goog.provide('anychart.styles.background');
goog.require('anychart.styles');
//------------------------------------------------------------------------------
//
//                    StrokeStyleShape class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.background.StrokeStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.styles.background.StrokeStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.StrokeStyleShape.prototype.createElement = function() {
    return this.styleShapes_.createElement();
};
/**@inheritDoc*/
anychart.styles.background.StrokeStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updatePath(this.element_);
};
/**@inheritDoc*/
anychart.styles.background.StrokeStyleShape.prototype.updateStyle = function(styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getBounds();

    if (styleState.needUpdateBorder()) {
        style += styleState.getBorder().getSVGStroke(svgManager, bounds, color);
        style += svgManager.getEmptySVGFill();
    } else style += svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
/**
 * @inheritDoc
 */
anychart.styles.background.StrokeStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                       FillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.background.FillStyleShape = function() {
    goog.base(this);
};
goog.inherits(anychart.styles.background.FillStyleShape,
        anychart.styles.BaseFillShape);

/** @inheritDoc */
anychart.styles.background.FillStyleShape.prototype.getFill = function(styleContainer) {
    return styleContainer.getFill();
};

/** @inheritDoc */
anychart.styles.background.FillStyleShape.prototype.needUpdateFill = function(styleContainer) {
    return styleContainer.needUpdateFill();
};
/**
 * @inheritDoc
 */
anychart.styles.background.FillStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                    FillWithStrokeStyleShape class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.background.FillWithStrokeStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.styles.background.FillWithStrokeStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.FillWithStrokeStyleShape.prototype.createElement = function() {
    return this.styleShapes_.createElement();
};
/**@inheritDoc*/
anychart.styles.background.FillWithStrokeStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updatePath(this.element_);
};
/**@inheritDoc*/
anychart.styles.background.FillWithStrokeStyleShape.prototype.updateStyle = function(styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getBounds();

    if (styleState.needUpdateFillShape()) {
        //fill
        if (styleState.needUpdateFill())
            style += styleState.getFill().getSVGFill(svgManager, bounds, color);
        else
            style += svgManager.getEmptySVGFill();

        //stroke
        if (styleState.needUpdateBorder())
            style += styleState.getBorder().getSVGStroke(svgManager, bounds, color);
        else
            style += svgManager.getEmptySVGStroke();
    } else style += svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
/**
 * @inheritDoc
 */
anychart.styles.background.FillWithStrokeStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                    HatchFillStyleShape class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.background.HatchFillStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.styles.background.HatchFillStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.HatchFillStyleShape.prototype.createElement = function() {
    return this.styleShapes_.createElement();
};
/**@inheritDoc*/
anychart.styles.background.HatchFillStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updatePath(this.element_);
};
/**@inheritDoc*/
anychart.styles.background.HatchFillStyleShape.prototype.updateStyle = function(styleState) {
    var style;

    if (styleState.needUpdateHatchFill()) {
        style = styleState.getHatchFill().getSVGHatchFill(
                this.point_.getSVGManager(),
                this.point_.getHatchType(),
                this.point_.getColor().getColor());
    } else style = this.point_.getSVGManager().getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
/**
 * @inheritDoc
 */
anychart.styles.background.HatchFillStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                    BackgroundStyleShapes class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 */
anychart.styles.background.BackgroundStyleShapes = function() {
    anychart.styles.StyleShapes.call(this);
};
goog.inherits(anychart.styles.background.BackgroundStyleShapes,
        anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.background.FillWithStrokeStyleShape}
 */
anychart.styles.background.BackgroundStyleShapes.prototype.fillShape_ = null;
/**
 * @private
 * @type {anychart.styles.background.HatchFillStyleShape}
 */
anychart.styles.background.BackgroundStyleShapes.prototype.hatchFillShape_ = null;
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.BackgroundStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.styles.background.FillWithStrokeStyleShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.styles.background.HatchFillStyleShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
/**@inheritDoc*/
anychart.styles.background.BackgroundStyleShapes.prototype.placeShapes = function() {
    var sprite = this.getSprite();
    if (this.fillShape_) sprite.appendChild(this.fillShape_.getElement());
    if (this.hatchFillShape_) sprite.appendChild(this.hatchFillShape_.getElement());
};
/**
 * Return sprite to place shapes
 * @protected
 * @return {anychart.svg.SVGSprite}
 */
anychart.styles.background.BackgroundStyleShapes.prototype.getSprite = function() {
    return this.point_.getSprite()
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.BackgroundStyleShapes.prototype.createElement = goog.abstractMethod;
/**@inheritDoc*/
anychart.styles.background.BackgroundStyleShapes.prototype.updatePath = goog.abstractMethod;

/**@inheritDoc*/
anychart.styles.background.BackgroundStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if (this.fillShape_) this.fillShape_.update(opt_styleState, opt_updateData);
    if (this.hatchFillShape_) this.hatchFillShape_.update(opt_styleState, opt_updateData);
};
/**
 * Update element effects
 * @param {SVGElement} element
 * @param {anychart.styles.StyleStateWithEffects} styleState
 */
anychart.styles.background.BackgroundStyleShapes.prototype.updateEffects = function(element, styleState) {
    if (styleState.needUpdateEffects())
        element.setAttribute('filter', styleState.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};
//------------------------------------------------------------------------------
//
//                    BackgroundStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.styles.Style} style
 * @param {anychart.styles.StateType} opt_stateType
 */
anychart.styles.background.BackgroundStyleState = function(style, opt_stateType) {
    anychart.styles.StyleStateWithEffects.call(this, style, opt_stateType);
};
goog.inherits(anychart.styles.background.BackgroundStyleState, anychart.styles.StyleStateWithEffects);
//------------------------------------------------------------------------------
//                         Fill properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.styles.background.BackgroundStyleState.prototype.fill_ = null;
/**
 * @return {anychart.visual.fill.Fill}
 */
anychart.styles.background.BackgroundStyleState.prototype.getFill = function() {
    return this.fill_;
};
//------------------------------------------------------------------------------
//                         Border properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.styles.background.BackgroundStyleState.prototype.border_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.styles.background.BackgroundStyleState.prototype.getBorder = function() {
    return this.border_;
};
//------------------------------------------------------------------------------
//                         Hatch fill properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.styles.background.BackgroundStyleState.prototype.hatchFill_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.styles.background.BackgroundStyleState.prototype.getHatchFill = function() {
    return this.hatchFill_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
anychart.styles.background.BackgroundStyleState.prototype.deserialize = function(config) {
    config = goog.base(this, 'deserialize', config);
    if (anychart.utils.deserialization.hasProp(config, "fill")) {
        this.fill_ = new anychart.visual.fill.Fill();
        this.fill_.deserialize(anychart.utils.deserialization.getProp(config, "fill"));
    }
    this.deserializeBorder(config);
    if (anychart.utils.deserialization.hasProp(config, "hatch_fill")) {
        this.hatchFill_ = new anychart.visual.hatchFill.HatchFill();
        this.hatchFill_.deserialize(anychart.utils.deserialization.getProp(config, "hatch_fill"))
    }
    return config;
};
/**@protected*/
anychart.styles.background.BackgroundStyleState.prototype.deserializeBorder = function(config) {
    if (anychart.utils.deserialization.hasProp(config, "border")) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(anychart.utils.deserialization.getProp(config, "border"));
    }
};
//------------------------------------------------------------------------------
//                         Definers
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyleState.prototype.needUpdateFill = function() {
    return this.fill_ && this.fill_.isEnabled();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyleState.prototype.needUpdateBorder = function() {
    return this.border_ && this.border_.isEnabled();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyleState.prototype.needUpdateHatchFill = function() {
    return this.hatchFill_ && this.hatchFill_.isEnabled();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyleState.prototype.needUpdateFillShape = function() {
    return this.needUpdateFill() || this.needUpdateBorder();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyleState.prototype.hasGradient = function() {
    return (this.needUpdateFill() && this.fill_.isGradient()) ||
            (this.needUpdateBorder() && this.border_.isGradient());
};
//------------------------------------------------------------------------------
//
//                    BackgroundStyle class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.styles.background.BackgroundStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.styles.background.BackgroundStyle,
        anychart.styles.Style);
//------------------------------------------------------------------------------
//                          Style state.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.BackgroundStyle.prototype.getStyleStateClass = function() {
    return anychart.styles.background.BackgroundStyleState;
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.background.BackgroundStyle.prototype.createStyleShapes = function() {
    return new anychart.styles.background.BackgroundStyleShapes();
};
//------------------------------------------------------------------------------
//                         Definers
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyle.prototype.needCreateFillShape = function() {
    return  this.normal_.needUpdateFillShape() ||
            this.hover_.needUpdateFillShape() ||
            this.pushed_.needUpdateFillShape() ||
            this.selectedNormal_.needUpdateFillShape() ||
            this.selectedHover_.needUpdateFillShape() ||
            this.missing_.needUpdateFillShape();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyle.prototype.needCreateStrokeShape = function() {
    return  this.normal_.needUpdateBorder() ||
            this.hover_.needUpdateBorder() ||
            this.pushed_.needUpdateBorder() ||
            this.selectedNormal_.needUpdateBorder() ||
            this.selectedHover_.needUpdateBorder() ||
            this.missing_.needUpdateBorder();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyle.prototype.needCreateFillAndStrokeShape = function() {
    return  this.normal_.needUpdateFillShape() ||
            this.hover_.needUpdateFillShape() ||
            this.pushed_.needUpdateFillShape() ||
            this.selectedNormal_.needUpdateFillShape() ||
            this.selectedHover_.needUpdateFillShape() ||
            this.missing_.needUpdateFillShape();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyle.prototype.needCreateHatchFillShape = function() {
    return  this.normal_.needUpdateHatchFill() ||
            this.hover_.needUpdateHatchFill() ||
            this.pushed_.needUpdateHatchFill() ||
            this.selectedNormal_.needUpdateHatchFill() ||
            this.selectedHover_.needUpdateHatchFill() ||
            this.missing_.needUpdateHatchFill();
};
/**
 * @return {Boolean}
 */
anychart.styles.background.BackgroundStyle.prototype.hasGradient = function() {
    return  this.normal_.hasGradient() ||
            this.hover_.hasGradient() ||
            this.pushed_.hasGradient() ||
            this.selectedNormal_.hasGradient() ||
            this.selectedHover_.hasGradient() ||
            this.missing_.hasGradient();
};
