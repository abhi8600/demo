/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.indicator.styles');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.pointer.styles');
goog.require('anychart.plots.gaugePlot.gauges.circular.pointers.styles');
goog.require('anychart.plots.gaugePlot.gauges.linear.pointers.styles');
goog.require('anychart.layout');

/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
    this.hAlign = anychart.layout.HorizontalAlign.CENTER;
    this.vAlign = anychart.layout.VerticalAlign.CENTER;
    this.x = .5;
    this.y = .5;
};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);

anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState.prototype.x = null;
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState.prototype.y = null;
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState.prototype.hAlign = null;
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleState.prototype.vAlign = null;

/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};

anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes.prototype.updatePath = function (element) {
    this.point_.calculateBounds();
    var path = this.shapeDrawer_.getSVGPathData(
        this.point_.getCurrentState().getMarker().getShape(),
        this.point_.getSVGManager(),
        Math.min(this.point_.getBounds().width, this.point_.getBounds().height),
        this.point_.getBounds().x,
        this.point_.getBounds().y,
        this.point_.getBounds().width,
        this.point_.getBounds().height,
        this.point_.getCurrentState().needUpdateBorder() ? this.point_.getCurrentState().getBorder().getThickness() : 1
    );
    if (path == null) path = "M0,0 Z";
    element.setAttribute('d', path);
};
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes.prototype.getSprite = function () {
    return this.point_.getSprite();
};

/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle,
    anychart.styles.background.BackgroundStyle);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle.prototype.createStyleShapes = function (point) {
    return new anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyleShapes(point);
};