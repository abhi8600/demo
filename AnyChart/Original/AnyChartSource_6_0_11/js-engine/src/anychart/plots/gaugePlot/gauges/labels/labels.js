/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.labels.GaugeLabel}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.labels');

//------------------------------------------------------------------------------
//
//                          GaugeLabel.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 *
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel = function () {
    this.color_ = 0;
    this.underPoints_ = false;
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.color.Color}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.color_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.underPoints_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.sprite_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.gaugeBounds_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.style_ = null;
/**
 * ND: Needs doc!
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.underPoints = function () {
    return  this.underPoints_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.labelSprite_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.deserialize = function (data, styleList) {
    var des = anychart.utils.deserialization;

    //self properties
    if (des.hasProp(data, 'color'))
        this.color_ = des.getColorProp(data, 'color');

    if (des.hasProp(data, 'under_pointers')) {
        this.underPoints_ = des.getBoolProp(data, 'under_pointers');
    }

    des.deleteProp(data, 'color');

    //style
    var styleName = des.hasProp(data, 'style') ? des.getLStringProp(data, 'style') : 'anychart_default';
    var styleNodeName = 'label_style';
    var actualStyle = styleList.getStyleByMerge(styleNodeName, data, styleName);
    this.style_ = new anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle();
    this.style_.deserialize(actualStyle);
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} gaugeBounds
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.initialize = function (svgManager, gaugeBounds) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.gaugeBounds_ = gaugeBounds;
    var label = this.style_.getNormal().getLabel();
    label.initSize(svgManager, label.getText());
    this.labelSprite_ = label.createSVGText(svgManager);
    this.sprite_.appendSprite(this.labelSprite_);
    return this.sprite_
};
//------------------------------------------------------------------------------
//                     Drawing.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.draw = function () {
    if (!this.labelSprite_) return;
    var label = this.style_.getNormal().getLabel();

    label.update(
        this.labelSprite_.getSVGManager(),
        this.labelSprite_,
        this.gaugeBounds_.x,
        this.gaugeBounds_.y,
        this.gaugeBounds_.width,
        this.gaugeBounds_.height
    );
};
//------------------------------------------------------------------------------
//                      Resize.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.utils.geom.Rectangle}
    */
anychart.plots.gaugePlot.gauges.labels.GaugeLabel.prototype.resize = function (bounds) {
    if (!this.labelSprite_) return;
    var label = this.style_.getNormal().getLabel();
    this.gaugeBounds_ = bounds;
    label.update(
        this.labelSprite_.getSVGManager(),
        this.labelSprite_,
        this.gaugeBounds_.x,
        this.gaugeBounds_.y,
        this.gaugeBounds_.width,
        this.gaugeBounds_.height
    );
};
//------------------------------------------------------------------------------
//
//                          GaugeLabelStyleState.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleState}
 *
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState,
    anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState.prototype.label_ = null;
/**
 * ND: Needs doc!
 * @return {anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement}
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState.prototype.getLabel = function () {
    return this.label_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {*} data
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.label_ = new anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement();
    this.label_.deserialize(data);
    return data;
};
//------------------------------------------------------------------------------
//
//                          GaugeLabelStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle,
    anychart.styles.Style);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.labels.GaugeLabelStyleState;
};









