/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame}</li>
 *  <li>@class {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect}</li>
 *  <li>@class {anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase}</li>
 *  <li>@class {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect}</li>
 *  <li>@class {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.frames.circular');
//------------------------------------------------------------------------------
//
//                          CircularGaugeFrame class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.frames.GaugeFrame}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame,
    anychart.plots.gaugePlot.frames.GaugeFrame);

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.createInnerStroke = function () {
    return new anychart.plots.gaugePlot.frames.circular.InnerFrameStroke_(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.createOuterStroke = function () {
    return new anychart.plots.gaugePlot.frames.circular.OuterFrameStroke_(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.createBackground = function () {
    return new anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_(this);
};

/**
 * @protected
 * @return {number}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.getThickness = function () {
    var thickness = 0;
    if (this.innerStroke && this.innerStroke.isEnabled())
        thickness += this.innerStroke.getThickness();
    if (this.outerStroke && this.outerStroke.isEnabled())
        thickness += this.outerStroke.getThickness();
    return thickness;
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.autoFit = function (bounds) {
    var radius = 1 + this.getThickness() + this.padding;
    /** @type {anychart.plots.gaugePlot.gauges.circular.CircularGauge} */
    var gauge = this.gauge;

    var width = radius * 2;
    var height = radius * 2;

    var pixRadius = Math.min(bounds.width / width, bounds.height / height);
    var newWidth = width * pixRadius;
    var newHeight = height * pixRadius;

    bounds.x = bounds.x + (bounds.width - newWidth) / 2;
    bounds.y = bounds.y + (bounds.height - newHeight) / 2;
    bounds.width = newWidth;
    bounds.height = newHeight;

    gauge.setMaxPixelRadius(pixRadius * 2);
    gauge.setFrameRadius(pixRadius);

    gauge.getPivotPoint().x = .5;
    gauge.getPivotPoint().y = .5;
};

/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} thickness
 * @return {String}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.execDrawing = function (svgManager, thickness) {
    var path = '',
        r = this.gauge.getFrameRadius(),
        pixPadding = this.padding * r,
        pixThickness = thickness * r,
        radius = r + pixPadding + pixThickness,
        center = this.gauge.getPixPivotPoint();

    path += svgManager.pCircle(center.x, center.y, radius);
    path += svgManager.pFinalize();

    return path;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} innerThickness Inner thickness
 * @param {Number} outerThickness Outer thickness
 * @return {String}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame.prototype.execDrawingBackground = function (svgManager, innerThickness, outerThickness) {
    var path = '',
        r = this.gauge.getFrameRadius(),
        pixPadding = this.padding * r,
        radius0 = r + pixPadding + innerThickness * r,
        radius = r + pixPadding + outerThickness * r,
        x = this.gauge.getPixPivotPoint().x,
        y = this.gauge.getPixPivotPoint().y;


    var innerStartX = x + radius0,
        innerEndX = x + radius0 * Math.cos(359.999 * Math.PI / 180),
        innerEndY = y + radius0 * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(innerStartX, y);
    path += svgManager.pCircleArc(radius0, 1, 1, innerEndX, innerEndY, radius0);

    var outerStartX = x + radius,
        outerEndX = x + radius * Math.cos(359.999 * Math.PI / 180),
        outerEndY = y + radius * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(outerEndX, outerEndY);
    path += svgManager.pCircleArc(radius, 1, 0, outerStartX, y, radius);
    path += svgManager.pMove(innerStartX, y);

    return path;
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @private
 * @constructor
 */
anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_ = function (frame) {
    anychart.plots.gaugePlot.visual.BackgroundBasedStroke.call(this);
    this.frame = frame;
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_,
    anychart.plots.gaugePlot.visual.BackgroundBasedStroke);

anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.prototype.initialize = function (svgManager) {
    if (!this.enabled_) return null;

    var fillSegment = this.createElement(svgManager);
    var hatchFillSegment = this.createElement(svgManager);

    return new anychart.visual.background.SVGBackgroundSprite(
        svgManager,
        fillSegment,
        hatchFillSegment);
};
anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.prototype.draw = function (sprite, bounds, opt_color) {
    var svgManager = sprite.getSVGManager();

    //effects
    var filter;
    if (this.effects_ && this.effects_.isEnabled())
        filter = this.effects_.createEffects(svgManager);

    var segmentData = this.getElementPath(svgManager);
    this.drawFill_(sprite.getFillSegment(), segmentData, sprite, bounds, opt_color, filter);
    this.drawHatchFill_(sprite.getHatchFillSegment(), segmentData, sprite, filter);
};

/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.prototype.getElementPath = function (svgManager) {
    var path = '', innerThickness, outerThickness;
    if (this instanceof anychart.plots.gaugePlot.frames.circular.InnerFrameStroke_) {
        innerThickness = 0;
        outerThickness = this.getThickness();
    } else {
        innerThickness = 0;
        if (this.frame.innerStroke.isEnabled())
            innerThickness = this.frame.innerStroke.getThickness();
        outerThickness = innerThickness + this.getThickness();
    }

    path += this.frame.execDrawingBackground(svgManager, innerThickness, outerThickness) + svgManager.pFinalize();

    return path;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame}
 */
anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.prototype.frame = null;

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.circular.InnerFrameStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @private
 * @constructor
 */
anychart.plots.gaugePlot.frames.circular.InnerFrameStroke_ = function (frame) {
    anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.call(this, frame);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.InnerFrameStroke_,
    anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_);

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.circular.OuterFrameStroke_ class
//
//------------------------------------------------------------------------------
/**
 * @private
 * @constructor
 */
anychart.plots.gaugePlot.frames.circular.OuterFrameStroke_ = function (frame) {
    anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_.call(this, frame);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.OuterFrameStroke_,
    anychart.plots.gaugePlot.frames.circular.CircularFrameStroke_);

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_ class
//
//------------------------------------------------------------------------------
/**
 * @private
 * @constructor
 */
anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_ = function (frame) {
    anychart.visual.background.ShapeBackground.call(this);
    this.frame_ = frame;
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_,
    anychart.visual.background.ShapeBackground);

/**
 * @private
 * @type {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame}
 */
anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_.prototype.frame_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_.prototype.createElement = function (svgManager) {
    return svgManager.createPath();
};

/** @inheritDoc */
anychart.plots.gaugePlot.frames.circular.CircularFrameBackground_.prototype.getElementPath = function (svgManager, bounds) {
    return this.frame_.execDrawing(svgManager, 0) + svgManager.pFinalize();
};

//------------------------------------------------------------------------------
//
//                          CircularGaugeFrameRect class
//
//------------------------------------------------------------------------------
/**
 * Circular gauge rectangular frame.
 * @constructor
 * @extends {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame}
 *
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect,
    anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame);

/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect.prototype.execDrawingBackground = function (svgManager, innerThickness, outerThickness) {
    var path = '';
    path += this.execDrawing(svgManager, innerThickness);
    path += this.execDrawing(svgManager, outerThickness);
    return path;
};

/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameRect.prototype.execDrawing = function (svgManager, thickness) {
    var path = '',
        gauge = this.gauge,
        r = gauge.getFrameRadius(),
        rc;
    if (this.allowAutoFit()) {
        var pixPadding = this.padding * r,
            pixThickness = thickness * r;
        r += pixPadding + pixThickness;
        rc = new anychart.utils.geom.Rectangle(
            gauge.getPixPivotPoint().x - r,
            gauge.getPixPivotPoint().y - r,
            r * 2,
            r * 2
        )
    } else {
        var m = new anychart.layout.Margin(),
            t = this.getThickness();
        m.setAll(t - thickness);
        var fullRect = new anychart.utils.geom.Rectangle();
        fullRect.setLeft(gauge.bounds.x + m.getLeft() * r);
        fullRect.setTop(gauge.bounds.y + m.getTop() * r);
        fullRect.setRight(gauge.bounds.getRight() - m.getRight() * r);
        fullRect.setBottom(gauge.bounds.getBottom() - m.getBottom() * r);
        rc = fullRect;
    }
    if (this.corners == null) {
        path += svgManager.pRectByBounds(rc);
    } else {
        /** @type {anychart.visual.corners.Corners} */
        var c = this.corners.clone();
        c.setLeftBottom(c.getLeftBottom() * r / 100);
        c.setLeftTop(c.getLeftTop() * r / 100);
        c.setRightBottom(c.getRightBottom() * r / 100);
        c.setRightTop(c.getRightTop() * r / 100);
        path += c.createSVGPolygonData(svgManager, rc);
    }

    return path;
};
//------------------------------------------------------------------------------
//
//                      CircularGaugeAutoFrameBase class
//
//------------------------------------------------------------------------------
/**
 * Represents base class for automatic frames.
 * @constructor
 * @extends {anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame}
 *
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase,
    anychart.plots.gaugePlot.frames.circular.CircularGaugeFrame);

/**
 * @type {anychart.layout.Margin}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.autoMargin = null;

/**
 * @private
 * @param {Number} radAngle
 * @return {int}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.getAngleFourth_ = function (radAngle) {
    var cos = Math.cos(radAngle),
        sin = Math.sin(radAngle);
    if (cos >= 0 && sin >= 0) return 0;
    if (cos <= 0 && sin >= 0) return 1;
    if (cos <= 0 && sin < 0) return 2;
    return 3;
};

/**
 * ND: Needs doc!
 * @private
 * @param {Number} startRadAngle
 * @param {Number} endRadAngle
 * @return {Array}
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.addExtraAngles_ = function (startRadAngle, endRadAngle) {
    var extraCheckAngles = [];
    var startF = this.getAngleFourth_(startRadAngle);
    var endF = this.getAngleFourth_(endRadAngle);

    if (startF != endF) {
        switch (startF) {
            case 0:
                if (endF == 1) {
                    extraCheckAngles.push(Math.PI / 2);
                } else if (endF == 2) {
                    extraCheckAngles.push(Math.PI / 2);
                    extraCheckAngles.push(Math.PI);
                } else if (endF == 3) {
                    extraCheckAngles.push(Math.PI / 2);
                    extraCheckAngles.push(Math.PI);
                    extraCheckAngles.push(Math.PI * 3 / 2);
                }
                break;
            case 1:
                if (endF == 0) {
                    extraCheckAngles.push(0);
                    extraCheckAngles.push(Math.PI);
                    extraCheckAngles.push(Math.PI * 3 / 2);
                } else if (endF == 2) {
                    extraCheckAngles.push(Math.PI);
                } else if (endF == 3) {
                    extraCheckAngles.push(Math.PI);
                    extraCheckAngles.push(Math.PI * 3 / 2);
                }
                break;
            case 2:
                if (endF == 1) {
                    extraCheckAngles.push(Math.PI / 2);
                    extraCheckAngles.push(0);
                    extraCheckAngles.push(Math.PI * 3 / 2);
                } else if (endF == 3) {
                    extraCheckAngles.push(Math.PI * 3 / 2);
                } else if (endF == 0) {
                    extraCheckAngles.push(0);
                    extraCheckAngles.push(Math.PI * 3 / 2);
                }
                break;
            case 3:
                if (endF == 0) {
                    extraCheckAngles.push(0);
                } else if (endF == 1) {
                    extraCheckAngles.push(0);
                    extraCheckAngles.push(Math.PI / 2);
                } else if (endF == 2) {
                    extraCheckAngles.push(0);
                    extraCheckAngles.push(Math.PI / 2);
                    extraCheckAngles.push(Math.PI);
                }
                break;
        }
    } else if ((endRadAngle - startRadAngle) > Math.PI) {
        extraCheckAngles.push(0);
        extraCheckAngles.push(Math.PI / 2);
        extraCheckAngles.push(Math.PI);
        extraCheckAngles.push(Math.PI * 3 / 2);
    }

    return extraCheckAngles;
};
/**
 * @private
 * @param {Number} radAngle
 * @param {Number} r
 * @param {anychart.layout.Margin} margin
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.checkBoundAngle_ = function (radAngle, r, margin) {
    var cos = Math.cos(radAngle),
        sin = Math.sin(radAngle);

    var tmpRc = new anychart.utils.geom.Rectangle(cos, sin, 0, 0);
    tmpRc.inflate(r, r);

    if (tmpRc.getLeft() < 0)
        margin.setLeft(Math.min(margin.getLeft(), tmpRc.getLeft()));
    else
        margin.setRight(Math.max(margin.getRight(), tmpRc.getLeft()));

    if (tmpRc.getRight() < 0)
        margin.setLeft(Math.min(margin.getLeft(), tmpRc.getRight()));
    else
        margin.setRight(Math.max(margin.getRight(), tmpRc.getRight()));

    if (tmpRc.getTop() < 0)
        margin.setTop(Math.min(margin.getTop(), tmpRc.getTop()));
    else
        margin.setBottom(Math.max(margin.getBottom(), tmpRc.getTop()));

    if (tmpRc.getBottom() < 0)
        margin.setTop(Math.min(margin.getTop(), tmpRc.getTop()));
    else
        margin.setBottom(Math.max(margin.getBottom(), tmpRc.getBottom()));
};

/**
 * @private
 * @param {Number} radAngle
 * @param {Number} r
 * @param {anychart.layout.Margin} margin
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.checkExtraAngle_ = function (radAngle, r, margin) {
    var cos = (1 + r) * Math.cos(radAngle),
        sin = (1 + r) * Math.sin(radAngle);

    if (cos < 0)
        margin.setLeft(Math.min(margin.getLeft(), cos));
    else
        margin.setRight(Math.max(margin.getRight(), cos));

    if (sin < 0)
        margin.setTop(Math.min(margin.getTop(), sin));
    else
        margin.setBottom(Math.max(margin.getBottom(), sin));
};

/**
 * @private
 * @param {Number} r
 * @param {anychart.layout.Margin} margin
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.checkCap_ = function (r, margin) {
    r += this.gauge.getMaxCapRadius();
    margin.setLeft(Math.max(Math.abs(margin.getLeft()), r));
    margin.setRight(Math.max(margin.getRight(), r));
    margin.setTop(Math.max(Math.abs(margin.getTop()), r));
    margin.setBottom(Math.max(margin.getBottom(), r));
};

/**
 * @param {anychart.utils.geom.Rectangle} gaugeRect
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase.prototype.autoFit = function (gaugeRect) {
    var gauge = this.gauge,
        thickness = this.getThickness();

    var r1 = 1,
        r2 = gauge.getMaxCapRadius(),
        r3 = this.padding;

    var startAngle = gauge.getMinAngle(),
        endAngle = gauge.getMaxAngle(),
        sweepAngle = endAngle - startAngle;

    var m = new anychart.layout.Margin();
    m.setAll(0);

    if (sweepAngle >= 320) {
        m.setAll(r1 + r3);
    } else {
        var startRadAngle = startAngle * Math.PI / 180,
            endRadAngle = endAngle * Math.PI / 180,
            checkAngles = [];

        checkAngles.push(startRadAngle);
        checkAngles.push(endRadAngle);

        var extraCheckAngles = this.addExtraAngles_(startRadAngle, endRadAngle);

        for (var i = 0; i < checkAngles.length; i++)
            this.checkBoundAngle_(checkAngles[i], r3, m);

        for (i = 0; i < extraCheckAngles.length; i++)
            this.checkExtraAngle_(extraCheckAngles[i], r3, m);

        this.checkCap_(r3, m);
    }

    m.setLeft(m.getLeft() + thickness);
    m.setRight(m.getRight() + thickness);
    m.setTop(m.getTop() + thickness);
    m.setBottom(m.getBottom() + thickness);

    this.autoMargin = m;

    var w = m.getLeft() + m.getRight(),
        h = m.getTop() + m.getBottom();

    var r = Math.min(gauge.bounds.width / w, gauge.bounds.height / h);
    var newW = w * r,
        newH = h * r;

    gauge.bounds.x = gauge.bounds.x + (gauge.bounds.width - newW) / 2;
    gauge.bounds.y = gauge.bounds.y + (gauge.bounds.height - newH) / 2;
    gauge.bounds.width = newW;
    gauge.bounds.height = newH;

    gauge.setMaxPixelRadius(r * 2);
    gauge.setFrameRadius(r);

    gauge.getPivotPoint().x = m.getLeft() / w;
    gauge.getPivotPoint().y = m.getTop() / h;
};
//------------------------------------------------------------------------------
//
//                      CircularGaugeFrameAutoRect class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase}
 *
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect,
    anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase);

/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect.prototype.execDrawing = function (svgManager, thickness) {
    var path = '',
        gauge = this.gauge,
        r = gauge.getFrameRadius(),
        m = new anychart.layout.Margin(),
        t = this.getThickness();

    m.setAll(t - thickness);

    var fullRect = new anychart.utils.geom.Rectangle();
    fullRect.setLeft(gauge.bounds.x + m.getLeft() * r);
    fullRect.setTop(gauge.bounds.y + m.getTop() * r);
    fullRect.setRight(gauge.bounds.getRight() - m.getRight() * r);
    fullRect.setBottom(gauge.bounds.getBottom() - m.getBottom() * r);

    if (this.corners == null) {
        path += svgManager.pRectByBounds(fullRect);
    } else {
        var c = this.corners.clone();
        c.setLeftBottom(c.getLeftBottom() * r / 100);
        c.setLeftTop(c.getLeftTop() * r / 100);
        c.setRightBottom(c.getRightBottom() * r / 100);
        c.setRightTop(c.getRightTop() * r / 100);
        path += c.createSVGPolygonData(svgManager, fullRect);
    }

    return path;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAutoRect.prototype.execDrawingBackground = function (svgManager, innerThickness, outerThickness) {
    var path = '';
    path += this.execDrawing(svgManager, innerThickness);
    path += this.execDrawing(svgManager, outerThickness);
    return path;
};
//------------------------------------------------------------------------------
//
//                      CircularGaugeFrameAuto class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase}
 *
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto,
    anychart.plots.gaugePlot.frames.circular.CircularGaugeAutoFrameBase);

/**inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto.prototype.execDrawing = function (svgManager, thickness) {
    var path = '',
        gauge = this.gauge,
        r = gauge.getFrameRadius(),
        pixPivotRadius = gauge.getMaxCapRadius() * r,
        pixPadding = this.padding * r,
        pixThickness = thickness * r,
        startEndRadius = pixPadding + pixThickness,

        startAngle = gauge.getMinAngle(),
        endAngle = gauge.getMaxAngle(),
        startRadAngle = startAngle * Math.PI / 180,
        endRadAngle = endAngle * Math.PI / 180,

        startCenterPt = new anychart.utils.geom.Point();

    startCenterPt.x = gauge.getPixPivotPoint().x + (r * Math.cos(startRadAngle));
    startCenterPt.y = gauge.getPixPivotPoint().y + (r * Math.sin(startRadAngle));

    var endCenterPt = new anychart.utils.geom.Point();
    endCenterPt.x = gauge.getPixPivotPoint().x + (r * Math.cos(endRadAngle));
    endCenterPt.y = gauge.getPixPivotPoint().y + (r * Math.sin(endRadAngle));

    var startPtRect = new anychart.utils.geom.Rectangle(startCenterPt.x, startCenterPt.y, 0, 0);
    startPtRect.inflate(startEndRadius, startEndRadius);

    var endPtRect = new anychart.utils.geom.Rectangle(endCenterPt.x, endCenterPt.y, 0, 0);
    endPtRect.inflate(startEndRadius, startEndRadius);

    var pivotRect = new anychart.utils.geom.Rectangle(
        gauge.getPixPivotPoint().x,
        gauge.getPixPivotPoint().y,
        0, 0
    );
    pivotRect.inflate(startEndRadius + pixPivotRadius, startEndRadius + pixPivotRadius);
    var fullRect = new anychart.utils.geom.Rectangle(
        gauge.getPixPivotPoint().x,
        gauge.getPixPivotPoint().y,
        0, 0);
    fullRect.inflate(r + startEndRadius, r + startEndRadius);

    var sweepAngle = endAngle - startAngle;
    if (sweepAngle >= 320) {
        path += svgManager.pCircle(gauge.getPixPivotPoint().x, gauge.getPixPivotPoint().y, r +startEndRadius);
    } else if (sweepAngle > 270) {
        var correction = 90 - ((360 - sweepAngle) / 2);
        path += this.renderRectArc_(svgManager, startPtRect, (startAngle + 270) + correction, 90 - correction, true);
        path += this.renderRectArc_(svgManager, fullRect, startAngle, sweepAngle, false);
        path += this.renderRectArc_(svgManager, endPtRect, endAngle, 90 - correction, false);
    } else {
        path += this.renderRectArc_(svgManager, startPtRect, (startAngle + 270), 90, true);
        path += this.renderRectArc_(svgManager, fullRect, startAngle, sweepAngle, false);
        path += this.renderRectArc_(svgManager, endPtRect, endAngle, 90, false);
        path += this.renderRectArc_(svgManager, pivotRect, endAngle + 45, (360 - sweepAngle) - 90, false);
    }
    return path;
};
/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} rect
 * @param {Number} startAngle
 * @param {Number} sweepAngle
 * @param {Boolean} opt_needMoveTo Default: false
 */
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto.prototype.renderRectArc_ = function (svgManager, rect, startAngle, sweepAngle, opt_needMoveTo) {
    var path = '',
        du = anychart.utils.geom.DrawingUtils;

    path += du.drawArc(svgManager,
        rect.x + rect.width / 2, rect.y + rect.height / 2,
        startAngle, startAngle + sweepAngle,
        rect.height / 2, rect.width / 2,
        opt_needMoveTo
    );
    return path;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.frames.circular.CircularGaugeFrameAuto.prototype.execDrawingBackground = function (svgManager, innerThickness, outerThickness) {
    var path = '';
    path += this.execDrawing(svgManager, innerThickness);
    path += this.execDrawing(svgManager, outerThickness);
    return path;
};