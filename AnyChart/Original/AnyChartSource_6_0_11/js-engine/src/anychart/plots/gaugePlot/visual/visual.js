/**
 * @fileoverview Gauge-specific visual classes.
 */

goog.provide('anychart.plots.gaugePlot.visual');

goog.require('anychart.elements.marker');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.visual.BackgroundBasedStroke class
//
//------------------------------------------------------------------------------
/**
 * Background with thickness.
 * @constructor
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke = function() {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.visual.BackgroundBasedStroke,
              anychart.visual.background.ShapeBackground);

/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.thickness_ = .05;

/**
 * @return {Number} Stroke thickness.
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.getThickness = function() {
    return this.thickness_;
};

/**
 * @return {boolean} is background exits and enabled.
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.hasBackground = function() {
    return this.isEnabled();
};

/**
 * @param {Object} config Configuration.
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.deserialize = function(config) {
    goog.base(this, 'deserialize', config);
    if (this.isEnabled() && anychart.utils.deserialization.hasProp(config, 'thickness'))
        this.thickness_ = anychart.utils.deserialization.getSimplePercentProp(config, 'thickness');
};

/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.initialize = function(svgManager) {
    if (!this.isEnabled()) return null;

    var s = new anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite(svgManager);
    s.innerStrokeElement = this.createInnerStrokeElement(svgManager);
    s.outerStrokeElement = this.createOuterStrokeElement(svgManager);
    s.backgroundFillElement = this.createBackgroundElement(svgManager);
    s.backgroundHatchFillElement = this.createBackgroundElement(svgManager);

    if (s.innerStrokeElement)
        s.appendChild(s.innerStrokeElement);
    if (s.outerStrokeElement)
        s.appendChild(s.outerStrokeElement);
    if (s.backgroundFillElement)
        s.appendChild(s.backgroundFillElement);
    if (s.backgroundHatchFillElement)
        s.appendChild(s.backgroundHatchFillElement);

    return s;
};

/**
 * @param {anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite} sprite
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.draw = function(sprite, bounds) {
    this.drawInnerStroke(sprite.getSVGManager(), sprite.innerStrokeElement, bounds);
    this.drawOuterStroke(sprite.getSVGManager(), sprite.outerStrokeElement, bounds);
    this.drawBackground(sprite.getSVGManager(), sprite.backgroundFillElement, sprite.backgroundHatchFillElement, bounds);
};
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.drawFill_ = function(segment, segmentData, sprite, bounds, opt_color, opt_filter) {
    var svgManager = sprite.getSVGManager();
    var style = '';
    //update fill
    var bool = this.fill_.getGradient() ? (this.fill_.getGradient().getType() == 1) : null;
    if (this.fill_ && this.fill_.isEnabled())
        style += this.fill_.getSVGFill(svgManager, bounds, opt_color, bool);
    else
        style += svgManager.getEmptySVGFill();
    if (this.border_ && this.border_.isEnabled())
        style += this.border_.getSVGStroke(svgManager, bounds, opt_color, true);
    else
        style += svgManager.getEmptySVGStroke();
    this.execDraw_(segment, segmentData, style, opt_filter);
};
/**
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {SVGElement} innerStrokeContainer
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.drawInnerStroke = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {SVGElement} outerStrokeContainer
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.drawOuterStroke = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {SVGElement} backgroundFillContainer
 * @param {SVGElement} backgroundHatchFillContainer
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.drawBackground = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.SVG.SVGManager} svgManager
 * @return {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.createInnerStrokeElement = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.SVG.SVGManager} svgManager
 * @return {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.createOuterStrokeElement = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.SVG.SVGManager} svgManager
 * @return {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.createBackgroundElement = goog.abstractMethod;

/**@inheritDoc*/
anychart.plots.gaugePlot.visual.BackgroundBasedStroke.prototype.createElement = function(svgManager) {
    return svgManager.createPath();
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite = function(svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
};
goog.inherits(anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite,
              anychart.svg.SVGSprite);

/**
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite.prototype.innerStrokeElement = null;

/**
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite.prototype.outerStrokeElement = null;

/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite.prototype.backgroundFillElement = null;

/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.visual.BackgroundBasedStrokeSprite.prototype.backgroundHatchFillElement = null;

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.visual.GaugeShapeType enum
//
//------------------------------------------------------------------------------
/**
 */
anychart.plots.gaugePlot.visual.GaugeShapeType = {
    NONE: -1,
    CIRCLE: 0,
    SQUARE: 1,
    DIAMOND: 2,
    CROSS: 3,
    DIAGONAL_CROSS: 4,
    H_LINE: 5,
    V_LINE: 6,
    STAR_4: 7,
    STAR_5: 8,
    STAR_6: 9,
    STAR_7: 10,
    STAR_10: 11,
    TRIANGLE_UP: 12,
    TRIANGLE_DOWN: 13,
    IMAGE: 14,
    RECTANGLE: 105,
    TRIANGLE: 106,
    TRAPEZOID: 107,
    LINE: 108,
    PENTAGON:109,

    shapeIsRectangle: function(type) {
        var t = anychart.plots.gaugePlot.visual.GaugeShapeType;
        return type == t.RECTANGLE || type == t.TRIANGLE || type == t.TRAPEZOID || type == t.LINE;
    },

    deserialize: function(type) {
        var t = anychart.plots.gaugePlot.visual.GaugeShapeType;
        switch (type) {
            default:
            case 'none': return t.NONE;
            case 'circle': return t.CIRCLE;
            case 'square': return t.SQUARE;
            case 'diamond': return t.DIAMOND;
            case 'cross': return t.CROSS;
            case 'diagonalcross':return t.DIAGONAL_CROSS;
            case 'hline': return t.H_LINE;
            case 'vline': return t.V_LINE;
            case 'star4': return t.STAR_4;
            case 'star5': return t.STAR_5;
            case 'star6': return t.STAR_6;
            case 'star7': return t.STAR_7;
            case 'star10': return t.STAR_10;
            case 'triangleup': return t.TRIANGLE_UP;
            case 'triangledown': return t.TRIANGLE_DOWN;
            case 'image': return t.IMAGE;
            case 'rectangle': return t.RECTANGLE;
            case 'triangle': return t.TRIANGLE;
            case 'trapezoid': return t.TRAPEZOID;
            case 'pentagon': return t.PENTAGON;
            case 'line': return t.LINE;
        }
    }
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.visual.GaugeShapeDrawer class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer = function() {
    anychart.elements.marker.MarkerDrawer.call(this);
};
goog.inherits(anychart.plots.gaugePlot.visual.GaugeShapeDrawer,
              anychart.elements.marker.MarkerDrawer);

/** @inheritDoc */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.getSVGPathData = function(type, svgManager, size, dx, dy, width, height, thickness) {
    switch (type) {
        case anychart.plots.gaugePlot.visual.GaugeShapeType.PENTAGON:
            return this.drawPentagon_(svgManager, size, dx, dy, width, height);
        case anychart.plots.gaugePlot.visual.GaugeShapeType.RECTANGLE:
            return this.drawRect_(svgManager, size, dx, dy, width, height);
        case anychart.plots.gaugePlot.visual.GaugeShapeType.TRAPEZOID:
            return this.drawTrapezoid_(svgManager, size, dx, dy, width, height);
        case anychart.plots.gaugePlot.visual.GaugeShapeType.TRIANGLE:
            return this.drawTriangle_(svgManager, size, dx, dy, width, height);
        case anychart.plots.gaugePlot.visual.GaugeShapeType.LINE:
            return this.drawLine_(svgManager, size, dx, dy, width, height, thickness);
        case anychart.plots.gaugePlot.visual.GaugeShapeType.H_LINE:
            return this.drawHLine_(svgManager, size, dx, dy, width, height, thickness);
        default:
            return goog.base(this, 'getSVGPathData', type, svgManager, size, dx, dy, width, height);
    }
};

/**
 * @private
 * @type {Array.<number>}
 * @const
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.pentagonCos_ = [
                                                                1+Math.cos((2/5-.5)*Math.PI),
                                                                1+Math.cos((4/5-.5)*Math.PI),
                                                                1+Math.cos((6/5-.5)*Math.PI),
                                                                1+Math.cos((8/5-.5)*Math.PI),
                                                                1+Math.cos(1.5*Math.PI)];
/**
 * @private
 * @type {Array.<number>}
 * @const
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.pentagonSin_ = [
                                                                1+Math.sin((2/5-.5)*Math.PI),
                                                                1+Math.sin((4/5-.5)*Math.PI),
                                                                1+Math.sin((6/5-.5)*Math.PI),
                                                                1+Math.sin((8/5-.5)*Math.PI),
                                                                1+Math.sin(1.5*Math.PI)];

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} size
 * @param {number} dx
 * @param {number} dy
 * @param {number} width
 * @param {number} height
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawPentagon_ = function(svgManager, size, dx, dy, width, height) {
    var r = Math.min(width, height)/2;
    var res = svgManager.pMove(dx + r*this.pentagonCos_[0], dy + r*this.pentagonSin_[0]);
    for (var i = 1; i < 5; i++)
        res += svgManager.pLine(dx + r*this.pentagonCos_[i], dy + r*this.pentagonSin_[i]);
    res += svgManager.pLine(dx + r*this.pentagonCos_[0], dy + r*this.pentagonSin_[0]);
    res += svgManager.pFinalize();
    return res;
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} size
 * @param {number} dx
 * @param {number} dy
 * @param {number} width
 * @param {number} height
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawRect_ = function(svgManager, size, dx, dy, width, height) {
    return svgManager.pRect(dx, dy, width, height);
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} size
 * @param {number} dx
 * @param {number} dy
 * @param {number} width
 * @param {number} height
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawTrapezoid_ = function(svgManager, size, dx, dy, width, height) {
    var d = width/3;
    var res = svgManager.pMove(dx + d, dy);
    res += svgManager.pLine(dx + width - d, dy);
    res += svgManager.pLine(dx + width, dy + height);
    res += svgManager.pLine(dx, dy + height);
    res += svgManager.pLine(dx + d, dy);
    res += svgManager.pFinalize();
    return res;
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} size
 * @param {number} dx
 * @param {number} dy
 * @param {number} width
 * @param {number} height
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawTriangle_ = function(svgManager, size, dx, dy, width, height) {
    var res = svgManager.pMove(dx + width/2, dy);
    res += svgManager.pLine(dx, dy+height);
    res += svgManager.pLine(dx + width, dy + height);
    res += svgManager.pLine(dx + width/2, dy);
    res += svgManager.pFinalize();
    return res;
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} size
 * @param {number} dx
 * @param {number} dy
 * @param {number} width
 * @param {number} height
 */
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawLine_ = function(svgManager, size, dx, dy, width, height, thickness) {
    return svgManager.pRLine(dx + width/2, dy, dx + width/2, dy + height, thickness);
};

/**
* @private
* @param {anychart.svg.SVGManager} svgManager
* @param {number} size
* @param {number} dx
* @param {number} dy
* @param {number} width
* @param {number} height
*/
anychart.plots.gaugePlot.visual.GaugeShapeDrawer.prototype.drawHLine_ = function(svgManager, size, dx, dy, width, height, thickness) {
    return svgManager.pRLine(dx, dy + height/2, dx + width, dy + height/2, thickness);
};
