/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.axisBased.axis.markers');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles');
goog.require('anychart.plots.gaugePlot.visual.text');
//------------------------------------------------------------------------------
//
//                          GaugeAxisMarker class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker = function () {
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.color_ = null;
/**
 * @return {anychart.visual.color.ColorContainer}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getColor = function () {
    return this.color_;
};
/**
 * @private
 * @type {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.style_ = null;
/**
 * @return {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getStyle = function () {
    return this.style_;
};
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.axis_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getAxis = function () {
    return this.axis_;
};
/**
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} value
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.setAxis = function (value) {
    this.axis_ = value;
};
/**
 * @param {anychart.plots.gaugePlot.gauges.GaugeBase} value
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getGauge = function () {
    return this.axis_.getGauge();
};
/**
 * @private
 * @type {anychart.styles.StyleShapes}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.styleShapes_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getSprite = function () {
    return this.sprite_;
};
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getSVGManager = function () {
    return this.sprite_.getSVGManager();
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getBounds = function () {
    return this.bounds_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize GaugeMarker settings.
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 * @param {Number} axisWidth
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.deserialize = function (data, stylesList, axisWidth) {
    var des = anychart.utils.deserialization;

    this.color_ = des.hasProp(data, 'color') ?
        des.getColorProp(data, 'color') :
        des.getColor('0xCCCCCC');

    this.style_ = this.deserializeStyleFromNode(data, stylesList);
};
/**
 * @protected
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 * @return {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.deserializeStyleFromNode = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    var actualStyle = stylesList.getStyleByMerge(
        this.getStyleNodeName(),
        data,
        des.getStringProp(data, 'style'));

    var style = this.createStyle(data);
    style.deserialize(actualStyle);
    return style;
};
/**
 * @protected
 * @param {Object} data
 * @return {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.createStyle = goog.abstractMethod;
/**
 * @protected
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getStyleNodeName = goog.abstractMethod;
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize gauge axis marker visual.
 * @param {anychart.svg.SVGManager}
    */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.initialize = function (svgManager) {
    this.bounds_ = new anychart.utils.geom.Rectangle();
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.styleShapes_ = this.style_.initialize(this);
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                         Drawing.
//------------------------------------------------------------------------------
/**
 * @return {anychart.styles.StyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.getCurrentStyleState = function () {
    return this.style_.getNormal();
};
/**
 * Draw marker.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.draw = function () {
    this.styleShapes_.update(this.getCurrentStyleState(), true);
};
//------------------------------------------------------------------------------
//                         Resize.
//------------------------------------------------------------------------------
/**
 * Resize marker.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.resize = function () {
    this.styleShapes_.update(null, true);
};
//------------------------------------------------------------------------------
//                          Scale.
//------------------------------------------------------------------------------
/**
 * @param {anychart.scales.BaseScale} scale
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.checkScale = goog.abstractMethod;
/**
 * @protected
 * @param {Number} value
 * @param {anychart.scales.BaseScale} scale
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.prototype.checkScaleValue = function (value, scale) {
    if (isNaN(scale.getDataRangeMinimum()) || value < scale.getDataRangeMinimum())
        scale.setDataRangeMinimum(value);
    if (isNaN(scale.getDataRangeMaximum()) || value > scale.getDataRangeMaximum())
        scale.setDataRangeMaximum(value);
};
//------------------------------------------------------------------------------
//
//                      GaugeAxisMarkerLabel class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel = function () {
    this.align_ = anychart.plots.gaugePlot.layout.Align.CENTER;
    this.padding_ = 0;
    this.textElement_ = new anychart.visual.text.TextElement();
    this.isDynamicFontSize_ = false;
    this.isFontSizeAxisDepend_ = false;
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.align_ = null;
/**
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.getAlign = function () {
    return this.align_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.getPadding = function () {
    return this.padding_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.textElement_ = null;
/**
 * @return {anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.getTextElement = function () {
    return this.textElement_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.fontSize_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.getFontSize = function () {
    return this.fontSize_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.isFontSizeAxisDepend_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.isFontSizeAxisDepend = function () {
    return this.isFontSizeAxisDepend_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.value_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.getValue = function () {
    return this.value_;
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {Object} data ND: Needs doc!
 * @param {anychart.styles.Style} style ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerLabel.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    if (des.hasProp(data, 'align'))
        this.align_ = gaugeDes.getAlignProp(data, 'align');

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getSimplePercentProp(data, 'padding');

    if (des.hasProp(data, 'font')) {
        var font = des.getProp(data, 'font');
        if (des.hasProp(font, 'size')) {
            var strFontSize = des.getStringProp(font, 'size');
            des.deleteProp(data, 'size');
            this.isFontSizeAxisDepend_ = gaugeDes.isAxisSizeDepend(strFontSize);
            this.fontSize_ = this.isFontSizeAxisDepend_ ?
                gaugeDes.getAxisSizeFactor(strFontSize) :
                des.getSimplePercent(strFontSize) * 100;
        }
    }

    if (des.hasProp(data, 'value'))
        this.value_ = des.getNumProp(data, 'value');

    this.textElement_ = new anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement();
    this.textElement_.deserialize(data);
};

//------------------------------------------------------------------------------
//
//                     RangeGaugeAxisMarker class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker = function () {
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker.call(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.start_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.getStart = function () {
    return this.start_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.end_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.getEnd = function () {
    return this.end_;
};
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.createStyle = function (data) {
    switch (this.getGauge().getGaugeType()) {
        case anychart.plots.gaugePlot.gauges.GaugeType.LINEAR:
            return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.LinearColorRangeStyle();

        case anychart.plots.gaugePlot.gauges.GaugeType.CIRCULAR:
            //TODO: Implement normal color range
            return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CircularColorRangeStyle();
//            anychart.errors.featureNotSupported('Circular gauge');
            break;
    }
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.getStyleNodeName = function () {
    return 'color_range_style';
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.deserialize = function (data, stylesList, axisWidth) {
    goog.base(this, 'deserialize', data, stylesList, axisWidth);
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'start'))
        this.start_ = des.getNumProp(data, 'start');

    if (des.hasProp(data, 'end'))
        this.end_ = des.getNumProp(data, 'end');
};
//------------------------------------------------------------------------------
//                  Scales.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.checkScale = function (scale) {
    //check start
    this.checkScaleValue(this.start_, scale);
    this.start_ = (this.start_ > scale.getMaximum()) ?
        scale.getMaximum() : (this.start_ < scale.getMinimum()) ? scale.getMinimum() :
        this.start_;

    //check end
    this.checkScaleValue(this.end_, scale);
    this.end_ = (this.end_ > scale.getMaximum()) ?
        scale.getMaximum() : (this.end_ < scale.getMinimum()) ? scale.getMinimum() :
        this.end_;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker.prototype.getValue = function() {
    return (this.start_ + this.end_) / 2
};

//------------------------------------------------------------------------------
//
//                 GaugeAxisTrendlineMarker class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker}
 *
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.value_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.getValue = function () {
    return this.value_;
};
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.getStyleNodeName = function () {
    return 'trendline_style';
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.TrendlineStyle();
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.deserialize = function (data, styleList, axisWidth) {
    goog.base(this, 'deserialize', data, styleList, axisWidth);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'value'))
        this.value_ = des.getNumProp(data, 'value');

    var stroke = this.getCurrentStyleState().getBorder();
    if (stroke && stroke.isGradient())
        this.getGauge().setGradientRotation(stroke.getGradient());
};
//------------------------------------------------------------------------------
//                          Scale.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker.prototype.checkScale = function (scale) {
    this.checkScaleValue(this.value_, scale);
};
//------------------------------------------------------------------------------
//
//                     GaugeAxisMarkerTickmark class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark,
    anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark.prototype.isWidthAxisSizeDepend_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark.prototype.isHeightAxisSizeDepend_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarkerTickmark.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    var temp;
    if (des.hasProp(data, 'length')) {
        temp = des.getStringProp(data, 'length');
        this.isWidthAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.length_ = this.isWidthAxisSizeDepend_ ?
            gaugeDes.getAxisSizeFactor(temp) :
            des.getSimplePercent(temp);
    }

    if (des.hasProp(data, 'width')) {
        temp = des.getStringProp(data, 'width');
        this.isHeightAxisSizeDepend_ = gaugeDes.isAxisSizeDepend(temp);
        this.width_ = this.isHeightAxisSizeDepend_ ?
            gaugeDes.getAxisSizeFactor(temp) :
            des.getSimplePercent(temp);
    }
};
//------------------------------------------------------------------------------
//
//                    GaugeAxisLabelMarker class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker,
    anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.value_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.getValue = function() {
    return this.value_;
};
//------------------------------------------------------------------------------
//                          Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.getStyleNodeName = function () {
    return 'custom_label_style';
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.styles.CustomLabelStyle();
};
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.deserialize = function (data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    var tickmark = this.style_.getNormal().getTickmark();
    if (tickmark)
        this.getGauge().setBackgroundRotation(tickmark.getBackground());

    if (des.hasProp(data, 'value'))
        this.value_ = des.getNumProp(data, 'value');
};
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);

    var tickmark = this.style_.getNormal().getTickmark();
    //if (tickmark)
    //        this.sprite_.appendSprite(tickmark.initialize(svgManager));

    return  this.sprite_;
};
//------------------------------------------------------------------------------
//                      Scale.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker.prototype.checkScale = function (scale) {
    this.checkScaleValue(this.value_, scale);
};
//------------------------------------------------------------------------------
//
//                  AxisMarkersList class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList = function () {
    this.markers = [];
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.axis_ = null;
/**
 *
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} value
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.setAxis = function (value) {
    this.axis_ = value;
};
/**
 * @private
 * @type {anychart.scales.BaseScale}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.scale_ = null;
/**
 * @param {anychart.scales.BaseScale} value
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.setScale = function (value) {
    this.scale_ = value;
};
/**
 * @protected
 * @type {Array.<anychart.plots.gaugePlot.gauges.axisBased.axis.marker.AxisMarker>}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.markers = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.sprite_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.deserialize = function (data, stylesList, axisWidth) {
    var i, children, childrenCount;
    var des = anychart.utils.deserialization;
    //color ranges
    if (des.hasProp(data, 'color_ranges')) {
        children = des.getPropArray(des.getProp(data, 'color_ranges'), 'color_range');
        for (i = 0, childrenCount = children.length; i < childrenCount; i++) {
            if (des.isEnabled(children[i]))
                this.addMarker_(
                    new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeRangeAxisMarker(),
                    children[i],
                    stylesList,
                    axisWidth);
        }
    }

    //trendlines
    if (des.hasProp(data, 'trendlines')) {
        children = des.getPropArray(des.getProp(data, 'trendlines'), 'trendline');
        for (i = 0, childrenCount = children.length; i < childrenCount; i++) {
            if (des.isEnabled(children[i]))
                this.addMarker_(
                    new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisTrendlineMarker(),
                    children[i],
                    stylesList,
                    axisWidth);
        }
    }

    //custom lebels
    if (des.hasProp(data, 'custom_labels')) {
        children = des.getPropArray(des.getProp(data, 'custom_labels'), 'custom_label');
        for (i = 0, childrenCount = children.length; i < childrenCount; i++) {
            if (des.isEnabled(children[i]))
                this.addMarker_(
                    new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisCustomLabelMarker(),
                    children[i],
                    stylesList,
                    axisWidth);
        }
    }
};

/**
 * Add marker to marker list, call marker deserialization.
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.GaugeAxisMarker} marker
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 * @param {Number} axisWidth
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.addMarker_ = function (marker, data, stylesList, axisWidth) {
    marker.setAxis(this.axis_);
    marker.deserialize(data, stylesList, axisWidth);
    marker.checkScale(this.scale_);
    this.markers.push(marker);
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGManager} svgManager
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.initialize = function (svgManager) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('GaugeAxisMarkersList');

    var markersCount = this.markers.length;
    for (var i = 0; i < markersCount; i++)
        this.sprite_.appendSprite(this.markers[i].initialize(svgManager));

    return this.sprite_;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * Draw all markers in marker list
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.draw = function () {
    var markersCount = this.markers.length;
    for (var i = 0; i < markersCount; i++) this.markers[i].draw();
};
//------------------------------------------------------------------------------
//                  Resize.
//------------------------------------------------------------------------------
/**
 * Draw all markers in marker list
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList.prototype.resize = function () {
    var markersCount = this.markers.length;
    for (var i = 0; i < markersCount; i++) this.markers[i].resize();
};



