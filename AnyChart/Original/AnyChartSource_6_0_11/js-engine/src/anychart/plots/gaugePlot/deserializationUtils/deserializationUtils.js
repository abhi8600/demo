goog.provide('anychart.plots.gaugePlot.deserializationUtils');

/**
 * @param {object} config
 * @param {string} prop
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.deserializationUtils.getAlignProp = function(config, prop) {
    switch (anychart.utils.deserialization.getLStringProp(config, prop)) {
        case 'inside': return anychart.plots.gaugePlot.layout.Align.INSIDE;
        case 'outside': return anychart.plots.gaugePlot.layout.Align.OUTSIDE;
        default: return anychart.plots.gaugePlot.layout.Align.CENTER;
    }
};

/**
 * @param {object} config
 * @param {string} prop
 * @param {number} axisSize
 */
anychart.plots.gaugePlot.deserializationUtils.getAxisSizeDependProp = function(config, prop, axisSize) {
    var w = anychart.utils.deserialization.getStringProp(config, prop);
    if (w.indexOf('%') == 0) {
        var res = axisSize;
        if (w.indexOf('*') != -1)
            res *= Number(w.substr(w.indexOf('*')+1));
        return res;
    }
    return anychart.utils.deserialization.getSimplePercentProp(config, prop);
};

/**
 * @param {String || Number} value
 * @return {Boolean}
 */
anychart.plots.gaugePlot.deserializationUtils.isAxisSizeDepend = function (value) {
    return String(value).indexOf('%') == 0;
};
/**
 * @param {String || Number} value
 * @return {Boolean}
 */
anychart.plots.gaugePlot.deserializationUtils.getAxisSizeFactor = function (value) {
    var s = String(value);
    if (s.indexOf('*') == -1) return 1;
    return Number(s.substr(s.indexOf('*') + 1));
};
