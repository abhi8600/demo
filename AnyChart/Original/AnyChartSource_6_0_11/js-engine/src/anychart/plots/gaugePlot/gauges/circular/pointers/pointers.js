/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.circular.pointers');
goog.require('anychart.plots.gaugePlot.gauges.circular.pointers.styles');
goog.require('anychart.plots.gaugePlot.gauges.circular.drawers');

//------------------------------------------------------------------------------
//
//                          CircularGaugePointer class
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer);

/**
 * @override
 * @inheritDoc
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer.prototype.getTokenValue = function (token) {
    if (token == '%Value') return this.value_;
    goog.base(this, 'getTokenValue', token);
};
/**
 * ND: Needs doc!
 * @protected
 * @param {uint} anchor
 * @param {anychart.utils.geom.Point} anchorPos
 * @param {Number} w
 * @param {Number} h
 * @param {Number} r
 * @param {Number} angle
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer.prototype.getRectBasedAnchorPoint = function (anchor, anchorPos, w, h, r, angle) {

    var leftTop = new anychart.utils.geom.Point();
    var rightTop = new anychart.utils.geom.Point();
    var leftBottom = new anychart.utils.geom.Point();
    var rightBottom = new anychart.utils.geom.Point();

    var sin = Math.sin(angle * Math.PI / 180);
    var cos = Math.cos(angle * Math.PI / 180);

    var center = this.gauge_.getPixPivotPoint();
    var c1x = center.x + (w / 2) * sin;
    var c1y = center.y - (w / 2) * cos;
    var c2x = center.x - (w / 2) * sin;
    var c2y = center.y + (w / 2) * cos;

    leftBottom.x = c1x + (r - h / 2) * cos;
    leftBottom.y = c1y + (r - h / 2) * sin;

    rightTop.x = c2x + (r + h / 2) * cos;
    rightTop.y = c2y + (r + h / 2) * sin;

    leftTop.x = c1x + (r + h / 2) * cos;
    leftTop.y = c1y + (r + h / 2) * sin;

    rightBottom.x = c2x + (r - h / 2) * cos;
    rightBottom.y = c2y + (r - h / 2) * sin;

    var mAnchor = anychart.layout.Anchor;

    switch (anchor) {
        case mAnchor.CENTER:
            anchorPos.x = (leftTop.x + rightBottom.x) / 2;
            anchorPos.y = (leftTop.y + rightBottom.y) / 2;
            break;
        case mAnchor.CENTER_BOTTOM:
            anchorPos.x = (leftBottom.x + rightBottom.x) / 2;
            anchorPos.y = (leftBottom.y + rightBottom.y) / 2;
            break;
        case mAnchor.CENTER_LEFT:
            anchorPos.x = (leftTop.x + leftBottom.x) / 2;
            anchorPos.y = (leftTop.y + leftBottom.y) / 2;
            break;
        case mAnchor.CENTER_RIGHT:
            anchorPos.x = (rightBottom.x + rightTop.x) / 2;
            anchorPos.y = (rightBottom.y + rightTop.y) / 2;
            break;
        case mAnchor.CENTER_TOP:
            anchorPos.x = (leftTop.x + rightTop.x) / 2;
            anchorPos.y = (leftTop.y + rightTop.y) / 2;
            break;
        case mAnchor.LEFT_BOTTOM:
            anchorPos.x = leftBottom.x;
            anchorPos.y = leftBottom.y;
            break;
        case mAnchor.LEFT_TOP:
            anchorPos.x = leftTop.x;
            anchorPos.y = leftTop.y;
            break;
        case mAnchor.RIGHT_BOTTOM:
            anchorPos.x = rightBottom.x;
            anchorPos.y = rightBottom.y;
            break;
        case mAnchor.RIGHT_TOP:
            anchorPos.x = rightTop.x;
            anchorPos.y = rightTop.y;
            break;
    }
};
/**@override*/
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer.prototype.changeValue = function(event, mouseX, mouseY) {
    var pt = new anychart.utils.geom.Point(mouseX, mouseY);
    var clientPosition = this.sprite_.getClientPosition();
    pt.x -= clientPosition.x;
    pt.y -= clientPosition.y;
    pt.x -= this.getGauge().getPixPivotPoint().x;
    pt.y -= this.getGauge().getPixPivotPoint().y;

    var angle = Math.atan2(pt.y, pt.x) * 180/Math.PI;
    while (angle < this.getAxis().getScale().getPixelRangeMinimum()) {
        angle += 360;
    }
    while (angle > this.getAxis().getScale().getPixelRangeMaximum()) {
        angle -= 360;
    }
    this.value_ = this.axis_.getScale().transformPixelToValue(angle);
    this.updateFixedValue(this.value_);
};
//------------------------------------------------------------------------------
//
//                          CircularGaugePointerWithCap class
//
//------------------------------------------------------------------------------
/**
 * Class represents pointer with cap sprite
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap,
    anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer);

/**
 * Sprite for pointer and cap.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap.prototype.fullSprite_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap.prototype.getFullSprite = function () {
    return this.fullSprite_;
};

/**
 * Sprite for cap.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap.prototype.capSprite_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap.prototype.getCapSprite = function () {
    return this.capSprite_;
};

/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap.prototype.initialize = function (svgManager) {
    this.fullSprite_ = new anychart.svg.SVGSprite(svgManager);
    this.capSprite_ = new anychart.svg.SVGSprite(svgManager);

    goog.base(this, 'initialize', svgManager);

    if (IS_ANYCHART_DEBUG_MOD) {
        this.fullSprite_.setId('CircularGaugePointerWithCap');
        this.capSprite_.setId('CapSprite');
    }
};


//------------------------------------------------------------------------------
//
//                          NeedleCircularGaugePointer class
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer = function () {
    goog.base(this);
    this.drawer_ = new anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer();
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer,
    anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap);

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.drawer_ = null;
/**
 * ND: Needs doc!
 */
/**
 * ND: Needs doc!
 * @return {anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.getDrawer = function () {
    return this.drawer_;
};


/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.getStyleNodeName = function () {
    return 'needle_pointer_style';
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle();
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('NeedleCircularGaugePointer');
    this.fullSprite_.appendSprite(this.sprite_);
    this.fullSprite_.appendSprite(this.capSprite_);
    return this.fullSprite_;
};

/**
 * ND: Needs doc!
 * @param {int} anchor
 * @param {anychart.utils.geom.Point} anchorPos
 */
anychart.plots.gaugePlot.gauges.circular.pointers.NeedleCircularGaugePointer.prototype.getAnchorPoint = function (anchor, anchorPos) {
    var angle = this.axis_.getScale().transformValueToPixel(this.fixValueScaleOut(this.value_));

    var outerR = Math.max(this.drawer_.getPixelBaseRadius(), this.drawer_.getPixelPointRadius(), this.drawer_.getPixelRadius());
    var innerR = Math.min(this.drawer_.getPixelBaseRadius(), this.drawer_.getPixelPointRadius(), this.drawer_.getPixelRadius());
    var w = Math.max(this.drawer_.getPixelPointThickness(), this.drawer_.getPixelThickness());

    this.getRectBasedAnchorPoint(anchor, anchorPos, w, (outerR - innerR), (innerR + outerR) / 2, angle);
};

//------------------------------------------------------------------------------
//
//                          BarCircularGaugePointer class
//
//------------------------------------------------------------------------------
/**
 * Class representing bar circular gauge pointer
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer,
    anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer);

/**
 * Start angle of the bar.
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.startAngle_ = null;

/**
 * End angle of the bar
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.endAngle_ = null;

/**
 * Inner radius of the bar.
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.innerRadius_ = null;

/**
 * Outer radius of the bar
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.outerRadius_ = null;


/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.getStyleNodeName = function () {
    return 'bar_pointer_style';
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle();
};

/**
 * ND: Needs doc!
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.getBarPath = function () {
    var axis = this.axis_,
        center = axis.getGauge().getPixPivotPoint(),
        /**
         * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState}
         */
            state = this.getCurrentStyleState(this.getStyle()),
        w = (state.isAxisSizeWidth() ? axis.size : 1) * state.getWidth(),
        startValue, endValue;
    if (this instanceof anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer) {
        startValue = this.fixValueScaleOut(this.value_);
        endValue = this.fixValueScaleOut(this.endValue_);
    } else {
        startValue = state.isStartFromZero() ? 0 : axis.getScale().getMinimum();
        endValue = this.fixValueScaleOut(this.value_);
    }

    this.startAngle_ = axis.getScale().transformValueToPixel(startValue);
    this.endAngle_ = axis.getScale().transformValueToPixel(endValue);

    var sweepAngle = Math.abs(this.startAngle_ - this.endAngle_);

    this.innerRadius_ = axis.getComplexRadius(state.getAlign(), state.getPadding(), w, true);
    this.outerRadius_ = axis.getComplexRadius(state.getAlign(), state.getPadding(), w, false);

    var startAngle, endAngle, min;
    if (this.endAngle_ % 360 != this.startAngle_ % 360) {
        endAngle = (this.endAngle_ + 0.5) * Math.PI / 180;
        startAngle = this.startAngle_ * Math.PI / 180;
    } else {
        // В случае одной точки - криво рассчитываются координаты при изменении start_angle
        // для корректного рассчета сделано чтобы точка рисовалась по координатам как будто бы
        // она рисуется от угла=0 до угла=360
        min = Math.min(this.startAngle_, this.endAngle_);
        endAngle = (this.endAngle_ - min - 0.001) * Math.PI / 180;
        startAngle = (this.startAngle_ - min) * Math.PI / 180
    }

    var innerStartX = center.x + this.innerRadius_ * Math.cos(startAngle),
        innerStartY = center.y + this.innerRadius_ * Math.sin(startAngle),
        innerEndX = center.x + this.innerRadius_ * Math.cos(endAngle),
        innerEndY = center.y + this.innerRadius_ * Math.sin(endAngle),

        outerStartX = center.x + this.outerRadius_ * Math.cos(startAngle),
        outerStartY = center.y + this.outerRadius_ * Math.sin(startAngle),
        outerEndX = center.x + this.outerRadius_ * Math.cos(endAngle),
        outerEndY = center.y + this.outerRadius_ * Math.sin(endAngle);

    var largeArc = 1, sweepArc = 1;
    if (sweepAngle < 180) {
        largeArc = 0;
    }
    var svgManager = this.getSVGManager(),
        path = '';
    path += svgManager.pMove(innerStartX, innerStartY);
    path += svgManager.pCircleArc(this.innerRadius_, largeArc, sweepArc, innerEndX, innerEndY, this.innerRadius_);
    path += svgManager.pLine(outerEndX, outerEndY);
    path += svgManager.pCircleArc(this.outerRadius_, largeArc, !sweepArc, outerStartX, outerStartY, this.outerRadius_);
    path += svgManager.pLine(innerStartX, innerStartY);
    //path += svgManager.pCircleArc(innerRadius, largeArc, !sweepArc, innerStartX, innerStartY, innerRadius);
    path += svgManager.pFinalize();


    /** fill and stroke  */

//    this.drawBox(g, center, startAngle, endAngle, innerRadius, outerRadius);

    return path;
};


/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer.prototype.getAnchorPoint = function (anchor, pos) {
    var center = this.axis_.getGauge().getPixPivotPoint(),
        angle, r,
        mAnchor = anychart.layout.Anchor;

    pos.x = center.x;
    pos.y = center.y;

    switch (anchor) {
        case mAnchor.CENTER:
            angle = (this.startAngle_ + this.endAngle_) / 2;
            r = (this.innerRadius_ + this.outerRadius_) / 2;
            break;
        case mAnchor.CENTER_BOTTOM:
            angle = (this.startAngle_ + this.endAngle_) / 2;
            r = this.innerRadius_;
            break;
        case mAnchor.CENTER_LEFT:
            angle = this.startAngle_;
            r = (this.innerRadius_ + this.outerRadius_) / 2;
            break;
        case mAnchor.CENTER_RIGHT:
            angle = this.endAngle;
            r = (this.innerRadius_ + this.outerRadius_) / 2;
            break;
        case mAnchor.CENTER_TOP:
            angle = (this.startAngle_ + this.endAngle) / 2;
            r = this.outerRadius_;
            break;
        case mAnchor.LEFT_BOTTOM:
            angle = this.startAngle_;
            r = this.innerRadius_;
            break;
        case mAnchor.LEFT_TOP:
            angle = this.startAngle_;
            r = this.outerRadius_;
            break;
        case mAnchor.RIGHT_BOTTOM:
            angle = this.endAngle_;
            r = this.innerRadius_;
            break;
        case mAnchor.RIGHT_TOP:
            angle = this.endAngle_;
            r = this.outerRadius_;
            break;
    }
    pos.x += r * Math.cos(angle * Math.PI / 180);
    pos.y += r * Math.sin(angle * Math.PI / 180);
};

//------------------------------------------------------------------------------
//
//                       RangeBarCircularGaugePointer class
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer,
    anychart.plots.gaugePlot.gauges.circular.pointers.BarCircularGaugePointer);

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer.prototype.endValue_ = null;


/**
 * ND: Needs doc!
 * @protected
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer.prototype.getStyleNodeName = function () {
    return 'range_bar_pointer_style';
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.RangeBarCircularGaugePointer.prototype.deserialize = function (data, styleList, mainChartView) {
    data['value'] = data['start'];
    goog.base(this, 'deserialize', data, styleList, mainChartView);
    this.editable_ = false;
    this.endValue_ = anychart.utils.deserialization.getNumProp(data, 'end');
};
//------------------------------------------------------------------------------
//
//                       MarkerCircularGaugePointer class
//
//------------------------------------------------------------------------------
/**
 * Marker circular gauge pointer.
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer,
    anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap);
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.markerPos_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.getMarkerPos = function () {
    return this.markerPos_;
};
/** ND: Needs doc!
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.markerSprite_ = null;
/**
 * ND: Needs doc!
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.getMarkerSprite = function () {
    return this.markerSprite_;
};
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.initialize = function (svgManager) {
    this.markerSprite_ = new anychart.svg.SVGSprite(svgManager);
    goog.base(this, 'initialize', svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.markerSprite_.setId('MarkerPointerMarkerSprite');
    this.sprite_.appendSprite(this.markerSprite_);
    this.sprite_.appendSprite(this.capSprite_);
    return this.sprite_;
};
/**
 * @param {int} anchor
 * @param {anychart.utils.geom.Point} anchorPos
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.getAnchorPoint = function (anchor, anchorPos) {
    var mAlign = anychart.plots.gaugePlot.layout.Align,
        state = this.currentState_,
        marker = state.getMarker(),
        axis = this.axis_;

    var percentW = (marker.isWidthAxisSizeDepend() ? axis.size : 1) * marker.getWidth();
    var percentH = (marker.isHeightAxisSizeDepend() ? axis.size : 1) * marker.getHeight();

    var w = axis.getPixelRadius(percentW);
    var h = axis.getPixelRadius(percentH);

    var r = axis.getRadius(state.getAlign(), state.getPadding(), 0, false);
    switch (state.getAlign()) {
        case mAlign.INSIDE:
            r -= w / 2;
            break;
        case mAlign.OUTSIDE:
            r += w / 2;
            break;
    }

    var pos = new anychart.utils.geom.Point();
    var angle = axis.getScale().transformValueToPixel(this.fixValueScaleOut(this.value_));
    axis.setPixelPoint(r, angle, pos);

    this.getRectBasedAnchorPoint(anchor, anchorPos, w, h, r, angle);
};

/**
 * Calculate bounds.
 */
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.calculateBounds = function () {
    var mAlign = anychart.plots.gaugePlot.layout.Align,
        state = this.getCurrentState(),
        marker = state.getMarker(),
        axis = this.getAxis(),
        percentW = (marker.isWidthAxisSizeDepend() ? axis.size : 1) * marker.getWidth(),
        percentH = (marker.isHeightAxisSizeDepend() ? axis.size : 1) * marker.getHeight(),
        w = axis.getPixelRadius(percentW),
        h = axis.getPixelRadius(percentH),
        r = axis.getRadius(state.getAlign(), state.getPadding(), 0, false);

    switch (state.getAlign()) {
        case mAlign.INSIDE:
            r -= w / 2;
            break;
        case mAlign.OUTSIDE:
            r += w / 2;
            break;
    }

    var pos = new anychart.utils.geom.Point(),
        angle = axis.getScale().transformValueToPixel(this.value_);
    axis.setPixelPoint(r, angle, pos);

    this.markerPos_ = new anychart.utils.geom.Point(-w / 2, -h / 2);

    this.bounds_.x = pos.x;
    this.bounds_.y = pos.y;
    this.bounds_.width = w;
    this.bounds_.height = h;

    var rotation = 0;
    if (marker.isAutoRotate()) {
        switch (state.getAlign()) {
            case mAlign.INSIDE:
            case mAlign.CENTER:
                rotation = angle + 90;
                break;
            case mAlign.OUTSIDE:
                rotation = angle - 90;
                break;
        }
    }
    this.markerSprite_.setRotation(rotation + marker.getRotation());
    this.markerSprite_.setX(pos.x);
    this.markerSprite_.setY(pos.y);
};


/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.getStyleNodeName = function () {
    return "marker_pointer_style";
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.MarkerCircularGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle();
};

//------------------------------------------------------------------------------
//
//                       KnobCircularGaugePointer class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer = function () {
    goog.base(this);
    this.drawer_ = new anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer();
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer,
    anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointerWithCap);
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.drawer_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.getDrawer = function() {
    return this.drawer_;
};


/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.knobSprite_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.getKnobSprite = function() {
    return this.knobSprite_
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.getStyleNodeName = function () {
    return "knob_pointer_style";
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState;
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle();
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.KnobCircularGaugePointer.prototype.initialize = function(svgManager) {
    this.knobSprite_ = new anychart.svg.SVGSprite(svgManager);
    goog.base(this, 'initialize', svgManager);
    if (IS_ANYCHART_DEBUG_MOD) {
        this.sprite_.setId('KnobCircularGaugePointer');
        this.knobSprite_.setId('KnobSprite');
    }
    this.fullSprite_.appendSprite(this.knobSprite_);
    this.fullSprite_.appendSprite(this.sprite_);
    this.fullSprite_.appendSprite(this.capSprite_);
    return this.fullSprite_;
};
