/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark');
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark = function () {
    this.background_ = new anychart.visual.background.Background();
    this.shapeDrawer_ = new anychart.plots.gaugePlot.visual.GaugeShapeDrawer();
};

/**
 * @private
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.background_ = null;

/**
 * @return {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getBackground = function () {
    return this.background_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.padding_ = 0;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getPadding = function () {
    return this.padding_;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.enabled_ = null;

/**
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.isEnabled = function () {
    return this.enabled_;
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.align_ = anychart.plots.gaugePlot.layout.Align.CENTER;

/**
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getAlign = function () {
    return this.align_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.length_ = .05;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getLength = function () {
    return this.length_;
};

/**
 * @param {number} length
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.setLength = function (length) {
    this.length_ = length;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.width_ = .05;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getWidth = function () {
    return this.width_;
};

/**
 * @param {number} width
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.setWidth = function (width) {
    this.width_ = width;
};
/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.height_ = .05;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getHeight = function () {
    return this.height_;
};

/**
 * @param {number} height
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.setHeight = function (height) {
    this.height_ = height;
};
/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.rotation_ = 0;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getRotation = function () {
    return this.rotation_;
};
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.setRotation = function (value) {
    this.rotation_ = value;
};
/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.autoRotate_ = true;

/**
 * @private
 * @type {anychart.plots.gaugePlot.visual.GaugeShapeType}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.shape_ = 0;

/**
 * @return {anychart.plots.gaugePlot.visual.GaugeShapeType}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.getShapeType = function () {
    return this.shape_;
};

/**
 * @param {anychart.plots.gaugePlot.visual.GaugeShapeType} shapeType
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.setShapeType = function (shapeType) {
    this.shape_ = shapeType;
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.visual.GaugeShapeDrawer}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.shapeDrawer_ = null;

/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {number} x
 * @param {number} y
 * @param {number} size
 * @param {number} width
 * @param {number} height
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.drawTickmark = function (svgManager, x, y, size, width, height, opt_color) {
    var sprite = new anychart.visual.background.SVGBackgroundSprite(svgManager,
        svgManager.createPath(),
        svgManager.createPath());
    if(IS_ANYCHART_DEBUG_MOD) sprite.setId('AxisTickmarkSprite');

    var bounds = new anychart.utils.geom.Rectangle(x, y, width, height);
    var path = this.shapeDrawer_.getSVGPathData(
        this.shape_,
        svgManager,
        size,
        x,
        y,
        width,
        height,
        this.background_.hasBorder() ? this.background_.getBorder().getThickness() : 1);

    var style = "";
    if (this.background_.hasFill())
        style += this.background_.getFill().getSVGFill(svgManager, bounds, opt_color);
    else
        style += svgManager.getEmptySVGFill();
    if (this.background_.hasBorder())
        style += this.background_.getBorder().getSVGStroke(svgManager, bounds, opt_color);
    else
        style += svgManager.getEmptySVGStroke();

    sprite.getFillSegment().setAttribute('style', style);

    style = '';
    if (this.background_.hasHatchFill())
        style += this.background.getHatchFill().getSVGHatchFill(svgManager, null, opt_color);
    else
        style += svgManager.getEmptySVGStyle();
    sprite.getHatchFillSegment().setAttribute('style', style);

    sprite.getFillSegment().setAttribute('d', path);
    sprite.getHatchFillSegment().setAttribute('d', path);
    return sprite;
};

/**
 * @param {Object} config
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark.prototype.deserialize = function (config, axisSize) {
    this.background_.deserialize(config);
    var des = anychart.utils.deserialization;

    if (!this.background_.isEnabled()) return;

    if (des.hasProp(config, 'enabled')) {
        this.enabled_ = des.getBoolProp(config, 'enabled')
    }

    if (des.hasProp(config, 'padding'))
        this.padding_ = des.getSimplePercentProp(config, 'padding');

    if (des.hasProp(config, 'length'))
        this.length_ = anychart.plots.gaugePlot.deserializationUtils.getAxisSizeDependProp(config, 'length', axisSize);

    if (des.hasProp(config, 'width'))
        this.width_ = anychart.plots.gaugePlot.deserializationUtils.getAxisSizeDependProp(config, 'width', axisSize);

    if (des.hasProp(config, 'shape'))
        this.shape_ = anychart.plots.gaugePlot.visual.GaugeShapeType.deserialize(
            des.getLStringProp(config, 'shape'));

    if (des.hasProp(config, 'align'))
        this.align_ = anychart.plots.gaugePlot.deserializationUtils.getAlignProp(config, 'align');

    if (des.hasProp(config, 'rotation'))
        this.rotation_ = des.getNumProp(config, 'rotation');

    if (des.hasProp(config, 'auto_rotate'))
        this.autoRotate_ = des.getBoolProp(config, 'auto_rotate');

    if (!anychart.plots.gaugePlot.visual.GaugeShapeType.shapeIsRectangle(this.shape_))
        this.length_ = this.width_ = Math.min(this.width_, this.length_);

    if (this.shape_ == anychart.plots.gaugePlot.visual.GaugeShapeType.LINE) {
        this.background_.setFill(null);
        this.background_.setHatchFill(null);
    }
};