goog.provide('anychart.plots.gaugePlot.gauges.axisBased.axis');

goog.require('anychart.plots.gaugePlot.layout');
goog.require('anychart.plots.gaugePlot.deserializationUtils');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.scales');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.axis.markers');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @implements {anychart.formatting.ITextFormatInfoProvider}
 * @param {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge}
    */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis = function (gauge) {
    this.gauge = gauge;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.gauge = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getGauge = function () {
    return this.gauge;
};

/**
 * @private
 * @type {string}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.name_ = null;

/**
 * Sets gauge axis name.
 * @param {string} name Gauge axis name.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.setName = function (name) {
    this.name_ = name;
};

/**
 * @return {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.hasName = function () {
    return this.name_ != null && this.name_.length > 0;
};

/**
 * @return {string}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getName = function () {
    return this.name_;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.scaleBar = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.extraScaleBar = null;

/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.enabled = true;

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.size = 0;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getSize = function () {
    return this.size;
};

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.pixelSize = null;

/**
 * @protected
 * @type {anychart.scales.BaseScale}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.scale = null;
/**
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getScale = function () {
    return this.scale;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.majorTickmark = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.minorTickmark = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.labels = null;

/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.sprite = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.minorTickmarksSprite_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.majorTickmarksSprite_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.labelsSprite_ = null;
//------------------------------------------------------------------------------
//                  AxisMarkers.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.markersList_ = null;
/**
 * @protected
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.createMarkersList = function () {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.markers.AxisMarkersList();
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.initialize = function (svgManager) {
    this.sprite = new anychart.svg.SVGSprite(svgManager);
    //scale bars
    if (this.scaleBar)
        this.sprite.appendSprite(this.scaleBar.initialize(svgManager));
    if (this.extraScaleBar)
        this.sprite.appendSprite(this.extraScaleBar.initialize(svgManager));
    //tickmarks
    if (this.minorTickmark) {
        this.minorTickmarksSprite_ = new anychart.svg.SVGSprite(svgManager);
        this.sprite.appendSprite(this.minorTickmarksSprite_);
    }
    if (this.majorTickmark) {
        this.majorTickmarksSprite_ = new anychart.svg.SVGSprite(svgManager);
        this.sprite.appendSprite(this.majorTickmarksSprite_);
    }
    if (this.labels && this.labels.isEnabled()) {
        this.labelsSprite_ = new anychart.svg.SVGSprite(svgManager);
        this.sprite.appendSprite(this.labelsSprite_);
    }
    //markers
    if (this.markersList_)
        this.sprite.appendSprite(this.markersList_.initialize(svgManager));

    return this.sprite;
};

/**
 * Initialize axis size.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.initializeSize = function () {
};

/**
 * Draw axis.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.draw = function () {
    if (this.scaleBar)
        this.scaleBar.draw();
    if (this.extraScaleBar)
        this.extraScaleBar.draw();
    if (this.minorTickmark && this.minorTickmark.isEnabled())
        this.drawMinorTickmarks_();
    if (this.majorTickmark && this.majorTickmark.isEnabled())
        this.drawMajorTickmarks_();
    if (this.labels && this.labels.isEnabled())
        this.drawLabels_();
    if (this.markersList_)
        this.markersList_.draw();
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.drawMinorTickmarks_ = function () {
    var min = this.scale.linearize(this.scale.getMinimum());
    var max = this.scale.linearize(this.scale.getMaximum());
    for (var i = 0; i <= this.scale.getMajorIntervalCount(); i++) {
        var majorValue = this.scale.getMajorIntervalValue(i);

        for (var j = 1; j < this.scale.getMinorIntervalCount(); j++) {
            var minorValue = this.scale.getMinorIntervalValue(majorValue, j);
            if (minorValue < min || minorValue > max) continue;
            this.minorTickmarksSprite_.appendSprite(this.drawTickmark(this.minorTickmark, this.scale.localTransform(minorValue)));
        }
    }
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.drawMajorTickmarks_ = function () {
    for (var i = 0; i <= this.scale.getMajorIntervalCount(); i++) {
        this.majorTickmarksSprite_.appendSprite(this.drawTickmark(this.majorTickmark, this.scale.localTransform(this.scale.getMajorIntervalValue(i))));
    }
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.drawLabels_ = function () {
    var start = this.labels.isFirstVisible() ? 0 : 1;
    var count = this.scale.getMajorIntervalCount();
    var end = this.labels.isLastVisible() ? count : count - 1;

    var svgManager = this.sprite.getSVGManager();

    for (var i = start; i <= end; i++) {
        this.tmpValue_ = this.scale.getMajorIntervalValue(i);
        this.setLabelRotation(this.tmpValue_);
        this.labels.initBounds(svgManager);
        var pos = this.getLabelPosition(this.scale.localTransform(this.tmpValue_));
        this.labelsSprite_.appendSprite(this.labels.draw(svgManager, pos));
        this.resetLabelRotation(this.tmpValue_);
    }
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getTokenValue = function (token) {
    if (token == '%Value')
        return this.scale.delinearize(this.tmpValue_);
    return undefined;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getTokenType = function (token) {
    if (token == '%Value')
        return anychart.formatting.TextFormatTokenType.NUMBER;
    return anychart.formatting.TextFormatTokenType.UNKNOWN;
};

/**
 * @param {number} pixValue
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.setLabelRotation = function (pixValue) {
};

/**
 * @param {number} pixValue
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.resetLabelRotation = function (pixValue) {
};

/**
 * @param {number} pixValue
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getLabelPosition = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark} tickmark
 * @param {number} pixValue
 * @return {anychart.svg.SVGSprtie}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.drawTickmark = goog.abstractMethod;

/**
 * Draw scale bar.
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar} scaleBar
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getScaleBarPath = goog.abstractMethod;

/**
 * Gets scale bar bounds.
 * @protected
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar} scaleBar
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.getScaleBarBounds = goog.abstractMethod;

/**
 * Resize axis.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.resize = function () {
    this.initializeSize();
    if (this.majorTickmarksSprite_) this.majorTickmarksSprite_.clear();
    if (this.minorTickmarksSprite_) this.minorTickmarksSprite_.clear();
    if (this.labelsSprite_) this.labelsSprite_.clear();
    this.draw();
    if (this.markersList_)
        this.markersList_.resize();
};

/**
 * @param {Object} config
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.deserialize = function (config, stylesList) {
    if (!config) {
        this.scale = new anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale();
        return;
    }
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'enabled'))
        this.enabled = des.getBoolProp(config, 'enabled');
    if (des.hasProp(config, 'size'))
        this.size = des.getSimplePercentProp(config, 'size');
    if (des.hasProp(config, 'scale')) {
        if (des.hasProp(des.getProp(config, 'scale'), 'type') &&
            des.getLStringProp(des.getProp(config, 'scale'), 'type') == 'logarithmic')
            this.scale = new anychart.plots.gaugePlot.gauges.axisBased.scales.LogScale();
        else
            this.scale = new anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale();
        this.scale.deserialize(des.getProp(config, 'scale'));
    } else {
        this.scale = new anychart.plots.gaugePlot.gauges.axisBased.scales.LinearScale();
    }
    if (!this.enabled) return;
    if (des.hasProp(config, 'scale_bar')) {
        this.scaleBar = new anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar(this, false);
        this.scaleBar.deserialize(des.getProp(config, 'scale_bar'), this.size);
        this.gauge.setBackgroundRotation(this.scaleBar.getBackground());
    }
    if (des.hasProp(config, 'scale_line')) {
        this.extraScaleBar = new anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar(this, true);
        this.extraScaleBar.deserialize(des.getProp(config, 'scale_line'), this.size);
        this.gauge.setBackgroundRotation(this.extraScaleBar.getBackground());
    }
    if (des.hasProp(config, 'major_tickmark')) {
        this.majorTickmark = new anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark();
        this.majorTickmark.deserialize(des.getProp(config, 'major_tickmark'), this.size);
        this.gauge.setBackgroundRotation(this.majorTickmark.getBackground());
    }
    if (des.hasProp(config, 'minor_tickmark')) {
        this.minorTickmark = new anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark();
        this.minorTickmark.deserialize(des.getProp(config, 'minor_tickmark'), this.size);
        this.gauge.setBackgroundRotation(this.minorTickmark.getBackground());
    }
//    this.labels = new anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels(this);
    this.labels = this.createLabels(this);
    if (des.hasProp(config, 'labels'))
        this.labels.deserialize(des.getProp(config, 'labels'), this.size);

    //markers
    this.markersList_ = this.createMarkersList();
    this.markersList_.setAxis(this);
    this.markersList_.setScale(this.scale);
    this.markersList_.deserialize(config, stylesList, this.size);
};
/**
 * Method that creates gauge axis labels.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.createLabels = goog.abstractMethod;

//------------------------------------------------------------------------------
//                  Check.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer} pointer
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.checkPointer = function (pointer) {
    this.checkScale_(pointer.getValue());
    if (pointer.getEndValue) this.checkScale_(pointer.getEndValue());
};
/**
 * ND: Needs doc!
 * @private
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis.prototype.checkScale_ = function (value) {
    if (isNaN(this.scale.getDataRangeMinimum()) || value < this.scale.getDataRangeMinimum())
        this.scale.setDataRangeMinimum(value);
    if (isNaN(this.scale.getDataRangeMaximum()) || value > this.scale.getDataRangeMaximum())
        this.scale.setDataRangeMaximum(value);
};
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType enum
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType = {
    LINE:0,
    BAR:1
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} axis
 * @param {boolean} extra
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar = function (axis, extra) {
    this.axis_ = axis;
    this.extra_ = extra;
    this.background_ = new anychart.visual.background.Background();
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.axis_ = null;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.extra_ = false;

/**
 * @return {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.isExtra = function () {
    return this.extra_;
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.shape_ = anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;

/**
 * @return {anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.getShapeType = function () {
    return this.shape_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.size_ = 0.05;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.getSize = function () {
    return this.size_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.padding_ = 0;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.getPadding = function () {
    return this.padding_;
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.align_ = anychart.plots.gaugePlot.layout.Align.CENTER;

/**
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.getAlign = function () {
    return this.align_;
};

/**
 * @private
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.background_ = null;

/**
 * @return {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.getBackground = function () {
    return this.background_;
};

/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.sprite_ = null;

/**
 * @param {anychart.svg.SVGManager}
    */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.initialize = function (svgManager) {
    this.sprite_ = new anychart.visual.background.SVGBackgroundSprite(svgManager,
        svgManager.createPath(),
        svgManager.createPath());
    this.sprite_.getElement().setAttribute('id', 'scaleBar');
    return this.sprite_;
};

/**
 * Draw scale bar
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.draw = function () {
    var bounds = this.axis_.getScaleBarBounds(this);
    var svgManager = this.sprite_.getSVGManager();
    var style = "";
    if (this.background_.hasFill())
        style += this.background_.getFill().getSVGFill(svgManager, bounds);
    else
        style += svgManager.getEmptySVGFill();
    if (this.background_.hasBorder())
        style += this.background_.getBorder().getSVGStroke(svgManager, bounds);
    else
        style += svgManager.getEmptySVGStroke();

    this.sprite_.getFillSegment().setAttribute('style', style);

    style = '';
    if (this.background_.hasHatchFill())
        style += this.background_.getHatchFill().getSVGHatchFill(svgManager, bounds);
    else
        style += svgManager.getEmptySVGStyle();
    this.sprite_.getHatchFillSegment().setAttribute('style', style);

    var path = this.axis_.getScaleBarPath(svgManager, this);
    this.sprite_.getFillSegment().setAttribute('d', path);
    this.sprite_.getHatchFillSegment().setAttribute('d', path);
};


/**
 * @param {Object} config
 * @param {number} axisSize
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBar.prototype.deserialize = function (config, axisSize) {
    if (this.extra_) {
        this.shape_ = anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.LINE;
        if (this.background_.hasFill())
            this.background_.getFill().setEnabled(false);
        this.background_.deserialize({
            'border':config
        });
    } else {
        if (anychart.utils.deserialization.hasProp(config, 'shape')) {
            switch (anychart.utils.deserialization.getLStringProp(config, 'shape')) {
                case 'line':
                    this.shape_ = anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.LINE;
                    break;
                case 'bar':
                    this.shape_ = anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;
                    break;
            }
        }
        this.background_.deserialize(config);
    }
    if (anychart.utils.deserialization.hasProp(config, 'size'))
        this.size_ = anychart.plots.gaugePlot.deserializationUtils.getAxisSizeDependProp(config, 'size', axisSize);
    if (anychart.utils.deserialization.hasProp(config, 'padding'))
        this.padding_ = anychart.utils.deserialization.getSimplePercentProp(config, 'padding');
    if (anychart.utils.deserialization.hasProp(config, 'align'))
        this.align_ = anychart.plots.gaugePlot.deserializationUtils.getAlignProp(config, 'align');
};
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels = function (axis) {
    this.textElement_ = new anychart.visual.text.TextElement();
    this.textElement_.setEnabled(false);
    this.axis_ = axis;
};

/**
 * @private
 * @type {int}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.align_ = anychart.plots.gaugePlot.layout.Align.INSIDE;

/**
 * @return {int}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.getAlign = function () {
    return this.align_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.padding_ = .02;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.getPadding = function () {
    return this.padding_;
};

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.showFirst_ = true;

/** @return {boolean} */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.isFirstVisible = function () {
    return this.showFirst_;
};

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.showLast_ = true;

/** @return {boolean} */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.isLastVisible = function () {
    return this.showLast_;
};

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.axis_ = null;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.isDynamicFontSize_ = false;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.percentFontSize_ = NaN;

/**
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.textElement_ = null;

/**
 * @return {anychart.visual.text.TextElement}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.getTextElement = function () {
    return this.textElement_;
};

/**
 * @return {boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.isEnabled = function () {
    return this.textElement_ && this.textElement_.isEnabled();
};

/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.textFormatter_ = null;

/**
 * @param {object} config
 * @param {number} axisSize
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.deserialize = function (config, axisSize) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(config, 'font')) {
        var fontConfig = des.getProp(config, 'font');
        if (des.hasProp(fontConfig, 'size')) {
            des.setProp(fontConfig, 'size',
                String(anychart.plots.gaugePlot.deserializationUtils.getAxisSizeDependProp(fontConfig, 'size', axisSize) * 100));
        }
    }
    this.textElement_.deserialize(config);
    if (des.hasProp(config, 'enabled'))
        this.textElement_.setEnabled(des.getBoolProp(config, 'enabled'));

    if (des.hasProp(config, 'format')) {
        var format = des.getStringProp(config, 'format');
        this.textFormatter_ = new anychart.formatting.TextFormatter(format);
    }
    if (des.hasProp(config, 'align')) this.align_ = anychart.plots.gaugePlot.deserializationUtils.getAlignProp(config, 'align');
    if (des.hasProp(config, 'padding')) this.padding_ = des.getSimplePercentProp(config, 'padding');
    if (des.hasProp(config, 'show_first')) this.showFirst_ = des.getBoolProp(config, 'show_first');
    if (des.hasProp(config, 'show_last')) this.showLast_ = des.getBoolProp(config, 'show_last');
};

/**
 * @private
 * @return {string}
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.getActualText_ = function () {
    return this.textFormatter_.getValue(this.axis_, null);
};

/**
 * @param {anychart.svg.SVGManager}
    */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.initBounds = function (svgManager) {
    this.textElement_.initSize(svgManager, this.getActualText_());
};

/**
 * Draw label.
 */
anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels.prototype.draw = function (svgManager, pos) {
    var labelSprite = this.textElement_.createSVGText(svgManager);
    labelSprite.setPosition(pos.x, pos.y);
    return labelSprite;
};
