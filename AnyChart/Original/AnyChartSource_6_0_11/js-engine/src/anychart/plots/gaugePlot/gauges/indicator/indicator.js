goog.provide('anychart.plots.gaugePlot.gauges.indicator');

goog.require('anychart.plots.gaugePlot.frames.linear');
goog.require('anychart.plots.gaugePlot.gauges.indicator.styles');
goog.require('anychart.plots.gaugePlot.elements');

/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge,
    anychart.plots.gaugePlot.gauges.GaugeBase);

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer}
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.pointer_ = null;


/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.serialize = function (res) {
    res = goog.base(this, 'serialize', res);
    if (this.pointer_)
        res['pointer'] = this.pointer_.serialize();
    return res;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.createFrame = function () {
    return new anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.setBounds = function (newBounds) {
    goog.base(this, 'setBounds', newBounds);
    this.frame.autoFit(this.bounds);
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var prop = anychart.utils.deserialization.getProp(data, 'marker');
    if (prop) {
        this.pointer_ = new anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer();
        this.pointer_.setGauge(this);
        if (this.pointer_.label_ != null)
            this.pointer_.label_.setGauge(this);
        this.pointer_.deserialize(prop, this.stylesList, this.plot.mainChartView_)
    }
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.initialize = function (svgManager, bounds, tooltipsContainer) {
    var sprite = goog.base(this, 'initialize', svgManager, bounds, tooltipsContainer);
    if (this.pointer_ != null)
        //TODO: придумать что-нить чтобы убрать этот dirty-hack
        sprite.childrens_[2].appendSprite(this.pointer_.initialize(svgManager));
    return sprite;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.setPointerValue = function (value, opt_pointerName) {
    if (this.pointer_)
        this.pointer_.updateValue(value);
};

anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.draw = function () {
    goog.base(this, 'draw');
    if (this.pointer_ != null)
        this.pointer_.draw();
};

anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge.prototype.resize = function (newBounds, setBounds) {
    if (setBounds == undefined) setBounds = true;
    goog.base(this, 'resize', newBounds, setBounds);
    if (this.pointer_ != null)
        this.pointer_.resize();
};


/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}
 *
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer = function () {
    goog.base(this);
    this.selectable_ = true;
    this.editable_ = false;
    this.useHandCursor_ = true;

};
goog.inherits(anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer);
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.getStyleNodeName = function () {
    return 'indicator_pointer_style';
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.getInlineStyleName = function () {
    return null;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.createStyle = function () {
    return new anychart.plots.gaugePlot.gauges.indicator.styles.IndicatorPointerStyle();
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.width_ = .1;

/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.height_ = .1;

/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.x_ = .5;

/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.y_ = .5;

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.deserialize = function (data, styleList, mainChartView) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'color')) {
        this.color_ = des.getColorProp(data, 'color');
        delete data['color'];
    }

    if (des.hasProp(data, 'label'))
        this.label_ = new anychart.plots.gaugePlot.elements.GaugeLabel();
    if (des.hasProp(data, 'tooltip'))
        this.tooltip_ = new anychart.plots.gaugePlot.elements.GaugeTooltip();

    goog.base(this, 'deserialize', data, styleList, mainChartView);

    if (des.hasProp(data, 'width'))
        this.width_ = des.getSimplePercentProp(data, 'width');
    if (des.hasProp(data, 'height'))
        this.height_ = des.getSimplePercentProp(data, 'height');
    if (des.hasProp(data, 'x'))
        this.x_ = des.getSimplePercentProp(data, 'x');
    if (des.hasProp(data, 'y'))
        this.y_ = des.getSimplePercentProp(data, 'y');
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.indicator.IndicatorPointer.prototype.calculateBounds = function () {
    var gaugeBounds = this.gauge_.bounds;

    var shape = this.getCurrentState().getMarker().getShape();
    this.bounds_.x = gaugeBounds.x + this.x_ * gaugeBounds.width;
    this.bounds_.y = gaugeBounds.y + this.y_ * gaugeBounds.height;
    this.bounds_.width = gaugeBounds.width * this.width_;
    this.bounds_.height = gaugeBounds.height * this.height_;

    if (!anychart.plots.gaugePlot.visual.GaugeShapeType.shapeIsRectangle(shape)) {
        var min = Math.min(this.bounds_.width, this.bounds_.height);
        this.bounds_.width = min;
        this.bounds_.height = min;
    }

    this.bounds_.x -= this.bounds_.width / 2;
    this.bounds_.y -= this.bounds_.height / 2;
};
