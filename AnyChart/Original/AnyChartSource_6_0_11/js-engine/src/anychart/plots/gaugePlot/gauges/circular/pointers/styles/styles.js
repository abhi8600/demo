/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape}</li>

 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle}</li>

 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle}</li>

 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle}</li>
 *
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes}</li>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.circular.pointers.styles');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.pointer.styles');
goog.require('anychart.plots.gaugePlot.gauges.linear.pointers.styles');
//------------------------------------------------------------------------------
//
//                            CapFigure class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure = function () {
};
/**
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.innerStrokeElement = null;
/**
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.outerStrokeElement = null;
/**
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.backgroundElement = null;
/**
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle}
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.style_ = null;

/**
 * Initialization of full cap figure.
 * @param {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer} point
 * @param {anychart.styles.StyleShapes} styleShapes
 * @param {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle} capStyle
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.initialize = function (point, styleShapes, capStyle) {
    this.style_ = capStyle;
    this.point_ = point;

    if (capStyle.getInnerStroke() && capStyle.getInnerStroke().isEnabled())
        this.innerStrokeElement = point.getSVGManager().createPath();

    if (capStyle.getOuterStroke() && capStyle.getOuterStroke().isEnabled())
        this.outerStrokeElement = point.getSVGManager().createPath();

    if (capStyle.getBackground() && capStyle.getBackground().isEnabled())
        this.backgroundElement = point.getSVGManager().createPath();
};
/**
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.appendTo = function (sprite) {
    if (this.backgroundElement)
        sprite.appendChild(this.backgroundElement);
    if (this.innerStrokeElement)
        sprite.appendChild(this.innerStrokeElement);
    if (this.outerStrokeElement)
        sprite.appendChild(this.outerStrokeElement);
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.update = function () {
    if (this.backgroundElement)
        this.updateBackground_(this.calculateBackgroundBounds_());
    if (this.innerStrokeElement)
        this.updateInnerStroke_(this.calculateStrokeBounds_(this.style_.getInnerStroke()));
    if (this.outerStrokeElement)
        this.updateOuterStroke_(this.calculateStrokeBounds_(this.style_.getOuterStroke()));
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.updateBackground_ = function (bounds) {
    if (this.backgroundElement) {
        this.backgroundElement.setAttribute(
            'd',
            this.getBackgroundPath_(this.point_.getSVGManager())
        );
        var style = this.getElementStyle_(this.style_.getBackground(), bounds);
        this.backgroundElement.setAttribute(
            'style', style
        );
    }
};
/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.updateInnerStroke_ = function (bounds) {
    if (this.innerStrokeElement) {
        this.innerStrokeElement.setAttribute(
            'd',
            this.getInnerStrokePath_(this.point_.getSVGManager())
        );
        var style = this.getElementStyle_(this.style_.getInnerStroke(), bounds);
        this.innerStrokeElement.setAttribute(
            'style', style
        );
    }
};
/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.updateOuterStroke_ = function (bounds) {
    if (this.outerStrokeElement) {
        this.outerStrokeElement.setAttribute(
            'd',
            this.getOuterStrokePath_(this.point_.getSVGManager())
        );
        var style = this.getElementStyle_(this.style_.getOuterStroke(), bounds);
        this.outerStrokeElement.setAttribute(
            'style', style
        );
    }
};
/**
 * @private
 * @param {(anychart.visual.background.Background|anychart.plots.gaugePlot.visual.BackgroundBasedStroke)} element
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.getElementStyle_ = function (element, bounds) {
    var style = '';

    style += (element.getBorder() ?
        element.getBorder().getSVGStroke(this.point_.getSVGManager(), bounds, null, true) :
        '');
    var bool = element.getFill().getGradient() ? (element.getFill().getGradient().getType() == 1) : null;
    style += (element.getFill() ?
        element.getFill().getSVGFill(this.point_.getSVGManager(), bounds, null, bool) :
        '');

    style += (element.getHatchFill() ?
        element.getHatchFill().getSVGHatchFill(this.point_.getSVGManager()) :
        '');

    return style;
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.calculateBackgroundBounds_ = function () {
    var axis = this.point_.getAxis();
    var r = (this.style_.isRadiusAxisSizeDepend ? axis.size : 1) * this.style_.radius;

    var pivotPoint = axis.getGauge().getPixPivotPoint();
    var pixelRadius = axis.getPixelRadius(r);

    var bounds = new anychart.utils.geom.Rectangle();

    bounds.x = pivotPoint.x - pixelRadius;
    bounds.y = pivotPoint.y - pixelRadius;
    bounds.width = pixelRadius * 2;
    bounds.height = pixelRadius * 2;
    return bounds;
};
/**
 * @private
 * @param {*} stroke
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.calculateStrokeBounds_ = function (stroke) {
    var bounds = new anychart.utils.geom.Rectangle(),
        axis = this.point_.getAxis(),
        r = (this.style_.isRadiusAxisSizeDepend ? axis.size : 1) * this.style_.radius,

        pivotPoint = axis.getGauge().getPixPivotPoint(),

        outerR = (stroke == this.style_.getInnerStroke()) ?
            axis.getPixelRadius(r + this.style_.getInnerStroke().getThickness()) :
            axis.getPixelRadius(r + this.style_.getInnerStroke().getThickness() + this.style_.getOuterStroke().getThickness());


    bounds.x = pivotPoint.x - outerR;
    bounds.y = pivotPoint.y - outerR;
    bounds.width = bounds.height = outerR * 2;
    return bounds;
};

/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.getBackgroundPath_ = function (svgManager) {
    var path = '',
        axis = this.point_.getAxis();
    var r = (this.style_.isRadiusAxisSizeDepend ? axis.size : 1) * this.style_.radius;

    var pivotPoint = axis.getGauge().getPixPivotPoint();
    var pixelRadius = axis.getPixelRadius(r);

    path += svgManager.pCircle(pivotPoint.x, pivotPoint.y, pixelRadius);
    path += svgManager.pFinalize();

    return path;
};
/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.getInnerStrokePath_ = function (svgManager) {
    var path = '',
        axis = this.point_.getAxis(),
        r = (this.style_.isRadiusAxisSizeDepend ? axis.size : 1) * this.style_.radius,
        pivotPoint = axis.getGauge().getPixPivotPoint(),
        pixelRadius = axis.getPixelRadius(r),
        outerR = axis.getPixelRadius(r + this.style_.getInnerStroke().getThickness()),
        x = pivotPoint.x,
        y = pivotPoint.y;


    var innerStartX = x + pixelRadius,
        innerEndX = x + pixelRadius * Math.cos(359.999 * Math.PI / 180),
        innerEndY = y + pixelRadius * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(innerStartX, y);
    path += svgManager.pCircleArc(pixelRadius, 1, 1, innerEndX, innerEndY, pixelRadius);

    var outerStartX = x + outerR,
        outerEndX = x + outerR * Math.cos(359.999 * Math.PI / 180),
        outerEndY = y + outerR * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(outerEndX, outerEndY);
    path += svgManager.pCircleArc(outerR, 1, 0, outerStartX, y, outerR);
    path += svgManager.pMove(innerStartX, y);

    path += svgManager.pFinalize();

    return path;
};
/**
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure.prototype.getOuterStrokePath_ = function (svgManager) {
    var path = '',
        axis = this.point_.getAxis(),
        r = (this.style_.isRadiusAxisSizeDepend ? axis.size : 1) * this.style_.radius,
        pivotPoint = axis.getGauge().getPixPivotPoint(),
        pixelRadius = axis.getPixelRadius(r + this.style_.getInnerStroke().getThickness()),
        outerR = axis.getPixelRadius(r + this.style_.getInnerStroke().getThickness() + this.style_.getOuterStroke().getThickness()),
        x = pivotPoint.x,
        y = pivotPoint.y;


    var innerStartX = x + pixelRadius,
        innerEndX = x + pixelRadius * Math.cos(359.999 * Math.PI / 180),
        innerEndY = y + pixelRadius * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(innerStartX, y);
    path += svgManager.pCircleArc(pixelRadius, 1, 1, innerEndX, innerEndY, pixelRadius);

    var outerStartX = x + outerR,
        outerEndX = x + outerR * Math.cos(359.999 * Math.PI / 180),
        outerEndY = y + outerR * Math.sin(359.999 * Math.PI / 180);

    path += svgManager.pMove(outerEndX, outerEndY);
    path += svgManager.pCircleArc(outerR, 1, 0, outerStartX, y, outerR);
    path += svgManager.pMove(innerStartX, y);

    path += svgManager.pFinalize();

    return path;
};
//------------------------------------------------------------------------------
//
//                            KnobShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.background.ShapeBackground}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape,
    anychart.visual.background.ShapeBackground);

/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.backgroundElement_ = null;

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.style_ = null;


/**
 * @private
 * @type {*}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.point_ = null;

/**
 * @param {*} point
 * @param {*} knobStyle
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.initialize = function (point, knobStyle) {
    this.style_ = knobStyle;
    this.point_ = point;

    if (knobStyle.getBackground() && knobStyle.getBackground().isEnabled())
        this.backgroundElement_ = point.getSVGManager().createPath();
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.appendTo = function (sprite) {
    if (this.backgroundElement_)
        sprite.appendChild(this.backgroundElement_)
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.update = function () {
    if (this.backgroundElement_)
        this.updateBackground_(this.calculateBackgroundBounds_());
    var angle = this.point_.getAxis().getScale().transformValueToPixel(this.point_.fixValueScaleOut(this.point_.getValue()));
    this.point_.getKnobSprite().setRotation(angle);
};

/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.updateBackground_ = function (bounds) {
    if (this.backgroundElement_) {
        this.backgroundElement_.setAttribute(
            'd',
            this.getBackgroundPath_(this.point_.getSVGManager())
        );
        var style = this.getElementStyle_(this.style_.getBackground(), bounds);
        this.backgroundElement_.setAttribute(
            'style', style
        );
    }
};

anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.getElementStyle_ = function (element, bounds) {
    var style = '';

    style += (element.getBorder() ?
        element.getBorder().getSVGStroke(this.point_.getSVGManager(), bounds, null, true) :
        '');

    style += (element.getFill() ?
        element.getFill().getSVGFill(this.point_.getSVGManager(), bounds, null, true) :
        '');

    style += (element.getHatchFill() ?
        element.getHatchFill().getSVGHatchFill(this.point_.getSVGManager()) :
        '');

    return style;
};
/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.calculateBackgroundBounds_ = function () {
    var axis = this.point_.getAxis(),
        style = this.style_,
        percentR = (style.isRadiusAxisSizeDepend ? axis.size : 1) * style.radius,
        dW = (style.isGearHeightAxisSizeDepend ? axis.size : 1) * style.dW_,
        outerR = axis.getPixelRadius(percentR + dW),
        center = axis.getGauge().getPixPivotPoint();
    return new anychart.utils.geom.Rectangle(-outerR, -outerR, outerR * 2, outerR * 2);
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape.prototype.getBackgroundPath_ = function (svgManager) {
    var path = '',
        point = this.point_,
        axis = point.getAxis(),
        state = point.getCurrentState(),
        style = state.getKnobStyle(),
        percentR = (style.isRadiusAxisSizeDepend ? axis.size : 1) * style.radius,
        dW = (style.isGearHeightAxisSizeDepend ? axis.size : 1) * style.dW_,
        elementsCount = style.getElementsCount();

    var innerR = axis.getPixelRadius(percentR),
        outerR = axis.getPixelRadius(percentR + dW);

    var center = axis.getGauge().getPixPivotPoint(),
        angleStep = 360 / elementsCount;

    point.getKnobSprite().setX(center.x);
    point.getKnobSprite().setY(center.y);

    var startAngle = 90 - angleStep / 2,
        isInner = false;
    center = new anychart.utils.geom.Point(0, 0);

    var startPt = new anychart.utils.geom.Point();
    startPt.x = center.x + outerR * Math.cos(startAngle * Math.PI / 180);
    startPt.y = center.y + outerR * Math.sin(startAngle * Math.PI / 180);

    var actualR, start, end,
        startX = startPt.x,
        startY = startPt.y,
        endX, endY;

    path += svgManager.pMove(startX, startY);

    for (var i = 0; i < elementsCount; i++) {
        actualR = isInner ? innerR : outerR;
        start = startAngle + angleStep * i;
        end = start + angleStep;

        endX = center.x + actualR * Math.cos(end * Math.PI / 180);
        endY = center.x + actualR * Math.sin(end * Math.PI / 180);
        startX = center.x + actualR * Math.cos(start * Math.PI / 180);
        startY = center.y + actualR * Math.sin(start * Math.PI / 180);
        path += svgManager.pLine(startX, startY);

        path += svgManager.pCircleArc(actualR, 0, 1, endX, endY, actualR);
        isInner = !isInner;
    }

    path += svgManager.pLine(startPt.x, startPt.y);
    path += svgManager.pFinalize();
    return path;
};

//------------------------------------------------------------------------------
//
//                            KnobStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle = function () {
    this.background = new anychart.visual.background.Background();
    this.radius = .5;
    this.isRadiusAxisSizeDepend = false;
    this.isGearHeightAxisSizeDepend = false;
};
/**
 * @private
 * @type {int}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.elementsCount_ = 12;
/**
 * @return {int}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.getElementsCount = function () {
    return this.elementsCount_
};

/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.dW_ = .03;
/**
 * @protected
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.radius = null;
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.isRadiusAxisSizeDepend = null;
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.isGearHeightAxisSizeDepend = null;
/**
 * @protected
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.background = null;
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.getBackground = function () {
    return this.background;
};

/**
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization,
        gaugeDes = anychart.plots.gaugePlot.deserializationUtils,
        prop;
    this.background.deserialize(data);
    if (des.hasProp(data, 'radius')) {
        prop = des.getStringProp(data, 'radius');
        this.isRadiusAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.radius = this.isRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.hasProp(data, 'gear_height')) {
        prop = des.getStringProp(data, 'gear_height');
        this.isGearHeightAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.dW_ = this.isRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
};


//------------------------------------------------------------------------------
//
//                            CapStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle = function () {
    this.radius = 0.1;
    this.isRadiusAxisSizeDepend = false;
};

/**
 * @protected
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.radius = null;
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.isRadiusAxisSizeDepend = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.innerStroke = null;
/**
 * @return {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.getInnerStroke = function () {
    return this.innerStroke;
};


/**
 * @protected
 * @type {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.outerStroke = null;
/**
 * @return {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.getOuterStroke = function () {
    return this.outerStroke;
};

/**
 * @protected
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.background = null;
/**
 * @return {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.getBackground = function () {
    return this.background;
};

//private var effects:EffectsList;

/**
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization,
        gaugeDes = anychart.plots.gaugePlot.deserializationUtils,
        prop;

    if (des.hasProp(data, 'radius')) {
        prop = des.getStringProp(data, 'radius');
        this.isRadiusAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.radius = this.isRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.isEnabledProp(data, 'inner_stroke')) {
        this.innerStroke = new anychart.plots.gaugePlot.visual.BackgroundBasedStroke();
        this.innerStroke.deserialize(des.getProp(data, 'inner_stroke'));
    }
    if (des.isEnabledProp(data, 'outer_stroke')) {
        this.outerStroke = new anychart.plots.gaugePlot.visual.BackgroundBasedStroke();
        this.outerStroke.deserialize(des.getProp(data, 'outer_stroke'));
    }
    if (des.isEnabledProp(data, 'background')) {
        this.background = new anychart.visual.background.Background();
        this.background.deserialize(des.getProp(data, 'background'));
    }
};

/**
 * ND: Needs doc!
 * @param {anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis} axis
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle.prototype.getPercentRadius = function (axis) {
    return (this.isRadiusAxisSizeDepend ? axis.size : 1) * this.radius;
};

//------------------------------------------------------------------------------
//
//                      CircularGaugePointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState = function (style) {
    goog.base(this, style);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState,
    anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState);

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState.prototype.capStyle_ = null;

/**
 * @return {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState.prototype.getCapStyle = function () {
    return this.capStyle_;
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization;

    if (des.isEnabledProp(data, "cap")) {
        this.capStyle_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapStyle();
        this.capStyle_.deserialize(des.getProp(data, 'cap'));
    } else this.capStyle_ = null;
    return data;
};

//------------------------------------------------------------------------------
//
//                          CircularGaugePointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);

/**
 * Class represents cap.
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.capFigure_ = null;


/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.createShapes = function () {
    goog.base(this, 'createShapes');
    var capStyle = this.point_.getCurrentState().getCapStyle();
    if (capStyle != null) {
        this.capFigure_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.CapFigure();
        this.capFigure_.initialize(this.point_, this, capStyle);
    }
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.placeShapes = function () {
    goog.base(this, 'placeShapes');
    if (this.capFigure_ != null)
        this.capFigure_.appendTo(this.point_.getCapSprite());
};

/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.updatePath = function () {
    var pt = this.point_;
    if (this.fillShape_)
        this.fillShape_.getElement().setAttribute(
            'd',
            pt.getDrawer().draw(pt.getSprite(), pt)
        );
    if (this.hatchFillShape_)
        this.hatchFillShape_.getElement().setAttribute(
            'd',
            pt.getDrawer().draw(pt.getSprite(), pt)
        );
    if (this.capFigure_ != null)
        this.capFigure_.update();
};

anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes.prototype.updateEffects = function () {
};

//------------------------------------------------------------------------------
//
//                          CircularGaugePointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyle,
    anychart.styles.Style);
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes();
};
//------------------------------------------------------------------------------
//
//                          NeedleStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @param {Boolean} useBackground Whether to use background or not.
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle = function (useBackground) {
    if (useBackground) this.background = new anychart.visual.background.Background();
    this.isBaseRadiusAxisSizeDepend = false;
    this.isPointRadiusAxisSizeDepend = false;
    this.isPointThicknessAxisSizeDepend = false;
    this.isRadiusAxisSizeDepend = false;
    this.isThicknessAxisSizeDepend = false;
};
/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.radius = null;
/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.isRadiusAxisSizeDepend = null;


/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.baseRadius = null;

/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.isBaseRadiusAxisSizeDepend = null;


/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.pointThickness = null;

/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.isPointThicknessAxisSizeDepend = null;


/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.pointRadius = null;

/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.isPointRadiusAxisSizeDepend = null;


/**
 * ND: Needs doc!
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.thickness = null;

/**
 * ND: Needs doc!
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.isThicknessAxisSizeDepend = null;


/**
 * ND: Needs doc!
 * @param {anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis} axis
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.getPercentThickness = function (axis) {
    return (this.isThicknessAxisSizeDepend ? axis.size : 1) * this.thickness;
};

/**
 * ND: Needs doc!
 * @param {anychart.plots.gaugePlot.gauges.circular.CircularGaugeAxis} axis
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.getPercentBaseRadius = function (axis) {
    return (this.isBaseRadiusAxisSizeDepend ? axis.size : 1) * this.baseRadius;
};
/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle.prototype.deserialize = function (data, style) {
    var des = anychart.utils.deserialization,
        gaugeDes = anychart.plots.gaugePlot.deserializationUtils,
        prop;
    if (des.hasProp(data, 'radius')) {
        prop = des.getStringProp(data, 'radius');
        this.isRadiusAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.radius = this.isRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.hasProp(data, 'thickness')) {
        prop = des.getStringProp(data, 'thickness');
        this.isThicknessAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.thickness = this.isThicknessAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.hasProp(data, 'base_radius')) {
        prop = des.getStringProp(data, 'base_radius');
        this.isBaseRadiusAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.baseRadius = this.isBaseRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.hasProp(data, 'point_thickness')) {
        prop = des.getStringProp(data, 'point_thickness');
        this.isPointThicknessAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.pointThickness = this.isPointThicknessAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (des.hasProp(data, 'point_radius')) {
        prop = des.getStringProp(data, 'point_radius');
        this.isPointRadiusAxisSizeDepend = gaugeDes.isAxisSizeDepend(prop);
        this.pointRadius = this.isPointRadiusAxisSizeDepend ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }
    if (this.background != null)
        this.background.deserialize(data, style);
};

//------------------------------------------------------------------------------
//
//                      NeedlePointerStyleState class.
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState}
 *
 * @param {*} style
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState = function (style) {
    goog.base(this, style);
    this.needle_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle(false);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState);

/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState.prototype.needle_ = null;
/**
 * ND: Needs doc!
 * @return {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState.prototype.getNeedle = function () {
    return this.needle_;
};

/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.needle_.deserialize(data, this.getStyle());
    return data;
};
//------------------------------------------------------------------------------
//
//                      NeedlePointerStyleShapes class.
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleShapes,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes);

//------------------------------------------------------------------------------
//
//                      NeedlePointerStyle class.
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle,
    anychart.styles.background.BackgroundStyle);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedlePointerStyleShapes();
};

//------------------------------------------------------------------------------
//
//                      BarPointerStyleState class.
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState}
 *
 * @param {*} style
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState = function (style) {
    goog.base(this, style);
    this.startFromZero_ = false;
    this.width_ = .1;
    this.isAxisSizeWidth_ = false;
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState);

/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.startFromZero_ = null;
/**
 * ND: Needs doc!
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.isStartFromZero = function () {
    return this.startFromZero_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.width_ = null;
/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.getWidth = function () {
    return this.width_
};

/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.isAxisSizeWidth_ = null;
/**
 * ND: Needs doc!
 * @return {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.isAxisSizeWidth = function () {
    return this.isAxisSizeWidth_
};


/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization,
        gaugeDes = anychart.plots.gaugePlot.deserializationUtils,
        prop;

    if (des.hasProp(data, 'width')) {
        prop = des.getStringProp(data, 'width');
        this.isAxisSizeWidth_ = gaugeDes.isAxisSizeDepend(prop);
        this.width_ = this.isAxisSizeWidth_ ? gaugeDes.getAxisSizeFactor(prop) : des.getSimplePercent(prop);
    }

    if (des.hasProp(data, 'start_from_zero')) {
        this.startFromZero_ = des.getBoolProp(data, 'start_from_zero')
    }

    return data;
};


//------------------------------------------------------------------------------
//
//                      BarPointerStyleShapes class.
//
//------------------------------------------------------------------------------

/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
//    if (opt_updateData) this.point_.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes.prototype.updatePath = function (element) {
    var pt = this.point_;
    element.setAttribute('d', this.point_.getBarPath());
};


//------------------------------------------------------------------------------
//
//                        BarPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle,
    anychart.styles.background.BackgroundStyle);

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.BarPointerStyleShapes();
};

//------------------------------------------------------------------------------
//
//                 MarkerPointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState = function () {
    goog.base(this);
    this.marker_ = new anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle(false);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState.prototype.marker_ = null;
/**
 * @return {anychart.plots.gaugePlot.gauges.linear.pointers.styles.MarkerStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState.prototype.getMarker = function () {
    return this.marker_;
};
//------------------------------------------------------------------------------
//                         Deserialization.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.marker_.deserialize(data);
    return data;
};
//------------------------------------------------------------------------------
//
//                MarkerPointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes = function () {
    goog.base(this);
    this.shapeDrawer_ = new anychart.plots.gaugePlot.visual.GaugeShapeDrawer();
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.visual.GaugeShapeDrawer}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes.prototype.shapeDrawer_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes.prototype.getSprite = function () {
    return this.point_.getMarkerSprite();
};
//------------------------------------------------------------------------------
//                      Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    this.point_.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.shapeDrawer_.getSVGPathData(
        this.point_.getCurrentState().getMarker().getShape(),
        this.point_.getSVGManager(),
        Math.min(this.point_.getBounds().width, this.point_.getBounds().height),
        this.point_.getMarkerPos().x,
        this.point_.getMarkerPos().y,
        this.point_.getBounds().width,
        this.point_.getBounds().height,
        this.point_.getCurrentState().needUpdateBorder() ? this.point_.getCurrentState().getBorder().getThickness() : 1
    ));
    if (this.capFigure_ != null)
        this.capFigure_.update();
};
//------------------------------------------------------------------------------
//
//                          MarkerPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.Style}
 *
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                      Settings.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.MarkerPointerStyleShapes();
};
//------------------------------------------------------------------------------
//
//                          KnobPointerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState}
 * @param {*} style
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState = function (style) {
    goog.base(this, style);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleState);
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState.prototype.knobStyle_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState.prototype.getKnobStyle = function () {
    return this.knobStyle_
};
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState.prototype.needle_ = null;
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState.prototype.getNeedle = function () {
    return this.needle_
};


/**
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    if (des.isEnabledProp(data, "knob_background")) {
        this.knobStyle_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobStyle();
        this.knobStyle_.deserialize(des.getProp(data, 'knob_background'));
    } else this.knobStyle_ = null;
    if (des.isEnabledProp(data, "needle")) {
        goog.base(this, 'deserialize', des.getProp(data, 'needle'));
        this.needle_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.NeedleStyle(true);
        this.needle_.deserialize(des.getProp(data, 'needle'), this.getStyle());
    } else this.needle_ = null;
    data = goog.base(this, 'deserialize', data);
    return data;
};

//------------------------------------------------------------------------------
//
//                          KnobPointerStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes}

 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes,
    anychart.plots.gaugePlot.gauges.circular.pointers.styles.CircularGaugePointerStyleShapes);
/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes.prototype.knobShape_ = null;

/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes.prototype.createShapes = function () {
    goog.base(this, 'createShapes');
    var knobStyle = this.point_.getCurrentState().getKnobStyle();
    if (knobStyle != null) {
        this.knobShape_ = new anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobShape();
        this.knobShape_.initialize(this.point_, knobStyle)
    }
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes.prototype.placeShapes = function () {
    goog.base(this, 'placeShapes');
    if (this.knobShape_ != null)
        this.knobShape_.appendTo(this.point_.getKnobSprite());
};
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes.prototype.updatePath = function () {
    goog.base(this, 'updatePath');
    if (this.knobShape_ != null)
        this.knobShape_.update();
};

//------------------------------------------------------------------------------
//
//                          KnobPointerStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle,
    anychart.styles.background.BackgroundStyle);
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleState;
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.gaugePlot.gauges.circular.pointers.styles.KnobPointerStyleShapes();
};