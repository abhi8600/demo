/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.data.PiePoint};</li>
 *  <li>@class {anychart.plots.piePlot.data.PieSeries}.</li>
 *  <li>@class {anychart.plots.piePlot.data.Pie3DPoint};</li>
 *  <li>@class {anychart.plots.piePlot.data.Pie3DSeries}.</li>
 *  <li>@class {anychart.plots.piePlot.data.PieGlobalSeriesSettings};</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.data');

goog.require('anychart.plots.piePlot.data.styles');
goog.require('anychart.plots.piePlot.data.drawers');
goog.require('anychart.plots.seriesPlot.data');
goog.require('anychart.plots.seriesPlot.data.singleValue');
goog.require('anychart.plots.piePlot.labels');
goog.require('anychart.styles');
goog.require('anychart.utils');
goog.require('anychart.visual.stroke');
goog.require('anychart.utils.geom');
goog.require('anychart.layout');
//------------------------------------------------------------------------------
//
//                           PiePoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint}
 */
anychart.plots.piePlot.data.PiePoint = function () {
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.call(this);
    this.pathCache_ = {};
};
//inheritance
goog.inherits(anychart.plots.piePlot.data.PiePoint,
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint);
//------------------------------------------------------------------------------
//                          Drawing properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.PiePoint.prototype.startAngle_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.piePlot.data.PiePoint.prototype.getStartAngle = function () {
    return this.startAngle_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.PiePoint.prototype.endAngle_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.piePlot.data.PiePoint.prototype.getEndAngle = function () {
    return this.endAngle_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.PiePoint.prototype.sweepAngle_ = NaN;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.piePlot.data.PiePoint.prototype.centerPoint_ = null;
//------------------------------------------------------------------------------
//                          Explode properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.data.PiePoint.prototype.isExploded_ = false;
/**
 * @param {Boolean} value
 */
anychart.plots.piePlot.data.PiePoint.prototype.setExploded = function (value) {
    this.isExploded_ = value;
};
/**
 * @return {Boolean}
 */
anychart.plots.piePlot.data.PiePoint.prototype.isExploded = function () {
    return this.isExploded_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 */
anychart.plots.piePlot.data.PiePoint.prototype.checkMissing_ = function() {
    if (isNaN(this.y_)) {
        this.isMissing_ = true;
        this.y_ = 0;
    }
};


anychart.plots.piePlot.data.PiePoint.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    this.checkMissing_();
    if (anychart.utils.deserialization.hasProp(config, 'exploded'))
        this.isExploded_ = anychart.utils.deserialization.getBoolProp(config, 'exploded');

    if (this.isExploded_) {
        this.globalSettings_.setHasExplodedItems(true);
        this.getPlot().setExplodedPoint(this);
    }
};
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    if (this.hasConnector()) {
        this.connectorElement_ = svgManager.createPath();
        this.sprite_.appendChild(this.connectorElement_);
    }
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                      Update.
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.update = function () {
    this.calcBounds();
    goog.base(this, 'update');
    this.drawConnector(this.getSprite().getSVGManager());
};
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.calcBounds = function () {
    var center = this.getCenterPoint();
    this.bounds_.x = center.x - this.series_.getOuterRadius();
    this.bounds_.y = center.y - this.series_.getOuterRadius();
    this.bounds_.width = this.bounds_.height = this.series_.getOuterRadius() * 2;
};
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.piePlot.data.PiePoint.prototype.getCenterPoint = function () {
    if (this.centerPoint_ == null)
        this.centerPoint_ = new anychart.utils.geom.Point();

    this.centerPoint_.x = this.getPlot().getCenterPoint().x;
    this.centerPoint_.y = this.getPlot().getCenterPoint().y;
    if (this.isExploded_) {
        var dR = this.getPlot().getExplodeRadius();
        var centerAngle = (this.startAngle_ + this.endAngle_) / 2;
        centerAngle *= Math.PI / 180;
        this.centerPoint_.x += dR * Math.cos(centerAngle);
        this.centerPoint_.y += dR * Math.sin(centerAngle);
    }
    return this.centerPoint_;
};
//------------------------------------------------------------------------------
//                          Angle calculation
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.calculateAngles = function () {
    if (this.index_ == 0)
        this.startAngle_ = this.series_.getStartAngle();
    else
        this.startAngle_ = this.series_.getPointAt(this.index_ - 1).getEndAngle();

    var seriesTotal = this.series_.getSum();
    if (seriesTotal == 0) {
        this.sweepAngle_ = 360 / this.series_.getPoints().length;
    } else {
        this.sweepAngle_ = 360 * this.getValue() / seriesTotal;
    }
    this.endAngle_ = this.startAngle_ + this.sweepAngle_;
};

//------------------------------------------------------------------------------
//                          Explode
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.PiePoint.prototype.onMouseClick_ = function (event) {
    this.getPlot().checkExplode(this);
    goog.base(this, 'onMouseClick_', event);
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PiePoint.prototype.updateExplode = function () {
    this.calcBounds();
};
//------------------------------------------------------------------------------
//                          Path cache.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object.<String>}
 */
anychart.plots.piePlot.data.PiePoint.prototype.pathCache_ = null;
/**
 * @return {String}
 */
anychart.plots.piePlot.data.PiePoint.prototype.getPathData = function () {
    var key = this.getPathToString_();
    if (this.pathCache_[key])
        return this.pathCache_[key];
    else {
        var path = this.createPathData_();
        this.pathCache_[key] = path;
        return path;
    }
};
/**
 * @private
 * @return {String}
 */
anychart.plots.piePlot.data.PiePoint.prototype.getPathToString_ = function () {
    return  this.bounds_.toString() + ' ' + this.isExploded_.toString();
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Create path data by point drawing properties.
 * @private
 * @return {String}
 */
anychart.plots.piePlot.data.PiePoint.prototype.createPathData_ = function () {
    // Для innerR вычитается 1 пиксель чтобы закрыть дырку между несколькими сериями
    var innerR;
    if (this.series_.getInnerRadius() >= 1)
        innerR = this.series_.getInnerRadius() - 1
    else innerR = this.series_.getInnerRadius();

    var outerR = this.series_.getOuterRadius();

    // для закрытия дырок между секторами пая добавляется пиксель
    var startAngle;
    var endAngle;
    var min;

    if (this.endAngle_ % 360 != this.startAngle_ % 360) {
        endAngle = (this.endAngle_ + 0.5) * Math.PI / 180;
        startAngle = this.startAngle_ * Math.PI / 180;
    } else {
        // В случае одной точки - криво рассчитываются координаты при изменении start_angle
        // для корректного рассчета сделано чтобы точка рисовалась по координатам как будто бы
        // она рисуется от угла=0 до угла=360
        min = Math.min(this.startAngle_, this.endAngle_);
        endAngle = (this.endAngle_ - min) * Math.PI / 180;
        startAngle = (this.startAngle_ - min) * Math.PI / 180
    }

    var center = this.getCenterPoint();

    var innerStartX = center.x + innerR * Math.cos(startAngle);
    var innerStartY = center.y + innerR * Math.sin(startAngle);

    var outerStartX = center.x + outerR * Math.cos(startAngle);
    var outerStartY = center.y + outerR * Math.sin(startAngle);

    var innerEndX = center.x + innerR * Math.cos(endAngle);
    var innerEndY = center.y + innerR * Math.sin(endAngle);

    var outerEndX = center.x + outerR * Math.cos(endAngle);
    var outerEndY = center.y + outerR * Math.sin(endAngle);

    var largeArc1 = false;
    var sweepArc1 = true;

    var largeArc2 = false;
    var sweepArc2 = false;

    var drawBorder = true;

    if (this.sweepAngle_ > 180) {
        largeArc1 = true;
        largeArc2 = true;
    }

    var svgManager = this.getSVGManager();


    if (outerStartX == outerEndX) {
        outerEndY -= .0001;
        drawBorder = false;
    }

    if (innerStartX == innerEndX) {
        innerStartY += .0001;
    }

    var pathData = svgManager.pMove(innerStartX, innerStartY);
    pathData += svgManager.pMove(outerStartX, outerStartY);
    pathData += svgManager.pCircleArc(outerR, largeArc1, sweepArc1, outerEndX, outerEndY);
    pathData += drawBorder ? svgManager.pLine(innerEndX, innerEndY) : svgManager.pMove(innerEndX, innerEndY);
    pathData += svgManager.pCircleArc(innerR, largeArc2, sweepArc2, innerStartX, innerStartY);
    pathData += svgManager.pFinalize();

    return pathData;
};
//------------------------------------------------------------------------------
//                          Connectors and labels ignored
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.piePlot.data.PiePoint.prototype.connectorElement_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.piePlot.data.PiePoint.prototype.connectorBounds_ = null;

anychart.plots.piePlot.data.PiePoint.prototype.hasConnector = function () {
    return this.labelSprite_ && this.getGlobalSettings().hasConnector();
};
anychart.plots.piePlot.data.PiePoint.prototype.resizeConnector = function () {
    if (!this.hasConnector()) return;
    var pathData = this.getConnectorPathData(this.getSVGManager());
    if (!pathData) {
        this.connectorElement_.setAttribute('visibility', 'hidden');
    } else {
        this.connectorElement_.setAttribute('d', pathData);
        this.connectorElement_.setAttribute('visibility', 'visible');
    }
};
anychart.plots.piePlot.data.PiePoint.prototype.drawConnector = function (svgManager) {
    if (!this.hasConnector()) return;
    var pathData = this.getConnectorPathData(svgManager);
    if (pathData != null) this.connectorElement_.setAttribute('d', pathData);
    else this.connectorElement_.removeAttribute('d');
    this.setConnectorStyle(svgManager, this.connectorElement_);
};
/**
 * @param svgManager
 * @param element
 */
anychart.plots.piePlot.data.PiePoint.prototype.setConnectorStyle = function (svgManager, element) {
    var style = this.getGlobalSettings().getConnectorStroke().getSVGStroke(
        svgManager,
        this.connectorBounds_,
        this.color_.getColor());
    style += svgManager.getEmptySVGFill();
    element.setAttribute('style', style);
};
anychart.plots.piePlot.data.PiePoint.prototype.getConnectorPathData = function (svgManager) {
    if (!this.series_.isOutsideLabels() ||
        !this.labelSprite_ ||
        !this.isLabelEnabled_ ||
        !this.getGlobalSettings().isOutsideLabels() ||
        !this.getPlot().basicLabelsInfo_[this.labelIndex_].isEnabled()) return;
    var plot = this.getPlot();
    var firstPoint = new anychart.utils.geom.Point();
    var secondPoint = new anychart.utils.geom.Point();
    var thirdPoint = new anychart.utils.geom.Point();

    firstPoint = this.getCenterPoint().clone();
    var angle = plot.basicLabelsInfo_[this.labelIndex_].getAngle();

    var connectorPadding = this.getGlobalSettings().isPercentConnectorPadding_ ?
        plot.getOuterRadius() * this.getGlobalSettings().getConnectorPadding() :
        this.getGlobalSettings().getConnectorPadding();

    secondPoint = plot.getLabelPosition_(plot.basicLabelsInfo_[this.labelIndex_], false).clone();
    thirdPoint = plot.getLabelPosition_(plot.basicLabelsInfo_[this.labelIndex_], false).clone();

    if ((angle % 360) > 90 && (angle % 360) < 270)
        secondPoint.x += connectorPadding;
    else
        secondPoint.x -= connectorPadding;

    return this.drawConnectorSegment_(svgManager, firstPoint, secondPoint, thirdPoint);
};

anychart.plots.piePlot.data.PiePoint.prototype.drawConnectorSegment_ = function (svgManager, firstPoint, secondPoint, thirdPoint) {
    var plotRadius = this.series_.getOuterRadius();
    var angle = (this.startAngle_ + this.endAngle_) / 2;
    var du = anychart.utils.geom.DrawingUtils;
    firstPoint.x += du.getPointX(plotRadius, angle);
    firstPoint.y += du.getPointY(plotRadius, angle);
    return this.doConnectorDrawing_(svgManager, firstPoint, secondPoint, thirdPoint);
};

anychart.plots.piePlot.data.PiePoint.prototype.doConnectorDrawing_ = function (svgManager, firstPoint, secondPoint, thirdPoint) {
    var pathData = '';
    var connector = this.getGlobalSettings().getConnectorStroke();
    if (connector != null) {
        this.connectorBounds_ = new anychart.utils.geom.Rectangle(
            Math.min(firstPoint.x, secondPoint.x, thirdPoint.x),
            Math.min(firstPoint.y, secondPoint.y, thirdPoint.y),
            Math.max(firstPoint.x, secondPoint.x, thirdPoint.x) - Math.min(firstPoint.x, secondPoint.x, thirdPoint.x),
            Math.max(firstPoint.y, secondPoint.y, thirdPoint.y) - Math.min(firstPoint.y, secondPoint.y, thirdPoint.y)
        );
        pathData += svgManager.pMove(firstPoint.x, firstPoint.y);
        pathData += svgManager.pLine(secondPoint.x, secondPoint.y);
        pathData += svgManager.pLine(thirdPoint.x, thirdPoint.y);
    }
    return pathData;
};
anychart.plots.piePlot.data.PiePoint.prototype.updateConnector_ = function () {
};
//------------------------------------------------------------------------------
//                          Labels
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.labelIndex_ = NaN;
anychart.plots.piePlot.data.PiePoint.prototype.getLabelIndex = function () {
    return this.labelIndex_;
};
anychart.plots.piePlot.data.PiePoint.prototype.setLabelIndex = function (value) {
    this.labelIndex_ = value;
};

anychart.plots.piePlot.data.PiePoint.prototype.execLabelDrawing = function () {
    if (this.isLabelEnabled_) goog.base(this, 'execLabelDrawing');
};

anychart.plots.piePlot.data.PiePoint.prototype.initLabelSprite = function () {
    this.initLabelVisual();
};

anychart.plots.piePlot.data.PiePoint.prototype.isLabelEnabled_ = true;
anychart.plots.piePlot.data.PiePoint.prototype.hasLabel = function () {
    return this.labelSprite_ != null;
};
anychart.plots.piePlot.data.PiePoint.prototype.enableLabel = function (opt_enabled) {
    if (opt_enabled == undefined) opt_enabled = true;
    this.getLabel().enabled_ = opt_enabled;
    this.isLabelEnabled_ = opt_enabled;
};

anychart.plots.piePlot.data.PiePoint.prototype.getLabelPosition = function (element, state, bounds) {
    if (this.series_.isOutsideLabels()) {
        return this.getPlot().getLabelPositionByIndex(this.labelIndex_, true);
    }
    return this.getElementPosition(element, state, bounds);
};

anychart.plots.piePlot.data.PiePoint.prototype.getLabelSprite = function () {
    return this.labelSprite_;
};

//------------------------------------------------------------------------------
//                          Anchors
//------------------------------------------------------------------------------
/**
 *
 * @param {anychart.utils.geom.Point} position
 * @param {int} anchor
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {Number} padding
 */
anychart.plots.piePlot.data.PiePoint.prototype.setAnchorPoint = function (position, anchor, bounds, padding) {
    var angle;
    var radius;
    var innerR = this.series_.getInnerRadius();
    var outerR = this.series_.getOuterRadius();
    switch (anchor) {
        case anychart.layout.Anchor.CENTER:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            radius = (innerR + outerR) / 2;
            break;
        case anychart.layout.Anchor.CENTER_BOTTOM:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            radius = innerR;
            break;
        case anychart.layout.Anchor.CENTER_LEFT:
            angle = this.startAngle_;
            radius = (innerR + outerR) / 2;
            break;
        case anychart.layout.Anchor.CENTER_RIGHT:
            angle = this.endAngle_;
            radius = (innerR + outerR) / 2;
            break;
        case anychart.layout.Anchor.CENTER_TOP:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            radius = outerR;
            break;
        case anychart.layout.Anchor.LEFT_BOTTOM:
            angle = this.startAngle_;
            radius = innerR;
            break;
        case anychart.layout.Anchor.LEFT_TOP:
            angle = this.startAngle_;
            radius = outerR;
            break;
        case anychart.layout.Anchor.RIGHT_BOTTOM:
            angle = this.endAngle_;
            radius = innerR;
            break;
        case anychart.layout.Anchor.RIGHT_TOP:
            angle = this.endAngle_;
            radius = outerR;
            break;
    }
    var center = this.getCenterPoint();
    angle *= Math.PI / 180;
    position.x = center.x + radius * Math.cos(angle);
    position.y = center.y + radius * Math.sin(angle);
};
//------------------------------------------------------------------------------
//                          Resize
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.resize = function () {
    goog.base(this, 'resize');
    this.updateElements();
    this.resizeConnector();
    this.styleShapes_.updateGradients(this.currentState_);
};

//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PiePoint.prototype.checkCategory = function (category) {
};
/**
 * @inheritDoc
 */
anychart.plots.piePlot.data.PiePoint.prototype.setDefaultTokenValues = function () {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%YValue', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.piePlot.data.PiePoint.prototype.getTokenType = function (token) {
    if (this.tokensHash_.isCategoryToken(token) && this.dataCategory_)
        return this.dataCategory_.getTokenType(token);

    switch (token) {
        case '%YValue':
        case '%Value':
        case '%YPercentOfSeries':
        case '%YPercentOfTotal':
        case '%YPercentOfCategory':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return  goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.plots.piePlot.data.PiePoint.prototype.calculatePointTokenValues = function () {
    goog.base(this, 'calculatePointTokenValues');
    if (this.y_) {
        this.tokensHash_.add('%YValue', this.y_);
        this.tokensHash_.add('%Value', this.y_);
    }
};
//------------------------------------------------------------------------------
//                    Formatting token value getters
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.piePlot.data.PiePoint.prototype.getTokenValue = function (token) {
    if (this.tokensHash_.isCategoryToken(token) && this.dataCategory_)
        return this.dataCategory_.getTokenValue(token);

    if (!this.tokensHash_.has('%YPercentOfSeries'))
        this.tokensHash_.add(
            '%YPercentOfSeries',
            this.y_ / this.series_.getSeriesTokenValue('%SeriesYSum') * 100);

    if (!this.tokensHash_.has('%YPercentOfTotal'))
        this.tokensHash_.add(
            '%YPercentOfTotal',
            this.y_ / this.getPlot().getTokenValue('%DataPlotYSum') * 100);

    if (!this.tokensHash_.has('%YPercentOfCategory') && this.dataCategory_)
        this.tokensHash_.add(
            '%YPercentOfCategory',
            this.y_ / this.dataCategory_().getTokenValue('%CategoryYSum') * 100);

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//
//                           PieSeries class.
//
//------------------------------------------------------------------------------
/**
 * Class represent pie series.
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.piePlot.data.PieSeries = function () {
    anychart.plots.seriesPlot.data.BaseSeries.call(this);
    this.isOutsideLabels_ = false;
};
//inheritance
goog.inherits(anychart.plots.piePlot.data.PieSeries, anychart.plots.seriesPlot.data.BaseSeries);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.piePlot.data.PieSeries.prototype.isOutsideLabels_ = null;
/**
 * ND: Needs doc!
 * @return {Boolean}
 */
anychart.plots.piePlot.data.PieSeries.prototype.isOutsideLabels = function () {
    return this.isOutsideLabels_;
};
anychart.plots.piePlot.data.PieSeries.prototype.startAngle_ = 0;
anychart.plots.piePlot.data.PieSeries.prototype.setStartAngle = function (value) {
    this.startAngle_ = value;
};
anychart.plots.piePlot.data.PieSeries.prototype.getStartAngle = function () {
    return this.startAngle_;
};

anychart.plots.piePlot.data.PieSeries.prototype.innerRadius_ = 0;
anychart.plots.piePlot.data.PieSeries.prototype.getInnerRadius = function () {
    return this.innerRadius_;
};
anychart.plots.piePlot.data.PieSeries.prototype.setInnerRadius = function (value) {
    this.innerRadius_ = value;
};

anychart.plots.piePlot.data.PieSeries.prototype.outerRadius_ = 0;
anychart.plots.piePlot.data.PieSeries.prototype.getOuterRadius = function () {
    return this.outerRadius_;
};
anychart.plots.piePlot.data.PieSeries.prototype.setOuterRadius = function (value) {
    this.outerRadius_ = value;
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PieSeries.prototype.needCallOnBeforeDraw = function () {
    return true;
};
/**
 * Sum of pie point value.
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.PieSeries.prototype.sum_ = 0;

/**
 * Return sum of pie point value.
 * @public
 * @return {Number}
 */
anychart.plots.piePlot.data.PieSeries.prototype.getSum = function () {
    return this.sum_;
};
//------------------------------------------------------------------------------
//                           Deserialization
//------------------------------------------------------------------------------
/**
 * Create pie point.
 * @public
 * @return {anychart.plots.piePlot.data.PiePoint}
 */
anychart.plots.piePlot.data.PieSeries.prototype.createPoint = function () {
    return new anychart.plots.piePlot.data.PiePoint();
};

anychart.plots.piePlot.data.PieSeries.prototype.setPixelRadius = function () {
    this.innerRadius_ = this.globalSettings_.getPlot().getSeriesRadius(this.index_, true);
    this.outerRadius_ = this.globalSettings_.getPlot().getSeriesRadius(this.index_, false);
};

/**
 * @override
 */
anychart.plots.piePlot.data.PieSeries.prototype.addPoint = function (point, index) {
    goog.base(this, 'addPoint', point, index);
    this.sum_ += point.getValue();
};
//TODO description
anychart.plots.piePlot.data.PieSeries.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    if (anychart.utils.deserialization.hasProp(config, 'start_angle'))
        this.startAngle_ = anychart.utils.deserialization.getNumProp(config, 'start_angle');
    this.startAngle_ += this.globalSettings_.getStartAngle();
};
//------------------------------------------------------------------------------
//                        OnBefore.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.PieSeries.prototype.onBeforeDraw = function () {
    this.setPixelRadius();
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PieSeries.prototype.onBeforeResize = function () {
    this.setPixelRadius();
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.piePlot.data.PieSeries.prototype.setDefaultTokenValues = function () {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesLastYValue', 0);
    this.tokensHash_.add('%SeriesYSum', 0);
    this.tokensHash_.add('%SeriesYMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYMin', Number.MAX_VALUE);
    this.tokensHash_.add('%SeriesYAverage', 0);

    this.tokensHash_.add('%MaxYValuePointName', '');
    this.tokensHash_.add('%MinYValuePointName', '');
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.piePlot.data.PieSeries.prototype.getTokenType = function (token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenType(token);

    switch (token) {
        case '%SeriesLastYValue':
        case '%SeriesFirstYValue':
        case '%SeriesYSum':
        case '%SeriesYMax':
        case '%SeriesYMin':
        case '%SeriesYAverage' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
        case '%MaxYValuePointName' :
        case '%MinYValuePointName' :
            return anychart.formatting.TextFormatTokenType.TEXT;
    }

    return goog.base(this, 'getTokenType', token)
};
//------------------------------------------------------------------------------
//                          Formatting value calculation
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieSeries.prototype.checkPlot = function (plot) {
    goog.base(this, 'checkPlot', plot);
    var plotTokensCash = plot.getTokensHash();

    plotTokensCash.increment('%DataPlotYSum', this.tokensHash_.get('%SeriesYSum'));
    plotTokensCash.increment('%YBasedPointsCount', this.points_.length);


    if (this.tokensHash_.get('%SeriesYMax') > plotTokensCash.get('%DataPlotYMax')) {
        plotTokensCash.add('%DataPlotYMax', this.tokensHash_.get('%SeriesYMax'));
        plotTokensCash.add('%DataPlotMaxYValuePointName', this.tokensHash_.get('%SeriesMaxYValuePointName'));
        plotTokensCash.add('%DataPlotMaxYValuePointSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYMin') < plotTokensCash.get('%DataPlotYMin')) {
        plotTokensCash.add('%DataPlotYMin', this.tokensHash_.get('%SeriesYMin'));
        plotTokensCash.add('%DataPlotMinYValuePointName', this.tokensHash_.get('%SeriesMinYValuePointName'));
        plotTokensCash.add('%DataPlotMinYValuePointSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYSum') > plotTokensCash.get('%DataPlotMaxYSumSeries')) {
        plotTokensCash.add('%DataPlotMaxYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensCash.add('%DataPlotMaxYSumSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYSum') > plotTokensCash.get('%DataPlotMinYSumSeries')) {
        plotTokensCash.add('%DataPlotMinYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensCash.add('%DataPlotMinYSumSeriesName', this.name_);
    }
};
anychart.plots.piePlot.data.PieSeries.prototype.checkPoint = function (dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);
    this.tokensHash_.increment('%SeriesYSum', dataPoint.getY());

    if (dataPoint.getY() > this.tokensHash_.get('%SeriesYMax')) {
        this.tokensHash_.add('%SeriesYMax', dataPoint.getY());
        this.tokensHash_.add('%SeriesMaxYValuePointName', dataPoint.getName());
    }
    if (dataPoint.getY() < this.tokensHash_.get('%SeriesYMin')) {
        this.tokensHash_.add('%SeriesYMin', dataPoint.getY());
        this.tokensHash_.add('%SeriesMinYValuePointName', dataPoint.getName());
    }
};

anychart.plots.piePlot.data.PieSeries.prototype.calculateSeriesTokenValues = function () {
    goog.base(this, 'calculateSeriesTokenValues');

    this.tokensHash_.add(
        '%SeriesYAverage',
        this.tokensHash_.get('%SeriesYSum') / this.points_.length);

    if (this.points_ && this.getPointAt(0))
        this.tokensHash_.add('%SeriesFirstYValue', this.getPointAt(0).getY());

    if (this.points_ && this.getPointAt(this.points_.length - 1))
        this.tokensHash_.add('%SeriesLastYValue',
            this.getPointAt(this.points_.length - 1).getY());
};
//------------------------------------------------------------------------------
//
//                           Pie3DPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.piePlot.data.PiePoint}
 */
anychart.plots.piePlot.data.Pie3DPoint = function () {
    anychart.plots.piePlot.data.PiePoint.call(this);
};
goog.inherits(anychart.plots.piePlot.data.Pie3DPoint,
    anychart.plots.piePlot.data.PiePoint);
//------------------------------------------------------------------------------
//                           Drawing properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DPoint.prototype.startSideShape_ = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.endSideShape_ = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.topSideShape_ = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.frontSideShape_ = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.backSideShape_ = null;
/**
 * @private
 * @type {anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.frontSideDrawer_ = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.getFrontSideDrawer = function () {
    return this.frontSideDrawer_;
};
/**
 * @private
 * @type {anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.backSideDrawer_ = null;
//------------------------------------------------------------------------------
//                           Other properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DPoint.prototype.ANGLE_DRIFT = 0.001;
anychart.plots.piePlot.data.Pie3DPoint.prototype.topBounds = null;
anychart.plots.piePlot.data.Pie3DPoint.prototype.connectorShapeAdded_ = false;
//------------------------------------------------------------------------------
//                           Initialize
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DPoint.prototype.initialize = function (svgManager) {
    this.startSideShape_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.startSideShape_.setId('PiePlot__StartSideShape');

    this.endSideShape_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.endSideShape_.setId('PiePlot__EndSideShape');
    this.topSideShape_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.topSideShape_.setId('PiePlot__TopSideShape');

    this.frontSideShape_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.frontSideShape_.setId('PiePlot__FrontSideShape');

    this.backSideShape_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.backSideShape_.setId('PiePlot__BackSideShape');

    goog.base(this, 'initialize', svgManager);

    this.frontSideDrawer_ = new anychart.plots.piePlot.data.drawers.PiePoint3DFrontSideDrawer(svgManager);
    this.backSideDrawer_ = new anychart.plots.piePlot.data.drawers.PiePoint3DBackSideDrawer(svgManager);

    return this.sprite_;
};
//------------------------------------------------------------------------------
//                           Bounds
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DPoint.prototype.getCenterPoint = function (pt) {
    if (!pt) pt = new anychart.utils.geom.Point();

    pt.x = this.getPlot().getCenterPoint().x;
    pt.y = this.getPlot().getCenterPoint().y;
    if (this.isExploded_) {
        var xR = this.getPlot().getExplodeRadius();
        var yR = this.getPlot().getExplodeYRadius();

        var centerAngle = (this.startAngle_ + this.endAngle_) / 2;
        pt.x += anychart.utils.geom.DrawingUtils.getPointX(xR, centerAngle);
        pt.y += anychart.utils.geom.DrawingUtils.getPointY(yR, centerAngle);
    }
    return pt;
};


anychart.plots.piePlot.data.Pie3DPoint.prototype.calcBounds = function () {
    var center = this.getCenterPoint();
    var h = this.getPlot().get3DHeight();

    this.topBounds_ = new anychart.utils.geom.Rectangle(
        center.x - this.series_.getOuterRadius(),
        center.y - this.series_.getOuterYRadius(),
        this.series_.getOuterRadius() * 2,
        this.series_.getOuterYRadius() * 2);

    this.bounds_ = this.topBounds_.clone();
    this.bounds_.height += h;
};
//------------------------------------------------------------------------------
//                           Angles calculation
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DPoint.prototype.calculateAngles = function () {
    goog.base(this, 'calculateAngles');
    if (this.isMissing_) return;

    this.getPlot().getDepthManager().addTopSide(this.topSideShape_);
    this.getPlot().getDepthManager().addSide(
        this.startAngle_ + this.ANGLE_DRIFT,
        this.startSideShape_,
        this.series_.getIndex());

    this.getPlot().getDepthManager().addSide(
        this.endAngle_ - this.ANGLE_DRIFT,
        this.endSideShape_,
        this.series_.getIndex());

    if (!this.isMissing_) {
        this.addEventHandlers(this.startSideShape_);
        this.addEventHandlers(this.endSideShape_);
        this.addEventHandlers(this.topSideShape_);
    }

    var i;

    if (this.hasFrontSide_()) {
        this.frontSideDrawer_.initialize(this);
        this.getPlot().getDepthManager().addFrontSide(this.frontSideDrawer_, this.series_.getIndex());

        for (i = 0; i < this.frontSideDrawer_.getLength(); i++)
            this.addEventHandlers(this.frontSideDrawer_.getSprite(i));
    }

    if (this.hasBackSide()) {
        this.backSideDrawer_.initialize(this);
        this.getPlot().getDepthManager().addBackSide(this.backSideDrawer_, this.series_.getIndex());

        for (i = 0; i < this.backSideDrawer_.getLength(); i++)
            this.addEventHandlers(this.backSideDrawer_.getSprite(i));
    }
};
/**
 * @private
 * @return {Boolean}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.hasFrontSide_ = function () {
    return this.frontSideDrawer_.isEnabled(this.startAngle_, this.endAngle_);
};
/**
 * @private
 * @return {Boolean}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.hasBackSide = function () {
    return this.backSideDrawer_.isEnabled(this.startAngle_, this.endAngle_);
};
//------------------------------------------------------------------------------
//                           Drawing
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getFrontSidePath = function () {
    var pathData = '';
    if (this.frontSideDrawer_) {
        for (var i = 0; i < this.frontSideDrawer_.getLength(); i++) {
            pathData += this.frontSideDrawer_.draw(i);
        }
        pathData += this.getSVGManager().pFinalize();
        return pathData;
    } else return null
};

/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getBackSidePath = function () {
    var pathData = '';
    if (this.backSideDrawer_) {
        for (var i = 0; i < this.backSideDrawer_.getLength(); i++) {
            pathData += this.backSideDrawer_.draw(i);
        }
        pathData += this.getSVGManager().pFinalize();
        return pathData;
    } else return null
};

/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getTopSidePath = function () {
    var outerXR = this.series_.getOuterRadius();
    var outerYR = this.series_.getOuterYRadius();

    var innerXR = this.series_.getInnerRadius();
    var innerYR = this.series_.getInnerYRadius();

    var startAngle = this.startAngle_ % 360 * Math.PI / 180;
    var endAngle = this.endAngle_ % 360 * Math.PI / 180;

    if (this.startAngle_ % 360 == this.endAngle_ % 360) {
        startAngle = 0;
        endAngle = 0;
    }

    var center = this.getCenterPoint();

    var innerStartX = center.x + innerXR * Math.cos(startAngle);
    var innerStartY = center.y + innerYR * Math.sin(startAngle);

    var innerEndX = center.x + innerXR * Math.cos(endAngle);
    var innerEndY = center.y + innerYR * Math.sin(endAngle);

    var outerStartX = center.x + outerXR * Math.cos(startAngle);
    var outerStartY = center.y + outerYR * Math.sin(startAngle);

    var outerEndX = center.x + outerXR * Math.cos(endAngle);
    var outerEndY = center.y + outerYR * Math.sin(endAngle);

    var largeArc1 = false;
    var sweepArc1 = true;

    var largeArc2 = false;
    var sweepArc2 = false;

    if (this.sweepAngle_ >= 180) {
        largeArc1 = !largeArc1;
//        sweepArc1 = !sweepArc1;

        largeArc2 = !largeArc2;
//        sweepArc2 = !sweepArc2;

    }
    if (this.topSideShape_ != null) {
        var svgManager = this.topSideShape_.getSVGManager();
        var pathData = '';
        pathData += svgManager.pMove(innerStartX, innerStartY);
        pathData += svgManager.pCircleArc(innerXR, largeArc1, sweepArc1, innerEndX, innerEndY, innerYR);
        pathData += svgManager.pLine(outerEndX, outerEndY);
        pathData += svgManager.pCircleArc(outerXR, largeArc2, sweepArc2, outerStartX, outerStartY, outerYR);
        pathData += svgManager.pLine(innerStartX, innerStartY);

        if (outerStartX == outerEndX && outerStartY == outerEndY && (this.startAngle_ % 360 == this.endAngle_ % 360) /*&& this.startAngle_ != 360*/) {
            pathData = '';
            pathData += svgManager.pMove(innerStartX, innerStartY);
            pathData += svgManager.pCircleArc(innerXR, 1, 1, innerEndX, innerEndY - .001, innerYR);
            pathData += svgManager.pMove(outerStartX, outerStartY);
            pathData += svgManager.pCircleArc(outerXR, 1, 0, outerEndX, outerEndY + .001, outerYR);
            pathData += svgManager.pLine(innerStartX, innerStartY);
        }
        pathData += svgManager.pFinalize();
        return pathData;
    } else return null;
};

/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getStartSidePath = function () {
    if (this.startSideShape_ != null) {
        var svgManager = this.startSideShape_.getSVGManager();
        return this.getSidePath_(svgManager, this.startAngle_);
    } else return null;
};

/**
 * ND: Needs doc!
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getEndSidePath = function () {
    if (this.endSideShape_ != null) {
        var svgManager = this.endSideShape_.getSVGManager();
        return this.getSidePath_(svgManager, this.endAngle_);
    } else return null;
};

/**
 * @return {String}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getSidePath_ = function (svgManager, angle) {
    if (angle == NaN) return '';
    var center = this.getCenterPoint();
    var pathData = '';
    var utils = anychart.utils.geom.DrawingUtils;
    var outerXR = this.series_.getOuterRadius();
    var outerYR = this.series_.getOuterYRadius();
    var innerXR = this.series_.getInnerRadius();
    var innerYR = this.series_.getInnerYRadius();

    var h = this.getPlot().get3DHeight();
    var pt1 = new anychart.utils.geom.Point(
        center.x + utils.getPointX(innerXR, angle),
        center.y + utils.getPointY(innerYR, angle));

    var pt2 = new anychart.utils.geom.Point(
        center.x + utils.getPointX(outerXR, angle),
        center.y + utils.getPointY(outerYR, angle));

    var pt3 = new anychart.utils.geom.Point(pt2.x, pt2.y + h);
    var pt4 = new anychart.utils.geom.Point(pt1.x, pt1.y + h);

    pathData += svgManager.pMove(pt1.x, pt1.y);
    pathData += svgManager.pLine(pt2.x, pt2.y);
    pathData += svgManager.pLine(pt3.x, pt3.y);
    pathData += svgManager.pLine(pt4.x, pt4.y);
    pathData += svgManager.pLine(pt1.x, pt1.y);
    pathData += svgManager.pFinalize();

    return pathData;
};
/**
 * Used only for programmatic style.
 * @param {anychart.svg.SVGManager} svgManager
 * @return {SVGElement}
 */
anychart.plots.piePlot.data.Pie3DPoint.prototype.getSmilePath = function () {
    var svgManager = this.getSVGManager();
    var center = this.getCenterPoint();
    var pathData = '';
    var wRadius = this.series_.getOuterRadius();
    var hRadius = this.series_.getOuterYRadius();

    var dH = this.getPlot().getRadiusByFactor(.03);

    var j, rX, rY;

    for (var i = 0; i < this.frontSideDrawer_.getLength(); i++) {
        var start = this.frontSideDrawer_.getStartAngleAt(i) - .275;
        var end = this.frontSideDrawer_.getEndAngle(i) + .275;
        this.frontSideDrawer_.normalizeAngles(start, end);

        for (j = start; j <= end; j++) {
            rX = (dH / 2) * Math.abs(Math.sin(j * Math.PI / 180));
            rY = (dH / 2) * Math.abs(Math.sin(j * Math.PI / 180));
            if (j == start)
                pathData += svgManager.pMove(
                    center.x + anychart.utils.geom.DrawingUtils.getPointX(wRadius - rX, j),
                    center.y + anychart.utils.geom.DrawingUtils.getPointY(hRadius - rY, j));
            else
                pathData += svgManager.pLine(
                    center.x + anychart.utils.geom.DrawingUtils.getPointX(wRadius - rX, j),
                    center.y + anychart.utils.geom.DrawingUtils.getPointY(hRadius - rY, j));
        }
        rX = (dH / 2) * Math.abs(Math.sin(end * Math.PI / 180));
        rY = (dH / 2) * Math.abs(Math.sin(end * Math.PI / 180));
        pathData += svgManager.pLine(
            center.x + anychart.utils.geom.DrawingUtils.getPointX(wRadius - rX, end),
            center.y + anychart.utils.geom.DrawingUtils.getPointY(hRadius - rY, end));

        for (j = end; j >= start; j--) {
            rX = (dH / 2) * Math.abs(Math.sin(j * Math.PI / 180));
            rY = (dH / 2) * Math.abs(Math.sin(j * Math.PI / 180));

            pathData += svgManager.pLine(
                center.x + anychart.utils.geom.DrawingUtils.getPointX(wRadius + rX, j),
                center.y + anychart.utils.geom.DrawingUtils.getPointY(hRadius + rY, j));
        }

        rX = (dH / 2) * Math.abs(Math.sin(start * Math.PI / 180));
        rY = (dH / 2) * Math.abs(Math.sin(start * Math.PI / 180));
        pathData += svgManager.pLine(
            center.x + anychart.utils.geom.DrawingUtils.getPointX(wRadius - rX, start),
            center.y + anychart.utils.geom.DrawingUtils.getPointY(hRadius - rY, start));
        pathData += svgManager.pFinalize();
    }
    return pathData;
};
//------------------------------------------------------------------------------
//                    Connectors
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.piePlot.data.Pie3DPoint.prototype.drawConnectorSegment_ = function (svgManager, firstPoint, secondPoint, thirdPoint) {
    var plotRadius = this.series_.getOuterRadius();
    var plotYRadius = this.series_.getOuterYRadius();

    var angle = (this.startAngle_ + this.endAngle_) / 2;
    var du = anychart.utils.geom.DrawingUtils;

    firstPoint.y += this.getPlot().get3DHeight() / 2;
    firstPoint.x += du.getPointX(plotRadius, angle);
    firstPoint.y += du.getPointY(plotYRadius, angle);

    if (!this.connectorShapeAdded_) {
        if (Math.sin(angle * Math.PI / 180) < 0)
            this.getPlot().getDepthManager().addBackSideConnector(this.connectorElement_);
        else
            this.getPlot().getDepthManager().addFrontSideConnector(this.connectorElement_);
        this.connectorShapeAdded_ = true;
    }
    return this.doConnectorDrawing_(svgManager, firstPoint, secondPoint, thirdPoint);
};
//------------------------------------------------------------------------------
//                    Labels positioning
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.piePlot.data.Pie3DPoint.prototype.setAnchorPoint = function (position, anchor, bounds, padding) {
    this.getCenterPoint(position);

    var outerXR = this.series_.getOuterRadius();
    var outerYR = this.series_.getOuterYRadius();
    var innerXR = this.series_.getInnerRadius();
    var innerYR = this.series_.getInnerYRadius();

    var Anchor = anychart.layout.Anchor;

    var angle, xRadius, yRadius;
    switch (anchor) {
        case Anchor.CENTER:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            xRadius = (innerXR + outerXR) / 2;
            yRadius = (innerYR + outerYR) / 2;
            break;
        case Anchor.CENTER_BOTTOM:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            xRadius = innerXR;
            yRadius = innerYR;
            break;
        case Anchor.CENTER_LEFT:
            angle = this.startAngle_;
            xRadius = (innerXR + outerXR) / 2;
            yRadius = (innerYR + outerYR) / 2;
            break;
        case Anchor.CENTER_RIGHT:
            angle = this.endAngle_;
            xRadius = (innerXR + outerXR) / 2;
            yRadius = (innerYR + outerYR) / 2;
            break;
        case Anchor.CENTER_TOP:
            angle = this.startAngle_ + this.sweepAngle_ / 2;
            xRadius = outerXR;
            yRadius = outerYR;
            break;
        case Anchor.LEFT_BOTTOM:
            angle = this.startAngle_;
            xRadius = innerXR;
            yRadius = innerYR;
            break;
        case Anchor.LEFT_TOP:
            angle = this.startAngle_;
            xRadius = outerXR;
            yRadius = outerYR;
            break;
        case Anchor.RIGHT_BOTTOM:
            angle = this.endAngle_;
            xRadius = innerXR;
            yRadius = innerYR;
            break;
        case Anchor.RIGHT_TOP:
            angle = this.endAngle_;
            xRadius = outerXR;
            yRadius = outerYR;
            break;
    }
    position.x += anychart.utils.geom.DrawingUtils.getPointX(xRadius, angle);
    position.y += anychart.utils.geom.DrawingUtils.getPointY(yRadius, angle);
};

//------------------------------------------------------------------------------
//
//                           Pie3DSeries class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.piePlot.data.PieSeries}
 */
anychart.plots.piePlot.data.Pie3DSeries = function () {
    anychart.plots.piePlot.data.PieSeries.call(this);
};
goog.inherits(anychart.plots.piePlot.data.Pie3DSeries,
    anychart.plots.piePlot.data.PieSeries);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.Pie3DSeries.prototype.outerYRadius_ = null;
/**
 * @return {Number}
 */
anychart.plots.piePlot.data.Pie3DSeries.prototype.getOuterYRadius = function () {
    return this.outerYRadius_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.piePlot.data.Pie3DSeries.prototype.innerYRadius_ = null;
/**
 * @return {Number}
 */
anychart.plots.piePlot.data.Pie3DSeries.prototype.getInnerYRadius = function () {
    return this.innerYRadius_;
};
//------------------------------------------------------------------------------
//                           Override
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.Pie3DSeries.prototype.createPoint = function () {
    return new anychart.plots.piePlot.data.Pie3DPoint();
};
/**@inheritDoc*/
anychart.plots.piePlot.data.Pie3DSeries.prototype.setPixelRadius = function () {
    goog.base(this, 'setPixelRadius');
    this.outerYRadius_ = this.getPlot().getYRadius(this.outerRadius_);
    this.innerYRadius_ = this.getPlot().getYRadius(this.innerRadius_);
};
//------------------------------------------------------------------------------
//
//                      PieGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * Class represent pie global series settings, need to clarifies pie series settings.
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.GlobalSeriesSettings}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings = function () {
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.apply(this);
    this.applyDataPaletteToSeries_ = true;
};
//inheritance
goog.inherits(anychart.plots.piePlot.data.PieGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                           Explode properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.explodeRadius_ = .1;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getExplodeRadius = function () {
    return this.explodeRadius_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.hasExplodedItems_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.setHasExplodedItems = function (value) {
    this.hasExplodedItems_ = value;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.hasExplodedItems = function () {
    return this.hasExplodedItems_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isMultiExplodeAllowed_ = true;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isMultiExplodeAllowed = function () {
    return this.isMultiExplodeAllowed_;
};

anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.explodeOnClick_ = true;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.setExplodeOnClick = function (value) {
    this.explodeOnClick_ = value;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isExplodeOnClick = function () {
    return this.explodeOnClick_;
};
//------------------------------------------------------------------------------
//                           Radius properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.radius_ = 1;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getRadius = function () {
    return this.radius_;
};

anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.innerRadius_ = .3;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getInnerRadius = function () {
    return this.innerRadius_;
};

anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.startAngle_ = 0;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getStartAngle = function () {
    return this.startAngle_;
};
//------------------------------------------------------------------------------
//                           Labels properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.allowLabelsOverlap_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.allowLabelsOverlap = function () {
    return this.allowLabelsOverlap_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isOutsideLabels_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.disableOutsideLabels = function () {
    this.isOutsideLabels_ = false;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isOutsideLabels = function () {
    return this.isOutsideLabels_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isPercentLabelsPadding_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isPercentLabelsPadding = function () {
    return this.isPercentLabelsPadding_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.labelsPadding_ = 20;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getLabelsPadding = function () {
    return this.labelsPadding_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.labelsEnabled_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isLabelsEnabled = function () {
    return this.labelsEnabled_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.smartLabelsEnabled_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isSmartLabelsEnabled = function () {
    return this.smartLabelsEnabled_;
};
//------------------------------------------------------------------------------
//                           Connector properties
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.connectorStroke_ = null;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getConnectorStroke = function () {
    return this.connectorStroke_;
};

anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.connectorEnabled_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isConnectorEnabled = function () {
    return this.connectorEnabled_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isPercentConnectorPadding_ = false;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.isPercentConnectorPadding = function () {
    return this.isPercentConnectorPadding_;
};
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.connectorPadding_ = 5;
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getConnectorPadding = function () {
    return this.connectorPadding_;
};
//------------------------------------------------------------------------------
//                           Settings
//------------------------------------------------------------------------------
/**
 * Identifies whether connector is presented.
 * return {Boolean]
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.hasConnector = function () {
    return this.connectorStroke_ && this.isConnectorEnabled();
};

/**
 * Create pie series.
 * @public
 * @return {anychart.plots.piePlot.data.PieSeries}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.createSeries = function () {
    var series = this.plot_.is3D() ?
        new anychart.plots.piePlot.data.Pie3DSeries() : new anychart.plots.piePlot.data.PieSeries();
    series.setType(anychart.series.SeriesType.PIE);
    return series;
};

/**
 * Return pie series settings node name.
 * @public
 * @return {string}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'pie_series';
};

/**
 * Return pie series style node name.
 * @public
 * @return {string}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'pie_style';
};
/**
 * Return pie style state class.
 * @public
 * @return {anychart.plots.piePlot.data.styles.PieStyle}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.createStyle = function (styleConfig) {
    if (this.plot_.is3D())
        return new anychart.plots.piePlot.data.styles.Pie3DStyle();
    else {
        var des = anychart.utils.deserialization;
        if (des.hasProp(styleConfig, 'name')) {
            var styleName = des.getLStringProp(styleConfig, 'name');
            if (styleName == 'aqua')
                return new anychart.plots.piePlot.data.styles.PieAquaStyle();
        }
        return new anychart.plots.piePlot.data.styles.PieStyle();
    }
};
/**
 * @param {Boolean} opt_isExtra
 * @return {anychart.plots.piePlot.labels.PieLabelElement}
 */
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.createLabelElement = function (opt_isExtra) {
    if (opt_isExtra == null || opt_isExtra == undefined) opt_isExtra = false;
    var label = new anychart.plots.piePlot.labels.PieLabelElement();
    label.setIsExtra(opt_isExtra);
    return label;
};
//------------------------------------------------------------------------------
//                           Deserialize
//------------------------------------------------------------------------------
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);

    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'radius')) this.radius_ = des.getNumProp(config, 'radius') / 100;
    if (des.hasProp(config, 'inner_radius')) this.innerRadius_ = des.getNumProp(config, 'inner_radius') / 100;
    if (des.hasProp(config, 'explode_on_click')) this.explodeOnClick_ = des.getBoolProp(config, 'explode_on_click');
    if (des.hasProp(config, 'explode')) this.explodeRadius_ = des.getNumProp(config, 'explode') / 100;
    if (des.hasProp(config, 'allow_multiple_expand')) this.isMultiExplodeAllowed_ = des.getBoolProp(config, 'allow_multiple_expand');
    if (des.hasProp(config, 'start_angle')) this.startAngle_ = des.getNumProp(config, 'start_angle');

    if (des.hasProp(config, 'label_settings')) {
        var label = des.getProp(config, 'label_settings');
        if (des.hasProp(label, 'mode')) {
            this.labelsEnabled_ = true;
            this.isOutsideLabels_ = des.getLStringProp(label, 'mode') == 'outside';
        }

        if (des.hasProp(label, 'smart_labels'))
            this.smartLabelsEnabled_ = des.getBoolProp(label, 'smart_labels');

        if (this.isOutsideLabels_) {
            if (des.hasProp(label, 'allow_overlap')) this.allowLabelsOverlap_ = des.getBoolProp(label, 'allow_overlap');
            if (des.hasProp(label, 'position')) {
                var pos = des.getProp(label, 'position');
                if (des.hasProp(pos, 'padding')) {
                    var padding = des.getProp(pos, 'padding');
                    this.isPercentLabelsPadding_ = des.isPercent(padding);
                    this.labelsPadding_ = this.isPercentLabelsPadding_ ? des.getPercent(padding) :
                        des.getNum(padding);
                }
            }
            var stroke = des.getProp(config, 'connector');
            if (des.isEnabled(stroke)) {
                this.connectorEnabled_ = true;
                this.connectorStroke_ = new anychart.visual.stroke.Stroke();
                this.connectorStroke_.deserialize(stroke);
                if (des.hasProp(stroke, "padding")) {
                    var cPadding = des.getProp(stroke, "padding");
                    if (des.isPercent(cPadding)) {
                        this.isPercentConnectorPadding_ = true;
                        this.connectorPadding_ = des.getPercent(cPadding);
                    } else {
                        this.connectorPadding_ = des.getNum(cPadding);
                    }
                }
            }
        }
    }
};
//------------------------------------------------------------------------------
//                  Sorting points.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.canPreSortPoints = function () {
    return true;
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.canSortPoints = function () {
    return false;
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.execSortPoints = function (points, sortDescending) {
    sortDescending ?
        points.sort(function (a, b) {
            return b.getY() - a.getY();
        }) :
        points.sort(function (a, b) {
            return a.getY() - b.getY();
        });
};
/**@inheritDoc*/
anychart.plots.piePlot.data.PieGlobalSeriesSettings.prototype.execSortJSONPoints = function (points, sortDescending) {
    //fast way to copy array
    points = points.slice();

    //execute sorting by y
    sortDescending ?
        points.sort(function (a, b) {
            var des = anychart.utils.deserialization;
            return des.getNumProp(b, 'y') - des.getNumProp(a, 'y');
        }) :
        points.sort(function (a, b) {
            var des = anychart.utils.deserialization;
            return des.getNumProp(a, 'y') - des.getNumProp(b, 'y');
        });

    return points;
};
