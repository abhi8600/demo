/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.piePlot.labels.PieLabelElement};</li>
 *  <li>@class {anychart.plots.piePlot.labels.LabelInfo};</li>
 *  <li>@class {anychart.plots.piePlot.labels.Cluster}.</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.labels');
goog.require('anychart.elements.label');
goog.require('anychart.plots.piePlot.labels.styles');
//------------------------------------------------------------------------------
//
//                           PieLabelElement class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.piePlot.labels.PieLabelElement = function() {
    anychart.plots.seriesPlot.elements.LabelElement.call(this);
};
goog.inherits(anychart.plots.piePlot.labels.PieLabelElement,
        anychart.plots.seriesPlot.elements.LabelElement);
//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @type {Boolean}
 */
anychart.plots.piePlot.labels.PieLabelElement.prototype.isExtra_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.piePlot.labels.PieLabelElement.prototype.isExtra = function() {
    return this.isExtra_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.piePlot.labels.PieLabelElement.prototype.setIsExtra = function(value) {
    this.isExtra_ = value;
};
anychart.plots.piePlot.labels.PieLabelElement.prototype.createStyle = function() {
    return new anychart.plots.piePlot.labels.styles.PieLabelStyle();
};
//------------------------------------------------------------------------------
//                           Override
//------------------------------------------------------------------------------
/**
 * @param {anychart.elements.IElementContainer} container
 * @param {anychart.styles.StyleState} state
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.piePlot.labels.PieLabelElement.prototype.getPosition = function(container, state, sprite) {
    if(this.isExtra_)
        return goog.base(this, 'getPosition', container,state, sprite);
    
    return container.getLabelPosition(
            this,
            state,
            this.getBounds(container, state, sprite));
};
/**
 * @param {*} opt_target
 * @return {anychart.plots.piePlot.labels.PieLabelElement}
 */
anychart.plots.piePlot.labels.PieLabelElement.prototype.copy = function(opt_target) {
    opt_target = goog.base(this, 'copy', opt_target);
    opt_target.setIsExtra(this.isExtra_);
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                           LabelInfo class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param angle
 * @param bounds
 * @param point
 * @param index
 */
anychart.plots.piePlot.labels.LabelInfo = function(angle, bounds, point, index) {
    this.angle_ = angle;
    this.height_ = bounds.height;
    this.width_ = bounds.width;
    this.point_ = point;
    this.isShifted_ = false;
    this.enabled_ = true;
    this.index_ = index;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.angle_ = NaN;

anychart.plots.piePlot.labels.LabelInfo.prototype.getAngle = function() {
    return this.angle_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setAngle = function(value) {
    this.angle_ = value;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.height_ = NaN;

anychart.plots.piePlot.labels.LabelInfo.prototype.getHeight = function() {
    return this.height_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setHeight = function(value) {
    this.height_ = value;
};
anychart.plots.piePlot.labels.LabelInfo.prototype.width_ = NaN;

anychart.plots.piePlot.labels.LabelInfo.prototype.getWidth = function() {
    return this.width_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setWidth = function(value) {
    this.width_ = value;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.point_ = null;

anychart.plots.piePlot.labels.LabelInfo.prototype.getPoint = function() {
    return this.point_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setPoint = function(value) {
    this.point_ = value;
};


anychart.plots.piePlot.labels.LabelInfo.prototype.isShifted_ = false;

anychart.plots.piePlot.labels.LabelInfo.prototype.isShifted = function() {
    return this.isShifted_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setShifted = function(value) {
    this.isShifted_ = value;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.enabled_ = false;

anychart.plots.piePlot.labels.LabelInfo.prototype.isEnabled = function() {
    return this.enabled_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setEnabled = function(value) {
    this.enabled_ = value;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.index_ = NaN;

anychart.plots.piePlot.labels.LabelInfo.prototype.getIndex = function() {
    return this.index_;
};

anychart.plots.piePlot.labels.LabelInfo.prototype.setIndex = function(value) {
    this.index_ = value;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                           Cluster class.
//
//----------------------------------------------------------------------------------------------------------------------
/**
 * @constructor
 * @param label
 * @param criticalAngle
 * @param plot
 */
anychart.plots.piePlot.labels.Cluster = function(label, criticalAngle, plot) {
    this.plot_ = plot;
    this.initialAngle_ = this.normalizeAngle_(label.angle_, -90, 270, 90);
    label.angle_ = this.initialAngle_;
    this.left_ = this.between_(this.initialAngle_, 90, 270);

    this.labels_ = [];
    this.labels_.push(label);

    this.labelStartAngles_ = [];
    this.labelStartAngles_.push(label.angle_);

    this.labelBottomShifts_ = [];
    this.labelBottomShifts_.push(label.height_);

    this.totalHeight_ = label.height_;
    this.currentHeight_ = this.totalHeight_;
    if (this.left_) {
        this.currentMagicNumber_ = plot.getLabelPosition_(label, false).y + this.totalHeight_ / 2;
        this.currentBottom_ = this.currentMagicNumber_;
        this.currentTop_ = this.currentBottom_ - this.totalHeight_;
    } else {
        this.currentMagicNumber_ = plot.getLabelPosition_(label, false).y - this.totalHeight_ / 2;
        this.currentTop_ = this.currentMagicNumber_;
        this.currentBottom_ = this.currentTop_ + this.totalHeight_;
    }

    this.criticalAngle_ = criticalAngle;
    this.maximumTop_ = Number.MAX_VALUE;
    this.minimumTop_ = -Number.MAX_VALUE;
    this.updateBounds_(0);
};

anychart.plots.piePlot.labels.Cluster.prototype.left_ = false;
anychart.plots.piePlot.labels.Cluster.prototype.criticalAngle_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.labels_ = null;
anychart.plots.piePlot.labels.Cluster.prototype.labelStartAngles_ = null;
anychart.plots.piePlot.labels.Cluster.prototype.labelBottomShifts_ = null;
anychart.plots.piePlot.labels.Cluster.prototype.currentTop_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.currentBottom_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.maximumTop_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.minimumTop_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.effectiveRestrictionIndex_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.currentMagicNumber_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.currentHeight_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.totalHeight_ = NaN;
anychart.plots.piePlot.labels.Cluster.prototype.plot_ = null;
anychart.plots.piePlot.labels.Cluster.prototype.initialAngle_ = NaN;

anychart.plots.piePlot.labels.Cluster.prototype.getInitialAngle = function() {
    return this.initialAngle_;
};
anychart.plots.piePlot.labels.Cluster.prototype.getTotalHeight = function() {
    return this.totalHeight_;
};

anychart.plots.piePlot.labels.Cluster.prototype.updateBounds_ = function(index) {
    var label = this.labels_[index];
    var currAngle = label.getAngle();
    var y, tmp = 0;
    if (this.left_) {
        label.setAngle(Math.max(90.01, this.normalizeAngle_(this.labelStartAngles_[index] - this.criticalAngle_, -90, 270, 90)));
        y = this.plot_.getLabelPosition_(label, false).y - label.getHeight() / 2 + this.labelBottomShifts_[index];
        tmp = this.maximumTop_;
        this.maximumTop_ = Math.min(tmp, y);

        label.setAngle(Math.min(270, this.normalizeAngle_(this.labelStartAngles_[index] + this.criticalAngle_, 90, 450, 270)));
        y = this.plot_.getLabelPosition_(label, false).y - label.getHeight() / 2 + this.labelBottomShifts_[index];
        if (this.minimumTop_ < y) {
            this.minimumTop_ = y;
            this.effectiveRestrictionIndex_ = index;
        }
    } else {
        label.setAngle(Math.max(-90, this.normalizeAngle_(this.labelStartAngles_[index] - this.criticalAngle_, -270, 90, -90)));
        y = this.plot_.getLabelPosition_(label, false).y + label.getHeight() / 2 - this.labelBottomShifts_[index];
        tmp = this.minimumTop_;
        this.minimumTop_ = Math.max(tmp, y);

        label.setAngle(Math.min(90, this.normalizeAngle_(this.labelStartAngles_[index] + this.criticalAngle_, -90, 270, 90)));
        y = this.plot_.getLabelPosition_(label, false).y + label.getHeight() / 2 - this.labelBottomShifts_[index];
        if (this.maximumTop_ > y) {
            this.maximumTop_ = y;
            this.effectiveRestrictionIndex_ = index;
        }
    }

    label.setAngle(currAngle);
};

anychart.plots.piePlot.labels.Cluster.prototype.checkOverlap = function(cluster) {
    if (cluster.left_ != this.left_) return false;
    if (this.left_)
        return this.currentTop_ <= cluster.currentBottom_;
    else
        return this.currentBottom_ >= cluster.currentTop_;
};

anychart.plots.piePlot.labels.Cluster.prototype.merge = function(cluster) {
    var len = cluster.labels_.length;
    for (var i = 0; i < len; i++) {
        var label = cluster.labels_[i];
        label.setAngle(cluster.labelStartAngles_[i]);
        this.labels_.push(label);
        this.labelStartAngles_.push(label.getAngle());
        this.totalHeight_ += label.getHeight();
        this.labelBottomShifts_.push(this.totalHeight_);
        if (this.left_)
            this.currentMagicNumber_ += this.plot_.getLabelPosition_(label, false).y + label.height_ / 2 + this.currentHeight_;
        else
            this.currentMagicNumber_ += this.plot_.getLabelPosition_(label, false).y - label.height_ / 2 - this.currentHeight_;
        this.currentHeight_ += label.getHeight();
        this.updateBounds_(this.labels_.length - 1);
    }

    if (this.left_) {
        this.currentBottom_ = this.currentMagicNumber_ / this.labels_.length;
        if (this.minimumTop_ <= this.maximumTop_) {
            if (this.currentBottom_ > this.maximumTop_)
                this.currentBottom_ = this.maximumTop_;
            if (this.currentBottom_ < this.minimumTop_)
                this.currentBottom_ = this.minimumTop_;
            this.currentTop_ = this.currentBottom_ - this.totalHeight_;
        } else {
            this.currentBottom_ = this.maximumTop_;
            this.currentTop_ = this.minimumTop_ - this.totalHeight_;
        }
    } else {
        this.currentTop_ = this.currentMagicNumber_ / this.labels_.length;
        if (this.minimumTop_ <= this.maximumTop_) {
            if (this.currentTop_ > this.maximumTop_)
                this.currentTop_ = this.maximumTop_;
            if (this.currentTop_ < this.minimumTop_)
                this.currentTop_ = this.minimumTop_;
            this.currentBottom_ = this.currentTop_ + this.totalHeight_;
        } else {
            this.currentTop_ = this.minimumTop_;
            this.currentBottom_ = this.maximumTop_ + this.totalHeight_;
        }
    }
};

anychart.plots.piePlot.labels.Cluster.prototype.applyPositioning = function() {
    var i;
    var len = this.labels_.length;

    var k;
    if (this.minimumTop_ <= this.maximumTop_)
        k = 1;
    else {
        var h = this.labelBottomShifts_[this.effectiveRestrictionIndex_];
        k = 1 - (this.minimumTop_ - this.maximumTop_) / h;
    }

    if (this.left_) {
        for (i = 0; i < len; i++)
            this.labels_[i].angle_ = this.checkLabelAngle_(180 - this.plot_.getLabelAngleInRightHalf(this.labels_[i], this.currentBottom_ - this.labelBottomShifts_[i] * k + this.labels_[i].height_ / 2), this.labelStartAngles_[i]);
    } else
        for (i = 0; i < len; i++)
            this.labels_[i].angle_ = this.checkLabelAngle_(this.plot_.getLabelAngleInRightHalf(this.labels_[i], this.currentTop_ + this.labelBottomShifts_[i] * k - this.labels_[i].height_ / 2), this.labelStartAngles_[i]);
};

anychart.plots.piePlot.labels.Cluster.prototype.crit90270_ = 88 * Math.PI / 180;
anychart.plots.piePlot.labels.Cluster.prototype.checkLabelAngle_ = function(angle, startAngle) {
    var old_angle = angle;
    angle = angle * Math.PI / 180;
    startAngle = startAngle * Math.PI / 180;
    if (Math.cos(angle) * Math.cos(startAngle) < 0 || Math.abs(Math.cos(angle)) < Math.cos(this.crit90270_)) {
        if (Math.sin(startAngle) > 0)
            return 90 - (Math.cos(startAngle) > 0 ? 1 : -1) * 2;
        else
            return 270 + (Math.cos(startAngle) > 0 ? 1 : -1) * 2;
    }
    else return old_angle;
};

anychart.plots.piePlot.labels.Cluster.compare = function(c1, c2) {
    return c1.initialAngle_ - c2.initialAngle_;
};

/**
 * <p>
 *     Note : верхняя не включительно
 * </p>
 * @private
 * @param {Number} angle
 * @param {Number} lowerAngle
 * @param {Number} upperAngle
 * @return {Boolean}
 */
anychart.plots.piePlot.labels.Cluster.prototype.between_ = function(angle, lowerAngle, upperAngle) {
    return angle >= lowerAngle && angle < upperAngle;
};
/**
 * <p>
 *     Note : верхняя не включительно
 * </p>
 * @param {Number} angle
 * @param {Number} lowerBound
 * @param {Number} upperBound
 * @param {Number} center
 */
anychart.plots.piePlot.labels.Cluster.prototype.normalizeAngle_ = function(angle, lowerBound, upperBound, center) { // верхняя не включительно
    while (angle < lowerBound)
        angle += 360;
    while (angle >= upperBound)
        angle -= 360;
    if (angle == lowerBound || angle == center)
        angle += 0.01;
    return angle;
};
