/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.templates.DEFAULT_TEMPLATE}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.templates.DEFAULT_TEMPLATE');

anychart.plots.treeMapPlot.templates.DEFAULT_TEMPLATE = {
	'chart': {
		'styles': {
			'tree_map_style': {
				'branch': {
					'background': {
						'border': {
							'enabled': 'true'
						},
						'fill': {
							'enabled': 'true'
						},
						'enabled': 'false'
					},
					'inside_margin': {
						'all': '0',
						'top': '-1'
					},
					'outside_margin': {
						'all': '0'
					},
					'header': {
						'text': {
							'value': '{%Name}'
						},
						'background': {
							'inside_margin': {
								'all': '0',
								'left': '6',
								'top': '2'
							},
							'enabled': 'true'
						},
						'enabled': 'true',
						'align': 'Near'
					}
				},
				'states': {
					'normal': {
						'branch': {
							'header': {
								'background': {
									'fill': {
										'gradient': {
											'key': [
												{
													'color': '#FDFDFD'
												},
												{
													'color': '#EBEBEB'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient'
									},
									'border': {
										'gradient': {
											'key': [
												{
													'color': '#898B8D'
												},
												{
													'color': '#5D5F60'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient',
										'opacity': '1'
									}
								},
								'enabled': 'true',
								'align': 'Near'
							}
						},
						'fill': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '%Color',
							'opacity': '1'
						},
						'border': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '#898B8D',
							'opacity': '1'
						}
					},
					'hover': {
						'branch': {
							'header': {
								'font': {
									'underline': 'true'
								},
								'background': {
									'fill': {
										'gradient': {
											'key': [
												{
													'color': '#DFF2FF'
												},
												{
													'color': '#99D7FF'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient'
									},
									'border': {
										'gradient': {
											'key': [
												{
													'color': '#009DFF'
												},
												{
													'color': '#0056B8'
												}
											],
											'angle': '90'
										},
										'type': 'Gradient'
									}
								}
							}
						},
						'fill': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '%Color',
							'opacity': '1'
						},
						'border': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '#898B8D',
							'opacity': '1'
						},
						'hatch_fill': {
							'enabled': 'true',
							'color': 'White',
							'type': 'Percent50',
							'opacity': '0.6'
						}
					},
					'pushed': {
						'branch': {
							'header': {
								'font': {
									'underline': 'true'
								},
								'background': {
									'fill': {
										'gradient': {
											'key': [
												{
													'color': '#DFF2FF'
												},
												{
													'color': '#99D7FF'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient'
									},
									'border': {
										'gradient': {
											'key': [
												{
													'color': '#009DFF'
												},
												{
													'color': '#0056B8'
												}
											],
											'angle': '90'
										},
										'type': 'Gradient'
									}
								}
							}
						},
						'fill': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '%Color',
							'opacity': '1'
						},
						'border': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '#898B8D',
							'opacity': '1'
						},
						'hatch_fill': {
							'enabled': 'true',
							'color': 'Gray',
							'type': 'Percent50',
							'opacity': '0.6'
						}
					},
					'selected_normal': {
						'branch': {
							'header': {
								'background': {
									'fill': {
										'gradient': {
											'key': [
												{
													'color': '#FDFDFD'
												},
												{
													'color': '#EBEBEB'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient'
									},
									'border': {
										'gradient': {
											'key': [
												{
													'color': '#898B8D'
												},
												{
													'color': '#5D5F60'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient',
										'opacity': '1'
									}
								},
								'enabled': 'true',
								'align': 'Near'
							}
						},
						'fill': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '%Color',
							'opacity': '1'
						},
						'border': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '#898B8D',
							'opacity': '1'
						},
						'hatch_fill': {
							'enabled': 'true',
							'color': 'Gray',
							'type': 'Percent50',
							'opacity': '0.7'
						}
					},
					'selected_hover': {
						'branch': {
							'header': {
								'font': {
									'underline': 'true'
								},
								'background': {
									'fill': {
										'gradient': {
											'key': [
												{
													'color': '#DFF2FF'
												},
												{
													'color': '#99D7FF'
												}
											],
											'angle': '90'
										},
										'enabled': 'true',
										'type': 'Gradient'
									},
									'border': {
										'gradient': {
											'key': [
												{
													'color': '#009DFF'
												},
												{
													'color': '#0056B8'
												}
											],
											'angle': '90'
										},
										'type': 'Gradient'
									}
								}
							}
						},
						'fill': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '%Color',
							'opacity': '1'
						},
						'border': {
							'enabled': 'true',
							'type': 'Solid',
							'color': '#898B8D',
							'opacity': '1'
						},
						'hatch_fill': {
							'enabled': 'true',
							'color': 'Gray',
							'type': 'Percent50',
							'opacity': '0.5'
						},
						'color': '%Color'
					}
				},
				'name': 'anychart_default'
			}
		},
		'data_plot_settings': {
			'tree_map': {
				'label_settings': {
					'position': {
						'anchor': 'Center',
						'valign': 'Center',
						'halign': 'Center'
					},
					'font': {
						'bold': 'false'
					},
					'format': {
						'value': '{%Name}'
					},
					'enabled': 'false'
				},
				'tooltip_settings': {
					'format': {
						'value': '{%Name} - {%YValue}{numDecimals:0}'
					},
					'enabled': 'false'
				},
				'interactivity': {
					'use_hand_cursor': 'false',
					'allow_select': 'true'
				},
				'marker_settings': {
				},
				'enable_drilldown': 'true'
			}
		}
	}
};
