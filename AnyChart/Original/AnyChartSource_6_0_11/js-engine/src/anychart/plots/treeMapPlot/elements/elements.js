/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains interfaces:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle}</li>
 * <ul>
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.elements.ElementsDrawingMod}</li>
 *  <li>@class {anychart.plots.treeMapPlot.elements.TreeMapBranchTitle}</li>
 *  <li>@class {anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle}</li>
 *  <li>@class {anychart.plots.treeMapPlot.elements.TreeMapHiddenBranchTitle}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.elements');
goog.require('anychart.plots.treeMapPlot.elements.styles');
//------------------------------------------------------------------------------
//
//                       ITreeMapBranchTitle interface.
//
//------------------------------------------------------------------------------
/**
 * @interface
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle = function () {
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle.prototype.getBounds = function () {
};
/**
 * @param {anychart.svg.SVGSprite} sprite
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.visual.color.Color} opt_color
 * @param {int} opt_hatchType
 * @return {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle.prototype.createSVGText = function (sprite, bounds, opt_color, opt_hatchType) {
};
/**
 * @param {Object} data
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle.prototype.deserialize = function (data) {
};
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle.prototype.getHeight = function () {
};
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle.prototype.hasHeight = function () {
};
//------------------------------------------------------------------------------
//
//                          ElementsDrawingMod class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.treeMapPlot.elements.ElementsDrawingMod = {
    'DISPLAY_ALL':0,
    'CROP':1,
    'HIDE':2
};
/**
 * @param value
 * @return {anychart.plots.treeMapPlot.elements.ElementsDrawingMod}
 */
anychart.plots.treeMapPlot.elements.ElementsDrawingMod.deserialize = function (value) {
    switch (value) {
        case 'displayall':
            return anychart.plots.treeMapPlot.elements.ElementsDrawingMod.DISPLAY_ALL;
        case 'hide':
            return anychart.plots.treeMapPlot.elements.ElementsDrawingMod.HIDE;
        default:
        case 'crop':
            return anychart.plots.treeMapPlot.elements.ElementsDrawingMod.CROP;
    }
};
//------------------------------------------------------------------------------
//
//                   TreeMapBranchTitle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.text.BaseTitle}
 */
anychart.plots.treeMapPlot.elements.TreeMapBranchTitle = function () {
    anychart.visual.text.BaseTitle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.TreeMapBranchTitle,
    anychart.visual.text.BaseTitle);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.prototype.height_ = null;
/**
 * @return {Number}
 */
anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.prototype.getHeight = function () {
    return this.height_;
};

//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'height'))
        this.height_ = des.getNumProp(data, 'height');
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @param sprite
 * @param bounds
 */
anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.prototype.update = function (sprite, bounds, opt_color, opt_noUpdateText) {
    goog.base(this, 'update', sprite.getSVGManager(), sprite, opt_color, opt_noUpdateText)
    var x;
    switch (this.align_) {
        case anychart.layout.AbstractAlign.NEAR:
            x = bounds.x;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            x = bounds.x + (bounds.width - this.bounds_.width) / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            x = bounds.x + (bounds.width - this.bounds_.width);
            break;
    }

    sprite.setPosition(x, bounds.y);
};
//------------------------------------------------------------------------------
//
//                   TreeMapBaseBranchTitle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.text.BaseTitle}
 */
anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle = function () {
    anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle,
    anychart.plots.treeMapPlot.elements.TreeMapBranchTitle);
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle.prototype.createSVGText = function (svgManager, opt_color, opt_updateText) {
    var sprite = goog.base(this, 'createSVGText', svgManager, opt_color, opt_updateText);
    sprite.initializeClipPath(new anychart.utils.geom.Rectangle());
    return sprite;
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle.prototype.update = function (sprite, bounds, opt_color, opt_noUpdateText) {
    goog.base(this, 'update', sprite, bounds, opt_color, opt_noUpdateText);
    sprite.updateClipPath(new anychart.utils.geom.Rectangle(0, 0, bounds.width, bounds.height));
};
//------------------------------------------------------------------------------
//
//                   TreeMapBaseBranchTitle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.text.BaseTitle}
 */
anychart.plots.treeMapPlot.elements.TreeMapHiddenBranchTitle = function () {
    anychart.plots.treeMapPlot.elements.TreeMapBranchTitle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.TreeMapHiddenBranchTitle,
    anychart.plots.treeMapPlot.elements.TreeMapBranchTitle);
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapHiddenBranchTitle.prototype.update = function (sprite, bounds, opt_color, opt_noUpdateText) {
    goog.base(this, 'update', sprite, bounds, opt_color, opt_noUpdateText);

    if (bounds.width >= this.bounds_.width && bounds.height >= this.bounds_.height) {
        sprite.getTextSegment().setAttribute('visibility', 'visible');
    } else sprite.getTextSegment().setAttribute('visibility', 'hidden');
};
//------------------------------------------------------------------------------
//
//                          TreeMapLabelSprite class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.ElementsSprite}
 */
anychart.plots.treeMapPlot.elements.TreeMapLabelSprite = function(svgManager) {
    anychart.elements.ElementsSprite.call(this, svgManager);
};
goog.inherits(anychart.plots.treeMapPlot.elements.TreeMapLabelSprite,
    anychart.elements.ElementsSprite);
/**
 * Update TreeMap label specific prop such as hide or crop (drawing_mod = 'hide' || drawing_mod = 'crop')
 */
anychart.plots.treeMapPlot.elements.TreeMapLabelSprite.prototype.updateByDrawingMode = function(state) {
    this.styleShapes_.updateByDrawingMode(state);
};
//------------------------------------------------------------------------------
//
//                          TreeMapLabel class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.treeMapPlot.elements.TreeMapLabel = function () {
    anychart.plots.seriesPlot.elements.LabelElement.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.elements.TreeMapLabel,
    anychart.plots.seriesPlot.elements.LabelElement);
//------------------------------------------------------------------------------
//                                  Style.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapLabel.prototype.createStyle = function () {
    return new anychart.plots.treeMapPlot.elements.styles.TreeMapLabelStyle();
};
//------------------------------------------------------------------------------
//                            Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.elements.TreeMapLabel.prototype.resize = function(container, state, sprite) {
    goog.base(this, 'resize', container, state, sprite);
    sprite.updateByDrawingMode(state);
};
//------------------------------------------------------------------------------
//                            Sprite.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {anychart.plots.treeMapPlot.elements.TreeMapLabelSprite}
 */
anychart.plots.treeMapPlot.elements.TreeMapLabel.prototype.createSprite = function(svgManager) {
    return new anychart.plots.treeMapPlot.elements.TreeMapLabelSprite(svgManager);
};


