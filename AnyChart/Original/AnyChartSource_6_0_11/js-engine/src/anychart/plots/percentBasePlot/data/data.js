/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.percentBasePlot.data.PercentBasePoint}</li>.
 *  <li>@class {anychart.plots.percentBasePlot.data.PercentBaseSeries}</li>.
 *  <li>@class {anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.percentBasePlot.data.ConePyramidBasePoint}</li>
 *  <li>@class {anychart.plots.percentBasePlot.data.ConePyramidDIWPoint}</li>
 *  <li>@class {anychart.plots.percentBasePlot.data.ConePyramidDIWSeries}</li>.
 * <ul>
 */
goog.provide('anychart.plots.percentBasePlot.data');
goog.require('anychart.plots.percentBasePlot.labels');
goog.require('anychart.plots.percentBasePlot.drawingInfo');
goog.require('anychart.plots.seriesPlot.data.singleValue');

//------------------------------------------------------------------------------
//
//                          PercentBasePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint = function() {
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.data.PercentBasePoint,
        anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint);
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.bottomWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.topWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.bottomHeight_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.topHeight_ = null;
/**
 * @private
 * @type {anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.drawingInfo_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.percentHeight_ = null;
/**
 * @private
 * @param {Number} padding
 * @param {Number} size3D
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.getRealPercentHeight_ = function(padding, size3D) {
    return (1 - (padding) * (this.series_.getPoints().length - 1));
};
/**
 * @private
 * type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.calculationValue_ = null;
/**
 * Getter for calculating value.
 * @return {Number} Calculating value.
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.getCalculationValue = function() {
    return this.calculationValue_;
};
/**
 * Setter for calculating value.
 * @param {Number} value New calculating value.
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.setCalculationValue = function(value) {
    this.calculationValue_ = value;
};
//------------------------------------------------------------------------------
//                              Initialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
};
//------------------------------------------------------------------------------
//                              Update.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.update = function () {
    goog.base(this, 'update');
    this.drawConnector(this.getSVGManager());
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.resize = function () {
    goog.base(this, 'resize');
    this.resizeConnector();
};
/**
 * Resize point connector.
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.resizeConnector = function() {
    if(!this.hasConnector()) return;
    var svgManager = this.getSVGManager();
    this.connectorElement_.setAttribute('d', this.getConnectorPathData(svgManager));
    if(this.getGlobalSettings().getConnector().isGradient())
        this.setConnectorStyle(svgManager, this.connectorElement_);
};
//------------------------------------------------------------------------------
//                              Connector.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.connectorElement_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.connectorBounds_ = null;
/**
 * Draw point connector
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.drawConnector = function(svgManager) {
    if (!this.hasConnector()) return;
    this.connectorElement_ = svgManager.createPath();
    this.connectorElement_.setAttribute('d', this.getConnectorPathData(svgManager));
    this.setConnectorStyle(svgManager, this.connectorElement_);
    this.sprite_.appendChild(this.connectorElement_);
};
/**
 * @param svgManager
 * @param element
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.setConnectorStyle = function(svgManager, element) {
    element.setAttribute('style', this.getGlobalSettings().getConnector().getSVGStroke(
            svgManager,
            this.connectorBounds_));
};
/**
 * Create point connector path data
 * @param svgManager
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.getConnectorPathData = function(svgManager) {
    var globalSettings = this.getGlobalSettings();
    var placementMod = globalSettings.getLabelsPlacementMode();
    var connector = globalSettings.getConnector();
    var connectorPadding = globalSettings.getConnectorPadding();
    var labelsPlacementEnum = anychart.plots.percentBasePlot.labels.LabelsPlacementMode;
    if (placementMod == labelsPlacementEnum.INSIDE) return '';


    var moveToPosition = new anychart.utils.geom.Point();
    var lineToPosition = new anychart.utils.geom.Point();
    var centerPosition = new anychart.utils.geom.Point();
    var labelRect = new anychart.utils.geom.Rectangle();
    var labelPos = new anychart.utils.geom.Point();
    var pathData;
    var currentState = this.getCurrentStyleState(this.label_.getStyle());
    labelRect = this.label_.getBounds(this, currentState, this.labelSprite_);
    labelPos = this.getLabelPosition(currentState, this.label_, labelRect);

    
    switch (placementMod) {
        case labelsPlacementEnum.OUTSIDE_LEFT:
        case labelsPlacementEnum.OUTSIDE_LEFT_IN_COLUMN:
            moveToPosition.x = (this.drawingInfo_.getLeftTop().x + this.drawingInfo_.getLeftBottom().x) / 2;
            moveToPosition.y = (this.drawingInfo_.getLeftTop().y + this.drawingInfo_.getLeftBottom().y) / 2;
            lineToPosition.x = labelPos.x + labelRect.width;
            lineToPosition.y = labelPos.y + labelRect.height / 2;
            centerPosition.y = lineToPosition.y;
            centerPosition.x = lineToPosition.x + connectorPadding;
            break;

        case labelsPlacementEnum.OUTSIDE_RIGHT:
        case labelsPlacementEnum.OUTSIDE_RIGHT_IN_COLUMN:
            moveToPosition.x = (this.drawingInfo_.getRightTop().x + this.drawingInfo_.getRightBottom().x) / 2;
            moveToPosition.y = (this.drawingInfo_.getRightTop().y + this.drawingInfo_.getRightBottom().y) / 2;
            lineToPosition.x = labelPos.x;
            lineToPosition.y = labelPos.y + labelRect.height / 2;
            centerPosition.y = lineToPosition.y;
            centerPosition.x = lineToPosition.x - connectorPadding;
            break;
    }

    if (connector.isGradient()) {
        this.connectorBounds_ = new anychart.utils.geom.Rectangle(
                Math.min(moveToPosition.x, lineToPosition.x),
                Math.min(moveToPosition.y, lineToPosition.y),
                Math.max(moveToPosition.x, lineToPosition.x) - Math.min(moveToPosition.x, lineToPosition.x),
                Math.max(moveToPosition.y, lineToPosition.y) - Math.min(moveToPosition.y, lineToPosition.y));
    }

    pathData = svgManager.pMove(moveToPosition.x,moveToPosition.y);
    pathData += svgManager.pLine(centerPosition.x,centerPosition.y);
    pathData += svgManager.pLine(lineToPosition.x,lineToPosition.y);

    return pathData;
};
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.hasConnector = function() {
    return this.labelSprite_ && this.getGlobalSettings().hasConnector();
};
//------------------------------------------------------------------------------
//                          Point bounds.
//------------------------------------------------------------------------------
/**
 * Set point bounds
 * @protected
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.setBounds = function() {
    var h = this.series_.getHeight();
    this.bounds_.x = Math.min(this.drawingInfo_.getLeftTop().x, this.drawingInfo_.getLeftBottom().x);
    this.bounds_.y = Math.min(this.drawingInfo_.getLeftTop().y, this.drawingInfo_.getLeftBottom().y);
    this.bounds_.width = Math.max(
            this.drawingInfo_.getRightTop().x - this.drawingInfo_.getLeftTop().x,
            this.drawingInfo_.getRightBottom().x - this.drawingInfo_.getLeftBottom().x);
    this.bounds_.height = this.percentHeight_ * h;
};
//------------------------------------------------------------------------------
//                  Sector's values.
//------------------------------------------------------------------------------
/**
 * Set the x coordinate of the sector.
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.setSectorXValue = function() {
    this.bottomWidth_ = this.bottomHeight_;
    this.topWidth_ = this.topHeight_;
};
/**
 * @param {Boolean} dataIsWidth
 * @param {Number} minValue
 * @param {Number} padding
 * @param {Number} realSummHeight
 * @param {Number} size3D
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.setSectorYValue = function(dataIsWidth, minValue, padding, realSummHeight, size3D) {
    if (!dataIsWidth) {
        if (this.getCalculationValue() < minValue) this.percentHeight_ =
                minValue / realSummHeight;
        else this.percentHeight_ = this.getCalculationValue() / realSummHeight;
    } else this.percentHeight_ = 1 /
            (this.series_.getPoints().length - 1);

    this.percentHeight_ *= this.getRealPercentHeight_(padding, size3D);

    this.bottomHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() -
            this.percentHeight_);

    this.topHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() - padding);
};
//------------------------------------------------------------------------------
//                          Labels position.
//------------------------------------------------------------------------------
/**
 * Gets the label position from sector.
 * @param {anychart.elements.styles.BaseElementStyleState} state
 * @param {anychart.elements.BaseElement} element
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Point} Point.
 */
anychart.plots.percentBasePlot.data.PercentBasePoint.prototype.getLabelPosition = function(state, element, bounds) {
    var placement = this.globalSettings_.getLabelsPlacementMode();
    var pos = new anychart.utils.geom.Point();

    if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.INSIDE) {
        return this.getElementPosition(element, state, bounds);//todo: wtd?
    }

    if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_LEFT) {
        pos.x = (this.drawingInfo_.getLeftTop().x +
                this.drawingInfo_.getLeftBottom().x) / 2;
        pos.y = (this.drawingInfo_.getLeftTop().y +
                this.drawingInfo_.getLeftBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(
                pos,
                anychart.layout.HorizontalAlign.LEFT,
                bounds,
                state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_RIGHT) {
        pos.x = (this.drawingInfo_.getRightTop().x +
                this.drawingInfo_.getRightBottom().x) / 2;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.HorizontalAlign.RIGHT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN &&
            this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x +
                this.series_.getWidth() -
                this.label_.getBounds(this,
                        this.label_.getStyle().getNormal(),
                        element).width;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
//            anychart.layout.VerticalAlign.apply(pos, anychart.visual.layout.HorizontalAlign.RIGHT,
//                            bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN &&
            this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
//            anychart.layout.VerticalAlign.apply(pos, anychart.visual.layout.HorizontalAlign.LEFT,
//                            bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    }

    return pos;
};
//------------------------------------------------------------------------------
//
//                          PercentBaseSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries = function() {
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.data.PercentBaseSeries,
        anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries);
//------------------------------------------------------------------------------
//                                 Properties
//------------------------------------------------------------------------------
/**
 * Series bound.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.seriesBounds_ = null;
/**
 * Gets the series bounds.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.getSeriesBounds = function() {
    return this.seriesBounds_;
};
/**
 * @param {anychart.utils.geom.Rectangle} value
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.setSeriesBounds = function(value) {
    this.seriesBounds_ = value;
};
/**
 * Current value of top or bottom bound of sector.
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.oldClientY_ = 1;
/**
 * Getter for currentY.
 * @return {Number} Current Y value.
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.getCurrentY = function() {
    return this.oldClientY_;
};
/**
 * Setter for currentY.
 * @param {Number} currentY The value of the current Y value.
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.setCurrentY = function(currentY) {
    this.oldClientY_ = currentY;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.realSummHeight_ = null;
/**
 * Count of visible points.
 * @private
 * @type {int}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.visiblePointsCount_ = null;
/**
 * @return {int}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.getVisiblePoints = function() {
    return this.visiblePointsCount_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.shiftNumber_ = 0;
/**
 * Returns the height of plot.
 * @return {Number} Height of plot.
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.getHeight = function() {
    return this.seriesBounds_.height;
};
/**
 * Returns the width of plot.
 * @return {Number} Height of plot.
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.getWidth = function() {
    return this.seriesBounds_.width;
};
//------------------------------------------------------------------------------
//                                   Methods
//------------------------------------------------------------------------------
/**
 * Set the real sum of values considering the values that less then min and the
 * ignoring last point in dataIsWidth.
 * @private
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.setRealSummHeight_ = function() {
    var point;
    var dataIsWidth = this.globalSettings_.isDataIsWidth();
    var minValue = this.globalSettings_.getMinPointSize() *
            this.tokensHash_.get('%SeriesYSum');
    this.realSummHeight_ = 0;
    this.visiblePointsCount_ = 0;
    for (var i = 0; i < this.points_.length; i++) {
        point = this.points_[i];
        this.visiblePointsCount_++;
        if ((i == this.points_.length - 1) && dataIsWidth)
            this.visiblePointsCount_--;
        else {
            if (point.getCalculationValue() < minValue) this.realSummHeight_ += minValue;
            else this.realSummHeight_ += point.getCalculationValue();
        }
    }
};
/**
 * @protected
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.setLabelsPosition = function() {
    var labelPos = new anychart.utils.geom.Point();
    var labelRect = new anychart.utils.geom.Rectangle();
    var w = this.getWidth();
    var h = this.getHeight();
    var tangens = w / (h * 2);
    var labelH;
    var length = this.points_.length;
    var point; //PercentBasePoint
    var pos;
    var labelPadding;
    for (var i = 0; i < length; i++) {
        point = this.points_[i];
        if ((point.label_.isEnabled()) && !(point.isMissing())) {
            pos = (point.drawingInfo_.getRightBottom().x +
                    point.drawingInfo_.getRightTop.x) / 2;
            labelRect = point.getLabel().getBounds(point,
                    point.getLabel().getStyle().getNormal(),
                    this.getLabel().getSprite());
            labelPos = point.getLabelPosition(
                    point.getLabel().getStyle().getNormal(),
                    point.getLabel(),
                    labelRect); //TODO: breakpoint;
            labelPadding = point.getLabel().getStyle().
                    getNormal().getPadding();
            labelH = (point.bottomHeight_ + point.topHeight_) *
                    h / 2;
            if ((labelPos.x - labelRect.width < this.seriesBounds_.x) ||
                    (labelPos.x + labelRect.width > this.getWidth()) ||
                    (pos + labelRect.width > this.getWidth())) {
                tangens = Math.min(tangens,
                        (w - labelRect.width - labelPadding) / (h + labelH));
            }
        }
    }
    this.shiftNumber_ = 2 * h * tangens;
};
/**
 * @protected
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.shiftPlotPoints = function() {
    var labelPlacement = this.globalSettings_.getLabelsPlacementMode();
    var inverted = this.globalSettings_.isInverted();
    var point;
    for (var i = 0; i < this.points_.length; i++) {
        point = this.points_[i];
        switch (labelPlacement) {
            case anychart.plots.percentBasePlot.labels.LabelsPlacementMode.
                    OUTSIDE_LEFT:
                point.drawingInfo_.setSectorPoint(point, inverted,
                        this.shiftNumber_);
                point.drawingInfo_.updateSectorPoint(
                        (this.shiftNumber_ - this.getWidth()) / 2);
                break;
            case anychart.plots.percentBasePlot.labels.LabelsPlacementMode.
                    OUTSIDE_RIGHT:
                point.drawingInfo_.setSectorPoint(point, inverted,
                        this.shiftNumber_);
                point.drawingInfo_.updateSectorPoint(
                        (this.getWidth() - this.shiftNumber_) / 2);
                break;
            case anychart.plots.percentBasePlot.labels.LabelsPlacementMode.
                    OUTSIDE_LEFT_IN_COLUMN:
                point.drawingInfo_.setSectorPoint(point, inverted,
                        this.shiftNumber_);
                point.drawingInfo_.updateSectorPoint(
                        (this.shiftNumber_ - this.getWidth()) / 2);
                break;
            case anychart.plots.percentBasePlot.labels.LabelsPlacementMode.
                    OUTSIDE_RIGHT_IN_COLUMN:
                point.drawingInfo_.setSectorPoint(point, inverted,
                        this.shiftNumber_);
                point.drawingInfo_.updateSectorPoint(
                        (this.getWidth() - this.shiftNumber_) / 2);
                break;
        }
    }
};
//------------------------------------------------------------------------------
//                              onBefore.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.needCallOnBeforeDraw = function() {
    return true;
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.onBeforeDraw = function() {
    this.setLabelsPosition();
    this.shiftPlotPoints();
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBaseSeries.prototype.onBeforeResize = function() {
    this.setLabelsPosition();
    this.shiftPlotPoints();
};
//------------------------------------------------------------------------------
//
//                    PercentBaseGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings = function() {
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings.call(this);
    this.size3D_ = 0.09;
    this.padding_ = 0.02;
    this.minPointSize_ = 0.01;
    this.labelsPlacementMode_ =
            anychart.plots.percentBasePlot.labels.
                    LabelsPlacementMode.OUTSIDE_LEFT;
    this.inverted_ = false;
    this.dataIsWidth_ = false;
    this.connectorPadding_ = 5;
    this.feet_ = 0.6;
};
goog.inherits(anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings,
        anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.size3D_ = null;
/**
 * @return {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getSize3D = function() {
    return this.size3D_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getPadding = function() {
    return this.padding_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.minPointSize_ = null;
/**
 * @return {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getMinPointSize = function() {
    return this.minPointSize_;
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.connector_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.hasConnector = function() {
    return this.connector_ && this.connector_.isEnabled();
};
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getConnector = function() {
    return this.connector_;
};
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.connector_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.connectorPadding_ = null;
/**
 * @return {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getConnectorPadding = function() {
    return this.connectorPadding_;
};
/**
 * @private
 * @type {int}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.labelsPlacementMode_ = null;
/**
 * @return {int}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getLabelsPlacementMode = function() {
    return this.labelsPlacementMode_
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.inverted_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.isInverted = function() {
    return this.inverted_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.dataIsWidth_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.isDataIsWidth = function () {
    return this.dataIsWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.feet_ = null;
/**
 * @return {Number}
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.getFeet = function () {
    return this.feet_;
};
/**
 * @see {anychart.plots.seriesPlot.data.GlobalSeriesSettings}
 * missing точки добавлять в массив не нужно для того что бы в режиме dataIsWidht не пробегать
 * массив до следующей не missing точки а просто брать значение из следующей
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.canDrawMissingPoints = function() {
    return false
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.deserialize = function(data, stylesList) {/////////////////////////////////////////////////////////////////////////////////////
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'depth_3d'))
        this.size3D_ = des.getRatioProp(data, 'depth_3d');
    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getRatioProp(data, 'padding');
    if (des.hasProp(data, 'min_point_size'))
        this.minPointSize_ = des.getRatioProp(data, 'min_point_size');
    if (des.hasProp(data, 'fit_aspect'))
        this.feet_ = des.getNumProp(data, 'fit_aspect');
    if (des.hasProp(data, 'inverted'))
        this.inverted_ = des.getBoolProp(data, 'inverted');
    if (des.hasProp(data, 'data_is_width'))
        this.dataIsWidth_ = des.getBoolProp(data, 'data_is_width');

    if (des.hasProp(data, 'label_settings')) {
        var settings = des.getProp(data, 'label_settings');
        if (des.hasProp(settings, 'placement_mode'))
            this.labelsPlacementMode_ = anychart.plots.percentBasePlot.labels.LabelsPlacementMode.deserialize(
                    des.getEnumProp(
                            settings,
                            'placement_mode'));
    }

    if (des.isEnabledProp(data, 'connector')) {
        var connectorSettings = des.getProp(data, 'connector');
        this.connector_ = new anychart.visual.stroke.Stroke();
        this.connector_.deserialize(connectorSettings);

        if (des.hasProp(connectorSettings, 'padding'))
            this.connectorPadding_ = des.getNumProp(connectorSettings, 'padding');
    }
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.PercentBaseGlobalSeriesSettings.prototype.createLabelElement = function(opt_isExtra) {
    return new anychart.plots.percentBasePlot.labels.CustomLabelElement();
};
//------------------------------------------------------------------------------
//
//                        ConePyramidBasePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.data.PercentBasePoint}
 */
anychart.plots.percentBasePlot.data.ConePyramidBasePoint = function() {
    this.drawingInfo_ = new anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo();
    anychart.plots.percentBasePlot.data.PercentBasePoint.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.data.ConePyramidBasePoint,
        anychart.plots.percentBasePlot.data.PercentBasePoint);
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidBasePoint.prototype.setBounds = function() {
    var h = this.series_.getHeight();
    this.bounds_.x = Math.min(this.drawingInfo_.getLeftTop().x, this.drawingInfo_.getLeftBottom().x);
    this.bounds_.y = Math.min(this.drawingInfo_.getLeftBottom().y, this.drawingInfo_.getLeftBottom().y);
    this.bounds_.width = Math.max(
            this.drawingInfo_.getRightTop().x - this.drawingInfo_.getLeftTop().x,
            this.drawingInfo_.getRightBottom().x - this.drawingInfo_.getLeftBottom().x);
    this.bounds_.height = this.percentHeight_ * h;
};
//------------------------------------------------------------------------------
//                      Sector's value.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidBasePoint.prototype.setSectorXValue = function() {
    this.bottomWidth_ = this.bottomHeight_;
    this.topWidth_ = this.topHeight_;
};
//------------------------------------------------------------------------------
//                          Labels.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidBasePoint.prototype.getLabelPosition = function(state, element, bounds) {
    var placement = this.globalSettings_.getLabelsPlacementMode();
    var pos = new anychart.utils.geom.Point();

    if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.INSIDE) {
        return this.getElementPosition(element, state, bounds);//todo: wtd?
    }

    if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_LEFT) {
        pos.x = (this.drawingInfo_.getLeftTop().x +
                this.drawingInfo_.getLeftBottom().x) / 2;
        pos.y = (this.drawingInfo_.getLeftTop().y +
                this.drawingInfo_.getLeftBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.HorizontalAlign.LEFT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_RIGHT) {
        pos.x = (this.drawingInfo_.getRightTop().x +
                this.drawingInfo_.getRightBottom().x) / 2;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.HorizontalAlign.RIGHT,
                bounds, state.getPadding());
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN &&
            this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x +
                this.series_.getWidth() -
                this.label_.getBounds(this,
                        this.label_.getStyle().getNormal(),
                        element).width;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    } else if (placement == anychart.plots.percentBasePlot.labels.
            LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN &&
            this.label_ != null) {
        pos.x = this.series_.getSeriesBounds().x;
        pos.y = (this.drawingInfo_.getRightTop().y +
                this.drawingInfo_.getRightBottom().y) / 2;
        anychart.layout.VerticalAlign.apply(pos, anychart.layout.VerticalAlign.CENTER,
                bounds, state.getPadding());
    }

    return pos;
};

/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidBasePoint.prototype.setAnchorPoint = function(pos, anchor, bounds, padding) {
    this.drawingInfo_.setAnchorPoint(anchor, pos, this.globalSettings_.isInverted());
};
//------------------------------------------------------------------------------
//
//                    ConePyramidDIWPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.data.PercentBasePoint}
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWPoint = function() {
    anychart.plots.percentBasePlot.data.PercentBasePoint.call(this);
    this.drawingInfo_ = new anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo();
};
goog.inherits(anychart.plots.percentBasePlot.data.ConePyramidDIWPoint,
        anychart.plots.percentBasePlot.data.PercentBasePoint);
//------------------------------------------------------------------------------
//                                Overrides
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWPoint.prototype.setSectorYValue = function(dataIsWidth, minValue, padding, realSummHeight, size3D) {
    var firstPoint = this.series_.getPointAt(0);
    var inverted = this.globalSettings_.isInverted();
    var previousPoint = this.series_.getPointAt(this.index_ - 1);

    this.percentHeight_ = 1 / this.series_.getPoints().length;
    this.percentHeight_ *= this.getRealPercentHeight_(padding, size3D);
    this.bottomHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() -
            this.percentHeight_);
    this.topHeight_ = this.series_.getCurrentY();
    this.series_.setCurrentY(this.series_.getCurrentY() -
            this.padding_);
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWPoint.prototype.getRealPercentHeight_ = function(padding, size3D) {
    return (1 - padding * (this.series_.getPoints().length - 1));
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWPoint.prototype.setSectorXValue = function() {
    var index = this.index_;
    var nextPoint = this.series_.getPointAt(index + 1);
    this.bottomWidth_ = this.getCalculationValue() /
            this.series_.tokensHash_.get('%SeriesYMax');
    if (nextPoint != null) {
        this.topWidth_ = nextPoint.getCalculationValue() /
                this.series_.tokensHash_.get('%SeriesYMax');
    } else this.topWidth_ = 0;
};
//------------------------------------------------------------------------------
//
//                    ConePyramidDIWSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.data.PercentBaseSeries}
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWSeries = function() {
    anychart.plots.percentBasePlot.data.PercentBaseSeries.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.data.ConePyramidDIWSeries,
        anychart.plots.percentBasePlot.data.PercentBaseSeries);
//------------------------------------------------------------------------------
//                                Overrides
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWSeries.prototype.onBeforeDraw = function() {
    this.oldClientY_ = 1;
    var padding = this.globalSettings_.getPadding();
    var dataIsWidth = this.globalSettings_.isDataIsWidth();
    var inverted = this.globalSettings_.isInverted();
    var size3D = this.globalSettings_.getSize3D();
    var minValue = this.globalSettings_.getMinPointSize() *
            this.tokensHash_.get('%SeriesYSum');
    for (var i = 0; i < this.points_.length; i++) {
        var point = this.points_[i];
        point.setSectorYValue(dataIsWidth, minValue, padding,
                this.realSummHeight_, size3D);
        point.setSectorXValue();
        point.drawingInfo_.setSectorPoint(point, inverted, this.getWidth());
    }
};
/**
 * @inheritDoc
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.percentBasePlot.data.ConePyramidDIWSeries.prototype.createPoint = function() {
    return new anychart.plots.percentBasePlot.data.ConePyramidDIWPoint();
};