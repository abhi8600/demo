/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.percentBasePlot.labels.LabelsPlacementMode},</li>.
 *  <li>@class {anychart.plots.percentBasePlot.labels.CustomLabelElement}.</li>.
 * <ul>
 */
//------------------------------------------------------------------------------
//
//                          LabelsPlacementMode class.
//
//------------------------------------------------------------------------------
goog.provide('anychart.plots.percentBasePlot.labels');
goog.require('anychart.plots.seriesPlot.elements');
/**
 * @enum {int}
 */
anychart.plots.percentBasePlot.labels.LabelsPlacementMode = {
    OUTSIDE_LEFT_IN_COLUMN: 0,
    OUTSIDE_LEFT: 1,
    OUTSIDE_RIGHT_IN_COLUMN: 2,
    OUTSIDE_RIGHT: 3,
    INSIDE: 4
};
anychart.plots.percentBasePlot.labels.LabelsPlacementMode.deserialize = function(value) {
    switch (value) {
        default:
        case 'outsideleft' : return anychart.plots.percentBasePlot.labels.LabelsPlacementMode.OUTSIDE_LEFT;
        case 'outsideleftincolumn' : return anychart.plots.percentBasePlot.labels.LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN;
        case 'outsiderightincolumn': return anychart.plots.percentBasePlot.labels.LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN;
        case 'outsideright' : return anychart.plots.percentBasePlot.labels.LabelsPlacementMode.OUTSIDE_RIGHT;
        case 'inside' : return anychart.plots.percentBasePlot.labels.LabelsPlacementMode.INSIDE;
    }
};
//------------------------------------------------------------------------------
//
//                          CustomLabelElement class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.percentBasePlot.labels.CustomLabelElement = function() {
    anychart.plots.seriesPlot.elements.LabelElement.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.labels.CustomLabelElement,
        anychart.plots.seriesPlot.elements.LabelElement);
//------------------------------------------------------------------------------
//                          Position.
//------------------------------------------------------------------------------
/**
 * @param {anychart.elements.IElementContainer} container
 * @param {anychart.styles.StyleState} state
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.plots.percentBasePlot.labels.CustomLabelElement.prototype.getPosition = function(container, state, sprite) {
    return container.getLabelPosition(state, this, this.getBounds(container, state, sprite));
};

