/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.viewPorts.markers');
//------------------------------------------------------------------------------
//
//                     IMarkersViewPort interface.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort = function(viewPort) {
    this.viewPort_ = viewPort;
};
/**
 * @private
 * @type {anychart.plots.axesPlot.axes.viewPorts.AxisViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.viewPort_ = null;
/**
 * @param {Number} value
 * @param {anychart.utils.geom.Rectangle} point
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.setStartValue = function(value, point) {
    goog.abstractMethod();
};
/**
 * @param {Number} value
 * @param {anychart.utils.geom.Rectangle} point
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.setEndValue = function(value, point) {
    goog.abstractMethod();
};
/**
 *
 * @param {Number} pixPos
 * @param {Number} padding
 * @param {Number} offset
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.utils.geom.Point} pos
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    goog.abstractMethod();
};
/**
 * @param {Number} startPixPos
 * @param {Number} endPixPos
 * @param {int} align
 * @param {Number} padding
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.utils.geom.Point} pos
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.setInsideMarkerLabelPosition = function(startPixPos, endPixPos, align, padding, bounds, pos) {
    goog.abstractMethod();
};
/**
 * @param {Number} startMinValue
 * @param {Number} endMinValue
 * @param {Number} startMaxValue
 * @param {Number} endMaxValue
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.getRangeMarkerBounds = function(startMinValue, endMinValue, startMaxValue, endMaxValue) {
    var res = new anychart.utils.geom.Rectangle();

    var startMinPt = new anychart.utils.geom.Point();
    var endMinPt = new anychart.utils.geom.Point();
    var startMaxPt = new anychart.utils.geom.Point();
    var endMaxPt = new anychart.utils.geom.Point();

    this.setStartValue(startMinValue, startMinPt);
    this.setStartValue(startMaxValue, startMaxPt);
    this.setEndValue(endMinValue, endMinPt);
    this.setEndValue(endMaxValue, endMaxPt);

    res.x = Math.min(startMinPt.x, startMaxPt.x, endMinPt.x, endMaxPt.x);
    res.y = Math.min(startMinPt.y, startMaxPt.y, endMinPt.y, endMaxPt.y);
    res.width = Math.max(startMinPt.x, startMaxPt.x, endMinPt.x, endMaxPt.x) - res.x;
    res.height = Math.max(startMinPt.y, startMaxPt.y, endMinPt.y, endMaxPt.y) - res.y;

    return res;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} startValue
 * @param {Number} endValue
 * @param {Boolean} opt_moveTo
 * @param {Boolean} opt_invertedDrawing
 * @return {String}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.prototype.getMarkerLinePathData = function(svgManager, startValue, endValue, opt_moveTo, opt_invertedDrawing) {
    if (opt_moveTo == null || opt_moveTo == undefined) opt_moveTo = true;
    if (opt_invertedDrawing == null || opt_invertedDrawing == undefined) opt_invertedDrawing = false;

    var startPt = new anychart.utils.geom.Point();
    var endPt = new anychart.utils.geom.Point();
    var pathData;

    this.setStartValue(startValue, startPt);
    this.setEndValue(endValue, endPt);

    if (opt_invertedDrawing) {
        var tmpP = startPt;
        startPt = endPt;
        endPt = tmpP;
        tmpP = null;
    }

    if (opt_moveTo) pathData = svgManager.pMove(startPt.x, startPt.y);
    else pathData = svgManager.pLine(startPt.x, startPt.y);

    pathData += svgManager.pLine(endPt.x, endPt.y);

    return pathData;
};
//------------------------------------------------------------------------------
//
//                      HorizontalMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort.prototype.setStartValue = function(value, point) {
    point.x = value + this.viewPort_.getBounds().x;
    point.y = this.viewPort_.getBounds().y;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort.prototype.setEndValue = function(value, point) {
    point.x = value + this.viewPort_.getBounds().x;
    point.y = this.viewPort_.getBounds().getBottom() ;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    pos.x = this.viewPort_.getBounds().x + pixPos - bounds.width / 2;
    pos.y = 0;
};
//------------------------------------------------------------------------------
//
//                      VerticalMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.MarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort.prototype.setStartValue = function(value, point) {
    point.x = this.viewPort_.getBounds().x;
    point.y = this.viewPort_.getAxis().getScale().getPixelRangeMaximum() - value + this.viewPort_.getBounds().y;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort.prototype.setEndValue = function(value, point) {
    point.x = this.viewPort_.getBounds().getRight();
    point.y = this.viewPort_.getAxis().getScale().getPixelRangeMaximum() - value + this.viewPort_.getBounds().y;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    pos.y = this.viewPort_.getBounds().getBottom() - pixPos - bounds.height / 2;
    pos.x = 0;
};
//------------------------------------------------------------------------------
//
//                      LeftMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    goog.base(this, 'setOutsideMarkerLabelPosition', pixPos, padding, offset, bounds, pos);
    pos.x += this.viewPort_.getBounds().getLeft() - offset - bounds.width - padding;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.LeftMarkersViewPort.prototype.setInsideMarkerLabelPosition = function(startPixPos, endPixPos, align, padding, bounds, pos) {
    var viewPortBounds = this.viewPort_.getBounds();
    var near = new anychart.utils.geom.Point(viewPortBounds.x, viewPortBounds.getBottom() - startPixPos);
    var far = new anychart.utils.geom.Point(viewPortBounds.getRight(), viewPortBounds.getBottom() - endPixPos);

    var angle = Math.atan2(far.y - near.y, far.x - near.x);
    var xOffset = padding * Math.cos(angle);
    var yOffset = padding * Math.sin(angle);
    switch (align) {
        case anychart.layout.AbstractAlign.NEAR:
            pos.x = near.x + xOffset;
            pos.y = near.y + yOffset - bounds.height / 2;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            pos.y = (near.y + far.y) / 2 - bounds.height / 2;
            pos.x = (near.x + far.x) / 2 - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            pos.x = far.x - xOffset - bounds.width;
            pos.y = far.y - bounds.height / 2 - yOffset;
            break;
    }
};
//------------------------------------------------------------------------------
//
//                      RightMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.VerticalMarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    goog.base(this, 'setOutsideMarkerLabelPosition', pixPos, padding, offset, bounds, pos);
    pos.x += this.viewPort_.getBounds().getRight() + offset + padding;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.prototype.setInsideMarkerLabelPosition = function(startPixPos, endPixPos, align, padding, bounds, pos) {
    var near = new anychart.utils.geom.Point(this.viewPort_.getBounds().getRight(), this.viewPort_.getBounds().getBottom() - startPixPos);
    var far = new anychart.utils.geom.Point(this.viewPort_.getBounds().x, this.viewPort_.getBounds().getBottom() - endPixPos);

    var angle = Math.atan2(far.y - near.y, far.x - near.x);
    var xOffset = padding * Math.cos(angle);
    var yOffset = padding * Math.sin(angle);
    switch (align) {
        case anychart.layout.AbstractAlign.NEAR:
            pos.x = near.x - xOffset - bounds.width;
            pos.y = near.y + yOffset - bounds.height / 2;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            pos.y = (near.y + far.y) / 2 - bounds.height / 2;
            pos.x = (near.x + far.x) / 2 - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            pos.x = far.x + xOffset;
            pos.y = far.y - bounds.height / 2 - yOffset;
            break;
    }
    pos.x += this.viewPort_.getBounds().x;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.prototype.setStartValue = function(value, point) {
    anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.superClass_.setEndValue.call(this, value, point);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.prototype.setEndValue = function(value, point) {
    anychart.plots.axesPlot.axes.viewPorts.markers.RightMarkersViewPort.superClass_.setStartValue.call(this, value, point);
};
//------------------------------------------------------------------------------
//
//                      TopMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    goog.base(this, 'setOutsideMarkerLabelPosition', pixPos, padding, offset, bounds, pos);
    pos.y += this.viewPort_.getBounds().getTop() - offset - bounds.height - padding;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.TopMarkersViewPort.prototype.setInsideMarkerLabelPosition = function(startPixPos, endPixPos, align, padding, bounds, pos) {
    var near = new anychart.utils.geom.Point(startPixPos, this.viewPort_.getBounds().y);
    var far = new anychart.utils.geom.Point(endPixPos, this.viewPort_.getBounds().getBottom());
    var angle = Math.atan2(far.y - near.y, far.x - near.x);
    var xOffset = padding * Math.cos(angle);
    var yOffset = padding * Math.sin(angle);
    switch (align) {
        case anychart.layout.AbstractAlign.NEAR:
            pos.y = near.y + yOffset;
            pos.x = startPixPos + xOffset - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            pos.y = (near.y + far.y) / 2 - bounds.height / 2;
            pos.x = (near.x + far.x) / 2 - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            pos.y = far.y - bounds.height - yOffset;
            pos.x = far.x - bounds.width / 2 - xOffset;
            break;
    }
};
//------------------------------------------------------------------------------
//
//                      BottomMarkersViewPort class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort}
 */
anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort.call(this, viewPort);
};
goog.inherits(anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort,
        anychart.plots.axesPlot.axes.viewPorts.markers.HorizontalMarkersViewPort);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.prototype.setOutsideMarkerLabelPosition = function(pixPos, padding, offset, bounds, pos) {
    goog.base(this, 'setOutsideMarkerLabelPosition', pixPos, padding, offset, bounds, pos);
    pos.y += this.viewPort_.getBounds().getBottom() + offset + padding;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.prototype.setInsideMarkerLabelPosition = function(startPixPos, endPixPos, align, padding, bounds, pos) {
    var near = new anychart.utils.geom.Point(startPixPos, this.viewPort_.getBounds().getBottom());
    var far = new anychart.utils.geom.Point(endPixPos, this.viewPort_.getBounds().y);
    
    var angle = Math.atan2(far.y - near.y, far.x - near.x);
    var xOffset = padding * Math.cos(angle);
    var yOffset = padding * Math.sin(angle);

    switch (align) {
        case anychart.layout.AbstractAlign.NEAR:
            pos.y = near.y - bounds.height + yOffset;
            pos.x = startPixPos + xOffset - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            pos.y = (near.y + far.y) / 2 - bounds.height / 2;
            pos.x = (near.x + far.x) / 2 - bounds.width / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            pos.y = far.y - yOffset;
            pos.x = far.x - bounds.width / 2 - xOffset;
            break;
    }
    pos.x += this.viewPort_.getBounds().x;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.prototype.setStartValue = function(value, point) {
    anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.superClass_.setEndValue.call(this, value, point);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.prototype.setEndValue = function(value, point) {
    anychart.plots.axesPlot.axes.viewPorts.markers.BottomMarkersViewPort.superClass_.setStartValue.call(this, value, point);
};