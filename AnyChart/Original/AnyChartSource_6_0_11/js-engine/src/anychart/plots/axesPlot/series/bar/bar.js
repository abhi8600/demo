/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.BarSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.BarPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.RangeBarPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.RangeBarSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.bar');
goog.require('anychart.plots.axesPlot.series.bar.styles');
goog.require('anychart.plots.axesPlot.data');
goog.require('anychart.utils');
//------------------------------------------------------------------------------
//
//                          BarPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangeStackablePoint}
 */
anychart.plots.axesPlot.series.bar.BarPoint = function () {
    anychart.plots.axesPlot.data.RangeStackablePoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.BarPoint,
    anychart.plots.axesPlot.data.RangeStackablePoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Point shape type
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.series.bar.BarPoint.prototype.shapeType_ = anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX;
/**
 * @return {int}
 */
anychart.plots.axesPlot.series.bar.BarPoint.prototype.getShapeType = function () {
    return this.shapeType_;
};
/**
 * @param {int} value
 */
anychart.plots.axesPlot.series.bar.BarPoint.prototype.setShapeType = function (value) {
    this.shapeType_ = value;
};
//------------------------------------------------------------------------------
//
//                          BarSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.SingleValueSeries}
 */
anychart.plots.axesPlot.series.bar.BarSeries = function () {
    anychart.plots.axesPlot.data.SingleValueSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.BarSeries,
    anychart.plots.axesPlot.data.SingleValueSeries);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.bar.BarSeries.prototype.valueAxisStart_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.bar.BarSeries.prototype.getValueAxisStart = function () {
    return  this.valueAxisStart_;
};
/**
 * Series bar shape type
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.series.bar.BarSeries.prototype.shapeType_ = anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX;
/**
 * @param {int} value
 */
anychart.plots.axesPlot.series.bar.BarSeries.prototype.setShapeType = function (value) {
    this.shapeType_ = value;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarSeries.prototype.needCallOnBeforeDraw = function () {
    return true;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.BarSeries.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    this.globalSettings_.deserializeShapeType(this, config);
};
//------------------------------------------------------------------------------
//                          Before draw.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.BarSeries.prototype.setValueAxisStart_ = function () {
    if (this.valueAxis_.getScale().getMinimum() > 0)
        this.valueAxisStart_ = this.valueAxis_.getScale().getMinimum();
    else
        this.valueAxisStart_ = 0;
};

anychart.plots.axesPlot.series.bar.BarSeries.prototype.onBeforeDraw = function () {
    this.setValueAxisStart_();
};
//------------------------------------------------------------------------------
//                          Point.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.BarSeries.prototype.createPoint = function () {
    var point = new anychart.plots.axesPlot.series.bar.BarPoint();
    point.setShapeType(this.shapeType_);
    return point;
};
//------------------------------------------------------------------------------
//
//                          BarGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings = function () {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings,
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Global bar shape type.
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.shapeType_ = anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX;
/**
 * @param {int} value
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.setShapeType = function (value) {
    this.shapeType_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.isFixedBarWidth_ = false;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.fixedBarWidth_ = NaN;
//-----------------------------------------------------------------------------
//                          Deserialize
//-----------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);
    if (anychart.utils.deserialization.hasProp(config, 'point_width')) {
        this.isFixedBarWidth_ = true;
        this.fixedBarWidth_ = anychart.utils.deserialization.getNumProp(config, 'point_width');
    }
    this.deserializeShapeType(this, config);
};
/**
 * Deserialize shape type for target.
 * @param {anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings|anychart.plots.axesPlot.series.bar.BarSeries} target
 * @param {Object} config
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.deserializeShapeType = function (target, config) {
    if (anychart.utils.deserialization.hasProp(config, 'shape_type'))
        target.setShapeType(
            anychart.plots.axesPlot.series.bar.styles.BarShapeType.deserialize(
                anychart.utils.deserialization.getLStringProp(
                    config,
                    'shape_type')));
};
/**
 * @param {anychart.plots.axesPlot.series.bar.BarPoint} point
 * @return {Number}
 */
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.getBarWidth = function (point) {
    if (this.plot_.isScatter())
        return (this.isPercentPointScatterWidth() ? this.plot_.getBounds().width : 1) * this.getPointScatterWidth();
    return this.isFixedBarWidth_ ? this.fixedBarWidth_ : point.getClusterSettings().getClusterWidth();
};
//------------------------------------------------------------------------------
//                          IGlobalSettings
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.createSeries = function (seriesType) {
    var series = new anychart.plots.axesPlot.series.bar.BarSeries();
    series.setType(anychart.series.SeriesType.BAR);
    series.setClusterKey(anychart.series.SeriesType.BAR);
    series.setShapeType(this.shapeType_);
    return series;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'bar_series';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'bar_style';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings.prototype.createStyle = function () {
    switch (this.shapeType_) {
        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.CONE:
            return this.plot_.isHorizontal_ ?
                new anychart.plots.axesPlot.series.bar.styles.HorizontalConeStyle() :
                new anychart.plots.axesPlot.series.bar.styles.VerticalConeStyle();

        case anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX:
        default:
            return new anychart.plots.axesPlot.series.bar.styles.BarStyle();
    }
};
//------------------------------------------------------------------------------
//                          Elements position
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.BarPoint.prototype.setAnchorPoint = function (point, anchor, bounds, padding) {
    anchor = this.checkAnchorOnInvert(anchor, this.series_.getArgumentAxis());
    if (anchor == anychart.layout.Anchor.X_AXIS)
        goog.base(this, 'setAnchorPoint', point, anchor, bounds, padding);
    else this.styleShapes_.setAnchorPoint(point, anchor, bounds, padding);
};
//------------------------------------------------------------------------------
//
//                          RangeBarPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangePoint}
 */
anychart.plots.axesPlot.series.bar.RangeBarPoint = function () {
    anychart.plots.axesPlot.data.RangePoint.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.RangeBarPoint,
    anychart.plots.axesPlot.data.RangePoint);
//------------------------------------------------------------------------------
//                          Initialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.getShapeType = function () {
    return anychart.plots.axesPlot.series.bar.styles.BarShapeType.BOX;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.calculateBounds = function () {
    var leftTop = new anychart.utils.geom.Point();
    var rightBottom = new anychart.utils.geom.Point();
    var series = this.getSeries();
    var width = this.getGlobalSettings().getBarWidth(this) / 2;

    series.getArgumentAxis().transform(this, this.getX(), leftTop, -width);
    series.getValueAxis().transform(this, this.start_, leftTop);
    series.getArgumentAxis().transform(this, this.getX(), rightBottom, width);
    series.getValueAxis().transform(this, this.end_, rightBottom);

    var x = Math.min(leftTop.x, rightBottom.x);
    var y = Math.min(leftTop.y, rightBottom.y);

    this.bounds_ = new anychart.utils.geom.Rectangle(
        x,
        y,
        Math.max(leftTop.x, rightBottom.x) - x,
        Math.max(leftTop.y, rightBottom.y) - y);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.isCornersCanBeVisible = function () {
    return true;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.isAllCornersVisible = function () {
    return true;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarPoint.prototype.isBottomGradientVisible = function () {
    return true;
};
//------------------------------------------------------------------------------
//
//                          RangeBarSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangeSeries}
 */
anychart.plots.axesPlot.series.bar.RangeBarSeries = function () {
    anychart.plots.axesPlot.data.RangeSeries.apply(this);
    this.isFixedBarWidth_ = false;
};
goog.inherits(anychart.plots.axesPlot.series.bar.RangeBarSeries,
    anychart.plots.axesPlot.data.RangeSeries);
//------------------------------------------------------------------------------
//                              Point.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.RangeBarSeries.prototype.createPoint = function () {
    return  new anychart.plots.axesPlot.series.bar.RangeBarPoint();
};
//------------------------------------------------------------------------------
//
//                          RangeBarGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangeGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings = function () {
    anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings,
    anychart.plots.axesPlot.data.RangeGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.isFixedBarWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.fixedBarWidth_ = null;
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    if (anychart.utils.deserialization.hasProp(data, 'point_width'))
        this.isFixedBarWidth_ = true;
    this.fixedBarWidth_ = anychart.utils.deserialization.getNumProp(data, 'point_width');
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.getBarWidth = function (point) {
    //todo : implement scatter
    /*if(this.plot_.isScatter())
     return (this.isPercentPointScetterWidth_ ? this.plot_.getBounds().width : 1) * this.pointScetterWidth_;*/
    return this.isFixedBarWidth_ ? this.fixedBarWidth_ : point.getClusterSettings().getClusterWidth();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.createStyle = function () {
    return new anychart.plots.axesPlot.series.bar.styles.RangeBarStyle();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.createSeries = function (seriesType) {
    var series = new anychart.plots.axesPlot.series.bar.RangeBarSeries();
    series.setType(anychart.series.SeriesType.RANGE_BAR);
    series.setClusterKey(anychart.series.SeriesType.RANGE_BAR);
    return series;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'range_bar_series';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'bar_style';
};



