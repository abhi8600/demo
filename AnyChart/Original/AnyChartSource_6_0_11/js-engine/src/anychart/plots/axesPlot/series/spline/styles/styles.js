/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {}</li>.
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.spline.styles');
goog.require('anychart.plots.axesPlot.series.line.styles');
//------------------------------------------------------------------------------
//
//                         SplineStyleShape class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.styles.LineStyleShape}
 */
anychart.plots.axesPlot.series.spline.styles.SplineStyleShape = function() {
    anychart.plots.axesPlot.series.line.styles.LineStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.styles.SplineStyleShape,
        anychart.plots.axesPlot.series.line.styles.LineStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.spline.styles.SplineStyleShape.prototype.updatePath = function() {
    this.point_.calcBounds();
    var drawingPoints = this.point_.getDrawingPoints();
    var splineDrawer  = this.point_.getSeries().getSplineDrawer();
    for (var j = 0; j < drawingPoints.length; j++) {
        splineDrawer.processNewItem(drawingPoints[j].x, drawingPoints[j].y)
    }
    if (this.point_.getSeries().getNumPoints() - 1 == this.point_.getIndex()) {
        splineDrawer.finalizeDrawing();
    }
    this.element_.setAttribute(
            'd',
            this.styleShapes_.getElementPath());
};
//------------------------------------------------------------------------------
//
//                         SplineStyleShapes class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.styles.LineStyleShapes}
 */
anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes = function() {
    anychart.plots.axesPlot.series.line.styles.LineStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes,
        anychart.plots.axesPlot.series.line.styles.LineStyleShapes);
//------------------------------------------------------------------------------
//                      Initialization.
//-----------------------------------------------------------------------------
anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes.prototype.createShapes = function() {
    if (this.point_.getStyle().needCreateStrokeShape()) {
        this.lineShape_ = new anychart.plots.axesPlot.series.spline.styles.SplineStyleShape();
        this.lineShape_.initialize(this.point_, this);
    }
};
//------------------------------------------------------------------------------
//                    Drawing
//------------------------------------------------------------------------------

anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes.prototype.createElement = function() {
    return this.point_.getSVGManager().createPath();
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes.prototype.getElementPath = function() {
    var svgManager = this.point_.getSVGManager();
    var drawingPoints = this.point_.getDrawingPoints();
    var pointsCount = drawingPoints.length;
    var drawingInfo = this.point_.getSeries().getDrawingInfo();

    var pathData;
    var cp, cp1, cp2, mp, i;
    i = this.point_.getIndex();
    if (pointsCount == 2) {
        if (i == this.point_.getSeries().getNumPoints() - 1) {
            cp = new anychart.utils.geom.Point();
            cp.x = drawingInfo[2 * i - 1].x;
            cp.y = drawingInfo[2 * i - 1].y;

            pathData = svgManager.pMove(drawingPoints[0].x, drawingPoints[0].y);
            pathData += svgManager.pQuadraticBezier(
                    cp.x, cp.y,
                    drawingPoints[1].x, drawingPoints[1].y);
        } else {
            pathData = svgManager.pMove(drawingPoints[0].x, drawingPoints[0].y);
            cp = new anychart.utils.geom.Point();
            cp.x = drawingInfo[i].x;
            cp.y = drawingInfo[i].y;
            pathData += svgManager.pQuadraticBezier(
                    cp.x, cp.y,
                    drawingPoints[1].x, drawingPoints[1].y)
        }
    } else {
        cp1 = drawingInfo[2 * i - 1][0];
        cp2 = drawingInfo[2 * i - 1][1];
        mp = drawingInfo[2 * i - 1][2];

        pathData = svgManager.pMove(drawingPoints[0].x, drawingPoints[0].y);
        pathData += svgManager.pQuadraticBezier(
                cp1.x, cp1.y,
                mp.x, mp.y);

        pathData += svgManager.pQuadraticBezier(
                cp2.x, cp2.y,
                drawingPoints[2].x, drawingPoints[2].y);
    }

    return pathData;
};

//------------------------------------------------------------------------------
//
//                       SplineStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyle}
 */
anychart.plots.axesPlot.series.spline.styles.SplineStyle = function() {
    anychart.styles.line.LineStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.styles.SplineStyle,
        anychart.styles.line.LineStyle);
//------------------------------------------------------------------------------
//                          StyleShapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.spline.styles.SplineStyle.prototype.createStyleShapes = function(point) {
    return new anychart.plots.axesPlot.series.spline.styles.SplineStyleShapes();
};