/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort}</li>
 *  <li>@class{anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.viewPorts.labels');
goog.require('anychart.plots.axesPlot.axes.elements.labels');
//-----------------------------------------------------------------------------------
//
//                          AxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort = function(viewPort) {
    this.viewPort_ = viewPort;
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.viewPort_ = null;
anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getMainViewPort = function() { return this.viewPort_; };

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getMaxRotatedLabelsCount = function(maxNonRotatedLabelSize,maxLabelSize,rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getTextSpace = function(bounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.setLabelPosition = function(label, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getLabelLastPosition = function(isInvertedLabelsCheck, rotation, pos, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.isFirstTypeLabelsOverlap = function(maxNonRotatedLabelSize, rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.isLabelsOverlaped = function(isInvertedLabelsCheck, rotation, pos, lastX, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getLabelXOffsetWidthMult = function(qIndex, rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getLabelXOffsetHeightMult = function(qIndex, rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getLabelYOffsetWidthMult = function(qIndex, rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getLabelYOffsetHeightMult = function(qIndex, rotation) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.isInvertedLabelsCheck = function(isScaleInverted) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.applyLabelsOffsets = function(bounds, firstLabeBounds, lastLabelBounds) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.isStagerLabelsOverlaped = function(isInvertedLabelsCheck, pos, bounds, lastX) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.prototype.getStagerLabelLastPosition = function(isInvertedLabelsCheck, pos, bounds) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort);

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getTextSpace = function(bounds) {
    return bounds.height;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getMaxRotatedLabelsCount =
        function(maxNonRotatedLabelSize,maxLabelSize,rotation) {

    var w = this.viewPort_.getBounds().width;
    var maxLabels = 0;
    
    if (rotation.degRotation % 90 != 0) {
        var dl = maxNonRotatedLabelSize.width*rotation.absTan;
        maxLabels = (dl < maxNonRotatedLabelSize.height) ? (Number((w*rotation.absCos)/maxNonRotatedLabelSize.width)) :
                                                           (Number((w*rotation.absSin)/maxNonRotatedLabelSize.height));
    }else if (rotation.degRotation % 180 != 0) {
        maxLabels = w/maxNonRotatedLabelSize.height;
    }else {
        maxLabels = w/maxLabelSize.width;
    }
    return maxLabels;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.isInvertedLabelsCheck = function(isScaleInverted) {
    return isScaleInverted;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.isFirstTypeLabelsOverlap = function(maxNonRotatedLabelSize, rotation) {
    return (maxNonRotatedLabelSize.width*rotation.absTan) < maxNonRotatedLabelSize.height;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getLabelYOffsetWidthMult = function() {
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getLabelYOffsetHeightMult = function() {
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.setLabelPosition = function(label, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    pos.x = this.viewPort_.getBounds().x + pixValue;
    pos.y = 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.isLabelsOverlaped = function(isInvertedLabelsCheck, rotation, pos, lastX, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    if (isInvertedLabelsCheck) {
        if (rotation.is90C) {
            return (pos.x + rotatedBounds.width > lastX);
        }else {
            var dw = (isT1Overlap ? (nonRotatedBounds.width/rotation.absCos) : (nonRotatedBounds.height/rotation.absSin));
            return (pos.x + dw > lastX);
        }
    }
    return (Math.round(pos.x) < lastX);
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getLabelLastPosition = function(isInvertedLabelsCheck, rotation, pos, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    if (isInvertedLabelsCheck)
        return pos.x;
    return pos.x + (rotation.is90C ? (rotatedBounds.width) :
                             (isT1Overlap ? (nonRotatedBounds.width/rotation.absCos) : (nonRotatedBounds.height/rotation.absSin))
                    );
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.applyLabelsOffsets = function(bounds, firstLabelBounds, lastLabelBounds) {
    var leftLabel = firstLabelBounds;
    var rightLabel = lastLabelBounds;

    var scale = this.viewPort_.getAxis().getScale();

    if (scale.isInverted()) {
        leftLabel = lastLabelBounds;
        rightLabel = firstLabelBounds;
    }
    if (leftLabel.x < 0) {
        bounds.x += -leftLabel.x;
        bounds.width += leftLabel.x;
    }
    if (rightLabel.x + rightLabel.width > this.viewPort_.getBounds().x + this.viewPort_.getBounds().width) {
        bounds.width -= (rightLabel.x + rightLabel.width) - this.viewPort_.getBounds().width - this.viewPort_.getBounds().x;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    pos.x = pixValue - bounds.x - bounds.width/2 + this.viewPort_.getBounds().x;
    pos.y = 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.isStagerLabelsOverlaped = function(isInvertedLabelsCheck, pos, bounds, lastX) {
    if (isInvertedLabelsCheck)
        return (pos.x + bounds.width > lastX);
    return (pos.x < lastX);
};

anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.prototype.getStagerLabelLastPosition = function(isInvertedLabelsCheck, pos, bounds) {
    if (isInvertedLabelsCheck)
        return pos.x;
    return pos.x + bounds.width;
};

//-----------------------------------------------------------------------------------
//
//                          BottomAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort);

anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort.prototype.getLabelXOffsetWidthMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return -.5;
    if (rotation.degRotation % 90 == 0) return 0;
    if (qIndex == 1 || qIndex == 3)
        return -rotation.absCos;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort.prototype.getLabelXOffsetHeightMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return 0;
    if (rotation.degRotation % 90 == 0) return -.5;
    if (qIndex == 0 || qIndex == 2)
        return -rotation.absSin;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort.prototype.setLabelPosition = function(labels, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    goog.base(this, 'setLabelPosition', labels, pos, rotatedBounds, nonRotatedBounds, pixValue);

    pos.y += this.viewPort_.getBounds().getBottom() + labels.getOffset();
    switch (labels.getAlign()) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.y += (labels.getSpace() - rotatedBounds.height)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.y += labels.getSpace() - rotatedBounds.height;
            break;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.BottomAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    goog.base(this, 'setStagerLabelPosition', labels, pos, bounds, pixValue, offset, space);

    pos.y += this.viewPort_.getBounds().getBottom() + labels.getOffset() + offset;
    switch (labels.getAlign()) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.y += (space - bounds.height)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.y += space - bounds.height;
            break;
    }
};

//-----------------------------------------------------------------------------------
//
//                          TopAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.HorizontalAxisLabelsViewPort);

anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort.prototype.getLabelXOffsetWidthMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return -.5;
    if (rotation.degRotation % 90 == 0) return 0;
    if (qIndex == 0 || qIndex == 2)
        return -rotation.absCos;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort.prototype.getLabelXOffsetHeightMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return 0;
    if (rotation.degRotation % 90 == 0) return -.5;
    if (qIndex == 1 || qIndex == 3)
        return -rotation.absSin;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort.prototype.setLabelPosition = function(labels, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    goog.base(this, 'setLabelPosition', labels, pos, rotatedBounds, nonRotatedBounds, pixValue);

    pos.y += this.viewPort_.getBounds().y - labels.getOffset() - rotatedBounds.height;
    switch (labels.getAlign()) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.y -= (labels.getSpace() - rotatedBounds.height)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.y -= labels.getSpace() - rotatedBounds.height;
            break;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.TopAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    goog.base(this, 'setStagerLabelPosition', labels, pos, bounds, pixValue, offset, space);

    pos.y += this.viewPort_.getBounds().y - labels.getOffset() - bounds.height - offset;
    switch (labels.getAlign()) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.y -= (space - bounds.height)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.y -= space - bounds.height;
            break;
    }
};


//-----------------------------------------------------------------------------------
//
//                          VerticalAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.AxisLabelsViewPort);

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getTextSpace = function(bounds) {
    return bounds.width;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getMaxRotatedLabelsCount =
        function(maxNonRotatedLabelSize,maxLabelSize,rotation) {

    var w = this.viewPort_.getBounds().height;
    var maxLabels = 0;
    if (rotation && rotation.degRotation % 90 != 0) {
        var dl = maxNonRotatedLabelSize.width/rotation.absTan;
        maxLabels = (dl < maxNonRotatedLabelSize.height) ? (Number((w*rotation.absSin)/maxNonRotatedLabelSize.width)) :
                                                          (Number((w*rotation.absCos)/maxNonRotatedLabelSize.height));
    }else if (rotation && rotation.degRotation % 180 != 0) {
        maxLabels = w/maxNonRotatedLabelSize.width;
    }else {
        maxLabels = w/maxLabelSize.height;
    }    
    return maxLabels;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.isInvertedLabelsCheck = function(isScaleInverted) {
    return !isScaleInverted;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.isFirstTypeLabelsOverlap = function(maxNonRotatedLabelSize, rotation) {
    return (maxNonRotatedLabelSize.width/rotation.absTan) < maxNonRotatedLabelSize.height;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getLabelXOffsetWidthMult = function() {
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getLabelXOffsetHeightMult = function() {
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.setLabelPosition = function(label, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    pos.y = this.viewPort_.getBounds().getBottom() - pixValue;
    pos.x = 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.isLabelsOverlaped = function(isInvertedLabelsCheck, rotation, pos, lastPosition, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    if (isInvertedLabelsCheck) {
        if (rotation.is90C) {
            return (pos.y + rotatedBounds.height > lastPosition);
        }else {
            var dw = isT1Overlap ? (nonRotatedBounds.width/rotation.absSin) : (nonRotatedBounds.height/rotation.absCos);
            return (pos.y + dw > lastPosition);
        }
    }
    return (Math.round(pos.y) < lastPosition);
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getLabelLastPosition = function(isInvertedLabelsCheck, rotation, pos, isT1Overlap, rotatedBounds, nonRotatedBounds) {
    if (isInvertedLabelsCheck) return pos.y;
    return pos.y + (rotation.is90C ?
            (rotatedBounds.height) :
                    (isT1Overlap ? ((nonRotatedBounds.width/rotation.absSin)) :
                    (nonRotatedBounds.height/rotation.absCos)));
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.applyLabelsOffsets = function(bounds, firstLabelBounds, lastLabelBounds) {
    var topLabel = lastLabelBounds;
    var bottomLabel = firstLabelBounds;
    if (this.viewPort_.getAxis().getScale().isInverted()) {
        topLabel = firstLabelBounds;
        bottomLabel = lastLabelBounds;
    }
    if (topLabel.y < 0) {
        bounds.y += -topLabel.y;
        bounds.height += topLabel.y;
    }
    if (bottomLabel.y + bottomLabel.height > this.viewPort_.getBounds().y + this.viewPort_.getBounds().height) {
        bounds.height -= (bottomLabel.y + bottomLabel.height) - this.viewPort_.getBounds().height - this.viewPort_.getBounds().y;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    pos.y = this.viewPort_.getBounds().getBottom() - pixValue - bounds.height/2;
    pos.x = 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.isStagerLabelsOverlaped = function(isInvertedLabelsCheck, pos, bounds, lastPos) {
    if (isInvertedLabelsCheck)
        return (pos.y + bounds.height > lastPos);
    return (pos.y < lastPos);
};

anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.prototype.getStagerLabelLastPosition = function(isInvertedLabelsCheck, pos, bounds) {
    if (isInvertedLabelsCheck)
        return pos.y;
    return pos.y + bounds.height;
};

//-----------------------------------------------------------------------------------
//
//                          LeftAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort);

anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort.prototype.getLabelYOffsetWidthMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return 0;
    if (rotation.degRotation % 90 == 0) return -.5;
    if (qIndex == 0 || qIndex == 2)
        return -rotation.absSin;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort.prototype.getLabelYOffsetHeightMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return -.5;
    if (rotation.degRotation % 90 == 0) return 0;
    if (qIndex == 1 || qIndex == 3)
        return -rotation.absCos;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort.prototype.setLabelPosition = function(labels, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    goog.base(this, 'setLabelPosition', labels, pos, rotatedBounds, nonRotatedBounds, pixValue);

    pos.x += this.viewPort_.getBounds().x - labels.getOffset() - rotatedBounds.width;
    switch (labels.getAlign()) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.x -= (labels.getSpace() - rotatedBounds.width)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.x -= labels.getSpace() - rotatedBounds.width;
            break;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.LeftAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    goog.base(this, 'setStagerLabelPosition', labels, pos, bounds, pixValue, offset, space);

    pos.x += this.viewPort_.getBounds().x - labels.getOffset() - bounds.width - offset;
    switch (labels.align) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.x -= (space - bounds.width)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.x -= space - bounds.width;
            break;
    }
};

//-----------------------------------------------------------------------------------
//
//                          RightAxisLabelsViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort = function(viewPort) {
    anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort.call(this,viewPort);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort,
              anychart.plots.axesPlot.axes.viewPorts.labels.VerticalAxisLabelsViewPort);


anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort.prototype.getLabelYOffsetWidthMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return 0;
    if (rotation.degRotation % 90 == 0) return -.5;
    if (qIndex == 1 || qIndex == 3)
        return -rotation.absSin;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort.prototype.getLabelYOffsetHeightMult = function(qIndex, rotation) {
    if (rotation.degRotation % 180 == 0) return -.5;
    if (rotation.degRotation % 90 == 0) return 0;
    if (qIndex == 0 || qIndex == 2)
        return -rotation.absCos;
    return 0;
};

anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort.prototype.setLabelPosition = function(labels, pos, rotatedBounds, nonRotatedBounds, pixValue) {
    goog.base(this, 'setLabelPosition', labels, pos, rotatedBounds, nonRotatedBounds, pixValue);

    pos.x += this.viewPort_.getBounds().getRight() + labels.getOffset();
    switch (labels.align) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.x += (labels.getSpace() - rotatedBounds.width)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.x += labels.getSpace() - rotatedBounds.width;
            break;
    }
};

anychart.plots.axesPlot.axes.viewPorts.labels.RightAxisLabelsViewPort.prototype.setStagerLabelPosition = function(labels, pos, bounds, pixValue, offset, space) {
    goog.base(this, 'setStagerLabelPosition', labels, pos, bounds, pixValue, offset, space);

    pos.x += this.viewPort_.getBounds().getRight() + labels.getOffset() + offset;
    switch (labels.align) {
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.CENTER:
            pos.x += (space - bounds.width)/2;
            break;
        case anychart.plots.axesPlot.axes.elements.labels.AxisLabelsAlign.OUTSIDE:
            pos.x += space - bounds.width;
            break;
    }
};