/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyle}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.markers.styles');
//------------------------------------------------------------------------------
//
//                          AxisMarkerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState = function(style, opt_stateType) {
    anychart.styles.StyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState,
        anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel}
 */
anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState.prototype.label_ = null;
/**
 * @return {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel}
 */
anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState.prototype.getLabel = function() {
    return this.label_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    if (des.isEnabledProp(data, 'label')) {
        this.label_ = new anychart.plots.axesPlot.axes.markers.AxisMarkerLabel();
        this.label_.deserialize(des.getProp(data, 'label'));
    }
    return data;
};
//------------------------------------------------------------------------------
//
//                    LineAxisMarkerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState}
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState = function(style, opt_stateType) {
    anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState,
        anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState.prototype.stroke_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState.prototype.getStroke = function() {
    return this.stroke_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState.prototype.deserialize = function(data) {
    data = goog.base(this, 'deserialize', data);
    this.stroke_ = new anychart.visual.stroke.Stroke();
    this.stroke_.deserialize(data);
    return data;
};
//------------------------------------------------------------------------------
//
//                     LineAxisMarkerStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyle,
        anychart.styles.Style);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyleState;
};
//------------------------------------------------------------------------------
//
//                          AxisMarkerStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState = function(style, opt_styleState) {
    anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState.call(this, opt_styleState);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState,
        anychart.plots.axesPlot.axes.markers.styles.AxisMarkerStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.minimumLine_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.getMinimumLine = function() {
    return this.minimumLine_;
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.maximumLine_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.getMaximumLine = function() {
    return this.maximumLine_;
};
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.fill_ = null;
/**
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.getFill = function() {
    return this.fill_;
};
/**
 * @private
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.hatchFill_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.getHatchFill = function() {
    return this.hatchFill_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState.prototype.deserialize = function(data) {
    data = goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization;

    if (des.isEnabledProp(data, 'minimum_line')) {
        this.minimumLine_ = new anychart.visual.stroke.Stroke();
        this.minimumLine_.deserialize(des.getProp(data, 'minimum_line'));
    }

    if (des.isEnabledProp(data, 'maximum_line')) {
        this.maximumLine_ = new anychart.visual.stroke.Stroke();
        this.maximumLine_.deserialize(des.getProp(data, 'maximum_line'));
    }

    if (des.isEnabledProp(data, 'fill')) {
        this.fill_ = new anychart.visual.fill.Fill();
        this.fill_.deserialize(des.getProp(data, 'fill'));
    }

    if (des.isEnabledProp(data, 'hatch_fill')) {
        this.hatchFill_ = new anychart.visual.hatchFill.HatchFill();
        this.hatchFill_.deserialize(des.getProp(data, 'hatch_fill'));
    }

    return data;
};
//------------------------------------------------------------------------------
//
//                     RangeAxisMarkerStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyle,
        anychart.styles.Style);
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyleState;
};