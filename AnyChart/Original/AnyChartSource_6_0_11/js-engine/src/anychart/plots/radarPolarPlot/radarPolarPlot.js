/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.RadarPolarPlot}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.PolarPlot}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.RadarPlot}</li>
 * <ul>
 */
//namespace
goog.provide('anychart.plots.radarPolarPlot');
//requires
goog.require('anychart.plots.radarPolarPlot.templates');
goog.require('anychart.plots.radarPolarPlot.visual.background');
goog.require('anychart.plots.radarPolarPlot.axes');
goog.require('anychart.plots.radarPolarPlot.series.line');
goog.require('anychart.plots.radarPolarPlot.series.area');
goog.require('anychart.plots.radarPolarPolar.series.marker');
//------------------------------------------------------------------------------
//
//                          RadarPolarPlot class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot = function () {
    goog.base(this);
    this.isFixedRadius_ = false;
    this.isPercentRadius_ = true;
    this.startAngle_ = -90;
    this.radius_ = .7;
    this.xCategories = [];
    this.drawingModeCircle = true;
    this.usePolarCoords = false;
    this.displayUnderData_ = true;
    this.center_ = new anychart.utils.geom.Point();
};
goog.inherits(
    anychart.plots.radarPolarPlot.RadarPolarPlot,
    anychart.plots.seriesPlot.SeriesPlot
);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.displayUnderData_ = null;
/**
 * @protected
 * @type {anychart.plots.radarPolarPlot.axes.RotatedAxis}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.xAxis_ = null;
/**
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getXScale = function () {
    return this.xAxis_.getScale();
};
/**
 * @protected
 * @type {Number}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.startAngle_ = null;
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.isFixedRadius_ = null;
/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.isPercentRadius_ = null;
/**
 * @protected
 * @type {Number}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.radius_ = null;
/**
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.drawingModeCircle = null;
/**
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.usePolarCoords = null;
/**
 * @type {anychart.plots.radarPolarPlot.axes.ValueAxis}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.yAxis = null;
/**
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getYScale = function () {
    return this.yAxis.getScale();
};
/**
 * @type {Array}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.xCategories = null;
/**
 * @return {Array}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getXCategories = function () {
    return this.xCategories;
};
//------------------------------------------------------------------------------
//                  Axes visual properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.axesSprite_ = null;
/**
 * @private
 * @type {Array.<anychart.svg.SVGSprite>}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.resizeClearSprites_ = null;
//------------------------------------------------------------------------------
//                  Bounds properties.
//------------------------------------------------------------------------------
/**
 * Radar/polar plot center point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.center_ = null;
/**
 * Return radar/polar plot center point.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getCenter = function () {
    return this.center_;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.axesBounds_ = null;
//------------------------------------------------------------------------------
//                  Background visual properties.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.radarPolarBackground = null;
/**
 * @protected
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.radarPolarBackgroundSprite = null;
//------------------------------------------------------------------------------
//                      Default Templates.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getTemplatesMerger = function () {
    return new anychart.plots.radarPolarPlot.templates.RadarPolarPlotTemplatesMerger();
};
//------------------------------------------------------------------------------
//                  Global series settings.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.createGlobalSeriesSettings = function (seriesType) {
    switch (seriesType) {
        default:
        case anychart.series.SeriesType.LINE:
            return new anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings();
        case anychart.series.SeriesType.AREA:
            return new anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings();
        case anychart.series.SeriesType.MARKER:
            return new anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings();
    }
};
//------------------------------------------------------------------------------
//                  Series type.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getSeriesType = function (seriesType) {
    switch (seriesType) {
        case 'line':
            return anychart.series.SeriesType.LINE;
        case 'area':
            return anychart.series.SeriesType.AREA;
        case 'marker':
            return anychart.series.SeriesType.MARKER;
        default:
            return this.defaultSeriesType_;
    }
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.deserialize = function (data, stylesList) {
    var des = anychart.utils.deserialization;

    //deserialize axes settings
    if (des.hasProp(data, 'chart_settings')) {
        var chartSettings = des.getProp(data, 'chart_settings');
        if (des.hasProp(chartSettings, 'axes'))
            this.deserializeAxes_(des.getProp(chartSettings, 'axes'));
    }

    goog.base(this, 'deserialize', data, stylesList);

    //axes after deserialize
    this.xAxis_.onAfterDeserializeData();
    this.yAxis.initAngle();
    this.yAxis.onAfterDeserializeData()
};
/**
 * @protected
 * @param {Object} data JSON object with radar/polar settings.
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.deserializeRadarPolarSettings = function (data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'start_angle'))
        this.startAngle_ = -90 + des.getNumProp(data, 'start_angle');

    if (des.hasProp(data, 'fixed_radius'))
        this.isFixedRadius_ = des.getBoolProp(data, 'fixed_radius');

    if (this.isFixedRadius_) {
        if (des.hasProp(data, 'radius')) {
            this.isPercentRadius_ = des.isPercentProp(data, 'radius');
            this.radius_ = this.isPercentRadius_ ?
                des.getPercentProp(data, 'radius') :
                des.getNumProp(data, 'radius');
        }
    }

    if (des.hasProp(data, 'use_polar_coords'))
        this.usePolarCoords = des.getBoolProp(data, 'use_polar_coords');

    if (des.hasProp(data, 'display_data_mode'))
        this.displayUnderData_ = des.getEnumProp(data, 'display_data_mode') == 'aboveaxes';
};
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.deserializeBackground = function (data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'background')) {
        this.radarPolarBackground = new anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground(this);
        this.radarPolarBackground.deserialize(des.getProp(data, 'background'));
    }
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.onAfterDeserializeData = function () {

};
//------------------------------------------------------------------------------
//                  Axes deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize radar/polar axes settings.
 * @private
 * @param {Object} data Data object with axes settings.
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.deserializeAxes_ = function (data) {
    var des = anychart.utils.deserialization;
    //deserialize yAxis
    this.yAxis = new anychart.plots.radarPolarPlot.axes.ValueAxis(this);
    this.yAxis.deserialize(des.getProp(data, 'y_axis'));
    //deserialize xAxis
    this.xAxis_ = this.createXAxis();
    this.xAxis_.deserialize(des.getProp(data, 'x_axis'));
};
/**
 * Deserialize xAxis settings.
 * @protected
 * @return {anychart.plots.radarPolarPlot.axes.RotatedAxis}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.createXAxis = function () {
    return new anychart.plots.radarPolarPlot.axes.RotatedAxis(this);
};
//------------------------------------------------------------------------------
//                  Check point value.
//------------------------------------------------------------------------------
/**
 * Check radar polar point value.
 * @param {anychart.plots.radarPolarPlot.data.RadarPolarPoint} point
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.checkPoint = function (point) {
    var xScale = this.xAxis_.getScale();
    if (isNaN(xScale.getDataRangeMinimum()) || (point.getX() < xScale.getDataRangeMinimum()))
        xScale.setDataRangeMinimum(point.getX());

    if (isNaN(xScale.getDataRangeMaximum()) || point.getX() > xScale.getDataRangeMaximum())
        xScale.setDataRangeMaximum(point.getX());
};

anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.checkYValue = function (value) {
    var yScale = this.yAxis.getScale();
    if (isNaN(yScale.getDataRangeMinimum()) || (value < yScale.getDataRangeMinimum()))
        yScale.setDataRangeMinimum(value);

    if (isNaN(yScale.getDataRangeMaximum()) || value > yScale.getDataRangeMaximum())
        yScale.setDataRangeMaximum(value);
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.initialize = function (bounds, tooltipContainer, opt_drawBackground) {
    goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);

    if (this.radarPolarBackground && this.radarPolarBackground.isEnabled()) {
        this.radarPolarBackgroundSprite = this.radarPolarBackground.initialize(this.getSVGManager());
        if (IS_ANYCHART_DEBUG_MOD) this.radarPolarBackgroundSprite.setId('RadarPolarBackgroundSprite');
        this.beforeDataSprite_.appendSprite(this.radarPolarBackgroundSprite);
    }

    this.axesSprite_ = new anychart.svg.SVGSprite(this.getSVGManager());
    var axesLinesSprite = new anychart.svg.SVGSprite(this.getSVGManager()),
        axesLabelsSprite = new anychart.svg.SVGSprite(this.getSVGManager()),
        axesTickmarksSprite = new anychart.svg.SVGSprite(this.getSVGManager()),
        axesGridInterlacesSprite = new anychart.svg.SVGSprite(this.getSVGManager()),
        axesGridLinesSprite = new anychart.svg.SVGSprite(this.getSVGManager());

    if (IS_ANYCHART_DEBUG_MOD) {
        this.axesSprite_.setId('AxesSprite');
        axesLabelsSprite.setId('AxesLabelsSprite');
        axesLinesSprite.setId('AxesLinesSprite');
        axesTickmarksSprite.setId('AxesTickmarksSprite');
        axesGridLinesSprite.setId('AxesGridLinesSprite');
        axesGridInterlacesSprite.setId('AxesInterlacesSprite');
    }


    this.axesSprite_.appendSprite(axesGridInterlacesSprite);
    this.axesSprite_.appendSprite(axesGridLinesSprite);
    this.beforeDataSprite_.appendSprite(this.axesSprite_);

    if (this.displayUnderData_) {
        this.axesSprite_.appendSprite(axesLinesSprite);
        this.axesSprite_.appendSprite(axesTickmarksSprite);
        this.axesSprite_.appendSprite(axesLabelsSprite);
    } else {
        this.afterDataSprite_.appendSprite(axesLinesSprite);
        this.afterDataSprite_.appendSprite(axesTickmarksSprite);
        this.afterDataSprite_.appendSprite(axesLabelsSprite);
    }

    //clear this sprites before resize
    this.resizeClearSprites_ = [
        axesTickmarksSprite,
        axesLabelsSprite,
        axesGridInterlacesSprite,
        axesGridLinesSprite
    ];

    //initialize xAxis
    this.xAxis_.setContainers(
        axesGridInterlacesSprite,
        axesGridLinesSprite,
        axesTickmarksSprite,
        axesLabelsSprite,
        axesLinesSprite
    );
    this.xAxis_.initialize(this.getSVGManager());

    //initialize yAxis
    this.yAxis.setContainers(
        axesGridInterlacesSprite,
        axesGridLinesSprite,
        axesTickmarksSprite,
        axesLabelsSprite,
        axesLinesSprite
    );
    this.yAxis.initialize(this.getSVGManager());

    this.axesBounds_ = this.calcBoundsPadding(bounds);
    this.xAxis_.calcBounds(this.axesBounds_);
    this.yAxis.calcBounds(this.axesBounds_);
    this.calcCenter_();

    this.sprite_.updateClipPath(new anychart.utils.geom.Rectangle(
        0,
        0,
        this.bounds_.width,
        this.bounds_.height
    ));
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                  Bounds calculating.
//------------------------------------------------------------------------------
/**
 * Calculate radar/polar plot center point.
 * @private
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.calcCenter_ = function () {
    this.center_.x = this.bounds_.width / 2;
    this.center_.y = this.bounds_.height / 2;
};
/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.calcBoundsPadding = function (bounds) {
    var axisBounds = bounds.clone();
    axisBounds.x = 0;
    axisBounds.y = 0;

    var cx = axisBounds.width / 2,
        cy = axisBounds.height / 2,
        radius = Math.min(cx, cy),
        minRadius = radius;

    //calculate fixed radius and return
    if (this.isFixedRadius_) {
        if (this.isPercentRadius_)
            minRadius = radius * this.radius_;
        else
            minRadius = this.radius_;

        this.reduceRectBounds(axisBounds, radius - minRadius);
        return axisBounds
    }

    //no axis => no padding
    if (!this.xAxis_) return axisBounds;

    var crossing = this.getYAxisCrossing(),
        tickmarks = 0;

    if (this.xAxis_.hasMajorTickmarks() && this.xAxis_.getMajorTickmarks().isOutside()) {
        tickmarks = this.xAxis_.getMajorTickmarks().getSize();
        minRadius = Math.min(minRadius, radius - tickmarks);
    }

    if (this.xAxis_.hasLabels()) {
        var labels = this.xAxis_.getLabels();
        if (!labels.isInside()) {
            var scale = this.xAxis_.getScale(),
                startIndex = labels.showFirst() ? 0 : 1,
                endIndex = scale.getMajorIntervalCount(),
                i, labelBounds, angle;
            if (!labels.showLast()) endIndex--;
            if (this.plotType_ == 'radar') endIndex--;

            for (i = startIndex; i <= endIndex; i++) {
                if (labels.setAngle) labels.setAngle(-(scale.getMajorIntervalValue(i) + this.getYAxisCrossing()));
                labels.initSize(this.getSVGManager(), i);
                labelBounds = labels.getBounds();
                angle = scale.localTransform(scale.getMajorIntervalValue(i)) + crossing;
                minRadius = Math.min(
                    minRadius,
                    (cx - labelBounds.width - labels.getPadding() - tickmarks) / Math.abs(anychart.utils.geom.DrawingUtils.getPointX(1, angle)),
                    (cy - labelBounds.height - labels.getPadding() - tickmarks) / Math.abs(anychart.utils.geom.DrawingUtils.getPointY(1, angle))
                );
            }
        }
    }

    if (this.xAxis_.hasMinorTickmarks() && this.xAxis_.getMinorTickmarks().isOutside())
        minRadius = Math.min(minRadius, radius - this.xAxis_.getMinorTickmarks().getSize());

    if (radius > minRadius)
        this.reduceRectBounds(axisBounds, radius - minRadius);

    return axisBounds;
};
/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.reduceRectBounds = function (bounds, delta) {
    bounds.x += delta;
    bounds.width -= 2 * delta;
    bounds.y += delta;
    bounds.height -= 2 * delta;
};

/**
 *
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getYAxisCrossing = function () {
    return this.startAngle_;
};

/**
 * Return total thickness of outer axes elements like axes lines, last major/minor grid lines, etc.
 * @return {Number}
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.getPlotOuterThickness = function () {
    var res = 0;

    if (this.yAxis.hasMajorGrid() && this.yAxis.getMajorGrid().hasLine() && this.yAxis.getMajorGrid().drawLastLine())
        res = Math.max(res, this.yAxis.getMajorGrid().getLine().getThickness());

    if (this.yAxis.hasMinorGrid() && this.yAxis.getMinorGrid().hasLine() && this.yAxis.getMinorGrid().drawLastLine())
        res = Math.max(res, this.yAxis.getMinorGrid().getLine().getThickness());

    res /= 2;

    if (this.xAxis_.hasLine())
        res += this.xAxis_.getLine().getThickness();

    return res;
};
/**
 * @param {Number} xValue
 * @param {Number} yValue
 * @param {anychart.utils.geom.Point} point
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.transform = function (xValue, yValue, point) {
    var a = (this.xAxis_.getScale().transformValueToPixel(xValue) + this.getYAxisCrossing());
    var r = this.yAxis.getScale().transformValueToPixel(yValue);
    point.x = anychart.utils.geom.DrawingUtils.getPointX(r, a) + this.center_.x;
    point.y = anychart.utils.geom.DrawingUtils.getPointY(r, a) + this.center_.y;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------- -----
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.draw = function () {
    goog.base(this, 'draw');
    if (this.radarPolarBackgroundSprite) {

        this.radarPolarBackground.draw(
            this.radarPolarBackgroundSprite,
            this.calcBoundsPadding(this.bounds_),
            null
        );
    }

    this.yAxis.draw();
    this.xAxis_.draw();
};

//------------------------------------------------------------------------------
//                      Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.calculateResize = function (bounds) {
    goog.base(this, 'calculateResize', bounds);
    this.axesBounds_ = this.calcBoundsPadding(bounds);
    this.xAxis_.calcBounds(this.axesBounds_);
    this.yAxis.calcBounds(this.axesBounds_);
    this.calcCenter_();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.RadarPolarPlot.prototype.execResize = function () {
    goog.base(this, 'execResize');
    if (this.radarPolarBackgroundSprite)
        this.radarPolarBackground.resize(
            this.radarPolarBackgroundSprite,
            this.calcBoundsPadding(this.bounds_)
        );

    for (var i = 0, count = this.resizeClearSprites_.length; i < count; i++)
        this.resizeClearSprites_[i].clear();

    this.yAxis.resize();
    this.xAxis_.resize();
    this.sprite_.updateClipPath(new anychart.utils.geom.Rectangle(
        0,
        0,
        this.bounds_.width,
        this.bounds_.height
    ));
};
//------------------------------------------------------------------------------
//
//                          PolarPlot class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.RadarPolarPlot}
 */
anychart.plots.radarPolarPlot.PolarPlot = function () {
    goog.base(this);
    this.usePolarCoords = true;
};
goog.inherits(anychart.plots.radarPolarPlot.PolarPlot, anychart.plots.radarPolarPlot.RadarPolarPlot);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.PolarPlot.prototype.deserializeDataPlotSettings = function (data) {
    goog.base(this, 'deserializeDataPlotSettings', data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'polar')) {
        var polarData = des.getProp(data, 'polar');
        this.deserializeBackground(polarData);
        this.deserializeRadarPolarSettings(polarData);
    }
};
//------------------------------------------------------------------------------
//
//                          RadarPlot class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.RadarPolarPlot}
 */
anychart.plots.radarPolarPlot.RadarPlot = function () {
    goog.base(this);
    this.drawingModeCircle = false;
};
goog.inherits(
    anychart.plots.radarPolarPlot.RadarPlot,
    anychart.plots.radarPolarPlot.RadarPolarPlot
);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.RadarPlot.prototype.createXAxis = function () {
    return new anychart.plots.radarPolarPlot.axes.CategorizedAxis(this);
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.RadarPlot.prototype.deserializeRadarPolarSettings = function (data) {
    goog.base(this, 'deserializeRadarPolarSettings', data);

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'drawing_style'))
        this.drawingModeCircle = des.getEnumProp(data, 'drawing_style') == 'circle';
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.RadarPlot.prototype.deserializeDataPlotSettings = function (data) {
    goog.base(this, 'deserializeDataPlotSettings', data);

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'radar')) {
        var radarData = des.getProp(data, 'radar');
        this.deserializeBackground(radarData);
        this.deserializeRadarPolarSettings(radarData);
    }
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.RadarPlot.prototype.calcBoundsPadding = function (bounds) {
    if (this.drawingModeCircle)
        return goog.base(this, 'calcBoundsPadding', bounds);

    var axisBounds = bounds.clone(),
        cx = axisBounds.width / 2,
        cy = axisBounds.height / 2,
        radius = Math.min(cx, cy),

        minRadius;

    axisBounds.x = 0;
    axisBounds.y = 0;

    if (this.isFixedRadius_) {
        if (this.isPercentRadius_) {
            minRadius = radius * this.radius_;
        } else {
            minRadius = this.radius_;
        }

        this.reduceRectBounds(axisBounds, radius - minRadius);

        return axisBounds;
    }

    if (!this.xAxis_) return axisBounds;

    minRadius = Number.MAX_VALUE;
    var crossing = this.getYAxisCrossing(),
        len = this.getXScale().getMajorIntervalCount(),
        tickmark = 0,
        tmpRadius, angle, i;

    if (this.xAxis_.hasMajorTickmarks() && this.xAxis_.getMajorTickmarks().isOutside())
        tickmark = this.xAxis_.getMajorTickmarks().getSize();

    for (i = 0; i < len; i++) {
        angle = this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(i)) + crossing;

        tmpRadius = anychart.plots.radarPolarPlot.utils.MathUtils.divide(
            cx,
            Math.abs(anychart.utils.geom.DrawingUtils.getPointX(1, angle))
        );
        minRadius = Math.min(minRadius, tmpRadius - tickmark);

        tmpRadius = anychart.plots.radarPolarPlot.utils.MathUtils.divide(
            cy,
            Math.abs(anychart.utils.geom.DrawingUtils.getPointY(1, angle))
        );
        minRadius = Math.min(minRadius, tmpRadius - tickmark);
    }

    minRadius = Math.max(minRadius, radius);

    if (this.xAxis_.hasLabels() && !this.xAxis_.getLabels().isInside()) {
        var startIndex = this.xAxis_.getLabels().showFirst() ? 0 : 1,
            endIndex = len - (this.xAxis_.getLabels().showLast() ? 0 : 1),
            labels = this.xAxis_.getLabels(),
            labelBounds;

        for (i = startIndex; i < endIndex; i++) {

            labels.initSize(this.getSVGManager(), i);
            labelBounds = labels.getBounds();
            angle = this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(i)) + crossing;

            tmpRadius = anychart.plots.radarPolarPlot.utils.MathUtils.divide(
                cx - labelBounds.width - labels.getPadding() - tickmark,
                Math.abs(anychart.utils.geom.DrawingUtils.getPointX(1, angle)));

            minRadius = Math.min(minRadius, tmpRadius);

            tmpRadius = anychart.plots.radarPolarPlot.utils.MathUtils.divide(
                cy - labelBounds.height - labels.getPadding() - tickmark,
                Math.abs(anychart.utils.geom.DrawingUtils.getPointY(1, angle)));

            minRadius = Math.min(minRadius, tmpRadius);
        }
    }

    if (this.xAxis_.getMinorTickmarks() && this.xAxis_.getMinorTickmarks.isOutside())
        minRadius = Math.min(minRadius, radius - this.xAxis_.getMinorTickmarks().getSize());

    minRadius = Math.min(minRadius, radius - tickmark);

    if (radius > minRadius)
        this.reduceRectBounds(axisBounds, radius - minRadius);

    return axisBounds;

};






