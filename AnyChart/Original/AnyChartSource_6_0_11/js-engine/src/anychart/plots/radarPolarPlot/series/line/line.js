/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.LinePoint}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.LineSeries}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.PolarPoint}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.RadarPoint}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.series.line');
goog.require('anychart.plots.radarPolarPlot.data');
goog.require('anychart.plots.radarPolarPlot.utils');
goog.require('anychart.plots.radarPolarPlot.series.line.styles');
//------------------------------------------------------------------------------
//
//                          LinePoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.data.RadarPolarPoint}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint = function () {
    goog.base(this);
    this.pixValuePt_ = new anychart.utils.geom.Point();
    this.isValuePointsInitialized_ = false;
    this.isLineDrawable_ = false;
    this.bottomEnd_ = new anychart.utils.geom.Point();
    this.bottomStart_ = new anychart.utils.geom.Point();
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.LinePoint,
    anychart.plots.radarPolarPlot.data.RadarPolarPoint
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.bottomStart_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.getBottomStart = function () {
    return this.bottomStart_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.bottomEnd_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.getBottomEnd = function () {
    return this.bottomEnd_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.hasPrev_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.hasNext_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.isValuePointsInitialized_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.isLineDrawable_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.isLineDrawable = function () {
    return this.isLineDrawable_;
};
/**
 * @private
 * @type {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.drawingPoints_ = null;
/**
 * @return {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.getDrawingPoints = function () {
    return this.drawingPoints_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.pixValuePt_ = null;
/**
 * @return anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.getPixValuePt = function () {
    return this.pixValuePt_;
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);

    if (this.series_.needCreateGradientRect()) return;
    if (this.isMissing()) {
        this.series_.setNeedCreateGradientRect(this.isGradientState(this.style_.getMissing()));
    } else {
        this.series_.setNeedCreateGradientRect(
            this.isGradientState(this.style_.getNormal())
                || this.isGradientState(this.style_.getHover())
                || this.isGradientState(this.style_.getPushed())
                || this.isGradientState(this.style_.getSelectedNormal())
                || this.isGradientState(this.style_.getSelectedHover())
        );
    }

    return this.sprite_;
};
/**
 * @protected
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.initializePixValue = function () {
    if (!this.isValuePointsInitialized_) {
        var isClosed = true,
            numPoints = this.series_.getNumPoints();
        if (this.series_.isClosed) isClosed = this.series_.isClosed();

        if (this.index_ == 0)
            this.hasPrev_ = isClosed && !this.series_.getPointAt(numPoints - 1).isMissing();
        else
            this.hasPrev_ = (this.index_ > 0) &&
                (this.series_.getPointAt(this.index_ - 1)) &&
                (!this.series_.getPointAt(this.index_ - 1).isMissing() || !this.getPlot().needIgnoreMissingPoints());

        if (this.index_ == numPoints - 1)
            this.hasNext_ = isClosed && this.series_.getPointAt(0) && !this.series_.getPointAt(0).isMissing();
        else
            this.hasNext_ = (this.index_ < (numPoints - 1) ) &&
                (this.series_.getPointAt(this.index_ + 1) != null) &&
                (!this.series_.getPointAt(this.index_ + 1).isMissing() || !this.getPlot().needIgnoreMissingPoints());


        this.isLineDrawable_ = this.hasPrev_ || this.hasNext_;
        this.isValuePointsInitialized_ = true;
    }

    this.getPlot().transform(this.x_, this.stackValue_, this.pixValuePt_);
};
/**
 * @protected
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.createDrawingPoints = function () {
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 *
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.calcBounds = function () {
    this.initializePixValue();

    this.bounds_ = new anychart.utils.geom.Rectangle(
        this.pixValuePt_.x,
        this.pixValuePt_.y,
        0,
        0
    );

    if (this.isLineDrawable())
        this.createDrawingPoints();
};
/**
 * @param {anychart.styles.line.LineStyleState} state
 */
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.isGradientState = function (state) {
    return state.getStroke() &&
        state.getStroke().isEnabled() &&
        state.getStroke().getType() == anychart.visual.stroke.Stroke.StrokeType.GRADIENT;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.update = function () {
    this.calcBounds();
    goog.base(this, 'update');
};
//------------------------------------------------------------------------------
//                  Resize.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.series.line.LinePoint.prototype.resize = function () {
    this.calcBounds();
    goog.base(this, 'resize');
};
//------------------------------------------------------------------------------
//
//                          LineSeries class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.data.RadarPolarSeries}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries = function () {
    goog.base(this);
    this.needCreateGradientRect_ = false;
    this.isClosed_ = true;
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.LineSeries,
    anychart.plots.radarPolarPlot.data.RadarPolarSeries
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.isClosed_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.isClosed = function () {
    return this.isClosed_;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.gradientBounds_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.needCreateGradientRect_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.needCreateGradientRect = function () {
    return this.needCreateGradientRect_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.setNeedCreateGradientRect = function (value) {
    this.needCreateGradientRect_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.valueAxisStart_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.getValueAxisStart = function () {
    return this.valueAxisStart_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.minimum_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.maximum_ = null;
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.needCallOnBeforeDraw = function () {
    return true;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.lt_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.rb_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    this.deserializeClose(data);
};
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.deserializeClose = function (data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'close_contour'))
        this.isClosed_ = des.getBoolProp(data, 'close_contour');
};
/**
 * @return {anychart.plots.radarPolarPlot.series.line.LinePoint}
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.createPoint = function () {
    return  this.getPlot().usePolarCoords ?
        new anychart.plots.radarPolarPlot.series.line.PolarPoint() :
        new anychart.plots.radarPolarPlot.series.line.RadarPoint();
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 *
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.doGradientBounds_ = function () {
    if (!this.needCreateGradientRect()) return;

    this.lt_ = new anychart.utils.geom.Point(Number.MAX_VALUE, Number.MAX_VALUE);
    this.rb_ = new anychart.utils.geom.Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
    var point;

    for (var j = 0, pointsCount = this.points_.length; j < pointsCount; j++) {
        point = this.points_[j];
        if (point == null) continue;
        if (!point.isMissing()) {
            point.setNeedCreateDrawingPoints(true);
            point.setNeedInitializePixValues(true);

            point.execInitializePixValues();

            if (point.isLineDrawable())
                point.execCreateDrawingPoints();

            this.getRectPoints_(point);
            this.lt_.x = Math.min(point.getBottomEnd().x, point.getBottomStart().x, this.lt_.x);
            this.lt_.y = Math.min(point.getBottomEnd().y, point.getBottomStart().y, this.lt_.y);

            this.rb_.x = Math.max(point.getBottomEnd().x, point.getBottomStart().x, this.rb_.x);
            this.rb_.y = Math.max(point.getBottomEnd().y, point.getBottomStart().y, this.rb_.y);
        }
    }

    this.gradientBounds_ = new anychart.utils.geom.Rectangle(
        this.lt_.x,
        this.lt_.y,
        this.rb_.x - this.lt_.x,
        this.rb_.y - this.lt_.y
    );
};
/**
 * @private
 * @param {anychart.plots.radarPolarPlot.series.line.LinePoint} point
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.getRectPoints_ = function (point) {
    var drawPoints = point.getDrawingPoints();
    for (var i = 0, pointCount = drawPoints.length; i < pointCount; i++) {
        this.lt_.x = Math.min(drawPoints[i].getX(), this.lt_.x);
        this.lt_.y = Math.min(drawPoints[i].getY(), this.lt_.y);

        this.rb_.x = Math.max(drawPoints[i].getX(), this.rb_.x);
        this.rb_.y = Math.max(drawPoints[i].getY(), this.rb_.y);
    }
};
//------------------------------------------------------------------------------
//                  onBefore.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.onBeforeResize = function () {
    this.doGradientBounds_();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineSeries.prototype.onBeforeDraw = function () {
    this.valueAxisStart_ = this.getPlot().getYScale().getMinimum();
    this.doGradientBounds_();
};
//------------------------------------------------------------------------------
//
//                      LineGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings}
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings = function () {
    goog.base(this);
    this.shiftStepLine_ = false;
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.shiftStepLine_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'shift_step_line'))
        this.shiftStepLine_ = des.getBoolProp(data, 'shift_step_line');
};
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.createSeries = function () {
    var series = new anychart.plots.radarPolarPlot.series.line.LineSeries();
    series.setType(anychart.series.SeriesType.LINE);
    return series;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.createStyle = function () {
    return new anychart.plots.radarPolarPlot.series.line.styles.LineStyle();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'line_series';
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.LineGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'line_style';
};
//------------------------------------------------------------------------------
//
//                          PolarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.series.line.LinePoint}
 */
anychart.plots.radarPolarPlot.series.line.PolarPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.PolarPoint,
    anychart.plots.radarPolarPlot.series.line.LinePoint
);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.PolarPoint.prototype.createDrawingPoints = function () {
    this.drawingPoints_ = [];
    var pt1 = new anychart.plots.radarPolarPlot.utils.PolarPoint(this.getPlot().getCenter()),
        pt2 = new anychart.plots.radarPolarPlot.utils.PolarPoint(pt1.getCenter()),
        t1 = new anychart.utils.geom.Point(),
        plot = this.getPlot(),
        numPoints = this.series_.getNumPoints(),
        k = this.index_ == 0 ? numPoints : this.index_;

    if (this.hasPrev_) {
        this.series_.getPointAt(k - 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k - 1).getX(), this.series_.getPointAt(k - 1).getY(), t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt1.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt1.setPhi(plot.getXScale().transformValueToPixel(this.series_.getPointAt(k - 1).getX()) + plot.getYAxisCrossing());
        plot.transform(this.x_, this.y_, t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt2.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt2.setPhi(plot.getXScale().transformValueToPixel(this.x_) + plot.getYAxisCrossing());
        this.drawingPoints_.push(this.getPointBetween(pt1, pt2));
    }

    this.drawingPoints_.push(pt2);

    if (this.index_ == numPoints - 1) k = -1;
    else k = this.index_;

    if (this.hasNext_) {
        this.series_.getPointAt(k + 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k + 1).getX(), this.series_.getPointAt(k + 1).getY(), t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt1.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt1.setPhi(plot.getXScale().transformValueToPixel(this.series_.getPointAt(k + 1).getX()) + plot.getYAxisCrossing());
        plot.transform(this.x_, this.y_, t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt2.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt2.setPhi(plot.getXScale().transformValueToPixel(this.x_) + plot.getYAxisCrossing());
        this.drawingPoints_.push(this.getPointBetween(pt2, pt1));
    }
};

/**
 *
 * @param {anychart.plots.radarPolarPlot.utils.PolarPoint} pt1
 * @param {anychart.plots.radarPolarPlot.utils.PolarPoint} pt2
 */
anychart.plots.radarPolarPlot.series.line.PolarPoint.prototype.getPointBetween = function (pt1, pt2) {
    var res = new anychart.plots.radarPolarPlot.utils.PolarPoint(this.getPlot().getCenter());
    var tmpPt2 = pt2.getPhi();

    if (pt2.getPhi() < pt1.getPhi()) {
        tmpPt2 += 360;
    }

    if (tmpPt2 == pt1.getPhi()) {
        res.setPhi(tmpPt2);
        res.setRadius((pt1.getRadius() + pt2.getRadius()) / 2);
        return res;
    }

    res.setPhi((tmpPt2 + pt1.getPhi()) / 2);  // get angle between points
    res.setRadius((pt2.getRadius() - pt1.getRadius()) * (res.getPhi() - pt1.getPhi()) / (tmpPt2 - pt1.getPhi()) + pt1.getRadius());

    return res;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.series.line.PolarPoint.prototype.getPointPath = function () {
    var svgManager = this.getSVGManager(), pathData, i, pointCount;

    pathData = svgManager.pMove(this.drawingPoints_[0].getX(), this.drawingPoints_[0].getY());
    for (i = 1, pointCount = this.drawingPoints_.length; i < pointCount; i++)
        pathData += this.polarLine(svgManager, this.drawingPoints_[i - 1], this.drawingPoints_[i]);

    return pathData;
};

/**
 *
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.plots.radarPolarPlot.utils.PolarPoint} pt1
 * @param {anychart.plots.radarPolarPlot.utils.PolarPoint} pt2
 */
anychart.plots.radarPolarPlot.series.line.PolarPoint.prototype.polarLine = function (svgManager, pt1, pt2) {
    var dr = pt2.getRadius() - pt1.getRadius(),
        dphi = this.normalizeAngle(pt2.getPhi() - pt1.getPhi() + 360),
        tmp = new anychart.plots.radarPolarPlot.utils.PolarPoint(pt1.getCenter()),
        factor, polarLinePathData = '';

    tmp.setPhi(pt1.getPhi());
    factor = Math.min(Math.max(500 / Math.min(tmp.getX(), tmp.getY()), 0.1), 5);
    while (this.normalizeAngle(pt2.getPhi() + 360 - tmp.getPhi()) >= factor) {
        tmp.setRadius(dr * (tmp.getPhi() - pt1.getPhi()) / dphi + pt1.getRadius());
        polarLinePathData += svgManager.pLine(tmp.getX(), tmp.getY());
        tmp.setPhi(tmp.getPhi() + factor);
    }
    polarLinePathData += svgManager.pLine(pt2.getX(), pt2.getY());

    return  polarLinePathData;

};
anychart.plots.radarPolarPlot.series.line.PolarPoint.prototype.normalizeAngle = function (value) {
    return value - Math.floor(value / 360) * 360;
};
//------------------------------------------------------------------------------
//
//                          RadarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.series.line.LinePoint}
 */
anychart.plots.radarPolarPlot.series.line.RadarPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.RadarPoint,
    anychart.plots.radarPolarPlot.series.line.LinePoint
);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.line.RadarPoint.prototype.createDrawingPoints = function () {
    this.drawingPoints_ = [];
    var numPoints = this.series_.getNumPoints(),
        k = this.index_ == 0 ? numPoints : this.index_;

    if (this.hasPrev_) {
        this.series_.getPointAt(k - 1).initializePixValue();
        this.drawingPoints_.push(this.getPointBetween(this.series_.getPointAt(k - 1), this));
    }

    this.drawingPoints_.push(this.pixValuePt_);

    k = (this.index_ == numPoints - 1) ? -1 : this.index_;

    if (this.hasNext_) {
        this.series_.getPointAt(k + 1).initializePixValue();
        this.drawingPoints_.push(this.getPointBetween(this, this.series_.getPointAt(k + 1)));
    }
};
/**
 * @param {anychart.plots.radarPolarPlot.series.line.LinePoint} pt1
 * @param {anychart.plots.radarPolarPlot.series.line.LinePoint} pt2
 */
anychart.plots.radarPolarPlot.series.line.RadarPoint.prototype.getPointBetween = function (pt1, pt2) {
    var res = new anychart.utils.geom.Point(),
        resTmp = new anychart.utils.geom.Point();
    this.getPlot().transform((pt1.getX() + pt2.getX()) / 2, Math.max(pt1.getStackValue(), pt2.getStackValue()), resTmp);
    res.x = resTmp.x;
    res.y = resTmp.y;

    var x3 = res.x,
        y3 = res.y,

        x1 = pt1.getPixValuePt().x,
        y1 = pt1.getPixValuePt().y,

        x2 = pt2.getPixValuePt().x,
        y2 = pt2.getPixValuePt().y,

        x0 = this.getPlot().getCenter().x,
        y0 = this.getPlot().getCenter().y;

    if (x1 == x2 && y1 == y2) return res;

    if (x2 == x1) {
        res.x = x2;
        res.y = (( res.x - x0 ) / (x3 - x0)) * (y3 - y0) + y0;
        return res;
    }
    if (x0 == x3)
        res.x = x0;
    else
        res.x = ( (y1 - y0) + (x0 * (y3 - y0)) / (x3 - x0) - (x1 * (y2 - y1)) / (x2 - x1) ) / ( (y3 - y0) / (x3 - x0) - (y2 - y1) / (x2 - x1));
    res.y = (( res.x - x1 ) / (x2 - x1)) * (y2 - y1) + y1;

    return res;
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.line.RadarPoint.prototype.getPointPath = function () {
    var svgManager = this.getSVGManager(), i, pathData;

    pathData = svgManager.pMove(this.drawingPoints_[0].x, this.drawingPoints_[0].y);
    for (i = 1; i < this.drawingPoints_.length; i++)
        pathData += svgManager.pLine(this.drawingPoints_[i].x, this.drawingPoints_[i].y);

    return pathData;
};



