/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.series.area.styles');
//------------------------------------------------------------------------------
//
//                          AreaBorderStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.StrokeStyleShape}
 */
anychart.plots.radarPolarPlot.series.area.styles.AreaBorderStyleShape = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.styles.AreaBorderStyleShape,
    anychart.styles.background.StrokeStyleShape
);
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaBorderStyleShape.prototype.updatePath = function () {
    this.styleShapes_.updateAreaPath(this.element_);
};
//------------------------------------------------------------------------------
//
//                          AreaStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes,
    anychart.styles.background.BackgroundStyleShapes
);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.createShapes = function () {
    var style = this.point_.getStyle();

    if (style.needCreateStrokeShape()) {
        this.strokeShape_ = new anychart.plots.radarPolarPlot.series.area.styles.AreaBorderStyleShape();
        this.strokeShape_.initialize(this.point_, this);
    }

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.styles.background.FillStyleShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.styles.background.HatchFillStyleShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.placeShapes = function () {
    var sprite = this.getSprite();
    if (this.fillShape_) sprite.appendChild(this.fillShape_.getElement());
    if (this.strokeShape_) sprite.appendChild(this.strokeShape_.getElement());
    if (this.hatchFillShape_) sprite.appendChild(this.hatchFillShape_.getElement());
};
//------------------------------------------------------------------------------
//                  Update.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.update = function (opt_styleState, opt_updateData) {
    goog.base(this, 'update', opt_styleState, opt_updateData);
    if (this.strokeShape_) this.strokeShape_.update(opt_styleState, opt_updateData);
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.point_.getPointPathData());
};
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes.prototype.updateAreaPath = function (element) {
    element.setAttribute('d', this.point_.getPointPath());
};
//------------------------------------------------------------------------------
//
//                          AreaStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleState = function (style) {
    goog.base(this, style);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.styles.AreaStyleState,
    anychart.styles.background.BackgroundStyleState
);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.series.area.styles.AreaStyleState.prototype.deserializeBorder = function (data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'line')) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(des.getProp(data, 'line'));
    }
};
//------------------------------------------------------------------------------
//
//                          AreaStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.radarPolarPlot.series.area.styles.AreaStyle = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.styles.AreaStyle,
    anychart.styles.background.BackgroundStyle
);

/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.radarPolarPlot.series.area.styles.AreaStyleState;
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.styles.AreaStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.radarPolarPlot.series.area.styles.AreaStyleShapes();
};
