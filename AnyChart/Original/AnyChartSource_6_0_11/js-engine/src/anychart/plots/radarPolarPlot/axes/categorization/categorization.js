/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.categorization.StackSettings}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.categorization.Category}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.axes.categorization');
//------------------------------------------------------------------------------
//
//                          CategoryInfo class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo = function (plot) {
    this.plot_ = plot;
    this.points_ = [];
    this.initFormatting();
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.name_ = null;
/**
 * @param {String} value
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.setName = function (value) {
    this.name_ = value;
};
/**
 * @return {String}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getName = function () {
    return this.name_;
};

anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.index_ = NaN;
/**
 * @param {int} value
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.setIndex = function (value) {
    this.index_ = value;
};
/**
 * @return {int}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getIndex = function () {
    return this.index_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.color_ = 0;
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.RadarPlot}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.plot_ = null;
/**
 * @return {anychart.plots.radarPolarPlot.RadarPlot}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getPlot = function () {
    return this.plot_;
};
/**
 * @private
 * @type {Array}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.points_ = null;
/**
 * @return {Array}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getPoints = function () {
    return this.points_;
};
/**
 * Category tokens hash.
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.tokensHash_ = null;
/**
 * @return {anychart.formatting.TokensHash}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getTokensHash = function () {
    return this.tokensHash_;
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
/**
 * Init formatting for category.
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.initFormatting = function () {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
/**
 * Set default values for tokens.
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.setDefaultTokenValues = function () {
    this.tokensHash_.add('%CategoryName', '');
    this.tokensHash_.add('%Name', '');
    this.tokensHash_.add('%CategoryIndex', '');
    this.tokensHash_.add('%CategoryPointCount', 0);
    this.tokensHash_.add('%CategoryYSum', 0);
    this.tokensHash_.add('%CategoryYMax', 0);
    this.tokensHash_.add('%CategoryYMin', 0);
    this.tokensHash_.add('%CategoryYRangeMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%CategoryYRangeMin', Number.MAX_VALUE);
    this.tokensHash_.add('%CategoryYRangeSum', 0);
    this.tokensHash_.add('%CategoryYPercentOfTotal', 0);
    this.tokensHash_.add('%CategoryYAverage', 0);
};
/**
 * @param {String} token
 * @return {anychart.formatting.TextFormatTokenType}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getTokenType = function (token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokenType(token);
    switch (token) {
        case '%CategoryPointCount':
        case '%CategoryYSum':
        case '%CategoryYMax':
        case '%CategoryYMin':
        case '%CategoryYRangeMax':
        case '%CategoryYRangeMin':
        case '%CategoryYRangeSum':
        case '%CategoryYPercentOfTotal':
        case '%CategoryYAverage':
            return anychart.formatting.TextFormatTokenType.NUMBER;
        default:
        case '%CategoryName':
        case '%Name':
            return anychart.formatting.TextFormatTokenType.TEXT;
    }
};
/**
 * Calculate tokens values.
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.calculateCategoryTokenValues = function () {
    this.tokensHash_.add('%CategoryName', this.name_);
    this.tokensHash_.add('%Name', this.name_);
    this.tokensHash_.add('%CategoryIndex', this.index_);
};
/**
 * @param {String} token
 * @return {*}
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.getTokenValue = function (token) {
    if (this.tokensHash_.isDataPlotToken(token, this.getPlot()))
        return this.getPlot().getTokensHash().get(token);

    if (!this.tokensHash_.has('%CategoryYPercentOfTotal'))
        this.tokensHash_.add(
            '%CategoryYPercentOfTotal',
            this.tokensHash_.get('%CategoryYSum') /
                this.getPlot().getTokenValue('%DataPlotYSum') * 100);

    if (!this.tokensHash_.has('%CategoryYAverage'))
        this.tokensHash_.add(
            '%CategoryYAverage',
            this.tokensHash_.get('%CategoryYSum') /
                this.tokensHash_.get('%CategoryPointCount'));

    return this.tokensHash_.get(token);
};
//------------------------------------------------------------------------------
//                  Interactivity.
//------------------------------------------------------------------------------
/**
 * Set hover state for all category points.
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.setHover = function () {
    var pointCount = this.points_.length;
    for (var i = 0; i < pointCount; i++) this.points_[i].setHover();
};
/**
 * Set normal state for all category points.
 */
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.setNormal = function () {
    var pointCount = this.points_.length;
    for (var i = 0; i < pointCount; i++) this.points_[i].setNormal();
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo.prototype.serialize = function (opt_target) {
    if (opt_target == null || opt_target == undefined) opt_target = {};
    opt_target['Name'] = this.name_;
    opt_target['XSum'] = this.tokensHash_.get('%CategoryXSum');
    opt_target['YSum'] = this.tokensHash_.get('%CategoryYSum');
    opt_target['BubbleSizeSum'] = this.tokensHash_.get('%CategoryBubbleSizeSum');
    opt_target['YRangeMax'] = this.tokensHash_.get('%CategoryRangeMax');
    opt_target['YRangeMin'] = this.tokensHash_.get('%CategoryRangeMin');
    opt_target['YRangeSum'] = this.tokensHash_.get('%CategoryRangeSum');
    opt_target['YPercentOfTotal'] = this.tokensHash_.get('%CategoryYPercentOfTotal');
    opt_target['YMedian'] = this.tokensHash_.get('%CategoryYMedian');
    opt_target['YMode'] = this.tokensHash_.get('%CategoryYMode');
    opt_target['YAverage'] = this.tokensHash_.get('%CategoryYAverage');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          ClusterSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings = function () {
    this.axes_ = {};
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.groupPadding_ = NaN;
/**
 * @param {Number} value
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.setGroupPadding = function (value) {
    this.groupPadding_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.pointPadding_ = NaN;
/**
 * @param {Number} value
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.setPointPadding = function (value) {
    this.pointPadding_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.numClusters_ = 0;
/**
 * @param {int} value
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.setNumClusters = function (value) {
    this.numClusters_ = value;
};
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.getNumClusters = function () {
    return this.numClusters_;
};
/**
 * @type {Object.<String>}
 * @private
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.axes_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.clusterWidth_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.getClusterWidth = function () {
    return this.clusterWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.categoryWidth_ = NaN;

anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.calculate = function (categoryWidth) {
    this.categoryWidth_ = categoryWidth;
    this.clusterWidth_ = categoryWidth / (this.groupPadding_ + this.numClusters_ + this.pointPadding_ * (this.numClusters_ - 1));
};
/**
 * Return category offset by index.
 * @param {int} index
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.getOffset = function (index) {
    var offset = this.clusterWidth_ * (index * (1 + this.pointPadding_) + .5 + this.groupPadding_ / 2);
    return offset - this.categoryWidth_ / 2;
};

anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings.prototype.checkAxis = function (axis) {
    var index = this.numClusters_;

    if ((axis.getType() != anychart.plots.axesPlot.axes.AxisType.VALUE) || axis.getScale().getMode() == anychart.scales.ScaleMode.NORMAL) {
        this.numClusters_++;
        return index;
    }
    if (this.axes_[axis.getName()] != null)
        return this.axes_[axis.getName()];
    this.axes_[axis.getName()] = index;
    this.numClusters_++;
    return index;
};
//------------------------------------------------------------------------------
//
//                          StackSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings = function (isPercent) {
    this.isPercent_ = isPercent;
    this.positivePoints_ = [];
    this.negativePoints_ = [];
    this.points_ = [];
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.isPercent_ = false;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.positiveStackSumm_ = 0;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.negativeStackSumm_ = 0;
/**
 * @private
 * @type {Array}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.positivePoints_ = null;
/**
 * @private
 * @type {Array}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.negativePoints_ = null;
/**
 * @private
 * @type {Array}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.points_ = null;
/**
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.getPrevious = function (value) {
    if (value < 0) return this.negativeStackSumm_;
    return this.positiveStackSumm_;
};
/**
 * @param {} pt
 * @param value
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.addPoint = function (pt, value) {
    var isPrevPositive = true;
    if (value == 0 && this.points_.length > 0) {
        var prevIndex = this.points_.length - 1;
        var prevPt = this.points_[prevIndex];
        while (prevPt.stackValue_ == 0 && prevIndex > 0) {
            prevIndex--;
            prevPt = this.points_[prevIndex];
        }
        isPrevPositive = (prevPt.stackValue_ >= 0);
    }
    this.points_.push(pt);
    var isPositive = value > 0;
    if (value == 0)
        isPositive = isPrevPositive;

    if (isPositive) {
        if (this.positivePoints_.length > 0)
            pt.setPreviousStackPoint(this.positivePoints_[this.positivePoints_.length - 1]);
        this.positivePoints_.push(pt);
        this.positiveStackSumm_ += value;
        return this.positiveStackSumm_;
    }
    if (this.negativePoints_.length > 0)
        pt.setPreviousStackPoint(this.negativePoints_[this.negativePoints_.length - 1]);

    this.negativePoints_.push(pt);
    this.negativeStackSumm_ += value;
    return this.negativeStackSumm_;
};

anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.setPercent = function () {
    var i;
    for (i = 0; i < this.positivePoints_.length; i++)
        this.positivePoints_[i].setStackMax(this.positiveStackSumm_);
    for (i = 0; i < this.negativePoints_.length; i++)
        this.negativePoints_[i].setStackMax(this.negativeStackSumm_);
};
/**
 * Define, is passed point last in stack.
 * @param {anychart.plots.axesPlot.data.StackablePoint} point
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.isLastInStack = function (point) {
    if (this.negativePoints_.length > 0 && this.negativePoints_[this.negativePoints_.length - 1] == point) return true;
    return this.positivePoints_.length > 0 && this.positivePoints_[this.positivePoints_.length - 1] == point;
};
/**
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.categorization.StackSettings.prototype.getStackSum = function (value) {
    return this.isPercent_ ? 100 : ((value > 0) ? this.positiveStackSumm_ : this.negativeStackSumm_);
};
//------------------------------------------------------------------------------
//
//                          Category class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.radarPolarPlot.axes.categorization.Category = function (plot, index, name, axis) {
    goog.base(this, plot);
    this.index_ = index;
    this.name_ = name;
    this.axis_ = axis;

    this.stackSettings_ = {};
    this.percentStackSettingsList_ = [];
};
goog.inherits(
    anychart.plots.radarPolarPlot.axes.categorization.Category,
    anychart.plots.radarPolarPlot.axes.categorization.CategoryInfo
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings}
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.clusterSettings_ = null;
/**
 * @param {anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings} value
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.setClusterSettings = function (value) {
    this.clusterSettings_ = value;
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.categorization.ClusterSettings}
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.getClusterSettings = function () {
    return this.clusterSettings_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.categorization.StackSettings}
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.stackSettings_ = null;
/**
 * @private
 * @type {Array.<anychart.plots.radarPolarPlot.axes.categorization.StackSettings>}
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.percentStackSettingsList_ = null;
//------------------------------------------------------------------------------
//                  Stack settings.
//------------------------------------------------------------------------------
/**
 * @param {int} seriesType
 * @param {Boolean} isPercent
 * @return {anychart.plots.radarPolarPlot.axes.categorization.StackSettings}
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.getStackSettings = function (seriesType, isPercent) {
    if (this.stackSettings_ == null)
        this.stackSettings_ = {};
    if (this.stackSettings_[seriesType] == null) {
        this.stackSettings_[seriesType] = new anychart.plots.radarPolarPlot.axes.categorization.StackSettings(isPercent);
        if (isPercent)
            this.percentStackSettingsList_.push(this.stackSettings_[seriesType]);
    }

    return this.stackSettings_[seriesType];
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Called after data deserialize.
 */
anychart.plots.radarPolarPlot.axes.categorization.Category.prototype.onAfterDeserializeData = function () {
    if (this.percentStackSettingsList_ != null) {
        for (var i = 0, count = this.percentStackSettingsList_.length; i < count; i++)
            this.percentStackSettingsList_[i].setPercent();
        this.percentStackSettingsList_ = null;
    }
};
