/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.visual.background');
goog.require('anychart.visual.background');
//------------------------------------------------------------------------------
//
//                          RadarPolarBackground class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.background.ShapeBackground}
 * @param {anychart.plots.radarPolarPlot.RadarPolarPlot}
    */
anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground = function (plot) {
    goog.base(this);
    this.plot_ = plot;
};
goog.inherits(anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground, anychart.visual.background.ShapeBackground);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.RadarPolarPlot}
 */
anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground.prototype.plot_ = null;
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground.prototype.createElement = function (svgManager) {
    return svgManager.createPath();
};

anychart.plots.radarPolarPlot.visual.background.RadarPolarBackground.prototype.getElementPath = function (svgManager, bounds) {
    if (this.plot_.drawingModeCircle) {
        return svgManager.pCircle(
            bounds.x + bounds.width / 2,
            bounds.y + bounds.height / 2,
            Math.min(bounds.width, bounds.height) / 2);
    } else {
        var thickness = this.hasBorder() ? this.border_.getThickness() : 0;
        bounds.x += thickness;
        bounds.y += thickness;
        bounds.width -= thickness + thickness;
        bounds.height -= thickness + thickness;

        var crossingAngle = this.plot_.getYAxisCrossing(),
            scale = this.plot_.getXScale(),
            radius = Math.min(bounds.width, bounds.height) / 2,
            rad = radius + thickness,
            majIntervals = scale.getMajorIntervalCount(),
            tmp = scale.localTransform(scale.getMajorIntervalValue(0)) + crossingAngle,
            tmpNext, pathData;

        pathData = svgManager.pMove(
            anychart.utils.geom.DrawingUtils.getPointX(rad, tmp) + this.plot_.getCenter().x,
            anychart.utils.geom.DrawingUtils.getPointY(rad, tmp) + this.plot_.getCenter().y
        );


        for (var i = 0; i <= majIntervals; i++) {
            tmp = scale.localTransform(scale.getMajorIntervalValue(i)) + crossingAngle;
            tmpNext = scale.localTransform(scale.getMajorIntervalValue(i + 1)) + crossingAngle;
            pathData += svgManager.pLine(
                anychart.utils.geom.DrawingUtils.getPointX(rad, tmpNext) + this.plot_.getCenter().x,
                anychart.utils.geom.DrawingUtils.getPointY(rad, tmpNext) + this.plot_.getCenter().y
            );
        }

        pathData += svgManager.pFinalize();
        return pathData;
    }
};
