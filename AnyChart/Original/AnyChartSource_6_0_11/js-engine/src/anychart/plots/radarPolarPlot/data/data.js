/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.data.RadarPolarPoint}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.data.RadarPolarSeries}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.data');
//------------------------------------------------------------------------------
//
//                          RadarPolarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.data.RadarPolarPoint,
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.x_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.getX = function () {
    return this.x_;
};
/**
 * @param {Number} value
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setX = function (value) {
    this.x_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.stackValue_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.getStackValue = function () {
    return this.stackValue_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.needCreateDrawingPoints_ = null;
/**
 * @param {Boolean} value
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setNeedCreateDrawingPoints = function (value) {
    this.needCreateDrawingPoints_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.needInitializePixValues_ = null;
/**
 * @param {Boolean} value
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setNeedInitializePixValues = function (value) {
    this.needInitializePixValues_ = value;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.data.RadarPolarPoint}
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.previousStackPoint_ = null;
/**
 * @param {anychart.plots.radarPolarPlot.data.RadarPolarPoint} value
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setPreviousStackPoint = function(value) {
    this.previousStackPoint_ = value;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.deserialize = function (data) {
    this.checkMissing_(data);
    goog.base(this, 'deserialize', data);
    this.deserializeArgument_(data);
    this.deserializeValue_(data);
    this.getPlot().checkPoint(this);
    this.checkYValue();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.deserializeArgument_ = function (data) {
    var des = anychart.utils.deserialization;
    if (this.getPlot().getPlotType() == 'polar') {
        this.x_ = Number(this.getPlot().getXScale().deserializeValue(des.getProp(data, 'x')));
    } else {
        this.category_ = this.getPlot().xAxis_.getCategory(this.name_);
        this.x_ = this.category_.getIndex();
        this.clusterSettings_ = this.category_.getClusterSettings();
        this.index_ = this.paletteIndex_ = this.category_.getIndex();
    }
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.deserializeValue_ = function (data) {
    var des = anychart.utils.deserialization;
    this.y_ = this.getPlot().getYScale().deserializeValue(des.getProp(data, 'y'));

    if (!this.isMissing() && this.category_) {
        var axis = this.getPlot().yAxis;

        if (axis.getScale().getMode() == anychart.scales.ScaleMode.STACKED) {
            this.stackValue_ = this.category_.getStackSettings(this.series_.getType(), false).addPoint(this, this.y_);
        } else if (axis.getScale().getMode() == anychart.scales.ScaleMode.PERCENT_STACKED) {
            this.stackValue_ = this.category_.getStackSettings(this.series_.getType(), true).addPoint(this, this.y_);
        } else {
            this.stackValue_ = this.y_;
        }
    } else {
        this.stackValue_ = this.y_;
    }
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.deserializeYValue = function (data) {
};
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.checkYValue = function () {
    this.getPlot().checkYValue(this.stackValue_);
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.checkMissing_ = function (data) {
    var des = anychart.utils.deserialization;
    this.isMissing_ = !des.hasProp(data, 'y') || isNaN(des.getNumProp(data, 'y')) || des.getStringProp(data, 'y').replace(/^\s+|\s+$/g, "") == "";
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.execInitializePixValues = function () {
    this.initializePixValue();
};
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.execCreateDrawingPoints = function () {
    this.createDrawingPoints();
};
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.createDrawingPoints = goog.abstractMethod;
//------------------------------------------------------------------------------
//                  Categorization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.checkCategory = function (categoryTokens) {

};

anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setStackMax = function (value) {
    if (value != 0) this.stackValue_ *= 100 / value;
    else this.stackValue_ = 0;
};

//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.setDefaultTokenValues = function () {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%XValue', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.getTokenType = function (token) {
    if (token == '%XValue')
        return anychart.formatting.TextFormatTokenType.NUMBER;
    return goog.base(this, 'getTokenType', token);
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.data.RadarPolarPoint.prototype.calculatePointTokenValues = function () {
    goog.base(this, 'calculatePointTokenValues');
    this.tokensHash_.add('%XValue', this.x_);
};
//------------------------------------------------------------------------------
//
//                          RadarPolarSeries class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries}
 */
anychart.plots.radarPolarPlot.data.RadarPolarSeries = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.radarPolarPlot.data.RadarPolarSeries,
    anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries
);




