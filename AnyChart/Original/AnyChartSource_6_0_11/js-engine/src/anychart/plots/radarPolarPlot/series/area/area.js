/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.series.area.AreaSeries}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.area.PolarPoint}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.area.RadarPoint}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.series.area');
goog.require('anychart.plots.radarPolarPlot.series.line');
goog.require('anychart.plots.radarPolarPlot.series.area.styles');
goog.require('anychart.plots.radarPolarPlot.utils');
//------------------------------------------------------------------------------
//
//                          AreaSeries class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.series.line.LineSeries}
 */
anychart.plots.radarPolarPlot.series.area.AreaSeries = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.AreaSeries,
    anychart.plots.radarPolarPlot.series.line.LineSeries
);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaSeries.prototype.deserializeClose = function () {
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaSeries.prototype.createPoint = function () {
    return this.getPlot().usePolarCoords ?
        new anychart.plots.radarPolarPlot.series.area.PolarPoint() :
        new anychart.plots.radarPolarPlot.series.area.RadarPoint();
};
//------------------------------------------------------------------------------
//
//                          AreaGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings}
 */
anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings
);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings.prototype.createSeries = function () {
    var series = new anychart.plots.radarPolarPlot.series.area.AreaSeries();
    series.setType(anychart.series.SeriesType.AREA);
    return series;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings.prototype.createStyle = function () {
    return new anychart.plots.radarPolarPlot.series.area.styles.AreaStyle();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'area_series';
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.AreaGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'area_style';
};
//------------------------------------------------------------------------------
//
//                          PolarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.series.line.PolarPoint}
 */
anychart.plots.radarPolarPlot.series.area.PolarPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.PolarPoint,
    anychart.plots.radarPolarPlot.series.line.PolarPoint
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.utils.PolarPoint}
 */
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.polarBottomEnd_ = null;
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.utils.PolarPoint}
 */
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.polarBottomStart_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.createDrawingPoints = function () {
    var plot = this.getPlot(),
        pt1 = new anychart.plots.radarPolarPlot.utils.PolarPoint(plot.getCenter()),
        pt2 = new anychart.plots.radarPolarPlot.utils.PolarPoint(plot.getCenter()),
        t1 = new anychart.utils.geom.Point(),
        bottomValue = this.series_.getValueAxisStart(),
        k = this.index_ == 0 ? this.series_.getNumPoints() : this.index_;

    this.drawingPoints_ = [];
    this.polarBottomEnd_ = new anychart.plots.radarPolarPlot.utils.PolarPoint(plot.getCenter());
    this.polarBottomStart_ = new anychart.plots.radarPolarPlot.utils.PolarPoint(plot.getCenter());


    if (this.hasPrev_) {
        this.series_.getPointAt(k - 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k - 1).getX(), bottomValue, this.bottomStart_);
        plot.transform(this.series_.getPointAt(k - 1).getX(), this.series_.getPointAt(k - 1).getY(), t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt1.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt1.setPhi(plot.getXScale().transformValueToPixel(this.series_.getPointAt(k - 1).getX()) + plot.getYAxisCrossing());
        plot.transform(this.x_, this.y_, t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt2.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt2.setPhi(plot.getXScale().transformValueToPixel(this.x_) + plot.getYAxisCrossing());
        this.drawingPoints_.push(this.getPointBetween(pt1, pt2));
    } else plot.transform(this.x_, bottomValue, this.bottomStart_);

    this.polarBottomStart_.setRadius(Math.sqrt(Math.pow(this.bottomStart_.x - pt1.getCenter().x, 2) + Math.pow(this.bottomStart_.y - pt1.getCenter().y, 2)));
    this.polarBottomStart_.setPhi(plot.getXScale().transformValueToPixel(this.bottomStart_.x) + plot.getYAxisCrossing());
    this.drawingPoints_.push(pt2);

    k = this.index_ == this.series_.getNumPoints() - 1 ? -1 : this.index_;

    if (this.hasNext_) {
        this.series_.getPointAt(k + 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k + 1).getX(), bottomValue, this.bottomEnd_);
        plot.transform(this.series_.getPointAt(k + 1).getX(), this.series_.getPointAt(k + 1).getY(), t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt1.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt1.setPhi(plot.getXScale().transformValueToPixel(this.series_.getPointAt(k + 1).getX()) + plot.getYAxisCrossing());
        plot.transform(this.x_, this.y_, t1);
        t1.x -= pt1.getCenter().x;
        t1.y -= pt1.getCenter().y;
        pt2.setRadius(Math.sqrt(t1.x * t1.x + t1.y * t1.y));
        pt2.setPhi(plot.getXScale().transformValueToPixel(this.x_) + plot.getYAxisCrossing());
        this.drawingPoints_.push(this.getPointBetween(pt2, pt1));
    } else plot.transform(this.x_, bottomValue, this.bottomEnd_);

    this.polarBottomEnd_.setRadius(Math.sqrt(Math.pow(this.bottomEnd_.x - pt1.getCenter().x, 2) + Math.pow(this.bottomEnd_.y - pt1.getCenter().y, 2)));
    this.polarBottomEnd_.setPhi(plot.getXScale().transformValueToPixel(this.bottomEnd_.x) + plot.getYAxisCrossing());
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.isGradientState = function (state) {
    return state && state.hasGradient();
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.getPointPathData = function () {
    var i, svgManager = this.getSVGManager(), pathData, pointsCount;

    pathData = svgManager.pMove(this.drawingPoints_[0].getX(), this.drawingPoints_[0].getY());
    for (i = 1, pointsCount = this.drawingPoints_.length; i < pointsCount; i++)
        pathData += this.polarLine(svgManager, this.drawingPoints_[i - 1], this.drawingPoints_[i])

    if (this.previousStackPoint_ == null) {
        pathData += svgManager.pLine(this.polarBottomEnd_.getX(), this.polarBottomEnd_.getY());
        pathData += svgManager.pLine(this.polarBottomStart_.getX(), this.polarBottomStart_.getY());
    } else pathData += this.previousStackPoint_.getSegmentBackwardPathData();

    return pathData;
};
anychart.plots.radarPolarPlot.series.area.PolarPoint.prototype.getSegmentBackwardPathData = function () {
    var pathData = '', svgManager = this.point_.getSVGManager();
    if (this.drawingPoints_)
        for (var i = this.drawingPoints_.length - 1; i >= 1; i--)
            pathData += this.polarLine(svgManager, this.drawingPoints_[i], this.drawingPoints_[i - 1])

    return pathData;
};
//------------------------------------------------------------------------------
//
//                          RadarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.series.line.RadarPoint}
 */
anychart.plots.radarPolarPlot.series.area.RadarPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.area.RadarPoint,
    anychart.plots.radarPolarPlot.series.line.RadarPoint
);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.RadarPoint.prototype.createDrawingPoints = function () {
    var bottomValue = this.series_.getValueAxisStart(),
        plot = this.getPlot(),
        k = this.index_ == 0 ? this.series_.getNumPoints() : this.index_;

    this.drawingPoints_ = [];

    if (this.hasPrev_) {
        this.series_.getPointAt(k - 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k - 1).getX(), bottomValue, this.bottomStart_);
        this.drawingPoints_.push(this.getPointBetween(this.series_.getPointAt(k - 1), this));
    }
    else plot.transform(this.x_, bottomValue, this.bottomStart_);

    this.drawingPoints_.push(this.pixValuePt_);

    k = this.index_ == this.series_.getNumPoints() - 1 ? -1 : this.index_;

    if (this.hasNext_) {
        this.series_.getPointAt(k + 1).initializePixValue();
        plot.transform(this.series_.getPointAt(k + 1).getX(), bottomValue, this.bottomEnd_);
        this.drawingPoints_.push(this.getPointBetween(this, this.series_.getPointAt(k + 1)));
    } else plot.transform(this.x_, bottomValue, this.bottomEnd_);
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.RadarPoint.prototype.isGradientState = function (state) {
    return state && state.hasGradient();
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.series.area.RadarPoint.prototype.getPointPathData = function () {
    var svgManager = this.getSVGManager(), i, pathData, count;

    pathData = svgManager.pMove(this.drawingPoints_[0].x, this.drawingPoints_[0].y);
    for (i = 1, count = this.drawingPoints_.length; i < count; i++)
        pathData += svgManager.pLine(this.drawingPoints_[i].x, this.drawingPoints_[i].y);

    if (!this.previousStackPoint_) {
        pathData += svgManager.pLine(this.bottomEnd_.x, this.bottomEnd_.y);
        pathData += svgManager.pLine(this.bottomStart_.x, this.bottomStart_.y);
    } else pathData += this.previousStackPoint_.drawSegmentBackward(svgManager);

    return pathData
};

anychart.plots.radarPolarPlot.series.area.RadarPoint.prototype.drawSegmentBackward = function (svgManager) {
    var pathData = '';
    if (this.drawingPoints_)
        for (var i = this.drawingPoints_.length - 1; i >= 0; i--)
            pathData += svgManager.pLine(this.drawingPoints_[i].x, this.drawingPoints_[i].y);

    return pathData;
};

