/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.labels.Labels}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.labels.RotatedLabels}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.axes.labels');
goog.require('anychart.visual.text');
//------------------------------------------------------------------------------
//
//                          Labels class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.text.TextElement}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels = function (axis) {
    goog.base(this);
    this.axis_ = axis;
    this.space_ = 0;
    this.isTextScale_ = false;
    this.padding_ = 5;
    this.isInsideLabels_ = false;
    this.showFirst_ = true;
    this.showLast_ = true;
    this.allowOverlap_ = false;
};
goog.inherits(anychart.plots.radarPolarPlot.axes.labels.Labels,
    anychart.visual.text.TextElement);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.isInside_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.isInside = function () {
    return this.isInside_;
};
/**
 * @protected
 * @type {anychart.plots.radarPolarPlot.axes.Axis}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.axis_ = null;
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.textFormatter_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.showFirst_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.showFirst = function () {
    return this.showFirst_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.showLast_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.showLast = function () {
    return this.showLast_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.allowOverlap_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.allowOverlap = function () {
    return this.allowOverlap_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.space_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.getPadding = function () {
    return this.padding_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.tempValue_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);

    if (!this.enabled_) return;
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'format'))
        this.textFormatter_ = new anychart.formatting.TextFormatter(des.getStringProp(data, 'format'));

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getNumProp(data, 'padding');

    if (des.hasProp(data, 'allow_overlap'))
        this.allowOverlap_ = des.getBoolProp(data, 'allow_overlap');

    if (des.hasProp(data, 'position'))
        this.isInside_ = des.getEnumProp(data, 'position') == 'inside';

    if (des.hasProp(data, 'show_first_label'))
        this.showFirst_ = des.getBoolProp(data, 'show_first_label');

    if (des.hasProp(data, 'show_last_label'))
        this.showLast_ = des.getBoolProp(data, 'show_last_label');
};
//------------------------------------------------------------------------------
//                  Size.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.initSize = function (svgManager, majorIntervalIndex) {
    this.tempValue_ = this.axis_.getScale().getMajorIntervalValue(majorIntervalIndex);
    goog.base(this, 'initSize', svgManager, this.textFormatter_.getValue(this, null));
};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/**
 * Return token type by token.
 * @param {String} token
 * @return {anychart.formatting.TextFormatTokenType}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.getTokenType = function (token) {
    return this.axis_.getTokenType(token);
};
/**
 * Return token value by token.
 * @param {String} token
 * @return {String}
 */
anychart.plots.radarPolarPlot.axes.labels.Labels.prototype.getTokenValue = function (token) {
    return this.axis_.formatLabelValue(token, this.tempValue_);
};
//------------------------------------------------------------------------------
//
//                          RotatedLabels class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.axes.labels.Labels}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels = function (axis) {
    goog.base(this, axis);
    this.rotateCircular_ = true;
    this.autoOrientation_ = false;
    this.baseRotation_ = 0;
};
goog.inherits(
    anychart.plots.radarPolarPlot.axes.labels.RotatedLabels,
    anychart.plots.radarPolarPlot.axes.labels.Labels
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.rotateCircular_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.getRotateCircular = function () {
    return this.rotateCircular_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.autoOrientation_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.baseRotation_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.currentText_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    des.setProp(data, 'rotation', '0');
    switch (des.getEnumProp(data, 'circular_labels_style')) {
        default:
        case 'horizontal':
            this.rotateCircular_ = false;
            this.autoOrientation_ = false;
            break;
        case 'circular':
            this.rotateCircular_ = true;
            this.autoOrientation_ = true;
            break;
        case 'radial':
            this.rotateCircular_ = true;
            this.autoOrientation_ = true;
            this.baseRotation_ = 90;
            break;
    }
    goog.base(this, 'deserialize', data);
};
//------------------------------------------------------------------------------
//                  Init size.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.initSize = function (svgManager, text) {
    this.currentText_ = text;
    goog.base(this, 'initSize', svgManager, text);
};
//------------------------------------------------------------------------------
//                  Label position.
//------------------------------------------------------------------------------
/**
 * @param {Number} pixValue
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.getPosition = function (pixValue) {
    return this.rotateCircular_ ?
        this.getRotateCircularPosition(pixValue) :
        this.getFixedPosition(pixValue);
};
//------------------------------------------------------------------------------
//                  Fixed position.
//------------------------------------------------------------------------------
/**
 * @param {Number} pixValue
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.getFixedPosition = function (pixValue) {
    var anchorAngle = (pixValue + this.axis_.getPlot().getYAxisCrossing()) * Math.PI / 180,
        r = Math.min(this.axis_.getBounds().height, this.axis_.getBounds().width) / 2,
        insideTickmarksSpace = 0, outsideTickmarksSpace = 0,
        w = this.getNonRotatedBounds().width, h = this.getNonRotatedBounds().height,
        cos = Math.round(Math.cos(anchorAngle) * 100) / 100,
        sin = Math.round(Math.sin(anchorAngle) * 100) / 100,
        dx = -w / 2,
        dy = -h / 2,
        dr = 0,
        criticalSin = Math.abs(h) / ((2 / Math.sqrt(2)) * Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2)));
    criticalSin = Math.round(criticalSin * 100) / 100;

    if (this.axis_.hasMajorTickmarks()) {
        var size = this.axis_.getMajorTickmarks().getSize();
        if (this.axis_.getMajorTickmarks().isInside())
            insideTickmarksSpace += size;
        if (this.axis_.getMajorTickmarks().isOutside())
            outsideTickmarksSpace += size;
    }

    if (Math.abs(sin) < criticalSin) {
        if (cos != 0)
            dr += w / (2 * Math.abs(cos));
    } else {
        if (sin != 0)
            dr += h / (2 * Math.abs(sin));
    }

    if (this.isInside_) {
        r -= this.padding_ + dr + insideTickmarksSpace;
    } else {
        var r0 = r + this.padding_,
            r1 = r + this.padding_ + outsideTickmarksSpace,
            r2 = r + this.padding_ + dr + outsideTickmarksSpace,
            r0p2 = Math.pow(r0, 2),
            r1p2 = Math.pow(r1, 2),
            xOffset = r2 * Math.abs(cos) - Math.sqrt(r0p2 - r1p2 + r1p2 * Math.pow(cos, 2)),
            labelXOffset;

        if (cos > 0) {
            labelXOffset = w / 2 - xOffset;
            if (labelXOffset < w / 2 && labelXOffset > -w / 2) {
                dx += labelXOffset;
            }
        } else {
            labelXOffset = w / 2 - xOffset;
            if (labelXOffset < w / 2 && labelXOffset > -w / 2) {
                dx -= labelXOffset;
            }
        }

        r += this.padding_ + dr + outsideTickmarksSpace;
    }

    return  new anychart.utils.geom.Point(
        this.axis_.getCenter().x + r * cos + dx,
        this.axis_.getCenter().y + r * sin + dy
    );
};
//------------------------------------------------------------------------------
//                  Rotated position.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.setAngle = function (value) {
    if (this.rotateCircular_) {
        var invert = this.autoOrientation_ && Math.sin(value * Math.PI / 180) > 0;
        this.setRotationAngle((!invert) ? (-this.baseRotation_ + value + 90) : (-this.baseRotation_ + value - 90));
    }
};

anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.clearAngle = function () {
    if (this.rotateCircular_) {
        this.setRotationAngle(this.baseRotation_);
    }
};
anychart.plots.radarPolarPlot.axes.labels.RotatedLabels.prototype.getRotateCircularPosition = function (pixValue) {
    var anchorAngle = (pixValue + this.axis_.getPlot().getYAxisCrossing()),
        svgManager = this.axis_.getPlot().getSVGManager(),
        pos = this.axis_.getCenter().clone(),
        insideTickmarksSpace = (this.axis_.hasMajorTickmarks() && this.axis_.getMajorTickmarks().isInside()) ? this.axis_.getMajorTickmarks().getSize() : 0,
        outsideTickmarksSpace = (this.axis_.hasMajorTickmarks() && this.axis_.getMajorTickmarks().isOutside()) ? this.axis_.getMajorTickmarks().getSize() : 0,
        r = Math.min(this.axis_.getBounds().width, this.axis_.getBounds().height) / 2,
        angle = anchorAngle * Math.PI / 180,
        cos = Math.round(Math.cos(angle) * 1000) / 1000,
        sin = Math.round(Math.sin(angle) * 1000) / 1000,
        size, labelBounds;


    //get label size with base rotation
    this.clearAngle();
    this.initSize(svgManager, this.currentText_);
    size = this.getBounds().height;

    //get label bounds with required rotation
    this.setAngle(-anchorAngle);
    this.initSize(svgManager, this.currentText_);
    labelBounds = this.getBounds();


    if (this.isInside_) {
        r -= this.padding_ + size / 2 + insideTickmarksSpace;
    } else {
        r += this.padding_ + size / 2 + outsideTickmarksSpace;
    }

    pos.x += r * cos;
    pos.y += r * sin;

    pos.x -= labelBounds.width / 2;
    pos.y -= labelBounds.height / 2;

    return pos;
};




