/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.Axis}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.ValueAxis}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.RotatedAxis}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.CategorizedAxis}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.axes');
goog.require('anychart.plots.radarPolarPlot.scales');
goog.require('anychart.plots.radarPolarPlot.axes.grids');
goog.require('anychart.plots.radarPolarPlot.axes.tickmarks');
goog.require('anychart.plots.radarPolarPlot.axes.labels');
goog.require('anychart.plots.radarPolarPlot.axes.categorization');
//------------------------------------------------------------------------------
//
//                          Axis class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.plots.radarPolarPlot.RadarPolarPlot}
    */
anychart.plots.radarPolarPlot.axes.Axis = function (plot) {
    this.plot_ = plot;
    this.center_ = new anychart.utils.geom.Point();
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.RadarPolarPlot}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.plot_ = null;
/**
 * @return {anychart.plots.radarPolarPlot.RadarPolarPlot}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getPlot = function () {
    return this.plot_
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.enabled_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.isEnabled = function () {
    return this.enabled_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.grids.Grid}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.majorGrid_ = null;
/**
 * Define, has axis major grid.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasMajorGrid = function () {
    return this.majorGrid_ && this.majorGrid_.isEnabled();
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.grids.Grid}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getMajorGrid = function () {
    return this.majorGrid_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.grids.Grid}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.minorGrid_ = null;
/**
 * Define, has axis minor grid.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasMinorGrid = function () {
    return this.minorGrid_ && this.minorGrid_.isEnabled();
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.grids.Grid}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getMinorGrid = function () {
    return this.minorGrid_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.majorTickmarks_ = null;
/**
 * Define, has axis minor tickmarks.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasMajorTickmarks = function () {
    return this.majorTickmarks_ && this.majorTickmarks_.isEnabled();
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getMajorTickmarks = function () {
    return this.majorTickmarks_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.minorTickmarks_ = null;
/**
 * Define, has axis minor tickmarks.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasMinorTickmarks = function () {
    return this.minorTickmarks_ && this.minorTickmarks_.isEnabled();
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getMinorTickmarks = function () {
    return this.minorTickmarks_;
};
/**
 * @private
 * @type {anychart.plots.radarPolarPlot.axes.labels.Labels}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.labels_ = null;
/**
 * Define, has axis labels.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasLabels = function () {
    return this.labels_ && this.labels_.isEnabled();
};
/**
 * @return {anychart.plots.radarPolarPlot.axes.labels.Labels}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getLabels = function () {
    return this.labels_;
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.line_ = null;
/**
 * Define, has axis labels.
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.hasLine = function () {
    return this.line_ && this.line_.isEnabled();
};
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getLine = function () {
    return this.line_;
};
//------------------------------------------------------------------------------
//                  Visual properties.
//------------------------------------------------------------------------------
//TODO: Скорее всего ссылки на спрайты не нужны и только зря занимают память. Проверить после полного имплемента осей.
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.axesGridInterlacesSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.axesGridLinesSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.axesTickmarksSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.axesLabelsSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.axesLinesSprite_ = null;
/**
 * @private
 * @type {SVGPathElement}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.lineElement_ = null;
//------------------------------------------------------------------------------
//                  Bounds properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.scale_ = null;
/**
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getScale = function () {
    return this.scale_;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getBounds = function () {
    return this.bounds_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.center_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getCenter = function () {
    return this.center_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize axis settings.
 * @param {Object} data
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;

    this.enabled_ = des.getBoolProp(data, 'enabled');
    this.scale_ = this.createScale(des.hasProp(data, 'scale') ?
        des.getEnumProp(des.getProp(data, 'scale'), 'type') :
        null
    );

    if (des.isEnabledProp(data, 'scale'))
        this.scale_.deserialize(des.getProp(data, 'scale'));

    if (!this.enabled_) return;

    if (des.isEnabledProp(data, 'major_grid')) {
        this.majorGrid_ = new anychart.plots.radarPolarPlot.axes.grids.Grid();
        this.majorGrid_.deserialize(des.getProp(data, 'major_grid'));
    }

    if (des.isEnabledProp(data, 'minor_grid')) {
        this.minorGrid_ = new anychart.plots.radarPolarPlot.axes.grids.Grid();
        this.minorGrid_.deserialize(des.getProp(data, 'minor_grid'));
    }

    if (des.isEnabledProp(data, 'major_tickmark')) {
        this.majorTickmarks_ = new anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks();
        this.majorTickmarks_.deserialize(des.getProp(data, 'major_tickmark'));
    }

    if (des.isEnabledProp(data, 'minor_tickmark')) {
        this.minorTickmarks_ = new anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks();
        this.minorTickmarks_.deserialize(des.getProp(data, 'minor_tickmark'));
    }

    if (des.isEnabledProp(data, 'labels')) {
        this.labels_ = this.createLabels();
        this.labels_.deserialize(des.getProp(data, 'labels'));
    }

    if (des.isEnabledProp(data, 'line')) {
        this.line_ = new anychart.visual.stroke.Stroke();
        this.line_.deserialize(des.getProp(data, 'line'));
    }
};
/**
 * Create axis scale.
 * @protected
 * @return {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.createScale = function () {
    return new anychart.plots.radarPolarPlot.scales.LinearScale();
};
/**
 * Create axis labels.
 * @protected
 * @return {anychart.plots.radarPolarPlot.axes.labels.Labels}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.createLabels = function () {
    return new anychart.plots.radarPolarPlot.axes.labels.Labels(this);
};
/**
 * Called after plot data deserialize.
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.onAfterDeserializeData = function () {

};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize axis.
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.initialize = function (svgManager) {
    if (this.hasLine()) {
        this.lineElement_ = svgManager.createPath();
        this.axesLinesSprite_.appendChild(this.lineElement_);
    }
};
/**
 * Set containers for axis drawing.
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.setContainers = function (axesGridInterlacesFillingSprite, axesGridLinesSprite, axesTickmarksSprite, axesLabelsSprite, axesLinesSprite) {
    this.axesGridInterlacesSprite_ = axesGridInterlacesFillingSprite;
    this.axesGridLinesSprite_ = axesGridLinesSprite;
    this.axesTickmarksSprite_ = axesTickmarksSprite;
    this.axesLabelsSprite_ = axesLabelsSprite;
    this.axesLinesSprite_ = axesLinesSprite;
};
//------------------------------------------------------------------------------
//                  Bounds calculation.
//------------------------------------------------------------------------------
/**
 * Calculate axis bounds.
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.calcBounds = function (bounds) {
    this.bounds_ = bounds.clone();
    var thickness = this.plot_.getPlotOuterThickness();
    bounds.x += thickness;
    bounds.y += thickness;
    bounds.width -= thickness + thickness;
    bounds.height -= thickness + thickness;
    this.calcCenter_();
};
/**
 * Calculate axis center.
 * @private
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.calcCenter_ = function () {
    this.center_.x = this.bounds_.x + this.bounds_.width / 2;
    this.center_.y = this.bounds_.y + this.bounds_.height / 2;
};
/**
 * Calculate and return line bounds by line coords and thickness and sin/cos.
 * @param {Number} startX
 * @param {Number} startY
 * @param {Number} endX
 * @param {Number} endY
 * @param {Number} thickness
 * @param {Number} cos
 * @param {Number} sin
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.calcLineBounds = function (startX, startY, endX, endY, thickness, cos, sin) {
    var xThicknessHalf = Math.abs(thickness * cos / 2),
        yThicknessHalf = Math.abs(thickness * sin / 2);
    return new anychart.utils.geom.Rectangle(
        Math.min(startX, endX) - xThicknessHalf,
        Math.min(startY, endY) - yThicknessHalf,
        Math.abs(endX - startX) + xThicknessHalf + xThicknessHalf,
        Math.abs(endY - startY) + yThicknessHalf + yThicknessHalf
    );
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
/**
 * Draw axis ant it's elements.
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.draw = function () {
    if (this.hasLine()) this.drawLine();
    if (this.hasMajorGrid() || this.hasMinorGrid() || this.hasMajorTickmarks() || this.hasMinorTickmarks())
        this.drawGrid();
};
/**
 * Update major/minor axis grid and tickmarks elements paths.
 * @protected
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.drawGrid = function () {
    this.tempAxesGridInterlacesArray_ = [];
    var majIntervals = this.scale_.getMajorIntervalCount(),
        minIntervals = this.scale_.getMinorIntervalCount() - 1,
        majorOdd = true,
        i, majorInterval, pixValue, pixValueNext;

    for (i = 0; i <= majIntervals; i++) {
        majorInterval = this.scale_.getMajorIntervalValue(i);
        pixValue = this.scale_.localTransform(majorInterval);
        pixValueNext = this.scale_.localTransform(this.scale_.getMajorIntervalValue(i + 1));

        //draw major grid
        if (this.hasMajorGrid()) {
            majorOdd = !majorOdd;
            if (this.scale_.getMajorIntervalValue(i + 1) > this.scale_.getMaximum())
                pixValueNext = pixValue;

            this.drawGridInterlace(
                this.majorGrid_,
                pixValue,
                pixValueNext,
                majorOdd,
                (i == majIntervals && this.majorGrid_.drawLastLine()) || (i != majIntervals));
        }

        if (this.hasMajorTickmarks())
            this.drawTickmark(pixValue, this.majorTickmarks_);

        //draw minor grid
        if (i != majIntervals && (this.hasMinorGrid() || this.hasMinorTickmarks())) {
            var minorOdd = true,
                minorPixValue, minorPixValueNext, j;

            for (j = 0; j <= minIntervals; j++) {
                minorPixValue = this.scale_.localTransform(this.scale_.getMinorIntervalValue(majorInterval, j));
                minorPixValueNext = this.scale_.localTransform(this.scale_.getMinorIntervalValue(majorInterval, j + 1));

                if (this.hasMinorGrid()) {
                    minorOdd = !minorOdd;
                    this.drawGridInterlace(
                        this.minorGrid_,
                        minorPixValue,
                        minorPixValueNext,
                        minorOdd,
                        (j == minIntervals && this.minorGrid_.drawLastLine()) || (j != minIntervals),
                        pixValue,
                        pixValueNext,
                        true
                    );
                }

                if (this.hasMinorTickmarks()) {
                    this.drawTickmark(minorPixValue, this.minorTickmarks_);
                }
            }
        }

        if (this.needDrawLabel(i))
            this.drawLabel_(i);
    }

    for (i = this.tempAxesGridInterlacesArray_.length - 1; i >= 0; i--) {
        this.axesGridInterlacesSprite_.appendChild(this.tempAxesGridInterlacesArray_[i]);
    }
};
/**
 * @private
 * @param {int} majorIntervalIndex
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.drawLabel_ = function (majorIntervalIndex) {
    this.labels_.initSize(this.plot_.getSVGManager(), majorIntervalIndex);
    var pixValue = this.scale_.localTransform(this.scale_.getMajorIntervalValue(majorIntervalIndex)),
        pos = new anychart.utils.geom.Point(),
        leftTopCornerPoint = new anychart.utils.geom.Point();

    this.setLabelPosition(pos, pixValue, leftTopCornerPoint);
    if (this.labels_.allowOverlap() || !this.checkOverlap(pos, pixValue, leftTopCornerPoint))
        this.execDrawLabel(pos.x, pos.y, pixValue);
};
/**
 * Draw axes line.
 * @protected
 * @param {Number} pos
 * @param {Number} pixPos
 * @param {Number} leftTopCornerPos
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.setLabelPosition = goog.abstractMethod;
/**
 * @protected
 * @param {Number} x
 * @param {Number} y
 * @param {Number} pixValue
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.execDrawLabel = goog.abstractMethod;
/**
 * Draw axes line.
 * @protected
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.drawLine = goog.abstractMethod;
//------------------------------------------------------------------------------
//                  Grid interlaces drawing.
//------------------------------------------------------------------------------
/**
 * Draw axes grid line.
 * @protected
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.drawGridInterlace = goog.abstractMethod;
/**
 * Draw grid fill.
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {String} pathData
 * @param {anychart.visual.fill.Fill} fillSettings
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.execDrawFillGridLine = function (svgManager, pathData, fillSettings, bounds) {
    var element = svgManager.createPath(), style;
    element.setAttribute('d', pathData);
    style = fillSettings.getSVGFill(svgManager, bounds, null, false) + svgManager.getEmptySVGStroke();
    element.setAttribute('style', style);
    this.tempAxesGridInterlacesArray_.push(element);
};
/**
 * Draw grid hatch fill.
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {String} pathData
 * @param {anychart.visual.hatchFill.HatchFill} hatchFillSettings
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.execDrawHatchFillGridLine = function (svgManager, pathData, hatchFillSettings) {
    var element = svgManager.createPath();
    element.setAttribute('d', pathData);
    element.setAttribute('style', hatchFillSettings.getSVGHatchFill(svgManager, null, null));
    this.tempAxesGridInterlacesArray_.push(element);
};
/**
 * Update axis line path.
 * @protected
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.updateLinePath = goog.abstractMethod;
/**
 * Draw tickmarks.
 * @protected
 * @param {Number} pixValue
 * @param {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks} tickmarksSettings
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.drawTickmark = goog.abstractMethod;
/**
 * @protected
 * @param {int} interval
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.needDrawLabel = function (interval) {
    return this.labels_ &&
        this.labels_.isEnabled() &&
        (interval == 0 ? this.labels_.showFirst() : true) &&
        (interval == this.scale_.getMajorIntervalCount() ? this.labels_.showLast() : true);
};
//------------------------------------------------------------------------------
//                  Resize.
//------------------------------------------------------------------------------
/**
 * Resize axis.
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.resize = function () {
    if (this.hasLine()) this.updateLinePath();
    if (this.hasMajorGrid() || this.hasMinorGrid() || this.hasMajorTickmarks() || this.hasMinorTickmarks())
        this.drawGrid();

};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/**
 * @param {String} token
 * @param {Number} scaleValue
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.formatLabelValue = function (token, scaleValue) {
    return token == '%Value' ? scaleValue : '';
};
/**
 * Return token type by toke name.
 * @param {String} token
 * @return {anychart.formatting.TextFormatTokenType}
 */
anychart.plots.radarPolarPlot.axes.Axis.prototype.getTokenType = function (token) {
    return anychart.formatting.TextFormatTokenType.NUMBER;
};
//------------------------------------------------------------------------------
//
//                          ValueAxis class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.axes.Axis}
 * @param {anychart.plots.radarPolarPlot.RadarPolarPlot}
    */
anychart.plots.radarPolarPlot.axes.ValueAxis = function (plot) {
    goog.base(this, plot);
};
goog.inherits(anychart.plots.radarPolarPlot.axes.ValueAxis, anychart.plots.radarPolarPlot.axes.Axis);
//------------------------------------------------------------------------------
//                  Drawing properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.cos_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.sin_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.lineEndPoint_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.createScale = function (scaleType) {
    return scaleType != undefined && scaleType == 'logarithmic' ?
        new anychart.plots.radarPolarPlot.scales.LogScale() :
        new anychart.plots.radarPolarPlot.scales.LinearScale();
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.calcBounds = function (bounds) {
    goog.base(this, 'calcBounds', bounds);
    this.scale_.setPixelRange(0, Math.min(this.bounds_.height, this.bounds_.width) / 2);
    this.scale_.calculate();
};
/**
 * Initialize value axis angles.
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.initAngle = function () {
    var angleInRadians = this.plot_.getYAxisCrossing() * Math.PI / 180;
    this.cos_ = Math.round(Math.cos(angleInRadians) * 1e15) / 1e15;
    this.sin_ = Math.round(Math.sin(angleInRadians) * 1e15) / 1e15;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.calcValueAxisLineBounds = function () {
    this.calcLineEndPoint();
    return this.calcLineBounds(
        this.lineEndPoint_.x,
        this.lineEndPoint_.y,
        this.center_.x,
        this.center_.y,
        this.line_.getThickness(),
        this.sin_,
        this.cos_);
};
/**
 * Calculate line end point value.
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.calcLineEndPoint = function () {
    var radius = Math.min(this.bounds_.height, this.bounds_.width);
    this.lineEndPoint_.x = radius * this.cos_ / 2 + this.center_.x;
    this.lineEndPoint_.y = radius * this.sin_ / 2 + this.center_.y;
};
//------------------------------------------------------------------------------
//                  Labels drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.execDrawLabel = function (x, y, pixValue) {
    var sprite = this.labels_.createSVGText(this.plot_.getSVGManager(), null);
    sprite.setPosition(x, y);
    this.axesLabelsSprite_.appendSprite(sprite);
};
/**
 * @protected
 * @param {anychart.utils.geom.Point} pos
 * @param {Number} pixValue
 * @param {anychart.utils.geom.Point} leftTopCornerPos
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.checkOverlap = function (pos, pixValue, leftTopCornerPos) {
    var labelRotationAngle = this.getLabelRotation(pixValue);
    var corners = this.getAllPoints(
        this.labels_.getNonRotatedBounds(),
        labelRotationAngle,
        new anychart.utils.geom.Point()
    );
    for (var i = 0; i < 4; i++) {
        corners[i].x += leftTopCornerPos.x;
        corners[i].y += leftTopCornerPos.y;

    }
    var bounds = new anychart.utils.geom.Rectangle(
        pos.x,
        pos.y,
        this.labels_.getBounds().width,
        this.labels_.getBounds().height
    );

    if (this.lastRotatedBounds_ && this.lastNonRotatedLines_ &&
        this.lastRotatedBounds_.intersects(bounds) && this.intersects(this.lastNonRotatedLines_, corners))
        return true;
    else {
        this.lastRotatedBounds_ = bounds;
        this.lastNonRotatedLines_ = [
            anychart.utils.geom.LineEquation.getLineFrom2Points(corners[0], corners[1]),
            anychart.utils.geom.LineEquation.getLineFrom2Points(corners[1], corners[2]),
            anychart.utils.geom.LineEquation.getLineFrom2Points(corners[2], corners[3]),
            anychart.utils.geom.LineEquation.getLineFrom2Points(corners[3], corners[0])
        ];
        return false;
    }
};

anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.getLabelRotation = function (pixValue) {
    return this.labels_.getRotationAngle();
};
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.intersects = function (lines, rc2) {
    var line1,
        line2,
        fail;
    for (var i = 0; i < 2; i++) {
        line1 = lines[i];
        line2 = lines[i + 2];
        var b1 = line1.isLeftPoint(rc2[0]),
            b2 = line2.isLeftPoint(rc2[0]),
            b = b1;
        fail = false;
        for (var j = 1; j < 4; j++) {
            if (((b1 && b2) || (!b1 && !b2)) && b == b1) {
                b1 = line1.isLeftPoint(rc2[j]);
                b2 = line2.isLeftPoint(rc2[j]);
            } else {
                fail = true;
                break;
            }
        }
        if (!fail)
            return false;
    }
    return fail;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.setLabelPosition = function (pos, pixValue, leftTopCornerPos) {
    var padding = this.labels_.getPadding();
    if (this.hasMajorTickmarks() &&
        ((this.majorTickmarks_.isInside() && this.labels_.isInside()) ||
            (this.majorTickmarks_.isOutside() && !this.labels_.isInside())))
        padding += this.majorTickmarks_.getSize();

    var dx = padding * this.sin_,
        dy = padding * this.cos_,
        anchor = new anychart.utils.geom.Point(
            pixValue * this.cos_ + this.center_.x,
            pixValue * this.sin_ + this.center_.y),
        line;
    if (pixValue == 0) {
        var radius = Math.min(this.bounds_.height, this.bounds_.width) / 2;
        line = anychart.utils.geom.LineEquation.getLineFrom2Points(
            new anychart.utils.geom.Point(
                radius * this.cos_ + this.center_.x,
                radius * this.sin_ + this.center_.y),
            this.center_);
    } else
        line = anychart.utils.geom.LineEquation.getLineFrom2Points(anchor, this.center_);
    if (!this.labels_.isInside()) {
        anchor.x += dx;
        anchor.y -= dy;
    } else {
        anchor.x -= dx;
        anchor.y += dy;
    }

    var labelRotationAngle = this.labels_.getRotationAngle(),
        corners = this.getAllPoints(
            this.labels_.getNonRotatedBounds(),
            labelRotationAngle,
            new anychart.utils.geom.Point()
        ),
        nearestCorner;

    if (!this.labels_.isInside())
        if (this.sin_ * this.cos_ <= 0)
            nearestCorner = this.getNearestPoint(line, corners, this.sin_, this.cos_);
        else
            nearestCorner = this.getNearestPoint(line, corners, -this.sin_, -this.cos_);
    else if (this.sin_ * this.cos_ > 0)
        nearestCorner = this.getNearestPoint(line, corners, this.sin_, this.cos_);
    else
        nearestCorner = this.getNearestPoint(line, corners, -this.sin_, -this.cos_);

    if (!nearestCorner) nearestCorner = new anychart.utils.geom.Point();

    leftTopCornerPos.x = anchor.x - nearestCorner.x;
    leftTopCornerPos.y = anchor.y - nearestCorner.y;

    pos.x = Math.min(corners[0].x, corners[1].x, corners[2].x, corners[3].x) + leftTopCornerPos.x;
    pos.y = Math.min(corners[0].y, corners[1].y, corners[2].y, corners[3].y) + leftTopCornerPos.y;
};

anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.getAllPoints = function (nonRotatedBounds, rotationAngle, anchor) {
    var res = [];
    var dx = nonRotatedBounds.width;
    var dy = nonRotatedBounds.height;
    res[0] = new anychart.utils.geom.Point(anchor.x, anchor.y);
    res[1] = new anychart.utils.geom.Point(rotationAngle, dx);
    res[2] = new anychart.utils.geom.Point(rotationAngle + Math.atan2(dy, dx) * 180 / Math.PI, Math.sqrt(dx * dx + dy * dy));
    res[3] = new anychart.utils.geom.Point(rotationAngle + 90, dy);
    for (var i = 1; i < 4; i++) {
        var r = res[i].y;
        var a = res[i].x;
        res[i].x = anychart.utils.geom.DrawingUtils.getPointX(r, a) + anchor.x;
        res[i].y = anychart.utils.geom.DrawingUtils.getPointY(r, a) + anchor.y;
    }
    return res;
};

anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.getNearestPoint = function (line, points, sin, cos) {
    var findMax;
    if (sin == 0) findMax = (cos > 0);
    else findMax = (sin < 0);

    var nearest = [], foundDistance;
    if (findMax)
        foundDistance = -Number.MAX_VALUE;
    else
        foundDistance = Number.MAX_VALUE;

    for (var i = 0; i < 4; i++) {
        var distance = line.getDistance(points[i]);
        if (Math.abs(distance - foundDistance) < 1) {
            nearest.push(i);
        } else if ((findMax && distance > foundDistance) || ((!findMax) && distance < foundDistance)) {
            nearest = [i];
            foundDistance = distance;
        }
    }
    if (nearest.length == 0)
        return null;
    else if (nearest.length == 1)
        return points[nearest[0]];
    else     return  new anychart.utils.geom.Point(
            (points[nearest[0]].x + points[nearest[1]].x) / 2,
            (points[nearest[0]].y + points[nearest[1]].y) / 2
        );
};
//------------------------------------------------------------------------------
//                  Line Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.drawLine = function () {
    this.lineEndPoint_ = new anychart.utils.geom.Point();
    this.lineElement_.setAttribute('style', this.line_.getSVGStroke(
        this.plot_.getSVGManager(),
        this.calcValueAxisLineBounds())
    );
    this.updateLinePath();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.updateLinePath = function () {
    this.calcLineEndPoint();

    this.lineElement_.setAttribute('d',
        this.plot_.getSVGManager().pMove(this.center_.x, this.center_.y) +
            this.plot_.getSVGManager().pLine(this.lineEndPoint_.x, this.lineEndPoint_.y)
    );
};
//------------------------------------------------------------------------------
//                  Grid tickmarks drawing.
//------------------------------------------------------------------------------
/**
 * Draw grid tickmark
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.drawTickmark = function (pixValue, tickmarksSettings) {
    var x = pixValue * this.cos_ + this.center_.x,
        y = pixValue * this.sin_ + this.center_.y,
        size = tickmarksSettings.getSize(),
        dx = size * this.sin_,
        dy = size * this.cos_,
        svgManager = this.plot_.getSVGManager(),
        x1, y1, x2, y2, path;


    if (tickmarksSettings.isInside()) {
        x1 = x + dx;
        y1 = y - dy;
    } else {
        x1 = x;
        y1 = y;
    }
    if (tickmarksSettings.isOutside()) {
        x2 = x - dx;
        y2 = y + dy;
    } else {
        x2 = x;
        y2 = y;
    }

    path = svgManager.createPath();
    path.setAttribute('d', svgManager.pMove(x1, y1) + svgManager.pLine(x2, y2));
    path.setAttribute('style', tickmarksSettings.getSVGStroke(
        svgManager,
        this.calcLineBounds(x1, y1, x2, y2, tickmarksSettings.getThickness(), this.cos_, this.sin_),
        null,
        false
    ));

    this.axesTickmarksSprite_.appendChild(path);
};
//------------------------------------------------------------------------------
//                  Grid interlaces drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.drawGridInterlace = function (gridLine, pixValue, nextValue, odd, drawLine, opt_majPixValue, opt_majPixValueNext, opt_isMinor) {
    var svgManager = this.plot_.getSVGManager(),
        rc = new anychart.utils.geom.Rectangle(
            this.center_.x - nextValue,
            this.center_.y - nextValue,
            nextValue + nextValue,
            nextValue + nextValue
        ),
        pathData = this.getGridShapePathData(
            svgManager,
            this.center_.x,
            this.center_.y,
            pixValue,
            nextValue
        );

    if (gridLine.hasLine() && drawLine) {
        var path = svgManager.createPath();
        path.setAttribute(
            'style',
            gridLine.getLine().getSVGStroke(svgManager, rc, null, false) + svgManager.getEmptySVGFill()
        );
        path.setAttribute('d', pathData);
        this.axesGridLinesSprite_.appendChild(path);
    }

    if (gridLine.hasInterlaced() && pixValue != nextValue &&
        (!odd && (gridLine.hasEvenFill() || gridLine.hasEvenHatchFill())) ||
        (odd && (gridLine.hasOddFill() || gridLine.hasOddHatchFill()))) {

        if (odd) {
            if (gridLine.hasOddHatchFill()) {
                this.execDrawHatchFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getOddHatchFill()
                );
            }
            if (gridLine.hasOddFill()) {
                this.execDrawFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getOddFill(),
                    rc
                );
            }
        } else {
            if (gridLine.hasEvenHatchFill()) {
                this.execDrawHatchFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getEvenHatchFill()
                );
            }
            if (gridLine.hasEvenFill()) {
                this.execDrawFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getEvenFill(),
                    rc
                );
            }
        }
    }
};
/**
 * Create and return grid shape path data depend on plot drawing mode.
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} cx
 * @param {Number} this.center_y
 * @param {Number} pixValue
 * @param {Number} nextValue
 * @return {String}
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.getGridShapePathData = function (svgManager, cx, cy, pixValue, nextValue) {
    if (this.plot_.drawingModeCircle)
        return svgManager.pCircle(cx, cy, nextValue) + svgManager.pCircle(cx, cy, pixValue);
    else {
        var crossingAngle = this.plot_.getYAxisCrossing();
        var xScale = this.plot_.getXScale(),
            majIntervals = xScale.getMajorIntervalCount(),
            i = 0, majorInterval, tmp, tmpNext, nextMajorInterval, pathData;

        majorInterval = xScale.getMajorIntervalValue(i);
        tmp = xScale.localTransform(majorInterval) + crossingAngle;
        nextMajorInterval = xScale.getMajorIntervalValue(i + 1);
        tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;

        pathData = svgManager.pMove(
            anychart.utils.geom.DrawingUtils.getPointX(pixValue, tmp) + cx,
            anychart.utils.geom.DrawingUtils.getPointY(pixValue, tmp) + cy
        );

        for (i = 0; i < majIntervals; i++) {
            majorInterval = xScale.getMajorIntervalValue(i);
            tmp = xScale.localTransform(majorInterval) + crossingAngle;
            nextMajorInterval = xScale.getMajorIntervalValue(i + 1);
            tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;

            pathData += svgManager.pLine(
                anychart.utils.geom.DrawingUtils.getPointX(pixValue, tmpNext) + cx,
                anychart.utils.geom.DrawingUtils.getPointY(pixValue, tmpNext) + cy
            );
        }

        return pathData;
    }
};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/**
 * @param {String} token
 * @param {Number} scaleValue
 */
anychart.plots.radarPolarPlot.axes.ValueAxis.prototype.formatLabelValue = function (token, scaleValue) {
    return this.scale_.delinearize ? this.scale_.delinearize(scaleValue) : (scaleValue);
};
//------------------------------------------------------------------------------
//
//                          RotatedAxis class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.axes.Axis}
 * @param {anychart.plots.radarPolarPlot.RadarPolarPlot}
    */
anychart.plots.radarPolarPlot.axes.RotatedAxis = function (plot) {
    goog.base(this, plot);
};
goog.inherits(anychart.plots.radarPolarPlot.axes.RotatedAxis, anychart.plots.radarPolarPlot.axes.Axis);
//------------------------------------------------------------------------------
//                  Grid properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.angle1_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.x1_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.y1_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.angle2_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.x2_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.y2_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    this.scale_.setPixelRange(0, 360);
    this.scale_.calculate();
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    if (this.hasLine()) {
        this.line_.setCaps(anychart.visual.stroke.Stroke.CapsType.NONE);
        this.line_.setJoints(anychart.visual.stroke.Stroke.JoinType.MITER);
    }
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.createScale = function () {
    var scale = goog.base(this, 'createScale');
    scale.setMaximumOffset(0);
    return scale;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.createLabels = function () {
    return  new anychart.plots.radarPolarPlot.axes.labels.RotatedLabels(this);
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.calcBounds = function (bounds) {
    goog.base(this, 'calcBounds', bounds);
    this.scale_.calculate();
};
//------------------------------------------------------------------------------
//                  Grid line drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.drawLine = function () {
    var dim = Math.min(this.bounds_.height, this.bounds_.width) / 2,
        thickness = this.line_.getThickness() / 2,
        bounds = new anychart.utils.geom.Rectangle(
            this.center_.x - dim + thickness,
            this.center_.y - dim + thickness,
            dim + dim - thickness - thickness,
            dim + dim - thickness - thickness
        );

    var style = this.line_.getSVGStroke(this.plot_.getSVGManager(), bounds, null);
    style += this.plot_.getSVGManager().getEmptySVGFill();
    this.lineElement_.setAttribute('style', style);

    this.updateLinePath();
};
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.updateLinePath = function () {
    var cx = this.center_.x,
        cy = this.center_.y,
        dim = Math.min(this.bounds_.height, this.bounds_.width) / 2,
        thickness = this.line_.getThickness() / 2,
        crossingAngle = this.plot_.getYAxisCrossing(),
        radius = dim - thickness,
        majIntervals = this.scale_.getMajorIntervalCount(),
        minIntervals = this.scale_.getMinorIntervalCount(),
        hasMajorGrid = this.hasMajorGrid() && this.getMajorGrid().hasInterlaced(),
        hasMinorGrid = this.hasMinorGrid() && this.getMinorGrid().hasInterlaced(),
        svgManager = this.plot_.getSVGManager(),
        pathData = '';

    if (hasMajorGrid) {
        for (var i = 0; i < majIntervals; i++) {
            var majorInterval = this.scale_.getMajorIntervalValue(i),
                majorPixValue = this.scale_.localTransform(majorInterval),
                nextMajorInterval = this.scale_.getMajorIntervalValue(i + 1),
                nextMajorPixValue = this.scale_.localTransform(nextMajorInterval);


            if (hasMinorGrid) {
                for (var j = 0; j < minIntervals; j++) {
                    var minorPixValue = this.scale_.localTransform(this.scale_.getMinorIntervalValue(majorInterval, j)),
                        minorNextPixValue = this.scale_.localTransform(this.scale_.getMinorIntervalValue(majorInterval, j + 1));

                    pathData += svgManager.pMove(
                        cx + anychart.utils.geom.DrawingUtils.getPointX(radius, minorPixValue + crossingAngle),
                        cy + anychart.utils.geom.DrawingUtils.getPointY(radius, minorPixValue + crossingAngle)
                    );
                    pathData += svgManager.pCircleArc(
                        radius,
                        false,
                        true,
                        cx + anychart.utils.geom.DrawingUtils.getPointX(radius, minorNextPixValue + crossingAngle),
                        cy + anychart.utils.geom.DrawingUtils.getPointY(radius, minorNextPixValue + crossingAngle)
                    );
                }
            } else {
                pathData += svgManager.pMove(
                    cx + anychart.utils.geom.DrawingUtils.getPointX(radius, majorPixValue + crossingAngle),
                    cy + anychart.utils.geom.DrawingUtils.getPointY(radius, majorPixValue + crossingAngle)
                );
                pathData += svgManager.pCircleArc(
                    radius,
                    false,
                    true,
                    cx + anychart.utils.geom.DrawingUtils.getPointX(radius, nextMajorPixValue + crossingAngle),
                    cy + anychart.utils.geom.DrawingUtils.getPointY(radius, nextMajorPixValue + crossingAngle)
                );
            }
        }
    } else pathData = svgManager.pCircle(cx, cy, radius);

    this.lineElement_.setAttribute('d', pathData);
};
//------------------------------------------------------------------------------
//                  Grid interlace drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.drawGridInterlace = function (gridLine, pixValue, nextValue, odd, drawLine, opt_majPixValue, opt_majPixValueNext, opt_isMinor) {
    //defaults
    opt_majPixValue = (opt_majPixValue == undefined || opt_majPixValue == null) ? 0 : opt_majPixValue;
    opt_majPixValueNext = (opt_majPixValueNext == undefined || opt_majPixValueNext == null) ? 0 : opt_majPixValueNext;
    opt_isMinor = (opt_isMinor == undefined || opt_isMinor == null) ? false : opt_isMinor;

    //prepare grid sector properties
    var cx = this.center_.x, cy = this.center_.y,
        radius = Math.min(this.bounds_.height, this.bounds_.width) / 2,
        plotBounds = this.plot_.getBounds(),
        svgManager = this.plot_.getSVGManager(),
        rc, pathData;

    if (this.hasLine()) radius -= this.line_.getThickness();
    this.getGridSectorParams(pixValue, nextValue, radius, cx, cy, opt_isMinor, opt_majPixValue, opt_majPixValueNext);

    if (gridLine.hasLine() && drawLine) {
        var path = svgManager.createPath();
        pathData = svgManager.pMove(cx, cy);
        pathData += svgManager.pLine(this.x1_, this.y1_);
        path.setAttribute('d', pathData);
        path.setAttribute('style', gridLine.getLine().getSVGStroke(svgManager, null, null, false));
        this.axesGridLinesSprite_.appendChild(path);
    }

    if (gridLine.hasInterlaced() && pixValue != nextValue &&
        (!odd && (gridLine.hasEvenFill() || gridLine.hasEvenFill())) ||
        (odd && (gridLine.hasOddFill() || gridLine.hasOddHatchFill()))) {

        pathData = this.getGridShapePathData(svgManager, cx, cy, radius);

        if (odd) {
            if (gridLine.hasOddHatchFill()) {
                this.execDrawHatchFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getOddHatchFill()
                );
            }
            if (gridLine.hasOddFill()) {
                this.execDrawFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getOddFill(),
                    plotBounds
                );
            }
        } else {
            if (gridLine.hasEvenHatchFill()) {
                this.execDrawHatchFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getEvenHatchFill()
                );
            }
            if (gridLine.hasEvenFill()) {
                this.execDrawFillGridLine(
                    svgManager,
                    pathData,
                    gridLine.getEvenFill(),
                    plotBounds
                );
            }
        }
    }
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.getGridShapePathData = function (svgManager, cx, cy, radius) {
    var pathData = svgManager.pMove(cx, cy);
    pathData += svgManager.pLine(this.x1_, this.y1_);
    pathData += svgManager.pCircleArc(radius, false, true, this.x2_, this.y2_);
    pathData += svgManager.pLine(cx, cy);
    pathData += svgManager.pFinalize();
    return pathData
};
/**
 * @param {Array.<anychart.utils.geom.Point>} array
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.countBoundsByArray = function (array) {
    var len = array.length;
    if (len == 0) return null;
    var point = array[0];
    var res = new anychart.utils.geom.Rectangle(point.x, point.y);
    for (var i = 1; i < len; i++) {
        point = array[1];
        var x = res.x, y = res.y;
        res.x = Math.min(x, point.x);
        res.y = Math.min(y, point.y);
        res.width = Math.max(x + res.width, point.x) - res.x;
        res.height = Math.max(y + res.height, point.y) - res.y;
    }
    return res;
};
/**
 * @protected
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.getGridSectorParams = function (pixValue, nextValue, radius, cx, cy, minorGrid, majPixValue, majPixValueNext) {
    var angle1 = (pixValue + this.plot_.getYAxisCrossing()), angle2;
    this.x1_ = cx + anychart.utils.geom.DrawingUtils.getPointX(radius, angle1);
    this.y1_ = cy + anychart.utils.geom.DrawingUtils.getPointY(radius, angle1);

    if (pixValue == nextValue) {
        angle2 = angle1;
        this.x2_ = this.x1_;
        this.y2_ = this.y1_;
    } else {
        angle2 = (nextValue + this.plot_.getYAxisCrossing());
        this.x2_ = cx + anychart.utils.geom.DrawingUtils.getPointX(radius, angle2);
        this.y2_ = cy + anychart.utils.geom.DrawingUtils.getPointY(radius, angle2);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.drawTickmark = function (pixValue, tickmark) {
    var dim = Math.min(this.bounds_.height, this.bounds_.width) / 2;
    var p1 = tickmark.isInside() ? dim - tickmark.getSize() : dim,
        p2 = tickmark.isOutside() ? dim + tickmark.getSize() : dim,
        angle = (pixValue + this.plot_.getYAxisCrossing()),
        sin = anychart.utils.geom.DrawingUtils.getPointY(1, angle),
        cos = anychart.utils.geom.DrawingUtils.getPointX(1, angle),
        x1 = anychart.utils.geom.DrawingUtils.getPointX(p1, angle) + this.center_.x,
        y1 = anychart.utils.geom.DrawingUtils.getPointY(p1, angle) + this.center_.y,
        x2 = anychart.utils.geom.DrawingUtils.getPointX(p2, angle) + this.center_.x,
        y2 = anychart.utils.geom.DrawingUtils.getPointY(p2, angle) + this.center_.y,
        rc = this.calcLineBounds(x1, y1, x2, y2, tickmark.getThickness(), sin, cos),
        svgManager = this.plot_.getSVGManager(),
        path = svgManager.createPath();

    path.setAttribute('d', svgManager.pMove(x1, y1) + svgManager.pLine(x2, y2));
    path.setAttribute('style', tickmark.getSVGStroke(
        svgManager,
        rc,
        null,
        false
    ));
    this.axesTickmarksSprite_.appendChild(path);
};
//------------------------------------------------------------------------------
//                  Labels drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.setLabelPosition = function (pos, pixPos, leftTopCornerPos) {
    var tempPos = this.labels_.getPosition(pixPos);
    pos.x = tempPos.x;
    pos.y = tempPos.y;
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.execDrawLabel = function (x, y, pixValue) {
    var sprite = this.labels_.createSVGText(this.plot_.getSVGManager(), null);
    sprite.setPosition(x, y);
    this.axesLabelsSprite_.appendSprite(sprite);
};
anychart.plots.radarPolarPlot.axes.RotatedAxis.prototype.needDrawLabel = function (majorInterval) {
    if (majorInterval == this.scale_.getMajorIntervalCount()) return false;
    return  goog.base(this, 'needDrawLabel', majorInterval);
};
//------------------------------------------------------------------------------
//
//                          CategorizedAxis class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.axes.RotatedAxis}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis = function (plot) {
    goog.base(this, plot);
    this.categoriesList_ = [];
    this.categoriesMap_ = {};
};
goog.inherits(
    anychart.plots.radarPolarPlot.axes.CategorizedAxis,
    anychart.plots.radarPolarPlot.axes.RotatedAxis
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<anychart.plots.radarPolarPlot.axes.categorization.Category>}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.categoriesList_ = null;
/**
 * @private
 * @type {Object.<String>}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.categoriesMap_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majPixValue1_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majAngle1_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majX1_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majY1_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majPixValue2_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majAngle2_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majX2_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.majY2_ = NaN;
/**
 * @private
 * @type {anychart.utils.geom.LineEquation}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.equation_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.createScale = function () {
    var scale = new anychart.plots.radarPolarPlot.scales.TextScale();
    scale.setMaximumOffset(0);
    return scale;
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.onAfterDeserializeData = function () {
    for (var i = 0, count = this.categoriesList_.length; i < count; i++) {
        this.categoriesList_[i].onAfterDeserializeData();
    }
};
//------------------------------------------------------------------------------
//                  Categorization.
//------------------------------------------------------------------------------
/**
 * Create category by name.
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.createCategory = function (name) {
    var category = new anychart.plots.radarPolarPlot.axes.categorization.Category(
        this.plot_,
        this.categoriesList_.length,
        name,
        this
    );
    this.plot_.getXCategories().push(category);
    this.categoriesList_.push(category);
    this.categoriesMap_[name] = category;
    return category;
};
/**
 * Create category by name.
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.getCategory = function (name) {
    var category = this.categoriesMap_[name];
    if (!category) category = this.createCategory(name);
    return category;
};
/**
 * @param {int} index
 * @return {anychart.plots.radarPolarPlot.axes.categorization.Category}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.getCategoryByIndex = function (index) {
    return this.categoriesList_[index];
};
/**
 * @param {String} name
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.hasCategory = function (name) {
    return this.categoriesMap_[name] != null;
};
//------------------------------------------------------------------------------
//                  Line drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.updateLinePath = function () {
    if (this.plot_.drawingModeCircle) goog.base(this, 'updateLinePath');
    else {
        var dim = Math.min(this.bounds_.height, this.bounds_.width) / 2,
            thickness = this.line_.getThickness() / 2,
            crossingAngle = this.plot_.getYAxisCrossing(),
            radius = dim - thickness,
            svgManager = this.plot_.getSVGManager(),
            majIntervals = this.scale_.getMajorIntervalCount(), pathData;

        pathData = svgManager.pMove(
            anychart.utils.geom.DrawingUtils.getPointX(
                radius,
                this.scale_.localTransform(this.scale_.getMajorIntervalValue(0)) + crossingAngle) + this.center_.x,
            anychart.utils.geom.DrawingUtils.getPointY(
                radius,
                this.scale_.localTransform(this.scale_.getMajorIntervalValue(0)) + crossingAngle) + this.center_.y
        );

        for (var i = 0; i <= majIntervals; i++) {
            var nextMajorInterval = this.scale_.getMajorIntervalValue(i);
            var tmpNext = this.scale_.localTransform(nextMajorInterval) + crossingAngle;
            pathData += svgManager.pLine(
                anychart.utils.geom.DrawingUtils.getPointX(radius, tmpNext) + this.center_.x,
                anychart.utils.geom.DrawingUtils.getPointY(radius, tmpNext) + this.center_.y
            );
        }
        this.lineElement_.setAttribute('d', pathData);
    }
};


//------------------------------------------------------------------------------
//                  Grid drawing.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.getGridShapePathData = function (svgManager, cx, cy, radius) {
    if (this.plot_.drawingModeCircle) {
        return goog.base(this, 'getGridShapePathData', svgManager, cx, cy, radius);
    } else {
        var pathData = svgManager.pMove(cx, cy);
        pathData += svgManager.pLine(this.x1_, this.y1_);
        pathData += svgManager.pLine(this.x2_, this.y2_);
        pathData += svgManager.pLine(cx, cy);
        return pathData;
    }
};
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.formatLabelValue = function (token, scaleValue) {
    return token == '%Value' ? this.categoriesList_[scaleValue].getName() : '';
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.axes.CategorizedAxis.prototype.getTokenType = function (token) {
    return anychart.formatting.TextFormatTokenType.TEXT;

};



