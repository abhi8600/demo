/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.grids.Grid}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.axes.grids');
//------------------------------------------------------------------------------
//
//                          Grid class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.radarPolarPlot.axes.grids.Grid = function () {
    this.enabled_ = true;
    this.interlaced_ = false;
    this.drawLastLine_ = true;
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.enabled_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.isEnabled = function () {
    return this.enabled_;
};
/**
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.interlaced_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasInterlaced = function () {
    return this.interlaced_;
};
/**
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.drawLastLine_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.drawLastLine = function () {
    return this.drawLastLine_;
};
/**
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.line_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.getLine = function () {
    return this.line_;
};
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasLine = function () {
    return this.line_ && this.line_.isEnabled();
};
/**
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.evenFill_ = null;
/**
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.getEvenFill = function () {
    return this.evenFill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasEvenFill = function () {
    return this.evenFill_ && this.evenFill_.isEnabled();
};
/**
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.evenHatchFill_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.getEvenHatchFill = function () {
    return this.evenHatchFill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasEvenHatchFill = function () {
    return this.evenHatchFill_ && this.evenHatchFill_.isEnabled();
};
/**
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.oddFill_ = null;
/**
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.getOddFill = function () {
    return this.oddFill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasOddFill = function () {
    return this.oddFill_ && this.oddFill_.isEnabled();
};
/**
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.oddHatchFill_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.getOddHatchFill = function () {
    return this.oddHatchFill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.hasOddHatchFill = function () {
    return this.oddHatchFill_ && this.oddHatchFill_.isEnabled();
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize grid settings from data object.
 * @param {Object} data
 */
anychart.plots.radarPolarPlot.axes.grids.Grid.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;

    this.enabled_ = des.isEnabledProp(data, 'enabled');
    if (!this.enabled_) return;

    if (des.hasProp(data, 'draw_last_line'))
        this.drawLastLine_ = des.getBoolProp(data, 'draw_last_line');

    if (des.hasProp(data, 'interlaced'))
        this.interlaced_ = des.getBoolProp(data, 'interlaced');


    if (des.isEnabledProp(data, 'line')) {
        this.line_ = new anychart.visual.stroke.Stroke();
        this.line_.deserialize(des.getProp(data, 'line'));
    }

    if (this.interlaced_ && des.hasProp(data, 'interlaced_fills')) {
        var interlacedFills = des.getProp(data, 'interlaced_fills');

        if (des.hasProp(interlacedFills, 'even')) {
            var even = des.getProp(interlacedFills, 'even');

            if (des.isEnabledProp(even, 'fill')) {
                this.evenFill_ = new anychart.visual.fill.Fill();
                this.evenFill_.deserialize(des.getProp(even, 'fill'));
            }

            if (des.isEnabledProp(even, 'hatch_fill')) {
                this.evenHatchFill_ = new anychart.visual.hatchFill.HatchFill();
                this.evenHatchFill_.deserialize(des.getProp(even, 'hatch_fill'));
            }
        }

        if (des.hasProp(interlacedFills, 'odd')) {
            var odd = des.getProp(interlacedFills, 'odd');

            if (des.isEnabledProp(odd, 'fill')) {
                this.oddFill_ = new anychart.visual.fill.Fill();
                this.oddFill_.deserialize(des.getProp(odd, 'fill'));
            }

            if (des.isEnabledProp(odd, 'hatch_fill')) {
                this.oddHatchFill_ = new anychart.visual.hatchFill.HatchFill();
                this.oddHatchFill_.deserialize(des.getProp(odd, 'hatch_fill'));
            }
        }
    }
};

