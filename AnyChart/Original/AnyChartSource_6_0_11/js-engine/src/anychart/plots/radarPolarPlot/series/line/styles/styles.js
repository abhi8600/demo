/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.styles.LineStyleShape}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.series.line.styles.LineStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.series.line.styles');
//------------------------------------------------------------------------------
//
//                          LineStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyleShape}
 */
anychart.plots.radarPolarPlot.series.line.styles.LineStyleShape = function() {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.styles.LineStyleShape,
    anychart.styles.line.LineStyleShape
);
anychart.plots.radarPolarPlot.series.line.styles.LineStyleShape.prototype.updatePath = function() {
    this.element_.setAttribute('d', this.point_.getPointPath());
};
//------------------------------------------------------------------------------
//
//                          LineStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyleShapes}
 */
anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes = function() {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes,
    anychart.styles.line.LineStyleShapes
);
anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes.prototype.createElement = function() {
    return this.point_.getSVGManager().createPath();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes.prototype.createShapes = function() {
    if (this.point_.getStyle().needCreateStrokeShape()) {
        this.lineShape_ = new anychart.plots.radarPolarPlot.series.line.styles.LineStyleShape();
        this.lineShape_.initialize(this.point_, this);
    }
};
//------------------------------------------------------------------------------
//
//                          LineStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.line.LineStyle}
 */
anychart.plots.radarPolarPlot.series.line.styles.LineStyle = function() {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.series.line.styles.LineStyle,
    anychart.styles.line.LineStyle
);
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.series.line.styles.LineStyle.prototype.createStyleShapes = function() {
    return  new anychart.plots.radarPolarPlot.series.line.styles.LineStyleShapes();
};





