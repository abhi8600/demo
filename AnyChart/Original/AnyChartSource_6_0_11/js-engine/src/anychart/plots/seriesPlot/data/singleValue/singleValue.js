/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint}</li>
 *  <li>@class {anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries}</li>
 * <ul>
 */

goog.provide('anychart.plots.seriesPlot.data.singleValue');

//require
goog.require('anychart.plots.seriesPlot.data');
goog.require('anychart.utils');

//------------------------------------------------------------------------------
//
//                          BaseSingleValuePoint class.
//
//------------------------------------------------------------------------------

/**
 * Class represent base single value point.
 * <p>
 *  Description: Single value point is a point based on one number data value.
 * </p>
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint = function() {
    anychart.plots.seriesPlot.data.BasePoint.call(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint,
        anychart.plots.seriesPlot.data.BasePoint);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * Point value
 * @protected
 * @type {Number}
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.y_ = NaN;
/**
 * Return single point value.
 * @public
 * @return {Number}
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.getY = function() {
    return this.y_;
};
/**
 * @param {Number} value
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.setY = function(value) {
    this.y_ = value;
};
/**
 * Return single point value.
 * @public
 * @return {Number}
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.getValue = function() {
    return this.y_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize base single value point settings.
 * @public
 * @param {object} config JSON with single value point settings.
 * @param {object} stylesList Chart styles list.
 * <p>
 *  Expected JSON structure:
 *  <code>
 *      object {
 *          value : number // single point value.
 *      }
 *  </code>
 * </p>
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.deserialize = function(config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);

    if (anychart.utils.deserialization.hasProp(config, 'value'))
        this.y_ = anychart.utils.deserialization.getProp(config, 'value');
    else if (anychart.utils.deserialization.hasProp(config, 'y'))
        this.y_ = anychart.utils.deserialization.getProp(config, 'y');

    this.y_ = anychart.utils.deserialization.getNum(this.y_);
};
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%Value', 0);
    this.tokensHash_.add('%YValue', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.getTokenType = function(token) {
    switch (token) {
        case '%Value' :
        case '%YValue' :
        case '%YPercentOfSeries' :
        case '%YPercentOfTotal' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.calculatePointTokenValues = function() {
    goog.base(this, 'calculatePointTokenValues');
    
    if (this.y_) {
        this.tokensHash_.add('%Value', this.y_);
        this.tokensHash_.add('%YValue', this.y_);
    }
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.getTokenValue = function(token) {
    if (!this.tokensHash_.has('%YPercentOfSeries'))
        this.tokensHash_.add(
                '%YPercentOfSeries',
                this.y_ / this.series_.getSeriesTokenValue('%SeriesYSum') * 100);

    if (!this.tokensHash_.has('%YPercentOfTotal'))
        this.tokensHash_.add(
                '%YPercentOfTotal',
                this.y_ / this.getPlot().getTokenValue('%DataPlotYSum') * 100);

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//                          Serialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValuePoint.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);

    opt_target['YValue'] = this.tokensHash_.get('%YValue');
    opt_target['YPercentOfSeries'] = this.tokensHash_.get('%YPercentOfSeries');
    opt_target['YPercentOfTotal'] = this.tokensHash_.get('%YPercentOfTotal');

    return opt_target;
};
//------------------------------------------------------------------------------
//
//                         BaseSingleValueSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries = function() {
    anychart.plots.seriesPlot.data.BaseSeries.call(this);
};
goog.inherits(anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries,
        anychart.plots.seriesPlot.data.BaseSeries);
//------------------------------------------------------------------------------
//                               Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%SeriesYSum', 0);
    this.tokensHash_.add('%SeriesYMax', 0);
    this.tokensHash_.add('%SeriesYMin', 0);
    this.tokensHash_.add('%SeriesYBasedPointsCount', 0);
    this.tokensHash_.add('%SeriesFirstYValue', 0);
    this.tokensHash_.add('%SeriesLastYValue', 0);
    this.tokensHash_.add('%SeriesYAverage', 0);

    this.tokensHash_.add('%SeriesMaxYValuePointName', '');
    this.tokensHash_.add('%SeriesMinYValuePointName', '');
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.getTokenType = function(token) {
    switch (token) {
        case '%SeriesYSum' :
        case '%SeriesYMax' :
        case '%SeriesYMin' :
        case '%SeriesYBasedPointsCount' :
        case '%SeriesFirstYValue' :
        case '%SeriesLastYValue' :
        case '%SeriesYAverage' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
        case '%SeriesMaxYValuePointName' :
        case '%SeriesMinYValuePointName':
            return anychart.formatting.TextFormatTokenType.TEXT;
    }
    return goog.base(this, 'getTokenType', token);
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.calculateSeriesTokenValues = function() {
    goog.base(this, 'calculateSeriesTokenValues');

    var seriesYSum = this.tokensHash_.get('%SeriesYSum');

    this.tokensHash_.add('%SeriesValue', seriesYSum);
    this.tokensHash_.add('%SeriesYValue', seriesYSum);

    var numPoints = this.getNumPoints();

    this.tokensHash_.add('%SeriesYAverage', seriesYSum / numPoints);
    if (numPoints <= 0) return;

    if (this.getPointAt(0))
        this.tokensHash_.add('%SeriesFirstYValue', this.getPointAt(0).getY());

    if (this.getPointAt(numPoints - 1))
        this.tokensHash_.add('%SeriesLastYValue', this.getPointAt(numPoints - 1).getY());


};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.checkPlot = function(plot) {
    goog.base(this, 'checkPlot', plot);
    var plotTokensHash = plot.getTokensHash();

    plotTokensHash.increment('%DataPlotYSum', this.tokensHash_.get('%SeriesYSum'));
    plotTokensHash.increment('%DataPlotYBasedPointsCount', this.getNumPoints());

    if (this.tokensHash_.get('%SeriesYMax') > plotTokensHash.get('%DataPlotYMax')) {
        plotTokensHash.add('%DataPlotYMax', this.tokensHash_.get('%SeriesYMax'));
        plotTokensHash.add('%DataPlotMaxYValuePointName', this.tokensHash_.get('%SeriesMaxYValuePointName'));
        plotTokensHash.add('%DataPlotMaxYValuePointSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYMin') < plotTokensHash.get('%DataPlotYMin')) {
        plotTokensHash.add('%DataPlotYMin', this.tokensHash_.get('%SeriesYMin'));
        plotTokensHash.add('%DataPlotMinYValuePointName', this.tokensHash_.get('%SeriesMinYValuePointName'));
        plotTokensHash.add('%DataPlotMinYValuePointSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYSum') > plotTokensHash.get('%DataPlotMaxYSumSeries')) {
        plotTokensHash.add('%DataPlotMaxYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensHash.add('%DataPlotMaxYSumSeriesName', this.name_);
    }

    if (this.tokensHash_.get('%SeriesYSum') < plotTokensHash.get('%DataPlotMinYSumSeries')) {
        plotTokensHash.add('%DataPlotMinYSumSeries', this.tokensHash_.get('%SeriesYSum'));
        plotTokensHash.add('%DataPlotMinYSumSeriesName', this.name_);
    }
};
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.checkPoint = function(dataPoint) {
    goog.base(this, 'checkPoint', dataPoint);

    var pointY = dataPoint.getY();
    var cashedMax = Number(this.tokensHash_.get('%SeriesYMax'));
    var cashedMin = Number(this.tokensHash_.get('%SeriesYMin'));

    this.tokensHash_.increment('%SeriesYSum', pointY);

    if (pointY > cashedMax) {
        this.tokensHash_.add('%SeriesYMax', pointY);
        this.tokensHash_.add('%SeriesMaxYValuePointName', dataPoint.getName());
    }
    if (pointY < cashedMin) {
        this.tokensHash_.add('%SeriesYMin', pointY);
        this.tokensHash_.add('%SeriesMinYValuePointName', dataPoint.getName());
    }
};
//------------------------------------------------------------------------------
//                      Serialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.data.singleValue.BaseSingleValueSeries.prototype.serialize = function(opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);

    opt_target['FirstYValue'] = this.getSeriesTokenValue('%SeriesFirstYValue');
    opt_target['LastYValue'] = this.getSeriesTokenValue('%SeriesLastYValue');
    opt_target['YSum'] = this.getSeriesTokenValue['%SeriesYSum'];
    opt_target['YMax'] = this.getSeriesTokenValue['%SeriesYMax'];
    opt_target['YMin'] = this.getSeriesTokenValue['%SeriesYMin'];
    opt_target['YAverage'] = this.getSeriesTokenValue('%SeriesYAverage');
    opt_target['YMedian'] = this.getSeriesTokenValue('%SeriesYMedian');
    opt_target['YMode'] = this.getSeriesTokenValue('%SeriesYMode');

    return opt_target;

};