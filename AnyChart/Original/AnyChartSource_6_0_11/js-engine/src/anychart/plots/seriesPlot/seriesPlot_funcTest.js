/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes for test:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.TestChart}</li>
 *  <li>@class {anychart.plots.seriesPlot.TestChartView}</li>
 *  <li>@class {anychart.plots.seriesPlot.TestGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.seriesPlot.TestSeriesPlot}</li>
 *  <li>@class {anychart.plots.seriesPlot.TestSeries}</li>
 *  <li>@class {anychart.plots.seriesPlot.TestPoint}</li>
 * <ul>
 * Functional tests for style applying at global settings, series, point.
 */
//----------------------------------------------------------------------------------------------------------------------
//
//                          TestChart class
//
//----------------------------------------------------------------------------------------------------------------------

/***@constructor*/
anychart.plots.seriesPlot.TestChart = function() {
    anychart.chart.Chart.apply(this);
};
goog.inherits(anychart.plots.seriesPlot.TestChart, anychart.chart.Chart);
//----------------------------------------------------------------------------------------------------------------------
//                          Override abstract
//----------------------------------------------------------------------------------------------------------------------
anychart.plots.seriesPlot.TestChart.prototype.getPlot = function() {
    return this.plot_;
};

//----------------------------------------------------------------------------------------------------------------------
//
//                          TestChartView class
//
//----------------------------------------------------------------------------------------------------------------------

/***@constructor*/
anychart.plots.seriesPlot.TestChartView = function() {
    anychart.chartView.MainChartView.apply(this);
};
goog.inherits(anychart.plots.seriesPlot.TestChartView, anychart.chartView.MainChartView);
//----------------------------------------------------------------------------------------------------------------------
//                          Override abstract
//----------------------------------------------------------------------------------------------------------------------
anychart.plots.seriesPlot.TestChartView.prototype.getChartElement = function() {
    return this.chartElement_;
};

anychart.plots.seriesPlot.TestChartView.prototype.createChart = function(data) {
    if (anychart.utils.deserialization.hasProp(data, 'dashboard') && anychart.utils.deserialization.hasProp(anychart.utils.deserialization.getProp(data, 'dashboard'), 'view')) {
        this.chartElement_ = new anychart.dashboard.Dashboard();
    } else {
        this.chartElement_ = new anychart.plots.seriesPlot.TestChart();
    }
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          TestSeries class
//
//----------------------------------------------------------------------------------------------------------------------
/***@constructor*/
anychart.plots.seriesPlot.TestSeries = function() {
    anychart.plots.seriesPlot.data.BaseSeries.apply(this);
};
goog.inherits(anychart.plots.seriesPlot.TestSeries, anychart.plots.seriesPlot.data.BaseSeries);
//----------------------------------------------------------------------------------------------------------------------
//                          Override abstract
//----------------------------------------------------------------------------------------------------------------------
anychart.plots.seriesPlot.TestSeries.prototype.getStyle = function() {
    return this.style_;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          TestGlobalSeriesSettings class
//
//----------------------------------------------------------------------------------------------------------------------
/***@constructor*/
anychart.plots.seriesPlot.TestGlobalSeriesSettings = function() {
    anychart.plots.seriesPlot.data.GlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.seriesPlot.TestGlobalSeriesSettings, anychart.plots.seriesPlot.data.GlobalSeriesSettings);
//----------------------------------------------------------------------------------------------------------------------
//                          Override abstract
//----------------------------------------------------------------------------------------------------------------------
anychart.plots.seriesPlot.TestGlobalSeriesSettings.prototype.getStyle = function() {
    return this.style_;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          TestSeriesPlot class
//
//----------------------------------------------------------------------------------------------------------------------
/***@constructor*/
anychart.plots.seriesPlot.TestSeriesPlot = function() {
    anychart.plots.seriesPlot.SeriesPlot.apply(this);
};
goog.inherits(anychart.plots.seriesPlot.TestSeriesPlot, anychart.plots.seriesPlot.SeriesPlot);
//----------------------------------------------------------------------------------------------------------------------
//                          Override abstract
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------
//
//                          Tests
//
//----------------------------------------------------------------------------------------------------------------------
$(document).ready(function() {

    module('plots/seriesPlot/Functional styles test');
    test('Apply global styles test', function() {
        var obj = {
            charts : {
                chart : {
                    chart_settings : {},
                    data_plot_settings : {
                        bar_series: {
                            bar_style : {
                                fill : {
                                    type : 'solid',
                                    opacity : '0.1',
                                    color : 'red'
                                }
                            }
                        }
                    },
                    data : {
                        series : {
                            point : {
                                y:1
                            }
                        }
                    },
                    styles : {}
                }
            }
        };
        var manager = new anychart.svg.SVGManager(document);
        document.createElement("div").appendChild(manager.getSVG());
        var chartView = new anychart.plots.seriesPlot.TestChartView();
        chartView.initVisual(manager, manager.getSVG(), new anychart.utils.geom.Rectangle(0, 0, 550, 400));

        chartView.setJSON(obj);
        var globalSettingsStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getGlobalSettings().getStyle();
        var seriesStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getStyle();
        var pointStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getPointAt(0).getStyle();

        ok(globalSettingsStyle, 'Exist global styles');

        ok(globalSettingsStyle.getNormal(), 'Exist global styles normal state');
        ok(globalSettingsStyle.getNormal().getFill(), 'Exist global styles normal state fill');
        equal(globalSettingsStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().isGradient(), false, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles normal state');

        ok(globalSettingsStyle.getHover(), 'Exist global styles hover state');
        ok(globalSettingsStyle.getHover().getFill(), 'Exist global styles Hover state fill');
        equal(globalSettingsStyle.getHover().getFill().getOpacity(), 0.1, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().isGradient(), false, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Hover state');

        ok(globalSettingsStyle.getPushed(), 'Exist global styles Pushed state');
        ok(globalSettingsStyle.getPushed().getFill(), 'Exist global styles Pushed state fill');
        equal(globalSettingsStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().isGradient(), false, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Pushed state');

        ok(globalSettingsStyle.getSelectedHover(), 'Exist global styles SelectedHover state');
        ok(globalSettingsStyle.getSelectedHover().getFill(), 'Exist global styles SelectedHover state fill');
        equal(globalSettingsStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().isGradient(), false, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedHover state');

        ok(globalSettingsStyle.getSelectedNormal(), 'Exist global styles SelectedNormal state');
        ok(globalSettingsStyle.getSelectedNormal().getFill(), 'Exist global styles SelectedNormal state fill');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedNormal state');

        ok(globalSettingsStyle.getMissing(), 'Exist global styles Missing state');
        ok(globalSettingsStyle.getMissing().getFill(), 'Exist global styles Missing state fill');
        equal(globalSettingsStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().isGradient(), false, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Missing state');

        ok(seriesStyle, 'Exist series styles');

        ok(seriesStyle.getNormal(), 'Exist series styles normal state');
        ok(seriesStyle.getNormal().getFill(), 'Exist series styles normal state fill');
        equal(seriesStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().isGradient(), false, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles normal state');

        ok(seriesStyle.getHover(), 'Exist series styles hover state');
        ok(seriesStyle.getHover().getFill(), 'Exist series styles Hover state fill');
        equal(seriesStyle.getHover().getFill().getOpacity(), 0.1, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().isGradient(), false, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Hover state');

        ok(seriesStyle.getPushed(), 'Exist series styles Pushed state');
        ok(seriesStyle.getPushed().getFill(), 'Exist series styles Pushed state fill');
        equal(seriesStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().isGradient(), false, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Pushed state');

        ok(seriesStyle.getSelectedHover(), 'Exist series styles SelectedHover state');
        ok(seriesStyle.getSelectedHover().getFill(), 'Exist series styles SelectedHover state fill');
        equal(seriesStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().isGradient(), false, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles SelectedHover state');

        ok(seriesStyle.getSelectedNormal(), 'Exist series styles SelectedNormal state');
        ok(seriesStyle.getSelectedNormal().getFill(), 'Exist series styles SelectedNormal state fill');
        equal(seriesStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles SelectedNormal state');

        ok(seriesStyle.getMissing(), 'Exist series styles Missing state');
        ok(seriesStyle.getMissing().getFill(), 'Exist series styles Missing state fill');
        equal(seriesStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().isGradient(), false, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Missing state');

        ok(pointStyle, 'Exist point styles');

        ok(pointStyle.getNormal(), 'Exist point styles normal state');
        ok(pointStyle.getNormal().getFill(), 'Exist point styles normal state fill');
        equal(pointStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().isGradient(), false, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles normal state');

        ok(pointStyle.getHover(), 'Exist point styles hover state');
        ok(pointStyle.getHover().getFill(), 'Exist point styles Hover state fill');
        equal(pointStyle.getHover().getFill().getOpacity(), 0.1, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().isGradient(), false, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles Hover state');

        ok(pointStyle.getPushed(), 'Exist point styles Pushed state');
        ok(pointStyle.getPushed().getFill(), 'Exist point styles Pushed state fill');
        equal(pointStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().isGradient(), false, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles Pushed state');

        ok(pointStyle.getSelectedHover(), 'Exist point styles SelectedHover state');
        ok(pointStyle.getSelectedHover().getFill(), 'Exist point styles SelectedHover state fill');
        equal(pointStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().isGradient(), false, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles SelectedHover state');

        ok(pointStyle.getSelectedNormal(), 'Exist point styles SelectedNormal state');
        ok(pointStyle.getSelectedNormal().getFill(), 'Exist point styles SelectedNormal state fill');
        equal(pointStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles SelectedNormal state');

        ok(pointStyle.getMissing(), 'Exist point styles Missing state');
        ok(pointStyle.getMissing().getFill(), 'Exist point styles Missing state fill');
        equal(pointStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().isGradient(), false, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist point styles Missing state');
    });

    test('Override global style by series', function() {
        var obj = {
            charts : {
                chart : {
                    chart_settings : {},
                    data_plot_settings : {
                        bar_series: {
                            bar_style : {
                                fill : {
                                    type : 'solid',
                                    opacity : '0.1',
                                    color : 'red'
                                }
                            }
                        }
                    },
                    data : {
                        series : {
                            style : 'seriesStyle',
                            point : {
                                y:1
                            }
                        }
                    },
                    styles : {
                        bar_style : {
                            name: 'seriesStyle',
                            fill : {
                                type : 'solid',
                                opacity : '0.25',
                                color : 'green'
                            }
                        }
                    }
                }
            }
        };
        var manager = new anychart.svg.SVGManager(document);
        document.createElement("div").appendChild(manager.getSVG());
        var chartView = new anychart.plots.seriesPlot.TestChartView();
        chartView.initVisual(manager, manager.getSVG(), new anychart.utils.geom.Rectangle(0, 0, 550, 400));

        chartView.setJSON(obj);
        var globalSettingsStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getGlobalSettings().getStyle();
        var seriesStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getStyle();
        var pointStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getPointAt(0).getStyle();

        ok(globalSettingsStyle, 'Exist global styles');

        ok(globalSettingsStyle.getNormal(), 'Exist global styles normal state');
        ok(globalSettingsStyle.getNormal().getFill(), 'Exist global styles normal state fill');
        equal(globalSettingsStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().isGradient(), false, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles normal state');

        ok(globalSettingsStyle.getHover(), 'Exist global styles hover state');
        ok(globalSettingsStyle.getHover().getFill(), 'Exist global styles Hover state fill');
        equal(globalSettingsStyle.getHover().getFill().getOpacity(), 0.1, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().isGradient(), false, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Hover state');

        ok(globalSettingsStyle.getPushed(), 'Exist global styles Pushed state');
        ok(globalSettingsStyle.getPushed().getFill(), 'Exist global styles Pushed state fill');
        equal(globalSettingsStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().isGradient(), false, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Pushed state');

        ok(globalSettingsStyle.getSelectedHover(), 'Exist global styles SelectedHover state');
        ok(globalSettingsStyle.getSelectedHover().getFill(), 'Exist global styles SelectedHover state fill');
        equal(globalSettingsStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().isGradient(), false, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedHover state');

        ok(globalSettingsStyle.getSelectedNormal(), 'Exist global styles SelectedNormal state');
        ok(globalSettingsStyle.getSelectedNormal().getFill(), 'Exist global styles SelectedNormal state fill');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedNormal state');

        ok(globalSettingsStyle.getMissing(), 'Exist global styles Missing state');
        ok(globalSettingsStyle.getMissing().getFill(), 'Exist global styles Missing state fill');
        equal(globalSettingsStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().isGradient(), false, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Missing state');

        ok(seriesStyle, 'Exist series styles');

        ok(seriesStyle.getNormal(), 'Exist series styles normal state');
        ok(seriesStyle.getNormal().getFill(), 'Exist series styles normal state fill');
        equal(seriesStyle.getNormal().getFill().getOpacity(), 0.25, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().isGradient(), false, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles normal state');

        ok(seriesStyle.getHover(), 'Exist series styles hover state');
        ok(seriesStyle.getHover().getFill(), 'Exist series styles Hover state fill');
        equal(seriesStyle.getHover().getFill().getOpacity(), 0.25, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().isGradient(), false, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles Hover state');

        ok(seriesStyle.getPushed(), 'Exist series styles Pushed state');
        ok(seriesStyle.getPushed().getFill(), 'Exist series styles Pushed state fill');
        equal(seriesStyle.getPushed().getFill().getOpacity(), 0.25, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().isGradient(), false, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles Pushed state');

        ok(seriesStyle.getSelectedHover(), 'Exist series styles SelectedHover state');
        ok(seriesStyle.getSelectedHover().getFill(), 'Exist series styles SelectedHover state fill');
        equal(seriesStyle.getSelectedHover().getFill().getOpacity(), 0.25, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().isGradient(), false, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles SelectedHover state');

        ok(seriesStyle.getSelectedNormal(), 'Exist series styles SelectedNormal state');
        ok(seriesStyle.getSelectedNormal().getFill(), 'Exist series styles SelectedNormal state fill');
        equal(seriesStyle.getSelectedNormal().getFill().getOpacity(), 0.25, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles SelectedNormal state');

        ok(seriesStyle.getMissing(), 'Exist series styles Missing state');
        ok(seriesStyle.getMissing().getFill(), 'Exist series styles Missing state fill');
        equal(seriesStyle.getMissing().getFill().getOpacity(), 0.25, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().isGradient(), false, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist series styles Missing state');

        ok(pointStyle, 'Exist point styles');

        ok(pointStyle.getNormal(), 'Exist point styles normal state');
        ok(pointStyle.getNormal().getFill(), 'Exist point styles normal state fill');
        equal(pointStyle.getNormal().getFill().getOpacity(), 0.25, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().isGradient(), false, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles normal state');

        ok(pointStyle.getHover(), 'Exist point styles hover state');
        ok(pointStyle.getHover().getFill(), 'Exist point styles Hover state fill');
        equal(pointStyle.getHover().getFill().getOpacity(), 0.25, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().isGradient(), false, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles Hover state');

        ok(pointStyle.getPushed(), 'Exist point styles Pushed state');
        ok(pointStyle.getPushed().getFill(), 'Exist point styles Pushed state fill');
        equal(pointStyle.getPushed().getFill().getOpacity(), 0.25, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().isGradient(), false, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles Pushed state');

        ok(pointStyle.getSelectedHover(), 'Exist point styles SelectedHover state');
        ok(pointStyle.getSelectedHover().getFill(), 'Exist point styles SelectedHover state fill');
        equal(pointStyle.getSelectedHover().getFill().getOpacity(), 0.25, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().isGradient(), false, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles SelectedHover state');

        ok(pointStyle.getSelectedNormal(), 'Exist point styles SelectedNormal state');
        ok(pointStyle.getSelectedNormal().getFill(), 'Exist point styles SelectedNormal state fill');
        equal(pointStyle.getSelectedNormal().getFill().getOpacity(), 0.25, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles SelectedNormal state');

        ok(pointStyle.getMissing(), 'Exist point styles Missing state');
        ok(pointStyle.getMissing().getFill(), 'Exist point styles Missing state fill');
        equal(pointStyle.getMissing().getFill().getOpacity(), 0.25, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().isGradient(), false, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(0, 128, 0)', 'Exist point styles Missing state');
    });

    test('Override series style by point', function() {
        var obj = {
            charts : {
                chart : {
                    chart_settings : {},
                    data_plot_settings : {
                        bar_series: {
                            bar_style : {
                                fill : {
                                    type : 'solid',
                                    opacity : '0.1',
                                    color : 'red'
                                }
                            }
                        }
                    },
                    data : {
                        series : {
                            style : 'seriesStyle',
                            point : {
                                style:'pointStyle',
                                y:1
                            }
                        }
                    },
                    styles : {
                        bar_style : {
                            name: 'pointStyle',
                            fill : {
                                type : 'solid',
                                opacity : '0.55',
                                color : 'yellow'
                            }
                        }
                    }
                }
            }
        };
        var manager = new anychart.svg.SVGManager(document);
        document.createElement("div").appendChild(manager.getSVG());
        var chartView = new anychart.plots.seriesPlot.TestChartView();
        chartView.initVisual(manager, manager.getSVG(), new anychart.utils.geom.Rectangle(0, 0, 550, 400));

        chartView.setJSON(obj);
        var globalSettingsStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getGlobalSettings().getStyle();
        var seriesStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getStyle();
        var pointStyle = chartView.getChartElement().getPlot().getSeriesAt(0).getPointAt(0).getStyle();

        ok(globalSettingsStyle, 'Exist global styles');

        ok(globalSettingsStyle.getNormal(), 'Exist global styles normal state');
        ok(globalSettingsStyle.getNormal().getFill(), 'Exist global styles normal state fill');
        equal(globalSettingsStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().isGradient(), false, 'Exist global styles normal state');
        equal(globalSettingsStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles normal state');

        ok(globalSettingsStyle.getHover(), 'Exist global styles hover state');
        ok(globalSettingsStyle.getHover().getFill(), 'Exist global styles Hover state fill');
        equal(globalSettingsStyle.getHover().getFill().getOpacity(), 0.1, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().isGradient(), false, 'Exist global styles Hover state');
        equal(globalSettingsStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Hover state');

        ok(globalSettingsStyle.getPushed(), 'Exist global styles Pushed state');
        ok(globalSettingsStyle.getPushed().getFill(), 'Exist global styles Pushed state fill');
        equal(globalSettingsStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().isGradient(), false, 'Exist global styles Pushed state');
        equal(globalSettingsStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Pushed state');

        ok(globalSettingsStyle.getSelectedHover(), 'Exist global styles SelectedHover state');
        ok(globalSettingsStyle.getSelectedHover().getFill(), 'Exist global styles SelectedHover state fill');
        equal(globalSettingsStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().isGradient(), false, 'Exist global styles SelectedHover state');
        equal(globalSettingsStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedHover state');

        ok(globalSettingsStyle.getSelectedNormal(), 'Exist global styles SelectedNormal state');
        ok(globalSettingsStyle.getSelectedNormal().getFill(), 'Exist global styles SelectedNormal state fill');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist global styles SelectedNormal state');
        equal(globalSettingsStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles SelectedNormal state');

        ok(globalSettingsStyle.getMissing(), 'Exist global styles Missing state');
        ok(globalSettingsStyle.getMissing().getFill(), 'Exist global styles Missing state fill');
        equal(globalSettingsStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().isGradient(), false, 'Exist global styles Missing state');
        equal(globalSettingsStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist global styles Missing state');

        ok(seriesStyle, 'Exist series styles');

        ok(seriesStyle.getNormal(), 'Exist series styles normal state');
        ok(seriesStyle.getNormal().getFill(), 'Exist series styles normal state fill');
        equal(seriesStyle.getNormal().getFill().getOpacity(), 0.1, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().isGradient(), false, 'Exist series styles normal state');
        equal(seriesStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles normal state');

        ok(seriesStyle.getHover(), 'Exist series styles hover state');
        ok(seriesStyle.getHover().getFill(), 'Exist series styles Hover state fill');
        equal(seriesStyle.getHover().getFill().getOpacity(), 0.1, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().isGradient(), false, 'Exist series styles Hover state');
        equal(seriesStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Hover state');

        ok(seriesStyle.getPushed(), 'Exist series styles Pushed state');
        ok(seriesStyle.getPushed().getFill(), 'Exist series styles Pushed state fill');
        equal(seriesStyle.getPushed().getFill().getOpacity(), 0.1, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().isGradient(), false, 'Exist series styles Pushed state');
        equal(seriesStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Pushed state');

        ok(seriesStyle.getSelectedHover(), 'Exist series styles SelectedHover state');
        ok(seriesStyle.getSelectedHover().getFill(), 'Exist series styles SelectedHover state fill');
        equal(seriesStyle.getSelectedHover().getFill().getOpacity(), 0.1, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().isGradient(), false, 'Exist series styles SelectedHover state');
        equal(seriesStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles SelectedHover state');

        ok(seriesStyle.getSelectedNormal(), 'Exist series styles SelectedNormal state');
        ok(seriesStyle.getSelectedNormal().getFill(), 'Exist series styles SelectedNormal state fill');
        equal(seriesStyle.getSelectedNormal().getFill().getOpacity(), 0.1, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist series styles SelectedNormal state');
        equal(seriesStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles SelectedNormal state');

        ok(seriesStyle.getMissing(), 'Exist series styles Missing state');
        ok(seriesStyle.getMissing().getFill(), 'Exist series styles Missing state fill');
        equal(seriesStyle.getMissing().getFill().getOpacity(), 0.1, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().isGradient(), false, 'Exist series styles Missing state');
        equal(seriesStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 0, 0)', 'Exist series styles Missing state');

        ok(pointStyle, 'Exist point styles');

        ok(pointStyle.getNormal(), 'Exist point styles normal state');
        ok(pointStyle.getNormal().getFill(), 'Exist point styles normal state fill');
        equal(pointStyle.getNormal().getFill().getOpacity(), 0.55, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().isGradient(), false, 'Exist point styles normal state');
        equal(pointStyle.getNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles normal state');

        ok(pointStyle.getHover(), 'Exist point styles hover state');
        ok(pointStyle.getHover().getFill(), 'Exist point styles Hover state fill');
        equal(pointStyle.getHover().getFill().getOpacity(), 0.55, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().isGradient(), false, 'Exist point styles Hover state');
        equal(pointStyle.getHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles Hover state');

        ok(pointStyle.getPushed(), 'Exist point styles Pushed state');
        ok(pointStyle.getPushed().getFill(), 'Exist point styles Pushed state fill');
        equal(pointStyle.getPushed().getFill().getOpacity(), 0.55, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().isGradient(), false, 'Exist point styles Pushed state');
        equal(pointStyle.getPushed().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles Pushed state');

        ok(pointStyle.getSelectedHover(), 'Exist point styles SelectedHover state');
        ok(pointStyle.getSelectedHover().getFill(), 'Exist point styles SelectedHover state fill');
        equal(pointStyle.getSelectedHover().getFill().getOpacity(), 0.55, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().isGradient(), false, 'Exist point styles SelectedHover state');
        equal(pointStyle.getSelectedHover().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles SelectedHover state');

        ok(pointStyle.getSelectedNormal(), 'Exist point styles SelectedNormal state');
        ok(pointStyle.getSelectedNormal().getFill(), 'Exist point styles SelectedNormal state fill');
        equal(pointStyle.getSelectedNormal().getFill().getOpacity(), 0.55, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().isGradient(), false, 'Exist point styles SelectedNormal state');
        equal(pointStyle.getSelectedNormal().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles SelectedNormal state');

        ok(pointStyle.getMissing(), 'Exist point styles Missing state');
        ok(pointStyle.getMissing().getFill(), 'Exist point styles Missing state fill');
        equal(pointStyle.getMissing().getFill().getOpacity(), 0.55, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().isGradient(), false, 'Exist point styles Missing state');
        equal(pointStyle.getMissing().getFill().getColor().getColor().toRGB(), 'rgb(255, 255, 0)', 'Exist point styles Missing state');
    });
});
