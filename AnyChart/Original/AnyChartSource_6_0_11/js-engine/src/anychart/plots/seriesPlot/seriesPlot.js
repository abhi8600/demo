/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.SeriesPlot}</li>
 * <ul>
 */
//namespace
goog.provide('anychart.plots.seriesPlot');
//require
goog.require('anychart.plots');
goog.require('anychart.utils');
goog.require('anychart.visual.background');
goog.require('anychart.palettes');
goog.require('anychart.styles');
goog.require('anychart.thresholds');
//------------------------------------------------------------------------------
//
//                          SeriesPlot class
//
//------------------------------------------------------------------------------
/**
 * Class represent series plot.
 * @constructor
 * @extends {anychart.plots.BasePlot}
 */
anychart.plots.seriesPlot.SeriesPlot = function () {
    anychart.plots.BasePlot.apply(this);
    this.allowMultipleSelect_ = false;
    this.ignoreMissingPoints_ = true;
    this.drawingPoints_ = [];
    this.pointCache_ = {};
    this.seriesCache_ = {};
    this.globalSeriesSettingsMap_ = {};
    this.series_ = [];
    this.onBeforeDrawSeries_ = [];
    this.initLegendAdapter();
};
goog.inherits(anychart.plots.seriesPlot.SeriesPlot, anychart.plots.BasePlot);
//------------------------------------------------------------------------------
//
//                          Series type
//
//------------------------------------------------------------------------------

/**
 * Default plot series type value.
 * @protected
 * @type {anychart.series.SeriesType}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.defaultSeriesType_ = anychart.series.SeriesType.BAR;

/**
 * Method for parse series.type value.
 * Returns real supported series type.
 *
 * @protected
 * @param {string} type New series type.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSeriesType = function (type) {
    return this.defaultSeriesType_;
};
//------------------------------------------------------------------------------
//
//                          Series list
//
//------------------------------------------------------------------------------
/**
 * Contains all series object for this chart.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BaseSeries>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.series_ = null;
/**
 * Contains series witch need call onBeforeDraw method.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BaseSeries>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.onBeforeDrawSeries_ = null;
/**
 * Get total number of series
 * @public
 * @return {number}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getNumSeries = function () {
    return this.series_ ? this.series_.length : 0;
};

/**
 * Get series at index
 * @public
 * @param {number} index series index
 * @return {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSeriesAt = function (index) {
    return this.series_ ? this.series_[index] : null;
};
/**
 * @param {anychart.plots.seriesPlot.data.BaseSeries} series
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.pushSeries = function (series) {
    if (this.series_ && series) this.series_.push(series);
};
//------------------------------------------------------------------------------
//                          Locale.
//------------------------------------------------------------------------------
/**
 * @return {anychart.locale.DateTimeLocale}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getDateTimeLocale = function () {
    return null;
};
/**
 * @return {anychart.locale.NumberLocale}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getNumberLocale = function () {
    return null;
};
//------------------------------------------------------------------------------
//
//                          Global series settings
//
//------------------------------------------------------------------------------

/**
 * Hash map, contains global series settings for this chart.
 * @private
 * @type {object.<*>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.globalSeriesSettingsMap_ = null;

/**
 * Create global series settings object.
 * <p>
 *  This is internal method of this class, to get global series settings object @see #getGlobalSeriesSettings_().
 * </p>
 * @protected
 * @param {anychart.series.SeriesType} seriesType Series type.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.createGlobalSeriesSettings = goog.abstractMethod;

anychart.plots.seriesPlot.SeriesPlot.prototype.getSettingsType = function (seriesType) {
    return seriesType;
};

/**
 * Return global series settings object for this chart.
 * @private
 * @param {object} dataPlotSettings data_plot_settings node.
 * @param {object} dataNode Chart data node.
 * @param {string} seriesType Series type
 * @param {anychart.styles.StylesList} stylesList Styles list instance.
 * @return {anychart.plots.seriesPlot.data.GlobalSeriesSettings}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getGlobalSeriesSettings_ = function (dataPlotSettings, dataNode, seriesType, stylesList) {
    var settings = this.getCashedGlobalSeriesSettings_(seriesType);
    if (settings) return settings;
    settings = this.createGlobalSeriesSettings(seriesType);
    if (settings == null) return null;

    settings.setPlot(this);
    settings.setMainChartView(this.mainChartView_);
    settings.deserializeActions(dataNode);
    settings.deserializeInteractivity(dataNode);

    var settingsNode = anychart.utils.deserialization.getProp(dataPlotSettings, settings.getSettingsNodeName());
    settings.deserializeInteractivity(settingsNode);
    settings.deserializeActions(settingsNode);
    settings.deserialize(settingsNode, stylesList);
    settings.deserializeStyle(settingsNode, stylesList);
    settings.deserializeElements(settingsNode, stylesList);
    this.registerGlobalSeriesSettings_(this.getSettingsType(seriesType), settings);
    if (!this.ignoreMissingPoints_) settings.initializeMissingInterpolators();

    return settings;
};
anychart.plots.seriesPlot.SeriesPlot.prototype.getCashedGlobalSeriesSettings_ = function (seriesType) {
    var settingsType = this.getSettingsType(seriesType);
    if (this.globalSeriesSettingsMap_[settingsType]) return this.globalSeriesSettingsMap_[settingsType];
    else return null;
};
anychart.plots.seriesPlot.SeriesPlot.prototype.registerGlobalSeriesSettings_ = function (seriesType, settings) {
    this.globalSeriesSettingsMap_[this.getSettingsType(seriesType)] = settings;
};
//-----------------------------------------------------------------------------------
//
//                          Palettes
//
//-----------------------------------------------------------------------------------

/**
 * Return palette object for data.
 * @protected
 * @param {Function} paletteClass Palette class, can be : anychart.palettes.ColorPalette or anychart.palettes.HatchTypePalette.
 * @param {object} node Palette node.
 * @param {anychart.palettes.PalettesCollection} palettesCollection Palette collection object.
 * @return {anychart.palettes.ColorPalette || anychart.palettes.HatchTypePalette}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getDataPalette_ = function (paletteClass, nodeName, node, palettesCollection) {
    var paletteNode = null;
    var paletteProp = nodeName;

    if (anychart.utils.deserialization.hasProp(node, paletteProp)) {
        var dataPaletteName = anychart.utils.deserialization.getProp(node, paletteProp);
        paletteNode = palettesCollection.getPalette(paletteProp, dataPaletteName);
    }

    if (paletteNode == null)
        paletteNode = palettesCollection.getPalette(paletteProp, 'default');

    var palette = new paletteClass();
    palette.deserialize(paletteNode);

    return palette;
};

/**
 * Return palette object for series.
 * @protected
 * @param {Function} paletteClass Palette class, can be : anychart.palettes.ColorPalette or anychart.palettes.HatchTypePalette.
 * @param dataPalette Palette object for data, @see #getDataPalette_().
 * @param {anychart.plots.seriesPlot.data.GlobalSeriesSettings} globalSettings Global series settings object.
 * @param {object} seriesNode Series node object
 * @param {anychart.palettes.PalettesCollection} palettesCollection Palette collection object.
 * @return {anychart.palettes.ColorPalette || anychart.palettes.HatchTypePalette}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSeriesPalette_ = function (paletteClass, nodeName, dataPalette, globalSettings, seriesNode, palettesCollection) {
    var paletteProp = nodeName;
    if (anychart.utils.deserialization.hasProp(seriesNode, paletteProp)) {
        var seriesPaletteName = anychart.utils.deserialization.getProp(seriesNode, paletteProp);
        var seriesPaletteNode = palettesCollection.getPalette(paletteProp, seriesPaletteName);
        if (seriesPaletteNode != null) {
            var palette = new paletteClass();
            palette.deserialize(seriesPaletteNode);
            return palette;
        }
    } else {
        return globalSettings.isApplyDataPaletteToSeries() ? dataPalette : null;
    }
    return null;
};

//------------------------------------------------------------------------------
//                          Threshlods
//------------------------------------------------------------------------------

/**
 * Chart threshold list.
 * @private
 * @type {anychart.thresholds.ThresholdsList}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.thresholdsList_ = null;
/**
 * Return chart threshold list.
 * @return {anychart.thresholds.ThresholdsList}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getThresholdsList = function () {
    return this.thresholdsList_;
};
//------------------------------------------------------------------------------
//                          Legend
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.legend.LegendAdapter}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.legendAdapter_ = null;
/**
 * Create new legend adapter.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.initLegendAdapter = function () {
    this.legendAdapter_ = new anychart.controls.legend.LegendAdapter(this);
};
/**
 * @param {Object} itemFilter
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.setLegendData = function (itemFilter, container) {
    var des = anychart.utils.deserialization;

    if (!des.hasProp(itemFilter, 'source'))
        container.addItem(new anychart.controls.legend.LegendAdaptiveItem(null, 0, -1, true, null));
    else
        this.legendAdapter_.getLegendDataBySource(
            des.getEnumProp(itemFilter, 'source'),
            itemFilter,
            container);
};
/**@inheritDoc*/
anychart.plots.BasePlot.prototype.drawDataIcon = function (iconSettings, item, sprite, dx, dy) {
    if (!item.getColor()) item.setColor(iconSettings.getColor());

    if (iconSettings.isSimpleBox()) {
        this.drawSimpleIcon_(iconSettings, item, sprite, dx, dy);
        return;
    }

    var seriesType = iconSettings.isDynamicSeriesType() ?
        item.getSeriesType() : iconSettings.getSeriesType();

    var seriesSettings = this.getCashedGlobalSeriesSettings_(this.getSettingsType(seriesType));

    if (seriesSettings && seriesSettings.hasIconDrawer(seriesType)) {
        var bounds = new anychart.utils.geom.Rectangle(
            dx,
            dy,
            iconSettings.getWidth(),
            iconSettings.getHeight());
        seriesSettings.drawIcon(seriesType, item, sprite, bounds);
    } else this.drawSimpleIcon_(iconSettings, item, sprite, dx, dy);
};
/**
 * @private
 */
anychart.plots.BasePlot.prototype.drawSimpleIcon_ = function (iconSettings, item, sprite, dx, dy) {
    var rect = sprite.getSVGManager().createRect(
        dx,
        dy,
        iconSettings.getWidth(),
        iconSettings.getHeight());
    var style = 'stroke:none;';
    if (item.getColor())
        style += 'fill:' + item.getColor().getColor().toRGB() + ';';
    else style += 'fill:none;';

    rect.setAttribute('style', style);
    sprite.appendChild(rect);
};
//------------------------------------------------------------------------------
// Сей блок кода используется из извращенной необходимости обхода серий и поиска точек в них
// в MapPlot, дабы включить точки undefined регионов.
// Коментраий перенесен из flash версии юзается ещё и в легенде
//                                                          10.05.2011.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.seriesIndex_ = null;
anychart.plots.seriesPlot.SeriesPlot.prototype.resetSeriesIterator = function () {
    this.seriesIndex_ = 0;
};
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.hasSeries = function () {
    return this.seriesIndex_ < this.series_.length;
};
/**
 * @return {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getNextSeries = function () {
    var res = this.series_[this.seriesIndex_];
    this.seriesIndex_++;
    return res;
};
/**конец ебанутого блока (так же из флеша:)))))*/
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------

/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.deserializeDataPlotSettings = function (config) {
    goog.base(this, 'deserializeDataPlotSettings', config);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'default_series_type'))
        this.defaultSeriesType_ = this.getSeriesType(des.getLString(des.getProp(config, 'default_series_type')));

    if (des.hasProp(config, 'ignore_missing'))
        this.ignoreMissingPoints_ = des.getBoolProp(config, 'ignore_missing');

    if (des.hasProp(config, 'interactivity')) {
        var interactivitySettings = des.getProp(config, 'interactivity');
        if (des.hasProp(interactivitySettings, "allow_multiple_select"))
            this.allowMultipleSelect_ = des.getBoolProp(interactivitySettings, "allow_multiple_select");
    }
};

/**
 * Deserialize series props
 * @protected
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.deserializeSeriesArguments_ = function (series, seriesNode) {
};

/**
 * Prepare series points for deserialization
 * @protected
 * @param series
 * @param seriesNode
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.prepareSeriesPoints_ = function (series, seriesNode) {
};

/**
 * Should point names in series be unique or not
 * @param series
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.needCheckUniquePointNames_ = function (series) {
    return false;
};
/**
 * Выполняется перед десериализацией точек серии.
 * Здесь может выполнятся пре-сортировка точек или какие либо еще операции с настройками точки пока она не десериализовалась.
 * @param {anychart.plots.seriesPlot.data.BaseSeries} series
 * @param {Array.<Object>} points Points array.
 * @return {Array.<Object>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.beforeSeriesPointsDeserialize = function (series, points) {
    return points;
};
/**
 * Calls after each of series ends its deserialization.
 * @param {anychart.plots.seriesPlot.data.BaseSeries} series Series.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.onSeriesPointsDeserialize_ = function (series) {
    if (!this.ignoreMissingPoints_)
        series.getGlobalSettings().interpolateMissingPoints(series);

    if (series.getGlobalSettings().needSortPoints()) {
        series.getGlobalSettings().sortPoints(series);
        for (var i = 0; i < series.getNumPoints(); i++) {
            series.getPointAt(i).setIndex(i);
        }
    }
    series.checkPlot(this);
};

/**
 * Define, is data node of chart valid for deserialize.
 * @param {Object} config Chart node
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.checkNoData = function (config) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(config, 'data')) return true;

    var data = des.getProp(config, 'data');
    if (!des.hasProp(data, 'series')) return true;

    var series = des.getPropArray(data, 'series');
    return series.length == 0;
};
/**
 * Deserialize data series. The core of magic.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.deserializeData_ = function (config, stylesList) {
    goog.base(this, 'deserializeData_', config);

    //aliases
    var des = anychart.utils.deserialization;

    //thresholds list
    this.thresholdsList_ = new anychart.thresholds.ThresholdsList(
        des.getProp(config, 'thresholds'));
    //palettes list
    var palettesList = new anychart.palettes.PalettesCollection();
    if (des.hasProp(config, 'palettes'))
        palettesList.deserialize(des.getProp(config, 'palettes'));

    var dataNode = des.getProp(config, 'data');
    var seriesSettingsNode = des.getProp(config, 'data_plot_settings');

    var seriesNodes = des.getPropArray(dataNode, 'series');
    var seriesNodesLen = seriesNodes.length;

    //palettes
    var dataColorPalette = this.getDataPalette_(
        anychart.palettes.ColorPalette,
        'palette',
        dataNode,
        palettesList);

    var dataMarkerPalette = this.getDataPalette_(
        anychart.palettes.MarkerTypePalette,
        'marker_palette',
        dataNode,
        palettesList);

    var dataHatchTypePalette = this.getDataPalette_(
        anychart.palettes.HatchTypePalette,
        'hatch_palette',
        dataNode,
        palettesList);

    dataColorPalette = dataColorPalette ? dataColorPalette.checkAuto(seriesNodesLen) : null;

    //thresholdItem
    var dataThreshold = null;
    if (des.hasProp(dataNode, 'threshold'))
        dataThreshold = this.thresholdsList_.getThreshold(
            des.getLStringProp(dataNode, 'threshold'),
            palettesList);
    for (var i = 0; i < seriesNodesLen; i++) {
        var seriesNode = seriesNodes[i];

        if (des.hasProp(seriesNode, 'visible') && !des.getBoolProp(seriesNode, 'visible')) continue;

        var seriesType = (des.hasProp(seriesNode, 'type')) ?
            this.getSeriesType(des.getLStringProp(seriesNode, 'type'))
            : this.defaultSeriesType_;


        var globalSettings = this.getGlobalSeriesSettings_(seriesSettingsNode, dataNode, seriesType, stylesList);
        if (globalSettings == null) continue;

        var series = globalSettings.createSeries(seriesType);
        series.setIndex(i);
        series.setName('Series ' + i.toString());
        series.deserializeName(seriesNode);
        series.setGlobalSettings(globalSettings);
        series.setActionsList(globalSettings.getActionsList());
        series.deserializeActions(seriesNode);

        //series style
        series.setStyle(globalSettings.getStyle());
        series.deserializeStyle(seriesNode, stylesList);

        //series interactivity
        series.setInteractivitySettings(globalSettings);
        series.deserializeInteractivity(seriesNode);

        //series color
        if (des.hasProp(seriesNode, 'color'))
            series.setColor(des.getColorProp(seriesNode, 'color'));
        else
            series.setColor(dataColorPalette.getItemAt(i));

        //series hatch type
        if (des.hasProp(seriesNode, 'hatch_type'))
            series.setHatchType(anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(des.getLStringProp(seriesNode, 'hatch_type')));
        else
            series.setHatchType(dataHatchTypePalette.getItemAt(i));

        //series marker type
        if (des.hasProp(seriesNode, 'marker_type'))
            series.setMarkerType(anychart.elements.marker.MarkerType.deserialize(des.getLStringProp(seriesNode, 'marker_type')));
        else
            series.setMarkerType(dataMarkerPalette.getItemAt(i));

        //series threshold
        if (des.hasProp(seriesNode, 'threshold'))
            series.setThreshold(this.thresholdsList_.getThreshold(
                des.getLStringProp(seriesNode, 'threshold'),
                palettesList));
        else
            series.setThreshold(dataThreshold);

        //elements
        globalSettings.setElements(series);

        this.prepareSeriesPoints_(series, seriesNode);


        series.deserializeElements(seriesNode, stylesList);
        series.deserialize(seriesNode);

        this.deserializeSeriesArguments_(series, seriesNode);

        var seriesColorPalette = this.getSeriesPalette_(anychart.palettes.ColorPalette,
            'palette',
            dataColorPalette,
            globalSettings,
            seriesNode,
            palettesList);

        var seriesMarkerPalette = this.getSeriesPalette_(
            anychart.palettes.MarkerTypePalette,
            'marker_palette',
            dataMarkerPalette,
            globalSettings,
            seriesNode,
            palettesList);

        var seriesHatchPalette = this.getSeriesPalette_(
            anychart.palettes.HatchTypePalette,
            'hatch_palette',
            dataMarkerPalette,
            globalSettings,
            seriesNode,
            palettesList);

        des.setProp(seriesNode, 'point', this.beforeSeriesPointsDeserialize(series, des.getPropArray(seriesNode, 'point')));


        var checkPointNames = this.needCheckUniquePointNames_(series);
        var pointNamesHashMap = checkPointNames ? {} : null;

        var addPointsToDrawing = series.addPointsToDrawing();

        var pointsList = des.getPropArray(seriesNode, 'point');
        var pointsCount = pointsList.length;

        seriesColorPalette = seriesColorPalette ? seriesColorPalette.checkAuto(pointsCount) : null;

        for (var j = 0; j < pointsCount; j++) {

            var pointNode = pointsList[j];

            var dataPoint = series.createPoint();
            dataPoint.setGlobalSettings(globalSettings);
            //point style
            dataPoint.setStyle(series.getStyle());
            dataPoint.deserializeStyle(pointNode, stylesList);
            //point actions
            dataPoint.setActionsList(series.getActionsList());
            dataPoint.deserializeActions(pointNode);
            //point interactivity
            dataPoint.setInteractivitySettings(series);
            dataPoint.deserializeInteractivity(pointNode);

            dataPoint.setIndex(j);
            dataPoint.setSeries(series);
            dataPoint.setName(j.toString());
            dataPoint.deserializeName(pointNode);
            if (checkPointNames) {
                if (pointNamesHashMap[dataPoint.getName()]) continue;
                pointNamesHashMap[dataPoint.getName()] = true;
            }

            dataPoint.deserialize(pointNode, this.svgManager_);

            //missing
            if (dataPoint.isMissing() && !globalSettings.canDrawMissingPoints())
                continue;

            if (!this.ignoreMissingPoints_)
                globalSettings.checkMissingPointDuringDeserialize(dataPoint);

            series.setElements(dataPoint);
            dataPoint.deserializeElements(pointNode, stylesList);
            dataPoint.setColor(series.getColor());
            dataPoint.setHatchType(series.getHatchType());
            dataPoint.setMarkerType(series.getMarkerType());

            if (des.hasProp(pointNode, 'color'))
                dataPoint.setColor(des.getColorProp(pointNode, 'color'));
            else
                dataPoint.setColor(seriesColorPalette ? seriesColorPalette.getItemAt(dataPoint.getPaletteIndex()) : series.getColor());

            if (des.hasProp(pointNode, 'hatch_type'))
                dataPoint.setHatchType(anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(des.getLStringProp(pointNode, 'hatch_type')));
            else
                dataPoint.setHatchType(seriesHatchPalette ? seriesHatchPalette.getItemAt(dataPoint.getPaletteIndex()) : series.getHatchType());

            if (des.hasProp(pointNode, 'marker_type'))
                dataPoint.setMarkerType(anychart.elements.marker.MarkerType.deserialize(des.getLStringProp(pointNode, 'marker_type')));
            else
                dataPoint.setMarkerType(seriesMarkerPalette ? seriesMarkerPalette.getItemAt(dataPoint.getPaletteIndex()) : series.getMarkerType());

            if (des.hasProp(pointNode, 'threshold'))
                dataPoint.setThreshold(this.thresholdsList_.getThreshold(
                    des.getLStringProp(pointNode, 'threshold'),
                    palettesList));
            else
                dataPoint.setThreshold(series.getThreshold());

            series.addPoint(dataPoint, dataPoint.getIndex());

            if (!dataPoint.isMissing())
                series.checkPoint(dataPoint);


            if (addPointsToDrawing && (!(dataPoint.isMissing() && this.ignoreMissingPoints_))) {
                if (dataPoint.hasThreshold())
                    dataPoint.getThreshold().checkAfterDeserialize(dataPoint);
                this.drawingPoints_.push(dataPoint);
            }

        }
        this.onSeriesPointsDeserialize_(series);

        if (series.needCallOnBeforeDraw()) this.onBeforeDrawSeries_.push(series);
        this.series_.push(series);
    }

    if (this.thresholdsList_) this.thresholdsList_.initializeAutomaticThresholds();
    this.onSeriesDeserialize();
};
/**
 * @protected
 * @param {Array.<anychart.plots.seriesPlot.data.BaseSeries>} seriesArray
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.sortSeriesByOrder = function (seriesArray) {

};

anychart.plots.seriesPlot.SeriesPlot.prototype.getTokenType = function (token) {
    if (token == '%DataPlotSeriesCount')
        return  anychart.formatting.TextFormatTokenType.NUMBER;
    return goog.base(this, 'getTokenType', token);
};

anychart.plots.seriesPlot.SeriesPlot.prototype.onSeriesDeserialize = function () {
    this.tokensHash_.add('%DataPlotSeriesCount', this.series_.length);
};
//-----------------------------------------------------------------------------------
//
//                          Templates
//
//-----------------------------------------------------------------------------------

/**
 * Return template merger specific for each of series.
 * @public
 * @return {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getTemplatesMerger = function () {
    return null;
};

//-----------------------------------------------------------------------------------
//
//                          Missing points
//
//-----------------------------------------------------------------------------------
/**
 * Indicate, need ignore missing points.
 * @private
 * @type {Boolean}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.ignoreMissingPoints_ = true;
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.needIgnoreMissingPoints = function () {
    return this.ignoreMissingPoints_;
};
//------------------------------------------------------------------------------
//                          Points selection
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.allowMultipleSelect_ = false;
/**
 * @return {Boolean}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.isAllowMultipleSelect = function () {
    return this.allowMultipleSelect_;
};
/**
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.selectedPoints_ = null;
/**
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.selectedPoint_ = null;
/**
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSelectedPoint = function () {
    return this.selectedPoint_;
};
/**
 * Select point.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 * @param {Boolean} opt_redraw
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.selectPoint = function (point, opt_redraw) {
    if (this.allowMultipleSelect_) {
        if (this.selectedPoints_ == null) this.selectedPoints_ = [];
        if (this.selectedPoints_.indexOf(point) == -1)
            this.selectedPoints_.push(point);
        this.dispatchMultiplePointsEvent_(
            anychart.events.MultiplePointsEvent.MULTIPLE_POINTS_SELECT,
            this.selectedPoint_);
    } else if (this.selectedPoint_ != point) {
        if (this.selectedPoint_ != null) {
            this.selectedPoint_.deselect(opt_redraw);
        }

        this.selectedPoint_ = point;
    }
    if (opt_redraw) this.selectedPoint_.update();
};
/**
 * @private
 * @param {String} type
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} points
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.dispatchMultiplePointsEvent_ = function (type, points) {
    this.mainChartView_.dispatchEvent(new anychart.events.MultiplePointsEvent(type, points));
};
//------------------------------------------------------------------------------
//                           Initialization
//------------------------------------------------------------------------------
/**
 * Series initialization
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.svg.SVGSprite} tooltipContainer
 * @param {Boolean} opt_drawBackground
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.initialize = function (bounds, tooltipContainer, opt_drawBackground) {
    goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);
    this.initializeSeries_();
    return this.sprite_;
};
/**
 * Initialize all plot series
 * @private
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.initializeSeries_ = function () {
    var i, len;
    for (i = 0, len = this.drawingPoints_.length; i < len; i++)
        this.dataSprite_.appendChild(this.drawingPoints_[i].initialize(this.svgManager_).getElement());
};
//------------------------------------------------------------------------------
//                      Post initialization.
//------------------------------------------------------------------------------
/**
 * Plot post initialize.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.postInitialize = function () {
    if (!this.initialized_) return;
    if (this.postInitialized_) return;
    this.postInitialized_ = true;

    if (this.selectedPoint_ && this.selectedPoint_.getActionsList())
        this.selectedPoint_.getActionsList().execute(this.selectedPoint_);

    if (this.selectedPoints_) {
        var pointsCount = this.selectedPoints_.length;
        for (var i = 0; i < pointsCount; i++)
            if (this.selectedPoints_[i].getActionsList())
                this.selectedPoints_[i].getActionsList().execute(this.selectedPoints_[i]);
    }
};
//------------------------------------------------------------------------------
//                            Drawing.
//------------------------------------------------------------------------------
/**
 * Drawing points collection.
 * @private
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.drawingPoints_ = null;
/**
 * Draw all plot series
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.draw = function () {
    goog.base(this, 'draw');
    this.drawSeries_();
};
/**
 * Draw all plot series
 * @private
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.drawSeries_ = function () {
    var i, len;

    for (i = 0, len = this.onBeforeDrawSeries_.length; i < len; i++) {
        this.onBeforeDrawSeries_[i].onBeforeDraw();
    }

    for (i = 0, len = this.drawingPoints_.length; i < len; i++) {
        this.drawingPoints_[i].checkThreshold();
        this.drawingPoints_[i].update();
    }
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.baseBounds_ = null;
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.execResize = function () {
    goog.base(this, 'execResize');
    this.resizeSeries();
};
/**
 * Resize all plot series.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.resizeSeries = function () {
    var i, len;

    for (i = 0, len = this.onBeforeDrawSeries_.length; i < len; i++)
        this.onBeforeDrawSeries_[i].onBeforeResize();

    for (i = 0, len = this.drawingPoints_.length; i < len; i++)
        this.drawingPoints_[i].resize();
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.SeriesPlot.prototype.serialize = function (opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['Series'] = [];
    var totalPointsCount = 0;
    var seriesCount = this.getNumSeries();
    for (var i = 0; i < seriesCount; i++) {
        opt_target['Series'].push(this.series_[i].serialize());
        totalPointsCount += this.series_[i].getNumPoints();
    }
    opt_target['SeriesCount'] = seriesCount;
    opt_target['PointCount'] = totalPointsCount;
    return opt_target;

};
//------------------------------------------------------------------------------
//                          Points external methods
//------------------------------------------------------------------------------
/**
 * Add point data to chart data.
 * @param {String|Number} seriesId
 * @param {Object} pointData
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.addPoint = function (seriesId, pointData) {
    if (!seriesId || !pointData) return;
    var series = this.getSeriesData_(seriesId);
    if (!series) return;
    pointData = this.checkData(pointData);
    var seriesPoints = this.getPointsNodes(series);

    if (goog.isArray(pointData)) {
        var pointsCount = pointData.length;
        for (var i = 0; i < pointsCount; i++) {
            seriesPoints.push(pointData[i]);
            this.registerPointData_(
                pointData,
                this.getPointCacheKey_(seriesId, pointData));
        }


    } else if (goog.isObject(pointData)) {
        seriesPoints.push(pointData);
        this.registerPointData_(
            pointData,
            this.getPointCacheKey_(seriesId, pointData));
    }
};
/**
 * Add point data to chart data at passed index.
 * @param {String|Number} seriesId
 * @param {Number} index
 * @param {Object|String} pointData
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.addPointAt = function (seriesId, index, pointData) {
    if (!seriesId || index == null || index == undefined || !pointData) return;

    var des = anychart.utils.deserialization;
    var series = this.getSeriesData_(seriesId);

    if (!series) return;
    pointData = this.checkData(pointData);
    var seriesPoints = this.getPointsNodes(series);

    seriesPoints = anychart.utils.JSONUtils.insert(seriesPoints, index, pointData);
    this.registerPointData_(
        pointData,
        this.getPointCacheKey_(seriesId, pointData));

    des.setProp(series, 'point', seriesPoints);
};
/**
 * Remove point data from cached chart data.
 * @param {String|Number} seriesId
 * @param {String|Number} pointId
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.removePoint = function (seriesId, pointId) {
    var series = this.getSeriesData_(seriesId);
    if (!series) return;

    //remove from data
    var removedPoints = anychart.utils.JSONUtils.removeById(
        this.getPointsNodes(series),
        pointId);

    //remove from cache
    var removedPointsCount = removedPoints.length;
    for (var i = 0; i < removedPointsCount; i++)
        this.unRegisterPointData_(this.getPointCacheKey_(
            seriesId,
            removedPoints[i]));
};
/**
 * Merge point data with passed point data.
 * @param {String|Number} seriesId
 * @param {String|Number} pointId
 * @param {Object|String} pointData
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.updatePoint = function (seriesId, pointId, pointData) {
    var point = this.getPointData_(seriesId, pointId);
    if (!point) return;
    pointData = this.checkData(pointData);
    point = anychart.utils.JSONUtils.mergeDataNode(point, pointData);
    this.registerPointData_(this.getPointCacheKey_(seriesId, pointId));
};

anychart.plots.seriesPlot.SeriesPlot.prototype.updatePointData = function (groupName, pointName, data) {

};
/**
 * Set highlight to point.
 * @param {String|Number} seriesId
 * @param {String|Number} pointId
 * @param {Boolean} highlight
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.highlightPoint = function (seriesId, pointId, highlight) {
    var point = this.getDeserializedPoint_(seriesId, pointId);
    if (!point) return;
    if (highlight) point.setHover();
    else point.setNormal();
    this.registerPointData_(this.getPointCacheKey_(seriesId, pointId));
};
/**
 * Set point selected.
 * @param {String|Number} seriesId
 * @param {String|Number} pointId
 * @param {Boolean} select
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.selectPointById = function (seriesId, pointId, select) {
    var point = this.getDeserializedPoint_(seriesId, pointId);
    if (!point) return;

    if (select) point.select(true);
    else point.deselect(true);

    this.registerPointData_(this.getPointCacheKey_(seriesId, pointId));
};
/**
 * Return  deserialized point by series id and point id.
 * @private
 * @param {String|Number} seriesId
 * @param {String|Number} pointId
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getDeserializedPoint_ = function (seriesId, pointId) {
    var series = this.getDeserializedSeries_(seriesId);
    if (!series) return null;

    var pointCount = series.getNumPoints();
    for (var i = 0; i < pointCount; i++) {
        var point = series.getPointAt(i);
        if (point.getId() && point.getId() == pointId) return point;
    }
    return null;
};
//------------------------------------------------------------------------------
//                          Series external methods
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.SeriesPlot.prototype.addSeries = function (seriesData) {
    if (!seriesData) return;
    seriesData = this.checkData(seriesData);
    var seriesNodes = this.getSeriesNodes();

    if (goog.isArray(seriesData)) {
        var seriesCount = seriesData.length;
        for (var i = 0; i < seriesCount; i++) {
            seriesNodes.push(seriesData[i]);
            this.registerSeriesData_(seriesData[i]);
        }
    } else if (goog.isObject(seriesData)) {
        seriesNodes.push(seriesData);
        this.registerSeriesData_(seriesData);
    }
    this.setSeriesNodes(seriesNodes);
};
anychart.plots.seriesPlot.SeriesPlot.prototype.addSeriesAt = function (index, seriesData) {
    if (index == null || index == undefined || !seriesData) return;
    seriesData = this.checkData(seriesData);
    var seriesNodes = this.getSeriesNodes();
    anychart.utils.JSONUtils.insert(seriesNodes, index, seriesData);
    this.setSeriesNodes(seriesNodes);
    this.registerSeriesData_(seriesData)
};
anychart.plots.seriesPlot.SeriesPlot.prototype.removeSeries = function (seriesId) {
    if (seriesId == null || seriesId == undefined) return;
    var seriesNodes = this.getSeriesNodes();
    anychart.utils.JSONUtils.removeById(seriesNodes, seriesId);
    this.setSeriesNodes(seriesNodes);
    this.unRegisterSeriesData_(seriesId);
};
anychart.plots.seriesPlot.SeriesPlot.prototype.updateSeries = function (seriesId, seriesData) {
    if (!seriesId || !seriesData) return;
    seriesData = this.checkData(seriesData);
    var series = this.getSeriesData_(seriesId);
    if (!series) return;
    series = anychart.utils.JSONUtils.mergeDataNode(series, seriesData);
    this.registerSeriesData_(seriesData, seriesId);
};
anychart.plots.seriesPlot.SeriesPlot.prototype.showSeries = function (seriesId, visible) {
    if (!seriesId || visible == null || visible == undefined) return;
    var series = this.getSeriesData_(seriesId);
    if (!series) return;
    var des = anychart.utils.deserialization;
    des.setProp(series, 'visible', des.getLString(visible));
    this.registerSeriesData_(series, seriesId);
};
anychart.plots.seriesPlot.SeriesPlot.prototype.highlightSeries = function (seriesId, highlight) {
    if (!seriesId || highlight == null || highlight == undefined) return;
    var series = this.getDeserializedSeries_(seriesId);
    if (!series) return;
    if (highlight) series.setHover(true);
    else series.setNormal(true);
};
/**
 * Return  deserialized series by series id.
 * @private
 * @param {String|Number} seriesId
 * @return {anychart.plots.seriesPlot.data.BaseSeries}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getDeserializedSeries_ = function (seriesId) {
    var seriesCount = this.getNumSeries();
    for (var i = 0; i < seriesCount; i++) {
        var series = this.getSeriesAt(i);
        if (series.getId() && series.getId() == seriesId) {
            return series;
        }
    }
    return null;
};
//------------------------------------------------------------------------------
//                          Clear/Refresh
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.SeriesPlot.prototype.clear = function () {
    var des = anychart.utils.deserialization;
    des.setProp(des.getProp(this.cachedData_, 'data'), 'series', []);
};
anychart.plots.seriesPlot.SeriesPlot.prototype.refresh = function () {
    this.chartView_.setJSON(this.cachedData_);
};
//------------------------------------------------------------------------------
//                  Series data cache
//------------------------------------------------------------------------------
/**
 * Series data cache.
 * @private
 * @type {Object.<String>}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.seriesCache_ = null;

/**
 * Return series data from cachedData object by series id or from series cache if series was cached.
 * @param {String|Number} seriesId
 * @return {Object}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSeriesData_ = function (seriesId) {
    if (!seriesId) return null;
    //cache
    if (this.seriesCache_[seriesId]) return this.seriesCache_[seriesId];
    //search
    var seriesNodes = this.getSeriesNodes();
    var series = anychart.utils.JSONUtils.getNodeById(seriesNodes, seriesId);
    this.setSeriesNodes(seriesNodes);

    //register
    this.registerSeriesData_(series, seriesId);
    return series;
};
/**
 * Register series data in point cache by passed key.
 * @private
 * @param {Object} data
 * @param {String|Number} opt_key Series key for registration, if known (series id).
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.registerSeriesData_ = function (data, opt_key) {
    if (opt_key) this.seriesCache_[opt_key] = data;
    else {
        var des = anychart.utils.deserialization;
        if (des.hasProp(data, 'id'))
            this.seriesCache_[des.getProp(data, 'id')] = data;
    }
};
/**
 * Remove series data from cache by series id.
 * @private
 * @param {String} seriesId series id.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.unRegisterSeriesData_ = function (seriesId) {
    if (this.seriesCache_[seriesId]) delete this.seriesCache_[seriesId];
};
/**
 * Check cached data on existing nodes data and data.series, if doesn't exist, then create.
 * @private
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getSeriesNodes = function () {
    var des = anychart.utils.deserialization;
    var data, series;

    if (des.hasProp(this.cachedData_, 'data'))
        data = des.getProp(this.cachedData_, 'data');
    else {
        data = {};
        des.setProp(this.cachedData_, 'data', data);
    }

    if (des.hasProp(data, 'series'))
        series = des.getPropArray(data, 'series');
    else  des.setProp(data, 'series', []);
    return series;
};
anychart.plots.seriesPlot.SeriesPlot.prototype.getPointsNodes = function (seriesNode) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(seriesNode, 'point')) return des.getPropArray(seriesNode, 'point');
    else {
        var points = [];
        des.setProp(seriesNode, 'point', points);
        return points;
    }
};
anychart.plots.seriesPlot.SeriesPlot.prototype.setSeriesNodes = function (value) {
    var des = anychart.utils.deserialization;
    des.setProp(des.getProp(this.cachedData_, 'data'),
        'series',
        value);
};
//------------------------------------------------------------------------------
//                  Point data cache
//------------------------------------------------------------------------------
anychart.plots.seriesPlot.SeriesPlot.prototype.pointCache_ = null;
/**
 * Return point data from cachedData object by series id or from series cache if series was cached.
 * @param {String|Number} seriesId
 * @return {Object}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getPointData_ = function (seriesId, pointId) {
    if (!seriesId || !pointId) return null;
    //cache
    var pointKey = this.getPointCacheKey_(seriesId, pointId);
    if (this.pointCache_[pointKey]) return this.pointCache_[pointKey];
    //search
    var series = this.getSeriesData_(seriesId);
    var des = anychart.utils.deserialization;
    var pointNodes = des.getPropArray(series, 'point');
    var point = anychart.utils.JSONUtils.getNodeById(pointNodes, pointId);
    //register
    this.registerPointData_(point, pointKey);
    return point;
};
/**
 * Register point data in point cache by passed key.
 * @private
 * @param {Object} data
 * @param {String|Number} key Point key for registration, @see #getPointCacheKey_.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.registerPointData_ = function (data, key) {
    this.pointCache_[key] = data;
};
/**
 * Remove point data from cache by point key @see #getPointCacheKey_.
 * @private
 * @param {String} key Point cache key.
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.unRegisterPointData_ = function (key) {
    if (this.pointCache_[key]) delete this.pointCache_[key];
};
/**
 * Generate point cache key by series id and point id.
 * @private
 * @param {String|Number|Object} series
 * @param {String|Number|Object} point
 * @return {String}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.getPointCacheKey_ = function (series, point) {
    var seriesId, pointId;

    if (goog.isObject(series))
        seriesId = anychart.utils.deserialization.getStringProp(series, 'id');
    else seriesId = series;

    if (goog.isObject(point))
        seriesId = anychart.utils.deserialization.getStringProp(point, 'id');
    else pointId = point;

    if (!series || !point) return null;

    return 'series:' + seriesId + ':point:' + pointId;
};
/**
 * Проверка данных переданых во все внешние методы по добавлению серий  елементов серий (addPoint, addSeries).
 * @param {Object|String} data
 * @return {Object}
 */
anychart.plots.seriesPlot.SeriesPlot.prototype.checkData = function (data) {
    if (goog.isString(data))
        return anychart.utils.deserialization.parseXMLString(data);
    else if (goog.isObject(data)) return data;
};
