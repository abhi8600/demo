/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview Contains default settings template for FunnelPlot.
 */
goog.provide('anychart.plots.funnelPlot.templates.DEFAULT_TEMPLATE');

anychart.plots.funnelPlot.templates.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"funnel_style": {
				"border": {
					"type": "Solid",
					"color": "DarkColor(%Color)"
				},
				"fill": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "%Color",
								"opacity": "1"
							},
							{
								"position": "1",
								"color": "Blend(DarkColor(%Color),%Color,0.7)",
								"opacity": "1"
							}
						],
						"angle": "90"
					},
					"type": "Gradient",
					"color": "%Color",
					"opacity": "0.9"
				},
				"effects": {
					"bevel": {
						"enabled": "true",
						"distance": "1"
					},
					"enabled": "True"
				},
				"states": {
					"hover": {
						"border": {
							"type": "Solid",
							"color": "DarkColor(White)"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "White",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "Blend(DarkColor(White),%White,0.7)",
										"opacity": "1"
									}
								],
								"angle": "90"
							},
							"type": "Gradient",
							"color": "White",
							"opacity": "0.9"
						}
					},
					"pushed": {
						"border": {
							"type": "Solid",
							"color": "DarkColor(White)"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "White",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "Blend(DarkColor(White),White,0.7)",
										"opacity": "1"
									}
								],
								"angle": "90"
							},
							"type": "Gradient",
							"color": "White",
							"opacity": "0.9"
						},
						"effects": {
							"bevel": {
								"enabled": "true",
								"distance": "2",
								"angle": "225"
							}
						}
					},
					"selected_normal": {
						"border": {
							"thickness": "2",
							"color": "DarkColor(%Color)"
						},
						"hatch_fill": {
							"enabled": "true",
							"thickness": "3",
							"type": "ForwardDiagonal",
							"opacity": "0.3"
						}
					},
					"selected_hover": {
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "White",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "Blend(DarkColor(White),White,0.7)",
										"opacity": "1"
									}
								],
								"angle": "90"
							},
							"type": "Gradient",
							"color": "White",
							"opacity": "0.9"
						},
						"border": {
							"thickness": "2",
							"color": "DarkColor(White)"
						},
						"hatch_fill": {
							"enabled": "true",
							"thickness": "3",
							"type": "ForwardDiagonal",
							"color": "DarkColor(White)",
							"opacity": "0.3"
						}
					},
					"missing": {
						"border": {
							"type": "Solid",
							"color": "DarkColor(White)",
							"thickness": "1",
							"opacity": "0.4"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "White",
										"opacity": "1"
									},
									{
										"position": "1",
										"color": "Blend(DarkColor(White),White,0.7)",
										"opacity": "1"
									}
								],
								"angle": "90"
							},
							"type": "Gradient",
							"opacity": "0.3"
						},
						"hatch_fill": {
							"type": "Checkerboard",
							"pattern_size": "20",
							"enabled": "true",
							"opacity": "0.1"
						},
						"effects": {
							"bevel": {
								"enabled": "false",
								"distance": "1",
								"angle": "225"
							}
						}
					}
				},
				"name": "anychart_default"
			}
		},
		"chart_settings": {
			"data_plot_background": {
				"inside_margin": {
					"all": "0"
				}
			}
		},
		"data_plot_settings": {
			"funnel_series": {
				"label_settings": {
				},
				"marker_settings": {
				},
				"tooltip_settings": {
				},
				"apply_palettes_to": "points",
				"neck_height": "0.2",
				"min_width": "0.2",
				"mode": "circular",
				"padding": "0.02",
				"min_point_size": "0.02"
			}
		}
	}
};