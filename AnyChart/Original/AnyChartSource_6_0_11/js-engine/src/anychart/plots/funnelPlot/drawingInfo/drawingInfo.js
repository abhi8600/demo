/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>
 *    @class {anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo}
 *  </li>.
 *  <li>
 * @class {anychart.plots.funnelPlot.drawingInfo.FunnelDIWDrawingInfo}
 *  </li>.
 * <ul>
 */

goog.provide('anychart.plots.funnelPlot.drawingInfo');
goog.require('anychart.plots.percentBasePlot.drawingInfo');
goog.require('anychart.utils.geom');
//------------------------------------------------------------------------------
//
//                         FunnelDrawingInfo class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo = function() {
    anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.call(this);
    this.leftCenter_ = new anychart.utils.geom.Point();
    this.rightCenter_ = new anychart.utils.geom.Point();

    this.centerTopFront_ = new anychart.utils.geom.Point();
    this.centerBottomFront_ = new anychart.utils.geom.Point();
    this.centerTopBack_ = new anychart.utils.geom.Point();
    this.centerBottomBack_ = new anychart.utils.geom.Point();
    this.centerCenterFront_ = new anychart.utils.geom.Point();
    this.centerCenterBack_ = new anychart.utils.geom.Point();

    this.shiftNumber_ = 0;
};
goog.inherits(anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo,
        anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo);
//------------------------------------------------------------------------------
//                              Properties
//------------------------------------------------------------------------------

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.leftCenter_ = null;
/**
 * Getter for left center.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getLeftCenter = function() {
    return this.leftCenter_;
};

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.rightCenter_ = null;

anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getRightCenter = function() {
    return this.rightCenter_;
};

//Points for calculating 3D
/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerTopFront_ = null;

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerBottomFront_ = null;

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerTopBack_ = null;

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerBottomBack_ = null;

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerCenterFront_ = null;

/**
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerCenterBack_ = null;

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.centerSize3D_ = null;

/**
 * @type {Number}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.shiftNumber_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getShiftNumber = function() {
    return this.shiftNumber_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.topBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getTopBounds = function() {
    return this.topBounds_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.bottomBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getBottomBounds = function() {
    return this.bottomBounds_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.leftBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getLeftBounds = function() {
    return this.leftBounds_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.rightBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getRightBounds = function() {
    return this.rightBounds_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.frontBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getFrontBounds = function() {
    return this.frontBounds_;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.backBounds_ = null;
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getBackBounds = function() {
    return this.backBounds_;
};
//------------------------------------------------------------------------------
//                          methods, overrides, etc
//------------------------------------------------------------------------------

anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.initialiseBounds = function() {
    var sides = anychart.plots.funnelPlot.drawingInfo.Sides;
    this.topBounds_ = this.getBounds(this.createSides_(sides.TOP));
    this.backBounds_ = this.getBounds(this.createSides_(sides.BACK));
    this.frontBounds_ = this.getBounds(this.createSides_(sides.FRONT));
    this.leftBounds_ = this.getBounds(this.createSides_(sides.LEFT));
    this.rightBounds_ = this.getBounds(this.createSides_(sides.RIGHT));
    this.bottomBounds_ = this.getBounds(this.createSides_(sides.BOTTOM));
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.setAnchorPoint = function(anchor, pos, inverted) {
    var anchorEnum = anychart.layout.Anchor;
    switch (anchor) {
        case anchorEnum.CENTER:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.centerTopFront_.y +
                    this.centerBottomFront_.y) / 2;
            break;
        }
        case anchorEnum.CENTER_LEFT:
        {
            pos.x = (this.leftTop_.x + this.leftBottom_.x) / 2;
            pos.y = (this.leftTop_.y + this.leftBottom_.y) / 2;
            if (this.leftTop_.y != this.leftCenter_.y) {// учет neck'a
                if (pos.y <= this.leftCenter_.y) {
                    pos.x = inverted ? (this.leftTop_.x) :
                            (this.leftTop_.x + ((this.leftCenter_.x -
                                    this.leftTop_.x) *
                                    (pos.y - this.leftTop_.y)) /
                                    (this.leftCenter_.y - this.leftTop_.y));
                } else pos.x = inverted ? (this.leftCenter_.x -
                        ((this.leftCenter_.x - this.leftBottom_.x) /
                                (this.leftBottom_.y - this.leftCenter_.y)) *
                                (pos.y - this.leftCenter_.y)) : this.leftBottom_.x;
            }
            break;
        }
        case anchorEnum.CENTER_RIGHT:
        {
            pos.x = (this.rightTop_.x + this.rightBottom_.x) / 2;
            pos.y = (this.rightTop_.y + this.rightBottom_.y) / 2;
            if (this.rightTop_.y != this.rightCenter_.y) {
                if (pos.y <= this.leftCenter_.y) {// учет neck'a
                    pos.x = inverted ? (this.rightTop_.x) :
                            (this.rightTop_.x -
                                    (this.rightTop_.x - this.rightCenter_.x) /
                                            (this.rightCenter_.y - this.rightTop_.y) *
                                            (pos.y - this.rightTop_.y));
                } else pos.x = inverted ? (this.rightCenter_.x +
                        (this.rightBottom_.x - this.rightCenter_.x) /
                                (this.rightBottom_.y - this.rightCenter_.y) *
                                (pos.y - this.rightCenter_.y)) :
                        this.rightBottom_.x;
            }
            break;
        }
        case anchorEnum.CENTER_BOTTOM:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.centerBottomFront_.y);
            break;
        }
        case anchorEnum.CENTER_TOP:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.centerTopFront_.y);
            break;
        }
        case anchorEnum.LEFT_TOP:
        {
            pos.x = (this.leftTop_.x);
            pos.y = (this.leftTop_.y);
            break;
        }
        case anchorEnum.RIGHT_TOP:
        {
            pos.x = (this.rightTop_.x);
            pos.y = (this.rightTop_.y);
            break;
        }
        case anchorEnum.LEFT_BOTTOM:
        {
            pos.x = (this.leftBottom_.x);
            pos.y = (this.centerBottomFront_.y);
            break;
        }
        case anchorEnum.RIGHT_BOTTOM:
        {
            pos.x = (this.rightBottom_.x);
            pos.y = (this.rightBottom_.y);
            break;
        }
    }
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.prototype.setSectorPoint = function(pt, inverted, w) {
    var h = pt.series_.getHeight();
    var x = pt.series_.getSeriesBounds().x;
    var y = pt.series_.getSeriesBounds().y;

    var shift = pt.series_.shiftNumber_;
    var seriesWidth = pt.series_.getSeriesBounds().width;
    var seriesHeight = pt.series_.getSeriesBounds().height;
    var neckHeight = pt.globalSettings_.getNeckHeight();
    var baseWidth = pt.globalSettings_.getBaseWidth * w;
    var dataIsWidth = pt.globalSettings_.isDataIsWidth();
    var feet = pt.globalSettings_.getFeet();
    w = h * feet;
    if (shift == 0) {
        if (w > seriesWidth) {
            w = seriesWidth;
            h = seriesWidth / feet;
            y = (seriesHeight - h) / 2;
        }
    } else {
        if (w > shift) {
            w = shift;
            h = shift / feet;
            y = (seriesHeight - h) / 2;
        }
    }

    this.shiftNumber_ = w - seriesWidth;
    var size3D = pt.globalSettings_.getSize3D() * h;

    if (!pt.is3D()) {
        size3D = 0;
    }

    this.bottomSize3D_ = pt.bottomWidth_ * size3D;
    this.topSize3D_ = pt.topWidth_ * size3D;
    this.centerSize3D_ = pt.centerWidth_ * size3D;

    this.centerBottomBack_.x = this.centerBottomFront_.x =
            this.centerCenterBack_.x = this.centerCenterFront_.x =
                    this.centerTopBack_.x = this.centerTopFront_.x =
                            x + seriesWidth / 2;

    if (!inverted && neckHeight == 0 && baseWidth == 0) {
        this.centerBottomBack_.y = (y + h - pt.topHeight_ * h);
        this.centerCenterBack_.y = (y + h - pt.centerHeight_ * h);
        this.centerTopBack_.y = (y + h - pt.bottomHeight_ * h);

        this.centerBottomFront_.y = this.centerBottomBack_.y + this.bottomSize3D_ * 2;
        this.centerCenterFront_.y = this.centerCenterBack_.y + this.centerSize3D_ * 2;
        this.centerTopFront_.y = this.centerTopBack_.y + this.topSize3D_ * 2;
    } else {
        this.centerBottomFront_.y = inverted ? (y + pt.bottomHeight_ * h) : (y + h - pt.topHeight_ * h);
        this.centerCenterFront_.y = inverted ? (y + pt.centerHeight_ * h) : (y + h - pt.centerHeight_ * h);
        this.centerTopFront_.y = inverted ? (y + pt.topHeight_ * h) : (y + h - pt.bottomHeight_ * h);

        this.centerBottomBack_.y = this.centerBottomFront_.y - this.bottomSize3D_ * 2;
        this.centerCenterBack_.y = this.centerCenterFront_.y - this.centerSize3D_ * 2;
        this.centerTopBack_.y = this.centerTopFront_.y - this.topSize3D_ * 2;
    }

    this.leftBottom_.y = this.rightBottom_.y = this.centerBottomFront_.y - this.bottomSize3D_;
    this.leftCenter_.y = this.rightCenter_.y = this.centerCenterFront_.y - this.centerSize3D_;
    this.leftTop_.y = this.rightTop_.y = this.centerTopFront_.y - this.topSize3D_;

    this.leftBottom_.x = x + seriesWidth / 2 - pt.bottomWidth_ * w / 2;
    this.leftCenter_.x = x + seriesWidth / 2 - pt.centerWidth_ * w / 2;
    this.leftTop_.x = x + seriesWidth / 2 - pt.topWidth_ * w / 2;
    this.rightBottom_.x = x + seriesWidth / 2 + pt.bottomWidth_ * w / 2;
    this.rightCenter_.x = x + seriesWidth / 2 + pt.centerWidth_ * w / 2;
    this.rightTop_.x = x + seriesWidth / 2 + pt.topWidth_ * w / 2;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.updateSectorPoint = function(x) {
    this.leftBottom_.x -= x;
    this.leftCenter_.x -= x;
    this.leftTop_.x -= x;
    this.rightBottom_.x -= x;
    this.rightCenter_.x -= x;
    this.rightTop_.x -= x;
    this.centerTopFront_.x -= x;
    this.centerBottomFront_.x -= x;
    this.centerTopBack_.x -= x;
    this.centerBottomBack_.x -= x;
    this.centerCenterBack_.x -= x;
    this.centerCenterFront_.x -= x;
};

/**
 * @private
 * @param {int} side Side.
 * @return {Array.<anychart.utils.geom.Rectangle>}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.createSides_ = function(side) {
    var res = new Array();
    var sidesEnum = anychart.plots.funnelPlot.drawingInfo.Sides;
    switch (side) {
        case sidesEnum.FRONT:
            res[0] = this.leftBottom_;
            res[1] = this.leftCenter_;
            res[2] = this.leftTop_;
            res[3] = this.centerTopFront_;
            res[4] = this.centerCenterFront_;
            res[5] = this.centerBottomFront_;
            break;
        case sidesEnum.RIGHT:
            res[0] = this.centerBottomFront_;
            res[1] = this.centerCenterFront_;
            res[2] = this.centerTopFront_;
            res[3] = this.rightTop_;
            res[4] = this.rightCenter_;
            res[5] = this.rightBottom_;
            break;
        case sidesEnum.BACK:
            res[0] = this.rightBottom_;
            res[1] = this.rightCenter_;
            res[2] = this.rightTop_;
            res[3] = this.centerTopBack_;
            res[4] = this.centerCenterBack_;
            res[5] = this.centerBottomBack_;
            break;
        case sidesEnum.LEFT:
            res[0] = this.centerBottomBack_;
            res[1] = this.centerCenterBack_;
            res[2] = this.centerTopBack_;
            res[3] = this.leftTop_;
            res[4] = this.leftCenter_;
            res[5] = this.leftBottom_;
            break;
        case sidesEnum.TOP:
            res[0] = this.leftTop_;
            res[1] = this.centerTopFront_;
            res[2] = this.rightTop_;
            res[3] = this.centerTopBack_;
            break;
        case sidesEnum.BOTTOM:
            res[0] = this.leftBottom_;
            res[1] = this.centerBottomFront_;
            res[2] = this.rightBottom_;
            res[3] = this.centerBottomBack_;
            break;
    }
    return res;
};

/**
 * @private
 * @param {Array.<anychart.utils.geom.Rectangle>} points Points.
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.
        prototype.getBound_ = function(points) {
    var lt = new anychart.utils.geom.Point();
    var rb = new anychart.utils.geom.Point();
    lt.x = lt.y = Number.MAX_VALUE;
    rb.x = rb.y = -Number.MAX_VALUE;
    var rect = new anychart.utils.geom.Rectangle();

    for (var i; i < points.length; i++) {
        lt.x = Math.min(lt.x, points[i].x);
        lt.y = Math.min(lt.y, points[i].y);
        rb.x = Math.max(rb.x, points[i].x);
        rb.y = Math.max(rb.y, points[i].y);
    }
    rect.x = lt.x;
    rect.y = lt.y;
    rect.width = rb.x - lt.x;
    rect.height = rb.y - lt.y;

    return rect;
};

//TODO: Drawing methods


//------------------------------------------------------------------------------
//
//                         FunnelDIWDrawingInfo class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo}
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDIWDrawingInfo = function() {
    anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo.call(this);
};
goog.inherits(anychart.plots.funnelPlot.drawingInfo.FunnelDIWDrawingInfo,
        anychart.plots.funnelPlot.drawingInfo.FunnelDrawingInfo);

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDIWDrawingInfo.
        prototype.setSectorPoint = function(point, inverted, w) {
    var h = point.series_.getHeight();
    var x = point.series_.getSeriesBounds().x;
    var y = point.series_.getSeriesBounds().y;

    this.bottomSize3D_ = point.bottomWidth_ *
            point.globalSettings_.getSize3D() * h;
    this.topSize3D_ = point.topWidth_ *
            point.globalSettings_.getSize3D() * h;

    if (!point.is3D()) {
        this.bottomSize3D_ = 0;
        this.topSize3D_ = 0;
    }

    this.centerBottomFront_.x =
            this.centerBottomBack_.x =
                    this.centerTopBack_.x =
                            this.centerTopFront_.x = x + w / 2;
    this.centerBottomFront_.y = inverted ? (y + h - point.bottomHeight_ * h) :
            (y + point.bottomHeight_ * h);
    this.centerTopFront_.y = inverted ? (y + h - point.topHeight_ * h) :
            (y + point.topHeight_ * h);
    this.centerBottomBack_.y = this.centerBottomFront_.y -
            this.bottomSize3D_ * 2;
    this.centerTopBack_.y = this.centerTopFront_.y - this.topSize3D_ * 2;

    this.leftBottom_.x = x + w / 2 - point.bottomWidth_ * w / 2;
    this.rightBottom_.x = x + w / 2 + point.bottomWidth_ * w / 2;
    this.leftTop_.x = x + w / 2 - point.topWidth_ * w / 2;
    this.rightTop_.x = x + w / 2 + point.topWidth_ * w / 2;
    this.leftBottom_.y =
            this.rightBottom_.y =
                    this.centerBottomFront_.y - this.bottomSize3D_;
    this.leftTop_.y =
            this.rightTop_.y =
                    this.centerTopFront_.y - this.topSize3D_;
};

/**
 * @inheritDoc
 */
anychart.plots.funnelPlot.drawingInfo.FunnelDIWDrawingInfo.
        prototype.setAnchorPoint = function(anchor, pos, inverted) {
    var anchorEnum = anychart.layout.Anchor;
    switch (anchor) {
        case anchorEnum.CENTER:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.leftBottom_.y + this.leftTop_.y) / 2;
            break;
        }
        case anchorEnum.CENTER_LEFT:
        {
            pos.x = (this.leftTop_.x + this.leftBottom_.x) / 2;
            pos.y = (this.leftTop_.y + this.leftBottom_.y) / 2;
            break;
        }
        case anchorEnum.CENTER_RIGHT:
        {
            pos.x = (this.rightTop_.x + this.rightBottom_.x) / 2;
            pos.y = (this.rightTop_.y + this.rightBottom_.y) / 2;
            break;
        }
        case anchorEnum.CENTER_BOTTOM:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.rightBottom_.y);
            break;
        }
        case anchorEnum.CENTER_TOP:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.rightTop_.y);
            break;
        }
        case anchorEnum.LEFT_TOP:
        {
            pos.x = (this.leftTop_.x);
            pos.y = (this.leftTop_.y);
            break;
        }
        case anchorEnum.RIGHT_TOP:
        {
            pos.x = (this.rightTop_.x);
            pos.y = (this.rightTop_.y);
            break;
        }
        case anchorEnum.LEFT_BOTTOM:
        {
            pos.x = (this.leftBottom_.x);
            pos.y = (this.leftBottom_.y);
            break;
        }
        case anchorEnum.RIGHT_BOTTOM:
        {
            pos.x = (this.rightBottom_.x);
            pos.y = (this.rightBottom_.y);
            break;
        }
    }
};


//------------------------------------------------------------------------------
//
//                              Sides enum
//
//------------------------------------------------------------------------------

/**
 * @enum {int}
 */
anychart.plots.funnelPlot.drawingInfo.Sides = {
    FRONT: 0,
    BACK: 1,
    TOP: 2,
    BOTTOM: 3,
    LEFT: 4,
    RIGHT: 5
};