/**
 *
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.funnelPlot.templates.FunnelPlotTemplatesMerger}</li>.
 * <ul>
 */
goog.provide('anychart.plots.funnelPlot.templates');
goog.require('anychart.plots.seriesPlot.templates');
goog.require('anychart.plots.seriesPlot.templates.DEFAULT_TEMPLATE');
goog.require('anychart.plots.funnelPlot.templates.DEFAULT_TEMPLATE');
//------------------------------------------------------------------------------
//
//        FunnelPlotTemplatesMerger class
//
//------------------------------------------------------------------------------
/**
 * Creates FunnelPlotTemplatesMerger.
 * @constructor
 * @extends {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.funnelPlot.templates.FunnelPlotTemplatesMerger = function() {
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.call(this);
};
goog.inherits(anychart.plots.funnelPlot.templates.FunnelPlotTemplatesMerger,
              anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger);
/**
 * @inheritDoc
 * @return {Object}
 */
anychart.plots.funnelPlot.templates.FunnelPlotTemplatesMerger.prototype.getPlotDefaultTemplate = function() {
    return this.mergeTemplates(
            goog.base(this, 'getPlotDefaultTemplate'),
            anychart.plots.funnelPlot.templates.DEFAULT_TEMPLATE);
};


