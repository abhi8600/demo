/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.AbstractPlot}</li>
 *  <li>@class {anychart.plots.BasePlot}</li>
 *  <li>@class {anychart.plots.PlotFactory}</li>
 * <ul>
 */

goog.provide('anychart.plots');

goog.require('anychart.visual.background');
goog.require('anychart.utils');
goog.require('anychart.formatting');
//------------------------------------------------------------------------------
//
//                          AbstractPlot.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.AbstractPlot = function () {
};
//------------------------------------------------------------------------------
//                          Chart views.
//------------------------------------------------------------------------------
/**
 * Plot main chart view setter.
 * @param {anychart.chartView.IAnyChart} value
 */
anychart.plots.AbstractPlot.prototype.setMainChartView = goog.abstractMethod;
/**
 * Plot chart view setter.
 * @param {anychart.chartView.ChartView} value
 */
anychart.plots.AbstractPlot.prototype.setChartView = goog.abstractMethod;
/**
 * Plot type setter.
 * @param {String} value
 */
anychart.plots.AbstractPlot.prototype.setPlotType = goog.abstractMethod;
//------------------------------------------------------------------------------
//                  Custom Attributes.
//------------------------------------------------------------------------------
/**
 * Add custom attribute to custom attributes storage.
 * @param {String} attributeName
 * @param {*} attributeValue
 */
anychart.plots.AbstractPlot.prototype.setPlotCustomAttribute = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Templates
//------------------------------------------------------------------------------
/**
 * Return plot template merger.
 * @return {anychart.templates.BaseTemplatesMerger}
 */
anychart.plots.AbstractPlot.prototype.getTemplatesMerger = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize plot settings.
 * @param {Object} config
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.AbstractPlot.prototype.deserialize = goog.abstractMethod;

/**
 * Десереализует дополнительные настройки после того как весь чарт уже десериализован и точки для рисования сформированы (drawingPoints_).
 * @protected
 * @param {Object} config JSON object with plot settings (chart node).
 */
anychart.plots.AbstractPlot.prototype.onAfterDeserializeData = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize plot SVG settings.
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.AbstractPlot.prototype.initializeSVG = goog.abstractMethod;

/**
 * Initialize plot containers.
 * @protected
 */
anychart.plots.AbstractPlot.prototype.initializeContainers_ = goog.abstractMethod;
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.plots.AbstractPlot.prototype.getSVGManager = goog.abstractMethod;
/**
 * Return plot sprite
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.AbstractPlot.prototype.getSprite = goog.abstractMethod;
/**
 * Initialize plot visual.
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.svg.SVGSprite} tooltipContainer
 * @param {Boolean} opt_drawBackground
 */
anychart.plots.AbstractPlot.prototype.initialize = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw plot.
 */
anychart.plots.AbstractPlot.prototype.draw = goog.abstractMethod;
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.AbstractPlot.prototype.calculateResize = goog.abstractMethod;
/**
 * Execute plot resize.
 */
anychart.plots.AbstractPlot.prototype.execResize = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Config cache.
//------------------------------------------------------------------------------
/**
 * Indicates, is plot support caching config.
 * @return {Boolean}
 */
anychart.plots.AbstractPlot.prototype.supportsCache = goog.abstractMethod;
/**
 * Return plot cached config.
 * @return {Object}
 */
anychart.plots.AbstractPlot.prototype.getCachedData = goog.abstractMethod;
/**
 * Cache plot config.
 */
anychart.plots.AbstractPlot.prototype.setCachedData = goog.abstractMethod;
//------------------------------------------------------------------------------
//                              Bounds.
//------------------------------------------------------------------------------
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.AbstractPlot.prototype.getBounds = goog.abstractMethod;
/**
 * Возвращает внутренние баунды плота. Содержат только данные, без каких либо
 * дополнительных елементов (например оси).
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.AbstractPlot.prototype.getInnerBounds = goog.abstractMethod;
/**
 * @param {anychart.utils.geom.Rectangle} bounds New bounds.
 */
anychart.plots.AbstractPlot.prototype.setBounds = goog.abstractMethod;
//------------------------------------------------------------------------------
//                            Tooltips.
//------------------------------------------------------------------------------
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.AbstractPlot.prototype.getTooltipsSprite = goog.abstractMethod;
//------------------------------------------------------------------------------
//                             Hover/Normal.
//------------------------------------------------------------------------------
/**
 * Заглушка, плот может быть тегом для контролов и трешхолдов,
 * которые вызывают setHover/setNormal по клику.
 */
anychart.plots.AbstractPlot.prototype.setHover = function () {
};
/**
 * Заглушка, плот может быть тегом для контролов и трешхолдов,
 * которые вызывают setHover/setNormal по клику.
 */
anychart.plots.AbstractPlot.prototype.setNormal = function () {
};
//------------------------------------------------------------------------------
//                          Controls.
//------------------------------------------------------------------------------
/**
 * Plot controls list setter.
 * @param {anychart.controls.ControlsList} value
 */
anychart.plots.AbstractPlot.prototype.setControlsList = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Legend control.
//------------------------------------------------------------------------------
/**
 * @return {anychart.controls.legend.SeriesIconSetting}
 */
anychart.plots.AbstractPlot.prototype.createIconSettings = goog.abstractMethod;
/**
 * @param {Object} itemFilter
 * @param {anychart.controls.legend.ILegendItemsContainer} container
 */
anychart.plots.AbstractPlot.prototype.setLegendData = goog.abstractMethod;
/**
 * @param {anychart.controls.legend.SeriesIconSetting} iconSettings
 * @param {anychart.controls.legend.ILegendItem} item
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} dx
 * @param {Number} dy
 */
anychart.plots.AbstractPlot.prototype.drawDataIcon = goog.abstractMethod;
//------------------------------------------------------------------------------
//                           Formatting.
//------------------------------------------------------------------------------
/**
 * Initialize plot formatting.
 * @protected
 */
anychart.plots.AbstractPlot.prototype.initFormatting = goog.abstractMethod;
/**
 * @return {String}
 */
anychart.plots.AbstractPlot.prototype.getTokenValue = goog.abstractMethod;
/**
 * @return {int}
 */
anychart.plots.AbstractPlot.prototype.getTokenType = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Points external methods
//------------------------------------------------------------------------------
anychart.plots.AbstractPlot.prototype.addPoint = function (seriesId, pointData) {
};
anychart.plots.AbstractPlot.prototype.addPointAt = function (seriesId, pointData) {
};
anychart.plots.AbstractPlot.prototype.removePoint = function (seriesId, pointId) {
};
anychart.plots.AbstractPlot.prototype.updatePoint = function (seriesId, pointId, pointData) {
};
anychart.plots.AbstractPlot.prototype.updatePointData = function (groupName, pointName, data) {
};
anychart.plots.AbstractPlot.prototype.highlightPoint = function (seriesId, pointId, highlight) {
};
anychart.plots.AbstractPlot.prototype.selectPointById = function (seriesId, pointId, select) {
};
//------------------------------------------------------------------------------
//                          Series external methods
//------------------------------------------------------------------------------
anychart.plots.AbstractPlot.prototype.addSeries = function (seriesData) {
};
anychart.plots.AbstractPlot.prototype.addSeriesAt = function (index, seriesData) {
};
anychart.plots.AbstractPlot.prototype.removeSeries = function (seriesId) {
};
anychart.plots.AbstractPlot.prototype.updateSeries = function (seriesId, seriesData) {
};
anychart.plots.AbstractPlot.prototype.showSeries = function (seriesId, seriesData) {
};
anychart.plots.AbstractPlot.prototype.highlightSeries = function (seriesId, highlight) {
};
anychart.plots.AbstractPlot.prototype.highlightCategory = function (categoryName, highlight) {
    anychart.errors.axesPlotOnly();
};
//------------------------------------------------------------------------------
//                           Serialization.
//------------------------------------------------------------------------------
anychart.plots.AbstractPlot.prototype.serialize = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Clear/Refresh
//------------------------------------------------------------------------------
/**
 * Clear plot data.
 */
anychart.plots.AbstractPlot.prototype.clear = goog.abstractMethod;
/**
 * Refresh plot visual.
 */
anychart.plots.AbstractPlot.prototype.refresh = goog.abstractMethod;
/**
 * Validate plot data.
 * @param {Object} data
 */
anychart.plots.AbstractPlot.prototype.checkNoData = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                          BasePlot class
//
//------------------------------------------------------------------------------
/**
 * BasePlot - самый базовый класс для всех plot-ов.
 *
 * Десериализует следующую структуру с настройками:
 * <code>
 * chart: {
 *     plot_type: "plotType",
 *     chart_settings: {
 *         data_plot_background: {} //anychart.visual.background.RectBackground
 *     },
 *     data_plot_settings: {},
 *     styles: {} //styles list,
 *     data: {
 *         attributes: {} //custom attributes
 *     }
 * }
 * </code>
 *
 * Поддерживает только токены форматирования кастомных аттрибутов.
 *
 * Содержит в себе:
 * 1. plotType - string plot type
 * 2. настройки и рисование data plot background
 * 3. кастомные аттрибуты
 * 4. форматирование
 * 5. контейнеры для element-ов: label, marker, tooltip
 * 6. логику сортировки спрайтов
 * 7. спрайты для данных и различные утилитные спрайты
 *
 * Для создания плота на основе BasePlot необходимо сделать следующее:
 *
 * WARNING: не забывайте вызывать super method-ы
 *
 * 1. Десериализация и оверрайд методов десериализации
 * 1.1. deserializeChartSettings_ десериализует настройки chart.chart_settings
 * 1.2. deserializeDataPlotSettings десериализует настройки chart.data_plot_settings
 * 1.3. deserializeData_ - десериализация данных плота. В метод приходит объект chart
 * 1.4. onAfterDeserializeData - метод вызывается после десериализации всех данных. В метод приходит объект chart
 * 1.5. Если совсем необходимо, а так же структура настроек отличается от "классической" (chart_settings, data_plot_settings, data),
 * то можно оверрайдить метод deserialize
 *
 * 2. Templates merger
 * Для нового плота необходимо заоверрайдить логику мерджа шаблонов.
 * Для этого заоверрайдите метод getTemplatesMerger. Этот метод должен возвращать экземпляр класса,
 * наследуемого от anychart.templates.BaseTemplatesMerger.
 * TODO: описание логики templates merger-а
 *
 * 3. Tokens
 * Для добавления токенов форматирования, например %DataPlotMyMagicToken, требуется сделать следующее:
 *
 * 3.1. Заоверрайдить метод initFormatting и прописать в нем дефолтные значения токенов в tokens hash:
 * <code>
 * anychart.plots.CustomPlot.prototype.initFormatting = function() {
 *     anychart.plots.CustomPlot.superClass_.initFormatting.call(this);
 *     this.tokensHash_.set('%DataPlotMyMagicToken', 0);
 * };
 * </code>
 * 3.2. Если требуется калькуляция значения при вызове getTokenValue, то оверрайдим getTokenValue:
 * <code>
 * anychart.plots.CustomPlot.prototype.getTokenValue = function(token) {
 *     this.tokensHash_.set('%DataPlotMyMagicToken', 100500);
 *     return anychart.plots.CustomPlot.superClass_.getTokenValue(this, token);
 * }
 * </code>
 * В идеале этот метод не должен оверрайдиться и все токены должны обновлять свои значения используя метод
 * setTokenValue(token, value) в разных местах.
 *
 *
 * 3.3. Для того, что бы форматинг знал тип токена, требуется заоверрайдить метод getTokenType.
 * Этот метод должен возвращать элемент энума anychart.formatting.TextFormatTokenType. e.g.
 * <code>
 * anychart.plots.CustomPlot.prototype.getTokenType = function(token) {
 *     if (token == '%DataPlotMyMagicToken') return anychart.formatting.TextFormatTokenType.NUMBER;
 *     return anychart.plots.CustomPlot.superClass_.getTokenType.call(this, token);
 * }
 * </code>
 *
 * WARNING: во всех методах форматирования при оверрайде ВСЕГДА вызывается метод суперкласса.
 * WARNING: токены DataPlot-а должны начинатья с префикса %DataPlot
 *
 * 4. Visualization
 * Для создания визуализации можно оверрайдить следующие методы:
 * 1. initializeBottomPlotContainers_() - для добавления дополнительных спрайтов, которые должны находиться под данными
 * 2. initializeTopPlotContainers_() - для добавления дополнительных спрайтов, которые должны находиться над данными, label-ами, marker-ами, но под тултипами.
 * Так же есть уже предсозданные спрайты и helper object-ы:
 * 1. _sprite, getSprite() - основной спрайт плота. Не рекомендуется оперировать с его детками, ибо может произойти коллапс и жестокая, кровавая месть
 * 2. svgManager_ - экземпляр класса anychart.svg.SVGManager - требуется для создания SVGSprite
 * 3. beforeDataSprite_ - спрайт под данными
 * 4. dataSprite_ - спрайт для данных
 * 5. afterDataSprite_ - спрайт над данными, но под label-ами и marker-ами
 * 6. bounds_, getBounds() - размеры plot-а
 *
 * Предпочтительно использовать их, нежели initializeBottomPlotContainers_ или initializeTopPlotContainers_.
 *
 * Основной метод для создания визуализации - initialize(bounds). ПЕРЕД СВОЕЙ МАГИЕЙ, ВЫЗОВИТЕ РОДИТЕЛЬСКУЮ МАГИЮ!
 * Не забывайте, что initialize должен вернуть this.sprite_
 *
 * @constructor
 * @extends {anychart.plots.AbstractPlot}
 * @implements {anychart.elements.IElementContainer}
 * @implements {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.plots.BasePlot = function () {
    anychart.plots.AbstractPlot.call(this);
    this.initFormatting();
    this.initialized_ = false;
    this.postInitialized_ = false;
};
goog.inherits(anychart.plots.BasePlot, anychart.plots.AbstractPlot);
//------------------------------------------------------------------------------
//                      Chart views.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.IAnyChart}
 */
anychart.plots.BasePlot.prototype.mainChartView_ = null;
/**
 * @param {anychart.chartView.IAnyChart} value
 */
anychart.plots.BasePlot.prototype.setMainChartView = function (value) {
    this.mainChartView_ = value;
};
/**
 * @private
 * @type {anychart.chartView.ChartView}
 */
anychart.plots.BasePlot.prototype.chartView_ = null;
/**
 * @param {anychart.chartView.ChartView} value
 */
anychart.plots.BasePlot.prototype.setChartView = function (value) {
    this.chartView_ = value;
};
//------------------------------------------------------------------------------
//                          Plot settings
//------------------------------------------------------------------------------
/**
 * Plot type.
 * @protected
 * @type {String}
 */
anychart.plots.BasePlot.prototype.plotType_ = null;
/**
 * @return {String}
 */
anychart.plots.BasePlot.prototype.getPlotType = function() {
    return this.plotType_;
};
/**
 * Set plot type.
 * @public
 * @param {string} value Plot type value.
 */
anychart.plots.BasePlot.prototype.setPlotType = function (value) {
    this.plotType_ = value;
};
//------------------------------------------------------------------------------
//                          Cached config
//------------------------------------------------------------------------------
/**
 * Chart config clone.
 * @private
 * @type {Object}
 */
anychart.plots.BasePlot.prototype.cachedData_ = null;

/**
 * @inheritDoc
 */
anychart.plots.BasePlot.prototype.supportsCache = function () {
    return true;
};

/**
 * @return {Object}
 */
anychart.plots.BasePlot.prototype.getCachedData = function () {
    return this.cachedData_;
};
/**
 *
 * @param {Object} value
 */
anychart.plots.BasePlot.prototype.setCachedData = function (value) {
    this.cachedData_ = anychart.utils.deserialization.copy(value);
};
//------------------------------------------------------------------------------
//                          Background.
//------------------------------------------------------------------------------
/**
 * Data plot background
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.plots.BasePlot.prototype.background_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.BasePlot.prototype.backgroundSprite_ = null;
/**
 * В базовом плоте рисование бэкграунда происходит опционально.
 * Сам же метод #drawBackground может вызываться плотами в различное время.
 * Поэтому контейнер вынесен отдельно, дабы всегда находился в нужном месте.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.backgroundContainer_ = null;
/**
 * Get bounds.
 * @return {anychart.utils.geom.Rectangle} bounds.
 */
anychart.plots.BasePlot.prototype.getBackgroundBounds = function () {
    return this.bounds_;
};
/**
 * Initialize plot background
 */
anychart.plots.BasePlot.prototype.initializeBackground = function () {
    if (this.background_ && this.background_.isEnabled()) {

        this.backgroundSprite_ = this.background_.initialize(this.svgManager_);
        this.backgroundSprite_.setPosition(.5, .5);
        if (IS_ANYCHART_DEBUG_MOD) this.backgroundSprite_.setId('PlotBackgroundSprite');

        this.backgroundContainer_.appendSprite(this.backgroundSprite_);
    }
};
/**
 * Draw plot background.
 * @protected
 */
anychart.plots.BasePlot.prototype.drawBackground_ = function () {
    if (this.backgroundSprite_)
        this.background_.draw(this.backgroundSprite_, this.getBackgroundBounds());
};
//------------------------------------------------------------------------------
//                              Bounds.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.BasePlot.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.BasePlot.prototype.getBounds = function () {
    return this.bounds_;
};
/**@inheritDoc*/
anychart.plots.BasePlot.prototype.getInnerBounds = function () {
    return this.bounds_;
};
/**
 * @param {anychart.utils.geom.Rectangle} bounds New bounds.
 */
anychart.plots.BasePlot.prototype.setBounds = function (bounds) {
    this.bounds_ = bounds;
};
//------------------------------------------------------------------------------
//                          Controls.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.ControlsList}
 */
anychart.plots.BasePlot.prototype.controlsList_ = null;
/**
 * @param {anychart.controls.ControlsList} value
 */
anychart.plots.BasePlot.prototype.setControlsList = function (value) {
    this.controlsList_ = value;
};
//------------------------------------------------------------------------------
//                          Legend control
//------------------------------------------------------------------------------
/**
 * @return {anychart.controls.legend.SeriesIconSetting}
 */
anychart.plots.BasePlot.prototype.createIconSettings = function () {
    return new anychart.controls.legend.SeriesIconSetting(this);
};
//------------------------------------------------------------------------------
//                          Containers
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.tooltipsSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.getTooltipsSprite = function () {
    return this.tooltipsSprite_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.labelsSprite_ = null;
/**
 * Return labels sprite.
 * <p>
 *     Note : Param label necessary is axesPlot only for label anchor XAxis.
 * </p>
 * @param {anychart.elements.label.BaseLabelElement} label Label element instance.
 * @return {anychart.svg.SVGSprite} Container for labels.
 */
anychart.plots.BasePlot.prototype.getLabelsSprite = function (label) {
    return this.labelsSprite_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.markersSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.getMarkersSprite = function () {
    return this.markersSprite_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.beforeDataSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.getBeforeDataSprite = function () {
    return this.beforeDataSprite_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.dataSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.afterDataSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.getAfterDataSprite = function () {
    return this.afterDataSprite_;
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * Deserialize plot.
 * @public
 * @param {Object} config JSON object with plot settings.
 * @return {Object}
 */
anychart.plots.BasePlot.prototype.deserialize = function (config, stylesList) {
    if (config == null) return null;

    if (anychart.utils.deserialization.hasProp(config, "chart_settings"))
        this.deserializeChartSettings_(anychart.utils.deserialization.getProp(config, "chart_settings"), stylesList);

    if (anychart.utils.deserialization.hasProp(config, "data_plot_settings"))
        this.deserializeDataPlotSettings(anychart.utils.deserialization.getProp(config, "data_plot_settings"));

    this.deserializeData_(config, stylesList);

    this.onAfterDeserializeData(config);

    return config;
};

/**
 * @protected
 * @param {anychart.plots.seriesPlot.data.BaseSeries} data
 * @param {anychart.styles.StylesList} styles
 */
anychart.plots.BasePlot.prototype.deserializeChartSettings_ = function (data, styles) {
    if (anychart.utils.deserialization.hasProp(data, 'data_plot_background')) {
        this.background_ = new anychart.visual.background.RectBackground();
        this.background_.deserialize(anychart.utils.deserialization.getProp(data, 'data_plot_background'));
    }
};

/**
 * @protected
 * @param {Object} data
 */
anychart.plots.BasePlot.prototype.deserializeDataPlotSettings = function (data) {
};

/**
 * Deserialize plot data. E.g. series for seriesPlot, etc...
 * @protected
 * @param {Object} data
 */
anychart.plots.BasePlot.prototype.deserializeData_ = function (data) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(data, 'data')) return;
    var dataNode = des.getProp(data, 'data');
    if (des.hasProp(dataNode, 'attributes')) {
        this.customAttributes_ = {};
        var attributes = des.getPropArray(des.getProp(dataNode, 'attributes'), 'attribute');
        var attributesCount = attributes.length;
        for (var i = 0; i < attributesCount; i++) {
            var attribute = attributes[i];
            if (des.hasProp(attribute, 'name')) {
                //old version support
                var tempVal;
                if (des.hasProp(attribute, 'custom_attribute_value'))
                    tempVal = des.getProp(attribute, 'custom_attribute_value');
                else if (des.hasProp(attribute, 'value'))
                    tempVal = des.getProp(attribute, 'value');

                if (tempVal) des.setProp(this.customAttributes_, '%' + des.getProp(attribute, 'name'), tempVal);
            }
        }
    }
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.BasePlot.prototype.initialized_ = null;
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.BasePlot.prototype.svgManager_ = null;
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.plots.BasePlot.prototype.getSVGManager = function () {
    return this.svgManager_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.BasePlot.prototype.getSprite = function () {
    return this.sprite_;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.BasePlot.prototype.initializeSVG = function (svgManager) {
    this.svgManager_ = svgManager;
    this.initializeContainers_();
};
anychart.plots.BasePlot.prototype.initializeTopPlotContainers_ = function () {
};
anychart.plots.BasePlot.prototype.initialize = function (bounds, tooltipContainer, opt_drawBackground) {
    this.sprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('DataPlot');
    this.tooltipsSprite_ = tooltipContainer;

    this.bounds_ = bounds;
    this.sprite_.setX(this.bounds_.x);
    this.sprite_.setY(this.bounds_.y);


    if (opt_drawBackground) this.initializeBackground();

    this.sprite_.appendSprite(this.backgroundContainer_);
    this.initializeBottomPlotContainers_();
    this.sprite_.appendSprite(this.beforeDataSprite_);
    this.sprite_.appendSprite(this.dataSprite_);
    this.sprite_.appendSprite(this.afterDataSprite_);
    this.sprite_.appendSprite(this.labelsSprite_);
    this.sprite_.appendSprite(this.markersSprite_);
    this.initializeTopPlotContainers_();
    this.setContainerPosition();

    this.initialized_ = true;

    return this.sprite_;
};
/**
 * Initialize plot containers.
 */
anychart.plots.BasePlot.prototype.initializeContainers_ = function () {
    this.backgroundContainer_ = new anychart.svg.SVGSprite(this.svgManager_);
    this.beforeDataSprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    this.dataSprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    this.afterDataSprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    this.labelsSprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    this.markersSprite_ = new anychart.svg.SVGSprite(this.svgManager_);

    if (IS_ANYCHART_DEBUG_MOD) {
        this.backgroundContainer_.setId('Plot background sprite container');
        this.beforeDataSprite_.setId('Plot before data sprite');
        this.dataSprite_.setId('Plot data sprite');
        this.afterDataSprite_.setId('Plot after data sprite');
        this.labelsSprite_.setId('Plot labels sprite');
        this.markersSprite_.setId('Plot markers sprite');
    }
};
/**
 * Set tooltip and scroll container positions.
 */
anychart.plots.BasePlot.prototype.setContainerPosition = function () {
    if (this.tooltipsSprite_) {
        this.tooltipsSprite_.setX(this.bounds_.x);
        this.tooltipsSprite_.setY(this.bounds_.y);
    }
};
/**
 * Initialize plot bottom level containers.
 * @protected
 */
anychart.plots.BasePlot.prototype.initializeBottomPlotContainers_ = function() {};
//------------------------------------------------------------------------------
//                  Post initialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.BasePlot.prototype.postInitialized_ = null;
/**
 * Plot post initialize.
 */
anychart.plots.BasePlot.prototype.postInitialize = function () {
};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.BasePlot.prototype.draw = function () {
    this.drawBackground_();
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.plots.BasePlot.prototype.calculateResize = function (bounds) {
    this.bounds_ = bounds.clone();
    //this.setContainerPosition(bounds);
};
/**
 * Execute plot resize.
 */
anychart.plots.BasePlot.prototype.execResize = function () {
    if (this.backgroundSprite_)
        this.background_.resize(
            this.backgroundSprite_,
            this.getBackgroundBounds());
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.plots.BasePlot.prototype.tokensHash_ = null;
anychart.plots.BasePlot.prototype.getTokensHash = function () {
    return this.tokensHash_;
};

/**
 * @protected
 */
anychart.plots.BasePlot.prototype.initFormatting = function () {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();

};
anychart.plots.BasePlot.prototype.setDefaultTokenValues = function () {
    this.tokensHash_.add('%DataPlotPointCount', 0);
    this.tokensHash_.add('%DataPlotYSum', 0);
    this.tokensHash_.add('%DataPlotYMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotYMin', Number.MIN_VALUE);
    this.tokensHash_.add('%DataPlotYAverage', 0);
    this.tokensHash_.add('%DataPlotBubbleSizeSum', 0);
    this.tokensHash_.add('%DataPlotBubbleMaxSize', -Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotBubbleMinSize', Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotBubbleSizeAverage', 0);

    this.tokensHash_.add('%DataPlotMaxYValuePointName', '');
    this.tokensHash_.add('%DataPlotMinYValuePointName', '');
    this.tokensHash_.add('%DataPlotMaxYValuePointSeriesName', '');
    this.tokensHash_.add('%DataPlotMinYValuePointSeriesName', '');
    this.tokensHash_.add('%DataPlotMaxYSumSeriesName', '');
    this.tokensHash_.add('%DataPlotMinYSumSeriesName', '');
};
anychart.plots.BasePlot.prototype.getTokenValue = function (token) {
    if (this.hasCustomAttribute(token)) return this.customAttributes_[token];

    if (!this.tokensHash_.has('%DataPlotBubbleSizeAverage'))
        this.tokensHash_.add(
            '%DataPlotBubbleSizeAverage',
            this.tokensHash_.get('%DataPlotBubbleSizeSum') /
                this.tokensHash_.get('%DataPlotBubblePointCount'));
    return this.tokensHash_.get(token);
};

anychart.plots.BasePlot.prototype.getTokenType = function (token) {
    if (this.hasCustomAttribute(token)) return anychart.formatting.TextFormatTokenType.TEXT;
    switch (token) {
        case '%DataPlotPointCount':
        case '%DataPlotBubblePointCount':
        case '%DataPlotYSum':
        case '%DataPlotYMax':
        case '%DataPlotYMin':
        case '%DataPlotYAverage':
        case '%DataPlotBubbleSizeSum':
        case '%DataPlotBubbleMaxSize':
        case '%DataPlotBubbleMinSize':
        case '%DataPlotBubbleSizeAverage':
            return anychart.formatting.TextFormatTokenType.NUMBER;
        case '%DataPlotMaxYValuePointName':
        case '%DataPlotMinYValuePointName':
        case '%DataPlotMaxYValuePointSeriesName':
        case '%DataPlotMinYValuePointSeriesName':
        case '%DataPlotMaxYSumSeriesName':
        case '%DataPlotMinYSumSeriesName':
            return anychart.formatting.TextFormatTokenType.TEXT;
    }
};
//------------------------------------------------------------------------------
//                          Custom attributes
//------------------------------------------------------------------------------

/**
 * Custom attributes hash map
 * @private
 * @type {Object.<string, *>}
 */
anychart.plots.BasePlot.prototype.customAttributes_ = null;
/**
 * @param {String} token
 * @return {Boolean}
 */
anychart.plots.BasePlot.prototype.hasCustomAttribute = function (token) {
    return Boolean(this.customAttributes_ && this.customAttributes_[token]);
};
/**
 *
 * @param {String} attributeName
 * @param {*} attributeValue
 */
anychart.plots.BasePlot.prototype.setPlotCustomAttribute = function (attributeName, attributeValue) {
    var des = anychart.utils.deserialization;
    if (!this.customAttributes_) this.customAttributes_ = {};
    this.customAttributes_[attributeName] = attributeValue;
    if (this.cachedData_ && des.hasProp(this.cachedData_, 'data')) {
        var data = des.getProp(this.cachedData_, 'data');

        var attributesNode;
        if (des.hasProp(data, 'attributes'))
            attributesNode = des.getProp(data, 'attributes');
        else {
            attributesNode = {};
            des.setProp(data, 'attributes', attributesNode);
        }

        var attributes;
        if (des.hasProp(attributesNode, 'attribute'))
            attributes = des.getPropArray(attributesNode, 'attribute');
        else {
            attributes = [];
            des.setProp(attributesNode, 'attribute', attributes);
        }

        var attribute = anychart.utils.JSONUtils.getNodeByName(attributes, attributeName);

        if (!attribute) attribute = {};

        if (goog.isArray(attribute)) //can be object
            attribute.push(this.createCustomAttribute(attributeName, attributeValue));
        else
            attribute = this.createCustomAttribute(attributeName, attributeValue);

        attributes.push(attribute);
    }
};
anychart.plots.BasePlot.prototype.createCustomAttribute = function (attributeName, attributeValue) {
    return {
        '#name#':'attribute',
        '#children#':[attributeValue],
        'name':attributeName,
        'value':attributeValue}
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.plots.BasePlot.prototype.serialize = function (opt_target) {
    if (opt_target == null || opt_target == undefined) opt_target = {};

    if (this.customAttributes_)
        for (var i in this.customAttributes_)
            opt_target[i] = this.customAttributes_[i];

    opt_target['YSum'] = this.tokensHash_.get('%DataPlotYSum');
    opt_target['YMax'] = this.tokensHash_.get('%DataPlotYMax');
    opt_target['YMin'] = this.tokensHash_.get('%DataPlotYMin');
    opt_target['YAverage'] = this.tokensHash_.get('%DataPlotYSum') / this.tokensHash_.get('%DataPlotYBasedPointsCount');

    opt_target['BubbleSizeSum'] = this.tokensHash_.get('%DataPlotBubbleSizeSum');
    opt_target['BubbleMaxSize'] = this.tokensHash_.get('%DataPlotBubbleSizeMax');
    opt_target['BubbleMinSize'] = this.tokensHash_.get('%DataPlotBubbleSizeMin');
    opt_target['BubbleSizeAverage'] = this.tokensHash_.get('%DataPlotBubbleSizeSum') / this.tokensHash_.get('%DataPlotBubblePointCount');

    opt_target['MaxYValuePointName'] = this.tokensHash_.get('%DataPlotMaxYValuePointName');
    opt_target['MaxYValuePointSeriesName'] = this.tokensHash_.get('%DataPlotMaxYValuePointSeriesName');
    opt_target['MinYValuePointName'] = this.tokensHash_.get('%DataPlotMinYValuePointName');
    opt_target['MinYValuePointSeriesName'] = this.tokensHash_.get('%DataPlotMinYValuePointSeriesName');
    opt_target['MaxYSumSeriesName'] = this.tokensHash_.get('%DataPlotMaxYSumSeriesName');
    opt_target['MinYSumSeriesName'] = this.tokensHash_.get('%DataPlotMinYSumSeriesName');
    opt_target['PointCount'] = this.tokensHash_.get('%DataPlotPointCount');
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          PlotFactory class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.PlotFactory = function () {
};
/**
 * Create single plot.
 * @param {anychart.chartView.ChartView} chartView
 * @param {anychart.chart.Chart} chart
 * @param {Object} config
 */
anychart.plots.PlotFactory.prototype.createSinglePLot = function (chartView, chart, config) {
    var chartNode, tempNode, isGauge;
    var des = anychart.utils.deserialization;

    if (des.hasProp(config, 'charts')) {
        tempNode = des.getProp(config, 'charts');
        if (des.hasProp(tempNode, 'chart')) {
            chartNode = des.getProp(tempNode, 'chart');
            isGauge = false;
        }
    } else {
        if (des.hasProp(config, 'gauges')) {
            tempNode = des.getProp(config, 'gauges');
            if (des.hasProp(tempNode, 'gauge')) {
                chartNode = des.getProp(tempNode, 'gauge');
                isGauge = true;
            }
        }
    }

    if (chartNode) this.createPlot_(chartView, chart, chartNode, isGauge);
};
/**
 * @private
 * @param {anychart.chartView.ChartView} chartView
 * @param {anychart.chart.Chart} chart
 * @param {Object} chartNode
 */
anychart.plots.PlotFactory.prototype.createPlot_ = function (chartView, chart, chartNode, isGauge) {
    var plotType, i, l, tempNode;
    var des = anychart.utils.deserialization;

    //plot template list
    var templatesList = des.hasProp(chartNode, 'template') ?
        chartView.getTemplatesContainer().getTemplatesChain(
            des.getLStringProp(chartNode, 'template')) : null;

    //plot_type from template
    if (!isGauge && templatesList) {
        for (i = 0, l = templatesList.length; i < l; i++) {
            if (des.hasProp(templatesList[i], 'chart')) {
                tempNode = des.getProp(templatesList[i], 'chart');
                if (des.hasProp(tempNode, 'plot_type'))
                    plotType = des.getLStringProp(tempNode, 'plot_type');
                break;
            }
        }
    }
    //plot_type from node
    if (des.hasProp(chartNode, 'plot_type'))
        plotType = des.getLStringProp(chartNode, 'plot_type');

    //plot class
    var plotClass = this.getPlotClass_(plotType, isGauge);
    if (!plotType) plotType = 'categorizedvertical';
    if (!plotClass) return;

    //plot creation
    var plot = new plotClass();
    plot.setPlotType(plotType);
    plot.setChartView(chartView);
    plot.setMainChartView(chartView.getMainChartView());

    //merge plot node
    var plotNode = this.mergePlotNode_(
        plot.getTemplatesMerger(),
        chartNode,
        templatesList);
    if (plot.supportsCache()) plot.setCachedData(plotNode);

    //chart
    chart.setPlot(plot);
    chart.deserialize(plotNode);
};
/**
 * @private
 * @param {anychart.templates.BaseTemplatesMerger} plotTemplateMerger
 * @param {Object} chartNode
 * @param {anychart.templates.GlobalTemplatesContainer} opt_templatesList
 */
anychart.plots.PlotFactory.prototype.mergePlotNode_ = function (plotTemplateMerger, chartNode, opt_templatesList) {
    var des = anychart.utils.deserialization;
    var chartDefaultTemplate = des.copy(anychart.templates.DEFAULT_TEMPLATE);
    var plotDefaultTemplate = plotTemplateMerger.getPlotDefaultTemplate();
    chartDefaultTemplate[plotTemplateMerger.getBaseNodeName()] =
        des.getProp(chartDefaultTemplate, 'chart');
    var template = plotTemplateMerger.mergeTemplates(chartDefaultTemplate,
        plotDefaultTemplate);

    if (opt_templatesList)
        for (var i = opt_templatesList.length - 1; i >= 0; i--)
            template = plotTemplateMerger.mergeTemplates(template,
                opt_templatesList[i]);


    return plotTemplateMerger.mergeWithChart(template, chartNode);
};
/**
 * @private
 * @param {String} plotType
 * @return {Function}
 */
anychart.plots.PlotFactory.prototype.getPlotClass_ = function (plotType, opt_isGauge) {
    var plotClass;
    if (opt_isGauge) {
        plotClass = anychart.plots.gaugePlot.GaugePlot;
    } else {
        switch (plotType) {
            case 'map':
                anychart.errors.featureNotSupported('Plot type Map');
                break;
            case 'table':
                anychart.errors.featureNotSupported('Plot type Table');
                break;
            case 'polar':
                plotClass = anychart.plots.radarPolarPlot.PolarPlot;
                break;
            case 'radar':
                plotClass = anychart.plots.radarPolarPlot.RadarPlot;
                break;
            case 'treemap':
                plotClass = anychart.plots.treeMapPlot.TreeMapPlot;
                break;
            case 'pie':
            case 'doughnut':
                plotClass = anychart.plots.piePlot.PiePlot;
                break;
            case 'funnel':
                plotClass = anychart.plots.funnelPlot.FunnelPlot;
                break;
            case 'categorizedvertical':
            case 'categorizedhorizontal':
            case 'categorizedbyseriesvertical':
            case 'categorizedbyserieshorizontal':
            case 'scatter':
            case 'heatmap':
            default:
                plotClass = anychart.plots.axesPlot.AxesPlot;
                break;
        }
    }
    return plotClass;
};
