//------------------------------------------------------------------------------
//
//                          QUnit
//
//------------------------------------------------------------------------------
/**
 * A boolean assertion, equivalent to JUnit's assertTrue. Passes if the first argument is truthy.
 * @param {*} statement can be a boolean or any other type, its boolean default is evaluated.
 * @param {string=} opt_comment Test message.
 */
function ok(statement, opt_comment){statement = opt_comment; opt_comment = statement}

/**
 * A comparison assertion, equivalent to JUnit's assertEquals.
 * @param {*} actual Actual value.
 * @param {*} expected Expected value
 * @param {String} message Test message.
 */
function equal(actual, expected, message){actual = expected = message}

/**
 * A deep recursive comparison assertion, working on primitive types, arrays and objects.
 * @param {Object | Array} actual Actual value.
 * @param {Object | Array} expected Expected value
 * @param {String} message Test message.
 */
function deepEqual(actual, expected, message) {actual = expected = message;}

/**
 * A deep recursive comparison assertion, working on primitive types, arrays and objects, with the result inverted, passing when some property isn't equal.
 * @param {Object | Array} actual Actual value.
 * @param {Object | Array} expected Expected value
 * @param {String} message Test message.
 */
function notDeepEqual(actual, expected, message) {actual = expected = message;}

/**
 * A stricter comparison assertion then equal
 * @param {Object} actual Actual value.
 * @param {Object} expected Expected value
 * @param {String} message Test message.
 */
function strictEqual(actual, expected, message) {actual = expected = message;}

/**
 * A stricter comparison assertion then notEqual.
 * @param {Object} actual Actual value.
 * @param {Object} expected Expected value
 * @param {String} message Test message.
 */
function notStrictEqual(actual, expected, message) {actual = expected = message;}


/**
 * Separate tests into modules.
 * @param {string} str
 */
function module(str){str = ""}

/**
 * Add a test to run.
 * @param {string} comment
 * @param {Function} func
 */
function test(comment, func){comment = func}{}

/**
 * Add an asynchronous test to run. The test must include a call to start().
 * @param {string} name
 * @param {*} expected
 * @param {*=} test
 */
function asyncTest(name, expected, test) {
    name = expected;
    expected = test;
}

/**
 * Start running tests again after the testrunner was stopped
 */
function start(){}

/**
 * Stop the testrunner to wait to async tests to run
 * @param {int=} opt_time
 */
function stop(opt_time){ opt_time = 1}
//----------------------------------------------------------------------------------------------------------------------
//
//                          SVG Elements
//
//----------------------------------------------------------------------------------------------------------------------
SVGElement = {};
SVGGradientElement = {};
SVGRadialGradientElement = {};
SVGLinearGradientElement = {};
SVGPatternElement = {};
SVGPathElement = {};
SVGPolylineElement = {};
SVGFilter = {};
//----------------------------------------------------------------------------------------------------------------------
//
//                          Console
//
//----------------------------------------------------------------------------------------------------------------------

console = {};
/**
 *
 * @param {*} value
 */
console.log = function(value){value = ''};

/**
 * console.log alias
 * @param value
 */
var trace = function(value, opt_val2) {value=opt_val2;};