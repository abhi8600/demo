/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.locale.dateTimeParser.ParserInfo}</li>
 *  <li>@class {anychart.locale.dateTimeParser.DateTimeParser}</li>
 *  <li>@class {anychart.locale.dateTimeParser.DateTimeParserAspects}.</li>
 * <ul>
 */
goog.provide('anychart.locale.dateTimeParser');
//------------------------------------------------------------------------------
//
//                          ParserInfo class.
//
//------------------------------------------------------------------------------
/**
 * Class represent information object, witch contains parsing process
 * information.
 * @constructor
 * @param {anychart.locale.DateTimeLocale} locale Date time locale.
 */
anychart.locale.dateTimeParser.ParserInfo = function(locale) {
    this.locale_ = locale;
};
//------------------------------------------------------------------------------
//                          Common properties.
//------------------------------------------------------------------------------
/**
 * Link to date/time locale.
 * @private
 * @type {anychart.locale.DateTimeLocale}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.locale_ = null;
/**
 * @return {anychart.locale.DateTimeLocale}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getLocale = function() {
    return this.locale_;
};
/**
 * Current parse position.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.position_ = null;
/**
 * @param {int} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setPosition = function(value) {
    this.position_ = value;
};
/**
 * @return {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getPosition = function() {
    return this.position_;
};
/**
 * @param {int} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.incrementPosition = function(value) {
    this.position_ += value;
};
//------------------------------------------------------------------------------
//                          Date properties.
//------------------------------------------------------------------------------
/**
 * Year value.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.year_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setYear = function(value) {
    this.year_ = value;
};
/**
 * Month value, range: 0-11.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.month_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setMonth = function(value) {
    this.month_ = value;
};

/**
 * Day value, range: 1-31.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.day_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setDay = function(value) {
    this.day_ = value;
};
/**
 * Hour value, range: 0-23.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.hour_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setHour = function(value) {
    this.hour_ = value;
};
/**
 * Minute value, range: 0-59.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.minute_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setMinute = function(value) {
    this.minute_ = value;
};
/**
 * Second value, range: 0-59.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.second_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setSecond = function(value) {
    this.second_ = value;
};
/**
 * Unix time interpretation.
 * @private
 * @type {Number}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.unixTime_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setUnixTime = function(value) {
    this.unixTime_ = value;
};
/**
 * Day of the week.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.dayOfWeek_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setDayOfWeek = function(value) {
    this.dayOfWeek_ = value;
};
/**
 * Hour count.
 * @private
 * @type {int}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.hour12_ = null;
/**
 * @param {Number} value
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.setHour12 = function(value) {
    this.hour12_ = value;
};
/**
 * Date value at string.
 * @private
 * @type {string}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.stringDate_ = null;
/**
 * @return {String}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getStringDate = function() {
    return this.stringDate_;
};

/**
 * Flag, is AM time.
 * @private
 * @type {boolean}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.isAm_ = null;
/**
 * @return {Boolean}
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.isAm = function() {
    return this.isAm_;
};
//------------------------------------------------------------------------------
//                              Parse.
//------------------------------------------------------------------------------
/**
 * Parse date from string value to Date instance.
 * <p>Note: In this method execute all aspects with was added at addAspect_
 * method</p>
 * @param {Array.<Function>} aspectsList Aspects list.
 * @param {Array.<int>} constants Constants.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.parseDate = function(aspectsList, constants) {
    var dateLength = this.stringDate_.length;
    var aspectsCount = aspectsList.length;

    this.position_ = constants[0];
    for (var i = 0; i < aspectsCount; i++) {
        aspectsList[i](this);
        this.position_ += constants[i + 1];
        if (this.position_ >= dateLength) break;
    }
};

/**
 * Create Date instance.
 * @return {Date} Date instance.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.createDate = function() {
    if (!isNaN(this.unixTime_)) return new Date(this.unixTime_ * 1000);

    if (this.hour_ == -1 && this.hour12_ != -1) {
        this.hour12_ = Math.floor(this.hour12_ % 12);
        this.hour_ = this.isAm_ ? this.hour12_ : (12 + this.hour12_);
    }

    var res = new Date(Date.UTC(
            this.year_ == -1 ? 1970 : this.year_,
            this.month_ == -1 ? 0 : this.month_,
            this.day_ == -1 ? 1 : this.day_,
            this.hour_ == -1 ? 0 : this.hour_,
            this.minute_ == -1 ? 0 : this.minute_,
            isNaN(this.second_) ? 0 : this.second_
            ));

    if (this.dayOfWeek_ != -1 && this.day_ == -1) {
        var tmp = this.dayOfWeek_ - res.getDay();
        res.setUTCDate(res.getDate() + tmp + (tmp < 0 ? 7 : 0));
    }
    return res;
};


/**
 * Return item index.
 * @param {Array.<String>} array Array of items.
 * @return {int} Item index.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getArrayItem = function(array) {
    var arrayLength = array.length;
    var des = anychart.utils.deserialization;
    for (var i = 0; i < arrayLength; i++) {
        var item = des.getLString(array[i]);
        if ((this.position_ + item.length) <= this.stringDate_.length &&
                des.getLString(this.stringDate_.substr(this.position_,
                        item.length)) == item)
            return i;
    }
    return -1;
};
//------------------------------------------------------------------------------
//                          Digital items.
//------------------------------------------------------------------------------
/**
 * Return item by max length.
 * @param {int} maxLength Max length.
 * @return {int} Item.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getLongDigitalItem = function(maxLength) {
    var res = this.stringDate_.charAt(this.position_);
    this.position_++;
    while (res.length < maxLength &&
            this.position_ < this.stringDate_.length &&
            !isNaN(Number(this.stringDate_.charAt(this.position_)))) {
        res += this.stringDate_.charAt(this.position_);
        this.position_++;
    }
    return Math.floor((Number(res)));
};
/**
 * Return item by length.
 * @param {int} length Length.
 * @return {int} Item.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.getDigitalItem = function(length) {
    var res = Math.floor(Number(this.stringDate_.substr(this.position_, length)));
    this.position_ += length;
    return res;
};
/**
 * Check is AM or PM time, change this.isAm_ flag.
 * @param {string} amString AM string sample.
 * @param {string} pmString PM string sample.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.checkAMPM = function(amString, pmString) {
    var des = anychart.utils.deserialization;
    var am = des.getLString(amString);
    var pm = des.getLString(pmString);

    if (this.stringDate_.substr(des.getLString(this.position_, am.length)) == am) {
        this.isAm_ = true;
        this.position_ += am.length;
    } else if (this.stringDate_.substr(des.getLString(this.position_, pm.length)) == pm) {
        this.isAm_ = false;
        this.position_ += pm.length;
    }
};
//------------------------------------------------------------------------------
//                          Clear info
//------------------------------------------------------------------------------
/**
 * Reset parser process info to default.
 */
anychart.locale.dateTimeParser.ParserInfo.prototype.clear = function() {
    this.year_ = -1;
    this.month_ = -1;
    this.day_ = -1;
    this.dayOfWeek_ = -1;
    this.hour_ = -1;
    this.hour12_ = -1;
    this.minute_ = -1;
    this.second_ = NaN;
    this.unixTime_ = NaN;
    this.isAm_ = true;
};
//------------------------------------------------------------------------------
//
//                    DateTimeParserAspects class.
//
//------------------------------------------------------------------------------
/**
 * Class represent date time parser aspects.
 * @enum {Function}
 */
anychart.locale.dateTimeParser.DateTimeParserAspects = {
    /**
     * %u
     * Parse unix time
     */
    'u': function(parserInfo) {
        var parsed = '';
        var position = parserInfo.getPosition();
        var strDate = parserInfo.getStringDate();

        while ((position < strDate.length) && !isNaN(Number(strDate.charAt(position)))) {
            parsed += strDate.charAt(position);
            position++;
        }

        parserInfo.setPosition(position);
        parserInfo.setUnixTime(Number(parsed));
    },
    /*
     * d - day of the month without leading zero
     * dd - day of the month with leading zero
     * ddd - short day name (day of the week)
     * dddd - long day name (day of the week)
     */
    /**
     * %d
     * Parse day of the month without leading zero
     */
    'd': function(parserInfo) {
        parserInfo.setDay(parserInfo.getLongDigitalItem(2));
    },
    /**
     * %dd
     * Parse day of the month with leading zero
     */
    'dd': function(parserInfo) {
        parserInfo.setDay(parserInfo.getDigitalItem(2));
    },
    /**
     * %ddd
     * Parse short day name (day of the week)
     */
    'ddd': function(parserInfo) {
        var index = parserInfo.getArrayItem(parserInfo.locale.getShortWeekDayNames());
        parserInfo.setDayOfWeek(index);
        parserInfo.incrementPosition(parserInfo.locale.shortWeekDayNames[index].length);
    },
    /**
     * %dddd
     * Parse long day name (day of the week)
     */
    'dddd': function(parserInfo) {
        var index = parserInfo.getArrayItem(parserInfo.locale.getWeekDayNames());
        parserInfo.setDayOfWeek(index);
        parserInfo.incrementPosition(parserInfo.locale.weekDayNames[index].length);
    },
    /*
     * M - month without leading zero
     * MM - month with leading zero
     * MMM - month as its abbr
     * MMMM - month as its name
     */
    /**
     * %M
     * Parse month without leading zero
     */
    'M': function(parserInfo) {
        parserInfo.setMonth(parserInfo.getLongDigitalItem(2) - 1);
    },
    /**
     * %MM
     * Parse month with leading zero
     */
    'MM': function(parserInfo) {
        parserInfo.setMonth(parserInfo.getDigitalItem(2) - 1);
    },
    /**
     * %MMM
     * Parse month as its abbr
     */
    'MMM': function(parserInfo) {
        var index = parserInfo.getArrayItem(parserInfo.getLocale().getShortMonthNames());
        parserInfo.setMonth(index);
        parserInfo.incrementPosition(parserInfo.getLocale().getShortMonthNames(index).length);
    },
    /**
     * %MMMM
     * Parse month as its name
     */
    'MMMM': function(parserInfo) {
        var index = parserInfo.getArrayItem(parserInfo.getLocale().getMonthNames());
        parserInfo.setMonth(index);
        parserInfo.incrementPosition(parserInfo.getLocale().getMonthNames[index].length);
    },
    /*
     * y - 2-digits without leading zero
     * yy - 2-digits with leading zero
     * yyyy - 4-digits year
     */
    /**
     * %y
     * Parse 2-digits year without leading zero
     */
    'y': function(parserInfo) {
        parserInfo.setYear(2000 + parserInfo.getLongDigitalItem(2));
    },
    /**
     * %yy
     * Parse 2-digits year with leading zero
     */
    'yy': function(parserInfo) {
        parserInfo.setYear(2000 + parserInfo.getDigitalItem(2));
    },
    /**
     * %yyyy
     * Parse 4-digits year
     */
    'yyyy': function(parserInfo) {
        parserInfo.setYear(parserInfo.getDigitalItem(4));
    },
    /*
     * h - hours without leading zero (12 hour clock)
     * hh - hours with leading zero (12 hour clock)
     * H - hours without leading zero (24 hour clock)
     * HH - hours with leading zero (24 hour clock)
     */
    /**
     * %h
     * Parse hours without leading zero (12 hour clock)
     */
    'h': function(parserInfo) {
        parserInfo.setHour12(parserInfo.getLongDigitalItem(2));
    },
    /**
     * %hh
     * Parse hours with leading zero (12 hour clock)
     */
    'hh': function(parserInfo) {
        parserInfo.setHour12(parserInfo.getDigitalItem(2));
    },
    /**
     * %H
     * Parse hours without leading zero (24 hour clock)
     */
    'H': function(parserInfo) {
        parserInfo.setHour(parserInfo.getLongDigitalItem(2));
    },
    /**
     * %HH
     * Parse hours with leading zero (24 hour clock)
     */
    'HH': function(parserInfo) {
        parserInfo.setHour(parserInfo.getDigitalItem(2));
    },
    /*
     * m - minutes without leading zero
     * mm - minutes with leading zero
     */
    /**
     * %m
     * Parse minutes without leading zero
     */
    'm': function(parserInfo) {
        parserInfo.setMinute(parserInfo.getLongDigitalItem(2));
    },
    /**
     * %mm
     * Parse minutes with leading zero
     */
    'mm': function(parserInfo) {
        parserInfo.setMinute(parserInfo.getDigitalItem(2));
    },
    /*
     * s - seconds without leading zero
     * ss - seconds with leading zero
     */
    /**
     * %s
     * Parse seconds without leading zero
     */
    's': function(parserInfo) {
        parserInfo.setSecond(parserInfo.getLongDigitalItem(2));
    },
    /**
     * %ss
     * Parse seconds with leading zero
     */
    'ss': function(parserInfo) {
        parserInfo.setSecond(parserInfo.getDigitalItem(2));
    },
    /*
     * t - one character time-marker string (A,P)
     * tt - multicharacter time-marker string (AM,PM)
     */
    /**
     * %t
     * Parse one character time-marker string (A,P)
     */
    't': function(parserInfo) {
        parserInfo.checkAMPM(parserInfo.getLocale().getShortAmString(), parserInfo.getLocale().getShortPmString());
    },
    /**
     * %tt
     * Parse multicharacter time-marker string (AM,PM)
     */
    'tt': function(parserInfo) {
        parserInfo.checkAMPM(parserInfo.getLocale().getAmString(), parserInfo.getLocale().getPmString());
    }
};
//------------------------------------------------------------------------------
//
//                           DateTimeParser class.
//
//------------------------------------------------------------------------------
/**
 * Class represent date time parser.
 * <p>
 * Usage sample:
 *  <code>
 *      var parser = new anychart.locale.dateTimeParser.DateTimeParser();
 *      var date = parser.getDate(stringDate);
 *      var timeStamp = parser.getDateAsNumber(stringDate);
 *  </code>
 * </p>
 */
anychart.locale.dateTimeParser.DateTimeParser = function() {

};
//-------------------------------------------------------------------------
//                              PARSER PROPERTIES
//-------------------------------------------------------------------------
/**
 * Default date/time format.
 * @type {string}
 */
anychart.locale.dateTimeParser.DateTimeParser.DEFAULT_FORMAT = '%yyyy/%MM/%dd %hh:%mm:%ss';

/**
 * List of aspects to execute.
 * @private
 * @type {Array.<Function>}
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.aspectsList_ = null;

/**
 * Constants list
 * @private
 * @type {Array.<string>}
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.consts_ = null;

/**
 * Information about parsing process,
 * @private
 * @type {anychart.locale.dateTimeParser.ParserInfo}
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.parserInfo_ = null;

/**
 * Initialize DateTime parser.
 * @param {anychart.locale.DateTimeLocale} locale Link to date/time locale.
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.initialize = function(locale) {
    this.aspectsList_ = [];
    this.consts_ = [];
    this.parserInfo_ = new anychart.locale.dateTimeParser.ParserInfo(locale);
    this.parseMask_(locale.getMask());
};

//-------------------------------------------------------------------------
//                  DATETIME FORMAT PARSING
//-------------------------------------------------------------------------
/**
 * Parsing format mask.
 * @private
 * @param {string} mask Date/time format mask.
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.parseMask_ = function(mask) {
    anychart.locale.dateTimeParser.DateTimeParser.DEFAULT_FORMAT = mask;
    var lexemes = mask.split('%');
    this.consts_ = [];
    this.consts_.push(lexemes[0].length); //if (lexems[0] != '')
    var lexemesCount = lexemes.length;
    for (var i = 1; i < lexemesCount; i++) {
        var letter = String(lexemes[i]).charAt(0);
        var newPattern = letter;
        var count = String(lexemes[i]).length;
        for (var j = 1; j < count; j++) {
            if (String(lexemes[i]).charAt(j) != letter) break;
            else newPattern += letter;
        }
        var aspect = anychart.locale.dateTimeParser.DateTimeParserAspects[newPattern];
        if (aspect) {
            this.aspectsList_.push(aspect);
            this.consts_.push(String(lexemes[i]).substring(j).length);
        } else {
            this.consts_[this.consts_.length - 1] += String(lexemes[i]).length;
        }
    }
};
//-------------------------------------------------------------------------
//                          DATE PARSING
//-------------------------------------------------------------------------
/**
 * Get date instance by string date value.
 * @param {string} stringDate String date value.
 * @return {Date} Date instance.
 *
 * <p>
 * Usage sample:
 *  <code>
 *      var parser = new anychart.locale.dateTimeParser.DateTimeParser();
 *      var date = parser.getDate(stringDate);
 *  </code>
 * </p>
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.getDate = function(stringDate) {
    this.parserInfo_.clear();
    this.parserInfo_.stringDate_ = stringDate;
    this.parserInfo_.parseDate(this.aspectsList_, this.consts_);
    return this.parserInfo_.createDate();
};

/**
 * Get date as milliseconds value.
 * @param {string} stringDate String date value.
 * @return {Number} Milliseconds value.
 *
 * <p>
 * Usage sample:
 *  <code>
 *      var parser = new anychart.locale.dateTimeParser.DateTimeParser();
 *      var timeStamp = parser.getDateAsNumber(stringDate);
 *  </code>
 * </p>
 */
anychart.locale.dateTimeParser.DateTimeParser.prototype.getDateAsNumber =
        function(stringDate) {
            return this.getDate(stringDate).getTime();
        };

