/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.layouters.anchored.AnchoredControlsCollection};</li>
 *  <li>@class {anychart.controls.layouters.anchored.FixedControlsCollection};</li>
 *  <li>@class {anychart.controls.layouters.anchored.FloatControlsCollection};</li>
 * <ul>
 */
goog.provide('anychart.controls.layouters.anchored');
goog.require('anychart.controls.layouters');
//------------------------------------------------------------------------------
//
//                           AnchoredControlsCollection class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.layouters.anchored.AnchoredControlsCollection = function() {
    anychart.controls.layouters.ControlsCollection.apply(this);
    this.controls = [];
};
goog.inherits(anychart.controls.layouters.anchored.AnchoredControlsCollection,
    anychart.controls.layouters.ControlsCollection);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {Array.<anychart.controls.controlsBase.Control>}
 */
anychart.controls.layouters.anchored.AnchoredControlsCollection.prototype.controls = null;
//------------------------------------------------------------------------------
//                          Adding controls
//------------------------------------------------------------------------------
/**
 * @param {anychart.controls.controlsBase.Control} control
 */
anychart.controls.layouters.anchored.AnchoredControlsCollection.prototype.addControl = function(control) {
    this.controls.push(control);
};
//------------------------------------------------------------------------------
//                          Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.anchored.AnchoredControlsCollection.prototype.finalizeLayout = function(plotBounds, chartBounds) {
    var controlCount = this.controls.length;

    for (var i = 0; i < controlCount; i++) {
        this.controls[i].getLayout().setMaxWidth(chartBounds.width);
        this.controls[i].getLayout().setMaxHeight(chartBounds.height);
        this.controls[i].setBounds(chartBounds, plotBounds);

        this.setControlPosition(
            this.controls[i],
            (this.controls[i].getLayout().isAlignByDataPlot()) ? plotBounds : chartBounds);
    }
    return true;
};
//------------------------------------------------------------------------------
//                          Position
//------------------------------------------------------------------------------

anychart.controls.layouters.anchored.AnchoredControlsCollection.prototype.setControlPosition = function(control, targetBounds) {
    var layout = control.getLayout();
    var hAlign, vAlign;
    var position = new anychart.utils.geom.Point(targetBounds.x, targetBounds.y);

    switch (layout.getAnchor()) {
        case anychart.layout.Anchor.CENTER:
            hAlign = anychart.layout.HorizontalAlign.CENTER;
            vAlign = anychart.layout.VerticalAlign.CENTER;
            position.x += targetBounds.width / 2;
            position.y += targetBounds.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_BOTTOM:
            hAlign = anychart.layout.HorizontalAlign.CENTER;
            vAlign = anychart.layout.VerticalAlign.TOP;
            position.x += targetBounds.width / 2;
            position.y += targetBounds.height;
            break;
        case anychart.layout.Anchor.CENTER_LEFT:
            hAlign = anychart.layout.HorizontalAlign.RIGHT;
            vAlign = anychart.layout.VerticalAlign.CENTER;
            position.y += targetBounds.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_RIGHT:
            hAlign = anychart.layout.HorizontalAlign.LEFT;
            vAlign = anychart.layout.VerticalAlign.CENTER;
            position.x += targetBounds.width;
            position.y += targetBounds.height / 2;
            break;
        case anychart.layout.Anchor.CENTER_TOP:
            hAlign = anychart.layout.HorizontalAlign.CENTER;
            vAlign = anychart.layout.VerticalAlign.BOTTOM;
            position.x += targetBounds.width / 2;
            break;
        case anychart.layout.Anchor.LEFT_BOTTOM:
            hAlign = anychart.layout.HorizontalAlign.RIGHT;
            vAlign = anychart.layout.VerticalAlign.TOP;
            position.y += targetBounds.height;
            break;
        case anychart.layout.Anchor.LEFT_TOP:
            hAlign = anychart.layout.HorizontalAlign.RIGHT;
            vAlign = anychart.layout.VerticalAlign.BOTTOM;
            break;
        case anychart.layout.Anchor.RIGHT_BOTTOM:
            hAlign = anychart.layout.HorizontalAlign.LEFT;
            vAlign = anychart.layout.VerticalAlign.TOP;
            position.x += targetBounds.width;
            position.y += targetBounds.height;
            break;
        case anychart.layout.Anchor.RIGHT_TOP:
            hAlign = anychart.layout.HorizontalAlign.LEFT;
            vAlign = anychart.layout.VerticalAlign.BOTTOM;
            position.x += targetBounds.width;
            break;
    }

    hAlign = (layout.getHAlign() == anychart.layout.HorizontalAlign.UNDEFINED) ? hAlign : layout.getHAlign();
    vAlign = (layout.getVAlign() == anychart.layout.HorizontalAlign.UNDEFINED) ? vAlign : layout.getVAlign();

    var width = control.getBounds().width;
    var height = control.getBounds().height;

    switch (hAlign) {
        case anychart.layout.HorizontalAlign.LEFT:
            position.x -= width + layout.getPixHorizontalPadding();
            break;
        case anychart.layout.HorizontalAlign.CENTER:
            position.x -= width / 2;
            break;
        case anychart.layout.HorizontalAlign.RIGHT:
            position.x += layout.getPixHorizontalPadding();
            break;
    }

    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP:
            position.y -= height + layout.getPixVerticalPadding();
            break;
        case anychart.layout.VerticalAlign.CENTER:
            position.y -= height / 2;
            break;
        case anychart.layout.VerticalAlign.BOTTOM:
            position.y += layout.getPixVerticalPadding();
            break;
    }

    control.getBounds().x = position.x;
    control.getBounds().y = position.y;
};
//------------------------------------------------------------------------------
//
//                           FixedControlsCollection class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.layouters.anchored.FixedControlsCollection = function() {
    anychart.controls.layouters.anchored.AnchoredControlsCollection.apply(this);
};
goog.inherits(anychart.controls.layouters.anchored.FixedControlsCollection,
    anychart.controls.layouters.anchored.AnchoredControlsCollection);
//------------------------------------------------------------------------------
//
//                           FloatControlsCollection class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.layouters.anchored.FloatControlsCollection = function() {
    anychart.controls.layouters.anchored.AnchoredControlsCollection.apply(this);
};
goog.inherits(anychart.controls.layouters.anchored.FloatControlsCollection,
    anychart.controls.layouters.anchored.AnchoredControlsCollection);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.isLayoutFinalized_ = false;
/**
 * @private
 * @type {anychart.chartView.IChartViewElement}
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.mainChartView_ = null;
/**
 * Current drag target.
 * @private
 * @type {anychart.controls.controlsBase.Control}
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.currentTarget_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.oldClientX_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.oldClientY_ = null;
//------------------------------------------------------------------------------
//                          Adding controls.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.addControl = function(control) {

    this.mainChartView_ = control.getPlotContainer();
    control.addEventListener(
        goog.events.EventType.MOUSEDOWN,
        this.mouseDownHandler_,
        false,
        this);

    goog.base(this, 'addControl', control);
};
//------------------------------------------------------------------------------
//                          Layout
//------------------------------------------------------------------------------
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.finalizeLayout = function(plotBounds, chartBounds) {
    if (!this.mainChartView_) return true;

    if (this.isLayoutFinalized_) {
        this.updateLayout_(plotBounds, chartBounds);
    } else {
        goog.base(this, 'finalizeLayout', plotBounds, chartBounds);
        this.isLayoutFinalized_ = true;
    }
    return true;
};

anychart.controls.layouters.anchored.FloatControlsCollection.prototype.updateLayout_ = function(plotBounds, chartBounds) {
    var rc = this.mainChartView_.getBounds();
    var controlCount = this.controls.length;
    for (var i = 0; i < controlCount; i++) {
        this.controls[i].getLayout().setMaxWidth(chartBounds.width);
        this.controls[i].getLayout().setMaxHeight(chartBounds.height);
        this.controls[i].setBounds(chartBounds, plotBounds);

        if (!isNaN(this.controls[i].getLayout().getFloatX() && !isNaN(this.controls[i].getLayout().getFloatY()))) {
            this.controls[i].getBounds().x = this.controls[i].getLayout().getFloatX() * rc.width;
            this.controls[i].getBounds().y = this.controls[i].getLayout().getFloatY() * rc.height;
        } else {
            this.setControlPosition(
                this.controls[i],
                (this.controls[i].getLayout().isAlignByDataPlot()) ? plotBounds : chartBounds);
        }
    }
};
//------------------------------------------------------------------------------
//                          Events
//------------------------------------------------------------------------------
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.layouters.anchored.FloatControlsCollection.prototype.mouseDownHandler_ = function(event) {
    this.currentTarget_ = event.target;

    this.oldClientX_ = event.clientX;
    this.oldClientY_ = event.clientY;


    goog.events.listen(
        this.currentTarget_.getSVGManager().getSVG(),
        goog.events.EventType.MOUSEMOVE,
        this.mouseMoveHandler_,
        false,
        this);

    goog.events.listen(
        this.currentTarget_.getSVGManager().getSVG(),
        goog.events.EventType.MOUSEUP,
        this.stopDrag_,
        false,
        this);
};

anychart.controls.layouters.anchored.FloatControlsCollection.prototype.mouseMoveHandler_ = function(event) {
    if (this.currentTarget_) {
        var chartBounds = this.mainChartView_.getBounds();
        var controlBounds = this.currentTarget_.getBounds();

        var pos = new anychart.utils.geom.Point(
            event.clientX - this.oldClientX_ + controlBounds.x,
            event.clientY - this.oldClientY_ + controlBounds.y);

        //x coordinate
        if (pos.x < chartBounds.getLeft()) {
            pos.x = chartBounds.getLeft();
        } else if ((pos.x + controlBounds.width) > chartBounds.getRight()) {
            pos.x = chartBounds.getRight() - controlBounds.width;
        }

        //y coordinate
        if (pos.y < chartBounds.getTop()) {
            pos.y = chartBounds.getTop();
        } else if (pos.y + controlBounds.height > chartBounds.getBottom()) {
            pos.y = chartBounds.getBottom() - controlBounds.height;
        }

        this.currentTarget_.getSprite().setPosition(pos.x, pos.y);
    }
};

anychart.controls.layouters.anchored.FloatControlsCollection.prototype.stopDrag_ = function(event) {
    goog.events.unlisten(
        this.currentTarget_.getSVGManager().getSVG(),
        goog.events.EventType.MOUSEMOVE,
        this.mouseMoveHandler_,
        false,
        this);

    goog.events.unlisten(
        this.currentTarget_.getSVGManager().getSVG(),
        goog.events.EventType.MOUSEUP,
        this.stopDrag_,
        false,
        this);

    this.oldClientX_ = event.clientX;
    this.oldClientY_ = event.clientY;

    var targetX = this.currentTarget_.getSprite().getX();
    var targetY = this.currentTarget_.getSprite().getY();

    this.currentTarget_.getBounds().x = targetX;
    this.currentTarget_.getBounds().y = targetY;

    this.currentTarget_.getLayout().setFloatX(targetX / this.mainChartView_.getBounds().width);
    this.currentTarget_.getLayout().setFloatY(targetY / this.mainChartView_.getBounds().height);

    this.currentTarget_ = null;
};