/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.resources.ResourcesManager}</li>
 *  <li>@class {anychart.resources.ResourceEntry}</li>
 *  <li>@class {anychart.resources.TextResourceEntry}</li>
 *  <li>@class {anychart.resources.XMLResourceEntry}</li>
 * <ul>
 */

goog.provide('anychart.resources');
goog.require('goog.Uri');

/**
 * ResourcesManager class. This class is used for loading external resources,
 * like xml files, text files and others.
 * Simple usage example:
 * <code>
 *  var manager = new anychart.resources.ResourcesManager();
 *  manager.addTextEntry("data/urlLoaderData/simple.txt", true);
 *  manager.addXMLEntry("data/urlLoaderData/simple.xml", true);
 *  manager.onLoad = function() {
 *      alert(anychart.resources.get('data/urlLoaderData/simple.txt'));
 *      alert(anychart.resources.get('data/urlLoaderData/simple.xml'));
 *  };
 *  manager.load();
 * </code>
 *
 * @constructor
 */
anychart.resources.ResourcesManager = function() {
    this.nonLoadedEntriesList_ = [];
    this.nonLoadedEntriesMap_ = {};
};

/**
 * Non-loaded entries hash map. Used for checking - is entry already added for
 * loading or not. Key is entry path.
 * @private
 * @type {Object.<string, anychart.resources.ResourceEntry>}
 */
anychart.resources.ResourcesManager.prototype.nonLoadedEntriesList_ = null;

/**
 * Non-loaded entries array. Used for checking is all entries loaded or not.
 * And for starting entries loading.
 * @private
 * @type {Array.<anychart.resources.ResourceEntry>}
 */
anychart.resources.ResourcesManager.prototype.nonLoadedEntriesMap_ = null;

/**
 * onLoad handler. Called then all entries loaded
 * @type {function()}
 */
anychart.resources.ResourcesManager.prototype.onLoad = null;

/**
 * onError handler. Called then ResourcesMaanger failed to load entries
 * @type {function(anychart.resources.ResourceEntry,anychart.resources.URLLoaderError)}
 */
anychart.resources.ResourcesManager.prototype.onError = null;

/**
 * Add new text entry for loading.
 * Sample:
 * <code>
 *  var manager = new anychart.resources.ResourcesManager();
 *  manager.addTextEntry("data/urlLoaderData/simple.txt", true);
 *  manager.onLoad = function() {
 *      alert(anychart.resources.get('data/urlLoaderData/simple.txt'));
 *  };
 *  manager.load();
 * </code>
 *
 * @param {String} path Path.
 * @param {Boolean} cache Cache entry or not. If cache disabled,
 * ResourcesManager will add XMLCallDate param with current timestamp to entry
 * path.
 */
anychart.resources.ResourcesManager.prototype.addTextEntry = function(path,
                                                                      cache) {
    if (this.canAddEntry_(path)) {
        var entry = new anychart.resources.TextResourceEntry();
        entry.initialize(path, cache);
        this.addEntry_(entry);
    }
};

/**
 * Add new XML entry for loading.
 * Sample:
 * <code>
 *  var manager = new anychart.resources.ResourcesManager();
 *  manager.addTextEntry("data/urlLoaderData/simple.xml", true);
 *  manager.onLoad = function() {
 *      // Document
 *      alert(anychart.resources.get('data/urlLoaderData/simple.txt'));
 *  };
 *  manager.load();
 * </code>
 *
 * @param {String} path Path.
 * @param {Boolean} cache Cache entry or not. If cache disabled,
 * ResourcesManager will add XMLCallDate param with current timestamp to entry
 * path.
 */
anychart.resources.ResourcesManager.prototype.addXMLEntry = function(path,
                                                                     cache) {
    if (!cache || this.canAddEntry_(path)) {
        var entry = new anychart.resources.XMLResourceEntry();
        entry.initialize(path, cache);
        this.addEntry_(entry);
    }
};

/**
 * Checks is entry already loaded or already added. If not - returns true
 * @private
 * @param {String} path Path.
 * @return {Boolean} boolean.
 */
anychart.resources.ResourcesManager.prototype.canAddEntry_ = function(path) {
    if (anychart.resources.contains(path)) return false;
    return this.nonLoadedEntriesMap_[path] == undefined;
};

/**
 * Register entry for loading.
 *
 * @private
 * @param {anychart.resources.ResourceEntry} entry Entry.
 */
anychart.resources.ResourcesManager.prototype.addEntry_ = function(entry) {
    this.nonLoadedEntriesList_.push(entry);
    this.nonLoadedEntriesMap_[entry.getPath()] = entry;
    var ths = this;
    //register onLoad handler
    entry.onLoad = function() {
        ths.onEntryLoad_.call(ths, this);
    };
    //register onError handler
    entry.onError = function(error) {
        ths.onEntryError_.call(ths, this, error);
    };
};

/**
 * Entry loaded handler
 *
 * @private
 * @param {anychart.resources.ResourceEntry} entry Entry.
 */
anychart.resources.ResourcesManager.prototype.onEntryLoad_ = function(entry) {
    delete this.nonLoadedEntriesMap_[entry.getPath()];
    var index = this.nonLoadedEntriesList_.indexOf(entry);
    if (index != -1)
        this.nonLoadedEntriesList_.splice(index, 1);

    if (this.onLoad && this.nonLoadedEntriesList_.length == 0)
        this.onLoad();
};

/**
 * Entry loading error handler
 *
 * @private
 * @param {anychart.resources.ResourceEntry} entry Entry.
 * @param {anychart.resources.URLLoaderError} error Error.
 */
anychart.resources.ResourcesManager.prototype.onEntryError_ = function(entry,
                                                                       error) {
    this.stopLoading();
    if (this.onError) this.onError(entry, error);
};

/**
 * Is all entries loaded or not.
 *
 * @return {Boolean} true if all entries loaded.
 */
anychart.resources.ResourcesManager.prototype.isLoaded = function() {
    return this.nonLoadedEntriesList_.length == 0;
};

/**
 * Start loading entries
 *
 * @see anychart.resources.ResourcesManager()
 */
anychart.resources.ResourcesManager.prototype.load = function() {
    for (var i = 0; i < this.nonLoadedEntriesList_.length; i++)
        this.nonLoadedEntriesList_[i].startLoading();
};

/**
 * Stop loading entries
 */
anychart.resources.ResourcesManager.prototype.stopLoading = function() {
    for (var i = 0; i < this.nonLoadedEntriesList_.length; i++)
        this.nonLoadedEntriesList_[i].stopLoading();

    this.nonLoadedEntriesList_ = [];
    this.nonLoadedEntriesMap_ = {};
};

//-----------------------------------------------------------------------------------
//                          GLOBAL LOADED RESOURCES LIST
//-----------------------------------------------------------------------------------

/**
 * Loaded resources hash map. Key is resource path, value - resource data
 * @type {Object.<string, *>}
 * @private
 */
anychart.resources.resources_ = {};

/**
 * Get loaded resource data
 *
 * @param {String} path Resource path.
 * @return {*} Resource data.
 */
anychart.resources.get = function(path) {
    return anychart.resources.resources_[path];
};

/**
 * Add loaded resource data
 *
 * @see anychart.resources.ResourceEntry.onEntryLoad_()
 * @param {String} path entry path.
 * @param {*} data entry data.
 */
anychart.resources.add = function(path, data) {
    anychart.resources.resources_[path] = data;
};

/**
 * Check is entry data available. Returns true if yes.
 * @param {String} path entry path.
 * @return {Boolean} Is entry data available.
 */
anychart.resources.contains = function(path) {
    return anychart.resources.resources_[path] != undefined;
};


//-----------------------------------------------------------------------------------
//                          BASE RESOURCE ENTRY
//-----------------------------------------------------------------------------------

/**
 * Base ABSTRACT resource entry.
 * @constructor
 */
anychart.resources.ResourceEntry = function() {
    this.cache_ = true;
    this.loader_ = new goog.net.XhrIo();
    this.loader_.setWithCredentials(true);
    goog.events.listen(this.loader_, goog.net.EventType.COMPLETE,
            this.onLoad_, null, this);
    if (this.onError)
    goog.events.listen(this.loader_, [goog.net.EventType.ERROR,
        goog.net.EventType.ABORT], this.onError, null, this);
};

/**
 * Entry loader
 *
 * @private
 * @type {goog.net.XhrIo}
 */
anychart.resources.ResourceEntry.prototype.loader_ = null;

/**
 * Cache entry data or not.
 * @private
 * @type {Boolean}
 */
anychart.resources.ResourceEntry.prototype.cache_ = true;

/**
 * Entry path
 * @private
 * @type {String}
 */
anychart.resources.ResourceEntry.prototype.path_ = null;

/**
 * Loader utl.
 * @private
 * @type {goog.Uri}
 */
anychart.resources.ResourceEntry.prototype.uri_ = null;

/**
 * Get entry path.
 * @return {String} Entry path.
 */
anychart.resources.ResourceEntry.prototype.getPath = function() {
    return this.path_;
};

/**
 * Initialize entry with path and cache settings.
 * @see anychart.resources.ResourcesManager.addTextEntry for params details
 *
 * @param {String} path Path.
 * @param {Boolean} cache Cache.
 */
anychart.resources.ResourceEntry.prototype.initialize = function(path, cache) {
    this.path_ = path;
    this.cache_ = cache;

    if (!this.cache_) {
        if (path.indexOf('?') != -1)
            path += '&';
        else
            path += '?';
        path += 'XMLCallDate=' + new Date().getTime().toString();
    }
    this.uri_ = new goog.Uri(path);
};

/**
 * Start entry loading
 */
anychart.resources.ResourceEntry.prototype.startLoading = function() {
    this.loader_.send(this.uri_, 'GET', null, 'anychart.com');
};

/**
 * Break entry loading
 */
anychart.resources.ResourceEntry.prototype.stopLoading = function() {
    this.loader_.abort();
};

/**
 * Get entry data.
 *
 * @protected
 * @return {*} Entry data.
 */
anychart.resources.ResourceEntry.prototype.getData = function() {
    goog.abstractMethod();
};

/**
 * Entry load handler.
 * @private
 */
anychart.resources.ResourceEntry.prototype.onLoad_ = function() {
    var data = this.getData();
    anychart.resources.add(this.path_, data);
    if (this.onLoad) this.onLoad.call(this, data);
};

/**
 * Entry onLoad handler
 * @type {function(*)}
 */
anychart.resources.ResourceEntry.prototype.onLoad = null;

/**
 * Entry onError handler
 * @type {function(anychart.resources.URLLoaderError)}
 */
anychart.resources.ResourceEntry.prototype.onError = null;

//-----------------------------------------------------------------------------------
//                          TEXT RESOURCE ENTRY
//-----------------------------------------------------------------------------------

/**
 * Text resource entry
 *
 * @constructor
 * @extends {anychart.resources.ResourceEntry}
 */
anychart.resources.TextResourceEntry = function() {
    anychart.resources.ResourceEntry.apply(this);
};
goog.inherits(anychart.resources.TextResourceEntry,
        anychart.resources.ResourceEntry);

/**
 * @inheritDoc
 */
anychart.resources.TextResourceEntry.prototype.getData = function() {
    return this.loader_.xhr_ ? this.loader_.xhr_.responseText : '';
};

//-----------------------------------------------------------------------------------
//                          XML RESOURCE ENTRY
//-----------------------------------------------------------------------------------

/**
 * XML resource entry
 *
 * @constructor
 * @extends {anychart.resources.ResourceEntry}
 */
anychart.resources.XMLResourceEntry = function() {
    anychart.resources.ResourceEntry.apply(this);
};
goog.inherits(anychart.resources.XMLResourceEntry,
        anychart.resources.ResourceEntry);

/**
 * @inheritDoc
 */
anychart.resources.XMLResourceEntry.prototype.getData = function() {
    return this.loader_.getResponseXml();
};
