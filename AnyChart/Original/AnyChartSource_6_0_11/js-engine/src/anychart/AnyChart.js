﻿/**
 * Copyright 2011 AnyChart. All rights reserved.
 * @fileoverview Contains classes:
 * <ul>
 *   <li>@class {anychart.WMode}.</li>
 *   <li>@class {anychart.RenderingType}.</li>
 *   <li>@class {anychart.AnyChart}.</li>
 * </ul>.
 */
goog.provide('anychart');

goog.require('goog.events');
goog.require('goog.net.XhrIo');

goog.require('anychart.render');
goog.require('anychart.render.flash');

goog.exportSymbol('anychart.scope.goog', goog);
goog.exportSymbol('anychart.scope.Event', goog.events.Event);
goog.exportSymbol('anychart.scope.EventTarget', goog.events.EventTarget);
goog.exportSymbol('anychart.scope.EventType', goog.events.EventType);
goog.exportSymbol('anychart.scope.XhrIo', goog.net.XhrIo);
//------------------------------------------------------------------------------
//
//                  WMod class.
//
//------------------------------------------------------------------------------
/**
 * WMode parameter changes Flash OBJECT and EMBED tags wMode
 * @export
 * @enum {String}
 */
anychart.WMode = {};
/**
 * In window mode SWF file is always above all elements in HTML, 
 * even z-index and position are ignored.
 * @export
 * @type {String}
 */
anychart.WMode.WINDOW = 'window';
/**
 * This mode allows to overlay movie with HTML elements,
 * but it may significantly slow down swf rendering.
 * @export
 * @type {String}
 */
anychart.WMode.TRANSPARENT = 'transparent';
/**
 * In this mode you can overlay swf using z-index,
 * but it may lead to unexpected rendering results.
 * @export
 * @type {String}
 */
anychart.WMode.OPAQUE = 'opaque';
//------------------------------------------------------------------------------
//
//                  RenderingType class.
//
//------------------------------------------------------------------------------
/**
 * Chart Rendering Types
 * @export
 * @enum {String}
 */
anychart.RenderingType = {};
/**
 * Flash engine is used in any case.
 * @export
 * @type {String}
 */
anychart.RenderingType.FLASH_ONLY = 'flash';
/**
 * HTML5 engine is used in any case.
 * @export
 * @type {String}
 */
anychart.RenderingType.SVG_ONLY = 'svg';
/**
 * Flash is the first option if both Flash and HTML5 are available.
 * @export
 * @type {String}
 */
anychart.RenderingType.FLASH_PREFERRED = 'flashPreferred';
/**
 * HTML5 engine is the first option if both Flash and HTML5 are available.
 * @export
 * @type {String}
 */
anychart.RenderingType.SVG_PREFERRED = 'svgPreferred';
//------------------------------------------------------------------------------
//
//                  AnyChart class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {String} opt_swfFile swf file path
 * @param {String} opt_preloaderSWFFile Preloader swf path
 * @param {String} opt_id Custom id
 */
anychart.AnyChart = function(opt_swfFile, opt_preloaderSWFFile, opt_id) {
    if (opt_id) this['id'] = opt_id;

    // apply global settings
    this['swfFile'] = AnyChart['swfFile'];
    this['preloaderSWFFile'] = AnyChart['preloaderSWFFile'];
    this['width'] = AnyChart['width'];
    this['height'] = AnyChart['height'];
    this['enableFirefoxPrintPreviewFix'] = AnyChart['enableFirefoxPrintPreviewFix'];
    this['enabledChartEvents'] = AnyChart['enabledChartEvents'];
    this['enabledChartMouseEvents'] = AnyChart['enabledChartMouseEvents'];
    this['renderingType'] = AnyChart['renderingType'];

    this['watermark'] = AnyChart['watermark'];
    this['messages']['preloaderInit'] = AnyChart['messages']['preloaderInit'];
    this['messages']['preloaderLoading'] = AnyChart['messages']['preloaderLoading'];
    this['messages']['init'] = AnyChart['messages']['init'];
    this['messages']['loadingConfig'] = AnyChart['messages']['loadingConfig'];
    this['messages']['loadingResources'] = AnyChart['messages']['loadingResources'];
    this['messages']['loadingTemplates'] = AnyChart['messages']['loadingTemplates'];
    this['messages']['noData'] = AnyChart['messages']['noData'];
    this['messages']['waitingForData'] = AnyChart['messages']['waitingForData'];

    // constructor param settings
    if (opt_swfFile) this['swfFile'] = opt_swfFile;
    if (opt_preloaderSWFFile) this['preloaderSWFFile'] = opt_preloaderSWFFile;
    this.listeners_ = {};

    // registering chart in hash map, key is chart id
    AnyChart.register_(this);
};
//------------------------------------------------------------------------------
//              Component global settings
//------------------------------------------------------------------------------
/**
 * @define {boolean}
 */
var IS_ANYCHART_DEBUG_MOD = true;
/**
 * @define {boolean}
 */
var IS_ORACLE_BUILD = false;
/**
 * AnyChart class Alias.
 * @export
 * @type {anychart.AnyChart}
 */
var AnyChart = anychart.AnyChart;
/**
 * Current component version.
 * @export
 * @define {string}
 */
AnyChart.VERSION = 'version';
//------------------------------------------------------------------------------
//              Global size settings
//------------------------------------------------------------------------------
/**
 * Set default chart width.
 * @export
 * @type {Number}
 */
AnyChart.width = 550;
/**
 * Set default chart height.
 * @export
 * @type {Number}
 */
AnyChart.height = 400;
//------------------------------------------------------------------------------
//              Global rendering settings
//------------------------------------------------------------------------------
/**
 * Set default rendering type.
 * @export
 * @type {String}
 */
AnyChart.renderingType = anychart.RenderingType.FLASH_ONLY;
//------------------------------------------------------------------------------
//              Global events settings
//------------------------------------------------------------------------------
/**
 * Enable/disable chart events.
 * @export
 * @type {Boolean}
 */
AnyChart.enabledChartEvents = true;
/**
 * Enable/disable mouse events.
 * @export
 * @type {Boolean}
 */
AnyChart.enabledChartMouseEvents = false;
//------------------------------------------------------------------------------
//              Global flash specific settings
//------------------------------------------------------------------------------
/**
 * Path to swf file (only for flash version).
 * @export
 * @type {String}
 */
AnyChart.swfFile = null;
/**
 * Path to preloader swf file (only for flash version).
 * @export
 * @type {String}
 */
AnyChart.preloaderSWFFile = null;
/**
 * Enables Firefox print preview fix.
 * @export
 * @type {Boolean}
 */
AnyChart.enableFirefoxPrintPreviewFix = true;
//------------------------------------------------------------------------------
//                         Chart identification
//------------------------------------------------------------------------------
/**
 * Stores instance identifier.
 * Can be specified in constructor (opt_id argument)
 * @export
 * @type {String}
 */
AnyChart.prototype.id = null;
/**
 * Stores all AnyChart instances.
 * @private
 * @type {Array.<anychart.AnyChart>}
 */
AnyChart.charts_ = [];
/**
 * Determines if there are any AnyChart instances.
 * @private
 * @return {Boolean} Returns true if there are any instances, false otherwise.
 */
AnyChart.hasCharts = function() {
    return AnyChart.charts_ && AnyChart.getNumCharts() > 0;
};
/**
 * Returns number of created AnyChart instances.
 * @private
 * @return {Number} Number of chart instances.
 */
AnyChart.getNumCharts = function() {
    return AnyChart.charts_.length;
};
/**
 * Returns an array with AnyChart instances.
 * @private
 * @return {Array.<anychart.AnyChart>} AnyChart instances array.
 */
AnyChart.getCharts = function() {
    return AnyChart.charts_;
};
/**
 * AnyChart instances hash map.
 * @private
 * @type {Object.<string>}
 */
AnyChart.chartsMap_ = {};
/**
 * Returns AnyChart instances hash map.
 * @private
 * @return {Object} AnyChart instances hash map.
 */
AnyChart.getChartsMap = function() {
    return AnyChart.chartsMap_;
};
//------------------------------------------------------------------------------
//                         Chart embed settings
//------------------------------------------------------------------------------
/**
 * Sets default rendering type.
 * @export
 * @type {int}
 */
AnyChart.prototype.renderingType = null;
/**
 * @private
 * @type {anychart.render.IChartRenderer}
 */
AnyChart.prototype.renderer_ = null;
/**
 * @return {anychart.render.IChartRenderer}
 */
AnyChart.prototype.getRenderer = function() {
    return this.renderer_;
};
/**
 * @private
 * @type {Boolean}
 */
AnyChart.prototype.isAddedToPage_ = false;
/**
 * Name of XML filq, with path, for a chart instance.
 * @export
 * @type {String}
 */
AnyChart.prototype.xmlFile = null;
/**
 * Name of JSON file, with path, for a chart instance.
 * @export
 * @type {String}
 */
AnyChart.prototype.jsonFile = null;
/**
 * XML data of a chart instance.
 * @export
 * @type {String}
 */
AnyChart.prototype.xmlData = null;
/**
 * JSON data of a chart instance.
 * @export
 * @type {String}
 */
AnyChart.prototype.jsonData = null;
//------------------------------------------------------------------------------
//                         Chart size settings
//------------------------------------------------------------------------------
/**
 * Width of a chart instance.
 * @export
 * @type {Number}
 */
AnyChart.prototype.width = null;
/**
 * Height of a chart instance.
 * @export
 * @type {Number}
 */
AnyChart.prototype.height = null;
//------------------------------------------------------------------------------
//                           Watermark.
//------------------------------------------------------------------------------
/**
 * @export
 * @type {String}
 */
AnyChart.prototype.watermark = null;
//------------------------------------------------------------------------------
//                   Chart flash specific settings
//------------------------------------------------------------------------------
/**
 * Path to swf file (only for flash version).
 * @export
 * @type {String}
 */
AnyChart.prototype.swfFile = null;
/**
 * Path to preloader swf file (only for flash version).
 * @export
 * @type {String}
 */
AnyChart.prototype.preloaderSWFFile = null;
/**
 * Enables Firefox print preview fix for AnyChart instance.
 * @export
 * @type {Boolean}
 */
AnyChart.prototype.enableFirefoxPrintPreviewFix = true;
/**
 * Sets instance wMode (only flash version).
 * @export
 * @type {String}
 */
AnyChart.prototype.wMode = null;
//------------------------------------------------------------------------------
//                   Chart events
//------------------------------------------------------------------------------
/**
 * Enables/disables chart events.
 * @export
 * @type {Boolean}
 */
AnyChart.prototype.enabledChartEvents = true;
/**
 * Enables/disables mouse events.
 * @export
 * @type {Boolean}
 */
AnyChart.prototype.enabledChartMouseEvents = false;
/**
 * Stores all eventListener of an instance.
 * @private
 * @type {Array.<Array.<Function>>}
 */
AnyChart.prototype.listeners_ = null;
//------------------------------------------------------------------------------
//                  Chart background settings
//------------------------------------------------------------------------------
/**
 * Set chart background color.
 * @export
 * @type {String}
 */
AnyChart.prototype.bgColor = "#FFFFFF";
//------------------------------------------------------------------------------
//                      Chart visible
//------------------------------------------------------------------------------
/**
 * Sets chart instance visibility.
 * @export
 * @type {Boolean}
 */
AnyChart.prototype.visible = true;
/**
 * Make chart instance visible.
 * @export
 */
AnyChart.prototype.show = function() {
    this['visible'] = true;
    this.renderer_.show();
};
/**
 * Hide chart instance.
 * @export
 */
AnyChart.prototype.hide = function() {
    this['visible'] = false;
    this.renderer_.hide();
};
//------------------------------------------------------------------------------
//                           Utils
//------------------------------------------------------------------------------
AnyChart.utils = {};
/**
 * Check if a parameter exists.
 * @private
 * @param {Object} target
 * @return {Boolean} Returns false, if target parameter doesn't exist.
 */
AnyChart.utils.hasProp = function(target) {
    return typeof target != 'undefined';
};
/**
 * Puts item in the end of arr array.
 * @private
 * @param {Array.<*>} arr
 * @param {*} item
 */
AnyChart.utils.push = function(arr, item) {
    arr[arr.length] = item;
};
//------------------------------------------------------------------------------
//                     Browser and os detection.
//------------------------------------------------------------------------------
/**
 * Contains client userAgent (browser) name.
 * @private
 * @type {String}
 */
var tmpUa_ = (navigator && navigator.userAgent) ? navigator.userAgent.toLowerCase() : null;
/**
 * Contains client OS name.
 * @private
 * @type {String}
 */
var tmpUp_ = (navigator && navigator.platform) ? navigator.platform.toLowerCase() : null;
/**
 * Contains browser OS and browser names.
 * @export
 * @constructor
 */
AnyChart.platform = {};
/**
 * Check if OS is Windows
 * @export
 * @type {Boolean}
 */
AnyChart.platform.isWin = tmpUp_ ? /win/.test(tmpUp_) : /win/.test(tmpUa_);
/**
 * Check if OS os iOS
 * @export
 * @type {Boolean}
 */
AnyChart.platform.isMac = !AnyChart.platform.isWin && (tmpUp_ ? /mac/.test(tmpUp_) : /mac/.test(tmpUa_));
/**
 * Check if DOM is available.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.hasDom = typeof document.getElementById != 'undefined' &&
        typeof document.getElementsByTagName != 'undefined' &&
        typeof document.createElement != 'undefiend';
/**
 * Check if browser is webKit based.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.webKit = /webkit/.test(tmpUa_) ? parseFloat(tmpUa_.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false;
/**
 * Check if browser is Internet Explorer.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.isIE = /msie/.test(tmpUa_);
/**
 * Check if browser is Firefox.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.isFirefox = /firefox/.test(tmpUa_);
/**
 * Checks what protocol is used (http or https).
 * @export
 * @type {String}
 */
AnyChart.platform.protocol = location.protocol == "https:" ? "https:" : "http:";
//------------------------------------------------------------------------------
//                     Flash player version
//------------------------------------------------------------------------------
/**
 * Validate flash player version
 * @export
 * @return {Boolean} Returns true, if FlashPlayer is 9 or above.
 */
AnyChart.isFlashPlayerValidVersion = function() {
    AnyChart.platform.flashPlayerVersion = [0,0,0];
    var d;
    if (navigator.plugins &&
            (typeof navigator.plugins["Shockwave Flash"] == "object")) {

        d = navigator.plugins["Shockwave Flash"].description;
        if (d && !(
                navigator.mimeTypes &&
                        navigator.mimeTypes[["application/x-shockwave-flash"]] &&
                        !navigator.mimeTypes["application/x-shockwave-flash"]['enabledPlugin'])) {

            d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");

            AnyChart.platform.flashPlayerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
            AnyChart.platform.flashPlayerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
            AnyChart.platform.flashPlayerVersion[2] = /[a-zA-Z]/.test(d) ?
                    parseInt(d.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) :
                    0;
        }
    } else if (typeof window.ActiveXObject != 'undefined') {
        try {
            var a = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            if (a) { // a will return null when ActiveX is disabled
                d = a.GetVariable("$version");
                if (d) {
                    d = d.split(" ")[1].split(",");
                    AnyChart.platform.flashPlayerVersion = [parseInt(d[0], 10),
                        parseInt(d[1], 10),
                        parseInt(d[2], 10)];
                }
            }
        }
        catch (e) {
        }
    }
    return AnyChart.platform.flashPlayerVersion != null && Number(AnyChart.platform.flashPlayerVersion[0]) >= 9;
};
/**
 * AnyChart.isFlashPlayerValidVersion() alias.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.hasRequiredVersion = AnyChart.isFlashPlayerValidVersion();
/**
 * Contains FlashPlayer version.
 * @export
 * @type {Array.<Number>}
 */
AnyChart.platform.flashPlayerVersion = AnyChart.platform.flashPlayerVersion;
/**
 * Checks if InternetExplorer for fix should be used.
 * @export
 * @type {Boolean}
 */
AnyChart.platform.needFormFix = AnyChart.platform.hasRequiredVersion &&
        AnyChart.platform.isIE &&
        AnyChart.platform.isWin;

if (AnyChart.platform.needFormFix) {
    //check player version
    AnyChart.platform.needFormFix = Number(AnyChart.platform.flashPlayerVersion[0]) == 9;
    AnyChart.platform.needFormFix = AnyChart.platform.needFormFix && Number(AnyChart.platform.flashPlayerVersion[1]) == 0;
    AnyChart.platform.needFormFix = AnyChart.platform.needFormFix && Number(AnyChart.platform.flashPlayerVersion[2]) < 115;
}
//------------------------------------------------------------------------------
//                       SVG Engine loading.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<Function>}
 */
AnyChart.engineReadyHendlers_ = [];
/**
 * Loads function when HTML5 engine is loaded.
 * @export
 * @param {Function} callback
 */
AnyChart.ready = function(callback) {
    if (goog.isFunction(callback))
        AnyChart.engineReadyHendlers_.push(callback);
};
/**
 * Inits HTML5 engine.
 * @export
 * @param {String} opt_path
 */
AnyChart.loadHTML5Engine = function(opt_path) {
    if (opt_path == null || opt_path == undefined)
        opt_path = AnyChart.getAnyChartBasePath();

    opt_path = opt_path + 'AnyChartHTML5.js';
    var engineLoader = new goog.net.XhrIo();
    engineLoader.setWithCredentials(true);
    goog.events.listen(
            engineLoader,
            goog.net.EventType.COMPLETE,
            AnyChart.onHTML5EngineReady_,
            null,
            AnyChart);

    goog.events.listen(
            engineLoader,
            [goog.net.EventType.ERROR, goog.net.EventType.ABORT],
            AnyChart.onHTML5EngineError_,
            null,
            AnyChart);

    engineLoader.send(opt_path, 'GET', null, 'anychart.com');
};
/**
 * @private
 */
AnyChart.onHTML5EngineReady_ = function(event) {
    var engineLoader = event.target;
    eval(engineLoader.getResponseText().toString());
    var count = AnyChart.engineReadyHendlers_.length;
    for (var i = 0; i < count; i++)
        AnyChart.engineReadyHendlers_[i]();
    AnyChart.destroyHTML5Loader_(engineLoader);
};
/**
 * @private
 */
AnyChart.onHTML5EngineError_ = function() {
    throw new Error('Can not find AnyChart html5 engine in JS folder.');
};
/**
 * @private
 * @param {goog.net.XhrIo} loader
 */
AnyChart.destroyHTML5Loader_ = function(loader) {
    goog.events.unlisten(
            loader,
            goog.net.EventType.COMPLETE,
            AnyChart.onHTML5EngineReady_,
            null,
            AnyChart);

    goog.events.unlisten(
            loader,
            [goog.net.EventType.ERROR, goog.net.EventType.ABORT],
            AnyChart.onHTML5EngineError_,
            null,
            AnyChart);

    loader = null;
};
/**
 * @private
 * @return {String}
 */
AnyChart.getAnyChartBasePath = function() {
    var doc = goog.global.document;
    var scripts = doc.getElementsByTagName('script');
    // Search backwards since the current script in almost all cases is  the one
    // that has base.js.
    for (var i = scripts.length - 1; i >= 0; --i) {
        var src = scripts[i].src;
        var qmark = src.lastIndexOf('?');
        var l = qmark == -1 ? src.length : qmark;
        var fullPath = src.substr(0, l);
        var array = fullPath.split('/');
        var fileName = array[array.length - 1].toLowerCase();
        var fileNameLength = fileName.length;

        if (fileName.indexOf('anychart') != -1 && fileName.substr(fileNameLength - 3, 3) == '.js') {
            return  fullPath.substr(0, l - fileNameLength);
        }
    }
    return null;
};
//------------------------------------------------------------------------------
//                  Global event listeners.
//------------------------------------------------------------------------------
/**
 * Calls fn after event is fired on target.
 * @param {Object} target
 * @param {Object} event
 * @param {Function} fn
 */
AnyChart.utils.attachEvent = function(target, event, fn) {
    target.attachEvent(event, fn);
    AnyChart.utils.push(AnyChart.utils.listeners, [target, event, fn]);
};
/**
 * Add global event listener
 * @export
 * @param {Object} event
 * @param {Function} fn
 */
AnyChart.utils.addGlobalEventListener = function(event, fn) {
    if (AnyChart.utils.hasProp(window.addEventListener)) window.addEventListener(event, fn, false);
    else if (AnyChart.utils.hasProp(document.addEventListener)) document.addEventListener(event, fn, false);
    else if (AnyChart.utils.hasProp(window.attachEvent)) AnyChart.utils.attachEvent(window, 'on' + event, fn);
    else if (typeof window['on' + event] == 'function') {
        var fnOld = window['on' + event];
        window['on' + event] = function() {
            fnOld();
            fn();
        };
    } else window['on' + event] = fn;
};
//------------------------------------------------------------------------------
//                           Dom load.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
AnyChart.utils.isDomLoaded_ = false;
/**
 * @private
 * @type {Array.<Function>}
 */
AnyChart.utils.domLoadListeners_ = [];
/**
 * @private
 * @param {Function} fn
 */
AnyChart.utils.addDomLoadEventListener_ = function(fn) {
    if (AnyChart.utils.isDomLoaded_) fn();
    else AnyChart.utils.domLoadListeners_.push(fn);
};
/**
 * Execute listeners loading
 * @private
 */
AnyChart.utils.execDomLoadListeners_ = function() {
    if (AnyChart.utils.isDomLoaded_) return;
    try {
        var t = document.getElementsByTagName("body")[0].appendChild(document.createElement("span"));
        t['parentNode'].removeChild(t);
    } catch (e) {
        return;
    }

    AnyChart.utils.isDomLoaded_ = true;

    var len = AnyChart.utils.domLoadListeners_.length;
    for (var i = 0; i < len; i++)
        AnyChart.utils.domLoadListeners_[i]();
};
/**
 * @private
 */
AnyChart.utils.registerDomLoad_ = function() {

    if (!AnyChart.platform.hasDom) return;
    if (typeof document['readyState'] != 'undefined' &&
            (document['readyState'] == "complete" || document.getElementsByTagName("body")[0] || document.body))
        AnyChart.utils.execDomLoadListeners_();

    if (!AnyChart.utils.isDomLoaded_) {
        if (document.addEventListener)
            document.addEventListener("DOMContentLoaded", AnyChart.utils.execDomLoadListeners_, false);

        if (AnyChart.platform.isIE && AnyChart.platform.isWin) {
            document.attachEvent("onreadystatechange", function() {
                if (document['readyState'] == "complete") {
                    document.detachEvent("onreadystatechange", arguments.callee);
                    AnyChart.utils.execDomLoadListeners_();
                }
            });

            if (window == top) {
                (function() {
                    if (AnyChart.utils.isDomLoaded_) {
                        return;
                    }
                    try {
                        document.documentElement.doScroll("left");
                    } catch (e) {
                        setTimeout(arguments.callee, 0);
                        return;
                    }
                    AnyChart.utils.execDomLoadListeners_();
                })();
            }
        }
        if (AnyChart.platform.webKit) {
            (function() {
                if (AnyChart.utils.isDomLoaded_) {
                    return;
                }
                if (!/loaded|complete/.test(document.readyState)) {
                    setTimeout(arguments.callee, 0);
                    return;
                }
                AnyChart.utils.execDomLoadListeners_();
            })();
        }

        goog.events.listen(window, 'load', AnyChart.utils.execDomLoadListeners_);
    }
};
/**
 * @private
 */
AnyChart.utils.registerDomLoad_();
//------------------------------------------------------------------------------
//                  Events calling.
//------------------------------------------------------------------------------
/**
 * Adds event handler to a chart instance.
 * @export
 * @param {String} chartId
 * @param {Object} eventObj
 */
AnyChart.dispatchEvent = function(chartId, eventObj) {
    if (!AnyChart.chartExists_(chartId) || eventObj == null) return;
    AnyChart.chartsMap_[chartId].dispatchEvent(eventObj);
};
//------------------------------------------------------------------------------
//                      Rendering type
//------------------------------------------------------------------------------
/**
 * Create renderer by rendering type.
 * @private
 */
AnyChart.prototype.createRenderer_ = function() {
    switch (this['renderingType']) {

        case anychart.RenderingType.SVG_ONLY:
            return new window['anychart']['render']['svg']['SVGRenderer']();
            break;

        case anychart.RenderingType.FLASH_ONLY:
            return new anychart.render.flash.FlashRenderer();
            break;

        default:
        case anychart.RenderingType.FLASH_PREFERRED:
            return AnyChart['platform']['hasRequiredVersion'] ?
                    new anychart.render.flash.FlashRenderer() :
                    new window['anychart']['render']['svg']['SVGRenderer']();
            break;

        case anychart.RenderingType.SVG_PREFERRED:
            return (window['SVGAngle'] != undefined) ?
                    new window['anychart']['render']['svg']['SVGRenderer']() :
                    new anychart.render.flash.FlashRenderer();
            break;
    }
};
//------------------------------------------------------------------------------
//              Charts hash map
//------------------------------------------------------------------------------
/**
 *
 * @private
 * @param {anychart.AnyChart} chart
 */
AnyChart.register_ = function(chart) {
    chart['id'] = chart['id'] == undefined ? ("__AnyChart___" + AnyChart.charts_.length) : chart['id'];
    AnyChart.chartsMap_[chart['id']] = chart;
    AnyChart.charts_.push(chart);
};
/**
 * Checks whether chart with the given chartId exists.
 * @private
 * @param {String} chartId
 * @return {Boolean} Returns true if chart with the given chartId exists.
 */
AnyChart.chartExists_ = function(chartId) {
    return chartId && AnyChart.chartsMap_ && AnyChart.chartsMap_[chartId];
};
/**
 * Returns chart instance by the given chartId.
 * @export
 * @param {String} chartId
 * @return {anychart.AnyChart} Returns chart instance by the given chartId.
 */
AnyChart.getChartById = function(chartId) {
    return AnyChart.chartExists_(chartId) ? AnyChart.chartsMap_[chartId] : null;
};
//------------------------------------------------------------------------------
//                          Dispose.
//------------------------------------------------------------------------------
/**
 * AnyChart.disposeVisual alias.
 * @export
 * @deprecated
 * @param {Object} obj
 * @param {String} id
 */
AnyChart.disposeFlashObject = function(obj, id) {
    AnyChart.disposeVisual(obj, id);
};
/**
 * Removes Flash object from HTML page.
 * @export
 * @param {Object} obj
 * @param {String} id
 */
AnyChart.disposeVisual = function(obj, id) {
    obj.disposeVisual(obj, id);
};
/**
 * Removes Flash object from HTML page.
 * @param {Object} obj
 * @param {String} id
 */
AnyChart.prototype.disposeVisual = function(obj, id) {
    this.renderer_.disposeVisual(obj, id);
};
//------------------------------------------------------------------------------
//                  JS Converter.
//------------------------------------------------------------------------------
/**
 * JSON to XML config converter.
 * @export
 * @constructor
 */
AnyChart.utils.JSConverter = {};
/**
 * Checks whether the given value is a valid attribue value.
 * @export
 * @param {*} prop
 * @return {Boolean} Returns true, in input is string, number or boolean.
 */
AnyChart.utils.JSConverter.isAttribute = function(prop) {
    var type = typeof prop;
    return type == "string" || type == "number" || type == "boolean";
};
/**
 * Checks whether parameter is an array.
 * @export
 * @param {*} prop
 * @return {Boolean} Returns true if parameter is array.
 */
AnyChart.utils.JSConverter.isArray = function(prop) {
    return typeof prop != "string" && typeof prop.length != "undefined";
};
/**
 * Converts JSON format into XML format.
 * @export
 * @param {Object} obj
 * @return {Object} Returns data in XML format.
 */
AnyChart.utils.JSConverter.convert = function(obj) {
    return AnyChart.utils.JSConverter.createNode_('anychart', obj);
};
/**
 * Creates data in XML format.
 * @private
 * @param {String} nodeName
 * @param {Object} data
 * @return {Object} Returns data in XML format.
 */
AnyChart.utils.JSConverter.createNode_ = function(nodeName, data) {
    var j, i;
    var res = "<" + nodeName;

    if (typeof data["functionName"] != "undefined") {
        data["function"] = data["functionName"];
        delete data["functionName"];
    }

    for (j in data) {
        if (j != "format" && j != "text" && j != "custom_attribute_value" && j != "attr" && AnyChart.utils.JSConverter.isAttribute(data[j])) {
            res += " " + j + "=\"" + data[j] + "\"";
        }
    }
    res += ">";
    for (j in data) {
        if (j == "arg" && AnyChart.utils.JSConverter.isArray(data[j])) {
            var args = data[j];
            for (i = 0; i < args.length; i++) {
                res += "<arg><![CDATA[" + args[i] + "]]></arg>";
            }
        } else if (j == "custom_attribute_value" || j == "attr") {
            res += "<![CDATA[" + data[j] + "]]>";
        } else if (j == "format" || j == "text") {
            res += "<" + j + "><![CDATA[" + data[j] + "]]></" + j + ">";
        } else if (AnyChart.utils.JSConverter.isArray(data[j])) {
            var nodes = data[j];
            for (i = 0; i < nodes.length; i++) {
                res += AnyChart.utils.JSConverter.createNode_(j, nodes[i]);
            }
        } else if (!AnyChart.utils.JSConverter.isAttribute(data[j])) {
            res += AnyChart.utils.JSConverter.createNode_(j, data[j]);
        }
    }
    res += "</" + nodeName + ">";
    return res;
};
//------------------------------------------------------------------------------
//                       Global messages
//------------------------------------------------------------------------------
/**
 * Sets chart messages.
 * @constructor
 */
AnyChart['messages'] = {};
/**
 * Preloader init message.
 * @type {String}
 */
AnyChart['messages']['preloaderInit'] = ' ';
/**
 * Preloader loading message.
 * @type {String}
 */
AnyChart['messages']['preloaderLoading'] = 'Loading... ';
/**
 * Component init message.
 * @type {String}
 */
AnyChart['messages']['init'] = 'Initializing...';
/**
 * Loading config message.
 * @type {String}
 */
AnyChart['messages']['loadingConfig'] = 'Loading config...';
/**
 * Loading resources message.
 * @type {String}
 */
AnyChart['messages']['loadingResources'] = 'Loading resources...';
/**
 * Loading templates message.
 * @type {String}
 */
AnyChart['messages']['loadingTemplates'] = 'Loading templates...';
/**
 * No data message.
 * @type {String}
 */
AnyChart['messages']['noData'] = 'No Data';
/**
 * No config set message.
 * @type {String}
 */
AnyChart['messages']['waitingForData'] = 'Waiting for data...';
/**
 * @private
 * @type {String}
 */
AnyChart['watermark'] = null;
//------------------------------------------------------------------------------
//                  Instance messages.
//------------------------------------------------------------------------------
/**
 * @export
 * @constructor
 */
AnyChart.prototype.messages = {};
/**
 * Preloader init message.
 * @type {String}
 */
AnyChart.prototype['messages']['preloaderInit'] = null;
/**
 * Preloader loading message.
 * @type {String}
 */
AnyChart.prototype['messages']['preloaderLoading'] = null;
/**
 * Component init message.
 * @type {String}
 */
AnyChart.prototype['messages']['init'] = null;
/**
 * Loading config message.
 * @type {String}
 */
AnyChart.prototype['messages']['loadingConfig'] = null;
/**
 * Loading resources message.
 * @type {String}
 */
AnyChart.prototype['messages']['loadingResources'] = null;
/**
 * Loading templates message.
 * @type {String}
 */
AnyChart.prototype['messages']['loadingTemplates'] = null;
/**
 * No data message.
 * @type {String}
 */
AnyChart.prototype['messages']['noData'] = null;
/**
 * No config set message.
 * @type {String}
 */
AnyChart.prototype['messages']['waitingForData'] = null;
//------------------------------------------------------------------------------
//      Events
//------------------------------------------------------------------------------
/**
 * Adds callback on event.
 * @export
 * @param {String}  event
 * @param {Function} callback
 */
AnyChart.prototype.addEventListener = function(event, callback) {
    if (this.listeners_[event] == null) this.listeners_[event] = [];
    this.listeners_[event].push(callback);
};
/**
 * Removes callback on event.
 * @export
 * @param {String} event
 * @param {Function} callback
 */
AnyChart.prototype.removeEventListener = function(event, callback) {
    if (this.listeners_ == null || this.listeners_[event] == null) return;

    var index = -1;
    for (var i = 0; i < this.listeners_[event].length; i++) {
        if (this.listeners_[event][i] == callback) {
            index = i;
            break;
        }
    }

    if (index != -1)
        this.listeners_[event].splice(index, 1);
};
/**
 * Dispatches event.
 * @export
 * @param {String} event
 */
AnyChart.prototype.dispatchEvent = function(event) {
    if (event == null || event['type'] == null) return;
    if (event['type'] == 'create') this.onBeforeChartCreate_();

    else if (event['type'] == 'draw') this.onBeforeChartDraw_();

    event['target'] = this;

    if (this.listeners_ == null || this.listeners_[event['type']] == null) return;

    var len = this.listeners_[event['type']].length;
    for (var i = 0; i < len; i++)  this.listeners_[event['type']][i](event);

};
/**@private*/
AnyChart.prototype.onBeforeChartCreate_ = function() {
    this.renderer_.onBeforeChartCreate();
};
/**@private*/
AnyChart.prototype.onBeforeChartDraw_ = function() {
    this.renderer_.onBeforeChartDraw();
};
//------------------------------------------------------------------------------
//
//                         API
//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//                          Embed
//------------------------------------------------------------------------------
/**
 * Checks whether the component can be displayed on HTML page.
 * @export
 * @inheritDoc
 * @return {Boolean} False if component can't be displayed on HTML page.
 */
AnyChart.prototype.canWrite = function() {
    return this.renderer_.canWrite();
};
/**
 * Adds chart to a HTML page. Parameter sets an id of a chart container in HTML page.
 * @export
 * @inheritDoc
 * @param {String}
 */
AnyChart.prototype.write = function(opt_target) {
    //create chart renderer
    this.renderer_ = this.createRenderer_();
    this.renderer_.init(this);

    if (!this.canWrite()) return;

    if (opt_target == undefined) {
        opt_target = "__chart_generated_container__" + this['id'];
        document.write("<div id=\"" + opt_target + "\"></div>");
    }

    var ths = this;

    AnyChart.utils.addDomLoadEventListener_(function() {
        if (typeof opt_target == "string")
            ths.write_(document.getElementById(opt_target));
        else
            ths.write_(opt_target);
    });
};
/**@private*/
AnyChart.prototype.write_ = function(target) {
    this.isAddedToPage_ = this.renderer_.write(target);
    if (this.isAddedToPage_ && this.canSetConfigAfterWrite()) {
        if (this['xmlFile'] != null) this.setXMLFile(this['xmlFile']);
        else if (this['jsonFile'] != null) this.setJSONFile(this['jsonFile']);
        else if (this['xmlData'] != null) this.setXMLDataFromString(this['xmlData']);
        else if (this['jsonData'] != null) this.setJSONData(this['jsonData']);
    }
};
/**
 * @export
 * @inheritDoc
 */
AnyChart.prototype.canSetConfigAfterWrite = function() {
    return this.renderer_.canSetConfigAfterWrite();
};
/**
 * Update fire fox print fix after change enableFirefoxPrintPreviewFix value.
 * @export
 */
AnyChart.prototype.updatePrintForFirefox = function() {
    this.renderer_.updatePrintForFirefox();
};
/**
 * Sets width and height of a chart instance.
 * @export
 * @param {Number} width Chart width.
 * @param {Number} height Chart height.
 */
AnyChart.prototype.setSize = function(width, height) {
    this['width'] = width;
    this['height'] = height;
    this.renderer_.setSize(width, height);
};
goog.exportSymbol('AnyChart.prototype.resize', AnyChart.prototype.setSize);
/**
 * Removes chart instance completely.
 * @export
 */
AnyChart.prototype.remove = function() {
    this.renderer_.remove();
};
/**
 * Removes chart from the screen, but instances remains.
 * @export
 */
AnyChart.prototype.clear = function() {
    this.renderer_.clear();
};
/**
 * Refreshes data (used along with update methods).
 * @export
 */
AnyChart.prototype.refresh = function() {
    this.renderer_.refresh();
};
/**
 * Updates data node.
 * @export
 */
AnyChart.prototype.updateData = function(path, data) {
    this.renderer_.updateData(path, data);
};
/**
 * Shows loading animation on a chart.
 * @export
 * @param opt_message
 */
AnyChart.prototype.setLoading = function(opt_message) {
    this.renderer_.setLoading(opt_message);
};
/**
 * Returns an object with chart instance data collection.
 * @export
 * @return {Object} Returns an object with instance data.
 */
AnyChart.prototype.getInformation = function() {
    return this.renderer_.getInformation();
};
//------------------------------------------------------------------------------
//                          XML config setters
//------------------------------------------------------------------------------
/**
 * Sets an address of config XML file.
 * @export
 * @param {String} path
 */
AnyChart.prototype.setXMLFile = function(path) {
    this['xmlFile'] = path;
    if (this.isAddedToPage_) this.renderer_.setXMLFile(path);
};
/**
 * Sets config data in XML format.
 * @export
 * @param {Object} data
 */
AnyChart.prototype.setXMLData = function(data) {
    data = String(data);
    this['xmlData'] = data;
    if (this.isAddedToPage_) this.renderer_.setXMLData(this['xmlData']);
};
/**
 * setXMLData alias.
 * @deprecated
 * @export
 * @param {String} data
 */
AnyChart.prototype.setXMLDataFromString = function(data) {
    if (this.isAddedToPage_) this.renderer_.setXMLDataFromString(data);
};
/**
 * setXMLFile alias.
 * @deprecated
 * @export
 * @param {String} data
 */
AnyChart.prototype.setXMLDataFromURL = function(data) {
    if (this.isAddedToPage_) this.renderer_.setXMLDataFromURL(data);
};
/**
 * Sets XML file for a dashboard view with a given viewId.
 * @export
 * @param {String} viewId
 * @param {String} path
 */
AnyChart.prototype.setViewXMLFile = function(viewId, path) {
    this.renderer_.setViewXMLFile(viewId, path);
};
/**
 * Sets config XML for a dashboard view with a given viewId.
 * @export
 * @param {String} viewId
 * @param {Object} data
 */
AnyChart.prototype.setViewData = function(viewId, data) {
    this.renderer_.setViewData(viewId, data);
};
//------------------------------------------------------------------------------
//                  JSON config setters.
//------------------------------------------------------------------------------
/**
 * Sets JSON config file.
 * @export
 * @param path
 */
AnyChart.prototype.setJSONFile = function(path) {
    this['jsonFile'] = path;
    if (this.isAddedToPage_) this.renderer_.setJSONFile(path);
};
/**
 * Sets config data in JSON format.
 * @export
 * @param data
 */
AnyChart.prototype.setJSONData = function(data) {
    this['jsonData'] = data;
    if (this.isAddedToPage_) this.renderer_.setJSONData(this['jsonData']);
};
// Old API support
goog.exportSymbol('AnyChart.prototype.setJSData', AnyChart.prototype.setJSONData);
//------------------------------------------------------------------------------
//                  Smart data setters.
//------------------------------------------------------------------------------
/**
 * Sets config in XML or JSON format.
 * @export
 * @param {Object} data
 */
AnyChart.prototype.setData = function(data) {
    if (data == null) return;
    if (this.isXMLData_(data)) {
        this['xmlData'] = String(data);
        if (this.isAddedToPage_) this.renderer_.setXMLData(this['xmlData']);
    } else {
        this['jsonData'] = data;
        if (this.isAddedToPage_) this.renderer_.setJSONData(this['jsonData']);
    }
};
/**
 * Checks whether parameter is an XML.
 * @private
 * @param {Object} data
 * @return {Boolean} Returns true, if format is an XML.
 */
AnyChart.prototype.isXMLData_ = function(data) {
    var strData = String(data);
    while (strData.charAt(0) == ' ' && strData.length > 0) strData = strData.substr(1);
    return strData.charAt(0) == '<';
};
//------------------------------------------------------------------------------
//                          Point
//------------------------------------------------------------------------------
/**
 * Adds point with pointData in XML format in the end of series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {Object} pointData
 */
AnyChart.prototype.addPoint = function(seriesId, pointData) {
    this.renderer_.addPoint(seriesId, pointData);
};
/**
 * Adds point with pointData in XML format at pointIndex position to the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {Number} pointIndex
 * @param {Object} pointData
 */
AnyChart.prototype.addPointAt = function(seriesId, pointIndex, pointData) {
    this.renderer_.addPointAt(seriesId, pointIndex, pointData);
};
/**
 * Removes point with the given pointId from the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {String} pointId
 */
AnyChart.prototype.removePoint = function(seriesId, pointId) {
    this.renderer_.removePoint(seriesId, pointId);
};
/**
 * Sets new XML data to the point with the given pointId in the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Object} pointData
 */
AnyChart.prototype.updatePoint = function(seriesId, pointId, pointData) {
    this.renderer_.updatePoint(seriesId, pointId, pointData);
};
/**
 * Updates pointData with the given pointName in gauge.
 * @export
 * @param {String} groupName
 * @param {String} pointName
 * @param {Object} pointData
 */
AnyChart.prototype.updatePointData = function(groupName, pointName, pointData) {
    this.renderer_.updatePointData(groupName, pointName, pointData);
};
/**
 * Highlights/dehighlights the point with the given pointId in the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Boolean} highlight
 */
AnyChart.prototype.highlightPoint = function(seriesId, pointId, highlight) {
    this.renderer_.highlightPoint(seriesId, pointId, highlight);
};
/**
 * Selects/deselects the point with the given pointId in the series with the given seriesId..
 * @export
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Boolean} select
 */
AnyChart.prototype.selectPoint = function(seriesId, pointId, select) {
    this.renderer_.selectPoint(seriesId, pointId, select);
};
/**
 * Updates the value of the point with the given pointId in the series with the given seriesId: sets it to newValue animating it with animation defined in animationSettings.
 * @export
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Number} newValue
 * @param {Object} animationSettings
 */
AnyChart.prototype.updatePointWithAnimation = function(seriesId, pointId, newValue, animationSettings) {
    this.renderer_.updatePointWithAnimation(seriesId, pointId, newValue, animationSettings);
};
/**
 * Re-animates the chart.
 * @export
 */
AnyChart.prototype.startPointsAnimation = function() {
    this.renderer_.startPointsAnimation();
};
//------------------------------------------------------------------------------
//                          Series
//------------------------------------------------------------------------------
/**
 * Adds new series to the series collection.
 * @export
 * @param {Object} seriesData
 */
AnyChart.prototype.addSeries = function(seriesData) {
    this.renderer_.addSeries(seriesData);
};
/**
 * Adds series at the given position.
 * @export
 * @param {Number} index
 * @param {Object} seriesData
 */
AnyChart.prototype.addSeriesAt = function(index, seriesData) {
    this.renderer_.addSeriesAt(index, seriesData);
};
/**
 * Removes the series with the given seriesId.
 * @export
 * @param {String} seriesId
 */
AnyChart.prototype.removeSeries = function(seriesId) {
    this.renderer_.removeSeries(seriesId);
};
/**
 * Updates the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {Object} seriesData
 */
AnyChart.prototype.updateSeries = function(seriesId, seriesData) {
    this.renderer_.updateSeries(seriesId, seriesData);
};
/**
 * Shows/hides the series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {Boolean} isVisible
 */
AnyChart.prototype.showSeries = function(seriesId, isVisible) {
    this.renderer_.showSeries(seriesId, isVisible);
};
/**
 * Highlights/dehiglights a series with the given seriesId.
 * @export
 * @param {String} seriesId
 * @param {Boolean} isHighlight
 */
AnyChart.prototype.highlightSeries = function(seriesId, isHighlight) {
    this.renderer_.highlightSeries(seriesId, isHighlight);
};
/**
 *  Highlights/dehiglights category with categoryName.
 * @export
 * @param {String} categoryName
 * @param {Boolean} isHighlight
 */
AnyChart.prototype.highlightCategory = function(categoryName, isHighlight) {
    this.renderer_.highlightCategory(categoryName, isHighlight);
};
//------------------------------------------------------------------------------
//                          Axes
//------------------------------------------------------------------------------
/**
 * Shows/hides axes markers.
 * @export
 * @param {String} markerId
 * @param {Boolean} isVisible
 */
AnyChart.prototype.showAxisMarker = function(markerId, isVisible) {
    this.renderer_.showAxisMarker(markerId, isVisible);
};
//------------------------------------------------------------------------------
//                          Custom attributes
//------------------------------------------------------------------------------
/**
 * Sets plot custom attribute value.
 * @export
 * @param {String} attributeName
 * @param {Object} attributeValue
 */
AnyChart.prototype.setPlotCustomAttribute = function(attributeName, attributeValue) {
    this.renderer_.setPlotCustomAttribute(attributeName, attributeValue);
};
//------------------------------------------------------------------------------
//                    Dashboard external methods
//------------------------------------------------------------------------------
/**
 * Sets plot custom attribute in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} attributeName
 * @param {Object} attributeValue
 */
AnyChart.prototype.view_setPlotCustomAttribute = function(viewId, attributeName, attributeValue) {
    this.renderer_.view_setPlotCustomAttribute(viewId, attributeName, attributeValue);
};
/**
 * Adds series into the dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {Object} seriesData
 */
AnyChart.prototype.view_addSeries = function(viewId, seriesData) {
    this.renderer_.view_addSeries(viewId, seriesData);
};
/**
 * Removes a series with the given seriesId from the dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 */
AnyChart.prototype.view_removeSeries = function(viewId, seriesId) {
    this.renderer_.view_removeSeries(viewId, seriesId);
};
/**
 * Adds series at the given index position in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {Number} index
 * @param {Object} seriesData
 */
AnyChart.prototype.view_addSeriesAt = function(viewId, index, seriesData) {
    this.renderer_.view_addSeriesAt(viewId, index, seriesData);
};
/**
 * Updates a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {Object} seriesData
 */
AnyChart.prototype.view_updateSeries = function(viewId, seriesId, seriesData) {
    this.renderer_.view_updateSeries(viewId, seriesId, seriesData);
};
/**
 * Shows/Hides a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {Boolean} isVisible
 */
AnyChart.prototype.view_showSeries = function(viewId, seriesId, isVisible) {
    this.renderer_.view_showSeries(viewId, seriesId, isVisible);
};
/**
 * Adds a point with pointData in a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {Object} pointData
 */
AnyChart.prototype.view_addPoint = function(viewId, seriesId, pointData) {
    this.renderer_.view_addPoint(viewId, seriesId, pointData);
};
/**
 * Adds a point with pointData at the given pointIndex in a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {Number} pointIndex
 * @param {Object} pointData
 */
AnyChart.prototype.view_addPointAt = function(viewId, seriesId, pointIndex, pointData) {
    this.renderer_.view_addPointAt(viewId, seriesId, pointIndex, pointData);
};
/**
 * Removes a point with the given pointId from a series with the given seriesId from a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {String} pointId
 */
AnyChart.prototype.view_removePoint = function(viewId, seriesId, pointId) {
    this.renderer_.view_removePoint(viewId, seriesId, pointId);
};
/**
 * Sets new pointData to a point with the given pointId in a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Object} pointData
 */
AnyChart.prototype.view_updatePoint = function(viewId, seriesId, pointId, pointData) {
    this.renderer_.view_updatePoint(viewId, seriesId, pointId, pointData);
};
/**
 * Removes data from a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 */
AnyChart.prototype.view_clear = function(viewId) {
    this.renderer_.view_clear(viewId);
};
/**
 * Refreshes data in a dashboard view with the given viewId (used along with update methods).
 * @export
 * @param {String} viewId
 */
AnyChart.prototype.view_refresh = function(viewId) {
    this.renderer_.view_refresh(viewId);
};
/**
 * Highlights/dehighlights a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {Boolean} isHighlight
 */
AnyChart.prototype.view_highlightSeries = function(viewId, seriesId, isHighlight) {
    this.renderer_.view_highlightSeries(viewId, seriesId, isHighlight);
};
/**
 * Highlights/dehighlights a point with the given pointId in a series with the given seriesId in a dashboardview with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Boolean} isHighlight
 */
AnyChart.prototype.view_highlightPoint = function(viewId, seriesId, pointId, isHighlight) {
    this.renderer_.view_highlightPoint(viewId, seriesId, pointId, isHighlight);
};
/**
 * Highlights/dehighlights a category with the given categoryName in a dashboard view with the given viewId.
 * @export
 * @param viewId
 * @param categoryName
 * @param isHighlight
 */
AnyChart.prototype.view_highlightCategory = function(viewId, categoryName, isHighlight) {
    this.renderer_.view_highlightCategory(viewId, categoryName, isHighlight);
};
/**
 * Selects/deselects a point with the given pointId in a series with the given seriesId in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} seriesId
 * @param {String} pointId
 * @param {Boolean} isSelect
 */
AnyChart.prototype.view_selectPoint = function(viewId, seriesId, pointId, isSelect) {
    this.renderer_.view_selectPoint(viewId, seriesId, pointId, isSelect);
};
/**
 * Updates a gauge pointer with the given pointName in a given gauge in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {String} groupName
 * @param {String} pointName
 * @param {Object} data
 */
AnyChart.prototype.updateViewPointData = function(viewId, groupName, pointName, data) {
    this.renderer_.updateViewPointData(viewId, groupName, pointName, data);
};
//------------------------------------------------------------------------------
//                          Export (ignored)
//------------------------------------------------------------------------------
/**
 * Exports visible part of the chart as a PNG image of a given width and height as a base64 encoded PNG string. If no width and height specified - current chart instance width and height are used.
 * @export
 * @param {Number} opt_width
 * @param {Number} opt_height
 * @return {String} Возвращает base64string PNG картинки.
 */
AnyChart.prototype.getPNG = function(opt_width, opt_height) {
    if (opt_width == null || opt_width == undefined) opt_width = NaN;
    if (opt_height == null || opt_height == undefined) opt_height = NaN;
    return this.renderer_.getPNG(opt_width, opt_height);
};
goog.exportSymbol('AnyChart.prototype.getPNGImage', AnyChart.prototype.getPNG);

/**
 * Exports visible part of the chart as a PDF document of a given width and height as a base64 encoded PNG string. If no width and height specified - current chart instance width and height are used.
 * @export
 * @param {Number} opt_width
 * @param {Number} opt_height
 * @return {String} Возвращает base64string PDF докумнета.
 */
AnyChart.prototype.getPDF = function(opt_width, opt_height) {
    if (opt_width == null || opt_width == undefined) opt_width = NaN;
    if (opt_height == null || opt_height == undefined) opt_height = NaN;
    return this.renderer_.getPDF(opt_width, opt_height);
};

/**
 * Opens Print chart dialog.
 * @export
 */
AnyChart.prototype.printChart = function() {
    this.renderer_.printChart();
};

/**
 * Passes base64string encoded image to the save as image script specified in config, script should send binary image back.
 * @export
 * @param {Number} opt_width
 * @param {Number} opt_height
 */
AnyChart.prototype.saveAsImage = function(opt_width, opt_height) {
    if (opt_width == null || opt_width == undefined) opt_width = NaN;
    if (opt_height == null || opt_height == undefined) opt_height = NaN;
    
    this.renderer_.saveAsImage(opt_width, opt_height);
};
/**
 * Passes base64string encoded image to the save as PDF script specified in config, script should send binary PDF back.
 * @export
 * @param {Number} opt_width
 * @param {Number} opt_height
 */
AnyChart.prototype.saveAsPDF = function(opt_width, opt_height) {
    if (opt_width == null || opt_width == undefined) opt_width = NaN;
    if (opt_height == null || opt_height == undefined) opt_height = NaN;
    
    this.renderer_.saveAsPDF(opt_width, opt_height);
};
//------------------------------------------------------------------------------
//                          Scroll (ignored)
//------------------------------------------------------------------------------
/**
 * Scrolls chart by Х, to the given start value.
 * @export
 * @param {Object} value
 */
AnyChart.prototype.scrollXTo = function(value) {
    this.renderer_.scrollXTo(value);
};
/**
 * Scrolls chart by Y, to the given start value.
 * @export
 * @param {Object} value
 */
AnyChart.prototype.scrollYTo = function(value) {
    this.renderer_.scrollYTo(value);
};
/**
 * Scrolls chart by Х and Y, to the given start values.
 * @export
 * @param {Object} xValue
 * @param {Object} yValue
 */
AnyChart.prototype.scrollTo = function(xValue, yValue) {
    this.renderer_.scrollTo(xValue, yValue);
};
/**
 * Scrolls charts in a dashboard view with the given viewId by Х, to the given start value.
 * @export
 * @param {String} viewId
 * @param {Object} value
 */
AnyChart.prototype.viewScrollXTo = function(viewId, value) {
    this.renderer_.viewScrollXTo(viewId, value);
};
/**
 * Scrolls charts in a dashboard view with the given viewId by Y, to the given start value.
 * @export
 * @param {String} viewId
 * @param {Object} value
 */
AnyChart.prototype.viewScrollYTo = function(viewId, value) {
    this.renderer_.viewScrollYTo(viewId, value);
};
/**
 * Scrolls charts in a dashboard view with the given viewId by Х and Y, to the given start values.
 * @param {String} viewId
 * @param {Object} xValue
 * @param {Object} yValue
 */
AnyChart.prototype.viewScrollTo = function(viewId, xValue, yValue) {
    this.renderer_.viewScrollTo(viewId, xValue, yValue);
};
/**
 * Gets X Axis Scroll info.
 * @export
 * @return {Object}  range - zoom range, start - range start.
 */
AnyChart.prototype.getXScrollInfo = function() {
    return this.renderer_.getXScrollInfo();
};
/**
 * Gets Y Axis Scroll info.
 * @export
 * @return {Object}  range - zoom range, start - range start.
 */
AnyChart.prototype.getYScrollInfo = function() {
    return this.renderer_.getYScrollInfo();
};
/**
 * Gets X Axis Scroll info of a chart in а dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @return {Object}  range - zoom range, start - range start.
 */
AnyChart.prototype.getViewXScrollInfo = function(viewId) {
    return this.renderer_.getViewXScrollInfo(viewId);
};
/**
 * Gets Y Axis Scroll info of a chart in а dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @return {Object}  range - zoom range, start - range start.
 */
AnyChart.prototype.getViewYScrollInfo = function(viewId) {
    return this.renderer_.getViewYScrollInfo(viewId);
};
//------------------------------------------------------------------------------
//                          Zoom (ignored)
//------------------------------------------------------------------------------
/**
 * Sets Х Axis zoom settings.
 * @export
 * @param {Object} settings
 */
AnyChart.prototype.setXZoom = function(settings) {
    this.renderer_.setXZoom(settings);
};
/**
 * Sets Y Axis zoom settings.
 * @export
 * @param {Object} settings
 */
AnyChart.prototype.setYZoom = function(settings) {
    this.renderer_.setYZoom(settings);
};
/**
 * Sets Х and Y Axes zoom settings.
 * @export
 * @param {Object} xSettings
 * @param {Object} ySettings
 */
AnyChart.prototype.setZoom = function(xSettings, ySettings) {
    this.renderer_.setZoom(xSettings, ySettings);
};
/**
 * Sets X Axis zoom settings for a chart in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {Object} settings
 */
AnyChart.prototype.setViewXZoom = function(viewId, settings) {
    this.renderer_.setViewXZoom(viewId, settings);
};
/**  
 * Sets Y Axis zoom settings for a chart in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {Object} settings
 */
AnyChart.prototype.setViewYZoom = function(viewId, settings) {
    this.renderer_.setViewYZoom(viewId, settings);
};
/**
 * Sets X and Y Axes zoom settings for a chart in a dashboard view with the given viewId.
 * @export
 * @param {String} viewId
 * @param {Object} xSettings
 * @param {Object} ySettings
 */
AnyChart.prototype.setViewZoom = function(viewId, xSettings, ySettings) {
    this.renderer_.setViewZoom(viewId, xSettings, ySettings);
};
//------------------------------------------------------------------------------
//                          Animation (ignored)
//------------------------------------------------------------------------------
/**
 * Re-animates a chart.
 * @export
 */
AnyChart.prototype.animate = function() {
    this.renderer_.animate();
};

//------------------------------------------------------------------------------
//                      svg2base64 converter.
//------------------------------------------------------------------------------
/**
 * Gets chart SVG image as string or base64 encoded string.
 * @export
 * @param {Boolean} base64 If set to true then base64 encoded string is returned, if set to false - plain string.
 * @return {String} Base64 encoded string or plain string.
 */
AnyChart.prototype.getSVG = function(base64) {
    return this.renderer_.getSVG(base64);
};
