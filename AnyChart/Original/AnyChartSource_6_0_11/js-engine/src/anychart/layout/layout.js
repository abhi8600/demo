/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.layout.AbstractAlign}</li>
 *  <li>@class {anychart.layout.Anchor}</li>
 *  <li>@class {anychart.layout.HorizontalAlign}</li>
 *  <li>@class {anychart.layout.Margin}</li>
 *  <li>@class {anychart.layout.Position}</li>
 *  <li>@class {anychart.layout.VerticalAlign}.</li>
 * <ul>
 */
goog.provide('anychart.layout');
//------------------------------------------------------------------------------
//
//                          AbstractAlign class.
//
//------------------------------------------------------------------------------
/**
 * Abstract Align: near, center, far
 * Abstract align created for orientation-independent objects.
 * @enum {int}
 */
anychart.layout.AbstractAlign = {
    NEAR: 0,
    CENTER: 1,
    FAR: 2
};

/**
 * @param {*} value align value.
 * @return {anychart.layout.AbstractAlign} align.
 */
anychart.layout.AbstractAlign.deserialize = function(value) {
    value = anychart.utils.deserialization.getLString(value);
    switch (value) {
        case 'near' : return anychart.layout.AbstractAlign.NEAR;
        case 'center' : return anychart.layout.AbstractAlign.CENTER;
        case 'far' : return anychart.layout.AbstractAlign.FAR;
        default : return anychart.layout.AbstractAlign.CENTER;
    }
};

//------------------------------------------------------------------------------
//
//                          HorizontalAlign class.
//
//------------------------------------------------------------------------------
/**
 * Text horizontal position enum. Contains left, right, center positions.
 * @enum {int}
 */
anychart.layout.HorizontalAlign = {
    UNDEFINED: -1,
    LEFT: 0,
    RIGHT: 1,
    CENTER: 2
};
/**
 * Apply default horizontal algorithm to position.
 * @param {anychart.utils.geom.Point} position
 * @param {int} hAlign
 * @param {Number} elementSize
 * @param {Number} padding
 */
anychart.layout.HorizontalAlign.apply = function(position, hAlign, elementSize, padding) {
    switch (hAlign) {
        case anychart.layout.HorizontalAlign.LEFT:
            position.x -= elementSize.width + padding;
            break;
        case anychart.layout.HorizontalAlign.RIGHT:
            position.x += padding;
            break;
        case anychart.layout.HorizontalAlign.CENTER:
            position.x -= elementSize.width / 2;
            break;
    }
};
/**
 * @param {*} value Align value.
 * @return {anychart.layout.HorizontalAlign} hotizontal align.
 */
anychart.layout.HorizontalAlign.deserialize = function(value) {
    value = anychart.utils.deserialization.getLString(value);
    switch (value) {
        case 'left' : return anychart.layout.HorizontalAlign.LEFT;
        case 'right' : return anychart.layout.HorizontalAlign.RIGHT;
        case 'center' : return anychart.layout.HorizontalAlign.CENTER;
        default : return anychart.layout.HorizontalAlign.CENTER;
    }
};
//------------------------------------------------------------------------------
//
//                          VerticalAlign class.
//
//------------------------------------------------------------------------------

/**
 * Vertical position enum.
 * @enum {int}
 */
anychart.layout.VerticalAlign = {
    UNDEFINED : -1,
    TOP : 0,
    CENTER : 1,
    BOTTOM : 2
};
/**
 * Apply default vertical algorithm to position.
 * @param {anychart.utils.geom.Point} position
 * @param {int} vAlign
 * @param {Number} elementSize
 * @param {Number} padding
 */
anychart.layout.VerticalAlign.apply = function(position, vAlign, elementSize, padding) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP:
            position.y -= elementSize.height + padding;
            break;
        case anychart.layout.VerticalAlign.BOTTOM:
            position.y += padding;
            break;
        case anychart.layout.VerticalAlign.CENTER:
            position.y -= elementSize.height / 2;
            break;
    }
};
/**
 * @param {*} value Align value.
 * @return {anychart.layout.VerticalAlign} Vertical align.
 */
anychart.layout.VerticalAlign.deserialize = function(value) {
    value = anychart.utils.deserialization.getLString(value);
    switch (value) {
        case 'top' : return anychart.layout.VerticalAlign.TOP;
        default :
        case 'center' : return anychart.layout.VerticalAlign.CENTER;
        case 'bottom' : return anychart.layout.VerticalAlign.BOTTOM;
    }
};
//------------------------------------------------------------------------------
//
//                          Position class.
//
//------------------------------------------------------------------------------
/**
 * Position enum. Contains left, right, top, bottom positions
 * @enum {Number}
 */
anychart.layout.Position = {
    LEFT: 0,
    RIGHT: 1,
    TOP: 2,
    BOTTOM: 3
};
//------------------------------------------------------------------------------
//
//                          Anchor class.
//
//------------------------------------------------------------------------------
/**
 * Anchor
 * @enum {int}
 */
anychart.layout.Anchor = {
    CENTER: 0,
    CENTER_LEFT: 1,
    LEFT_TOP: 2,
    CENTER_TOP: 3,
    RIGHT_TOP: 4,
    CENTER_RIGHT: 5,
    RIGHT_BOTTOM: 6,
    CENTER_BOTTOM: 7,
    LEFT_BOTTOM: 8,
    FLOAT: 9,
    X_AXIS: 10
};

/**
 * @param {*} value config value.
 * @return {anychart.layout.Anchor} anchor.
 */
anychart.layout.Anchor.deserialize = function(value) {
    value = anychart.utils.deserialization.getLString(value);
    switch (value) {
        default:
        case 'center':
            return anychart.layout.Anchor.CENTER;
        case 'centerleft':
        case 'leftcenter':
        case 'left':
            return anychart.layout.Anchor.CENTER_LEFT;
        case 'lefttop':
        case 'topleft':
            return anychart.layout.Anchor.LEFT_TOP;
        case 'centertop':
        case 'topcenter':
        case 'top':
            return anychart.layout.Anchor.CENTER_TOP;
        case 'righttop':
        case 'topright':
            return anychart.layout.Anchor.RIGHT_TOP;
        case 'centerright':
        case 'rightcenter':
        case 'right':
            return anychart.layout.Anchor.CENTER_RIGHT;
        case 'rightbottom':
        case 'bottomright':
            return anychart.layout.Anchor.RIGHT_BOTTOM;
        case 'centerbottom':
        case 'bottomcenter':
        case 'bottom':
            return anychart.layout.Anchor.CENTER_BOTTOM;
        case 'leftbottom':
        case 'bottomleft':
            return anychart.layout.Anchor.LEFT_BOTTOM;
        case 'float': return anychart.layout.Anchor.FLOAT;
        case 'xaxis': return anychart.layout.Anchor.X_AXIS;
    }
};

//------------------------------------------------------------------------------
//
//                          Margin class.
//
//------------------------------------------------------------------------------
/**
 * Margin constructor. Creates new Margin instance.
 * Default Margins is 10, 10, 10, 10
 * @constructor
 * @param {Number} opt_marginValue Margin value to set.
 */
anychart.layout.Margin = function(opt_marginValue) {
    if (opt_marginValue == null || opt_marginValue == undefined)
        this.setAll(10);
    else this.setAll(opt_marginValue);
};

//------------------------------------------------------------------------------
//                          Left
//------------------------------------------------------------------------------

/**
 * Left margin
 * @private
 * @type {Number}
 */
anychart.layout.Margin.prototype.left_ = 0;

/**
 * @return {Number} Left margin.
 */
anychart.layout.Margin.prototype.getLeft = function() {
    return this.left_;
};

/**
 * @param {Number} value Left margin.
 */
anychart.layout.Margin.prototype.setLeft = function(value) {
    this.left_ = value;
};

//------------------------------------------------------------------------------
//                          Right
//------------------------------------------------------------------------------

/**
 * Right margin
 * @private
 * @type {Number}
 */
anychart.layout.Margin.prototype.right_ = 0;

/**
 * @return {Number} Right margin.
 */
anychart.layout.Margin.prototype.getRight = function() {
    return this.right_;
};

/**
 * @param {Number} value Right margin.
 */
anychart.layout.Margin.prototype.setRight = function(value) {
    this.right_ = value;
};

//------------------------------------------------------------------------------
//                          Top
//------------------------------------------------------------------------------

/**
 * Top margin
 * @private
 * @type {Number}
 */
anychart.layout.Margin.prototype.top_ = 0;

/**
 * @return {Number} Top margin.
 */
anychart.layout.Margin.prototype.getTop = function() {
    return this.top_;
};

/**
 * @param {Number} value Top margin.
 */
anychart.layout.Margin.prototype.setTop = function(value) {
    this.top_ = value;
};

//------------------------------------------------------------------------------
//                          Bottom
//------------------------------------------------------------------------------

/**
 * Bottom margin
 * @private
 * @type {Number}
 */
anychart.layout.Margin.prototype.bottom_ = 0;

/**
 * @return {Number} Bottom margin.
 */
anychart.layout.Margin.prototype.getBottom = function() {
    return this.bottom_;
};

/**
 * @param {Number} value Bottom margin.
 */
anychart.layout.Margin.prototype.setBottom = function(value) {
    this.bottom_ = value;
};

//------------------------------------------------------------------------------
//                          All
//------------------------------------------------------------------------------

/**
 * Sets all margins value.
 * @param {Number} value Default margins value.
 */
anychart.layout.Margin.prototype.setAll = function(value) {
    this.left_ = this.right_ = this.top_ = this.bottom_ = value;
};

//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------

/**
 * Deserialize margins config
 * Config sample:
 * <code>
 * {
 *  all: 10,
 *  left: 20,
 *  right: 10,
 *  top: 30,
 *  bottom: 41
 * }
 * </code>
 *
 * @param {object} config Margins config.
 */
anychart.layout.Margin.prototype.deserialize = function(config) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'all'))
        this.setAll(isNaN(des.getNumProp(config, 'all')) ?
                10 :
                des.getNumProp(config, 'all'));

    if (des.hasProp(config, 'left'))
        this.setLeft(isNaN(des.getNumProp(config, 'left')) ?
                10 :
                des.getNumProp(config, 'left'));

    if (des.hasProp(config, 'right'))
        this.setRight(isNaN(des.getNumProp(config, 'right')) ?
                10 :
                des.getNumProp(config, 'right'));

    if (des.hasProp(config, 'top'))
        this.setTop(isNaN(des.getNumProp(config, 'top')) ?
                10 :
                des.getNumProp(config, 'top'));

    if (des.hasProp(config, 'bottom'))
        this.setBottom(isNaN(des.getNumProp(config, 'bottom')) ?
                10 :
                des.getNumProp(config, 'bottom'));
};

//------------------------------------------------------------------------------
//                          Apply methods
//------------------------------------------------------------------------------

/**
 * 'Compress' rectangle using margins.
 *
 * Sample:
 * <code>
 * var rect = new anychart.utils.geom.Rectangle(10, 10, 100, 120);
 * var margin = new anychart.layout.Margin();
 * margin.setAll(5);
 *
 * margin.applyInside(rect);
 * alert (rect.x); //15. rect.x + margin.getLeft()
 * alert (rect.y); //15. rect.y + margin.getTop()
 * alert (rect.width); //90. rect.width -margin.getLeft() -margin.getRight()
 * alert (rect.height); //110. rect.height -margin.getTop() -margin.getBottom()
 * </code>
 * @param {anychart.utils.geom.Rectangle} rect target rectangle.
 */
anychart.layout.Margin.prototype.applyInside = function(rect) {
    rect.x += this.left_;
    rect.y += this.top_;

    rect.width -= this.left_ + this.right_;
    rect.height -= this.top_ + this.bottom_;
};

/**
 * 'Extend' rectangle using margins.
 *
 * Sample:
 * <code>
 * var rect = new anychart.utils.geom.Rectangle(10, 10, 100, 120);
 * var margin = new anychart.layout.Margin();
 * margin.setAll(5);
 *
 * margin.applyOutside(rect);
 * alert (rect.x); //5. rect.x - margin.getLeft()
 * alert (rect.y); //5. rect.y - margin.getTop()
 * alert (rect.width); //110. rect.width + margin.getLeft()+margin.getRight()
 * alert (rect.height); //130. rect.height + margin.getTop()+margin.getBottom()
 * </code>
 * @param {anychart.utils.geom.Rectangle} rect target rectangle.
 */
anychart.layout.Margin.prototype.applyOutside = function(rect) {
    rect.x -= this.left_;
    rect.width += this.left_ + this.right_;
    rect.y -= this.top_;
    rect.height += this.top_ + this.bottom_;
};
