/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.tooltip.TooltipElementStyleState}</li>
 *  <li>@class {anychart.elements.tooltip.BaseTooltipElement}.</li>
 * <ul>
 */
goog.provide('anychart.elements.tooltip');
goog.require('anychart.elements.tooltip.styles');
goog.require('anychart.layout');
goog.require('goog.userAgent');

//------------------------------------------------------------------------------
//
//                           BaseTooltipElement class.
//
//------------------------------------------------------------------------------
/**
 * Class represent base tooltip element.
 * @constructor
 * @extends {anychart.elements.label.BaseLabelElement}
 */
anychart.elements.tooltip.BaseTooltipElement = function () {
    anychart.elements.label.BaseLabelElement.apply(this);

    if (goog.userAgent.WEBKIT)
        this.getAbsolutePosition_ = this.getAbsolutePositionWebKit_;
    else this.getAbsolutePosition_ = this.getAbsolutePositionFF_;


};
//inheritance
goog.inherits(anychart.elements.tooltip.BaseTooltipElement,
    anychart.elements.label.BaseLabelElement);
//------------------------------------------------------------------------------
//
//                          Override
//
//------------------------------------------------------------------------------
/**
 * Return tooltip style node name.
 * @override
 * @return {String} Style node name.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getStyleNodeName =
    function () {
        return 'tooltip_style';
    };

/**
 * Creates a style.
 * @return {anychart.elements.tooltip.styles.TooltipElementStyle} Tooltip
 * element style.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.createStyle =
    function () {
        return new anychart.elements.tooltip.styles.TooltipElementStyle();
    };

/**
 * @inheritDoc
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getElementContainer =
    function (container) {
        return container.getPlot().getTooltipsSprite();
    };


//------------------------------------------------------------------------------
//
//                          Drawing
//
//------------------------------------------------------------------------------
/**
 *
 * @param {Event} event Event.
 * @param {anychart.plots.seriesPlot.data.BasePoint} container Container.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.execMoveTooltip = function (event, container, state, sprite) {
    if (state.getAnchor() == anychart.layout.Anchor.FLOAT) {        
        var bounds = this.getBounds(container, state, sprite);
        var pos = new anychart.utils.geom.Point();
        this.getAbsolutePosition_(event, sprite, pos);

        container.setElementHorizontalAlign(
            pos,
            state.getAnchor(),
            state.getHAlign(),
            state.getVAlign(),
            bounds,
            state.getPadding());

        container.setElementVerticalAlign(
            pos,
            state.getAnchor(),
            state.getHAlign(),
            state.getVAlign(),
            bounds,
            state.getPadding());

        this.setPosition(pos, sprite);
    }
};

/**
 * Creates text sprite.
 * @private
 * @param {anychart.plots.seriesPlot.data.BasePoint} container Container.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.createTextSprite_ =
    function (container, state, sprite) {
        goog.base(this, 'createTextSprite_', container, state, sprite);
        sprite.getElement().setAttribute('pointer-events', 'none');
    };
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**
 * Специфичная по браузеру определение положения мыши.
 * @private
 * @type {Function)}
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePosition_ = null;

anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePositionFF_ = function (event, sprite, pos) {
    var el = sprite.getSVGManager().getSVG()['parentNode'];
    var clientPosition = sprite.getClientPosition();
    var lx = 0;
    var ly = 0;
    while (el != null) {
        lx += el.offsetLeft - el.scrollLeft;
        ly += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }

    pos.x = event.clientX - (clientPosition.x + lx);
    pos.y = event.clientY - (clientPosition.y + ly);

    return pos
};
anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePositionWebKit_ = function (event, sprite, pos) {
    var clientPosition = sprite.getClientPosition();
    pos.x = event.offsetX - clientPosition.x;
    pos.y = event.offsetY - clientPosition.y;
    return  pos;
};

/**
 * Return copy of base marker element.
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.elements.tooltip.BaseTooltipElement} Base tooltip element.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.copy =
    function (opt_target) {
        if (!opt_target) opt_target =
            new anychart.elements.tooltip.BaseTooltipElement();
        return goog.base(this, 'copy', opt_target);
    };
