/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.label.styles.LabelElementStyleState}</li>
 *  <li>@class {anychart.elements.label.styles.LabelElementStyleShapes}</li>
 *  <li>@class {anychart.elements.label.styles.LabelElementStyle}.</li>
 * <ul>
 */
goog.provide('anychart.elements.label.styles');
goog.require('anychart.elements.styles');
//------------------------------------------------------------------------------
//
//                     LabelElementStyleState class.
//
//------------------------------------------------------------------------------
/**
 * Class represent label style state.
 * @constructor
 * @extends {anychart.elements.styles.BaseElementStyleState}
 * @param {anychart.styles.Style} style Style for this state.
 * @param {anychart.styles.StateType} opt_stateType State type.
 */
anychart.elements.label.styles.LabelElementStyleState = function (style, opt_stateType) {
    anychart.elements.styles.BaseElementStyleState.call(this, style,
        opt_stateType);
};
goog.inherits(anychart.elements.label.styles.LabelElementStyleState,
    anychart.elements.styles.BaseElementStyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------

/**
 * Label text element.
 * @protected
 * @type {anychart.visual.text.TextElement}
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.textElement = null;
/**
 * @return {anychart.visual.text.TextElement} Text element.
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.getTextElement = function () {
    return this.textElement;
};

/**
 * Label text format.
 * @protected
 * @type {anychart.formatting.TextFormatter}
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.textFormatter = null;
/**
 * @return {anychart.formatting.TextFormatter} Text formatter.
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.getTextFormatter = function () {
    return this.textFormatter;
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * Deserialize label element style state settings.
 * @override
 * @param {object} data JSON object with label element style state settings.
 * <p>
 *  Expected JSON structure:
 *  <code>
 *      Object {
 *          name : string, // style name.
 *          parent : string, //parent style name.
 *          color : string, //color that replaces %Color token value.
 *          hatch_type : string, //hatch type that replaces %HatchType token
 *          value.
 *          multi_line_align :string, //Sets multi line align mode.
 *          rotation : number, // Sets text rotation angle.
 *          format : string, //Sets label text formatting.
 *          font : object, // Deserialize by anychart.visual.text.TextElement
 *          position : object, // Sets label position.
 *          background : // deserialize by
 *          anychart.visual.background.RectBackground.
 *
 *      }
 *  </code>
 * </p>
 * @return {Object} JSON object.
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    if (!this.enabled) return data;

    this.textElement = this.createTextElement();
    this.textElement.deserialize(data);

    if (!this.textElement.isEnabled()) {
        this.enabled = false;
        return data;
    }

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'position')) {
        var position = des.getProp(data, 'position');
        if (des.hasProp(position, 'anchor'))
            this.anchor = anychart.layout.Anchor.deserialize(
                des.getProp(position, 'anchor'));
        if (des.hasProp(position, 'halign'))
            this.hAlign = anychart.layout.HorizontalAlign.deserialize(
                des.getProp(position, 'halign'));
        if (des.hasProp(position, 'valign'))
            this.vAlign = anychart.layout.VerticalAlign.deserialize(
                des.getProp(position, 'valign'));
        if (des.hasProp(position, 'padding'))
            this.padding = des.getNumProp(position, 'padding');
    }
    if (des.hasProp(data, 'format'))
        this.textFormatter = new anychart.formatting.TextFormatter(
            des.getStringProp(data, 'format'));
    return data;
};
/**
 * @protected
 * @return {anychart.visual.text.TextElement}
 */
anychart.elements.label.styles.LabelElementStyleState.prototype.createTextElement = function () {
    return new anychart.visual.text.TextElement();
};
//------------------------------------------------------------------------------
//
//                  LabelElementStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.styles.BaseElementsStyleShapes}
 */
anychart.elements.label.styles.LabelElementStyleShapes = function () {
    anychart.elements.styles.BaseElementsStyleShapes.call(this);
};
goog.inherits(anychart.elements.label.styles.LabelElementStyleShapes,
    anychart.elements.styles.BaseElementsStyleShapes);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.textSprite_ = null;
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.createShapes = function (point, element) {
    var state = point.getCurrentStyleState(element.getStyle());
    var textElement = state.getTextElement();
    textElement.setEventsEnabled(true);
    var textFormatter = element.getActualTextFormatter(state);
    var svgManager = point.getSVGManager();
    textElement.initSize(svgManager, textFormatter.getValue(point, this.getDateTimeLocale(point)));
    this.createSVGText(point, element, textElement);
};
/**
 * Return  date time locale for label element text formatter
 * @return {anychart.formatting.IDateTimeInfoProvider}
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.getDateTimeLocale = function (point) {
    return (point.getPlot) ? point.getPlot().getDateTimeLocale() : null;

};

anychart.elements.label.styles.LabelElementStyleShapes.prototype.createSVGText = function (point, element, textElement) {
    this.textSprite_ = textElement.createSVGText(
        point.getSVGManager(),
        point.getColor().getColor(),
        false);
};
/**
 * @inheritDoc
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.placeShapes = function (sprite) {
    sprite.appendSprite(this.textSprite_);
};
/**
 * @inheritDoc
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.update = function (state) {
    if (!state.isEnabled() || !state.getTextElement()) {
        this.setVisible(false);
        return;
    }
    goog.base(this, 'update', state);
    var textElement = state.getTextElement();
    var textFormatter = this.element_.getActualTextFormatter(state);
    var svgManager = this.point_.getSVGManager();
    //size
    textElement.initSize(svgManager, textFormatter.getValue(this.point_, this.getDateTimeLocale(this.point_)));
    //text
    this.execUpdate(svgManager, textElement, this.textSprite_, this.point_.getColor().getColor());
    //visible
    this.setVisible(true);
};
/**
 * @param svgManager
 * @param textElement
 * @param sprite
 * @param color
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.execUpdate = function (svgManager, textElement, sprite, color) {
    textElement.update(
        svgManager,
        sprite,
        color);
};
/**
 * Sets visibility.
 * @param {Boolean} value Value.
 */
anychart.elements.label.styles.LabelElementStyleShapes.prototype.setVisible = function (value) {
    if (value) this.textSprite_.getElement().setAttribute('visibility', 'visible');
    else this.textSprite_.getElement().setAttribute('visibility', 'hidden');
};
//------------------------------------------------------------------------------
//
//                          LabelElementStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.styles.BaseElementsStyle}
 */
anychart.elements.label.styles.LabelElementStyle = function () {
    anychart.elements.styles.BaseElementsStyle.call(this);
};
goog.inherits(anychart.elements.label.styles.LabelElementStyle,
    anychart.elements.styles.BaseElementsStyle);
//------------------------------------------------------------------------------
//                           Shapes.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.label.styles.LabelElementStyle.prototype.createStyleShapes = function () {
    return new anychart.elements.label.styles.LabelElementStyleShapes();
};
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.label.styles.LabelElementStyle.prototype.getStyleStateClass =
    function () {
        return anychart.elements.label.styles.LabelElementStyleState;
    };
