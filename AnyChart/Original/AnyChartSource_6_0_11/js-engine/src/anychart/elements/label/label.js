/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.label.BaseLabelElement}.</li>
 * <ul>
 */
goog.provide('anychart.elements.label');
goog.require('anychart.elements');
goog.require('anychart.elements.label.styles');
goog.require('anychart.formatting');
goog.require('anychart.utils.geom');
goog.require('anychart.visual.text');

//------------------------------------------------------------------------------
//
//                           BaseLabelElement class.
//
//------------------------------------------------------------------------------
/**
 * Class represent base label element.
 * @constructor
 * @extends {anychart.elements.BaseElement}
 */
anychart.elements.label.BaseLabelElement = function() {
    anychart.elements.BaseElement.call(this);
};
goog.inherits(anychart.elements.label.BaseLabelElement,
        anychart.elements.BaseElement);
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
/**
 * Label text formatter.
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.elements.label.BaseLabelElement.prototype.textFormatter_ = null;
//------------------------------------------------------------------------------
//                          Override
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.getStyleNodeName =
        function() {
            return 'label_style';
        };
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.createStyle = function() {
    return new anychart.elements.label.styles.LabelElementStyle();
};
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.getElementContainer =
        function(container) {
            return container.getPlot().getLabelsSprite(this);
        };
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.canDeserializeForPoint = function(elementNode) {
    return elementNode && (anychart.utils.deserialization.hasProp(elementNode, 'format') || anychart.utils.deserialization.hasProp(elementNode, 'style'));
};
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.deserializeGlobalSettings =
        function(elementNode, stylesList) {
            goog.base(this, 'deserializeGlobalSettings', elementNode, stylesList);
            this.deserializeFormat_(elementNode);
        };
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.deserializeSeriesSettings =
        function(elementNode, stylesList) {
            goog.base(this, 'deserializeSeriesSettings', elementNode, stylesList);
            this.deserializeFormat_(elementNode);
        };
/**
 * @inheritDoc
 */
anychart.elements.label.BaseLabelElement.prototype.deserializePointSettings =
        function(elementNode, stylesList) {
            goog.base(this, 'deserializePointSettings', elementNode, stylesList);
            this.deserializeFormat_(elementNode);
        };
/**
 * @private
 * @param {Object} elementNode Element node.
 */
anychart.elements.label.BaseLabelElement.prototype.deserializeFormat_ =
        function(elementNode) {
            if (anychart.utils.deserialization.hasProp(elementNode, 'format'))
                this.textFormatter_ = new anychart.formatting.TextFormatter(
                        anychart.utils.deserialization.getStringProp(
                                elementNode,
                                'format'));
        };
/**
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @return {anychart.formatting.TextFormatter} Text formatter.
 */
anychart.elements.label.BaseLabelElement.prototype.getActualTextFormatter = function(state) {
    return this.textFormatter_ ? this.textFormatter_ : state.getTextFormatter();
};
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**
 *
 * @param {anychart.formatting.ITextFormatInfoProvider} container Container.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.elements.label.BaseLabelElement.prototype.getBounds =
        function(container, state, sprite) {
            var textFormatter = this.getActualTextFormatter(state);
            state.getTextElement().initSize(
                    sprite.getSVGManager(),
                    textFormatter.getValue(container, container.getDateTimeLocale ? container.getDateTimeLocale() : null));
            return state.getTextElement().getBounds().clone();
        };

/**
 * Return copy of base label element.
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.elements.label.BaseLabelElement} Label element.
 */
anychart.elements.label.BaseLabelElement.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target =
            new anychart.elements.label.BaseLabelElement();
    opt_target.textFormatter_ = this.textFormatter_;
    return goog.base(this, 'copy', opt_target);
};






