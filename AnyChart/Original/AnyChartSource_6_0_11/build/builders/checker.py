#!/usr/bin/python
# -*- coding: utf-8 -*-
from zipfile import ZipFile
from glob import glob
import build_utils
import os
import sys
import shutil
import hashlib
import string
import re

ROOT = build_utils.get_root()
PATH_TO_OUT = os.path.join(ROOT, "out")
PATH_TO_SITE = os.path.join(ROOT, "..", 'site')

class FailedTest:
    def __init__(self, file_name):
        self.file_name = file_name
    
class FailedTests:
    '''
    Класс файлов с ошибкой
    '''
    def __init__(self):
        self.failedTests = []
        self.missingFiles = []
    def add_fail(self, failedTest):
        '''
        Добавлет экземпляр класса FailedTest к списку провалившихся тестов
        @param failedTest - экземпляр класса FailedTest
        '''
        self.failedTests.append(failedTest)
    def add_miss(self, missingFile):
        '''
        Добавлет missingFile к списку отстуствующих файлов
        @param missingFile - экземпляр класса FailedTest
        '''
        self.missingFiles.append(missingFile)

class Checker:
    '''
    Проверка сравнением файлов
    '''
    def getmd5(self, file_name):
        '''
        Получение md5 файла
        @param file file_name : файл, md5 которого требуется получить
        @return md5 файла
        '''  
        try:
            infile = open(file_name, 'rb')
            fileContent = infile.read()
            infile.close()
        except:
            return None
        m = hashlib.md5()
        m.update(fileContent)
        return m.hexdigest()        
    
    def diff(self, tg_file, src_file):
        '''
        Сравнение файлов 
        @param tg_file - файл, который сравнивать
        @param src_file - файл с которым сравнивать
        '''
        if os.path.exists(tg_file) and os.path.exists(src_file):
            try:
                if not self.getmd5(tg_file) == self.getmd5(src_file):
                    raise 
            except:
                fail = FailedTest(tg_file)
                failed.add_fail(fail)
        elif os.path.exists(tg_file):
            missed = FailedTest(src_file)
            failed.add_miss(missed)
        else:
            missed = FailedTest(tg_file)
            failed.add_miss(missed)

    def check_folder(self, folder):
        '''
        проверка архива AnyChart_plain.zip
        '''
        count = 0
        zip_file = ZipFile(os.path.join(folder, 'AnyChart_plain.zip'), 'r')
        tmp_folder = os.path.join(folder, 'AnyChart_plain')
        if os.path.exists(tmp_folder):
            shutil.rmtree(tmp_folder)
        os.mkdir(tmp_folder)
        zip_file.extractall(tmp_folder)
        for nm in zip_file.namelist():
            namePattern = re.compile('(^basic-sample|^binaries)/(js|swf)/[A-Za-z0-9]*.(js|swf)')
            if namePattern.search(nm) is not None and string.find(nm, 'Preloader.swf') < 0:
                src_file = os.path.join(folder, os.path.basename(nm))
                self.diff(os.path.join(tmp_folder, nm), src_file)
                count += 1
            if string.find(nm, 'Preloader.swf') >= 0:
                src_file = (os.path.join(PATH_TO_OUT, 'Preloader.swf'))
                self.diff(os.path.join(tmp_folder, nm), src_file)
        shutil.rmtree(tmp_folder)       
        return count

    def check_out(self):
        '''
        проверка папки out
        '''
        count = 0
        dirs = glob(os.path.join(PATH_TO_OUT, '*'))
        for dr in dirs:
            if os.path.exists(os.path.join(dr, 'AnyChart_plain.zip')):
                count += self.check_folder(dr)
        return count

    def check_live_samples(self, path_to_samples):
        '''
        проверка папки LiveSamples в site
        @return int count - количество проверенных файлов 
        '''
        count = 0 
        path_to_src = os.path.join(PATH_TO_OUT, 'trial')
        samples = glob(os.path.join(path_to_samples, '*', 'swf', '*.swf')) + glob(os.path.join(path_to_samples, '*', 'js', '*.js'))
        for f in samples:
            src_file = os.path.join(PATH_TO_OUT, 'trial', os.path.basename(f))
            if os.path.basename(f)=='Preloader.swf':
                src_file = os.path.join(PATH_TO_OUT, os.path.basename(f))
            self.diff(f, src_file)
        count += len(samples)
        samples = glob(os.path.join(path_to_samples, '*', '*.zip'))
        for f in samples:
            if os.path.basename(f) != 'sample.zip':
                zip_file = ZipFile(f, 'r')
                tmp_folder = os.path.join(f.replace(os.path.basename(f), ''), 'unZip')
                if os.path.exists(tmp_folder):
                    shutil.rmtree(tmp_folder)
                os.mkdir(tmp_folder)
                zip_file.extractall(tmp_folder)
                for nm in zip_file.namelist():
                    namePattern = re.compile('^(js|swf)/[A-Za-z0-9]*.(js|swf)')
                    if namePattern.search(nm) is not None:
                        src_file = os.path.join(path_to_samples, '../../../../', 'engine', 'site', os.path.basename(nm))
                        if os.path.basename(nm)=='Preloader.swf':
                            src_file = os.path.join(path_to_samples, '../../../../', 'engine', os.path.basename(nm))
                        self.diff(os.path.join(tmp_folder, nm), src_file)
                        count += 1
                shutil.rmtree(tmp_folder) 
        return count
    
    def check_platforms(self, path_to_platforms):
        '''
        проверка папки Platforms в site
        @return int count - количество проверенных файлов 
        '''
        count = 0
        samples = glob(os.path.join(path_to_platforms, '*'))
        src_path = os.path.join(path_to_platforms, '../../../', 'engine', 'site')
        for f in samples:
            folder = os.path.basename(f)
            
            if folder == 'apex' or folder == 'php':
                cases = glob(os.path.join(path_to_platforms, folder, '*.js')) + glob(os.path.join(path_to_platforms, folder, '*.swf'))
                count += len(cases)
                for case in cases:
                    src_file = os.path.join(src_path, os.path.basename(case))
                    self.diff(case, src_file)
                    
            elif folder == 'air' or folder == 'as3':
                tmp_folder = os.path.join(path_to_platforms, folder, 'unZip')
                if os.path.exists(tmp_folder):
                    shutil.rmtree(tmp_folder)
                os.mkdir(tmp_folder)
                if folder == 'air':
                    case = os.path.join(path_to_platforms, folder, 'AnyChartAir.zip')
                else:
                    case = os.path.join(path_to_platforms, folder, 'AnyChartPlainAS3.zip')
                zip_file = ZipFile(case, 'r')
                zip_file.extractall(tmp_folder)
                src_file = os.path.join(src_path, 'AnyChartFlexComponent.swc')
                if folder == 'air':
                    tg_file =  os.path.join(tmp_folder, 'AnyChartAir', 'libs', 'AnyChartFlexComponent.swc')
                else:
                    tg_file =  os.path.join(tmp_folder, 'libs', 'AnyChartFlexComponent.swc')
                self.diff(tg_file, src_file)
                shutil.rmtree(tmp_folder)
                
            elif folder == 'asp' or folder == 'aspnet':
                tmp_folder = os.path.join(path_to_platforms, folder, 'unZip')
                if os.path.exists(tmp_folder):
                    shutil.rmtree(tmp_folder)
                os.mkdir(tmp_folder)
                if folder == 'asp':
                    zip_file = ZipFile(os.path.join(path_to_platforms, folder, 'anychart-asp-samples.zip'), 'r')
                else:
                    zip_file = ZipFile(os.path.join(path_to_platforms, folder, 'anychart-aspnet-samples.zip'), 'r')
                zip_file.extractall(tmp_folder)
                if folder == 'asp':
                    tg_path = os.path.join(tmp_folder, 'anychart_asp', 'anychart_files')
                else:
                    tg_path = os.path.join(tmp_folder, 'anychart_aspnet', 'anychart_files')
                cases = glob(os.path.join(tg_path, '*', '*.swf')) + glob(os.path.join(tg_path, '*', '*.js'))
                for case in cases:
                    src_file = os.path.join(src_path, os.path.basename(case))
                    self.diff(case, src_file)
                shutil.rmtree(tmp_folder)
                
            elif folder == 'flex':
                cases = glob(os.path.join(f, 'samples', '*', 'srcview', 'source', 'libs', 'AnyChartFlexComponent.swc'))
                for case in cases:
                    src_file = os.path.join(src_path, os.path.basename(case))
                    self.diff(case, src_file)
                
            elif folder == 'flash':
                cases = glob(os.path.join(f, 'samples', '*', 'assets', 'anychart', 'AnyChart.swf'))
                for case in cases:
                    src_file = os.path.join(src_path, os.path.basename(case))
                    self.diff(case, src_file)
                cases = glob(os.path.join(f, 'samples', '*.zip'))
                for case in cases:
                    tmp_folder = os.path.join(path_to_platforms, folder, 'samples' ,'unZip')
                    if os.path.exists(tmp_folder):
                        shutil.rmtree(tmp_folder)
                    os.mkdir(tmp_folder)
                    zip_file = ZipFile(case, 'r')
                    zip_file.extractall(tmp_folder)
                    target = os.path.join(f, 'samples', 'unZip', 'assets', 'anychart', 'AnyChart.swf')
                    src_file = os.path.join(src_path, os.path.basename(target))
                    self.diff(target, src_file)
                    shutil.rmtree(tmp_folder)
        return count
        
    def check_site(self, path_to_site):
        '''
        проверка site
        @param string path_to_site - путь до сайта 
        '''
        count = 0
        path_to_samples = os.path.join(path_to_site, 'deploy', 'docs', 'users-guide', 'livesamples')
        count += self.check_live_samples(path_to_samples)
        path_to_platforms = os.path.join(path_to_site, 'deploy', 'docs', 'platforms')
        count += self.check_platforms(path_to_platforms)
        return count
        
    def clean_results(self):
        '''
        сортировка результата (очистка ошибок, которые не нужно исправлять)
        '''
        samples = failed.failedTests + failed.missingFiles
        for i in samples:
            if os.path.basename(i.file_name) == 'AnyChartImageConverter.js':
                failed.missingFiles.remove(i)
        
    def get_result(self, count):
        '''
        получение результат
        '''
        if os.path.exists(os.path.join(PATH_TO_OUT, 'checking.log')):
            os.remove(os.path.join(PATH_TO_OUT, 'checking.log'))
        content = '%d files done. %d failed. %d different files, %d missing files:' % (count, len(failed.failedTests) + len(failed.missingFiles), len(failed.failedTests), len(failed.missingFiles)) 
        self.clean_results()
        try:
            if len(failed.failedTests) == 0 and len(failed.missingFiles)==0:
                print 'Cheking files of out/ done. Everything fine.'
            else:
                if not len(failed.failedTests) == 0:
                    content += '\nDifferent files:\n'
                    for fail in failed.failedTests:
                        content += '\t' + fail.file_name + '\n'
                if not len(failed.missingFiles) == 0:        
                    content += '\nMissing files:\n'    
                    for miss in failed.missingFiles:
                        if content.find(miss.file_name) < 0:
                            content += '\t' + miss.file_name + '\n'
                log_file = os.path.join(PATH_TO_OUT, 'checking.log')
                open(log_file, "w").write(content)
                raise
        except:
            print 'Cheking files of out/ failed. See out/checking.log' 
            print content
            raise Exception

failed = FailedTests()
                 
def main():
    checker = Checker()
    count = 0
    count += checker.check_out()
    count += checker.check_site(PATH_TO_SITE)
    checker.get_result(count)
    
if __name__ == "__main__":
    main()
