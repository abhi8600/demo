#!/usr/bin/python
import build_utils
import subprocess
import datetime
import platform
import shutil
import glob
import os

BASE_DIR = build_utils.get_root()
OUT_DIR = os.path.join(BASE_DIR, 'out')
BUILD_UTILS_DIR = os.path.join(BASE_DIR, 'build', 'utils')
CONFIG_FOLDER = os.path.join(BASE_DIR, 'install', 'setup-script')
INSTALL_SRC = os.path.join(BASE_DIR, 'install', 'src')

#INSTALL_CONFIG = os.path.join(CONFIG_FOLDER, "install.iss")
INSTALL_CONFIG = '../install/setup-script/install.iss'
INNO_SETUP_DIR = os.path.join(BUILD_UTILS_DIR, 'inno-setup')
#INNO_SETUP_EXE = os.path.join(INNO_SETUP_DIR,"ISCC.exe")
INNO_SETUP_EXE = 'utils/inno-setup/ISCC.exe'
INNO_SETUP_OUTPUT = os.path.join(CONFIG_FOLDER, 'Output')

def __copy_source(destdir, filename, isByMask):
    if (isByMask):
        files = glob.iglob(os.path.join(OUT_DIR+'/'+BUILD_TYPE, filename))
        for file in files:
            if os.path.isfile(file):
                __copy_source(destdir, os.path.basename(file), 0)
    else:
        if (os.path.exists(OUT_DIR+'/'+BUILD_TYPE)):
            if not os.path.exists(INSTALL_SRC+'/binaries/'+destdir):
                os.mkdir(INSTALL_SRC+'/binaries/'+destdir)
            print "## copying "+OUT_DIR+'/'+BUILD_TYPE+"/"+filename
            shutil.copy(OUT_DIR+'/'+BUILD_TYPE+"/"+filename, INSTALL_SRC+'/binaries/'+destdir+filename)

def generateInstall(buildType):
    global BUILD_TYPE
    BUILD_TYPE =  buildType
    print "----------------"
    print "# AnyChart "+buildType+" version"
    print " "
    __copy_source('js/', '*.js', 1)
    __copy_source('swf/', '*.swf', 1)
    __copy_source('flex/', 'AnyChartFlexComponent.swc', 0)
    print "# try to compile"
    fh = open("Log","w")
    if platform.system() == 'Windows':
        p = subprocess.call([INNO_SETUP_EXE, INSTALL_CONFIG], stdout = fh, stderr = fh)
    else:
        p = subprocess.call(["wine",INNO_SETUP_EXE,INSTALL_CONFIG], stdout=fh)
    fh.close()

    if (os.path.exists(INNO_SETUP_OUTPUT) and  os.path.exists(INNO_SETUP_OUTPUT+'/AnyChart.exe')):
        print "# EXE Successfully created"
        print "# move AnyChart.exe to "+OUT_DIR+'/'+buildType+'/AnyChart.exe'
        shutil.copyfile(INNO_SETUP_OUTPUT+'/AnyChart.exe', OUT_DIR+'/'+buildType+'/AnyChart.exe')
        print "# creating PLAIN zip"
        print "# copying  files for zip"
        __copy_source('../basic-sample/js/', '*.js', 1)
        __copy_source('../basic-sample/swf/', 'AnyChart.swf', 0)
        build_utils.zipdir(INSTALL_SRC, OUT_DIR+'/'+buildType+'/AnyChart_plain.zip')
        if (os.path.exists(OUT_DIR+'/'+buildType+'/AnyChart_plain.zip')):
            print "# ZIP Successfully created"
        shutil.rmtree(INNO_SETUP_OUTPUT)
    else:
        print " "
        print "*** FAIL! ***"

def __change_year():
    now = datetime.datetime.now()
    files = glob.iglob(os.path.join(INSTALL_SRC+'/*.html'))
    for file in files:
        if os.path.isfile(file):
           build_utils.replace_in_file(file, "&copy; [\d]+ AnyChart.Com", "&copy; %d AnyChart.Com" % now.year)
    build_utils.replace_in_file(INSTALL_SRC+'/license.rtf', "f39 20[\d][\d]}", "f39 %d}" % now.year)

def __check_inno_setup():
    archive = os.path.join(BUILD_UTILS_DIR, 'inno-setup.zip')
    if os.path.exists(INNO_SETUP_DIR):
        return
    build_utils.extract_zip(archive, INNO_SETUP_DIR)

def __replace_version(str, version):
    build_utils.replace_in_file(INSTALL_CONFIG, str+"[\d.]+[\d]", str+"%s" % version)

def main():
    __check_inno_setup()
    __change_year()
    ver = build_utils.get_version()
    productVersion = "%s.%s.%s" % (ver['global'], ver['major'], ver['minor'])
    __replace_version("AnyChart Flash Chart Component ", productVersion)
    __replace_version("AnyChart ", productVersion)
    __replace_version("=", productVersion)
    build_utils.replace_in_file(INSTALL_CONFIG, "=AnyChart-[\d-]+[\d]", "=AnyChart-%s" % productVersion)
    print "## copying Preloader.swf"
    if not os.path.exists(INSTALL_SRC+"/binaries/swf"):
        os.mkdir(INSTALL_SRC+"/binaries/swf")

    if not os.path.exists(INSTALL_SRC+"/basic-sample/swf"):
        os.mkdir(INSTALL_SRC+"/basic-sample/swf")

    shutil.copy(os.path.join(OUT_DIR,"Preloader.swf"), INSTALL_SRC+'/binaries/swf/Preloader.swf')
    shutil.copy(OUT_DIR+"/Preloader.swf", INSTALL_SRC+'/basic-sample/swf/Preloader.swf')
    generateInstall('bundle')
    generateInstall('charts')
    generateInstall('maps')
    generateInstall('trial')
    os.remove("Log")
    print "------------------------"
    print "Finish build installs!"

if __name__ == "__main__":
    main()
