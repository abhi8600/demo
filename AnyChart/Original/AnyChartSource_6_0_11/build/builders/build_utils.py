import os
import zipfile
import urllib2
import re
from contextlib import closing

ROOT = os.path.join(os.path.dirname(__file__), '..', '..')
ROOT = os.path.abspath(ROOT)

def get_root():
    return ROOT

def download(url, file_path):
    u = urllib2.urlopen(url)
    f = open(file_path, "wb")
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading %s mb" % (file_size/1024/1024)
    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,

    f.close()

def extract_zip(zip_file, dest_dir):
    zf = zipfile.ZipFile(zip_file)

    dirs = []
    for name in zf.namelist():
        if name.endswith('/'):
            dirs.append(name)

    dirs.sort()

    os.mkdir(dest_dir)

    for dir in dirs:
        dirPath = os.path.join(dest_dir,dir)
        if not os.path.exists(dirPath):
            os.mkdir(dirPath)

    for i, name in enumerate(zf.namelist()):
        if not name.endswith('/'):
            outfile = open(os.path.join(dest_dir, name), 'wb')
            outfile.write(zf.read(name))
            outfile.flush()
            outfile.close()

def zipdir(basedir, archivename, ex = ()):
    excludes = (".svn", ".DS_Store", "Thumbs.db") + ex
    assert os.path.isdir(basedir)
    with closing(zipfile.ZipFile(archivename, "w", zipfile.ZIP_DEFLATED)) as z:
        for root, dirs, files in os.walk(basedir):
            #NOTE: ignore empty directories
            if '.svn' in dirs:
                dirs.remove('.svn')
            for fn in files:
                if not fn in excludes:
                    absfn = os.path.join(root, fn)
                    zfn = absfn[len(basedir)+len(os.sep):] #XXX: relative path
                    if not zfn in excludes:
                        z.write(absfn, zfn)

def replace_in_file(path, regexp, val):
    if os.path.exists(path):
        data = open(path).read()
        o = open(path, "w")
        o.write(re.sub(regexp, val, data))
        o.close()
    else:
        print "Warning: file not found %s" %path

def __parse_ini(path):
    f = open(path, "r")
    res = {}
    for line in f:
        if '=' in line:
            s = '='
        else:
            continue
        info = line.split(s,1)
        res[info[0]] = "".join(info[1].split())
    return res

def get_version():
    version_info = __parse_ini(os.path.join(ROOT,"version.ini"))
    build_info = __parse_ini(os.path.join(ROOT,"build","build.number"))
    return { "global": version_info["version.global"], \
             "major": version_info["version.major"], \
             "minor": version_info["version.minor"], \
             "build": build_info["build.number"]}

def get_version_string():
    version = get_version()
    return "%s.%s.%s (build #%s)" % (version['global'], version['major'], version['minor'], version['build'])

if __name__ == '__main__':
    print get_version_string()
