#!/usr/bin/python
# -*- coding: utf-8 -*-

import optparse
import builders.flash
import builders.js
import builders.site
import builders.build_utils
import builders.installs
import os
import shutil

ROOT = builders.build_utils.get_root()
PID_PATH = os.path.join(ROOT, 'build', 'build.pid')
SITE = '/data/anychart-site/trunk'
LAST_RELEASE_IMAGES = '/home/anychart-builders/anychart-master-images'
SELECTED_IMAGES = '/home/anychart-builders/anychart-python-server/webapp/selectedImages'

def main():
    builders.flash.build()
    builders.js.build()


def installs(buildKey, buildNumber):
    builders.installs.main()


def release():
    builders.flash.build()
    builders.js.build()
    builders.installs.main()
    builders.site.main(SITE)


def jstests_develop(buildKey, buildNumber):
    builders.js.build_trial()
    builders.flash.build_trial()
    build_screenshots(buildKey, buildNumber)


def tests(buildKey, buildNumber):
    build_screenshots(buildKey, buildNumber)


def mkdir_output(output):
    if not os.path.exists(output):
        os.mkdir(output)


def assign_data_in():
    data_in = builders.js_screenshots.DataIn()
    data_in.add(builders.js_screenshots.InputFolder('gallery', os.path.join(SITE, 'deploy', 'gallery', 'samples')))
    data_in.add(
        builders.js_screenshots.InputFolder('docs', os.path.join(SITE, 'deploy', 'docs', 'users-guide', 'Samples')))
    data_in.add(builders.js_screenshots.InputFolder('tests', os.path.join(ROOT, 'tests', 'automatic')))
    return data_in

def master(key, number):
    key = key.replace('-JOB1','')
    output = os.path.join(ROOT, 'out', 'images')
    builders.js.build_trial()
    builders.flash.build_trial()
    mkdir_output(output)
    data_in = assign_data_in()
    builders.js_screenshots.copyJsFiles(key,number)
    passedTests, failedTests = builders.js_screenshots.ImagesGenerator().generate(8, output, os.path.join(ROOT, 'out', 'trial'), data_in)
    report = builders.js_screenshots.ImageGenerationResults(key, '0' * (5 - len(str(number))) + str(number), passedTests, failedTests)
    report.generateXMLReport()
    if os.path.exists(LAST_RELEASE_IMAGES):
        shutil.rmtree(LAST_RELEASE_IMAGES)
    shutil.copytree(output, LAST_RELEASE_IMAGES)


def build_screenshots(key, number):
    import builders.js_screenshots
    #Cut JOB1 for right way in links:
    key = key.replace('-JOB1', '')
    output = os.path.join(ROOT, 'out', 'images')
    if not os.path.exists(output):
        os.mkdir(output)
    if not os.path.exists(os.path.join(output, 'tmp')):
        os.mkdir(os.path.join(output, 'tmp'))
    mkdir_output(output)
    shutil.copy(os.path.join(ROOT, 'build', 'utils', 'js_screenshots', 'icons.png'), os.path.join(output, 'icons.png'))
    builders.js_screenshots.setOutput(output)
    builders.js_screenshots.copyJsFiles(key, number)
    data_in = assign_data_in()
    passedTests, failedTests = builders.js_screenshots.ImagesGenerator().generate(5, output,
        os.path.join(ROOT, 'out', 'trial'), data_in)
    all_generated = len(passedTests + failedTests)
    all_comparedVersion = len(passedTests)
    passedTests, suspected, failed = builders.js_screenshots.ImagesComparer().compareVersion(passedTests,
        LAST_RELEASE_IMAGES)
    failedTests += failed
    all_comparedSelected = len(passedTests)
    passedTests, failSelected = builders.js_screenshots.ImagesComparer().compareSelected(passedTests, suspected,
        SELECTED_IMAGES)
    failedTests += failSelected
    passed, failed = builders.js_screenshots.ImagesGenerator().generateResized(5, os.path.join(output, 'tmp'),
        passedTests + failSelected) ### failedSelected should be deleted from here.
    all_generatedResized = len(passed + failed)
    failedTests += failed
    passed, failed = builders.js_screenshots.ImagesComparer().compareResized(passed, os.path.join(output),
        'Comparing resized')
    all_comparedResized = len(passed + failed)
    failedTests += failed
    passedTests += passed
    report = builders.js_screenshots.ImageGenerationResults(key, '0' * (5 - len(str(number))) + str(number), passedTests
        , failedTests)
    report.generateXMLReport()
    report.generateHTMLReport(all_generated, all_comparedVersion, all_comparedSelected, all_generatedResized,
        all_comparedResized)
    report.generateFailedSamples()
    shutil.rmtree(os.path.join(output, 'tmp'))
#=======================================================================================================================
#           PID
#=======================================================================================================================
def __write_pid():
    file = open(PID_PATH, 'w')
    file.write(str(os.getpid()))
    file.close()


def __remove_pid():
    os.remove(PID_PATH)

#=======================================================================================================================
#           Main
#=======================================================================================================================
if __name__ == "__main__":
    __write_pid()

    usage = 'usage: %prog [options] arg'
    parser = optparse.OptionParser(usage)
    parser.add_option('-b',
        '--build',
        dest='build',
        action='store',
        help='Target to built, possible value "debug", "deps".')
    parser.add_option('--buildKey', dest='buildKey', action='store')
    parser.add_option('--buildNumber', dest='buildNumber', action='store')
    (options, args) = parser.parse_args()

    if not options.build:
        main()

    #    bamboo main builds
    elif options.build == 'jstests-develop':
        jstests_develop(options.buildKey, options.buildNumber)
    elif options.build == 'tests':
        tests(options.buildKey, options.buildNumber)
    elif options.build == 'installs':
        installs(options.buildKey, options.buildNumber)
    elif options.build == 'release':
        release()
    elif options.build == 'master':
        import builders.js_screenshots
        master(options.buildKey, options.buildNumber)

    #    custom builds
    elif options.build == 'js-debug':
        builders.js.build_debug()
    elif options.build == 'js-deps':
        builders.js.build_deps()
    elif options.build == 'js-trial':
        builders.js.build_trial()
    elif options.build == 'js-all':
        builders.js.build()
    elif options.build == 'screenshots':
        build_screenshots(options.buildKey, options.buildNumber)
    elif options.build == 'flash':
        builders.flash.build()
    elif options.build == 'flash-trial':
        builders.flash.build_trial()
    else: print 'Wrong args, see --help'

    os.remove(PID_PATH)
