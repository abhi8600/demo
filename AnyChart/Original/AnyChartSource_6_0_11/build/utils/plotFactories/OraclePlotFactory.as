package com.anychart.plot {
	
	import com.anychart.Chart;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.viewController.ChartView;
	import flash.utils.getDefinitionByName;
	
	public final class PlotFactory {
		
		//---------------------------------------------------------
		//charts
		//---------------------------------------------------------
		
		private static var chartsInitialized:Boolean = initializeCharts();
		private static function initializeCharts():Boolean {
			
			import com.anychart.axesPlot.AxesPlot;
			import com.anychart.piePlot.PiePlot;
			import com.anychart.axesPlot.AxesPlot3D;
			var p1:AxesPlot;
			var p2:PiePlot;
			var p3:AxesPlot3D;
			
			/* bar template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::TemplateUtils")).getDefaultTemplate().copy());
			/* 3d bar template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::TemplateUtils3D")).getDefaultTemplate().copy());
			/* stock template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.stock::TemplateUtils")).getDefaultTemplate().copy());			
			/* line template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::TemplateUtils")).getDefaultTemplate().copy());
			/* marker template */  BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.markerSeries::TemplateUtils")).getDefaultTemplate().copy());
			
			import com.anychart.axesPlot.series.barSeries.BarGlobalSeriesSettings;
			import com.anychart.axesPlot.series.barSeries.BarGlobalSeriesSettings3D;
			var tmp2:BarGlobalSeriesSettings;
			var tmp2_1:BarGlobalSeriesSettings3D;
			
			import com.anychart.axesPlot.series.stock.CandlestickGlobalSeriesSettings;
			var tmp3:CandlestickGlobalSeriesSettings;
			
			import com.anychart.axesPlot.series.barSeries.RangeBarGlobalSeriesSettings;
			var tmp5:RangeBarGlobalSeriesSettings;
			
			import com.anychart.axesPlot.series.lineSeries.LineGlobalSeriesSettings;
			var tmp6:LineGlobalSeriesSettings;
			
			import com.anychart.axesPlot.series.markerSeries.MarkerGlobalSeriesSettings;
			var tmp9:MarkerGlobalSeriesSettings;
			
			import com.anychart.axesPlot.series.barSeries.RangeBarGlobalSeriesSettings3D;
			var tmp11:RangeBarGlobalSeriesSettings3D;
			
			return true;
		}
		
		include "_gauge_inc.as"
		include "_pie_inc.as"
		include "_map_inc.as"
		
		public static function getChartXML(source:String):XML { return PlotFactoryBase.getChartXML(source); }
		public static function createSinglePlot(view:ChartView, chart:Chart, xmlData:XML):void { PlotFactoryBase.createSinglePlot(view, chart, xmlData); }
		public static function createPlot(view:ChartView, chart:Chart, plotNode:XML):void { PlotFactoryBase.createPlot(view, chart, plotNode); }
	}
}