package com.anychart.radarPolarPlot.series.lineSeries
{
	import com.anychart.radarPolarPlot.PolarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.utils.PolarPoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.geom.Point;

	internal class PolarLinePoint extends LinePoint
	{
		public function PolarLinePoint()
		{
			super();
			
			this.needCreateDrawingPoints = true;
			this.needInitializePixValues = true;
			this.bottomStart = new Point();
			this.bottomEnd = new Point();
		}
		
		public function doInitializePixValues():void {
			super.initializePixValues();
		}
		
		public function doCreateDrawingPoints():void {
			this.createDrawingPoints();
		}
		
		override public function initialize():void {
			super.initialize();
			var s:LineSeries = LineSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		public var needCreateDrawingPoints:Boolean;
		public var needInitializePixValues :Boolean;
		
		public var bottomStart:Point;
		public var bottomEnd:Point;
		
		
		override protected function createDrawingPoints():void{
			
			var plot:RadarPolarPlot = RadarPolarPlot(this.plot);
			
			this.drawingPoints = [];
			var pt1:PolarPoint = new PolarPoint(plot.getCenter());
			var pt2:PolarPoint = new PolarPoint(pt1.getCenter());
			var t1:Point = new Point();
			var k:Number=0;
			if (this.index == 0) k=this.series.points.length;
			else k=this.index;
			if (this.hasPrev) {
				this.series.points[k-1].initializePixValues();
				plot.transform(this.series.points[k-1].x,this.series.points[k-1].y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt1.r =Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt1.phi = plot.getXAxis().scale.transform(this.series.points[k-1].x) + plot.getYAxisCrossing();
				plot.transform(this.x,this.y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt2.r = Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt2.phi = plot.getXAxis().scale.transform(this.x) + plot.getYAxisCrossing();
				this.drawingPoints.push(this.getPointBetween(pt1, pt2));
			}
			
			this.drawingPoints.push(pt2);
			
			if (this.index == this.series.points.length-1) k = -1;
			else k=this.index;
			if (this.hasNext) {
				this.series.points[k+1].initializePixValues();
				plot.transform(this.series.points[k+1].x,this.series.points[k+1].y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt1.r =Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt1.phi = plot.getXAxis().scale.transform(this.series.points[k+1].x) + plot.getYAxisCrossing();
				plot.transform(this.x,this.y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt2.r = Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt2.phi = plot.getXAxis().scale.transform(this.x) + plot.getYAxisCrossing();
				this.drawingPoints.push(this.getPointBetween(pt2,pt1));
			}
		}
		
		protected function getPointBetween(pt1:PolarPoint, pt2:PolarPoint):PolarPoint
		{
			var res:PolarPoint = new PolarPoint(RadarPolarPlot(this.plot).getCenter());
			var tmpPt2:Number = pt2.phi;
			
			if (pt2.phi < pt1.phi) {
				tmpPt2 += 360;
			}
			if (tmpPt2 == pt1.phi) {
				res.phi = tmpPt2;
				res.r = (pt1.r + pt2.r)/2;
				return res;
			} 
				
			res.phi = (tmpPt2 + pt1.phi)/2;  // get angle between points
			
			res.r = (pt2.r-pt1.r)*(res.phi - pt1.phi)/(tmpPt2 - pt1.phi) + pt1.r; // get Radius
			
			return res;
		}
		
		protected function normAngle(a1:Number):Number
		{
			return a1 - Math.floor(a1/360)*360;
		}
		
		override protected function drawSegment(g:Graphics):void
		{
			var s:Stroke = LineStyleState(this.styleState).stroke;
			if (s != null) {
				s.apply(g, LineSeries(this.series).gradientBounds , this.color);
				
				var i:int;
				/*if (s.dashed) {
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 0;i<(this.drawingPoints.length-1);i++) {
						DrawingUtils.drawDashedLine(g, s.spaceLength, s.dashLength, this.drawingPoints[i].x, this.drawingPoints[i].y, this.drawingPoints[i+1].x, this.drawingPoints[i+1].y);
					}
				}else {*/
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 1;i<this.drawingPoints.length;i++) 
						lineTo(g,this.drawingPoints[i-1], this.drawingPoints[i]);
				/*}*/
			}
		}
		
		protected function lineTo(g:Graphics, pt1:PolarPoint, pt2:PolarPoint):void {
			var dr:Number  = pt2.r - pt1.r;
			var dphi:Number = normAngle(pt2.phi - pt1.phi + 360);
			var tmp:PolarPoint = new PolarPoint(pt1.getCenter());
			tmp.phi = pt1.phi;
			var factor:Number = Math.min(Math.max(500/Math.min(tmp.x,tmp.y),0.1),5);
			while ( normAngle(pt2.phi + 360 - tmp.phi) >= factor )
			{
				tmp.r = dr*(tmp.phi - pt1.phi)/dphi + pt1.r;
				g.lineTo(tmp.x, tmp.y);
				tmp.phi += factor;
			}
			g.lineTo(pt2.x,pt2.y);
		}
	}
}