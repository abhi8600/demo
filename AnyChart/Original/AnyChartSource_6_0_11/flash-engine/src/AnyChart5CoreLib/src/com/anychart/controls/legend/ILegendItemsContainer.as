package com.anychart.controls.legend {
	public interface ILegendItemsContainer {
		function addItem(item:ILegendItem):void;
	}
}