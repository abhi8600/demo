package com.anychart.visual.text {
	import flash.geom.Rectangle;
	
	public class TextElementInformation {
		public var formattedText:String;
		public var nonRotatedBounds:Rectangle;
		public var rotatedBounds:Rectangle;
	}
}