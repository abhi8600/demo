package com.anychart.serialization {
	import com.anychart.dateTime.DateTimeParser;
	import com.anychart.elements.marker.MarkerType;
	import com.anychart.locale.DateTimeLocale;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.hatchFill.HatchFillType;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	public class SerializerBase {
		
		public static function isAuto(value:*):Boolean {
			return (value != undefined && String(value).toLowerCase() == "auto");
		}
		
		public static function isPercent(value:*):Boolean {
			var stringValue:String = String(value);
			return (stringValue.charAt(stringValue.length-1) == '%');
		}
		
		public static function getPercent(value:*):Number {
			var stringValue:String = String(value);
			return Number(stringValue.substr(0,stringValue.length-1))/100;
		}
		
		public static function isEnabled(node:XML, defaultValue:Boolean = true):Boolean {
			if (node == null) return false;
			if (node.@enabled == undefined) return defaultValue;
			return getBoolean(node.@enabled);
		}
		
		public static function getRatio(value:*):Number { 
			var res:Number = Number(value);
			if (res < 0) return 0;
			if (res > 1) return 1;
			return res;
		}
		
		public static function getNumber(value:*):Number {
			return Number(value);
		}
		
		public static function getEnumItem(value:*):String {
			return String(value).toLowerCase();
		}
		
		public static function getString(value:*):String {
			return String(value);
		}
		
		public static function getCDATAString(value:XML):String {
			return value.toString().split("\r\n").join("\n");
		}
		
		public static function getLString(value:*):String {
			return String(value).toLowerCase();
		}
		
		public static function getColor(value:*):uint {
			return ColorParser.getInstance().parse(value);
		}
		
		public static function isDynamicColor(value:*):Boolean {
			return ColorParser.getInstance().isDynamic(value);
		}
		
		public static function getBoolean(value:*):Boolean {
			return String(value).toLowerCase() == "true";
		}
		
		public static function isDynamicHatchType(value:*):Boolean {
			return SerializerBase.getEnumItem(value) == '%hatchtype';
		}
		
		public static function getHatchType(value:*):int {
			switch (SerializerBase.getEnumItem(value)) {
				case 'none': return HatchFillType.NONE;
				case 'backwarddiagonal': return HatchFillType.BACKWARD_DIAGONAL;
				case 'forwarddiagonal': return HatchFillType.FORWARD_DIAGONAL;
				case 'horizontal': return HatchFillType.HORIZONTAL;
				case 'vertical': return HatchFillType.VERTICAL;
				case 'dashedbackwarddiagonal': return HatchFillType.DASHED_BACKWARD_DIAGONAL;
				case 'dashedforwarddiagonal': return HatchFillType.DASHED_FORWARD_DIAGONAL;
				case 'dashedhorizontal': return HatchFillType.DASHED_HORIZONTAL;
				case 'dashedvertical': return HatchFillType.DASHED_VERTICAL;
				case 'diagonalcross': return HatchFillType.DIAGONAL_CROSS;
				case 'diagonalbrick': return HatchFillType.DIAGONAL_BRICK;
				case 'divot': return HatchFillType.DIVOT;
				case 'horizontalbrick': return HatchFillType.HORIZONTAL_BRICK;
				case 'verticalbrick': return HatchFillType.VERTICAL_BRICK;
				case 'checkerboard': return HatchFillType.CHECKER_BOARD;
				case 'confetti': return HatchFillType.CONFETTI;
				case 'plaid': return HatchFillType.PLAID;
				case 'soliddiamond': return HatchFillType.SOLID_DIAMOND;
				case 'zigzag': return HatchFillType.ZIG_ZAG;
				case 'weave': return HatchFillType.WEAVE;
				case 'percent05': return HatchFillType.PERCENT_05;
				case 'percent10': return HatchFillType.PERCENT_10;
				case 'percent20': return HatchFillType.PERCENT_20;
				case 'percent25': return HatchFillType.PERCENT_25;
				case 'percent30': return HatchFillType.PERCENT_30;
				case 'percent40': return HatchFillType.PERCENT_40;
				case 'percent50': return HatchFillType.PERCENT_50;
				case 'percent60': return HatchFillType.PERCENT_60;
				case 'percent70': return HatchFillType.PERCENT_70;
				case 'percent75': return HatchFillType.PERCENT_75;
				case 'percent80': return HatchFillType.PERCENT_80;
				case 'percent90': return HatchFillType.PERCENT_90;
				case 'grid': return HatchFillType.GRID;
				default: return -1;
			}
		}
		
		public static function getHorizontalAlign(value:*):uint {
			switch (SerializerBase.getEnumItem(value)) {
				case 'left': return HorizontalAlign.LEFT;
				case 'center': default: return HorizontalAlign.CENTER;
				case 'right': return HorizontalAlign.RIGHT;
			}
		}
		
		public static function getVerticalAlign(value:*):uint {
			switch (SerializerBase.getEnumItem(value)) {
				case 'top': return VerticalAlign.TOP;
				case 'center': default: return VerticalAlign.CENTER;
				case 'bottom': return VerticalAlign.BOTTOM;
			}
		}
		
		public static function getAnchor(value:*):uint {
			switch (SerializerBase.getEnumItem(value)) {
				case "center": return Anchor.CENTER;
				case "centerleft":
				case "leftcenter":
				case "left": 
					return Anchor.CENTER_LEFT;
				case "lefttop":
				case "topleft": 
					return Anchor.LEFT_TOP;
				case "centertop": 
				case "topcenter":
				case "top":
					return Anchor.CENTER_TOP;
				case "righttop":
				case "topright": 
					return Anchor.RIGHT_TOP;
				case "centerright":
				case "rightcenter":
				case "right": 
					return Anchor.CENTER_RIGHT;
				case "rightbottom":
				case "bottomright": 
					return Anchor.RIGHT_BOTTOM;
				case "centerbottom":
				case "bottomcenter":
				case "bottom": 
					return Anchor.CENTER_BOTTOM;
				case "leftbottom":
				case "bottomleft": 
					return Anchor.LEFT_BOTTOM;
				case "float": return Anchor.FLOAT;
				case "xaxis": return Anchor.X_AXIS;
			}
			return Anchor.CENTER;
		}
		
		public static function isDynamicMarkerType(value:*):Boolean {
			return SerializerBase.getEnumItem(value) == '%markertype';
		}
		
		public static function getMarkerType(value:*):uint {
			
			switch (SerializerBase.getEnumItem(value)) {
				case "none": return MarkerType.NONE;
				case "circle": return MarkerType.CIRCLE;
				case "square": return MarkerType.SQUARE;
				case "diamond": return MarkerType.DIAMOND;
				case "cross": return MarkerType.CROSS;
				case "diagonalcross": return MarkerType.DIAGONAL_CROSS;
				case "hline": return MarkerType.H_LINE;
				case "vline": return MarkerType.V_LINE;
				case "star4": return MarkerType.STAR_4;
				case "star5": return MarkerType.STAR_5;
				case "star6": return MarkerType.STAR_6;
				case "star7": return MarkerType.STAR_7;
				case "star10": return MarkerType.STAR_10;
				case "triangleup": return MarkerType.TRIANGLE_UP;
				case "triangledown": return MarkerType.TRIANGLE_DOWN;
				case "image": return MarkerType.IMAGE;
			}
			return 0;
		}
		
		public static function getDateTime(value:*, locale:DateTimeLocale):Number {
			return locale.getTimeStamp(SerializerBase.getString(value));
		}
	}
}