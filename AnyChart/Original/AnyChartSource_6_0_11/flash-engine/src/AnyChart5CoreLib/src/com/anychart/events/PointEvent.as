package com.anychart.events {
	import com.anychart.formatters.IFormatableSerializableObject;
	
	import flash.events.Event;
	
	public final class PointEvent extends Event {
		
		public static const POINT_CLICK:String = "pointClick";
		public static const POINT_MOUSE_OVER:String = "pointMouseOver";
		public static const POINT_MOUSE_OUT:String = "pointMouseOut";
		public static const POINT_MOUSE_DOWN:String = "pointMouseDown";
		public static const POINT_MOUSE_UP:String = "pointMouseUp";
		public static const POINT_SELECT:String = "pointSelect";
		public static const POINT_DESELECT:String = "pointDeselect";
		
		public var point:IFormatableSerializableObject;
		public var mouseX:Number;
		public var mouseY:Number;
		
		public function PointEvent(type:String, point:IFormatableSerializableObject, mouseX:Number, mouseY:Number) {
			this.point = point;
			this.mouseX = mouseX;
			this.mouseY = mouseY;
			super(type);
		}
		
		override public function clone():Event {
			return new PointEvent(this.type, this.point, this.mouseX, this.mouseY);
		}

	}
}