package com.anychart.formatters.terms
{
	public class TermStringConst implements ITerm
	{
		private var strConst:String;
		private var parentCall:ITerm; 
		
		public function TermStringConst(strConst:String, parentCall:ITerm) {
			this.strConst = strConst;
			this.parentCall = (parentCall != null ? parentCall : new TermEmpty());
		}
		
		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return parentCall.getValue(getTokenFunction, isDateTimeToken) + strConst;
		}

	}
}