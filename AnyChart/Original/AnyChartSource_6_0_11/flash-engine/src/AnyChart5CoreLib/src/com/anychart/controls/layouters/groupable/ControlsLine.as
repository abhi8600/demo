package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlAlign;
	import com.anychart.controls.layout.ControlLayout;
	
	import flash.geom.Rectangle;
	
	
	internal class ControlsLine {
		protected var nearControls:ControlsGroup;
		protected var centerControls:ControlsGroup;
		protected var farControls:ControlsGroup;
		
		protected var isOpposite:Boolean;
		
		public var basePositionRect:Rectangle;
		
		public function ControlsLine(isOpposite:Boolean) {
			this.isOpposite = isOpposite;
			this.nearControls = new ControlsGroup();
			this.centerControls = new ControlsGroup();
			this.farControls = new ControlsGroup();
		}
		
		public function addControl(control:Control):void {
			switch (control.layout.align) {
				case ControlAlign.NEAR:
					this.nearControls.addControl(control);
					break;
					
				case ControlAlign.CENTER:
					this.centerControls.addControl(control);
					break;
					
				case ControlAlign.FAR:
					this.farControls.addControl(control);
					break;
					
				case ControlAlign.SPREAD:
					this.stretchControl(control.layout);
					this.centerControls.addControl(control);
			}
		}
		
		protected function stretchControl(layout:ControlLayout):void {}
		
		protected function finalizeNearOrFarLayout(controls:Array, space:Number):void {}
		protected function finalizeCenterLayout(controls:Array, space:Number):void {}
		
		public function setControlsLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			this.nearControls.setAlign();
			this.centerControls.setAlign();
			this.farControls.setAlign();
			
			var nearSpace:Number = this.setNearLayout(chartRect, plotRect);
			var centerSpace:Number = this.setCenterLayout(chartRect, plotRect);
			var farSpace:Number = this.setFarLayout(chartRect, plotRect);
			
			this.finalizeNearOrFarLayout(this.nearControls.controls, nearSpace);
			this.finalizeNearOrFarLayout(this.farControls.controls, farSpace);
			this.finalizeCenterLayout(this.centerControls.controls, centerSpace);
			
			return Math.max(nearSpace, centerSpace, farSpace);
		}
		
		protected function setNearLayout(chartRect:Rectangle, plotRect:Rectangle):Number {return 0;}
		protected function setCenterLayout(chartRect:Rectangle, plotRect:Rectangle):Number {return 0;}
		protected function setFarLayout(chartRect:Rectangle, plotRect:Rectangle):Number {return 0;}
		
		protected function setMaxBounds(layout:ControlLayout, chartRect:Rectangle, plotRect:Rectangle):void {}
	}
}