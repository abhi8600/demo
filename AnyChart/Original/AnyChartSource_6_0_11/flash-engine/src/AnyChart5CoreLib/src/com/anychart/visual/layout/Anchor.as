package com.anychart.visual.layout {
	
	public final class Anchor {
		public static const CENTER:uint = 0;
		public static const CENTER_LEFT:uint = 1;
		public static const LEFT_TOP:uint = 2;
		public static const CENTER_TOP:uint = 3;
		public static const RIGHT_TOP:uint = 4;
		public static const CENTER_RIGHT:uint = 5;
		public static const RIGHT_BOTTOM:uint = 6;
		public static const CENTER_BOTTOM:uint = 7;
		public static const LEFT_BOTTOM:uint = 8;
		
		public static const FLOAT:uint = 9;
		public static const X_AXIS:uint = 10;
	}
}