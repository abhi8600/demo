package com.anychart.elements.styleStates {
	
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IRotationManager;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.StringUtils;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.geom.Rectangle;
	
	public class BaseElementStyleState extends StyleState {
		
		public var enabled:Boolean;
		public var anchor:uint;
		public var hAlign:uint;
		public var vAlign:uint;
		
		protected var normalHAlign:uint;
		protected var normalVAlign:uint;
		protected var normalAnchor:uint;
		protected var invertedHAlign:uint;
		protected var invertedVAlign:uint;
		protected var invertedAnchor:uint;
		
		public var padding:Number;
		
		public var bounds:Rectangle;
		
		public function BaseElementStyleState(style:Style) {
			super(style);
			this.anchor = Anchor.CENTER;
			this.hAlign = HorizontalAlign.CENTER;
			this.vAlign = VerticalAlign.CENTER;
			this.padding = 5;
			this.enabled = true;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@hatch_type != undefined) {
				if (data.hatch_fill[0] != null && data.hatch_fill[0].@type != undefined)
					data.hatch_fill[0].@type = StringUtils.replace(String(data.hatch_fill[0].@type).toLowerCase(),"%hatchtype",data.@hatch_type);
			}
			if (data.@enabled != undefined) 
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			return data;
		}
		
		override public function initializeRotations(manager:IRotationManager, baseScaleInverted:Boolean):void {
			this.normalAnchor = manager.getNormalAnchor(this.anchor);
			this.normalHAlign = manager.getNormalHAlign(this.hAlign, this.vAlign);
			this.normalVAlign = manager.getNormalVAlign(this.hAlign, this.vAlign);
			this.invertedAnchor = manager.getInvertedAnchor(this.anchor);
			this.invertedHAlign = manager.getInvertedHAlign(this.hAlign, this.vAlign);
			this.invertedVAlign = manager.getInvertedVAlign(this.hAlign, this.vAlign);
		}
		
		override public function setNormalRotation():void {
			this.hAlign = this.normalHAlign;
			this.vAlign = this.normalVAlign;
			this.anchor = this.normalAnchor;
		}
		
		override public function setInvertedRotation():void {
			this.hAlign = this.invertedHAlign;
			this.vAlign = this.invertedVAlign;
			this.anchor = this.invertedAnchor;
		}
	}
}