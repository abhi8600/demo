package com.anychart.selector
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public interface ISelectionProvider {
		
		function get drawingContainer():Shape;
		
		function get selectionEventsProvider():Sprite;
		function get extraSelectionEventsProvider():Sprite;
		function get lockSelectionEventsProvider():Sprite;
		
		function getSelectionBasePointCoords():Point;
		function getSelectionActualPointCoords():Point;
		
		function selectPointsInArea(area:Rectangle):void;
		function finalizePointsSelection(area:Rectangle):void;
		function deselectSelectedPoints():void;
	}
}