package com.anychart.uiControls.scrollBar
{
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class ScrollBarVertical extends ScrollBarControl
	{
		public function ScrollBarVertical(container:InteractiveObject) {
			buttonSub = new ScrollBarButtonUp();
			buttonAdd = new ScrollBarButtonDown();
			thumb = new ScrollBarThumbV();
			buttonSizeRect = new Rectangle();
			dragRect = new Rectangle();
			super(container);
		}
		
		override protected function setScrollBarSize(bounds:Rectangle):void {
			bounds.width = this.scrollBarSize;
		}

		override protected function resizeButtons():void {
			var s:Rectangle = size;
			var sB:Rectangle = buttonSizeRect;
			sB.x = 0;
			sB.y = 0;
			sB.width = s.width;
			sB.height = currentButtonSize;
			buttonSub.resize(sB);
			sB.y = s.height - sB.height;
			buttonAdd.resize(sB);

			var w:Number = s.height - 2 * currentButtonSize;
			var d:Number = maxValue - minValue;
			
			sB.y = buttonSize + (currentValue - minValue) * w / d;
			sB.height = thumbSize;
			thumb.resize(sB);
		}

		override protected function getThumbDragRect():Rectangle {
			dragRect.x = 0;
			dragRect.y = buttonSize;
			dragRect.width = 0;
			dragRect.height = size.height - 2 * buttonSize - thumbSize;
			return dragRect;
		}

		override protected function getLength():Number {
			return size != null ? size.height : 0;
		}
		
		override protected function getMouseCoord(e:MouseEvent):Number {
			return e.localY;
		}
		
		override protected function setThumbPos(value:Number):void {
			thumb.y = value;
		}

		override protected function getThumbPos():Number {
			return thumb.y;
		}

	}
}