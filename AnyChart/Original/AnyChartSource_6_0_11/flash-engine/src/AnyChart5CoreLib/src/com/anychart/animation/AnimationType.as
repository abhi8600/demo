package com.anychart.animation {
	
	public final class AnimationType {
		public static const APPEAR:uint							= 0;
		public static const SIDE_FROM_LEFT:uint					= 1;
		public static const SIDE_FROM_TOP:uint					= 2;
		public static const SIDE_FROM_RIGHT:uint				= 3;
		public static const SIDE_FROM_BOTTOM:uint				= 4;
		public static const SIDE_FROM_LEFT_TOP:uint				= 5;
		public static const SIDE_FROM_RIGHT_TOP:uint			= 6;
		public static const SIDE_FROM_RIGHT_BOTTOM:uint			= 7;
		public static const SIDE_FROM_LEFT_BOTTOM:uint			= 8;
		public static const SIDE_FROM_LEFT_CENTER:uint			= 9;
		public static const SIDE_FROM_TOP_CENTER:uint			= 10;
		public static const SIDE_FROM_RIGHT_CENTER:uint			= 11;
		public static const SIDE_FROM_BOTTOM_CENTER:uint		= 12;
		public static const SCALE_X_CENTER:uint					= 13;
		public static const SCALE_X_LEFT:uint					= 14;
		public static const SCALE_X_RIGHT:uint					= 15;
		public static const SCALE_Y_CENTER:uint					= 16;
		public static const SCALE_Y_TOP:uint					= 17;
		public static const SCALE_Y_BOTTOM:uint					= 18;
		public static const SCALE_XY_CENTER:uint				= 19;
		public static const SCALE_XY_LEFT:uint					= 20;
		public static const SCALE_XY_TOP:uint					= 21;
		public static const SCALE_XY_RIGHT:uint					= 22;
		public static const SCALE_XY_BOTTOM:uint				= 23;
		public static const SCALE_XY_LEFT_TOP:uint				= 24;
		public static const SCALE_XY_RIGHT_TOP:uint				= 25;
		public static const SCALE_XY_RIGHT_BOTTOM:uint			= 26;
		public static const SCALE_XY_LEFT_BOTTOM:uint			= 27;
		public static const SHOW:uint							= 28;
		public static const OUTSIDE:uint 						= 29;
	}
}