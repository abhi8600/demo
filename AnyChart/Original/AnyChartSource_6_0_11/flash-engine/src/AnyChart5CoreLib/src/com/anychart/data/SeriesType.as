package com.anychart.data {
	
	public final class SeriesType {
		public static const BAR:uint = 0;
		public static const RANGE_BAR:uint = 1;
		
		public static const LINE:uint = 2;
		public static const SPLINE:uint = 3;
		public static const STEP_LINE_FORWARD:uint = 4;
		public static const STEP_LINE_BACKWARD:uint = 5;
		
		public static const AREA:uint = 6;
		public static const SPLINE_AREA:uint = 7;
		public static const STEP_AREA_FORWARD:uint = 8;
		public static const STEP_AREA_BACKWARD:uint = 9;
		public static const RANGE_AREA:uint = 10;
		public static const RANGE_SPLINE_AREA:uint = 11;
		
		public static const BUBBLE:uint = 12;
		
		public static const OHLC:uint = 13;
		public static const CANDLESTICK:uint = 14;
		
		public static const MAP_REGION:uint = 15;
		public static const PIE:uint = 16;
		
		public static const CONNECTOR:uint = 17;
		public static const MARKER:uint = 18;
		
		public static const FUNNEL:uint = 19;
		public static const TABLE:uint = 20;
		public static const CONE:uint = 21;
		public static const PYRAMID:uint = 22;
	
		public static const HEAT_MAP:uint = 23;
		
		public static const TREE_MAP:uint = 24;
		
		public static function isLine(type:uint):Boolean {
			return type == LINE || type == SPLINE || type == STEP_LINE_FORWARD || type == STEP_LINE_BACKWARD;
		}
	}
}