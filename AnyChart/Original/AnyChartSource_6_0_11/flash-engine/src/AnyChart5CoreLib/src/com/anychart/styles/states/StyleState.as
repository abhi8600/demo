package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IRotationManager;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.Style;
	import com.anychart.utils.StringUtils;
	import com.anychart.visual.gradient.Gradient;
	
	public class StyleState {
		
		public var style:Style;
		public var programmaticStyle:ProgrammaticStyle;
		
		/**
		 * 0 - normal
		 * 1 - hover
		 * 2 - pushed
		 * 3 - selected normal
		 * 4 - selected hover
		 * 5 - missing
		 */
		public var stateIndex:uint = 0;
		
		public function StyleState(style:Style) {
			this.style = style;
		}
		
		public function initializeRotations(manager:IRotationManager, baseScaleInverted:Boolean):void {}
		public function setNormalRotation():void {}
		public function setInvertedRotation():void {}
		
		public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = data.copy();
			if (data.@color != undefined) {
				var color:String = data.@color;
				var childElements:XMLList = data..*;
				for each (var childNode:XML in childElements) {
					if (childNode.@color != undefined) {
						childNode.@color = String(childNode.@color).toLowerCase();
						childNode.@color = StringUtils.replace(childNode.@color,"%color",color);
					}
				}
			}
			return data;
		}
		
		protected final function setGradientAngles(gradient:Gradient, baseScaleInverted:Boolean, manager:IRotationManager):void {
			gradient.normalAngle = manager.getNormalAngle(gradient.angle, baseScaleInverted);
			gradient.invertedAngle = manager.getInvertedAngle(gradient.angle, baseScaleInverted);
		}
	}
}