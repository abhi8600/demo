package com.anychart {
	import com.anychart.locale.DateTimeLocale;
	
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.ui.ContextMenu;
	
	
	public interface IAnyChart extends IEventDispatcher {
		
		function getXMLData():XML;
		
		function setXMLFile(path:String, replaces:Array = null, needClear:Boolean = true):void;
		function setXMLData(data:String, replaces:Array = null, needClear:Boolean = true):void;
		function setSource(source:String, replaces:Array = null, needClear:Boolean = true):void;
		function setViewXMLFile(viewId:String, path:String, replaces:Array = null):void;
		function setViewXMLData(viewId:String, data:String, reaplces:Array = null):void;
		function setViewInternalSource(viewId:String, sourceId:String, replaces:Array = null):void;
		function setLoading(text:String):void;
		function setViewLoading(viewId:String, text:String):void;
		
		function getBase64PNG(width:* = undefined, height:* = undefined):String;
		function getBase64PDF(width:* = undefined, height:* = undefined):String;
		function saveAsImage(width:* = undefined, height:* = undefined):void;
		function saveAsPDF(width:* = undefined, height:* = undefined):void;
		
		function printChart():void;
		
		function getBounds():Rectangle;
		function getContainer():Sprite;
		function resize(newBounds:Rectangle):void;
		function getTitle():String;
		function get contextMenu():ContextMenu;
		
		function showSecurityError(event:SecurityErrorEvent):void;
		function showFatalError(e:Error):void;
		
		function getMainContainer():InteractiveObject;
		function get enableAnimation():Boolean;
		function get loopAnimation():Boolean;
		function get loopDelay():Number;
		
		function get dateTimeLocale():DateTimeLocale;
		function showPreloaderOnTheTop(text:String):void;
		function hideTopPreloader():void;
		
		function setXZoom(settings:Object):void; 
		function setYZoom(settings:Object):void; 
		function setZoom(xZoomSettings:Object, yZoomSettings:Object):void; 
		function scrollXTo(xValue:String):void; 
		function scrollYTo(yValue:String):void; 
		function scrollTo(xValue:String, yValue:String):void; 
		
		function setViewXZoom(viewName:String, settings:Object):void; 
		function setViewYZoom(viewName:String, settings:Object):void; 
		function setViewZoom(viewName:String, xZoomSettings:Object, yZoomSettings:Object):void; 
		function viewScrollXTo(viewName:String, xValue:String):void; 
		function viewScrollYTo(viewName:String, yValue:String):void;
		function viewScrollTo(viewName:String, xValue:String, yValue:String):void; 
		
		function getDataSets():XML;
	}
}