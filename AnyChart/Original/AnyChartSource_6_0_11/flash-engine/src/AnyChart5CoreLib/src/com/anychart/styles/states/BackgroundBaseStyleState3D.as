package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.effects.EffectsList;
	
	public final class BackgroundBaseStyleState3D extends StyleStateWithEffects {
		
		public var front:BackgroundBaseStyleState3DEntry;
		public var back:BackgroundBaseStyleState3DEntry;
		public var visibleHorizontal:BackgroundBaseStyleState3DEntry;
		public var hiddenHorizontal:BackgroundBaseStyleState3DEntry;
		public var visibleVertical:BackgroundBaseStyleState3DEntry;
		public var hiddenVertical:BackgroundBaseStyleState3DEntry;
		
		public function BackgroundBaseStyleState3D(style:Style) {
			super(style);
			
			this.front = new BackgroundBaseStyleState3DEntry(style);
			this.back = new BackgroundBaseStyleState3DEntry(style);
			this.visibleHorizontal = new BackgroundBaseStyleState3DEntry(style);
			this.hiddenHorizontal = new BackgroundBaseStyleState3DEntry(style);
			this.visibleVertical = new BackgroundBaseStyleState3DEntry(style);
			this.hiddenVertical = new BackgroundBaseStyleState3DEntry(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			
			if (data.effects[0] != null) {
				if (SerializerBase.isEnabled(data.effects[0],true)) {
					if (this.effects == null) this.effects = new EffectsList();
					this.effects.deserialize(data.effects[0]);
				}else {
					this.effects = null;
				}
			}
			
			var frontSideNodeName:String = "front_side";
			var backSideNodeName:String = "back_side";
			var visibleHorizontalSideNodeName:String = "visible_horizontal_side";
			var hiddenHorizontalSideNodeName:String = "hidden_horizontal_side";
			var visibleVerticalSideNodeName:String = "visible_vertical_side";
			var hiddenVerticalSideNodeName:String = "hidden_vertical_side";
			
			var frontXML:XML = XMLUtils.merge(data, data[frontSideNodeName][0], frontSideNodeName);
			var backXML:XML = XMLUtils.merge(data, data[backSideNodeName][0], backSideNodeName);
			var visibleHorizontal:XML = XMLUtils.merge(data, data[visibleHorizontalSideNodeName][0], visibleHorizontalSideNodeName);
			var hiddenHorizontal:XML = XMLUtils.merge(data, data[hiddenHorizontalSideNodeName][0], hiddenHorizontalSideNodeName);
			var visibleVertical:XML = XMLUtils.merge(data, data[visibleVerticalSideNodeName][0], visibleVerticalSideNodeName);
			var hiddenVertical:XML = XMLUtils.merge(data, data[hiddenVerticalSideNodeName][0], hiddenVerticalSideNodeName);
			
			this.front.deserialize(frontXML, resources);
			this.back.deserialize(backXML, resources);
			this.visibleHorizontal.deserialize(visibleHorizontal, resources);
			this.hiddenHorizontal.deserialize(hiddenHorizontal, resources);
			this.visibleVertical.deserialize(visibleVertical, resources);
			this.hiddenVertical.deserialize(hiddenVertical, resources);
			return data;
		}

	}
}