package com.anychart.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.layout.AbstractAlign;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	
	/**
	 * Class for interactive titles without formatting
	 * Used in Preloader and error messages
	 * 
	 * Supports only one NavigateToURL action
	 */
	public final class NonFormatableTitle extends BaseNonFormatableTextElement {
		
		private var request:URLRequest;
		private var urlTarget:String;
		
		protected var container:Sprite;
		public var info:TextElementInformation;
		
		public var align:uint = AbstractAlign.CENTER;
		public var padding:Number = 5;
		
		public function NonFormatableTitle() {
			super();
			this.container = new Sprite();
			this.info = this.createInformation();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			super.deserialize(data, resources, style);
			if (data.text[0] != null) 
				this.text = this.getCDATAString(data.text[0]);
			if (data.@align != undefined) {
				switch (SerializerBase.getEnumItem(data.@align)) {
					case "near":
						this.align = AbstractAlign.NEAR;
						break;
					case "far":
						this.align = AbstractAlign.FAR;
						break;
					case "center":
						this.align = AbstractAlign.CENTER;
						break;
				}
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
			if (data.actions[0] != null && 
				data.actions[0].action[0] != null && 
				data.actions[0].action[0].@type != undefined &&
				SerializerBase.getEnumItem(data.actions[0].action[0].@type) == "navigatetourl") {
					var act:XML = data.actions[0].action[0];
					if (act.@url != undefined) this.request = new URLRequest(SerializerBase.getString(act.@url));
					if (act.@url_target != undefined) this.urlTarget = SerializerBase.getString(act.@url_target);
					if (this.urlTarget == null) this.urlTarget = "_blank";
					if (this.request != null) {
						this.container.buttonMode = true;
						this.container.useHandCursor = true;
						this.container.addEventListener(MouseEvent.CLICK, this.clickHandler);
					}
			}
		}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			return this.container;
		}

		public function resize(newBounds:Rectangle):void {}
		
		public function drawTitle():void {
			this.container.graphics.clear();
			this.draw(this.container.graphics,0,0,this.info,0,0);
		}
		
		private function clickHandler(event:MouseEvent):void {
			navigateToURL(this.request, this.urlTarget);
		}
	}
}