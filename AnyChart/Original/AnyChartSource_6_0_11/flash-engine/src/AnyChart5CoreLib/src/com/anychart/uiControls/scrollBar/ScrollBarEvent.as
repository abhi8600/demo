package com.anychart.uiControls.scrollBar
{
	import flash.events.Event;

	public final class ScrollBarEvent extends Event
	{
		public static const CHANGE:String = 'SB.ScrollBarChangeValue';

		public var value:Number;
		
		public function ScrollBarEvent(type:String, value:Number = 0) {
			this.value = value;
			super(type);
		}
	}
}