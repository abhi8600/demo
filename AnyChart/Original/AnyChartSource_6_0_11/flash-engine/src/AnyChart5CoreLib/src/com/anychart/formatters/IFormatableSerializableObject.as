package com.anychart.formatters {
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	
	public interface IFormatableSerializableObject extends IFormatable, IObjectSerializable {
	}
}