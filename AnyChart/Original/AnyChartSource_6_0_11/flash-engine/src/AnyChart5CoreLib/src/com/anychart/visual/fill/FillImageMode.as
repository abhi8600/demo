package com.anychart.visual.fill {
	
	internal final class FillImageMode {
		public static const STRETCH:uint = 0;
		public static const TILE:uint = 1;
		public static const FIT_BY_PROPORTIONS:uint = 2;
	}
}