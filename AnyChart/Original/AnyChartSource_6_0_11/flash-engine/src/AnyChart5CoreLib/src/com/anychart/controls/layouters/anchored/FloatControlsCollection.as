package com.anychart.controls.layouters.anchored {
	import com.anychart.IChart;
	import com.anychart.controls.Control;
	
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	
	public final class FloatControlsCollection extends AnchoredControlsCollection {
		private var isLayoutFinalized:Boolean = false;
		
		private var chart:IChart;
		
		override public function addControl(control:Control):void {
			this.chart = control.plotContainer;
			control.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			super.addControl(control);
		}
		
		override public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle):Boolean {
			if (this.chart == null) return true;
			if (isLayoutFinalized) {
				this.updateLayout(plotRect, chartRect);
			}else {
				super.finalizeLayout(plotRect, chartRect);
				this.isLayoutFinalized = true;
			}
			return true;
		}
		
		private function updateLayout(plotRect:Rectangle, chartRect:Rectangle):void {
			var rc:Rectangle = this.chart.getChartTotalBounds();
			for each (var control:Control in this.controls) {
				control.layout.maxWidth = chartRect.width;
				control.layout.maxHeight = chartRect.height;
				control.setBounds(chartRect, plotRect);
				if (!isNaN(control.layout.floatX)) {
					control.bounds.x = control.layout.floatX*rc.width;
					control.bounds.y = control.layout.floatY*rc.height;
				}else {
					this.setControlPosition(control, control.layout.alignByDataPlot ? plotRect : chartRect);
				}
			}
		}
		
		
		private var currentTarget:Control;
		private function mouseDownHandler(e:MouseEvent):void {
			var target:Control = Control(e.currentTarget);
			currentTarget = target;
			var rc:Rectangle = this.chart.getChartTotalBounds().clone();
			
			rc.width -= target.bounds.width;
			rc.height -= target.bounds.height;
			
			target.container.startDrag(false, rc);
			target.container.stage.addEventListener(MouseEvent.MOUSE_UP, this.mouseUpHandler);
		}
		
		private function mouseUpHandler(e:MouseEvent):void {
			
			var rc:Rectangle = this.chart.getChartTotalBounds();
			currentTarget.layout.floatX = currentTarget.container.x / rc.width;
			currentTarget.layout.floatY = currentTarget.container.y / rc.height;
			
			currentTarget.container.stopDrag();
			currentTarget.container.stage.removeEventListener(MouseEvent.MOUSE_UP, this.mouseUpHandler);
		}
	}
}