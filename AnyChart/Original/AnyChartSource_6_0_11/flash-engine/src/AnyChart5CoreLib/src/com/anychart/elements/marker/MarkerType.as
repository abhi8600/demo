package com.anychart.elements.marker {
	
	public final class MarkerType {
		public static const NONE:uint = 0;
		public static const CIRCLE:uint = 1;
		public static const SQUARE:uint = 2;
		public static const DIAMOND:uint = 3;
		public static const CROSS:uint = 4;
		public static const DIAGONAL_CROSS:uint = 5;
		public static const H_LINE:uint = 6;
		public static const V_LINE:uint = 7;
		public static const STAR_4:uint = 8;
		public static const STAR_5:uint = 9;
		public static const STAR_6:uint = 10;
		public static const STAR_7:uint = 11;
		public static const STAR_10:uint = 12;
		public static const TRIANGLE_UP:uint = 13;
		public static const TRIANGLE_DOWN:uint = 14;
		public static const IMAGE:uint = 15;
	}
}