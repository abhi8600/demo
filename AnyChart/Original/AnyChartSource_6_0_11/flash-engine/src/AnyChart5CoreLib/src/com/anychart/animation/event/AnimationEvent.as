package com.anychart.animation.event {
	import flash.events.Event;

	public final class AnimationEvent extends Event {
		
		public static const ANIMATION_FINISH:String = "animationFinish";
		
		public function AnimationEvent(type:String) {
			super(type);
		}
		
	}
}