package com.anychart.serialization {
	
	public interface ISerializable {
		function deserialize(data:XML):void;
	}
}