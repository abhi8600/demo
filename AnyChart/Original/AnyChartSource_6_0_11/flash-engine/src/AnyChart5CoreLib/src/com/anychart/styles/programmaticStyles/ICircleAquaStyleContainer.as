package com.anychart.styles.programmaticStyles {
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public interface ICircleAquaStyleContainer {
		function getCurrentState():BackgroundBaseStyleState;
		function getBounds():Rectangle;
		function getGraphics():Graphics;
		function getActualColor():uint;
		function getActualHatchType():uint;
		function drawShape():void;
	}
}