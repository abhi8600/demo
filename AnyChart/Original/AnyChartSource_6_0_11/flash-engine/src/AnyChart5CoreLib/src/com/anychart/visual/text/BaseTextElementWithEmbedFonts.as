package com.anychart.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	public class BaseTextElementWithEmbedFonts extends BaseTextElement {
		override protected function createFontSettings():FontSettings { return new EmbedableFontSettings(); }
		
		public function get numLines():int {
			return this.field.numLines;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources, style);
		}
		
		public function updateField():void {
			EmbedableFontSettings(this.font).updateFormat();
			this.field.defaultTextFormat = this.font.format;
		}
		
		override internal function createTextField():TextField {
			var tf:TextField = super.createTextField();
			tf.selectable = false;
			tf.embedFonts = EmbedableFontSettings(this.font).isEmbed;
			return tf;
		}
		
		public function drawEmbed(container:Sprite, x:Number, y:Number, info:TextElementInformation, color:uint, hatchType:uint):void {
			if (!EmbedableFontSettings(this.font).isEmbed) {
				this.draw(container.graphics, x, y, info, color, hatchType);
				return;
			}
			
			var field:TextField = this.createTextField();
			var s:Sprite = this.createBackgroundSprite(field);
			this.setFieldProperties(field, info, color);
			
			if (this.background != null) {
				this.background.draw(s.graphics, info.nonRotatedBounds, color, hatchType);
				if (this.background.effects != null)
					s.filters = this.background.effects.list;
			}
			
			s.rotation = this.rotation;
			container.addChild(s);
			
			var newBounds:Rectangle = s.getBounds(container);
			s.x = x - newBounds.x;
			s.y = y - newBounds.y;
		}
	}
}