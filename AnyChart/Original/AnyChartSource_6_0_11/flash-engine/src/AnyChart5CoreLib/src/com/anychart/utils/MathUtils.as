package com.anychart.utils {
	import flash.geom.Point;
	
	public final class MathUtils {
		
		public static function reflectAngleByCos(angle:Number):Number {
			return (180 - angle);
		}
		
		public static function reflectAngleBySin(angle:Number):Number {
			return (-angle);
		}

		/*** return the distance between two points */
		public static function getDistance(p0:Point, p1:Point):Number {
			return getDistanceBy4Coords(p0.x, p0.y, p1.x, p1.y);
			//return Math.sqrt(p0.x * p0.x - 2 * p0.x * p1.x + p1.x * p1.x + p0.y * p0.y - 2 * p0.y * p1.y + p1.y * p1.y);
		}

		/*** return the distance between two points */
		public static function getDistanceBy4Coords(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			return Math.sqrt(Math.abs(x1 * x1 - 2 * x1 * x2 + x2 * x2 + y1 * y1 - 2 * y1 * y2 + y2 * y2));
		}

		/*** return the middle of a segment define by two points */
		public static function getMiddle(p0:Point, p1:Point):Point {
			return new Point((p0.x + p1.x) * 0.5, (p0.y + p1.y) * 0.5);
		}

		/**
		 * return a point on a segment [p0, p1] which distance from P0
		 * is ratio of the length [p0, p1]
		 */
		public static function getPointOnSegment(p0:Point, p1:Point, ratio:Number):Point {
			return new Point((p0.x + ((p1.x - p0.x) * ratio)), (p0.y + ((p1.y - p0.y) * ratio)));
		}
		
		//dirty hack for this bug: http://habrahabr.ru/blogs/Flash_Platform/131301/
		//Hate this
		public static function divide(a:Number, b:Number):Number {
			if (b == 0) {
				if (a == 0) return Number.NaN;
				return a > 0 ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY; 
			}
			return a/b;
		}
	}
}