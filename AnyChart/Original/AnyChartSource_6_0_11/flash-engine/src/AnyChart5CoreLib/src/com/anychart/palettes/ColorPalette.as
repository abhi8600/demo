package com.anychart.palettes {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.gradient.GradientEntry;
	
	public class ColorPalette extends BasePalette {
		
		private var gradient:Gradient;
		private var isAutoGradient:Boolean;
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			var isRangePalette:Boolean = false;
			if (data.@type != undefined) 
				isRangePalette = SerializerBase.getEnumItem(data.@type) == "colorrange";
				
			if (isRangePalette) {
				this.isAutoGradient = true;
				if (data.@color_count != undefined && !SerializerBase.isAuto(data.@color_count)) {
					var g:Gradient = new Gradient(1);
					g.deserialize(data.gradient[0]);
					this.initRangedPalette(g, SerializerBase.getNumber(data.@color_count));
					this.isAutoGradient = false;
				}
				this.gradient = new Gradient(1);
				this.gradient.deserialize(data.gradient[0]);
			}else {
				var itemsCount:uint = data.item.length();
				for (var i:uint = 0;i<itemsCount;i++) {
					this.items.push(SerializerBase.getColor(data.item[i].@color));
				}
			}
		}
		
		public function checkAuto(itemsCount:uint, ignoreDefault:Boolean = false):ColorPalette {
			var res:ColorPalette = this;
			if ((this.isAutoGradient && !ignoreDefault) || (this.gradient && ignoreDefault)) {
				res = new ColorPalette();
				res.items = [];
				res.initRangedPalette(this.gradient, itemsCount);
			}
			return res;
		}
		
		private function initRangedPalette(grd:Gradient,count:uint):void {
			var i:int;
			if (count<2) {
				for (i = 0;i<count;i++) {
					if (grd.entries[i])
						this.items.push(GradientEntry(grd.entries[i]).color);
				}
			}
			for(i=0;i<count;i++)
				this.items.push(this.getColorItem(grd,Number(i)/Number(count-1)));
		}
		
		
		private function getColorItem(grd:Gradient,fPlace:Number):uint {
			var	idx_start:int=-1,idx_end:int=-1;
			var	place_end:Number=10000000,place_start:Number=-1;
		
			for(var i:int=0;i<grd.entries.length;i++) {
				var key:GradientEntry=GradientEntry(grd.entries[i]);
				var rt:Number=Number(grd.ratios[i])/255.0;
				if(rt==fPlace) return key.color;
		
				if(rt<fPlace) {
					if(rt>place_start) {
						idx_start=i;
						place_start=rt;
					}
				}
		
				if(rt>fPlace) {
					if(rt<place_end) {
						idx_end=i;
						place_end=rt;
					}
				}
			}
		
			if(idx_start==-1) {
				if(idx_end==-1)	return 0xFFFFFF;
				else return GradientEntry(grd.entries[idx_end]).color;
			}
			else if(idx_end==-1)	return GradientEntry(grd.entries[idx_start]).color;
			
			if(place_start==place_end)	return GradientEntry(grd.entries[idx_start]).color;
		
			var startKey:GradientEntry=GradientEntry(grd.entries[idx_start]);
			var srt:Number=Number(grd.ratios[idx_start])/255.0;
			var endKey:GradientEntry=GradientEntry(grd.entries[idx_end]);
			var ert:Number=Number(grd.ratios[idx_end])/255.0;
				
			var	pos:Number=1-(fPlace-srt)/(ert-srt);
			return ColorUtils.blendColor(startKey.color,endKey.color,pos);
		}
	}
}