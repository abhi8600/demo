package com.anychart.scales {
	
	public final class ScaleMode {
		public static const NORMAL:uint = 0;
		public static const STACKED:uint = 1;
		public static const PERCENT_STACKED:uint = 2;
		public static const OVERLAY:uint = 3;
		public static const SORTED_OVERLAY:uint = 4;
	}
}