package com.anychart.visual.hatchFill {
	
	import com.anychart.serialization.IStyleSerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class HatchFill implements IStyleSerializable {
		
		// Properties.		
		public var enabled:Boolean;
		
		public var isDynamicType:Boolean;
		public var type:uint;
		
		public var isDynamicHatchColor:Boolean;
		public var color:uint;
		
		
		public var thickness:Number;
		public var opacity:Number;
		public var patternSize:Number;
		
		private var _pattern:BitmapData;
		private var _matrix:Matrix;
		
		public function HatchFill() {
			this.enabled = false;
			this.isDynamicType = false;
			this.type = HatchFillType.BACKWARD_DIAGONAL;
			this.isDynamicHatchColor = false;
			this.color = 0;
			this.thickness = 1;
			this.opacity = .5;
			this.patternSize = 10;
		}
		
		private function initializePattern():void {
			if (this.type == HatchFillType.NONE) return;
			var patternInfo:Array = this.getPattern(this.type);
			this._pattern = patternInfo[0];
			this._matrix = patternInfo[1];
		}
		
		private function getARGB(color:uint,alpha:Number):uint {
			return color + ((alpha*0xFF)<<24);
		}
		
		public function getPattern(type:uint):Array {
			var sp:Shape=new Shape();
			var gp:Graphics=sp.graphics;
			
			gp.beginFill(0,0);
			gp.lineStyle(this.thickness,this.color,this.opacity,true);
			
			var rect:Rectangle;
			var s:Number;
			var ds:Number;
			
			var m:Matrix = new Matrix();	
			var b:BitmapData = new BitmapData(this.patternSize,this.patternSize,true,0);
			
			// create pattern
			switch(type) {
				case HatchFillType.BACKWARD_DIAGONAL:						
 					gp.moveTo(-1,0);
					gp.lineTo(this.patternSize+1,0);
					m.rotate(-Math.PI/4);
				break;
				
				case HatchFillType.FORWARD_DIAGONAL:
					gp.moveTo(-1,0);
					gp.lineTo(this.patternSize+1,0);
					m.rotate(Math.PI/4);
				break;
				
				case HatchFillType.HORIZONTAL:
					gp.moveTo(-1,this.patternSize/2);
					gp.lineTo(this.patternSize+1,this.patternSize/2);
				break;
				
				case HatchFillType.VERTICAL:
					gp.moveTo(this.patternSize/2,-1);
					gp.lineTo(this.patternSize/2,this.patternSize+1);
				break;					
					
				case HatchFillType.DIAGONAL_CROSS:
					gp.moveTo(0,this.patternSize/2);
					gp.lineTo(this.patternSize,this.patternSize/2);
					gp.moveTo(this.patternSize/2,0);
					gp.lineTo(this.patternSize/2,this.patternSize);
					m.rotate(Math.PI/4);
				break; 					
 				
 				case HatchFillType.GRID:
					gp.moveTo(-1,this.patternSize/2);
					gp.lineTo(this.patternSize+1,this.patternSize/2);
					gp.moveTo(this.patternSize/2,-1);
					gp.lineTo(this.patternSize/2,this.patternSize+1);
				break; 
 				
 				case HatchFillType.HORIZONTAL_BRICK:
				
					gp.moveTo(0,0);
					gp.lineTo(0,this.patternSize/2-1);
					gp.moveTo(0,this.patternSize/2-1);
					gp.lineTo(this.patternSize,this.patternSize/2-1);
					gp.moveTo(this.patternSize/2,this.patternSize/2-1);
					gp.lineTo(this.patternSize/2,this.patternSize-1);						
					gp.moveTo(0,this.patternSize-1);
					gp.lineTo(this.patternSize,this.patternSize-1);
				break;
				
				case HatchFillType.VERTICAL_BRICK:
					gp.moveTo(0,0);
					gp.lineTo(0,this.patternSize/2-1);
					gp.moveTo(0,this.patternSize/2-1);
					gp.lineTo(this.patternSize,this.patternSize/2-1);
					gp.moveTo(this.patternSize/2,this.patternSize/2-1);
					gp.lineTo(this.patternSize/2,this.patternSize-1);						
					gp.moveTo(0,this.patternSize-1);
					gp.lineTo(this.patternSize,this.patternSize-1);
					m.rotate(Math.PI/2);
				break;
					
				case HatchFillType.DIAGONAL_BRICK:
					gp.moveTo(0,0);
					gp.lineTo(0,this.patternSize/2-1);
					gp.moveTo(0,this.patternSize/2-1);
					gp.lineTo(this.patternSize,this.patternSize/2-1);
					gp.moveTo(this.patternSize/2,this.patternSize/2-1);
					gp.lineTo(this.patternSize/2,this.patternSize-1);						
					gp.moveTo(0,this.patternSize-1);
					gp.lineTo(this.patternSize,this.patternSize-1);
					m.rotate(Math.PI/4);
				break;
				
				case HatchFillType.CHECKER_BOARD:

					gp.lineStyle(0,0,0);
					gp.beginFill(color,opacity);
					gp.drawRect(0,0,this.patternSize/2,this.patternSize/2);
					gp.drawRect(this.patternSize/2,this.patternSize/2,this.patternSize,this.patternSize);
				break;
					
				case HatchFillType.CONFETTI:
					s = this.patternSize/8;
					
					rect = new Rectangle(0,0,this.patternSize/4,this.patternSize/4);
					
					gp.lineStyle(0,0,0);
					gp.beginFill(color,opacity);		
															
					gp.drawRect(0,s*2,rect.width,rect.height);
					gp.drawRect(s,5*s,rect.width,rect.height);
					gp.drawRect(2*s,0,rect.width,rect.height);
					gp.drawRect(s*4,s*4,rect.width,rect.height);
					gp.drawRect(s*5,s,rect.width,rect.height);
					gp.drawRect(s*6,s*6,rect.width,rect.height);
				break;
				
				case HatchFillType.PLAID:
					rect = new Rectangle(0,0,this.patternSize/4,this.patternSize/4);
					
					gp.lineStyle(0,0,0);
					gp.beginFill(color,opacity);		
					gp.drawRect(0,0,this.patternSize/2,this.patternSize/2);
					
					s = this.patternSize/8;
					var isSelected:Boolean = false;
					for (var dx:uint = 0;dx<2;dx++) {
						isSelected = false;
						for (var xPos:uint = 0;xPos<4;xPos++) {
							isSelected = !isSelected;
							for (var yPos:uint = 0;yPos<4;yPos++) {
								if (isSelected) {
									var xPosReal:Number = Number(xPos)*s + Number(dx)*this.patternSize/2;
									var yPosReal:Number = Number(yPos)*s + this.patternSize/2;
									gp.drawRect(xPosReal,yPosReal,s,s);
								}
								isSelected = !isSelected;
							}
						}
					}
				break;
				
				case HatchFillType.SOLID_DIAMOND:
					ds = this.patternSize*0;
					gp.lineStyle(0,0,0);
					
					gp.beginFill(color,opacity);
					gp.moveTo(this.patternSize/2,0);
					gp.lineTo(0,this.patternSize/2);
					gp.lineTo(this.patternSize/2,this.patternSize);
					gp.lineTo(this.patternSize,this.patternSize/2);
					gp.lineTo(this.patternSize/2,0);
				break;
				
				case HatchFillType.DASHED_FORWARD_DIAGONAL:
					gp.moveTo(0,0);
					gp.lineTo(this.patternSize/2,this.patternSize/2);
				break;
					
				case HatchFillType.DASHED_BACKWARD_DIAGONAL:
					gp.moveTo(this.patternSize/2,0);
					gp.lineTo(0,this.patternSize/2);
				break;
					
				 case HatchFillType.DASHED_HORIZONTAL:
					gp.moveTo(0,0);
					gp.lineTo(this.patternSize/2,0);
					gp.moveTo(this.patternSize/2,this.patternSize/2);
					gp.lineTo(this.patternSize,this.patternSize/2);
				break;
					
				case HatchFillType.DASHED_VERTICAL:
					gp.moveTo(0,0);
					gp.lineTo(0,this.patternSize/2);
					gp.moveTo(this.patternSize/2,this.patternSize/2);
					gp.lineTo(this.patternSize/2,this.patternSize);
				break;
					
				case HatchFillType.DIVOT:
					gp.endFill();
					var percent:Number = 0.1;
					var innerPercent:Number = 0.2;
					var padding:Number = this.patternSize*percent;
					ds = this.patternSize*(1-percent*2 - innerPercent)/2;						
					
					gp.moveTo(padding + ds,padding);
					gp.lineTo(padding,padding + ds/2);
					gp.lineTo(padding + ds,padding + ds);
					
					gp.moveTo(this.patternSize-padding -ds,this.patternSize-padding - ds);
					gp.lineTo(this.patternSize-padding,this.patternSize - padding - ds/2);
					gp.lineTo(this.patternSize-padding -ds,this.patternSize-padding);
				break;
					
				case HatchFillType.ZIG_ZAG:
					gp.endFill();
					gp.moveTo(0,0);
					gp.lineTo(this.patternSize/2,this.patternSize/2);
					gp.lineTo(this.patternSize,0);
					gp.moveTo(0,this.patternSize/2);
					gp.lineTo(this.patternSize/2,this.patternSize);
					gp.lineTo(this.patternSize,this.patternSize/2);
				break;
					
				case HatchFillType.WEAVE:
					gp.endFill();
					gp.moveTo(0,0);
					gp.lineTo(this.patternSize/2,this.patternSize/2);
					gp.lineTo(this.patternSize,0);
					gp.moveTo(0,this.patternSize/2);
					gp.lineTo(this.patternSize/2,this.patternSize);
					gp.lineTo(this.patternSize,this.patternSize/2);
					gp.moveTo(this.patternSize/2,this.patternSize/2);
					gp.lineTo(this.patternSize*3/4,this.patternSize*3/4);
					gp.moveTo(this.patternSize,this.patternSize/2);
					gp.lineTo(this.patternSize*3/4,this.patternSize/4);
					
					
				break;
				
				default:
				{
					switch(type)
					{
						case HatchFillType.PERCENT_05:
							b = drawPercent(color,opacity,5);
							break;
						case HatchFillType.PERCENT_10:
							b = drawPercent(color,opacity,10);
							break;
						case HatchFillType.PERCENT_20:
							b = drawPercent(color,opacity,20);
							break;
						case HatchFillType.PERCENT_25:
							b = drawPercent(color,opacity,25);
							break;
						case HatchFillType.PERCENT_30:
							b = drawPercent(color,opacity,30);
							break;
						case HatchFillType.PERCENT_40:
							b = drawPercent(color,opacity,40);
							break;
						case HatchFillType.PERCENT_50:
							b = drawPercent(color,opacity,50);
							break;
						case HatchFillType.PERCENT_60:
							b = drawPercent(color,opacity,60);
							break;
						case HatchFillType.PERCENT_70:
							b = drawPercent(color,opacity,70);
							break;
						case HatchFillType.PERCENT_75:
							b = drawPercent(color,opacity,75);
							break;
						case HatchFillType.PERCENT_80:
							b = drawPercent(color,opacity,80);
							break;
						case HatchFillType.PERCENT_90:
							b = drawPercent(color,opacity,90);
							break;
						break;
						
						default:
						{
							b = null;
							m = null;
							return [b,m];
						}
					}
					return [b,m];
				}
				break;
			}
			
			sp.width=this.patternSize;
			sp.height=this.patternSize;			
			gp.endFill();
			b.draw(sp);
			
			return [b,m];
		}
		
		private function drawPercent(color:uint,opacity:Number,percent:uint):BitmapData {			
			var b:BitmapData;
			var clr:uint = getARGB(color,opacity);
			var i:uint;
			var j:uint;
			
			var array:Array=null;
			
			switch (percent) {
				case 5:
					b = new BitmapData(8,8,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(4,4,clr);
					break;
				case 10:
					b = new BitmapData(8,4,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(4,2,clr);
					break;
				case 20:
					b = new BitmapData(4,4,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,2,clr);
					break;
				case 25:
					b = new BitmapData(4,2,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,1,clr);
					break;
				case 30:
					b = new BitmapData(4,4,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,0,clr);
					b.setPixel32(3,1,clr);
					b.setPixel32(0,2,clr);
					b.setPixel32(2,2,clr);
					b.setPixel32(1,3,clr);
					break;
				case 40:
					b = new BitmapData(4,8,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,0,clr);
					b.setPixel32(3,1,clr);
					b.setPixel32(0,2,clr);
					b.setPixel32(2,2,clr);
					b.setPixel32(1,3,clr);
					b.setPixel32(3,3,clr);
					b.setPixel32(0,4,clr);
					b.setPixel32(2,4,clr);
					b.setPixel32(1,5,clr);
					b.setPixel32(3,5,clr);
					b.setPixel32(0,6,clr);
					b.setPixel32(2,6,clr);
					b.setPixel32(1,7,clr);
					b.setPixel32(3,7,clr);
					break;
				case 50:
					b = new BitmapData(2,2,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(1,1,clr);
					break;
				case 60:
					b = new BitmapData(4,4,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,0,clr);
					b.setPixel32(0,1,clr);
					b.setPixel32(1,1,clr);
					b.setPixel32(3,1,clr);
					b.setPixel32(0,2,clr);
					b.setPixel32(2,2,clr);
					b.setPixel32(1,3,clr);
					b.setPixel32(2,3,clr);
					b.setPixel32(3,3,clr);
					break;
				case 70:
					b = new BitmapData(4,4,true,0);
					b.setPixel32(0,0,clr);
					b.setPixel32(2,0,clr);
					b.setPixel32(3,0,clr);
					b.setPixel32(0,1,clr);
					b.setPixel32(1,1,clr);
					b.setPixel32(2,1,clr);
					b.setPixel32(0,2,clr);
					b.setPixel32(2,2,clr);
					b.setPixel32(3,2,clr);
					b.setPixel32(0,3,clr);
					b.setPixel32(1,3,clr);
					b.setPixel32(2,3,clr);
					break;
				case 75:
					b = new BitmapData(4,4,true,0);
					for (i = 0;i<4;i++)
						for (j = 0;j<4;j++)
							b.setPixel32(i,j,clr);
					b.setPixel32(0,1,0);
					b.setPixel32(2,3,0);
					break;
				case 80:
					b = new BitmapData(8,4,true,0);
					for (i = 0;i<8;i++)
						for (j = 0;j<4;j++)
							b.setPixel32(i,j,clr);
					b.setPixel32(0,0,0);
					b.setPixel32(4,2,0);
					break;
				case 90:
					b = new BitmapData(8,8,true,0);
					for (i = 0;i<8;i++)
						for (j = 0;j<8;j++)
							b.setPixel32(i,j,clr);
					b.setPixel32(7,7,0);
					b.setPixel32(4,3,0);
					break;
			}
			
			return b;
		}
		
		private var transformBounds:Rectangle;
		private var colorTransformer:ColorTransform;
		
		// Methods.
		public function beginFill(target:Graphics, hatchType:int = 0, hatchColor:int = 0):void {
			if(!this.enabled) return;
			
			if (this.isDynamicType && hatchType == HatchFillType.NONE) return;
			if (!this.isDynamicType && this.type == HatchFillType.NONE) return;
			
			var pattern:BitmapData = this.isDynamicType ? HatchFillMap.getPattern(this.type,hatchType) : this._pattern;
			var matrix:Matrix = this.isDynamicType ? HatchFillMap.getMatrix(this.type, hatchType) : this._matrix;
			
			if (this.isDynamicHatchColor) {
				ColorUtils.toRGB(ColorParser.getInstance().getDynamicColor(this.color, hatchColor));
				this.colorTransformer.redOffset = ColorUtils.r;
				this.colorTransformer.greenOffset = ColorUtils.g;
				this.colorTransformer.blueOffset = ColorUtils.b;
				pattern.colorTransform(this.transformBounds,this.colorTransformer);
			}
			target.beginBitmapFill(pattern,matrix,true,true);
		}
		
		public function deserialize(data:XML, style:IStyle = null):void {
			
			if(data.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(data.@enabled);
			if(!this.enabled) return;
			
			if(data.@thickness!=undefined) this.thickness=SerializerBase.getNumber(data.@thickness);
			if(data.@opacity!=undefined) this.opacity=SerializerBase.getRatio(data.@opacity);	
			if(data.@pattern_size!=undefined) this.patternSize=SerializerBase.getNumber(data.@pattern_size);
			
			if(data.@color!=undefined) {
				this.color = SerializerBase.getColor(data.@color);
				if (style != null && SerializerBase.isDynamicColor(data.@color)) {
					this.isDynamicHatchColor = true;
					style.addDynamicColor(this.color);
					this.transformBounds = new Rectangle(0,0,this.patternSize, this.patternSize);
					this.colorTransformer = new ColorTransform(0,0,0);
				}else {
					this.isDynamicHatchColor = false;
				}
			} 
			
			if(data.@type!=undefined) {
				if (style != null && SerializerBase.isDynamicHatchType(data.@type)) {
					this.isDynamicType = true;
					this.type = HatchFillMap.addItem(this);
					style.addDynamicHatchType(this.type);
				}else {
					this.isDynamicType = false;
					var hatchType:int = SerializerBase.getHatchType(data.@type);
					if (hatchType != -1) this.type = hatchType;
					this.initializePattern();
				}
			}else {
				this.isDynamicType = false;
				this.type = HatchFillType.DIAGONAL_CROSS;
				this.initializePattern();
			}
		}

	}
}