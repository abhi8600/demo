package com.anychart.locale {
	import com.anychart.dateTime.DateTimeParser;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public final class DateTimeLocale implements ISerializable {
		public var monthNames:Array;
		public var shortMonthNames:Array;
		public var amString:String;
		public var shortAmString:String;
		public var pmString:String;
		public var shortPmString:String;
		public var weekDayNames:Array;
		public var shortWeekDayNames:Array;
		
		private var mask:String;
		
		private var parser:DateTimeParser;
		
		public function DateTimeLocale() {
			this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
			this.shortMonthNames = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			this.amString = "AM";
			this.pmString = "PM";
			this.shortAmString = "A";
			this.shortPmString = "P";
			this.weekDayNames = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			this.shortWeekDayNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
			this.mask = "%yyyy/%M/%d %h:%m:%s";
			this.parser = new DateTimeParser();
		}
		
		public function deserialize(data:XML):void {
			if (data.months[0] != null) {
				if (data.months[0].names[0] != null) this.monthNames = this.getArrayFromString(SerializerBase.getCDATAString(data.months[0].names[0]));
				if (data.months[0].short_names[0] != null) this.shortMonthNames = this.getArrayFromString(SerializerBase.getCDATAString(data.months[0].short_names[0]));
			}
			if (data.week_days[0] != null) {
				if (data.week_days[0].names[0] != null) this.weekDayNames = this.getArrayFromString(data.week_days[0].names[0]);
				if (data.week_days[0].short_names[0] != null) this.shortWeekDayNames = this.getArrayFromString(data.week_days[0].short_names[0]);
			}
			if (data.time[0] != null) {
				var time:XML = data.time[0];
				if (time.@am_string != undefined) this.amString = SerializerBase.getString(time.@am_string);
				if (time.@short_am_string != undefined) this.shortAmString = SerializerBase.getString(time.@short_am_string);
				if (time.@pm_string != undefined) this.pmString = SerializerBase.getString(time.@pm_string);
				if (time.@short_pm_string != undefined) this.shortPmString = SerializerBase.getString(time.@short_pm_string);
			}
			if (data.format[0] != null)
				this.mask = SerializerBase.getCDATAString(data.format[0]);
		}
		
		public function initialize():void {
			this.parser.initialize(this.mask, this);
		}
		
		public function getTimeStamp(date:String):Number {
			return this.parser.getDateAsNumber(date);
		}
		
		private function getArrayFromString(data:String):Array {
			return data.split(",");
		}
	}
}