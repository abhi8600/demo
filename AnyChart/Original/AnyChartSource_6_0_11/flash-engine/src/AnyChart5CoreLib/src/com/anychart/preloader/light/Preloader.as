package com.anychart.preloader.light {
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	public class Preloader {
		
		private var container:Sprite;
		private var bg:Shape;
		private var progress:Shape;
		private var labelContainer:Sprite;
		private var label:TextField;
		
		private var prefix:String;
		private var message:String;
		
		private var centerX:Number;
		private var centerY:Number;
		private var width:Number = 30;
		private var height:Number = 30;
		private var innerC:Number = .6;
		
		private var currentSector:int=0;
		private var numSectors:int=12;
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.container = new Sprite();
			this.bg = new Shape();
			this.progress = new Shape();
			this.labelContainer = new Sprite();
			this.container.addChild(this.bg);
			this.container.addChild(this.progress);
			this.container.addChild(this.labelContainer);
			
			this.label = new TextField();
			this.label.autoSize = TextFieldAutoSize.CENTER;
			this.label.defaultTextFormat = new TextFormat("Arial", 12);
			this.label.selectable = false;
			
			this.message = "";
			this.prefix = "";
			
			this.initPosition(bounds);
			
			return this.container;
		}
		
		private function initPosition(bounds:Rectangle):void {
			this.centerX = bounds.x + bounds.width/2;
			this.centerY = bounds.y + bounds.height/2 - 25;
		}
		
		public function resize(newBounds:Rectangle):void {
			this.initPosition(newBounds);
			this.drawLabel();
		}
		
		private var animationStep:Number = 35;
		private var animationInterval:uint;
		private var isAnimation:Boolean;
		
		public function showAnimated(message:String):void {
			this.isAnimation = true;
			if (this.animationInterval)
				clearInterval(this.animationInterval);
			this.animationInterval = setInterval(this.animate, 50);
			this.message = message;
			this.drawLabel();
		}
		
		private function animate():void {
			currentSector++;
			if(currentSector>numSectors-1) currentSector=0;
			drawCycledProgress(currentSector);
		}
		
		public function initProgress(prefix:String):void {
			this.isAnimation = false;
			if (this.animationInterval)
				clearInterval(this.animationInterval);
			this.animationInterval = setInterval(this.animate, 50);
			
			this.prefix = prefix;
			this.setProgress(0);
		}
		
		public function setProgress(value:Number):void {
			if (!this.isAnimation) {
				this.message = this.prefix + Math.floor(value*100) + "%";
				this.drawLabel();
			}
		}
		
		private function drawLabel():void {
			this.labelContainer.graphics.clear();
			if (this.label.parent == null)
				this.labelContainer.addChild(this.label);
			this.label.text = this.message;
			this.label.x = this.centerX - this.label.width/2;
			this.label.y = this.centerY + this.label.height/2 + 2;
		}
		
		private function drawSector(g:Graphics,startAngle:Number, endAngle:Number,color:uint):void {
			g.beginFill(color,1);
			DrawingUtils.drawArc(g, this.centerX, this.centerY, startAngle-90, endAngle-90, this.width/2, this.height/2, 0, true);
			DrawingUtils.drawArc(g, this.centerX, this.centerY, endAngle-90, startAngle-90, this.width/2*this.innerC, this.height/2*this.innerC, 0, false);
			g.endFill();
		}
		
		private function drawCycledProgress(sectorIndex:int):void
		{
			var g:Graphics = this.bg.graphics;
			g.clear();
			
			// Draw background.
			var step:Number=360/numSectors;
			for(var i:int=0;i<numSectors;i++)
				drawSector(g,i*step,i*step+step-4,0xE2E2E2);
			
			// Draw current sector.
			drawSector(g,sectorIndex*step,sectorIndex*step+step-4,0xC5C5C5);
			
			var prevSector:int=sectorIndex-1;
			if(prevSector==-1) prevSector=numSectors-1;
			drawSector(g,prevSector*step,prevSector*step+step-4,0xD3D3D3);
			
			var prevPrevSector:int=prevSector-1;
			if(prevPrevSector==-1) prevPrevSector=numSectors-1;
			drawSector(g,prevPrevSector*step,prevPrevSector*step+step-4,0xDDDDDD);
		}
		
		/* private function drawFixedProgress(sectorIndex:int):void
		{
			var g:Graphics = this.bg.graphics;
			g.clear();
			
			// Draw background.
			var step:Number=360/numSectors;
			for(var i:int=0;i<numSectors;i++)
			{
				if(i<sectorIndex) drawSector(g,i*step,i*step+step-4,0xAADDAA);
					else if(i==sectorIndex) drawSector(g,i*step,i*step+step-4,0x66CC66);
						else drawSector(g,i*step,i*step+step-4,0xDFDFDF);
			}
		} */
	}
}