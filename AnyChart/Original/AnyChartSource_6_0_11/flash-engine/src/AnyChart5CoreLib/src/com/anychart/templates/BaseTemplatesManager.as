package com.anychart.templates {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.utils.XMLUtils;
	
	public class BaseTemplatesManager {
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(BaseTemplatesManager);
			return true;
		}
		
		public static function clear():void {
			extraTemplates = [];
		}
		
		public static var extraTemplates:Array = [];
		
		public static function addBuildinTemplate(template:XML):void {
			extraTemplates.push(template);
		}
		
		public function getBaseNodeName():String { return null; }
		
		protected function applyDefaults(res:XML, defaults:XML):void {
			if (res == null || defaults == null) return;
			
			var defaultLegend:XML = defaults.legend[0];
			
			if (defaultLegend != null) { 
				if (res.chart_settings[0] != null) {
					if (res.chart_settings[0].legend[0] != null)
						res.chart_settings[0].legend[0] = XMLUtils.merge(defaultLegend, res.chart_settings[0].legend[0], "legend");
					if (res.chart_settings[0].controls[0] != null) {
						var legends:XMLList = res.chart_settings[0].controls[0].legend;
						var cnt:int = legends.length();
						
						for (var i:int = 0;i<cnt;i++)
							legends[i] = XMLUtils.merge(defaultLegend, legends[i], "legend");
					}
				}
			}
		}
		
		public function mergeTemplates(baseTemplate:XML, template:XML):XML {
			var res:XML = <template />;
			XMLUtils.mergeAttributes(baseTemplate, template, res);
			
			var nodeName:String = this.getBaseNodeName();
			var chartBaseTemplate:XML = baseTemplate[nodeName][0];
			var chartTemplate:XML = template[nodeName][0];
			
			res[nodeName][0] = <{nodeName} />;
			this.mergeChartNode(chartBaseTemplate, chartTemplate, res[nodeName][0]);
			
			var defaults:XML = XMLUtils.merge(baseTemplate.defaults[0], template.defaults[0]);
			if (defaults != null)
				res.defaults[0] = defaults;
			
			return res;
		}
		
		public function mergeWithChart(template:XML, chart:XML):XML {
			var nodeName:String = this.getBaseNodeName(); 
			var res:XML = <{nodeName} />;
			
			var chartTemplate:XML = template[nodeName][0];
			this.mergeChartNode(chartTemplate, chart, res);
			this.applyDefaults(res, template.defaults[0]);
			return res;
		}
		
		protected function mergeChartNode(template:XML, chart:XML, res:XML):void {
			XMLUtils.mergeAttributes(template, chart, res);
			var chartSettings:XML = XMLUtils.merge(template.chart_settings[0], chart.chart_settings[0]);
			if (chartSettings != null) 
				res.chart_settings[0] = chartSettings;
			var styles:XML = XMLUtils.mergeNamedChildren(template.styles[0], chart.styles[0]);
			if (styles != null) {
				res.styles[0] = styles;
				var childList:XMLList = res.styles[0].children();
				for each (var child:XML in childList) {
					if (child.@name != undefined)
						child.@name = String(child.@name).toLowerCase();
					if (child.@parent != undefined)
						child.@parent = String(child.@parent).toLowerCase();
				}
			}
			
			var templateControls:XML = null;
			var chartControls:XML = null;
			
			if (template.chart_settings[0] != null && template.chart_settings[0].controls[0] != null)
				templateControls = template.chart_settings[0].controls[0];
			
			if (chart.chart_settings[0] != null && chart.chart_settings[0].controls[0] != null)
				chartControls = chart.chart_settings[0].controls[0];
				 
			var controls:XML = XMLUtils.mergeNamedChildren(templateControls, chartControls);
			if (controls != null)
				res.chart_settings[0].controls[0] = controls;
		}
	}
}