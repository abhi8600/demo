package com.anychart.scales {
	import com.anychart.locale.NumberLocale;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public class BaseScale implements ISerializable {
		
		//------------------------------------------------------------------
		//
		//						PROPERTIES
		//
		//------------------------------------------------------------------
		
		public var optimalMajorSteps:Number = 7;
		
		public var fixedMinimum:Number;
		public var fixedMaximum:Number;
		
		public var minimum:Number;
		public var maximum:Number;
		public var isMinimumAuto:Boolean;
		public var isMinimumSet:Boolean;
		public var isMaximumAuto:Boolean;
		
		public var majorInterval:Number;
		public var isMajorIntervalAuto:Boolean;
		
		public var minorInterval:Number;
		public var isMinorIntervalAuto:Boolean;
		
		public var minimumOffset:Number;
		public var maximumOffset:Number;
		
		public var dataRangeMinimum:Number;
		public var dataRangeMaximum:Number;
		
		protected var _pixelRangeMinimum:Number;
		protected var _pixelRangeMaximum:Number;
		
		public function get pixelRangeMinimum():Number { return _pixelRangeMinimum; }
		public function get pixelRangeMaximum():Number { return _pixelRangeMaximum; }		
		
		public function setPixelRange(minimum:Number, maximum:Number):void {
			this._pixelRangeMinimum = minimum;
			this._pixelRangeMaximum = maximum;
		}
		
		public var baseValue:Number;
		public var isBaseValueAuto:Boolean;
		
		public var inverted:Boolean;
		
		protected var _majorIntervalsCount:int;
		public function get majorIntervalsCount():int { return this._majorIntervalsCount; }
		
		protected var _minorIntervalsCount:int;
		public function get minorIntervalsCount():int { return this._minorIntervalsCount; }
		
		protected var _minorStart:int;
		public function get minorIntervalStart():int { return this._minorStart; } 
		
		public var mode:uint;
		
		//------------------------------------------------------------------
		//
		//						CONSTRUCTOR
		//
		//------------------------------------------------------------------
		
		public function BaseScale() {
			this.setDefaults();
		}
		
		//------------------------------------------------------------------
		//
		//						METHODS
		//
		//------------------------------------------------------------------
		
		public function calculate():void {
			
			if (isNaN(this.dataRangeMinimum) && isNaN(this.dataRangeMaximum) &&
				this.isMinimumAuto && this.isMaximumAuto) {
					
				this.isMinimumAuto = false;
				this.minimum = 0;
				
				this.isMaximumAuto = false;
				this.maximum = 1;
			}
			
			if (!this.isMaximumAuto && !this.isMinimumAuto) return;
			
			var range:Number = this.dataRangeMaximum - this.dataRangeMinimum;
			
			if (this.isMinimumAuto)
				this.minimum = this.applyMinimumOffset(this.dataRangeMinimum, range);
			
			if (this.isMaximumAuto)
				this.maximum = this.applyMaximumOffset(this.dataRangeMaximum, range);
			
			if (this.maximum <= this.minimum) {
				if ( this.isMaximumAuto )
					this.maximum = this.minimum + 1.0;
				else if ( this.isMinimumAuto )
					this.minimum = this.maximum - 1.0;
			}
		}
		
		public function resize():void {
		}
		
		protected function calculateMinorStart():void {
			this._minorStart = int((this.minimum - this.baseValue)/this.minorInterval);
		}
		
		protected function calculateMajorTicksCount():void {
			this._majorIntervalsCount =  int( ( this.maximum - this.baseValue ) / this.majorInterval + 0.01 );
			if (this._majorIntervalsCount < 0) 
				this._majorIntervalsCount = 0;
		}
		
		protected function calculateTicksCount():void {
			
			this.calculateMajorTicksCount();
			
			this._minorIntervalsCount = int( ( this.majorInterval ) / this.minorInterval + 0.01 );
			if (this._minorIntervalsCount < 0)
				this._minorIntervalsCount = 0;
		}
		
		protected function calculateBaseValue():void {
			if (!this.isBaseValueAuto) return;
			this.baseValue = 1;
		}
		
		protected function applyMinimumOffset(minimum:Number, range:Number):Number {
			return (minimum < 0 || minimum - this.minimumOffset * range >= 0) ? (minimum - this.minimumOffset * range) : minimum;
		}
		
		protected function applyMaximumOffset(maximum:Number, range:Number):Number {
			return (maximum > 0 || maximum + this.maximumOffset * range <= 0) ? (maximum + this.maximumOffset * range) : maximum;
		}
				
		public function getMajorIntervalValue(index:Number):Number {
			return (this.baseValue*10 + index*this.majorInterval*10)/10;
		}
		
		public function getMinorIntervalValue(baseValue:Number, index:int):Number {
			return baseValue + index*this.minorInterval;
		}
		
		public function delinearize(value:Number):Number { return value; }
		public function linearize(value:Number):Number { return value; }
		
		public function localTransform(value:Number, isZeroAtAxisZero:Boolean = false):Number {
			var ratio:Number = (value - this.minimum)/( this.maximum - this.minimum);

			var rangeMax:Number = isZeroAtAxisZero ? (this.pixelRangeMaximum - this.pixelRangeMinimum) : pixelRangeMaximum;
			var rangeMin:Number = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum;

			var pixelDistance:Number = (this.pixelRangeMaximum - this.pixelRangeMinimum)*ratio;
			
			if (this.inverted)
				return rangeMax - pixelDistance;
				
			return rangeMin + pixelDistance;
		}
		
		public function transform(value:Number):Number {
			var ratio:Number = (value - this.minimum) / (this.maximum - this.minimum);
			
			var pixelDistance:Number = (this.pixelRangeMaximum - this.pixelRangeMinimum) * ratio; 
			
			if (this.inverted)
				return this.pixelRangeMaximum - pixelDistance;
				
			return this.pixelRangeMinimum + pixelDistance;  
		}
		
		public function fromPix(pixValue:Number):Number {
			var ratio:Number = (pixValue - this.pixelRangeMinimum)/ (this.pixelRangeMaximum - this.pixelRangeMinimum);
			
			var valueDistance:Number = (this.maximum - this.minimum) * ratio;
			
			if (this.inverted)
				return this.maximum - valueDistance;
			
			return this.minimum + valueDistance;
		}
		
		public function transformToValue(pixValue:Number):Number {
			var ratio:Number = (pixValue - this.pixelRangeMinimum) / (this.pixelRangeMaximum - this.pixelRangeMinimum);
			
			var distance:Number = (this.maximum - this.minimum) * ratio; 
			
			var val:Number = (this.inverted) ? (this.maximum - distance) : (this.minimum + distance);
			if (val > this.maximum)
				val = this.maximum;
			else if (val < this.minimum)
				val = this.minimum;
			return val;  
		}
		
		public function contains(value:Number):Boolean {
			// изменения внесены, для исправления еботы при сравнении бесконечномалых. часть 2
			var val:Number = value;
			if (val == 0) val = this.minimum;
			if (val == 0) val = this.maximum;
			var expValue:Number = Math.pow(10, (Math.max(6, Math.floor( Math.log( Math.abs(val) )/ Math.log(10) ) + 3 ) ) );
			return (value > this.minimum || Math.abs(value - this.minimum) <= Math.abs(val)/expValue) && (value < this.maximum || Math.abs(value - this.maximum) <= Math.abs(val)/expValue); 
		}
		
		public function linearizedContains(value:Number):Boolean {
			return this.contains(value);
		}
		
		protected function calculateOptimalStepSize(range:Number, steps:Number, isBounded:Boolean):Number {			
			var tempStep:Number = range / steps;

			var mag:Number = Math.floor(Math.log(tempStep)/Math.LN10 );
			var magPow:Number = Math.pow(Number(10),mag);

			var magMsd:int = isBounded ? Math.ceil(tempStep/magPow) : int(tempStep/magPow+.5);

			if (magMsd>5)
				magMsd = 10;
			else if (magMsd>2)
				magMsd = 5;
			else if (magMsd>1)
				magMsd = 2;
			
			return magMsd*magPow;	
		}
		
		protected function safeMod(value1:Number, value2:Number):Number {
			if (value1 == 0) return 0;
			var d:Number = value1/value2;
			return value2 * (d - Math.floor(d));
		}
		
		public function deserialize(data:XML):void {
			if (data == null) return;
			if (data.@mode != undefined) {
				switch (String(data.@mode).toLowerCase()) {
					default:
					case 'normal':
						this.mode = ScaleMode.NORMAL;
						break;
					case 'stacked':
						this.mode = ScaleMode.STACKED;
						break;
					case 'percentstacked':
						this.mode = ScaleMode.PERCENT_STACKED;
						break;
					case 'overlay':
						this.mode = ScaleMode.OVERLAY;
						break;
					case 'sortedoverlay':
						this.mode = ScaleMode.SORTED_OVERLAY;
						break;
				}
			}
			if (data.@minimum != undefined) {
				this.isMinimumSet = true;
				this.isMinimumAuto = true;
				if (!SerializerBase.isAuto(data.@minimum)) {
					this.minimum = this.deserializeValue(data.@minimum);
					if (!isNaN(this.minimum)) this.isMinimumAuto = false;
				}
			}else if (this.mode == ScaleMode.PERCENT_STACKED) {
				this.isMinimumAuto = false;
				this.minimum = 0;
				this.isMinimumSet = false;
			}
			
			if (data.@maximum != undefined) {
				this.isMaximumAuto = true;
				if (!SerializerBase.isAuto(data.@maximum)) {
					this.maximum = this.deserializeValue(data.@maximum);
					if (!isNaN(this.maximum)) this.isMaximumAuto = false;
				}
			}else if (this.mode == ScaleMode.PERCENT_STACKED) {
				this.isMaximumAuto = false;
				this.maximum = 100;
			}
			
			if (data.@major_interval != undefined) {
				if (SerializerBase.isAuto(data.@major_interval)) {
					this.isMajorIntervalAuto = true;
				}else {
					this.isMajorIntervalAuto = false;
					this.majorInterval = SerializerBase.getNumber(data.@major_interval);
				}
			}
			if (data.@minor_interval != undefined) {
				if (SerializerBase.isAuto(data.@minor_interval)) {
					this.isMinorIntervalAuto = true;
				}else {
					this.isMinorIntervalAuto = false;
					this.minorInterval = SerializerBase.getNumber(data.@minor_interval);
				}
			}
			if (data.@minimum_offset != undefined) this.minimumOffset = SerializerBase.getRatio(data.@minimum_offset);
			if (data.@maximum_offset != undefined) this.maximumOffset = SerializerBase.getRatio(data.@maximum_offset);
			if (data.@inverted != undefined) this.inverted = SerializerBase.getBoolean(data.@inverted);
			if (data.@base_value != undefined) {
				if (SerializerBase.isAuto(data.@base_value)) {
					this.isBaseValueAuto = true;
				}else {
					this.isBaseValueAuto = false;
					this.baseValue = this.deserializeValue(data.@base_value);
				}
			}
		}
		
		public function setDefaults():void {
			this.minimum = 0;
			this.maximum = 1;
			this.baseValue = 0;
			this.minimumOffset = .1;
			this.maximumOffset = .1;
			this.inverted = false;
			this.dataRangeMaximum = NaN;
			this.dataRangeMinimum = NaN;
			
			this.isMinimumAuto = true;
			this.isMinimumSet = false;
			this.isMaximumAuto = true;
			this.isMajorIntervalAuto = true;
			this.isMinorIntervalAuto = true;
			this.isBaseValueAuto = true;
			
			this.mode = ScaleMode.NORMAL;
		}
		
		public function deserializeValue(value:*):* {
			if (String(value) == "") return NaN;
			return NumberLocale.getValue(value);
		}
		
		protected function getOptimalSmartStepSize_(range:Number, steps:Number, isBounded:Boolean, numDecimals:Number):Number {
			var tempStep:Number = Math.max(range / steps, Math.pow(10, -numDecimals));
			var mag:Number = Math.floor(Math.log(tempStep) / Math.LN10);
			var magPow:Number = Math.pow(10, mag);
			var magMsd:Number = isBounded ? Math.ceil(tempStep / magPow) : int(tempStep / magPow + .5);
			
			if (magMsd > 5)
				magMsd = 10;
			else if (magMsd > 2)
				magMsd = 5;
			else if (magMsd > 1)
				magMsd = 2;
			return magMsd * magPow;
		};
		
		/*	AnyChart 4 BUG REPRODUCING 
		public function checkZero():void {
			if (this.isMinimumAuto)
				this.dataRangeMinimum = isNaN(this.dataRangeMinimum) ? 0 : Math.min(this.dataRangeMinimum, 0);
		} */
	}
}