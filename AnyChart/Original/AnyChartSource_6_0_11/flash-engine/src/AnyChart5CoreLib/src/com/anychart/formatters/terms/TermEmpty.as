package com.anychart.formatters.terms
{
	public class TermEmpty implements ITerm {
		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return "";
		}
	}
}