package com.anychart.visual.text {
	import com.anychart.IAnyChart;
	import com.anychart.actions.ActionsList;
	import com.anychart.formatters.IFormatable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class InteractiveTitle extends BaseTitle {
		
		protected var container:Sprite;
		public var info:TextElementInformation;
		
		private var chart:IAnyChart;
		private var formater:IFormatable;
		private var actions:ActionsList;
		
		public function InteractiveTitle(chart:IAnyChart, formater:IFormatable) {
			this.chart = chart;
			this.formater = formater;
			this.container = new Sprite();
			this.info = this.createInformation();
			super();
		}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			return this.container;
		}
		
		public function resize(newBounds:Rectangle):void {
		}
		
		public function drawTitle():void {
			this.container.graphics.clear();
			this.draw(this.container.graphics,0,0,this.info,0,0);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources, style);
			if (!this.enabled) return;
			if (data.actions[0] != null) {
				this.actions = new ActionsList(this.chart,this.formater);
				this.actions.deserialize(data.actions[0]);
				if (!this.actions.hasActions())
					this.actions = null;
				this.initListener(this.container);
			}
		}
		
		public function initListener(target:Sprite):void {
			if (!this.actions)
				return;
			target.buttonMode = true;
			target.useHandCursor = true;
			target.addEventListener(MouseEvent.CLICK, this.clickHandler);
		}
		
		private function clickHandler(event:MouseEvent):void {
			this.actions.execute();
		}
	}
}