package com.anychart.axesPlot.series.barSeries.pointsOrdering {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.visual.layout.Position;
	
	
	internal final class HorizontalPointsOrderer extends BasePointsOrderer {
		
		override protected function initialize():void {
			this.isArgumentAxisLeftToRight = true;
			
			var valueAxisAtBottom:Boolean = this.plot.getPrimaryValueAxis().position == Position.BOTTOM;
			var argumentAxisInverted:Boolean = this.plot.getPrimaryArgumentAxis().scale.inverted;
			
			if ((valueAxisAtBottom && argumentAxisInverted) || (!valueAxisAtBottom && !argumentAxisInverted)) 
				this.isArgumentAxisLeftToRight = false;
			
			super.initialize();
		}
		
		override protected function isInvertedStackInit(axis:Axis):Boolean { 
			var isArgumentAxisAtRight:Boolean = plot.getPrimaryArgumentAxis().position == Position.RIGHT;
			var isScaleInverted:Boolean = axis.scale.inverted;
			return (!isArgumentAxisAtRight && isScaleInverted) || (isArgumentAxisAtRight && !axis.scale.inverted);
		}
	}
}