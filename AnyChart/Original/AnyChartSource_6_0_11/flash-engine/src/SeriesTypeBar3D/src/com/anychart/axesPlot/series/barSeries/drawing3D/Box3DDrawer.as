package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.styles.states.BackgroundBaseStyleState3D;
	import com.anychart.visual.color.ColorParser;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	
	internal final class Box3DDrawer extends PolygonBased3DDrawer implements IBar3DDrawer {
		
		override protected function initializeSides():void {
			
			//front side
			var frontLeftTop:Point = this.leftTop.clone();
			var frontRightBottom:Point = this.rightBottom.clone();
			
			//back side
			var backLeftTop:Point = this.leftTop.clone();
			var backRightBottom:Point = this.rightBottom.clone();
			
			this.point.z.transform(frontLeftTop);
			this.point.z.transform(frontRightBottom)
			
			this.point.z.transform(backLeftTop, 0, 1);
			this.point.z.transform(backRightBottom, 0, 1);
			
			this.checkGeom(frontLeftTop, frontRightBottom);
			this.checkGeom(backLeftTop, backRightBottom);
			
			this.points[0].x = frontLeftTop.x;
			this.points[0].y = frontRightBottom.y;
			
			this.points[1].x = frontLeftTop.x;
			this.points[1].y = frontLeftTop.y;
			
			this.points[2].x = frontRightBottom.x;
			this.points[2].y = frontLeftTop.y;
			
			this.points[3].x = frontRightBottom.x;
			this.points[3].y = frontRightBottom.y;
			
			this.points[4].x = backLeftTop.x;
			this.points[4].y = backRightBottom.y;
			
			this.points[5].x = backLeftTop.x;
			this.points[5].y = backLeftTop.y;
			
			this.points[6].x = backRightBottom.x;
			this.points[6].y = backLeftTop.y;
			
			this.points[7].x = backRightBottom.x;
			this.points[7].y = backRightBottom.y;
		}
	}
}