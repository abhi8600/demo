package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.IBarCustomWidthSupport;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Bar3DDrawerBase {
		
		protected var point:AxesPlotPoint;
		
		protected var startValue:Number;
		protected var endValue:Number;
		
		public var frontBounds:Rectangle;
		public var backBounds:Rectangle;
		public var topBounds:Rectangle;
		public var bottomBounds:Rectangle;
		public var leftBounds:Rectangle;
		public var rightBounds:Rectangle;
		
		public function Bar3DDrawerBase() {
			this.topBounds = new Rectangle();
			this.bottomBounds = new Rectangle();
			this.frontBounds = new Rectangle();
			this.backBounds = new Rectangle();
			this.leftBounds = new Rectangle();
			this.rightBounds = new Rectangle();
		}
		
		public function initialize(point:AxesPlotPoint):void {
			this.point = point;
		}
		
		protected var leftTop:Point;
		protected var rightBottom:Point;
		
		public function initializePixels(startValue:Number, endValue:Number):void {
			
			this.leftTop = new Point();
			this.rightBottom = new Point();
			
			var series:AxesPlotSeries = AxesPlotSeries(point.series);
			
			var w:Number = IBarCustomWidthSupport(point.global).getBarWidth(point)/2;;
			
			series.argumentAxis.transform(this.point, this.point.x, this.leftTop,-w);
			series.valueAxis.transform(this.point, startValue, this.leftTop);
			
			series.argumentAxis.transform(this.point, this.point.x, this.rightBottom,w);
			series.valueAxis.transform(this.point, endValue, this.rightBottom);
			
			var lt:Point = this.leftTop.clone();
			var rb:Point = this.rightBottom.clone();
			
			this.point.z.transform(lt);
			this.point.z.transform(rb);
			
			this.point.bounds.x = Math.min(lt.x, rb.x);
			this.point.bounds.y = Math.min(lt.y, rb.y);
			this.point.bounds.width = Math.max(lt.x, rb.x) - point.bounds.x;
			this.point.bounds.height = Math.max(lt.y, rb.y) - point.bounds.y;
			
			this.initializeSides();
			this.initializeBounds();
		}
		
		protected function initializeSides():void {}
		protected function initializeBounds():void {}
		
		protected function isStacked ():Boolean {
			return this.point.category != null && this.point.category.isStackSettingsExists(AxesPlotSeries(this.point.series).valueAxis.name,this.point.series.type);
		}
	}
}