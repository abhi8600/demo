package com.anychart.axesPlot.series.barSeries.data3D {
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.data3D.ZPointSettings;
	import com.anychart.data.SeriesType;
	import com.anychart.scales.ScaleMode;
	
	public final class ZBarPointSettings extends ZPointSettings {
		
		override protected function setZValue(data:XML):void {
			if (!this.isClusteredScale()) {
				super.setZValue(data);
			}else {
				var axisName:String = AxesPlotSeries(this.point.series).valueAxis.name;
				this.z = this.point.category.getOverlaysCount(axisName, SeriesType.BAR);
				this.point.category.setOverlaysCount(axisName, SeriesType.BAR, this.z+1);
				
				this.zAxis.checkScaleValue(this.z);
			}
			this.zAxis.checkScaleValue(this.z+1);	
		}
		
		private function isClusteredScale():Boolean {
			return AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.OVERLAY ||
				   AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.SORTED_OVERLAY;
		}
	}
}