package com.anychart.axesPlot.series.barSeries.pointsOrdering {
	
	public final class PointsOrderingFactory {
		public static function create(plotType:String):IPointsOrderer {
			switch (plotType) {
				case "categorizedvertical":
				case "categorizedbyseriesvertical": 
					return new VerticalPointsOrderer();
				case "categorizedhorizontal":
				case "categorizedbyserieshorizontal":
					return new HorizontalPointsOrderer();
				default:
					return null;
			}
		}
	}
}