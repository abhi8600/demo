package com.anychart.axesPlot.series.barSeries.drawing3D {
	
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.BarPoint3D;
	import com.anychart.scales.ScaleMode;
	
	import flash.geom.Point;

	internal final class Pyramid3DDrawer extends PolygonBased3DDrawer implements IBar3DDrawer {
		
		public function Pyramid3DDrawer() {
			super();
		}
		
		override protected function initializeSides():void {
			var topXRadius:Number = 0;
			var bottomXRadius:Number = (this.rightBottom.x - this.leftTop.x)/2;
			var stackSumm:Number = this.endValue;
			if(this.isStacked()) {
				var stackSettings:StackSettings = this.point.category.getStackSettings(
					AxesPlotSeries(this.point.series).valueAxis.name, 
					this.point.series.type,
					AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED);
				stackSumm = stackSettings.getStackSumm(BarPoint3D(this.point).y);
				topXRadius = bottomXRadius * (stackSumm - this.endValue) / stackSumm;				
				bottomXRadius *= (stackSumm - this.startValue) / stackSumm;
			}
			this.init(topXRadius, bottomXRadius, stackSumm);
		}
		
		private function init(topXRadius:Number, bottomXRadius:Number, stackSumm:Number):void {
			var frontTopCenter:Point = new Point();
			var frontBottomCenter:Point = new Point();
			var backTopCenter:Point = new Point();
			var backBottomCenter:Point = new Point();
			
			frontTopCenter.y = this.rightBottom.y;
			frontBottomCenter.y = this.leftTop.y;
			frontBottomCenter.x = frontTopCenter.x = (this.leftTop.x + this.rightBottom.x)/2;
			
			backTopCenter.y = this.rightBottom.y;
			backBottomCenter.y = this.leftTop.y;
			backBottomCenter.x = backTopCenter.x = (this.leftTop.x + this.rightBottom.x)/2;
			
			var topOffset:Number = this.endValue/stackSumm;
			var bottomOffset:Number = this.startValue/stackSumm; 
			
			this.point.z.transform(frontTopCenter,0,topOffset/2);
			this.point.z.transform(frontBottomCenter,0,bottomOffset/2); 
			this.point.z.transform(backTopCenter,0,1-topOffset/2);
			this.point.z.transform(backBottomCenter,0,1-bottomOffset/2);  
			
			this.points[0].x = frontBottomCenter.x - bottomXRadius;
			this.points[0].y = frontBottomCenter.y;
			
			this.points[1].x = frontTopCenter.x - topXRadius;
			this.points[1].y = frontTopCenter.y;
			
			this.points[2].x = frontTopCenter.x + topXRadius;
			this.points[2].y = frontTopCenter.y;
			
			this.points[3].x = frontBottomCenter.x + bottomXRadius;
			this.points[3].y = frontBottomCenter.y;
			
			this.points[4].x = backBottomCenter.x - bottomXRadius;
			this.points[4].y = backBottomCenter.y;
			
			this.points[5].x = backTopCenter.x - topXRadius;
			this.points[5].y = backTopCenter.y;
			
			this.points[6].x = backTopCenter.x + topXRadius;
			this.points[6].y = backTopCenter.y;
			
			this.points[7].x = backBottomCenter.x + bottomXRadius;
			this.points[7].y = backBottomCenter.y;
		}
		
		override public function initializePixels(startValue:Number, endValue:Number):void {
			this.startValue = startValue;
			this.endValue = endValue;
			super.initializePixels(startValue,endValue);
		}
	}
}