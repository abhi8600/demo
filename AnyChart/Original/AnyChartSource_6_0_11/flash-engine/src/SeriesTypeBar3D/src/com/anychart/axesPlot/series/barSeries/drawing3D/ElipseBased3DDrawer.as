package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class ElipseBased3DDrawer extends Bar3DDrawerBase {
		protected var topXRadius:Number;
		protected var topYRadius:Number;
		protected var topCenter:Point;
		
		protected var bottomXRadius:Number;
		protected var bottomYRadius:Number;
		protected var bottomCenter:Point;
		
		public function ElipseBased3DDrawer() {
			super();
			this.topCenter = new Point();
			this.bottomCenter = new Point();
		}
		
		override protected function initializeSides():void {
			//front side
			var frontLeftTop:Point = this.leftTop.clone();
			var frontRightBottom:Point = this.rightBottom.clone();
			
			//back side
			var backLeftTop:Point = this.leftTop.clone();
			var backRightBottom:Point = this.rightBottom.clone();
			
			this.point.z.transform(frontLeftTop);
			this.point.z.transform(frontRightBottom);
			
			this.point.z.transform(backLeftTop, 0, 1);
			this.point.z.transform(backRightBottom, 0, 1);
			
			/* this.checkGeom(frontLeftTop, frontRightBottom);
			this.checkGeom(backLeftTop, backRightBottom);  */
			
			this.topCenter.x = (frontLeftTop.x + backRightBottom.x)/2;
			this.bottomCenter.x = this.topCenter.x;
			
			this.topCenter.y = (frontRightBottom.y + backRightBottom.y)/2;
			this.bottomCenter.y = (frontLeftTop.y + backLeftTop.y)/2;
			
			this.topXRadius = this.bottomXRadius = Math.abs(backRightBottom.x - backLeftTop.x)/2;
			this.topYRadius = this.bottomYRadius = Math.abs(backLeftTop.y - frontLeftTop.y)/2;
			
			this.topBounds.x = this.topCenter.x - this.topXRadius;
			this.topBounds.width = this.topXRadius*2;
			this.topBounds.y = this.topCenter.y - this.topYRadius;
			this.topBounds.height = this.topYRadius*2;
			
			this.bottomBounds.x = this.bottomCenter.x - this.bottomXRadius;
			this.bottomBounds.width = this.bottomXRadius*2;
			this.bottomBounds.y = this.bottomCenter.y - this.bottomYRadius;
			this.bottomBounds.height = this.bottomYRadius*2;
			
			this.frontBounds.x = this.topBounds.x;
			this.frontBounds.y = this.topBounds.y;
			this.frontBounds.width = this.topBounds.width;
			this.frontBounds.height = this.bottomBounds.bottom - this.frontBounds.y;
			
			this.backBounds.x = this.topBounds.x;
			this.backBounds.y = this.topBounds.y;
			this.backBounds.width = this.topBounds.width;
			this.backBounds.height = this.bottomBounds.top - this.backBounds.y;
			
			this.leftBounds.x = Math.min(frontLeftTop.x,backLeftTop.x); 
			this.leftBounds.y = Math.min(frontLeftTop.y,backLeftTop.y); 
			this.leftBounds.width = Math.abs(backRightBottom.x-frontRightBottom.x);
			this.leftBounds.height = Math.max(Math.abs(backRightBottom.y-frontLeftTop.y),Math.abs(frontRightBottom.y - backLeftTop.y));
			
			this.rightBounds.x = Math.min(backRightBottom.x,frontRightBottom.x); 
			this.rightBounds.y = Math.min(frontLeftTop.y,backLeftTop.y); 
			this.rightBounds.width = Math.abs(backRightBottom.x-frontRightBottom.x);
			this.rightBounds.height = Math.max(Math.abs(backRightBottom.y-frontLeftTop.y),Math.abs(frontRightBottom.y - backLeftTop.y));
		}
		
		protected function checkGeom(leftTop:Point, rightBottom:Point):void {
			var tmp:Number;
			if (leftTop.x > rightBottom.x) {
				tmp = leftTop.x;
				leftTop.x = rightBottom.x;
				rightBottom.x = tmp;
			}
			 if (leftTop.y > rightBottom.y) {
				tmp = leftTop.y;
				leftTop.y = rightBottom.y;
				rightBottom.y = tmp;
			} 
		} 
		
		public function drawTopSide(g:Graphics):void {
			this.drawEllipseSide(g,  this.topCenter, this.topXRadius, this.topYRadius, this.topBounds);
		}
		
		public function drawBottomSide(g:Graphics):void {
			this.drawEllipseSide(g, this.bottomCenter, this.bottomXRadius, this.bottomYRadius, this.bottomBounds);
		}
		
		public function drawLeftSide(g:Graphics):void	{}
		public function drawRightSide(g:Graphics):void {}
		
		public function drawFrontSide(g:Graphics):void {
			var startAngle:Number =I3DObject(this.point).isTopSideVisible ? 0:180; 
			this.drawSide(g, startAngle, startAngle+180, this.frontBounds);
		}
		
		public function drawBackSide(g:Graphics):void {
			var startAngle:Number =I3DObject(this.point).isTopSideVisible ? 180:0;
			this.drawSide(g, startAngle, startAngle+180, this.backBounds);
		}
		
		protected function drawSide(g:Graphics, startAngle:Number, endAngle:Number, bounds:Rectangle):void {
			this.execDrawVerticalSide(g, startAngle, endAngle);	
		} 
		
		protected function drawEllipseSide(g:Graphics, 
										 center:Point, xRadius:Number, yRadius:Number,
										 bounds:Rectangle):void {
			this.execDrawEllipseSide(g, center, xRadius, yRadius);	
		}
		
		private function execDrawEllipseSide(g:Graphics, center:Point, xRadius:Number, yRadius:Number):void {
			g.drawEllipse(center.x - xRadius, center.y - yRadius, xRadius*2, yRadius*2); 
		}
		
		private function execDrawVerticalSide(g:Graphics, startAngle:Number, endAngle:Number):void {
			DrawingUtils.drawArc(g, this.bottomCenter.x, this.bottomCenter.y, startAngle, endAngle, this.bottomYRadius, this.bottomXRadius, 0, true);
			DrawingUtils.drawArc(g, this.topCenter.x, this.topCenter.y, endAngle, startAngle, this.topYRadius, this.topXRadius, 0, false);
		}
	}
}