package com.anychart.funnelPlot{
	
	public final class FunnelMode{
		
		public static const SQUARE:uint = 0;
		public static const CIRCULAR:uint = 1;
	}
}