package com.anychart.funnelPlot{
	
	import com.anychart.seriesPlot.data.BasePoint;
	
	
	/**
	 * Класс описывающий двумерную серию типа Funnel
	 */
	public class Funnel2DSeries extends FunnelSeries{
		
		/**
		 * Обрабатываем точки перед рисованием
		 */
		override public function onBeforeDraw():void{
			this.currentY = 1;
			this.setRealSummHeight();
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			for (var i:uint = 0; i<this.points.length ; i++){
				var point:Funnel2DPoint = this.points[i];
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
			}
			super.onBeforeDraw();
		}
		
		/**
		 * Создаем точку
		 */
		override public function createPoint():BasePoint{
			return new Funnel2DPoint();
		}
		
	}
}