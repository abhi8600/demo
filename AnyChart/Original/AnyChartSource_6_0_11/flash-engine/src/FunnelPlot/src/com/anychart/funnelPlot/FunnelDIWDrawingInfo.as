package com.anychart.funnelPlot{
	
	import com.anychart.percentBase.PercentBaseGlobalSeriesSettings;
	import com.anychart.percentBase.PercentBasePoint;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.visual.layout.Anchor;
	
	import flash.geom.Point;

	public class FunnelDIWDrawingInfo extends FunnelDrawingInfo{
		
		override public function setSectorPoint(point:PercentBasePoint, inverted:Boolean, w:Number):void{
			var h:Number = PercentBaseSeries(point.series).getHeight();
			var x:Number = PercentBaseSeries(point.series).seriesBound.x;
			var y:Number = PercentBaseSeries(point.series).seriesBound.y;
			
			this.bottomSize3D = point.bottomWidth*PercentBaseGlobalSeriesSettings(point.global).size3D*h;
			this.topSize3D = point.topWidth*PercentBaseGlobalSeriesSettings(point.global).size3D*h;
			
			if (!Funnel2DDataIsWidthPoint(point).is3D){
				this.bottomSize3D = 0;
				this.topSize3D = 0;
			}
			
			this.centerBottomFront.x = this.centerBottomBack.x = this.centerTopBack.x = this.centerTopFront.x = x+w/2;
			this.centerBottomFront.y = inverted ? (y+h-point.bottomHeight*h) : (y + point.bottomHeight*h);
			this.centerTopFront.y = inverted ? (y + h - point.topHeight*h) : (y + point.topHeight*h);
			this.centerBottomBack.y = this.centerBottomFront.y - this.bottomSize3D*2;
			this.centerTopBack.y = this.centerTopFront.y - this.topSize3D*2;
			
			
			this.leftBottom.x = x+w/2-FunnelPoint(point).bottomWidth*w/2;
			this.rightBottom.x = x + w/2 + FunnelPoint(point).bottomWidth*w/2;
			this.leftTop.x = x + w/2 - FunnelPoint(point).topWidth*w/2;
			this.rightTop.x = x+w/2 + FunnelPoint(point).topWidth*w/2;
			this.leftBottom.y = this.rightBottom.y =this.centerBottomFront.y - this.bottomSize3D;
			this.leftTop.y = this.rightTop.y =this.centerTopFront.y - this.topSize3D;
		} 
		
		override public function setAnchorPoint(anchor:uint, pos:Point, inverted:Boolean):void{
			switch (anchor){
				case Anchor.CENTER:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.leftBottom.y + this.leftTop.y)/2;
					break;
				}
				case Anchor.CENTER_LEFT:{
					pos.x = (this.leftTop.x + this.leftBottom.x)/2;
					pos.y = (this.leftTop.y + this.leftBottom.y)/2;
					break;
				}
				case Anchor.CENTER_RIGHT:{
					pos.x = (this.rightTop.x + this.rightBottom.x)/2;
					pos.y = (this.rightTop.y + this.rightBottom.y)/2;
					break;
				}
				case Anchor.CENTER_BOTTOM:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.rightBottom.y);
					break;
				}
				case Anchor.CENTER_TOP:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.rightTop.y);
					break;
				}
				case Anchor.LEFT_TOP:{
					pos.x = (this.leftTop.x);
					pos.y = (this.leftTop.y);
					break;
				}
				case Anchor.RIGHT_TOP:{
					pos.x = (this.rightTop.x);
					pos.y = (this.rightTop.y);
					break;
				}
				case Anchor.LEFT_BOTTOM:{
					pos.x = (this.leftBottom.x);
					pos.y = (this.leftBottom.y);
					break;
				}
				case Anchor.RIGHT_BOTTOM:{
					pos.x = (this.rightBottom.x);
					pos.y = (this.rightBottom.y);
					break;
				}
			}
		}
	}
}