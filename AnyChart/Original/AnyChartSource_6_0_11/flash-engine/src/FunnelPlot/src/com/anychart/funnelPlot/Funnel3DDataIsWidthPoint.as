package com.anychart.funnelPlot{
	import com.anychart.percentBase.PercentBaseGlobalSeriesSettings;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	
	
	public class Funnel3DDataIsWidthPoint extends Funnel2DDataIsWidthPoint{
		
		public function Funnel3DDataIsWidthPoint():void {
			super();
			this.is3D = true;
		}
		
		override protected function drawSector():void{
			this.setBounds();
			this.drawShape();
			this.drawConnectors();	
		}
		
		override protected function drawShape():void {
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			var mode:uint = FunnelGlobalSeriesSettings(this.global).mode;
			var nextPoint:Funnel3DDataIsWidthPoint = PercentBaseSeries(this.series).points[this.index+1];
			if (inverted){
				if (mode == FunnelMode.SQUARE){
					this.drawSquareTopSide();
					this.drawSquareFrontSide();
					this.drawSquareBottomSide();
				}else{
					this.drawTopSide();
					this.drawFrontSide();
					this.drawBottomSide();					
				}
			}else{
				if (mode == FunnelMode.SQUARE){
					this.drawSquareBottomSide();
					this.drawSquareFrontSide();
					this.drawSquareTopSide();
				}else{
					this.drawBottomSide();
					this.drawFrontSide();
					this.drawTopSide();
				}
			}
		}
		
		private function drawTopSide():void {
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			g.drawEllipse(drawInfo.leftTop.x,drawInfo.leftTop.y-drawInfo.topSize3D,drawInfo.rightTop.x-drawInfo.leftTop.x,drawInfo.topSize3D*2);
			g.endFill();
			g.lineStyle();
		}
		
		private function drawBottomSide():void {
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			g.drawEllipse(drawInfo.leftBottom.x,drawInfo.leftBottom.y-drawInfo.bottomSize3D,drawInfo.rightBottom.x-drawInfo.leftBottom.x,drawInfo.bottomSize3D*2);
			g.endFill();
			g.lineStyle();
		}
		
		private function drawFrontSide():void {
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			DrawingUtils.drawArc(g,(drawInfo.rightBottom.x + drawInfo.leftBottom.x)/2,drawInfo.leftBottom.y,0,180,drawInfo.bottomSize3D,(drawInfo.rightBottom.x - drawInfo.leftBottom.x)/2,0,true);
			DrawingUtils.drawArc(g,(drawInfo.rightTop.x + drawInfo.leftTop.x)/2,drawInfo.leftTop.y,180,0,drawInfo.topSize3D,(drawInfo.rightTop.x - drawInfo.leftTop.x)/2,0,false);
			g.endFill();
			g.lineStyle();
			
			if (state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				DrawingUtils.drawArc(g,(drawInfo.rightBottom.x + drawInfo.leftBottom.x)/2,drawInfo.leftBottom.y,0,180,drawInfo.bottomSize3D,(drawInfo.rightBottom.x - drawInfo.leftBottom.x)/2,0,true);
				DrawingUtils.drawArc(g,(drawInfo.rightTop.x + drawInfo.leftTop.x)/2,drawInfo.leftTop.y,180,0,drawInfo.topSize3D,(drawInfo.rightTop.x - drawInfo.leftTop.x)/2,0,false);
				g.endFill();
			}
		}
		
		private function drawBackSide():void {
			
		}
		private function drawSquareFrontSide():void {
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
			g.lineTo(drawInfo.centerBottomFront.x,drawInfo.centerBottomFront.y);
			g.lineTo(drawInfo.rightBottom.x,drawInfo.rightBottom.y);
			g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
			g.lineTo(drawInfo.centerTopFront.x,drawInfo.centerTopFront.y);
			g.lineTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
			
			g.endFill();
			g.lineStyle();
			
			if (state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				
				g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
				g.lineTo(drawInfo.centerBottomFront.x,drawInfo.centerBottomFront.y);
				g.lineTo(drawInfo.rightBottom.x,drawInfo.rightBottom.y);
				g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
				g.lineTo(drawInfo.centerTopFront.x,drawInfo.centerTopFront.y);
				g.lineTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
				
				g.endFill();
			}
		}
		
		private function drawSquareTopSide():void {
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
				g.moveTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
				g.lineTo(drawInfo.centerTopFront.x,drawInfo.centerTopFront.y);
				g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
				g.lineTo(drawInfo.centerTopBack.x,drawInfo.centerTopBack.y);
				
				g.endFill();
				g.lineStyle();
		}
		
		private function drawSquareBottomSide():void{
			var drawInfo:FunnelDIWDrawingInfo = FunnelDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
			g.lineTo(drawInfo.centerBottomFront.x,drawInfo.centerBottomFront.y);
			g.lineTo(drawInfo.rightBottom.x,drawInfo.rightBottom.y);
			g.lineTo(drawInfo.centerBottomBack.x,drawInfo.centerBottomBack.y);
			
			g.endFill();
			g.lineStyle();
		}
	}
}