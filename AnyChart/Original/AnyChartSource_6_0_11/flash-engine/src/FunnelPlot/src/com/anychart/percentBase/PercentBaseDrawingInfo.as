﻿package com.anychart.percentBase{
	import com.anychart.visual.layout.Anchor;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывающий точки, по кторым рисуется сектор
	 */
	public class PercentBaseDrawingInfo {
		
		/**
		 * прямоугольник в котором рисуется весь плот, равный по ширине максимально широкому сектору 
		 * необходимо для сдвига и ресайза плота в зависимости от ширины лейблов, все точки привязываются и назначаются
		 * через него 
		 */
		protected var bound:Rectangle;
		
		/**
		 * нижняя левая точка 
		 */
		public var leftBottom:Point;
		
		/**
		 * верхняя левая точка
		 */
		public var leftTop:Point;
		
		/**
		 * нижняя правая точка 
		 */
		public var rightBottom:Point;
		
		/**
		 * верхняя правая точка
		 */
		public var rightTop:Point;
		
		public var topSize3D:Number;
		public var bottomSize3D:Number;
		
		/**
		 * Конструктор создает точки и прямоугольник для рисования
		 */
		public function PercentBaseDrawingInfo(){
			this.leftBottom = new Point;
			this.leftTop = new Point;
			this.rightBottom = new Point;
			this.rightTop = new Point;
			this.bound = new Rectangle;
		}
		
		/**
		 * Выставляем Прямоугольник рисования
		 * 
		 * @param plotBound - прямоугольник в котором рисуется Plot
		 * @param maxWidth - максимальная ширина Графика
		 */
		public function setDrawingBound(point: PercentBasePoint, maxWidth:Number):void{
			this.bound.x = 0;
			this.bound.y = 0;
			this.bound.height = PercentBaseSeries(point.series).getHeight();
			this.bound.width = PercentBaseSeries(point.series).getWidht();
		}
		
		
		 
		/**
		 *Выставляем точки для рисования
		 * 
		 * @param point - точка графика 
		 * @param inverted - инвертирован график или нет  
		 * @param w - ширина в которой рисуется график
		 */
		public function setSectorPoint (point: PercentBasePoint, inverted: Boolean,w:Number): void{
			var h:Number = PercentBaseSeries(point.series).getHeight();
			var x:Number = PercentBaseSeries(point.series).seriesBound.x;
			var y:Number = PercentBaseSeries(point.series).seriesBound.y;
			
			this.leftTop.x = inverted ? (x + w/2-point.bottomWidth/2*w) : (x + w/2 - point.topWidth/2*w);
			this.rightTop.x = inverted ? (x + w/2+point.bottomWidth/2*w) : (x + w/2 + point.topWidth/2*w) ;
			this.leftBottom.x = inverted ? (x + w/2 - point.topWidth/2*w) : (x + w/2 - point.bottomWidth/2*w);
			this.rightBottom.x = inverted ? (x + w/2 + point.topWidth/2*w) : (x + w/2 + point.bottomWidth/2*w);
			this.leftTop.y =  this.rightTop.y = inverted ? (y+h - point.bottomHeight*h) : (y+point.topHeight*h);
			this.leftBottom.y = this.rightBottom.y = inverted ? (y+h - point.topHeight*h) : (y+point.bottomHeight*h);
		}
		
		/**
		 * Выставляем точки рисования заново если один из лейблов не входит
		 * 
		 * @pram x - смещение графика
		 * @param point - точка, которая будет сдвинута
		 */
		public function updateSectorPoint (x: Number): void{
			this.leftBottom.x-=x;
			this.leftTop.x-=x;
			this.rightBottom.x-=x;
			this.rightTop.x-=x;	
		}
		
		/**
		 * Выставляем Якорь рисования
		 * 
		 * @param anchor  - тип якоря
		 * @param pos - точка позиционирования якаря
		 * @param inverted - инвертирован график или нет
		 */
		public function setAnchorPoint(anchor:uint, pos:Point, inverted:Boolean):void{
			switch (anchor){
				case Anchor.CENTER:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.leftBottom.y + this.leftTop.y)/2;
					break;
				}
				case Anchor.CENTER_LEFT:{
					pos.x = (this.leftTop.x + this.leftBottom.x)/2;
					pos.y = (this.leftTop.y + this.leftBottom.y)/2;
					break;
				}
				case Anchor.CENTER_RIGHT:{
					pos.x = (this.rightTop.x + this.rightBottom.x)/2;
					pos.y = (this.rightTop.y + this.rightBottom.y)/2;
					break;
				}
				case Anchor.CENTER_BOTTOM:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.rightBottom.y);
					break;
				}
				case Anchor.CENTER_TOP:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.rightTop.y);
					break;
				}
				case Anchor.LEFT_TOP:{
					pos.x = (this.leftTop.x);
					pos.y = (this.leftTop.y);
					break;
				}
				case Anchor.RIGHT_TOP:{
					pos.x = (this.rightTop.x);
					pos.y = (this.rightTop.y);
					break;
				}
				case Anchor.LEFT_BOTTOM:{
					pos.x = (this.leftBottom.x);
					pos.y = (this.leftBottom.y);
					break;
				}
				case Anchor.RIGHT_BOTTOM:{
					pos.x = (this.rightBottom.x);
					pos.y = (this.rightBottom.y);
					break;
				}
			}
		}
			
	}
}