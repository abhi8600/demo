package com.anychart.funnelPlot{
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	
	
	/**
	 * Класс описывает двумерный Funnel
	 */
	public class Funnel2DPoint extends FunnelPoint{
		
		public function Funnel2DPoint():void{
			this.is3D = false;
			super();
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#drawSector
		 */
		override protected function drawSector():void{
			this.setBounds();
			this.getFill();
			this.drawShape();
			this.getEndFill();
			if (BackgroundBaseStyleState(this.styleState).hatchFill != null) {
				BackgroundBaseStyleState(this.styleState).hatchFill.beginFill(this.container.graphics, this.hatchType, this.color);
				this.drawShape();
				this.container.graphics.endFill();
			 }
			this.drawConnectors();
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#drawShape
		 */
		override protected function drawShape():void{
			var g:Graphics = this.container.graphics;
			g.moveTo(FunnelDrawingInfo(this.drawingInfo).leftTop.x,FunnelDrawingInfo(this.drawingInfo).leftTop.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).leftCenter.x,FunnelDrawingInfo(this.drawingInfo).leftCenter.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).leftBottom.x,FunnelDrawingInfo(this.drawingInfo).leftBottom.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).rightBottom.x,FunnelDrawingInfo(this.drawingInfo).rightBottom.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).rightCenter.x,FunnelDrawingInfo(this.drawingInfo).rightCenter.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).rightTop.x,FunnelDrawingInfo(this.drawingInfo).rightTop.y);
			g.lineTo(FunnelDrawingInfo(this.drawingInfo).leftTop.x,FunnelDrawingInfo(this.drawingInfo).leftTop.y);
		}
		/**
		 *@see com.anychart.percentBase.PercentBasePoint#getRealPercentHeight 
		 */
		override protected function getRealPercentHeight(padding:Number, size3D:Number):Number{
			 return (1 - padding*(PercentBaseSeries(this.series).visiblePoints-1));
		}
	}
}