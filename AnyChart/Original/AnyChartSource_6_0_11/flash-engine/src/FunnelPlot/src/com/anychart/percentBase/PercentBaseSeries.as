package com.anychart.percentBase{
	
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSingleValueSeries;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Базовый класс для Funnel,Cone,Pyramid серий
	 */
	public class PercentBaseSeries extends BaseSingleValueSeries{
		
		/**
		 * Переменная хранящая баунд серии в котором она рисуется
		 */
		public var seriesBound:Rectangle; 
		 
		/**
		 * Переменная хранит текушее значение высоты верхней или нижней границы сектора
		 */
		public var currentY:Number = 1;
		
		/**
		 * сумма значений с уччетом значений меньше минимального
		 */
		protected var realSummHeight:Number;
		
		/**
		 * Колличество видимых точек, необходимо для того чтобы в режиме
		 * dataIsWidht не отображалась игнорируемая последняя точка 
		 */
		public var visiblePoints:Number;
		 
		 /**
		 * Переменная хранящее значение на которое необходимо проресайзить и сдвинуть график
		 */
		public var shiftNumber:Number = 0;
		
		/**
		 * @see com.anychart.seriesPlot.data.BaseSeries
		 */
		public function PercentBaseSeries():void{
			super();
			this.needCallingOnBeforeDraw = true;
		}
		
		/**
		 * Возвращает высоту плота
		 */
		public function getHeight():Number{
			return this.seriesBound.height;
		}
		
		/**
		 * Возвращает ширину плота
		 */
		public function getWidht():Number{
			return this.seriesBound.width;
		}
		
		/**
		 * Выставляет реальную сумму значений с учетом значений меньше минимального
		 * и игнорируемой последней точки в режиме dataIsWidth
		 */
		protected function setRealSummHeight():void{
			var point: PercentBasePoint;
			var dataIsWidth:Boolean = PercentBaseGlobalSeriesSettings(this.global).dataIsWidth;
			var minValue:Number =  PercentBaseGlobalSeriesSettings(this.global).minPointSize*this.cashedTokens['YSum']
			this.realSummHeight = 0;
			this.visiblePoints = 0;
			for (var i:Number = 0; i< this.points.length; i++){
				point = this.points[i];
				this.visiblePoints++;
				if (i == this.points.length - 1 && dataIsWidth){
					//point.isMissing = true;
					this.visiblePoints--;
				}else {
					if (point.calculationValue<minValue) realSummHeight+=minValue;
					else this.realSummHeight+=point.calculationValue;
				}
			}
		}
		
		/**
		 * обработка точек перед рисованием
		 */
		override public function onBeforeDraw():void{
			this.setLabelsPosition();
			this.shiftPlotPoints(); 
		}
		
		/**
		 * Вызывается при ресайзе окна
		 */
		override public function onBeforeResize():void{
			super.onBeforeResize();
			this.onBeforeDraw();
		}
		
		/**
		 * Получаем значения лейблов и выставляем значение shiftNumber
		 * более подробно об этом можно почитать в wiki.
		 */
		public function setLabelsPosition():void{
			var labelPos:Point = new Point();
			var labelRect:Rectangle = new Rectangle();
			var w:Number = this.getWidht();
			var h:Number = this.getHeight();
			var tangens:Number = w/(h*2);//Для тех кто не знает чему равен тангенс - tg(a)=отношеню противолежащего катета(w/2) к прилежащему(h)
			var labelH:Number;
			var lenght:Number = this.points.length;
			var point:PercentBasePoint;
			var pos:Number;
			var labelPadding:Number;
			for (var i:int = 0; i<lenght; i++){
				point = this.points[i];
				if (point.label.enabled == true && !point.isMissing){
					pos = (PercentBaseDrawingInfo(point.drawingInfo).rightBottom.x + PercentBaseDrawingInfo(point.drawingInfo).rightTop.x)/2;
					labelRect = point.label.getBounds(point,point.label.style.normal,BasePoint.LABEL_INDEX);
					labelPos = point.getLabelPosition(BaseElementStyleState(point.label.style.normal),null,labelRect);
					labelPadding = LabelElementStyleState(point.label.style.normal).padding;
					labelH = (point.bottomHeight+point.topHeight)*h/2;
					if (labelPos.x-labelRect.width < this.seriesBound.x || labelPos.x+labelRect.width > this.getWidht()|| pos+labelRect.width  > this.getWidht()){
						tangens = Math.min(tangens,(w-labelRect.width-labelPadding)/(h+labelH));
					}
				}
			}
			this.shiftNumber = 2*h*tangens;
		} 
		
		/**
		 * Ресайзим и если необходимо сдвигаем точки 
		 */
		public function shiftPlotPoints():void{
			var labelPlasement:uint = PercentBaseGlobalSeriesSettings(this.global).labelsPlacementMode;
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			for (var i:int = 0; i<this.points.length; i++){
				var point:PercentBasePoint = this.points[i];
				switch (labelPlasement){
					case LabelsPlacementMode.OUTSIDE_LEFT:
						point.drawingInfo.setSectorPoint(point, inverted, this.shiftNumber);
						point.drawingInfo.updateSectorPoint((this.shiftNumber - this.getWidht())/2);
						break;
					case LabelsPlacementMode.OUTSIDE_RIGHT:
						point.drawingInfo.setSectorPoint(point, inverted, this.shiftNumber);
						point.drawingInfo.updateSectorPoint((this.getWidht() - this.shiftNumber )/2);
						break;
					case LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN: 
						point.drawingInfo.setSectorPoint(point, inverted, this.shiftNumber);
						point.drawingInfo.updateSectorPoint((this.shiftNumber - this.getWidht())/2);
						break; 
					case LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN:
						point.drawingInfo.setSectorPoint(point, inverted, this.shiftNumber);
						point.drawingInfo.updateSectorPoint((this.getWidht() - this.shiftNumber )/2);
						break;
				}
			}
		}
	}
}