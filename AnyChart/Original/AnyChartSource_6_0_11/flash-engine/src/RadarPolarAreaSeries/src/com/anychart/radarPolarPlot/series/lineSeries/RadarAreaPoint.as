package com.anychart.radarPolarPlot.series.lineSeries
{
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	internal class RadarAreaPoint extends RadarLinePoint
	{
		
		
		public function RadarAreaPoint()
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
		}
		
		override protected function createDrawingPoints():void {
			//if (!this.needCreateDrawingPoints) return;
			
			var bottomValue:Number = AreaSeries(series).valueAxisStart;
			this.drawingPoints = [];
			var plot:RadarPolarPlot = RadarPolarPlot(this.plot);
			var k:Number=0;
			if (this.index == 0) k=this.series.points.length;
			else k=this.index;
			if (this.hasPrev) {
				this.series.points[k-1].initializePixValues();
				plot.transform(this.series.points[k-1].x, bottomValue, this.bottomStart)
				this.drawingPoints.push(this.getPointBetween(this.series.points[k-1], this));
			}
			else
				plot.transform(this.x, bottomValue, this.bottomStart)
			
			this.drawingPoints.push(this.pixValuePt);
			
			if (this.index == this.series.points.length-1) k = -1;
			else k=this.index;
			if (this.hasNext) {
				this.series.points[k+1].initializePixValues();
				plot.transform(this.series.points[k+1].x, bottomValue, this.bottomEnd);
				this.drawingPoints.push(this.getPointBetween(this, this.series.points[k+1]));
			}
			else
				plot.transform(this.x, bottomValue, this.bottomEnd);
			
			//this.needCreateDrawingPoints = false;
		}
		
		override protected function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}
		
		override final protected function drawSegment(g:Graphics):void {
			
			var i:uint;
			var s:AreaStyleState = AreaStyleState(this.styleState);
			
			var gradientBounds:Rectangle = new Rectangle;
			if (AreaSeries(this.series).needCreateGradientRect)
				gradientBounds = AreaSeries(this.series).gradientBounds;
			
			if (s.stroke != null)
				s.stroke.apply(g, gradientBounds, this.color);
			if (s.fill != null)
				s.fill.begin(g, gradientBounds, this.color);
			g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
			for (i = 1;i<this.drawingPoints.length;i++) 
				g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
			g.lineStyle();
			
			if (this.previousStackPoint == null) {
				g.lineTo(this.bottomEnd.x, this.bottomEnd.y);
				g.lineTo(this.bottomStart.x, this.bottomStart.y);	
			}else {
				RadarAreaPoint(this.previousStackPoint).drawSegmentBackward(g);
			}
			g.endFill();
			
			if (s.hatchFill != null) {
				s.hatchFill.beginFill(g, this.hatchType, this.color);
				g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
				for (i = 1;i<this.drawingPoints.length;i++) 
					g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
				if (this.previousStackPoint == null) {
					/*if (this.series.index > 0 && RadarPolarPlot(this.plot).getYScale().mode == ScaleMode.STACKED)
					{
					// Рисование пустых областей тут
					}else{*/
					g.lineTo(this.bottomEnd.x, this.bottomEnd.y);
					g.lineTo(this.bottomStart.x, this.bottomStart.y);	
					/*}*/
				}else {
					RadarAreaPoint(this.previousStackPoint).drawSegmentBackward(g);
				}
				g.endFill();
			}
		}
		
		private function drawSegmentBackward(g:Graphics):void {
			if (this.drawingPoints != null)
				for (var i:int = this.drawingPoints.length-1;i>=0;i--)
					g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
		}
	}
}