package com.anychart.axesPlot.data {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.scales.ScaleMode;
	
	public class StackablePoint extends SingleValuePoint {
		
		public var previousStackPoint:StackablePoint;
		
		public var stackValue:Number;
		
		override protected function deserializeValue(data:XML):void {
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				this.y = this.deserializeValueFromXML(data.@y);
				if (this.category != null) {
					if (valueAxis.scale.mode == ScaleMode.STACKED) {
						this.stackValue = this.category.getStackSettings(valueAxis.name, this.series.type, false).addPoint(this,this.y);
						valueAxis.checkScaleValue(this.stackValue);
					}else if (valueAxis.scale.mode == ScaleMode.PERCENT_STACKED) {
						this.stackValue = this.category.getStackSettings(valueAxis.name, this.series.type, true).addPoint(this,this.y);
					}else {
						this.stackValue = this.y;
						valueAxis.checkScaleValue(this.stackValue);
					}
				}else {
					this.stackValue = this.y;
					valueAxis.checkScaleValue(this.stackValue);
				}
				this.isMissing = isNaN(this.y);
				this.isInverted = (valueAxis.scale.inverted && (this.y > 0)) || (!valueAxis.scale.inverted && (this.y < 0));
			}else {
				super.deserializeValue(data);
			}
		}
		
		public function updateStackValue(prevValue:Number, currValue:Number):void {
			this.stackValue = currValue;
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.y != undefined) this.y = this.stackValue = newValue.y;
			super.updateValue(newValue);
		}
		
		override public function initializeAfterInterpolate():void {
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (this.category != null) {
				if (valueAxis.scale.mode == ScaleMode.STACKED) {
					this.stackValue = this.category.getStackSettings(valueAxis.name, this.series.type, false).addPoint(this,this.y);
					valueAxis.checkScaleValue(this.stackValue);
				}else if (valueAxis.scale.mode == ScaleMode.PERCENT_STACKED) {
					this.stackValue = this.category.getStackSettings(valueAxis.name, this.series.type, true).addPoint(this,this.y);
				}else {
					this.stackValue = this.y;
					valueAxis.checkScaleValue(this.stackValue);
				}
			}else {
				this.stackValue = this.y;
				valueAxis.checkScaleValue(this.stackValue);
			}
			this.isInverted = (valueAxis.scale.inverted && (this.y > 0)) || (!valueAxis.scale.inverted && (this.y < 0));
			super.initializeAfterInterpolate();
		}
		
		public function setStackMax(value:Number):void {
			this.stackValue *= 100/value;
		}
	}
}