package com.anychart.axesPlot.axes {
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.StagerAxisLabels;
	import com.anychart.axesPlot.scales.LinearScale;
	import com.anychart.axesPlot.scales.LogScale;
	import com.anychart.axesPlot.scales.dateTime.DateTimeScale;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	
	public class ValueAxis extends Axis {
		
		override protected function createScale(scaleNode:XML):BaseScale {
			if (scaleNode == null || scaleNode.@type == undefined) return new LinearScale(this);
			switch (SerializerBase.getEnumItem(scaleNode.@type)) {
				case "datetime":
					return new DateTimeScale(this);
				case "logarithmic":
					return new LogScale(this);
				case "linear":
				default:
					return new LinearScale(this);
			}
		}
		
		override protected function createLabels(labelsNode:XML):AxisLabels {
			if (labelsNode.@display_mode == undefined) 
				return new AxisLabels(this, this.viewPort);
			if (SerializerBase.getEnumItem(labelsNode.@display_mode) == "stager")
				return new StagerAxisLabels(this, this.viewPort);
			return new AxisLabels(this, this.viewPort);
		}
		
		override public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return super.formatAxisLabelToken(token,(this.scale is LogScale) ? this.scale.delinearize(scaleValue) : scaleValue);
		}
	}
}