package com.anychart.axesPlot.scales.dateTime {
	
	public final class DateUtils {
		
		public static function addFloatYear(date:Date, offset:Number):void {
			var intOffset:Number = Math.floor(offset);
			date.setUTCFullYear(date.getUTCFullYear() + intOffset);
			
			if (offset != intOffset) 
				addFloatMonths(date, (offset - intOffset)*12);
		}
		
		public static function addFloatMonths(date:Date, offset:Number):void {
			var intOffset:Number = Math.floor(offset);
			date.setUTCMonth(date.getUTCMonth() + intOffset);
			
			if (offset != intOffset)
				addFloatDays(date, (offset - intOffset)*30); 
		}
		
		public static function addFloatDays(date:Date, offset:Number):void {
			var intOffset:Number = Math.floor(offset);
			date.setUTCDate(date.getUTCDate() + intOffset);
			
			if (offset != intOffset)
				addFloatHours(date, (offset - intOffset)*24);
		}
		
		public static function addFloatHours(date:Date, offset:Number):void {
			var intOffset:Number = Math.floor(offset);
			date.setUTCHours(date.getUTCHours() + intOffset);
			
			if (offset != intOffset)
				addFloatMinutes(date, (offset - intOffset)*60);
		}
		
		public static function addFloatMinutes(date:Date, offset:Number):void {
			var intOffset:Number = Math.floor(offset);
			date.setUTCMinutes(date.getUTCMinutes() + intOffset);
			
			if (offset != intOffset)
				addFloatSeconds(date, (offset - intOffset)*60);
		}
		
		public static function addFloatSeconds(date:Date, offset:Number):void {
			date.setUTCSeconds(date.getUTCSeconds() + offset);
		}
	}
}