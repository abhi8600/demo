package com.anychart.axesPlot.data {
	import com.anychart.animation.AnimationManager;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.drawing.DrawingMode;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	import flash.display.Sprite;
	
	public class RangePoint extends AxesPlotPoint {
		
		public var start:Number;
		public var end:Number;
		public var range:Number;
		
		//-----------------------------------------------------------------------
		//			DESERIALIZATION
		//-----------------------------------------------------------------------
		
		override protected function checkMissing(data:XML):void {
			if (data.@start == undefined || data.@end == undefined) {
				this.isMissing = true;
				return;
			} 
			var valueAxis:Axis = RangeSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				this.isMissing = isNaN(this.deserializeValueFromXML(data.@start));
				this.isMissing = this.isMissing || isNaN(this.deserializeValueFromXML(data.@end));
			}
		}
		
		override public function checkScaleValue():void {
			if (!this.isMissing) {
				var valueAxis:Axis = RangeSeries(this.series).valueAxis;
				valueAxis.checkScaleValue(this.start);
				valueAxis.checkScaleValue(this.end);
			}
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.start != undefined) this.start = this.deserializeValueFromXML(newValue.start);
			if (newValue.end != undefined) this.end = this.deserializeValueFromXML(newValue.end);
			
			var valueAxis:Axis = RangeSeries(this.series).valueAxis;
			this.isInverted = (valueAxis.scale.inverted && (this.start < this.end)) || (!valueAxis.scale.inverted && (this.start > this.end));
			
			if (!this.isMissing) {
				this.range = this.end - this.start;
				valueAxis.checkScaleValue(this.start);
				valueAxis.checkScaleValue(this.end);
			}
			
			super.updateValue(newValue);
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				start: this.start,
				end: this.end
			};
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "start" || field == "end";
		}
		
		override protected function deserializeValue(data:XML):void {
			if (data.@start != undefined)
				this.start = this.deserializeValueFromXML(data.@start);
			if (data.@end != undefined)
				this.end = this.deserializeValueFromXML(data.@end);
				
			var valueAxis:Axis = RangeSeries(this.series).valueAxis;
			this.isInverted = (valueAxis.scale.inverted && (this.start < this.end)) || (!valueAxis.scale.inverted && (this.start > this.end));
			
			if (!this.isMissing) {
				this.range = this.end - this.start;
				valueAxis.checkScaleValue(this.start);
				valueAxis.checkScaleValue(this.end);
			}
			
			super.deserializeValue(data);
		}
		
		override public function initializeAfterInterpolate():void {
			this.range = this.end - this.start;
			var valueAxis:Axis = RangeSeries(this.series).valueAxis;
			valueAxis.checkScaleValue(this.start);
			valueAxis.checkScaleValue(this.end);
			super.initializeAfterInterpolate();
		}
		
		//-----------------------------------------------------------------------
		//			ELEMENTS
		//-----------------------------------------------------------------------
		
		override protected function createElementsSprites():void {
			this.markerSprite = this.marker.initialize(this, MARKER_INDEX);
			this.labelSprite = this.label.initialize(this, LABEL_INDEX);
			this.additionalMarkerSprite = this.additionalMarker.initialize(this, END_MARKER_INDEX);
			this.additionalLabelSprite = this.additionalLabel.initialize(this, END_LABEL_INDEX);
		}
		
		override public function initializeElementsAnimation(manager:AnimationManager, index:uint, total:uint):void {
			super.initializeElementsAnimation(manager, index, total);
			this.initializeElementAnimation(this.additionalLabel, this.additionalLabelSprite, manager, index, total);
			this.initializeElementAnimation(this.additionalMarker, this.additionalMarkerSprite, manager, index, total);
		}
		
		override public function initializeElementsStyle():void {
			super.initializeElementsStyle();
			
			this.initializeElementStyleHatchType(this.additionalLabel, this.hatchType)
			this.initializeElementStyleHatchType(this.additionalMarker, this.hatchType)
			this.initializeElementStyleHatchType(this.additionalTooltip, this.hatchType)
		}
		
		override public function initializeElementsStyleColor(color:uint):void {
			this.initializeElementStyleColor(this.additionalLabel, color);
			this.initializeElementStyleColor(this.additionalMarker, color);
			this.initializeElementStyleColor(this.additionalTooltip, color);
		}
		
		private static const END_MARKER_INDEX:uint = 3;
		private static const END_LABEL_INDEX:uint = 4;
		private static const END_TOOLTIP_INDEX:uint = 5;
		
		public var additionalLabel:LabelElement;
		public var additionalMarker:MarkerElement;
		public var additionalTooltip:TooltipElement;
		
		private var additionalMarkerSprite:Sprite;
		private var additionalLabelSprite:Sprite;
		private var additionalTooltipSprite:Sprite;
		
		override public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.start_point[0] != null) {
				this.marker = this.deserializeMarkerElement(data.start_point[0], styles, this.marker, MARKER_INDEX, resources);
				this.label = this.deserializeLabelElement(data.start_point[0], styles, this.label, LABEL_INDEX, resources);
				this.tooltip = this.deserializeTooltipElement(data.start_point[0], styles, this.tooltip, TOOLTIP_INDEX, resources);
			}
			if (data.end_point[0] != null) {
				this.additionalMarker = this.deserializeMarkerElement(data.end_point[0], styles, this.additionalMarker, END_MARKER_INDEX, resources);
				this.additionalLabel = this.deserializeLabelElement(data.end_point[0], styles, this.additionalLabel, END_LABEL_INDEX, resources);
				this.additionalTooltip = this.deserializeTooltipElement(data.end_point[0], styles, this.additionalTooltip, END_TOOLTIP_INDEX, resources);
			}
		}
		
		override protected function initializeElements():void {
			
			if (this.global.drawingMode == DrawingMode.DRAW_TO_PERSONAL_CONTAINER) {
				this.tooltipSprite = this.tooltip.initialize(this, TOOLTIP_INDEX);
				this.additionalTooltipSprite = this.additionalTooltip.initialize(this, END_TOOLTIP_INDEX);
				if (this.markerSprite != null || this.labelSprite != null) {
					if (this.settings.interactivity.useHandCursor) {
						this.applyHandCursor(this.markerSprite);
						this.applyHandCursor(this.labelSprite);
						this.applyHandCursor(this.additionalMarkerSprite);
						this.applyHandCursor(this.additionalLabelSprite);
					}
					if (!this.isMissing) {
						this.applyListeners(this.markerSprite);
						this.applyListeners(this.labelSprite);
						this.applyListeners(this.additionalMarkerSprite);
						this.applyListeners(this.additionalLabelSprite);
					}
				}
			}
		}
		
		override protected function resizeMissingElements():void {
			super.resizeMissingElements();
			this.resizeElement(this.additionalMarker, this.additionalMarker.style.missing, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.resizeElement(this.additionalLabel, this.additionalLabel.style.missing, this.additionalLabelSprite, END_LABEL_INDEX);
		}
		
		override protected function resizeSelectedElements():void {
			super.resizeSelectedElements();
			this.resizeElement(this.additionalMarker, this.additionalMarker.style.selectedNormal, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.resizeElement(this.additionalLabel, this.additionalLabel.style.selectedNormal, this.additionalLabelSprite, END_LABEL_INDEX);
		}
		
		override protected function resizeNormalElements():void {
			super.resizeNormalElements();
			this.resizeElement(this.additionalMarker, this.additionalMarker.style.normal, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.resizeElement(this.additionalLabel, this.additionalLabel.style.normal, this.additionalLabelSprite, END_LABEL_INDEX);
		}
		
		override protected function drawNormalElements():void {
			super.drawNormalElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.normal, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.normal, this.additionalLabelSprite, END_LABEL_INDEX);
			this.additionalTooltip.hide(this.additionalTooltipSprite);
		}
		
		override protected function drawHoverElements():void {
			super.drawHoverElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.hover, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.hover, this.additionalLabelSprite, END_LABEL_INDEX);
			if (this.canDrawToolip) {
				this.drawElement(this.additionalTooltip, this.additionalTooltip.style.normal, this.additionalTooltipSprite, END_TOOLTIP_INDEX);
				this.additionalTooltip.move(this.additionalTooltip.style.normal, this, END_TOOLTIP_INDEX, this.additionalTooltipSprite);
			}
		}
		
		override protected function drawPushedElements():void {
			super.drawPushedElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.hover, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.hover, this.additionalLabelSprite, END_LABEL_INDEX);
			this.drawElement(this.additionalTooltip, this.additionalTooltip.style.pushed, this.additionalTooltipSprite, END_TOOLTIP_INDEX);
			this.additionalTooltip.move(this.additionalTooltip.style.normal, this, END_TOOLTIP_INDEX, this.additionalTooltipSprite);
		}

		override protected function drawMissingElements():void {
			super.drawMissingElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.missing, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.missing, this.additionalLabelSprite, END_LABEL_INDEX);
			this.additionalTooltip.hide(this.additionalTooltipSprite);
		}
		
		override protected function drawSelectedElements():void {
			super.drawSelectedElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.selectedNormal, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.selectedNormal, this.additionalLabelSprite, END_LABEL_INDEX);
			this.additionalTooltip.hide(this.additionalTooltipSprite);
		}
		
		override protected function drawSelectedHoverElements():void {
			super.drawSelectedHoverElements();
			this.drawElement(this.additionalMarker, this.additionalMarker.style.selectedHover, this.additionalMarkerSprite, END_MARKER_INDEX);
			this.drawElement(this.additionalLabel, this.additionalLabel.style.selectedHover, this.additionalLabelSprite, END_LABEL_INDEX);
			if (this.canDrawToolip) {
				this.drawElement(this.additionalTooltip, this.additionalTooltip.style.selectedNormal, this.additionalTooltipSprite, END_TOOLTIP_INDEX);
				this.additionalTooltip.move(this.additionalTooltip.style.selectedNormal, this, END_TOOLTIP_INDEX, this.additionalTooltipSprite);
			}
		}
		
		override protected function moveElements():void {
			super.moveElements();
			this.additionalTooltip.move(this.isSelected ? this.additionalTooltip.style.selectedNormal : this.additionalTooltip.style.normal,
							  		    this, END_TOOLTIP_INDEX, this.additionalTooltipSprite);
		}
		
		//-----------------------------------------------------------------------
		//			TOKENTS
		//-----------------------------------------------------------------------
		
		override public function checkCategory(categoryTokens:Object):void {
			super.checkCategory(categoryTokens);
			if (categoryTokens['RangeSum'] == null)
				categoryTokens['RangeSum'] = 0;
				
			categoryTokens['RangeSum'] += this.range;
			if (categoryTokens['RangeMax'] == null || this.range > categoryTokens['RangeMax'])
				categoryTokens['RangeMax'] = this.range;
			if (categoryTokens['RangeMin'] == null || this.range < categoryTokens['RangeMin'])
				categoryTokens['RangeMin'] = this.range;
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%RangeStart' || token == '%YRangeStart') return this.start;
			if (token == '%RangeEnd' || token == '%YRangeEnd') return this.end;
			if (token == '%Range' || token == '%YRange') return this.range;
			if (token == '%Value') return this.range;
			return super.getPointTokenValue(token);
		}
		
		override public function isDateTimePointToken(token:String):Boolean {
			if (AxesPlotSeries(this.series).valueAxis.isDateTimeAxis()) {
				if (token == "%RangeStart") return true;
				if (token == "%RangeEnd") return true;
				if (token == "%YRangeStart") return true;
				if (token == "%YRangeEnd") return true;
			}
			return super.isDateTimePointToken(token);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.YRangeStart = this.start;
			res.YRangeEnd = this.end;
			res.YRange = this.range;
			res.StartValue = this.start;
			res.EndValue = this.end;
			res.StartMarkerType = this.markerType;
			res.EndMarkerType = this.markerType;
			return res;
		}
	}
}