package com.anychart.axesPlot {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.layout.Position;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class Space3D implements ISerializable {
		
		private static const Z_AXIS_LEFT_TOP:uint = 0;
		private static const Z_AXIS_LEFT_BOTTOM:uint = 1;
		private static const Z_AXIS_RIGHT_TOP:uint = 2;
		private static const Z_AXIS_RIGHT_BOTTOM:uint = 3;

		private static const MODE_ASPECT_PERCENT_OF_CLUSTER_WIDTH:uint = 0;
		private static const MODE_PERCENT_OF_MIN_SIDE:uint = 2;
		private static const MODE_PIXEL_ASPECT:uint = 3;

		private var isAnyChart4Compatible:Boolean;

		private var aspect:Number;
		private var aspectMode:uint;
		private var aspectApplyToCluster:Boolean;
		
		private var rotation:Number;
		private var elevation:Number;
		public var padding:Number;
		
		private var angle:Number;
		private var zAxisPosition:uint;
		
		public var zAxisRange:Number;
		public var minClusterWidthInPercents:Number;
		public var isHorizontal:Boolean;
		
		public function Space3D() {
			this.aspect = 1;
			this.aspectMode = MODE_ASPECT_PERCENT_OF_CLUSTER_WIDTH;
			this.aspectApplyToCluster = false;
			this.padding = 0;
			this.rotation = 90;
			this.elevation = 90;
			this.zAxisRange = 1;
			this.isAnyChart4Compatible = true;
		}
		
		public function deserialize(data:XML):void {
			if (data.@z_padding != undefined) this.padding = SerializerBase.getNumber(data.@z_padding);
			if (data.@z_rotation != undefined) this.rotation = SerializerBase.getNumber(data.@z_rotation);
			if (data.@z_elevation != undefined) this.elevation = SerializerBase.getNumber(data.@z_elevation);
			if (data.@z_aspect != undefined) this.aspect = SerializerBase.getNumber(data.@z_aspect);
			if (data.@z_aspect_mode != undefined) {
				switch (SerializerBase.getEnumItem(data.@z_aspect_mode)) {
					case "percentofminside":
						this.aspectMode = MODE_PERCENT_OF_MIN_SIDE;
						break;
						
					case "pixelaspect":
						this.aspectMode = MODE_PIXEL_ASPECT;
						break;
					
					default:
					case "percentofclusterwidth":
						this.aspectMode = MODE_ASPECT_PERCENT_OF_CLUSTER_WIDTH;
						break;
				}
			}
			if (data.@anychart_4_compatible != undefined)
				this.isAnyChart4Compatible = SerializerBase.getBoolean(data.@anychart_4_compatible);
			
			if (data.@z_aspect_apply_to != undefined)
				this.aspectApplyToCluster = SerializerBase.getEnumItem(data.@z_aspect_apply_to) == "cluster";
				
			this.aspectApplyToCluster = this.aspectApplyToCluster || this.isAnyChart4Compatible;
			
			this.rotation = Math.min(Math.max(this.rotation, 0),90);
			this.elevation = Math.min(Math.max(this.elevation, 0),90);
		}
		
		public function initialize(primaryXAxis:Axis, primaryYAxis:Axis):void {
			//Reproduce AnyChart 4 bug
			if (this.aspectMode == MODE_ASPECT_PERCENT_OF_CLUSTER_WIDTH && this.isAnyChart4Compatible)
				this.aspect /= 2;
				
			this.initializeAngle(primaryXAxis, primaryYAxis);
			this.initializeAspect();
		}
		
		//----------------------------------------------------------------------------
		//	Real angle calculation
		//----------------------------------------------------------------------------
		
		private function initializeAngle(primaryXAxis:Axis, primaryYAxis:Axis):void {
			this.angle = Math.atan(this.elevation/this.rotation);
			var pos1:uint = primaryXAxis.position;
			var pos2:uint = primaryYAxis.position;
			
			if (this.checkAxesPositions(pos1, pos2, Position.LEFT, Position.BOTTOM)) {
				this.angle = -this.angle;
				this.zAxisPosition = Z_AXIS_LEFT_BOTTOM;
			}else if (this.checkAxesPositions(pos1, pos2, Position.RIGHT, Position.TOP)) {
				this.angle = Math.PI - this.angle;
				this.zAxisPosition = Z_AXIS_RIGHT_TOP;
			}else if (this.checkAxesPositions(pos1, pos2, Position.RIGHT, Position.BOTTOM)) {
				this.angle = Math.PI + this.angle;
				this.zAxisPosition = Z_AXIS_RIGHT_BOTTOM;
			}else {
				this.zAxisPosition = Z_AXIS_LEFT_TOP;
			}
		}
		
		private function checkAxesPositions(pos1:uint, pos2:uint, requiredPos1:uint, requiredPos2:uint):Boolean {
			if (pos1 == requiredPos1 && pos2 == requiredPos2) return true;
			if (pos2 == requiredPos1 && pos1 == requiredPos2) return true;
			return false;
		}
		
		//----------------------------------------------------------------------------
		//	Aspect calculation
		//----------------------------------------------------------------------------
		
		private var xAspect:Number;
		private var yAspect:Number;
		private var absXAspect:Number;
		private var absYAspect:Number;
		
		private function initializeAspect():void {
			this.xAspect = this.aspect*Math.cos(this.angle);
			this.yAspect = this.aspect*Math.sin(this.angle);
			
			this.absXAspect = Math.abs(this.xAspect);
			this.absYAspect = Math.abs(this.yAspect);
		}
		
		//----------------------------------------------------------------------------
		//	Aspect apply
		//----------------------------------------------------------------------------
		
		private var croppedWidth:Number;
		private var croppedHeight:Number;
		private var isWidthBased:Boolean;
		
		private var pixXAspect:Number;
		private var pixYAspect:Number;
		
		private var _pixAspect:Number;
		public function get pixAspect():Number { return this._pixAspect; }
		public function getPixXAspect():Number { return this.pixXAspect; }
		public function getPixYAspect():Number { return this.pixYAspect; } 
		
		public function getXAxisOffset():Number {
			if (this.pixXAspect < 0) return 0;
			return this.pixXAspect;
		}
		
		public function getYAxisOffset():Number {
			if (this.pixYAspect < 0) return 0;
			return this.pixYAspect;
		}
		
		public function preCropBounds(bounds:Rectangle):void {
			if (isNaN(this.minClusterWidthInPercents))
				this.aspectMode = MODE_PERCENT_OF_MIN_SIDE;
			this.isWidthBased = bounds.width <= bounds.height;
			this.cropRect(bounds);
		}
		
		public function cropBounds(bounds:Rectangle):void {
			bounds.width += this.croppedWidth;
			bounds.height += this.croppedHeight;
			
			this.cropRect(bounds);
		}
		
		private function cropRect(rect:Rectangle):void {
			var multiplier:Number = this.aspectApplyToCluster ? this.zAxisRange : 1;
			
			switch (this.aspectMode) {
				
				case MODE_ASPECT_PERCENT_OF_CLUSTER_WIDTH:
					multiplier *= this.isHorizontal ? rect.height : rect.width;
					multiplier *= this.minClusterWidthInPercents;
					break;
				
				case MODE_PERCENT_OF_MIN_SIDE:
					multiplier *= this.isWidthBased ? rect.width : rect.height;
					break;
			}
			
			this.croppedWidth = this.absXAspect*multiplier;
			this.croppedHeight = this.absYAspect*multiplier;
			this.pixXAspect = this.xAspect*multiplier;
			this.pixYAspect = this.yAspect*multiplier;
			
			rect.width -= this.croppedWidth;
			rect.height -= this.croppedHeight;
			
			this._pixAspect = Math.sqrt(Math.pow(this.pixXAspect, 2) + Math.pow(this.pixYAspect, 2));
		}
		
		//----------------------------------------------------------------------------
		//	Axes and plot position
		//----------------------------------------------------------------------------
		
		public function movePlot(bounds:Rectangle):void {
			if (this.yAspect > 0) bounds.y += this.croppedHeight;
			if (this.xAspect > 0) bounds.x += this.croppedWidth;
		}
		
		private var _axesX:Number;
		private var _axesY:Number;
		
		public function initializeAxesPositions(bounds:Rectangle):void {
			this._axesX = bounds.x;
			this._axesY = bounds.y;
			
			this._axesY += (this.yAspect < 0) ? this.croppedHeight : 0;
			this._axesX += (this.xAspect < 0) ? this.croppedWidth : 0;
		}
		
		public function get axesX():Number { return this._axesX; }
		public function get axesY():Number { return this._axesY; } 
		
		//----------------------------------------------------------------------------
		//	3D transformations
		//----------------------------------------------------------------------------
		
		public function getFrontX(x:Number):Number {
			return x - this.pixXAspect;
		}
		
		public function getBackX(x:Number):Number {
			return x + this.pixXAspect;
		}
		
		public function getFrontY(y:Number):Number {
			return y - this.pixYAspect;
		}
		
		public function getBackY(y:Number):Number {
			return y + this.pixYAspect;
		}
		
		public function getPointXOffset(aspect:Number):Number {
			return -this.pixXAspect*(aspect/this._pixAspect);
		}
		
		public function getPointYOffset(aspect:Number):Number {
			return -this.pixYAspect*(aspect/this._pixAspect);
		}
		
		//----------------------------------------------------------------------------
		//	axes planes
		//----------------------------------------------------------------------------
		
		internal function getXAxesPlanePoints(plot:AxesPlot3D):Array {
			return this.getPlanePoints(plot.getBounds(), plot.getPrimaryArgumentAxis().position);
		}
		
		internal function getYAxesPlanePoints(plot:AxesPlot3D):Array {
			return this.getPlanePoints(plot.getBounds(), plot.getPrimaryValueAxis().position);
		}
		
		private function getPlanePoints(bounds:Rectangle, position:uint):Array {
			switch (position) {
				case Position.BOTTOM:
					return this.getHorizontalPlane(bounds, bounds.bottom);
				case Position.TOP:
					return this.getHorizontalPlane(bounds, bounds.top);
				case Position.LEFT:
					return this.getVerticalPlane(bounds, bounds.left);
				case Position.RIGHT:
					return this.getVerticalPlane(bounds, bounds.right);
			}
			return null;
		}
		
		private function getHorizontalPlane(bounds:Rectangle, baseY:Number):Array {
			return [
				new Point(bounds.left, baseY),
				new Point(bounds.left-this.pixXAspect, baseY - this.pixYAspect),
				new Point(bounds.right-this.pixXAspect, baseY - this.pixYAspect),
				new Point(bounds.right, baseY)
			];
		}
		
		private function getVerticalPlane(bounds:Rectangle, baseX:Number):Array {
			return [
				new Point(baseX, bounds.bottom),
				new Point(baseX - this.pixXAspect, bounds.bottom - this.pixYAspect),
				new Point(baseX - this.pixXAspect, bounds.top - this.pixYAspect),
				new Point(baseX, bounds.top)
			];
		}
		
		//----------------------------------------------------------------------------
		//	Z axis position
		//----------------------------------------------------------------------------
		
		public function isZAxisAtLeftBottom():Boolean { return this.zAxisPosition == Z_AXIS_LEFT_BOTTOM; }
		public function isZAxisAtLeftTop():Boolean { return this.zAxisPosition == Z_AXIS_LEFT_TOP; }
		public function isZAxisAtRightBottom():Boolean { return this.zAxisPosition == Z_AXIS_RIGHT_BOTTOM; }
		public function isZAxisAtRightTop():Boolean { return this.zAxisPosition == Z_AXIS_RIGHT_TOP; }
		
		//----------------------------------------------------------------------------
		//	Mask points
		//----------------------------------------------------------------------------
		
		public function getMaskPoints(frontRect:Rectangle, backRect:Rectangle):Array {
			var res:Array = [];
			
			var xPositive:Boolean = this.xAspect >= 0;
			var yPositive:Boolean = this.yAspect >= 0;
			
			//see SUP-335
			if (xPositive && !yPositive) {
				res.push(new Point(frontRect.x, frontRect.y));
				res.push(new Point(backRect.x, backRect.y));
				res.push(new Point(backRect.right, backRect.y));
				res.push(new Point(backRect.right, backRect.bottom));
				res.push(new Point(frontRect.right, frontRect.bottom));
				res.push(new Point(frontRect.x, frontRect.bottom));
				res.push(new Point(frontRect.x, frontRect.y));
			}else if (xPositive && yPositive) {
				res.push(new Point(frontRect.x, frontRect.y));
				res.push(new Point(frontRect.right, frontRect.y));
				res.push(new Point(backRect.right, backRect.y));
				res.push(new Point(backRect.right, backRect.bottom));
				res.push(new Point(backRect.x, backRect.bottom));
				res.push(new Point(frontRect.x, frontRect.bottom));
			}else if (!xPositive && !yPositive) {
				res.push(new Point(backRect.x, backRect.y));
				res.push(new Point(backRect.right, backRect.y));
				res.push(new Point(frontRect.right, frontRect.y));
				res.push(new Point(frontRect.right, frontRect.bottom));
				res.push(new Point(frontRect.x, frontRect.bottom));
				res.push(new Point(backRect.x, backRect.bottom));
			}else if (!xPositive && yPositive) {
				res.push(new Point(frontRect.x, frontRect.y));
				res.push(new Point(frontRect.right, frontRect.y));
				res.push(new Point(frontRect.right, frontRect.bottom));
				res.push(new Point(backRect.right, backRect.bottom));
				res.push(new Point(backRect.x, backRect.bottom));
				res.push(new Point(backRect.x, backRect.y));
			}
				
			return res;
		}
	}
}