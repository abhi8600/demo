package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.scrolling.ScrollBarPosition;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisLabelsAlign;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class BottomAxisViewPort extends HorizontalAxisViewPort implements IAxisViewPort {
		
		public function BottomAxisViewPort(axis:Axis) {
			super(axis);
		}
		
		override public function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void {
			super.setParallelLineBounds(lineThickness, start, end, offset, bounds);
			bounds.y += this.visibleBounds.bottom + offset - lineThickness/2;
		}
		
		public function drawParallelLine(g:Graphics, start:Number, end:Number, offset:Number):void {
			var y:Number = this.visibleBounds.bottom + offset + this.yOffset;
			g.moveTo(start+this.xOffset, y);
			g.lineTo(end+this.xOffset, y);
		}
		
		public function setInsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void {
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.x += this.xOffset;
			bounds.y = this.visibleBounds.bottom + offset - size + this.yOffset;
		}
		
		public function setOutsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void {
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.x += this.xOffset;
			bounds.y = this.visibleBounds.bottom + offset + this.yOffset;
		}
		
		public function setOppositeAxisTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void {
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.y = this.visibleBounds.top;
		}
		
		public function drawInsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void {
			g.moveTo(axisValue + this.xOffset, this.visibleBounds.bottom + offset - size + this.yOffset);
			g.lineTo(axisValue + this.xOffset, this.visibleBounds.bottom + offset + this.yOffset);
		}
		
		public function drawOutsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void {
			g.moveTo(axisValue + this.xOffset, this.visibleBounds.bottom + offset + this.yOffset);
			g.lineTo(axisValue + this.xOffset, this.visibleBounds.bottom + offset + size + this.yOffset);
		}
		
		public function drawOppositeAxisTickmark(g:Graphics, axisValue:Number, size:Number):void {
			g.moveTo(axisValue, this.visibleBounds.top);
			g.lineTo(axisValue, this.visibleBounds.top + size);
		}
		
		override public function setTitlePosition(title:AxisTitle, pos:Point):void {
			super.setTitlePosition(title, pos);
			pos.y += this.visibleBounds.bottom + title.offset + title.padding;
		}
		
		public function applyOffset(bounds:Rectangle, offset:Number):void {
			bounds.height -= offset;
		}
		
		public function getLabelXOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
	    	if (rotation % 180 == 0) return -.5;
	    	if (rotation % 90 == 0) return 0;
	    	if (qIndex == 1 || qIndex == 3)
	    		return -absCos;
	    	return 0;
	    }
	    
		public function getLabelXOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			if (rotation % 180 == 0) return 0;
			if (rotation % 90 == 0) return -.5;
			if (qIndex == 0 || qIndex == 2)
				return -absSin;
			return 0;
		}
		
		override public function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void {
			super.setLabelPosition(labels, info, pos, pixPos);
			pos.y += this.visibleBounds.bottom + labels.offset;
			switch (labels.align) {
				case AxisLabelsAlign.CENTER:
					pos.y += (labels.space - info.rotatedBounds.height)/2;
					break;
				case AxisLabelsAlign.OUTSIDE:
					pos.y += labels.space - info.rotatedBounds.height;
					break;
			}
		}
		
		override public function setMarkerLabelPosition(pixPos:Number, padding:Number, offset:Number, bounds:Rectangle, pos:Point):void {
			super.setMarkerLabelPosition(pixPos, padding, offset, bounds, pos);
			pos.y += this.visibleBounds.bottom + offset + padding;
		}
		
		public function setInsideMarkerLabelPosition(startPixPos:Number, 
	    										     endPixPos:Number, 
	    										     align:uint, 
	    										     padding:Number, 
	    										     bounds:Rectangle, 
	    										     pos:Point):void {
			var near:Point = new Point(startPixPos, this.bounds.height);
			var far:Point = new Point(endPixPos, 0);
			var angle:Number = Math.atan2(far.y - near.y, far.x - near.x);
			var xOffset:Number = padding*Math.cos(angle);
			var yOffset:Number = padding*Math.sin(angle);
	    	switch (align) {
	    		case AbstractAlign.NEAR:
	    			pos.y = near.y - bounds.height + yOffset;
	    			pos.x = startPixPos + xOffset - bounds.width/2;
	    			break;
	    		case AbstractAlign.CENTER:
	    			pos.y = (near.y + far.y)/2 - bounds.height/2;
	    			pos.x = (near.x + far.x)/2 - bounds.width/2;
	    			break;
	    		case AbstractAlign.FAR:
	    			pos.y = far.y - yOffset;
	    			pos.x = far.x - bounds.width/2 - xOffset;
	    			break;
	    	}
	    	this.offsetInsideMarkerLabel(pos);
	    }
		
		override public function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void {
			super.setStagerLabelPosition(labels, info, pos, pixPos, offset, space);
			pos.y += this.visibleBounds.bottom + labels.offset + offset;
			switch (labels.align) {
				case AxisLabelsAlign.CENTER:
					pos.y += (space - info.rotatedBounds.height)/2;
					break;
				case AxisLabelsAlign.OUTSIDE:
					pos.y += space - info.rotatedBounds.height;
					break;
			}
		}
		
		override public function setStartValue(value:Number, pt:Point):void {
			super.setEndValue(value, pt);
		}
		
		override public function setEndValue(value:Number, pt:Point):void {
			super.setStartValue(value, pt);
		}
		
		public function setOffset(offset:Number, bounds:Rectangle, pos:Point):void {
			pos.y = this.visibleBounds.bottom + offset + this.yOffset;
			pos.x -= bounds.width/2 - this.xOffset;
		}
		
		override public function getScrollBarPixPosition(scrollBarPosition:uint, offset:Number, size:Number):Point {
			var pos:Point = super.getScrollBarPixPosition(scrollBarPosition, offset, size);
			pos.y = this.visibleBounds.bottom + offset;
			if (scrollBarPosition == ScrollBarPosition.INSIDE)
				pos.y -= size; 
			return pos;
		}
	}
}