package com.anychart.axesPlot.axes.scrolling {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.uiControls.scrollBar.ScrollBarVertical;
	
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;

	internal final class VerticalAxisScrollBar extends ScrollBarVertical implements IAxisScrollBar {
		
		private var _position:uint;
		private var axis:Axis;
		
		public function VerticalAxisScrollBar(axis:Axis, container:InteractiveObject) {
			super(container);
			this.axis = axis;
			this._position = ScrollBarPosition.OUTSIDE;
			if ( (!this.axis.scale.inverted && this.axis as ValueAxis) || (this.axis.scale.inverted && !(this.axis as ValueAxis)) )	super.invertStep();
		}
		
		public function get position():uint { return this._position; }
		public function set position(value:uint):void { this._position = value; }
		
		override public function thumb_onMouseDown(e:MouseEvent):void {
			this.axis.scrollSource="scrollBar";
			this.axis.scrollPhase="start";
			AxesPlot(this.axis.plot).canHideMouse = false;
			AxesPlot(this.axis.plot).disablePointEvents();
			super.thumb_onMouseDown(e);
		}
		
		override protected function thumb_onMouseMove(e:MouseEvent):void
		{
			this.axis.scrollPhase="process";
			super.thumb_onMouseMove(e);
		}
		
		override protected function thumb_onMouseUp(e:MouseEvent):void {
			this.axis.scrollPhase="finish";
			AxesPlot(this.axis.plot).enablePointEvents();
			super.thumb_onMouseUp(e);
			AxesPlot(this.axis.plot).canHideMouse = true;
		}

		override protected function buttonAdd_onMouseDown(e:MouseEvent):void
		{
			this.axis.scrollSource="scrollBar";
			this.axis.scrollPhase="finish";
			super.buttonAdd_onMouseDown(e);
		}
		
		override protected function buttonSub_onMouseDown(e:MouseEvent):void
		{
			this.axis.scrollSource="scrollBar";
			this.axis.scrollPhase="finish";
			super.buttonSub_onMouseDown(e);
		}
	}
}