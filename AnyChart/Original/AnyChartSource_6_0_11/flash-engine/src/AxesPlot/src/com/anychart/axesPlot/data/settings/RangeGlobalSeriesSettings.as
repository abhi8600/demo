package com.anychart.axesPlot.data.settings {
	import com.anychart.interpolation.IMissingInterpolator;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BaseDataElement;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	public class RangeGlobalSeriesSettings extends GlobalSeriesSettings {
		public var additionalLabel:LabelElement;
		public var additionalMarker:MarkerElement;
		public var additionalTooltip:TooltipElement;
		
		override public function deserializeElements(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.marker = this.deserializeMarkerElement(data.start_point[0], stylesList, this.marker, resources);
			this.label = this.deserializeLabelElement(data.start_point[0], stylesList, this.label, resources);
			this.tooltip = this.deserializeTooltipElement(data.start_point[0], stylesList, this.tooltip, resources); 
			
			this.additionalMarker = this.deserializeMarkerElement(data.end_point[0], stylesList, this.additionalMarker, resources);
			this.additionalLabel = this.deserializeLabelElement(data.end_point[0], stylesList, this.additionalLabel, resources);
			this.additionalTooltip = this.deserializeTooltipElement(data.end_point[0], stylesList, this.additionalTooltip, resources);
		}
		
		override public function setElements(data:BaseDataElement):void {
			super.setElements(data);
			data['additionalMarker'] = this.additionalMarker;
			data['additionalLabel'] = this.additionalLabel;
			data['additionalTooltip'] = this.additionalTooltip;
		}
		
		private var startInterpolator:IMissingInterpolator;
		private var endInterpolator:IMissingInterpolator;
		
		override public function missingInitializeInterpolators():void {
			this.startInterpolator = this.createMissingInterpolator();
			this.endInterpolator = this.createMissingInterpolator();
			this.startInterpolator.initialize("start");
			this.endInterpolator.initialize("end");
		}
		
		override public function missingCheckPointDuringDeserialize(point:BasePoint):void {
			this.startInterpolator.checkDuringDeserialize(point, point.index);
			this.endInterpolator.checkDuringDeserialize(point, point.index);
		}
		
		override public function missingInterpolate(points:Array):void {
			var startMissings:Array = this.startInterpolator.interpolate(points);
			var endMissings:Array = this.endInterpolator.interpolate(points);
			var point:BasePoint;
			for each (point in startMissings) {
				if (endMissings.indexOf(point) != -1)
					endMissings.splice(endMissings.indexOf(point), 1);
				this.missingInitializeAfterInterpolate(point);
			}
			
			for each (point in endMissings) {
				this.missingInitializeAfterInterpolate(point);
			}
			
			this.startInterpolator.clear();
			this.endInterpolator.clear();
		}
	}
}