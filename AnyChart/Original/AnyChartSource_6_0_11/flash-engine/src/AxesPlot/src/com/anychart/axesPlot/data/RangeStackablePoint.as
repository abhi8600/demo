package com.anychart.axesPlot.data {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.scales.ScaleMode;
	
	
	public class RangeStackablePoint extends StackablePoint {
		
		public var startStackValue:Number;
		
		override protected function deserializeValue(data:XML):void {
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				this.y = this.deserializeValueFromXML(data.@y);
				if (this.category != null) {
					var stackSettings:StackSettings;
					if (valueAxis.scale.mode == ScaleMode.STACKED) {
						stackSettings = this.category.getStackSettings(valueAxis.name, this.series.type, false);
						this.startStackValue = stackSettings.getPrevious(this.y);
						this.stackValue = stackSettings.addPoint(this,this.y);
						valueAxis.checkScaleValue(this.startStackValue);
						valueAxis.checkScaleValue(this.stackValue);
					}else if (valueAxis.scale.mode == ScaleMode.PERCENT_STACKED) {
						stackSettings = this.category.getStackSettings(valueAxis.name, this.series.type, true);
						this.startStackValue = stackSettings.getPrevious(this.y);
						this.stackValue = stackSettings.addPoint(this,this.y);
					}else {
						this.startStackValue = NaN;
						this.stackValue = this.y;
						valueAxis.checkScaleValue(this.stackValue);
					}
				}else {
					this.startStackValue = NaN;
					this.stackValue = this.y;
					valueAxis.checkScaleValue(this.stackValue);
				}
				this.isMissing = isNaN(this.y);
				this.isInverted = (valueAxis.scale.inverted && (this.y > 0)) || (!valueAxis.scale.inverted && (this.y < 0));
			}else {
				super.deserializeValue(data);
				this.startStackValue = NaN;
				this.stackValue = this.y;
			}
		}
		
		override public function updateValue(newValue:Object):void {
			this.stackValue = this.y = this.deserializeValueFromXML(newValue.y);

			if (AxesPlotSeries(this.series).valueAxis is ValueAxis) {
				this.isMissing = isNaN(this.y);
				this.isInverted = (AxesPlotSeries(this.series).valueAxis.scale.inverted && (this.y > 0)) || (!AxesPlotSeries(this.series).valueAxis.scale.inverted && (this.y < 0));
			}
			
			super.updateValue(newValue);
		}
		
		override public function updateStackValue(prevValue:Number, currValue:Number):void {
			this.startStackValue = prevValue;
			this.stackValue = currValue;
		}
		
		override public function checkScaleValue():void {
			super.checkScaleValue();
			
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				if (this.category != null) {
					if (valueAxis.scale.mode == ScaleMode.STACKED) {
						valueAxis.checkScaleValue(this.startStackValue);
						valueAxis.checkScaleValue(this.stackValue);
					}else {
						valueAxis.checkScaleValue(this.stackValue);
					}
				}else {
					valueAxis.checkScaleValue(this.stackValue);
				}
			}else {
				valueAxis.checkScaleValue(this.stackValue);
			}
		}
		
		
		override public function initializeAfterInterpolate():void {
			
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (this.category != null) {
				var stackSettings:StackSettings;
				if (valueAxis.scale.mode == ScaleMode.STACKED) {
					stackSettings = this.category.getStackSettings(valueAxis.name, this.series.type, false);
					this.startStackValue = stackSettings.getPrevious(this.y);
					this.stackValue = stackSettings.addPoint(this,this.y);
					valueAxis.checkScaleValue(this.startStackValue);
					valueAxis.checkScaleValue(this.stackValue);
				}else if (valueAxis.scale.mode == ScaleMode.PERCENT_STACKED) {
					stackSettings = this.category.getStackSettings(valueAxis.name, this.series.type, true);
					this.startStackValue = stackSettings.getPrevious(this.y);
					this.stackValue = stackSettings.addPoint(this,this.y);
				}else {
					this.startStackValue = NaN;
					this.stackValue = this.y;
					valueAxis.checkScaleValue(this.stackValue);
				}
			}else {
				this.startStackValue = NaN;
				this.stackValue = this.y;
				valueAxis.checkScaleValue(this.stackValue);
			}
			
			super.initializeAfterInterpolate();
		}
		
		override public function setStackMax(value:Number):void {
			this.startStackValue *= (value == 0) ? 0 : (100/value);
			this.stackValue *= (value == 0) ? 0 : (100/value);
		}
	}
}