package com.anychart.axesPlot.data {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.IAxesPlot;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.CategorizedAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.categorization.CategoryInfo;
	import com.anychart.axesPlot.axes.categorization.ClusterSettings;
	import com.anychart.axesPlot.data.data3D.ZPointSettings;
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.scales.ScaleMode;
	import com.anychart.selector.HitTestUtil;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.categories.ICategorizedPoint;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.Position;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class AxesPlotPoint extends BasePoint implements ICategorizedPoint {
		public var x:Number;
		public var category:Category;
		public var dataCategory:CategoryInfo;
		public var z:ZPointSettings;
		public var clusterIndex:int;
		public var clusterSettings:ClusterSettings;
		
		public var isHorizontal:Boolean;
		public var isInverted:Boolean;
		
		public function AxesPlotPoint() {
			super();
			this.isHorizontal = false;
			this.isInverted = false;
		}
		
		override public final function deserialize(data:XML):void {
			if (AxesPlot(this.plot).is3D())
				this.z = this.create3D();
			this.checkMissing(data); 
			super.deserialize(data);
			this.deserializeArgument(data);
			
			if (!this.isMissing)
				this.deserializeValue(data);
				
			if (!this.isMissing && this.dataCategory) {
				this.dataCategory.checkPoint(this);
				if (this.dataCategory != this.category)
					this.category.checkPoint(this);
			}
			
			if (this.z)
				this.z.deserialize(this, data);
			AxesPlotSeries(this.series).argumentAxis.points.push(this);
			AxesPlotSeries(this.series).valueAxis.points.push(this);
		}
		
		protected function checkMissing(data:XML):void {}
		
		protected function deserializeValue(data:XML):void {
		}
		
		override public function updateValue(newValue:Object):void {
			super.updateValue(newValue);
		}
		
		public function checkScaleValue():void {
		}
		
		protected function deserializeArgument(data:XML):void {
			var argumentAxis:Axis = AxesPlotSeries(this.series).argumentAxis;
			
			if (data.@x != undefined)
				this.x = AxesPlotSeries(this.series).argumentAxis.scale.deserializeValue(data.@x);
			
			var axesSeries:AxesPlotSeries = AxesPlotSeries(this.series);
			this.isHorizontal = argumentAxis.position == Position.LEFT || argumentAxis.position == Position.RIGHT;
			
			if (argumentAxis is ValueAxis) {
				if (data.@name != undefined && isNaN(this.x))
					this.x = AxesPlotSeries(this.series).argumentAxis.scale.deserializeValue(data.@name);
				argumentAxis.checkScaleValue(this.x);
			}else {
				if (argumentAxis is CategorizedAxis) {
					this.dataCategory = this.category = CategorizedAxis(argumentAxis).getCategory(this.name);
					this.x = this.category.index;
					this.clusterSettings = axesSeries.clusterSettings;
					this.paletteIndex = this.category.index;
					this.index = this.category.index;
				}else {
					//argumentAxis is CategorizedBySeriesAxis
					this.category = axesSeries.category;
					this.dataCategory = IAxesPlot(this.plot).getDataCategory(argumentAxis, this.name);
					this.x = axesSeries.category.index;
					if (axesSeries.isSingleCluster) {
						this.clusterIndex = 0;
					}else {
						this.category.clusterSettings.numClusters++;
						this.clusterIndex = this.index;
					}
					this.clusterSettings = this.category.clusterSettings;
				}
				if (axesSeries.isSortable && axesSeries.valueAxis.scale.mode == ScaleMode.SORTED_OVERLAY)
					this.category.getSortedOverlay(axesSeries.valueAxis.name, axesSeries.type).push(this);
			}
		}
		
		protected function deserializeValueFromXML(value:*):Number {
			return AxesPlotSeries(this.series).valueAxis.scale.deserializeValue(value);
		}
		
		protected function create3D():ZPointSettings {
			return new ZPointSettings();
		}
		
		override public function initializeAfterInterpolate():void {
			super.initializeAfterInterpolate();
			if (this.dataCategory)
				this.dataCategory.checkPoint(this);
			if (this.dataCategory != null && this.category != null && this.dataCategory != this.category)
				this.category.checkPoint(this);
		}
		
		//-------------------------------------------------
		//				Drawing
		//-------------------------------------------------
		
		protected final function setStateRotations(state:StyleState):void {
			if (state == null) return;
			if (this.isInverted)
				state.setInvertedRotation();
			else 
				state.setNormalRotation();
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.setStateRotations(this.styleState);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.setStateRotations(this.styleState);
		}
		
		//-------------------------------------------------
		//				Formatting
		//-------------------------------------------------
		
		public function checkCategory(categoryTokens:Object):void {
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%XValue') return this.x;
			if (token == '%XPercentOfSeries') {
				if (this.cashedTokens['XPercentOfSeries'] == null) {
					this.cashedTokens['XPercentOfSeries'] = this.x/Number(this.series.getSeriesTokenValue('%SeriesXSum')) * 100;
				}
				return this.cashedTokens['XPercentOfSeries'];
			}
			if (token == '%XPercentOfTotal') {
				if (this.cashedTokens['XPercentOfTotal'] == null) {
					this.cashedTokens['XPercentOfTotal'] = this.x/Number(this.plot.getTokenValue('%DataPlotXSum')) * 100;
				}
				return this.cashedTokens['XPercentOfTotal'];
			}
			return super.getPointTokenValue(token);
		}
		
		override public function getTokenValue(token:String):* {
			if (token.indexOf("%YAxis") == 0)
				return AxesPlotSeries(this.series).valueAxis.getAxisTokenValue("%"+token.substr(2));
			if (token.indexOf("%XAxis") == 0)
				return AxesPlotSeries(this.series).argumentAxis.getAxisTokenValue("%"+token.substr(2));
			if (token.indexOf("%Category") == 0)
				return dataCategory.getTokenValue(token);
			return super.getTokenValue(token);
		}
		
		override public function isDateTimeToken(token:String):Boolean {
			if (token.indexOf("%YAxis") == 0)
				return AxesPlotSeries(this.series).valueAxis.isDateTimeToken(token);
			if (token.indexOf("%XAxis") == 0)
				return AxesPlotSeries(this.series).argumentAxis.isDateTimeToken(token);
			if (token.indexOf("%Category") == 0)
				return this.dataCategory.isDateTimeToken(token);
			
			return super.isDateTimeToken(token);
		}
		
		override public function isDateTimePointToken(token:String):Boolean {
			if (AxesPlotSeries(this.series).argumentAxis.isDateTimeAxis()) {
				if (token == "%XValue") return true;
				if (token == "%Name") return true; 
			}
			return false;
		}
		
		override protected function getNameToken():* {
			if (AxesPlotSeries(this.series).argumentAxis.isCategorizedDateTimeAxis()) {
				return SerializerBase.getDateTime(this.name, AxesPlot(this.plot).dateTimeLocale);
			}else { return super.getNameToken(); }
		}
		
		
		override public final function getElementPosition(state:BaseElementStyleState, sprite:Sprite, elementSize:Rectangle):Point {
			if (state == null) return null;
			this.setStateRotations(state);
			return super.getElementPosition(state, sprite, elementSize);
		}
		
		override protected function checkPointElementPosition(element:BaseElement, elementIndex:uint):void {
			if (BaseElementStyleState(element.style.normal).anchor == Anchor.X_AXIS)
				AxesPlotSeries(this.series).argumentAxis.addPointAxisElement(this, element, elementIndex); 
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			if (anchor == Anchor.X_AXIS)
				AxesPlotSeries(this.series).argumentAxis.setElementPosition(this, bounds, padding, pos);
			else
				super.getAnchorPoint(anchor, pos, bounds, padding);
		}
		
		//---------------------------------------------------------------
		//				Hit test
		//---------------------------------------------------------------
		
		override public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			var tmp:Shape = new Shape();
			tmp.graphics.beginFill(0xFF0000, 1);
			tmp.graphics.drawRect(0,0, area.width, area.height);
			tmp.x = area.x;
			tmp.y = area.y;
			this.container.parent.addChild(tmp);
			
			var res:Boolean = HitTestUtil.hitTest(this.container, tmp);
			
			this.container.parent.removeChild(tmp);
			tmp = null;
			return res;
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.XValue = this.x;
			
			res.XPercentOfSeries = this.getPointTokenValue("%XPercentOfSeries");
			res.XPercentOfTotal = this.getPointTokenValue("%XPercentOfTotal");
			res.XPercentOfCategory = this.getPointTokenValue("%XPercentOfCategory");
			
			res.Category = this.dataCategory ? this.dataCategory.name : null;
			return res;
		}
	}
}