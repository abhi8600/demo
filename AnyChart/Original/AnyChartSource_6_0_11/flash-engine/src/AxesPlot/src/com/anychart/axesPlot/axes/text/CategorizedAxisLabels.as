package com.anychart.axesPlot.axes.text {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.categorization.CategoryInfo;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.axesPlot.scales.ValueScale;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class CategorizedAxisLabels extends AxisLabels {
		
		public function CategorizedAxisLabels(axis:Axis, viewPort:IAxisViewPort) {
			super(axis, viewPort);
		}
		
		private function formatCategoryName(token:String):* {
			return (token == '%Value') ? ValueScale(this.axis.scale).getCategoryName(this.tmpCategory) : '';
		} 
		
		private var tmpCategory:Category;
		
		public function addCategory(category:Category):void {
			this.tmpCategory = category;
			category.info = this.createInformation();
			category.info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.formatCategoryName, this.isDateTimeToken) : this.text;
			this.getBounds(category.info);
			this.tmpCategory = null;
		}
		
		override public function initMaxLabelSize():void {
			this.maxNonRotatedLabelSize = new Rectangle();
			this.maxLabelSize = new Rectangle();
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
				this.maxLabelSize.width = info.rotatedBounds.width;
				this.maxLabelSize.height = info.rotatedBounds.height;
				this.maxNonRotatedLabelSize.width = info.nonRotatedBounds.width;
				this.maxNonRotatedLabelSize.height = info.nonRotatedBounds.height;
			}else {
				var majorIntervalsCount:uint = this.axis.scale.majorIntervalsCount; 
				for (var i:uint = 0;i<=majorIntervalsCount;i++) {
					if (this.isDynamicText) {
						var categoryIndex:uint = this.axis.scale.getMajorIntervalValue(i);
						var category:Category = BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex+.5);
						if (!category) continue;
						info = category.info;
						if (!info) continue;
					}
					
					if (info.rotatedBounds.width > this.maxLabelSize.width) {
						this.maxLabelSize.width = info.rotatedBounds.width;
						this.maxNonRotatedLabelSize.width = info.nonRotatedBounds.width;
					}if (info.rotatedBounds.height > this.maxLabelSize.height) {
						this.maxLabelSize.height = info.rotatedBounds.height;
						this.maxNonRotatedLabelSize.height = info.nonRotatedBounds.height;
					}
				}
			}
		}
		
		override public function drawLabels(g:Graphics):void {
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
			}
			
			var pos:Point = new Point();
			
			var checkLabels:Boolean = !this.allowOverlap;
			var isInvertedLabelsCheck:Boolean = checkLabels && this.viewPort.isInvertedLabelsCheck(this.axis.scale.inverted);
			var lastX:Number = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
			
			var isT1Overlap:Boolean; 
			if (!this.is90C)
				isT1Overlap = this.viewPort.isFirstTypeLabelsOverlap(this.maxNonRotatedLabelSize, this.absTan);
			for (var i:uint = 0;i<this.axis.scale.majorIntervalsCount;i++) {
				var categoryIndex:Number = this.axis.scale.getMajorIntervalValue(i);
				if (this.isDynamicText)
					info = BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex+.5).info;
				var pixPos:Number = this.axis.scale.localTransform(categoryIndex+.5);
				this.viewPort.setLabelPosition(this, info, pos, pixPos);
				
				if (checkLabels) {
					if (this.viewPort.isLabelsOverlaped(isInvertedLabelsCheck, is90C, pos, info, lastX, isT1Overlap, this.absSin, this.absCos))
						continue;
					
					lastX = this.viewPort.getLabelLastPosition(isInvertedLabelsCheck, is90C, pos, info, isT1Overlap, this.absSin, this.absCos);
				}
				this.moveLabel(pos, info);
				
				this.draw(g, pos.x, pos.y, info, 0, 0);
			}
		}
		
		override protected function checkFormat(format:String):String {
			return (this.isTextScale && format.indexOf("{%Value}{numDecimals:2}") != -1) ? format.split("{%Value}{numDecimals:2}").join("{%Value}{enabled:false}") : format;
		}
		
		override public function getLabelBounds(majorIntervalIndex:int):Rectangle {
			if (!this.isDynamicText)
				return super.getLabelBounds(majorIntervalIndex);
			
			var pos:Point = new Point();
			var categoryIndex:int = this.axis.scale.getMajorIntervalValue(majorIntervalIndex)+.5;
			if (!BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex))
				return null;
			var info:TextElementInformation = BaseCategorizedAxis(this.axis).getCategoryByIndex(categoryIndex).info;
			this.viewPort.setLabelPosition(this, info, pos, this.axis.scale.localTransform(categoryIndex));
			this.moveLabel(pos, info);
			
			info.rotatedBounds.x = pos.x;
			info.rotatedBounds.y = pos.y;
			return info.rotatedBounds;
		}
	}
}