package com.anychart.axesPlot.templates {
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.utils.XMLUtils;
	
	public class AxesPlotTemplatesManager extends SeriesPlotTemplatesManager {
		
		override protected function mergeChartNode(template:XML, chart:XML, res:XML):void {
			super.mergeChartNode(template, chart, res);
			var templateExtraAxes:XML = null;
			var chartExtraAxes:XML = null;
			if (template.chart_settings[0] != null && template.chart_settings[0].axes[0] != null)
				templateExtraAxes = template.chart_settings[0].axes[0].extra[0];
			if (chart.chart_settings[0] != null && chart.chart_settings[0].axes[0] != null)
				chartExtraAxes = chart.chart_settings[0].axes[0].extra[0];
			var extraAxes:XML = XMLUtils.mergeNamedChildren(templateExtraAxes, chartExtraAxes);
			if (extraAxes != null)
				res.chart_settings[0].axes[0].extra[0] = extraAxes;
		}
		
		override protected function applyDefaults(res:XML, defaults:XML):void {
			super.applyDefaults(res, defaults);
			if (res == null || defaults == null) return;
			
			var defaultAxis:XML = defaults.axis[0];
			if (defaultAxis != null) { 
				if (res.chart_settings[0] != null && res.chart_settings[0].axes[0] != null) {
					var axes:XML = res.chart_settings[0].axes[0];
					if (axes.x_axis[0] != null)
						axes.x_axis[0] = XMLUtils.merge(defaultAxis, axes.x_axis[0],"x_axis");
					if (axes.y_axis[0] != null)
						axes.y_axis[0] = XMLUtils.merge(defaultAxis, axes.y_axis[0],"y_axis");
					if (axes.extra[0] != null) {
						var extraAxes:XMLList = axes.extra[0].children();
						var axesCnt:int = extraAxes.length();
						for (var i:int = 0;i<axesCnt;i++) {
							extraAxes[i] = XMLUtils.merge(defaultAxis, extraAxes[i], extraAxes[i].name().toString());
							if (extraAxes[i].@name != undefined)
								extraAxes[i].@name = String(extraAxes[i].@name).toLowerCase();
						}
					}
				}
			}
		}
		
	}
}