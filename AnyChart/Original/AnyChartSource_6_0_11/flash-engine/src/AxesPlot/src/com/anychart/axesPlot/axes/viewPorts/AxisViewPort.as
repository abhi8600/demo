package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class AxisViewPort {
		
		protected var axis:Axis;
		protected var bounds:Rectangle;
		protected var visibleBounds:Rectangle;
		
		protected function get xOffset():Number { return 0; }
		protected function get yOffset():Number { return 0; }
		
		public function AxisViewPort(axis:Axis) {
			this.axis = axis;
		}
		
		public function setScaleBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			this.bounds = new Rectangle(0,0,totalBounds.width,totalBounds.height);
			this.visibleBounds = new Rectangle(0,0,visibleBounds.width,visibleBounds.height);
		}
		
		public function drawMarkerLine(g:Graphics, startValue:Number, endValue:Number, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean = true, invertedDrawing:Boolean = false):void {
			var startPt:Point = new Point();
			var endPt:Point = new Point();
			
			IAxisViewPort(this).setStartValue(startValue, startPt);
			IAxisViewPort(this).setEndValue(endValue, endPt);
			
			if (invertedDrawing) {
				var tmpP:Point = startPt;
				startPt = endPt;
				endPt = tmpP;
				tmpP = null;
			}
			if (!isDashed) {
				if (moveTo)
					g.moveTo(startPt.x, startPt.y);
				else
					g.lineTo(startPt.x, startPt.y);
					
				g.lineTo(endPt.x, endPt.y);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, startPt.x, startPt.y, endPt.x, endPt.y);
			}
		}
		
		public function getRangeMarkerBounds(startMinValue:Number, endMinValue:Number, startMaxValue:Number, endMaxValue:Number):Rectangle {
			var res:Rectangle = new Rectangle();
			
			var startMinPt:Point = new Point();
			var endMinPt:Point = new Point();
			var startMaxPt:Point = new Point();
			var endMaxPt:Point = new Point();
			
			IAxisViewPort(this).setStartValue(startMinValue, startMinPt);
			IAxisViewPort(this).setStartValue(startMaxValue, startMaxPt);
			IAxisViewPort(this).setEndValue(endMinValue, endMinPt);
			IAxisViewPort(this).setEndValue(endMaxValue, endMaxPt);
			
			res.x = Math.min(startMinPt.x, startMaxPt.x, endMinPt.x, endMaxPt.x);
			res.y = Math.min(startMinPt.y, startMaxPt.y, endMinPt.y, endMaxPt.y);
			res.width = Math.max(startMinPt.x, startMaxPt.x, endMinPt.x, endMaxPt.x) - res.x;
			res.height = Math.max(startMinPt.y, startMaxPt.y, endMinPt.y, endMaxPt.y) - res.y;
			
			return res;
		}
		
		protected function drawComplexLine(g:Graphics, pt1:Point, pt2:Point, pt3:Point, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean, invertedDrawing:Boolean):void {
			if (invertedDrawing) {
				if (!isDashed) {
					if (moveTo) g.moveTo(pt3.x, pt3.y);
					else		g.lineTo(pt3.x, pt3.y);
					g.lineTo(pt2.x, pt2.y);
					g.lineTo(pt1.x, pt1.y);
				}else {
					DrawingUtils.drawDashedLine(g, dashOff, dashOn, pt3.x, pt3.y, pt2.x, pt2.y);
					DrawingUtils.drawDashedLine(g, dashOff, dashOn, pt2.x, pt2.y, pt1.x, pt1.y);
				}
			}else {
				if (!isDashed) {
					if (moveTo) g.moveTo(pt1.x, pt1.y);
					else		g.lineTo(pt1.x, pt1.y);
					g.lineTo(pt2.x, pt2.y);
					g.lineTo(pt3.x, pt3.y);
				}else {
					DrawingUtils.drawDashedLine(g, dashOff, dashOn, pt1.x, pt1.y, pt2.x, pt2.y);
					DrawingUtils.drawDashedLine(g, dashOff, dashOn, pt2.x, pt2.y, pt3.x, pt3.y);
				}
			}
		}
		
		protected function offsetInsideMarkerLabel(pos:Point):void {
			pos.x += this.xOffset;
			pos.y += this.yOffset;
		}
		
		public function getScrollBarPixPosition(scrollBarPosition:uint, offset:Number, size:Number):Point { return new Point(this.bounds.left,this.bounds.top); }
		public function isInverted(inverted:Boolean):Boolean { return inverted; }
		
		public function checkPix(value:Number):Number { return value; }
	}
}