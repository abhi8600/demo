package com.anychart.axesPlot.scales.dateTime {
	internal final class DateTimeRange {
		private static const MULTIPLIER:Number = 24*60*60*1000;
		
		//range in day
		private static const YEAR_YEAR:Number = 1825*MULTIPLIER;
		private static const YEAR_MONTH:Number = 365*MULTIPLIER;
		private static const MONTH_MONTH:Number = 90*MULTIPLIER;
		private static const DAY_DAY:Number = 10*MULTIPLIER;
		private static const DAY_HOUR:Number = 3*MULTIPLIER;
		private static const HOUR_HOUR:Number = 0.4167*MULTIPLIER;
		private static const HOUR_MINUTE:Number = 0.125*MULTIPLIER;
		private static const MINUTE_MINUTE:Number = 6.94e-3*MULTIPLIER;
		private static const MINUTE_SECOND:Number = 2.083e-3*MULTIPLIER;
		
		public static function calculateIntervals(scale:DateTimeScale, targetSteps:uint):Number {
			
			if (scale.axis.zoomManager)
				targetSteps /= scale.axis.zoomManager.valueMult;
			
			var range:Number = scale.maximum - scale.minimum;
			
			var _instance:DateTimeRange = new DateTimeRange();
			
			var step:Number;
			 
			if (range > YEAR_YEAR)
				step = _instance.calcYearYear(scale, range, targetSteps);
			else if (range > YEAR_MONTH)
				step = _instance.calcYearMonth(scale, range, targetSteps);
			else if (range > MONTH_MONTH)
				step = _instance.calcMonthMonth(scale, range, targetSteps);
			else if (range > DAY_DAY)
				step = _instance.calcDayDay(scale, range, targetSteps);
			else if (range > DAY_HOUR)
				step = _instance.calcDayHour(scale, range, targetSteps);
			else if (range > HOUR_HOUR)
				step = _instance.calcHourHour(scale, range, targetSteps);
			else if (range > HOUR_MINUTE)
				step = _instance.calcHourMinute(scale, range, targetSteps);
			else if (range > MINUTE_MINUTE)
				step = _instance.calcMinuteMinute(scale, range, targetSteps);
			else if (range > MINUTE_SECOND)
				step = _instance.calcMinuteSecond(scale, range, targetSteps);
			else
				step = _instance.calcSecondSecond(scale, range, targetSteps);
				
			_instance = null;
			
			return step;
		}
		
		//-------------------------------------------------------------------------------
		//					UTILS
		//-------------------------------------------------------------------------------
		
		private function getYearStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.YEAR;
			tempStep = Math.ceil(tempStep/(365*MULTIPLIER));
			return tempStep;
		}
		
		private function getMonthStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.MONTH;
			tempStep = Math.ceil(tempStep/(30*MULTIPLIER));
			return tempStep;
		}
		
		private function getDayStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.DAY;
			tempStep = Math.ceil(tempStep/MULTIPLIER);
			return tempStep;
		}
		
		private function getHourStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.HOUR;
			tempStep = Math.ceil((tempStep)/MULTIPLIER);
			return tempStep;
		}
		
		private function getMinuteStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.MINUTE;
			tempStep = Math.ceil((tempStep*60*24)/MULTIPLIER);
			return tempStep;
		}
		
		private function getSecondStep(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = range/targetSteps;
			scale.majorIntervalUnit = DateTimeUnit.SECOND;
			tempStep = Math.ceil((tempStep*60*60*24)/MULTIPLIER);
			return tempStep;
		}
		
		private function boundMinuteOrSecondStep(step:Number):Number {
			if (step > 15)
				return 30;
			if (step > 5)
				return 15;
			if (step > 1)
				return 5;
			else
				return 1;
		}
		
		//-------------------------------------------------------------------------------
		//					CALCULATION
		//-------------------------------------------------------------------------------
		
		private function calcYearYear(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getYearStep(scale, range, targetSteps);
			
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.YEAR;
				scale.minorInterval = (tempStep == 1) ? .25 : scale.calcStepSize(tempStep, 7);
			}
			
			return tempStep;
		}
		
		private function calcYearMonth(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getYearStep(scale, range, targetSteps);
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.MONTH;
				scale.minorInterval = Math.ceil(range/(targetSteps*3)/(30*MULTIPLIER));
				if (scale.minorInterval > 6)
					scale.minorInterval = 12;
				else if (scale.minorInterval > 3)
					scale.minorInterval = 6;
			}
			
			return tempStep;
		}
		
		private function calcMonthMonth(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getMonthStep(scale, range, targetSteps);
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.DAY;
				scale.minorInterval = tempStep * (30/4);
			}
			
			return tempStep;
		}
		
		private function calcDayDay(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getDayStep(scale, range, targetSteps);
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.DAY;
				scale.minorInterval = tempStep/4;
				if (scale.minorInterval > 12)
					scale.minorInterval = 12;
				else if (scale.minorInterval > 6)
					scale.minorInterval = 6;
				else if (scale.minorInterval > 3)
					scale.minorInterval = 3;
				else if (scale.minorInterval > 2)
					scale.minorInterval = 2;
				else
					scale.minorInterval = 1;
			}
			return tempStep;
		}
		
		private function calcDayHour(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getDayStep(scale, range, targetSteps);
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.HOUR;
				scale.minorInterval = Math.ceil(range/(targetSteps*3)*24);
				if (scale.minorInterval > 6)
					scale.minorInterval = 12;
				else if (scale.minorInterval > 3)
					scale.minorInterval = 6;
				else
					scale.minorInterval = 1;
			}
			return tempStep;
		}
		
		private function calcHourHour(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getHourStep(scale, range, targetSteps);
			if (tempStep > 12)
				tempStep = 24;
			else if (tempStep > 6)
				tempStep = 12;
			else if (tempStep > 2)
				tempStep = 6;
			else if (tempStep > 1)
				tempStep = 2;
			else
				tempStep = 1;
			
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.HOUR;
				if (tempStep <= 1)
					scale.minorInterval = .25;
				else if (tempStep <= 6)
					scale.minorInterval = 1;
				else if (tempStep <= 12)
					scale.minorInterval = 2;
				else
					scale.minorInterval = 4;
			}
			
			return tempStep;
		}
		
		private function calcHourMinute(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getHourStep(scale, range, targetSteps);
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.MINUTE;
				scale.minorInterval = Math.ceil(range/(targetSteps*3)*60);
				scale.minorInterval = this.boundMinuteOrSecondStep(scale.minorInterval);
			}
			return tempStep;
		}
		
		private function calcMinuteMinute(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getMinuteStep(scale, range, targetSteps);
			tempStep = this.boundMinuteOrSecondStep(tempStep);
			
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.MINUTE;
				if (tempStep <= 1)
					scale.minorInterval = .25;
				else if (tempStep <= 5)
					scale.minorInterval = 1;
				else
					scale.minorInterval = 5;
			}
			
			return tempStep;
		}
		
		private function calcMinuteSecond(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getMinuteStep(scale, range, targetSteps);
			
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.SECOND;
				scale.minorInterval = Math.ceil(range/(targetSteps*3)*(60*60*24));
				scale.minorInterval = this.boundMinuteOrSecondStep(scale.minorInterval);
			}
				
			return tempStep;
		}
		
		private function calcSecondSecond(scale:DateTimeScale, range:Number, targetSteps:int):Number {
			var tempStep:Number = this.getSecondStep(scale, range, targetSteps);
			tempStep = this.boundMinuteOrSecondStep(tempStep);
			
			if (scale.isMinorIntervalAuto) {
				scale.minorIntervalUnit = DateTimeUnit.SECOND;
				if (tempStep <= 1)
					scale.minorInterval = .25;
				else if (tempStep <= 5)
					scale.minorInterval = 1;
				else
					scale.minorInterval = 5;
			}

			return tempStep;
		}
		
	}
}