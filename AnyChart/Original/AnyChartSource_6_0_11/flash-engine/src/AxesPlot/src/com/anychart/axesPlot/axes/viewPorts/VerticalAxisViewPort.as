package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class VerticalAxisViewPort extends AxisViewPort {
		
		public function VerticalAxisViewPort(axis:Axis) {
			super(axis);
		}
		
		public function getInvertedAngle(angle:Number, baseScaleInverted:Boolean):Number {
			return baseScaleInverted ? (90 - angle) : (angle - 90);
		}
		
		public function getNormalAngle(angle:Number, baseScaleInverted:Boolean):Number {
			return baseScaleInverted ? (angle - 270) : (270 - angle);
		}
		
		public function setTitleRotation(title:AxisTitle):void {
			title.rotation += 90;
		}
		
		
		public function setScaleValue(pixValue:Number, point:Point):void {
			point.y = this.axis.scale.pixelRangeMaximum - pixValue;
		}
		
		public function setScaleCrossingValue(pixValue:Number, point:Point):void {
			point.y = this.axis.scale.pixelRangeMaximum - pixValue;
		}
		
		
		public function getScaleValue(pixPt:Point):Number { 
			return this.axis.scale.pixelRangeMaximum - pixPt.y;
		}
		
		public function getNormalAnchor(anchor:uint):uint {
			switch (anchor) {
				case Anchor.CENTER_BOTTOM: return Anchor.CENTER_LEFT;
				case Anchor.CENTER_LEFT: return Anchor.CENTER_TOP;
				case Anchor.CENTER_RIGHT: return Anchor.CENTER_BOTTOM;
				case Anchor.CENTER_TOP: return Anchor.CENTER_RIGHT;
				case Anchor.LEFT_BOTTOM: return Anchor.LEFT_TOP;
				case Anchor.LEFT_TOP: return Anchor.RIGHT_TOP;
				case Anchor.RIGHT_BOTTOM: return Anchor.LEFT_BOTTOM;
				case Anchor.RIGHT_TOP: return Anchor.RIGHT_BOTTOM;
				default: return anchor;
			}
		}
		
		public function getNormalHAlign(hAlign:uint, vAlign:uint):uint {
			switch (vAlign) {
				case VerticalAlign.TOP: return HorizontalAlign.RIGHT;
				case VerticalAlign.BOTTOM: return HorizontalAlign.LEFT;
				default: return VerticalAlign.CENTER;
			}
		}
		
		public function getNormalVAlign(hAlign:uint, vAlign:uint):uint {
			switch (hAlign) {
				case HorizontalAlign.LEFT: return VerticalAlign.TOP;
				case HorizontalAlign.RIGHT: return VerticalAlign.BOTTOM;
				default: return HorizontalAlign.CENTER;
			}
		}
		
		public function getInvertedAnchor(anchor:uint):uint {
			switch (anchor) {
				case Anchor.CENTER_BOTTOM: return Anchor.CENTER_RIGHT;
				case Anchor.CENTER_LEFT: return Anchor.CENTER_TOP;
				case Anchor.CENTER_RIGHT: return Anchor.CENTER_BOTTOM;
				case Anchor.CENTER_TOP: return Anchor.CENTER_LEFT;
				case Anchor.LEFT_BOTTOM: return Anchor.RIGHT_TOP;
				case Anchor.RIGHT_TOP: return Anchor.LEFT_BOTTOM;
				default: return anchor;
			}
		}
		
		public function getInvertedHAlign(hAlign:uint, vAlign:uint):uint {
			switch (vAlign) {
				case VerticalAlign.TOP: return HorizontalAlign.LEFT;
				case VerticalAlign.BOTTOM: return HorizontalAlign.RIGHT;
				default: return HorizontalAlign.CENTER;
			}
		}
		
		public function getInvertedVAlign(hAlign:uint, vAlign:uint):uint {
			return this.getNormalVAlign(hAlign, vAlign);
		}
		
		override public function setScaleBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			super.setScaleBounds(totalBounds, visibleBounds);
			this.axis.scale.setPixelRange(0, totalBounds.height);
		}
		
		public function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {
			var y:Number = this.axis.scale.pixelRangeMaximum - axisValue;
			if (!dashed) { 
				g.moveTo(0,y);
				g.lineTo(this.bounds.width, y);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, 0, y, this.bounds.width, y);
			}
		}
		
		public function setStartValue(value:Number, pt:Point):void {
			pt.x = 0;
			pt.y = this.axis.scale.pixelRangeMaximum - value;
		}
		
		public function setEndValue(value:Number, pt:Point):void {
			pt.x = this.bounds.width;
			pt.y = this.axis.scale.pixelRangeMaximum - value;
		}
		
		public function setPerpendicularLineBounds(lineThickness:Number, axisValue:Number, bounds:Rectangle):void {
			bounds.y = this.axis.scale.pixelRangeMaximum - axisValue - lineThickness/2;
			bounds.height = lineThickness;
			bounds.x = 0;
			bounds.width = this.bounds.width;
		}
		
		public function setPerpendicularRectangle(axisStartValue:Number, axisEndValue:Number, bounds:Rectangle):void {
			axisStartValue = this.axis.scale.pixelRangeMaximum - axisStartValue;
			axisEndValue = this.axis.scale.pixelRangeMaximum - axisEndValue;
			bounds.y = Math.min(axisStartValue, axisEndValue);
			bounds.height = Math.max(axisStartValue, axisEndValue) - bounds.y;
			bounds.x = 0;
			bounds.width = this.bounds.width;
		}
		
		public function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void {
			g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
		}
		
		public function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void {
			bounds.x = this.xOffset;
			bounds.y = start + this.yOffset;
			bounds.height = end-start;
			bounds.width = lineThickness;
		}
		
		protected final function setTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void {
			bounds.y = axisValue - lineThickness/2;
			bounds.height = lineThickness;
			bounds.width = size; 
		}
		
		public function getTextSpace(bounds:Rectangle):Number {
			return bounds.width;
		}
		
		public function setTitlePosition(title:AxisTitle, pos:Point):void {
			switch (title.align) {
				case AbstractAlign.NEAR:
					pos.y = this.visibleBounds.bottom - title.info.rotatedBounds.height;
					break;
				case AbstractAlign.CENTER:
					pos.y = this.visibleBounds.top + (this.visibleBounds.height - title.info.rotatedBounds.height)/2;
					break;
				case AbstractAlign.FAR:
					pos.y = this.visibleBounds.top;
					break;
			}
			pos.y += this.yOffset;
			pos.x = this.xOffset;
		}
		
		public function getMaxRotatedLabelsCount(maxNonRotatedLabelSize:Rectangle,
											     maxLabelSize:Rectangle, 
											     rotation:Number,
											     absSin:Number,
											     absCos:Number,
											     absTan:Number):int {
			var w:Number = this.bounds.height;
			var maxLabels:int;
			if (rotation % 90 != 0) {
				var dl:Number = maxNonRotatedLabelSize.width/absTan;
				maxLabels = (dl < maxNonRotatedLabelSize.height) ? (Number((w*absSin)/maxNonRotatedLabelSize.width)) :
																  (Number((w*absCos)/maxNonRotatedLabelSize.height));
			}else if (rotation % 180 != 0) {
				maxLabels = w/maxNonRotatedLabelSize.width;
			}else {
				maxLabels = w/maxLabelSize.height;
			}
			return maxLabels;
		}
		
		public function getMaxLabelsCount(size:Rectangle):int {
	    	return this.bounds.height/size.height;
	    }
		
		public function isInvertedLabelsCheck(isScaleInverted:Boolean):Boolean {
	    	return !isScaleInverted;
	    }
	    
	    public function isFirstTypeLabelsOverlap(size:Rectangle, absTan:Number):Boolean {
	    	return (size.width/absTan) < size.height;;
	    }
	    
	    public function getLabelXOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
	    	return 0;
	    }
	    
		public function getLabelXOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			return 0;
		}
		
		public function setMarkerLabelPosition(pixPos:Number, 
											   padding:Number, 
											   offset:Number,
											   bounds:Rectangle,
											   pos:Point):void {
			pos.y = this.bounds.bottom - pixPos - bounds.height/2 + this.yOffset;
			pos.x = this.xOffset;
		}
		
		public function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void {
			pos.y = this.bounds.bottom - pixPos + this.yOffset;
			pos.x = this.xOffset;
		}
		
		public function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void {
			pos.y = this.bounds.bottom - pixPos - info.rotatedBounds.height/2 + this.yOffset;
			pos.x = this.xOffset;
		}
		
		public function isLabelsOverlaped(isInvertedCheck:Boolean,
										   is90C:Boolean, 
								   		   pos:Point, 
										   info:TextElementInformation, 
										   lastPosition:Number,
										   isT1Overlap:Boolean, 
										   absSin:Number, 
										   absCos:Number):Boolean {
			
			if (isInvertedCheck) {
				if (is90C) {
					return (pos.y + info.rotatedBounds.height > lastPosition);
				}else {
					var dw:Number = isT1Overlap ? (info.nonRotatedBounds.width/absSin) : (info.nonRotatedBounds.height/absCos);
					return (pos.y + dw > lastPosition);
				}
			}
			return (pos.y < lastPosition);
		}
		
		public function getLabelLastPosition(isInvertedCheck:Boolean,
										   	  is90C:Boolean, 
										   	  pos:Point, 
										  	  info:TextElementInformation, 
										  	  isT1Overlap:Boolean, 
										   	  absSin:Number, 
										   	  absCos:Number):Number {
			if (isInvertedCheck)
				return pos.y;
			return pos.y + (is90C ? (info.rotatedBounds.height) :
									 (isT1Overlap ? (info.nonRotatedBounds.width/absSin) : (info.nonRotatedBounds.height/absCos))
							);
 		}
 		
 		public function isStagerLabelsOverlaped(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation, lastPosition:Number):Boolean {
 			if (isInvertedCheck) 
				return (pos.y + info.rotatedBounds.height > lastPosition);
			return (pos.y < lastPosition);
 		}
 		
		public function getStagerLabelLastPosition(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation):Number {
			if (isInvertedCheck)
				return pos.y;
			return pos.y + info.rotatedBounds.height;
		}
		
		public function applyLabelsOffsets(bounds:Rectangle, firstLabel:Rectangle, lastLabel:Rectangle):void {
			var topLabel:Rectangle = lastLabel;
			var bottomLabel:Rectangle = firstLabel;
			if (this.axis.scale.inverted) {
				topLabel = firstLabel;
				bottomLabel = lastLabel;
			}
			if (topLabel.y < 0) {
				bounds.y += -topLabel.y;
				bounds.height += topLabel.y; 
			}
			if (bottomLabel.y + bottomLabel.height > this.bounds.y + this.bounds.height) {
				bounds.height -= (bottomLabel.y + bottomLabel.height) - this.bounds.height - this.bounds.y;
			}
		}
		
		public function getSpace(bounds:Rectangle):Number {
			return bounds.width;
		}
		
		public function applyZoomFactor(bounds:Rectangle, factor:Number):void {
			bounds.height *= factor;
		}
		
		public function getVisibleStart():Number { return this.visibleBounds.top; }
		public function getVisibleEnd():Number { return this.visibleBounds.bottom; }
		override public function isInverted(inverted:Boolean):Boolean { return !inverted; }
		
		public function getMouseCoord(e:MouseEvent):Number { return e.stageY; }
		public function getScrollRectStart(rect:Rectangle):Number { return rect.y; }
		override public function checkPix(value:Number):Number { return this.bounds.height - value; }
	}
}