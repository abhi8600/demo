package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	internal final class LeftOppositeAxisViewPort3D extends LeftAxisViewPort3D {
		
		override protected function get xOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixXAspect(); }
		override protected function get yOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixYAspect(); }
		
		public function LeftOppositeAxisViewPort3D(axis:Axis) {
			super(axis);
		}
		
		override public function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {
			var y:Number = this.axis.scale.pixelRangeMaximum - axisValue;
			
			var frontRightX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(this.bounds.width);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(y);
			 
			if (!dashed) {
				g.moveTo(0, y);
				g.lineTo(this.bounds.width, y);
				g.lineTo(frontRightX, frontY);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, 0, y, this.bounds.width, y);
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, this.bounds.width, y, frontRightX, frontY);
			}
		}
		
		override public function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void {
			if (this.axis.isOpposite)
				return super.drawPerpendicularRectangle(g, bounds);
				
			var frontRightX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(bounds.x+bounds.width);
			var frontRightTopY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(bounds.top);
			var frontRightBottomY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(bounds.bottom);
			
			g.moveTo(bounds.x, bounds.y);
			g.lineTo(bounds.right, bounds.y);
			g.lineTo(frontRightX, frontRightTopY);
			g.lineTo(frontRightX, frontRightBottomY);
			g.lineTo(bounds.x+bounds.width, bounds.bottom);
			g.lineTo(bounds.x, bounds.bottom);
			g.lineTo(bounds.x, bounds.y);
		}
		
		override public function drawMarkerLine(g:Graphics, startValue:Number, endValue:Number, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean = true, invertedDrawing:Boolean = false):void {
			
			var y:Number = this.axis.scale.pixelRangeMaximum - endValue;
			var frontRightX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(bounds.x+bounds.width);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(y);
			
			var startPt:Point = new Point();
			var endPt:Point = new Point();
			
			this.setStartValue(startValue, startPt);
			this.setEndValue(endValue, endPt);
			
			var pt1:Point = startPt;
			var pt2:Point = endPt;
			var pt3:Point = new Point(frontRightX, frontY);
			this.drawComplexLine(g, pt1, pt2, pt3, isDashed, dashOn, dashOff, moveTo, invertedDrawing);
		}
		
		override protected function offsetInsideMarkerLabel(pos:Point):void {}
	}
}