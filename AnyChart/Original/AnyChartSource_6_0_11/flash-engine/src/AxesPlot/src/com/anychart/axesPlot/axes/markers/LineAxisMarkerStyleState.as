package com.anychart.axesPlot.axes.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	import com.anychart.visual.stroke.Stroke;
	
	internal final class LineAxisMarkerStyleState extends AxisMarkerStyleState {
		
		public var stroke:Stroke;
		
		public function LineAxisMarkerStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.stroke = new Stroke();
			this.stroke.deserialize(data, this.style);
			return data;
		}
	}
}