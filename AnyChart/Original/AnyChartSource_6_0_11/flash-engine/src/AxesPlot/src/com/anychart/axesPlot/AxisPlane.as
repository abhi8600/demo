package com.anychart.axesPlot {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class AxisPlane implements ISerializableRes {
		private var fill:Fill;
		private var border:Stroke;
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (SerializerBase.isEnabled(data.fill[0])) {
				this.fill = new Fill();
				this.fill.deserialize(data.fill[0], resources);
			}
			
			if (SerializerBase.isEnabled(data.border[0])) {
				this.border = new Stroke();
				this.border.deserialize(data.border[0]);
			}
		}
		
		public function draw(g:Graphics, points:Array):void {
			if (!this.fill && !this.border) return;
			var bounds:Rectangle = new Rectangle();
			var lt:Point = new Point(Number.MAX_VALUE, Number.MAX_VALUE);
			var rb:Point = new Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
			for each (var pt:Point in points) {
				lt.x = Math.min(pt.x, lt.x);
				lt.y = Math.min(pt.y, lt.y);
				rb.x = Math.max(pt.x, rb.x);
				rb.y = Math.max(pt.y, rb.y);
			}
			bounds.x = lt.x;
			bounds.y = lt.y;
			bounds.width = rb.x - lt.x;
			bounds.height = rb.y - lt.y;
			
			if (this.fill)
				this.fill.begin(g, bounds);
			if (this.border)
				this.border.apply(g, bounds);
			
			g.moveTo(points[0].x, points[0].y);
			for (var i:int = 1;i<points.length;i++) {
				g.lineTo(points[i].x,points[i].y);
			}
			g.lineStyle();
			g.lineTo(points[0].x, points[0].y);
			
			g.endFill();
			g.lineStyle();
		}
	}
}