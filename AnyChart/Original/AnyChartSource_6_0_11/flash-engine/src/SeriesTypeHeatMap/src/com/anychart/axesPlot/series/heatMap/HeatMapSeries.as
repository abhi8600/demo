package com.anychart.axesPlot.series.heatMap {
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class HeatMapSeries extends SingleValueSeries {
		
		public function HeatMapSeries() {
			super();
			this.isClusterizableOnCategorizedAxis = false;
		}
		
		override public function createPoint():BasePoint {
			return new HeatMapPoint();
		}
		
	}
}