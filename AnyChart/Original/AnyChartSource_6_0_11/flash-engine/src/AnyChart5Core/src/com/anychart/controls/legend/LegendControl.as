package com.anychart.controls.legend {
	import com.anychart.controls.ControlWithTitle;
	import com.anychart.controls.visual.SeparatorLine;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.uiControls.scrollBar.ScrollBarEvent;
	import com.anychart.uiControls.scrollBar.ScrollBarVertical;
	import com.anychart.visual.layout.HorizontalAlign;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	//TODO: Data - Thresholds, Categories, Map Series
	//TODO: Icon drawing - ALL MAP SERIES!!
	//TODO: Scrollbar
	//TODO: Default template
	//TODO: item states && elements highlight
	//TODO: elements highlight
	internal class LegendControl extends ControlWithTitle implements ILegendItemsContainer {
		
		protected var columnsPadding:Number;
		protected var rowsPadding:Number;
		protected var elementsAlign:uint;
		protected var realElementsAlign:uint;
		
		private var format:String;
		
		protected var columnsSeparator:SeparatorLine;
		protected var rowsSeparator:SeparatorLine;
		
		protected var itemsContainer:Sprite;
		
		private var scrollBar:ScrollBarVertical;
		protected var scrollBarBounds:Rectangle;
		protected var scrollBarSpace:Number;
		
		protected var columnSpace:Number;
		protected var rowSpace:Number;
		
		protected var realContentHeight:Number;
		
		private static const SCROLL_PADDING:Number = 4;
		
		public function LegendControl() {
			super();
			this.itemsContainer = new Sprite();
			this.columnsPadding = 2;
			this.rowsPadding = 2;
			this.elementsAlign = HorizontalAlign.LEFT;
			this.format = "{%Icon} {%Name}";
			
			this.columnSpace = this.columnsPadding;
			this.rowSpace = this.rowsPadding;
			
			this.scrollBarBounds = new Rectangle(0,0,0,0);
		}
		
		override public function destroy():void {
			super.destroy();
			if (this.scrollBar) {
				this.scrollBar.removeEventListener(ScrollBarEvent.CHANGE, this.scrollBarChange);
				this.scrollBar.destroy();
				if (this.scrollBarContainer && this.scrollBarContainer.parent)
					this.scrollBarContainer.parent.removeChild(this.scrollBar);
			}
			this.scrollBar = null;
			this.scrollBarContainer = null;
			
			if (this.visualItems) {
				for each (var item:LegendItem in this.visualItems)
					item.destroy();
				this.visualItems = null;
			}
			if (this.itemsContainer) {
				while (this.itemsContainer.numChildren > 0)
					this.itemsContainer.removeChildAt(0);
			}
			this.itemsContainer = null;
		}
		
		protected var legendDataFilter:XMLList;
		
		override public function deserialize(baseData:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(baseData, styles, resources);
			var data:XML = baseData.copy();
			if (data.@width != undefined) delete data.@width;
			if (data.@height != undefined) delete data.@height; 
			
			if (data.@columns_padding != undefined) this.columnsPadding = SerializerBase.getNumber(data.@columns_padding);
			if (data.@rows_padding != undefined) this.rowsPadding = SerializerBase.getNumber(data.@rows_padding);
			if (data.@elements_align != undefined) this.elementsAlign = SerializerBase.getHorizontalAlign(data.@elements_align);
			if (data.format[0] != null) this.format = SerializerBase.getCDATAString(data.format[0]);
			if (SerializerBase.isEnabled(data.columns_separator[0])) {
				this.columnsSeparator = new SeparatorLine();
				this.columnsSeparator.deserialize(data.columns_separator[0]);
			}
			if (SerializerBase.isEnabled(data.rows_separator[0])) {
				this.rowsSeparator = new SeparatorLine();
				this.rowsSeparator.deserialize(data.rows_separator[0]);
			}
			this.createDataFilter(data, resources);
			
			this.columnSpace = this.columnsPadding;
			this.rowSpace = this.rowsPadding;
			
			if (this.columnsSeparator != null)
				this.columnSpace += this.columnsSeparator.padding;
			if (this.rowsSeparator != null)
				this.rowSpace += this.rowsSeparator.padding;
				
			this.realElementsAlign = this.elementsAlign;
			
			if (this.chart == null) return;
			
			if (data.scroll_bar[0] != null) {
				this.scrollBar = new ScrollBarVertical(this.chart.getMainContainer());
				this.scrollBar.deserialize(data.scroll_bar[0], resources);
			}
		}
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.container.addChild(this.itemsContainer);
			this.itemsContainer.scrollRect = new Rectangle();
			
			if (this.scrollBar) {
				this.scrollBar.addEventListener(ScrollBarEvent.CHANGE, this.scrollBarChange);
				this.scrollBarContainer = this.scrollBar.initialize(this.scrollBarBounds);
				this.container.addChild(this.scrollBarContainer);
			}
			
			return this.container;
		}
		
		//------------------------------------------------------------------------
		//					BOUNDS
		//------------------------------------------------------------------------
		
		override public function recalculateAndRedraw():void {
			this.isItemsInitialized = false;
			
			this.itemsContainer.graphics.clear();
			while (this.itemsContainer.numChildren > 0) this.itemsContainer.removeChildAt(0);
			
			this.initializeItems();
			this.updateLayout();
			super.recalculateAndRedraw();
			this.distributeItems();
		}
		
		protected function updateLayout():void {
			
		}
		
		protected var visualItems:Array;
		
		private var isItemsInitialized:Boolean = false;
		protected final function initializeItems():void {
			if (this.isItemsInitialized)
				return;
				
			this.visualItems = [];
			
			this.getFilteredData();
			this.initializeLayoutBase();
			
			this.isItemsInitialized = true;
		}
		
		protected function initializeLayoutBase():void {}
		
		override public function draw():void {
			super.draw();
			this.itemsContainer.x = this.contentBounds.x;
			this.itemsContainer.y = this.contentBounds.y;
			this.distributeItems();
		}
		
		override public function resize():void {
			super.resize();
			this.itemsContainer.x = this.contentBounds.x;
			this.itemsContainer.y = this.contentBounds.y;
			this.itemsContainer.graphics.clear();
			this.distributeItems();
		}
		
		override protected function setContentBounds():void {
			super.setContentBounds();
			this.setScrollRect();
		}
		
		private function setScrollRect():void {
			var rc:Rectangle = this.itemsContainer.scrollRect;
			rc.x = 0;
			rc.y = 0;
			rc.width = this.contentBounds.width;
			rc.height = this.contentBounds.height;
			this.itemsContainer.scrollRect = rc;
			
			this.scrollBarBounds.x = this.contentBounds.x + this.contentBounds.width - (this.scrollBar ? this.scrollBar.scrollBarSize : 0);
			this.scrollBarBounds.y = this.contentBounds.y;
			this.scrollBarBounds.height = this.contentBounds.height;
			
			if (this.realContentHeight > this.contentBounds.height && this.scrollBar) {
				this.scrollBar.min = 0;
				this.scrollBar.max = this.realContentHeight;
				this.scrollBar.visibleRange = this.contentBounds.height;
			}
			
			if (this.scrollBar)
				this.scrollBar.resize(this.scrollBarBounds);
		}
		
		private function scrollBarChange(e:ScrollBarEvent):void {
			var rc:Rectangle = this.itemsContainer.scrollRect;
			rc.y = e.value;
			this.itemsContainer.scrollRect = rc;
		}
		
		protected function distributeItems():void {}
		
		//------------------------------------------------------------------------
		//					Scroll Bar
		//------------------------------------------------------------------------
		
		private var scrollBarContainer:DisplayObject;
		protected final function createScrollBar():void {
			this.scrollBarSpace = this.scrollBar? this.scrollBar.scrollBarSize : 0 + SCROLL_PADDING;
			this.scrollBarContainer.visible = true;
		}
		
		protected final function removeScrollBar():void {
			this.scrollBarSpace = 0;
			this.scrollBarContainer.visible = false;
			if (this.scrollBar)
				this.scrollBar.value = 0;
		}
		
		//------------------------------------------------------------------------
		//					ITEMS CREATION
		//------------------------------------------------------------------------
		
		private var groups:Array;
		private var defaultGroup:LegendItemsGroup;
		
		private function createDataFilter(data:XML, resources:ResourcesLoader):void {
			this.groups = [];
			this.legendDataFilter = new XMLList();
			
			this.defaultGroup = new LegendItemsGroup(this.plot);
			this.defaultGroup.deserializeDefault(data, resources);
			
			//add auto item
			if (data.@ignore_auto_item == undefined || !SerializerBase.getBoolean(data.@ignore_auto_item)) {
				this.legendDataFilter += <item source="auto" />;
				this.groups.push(this.defaultGroup);
			}
				
			//parse all items
			if (data.items[0] != null) {
				var itemsCnt:int = data.items[0].item.length();
				for (var i:int = 0;i<itemsCnt;i++) {
					var item:XML = data.items[0].item[i];
					var group:LegendItemsGroup = this.defaultGroup.createCopy();
					group.deserialize(item, resources);
					this.groups.push(group);
					this.legendDataFilter += item;
				}
			}
		}
		
		private var currentGroup:LegendItemsGroup;
		private function getFilteredData():void {
			var itemsCnt:int = this.legendDataFilter.length();
			for (var i:int = 0;i<itemsCnt;i++) {
				this.currentGroup = this.groups[i];
				this.currentGroup.plot = this.plot;
				this.plot.getLegendData(this.legendDataFilter[i], this);
			}
			this.defaultGroup = null;
			this.currentGroup = null;
			//this.groups = null;
		}
		
		public final function addItem(item:ILegendItem):void {
			if (item.tag == null)
				item.tag = this.plot;
			var visualItem:LegendItem = this.currentGroup.createItem(item);
			
			this.visualItems.push(visualItem);
			this.itemsContainer.addChild(visualItem);
		}
	}
}