package com.anychart.dashboard {
	import com.anychart.IResizable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	internal class DashboardElement implements IResizable, ISerializableRes {
		
		protected var margin:Margin;
		protected var bounds:Rectangle;
		protected var container:Sprite;
		
		public var x:Number;
		public var isPercentX:Boolean;
		internal var calculatedX:Number;
		
		public var y:Number;
		public var isPercentY:Boolean;
		internal var calculatedY:Number;
		
		public var width:Number;
		public var isPercentWidth:Boolean;
		internal var calculatedWidth:Number;
		
		public var height:Number;
		public var isPercentHeight:Boolean;
		internal var calculatedHeight:Number;
		
		protected var dashboard:Dashboard;
		
		public function DashboardElement(dashboard:Dashboard) {
			this.dashboard = dashboard;
			this.x = 0;
			this.y = 0;
			this.width = 1;
			this.height = 1;
			this.isPercentX = true;
			this.isPercentY = true;
			this.isPercentWidth = true;
			this.isPercentHeight = true;
			
			this.margin = new Margin();
			this.margin.all = 5;
		}
		
		public function initialize(newBounds:Rectangle):DisplayObject {
			this.container = new Sprite();
			this.setLayout(newBounds);
			return this.container; 
		}
		
		public function draw():void {}
		
		public function resize(newBounds:Rectangle):void {
			this.setLayout(newBounds);
		}
		
		protected function setLayout(newBounds:Rectangle):void {
			this.setPosition(newBounds);
		}
		
		protected final function setPosition(newBounds:Rectangle):void {
			this.bounds = newBounds.clone();
			
			this.bounds.x = this.getX(newBounds);
			this.bounds.y = this.getY(newBounds);
			this.bounds.width = this.getWidth(newBounds);
			this.bounds.height = this.getHeight(newBounds);
			
			this.container.x = this.bounds.x;
			this.container.y = this.bounds.y;
			
			this.bounds.x = 0;
			this.bounds.y = 0;
			
			if (this.margin != null)
				this.margin.applyInside(this.bounds);
		}
		
		private function getX(newBounds:Rectangle):Number {
			if (isNaN(this.calculatedX))
				return newBounds.x + (this.isPercentX ? (newBounds.width*this.x) : this.x);
			return this.calculatedX;
		}
		
		private function getY(newBounds:Rectangle):Number {
			if (isNaN(this.calculatedY))
				return newBounds.y + (this.isPercentY ? (newBounds.height*this.y) : this.y)
			return this.calculatedY;
		}
		
		internal final function getWidth(newBounds:Rectangle):Number {
			if (isNaN(this.calculatedWidth))
				return this.isPercentWidth ? (newBounds.width*this.width) : this.width
			return this.calculatedWidth;
		}
		
		internal final function getHeight(newBounds:Rectangle):Number {
			if (isNaN(this.calculatedHeight))
				return this.isPercentHeight ? (newBounds.height*this.height) : this.height
			return this.calculatedHeight;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.margin[0] != null) {
				this.margin = new Margin();
				this.margin.deserialize(data.margin[0]);
			}
			if (data.@x != undefined) {
				this.isPercentX = SerializerBase.isPercent(data.@x);
				this.x = (this.isPercentX) ? SerializerBase.getPercent(data.@x) : SerializerBase.getNumber(data.@x);
			}
			if (data.@y != undefined) {
				this.isPercentY = SerializerBase.isPercent(data.@y);
				this.y = (this.isPercentY) ? SerializerBase.getPercent(data.@y) : SerializerBase.getNumber(data.@y);
			}
			if (data.@width != undefined) {
				this.isPercentWidth = SerializerBase.isPercent(data.@width);
				this.width = (this.isPercentWidth) ? SerializerBase.getPercent(data.@width) : SerializerBase.getNumber(data.@width);
			}
			if (data.@height != undefined) {
				this.isPercentHeight = SerializerBase.isPercent(data.@height);
				this.height = (this.isPercentHeight) ? SerializerBase.getPercent(data.@height) : SerializerBase.getNumber(data.@height);
			}
		}
	}
}