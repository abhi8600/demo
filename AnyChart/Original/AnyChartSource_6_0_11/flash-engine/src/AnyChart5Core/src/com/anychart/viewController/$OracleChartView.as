package com.anychart.viewController {
	import com.anychart.AnyChart;
	import com.anychart.Chart;
	import com.anychart.dashboard.Dashboard;
	import com.anychart.errors.ErrorMessage;
	import com.anychart.preloader.Preloader;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.utils.XMLUtils;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	
	public class ChartView extends EventDispatcher {
		
		public var container:Sprite;
		private var elementContainer:DisplayObject;
		private var preloaderContainer:DisplayObject;
		
		public var base:AnyChart;
		protected var bounds:Rectangle;
		
		public var resources:ResourcesLoader;
		
		public var element:IViewElement;
		protected var preloader:Preloader;
		
		public var xmlFilePath:String;
		public var xmlDataRaw:String;
		
		protected var status:uint;
		protected var anychartXMLData:XML;
		
		public function ChartView(base:AnyChart) {
			this.bounds = bounds;
			this.base = base;
			
			if (this is AnyChart)
				this.resources = new ResourcesLoader();
			else
				this.resources = base.resources.createCopy();
			this.resources.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.showSecurityError);
			this.resources.addEventListener(IOErrorEvent.IO_ERROR, this.showIOError);
			this.resources.addEventListener(ProgressEvent.PROGRESS, this.progressHandler);
			this.preloader = new Preloader();
		}
		
		public function initialize(bounds:Rectangle):void {
			this.bounds = bounds;
			this.preloaderContainer = this.preloader.initialize(bounds);
			if (this.xmlFilePath != null)
				this.setXMLFile(this.xmlFilePath, null);
			else if (this.xmlDataRaw != null)
				this.setXMLData(this.xmlDataRaw, null);
			else 
				this.showWaitingForData();
		}
		
		//-----------------------------------------------------
		//XML File loading
		//----------------------------------------------------- 
		
		private var replaces:Array;
		
		public final function setXMLFile(path:String, replaces:Array = null):void {
			this.setDefaults();
			this.clear();
			this.resources.stop();
			
			this.xmlFilePath = path;
			this.resources.addText(path, false);
			if (this.resources.isLoaded()) {
				this.setXMLData(ResourcesHashMap.getText(path), replaces);
			}else {
				this.replaces = replaces;
				this.resources.addEventListener(Event.COMPLETE, this.xmlFileLoadHandler);
				this.showPreloader(this.base.messageOnLoadingXML, false);
			}
		}
		
		private function xmlFileLoadHandler(event:Event):void {
			this.preloader.setProgress(1);
			this.resources.removeEventListener(Event.COMPLETE, this.xmlFileLoadHandler);
			this.setXMLData(ResourcesHashMap.getText(this.xmlFilePath), this.replaces);
			this.replaces = null;
		}
		
		//-----------------------------------------------------
		//XML Data converting
		//-----------------------------------------------------
		
		protected final function getXML(rawData:String):XML {
			var data:XML = null;
			try {
				data = new XML(rawData);
			}catch (e:Error) {
				this.showXMLError(e);
			}
			return data;
		}
		
		public final function setXMLData(rawData:String, replaces:Array = null):void {
			this.setDefaults();
			this.clear();
			this.resources.stop();
			
			this.showInitializing();
			
			if (replaces != null) {
				for (var i:int = 0;i<replaces.length;i++) {
					rawData = rawData.split(replaces[i][0]).join(replaces[i][1]);
				}
			}
			
			var data:XML = this.getXML(rawData);
			if (data != null) {
				this.processXML(data);
			}
		}
		
		public final function setXML(data:XML):void {
			this.setDefaults();
			this.clear();
			this.showInitializing();
			this.processXML(data);
		}
		
		protected function processXML(data:XML):XML {
			if (data == null || data.name() == null) {
				this.showNoData();
				return null;
			}
			
			var nodeName:String = data.name().toString();
			var newData:XML;
			
			if (nodeName == "root") {
				nodeName = "anychart";
				
				newData = <anychart>
					<gauges>
						<gauge>
							{convertAnyChart3GaugeXMLData(data.data[0])}
						</gauge>
					</gauges>
				</anychart>
			}else if (nodeName == "anychart" && isAnyChart3GaugeData(data)) {
				var tmpData:XML =
				<anychart>
					<gauges>
						<gauge> 
							{this.convertAnyChart3GaugeXMLData(data.gauges[0].gauge[0].data[0])}
						</gauge>
					</gauges>
				</anychart>;
				delete data.gauges[0].gauge[0].data[0];
				newData = XMLUtils.merge(data, tmpData);
			}else {
				var collectionName:String = nodeName+"s";
				
				if (nodeName != "anychart" && data[collectionName][0] == null) {
					newData = <anychart>
						<{collectionName}>{data}</{collectionName}>
					</anychart>;
				}else {
					newData = data;
				}
			}
			this.anychartXMLData = newData;
			return newData;
			//can load additional information
			//should call generateChart()
		}
		
		private function isAnyChart3GaugeData(data:XML):Boolean {
			return data.gauges[0] != null &&
				   data.gauges[0].gauge[0] != null &&
				   data.gauges[0].gauge[0].@use_anychart3_data_model != undefined &&
				   String(data.gauges[0].gauge[0].@use_anychart3_data_model).toLowerCase() == "true";
		}
		
		private function convertAnyChart3GaugeXMLData(data:XML):XML {
			//REMOVE IT!!!!!!!
			if (data == null) return null;
			var scaleNode:XML = <scale />;
			if (data.deal[0] != null) {
				var oldData:XML = data.deal[0];
				if (oldData.@minimum != undefined) scaleNode.@minimum = oldData.@minimum;
				if (oldData.@maximum != undefined) scaleNode.@maximum = oldData.@maximum;
			}
			
			var axisNode:XML = <axis>{scaleNode}</axis>;
			if (data.scales[0] != null && data.scales[0].scale[0] != null) {
				var oldAxis:XML = data.scales[0].scale[0];
				if (oldAxis.@startAngle != undefined) axisNode.@start_angle = Number(oldAxis.@startAngle)+90;
				if (oldAxis.@endAngle != undefined) {
					var sweep:Number = Number(oldAxis.@endAngle) - Number(oldAxis.@startAngle);
					if (!isNaN(sweep)) {
						axisNode.@sweep_angle = sweep;
					}
				}
				if (oldAxis.@angleStep != undefined) {
					var step:Number = Number(oldAxis.@angleStep);
					var intervalsCount:Number = Number(axisNode.@sweep_angle)/step;
					var interval:Number = (Number(scaleNode.@maximum) - Number(scaleNode.@minimum))/intervalsCount;
					axisNode.scale[0].@major_interval = interval;
				}
			}
			
			if (data.areas[0] != null) {
				axisNode.color_ranges = <color_ranges />;
				var oldAreas:XMLList = data.areas[0].area;
				if (oldAreas.length() > 0) {
					var oldAreasCnt:int = oldAreas.length();
					for (var i:uint = 0;i<oldAreasCnt;i++) {
						var oldAreaNode:XML = oldAreas[i];
						var newAreaNode:XML = <color_range />;
						if (oldAreaNode.@startValue != undefined) newAreaNode.@start = oldAreaNode.@startValue;
						if (oldAreaNode.@endValue != undefined) newAreaNode.@end = oldAreaNode.@endValue;
						
						axisNode.color_ranges[0].color_range += newAreaNode;
					}
				}
			}
			
			var pointerNode:XML;
			if (data != null && data.indicator[0] != null) {
				pointerNode = <pointer />;
				var oldPointer:XML = data.indicator[0];
				if (oldPointer.@value != undefined) pointerNode.@value = oldPointer.@value;
			}
				
			
			var res:XML = <circular>
				{axisNode}
				<pointers>
					{pointerNode}
				</pointers>
			</circular>;
			return res;
		}
		
		protected final function generateChart(data:XML):void {
			if (AnyChart.ENABLE_ERROR_REPORTING) {
				try {
					this.doGenerateChart(data);
				}catch (e:Error) {
					//FATAL ERROR 
					this.showErrorMessage(ErrorMessage.getFatalError(e));
					this.dispatchDrawEvent();
				}
			}else {
				this.doGenerateChart(data);
			}
		}
		
		private function doGenerateChart(data:XML):void {
			this.createElement(data);
			if (this.resources.isLoaded()) {
				this.showElement();
			}else {
				this.resources.addEventListener(Event.COMPLETE, this.resourcesLoadHandler);
				this.showPreloader(this.base.messageOnLoadingResources, true);
			}
		}
		
		//--------------------------------------------------------
		//	Resources
		//--------------------------------------------------------
		
		private function resourcesLoadHandler(event:Event):void {
			this.resources.removeEventListener(Event.COMPLETE, this.resourcesLoadHandler);
			this.preloader.setProgress(1);
			this.removePreloader();
			this.showElement();
		}
		
		//-----------------------------------------------------
		//Resize
		//-----------------------------------------------------
		
		public function resize(newBounds:Rectangle):void {
			this.bounds = newBounds;
			switch (this.status) {
				case ChartViewStatus.ELEMENT:
					this.element.resize(newBounds);
					break;
				case ChartViewStatus.PRELOADER:
					this.preloader.resize(newBounds);
					break;
				case ChartViewStatus.ERROR:
					this.showErrorMessage(this.errorMessage);
					break;
			}
		}
		
		//-----------------------------------------------------
		//CLEAR ALL
		//-----------------------------------------------------
		
		private function clear():void {
			if (this.container != null)
				this.container.graphics.clear();
			this.removeElement();
			this.removePreloader();
			if (this.errorMessageContainer != null) {
				this.errorMessageContainer.parent.removeChild(this.errorMessageContainer);
				this.errorMessageContainer = null;
			}
		}
		
		protected function setDefaults():void {
		}
		
		//-----------------------------------------------------
		//Element
		//-----------------------------------------------------
		
		protected function createElement(data:XML):void {
		}
		
		private function showElement():void {
			if (AnyChart.ENABLE_ERROR_REPORTING) {
				try {
					this.doShowElement();
				}catch (e:Error) {
					this.dispatchRenderEvent();
					this.showErrorMessage(ErrorMessage.getFatalError(e));
				}
			}else {
				this.doShowElement();
			}
			this.dispatchDrawEvent();
		}
		
		private function doShowElement():void {
			this.showInitializing();
			this.status = ChartViewStatus.ELEMENT;
			this.elementContainer = this.element.initialize(this.bounds);
			if (this.element is Dashboard)
				this.element.postInitialize();
			this.dispatchRenderEvent();
			this.element.draw();
			this.removePreloader();
			this.container.addChild(this.elementContainer);
		}
		
		protected function dispatchRenderEvent():void {
		}
		
		protected function dispatchDrawEvent():void {
		}
		
		protected function execDispatchDrawEvent():void {
		}
		
		protected function animationFinish(e:AnimationEvent):void {
			this.execDispatchDrawEvent();
		}
		
		private function removeElement():void {
			if (this.element != null)
				this.element = null;
			if (this.elementContainer != null) {
				if (this.elementContainer is DisplayObjectContainer) {
					while (DisplayObjectContainer(this.elementContainer).numChildren > 0) {
						DisplayObjectContainer(this.elementContainer).removeChildAt(0);
					}
				}
			
				if (this.elementContainer.parent != null)
					this.elementContainer.parent.removeChild(this.elementContainer);
				this.elementContainer = null;
			}
		}
		
		//-----------------------------------------------------
		//Errors
		//-----------------------------------------------------
		
		private function showIOError(event:IOErrorEvent):void {
			this.showErrorMessage(ErrorMessage.IO_ERROR);
			this.dispatchDrawEvent();
		}
		
		public function showSecurityError(event:SecurityErrorEvent):void {
			this.showErrorMessage(ErrorMessage.SECURITY_ERROR, ErrorMessage.SECURITY_ERROR_URL);
		}
		
		public function showFatalError(e:Error):void {
			this.showErrorMessage(ErrorMessage.FATAL_ERROR);
		}
		
		private function showXMLError(error:Error):void {
			this.showErrorMessage(ErrorMessage.getXMLErrorMessage(error));
		}
		
		private var errorMessage:String;
		private var errorMessageContainer:DisplayObject;
		
		private function showErrorMessage(message:String, url:String = null):void {
			this.resources.stop();
			this.clear();
			this.status = ChartViewStatus.ERROR;
			this.errorMessage = message;
			
			if (this.elementContainer != null)
				this.elementContainer.parent.removeChild(this.elementContainer);
			
			this.errorMessageContainer = ErrorMessage.showErrorMessage(this.bounds, message, url);
			this.container.addChild(this.errorMessageContainer);
		}
		
		//-----------------------------------------------------
		//Static messages
		//-----------------------------------------------------
		
		public function setLoading(text:String):void {
			this.clear();
			this.showAnimatedPreloader(text);
		}
		
		private function showWaitingForData():void {
			this.showAnimatedPreloader(this.base.messageOnWaitingForData);
		}
		
		public final function showNoData():void {
			this.showAnimatedPreloader(this.base.messageNoData);
		}
		
		protected final function showInitializing():void {
			this.showAnimatedPreloader(this.base.messageOnInitialize);
		}
		
		//-----------------------------------------------------
		//Preloader
		//-----------------------------------------------------
		
		private function showAnimatedPreloader(text:String):void {
			this.status = ChartViewStatus.PRELOADER;
			if (this.preloaderContainer.parent == null)
				this.container.addChild(this.preloaderContainer);
			this.preloader.showAnimated(text);
		}
		
		protected final function showPreloader(prefix:String, showProgress:Boolean):void {
			if (showProgress) {
				this.preloader.initProgress(prefix);
			}else {
				this.preloader.showAnimated(prefix);
			}
			if (this.preloaderContainer.parent == null)
				this.container.addChild(this.preloaderContainer);
			this.status = ChartViewStatus.PRELOADER;
			this.resources.load();
		}
		
		private function progressHandler(event:ProgressEvent):void {
			this.preloader.setProgress(event.bytesLoaded/event.bytesTotal);
		}
		
		protected final function removePreloader():void {
			if (this.preloaderContainer != null && this.preloaderContainer.parent != null)
				this.preloaderContainer.parent.removeChild(this.preloaderContainer);
		}
		
		public function updatePointData(groupName:String, pointName:String, data:Object):void {
			if (this.element is Chart)
				Chart(this.element).plot.updateData.apply(null,[groupName, pointName,data]);
		}
	}
}