package com.anychart.context {
	import com.anychart.IAnyChart;
	import com.anychart.serialization.SerializerBase;
	
	public class VersionInfo extends ContextMenuElement {
		
		public function VersionInfo(chart:IAnyChart) {
			super(chart);
			this.contextMenuItemText = "Version 6.0.11 (build #36916)";
		}
		
		override public function deserializeContextMenu(data:XML):void {
			if (data.@version_info != undefined)
				this.enabled = SerializerBase.getBoolean(data.@version_info);
		}
		
	}
}