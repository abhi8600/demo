package com.anychart.noData {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	import com.anychart.preloader.Preloader;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.text.InteractiveTitle;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public final class NoDataMessage extends InteractiveTitle implements IFormatable {
		
		private var showPreloader:Boolean;
		private var preloader:Preloader;		
		
		public function NoDataMessage(chart:IAnyChart) {
			super(chart, this);
			this.showPreloader = true;
		}
		
		public function set noDataText(value:String):void {
			this.info.formattedText = value;
			this.getBounds(this.info);
		}
		
		public function get noDataText():String { return this.info.formattedText; }
		
		public function getTokenValue(token:String):* { return ""; }
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			super.initialize(bounds);
			if (this.preloader) {
				this.container.addChild(this.preloader.initialize(this.getPreloaderBounds(bounds)));
				this.preloader.showAnimated("");
			}
			this.setPosition(bounds);
			return this.container;
		}
		
		override public function resize(newBounds:Rectangle):void {
			super.resize(newBounds);
			if (this.preloader)
				this.preloader.resize(this.getPreloaderBounds(newBounds));
			this.setPosition(newBounds);
		}
		
		private function getPreloaderBounds(bounds:Rectangle):Rectangle {
			var textBounds:Rectangle = info.rotatedBounds;
			return new Rectangle(textBounds.width/2, -preloader.height - 10, 0, 100);
		}
		
		private function setPosition(bounds:Rectangle):void {
			this.container.x = bounds.x + (bounds.width - info.rotatedBounds.width)/2;
			this.container.y = bounds.y + (bounds.height - info.rotatedBounds.height)/2;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			
			if (data != null && data.@show_waiting_animation != undefined)
				this.showPreloader = SerializerBase.getBoolean(data.@show_waiting_animation);
			if (this.showPreloader)
				this.preloader = new Preloader();
			
			var labelNode:XML = data != null ? data.label[0] : null;
			if (labelNode != null)
				labelNode = XMLUtils.merge(defaultTemplate, labelNode, "label");
			
			super.deserialize(labelNode != null ? labelNode : defaultTemplate, resources, style);
			if (this.text != null)
				this.noDataText = this.text;
		}
		
		private static var defaultTemplate:XML = 
			<noDataLabel>
				<background enabled="false">
					<border enabled="True" type="Gradient">
						<gradient angle="90">
							<key position="0" color="#DDDDDD"/>
							<key position="1" color="#D0D0D0"/>
						</gradient>
					</border>
					<fill type="Gradient">
						<gradient angle="90">
							<key position="0" color="#FFFFFF"/>
							<key position="0.5" color="#F3F3F3"/>
							<key position="1" color="#FFFFFF"/>
						</gradient>
					</fill>
					<inside_margin all="5"/>
					<effects enabled="True">
						<drop_shadow enabled="True" distance="1" opacity="0.1"/>
					</effects>
				</background>
				<font family="Tahoma" size="12" color="#232323" bold="False"/>
			</noDataLabel>;
	}
}