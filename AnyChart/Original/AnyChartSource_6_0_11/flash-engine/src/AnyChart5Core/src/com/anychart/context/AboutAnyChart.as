package com.anychart.context {
	import com.anychart.IAnyChart;
	import com.anychart.serialization.SerializerBase;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class AboutAnyChart extends ContextMenuElement {
		
		public function AboutAnyChart(chart:IAnyChart) {
			super(chart);
			this.contextMenuItemText = "About AnyChart...";
		}
		
		override public function deserializeContextMenu(data:XML):void {
			if (data.@about_anychart != undefined)
				this.enabled = SerializerBase.getBoolean(data.@about_anychart);
		}
		
		override public function execute():void {
			navigateToURL(new URLRequest("http://anychart.com"),"_blank");
		}
	}
}