package com.anychart.titles {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.layout.Position;
	import com.anychart.visual.text.InteractiveTitle;
	
	import flash.geom.Rectangle;
	
	public class ChartTitle extends InteractiveTitle implements IFormatable {
		
		private var position:uint;
		private var alignByDataPlot:Boolean;
		private var plot:IPlot;
		
		public function ChartTitle(chart:IAnyChart) {
			super(chart,this);
			this.position = Position.TOP;
			this.alignByDataPlot = false;
		}
		
		public function getTokenValue(token:String):* {
			return (this.plot != null) ? this.plot.getTokenValue(token) : "";
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		public function initTitleText(plot:IPlot):void {
			this.plot = plot;
			this.info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : this.text;
			this.getBounds(this.info);
			this.drawTitle();
		}
		
		public function crop(plotBounds:Rectangle):void {
			var bounds:Rectangle = this.info.rotatedBounds;
			switch (this.position) {
				case Position.LEFT:
					this.container.x = plotBounds.x;
					plotBounds.x += bounds.width + this.padding;
					plotBounds.width -= bounds.width + this.padding;
					break;
				case Position.RIGHT:
					plotBounds.width -= bounds.width + this.padding;
					this.container.x = plotBounds.right + this.padding;
					break;
				case Position.TOP:
					this.container.y = plotBounds.y;
					plotBounds.y += bounds.height + this.padding;
					plotBounds.height -= bounds.height + this.padding;
					break;
				case Position.BOTTOM:
					plotBounds.height -= bounds.height + this.padding;
					this.container.y = plotBounds.bottom + this.padding;
					break;
			}
		}
		
		public function cropTitle(chartRect:Rectangle):void {
			switch (this.position) {
				case Position.TOP:
				case Position.BOTTOM:
					if (this.container.x<chartRect.x){
						this.container.x=chartRect.x;
						this.container.scrollRect = new Rectangle (0,0,chartRect.width,chartRect.height);	
					}else {
						this.container.scrollRect = null;
					}
					break;
				case Position.LEFT:
				case Position.RIGHT:
					if (this.container.y<chartRect.y){
						this.container.y=chartRect.y;
						this.container.scrollRect = new Rectangle (0,0,chartRect.width,chartRect.height);	
					}else {
						this.container.scrollRect = null;
					}
					break;
			} 
		}
		
		public function setPosition(chartBounds:Rectangle, plotBounds:Rectangle):void {
			this.setAlign(this.alignByDataPlot ? plotBounds : chartBounds);
		}
		
		private function setAlign(rect:Rectangle):void {
			switch (this.position) {
				case Position.TOP:
				case Position.BOTTOM:
					switch (this.align) {
						case AbstractAlign.NEAR:
							this.container.x = rect.x;
							break;
						case AbstractAlign.CENTER:
							this.container.x = rect.x + (rect.width - this.info.rotatedBounds.width)/2;
							break;
						case AbstractAlign.FAR:
							this.container.x = rect.right - this.info.rotatedBounds.width;
							break;
					}
					break;
					
				case Position.LEFT:
				case Position.RIGHT:
					switch (this.align) {
						case AbstractAlign.NEAR:
							this.container.y = rect.y;
							break;
						case AbstractAlign.CENTER:
							this.container.y = rect.y + (rect.height - this.info.rotatedBounds.height)/2;
							break;
						case AbstractAlign.FAR:
							this.container.y = rect.bottom - this.info.rotatedBounds.height;
							break;
					}
					break;
			}
			this.cropTitle(rect);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (!this.enabled) return;
			if (data.@position != undefined) {
				switch (SerializerBase.getEnumItem(data.@position)) {
					case "left": 
						this.position = Position.LEFT;
						this.rotation -= 90; 
						break;
					case "right": 
						this.position = Position.RIGHT; 
						this.rotation += 90;
						break;
					case "top": this.position = Position.TOP; break;
					case "bottom": this.position = Position.BOTTOM; break;
				}
			}
			if (data.@align_by != undefined) 
				this.alignByDataPlot = SerializerBase.getEnumItem(data.@align_by) == "dataplot";
		}
	}
}