package com.anychart.controls.legend {
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	internal final class LegendItem extends Sprite {
		public var contentBounds:Rectangle;
		private var tag:ILegendTag;
		
		public function LegendItem() {
			this.contentBounds = new Rectangle();
		}
		
		public function destroy():void {
			this.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.tag = null;
		}
		
		public function initTag(tag:ILegendTag):void {
			this.mouseChildren = false;
			this.buttonMode = true;
			this.useHandCursor = true;
			if (tag != null) {
				this.tag = tag;
				this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			}	
		}
		
		private function mouseOverHandler(event:MouseEvent):void {
			this.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.tag.setHover();
			this.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
		}
		
		private function mouseOutHandler(event:MouseEvent):void {
			this.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.tag.setNormal();
			this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
		}
	}
}