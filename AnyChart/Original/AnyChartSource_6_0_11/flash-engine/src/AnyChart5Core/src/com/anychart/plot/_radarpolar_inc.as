private static var radarPolarInitialized:Boolean = initializeRadarPolar();
private static function initializeRadarPolar():Boolean {
	
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.PolarPlot;
	var p1:RadarPlot;
	var p2:PolarPlot;
	
	/* bubble template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.radarPolarPlot.series.bubbleSeries::TemplateUtils")).getDefaultTemplate().copy());
	import com.anychart.radarPolarPlot.series.bubbleSeries.BubbleGlobalSeriesSettings;
	var tmp1:BubbleGlobalSeriesSettings;
	
	/* marker template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.radarPolarPlot.series.markerSeries::TemplateUtils")).getDefaultTemplate().copy());
	import com.anychart.radarPolarPlot.series.markerSeries.MarkerGlobalSeriesSettings;
	var tmp2:MarkerGlobalSeriesSettings;
	
	/* line template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.radarPolarPlot.series.lineSeries::TemplateUtils")).getDefaultTemplate().copy());
	import com.anychart.radarPolarPlot.series.lineSeries.LineGlobalSeriesSettings;
	var tmp3:LineGlobalSeriesSettings;
	
	import com.anychart.radarPolarPlot.series.lineSeries.AreaGlobalSeriesSettings;
	var tmp4:AreaGlobalSeriesSettings;
	
	return true;
}