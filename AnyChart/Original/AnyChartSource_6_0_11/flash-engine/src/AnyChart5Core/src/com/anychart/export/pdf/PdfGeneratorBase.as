package com.anychart.export.pdf
{
	import flash.utils.ByteArray;
	
	internal class PdfGeneratorBase {
		
		protected var bytes:ByteArray;
		protected var offsets:Array;
		
		protected function initGeneration():void {
			this.bytes = new ByteArray();
			this.offsets = [];
		}
		
		protected function write(data:String):void {
			this.bytes.writeMultiByte(data+"\n", "windows-1252");
		}
		
		protected function startObj(index:int):void {
			this.offsets[index] = this.bytes.position;
			this.write(index+" 0 obj");
		}
		
		protected function endObj():void {
			this.write("endobj");
		}
		
		protected function finalize(infoIndex:int):void {
			var len:int = this.offsets.length;
			var xRefOffset:int = this.bytes.position;
			this.write("xref");
			this.write("0 "+len);
			this.write("0000000000 65535 f");
			for (var i:int = 1; i < len; i++)
				this.write(this.pad(this.offsets[i].toString(), 10, '0', true) + " 00000 n");
			this.write("trailer");
			this.write("<</Size "+len+"\n/Root 1 0 R/Info "+infoIndex.toString()+" 0 R>>");
			this.write("startxref");
			this.write(xRefOffset.toString());
			this.write("%%EOF");
		}
		
		private function pad(str:String, targetLength:int, glue:String, left:Boolean):String {
			var lenDiff:int = (targetLength - str.length);
			var intCount:int = Math.floor(lenDiff / glue.length);
			var mod:int = lenDiff % glue.length;
			var pad:String = "";
			for (var i:int = 0; i < intCount; i++)
				pad += glue;
			if (left) {
				pad = glue.substr(glue.length - mod, mod) + pad;
				return pad + str;
			} else {
				return str + pad + glue.substr(0, mod);
			}
		}
	}
}