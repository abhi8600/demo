package com.anychart.xmlStore {
	
	public final class XMLGlobalItems {
		
		private static var instance:XMLGlobalItems;
		public static function getInstance():XMLGlobalItems {
			if (instance == null)
				instance = new XMLGlobalItems();
			return instance; 
		}
		
		private var templates:Object;
		private var charts:Object;
		
		public function XMLGlobalItems() {
			this.templates = {};
			this.charts = {};
		}
		
		public function addTemplates(data:XMLList):void {
			for each (var item:XML in data) {
				if (item.@name != undefined)
					this.addTemplate(String(item.@name), item);
			}
		}
		
		private function addTemplate(templateName:String, templateData:XML):void {
			this.templates[templateName.toLowerCase()] = templateData;
		}
		
		public function getTemplate(templateName:String):XML {
			return this.templates[templateName.toLowerCase()] != null ? XML(this.templates[templateName.toLowerCase()]).copy() : null;
		}
		
		public function addCharts(data:XMLList):void {
			for each (var item:XML in data) {
				if (item.@name != undefined)
					this.addChart(String(item.@name), item);
			}
		}
		
		private function addChart(chartName:String, chartData:XML):void {
			chartName = chartName.toLowerCase();
			//если заменять - всплывет баг AC-217:
			//http://192.168.1.55:8080/browse/AC-217
			if (this.charts[chartName] == null)
				this.charts[chartName] = chartData;
		}
		
		public function getChart(chartName:String):XML {
			return this.charts[chartName.toLowerCase()];
		}
		
		public static function clear():void {
			if (instance) {
				instance.charts = {};
				instance.templates = {};
			}
		}
	}
}