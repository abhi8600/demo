package com.anychart.viewController {
	import com.anychart.IResizable;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public interface IViewElement extends IResizable, ISerializableRes, IObjectSerializable {
		function getTitle():String;
		function getBounds():Rectangle;
		function getContainer():Sprite;
		
		function setMargin(margin:Margin):void;
		function destroy():void;
		
		function postInitialize():void;
		function get isInitialized():Boolean;
	}
}