package com.anychart {
	import com.anychart.animation.event.AnimationEvent;
	import com.anychart.context.AboutAnyChart;
	import com.anychart.context.CopyXML;
	import com.anychart.context.VersionInfo;
	import com.anychart.dashboard.Dashboard;
	import com.anychart.dateTime.DateTimeFormatter;
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.events.DashboardEvent;
	import com.anychart.events.EngineEvent;
	import com.anychart.export.ImageExporter;
	import com.anychart.export.PDFExporter;
	import com.anychart.locale.DateTimeLocale;
	import com.anychart.locale.NumberLocale;
	import com.anychart.plot.PlotFactory;
	import com.anychart.printing.ChartPrinter;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.styles.StylesList;
	import com.anychart.utils.GCUtil;
	import com.anychart.utils.XMLUtils;
	import com.anychart.viewController.ChartView;
	import com.anychart.viewController.DashboardChartElementView;
	import com.anychart.visual.layout.Margin;
	import com.anychart.xmlStore.XMLGlobalItems;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.ui.ContextMenu;
	
	[Event(name="anychartCreate", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartRender", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartDraw", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartRefresh", type="com.anychart.events.EngineEvent")]
	[Event(name="dashboardViewRender", type="com.anychart.events.DashboardEvent")]
	[Event(name="dashboardViewDraw", type="com.anychart.events.DashboardEvent")]
	[Event(name="dashboardViewRefresh", type="com.anychart.events.DashboardEvent")]
	[Event(name="valueChange", type="com.anychart.events.ValueChangeEvent")]
	[Event(name="pointClick", type="com.anychart.events.PointEvent")]
	[Event(name="pointMouseOver", type="com.anychart.events.PointEvent")]
	[Event(name="pointMouseOut", type="com.anychart.events.PointEvent")]
	[Event(name="pointMouseDown", type="com.anychart.events.PointEvent")]
	[Event(name="pointMouseUp", type="com.anychart.events.PointEvent")]
	[Event(name="pointSelect", type="com.anychart.events.PointEvent")]
	[Event(name="pointDeselect", type="com.anychart.events.PointEvent")]
	[Event(name="multiplePointsSelect", type="com.anychart.events.PointsEvent")]
	public final class AnyChart extends ChartView implements IAnyChart {
		
		public static const ENABLE_ERROR_REPORTING:Boolean = false;
		
		public var messageOnWaitingForData:String = "Waiting for data...";
		public var messageOnInitialize:String = "Initializing...";
		public var messageOnLoadingXML:String = "Loading xml...";
		public var messageOnLoadingResources:String = "Loading resources...";
		public var messageOnLoadingTemplates:String = "Loading templates...";
		public var messageNoData:String = "No Data";
		public var settingsNoData:XML;
		
		private var margin:Margin;
		
		private var _contextMenu:ContextMenu;
		public function get contextMenu():ContextMenu { return this._contextMenu; }
		
		private var _enableAnimation:Boolean;
		private var _loopAnimation:Boolean;
		private var _loopDelay:Number;
		public function get enableAnimation():Boolean { return this._enableAnimation; }
		public function get loopAnimation():Boolean { return this._loopAnimation; }
		public function get loopDelay():Number { return this._loopDelay; }
		
		public function getMainContainer():InteractiveObject {
			return this.container.stage == null ? this.container : this.container.stage;
		}
		
		public function AnyChart(container:Sprite) {
			this.container = container;
			if (this.container.parent != null) {
				this.container.parent.contextMenu = new ContextMenu();
				this._contextMenu = this.container.parent.contextMenu;
			}else {
				this.container.contextMenu = new ContextMenu();
				this._contextMenu = this.container.contextMenu;
			}
			if (this._contextMenu)
				this._contextMenu.hideBuiltInItems();
			
			this.pngSaver = new ImageExporter(this);
			this.pdfSaver = new PDFExporter(this);
			this.printer = new ChartPrinter(this);
			this.about = new AboutAnyChart(this);
			this.versionInfo = new VersionInfo(this);
			this.copyXML = new CopyXML(this);
			
			this.margin = new Margin();
			this.margin.all = 20;
			super(this);
			
			this.dispatchEvent(new EngineEvent(EngineEvent.ANYCHART_CREATE));
		}
		
		public function getScrollableContent():BitmapData {
			if (!this.isDashboard && Chart(this.element).plot is BasePlot) {
				var source:Sprite = BasePlot(Chart(this.element).plot).scrollableContainer;
				var rect:Rectangle = source.scrollRect.clone();
				source.scrollRect = null;
				
				var w:Number, h:Number;
				if (Object(Chart(this.element).plot).hasOwnProperty("totalBounds")) {
					w = BasePlot(Chart(this.element).plot)["totalBounds"].width;
					h = BasePlot(Chart(this.element).plot)["totalBounds"].width;
				}
				
				var data:BitmapData = new BitmapData(w, h, false);
				data.draw(source);
				source.scrollRect = rect;
				return data;
			}
			
			return null;
		}
		
		public function clearTooltips():void {
			if (this.element && this.element is Chart) {
				Chart(this.element).clearTooltips();
			}
		}
		
		public function get isDashboard():Boolean {
			return this.element is Dashboard;
		}
		
		public function clearCashes():void {
			DisposableStaticList.clear();
		}
		
		override public function setXMLFile(path:String, replaces:Array=null,needClear:Boolean = true):void {
			if (needClear) {
				XMLGlobalItems.clear();
				StylesList.clear();
				if (this.element)
					this.element.destroy();
				this.element = null;
				GCUtil.execGCWithBubenAndDances();
			}
			super.setXMLFile(path, replaces, false);
		}
		
		override public function setXMLData(rawData:String, replaces:Array=null,needClear:Boolean = true):void {
			if (needClear) {
				XMLGlobalItems.clear();
				StylesList.clear();
				if (this.element)
					this.element.destroy();
				this.element = null;
				GCUtil.execGCWithBubenAndDances();
			}
			super.setXMLData(rawData, replaces, false);
		}
		
		//-----------------------------------------------
		// ChartView overrides
		//-----------------------------------------------
		
		private var topContainer:Sprite;
		public function showPreloaderOnTheTop(text:String):void {
			if (!this.topContainer) this.topContainer = new Sprite();
			if (!this.topContainer.parent) this.container.addChild(this.topContainer);
			
			this.topContainer.graphics.beginFill(0xFFFFFF, .3);
			this.topContainer.graphics.drawRect(0,0,this.bounds.width,this.bounds.height);
				 
			this.topContainer.addChild(this.preloaderContainer);
			this.preloader.showAnimated(text);
		}
		
		public function hideTopPreloader():void {
			if (this.preloaderContainer && this.preloaderContainer.parent)
				this.preloaderContainer.parent.removeChild(this.preloaderContainer);
			if (this.topContainer && this.topContainer.parent)
				this.topContainer.parent.removeChild(this.topContainer);
		}
		
		override protected function setDefaults():void {
			this.pngSaver.setDefaults();
			this.pdfSaver.setDefaults();
			this.printer.setDefaults();
			this.about.setDefaults();
			this.versionInfo.setDefaults();
			this.copyXML.setDefaults();
			this.initializeContextMenu();
			this._enableAnimation = false;
			this._loopAnimation = false;
			this._loopDelay = .5;
		}
		
		private var dataSets:XML;
		
		public function getDataSets():XML {
			return this.dataSets;
		}
		
		private var savedSettings:XML;
		private var savedMargin:XML;
		
		override protected function processXML(data:XML):XML {
				
			data = super.processXML(data);
			
			if (data != null && data.data_sets[0] != null)
				this.dataSets =  data.data_sets[0];
				
			if (data != null) {
				if (data.settings[0] != null && this.savedSettings == null)
					this.savedSettings = data.settings[0];
				else {
					var res:XML = XMLUtils.merge(this.savedSettings, data.settings[0], "settings");
					if (res != null) data.settings[0] = res.copy();
				}
					
				if (data.margin[0] != null && this.savedMargin == null)
					this.savedMargin = data.margin[0];
				else
					data.margin[0] = XMLUtils.merge(this.savedMargin, data.margin[0], "margin");
					
				this.savedSettings = data.settings[0];
				this.savedMargin = data.margin[0];
			} 
			
			this.margin.all = 20;
			
			if (data.margin[0] != null)
				this.margin.deserialize(data.margin[0]);
			
			this._dateTimeLocale = new DateTimeLocale();
			NumberLocale.clear();
			
			this.settingsNoData =  null;
			
			if (data.settings[0] != null) {
				this.pngSaver.deserializeSettings(data.settings[0].image_export[0]);
				this.pdfSaver.deserializeSettings(data.settings[0].pdf_export[0]);
				if (data.settings[0].context_menu[0] != null) {
					this.pngSaver.deserializeContextMenu(data.settings[0].context_menu[0]);
					this.pdfSaver.deserializeContextMenu(data.settings[0].context_menu[0]);
					this.printer.deserializeContextMenu(data.settings[0].context_menu[0]);
					this.about.deserializeContextMenu(data.settings[0].context_menu[0]);
					this.versionInfo.deserializeContextMenu(data.settings[0].context_menu[0]);
					this.copyXML.deserializeContextMenu(data.settings[0].context_menu[0]);
				}
				if (data.settings[0].animation[0] != null) {
					if (data.settings[0].animation[0].@enabled != undefined)
						this._enableAnimation = String(data.settings[0].animation[0].@enabled).toLowerCase() == "true";
					if (data.settings[0].animation[0].@loop != undefined)
						this._loopAnimation = String(data.settings[0].animation[0].@loop).toLowerCase() == "true";
					if (data.settings[0].animation[0].@loop_delay != undefined)
						this._loopDelay = Number(data.settings[0].animation[0].@loop_delay);
				}
				if (data.settings[0].locale[0] != null) {
					if (data.settings[0].locale[0].date_time_format[0] != null)
						this._dateTimeLocale.deserialize(data.settings[0].locale[0].date_time_format[0]);
					if (data.settings[0].locale[0].number_format[0] != null)
						NumberLocale.deserialize(data.settings[0].locale[0].number_format[0]);
				}
				if (data.settings[0].no_data[0] != null)
					this.settingsNoData = data.settings[0].no_data[0];
			}
			this._dateTimeLocale.initialize();
			DateTimeFormatter.initialize(this._dateTimeLocale);
			
			this.resources.deserializeSettings(data);
			
			this.initializeContextMenu();
			this.resources.initialize(this.container.loaderInfo != null ? this.container.loaderInfo.url : null);
			
			if (data.charts[0] != null)
				XMLGlobalItems.getInstance().addCharts(data.charts[0].chart);
			if (data.gauges[0] != null)
				XMLGlobalItems.getInstance().addCharts(data.gauges[0].gauge);
			
			if (data.templates[0] != null) {
				if (data.templates[0].@path != undefined) {
					this.loadTemplates(data.templates[0].@path);
				}else{
					XMLGlobalItems.getInstance().addTemplates(data.templates[0].template);
					this.generateChart(data);
				}
			}else {
				data.templates[0] = <templates />;
				this.generateChart(data);
			}
			return data;
		}
		
		override protected function createElement(data:XML):void {
			this.drawedViewsCount = 0;
			if (this.isDashboardXML(this.anychartXMLData)) {
				this.element = new Dashboard(this);
				this.element.setMargin(this.margin);
				this.element.deserialize(this.anychartXMLData.dashboard[0].view[0], this.resources);
			}else {
				this.element = new Chart(this, this);
				this.element.setMargin(this.margin);
				PlotFactory.createSinglePlot(this, Chart(this.element),this.anychartXMLData);
			}
		}
		
		private function isDashboardXML(data:XML):Boolean {
			return (data.dashboard[0] != null && data.dashboard[0].view[0] != null);
		}
		
		//-----------------------------------------------
		// Templates
		//-----------------------------------------------
		
		private var templatesPath:String;
		
		private function loadTemplates(path:String):void {
			this.templatesPath = path;
			this.resources.addText(path, true);
			if (this.resources.isLoaded()) {
				this.addTemplates(ResourcesHashMap.getText(path));
			}else {
				this.templatesPath = templatesPath;
				this.resources.addEventListener(Event.COMPLETE, this.templatesLoadHandler);
				this.showPreloader(this.messageOnLoadingTemplates, true);
			}
		}
		
		private function templatesLoadHandler(event:Event):void {
			this.resources.removeEventListener(Event.COMPLETE, this.templatesLoadHandler);
			this.preloader.setProgress(1);
			this.addTemplates(ResourcesHashMap.getText(this.templatesPath));
		}
		
		private function addTemplates(data:String):void {
			var templates:XML = this.getXML(data);
			if (templates != null) {
				var templatesList:XMLList = templates.template;
				var cnt:int = templatesList.length();
				for (var i:int = 0;i<cnt;i++) {
					this.anychartXMLData.templates[0].template += XML(templatesList[i]).copy();
				}
				XMLGlobalItems.getInstance().addTemplates(templatesList);
				this.removePreloader();
				this.showInitializing();
				this.generateChart(this.anychartXMLData);
			}
		}
		
		//-----------------------------------------------
		// Context Menu
		//-----------------------------------------------
		
		private var pngSaver:ImageExporter;
		private var pdfSaver:PDFExporter;
		private var printer:ChartPrinter;
		private var about:AboutAnyChart;
		private var versionInfo:VersionInfo;
		private var copyXML:CopyXML;
		
		private function initializeContextMenu():void {
			if (this.contextMenu)
				this.contextMenu.customItems = [];
			this.versionInfo.initializeContextMenu();
			this.copyXML.initializeContextMenu();
			this.about.initializeContextMenu();
			this.pngSaver.initializeContextMenu();
			this.pdfSaver.initializeContextMenu();
			this.printer.initializeContextMenu();
		}
		
		//-----------------------------------------------
		// Events
		//-----------------------------------------------
		
		override protected function dispatchRenderEvent():void {
			if (!this.isInRefresh)
				this.dispatchEvent(new EngineEvent(EngineEvent.ANYCHART_RENDER));
		}
		
		override protected function dispatchDrawEvent():void {
			if (this.element == null) return;
			if (this.element is IChart) {
				if (IChart(this.element).getAnimation() && 
					IChart(this.element).getAnimation().isAnimated()) {
					IChart(this.element).getAnimation().addEventListener(AnimationEvent.ANIMATION_FINISH, this.animationFinish);
				}else {
					this.execDispatchDrawEvent();
				}
			}else if (this.element is Dashboard) {
				if (Dashboard(this.element).getViewsCount() == 0)
					this.execDispatchDrawEvent();
			}
		}
		
		override protected function execDispatchDrawEvent():void {
			GCUtil.execGCWithBubenAndDances();
			if (!this.isInRefresh)
				this.dispatchEvent(new EngineEvent(EngineEvent.ANYCHART_DRAW));
			else 
				this.dispatchEvent(new EngineEvent(EngineEvent.ANYCHART_REFRESH));
			this.isInRefresh = false;
		}
		
		private var drawedViewsCount:uint;
		
		public function dispatchViewDrawEvent(view:DashboardChartElementView, viewName:String):void {
			if (!view.isInRefresh)
				this.dispatchEvent(new DashboardEvent(DashboardEvent.DASHBOARD_VIEW_DRAW, viewName));
			else
				this.dispatchEvent(new DashboardEvent(DashboardEvent.DASHBOARD_VIEW_REFRESH, viewName));
			view.isInRefresh = false;
				
			this.drawedViewsCount++;
			if (this.drawedViewsCount == Dashboard(this.element).getViewsCount()) {
				this.execDispatchDrawEvent();
			}
		} 
		
		//-----------------------------------------------
		// IAnyChart
		//-----------------------------------------------
		
		public function getTitle():String {
			return this.element.getTitle();
		}
		
		public function getBounds():Rectangle {
			return this.bounds;
		}
		
		public function getContainer():Sprite {
			return this.container;
		}
		
		public function getBase64PNG(width:* = undefined, height:* = undefined):String {
			var data:String = null;
			try {
				data = this.pngSaver.getData((width == null || width == undefined) ? NaN : Number(width), 
									   	 	 (height == null || height == undefined) ? NaN : Number(height));
			}catch (e:Error) {}
			return data;
		}
		
		public function getBase64PDF(width:* = undefined, height:* = undefined):String {
			var data:String = null;
			try {
				data = this.pdfSaver.getData((width == null || width == undefined) ? NaN : Number(width), 
					(height == null || height == undefined) ? NaN : Number(height));
			}catch (e:Error) {}
			return data;
		}
		
		public function saveAsImage(width:* = undefined, height:* = undefined):void {
			this.pngSaver.executeExternalSaving((width == null || width == undefined) ? NaN : Number(width), 
				(height == null || height == undefined) ? NaN : Number(height));
		}
		
		public function saveAsPDF(width:* = undefined, height:* = undefined):void {
			this.pdfSaver.executeExternalSaving((width == null || width == undefined) ? NaN : Number(width), 
				(height == null || height == undefined) ? NaN : Number(height));
		}
		
		public function setViewLoading(viewId:String, text:String):void {
			if (this.isDashboard)
				Dashboard(this.element).getView(viewId).setLoading(text);
		}
		
		public function setViewInternalSource(viewId:String, source:String, replaces:Array = null):void {
			if (this.isDashboard) {
				var view:DashboardChartElementView = Dashboard(this.element).getView(viewId);
				if (view != null) {
					if (replaces == null) {
						view.setXML(PlotFactory.getChartXML(source));
					}else {
						var data:XML = PlotFactory.getChartXML(source);
						data = XMLUtils.applyReplaces(data, replaces);
						view.setXML(data);
					}
				} 
			}
		}
		
		public function setViewXMLFile(viewId:String, path:String, replaces:Array = null):void {
			if (this.isDashboard)
				Dashboard(this.element).getView(viewId).setXMLFile(path, replaces);
		}
		
		public function setViewXMLData(viewId:String, data:String, replaces:Array = null):void {
			if (this.isDashboard)
				Dashboard(this.element).getView(viewId).setXMLData(data, replaces);
		}
		
		public function setSource(source:String, replaces:Array = null, needClear:Boolean = true):void {
			if (needClear) 
				XMLGlobalItems.clear();
			if (replaces == null) {
				this.setXML(PlotFactory.getChartXML(source));
			}else {
				var data:XML = PlotFactory.getChartXML(source);
				data = XMLUtils.applyReplaces(data, replaces);
				this.setXML(data);
			}
		}
		
		public function printChart():void {
			this.printer.execute();
		}
		
		public function getXMLData():XML {
			return this.anychartXMLData;
		}
		
		private var _dateTimeLocale:DateTimeLocale;
		public function get dateTimeLocale():DateTimeLocale { return this._dateTimeLocale; }
		
		override protected function showNoData():void {
			if (this.element && this.element is IChart) {
				IChart(this.element).showNoData(null);
			}else {
				super.showNoData();
			}
		}
		
		public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.updatePointWithAnimation(seriesId, pointId, newValues, settings);
		}	
		
		public function updateViewPointWithAnimation(viewId:String, seriesId:String, pointId:String, newValues:Object, settings:Object):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewId).element).plot.updatePointWithAnimation(seriesId, pointId, newValues, settings);
		}
		
		public function startPointsUpdate():void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.startPointsUpdate();
		}	
		
		public function startViewPointsUpdate(viewId:String):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewId).element).plot.startPointsUpdate();
		}
		
		public function setXZoom(settings:Object):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.setXZoom(settings); 
		}
		public function setYZoom(settings:Object):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.setYZoom(settings); 
		}
		public function setZoom(xZoomSettings:Object, yZoomSettings:Object):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.setZoom(xZoomSettings, yZoomSettings); 
		}
		public function scrollXTo(xValue:String):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.scrollXTo(xValue); 
		}
		public function scrollYTo(yValue:String):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.scrollYTo(yValue); 
		}
		public function scrollTo(xValue:String, yValue:String):void {
			if (this.isDashboard || !Chart(this.element).plot) return;
			Chart(this.element).plot.scrollTo(xValue, yValue); 
		}
		public function getXScrollInfo():Object {
			if (this.isDashboard || !Chart(this.element).plot) return null;
			return Chart(this.element).plot.getXScrollInfo();
		}
		public function getYScrollInfo():Object {
			if (this.isDashboard || !Chart(this.element).plot) return null;
			return Chart(this.element).plot.getYScrollInfo();
		}
		
		public function setViewXZoom(viewName:String, settings:Object):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.setXZoom(settings);
		}
		public function setViewYZoom(viewName:String, settings:Object):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.setYZoom(settings);
		}
		public function setViewZoom(viewName:String, xZoomSettings:Object, yZoomSettings:Object):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.setZoom(xZoomSettings, yZoomSettings);
		}
		public function viewScrollXTo(viewName:String, xValue:String):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.scrollXTo(xValue);
		}
		public function viewScrollYTo(viewName:String, yValue:String):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.scrollYTo(yValue);
		}
		public function viewScrollTo(viewName:String, xValue:String, yValue:String):void {
			if (this.isDashboard)
				Chart(Dashboard(this.element).getView(viewName).element).plot.scrollTo(xValue, yValue);
		}
		public function getViewXScrollInfo(viewName:String):Object {
			return (this.isDashboard) ? Chart(Dashboard(this.element).getView(viewName).element).plot.getXScrollInfo() : null;
		}
		public function getViewYScrollInfo(viewName:String):Object {
			return (this.isDashboard) ? Chart(Dashboard(this.element).getView(viewName).element).plot.getYScrollInfo() : null;
		}
		
		//-----------------------------------------------------
		//Dashboard data manipulation
		//-----------------------------------------------------
		
		private function getChartForAction(viewId:String):ChartView {
			if (!this.isDashboard) return null;
			if (Dashboard(this.element).getView(viewId) == null) return null;
			if (Dashboard(this.element).getView(viewId).element == null) return null;
			if (Chart(Dashboard(this.element).getView(viewId).element).plot == null) return null;
			return ChartView(Dashboard(this.element).getView(viewId));
		}
		
		public function view_setPlotCustomAttribute(viewId:String, attributeName:String, attributeValue:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.setPlotCustomAttribute(attributeName, attributeValue); 
		}
		
		public function view_addSeries(viewId:String, ...seriesData):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.addSeries.apply(null, seriesData); 
		}
		
		public function view_removeSeries(viewId:String, seriesId:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.removeSeries(seriesId); 
		}
		
		public function view_addSeriesAt(viewId:String, index:int, seriesData:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.addSeriesAt(index, seriesData); 
		}
		
		public function view_updateSeries(viewId:String, seriesId:String, seriesData:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.updateSeries(seriesId, seriesData); 
		}
		
		public function view_showSeries(viewId:String, seriesId:String, isVisible:Boolean):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.showSeries(seriesId, isVisible); 
		}
		
		public function view_addPoint(viewId:String, seriesId:String, ...pointData):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.addPoint.apply(null, [seriesId].concat(pointData)); 
		}
		
		public function view_removePoint(viewId:String, seriesId:String, pointId:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.removePoint(seriesId, pointId); 
		}
		
		public function view_addPointAt(viewId:String, seriesId:String, pointIndex:int, pointData:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.addPointAt(seriesId, pointIndex, pointData); 
		}
		
		public function view_updatePoint(viewId:String, seriesId:String, pointId:String, pointData:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.updatePoint(seriesId, pointId, pointData); 
		}
		
		public function view_refresh(viewId:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.refresh();
		}
		
		public function view_clearChartData(viewId:String):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.clearChartData();
		}
		
		public function view_highlightSeries(viewId:String, seriesId:String, highlighted:Boolean):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.highlightSeries(seriesId, highlighted);
		}
		
		public function view_highlightPoint(viewId:String, seriesId:String, pointId:String, highlighted:Boolean):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.highlightPoint(seriesId, pointId, highlighted);
		}
		
		public function view_highlightCategory(viewId:String, categoryName:String, highlighted:Boolean):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.highlightCategory(categoryName, highlighted);
		}
		
		public function view_selectPoint(viewId:String, seriesId:String, pointId:String, selected:Boolean):void {
			var chart:ChartView = this.getChartForAction(viewId);
			if (chart)
				chart.selectPoint(seriesId, pointId, selected);
		}
		//-----------------------------------------------------
		//Data updating
		//-----------------------------------------------------
		
		public function updateViewPointData(viewName:String, groupName:String, pointName:String, data:Object):void {
			if (this.isDashboard)
				Dashboard(this.element).getView(viewName).updatePointData(groupName, pointName, data);
		}
		
		public function updateData(path:String, data:Object):void {
			this.updateXMLData(path, data);
			this.setXML(this.anychartXMLData);
		}
		
		private function updateXMLData(path:String, data:Object):void {
			var i:uint = 0;
			var actualData:* = this.anychartXMLData;
			while (i<path.length) {
				var c:String = path.charAt(i);
				var tmp:Array;
				var s:String;
				var j:int;
				var newData:XMLList;
				switch (c) {
					case '/':
						i++;
						break;
					case '[':
						s = path.substring(i, path.indexOf(']',i));
						i += s.length+1;
						s = s.substring(1);
						actualData = actualData[s];
						break;
					case '(':
						s = path.substring(i, path.indexOf(')',i));
						i += s.length+1;
						s = s.substring(1);
						tmp = s.split('=');
						var paramName:String = "@"+tmp[0];
						var paramValue:String = tmp[1];
						newData = new XMLList();
						for (j = 0;j<actualData.length();j++) {
							if (actualData[j][paramName] != undefined && actualData[j][paramName] == paramValue) {
								newData += actualData[j];
							}
						}
						actualData = newData;
						break;
					default:
						if (c == '.' && i < (path.length-1) && path.charAt(i+1) == '.') {
							tmp = this.readString(path, i);
							i = tmp[1];
							newData = actualData.descendants(tmp[0].substr(2));
							actualData = newData;
							break;
						}
						tmp = this.readString(path, i);
						i = tmp[1];
						actualData = actualData[tmp[0]];
						break;
				}
			}
			if (actualData == null) return;
			
			if (actualData is XMLList) {
				for each (var node:XML in XMLList(actualData))
					this.setAttrs(node, data);
			}else {
				this.setAttrs(actualData,data);
			}
		}
		
		private function setAttrs(node:XML, data:Object):void {
			for (var j:String in data) {
				node['@'+j] = data[j];
				if (j == 'text')
					node.text[0] = <text>{data[j]}</text>;
				else if (j == 'format')
					node.format[0] = <format>{data[j]}</format>;
			}
		}
		
		/**
		 * Reads string from path
		 * @param data String path string
		 * @param startIndex node name start index
		 * @return Array 0 is node name, 1 is the index of the end of node name
		 */
		private function readString(data:String, startIndex:uint):Array {
			var c:String = data.charAt(startIndex);
			var nodeName:String = "";
			var isNodeName:Boolean = true;
			while (startIndex < data.length && isNodeName) {
				nodeName += c;
				startIndex++;
				c = data.charAt(startIndex);
				isNodeName = this.isSymbol(data, startIndex);
			}
			return [nodeName, startIndex];
		}
		
		private function isSymbol(data:String, index:uint):Boolean {
			var c:String = data.charAt(index);
			if (c == "[" || c == "]" || c == "(" || c == ")" || c == "/") return false;
			if (c == "." && (index < (data.length-1)) && data.charAt(index+1) == ".") return false;
			return true;
		}
		
		public function serialize():Object {
			return this.element.serialize();
		}
		
		public function repeatAnimation():void{
			if(this._enableAnimation && this.element is Chart)
			{
				Chart(this.element).getAnimation().reanimate();	
			}	
		}
	}
}