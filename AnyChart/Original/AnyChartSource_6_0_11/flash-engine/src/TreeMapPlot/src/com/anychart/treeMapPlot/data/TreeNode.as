package com.anychart.treeMapPlot.data {
	
	internal final class TreeNode {
		
		[ArrayElementType("TreeNode")]
		private var children:Array;
		public var xmlData:XML;
		
		public function TreeNode(xmlData:XML){
			this.children = new Array();
			this.xmlData = xmlData;
		}
		
		public function addChild(child:TreeNode):void {
			this.children.push(child);
		}
		
		public function hasChildren():Boolean{
			return (this.children.length > 0);
		}
		
		public function getChildren():XMLList {
			var len:int = this.children.length;
			if (len == 0)
				return null;
			var res:XMLList = new XMLList();
			for(var i:int = 0; i < len; i++)
				res += TreeNode(this.children[i]).xmlData;
			return res;
		}
	}
}