package com.anychart.treeMapPlot.style {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTitle;
	
	internal class TreeMapBaseBranchTitle extends BaseTitle {
		
		private var _height:Number;
		private var _heightSet:Boolean;
		public function getHeight():Number { return this._height; }
		public function hasHeight():Boolean { return this._heightSet; }
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources, style);
			if (data.@height != undefined){
				this._height = SerializerBase.getNumber(data.@height);
				this._heightSet = true;
			} else 
				this._heightSet = false;
		}
	}
}