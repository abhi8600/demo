package com.anychart.treeMapPlot.style {
	import com.anychart.styles.Style;

	public final class TreeMapHiddenStyleState extends TreeMapBaseStyleState {
		
		public function TreeMapHiddenStyleState(style:Style) {
			super(style);
		}
		
		override protected function createBranchTitle():ITreeMapBranchTitle {
			return new TreeMapHiddenBranchTitle();
		}
	}
}