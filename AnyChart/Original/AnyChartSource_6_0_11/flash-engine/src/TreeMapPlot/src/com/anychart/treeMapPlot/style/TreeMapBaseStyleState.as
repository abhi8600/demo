package com.anychart.treeMapPlot.style {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.background.RectBackground;
	import com.anychart.visual.layout.Margin;

	public class TreeMapBaseStyleState extends BackgroundBaseStyleState {
		
		public function TreeMapBaseStyleState(style:Style) {
			super(style);
		}
		
		public var background:RectBackground;
		public var insideMargin:Margin;
		public var outsideMargin:Margin;
		public var header:ITreeMapBranchTitle;
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			
			var xmlBranch:XML = data.branch[0];
			if (xmlBranch != null){
				if (SerializerBase.isEnabled(xmlBranch.background[0])){
					this.background = new RectBackground();
					this.background.deserialize(xmlBranch.background[0], resources, this.style);
				}
				if (SerializerBase.isEnabled(xmlBranch.inside_margin[0])){
					this.insideMargin = new Margin();
					this.insideMargin.deserialize(xmlBranch.inside_margin[0]);
				}
				if (SerializerBase.isEnabled(xmlBranch.outside_margin[0])){
					this.outsideMargin = new Margin();
					this.outsideMargin.deserialize(xmlBranch.outside_margin[0]);
				}
				if (SerializerBase.isEnabled(xmlBranch.header[0])){
					this.header = this.createBranchTitle();
					this.header.deserialize(xmlBranch.header[0], resources, this.style);
				}
			}
			
			return data;
		}
		
		protected function createBranchTitle():ITreeMapBranchTitle { return null; }
	}
}