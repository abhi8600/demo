package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	
	import flash.geom.Point;
	
	internal final class StepBackwardLinePoint extends LinePoint {
		
		override protected function createDrawingPoints():void {
			this.drawingPoints = [];
			
			var tmp:Point;
			var tmp1:Point;
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].pixValuePt;
				tmp1 = this.isHorizontal ? new Point(this.pixValuePt.x, (tmp.y + this.pixValuePt.y)/2)
										  : new Point((tmp.x + this.pixValuePt.x)/2, this.pixValuePt.y);
				this.drawingPoints.push(tmp1);
			}
			this.drawingPoints.push(this.pixValuePt);

			if (this.hasNext) {
				this.series.points[this.index+1].initializePixValues();
				this.series.points[this.index+1].needCalcPixValue = false;
				tmp = this.series.points[this.index+1].pixValuePt;
				
				tmp1 = this.isHorizontal ? new Point(tmp.x, this.pixValuePt.y) : new Point(this.pixValuePt.x, tmp.y);
				this.drawingPoints.push(tmp1);
				
				tmp1 = this.isHorizontal ? new Point(tmp.x, (tmp.y + this.pixValuePt.y)/2)
										  :new Point((tmp.x + this.pixValuePt.x)/2, tmp.y);
				this.drawingPoints.push(tmp1);
			}
		}
		
		override protected function transformArgument():void {
			super.transformArgument();
			if (this.category && LineGlobalSeriesSettings(this.global).shiftStepLine) {
				var axis:Axis = AxesPlotSeries(this.series).argumentAxis;
				var offset:Number = axis.scale.transform(1) - axis.scale.transform(.5); 
				axis.transform(this, this.x, this.pixValuePt, -offset);
			}else {
				super.transformArgument();
			}
		}
	}
}