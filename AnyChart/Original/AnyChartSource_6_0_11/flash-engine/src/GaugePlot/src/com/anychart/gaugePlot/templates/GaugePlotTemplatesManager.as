package com.anychart.gaugePlot.templates {
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.utils.XMLUtils;
	
	public final class GaugePlotTemplatesManager extends BaseTemplatesManager {
		override public function getBaseNodeName():String {
			return "gauge";
		}
		
		override protected function mergeChartNode(template:XML, chart:XML, res:XML):void {
			super.mergeChartNode(template, chart, res);
			
			var circularList:XML = this.mergeStyledGauges(template, chart, "circular_template");
			var linearList:XML = this.mergeStyledGauges(template, chart, "linear_template");
			var labelList:XML = this.mergeStyledGauges(template, chart, "label_template");
			var imgList:XML = this.mergeStyledGauges(template, chart, "image_template");
			var indList:XML = this.mergeStyledGauges(template, chart, "indicator_template");
			
			var child:XML;
			if (circularList != null) {
				for each (child in circularList.children()) {
					res.appendChild(child);
				}
			}
			
			if (linearList != null) {
				for each (child in linearList.children()) {
					res.appendChild(child);
				}
			}
			
			if (labelList != null) {
				for each (child in labelList.children()) {
					res.appendChild(child);
				}
			}
			
			if (imgList != null) {
				for each (child in imgList.children()) {
					res.appendChild(child);
				}
			}
			
			if (indList != null) {
				for each (child in indList.children()) {
					res.appendChild(child);
				}
			}
			
			if (chart.circular.length() > 0) res.circular = chart.circular;
			if (chart.linear.length() > 0)   res.linear = chart.linear;
			if (chart.label.length() > 0) res.label = chart.label;
			if (chart.image.length() > 0) res.image = chart.image;
			if (chart.indicator.length() > 0) res.indicator = chart.indicator;
		}
		
		private function mergeStyledGauges(template:XML, chart:XML, collectionName:String):XML {
			var templateCollection:XMLList = template[collectionName];
			var chartCollection:XMLList = chart[collectionName];
			var res:XML = <gauge />;
			var i:int;
			var tlen:int = templateCollection.length();
			
			for (i = 0;i<tlen;i++) {
				if (templateCollection[i].@name != undefined)
					templateCollection[i].@name = String(templateCollection[i].@name).toLowerCase();
				res.appendChild(templateCollection[i]);
			}
			var len:int = chartCollection.length();
			for (i = 0;i<len;i++) {
				if (chartCollection[i].@name != undefined) {
					chartCollection[i].@name = String(chartCollection[i].@name).toLowerCase();
					if (res[collectionName].(@name == chartCollection[i].@name).length() < 1) {
						res.appendChild(chartCollection[i]);
					}else {
						res[collectionName].(@name == chartCollection[i].@name)[0] = chartCollection[i];
					}
				}else {
					res.appendChild(chartCollection[i]);
				}
			}
			return res;
		}
	}
}