package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.axis.Tickmark;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class LinearGaugeAxisMarkersList extends AxisMarkersList {
		
		override public function initialize():DisplayObject {
			this.container = new Sprite();
			for (var i:uint = 0;i<this.markers.length;i++) {
				this.container.addChild(this.markers[i].initialize());
				if (LinearGaugeAxis(this.axis).isHorizontal && this.markers[i] is GaugeAxisLabelMarker) {
					var tickmark:Tickmark = CusomLabelStyleState(GaugeAxisMarker(this.markers[i]).getActualState()).tickmark;
					LinearGaugeAxis(this.axis).checkTickmark(tickmark);
				}
			}
			return this.container;
		}
		
		override protected function drawLineMarker(marker:GaugeAxisLineMarker):void {
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			
			var start:Point = new Point();
			var end:Point = new Point();
			
			var value:Number = this.scale.transform(marker.value);
			
			var state:TrendlineStyleState = TrendlineStyleState(marker.getActualState());
			var size:Number = (state.isSizeAxisSizeDepend ? axis.width : 1) * state.size;
			
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, size, true), value, start);
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, size, false), value, end);
			
			marker.draw(start, end);
			if (state.label != null)
				this.drawMarkerLabel(marker, state.label, marker.value, 0);
		}
		
		override protected function drawRangeMarker(marker:GaugeAxisRangeMarker):void {
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			
			var startValue:Number = this.scale.transform(marker.start);
			var endValue:Number = this.scale.transform(marker.end);
			
			var state:ColorRangeStyleState = ColorRangeStyleState(marker.style.normal);
			
			var startInnerPt:Point = new Point();
			var endInnerPt:Point = new Point();
			var startOuterPt:Point = new Point();
			var endOuterPt:Point = new Point();
			
			var startPercentW:Number = (state.isStartSizeAxisSizeDepend ? axis.width : 1)*state.startWidth;
			var endPercentW:Number =  (state.isEndSizeAxisSizeDepend ? axis.width : 1)*state.endWidth;
			
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, startPercentW, true), startValue, startInnerPt);
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, endPercentW, true), endValue, endInnerPt);
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, startPercentW, false), startValue, startOuterPt);
			axis.setPoint(axis.getComplexPosition(state.align, state.padding, endPercentW, false), endValue, endOuterPt);
			
			var bounds:Rectangle = new Rectangle();
			bounds.x = Math.min(startInnerPt.x, endInnerPt.x, startOuterPt.x, endOuterPt.x);
			bounds.y = Math.min(startInnerPt.y, endInnerPt.y, startOuterPt.y, endOuterPt.y);
			bounds.width = Math.max(startInnerPt.x, endInnerPt.x, startOuterPt.x, endOuterPt.x) - bounds.x;
			bounds.height = Math.max(startInnerPt.y, endInnerPt.y, startOuterPt.y, endOuterPt.y) - bounds.y;
			
			var g:Graphics = marker.container.graphics;
			
			if (state.stroke != null)
				state.stroke.apply(g, bounds, marker.color);
			
			if (state.fill != null)
				state.fill.begin(g, bounds, marker.color);
			
			this.doDrawRangeMarker(g, startInnerPt, endInnerPt, endOuterPt, startOuterPt);
			g.lineStyle();
			g.endFill();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, 0, marker.color);
				this.doDrawRangeMarker(g, startInnerPt, endInnerPt, endOuterPt, startOuterPt);
				g.endFill();
			}
			
			if (state.label != null)
				this.drawMarkerLabel(marker, state.label, (marker.start+marker.end)/2, marker.color);
		}
		
		private function doDrawRangeMarker(g:Graphics, pt1:Point, pt2:Point, pt3:Point, pt4:Point):void {
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
			g.lineTo(pt3.x, pt3.y);
			g.lineTo(pt4.x, pt4.y);
			g.lineTo(pt1.x, pt1.y);
		}
		
		override protected function getLabelPosition(pos:Point, pixVal:Number, align:uint, padding:Number):void {
			if (LinearGaugeAxis(this.axis).isHorizontal) {
				pos.x = pixVal;
				pos.y = LinearGaugeAxis(this.axis).getPosition(align, padding, 0);
			}else {
				pos.y = pixVal;
				pos.x = LinearGaugeAxis(this.axis).getPosition(align, padding, 0);
			}
		}
	}
}