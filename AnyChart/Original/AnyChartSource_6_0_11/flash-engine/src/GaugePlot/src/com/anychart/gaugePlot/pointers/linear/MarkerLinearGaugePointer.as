package com.anychart.gaugePlot.pointers.linear {
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.linear.MarkerPointerStyleState;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeDrawer;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	
	public final class MarkerLinearGaugePointer extends LinearGaugePointer {
		override protected function getStyleNodeName():String {
			return "marker_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return MarkerPointerStyleState;
		}
		
		private var shapeContainer:Shape;
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.shapeContainer = new Shape();
			this.container.addChild(this.shapeContainer);
			return this.container;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var state:MarkerPointerStyleState = MarkerPointerStyleState(this.currentState);
			
			var percentH:Number = (state.markerStyle.isWidthAxisSizeDepend ? axis.width : 1)*state.width;
			var percentW:Number = (state.markerStyle.isHeightAxisSizeDepend ? axis.width : 1)*state.height;
			
			var g:Graphics = this.shapeContainer.graphics;
			g.clear();
			
			var isHorizontal:Boolean = LinearGaugeAxis(this.axis).isHorizontal;
			
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			var r:Number =  axis.getTargetSize();
			var w:Number = percentW*r;
			var h:Number = percentH*r;
			
			var position:Number = axis.getPosition(state.align, state.padding, 0, false);
			var pos:Point = new Point();
			axis.setPoint(position, axis.scale.transform(this.fixValueScaleOut(this.value)), pos);
			
			pos.x -= w/2;
			pos.y -= h/2;
			
			bounds.x = -w/2;
			bounds.y = -h/2;
			bounds.width = w;
			bounds.height = h;
			
			var actualPos:Point = pos.clone();
			if (isHorizontal) {
				this.shapeContainer.y = pos.y+h/2;
				switch (state.align) {
					case GaugeItemAlign.INSIDE:
						this.shapeContainer.y -= h/2;
						break;
					case GaugeItemAlign.OUTSIDE:
						this.shapeContainer.y += h/2;
				}
				this.shapeContainer.x = pos.x+w/2;
			}else {
				this.shapeContainer.x = pos.x+w/2;
				switch (state.align) {
					case GaugeItemAlign.INSIDE:
						this.shapeContainer.x -= w/2;
						break;
					case GaugeItemAlign.OUTSIDE:
						this.shapeContainer.x += w/2;
				}
				this.shapeContainer.y = pos.y+h/2;
			}
			
			var posX:Number = -w/2;
			var posY:Number = -h/2;
			
			if (state.fill != null)
				state.fill.begin(g, bounds, this._color);
			if (state.stroke != null)
				state.stroke.apply(g, bounds, this._color);
				
			ShapeDrawer.draw(g, state.shape, w, h, posX, posY);
			g.endFill();
			g.lineStyle();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, this._hatchType, this._color);
				ShapeDrawer.draw(g, state.shape, w, h, posX, posY);
				g.endFill();
			}
			
			if (state.markerStyle.autoRotate) {
				switch (state.align) {
					case GaugeItemAlign.INSIDE:
					case GaugeItemAlign.CENTER:
						this.shapeContainer.rotation = isHorizontal ? -180 : 90;
						break;
					case GaugeItemAlign.OUTSIDE:
						this.shapeContainer.rotation = isHorizontal ? 0 : -90;
						break; 
				}
			}
			this.shapeContainer.rotation += state.markerStyle.rotation; 
			
			bounds.x = actualPos.x;
			bounds.y = actualPos.y;
		}
	}
}