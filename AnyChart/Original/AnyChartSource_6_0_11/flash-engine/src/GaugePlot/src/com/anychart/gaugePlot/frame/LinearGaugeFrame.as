package com.anychart.gaugePlot.frame {
	import com.anychart.visual.corners.Corners;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public final class LinearGaugeFrame extends GaugeFrame {
		
		private var minSide:Number;
		private var fullBounds:Rectangle;
		
		override public function autoFit(gaugeRect:Rectangle):void {
			var innerThickness:Number=0;
            if(innerStroke!=null && innerStroke.background!=null && innerStroke.background.enabled) innerThickness=innerStroke.thickness;
            
            var outerThickness:Number=0;
            if(outerStroke!=null && outerStroke.background!=null && outerStroke.background.enabled) outerThickness=outerStroke.thickness;
            
            var padding:Number = this.padding;
            
            this.minSide=Math.min(gaugeRect.width,gaugeRect.height);
            var d:Number = (innerThickness+outerThickness+padding)*minSide;
            
            fullBounds = gaugeRect.clone();
            
            gaugeRect.left += d;
            gaugeRect.right -= d;
            gaugeRect.top += d;
            gaugeRect.bottom -= d;
		}
		
		override public function draw():void {
			super.draw();
			
			var innerThickness:Number=0;
            if(innerStroke!=null && innerStroke.background!=null && innerStroke.background.enabled) innerThickness=innerStroke.thickness;
            
            var outerThickness:Number=0;
            if(outerStroke!=null && outerStroke.background!=null && outerStroke.background.enabled) outerThickness=outerStroke.thickness;

            var padding:Number = this.padding;
            
            var frameThickness:Number=innerThickness+outerThickness;
            
            var bgG:Graphics = this.backgrondShape.graphics;
            var innerG:Graphics = this.innerStrokeShape.graphics;
            var outerG:Graphics = this.outerStrokeShape.graphics;
            
            var rc1:Rectangle;
            var rc2:Rectangle;
            var th:Number;
            
            if(this.background!=null && this.background.enabled)
            {
            	if(this.background.fill!=null && this.background.fill.enabled)
            		this.background.fill.begin(bgG,fullBounds);
            	if(this.background.border!=null && this.background.border.enabled)
            		this.background.border.apply(bgG,fullBounds);
            		
            	th = -padding*minSide;
            	rc1 = new Rectangle(bounds.x+th,bounds.y+th,bounds.width-th*2,bounds.height-th*2);
            	this.drawShape(bgG, minSide,rc1);
            	
            	bgG.endFill();
            	bgG.lineStyle();
            }
            
            if(outerStroke!=null && this.outerStroke.background!=null && this.outerStroke.background.enabled)
            {
            	if(this.outerStroke.background.fill!=null && this.outerStroke.background.fill.enabled)
            		this.outerStroke.background.fill.begin(outerG,fullBounds);
            	if(this.outerStroke.background.border!=null && this.outerStroke.background.border.enabled)
            		this.outerStroke.background.border.apply(outerG,fullBounds);
            	
            	th=-(frameThickness+padding)*minSide;
            	rc1 = new Rectangle(bounds.x+th,bounds.y+th,bounds.width-th*2,bounds.height-th*2);
            	th=-(innerThickness+padding)*minSide;
            	rc2 = new Rectangle(bounds.x+th,bounds.y+th,bounds.width-th*2,bounds.height-th*2); 
            	this.drawShape(outerG, minSide, rc1);
            	this.drawShape(outerG, minSide, rc2);
            	outerG.endFill();
            	outerG.lineStyle();
            }
            
            if(innerStroke!=null && this.innerStroke.background!=null && this.innerStroke.background.enabled)
            {
            	if(this.innerStroke.background.fill!=null && this.innerStroke.background.fill.enabled)
            		this.innerStroke.background.fill.begin(innerG,fullBounds);
            	if(this.innerStroke.background.border!=null && this.innerStroke.background.border.enabled)
            		this.innerStroke.background.border.apply(innerG,fullBounds);
            		
            	th=-(innerThickness+padding)*minSide;
            	rc1 = new Rectangle(bounds.x+th,bounds.y+th,bounds.width-th*2,bounds.height-th*2);
            	th=-padding*minSide;
            	rc2 = new Rectangle(bounds.x+th,bounds.y+th,bounds.width-th*2,bounds.height-th*2);
            	this.drawShape(innerG, minSide, rc1); 
            	this.drawShape(innerG, minSide, rc2);
            		  
            	innerG.endFill();
            	innerG.lineStyle();
            }
		}
		
		private function drawShape(g:Graphics, r:Number, rect:Rectangle):void {
			if (this.corners == null){
	        	g.drawRect(rect.x, rect.y, rect.width,rect.height);
            }else {
            	var c:Corners = this.corners.createCopy();
            	c.leftBottom *= r/100;
            	c.leftTop *= r/100;
            	c.rightBottom *= r/100;
            	c.rightTop *= r/100;
            	c.drawRect(g, rect);
            }
		}
	}
}