package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.MarkerStyle;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeDrawer;
	import com.anychart.visual.effects.EffectsList;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class MarkerDrawer {
		
		private var shape:Shape;
		
		public function initialize(container:DisplayObjectContainer):void {
			this.shape = new Shape();
			container.addChild(this.shape);
		}
		
		public function clear():void {
			this.shape.graphics.clear();
		}
		
		public function draw(axis:CircularGaugeAxis, 
							 marker:MarkerStyle, 
							 pointer:CircularGaugePointer,
							 align:uint, 
							 padding:Number, 
							 fill:Fill, 
							 hatchFill:HatchFill,
							 border:Stroke, 
							 effects:EffectsList):void {
			this.shape.graphics.clear();
			
			var percentW:Number = (marker.isWidthAxisSizeDepend ? axis.width : 1)*marker.width;
			var percentH:Number = (marker.isHeightAxisSizeDepend ? axis.width : 1)*marker.height;
			
			var w:Number = axis.getPixelRadius(percentW);
			var h:Number = axis.getPixelRadius(percentH);
			
			var r:Number = axis.getRadius(align, padding, 0, false);
			switch (align) {
				case GaugeItemAlign.INSIDE: r -= w/2; break;
				case GaugeItemAlign.OUTSIDE: r += w/2; break;
				//case GaugeItemAlign.CENTER: r -= w/2; break;
			}
				
			var pos:Point = new Point();
			var angle:Number = axis.scale.transform(pointer.value);
			axis.setPixelPoint(r, angle, pos);
			 
			var bounds:Rectangle = new Rectangle(pos.x, pos.y, w, h);
			 
			var g:Graphics = this.shape.graphics;
			if (marker.autoRotate)
				this.shape.rotation = align == GaugeItemAlign.OUTSIDE ? (angle - 90) : (angle+90);
			this.shape.x = pos.x;
			this.shape.y = pos.y;
			this.shape.rotation += marker.rotation;
			
			if (fill != null)
				fill.begin(g, bounds, pointer.color);
			if (border != null)
				border.apply(g, bounds, pointer.color);
			ShapeDrawer.draw(g, marker.shape, w, h, -w/2, -h/2);
			g.lineStyle();
			g.endFill();
			
			if (hatchFill != null) {
				hatchFill.beginFill(g, pointer.hatchType, pointer.color);
				ShapeDrawer.draw(g, marker.shape, w, h, -w/2, -h/2);
				g.endFill();
			}
		}
	}
}