package com.anychart.gaugePlot.frame {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.visual.corners.Corners;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public final class CircularGaugeFrameRect extends CircularGaugeFrame {
		override protected function execDrawing(g:Graphics, thickness:Number):void {
			
			var gauge:CircularGauge = CircularGauge(this.gauge);
			var r:Number = gauge.frameRadius;
			
			var rc:Rectangle;
			if (this.allowAutoFit) {
				var pixPadding:Number = this.padding*r;
			 	var pixThickness:Number = thickness*r;
	            r += pixPadding+pixThickness;
	            rc = new Rectangle(gauge.pixPivotPoint.x - r, gauge.pixPivotPoint.y - r, r*2,r*2);
   			}else {
				var m:Margin = new Margin();
				var t:Number = this.getThickness();
				m.left = t - thickness;
				m.right = t - thickness;
				m.top = t - thickness;
				m.bottom = t - thickness;
				
	            var fullRect:Rectangle = new Rectangle();
	            fullRect.left = gauge.bounds.x + m.left*r;
	            fullRect.top = gauge.bounds.y + m.top*r;
	            fullRect.right = gauge.bounds.right - m.right*r;
	            fullRect.bottom = gauge.bounds.bottom - m.bottom*r;
	            
	            rc = fullRect;
	        	g.drawRect(fullRect.x, fullRect.y, fullRect.width,fullRect.height);
   			}
   			
   			if (this.corners == null){
	        	g.drawRect(rc.x, rc.y, rc.width,rc.height);
            }else {
            	var c:Corners = this.corners.createCopy();
            	c.leftBottom *= r/100;
            	c.leftTop *= r/100;
            	c.rightBottom *= r/100;
            	c.rightTop *= r/100;
            	c.drawRect(g, rc);
            }
		}
	}
}