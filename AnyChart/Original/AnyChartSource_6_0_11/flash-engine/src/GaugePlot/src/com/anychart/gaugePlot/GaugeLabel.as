package com.anychart.gaugePlot {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	internal final class GaugeLabel {
		
		private var info:TextElementInformation;
		private var container:Sprite;
		private var gaugeBounds:Rectangle;
		private var style:Style;
		private var color:uint;
		internal var underPointers:Boolean;
		
		private function getNormalState():GaugeLabelStyleState {
			return GaugeLabelStyleState(this.style.normal);
		}
		
		public function GaugeLabel() {
			this.color = 0;
			this.underPointers = false;
		}
		
		public function deserialize(data:XML, stylesList:XML, localStyles:StylesList, resources:ResourcesLoader):void {
			var styleName:String = data.@style == undefined ? "anychart_default" : String(data.@style).toLowerCase(); 
			
			var styleNodeName:String = "label_style";
			if (data.@color != undefined) this.color = SerializerBase.getColor(data.@color);
			if (data.@under_pointers != undefined) this.underPointers = SerializerBase.getBoolean(data.@under_pointers);
			data.@color = undefined;
			var styleData:XML = data;
			var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, styleNodeName, styleData ,styleName);
			this.style = new Style(GaugeLabelStyleState);
			this.style.deserialize(actualStyleXML,resources);
			this.style.setDynamicColors(this.color);
			this.style.setDynamicGradients(this.color);
		}
		
		public function initialize(gaugeBounds:Rectangle):DisplayObject {
			this.container = new Sprite();
			this.gaugeBounds = gaugeBounds;
			this.info = this.getNormalState().label.createInformation();
			this.info.formattedText = this.getNormalState().label.text;
			this.getNormalState().label.getBounds(this.info);
			return this.container;
		}
		public function draw():void {
			this.getNormalState().label.drawGaugeText(this.container, gaugeBounds.x, gaugeBounds.y, gaugeBounds.width, gaugeBounds.height, this.info, this.color, 0, true);
		}
		public function resize(gaugeBounds:Rectangle):void {
			this.container.graphics.clear();
			this.draw();
		}
	}
}