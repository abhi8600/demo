package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.styles.circular.NeedleStyle;
	import com.anychart.visual.effects.EffectsList;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.fill.FillType;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class NeedleDrawer {

		internal var pixelRadius:Number;
		internal var pixelBaseRadius:Number;
		internal var pixelThickness:Number;
		internal var pixelPointThickness:Number;
		internal var pixelPointRadius:Number;

		public function draw(container:Sprite, 
							 pointer:CircularGaugePointer, 
							 needle:NeedleStyle, 
							 axis:CircularGaugeAxis,
							 fill:Fill, hatchFill:HatchFill, stroke:Stroke, effects:EffectsList):void {
			var angle:Number = 0;
			var dAngle:Number = 0;
			if (fill != null && fill.type == FillType.GRADIENT && fill.gradient.type == GradientType.LINEAR) {
				dAngle = fill.gradient.angle;
				fill.gradient.angle += angle;
			}
			
			angle *= Math.PI/180;
			
			pixelRadius = axis.getPixelRadius((needle.isRadiusAxisSizeDepend ? axis.width : 1)*needle.radius);
			pixelBaseRadius = axis.getPixelRadius((needle.isBaseRadiusAxisSizeDepend ? axis.width : 1)*needle.baseRadius);
			pixelThickness = axis.getPixelRadius((needle.isThicknessAxisSizeDepend ? axis.width : 1)*needle.thickness);
			pixelPointThickness = axis.getPixelRadius((needle.isPointThicknessAxisSizeDepend ? axis.width : 1)*needle.pointThickness);
			pixelPointRadius = axis.getPixelRadius((needle.isPointRadiusAxisSizeDepend ? axis.width : 1)*needle.pointRadius);
			
			var center:Point = new Point(0,0);
			
			var pt1:Point = new Point();
			var pt2:Point = new Point();
			var pt3:Point = new Point();
			var pt4:Point = new Point();
			var pt5:Point = new Point();
			
			var sin:Number = Math.sin(angle);
			var cos:Number = Math.cos(angle);
			
			var dx:Number = pixelThickness/2*sin;
			var dy:Number = pixelThickness/2*cos;
			
			var tmp:Point = new Point();
			tmp.x = center.x + pixelBaseRadius*Math.cos(angle);
			tmp.y = center.y + pixelBaseRadius*Math.sin(angle);
			
			pt1.x = tmp.x - dx;
			pt1.y = tmp.y + dy;
			
			pt2.x = tmp.x + dx;
			pt2.y = tmp.y - dy;
			
			dx = pixelPointThickness/2*sin;
			dy = pixelPointThickness/2*cos;
			
			tmp.x = center.x + (pixelRadius - pixelPointRadius)*Math.cos(angle);
			tmp.y = center.y + (pixelRadius - pixelPointRadius)*Math.sin(angle);
			
			pt3.x = tmp.x - dx;
			pt3.y = tmp.y + dy;
			
			pt4.x = tmp.x + dx;
			pt4.y = tmp.y - dy;
			
			pt5.x = center.x + pixelRadius*Math.cos(angle);
			pt5.y = center.y + pixelRadius*Math.sin(angle);
			
			var pts:Array = [pt1, pt2, pt4, pt5, pt3];
			
			var bounds:Rectangle = new Rectangle();
			
			bounds.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x, pt5.x);
			bounds.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y, pt5.y);
			bounds.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x, pt5.x) - bounds.x;
			bounds.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y, pt5.y) - bounds.y;
			
			var g:Graphics = container.graphics;
			
			if (fill != null)
				fill.begin(g, bounds, pointer.color);
			if (stroke != null)
				stroke.apply(g, bounds, pointer.color);
			this.drawNeedle(g, pts);
			
			g.lineStyle();
			if (hatchFill != null) {
				if (hatchFill != null)
					hatchFill.beginFill(g, pointer.hatchType, pointer.color);
				this.drawNeedle(g,pts);
			}
			
			if (fill != null && fill.type == FillType.GRADIENT && fill.gradient.type == GradientType.LINEAR) {
				fill.gradient.angle = dAngle;
			}
			
			container.x = CircularGauge(axis.gauge).pixPivotPoint.x;
			container.y = CircularGauge(axis.gauge).pixPivotPoint.y;
			pointer.value=(pointer.value > axis.scale.maximum) ? axis.scale.maximum : (pointer.value < axis.scale.minimum) ? axis.scale.minimum : pointer.value;
			container.rotation = axis.scale.transform(pointer.value);
		}
		
		private function drawNeedle(g:Graphics, pts:Array):void {
			g.moveTo(pts[0].x, pts[0].y);
			g.lineTo(pts[1].x, pts[1].y);
			g.lineTo(pts[2].x, pts[2].y);
			g.lineTo(pts[3].x, pts[3].y);
			g.lineTo(pts[4].x, pts[4].y);
			g.lineTo(pts[0].x, pts[0].y);
			g.endFill();
		}
	}
}