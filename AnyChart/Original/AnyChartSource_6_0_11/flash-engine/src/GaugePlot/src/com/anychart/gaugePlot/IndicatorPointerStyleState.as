package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.pointers.styles.linear.MarkerPointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	internal final class IndicatorPointerStyleState extends MarkerPointerStyleState {
		
		public var x:Number;
		public var y:Number;
		public var hAlign:uint;
		public var vAlign:uint;
		
		public function IndicatorPointerStyleState(style:Style) {
			super(style);
			this.hAlign = HorizontalAlign.CENTER;
			this.vAlign = VerticalAlign.CENTER;
			this.x = .5;
			this.y = .5;
		}
	}
}