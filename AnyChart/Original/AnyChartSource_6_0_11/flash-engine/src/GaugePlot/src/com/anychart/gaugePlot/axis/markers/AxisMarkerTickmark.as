package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.Tickmark;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	
	public final class AxisMarkerTickmark extends Tickmark {
		
		public var isWidthAxisSizeDepend:Boolean;
		public var isHeightAxisSizeDepend:Boolean;
		
		public function deserializeMarker(data:XML, resources:ResourcesLoader, style:IStyle):void {
			this.deserialize(data, NaN, resources, style);
			if (data.@length != undefined) {
				this.isWidthAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@length);
				this.width = (this.isWidthAxisSizeDepend) ? GaugePlotSerializerBase.getAxisSizeFactor(data.@length) : GaugePlotSerializerBase.getSimplePercent(data.@length);
			}
			if (data.@width != undefined) {
				this.isHeightAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@width);
				this.height = (this.isHeightAxisSizeDepend) ? GaugePlotSerializerBase.getAxisSizeFactor(data.@width) : GaugePlotSerializerBase.getSimplePercent(data.@width);
			}
			
		}
	}
}