package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.pointers.styles.MarkerStyle;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	
	public final class KnobPointerStyleState extends CircularGaugePointerStyleState {
		
		public var knob:KnobStyle;
		public var marker:MarkerStyle;
		public var needle:NeedleStyle;
		
		public function KnobPointerStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data.fill[0] = null;
			data.hatch_fill[0] = null;
			data.border[0] = null;
			
			data = super.deserialize(data, resources);
			if (SerializerBase.isEnabled(data.knob_background[0])) {
				this.knob = new KnobStyle();
				this.knob.deserialize(data.knob_background[0], resources, this.style);
			}
			if (SerializerBase.isEnabled(data.marker[0])) {
				this.marker = new MarkerStyle(true);
				this.marker.deserialize(data.marker[0], resources, this.style);
			}
			if (SerializerBase.isEnabled(data.needle[0])) {
				this.needle = new NeedleStyle(true);
				this.needle.deserialize(data.needle[0], resources, this.style);
			}
			return data;
		}
	}
}