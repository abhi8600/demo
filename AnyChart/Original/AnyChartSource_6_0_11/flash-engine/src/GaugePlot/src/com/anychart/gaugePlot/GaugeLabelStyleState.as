package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	
	internal final class GaugeLabelStyleState extends StyleState {
		internal var label:GaugeBaseTextElement;
		
		public function GaugeLabelStyleState(style:Style) {
			super(style);
			this.label = new GaugeBaseTextElement();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.label.deserialize(data, resources, this.style);
			return data;
		}

	}
}