package com.anychart.gaugePlot.elements {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.tooltip.ItemTooltipElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	public final class TooltipElement extends ItemTooltipElement {
		public function TooltipElement() {
			super();
			this.enabled = true;
		}
		
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			this.deserializeStyleFromNode(stylesList, data, resources);
		}
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new TooltipElement();
			super.createCopy(element);
			return element;
		}
	}
}