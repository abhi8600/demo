package com.anychart.gaugePlot.visual {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.BackgroundBase;
	
	public final class BackgroundBasedStroke implements IStyleSerializableRes {
		public var thickness:Number;
		public var background:BackgroundBase;
		
		public function BackgroundBasedStroke() {
			this.thickness = .05;
			this.background = new BackgroundBase();
		}
		
		public function deserialize(node:XML, resources:ResourcesLoader, style:IStyle = null):void {
			this.background.deserialize(node, resources, style);
			if (this.background.enabled && node.@thickness != undefined)
				this.thickness = GaugePlotSerializerBase.getSimplePercent(node.@thickness);
		}
	}
}