package com.anychart.gaugePlot.events
{
	import com.anychart.formatters.IFormatablePointPointerEvents;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	
	public class GaugePointerTransormation implements IFormatablePointPointerEvents
	{
		private var _point:Object;
		function GaugePointerTransormation(pointer:GaugePointer)
		{
			_point = {};
			_point.series = pointer.gauge.name;
			if (pointer.name!=null) _point.name = pointer.name;
			else _point.name="";
			_point.value = pointer.value;
			_point.Attributes = {};
			for (var attr:String in pointer.customAttributes)
				_point.Attributes[attr.substr(1,attr.length-1)] = pointer.customAttributes[attr];
		}
		
		public function isDateTimeToken(token:String):Boolean{return false;}
		public function getTokenValue(token:String):* { 
			if (token.indexOf('%Name') == 0) 
				return _point.name;
			if (token.indexOf('%SeriesName') == 0) 
				return _point.series;
			if (token.indexOf('%Value') == 0) 
				return _point.value;
			return;
		}
		public function serialize(res:Object = null):Object
		{
			return _point;
		}
		
		public function serializeToEvent():Object {
			var res:Object = this.serialize();
			return res;
		}
	}
}