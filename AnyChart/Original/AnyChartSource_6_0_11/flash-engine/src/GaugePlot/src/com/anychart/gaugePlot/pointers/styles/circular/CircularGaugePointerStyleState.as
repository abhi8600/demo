package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.utils.StringUtils;
	
	public class CircularGaugePointerStyleState extends GaugePointerStyleState {
		
		public var cap:CapStyle;
		
		public function CircularGaugePointerStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			
			if (SerializerBase.isEnabled(data.cap[0])) {
				this.cap = new CapStyle();
				
				if (data.@hatch_type != undefined) {
					var hatchType:String = data.@hatch_type;
					var children:XMLList = data.cap[0]..*;
					var childCnt:int = children.length();
					for each (var childNode:XML in children) {
						if (childNode.name() == "hatch_fill" && childNode.@type != undefined)
							childNode.@type = StringUtils.replace(String(childNode.@type).toLowerCase(),"%hatchtype",hatchType)
					}
				}
				
				this.cap.deserialize(data.cap[0], resources, this.style);
			}
			return data;
		}
	}
}