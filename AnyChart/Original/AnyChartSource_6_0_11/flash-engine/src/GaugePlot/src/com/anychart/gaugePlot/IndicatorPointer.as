package com.anychart.gaugePlot {
	import com.anychart.IAnyChart;
	import com.anychart.gaugePlot.elements.LabelElement;
	import com.anychart.gaugePlot.elements.TooltipElement;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.styles.linear.MarkerPointerStyleState;
	import com.anychart.gaugePlot.visual.shape.ShapeDrawer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	import flash.display.Graphics;
	
	internal final class IndicatorPointer extends GaugePointer {
		override protected function getStyleNodeName():String { return "indicator_pointer_style"; }
		override protected function getStyleStateClass():Class { return MarkerPointerStyleState; }
		override protected function getInlineStyleName():String { return null; }
		
		private var width:Number = .1;
		private var height:Number = .1;
		private var x:Number = .5;
		private var y:Number = .5;
		
		public function IndicatorPointer() {
			this.selectable = true;
			this.editable = false;
			this.useHandCursor = true;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, localStyles:StylesList, stylesList:XML, chart:IAnyChart):void {
			if (SerializerBase.isEnabled(data.label[0]))
				this.label = new LabelElement();
			if (SerializerBase.isEnabled(data.tooltip[0]))
				this.tooltip = new TooltipElement();
			if (data.@color != undefined) {
				this.color = SerializerBase.getColor(data.@color);
				data.@color = undefined;
			}
			super.deserialize(data, resources, localStyles, stylesList, chart);
			if (data.@width != undefined) this.width = GaugePlotSerializerBase.getSimplePercent(data.@width);
			if (data.@height != undefined) this.height = GaugePlotSerializerBase.getSimplePercent(data.@height);
			if (data.@x != undefined) this.x = GaugePlotSerializerBase.getSimplePercent(data.@x);
			if (data.@y != undefined) this.y = GaugePlotSerializerBase.getSimplePercent(data.@y);
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			var state:MarkerPointerStyleState = MarkerPointerStyleState(this.currentState);
			
			this.bounds.x = this.gauge.bounds.x + this.x*this.gauge.bounds.width;
			this.bounds.y = this.gauge.bounds.y + this.y*this.gauge.bounds.height;
			this.bounds.width = this.gauge.bounds.width*this.width;
			this.bounds.height = this.gauge.bounds.height*this.height;
			
			if (!GaugePlotSerializerBase.shapeIsRectangle(state.shape))
				 this.bounds.width = this.bounds.height = Math.min(this.bounds.width,this.bounds.height);
			this.bounds.x -= this.bounds.width/2;
			this.bounds.y -= this.bounds.height/2;
			
			var g:Graphics = this.container.graphics;
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
			
			ShapeDrawer.draw(g, state.shape, this.bounds.width, this.bounds.height,this.bounds.x,this.bounds.y);
			g.endFill();
			g.lineStyle();
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, 0, this.color);
				ShapeDrawer.draw(g, state.shape, this.bounds.width, this.bounds.height,this.bounds.x,this.bounds.y);
				g.endFill();
			}
		}
		
	}
}