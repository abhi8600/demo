package com.anychart.gaugePlot.axis {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeDrawer;
	import com.anychart.gaugePlot.visual.shape.ShapeType;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.BackgroundBase;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class Tickmark {
		public var background:BackgroundBase;
		
		internal var padding:Number;
		internal var align:uint;
		
		public var width:Number;
		public var height:Number;
		
		internal var shape:uint;
		public var rotation:Number;
		public var autoRotate:Boolean;
		
		internal var container:Sprite;
		
		public function Tickmark() {
			this.padding = 0;
			this.align = GaugeItemAlign.CENTER;
			this.width = .05;
			this.height = .05;
			this.background = new BackgroundBase();
			this.rotation = 0;
			this.autoRotate = true;
		}
		
		public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader, style:IStyle = null):void {
			this.background.deserialize(data, resources, style);
			if (!this.background.enabled) return;
			if (data.@padding != undefined) this.padding = GaugePlotSerializerBase.getSimplePercent(data.@padding);
			if (data.@length != undefined) this.width = GaugePlotSerializerBase.getAxisSizeDependValue(data.@length, axisWidth);
			if (data.@width != undefined) this.height = GaugePlotSerializerBase.getAxisSizeDependValue(data.@width, axisWidth);
			if (data.@shape != undefined) this.shape = GaugePlotSerializerBase.getShapeType(data.@shape);
			if (data.@align != undefined) this.align = GaugePlotSerializerBase.getAlign(data.@align); 
			if (data.@rotation != undefined) this.rotation = SerializerBase.getNumber(data.@rotation);
			if (data.@auto_rotate != undefined) this.autoRotate = SerializerBase.getBoolean(data.@auto_rotate);
			if (!GaugePlotSerializerBase.shapeIsRectangle(this.shape))
				this.width = this.height = Math.min(this.width, this.height);
				
			if (this.shape == ShapeType.LINE) {
				this.background.fill = null;
				this.background.hatchFill = null;
			}
		}
		
		public function initialize():DisplayObject {
			this.container = new Sprite();
			if (this.background.effects != null)
				this.container.filters = this.background.effects.list;
			return this.container;
		}
		
		public function draw(x:Number, y:Number, width:Number, height:Number, color:uint = 0,useContainerGraphics:Boolean = true):DisplayObject {
			
			var g:Graphics;
			var s:Shape = null;
			if (useContainerGraphics) {
				g = this.container.graphics;
			}else {
				s = new Shape();
				s.x = x + width/2;
				s.y = y + height/2;
				g = s.graphics;
				this.container.addChild(s);
				
				x = -width/2;
				y = -height/2;
			}
			
			var bounds:Rectangle = new Rectangle(x, y, width, height);
			
			if (this.background.fill != null)
				this.background.fill.begin(g, bounds, color);
			
			if (this.background.border != null)
				this.background.border.apply(g, bounds, color);
				
			ShapeDrawer.draw(g, this.shape, width, height, x, y);
			
			g.lineStyle();
			if (this.background.fill != null)
				g.endFill();
				
			if (this.background.hatchFill != null) {
				this.background.hatchFill.beginFill(g, 0, color);
				ShapeDrawer.draw(g, this.shape, width, height, x, y);
				g.endFill();
			}
				
			bounds = null;
			
			return s;
		}
		
		public function clear():void {
			this.container.graphics.clear();
			while (this.container.numChildren > 0)
				this.container.removeChildAt(0);
		}
	}
}