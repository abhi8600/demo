package com.anychart.gaugePlot.visual.shape {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.elements.marker.MarkerDrawer;
	
	import flash.display.Graphics;
	
	public final class ShapeDrawer {
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(ShapeDrawer);
			return true;
		}
		
		public static function clear():void {
			instance = null;
		}
		
		private static var instance:ShapeDrawer;
		
		public static function draw(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			if (instance == null)
				instance = new ShapeDrawer();
			instance.drawShape(g, type, width, height, dx, dy);
		} 
		
		private function drawShape(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			switch (type) {
				case ShapeType.PENTAGON:
					this.drawPentagon(g, type, width, height, dx, dy);
					return;
				case ShapeType.RECTANGLE:
					this.drawRectangle(g, type, width, height, dx, dy);
					return;
				case ShapeType.TRAPEZOID:
					this.drawTrapezoid(g, type, width, height, dx, dy);
					return;
				case ShapeType.TRIANGLE:
					this.drawTriangle(g, type, width, height, dx, dy);
					return;
				case ShapeType.LINE:
					this.drawLine(g, type, width, height, dx, dy);
					return;
				case ShapeType.H_LINE:
					this.drawHLine(g, type, width, height, dx, dy);
					return;
				default:
					MarkerDrawer.draw(g, type, Math.min(width, height), dx, dy);
					return;					
			}
		}
		
		private static var pentagonCos:Array /* of Number */ = [
																1+Math.cos((2/5-.5)*Math.PI), 
																1+Math.cos((4/5-.5)*Math.PI),
																1+Math.cos((6/5-.5)*Math.PI),
																1+Math.cos((8/5-.5)*Math.PI),
																1+Math.cos(1.5*Math.PI)];
																
		private static var pentagonSin:Array /* of Number */ = [
																1+Math.sin((2/5-.5)*Math.PI), 
																1+Math.sin((4/5-.5)*Math.PI),
																1+Math.sin((6/5-.5)*Math.PI),
																1+Math.sin((8/5-.5)*Math.PI),
																1+Math.sin(1.5*Math.PI)];
		
		private function drawPentagon(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			var r:Number = Math.min(width, height)/2;
			g.moveTo(dx + r*pentagonCos[0], dy + r*pentagonSin[0]);
			for (var i:uint = 1;i<5;i++) {
				g.lineTo(dx + r*pentagonCos[i], dy + r*pentagonSin[i]);
			}
			g.lineTo(dx + r*pentagonCos[0], dy + r*pentagonSin[0]);
		}
		
		private function drawRectangle(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			g.drawRect(dx, dy, width, height);
		}
		
		private function drawTrapezoid(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			var d:Number = width/3;
			g.moveTo(dx + d, dy);
			g.lineTo(dx + width - d, dy);
			g.lineTo(dx + width, dy+height);
			g.lineTo(dx, dy + height);
			g.lineTo(dx + d, dy);
		}
		
		private function drawTriangle(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			g.moveTo(dx + width/2, dy);
			g.lineTo(dx, dy+height);
			g.lineTo(dx + width, dy + height);
			g.lineTo(dx + width/2, dy);
		}
		
		private function drawLine(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			g.moveTo(dx + width/2, dy);
			g.lineTo(dx + width/2, dy+height);
		}
		
		private function drawHLine(g:Graphics, type:uint, width:Number, height:Number, dx:Number, dy:Number):void {
			g.moveTo(dx, dy + height/2);
			g.lineTo(dx + width, dy+height/2);
		}
	}
}