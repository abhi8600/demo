package com.anychart.mapPlot.areaSeries {
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.mapPlot.multiPointSeries.MultiPoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	internal final class AreaPoint extends MultiPoint {
		override protected function drawShape():void {
			var g:Graphics = this.container.graphics;
			var style:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			if (this.linearGeoPoints.length == 0) return;
			if (style.stroke != null)
				style.stroke.apply(g, null, this.color); 
			if (style.fill != null)
				style.fill.begin(g, null, this.color);
			this.drawConnectorLine(g);
			g.endFill();
			g.lineStyle();
			if (style.hatchFill != null) {
				style.hatchFill.beginFill(g, this.hatchType, this.color);
				this.drawConnectorLine(g);
				g.endFill();
			}
		}
		
		override public function applyProjection():void {
			for (var i:int = 0;i<this.geoPoints.length;i++) {
				var pt:Point = new Point();
				var geoPt:Point = this.geoPoints[i];
				space.linearize( geoPt.x, geoPt.y, pt);
				this.linearGeoPoints.push(pt);
			}
		}
		
		private function drawConnectorLine(g:Graphics):void {
			var startPixPt:Point = new Point();
			var pixPt:Point = new Point();
			this.space.transformLinearized(this.linearGeoPoints[0], startPixPt);
			g.moveTo(startPixPt.x, startPixPt.y);
			for (var i:int = 1;i<this.linearGeoPoints.length;i++) {
				this.space.transformLinearized(this.linearGeoPoints[i], pixPt);
				g.lineTo(pixPt.x, pixPt.y);
			}
			g.lineTo(startPixPt.x, startPixPt.y);
		}
	}
}