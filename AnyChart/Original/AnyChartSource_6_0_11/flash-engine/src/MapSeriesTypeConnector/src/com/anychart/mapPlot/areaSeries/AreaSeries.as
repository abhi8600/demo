package com.anychart.mapPlot.areaSeries {
	import com.anychart.mapPlot.multiPointSeries.MultiSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class AreaSeries extends MultiSeries {
		override public function createPoint():BasePoint {
			return new AreaPoint();
		}
	}
}