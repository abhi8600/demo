package com.anychart.mapPlot.multiPointSeries {
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	
	public class MultiSeries extends BaseSeries {
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);

			var ptsCnt:int = data.point.length();
			var pt:BasePoint = this.createPoint();
			pt.index = 0;
			pt.series = this;
			pt.settings = this.settings;
			pt.deserialize(data);
			
			pt.color = this.color;
			pt.hatchType = this.hatchType;
			
			this.setElements(pt);
			
			pt.createContainer();
			pt.initializeStyle();
			pt.initializeElementsStyle();
			
			pt.initialize();
			this.plot.drawingPoints.push(pt);
			
			delete data.point;
			this.points.push(pt);
		}

	}
}