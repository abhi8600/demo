package {
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	
	public class AnyChartSite extends AnyChartRunnerBase {
		
		private static const DOMAINS_LIST:Array = ["http://anychart.com/", 
												  "http://www.anychart.com/",
												  "http://dev.anychart.com/",
												  "http://anychart.apex-evangelists.com",
												  "http://www.anychart.apex-evangelists.com",
												  "http://apex.shellprompt.net/"];
												  
		public function AnyChartSite() {
			
			var loaderInfo:LoaderInfo;
			var preloader:Class = null;
			try {
				preloader = Class(getDefinitionByName("AnyChart51HTMLPreloader"));
			}catch (e:Error) {
				try {
					preloader = Class(getDefinitionByName("AnyChartPreloaderBase"));
				}catch (e:Error) {
					try {
						preloader = Class(getDefinitionByName("AnyChartApex"));
					}catch (e:Error) {}
				}
			}
			
			if (preloader != null)
				loaderInfo = preloader.instance.loaderInfo;
			else
				loaderInfo = this.loaderInfo;
			
			checkURL (loaderInfo.loaderURL);
			checkURL (loaderInfo.url);
			
			if (this.isTrial) {
				var fmt:TextFormat = new TextFormat();
				fmt.font = 'Verdana';
				fmt.size = 60;
				
				var trialTextField:TextField = new TextField();
				trialTextField.text = "AnyChart Trial Version";
				//trialTextField.text = "loaderInfo.url";
				trialTextField.autoSize = TextFieldAutoSize.LEFT;
				trialTextField.setTextFormat(fmt);
				
				var d:BitmapData = new BitmapData(trialTextField.width, trialTextField.height, true, 0);
				d.draw(trialTextField);
				
				this.trialBitmap = new Bitmap(d);
				this.trialBitmap.alpha = 0.15;
				this.xmlFile = null;
				
				this.trialWidth = trialTextField.width;
				this.trialHeight = trialTextField.height;
			}
			
			this.xmlFile = null;
			super();
		}
		
		private var isTrial:Boolean=false;
		
		private function checkURL(path:String):void {
			if (path != null) {
				var isValidDomain:Boolean = false;
				for each (var domain:String in DOMAINS_LIST) {
					if (path.indexOf(domain) == 0) {
						isValidDomain = true;
						break;
					}
				}
				if (!isValidDomain)
					this.isTrial=true;
				else 
					this.isTrial=false;
			}
		}
			
		private var trialBitmap:Bitmap;
		private var mainContainer:DisplayObjectContainer;
		
		private var trialWidth:Number;
		private var trialHeight:Number;
		
		override public function createEngine(mainContainer:DisplayObjectContainer, paramsContainer:LoaderInfo):void {
			super.createEngine(mainContainer, paramsContainer);
			this.mainContainer = mainContainer;
			if (this.isTrial) {
				this.setBitmapSize();
				mainContainer.addChild(this.trialBitmap);
			}
		}
		
		override public function resizeHandler(event:Event):void {
			this.setBitmapSize();
			super.resizeHandler(event);
			this.setBitmapSize();
		}
		
		private function setBitmapSize():void {
			if (mainContainer == null || !this.isTrial) return;
			var w:Number = stage.stageWidth;
			var h:Number = stage.stageHeight;
			
			this.trialBitmap.scaleX = 1; 
			this.trialBitmap.scaleY = 1;
			
			var scale:Number
			if (w/h < this.trialWidth/this.trialHeight)
				scale = w / this.trialWidth;
			else
				scale = h / this.trialHeight;
			
			this.trialBitmap.scaleX = scale; 
			this.trialBitmap.scaleY = scale;
			
			this.trialBitmap.x = (w - this.trialBitmap.width)/2;
			this.trialBitmap.y = (h - this.trialBitmap.height)/2;
		}
	}
}