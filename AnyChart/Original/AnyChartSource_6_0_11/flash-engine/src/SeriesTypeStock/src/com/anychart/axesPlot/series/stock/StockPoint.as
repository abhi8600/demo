package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	internal class StockPoint extends AxesPlotPoint {
		
		public var open:Number;
		public var close:Number;
		public var high:Number;
		public var low:Number;
		
		protected var isUp:Boolean;
		
		public function StockPoint() {
			super();
			this.high = NaN;
			this.low = NaN;
			this.open = NaN;
			this.close = NaN;			
		}
		
		//-------------------------------------------------------------------
		//					DESERIALIZATION
		//-------------------------------------------------------------------

		override protected function checkMissing(data:XML):void {
			this.high = this.deserializeStockValue(data, "@high","@h");
			this.low = this.deserializeStockValue(data, "@low","@l");
			this.open = this.deserializeStockValue(data, "@open","@o");
			this.close = this.deserializeStockValue(data, "@close","@c");
			this.isMissing = isNaN(this.high) || isNaN(this.low) || isNaN(this.open) || isNaN(this.close);
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "open" || field == "high" || field == "low" || field == "close";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				open: this.open, 
				high: this.high, 
				low: this.low,
				close: this.close
			}
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.open != undefined) this.open = this.deserializeValueFromXML(newValue.open);
			if (newValue.close != undefined) this.close = this.deserializeValueFromXML(newValue.close);
			if (newValue.high != undefined) this.high = this.deserializeValueFromXML(newValue.high);
			if (newValue.low != undefined) this.low = this.deserializeValueFromXML(newValue.low);
			
			this.isMissing = isNaN(this.high) || isNaN(this.low) || isNaN(this.open) || isNaN(this.close);
			this.isUp = this.close > this.open;
		}
		
		override public function checkScaleValue():void {
			if (!this.isMissing) {
				var valueAxis:Axis = StockSeries(this.series).valueAxis;
				valueAxis.checkScaleValue(this.high);
				valueAxis.checkScaleValue(this.low);
				valueAxis.checkScaleValue(this.open);
				valueAxis.checkScaleValue(this.close);
			}
		}
		
		override protected function deserializeValue(data:XML):void {
			if (!this.isMissing) {
				this.isUp = this.close > this.open;
				var valueAxis:Axis = StockSeries(this.series).valueAxis;
				valueAxis.checkScaleValue(this.high);
				valueAxis.checkScaleValue(this.low);
				valueAxis.checkScaleValue(this.open);
				valueAxis.checkScaleValue(this.close);
			}
			super.deserializeValue(data);
		}
		
		private function deserializeStockValue(data:XML, fullAttrName:String, shortAttrName:String):Number {
			if (data[fullAttrName] != undefined) return this.deserializeValueFromXML(data[fullAttrName]);
			if (data[shortAttrName] != undefined) return this.deserializeValueFromXML(data[shortAttrName]);
			return NaN;
		} 

		//-------------------------------------------------------------------
		//					TOKENS
		//-------------------------------------------------------------------
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%High' || token == "%Hight") return this.high;
			if (token == '%Low') return this.low;
			if (token == '%Open') return this.open;
			if (token == '%Close') return this.close;
			return super.getPointTokenValue(token);
		}
		
		//-------------------------------------------------------------------
		//					DRAWING
		//-------------------------------------------------------------------
		
		protected var lowPt:Point;
		protected var highPt:Point;
		
		protected function getWidth():Number {
			if (AxesPlot(this.plot).isScatter)
				return (this.global.isPercentPointScetterWidth ? this.plot.getBounds().width : 1)*this.global.pointScetterWidth/2; 
			return this.clusterSettings.clusterWidth/2;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.calculatePixelValues();
			this.drawStock(this.container.graphics);
		}
		
		protected function calculatePixelValues():void {
			this.highPt = new Point();
			this.lowPt = new Point();
			var series:StockSeries = StockSeries(this.series);
			series.valueAxis.transform(this, this.high, this.highPt);
			series.valueAxis.transform(this, this.low, this.lowPt);
			series.argumentAxis.transform(this, this.x, this.highPt);
			series.argumentAxis.transform(this, this.x, this.lowPt);
			
			if (this.z) {
				this.z.transform(this.highPt);
				this.z.transform(this.lowPt);
			}
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawStock(this.container.graphics);
		} 
		
		protected function drawStock(g:Graphics):void {
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.High = this.high;
			res.Low = this.low;
			res.Open = this.open;
			res.Close = this.close;
			return res;
		}
		
		
	}
}