package com.anychart.axesPlot.series.stock {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class OHLCSeries extends StockSeries {
		
		override public function createPoint():BasePoint {
			return new OHLCPoint();
		}
	}
}