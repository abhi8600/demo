package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.series.stock.styles.OHLCStyleState;
	import com.anychart.axesPlot.series.stock.styles.OHLCStyleStateEntry;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	
	internal final class OHLCPoint extends StockPoint {
		
		private var openPt:Point;
		private var closePt:Point;
		
		private var centerOpenPt:Point;
		private var centerClosePt:Point;
		
		override protected function calculatePixelValues():void {
			super.calculatePixelValues();
			
			this.openPt = new Point();
			this.closePt = new Point();
			this.centerOpenPt = new Point();
			this.centerClosePt = new Point();
			
			var w:Number = this.getWidth();
			
			var series:StockSeries = StockSeries(this.series);

			series.valueAxis.transform(this, this.open, this.openPt);
			series.valueAxis.transform(this, this.open, this.centerOpenPt);
			series.valueAxis.transform(this, this.close, this.closePt);
			series.valueAxis.transform(this, this.close, this.centerClosePt);
			
			series.argumentAxis.transform(this, this.x, this.openPt, -w);
			series.argumentAxis.transform(this, this.x, this.centerOpenPt);
			series.argumentAxis.transform(this, this.x, this.closePt, w);
			series.argumentAxis.transform(this, this.x, this.centerClosePt);
			
			if (this.z) {
				this.z.transform(this.openPt);
				this.z.transform(this.closePt);
				this.z.transform(this.centerOpenPt);
				this.z.transform(this.centerClosePt);
			}
			
			this.bounds.x = Math.min(this.openPt.x, this.closePt.x, this.highPt.x, this.lowPt.x, this.centerOpenPt.x, this.centerClosePt.x); 
			this.bounds.y = Math.min(this.openPt.y, this.closePt.y, this.highPt.y, this.lowPt.y, this.centerOpenPt.y, this.centerClosePt.y);
			this.bounds.width = Math.max(this.openPt.x, this.closePt.x, this.highPt.x, this.lowPt.x, this.centerOpenPt.x, this.centerClosePt.x) - this.bounds.x;
			this.bounds.height = Math.max(this.openPt.y, this.closePt.y, this.highPt.y, this.lowPt.y, this.centerOpenPt.y, this.centerClosePt.y) - this.bounds.y;
		}
		
		override protected function drawStock(g:Graphics):void {
			var state:OHLCStyleStateEntry = this.isUp ? OHLCStyleState(this.styleState).up : OHLCStyleState(this.styleState).down;
			if (state.line != null) {
				state.line.apply(g, null, this.color);
				g.moveTo(this.highPt.x, this.highPt.y);
				g.lineTo(this.lowPt.x, this.lowPt.y);
				g.lineStyle();
			} 
			
			if (state.openLine != null) {
				state.openLine.apply(g, null, this.color);
				g.moveTo(this.openPt.x, this.openPt.y);
				g.lineTo(this.centerOpenPt.x, this.centerOpenPt.y);
				g.lineStyle();
			}
			
			if (state.closeLine != null) {
				state.closeLine.apply(g, null, this.color);
				g.moveTo(this.closePt.x, this.closePt.y);
				g.lineTo(this.centerClosePt.x, this.centerClosePt.y);
			}
			
			if (this.hasPersonalContainer)
				this.container.filters = (state.effects != null) ? state.effects.list : null;
		}
	}
}