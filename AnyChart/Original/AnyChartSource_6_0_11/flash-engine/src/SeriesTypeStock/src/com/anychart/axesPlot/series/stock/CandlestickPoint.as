package com.anychart.axesPlot.series.stock {
	import com.anychart.axesPlot.series.stock.styles.CandlestickStyleState;
	import com.anychart.axesPlot.series.stock.styles.CandlestickStyleStateEntry;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	internal final class CandlestickPoint extends StockPoint {
		
		private var openCloseRect:Rectangle;
		
		private static var openPt:Point = new Point();
		private static var closePt:Point = new Point();
		
		override protected function calculatePixelValues():void {
			super.calculatePixelValues();
			this.openCloseRect = new Rectangle();
			var series:StockSeries = StockSeries(this.series);
			var w:Number = this.getWidth();
			
			series.valueAxis.transform(this, this.open, CandlestickPoint.openPt);
			series.valueAxis.transform(this, this.close, CandlestickPoint.closePt);
			series.argumentAxis.transform(this, this.x, CandlestickPoint.openPt, -w);
			series.argumentAxis.transform(this, this.x, CandlestickPoint.closePt, w);
			
			if (this.z) {
				this.z.transform(CandlestickPoint.openPt);
				this.z.transform(CandlestickPoint.closePt);
			}
			
			this.openCloseRect.x = Math.min(CandlestickPoint.openPt.x, CandlestickPoint.closePt.x);
			this.openCloseRect.y = Math.min(CandlestickPoint.openPt.y, CandlestickPoint.closePt.y);
			this.openCloseRect.width = Math.max(CandlestickPoint.openPt.x, CandlestickPoint.closePt.x) - this.openCloseRect.x;
			this.openCloseRect.height = Math.max(CandlestickPoint.openPt.y, CandlestickPoint.closePt.y) - this.openCloseRect.y;
			
			this.bounds.x = Math.min(CandlestickPoint.openPt.x, CandlestickPoint.closePt.x, this.highPt.x, this.lowPt.x); 
			this.bounds.y = Math.min(CandlestickPoint.openPt.y, CandlestickPoint.closePt.y, this.highPt.y, this.lowPt.y);
			this.bounds.width = Math.max(CandlestickPoint.openPt.x, CandlestickPoint.closePt.x, this.highPt.x, this.lowPt.x) - bounds.x;
			this.bounds.height = Math.max(CandlestickPoint.openPt.y, CandlestickPoint.closePt.y, this.highPt.y, this.lowPt.y) - bounds.y;
		}
		
		override protected function drawStock(g:Graphics):void {
			var state:CandlestickStyleStateEntry = this.isUp ? CandlestickStyleState(this.styleState).up : CandlestickStyleState(this.styleState).down; 
			if (state.line != null) {
				state.line.apply(g, null, this.color);
				g.moveTo(this.highPt.x, this.highPt.y);
				g.lineTo(this.lowPt.x, this.lowPt.y);
				g.lineStyle();
			}
			
			if (state.fill != null || state.border) {
				g.moveTo(this.openCloseRect.x, this.openCloseRect.y);
				if (state.fill)
					state.fill.begin(g, this.openCloseRect, this.color);
				if (state.border)
					state.border.apply(g, this.openCloseRect, this.color);
				g.drawRect(this.openCloseRect.x, this.openCloseRect.y, this.openCloseRect.width, this.openCloseRect.height);
				g.lineStyle();
			}
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, this.hatchType, this.color);
				g.drawRect(this.openCloseRect.x, this.openCloseRect.y, this.openCloseRect.width, this.openCloseRect.height);
				g.endFill();
			}
			
			if (this.hasPersonalContainer) {
				this.container.filters = state.effects ? state.effects.list : null;
			}
		}
	}
}