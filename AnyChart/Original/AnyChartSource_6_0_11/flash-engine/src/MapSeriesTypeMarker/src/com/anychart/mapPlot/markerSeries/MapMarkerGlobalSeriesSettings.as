package com.anychart.mapPlot.markerSeries {
	import com.anychart.data.SeriesType;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	
	public final class MapMarkerGlobalSeriesSettings extends GlobalSeriesSettings {
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		}
		
		//-----------------------------------------------------------
		//			Elements
		//-----------------------------------------------------------
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (marker == null) 
				marker = this.createMarkerElement();
			if (data != null) 
				marker.deserializeGlobalAsSeries(data, styles, resources);
			else 
				marker.style = new Style(marker.getStyleStateClass());
			marker.enabled = true;
			return marker;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function getSettingsNodeName():String {
			return "marker_series";
		}
		
		override public function getStyleNodeName():String {
			return null;
		}
		
		override public function getStyleStateClass():Class {
			return null;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:MarkerSeries = new MarkerSeries();
			series.type = SeriesType.MARKER;
			return series; 
		}
	}
}