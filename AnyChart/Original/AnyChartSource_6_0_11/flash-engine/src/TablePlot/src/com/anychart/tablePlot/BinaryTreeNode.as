package com.anychart.tablePlot {
	internal class BinaryTreeNode {
		public var parent:BinaryTreeNode = null;
		public var left:BinaryTreeNode = null;
		public var right:BinaryTreeNode = null;
		public var value:CellDimValue;
		
		public function BinaryTreeNode(value:CellDimValue) {
			this.value = value;
		}

		public function addChild(child:BinaryTreeNode):void {
			if (child.value.value > this.value.value)
				if (this.right == null){
					this.right = child;
					child.parent = this;
				} else
					this.right.addChild(child);
			else
				if (this.left == null) {
					this.left = child;
					child.parent = this;
				} else
					this.left.addChild(child); 
		}
	}
}