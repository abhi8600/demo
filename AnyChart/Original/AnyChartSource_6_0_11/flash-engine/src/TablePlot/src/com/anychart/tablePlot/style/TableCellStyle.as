package com.anychart.tablePlot.style {
	import com.anychart.formatters.FormatsParser;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.text.BaseTextElement;

	public final class TableCellStyle extends BackgroundBaseStyleState {
		public function TableCellStyle(style:Style) {
			super(style);
			
			this.label = new BaseTextElement();
			
		}
		
		public var label:BaseTextElement;
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			
			this.label.deserialize(data, resources, this.style);
			
			if (data.format[0] != null) { 
				this.label.text = SerializerBase.getCDATAString(data.format[0]);
				this.label.isDynamicText = FormatsParser.isDynamic(this.label.text);
				if (this.label.isDynamicText)
					this.label.dynamicText = FormatsParser.parse(this.label.text);
			}
			
			return data;
		}
	}
}