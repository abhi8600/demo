package com.anychart.tablePlot {
	import flash.geom.Rectangle;
	
	public interface ITableCell {
		
		function getMinBounds():Rectangle;
	}
}