package com.anychart.tablePlot {
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.tablePlot.data.TableGlobalSeriesSettings;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	public final class TablePlot extends SeriesPlot {
		
		//--------------------------------------------------------------------------
		//		Drawing
		//--------------------------------------------------------------------------
		
		private var dict:Object = new Object();
		private var dictLength:uint = 0;
		private var seriesAsRows:Boolean;
		private var dimXValuesSorted:Array;
		private var dimYValuesSorted:Array;
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			var container:DisplayObject = super.initialize(bounds);
			this.seriesAsRows = TableGlobalSeriesSettings(this.settings[SeriesType.TABLE]).seriesAsRows;
			
			// Creating points dictionary
			var seriesCount:uint = series.length;
			var seriesLen:uint;
			var s:BaseSeries;
			var point:BasePoint;
			var dimXMinValues:Array = new Array();
			var dimYMinValues:Array = new Array();
			var tmp:Rectangle;
			if (this.seriesAsRows){
				for (var i:uint = 0; i < seriesCount; i++) {
					s = BaseSeries(this.series[i]);
					seriesLen = s.points.length;
					for (var j:uint = 0; j < seriesLen; j++) {
						point = BasePoint(s.points[j]);
						if (this.dict[point.name] == undefined)
							this.dict[point.name] = this.dictLength++;
						tmp = ITableCell(point).getMinBounds();
						if (dimXMinValues[j] == null)
							dimXMinValues[j] = new CellDimValue(tmp.width, j);
						else if (CellDimValue(dimXMinValues[j]).value < tmp.width)
							CellDimValue(dimXMinValues[j]).value = tmp.width;
						if (dimYMinValues[i] == null)
							dimYMinValues[i] = new CellDimValue(tmp.height, i);
						else if (CellDimValue(dimYMinValues[i]).value < tmp.height)
							CellDimValue(dimYMinValues[i]).value = tmp.height;
					}
				}
			} else {
				for (i = 0; i < seriesCount; i++) {
					s = BaseSeries(this.series[i]);
					seriesLen = s.points.length;
					for (j = 0; j < seriesLen; j++) {
						point = BasePoint(s.points[j]);
						if (this.dict[point.name] == undefined)
							this.dict[point.name] = this.dictLength++;
						tmp = ITableCell(point).getMinBounds();
						if (dimXMinValues[i] == null)
							dimXMinValues[i] = new CellDimValue(tmp.width, i);
						else if (CellDimValue(dimXMinValues[i]).value < tmp.width)
							CellDimValue(dimXMinValues[i]).value = tmp.width;
						if (dimYMinValues[j] == null)
							dimYMinValues[j] = new CellDimValue(tmp.height, j);
						else if (CellDimValue(dimYMinValues[j]).value < tmp.height)
							CellDimValue(dimYMinValues[j]).value = tmp.height;
					}
				}
			}
			
			// Sorting
			var tree:BinaryTreeWork = new BinaryTreeWork();
			tree.parseArray(dimXMinValues);	
			this.dimXValuesSorted = tree.fillArray(false);		
			tree.clear();
			tree.parseArray(dimYMinValues);
			this.dimYValuesSorted = tree.fillArray(false);
			
			this.countBounds();
			
			return container;
		}
		
		override public function draw():void {
			super.draw();
		}
		
		override protected function resizeSeries():void {
			this.countBounds();
			super.resizeSeries();
		}
		
		private function countBounds():void {
			
			// Computing cells' bounds
			var len:uint = this.dimXValuesSorted.length;
			var dimXValues:Array = new Array(len);
			var dimWidthValues:Array = new Array(len);
			len = this.dimYValuesSorted.length;
			var dimYValues:Array = new Array(len);
			var dimHeightValues:Array = new Array(len);
			distributeSpace(this.dimXValuesSorted, dimWidthValues, dimXValues, this.bounds.width); 
			distributeSpace(this.dimYValuesSorted, dimHeightValues, dimYValues, this.bounds.height);
			
			var seriesCount:uint = series.length;
			var seriesLen:uint;
			var s:BaseSeries;
			var pointBounds:Rectangle;
			var i:uint;
			var j:uint;
			if (this.seriesAsRows){
				for (i = 0; i < seriesCount; i++) {
					s = BaseSeries(this.series[i]);
					seriesLen = s.points.length;
					for (j = 0; j < seriesLen; j++) {
						pointBounds = BasePoint(s.points[j]).bounds;
						pointBounds.x = dimXValues[j];
						pointBounds.y = dimYValues[i];
						pointBounds.width = dimWidthValues[j];
						pointBounds.height = dimHeightValues[i];
					}
				}
			} else {
				for (i = 0; i < seriesCount; i++) {
					s = BaseSeries(this.series[i]);
					seriesLen = s.points.length;
					for (j = 0; j < seriesLen; j++) {
						pointBounds = BasePoint(s.points[j]).bounds;
						pointBounds.x = dimXValues[i];
						pointBounds.y = dimYValues[j];
						pointBounds.width = dimWidthValues[i];
						pointBounds.height = dimHeightValues[j];
					}
				}
			}
		}
		
		private function distributeSpace(minValues:Array, targetSizes:Array, targetCoords:Array, space:Number):void {
			if (minValues == null)
				return;
			var i:uint;
			var tmpDim:Number;
			var tmp:CellDimValue;
			var currDim:Number;
			var recountTmpDim:Boolean = true; 
			var len:uint = minValues.length;
			var unDistributedSpace:Number = space;
			for (i = 0; i < len; i++){
				tmp = CellDimValue(minValues[i]);
				if (unDistributedSpace <= 0) {
					targetSizes[tmp.num] = tmp.value;
					continue;
				}			
				if (recountTmpDim) {
					tmpDim = unDistributedSpace / (len - i);
					recountTmpDim = false;
				}
				if (tmp.value > tmpDim) {
					recountTmpDim = true;
					currDim = tmp.value;
				} else 
					currDim = tmpDim;
				targetSizes[tmp.num] = currDim;
				unDistributedSpace -= currDim;
			}	
			
			targetCoords[0] = 0;
			len = len - 1;
			for (i = 0; i < len; i++)
				targetCoords[i + 1] = targetCoords[i] + targetSizes[i];
		}
		
		//--------------------------------------------------------------------------
		//		Templates
		//--------------------------------------------------------------------------
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate().copy(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		private static var templatesManager:BaseTemplatesManager;
		
		override public function getTemplatesManager():BaseTemplatesManager {
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			return templatesManager;
		}		
		
		//--------------------------------------------------------
		//					Series 
		//--------------------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings{
			return new TableGlobalSeriesSettings();
		}
		
		override public function getSeriesType(type:*):uint {
			return SeriesType.TABLE;
		}
	}
}