package com.anychart.tablePlot.data {
	import com.anychart.data.SeriesType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.tablePlot.style.TableCellStyle;

	public final class TableGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		public function TableGlobalSeriesSettings() {
			super();
			
			this.seriesAsRows = false;
		}
		
		public var seriesAsRows:Boolean;
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:TableSeries = new TableSeries();
			series.type = SeriesType.TABLE;
			return series;
		}
		
		override public function getSettingsNodeName():String {
			return "table_series";
		}
		
		override public function getStyleNodeName():String {
			return "table_style";
		}
		
		override public function getStyleStateClass():Class {
			return TableCellStyle;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			
			if (data.@series_as_rows != undefined) this.seriesAsRows = SerializerBase.getBoolean(data.@series_as_rows);
		}
	}
}