package com.anychart.axesPlot.series.lineSeries {
	import flash.geom.Point;
	
	internal final class StepForwardAreaPoint extends AreaPoint {
		override protected function createDrawingPoints():void {
			this.drawingPoints = [];
			
			var tmp:Point;
			var tmp1:Point;
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].pixValuePt;
				tmp1 = this.isHorizontal ? new Point(tmp.x, (tmp.y + this.pixValuePt.y)/2) : new Point((tmp.x + this.pixValuePt.x)/2, tmp.y);
				this.drawingPoints.push(tmp1);
				
				if (this.previousStackPoint == null) {
					if (this.isHorizontal)
						this.bottomStart.y = tmp1.y;
					else 
						this.bottomStart.x = tmp1.x;
				}
				
				tmp1 = this.isHorizontal ? new Point(tmp.x, this.pixValuePt.y) : new Point(this.pixValuePt.x, tmp.y);
				this.drawingPoints.push(tmp1);
			}else if (this.previousStackPoint == null) {
				if (this.isHorizontal)
					this.bottomStart.y = this.pixValuePt.y;
				else 
					this.bottomStart.x = this.pixValuePt.x;
			}
			this.drawingPoints.push(this.pixValuePt);

			if (this.hasNext) {
				this.series.points[this.index+1].initializePixValues();
				this.series.points[this.index+1].needCalcPixValue = false;
				tmp = this.series.points[this.index+1].pixValuePt;
				tmp1 = this.isHorizontal ? new Point(this.pixValuePt.x, (tmp.y + this.pixValuePt.y)/2)
										  : new Point((tmp.x + this.pixValuePt.x)/2, this.pixValuePt.y);
				this.drawingPoints.push(tmp1);
				if (this.previousStackPoint == null) {
					if (this.isHorizontal)
						this.bottomEnd.y = tmp1.y;
					else 
						this.bottomEnd.x = tmp1.x;
				}
			}else if (this.previousStackPoint == null) {
				if (this.isHorizontal)
					this.bottomEnd.y = this.pixValuePt.y;
				else 
					this.bottomEnd.x = this.pixValuePt.x;
			}
		}
	}
}