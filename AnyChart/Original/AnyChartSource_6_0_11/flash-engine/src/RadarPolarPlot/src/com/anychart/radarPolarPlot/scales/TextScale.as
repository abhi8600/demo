package com.anychart.radarPolarPlot.scales {
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;

	public final class TextScale extends BaseScale {
		
		override public function calculate():void {
			
			this.minimum = this.dataRangeMinimum;
			this.maximum = this.dataRangeMaximum + 1;
			this.minorInterval = 1;
			this.baseValue = this.minimum;
			
			//var isLabelsCalculated:Boolean = false;
			if (this.isMajorIntervalAuto)
				this.majorInterval = 1;
			/* if () {
				if (this.axis.labels != null) {
					
					this.calculateMajorTicksCount();
					var maxLabels:int = this.axis.labels.calcMaxLabelsCount();
					this.majorInterval = Math.ceil((this.maximum - this.minimum)/maxLabels);
					isLabelsCalculated = true;
				}else {
					this.majorInterval = int((this.maximum - this.minimum - 1)/12) + 1;
				}
			}else { */
				this.majorInterval = int(this.majorInterval);
				if (this.majorInterval <= 0)
					this.majorInterval = 1;
			//}
			
			if (this.isMinorIntervalAuto)
				this.minorInterval = this.majorInterval;
			
			/* if (!isLabelsCalculated && this.axis.labels != null)
				this.axis.labels.initMaxLabelSize(); */
			
			this.calculateTicksCount();
			this.calculateMinorStart();
		}
		
		override public function deserializeValue(value:*):* {
			return SerializerBase.getString(value);
		}
	}
}