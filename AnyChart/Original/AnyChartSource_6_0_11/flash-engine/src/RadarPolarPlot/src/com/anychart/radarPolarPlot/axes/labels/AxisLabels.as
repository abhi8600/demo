package com.anychart.radarPolarPlot.axes.labels {
	
	import com.anychart.formatters.FormatsParser;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.Axis;
	import com.anychart.radarPolarPlot.axes.RotatedAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
		
	public class AxisLabels extends BaseTextElement {
		
		protected var axis:Axis;
		private var isTextScale:Boolean;
		public var padding:Number;
		public var isInsideLabels:Boolean;
		public var showFirst:Boolean;
		public var showLast:Boolean;
		public var space:Number;
		public var allowOverlap:Boolean;
		
		public function AxisLabels(axis:Axis){
			this.axis = axis;
			this.space = 0;
			this.isTextScale = isTextScale;
			this.padding = 5;
			this.isInsideLabels = false;
			this.showFirst = true;
			this.showLast = true;
			this.allowOverlap = false;
			super();
		}
		
		private function checkFormat(str:String):String {
			return str;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (!this.enabled) return;
			if (data.format[0] != null) { 
				this.text = this.getCDATAString(data.format[0]);
				this.text = this.checkFormat(this.text);
				this.isDynamicText = FormatsParser.isDynamic(this.text);
				if (this.isDynamicText)
					this.dynamicText = FormatsParser.parse(this.text);
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
			
			if (data.@allow_overlap != undefined) this.allowOverlap = SerializerBase.getBoolean(data.@allow_overlap);
			
			if (data.@position != undefined)
				this.isInsideLabels = SerializerBase.getEnumItem(data.@position) == "inside";
				
			if (data.@show_first_label != undefined)
				this.showFirst = SerializerBase.getBoolean(data.@show_first_label);
			if (data.@show_last_label != undefined)
				this.showLast = SerializerBase.getBoolean(data.@show_last_label);
				
			if (data.@position != undefined)
				this.isInsideLabels = SerializerBase.getEnumItem(data.@position) == "inside";	
		}
		
		private var tmpValue:Number;
		
		
		private var cache:Array;
		public function getTextInfo(majorIntervalIndex:Number, useCache:Boolean = true):TextElementInformation {
			var info:TextElementInformation;
			this.tmpValue = this.axis.scale.getMajorIntervalValue(majorIntervalIndex);
			var rotationAdditive:Number = (this.axis.scale.localTransform(this.tmpValue) + RadarPolarPlot(this.axis.plot).getYAxisCrossing());
			if (useCache){
				if (this.cache == null)
					this.cache = [];
				if (this.cache[majorIntervalIndex] == null){
					info = this.createInformation();
					info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.formatAxisLabel, this.isDateTimeToken) : this.text;
					/*if (realRelativeRotation){
						this.rotation += rotationAdditive;
						this.getBounds(info);
						this.rotation -= rotationAdditive;
					} else */
					this.getBounds(info);
					this.cache[majorIntervalIndex] = info;
				} else 
					info = this.cache[majorIntervalIndex];
			} else {
				info = this.createInformation();
				info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.formatAxisLabel, this.isDateTimeToken) : this.text;
				/*if (realRelativeRotation){
					this.rotation += rotationAdditive;
					this.getBounds(info);
					this.rotation -= rotationAdditive;
				} else */
				this.getBounds(info);
			}
			return info;
		}
		
		public function setRotation(rotation:Number):void {
			this.rotation = rotation;
			this.initRotation();
		}
		
		public function drawLabel(g:Graphics ,x:Number, y:Number, info:TextElementInformation, pixValue:Number):void {
			this.draw(g, x, y, info, 0, 0);
		}
		
		private function formatAxisLabel(token:String):* {
			return this.axis.formatAxisLabelToken(token, this.tmpValue);
		}
		
		private function isDateTimeToken(token:String):Boolean {
			return this.axis.isDateTimeToken(token);
		}
	}
}