package com.anychart.radarPolarPlot.axes.tickmarks {
	
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.stroke.Stroke;
	
	public class Tickmark extends Stroke {
		public var size:Number = 10;
		public var inside:Boolean = false;
		public var outside:Boolean = true;
		
		override public function deserialize(data:XML, style:IStyle = null):void {
			super.deserialize(data, style);
			if (data.@size != undefined) this.size = SerializerBase.getNumber(data.@size);
			if (data.@inside != undefined) this.inside = SerializerBase.getBoolean(data.@inside);
			if (data.@outside != undefined) this.outside = SerializerBase.getBoolean(data.@outside);
		}
	}
}