package com.anychart.radarPolarPlot.axes{
	
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.grids.RadarPolarGrid;
	import com.anychart.radarPolarPlot.axes.tickmarks.Tickmark;
	import com.anychart.radarPolarPlot.scales.LinearScale;
	import com.anychart.radarPolarPlot.scales.LogScale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.utils.LineEquation;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class ValueAxis extends Axis{
		
		public var angleInRadians:Number;
		private var cos:Number;
		private var sin:Number;
		public var name:String;
		
		public function ValueAxis(plot:RadarPolarPlot){
			super(plot);
			this.name = "primary";
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
		}
		
		override public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return super.formatAxisLabelToken(token,(this.scale is LogScale) ? this.scale.delinearize(scaleValue) : scaleValue);
		}
		
		override protected function createScale(scaleType:*):BaseScale {
			if (scaleType != undefined && String(scaleType).toLowerCase() == "logarithmic") return new LogScale();
			return new LinearScale();
		}
		
		public function initAngle():void {
			this.angleInRadians = this.plot.getYAxisCrossing() * Math.PI / 180;
			
			this.cos = Math.round(Math.cos(this.angleInRadians) * 1e15) / 1e15;
			this.sin = Math.round(Math.sin(this.angleInRadians) * 1e15) / 1e15;
		}
		
		override protected function getMaxMajorIntervalValue():int { return this.scale.majorIntervalsCount; }
		
		override protected function showLabel(index:int):Boolean {
			if (this.labels == null || !this.labels.enabled) return false;
			if (index == 0) return this.labels.showFirst;
			if (index == this.getMaxMajorIntervalValue()) return this.labels.showLast;
			return true;
		}
		
		override protected function drawLine():void {
			if (this.line == null || !this.line.enabled) return;
			var cx:Number = this.center.x;
			var cy:Number = this.center.y;
			var tmp:Number = Math.min(this.bounds.height, this.bounds.width);
			var x:Number = tmp * cos / 2 + cx;
			var y:Number = tmp * sin / 2 + cy;
			var bounds:Rectangle = this.countLineBounds(x, y, cx, cy, this.line.thickness, this.sin, this.cos);
			var g:Graphics = this.axisLinesContainer.graphics;
			g.moveTo(cx, cy);
			this.line.apply(g, bounds);
			g.lineTo(x, y);
			g.lineStyle();
		}
		
		override protected function drawGridLine(gridLine:RadarPolarGrid, pixValue:Number, nextValue:Number, odd:Boolean, drawLine:Boolean, majPixValue:Number = 0, majPixValueNext:Number = 0):void {
			var cx:Number = this.center.x;
			var cy:Number = this.center.y;
			var gridInterlacesGraphics:Graphics = this.gridInterlacesFillingContainer.graphics;
			var gridLinesGraphics:Graphics = this.gridLinesContainer.graphics;
			
			var rc:Rectangle = new Rectangle(cx - nextValue, cy - nextValue, nextValue + nextValue, nextValue + nextValue);
			
			if (gridLine.interlaced && pixValue != nextValue && 
				(!odd && (gridLine.evenFill || gridLine.evenHatchFill)) || 
				(odd && (gridLine.oddFill || gridLine.oddHatchFill))){
					
				if (odd){
					if (gridLine.oddFill != null && gridLine.oddFill.enabled){
						gridLine.oddFill.begin(gridInterlacesGraphics, rc);
						this.drawShape(gridInterlacesGraphics, cx, cy, pixValue, nextValue);
						gridLine.oddFill.end(gridInterlacesGraphics);
					}
					if (gridLine.oddHatchFill != null && gridLine.oddHatchFill.enabled){
						gridLine.oddHatchFill.beginFill(gridInterlacesGraphics);
						this.drawShape(gridInterlacesGraphics, cx, cy, pixValue, nextValue);
						gridInterlacesGraphics.endFill();
					}
				} else {
					if (gridLine.evenFill != null && gridLine.evenFill.enabled){
						gridLine.evenFill.begin(gridInterlacesGraphics, rc);
						this.drawShape(gridInterlacesGraphics, cx, cy, pixValue, nextValue);
						gridLine.evenFill.end(gridInterlacesGraphics);
					}
					if (gridLine.evenHatchFill != null && gridLine.evenHatchFill.enabled){
						gridLine.evenHatchFill.beginFill(gridInterlacesGraphics);
						this.drawShape(gridInterlacesGraphics, cx, cy, pixValue, nextValue);
						gridInterlacesGraphics.endFill();
					}
				}
			}
			
			if (gridLine.line != null && gridLine.line.enabled && drawLine){	
				gridLine.line.apply(gridLinesGraphics, rc);
				this.drawFront(gridLinesGraphics, cx, cy, nextValue);
				gridLinesGraphics.lineStyle();
			}
		}
		
		override protected function drawTickmark(pixvalue:Number, tickmark:Tickmark):void {
			var x:Number = pixvalue * this.cos + this.center.x;
			var y:Number = pixvalue * this.sin + this.center.y;
			var x1:Number;
			var y1:Number;
			var x2:Number;
			var y2:Number;
			var tmpSize:Number;
			var size:Number = tickmark.size;
			var dx:Number = size * this.sin;
			var dy:Number = size * this.cos;
			if (tickmark.inside){
				x1 = x + dx;
				y1 = y - dy;
			} else {
				x1 = x;
				y1 = y;
			}
			if (tickmark.outside){
				x2 = x - dx;
				y2 = y + dy;
			} else {
				x2 = x;
				y2 = y;
			}
			var rc:Rectangle = this.countLineBounds(x1, y1, x2, y2, tickmark.thickness, this.cos, this.sin);
			var g:Graphics = this.tickmarksContainer.graphics;
			g.moveTo(x1, y1);
			tickmark.apply(g, rc);
			g.lineTo(x2, y2);
			g.lineStyle();
		}		
		
		override protected function doLabelDrawing(x:Number, y:Number, info:TextElementInformation, pixValue:Number):void {
			this.labels.draw(this.labelsContainer.graphics, x, y, info, 0, 0);
		}
		
		override public function calcBounds(bounds:Rectangle):void {
			super.calcBounds(bounds);
			this.scale.setPixelRange(0, Math.min(this.bounds.height, this.bounds.width) / 2);
			this.scale.calculate();
		}
		
		override protected function setLabelPosition(info:TextElementInformation, pos:Point, pixPos:Number, leftTopCornerPos:Point):void {
			var padding:Number = this.labels.padding;
			if (this.majorTickmark != null && this.majorTickmark.enabled)
				if ((this.majorTickmark.inside && this.labels.isInsideLabels) ||
					(this.majorTickmark.outside && !this.labels.isInsideLabels))
					padding += this.majorTickmark.size;
			var dx:Number = padding * this.sin;
			var dy:Number = padding * this.cos;
			var anchor:Point = new Point(pixPos * this.cos + this.center.x, pixPos * this.sin + this.center.y);
			var line:LineEquation;
			if (pixPos == 0){
				var radius:Number = Math.min(this.bounds.height, this.bounds.width) / 2;
			 	line = LineEquation.getLineFrom2Points(new Point(radius * this.cos + this.center.x, radius * this.sin + this.center.y), this.center);
			} else
				line = LineEquation.getLineFrom2Points(anchor, this.center);
			if (!this.labels.isInsideLabels) {
				anchor.x += dx;
				anchor.y -= dy;
			} else {
				anchor.x -= dx;
				anchor.y += dy;
			}
			var labelRotationAngle:Number = this.labels.rotation;
			var corners:Array = this.getAllPoints(info.nonRotatedBounds, labelRotationAngle, new Point());
					 
			var nearestCorner:Point;
			if (!this.labels.isInsideLabels) 
				if (this.sin * this.cos <= 0)
					nearestCorner = this.getNearestPoint(line, corners, this.sin, this.cos);
				else
					nearestCorner = this.getNearestPoint(line, corners, -this.sin, -this.cos);
			else 
				if (this.sin * this.cos > 0)
					nearestCorner = this.getNearestPoint(line, corners, this.sin, this.cos);
				else
					nearestCorner = this.getNearestPoint(line, corners, -this.sin, -this.cos);
			if (nearestCorner == null)
				nearestCorner = new Point();
			leftTopCornerPos.x = anchor.x - nearestCorner.x;
			leftTopCornerPos.y = anchor.y - nearestCorner.y;
			pos.x = Math.min(corners[0].x, corners[1].x, corners[2].x, corners[3].x) + leftTopCornerPos.x;
			pos.y = Math.min(corners[0].y, corners[1].y, corners[2].y, corners[3].y) + leftTopCornerPos.y;
			
			//debug
			/* var g:Graphics = this.axisLinesContainer.graphics;
			for (var i:int = 0; i < 4; i++){
				g.beginFill(0xff00ff, 0.5);
				this.axisLinesContainer.graphics.drawCircle(corners[i].x + anchor.x, corners[i].y + anchor.y, 5);
				g.endFill();
			}
			
			g.lineStyle(5, 0xff0000, 0.5);
			g.moveTo(anchor.x - 30, line.y(anchor.x - 30));
			g.lineTo(anchor.x, line.y(anchor.x));
			g.lineStyle(5, 0x0000ff, 0.5);
			g.lineTo(anchor.x + 30, line.y(anchor.x + 30));
			g.lineStyle();
			g.beginFill(0x0000ff);
			this.axisLinesContainer.graphics.drawCircle(anchor.x, anchor.y, 5);
			g.endFill();
			g.beginFill(0x00ff00);
			this.axisLinesContainer.graphics.drawCircle(pos.x, pos.y, 4);
			g.endFill();
			g.beginFill(0xff0000);
			this.axisLinesContainer.graphics.drawCircle(nearestCorner.x + anchor.x, nearestCorner.y + anchor.y, 3);
			g.endFill(); */
		}
		
		override protected function getLabelInfo(pixValue:Number):TextElementInformation {
			return this.labels.getTextInfo(pixValue, false);
		}
		
		private function drawShape(g:Graphics, cx:Number, cy:Number, pixValue:Number, nextValue:Number):void {
			if (this.plot.drawingModeCircle)
				this.drawArc(g, cx, cy, pixValue, nextValue);
			else
				this.drawPolyLine(g, cx, cy, pixValue, nextValue);
		}
		
		private function drawArc(g:Graphics, cx:Number, cy:Number, pixValue:Number, nextValue:Number):void {
			DrawingUtils.drawArc2(g, cx, cy, 0, 360, pixValue, pixValue, 0, true);
			DrawingUtils.drawArc2(g, cx, cy, 0, 360, nextValue, nextValue, 0, true);
		}
		
		private function drawFront(g:Graphics, cx:Number, cy:Number, pixValue:Number):void {
			if (this.plot.drawingModeCircle)
				g.drawCircle(cx, cy, pixValue);
			else
				this.drawPolyLineFront(g, cx, cy, pixValue);
		}
		
		private function drawPolyLineFront(g:Graphics, cx:Number, cy:Number, pixValue:Number):void {
			var crossingAngle:Number = this.plot.getYAxisCrossing();
			var xScale:BaseScale = this.plot.getXAxis().scale;
			var majIntervals:uint = xScale.majorIntervalsCount;
			var i:uint;
			var majorInterval:Number;
			var tmp:Number;
			var tmpNext:Number;
			var nextMajorInterval:Number;
			
			for (i = 0; i < majIntervals; i++){
				majorInterval = xScale.getMajorIntervalValue(i);
				tmp = xScale.localTransform(majorInterval) + crossingAngle;
				nextMajorInterval = xScale.getMajorIntervalValue(i+1);
				tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;
				
				g.moveTo(DrawingUtils.getPointX(pixValue, tmp) + cx, DrawingUtils.getPointY(pixValue, tmp) + cy); 
				g.lineTo(DrawingUtils.getPointX(pixValue, tmpNext) + cx, DrawingUtils.getPointY(pixValue, tmpNext) + cy);
			}
		}
		
		private function drawPolyLine(g:Graphics, cx:Number, cy:Number, pixValue:Number, nextValue:Number):void {
			var crossingAngle:Number = this.plot.getYAxisCrossing();
			var xScale:BaseScale = this.plot.getXAxis().scale;
			var majIntervals:uint = xScale.majorIntervalsCount;
			var i:uint;
			var majorInterval:Number;
			var tmp:Number;
			var tmpNext:Number;
			var nextMajorInterval:Number;
			
			majorInterval = xScale.getMajorIntervalValue(i);
			tmp = xScale.localTransform(majorInterval) + crossingAngle;
			nextMajorInterval = xScale.getMajorIntervalValue(i+1);
			tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;
			
			g.moveTo(DrawingUtils.getPointX(pixValue, tmp) + cx, DrawingUtils.getPointY(pixValue, tmp) + cy); 
			
			for (i = 0; i < majIntervals; i++){
				majorInterval = xScale.getMajorIntervalValue(i);
				tmp = xScale.localTransform(majorInterval) + crossingAngle;
				nextMajorInterval = xScale.getMajorIntervalValue(i+1);
				tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;
				
				g.lineTo(DrawingUtils.getPointX(pixValue, tmpNext) + cx, DrawingUtils.getPointY(pixValue, tmpNext) + cy);
				//g.moveTo(DrawingUtils.getPointX(nextValue, tmp) + cx, DrawingUtils.getPointY(nextValue, tmp) + cy); 
				//g.lineTo(DrawingUtils.getPointX(nextValue, tmpNext) + cx, DrawingUtils.getPointY(nextValue, tmpNext) + cy);
			}
			
			majorInterval = xScale.getMajorIntervalValue(i);
			tmp = xScale.localTransform(majorInterval) + crossingAngle;
			nextMajorInterval = xScale.getMajorIntervalValue(i+1);
			tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;
			
			g.moveTo(DrawingUtils.getPointX(nextValue, tmp) + cx, DrawingUtils.getPointY(nextValue, tmp) + cy); 
			
			for (i = 0; i < majIntervals; i++){
				majorInterval = xScale.getMajorIntervalValue(i);
				tmp = xScale.localTransform(majorInterval) + crossingAngle;
				nextMajorInterval = xScale.getMajorIntervalValue(i+1);
				tmpNext = xScale.localTransform(nextMajorInterval) + crossingAngle;
				
				g.lineTo(DrawingUtils.getPointX(nextValue, tmpNext) + cx, DrawingUtils.getPointY(nextValue, tmpNext) + cy);
			}
		}
	}
}