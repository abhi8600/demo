package com.anychart.radarPolarPlot.axes {
	
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.grids.RadarPolarGrid;
	import com.anychart.radarPolarPlot.axes.labels.AxisLabels;
	import com.anychart.radarPolarPlot.axes.labels.RotatedAxisLabels;
	import com.anychart.radarPolarPlot.axes.tickmarks.Tickmark;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.utils.LineEquation;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class RotatedAxis extends Axis {
		
		public function RotatedAxis(plot:RadarPolarPlot){
			super(plot);
		}
		
		override public function initialize():void {
			super.initialize();
			this.scale.setPixelRange(0, 360);
			this.scale.calculate();
		}
		
		override protected function createScale(scaleType:*):BaseScale {
			var scale:BaseScale = super.createScale(scaleType);
			scale.maximumOffset = 0;
			return scale; 
		}
		
		override protected function createAxisLabels():AxisLabels {
			return new RotatedAxisLabels(this);
		}
		
		//-----------------------------------
		// OVERRIDING ABSTRACT METHODS
		//-----------------------------------
		
		override protected function drawLine():void {
			if (this.line == null || !this.line.enabled) return;
			var cx:Number = this.center.x;
			var cy:Number = this.center.y;
			var dim:Number = Math.min(this.bounds.height, this.bounds.width) / 2;
			var bounds:Rectangle = new Rectangle(cx - dim, cy - dim, dim + dim, dim + dim);
			var thickness:Number = this.line.thickness / 2;
			bounds.x += thickness;
			bounds.y += thickness;
			bounds.width -= thickness + thickness;
			bounds.height -= thickness + thickness;
			var g:Graphics = this.axisLinesContainer.graphics;
			this.line.apply(g, bounds);
			var crossingAngle:Number = this.plot.getYAxisCrossing();
			var radius:Number = dim - thickness; 
			if ((this.majorGrid != null && this.majorGrid.enabled && this.majorGrid.interlaced) || 
				(this.minorGrid != null && this.minorGrid.enabled && this.minorGrid.interlaced)){
				var majIntervals:uint = this.scale.majorIntervalsCount;
				var minIntervals:uint = this.scale.minorIntervalsCount;
				for (var i:uint = 0; i<majIntervals; i++){
					var majorInterval:Number = this.scale.getMajorIntervalValue(i);
					var tmp:Number = this.scale.localTransform(majorInterval);
					var nextMajorInterval:Number;
					nextMajorInterval = this.scale.getMajorIntervalValue(i+1);
					var tmpNext:Number = this.scale.localTransform(nextMajorInterval);
					if (this.minorGrid != null && this.minorGrid.enabled && this.minorGrid.interlaced)
						for (var j:uint = 0; j < minIntervals; j++){
							var tmp2:Number = this.scale.localTransform(this.scale.getMinorIntervalValue(majorInterval, j));
							var tmp2Next:Number;
						 	tmp2Next = this.scale.localTransform(this.scale.getMinorIntervalValue(majorInterval, j+1));
							DrawingUtils.drawArc2(g, cx, cy, tmp2 + crossingAngle, tmp2Next + crossingAngle, radius, radius, 0, true); 
						}
					else
						DrawingUtils.drawArc2(g, cx, cy, tmp + crossingAngle, tmpNext + crossingAngle, radius, radius, 0, true); 
				}
			} else {
				g.drawEllipse(bounds.x, bounds.y, bounds.width, bounds.height);
			}
			//g.drawCircle(cx, cy, dim - thickness / 2);
			g.lineStyle();
		}
		
		protected function getGridSectorParams(pixValue:Number, nextValue:Number, radius:Number, cx:Number, cy:Number, minorGrid:Boolean, majPixValue:Number, majPixValueNext:Number):void {
			this.angle1 = (pixValue + this.plot.getYAxisCrossing());
			this.x1 = DrawingUtils.getPointX(radius, this.angle1) + cx;
			this.y1 = DrawingUtils.getPointY(radius, this.angle1) + cy;
			
			if (pixValue == nextValue){
				this.angle2 = this.angle1;
				this.x2 = this.x1;
				this.y2 = this.y1;
			} else {
				this.angle2 = (nextValue + this.plot.getYAxisCrossing());
				this.x2 = DrawingUtils.getPointX(radius, this.angle2) + cx;
				this.y2 = DrawingUtils.getPointY(radius, this.angle2) + cy;
			}
		}
		
		protected var angle1:Number;
		protected var x1:Number;
		protected var y1:Number;
		
		protected var angle2:Number;
		protected var x2:Number;
		protected var y2:Number;
		
		override protected function drawGridLine(gridLine:RadarPolarGrid, pixValue:Number, nextValue:Number, odd:Boolean, drawLine:Boolean, majPixValue:Number = 0, majPixValueNext:Number = 0):void {
			var cx:Number = this.center.x;
			var cy:Number = this.center.y;
			var radius:Number = Math.min(this.bounds.height, this.bounds.width) / 2;
			if (this.line != null && this.line.enabled)
				radius -= this.line.thickness;
			var rc:Rectangle;
			var gridInterlacesGraphics:Graphics = this.gridInterlacesFillingContainer.graphics;
			var gridLinesGraphics:Graphics = this.gridLinesContainer.graphics;
			var plotBounds:Rectangle = RadarPolarPlot(this.plot).getBounds();
			
			this.getGridSectorParams(pixValue, nextValue, radius, cx, cy, gridLine.isMinor, majPixValue, majPixValueNext);
						
			if (gridLine.interlaced && pixValue != nextValue && 
				(!odd && (gridLine.evenFill || gridLine.evenHatchFill)) || 
				(odd && (gridLine.oddFill || gridLine.oddHatchFill))){
					
				var tmpAngle:Number;
				
				var array:Array = [new Point(cx, cy), new Point(x1, y1)];
				var halfPI:Number = 90;
				var i:int;
				var point:Point;
				for (tmpAngle = (Math.floor(angle1 / 90) - 1) * 90, i = 0; 
						tmpAngle < angle2 && i < 3; 
						tmpAngle -= halfPI, i++)
					array.push(new Point(
						DrawingUtils.getPointX(radius, tmpAngle) + cx,
						DrawingUtils.getPointY(radius, tmpAngle) + cy
					));
				array.push(new Point(x2, y2));
				rc = this.countBoundsByArray(array);
				if (odd){
					if (gridLine.oddFill != null && gridLine.oddFill.enabled){
						gridLine.oddFill.begin(gridInterlacesGraphics, plotBounds);
						this.drawShape(gridInterlacesGraphics, cx, cy, x1, y1, x2, y2,angle1, angle2, radius);
						gridLine.oddFill.end(gridInterlacesGraphics);
					}
					if (gridLine.oddHatchFill != null && gridLine.oddHatchFill.enabled){
						gridLine.oddHatchFill.beginFill(gridInterlacesGraphics);
						this.drawShape(gridInterlacesGraphics, cx, cy, x1, y1, x2, y2, angle1, angle2, radius);
						gridInterlacesGraphics.endFill();
					}
				} else {
					if (gridLine.evenFill != null && gridLine.evenFill.enabled){
						gridLine.evenFill.begin(gridInterlacesGraphics, plotBounds);
						this.drawShape(gridInterlacesGraphics, cx, cy, x1, y1, x2, y2, angle1, angle2, radius);
						gridLine.evenFill.end(gridInterlacesGraphics);
					}
					if (gridLine.evenHatchFill != null && gridLine.evenHatchFill.enabled){
						gridLine.evenHatchFill.beginFill(gridInterlacesGraphics);
						this.drawShape(gridInterlacesGraphics, cx, cy, x1, y1, x2, y2, angle1, angle2, radius);
						gridInterlacesGraphics.endFill();
					}
				}
			}
			
			if (gridLine.line != null && gridLine.line.enabled && drawLine){			
				rc = this.countLineBounds(x2, y2, cx, cy, gridLine.line.thickness, 
					DrawingUtils.getPointY(1, angle2), DrawingUtils.getPointX(1, angle2));
				gridLinesGraphics.moveTo(cx, cy);
				gridLine.line.apply(gridLinesGraphics, rc);
				gridLinesGraphics.lineTo(x2, y2);
				gridLinesGraphics.lineStyle();
			}
			
		}
		
		override protected function drawTickmark(pixvalue:Number, tickmark:Tickmark):void {
			var dim:Number = Math.min(this.bounds.height, this.bounds.width) / 2;
			var p1:Number = tickmark.inside ? dim - tickmark.size : dim;
			var p2:Number = tickmark.outside ? dim + tickmark.size : dim;
			var angle:Number = (pixvalue + this.plot.getYAxisCrossing());
			var sin:Number = DrawingUtils.getPointY(1, angle);
			var cos:Number = DrawingUtils.getPointX(1, angle);
			var x1:Number = DrawingUtils.getPointX(p1, angle) + this.center.x;
			var y1:Number = DrawingUtils.getPointY(p1, angle) + this.center.y;
			var x2:Number = DrawingUtils.getPointX(p2, angle) + this.center.x;
			var y2:Number = DrawingUtils.getPointY(p2, angle) + this.center.y;
			var rc:Rectangle = this.countLineBounds(x1, y1, x2, y2, tickmark.thickness, sin, cos);
			var g:Graphics = this.tickmarksContainer.graphics;
			g.moveTo(x1, y1);
			tickmark.apply(g, rc);
			g.lineTo(x2, y2);
			g.lineStyle();
		}
		
		override protected function doLabelDrawing(x:Number, y:Number, info:TextElementInformation, pixValue:Number):void {
			if (this.labels)
				this.labels.drawLabel(this.labelsContainer.graphics, x, y, info, pixValue); 
		}
		
		override protected function execDrawLastLabelHack():Boolean { return false; }
		
		override protected function getLabelInfo(pixValue:Number):TextElementInformation {
			return this.labels.getTextInfo(pixValue, true);
		}
		
		override public function calcBounds(bounds:Rectangle):void {
			super.calcBounds(bounds);
			this.scale.calculate();
		}
		
		override protected function setLabelPosition(info:TextElementInformation, pos:Point, pixPos:Number, leftTopCornerPos:Point):void {
			var labelPos:Point = RotatedAxisLabels(this.labels).getPosition(info, pixPos);
			pos.x = labelPos.x;
			pos.y = labelPos.y;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			if (this.line != null){
				this.line.caps = CapsStyle.NONE;
				this.line.joints = JointStyle.MITER;
			}
		}
		
		protected function drawShape(g:Graphics, cx:Number, cy:Number, x1:Number, y1:Number, x2:Number, y2:Number, angle1:Number, angle2:Number, radius:Number):void{
			g.moveTo(cx, cy);
			DrawingUtils.drawArc2(g, cx, cy, angle1, angle2, radius, radius, 0, false);
			g.lineTo(cx, cy);
		}
		
		protected function countBoundsByArray(array:Array):Rectangle {
			var len:int = array.length;
			if (len == 0)
				return null;
			var point:Point = array[0];
			var res:Rectangle = new Rectangle(point.x, point.y);
			for (var i:int = 1; i < len; i++){
				point = array[1];
				var x:int = res.x;
				var y:int = res.y;
				res.x = Math.min(x, point.x);
				res.y = Math.min(y, point.y);
				res.width = Math.max(x + res.width, point.x) - res.x;
				res.height = Math.max(y + res.height, point.y) - res.y;
			}
			return res;
		}
		
		override protected function getLabelRotation(pixValue:Number):Number {
			return this.labels.rotation;// + (this.labels.relativeRotation ? (pixValue + this.plot.getYAxisCrossing()) : 0);
		}
	}
}