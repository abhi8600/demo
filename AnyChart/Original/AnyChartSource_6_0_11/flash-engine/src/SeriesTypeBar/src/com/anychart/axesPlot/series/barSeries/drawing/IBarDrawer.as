package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.series.barSeries.BarPoint;
	import com.anychart.axesPlot.series.barSeries.BarStateStyle;
	
	import flash.display.Graphics;
	
	public interface IBarDrawer extends IBarDrawerBase {
		
		/**
		 * Рисование точки
		 * @param g
		 * @param state
		 * 
		 */
		function draw(g:Graphics,state:BarStateStyle):void;
		
		/**
		 * Перерисовка точки
		 * @param g
		 * @param state
		 * 
		 */
		function redraw(g:Graphics,state:BarStateStyle):void;
		
		/**
		 * инициализация Drawer'a
		 * @param point
		 * @param startValue
		 * @param endValue
		 * 
		 */
		function initialize(point:BarPoint ,startValue:Number, endValue:Number):void;
		
		function isCornersVisible():Boolean;
		function isBottomGradientVisible():Boolean;
	}
}