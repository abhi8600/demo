package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.settings.RangeGlobalSeriesSettings;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.DarkAquaBarStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.LightAquaBarStyle;
	import com.anychart.data.SeriesType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	
	public class RangeBarGlobalSeriesSettings extends RangeGlobalSeriesSettings implements IBarCustomWidthSupport {
		
		public var isFixedBarWidth:Boolean;
		public var fixedBarWidth:Number;
		
		public function RangeBarGlobalSeriesSettings() {
			super();
			this.isFixedBarWidth = false;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@point_width != undefined) {
				this.isFixedBarWidth = true;
				this.fixedBarWidth = SerializerBase.getNumber(data.@point_width);
			}
		}
		
		public function getBarWidth(point:AxesPlotPoint): Number {
			if (AxesPlot(this.plot).isScatter)
				return (this.isPercentPointScetterWidth ? plot.getBounds().width : 1)*this.pointScetterWidth; 
			return this.isFixedBarWidth ? this.fixedBarWidth : point.clusterSettings.clusterWidth;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BarSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return BarStateStyle;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "aqualight") {
				var pStyle:LightAquaBarStyle = new LightAquaBarStyle();
				pStyle.initialize(state);
				return pStyle;
			} else if (SerializerBase.getLString(styleNode.@name) == "aquadark") {
				var st:DarkAquaBarStyle = new DarkAquaBarStyle();
				st.initialize(state);
				return st;
			}
			
			return null;
		}
		
		override public function isProgrammaticStyle(styleNode:XML):Boolean {
			if (styleNode.@name == undefined) return false;
			var name:String = SerializerBase.getLString(styleNode.@name);
			return name == "aqualight" || name == "aquadark";
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:RangeBarSeries = new RangeBarSeries();
			series.type = SeriesType.RANGE_BAR;
			series.clusterKey = SeriesType.RANGE_BAR;
			return series;
		}
		
		override public function getSettingsNodeName():String {
			return "range_bar_series";
		}
		
		override public function getStyleNodeName():String {
			return "bar_style";
		}
	}
}