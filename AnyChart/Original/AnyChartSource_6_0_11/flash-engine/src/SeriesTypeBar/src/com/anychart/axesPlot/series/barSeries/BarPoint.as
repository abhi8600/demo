package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.data.RangeStackablePoint;
	import com.anychart.axesPlot.series.barSeries.drawing.BarDrawer;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawer;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class BarPoint extends RangeStackablePoint implements IBarAquaContainer{
		
		protected var drawer:IBarDrawer; 
		
		override public function initialize():void {
			super.initialize();
			this.drawer = IBarDrawer(BarGlobalSeriesSettings(this.global).drawingFactory.create(BarSeriesSettings(this.settings).shapeType));
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			var start:Number = isNaN(this.startStackValue) ? BarSeries(series).valueAxisStart : this.startStackValue;
			var end:Number = this.stackValue;
			var min:Number = BarSeries(this.series).valueAxis.scale.minimum;
			var max:Number = BarSeries(this.series).valueAxis.scale.maximum;
			start = Math.min(Math.max(min, start), max);
			end = Math.min(Math.max(min, end), max);
			
			this.drawer.initialize(this, start, end);
			this.drawBar(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			
			if (this.bounds == null) return;
			var g:Graphics = this.container.graphics;
			var state:BarStateStyle = BarStateStyle(this.styleState);
			
			if (this.styleState.programmaticStyle != null) {
				this.styleState.programmaticStyle.draw(this);
			}else {
				if (state.fill != null)
					state.fill.begin(g,this.bounds,this.color);
				
				if (state.hatchFill != null) {
					this.drawer.redraw(g,state);
					state.hatchFill.beginFill(g,this.hatchType,this.color);
				}
				
				if (state.stroke != null)
					state.stroke.apply(g, this.bounds,this.color);
					
				this.drawer.redraw(g,state);
				
				if (state.stroke != null)
					g.lineStyle();
			}
			
			if (this.hasPersonalContainer)
				this.container.filters = state.effects != null ? state.effects.list : null;
		} 
		
		private function drawBar(g:Graphics):void {
			var state:BarStateStyle = BarStateStyle(this.styleState);
			
			if (this.styleState.programmaticStyle != null) {
				this.styleState.programmaticStyle.draw(this);
			}else {
				if (state.fill != null)
					state.fill.begin(g,this.bounds,this.color);
				
				if (state.hatchFill != null) {
					this.drawer.draw(g,state);
					state.hatchFill.beginFill(g,this.hatchType,this.color);
				}
				
				if (state.stroke != null)
					state.stroke.apply(g, this.bounds,this.color);
					
				this.drawer.draw(g,state);
				
				if (state.stroke != null)
					g.lineStyle();
			}
			
			if (this.hasPersonalContainer)
				this.container.filters = state.effects != null ? state.effects.list : null;
		}
		
		public function drawShape(g:Graphics, state:BarStateStyle):void {
			this.drawer.draw(g, state);
		}
		
		public function isCornersCanBeVisible():Boolean { 
			return this.drawer.isCornersVisible();
		}
		
		public function isAllCornersVisible():Boolean { return false; }
		
		public function isBottomGradientVisible():Boolean {
			return this.drawer.isBottomGradientVisible();
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			if (anchor == Anchor.X_AXIS)
				super.getAnchorPoint(anchor, pos, bounds, padding);
			else
				BarDrawer(this.drawer).getAnchorPoint(anchor, pos, bounds, padding);
		}
		
	}
}