package com.anychart.axesPlot.series.barSeries.drawing{
	
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.BarPoint;
	import com.anychart.axesPlot.series.barSeries.BarStateStyle;
	import com.anychart.axesPlot.series.barSeries.IBarCustomWidthSupport;
	import com.anychart.scales.ScaleMode;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	/**
	 * Базовый клас описывающий Рисование баров
	 * @author P@zin
	 * 
	 */
	public class BarDrawer implements IBarDrawer{
		
		/**
		 * Собственно Точка 
		 */
		protected var point:BarPoint;
		
		/**
		 * Левая верхняя точка
		 */
		protected var leftTop:Point;
		
		/**
		 * Правая нижняя точка
		 */
		protected var rightBottom:Point;
		
		/**
		 * Переменная показывающая стекирован график или нет
		 */
		protected var isStacked:Boolean;
		
		public function BarDrawer ():void {
			this.leftTop = new Point;
			this.rightBottom = new Point;
		}
		
		/**
		 * метод рисующий точку
		 * @param g
		 * @param state
		 * 
		 */
		public function draw(g:Graphics, state:BarStateStyle):void{}
	
		/**
		 * метод перерисовывающий точку 
		 * @param g
		 * @param state
		 * 
		 */
		public final function redraw(g:Graphics, state:BarStateStyle):void{
			this.draw(g,state);
		}
		
		/**
		 * Метод инициализирует точку и кыширует все необходимые
		 * для рисования параметры 
		 * @param point
		 * @param startValue
		 * @param endValue
		 * 
		 */
		public function initialize(point:BarPoint, startValue:Number, endValue:Number):void{
			this.point = point;
			var series:AxesPlotSeries = AxesPlotSeries(point.series);
			var w:Number = IBarCustomWidthSupport(point.global).getBarWidth(point)/2;
			this.isStacked = this.point.category != null && this.point.category.isStackSettingsExists(AxesPlotSeries(this.point.series).valueAxis.name,this.point.series.type);
			
			series.argumentAxis.transform(point, point.x, leftTop,-w);
			series.valueAxis.transform(point, startValue, leftTop);
			series.argumentAxis.transform(point, point.x, rightBottom,w);
			series.valueAxis.transform(point, endValue, rightBottom);
			
			point.bounds.x = Math.min(leftTop.x, rightBottom.x);
			point.bounds.y = Math.min(leftTop.y, rightBottom.y);
			point.bounds.width = Math.max(leftTop.x, rightBottom.x) - point.bounds.x;
			point.bounds.height = Math.max(leftTop.y, rightBottom.y) - point.bounds.y;
		}
		
		/**
		 * Медо получает настройки стека
		 * 
		 * @return 
		 * 
		 */
		protected final function getStackSettings():StackSettings {
			return this.point.category.getStackSettings(AxesPlotSeries(this.point.series).valueAxis.name, this.point.series.type,AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED);
		}
		
		public function isCornersVisible():Boolean { return false; }
		public function isBottomGradientVisible():Boolean { return true; }
		
		public function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void{}
		
	}
}