package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.data.RangePoint;
	
	import flash.display.Graphics;
	
	public class RangeBarPoint extends RangePoint implements IBarAquaContainer{
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			BarBaseHelper.setPixelPosition(this, this.start, this.end);
			
			this.drawBar(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawBar(this.container.graphics);
		} 
		
		private function drawBar(g:Graphics):void {
			BarBaseHelper.drawBox(g, this,BarStateStyle(this.styleState));
		}
		
		public function drawShape(g:Graphics, state:BarStateStyle):void {
			if (state.corners)
				state.corners.drawRect(g, this.bounds);
			else
				g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
		}
		
		public function isCornersCanBeVisible():Boolean { return true; }
		public function isAllCornersVisible():Boolean { return true; }
		public function isBottomGradientVisible():Boolean { return true; }
	}
}