package com.anychart.piePlot.labels{
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.piePlot.data.PiePlot2DPoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	

	public class CustomLabelElement extends LabelElement{
		
		public var isExtra:Boolean;
		
		public function CustomLabelElement(){
			this.isExtra = isExtra;
			super();
		}
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (!element) element = new CustomLabelElement();
			CustomLabelElement(element).isExtra = this.isExtra;
			return super.createCopy(element);
		}
		
		override protected function getPosition(container:IElementContainer, state:BaseElementStyleState, sprite:Sprite, elementIndex:uint):Point{
			if (isExtra)
				return super.getPosition(container, state, sprite, elementIndex);
			return PiePlot2DPoint(container).getLabelPosition(state,sprite,this.getBounds(container,state,elementIndex));
		}
	}
}