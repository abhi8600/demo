package com.anychart.piePlot.data {
	import com.anychart.piePlot.PiePlot;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public final class PiePlot3DSeries extends PiePlot2DSeries {
		
		public var outerYRadius:Number;
		public var innerYRadius:Number;
		
		override public function createPoint():BasePoint {
			return new PiePlot3DPoint();
		}
		
		override protected function setPixelRadius():void {
			super.setPixelRadius();
			this.outerYRadius = PiePlot(this.plot).getYRadius(this.outerRadius);
			this.innerYRadius = PiePlot(this.plot).getYRadius(this.innerRadius);
		}
	}
}