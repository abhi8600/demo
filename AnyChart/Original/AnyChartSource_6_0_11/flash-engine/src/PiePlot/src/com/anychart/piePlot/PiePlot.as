package com.anychart.piePlot {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationType;
	import com.anychart.data.SeriesType;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.events.PointsEvent;
	import com.anychart.piePlot.animation.PieSliceAnimationTarget;
	import com.anychart.piePlot.data.PiePlot2DPoint;
	import com.anychart.piePlot.data.PiePlot3DPoint;
	import com.anychart.piePlot.data.PiePlotGlobalSeriesSettings;
	import com.anychart.piePlot.data.PiePlotSettings;
	import com.anychart.piePlot.labels.Cluster;
	import com.anychart.piePlot.labels.LabelInfo;
	import com.anychart.piePlot.legend.PiePlotLegendAdapter;
	import com.anychart.piePlot.templates.PiePlotTemplatesManager;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.categories.BaseCategoryInfo;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	
	public class PiePlot extends SeriesPlot {
		
		//3d settings
		private static const HEIGHT:Number = .2;
		private static const ASPECT:Number = .45;
		
		public var depthManager:PiePlot3DDepthManager; 
		
		public var is3D:Boolean;
		
		private var isDoughnut:Boolean;
		
		public function PiePlot() {
			this.centerPoint = new Point();
			this.isDoughnut = false;
			this.is3D = false;
			
			this._categoriesMap = {};
			this._categoriesList = [];
		}
		
		//--------------------------------------------------------
		//					Point update
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		//					Categorization
		//--------------------------------------------------------
		
		private var _categoriesMap:Object;
		public function get categoriesMap():Object { return this._categoriesMap; }
		
		private var _categoriesList:Array;
		public function get categoriesList():Array { return this._categoriesList; }
		
		public function createCategory(name:String):BaseCategoryInfo {
			if (this._categoriesMap[name] == null) {
				var info:BaseCategoryInfo = new BaseCategoryInfo(this);
				info.name = name;
				info.index = this._categoriesList.length;
				this._categoriesList.push(info);
				this._categoriesMap[name] = info;
			}
			return this._categoriesMap[name];
		}
		
		override protected function initLegendAdapter():void {
			this.legendAdapter = new PiePlotLegendAdapter(this);
		}
		
		//--------------------------------------------------------
		//					IPlot
		//--------------------------------------------------------
		
		override public function setPlotType(value:String):void {
			super.setPlotType(value);
			this.isDoughnut = value == "doughnut";
		}
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new PiePlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		private static var templatesManager:BaseTemplatesManager;
		
		override public function getTemplatesManager():BaseTemplatesManager {
			if (templatesManager == null)
				templatesManager = new PiePlotTemplatesManager();
			return templatesManager;
		}
		
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {
			super.deserializeDataPlotSettings(data, resources);
			if (data.@enable_3d_mode != undefined) this.is3D = SerializerBase.getBoolean(data.@enable_3d_mode);
			if (this.isDoughnut && this.is3D && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
				throw new FeatureNotSupportedError("3D Doughnut not supported in this swf version"); 
		}
		
		override public function checkNoData(data:XML):Boolean {
			if (data.data[0] == null) return true;
			if (data.data[0].series.length() == 0) return true;
			//if (data.data[0].series..point.length() == 0) return true;
			return false;
		}
		
		//--------------------------------------------------------
		//					Series 
		//--------------------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			return new PiePlotGlobalSeriesSettings();
		}
		
		override public function getSeriesType(type:*):uint {
			return SeriesType.PIE;
		}
		
		public function getSeriesRadius(seriesIndex:int, inner:Boolean):Number {
			
			var realSeriesCount:int = 0;
			for each (var series:BaseSeries in this.series) {
				if (series.points && series.points.length > 0) realSeriesCount++;
			}
			
			var dR:Number = (this.isDoughnut) ? (outerRadius - innerRadius) : outerRadius;
			var radius:Number = dR*(inner ? seriesIndex : (seriesIndex+1))/realSeriesCount;
			return (this.isDoughnut) ? (innerRadius + radius) : radius;
		}
		
		public function getYRadius(xRadius:Number):Number {
			return xRadius*ASPECT;
		}
		
		public function get3DHeight():Number {
			return this.outerRadius*HEIGHT;
		}
		
		public function getRadiusByFactor(f:Number):Number {
			return this.outerRadius*f;
		}
		
		public function getExplodeRadius():Number {
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			return this.outerRadius * globalSettings.explodeRadius;
		}
		
		public function getExplodeRadiusPercent():Number {
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			return globalSettings.explodeRadius;
		}
		
		public function getAllowOverlap():Boolean {
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			return globalSettings.allowLabelsOverlap;
		}
		
		public function getExplodeYRadius():Number {
			return this.getExplodeRadius()*ASPECT;
		}
		
		public var centerPoint:Point;
		public var outerRadius:Number;
		private var innerRadius:Number;
		
		private var outerYRadius:Number;
		private var innerYRadius:Number;
		
		override public function draw():void {
			super.draw();
			if (this.depthManager != null)
				this.depthManager.initSides();
		}
		
		override protected function beforeSeriesPointsDeserialization(series:BaseSeries, points:XMLList):XMLList {
			// сортировка точек
			return series.global.sortXMLPoints(points);
		}
		
		//--------------------------------------------------------
		//					Bounds 
		//--------------------------------------------------------
		
		override public function recalculateAndRedraw():void {
			if (this.is3D && this.depthManager) {
				this.depthManager.clear();
			}
			
			super.recalculateAndRedraw();
			
			if (this.is3D && this.depthManager) {
				this.depthManager.initSides();
			}
		}
		
		override protected function onRecalculateFinished():void {
			for (var i:uint = 0;i<this.series.length;i++) {
				for (var j:uint = 0;j<this.series[i].points.length;j++) { 
					this.series[i].points[j].calculateAngles();
				}
			}
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			
			this.baseBounds = bounds.clone();
			
			if (this.is3D)
				this.depthManager = new PiePlot3DDepthManager(this.dataContainer);
			
			for (var i:uint = 0;i<this.series.length;i++) {
				for (var j:uint = 0;j<this.series[i].points.length;j++) 
					this.series[i].points[j].calculateAngles();
			}
			
			var sprite:DisplayObjectContainer = DisplayObjectContainer(super.initialize(bounds));
			this.calculateBounds(bounds);
			this.setContainersPositions(bounds);
			
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			
			if (this.series.length > 1){
				globalSettings.explodeOnClick = false;
			}
			if (globalSettings.isOutside)
				this.calculateLabelsPositions(bounds);
			
			return sprite;
		}
		
		override public function calculateResize(bounds:Rectangle):void {
			this.baseBounds = bounds.clone();
			this.calculateBounds(bounds);
			super.calculateResize(bounds);
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			if (globalSettings.isOutside)
				this.calculateLabelsPositions(bounds);
		}
		
		private function calculateBounds(bounds:Rectangle):void {
			var g:Graphics = this.sprite.graphics;
			g.beginFill(0,0);
			g.drawRect(bounds.x,bounds.y,bounds.width, bounds.height);
			if (this.is3D)	
				this.calculate3DBounds(bounds);
			else
				this.caluclate2DBounds(bounds);
		}
		
		private function calculate3DBounds(bounds:Rectangle):void {
			var baseBounds:Rectangle = bounds;
			bounds = bounds.clone();
			
			var r:Number = Math.min(bounds.width/2, (bounds.height)/(2*ASPECT + HEIGHT));
			bounds.x = bounds.x + bounds.width/2 - r;
			bounds.y = bounds.y + bounds.height/2 - r*(ASPECT+HEIGHT/2);
			bounds.width = r*2;
			bounds.height = 2*r*ASPECT + r*HEIGHT;
			this.centerPoint.x = baseBounds.width/2;
			this.centerPoint.y = baseBounds.height/2 - r*HEIGHT/2;
			
			if (this.series.length > 0) {
				var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
				
				if (this.series.length == 1 && (globalSettings.hasExplodedItems || globalSettings.explodeOnClick)) {
					var c:Number = (globalSettings.radius + globalSettings.explodeRadius);
					var totalR:Number = r/c;
					this.outerRadius = totalR*globalSettings.radius;
					this.innerRadius = totalR*globalSettings.innerRadius;
				}else {
					this.outerRadius = globalSettings.radius*r;
					this.innerRadius = globalSettings.innerRadius*r;
				}
				
				this.outerYRadius = this.outerRadius*ASPECT;
				this.innerYRadius = this.innerRadius*ASPECT;
			}
		}
		
		private function caluclate2DBounds(bounds:Rectangle):void {
			var baseBounds:Rectangle = bounds;
			bounds = bounds.clone();
			var w:Number = Math.min(bounds.width, bounds.height);
			bounds.x = bounds.x + (bounds.width - w)/2;
			bounds.y = bounds.y + (bounds.height - w)/2;
			bounds.width = bounds.height = w;
			var r:Number = w/2;
			this.centerPoint.x = baseBounds.width/2;
			this.centerPoint.y = baseBounds.height/2;
			
			if (this.series.length > 0) {
				var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
				if (this.series.length == 1 && (globalSettings.hasExplodedItems || globalSettings.explodeOnClick)) {
					var c:Number = (globalSettings.radius + globalSettings.explodeRadius);
					var totalR:Number = r/c;
					this.outerRadius = totalR*globalSettings.radius;
					this.innerRadius = totalR*globalSettings.innerRadius;
				}else {
					this.outerRadius = globalSettings.radius*r;
					this.innerRadius = globalSettings.innerRadius*r;
				}
			}
		}
		
		private var criticalAngle:Number = -10; // illegal value
		public var labelRadius:Number;
		
		private function adjustRadiusByLabels (bounds:Rectangle):void{
			this.criticalAngle = -10;
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			var labelPadding:Number = globalSettings.isPercentLabelPadding ? (globalSettings.getLabelsPadding()*this.outerRadius):(globalSettings.getLabelsPadding());
			this.basicLabelsInfo = new Array();
			var connectorPadding:Number = globalSettings.connectorEnabled ? (globalSettings.connectorPadding): 0;
			
			var ji:int = this.series.length - 1;
			
			for (var i:int = 0; i<this.series[ji].points.length;i++){
				var point:PiePlot2DPoint = this.series[ji].points[i];
				if (point.hasLabel()) {
					var angle:Number = (point.startAngle+point.endAngle)*Math.PI/360;
					BaseElementStyleState(point.label.style.normal).padding = labelPadding;
					BaseElementStyleState(point.label.style.hover).padding = labelPadding;
					BaseElementStyleState(point.label.style.pushed).padding = labelPadding;
					BaseElementStyleState(point.label.style.selectedNormal).padding = labelPadding;
					BaseElementStyleState(point.label.style.selectedHover).padding = labelPadding;
					BaseElementStyleState(point.label.style.missing).padding = labelPadding;
					
					//point=checkOverlap();
					var labelRect:Rectangle = point.label.getBounds(point,point.label.style.normal,BasePoint.LABEL_INDEX);
					
					this.outerRadius=Math.min(this.outerRadius, 
						Math.abs(
							(bounds.width-(labelRect.width+connectorPadding)*2)/
							(2*(1+this.getExplodeRadiusPercent()+labelPadding/this.outerRadius)*Math.cos(angle))
						)
					);
					if (this.is3D)
						this.outerRadius=Math.min(this.outerRadius, 
							Math.abs(
								(bounds.height*ASPECT-labelRect.height*2)/
								(2*(1+this.getExplodeRadiusPercent()+labelPadding/this.outerRadius)*ASPECT*Math.sin(angle))
							)/ASPECT
						);
					else
						this.outerRadius=Math.min(this.outerRadius, 
							Math.abs(
								(bounds.height-labelRect.height*2)/
								(2*(1+this.getExplodeRadiusPercent()+labelPadding/this.outerRadius)*Math.sin(angle))
							)
						);
					var labelInfo:LabelInfo = new LabelInfo((point.startAngle+point.endAngle)/2,labelRect, point,this.basicLabelsInfo.length);
					this.basicLabelsInfo.push(labelInfo);
					point.labelIndex = this.basicLabelsInfo.length - 1;					
					if (this.criticalAngle == -10)
						this.criticalAngle = Math.acos(1/(1+labelPadding/this.outerRadius));
				}
			}
			this.labelRadius = labelPadding+this.outerRadius;
			this.innerRadius = this.outerRadius * PiePlotGlobalSeriesSettings(this.series[0].global).innerRadius;
		}
		
		
		private var basicLabelsInfo:Array = new Array();
		
		private function calculateLabelsPositions(bounds:Rectangle):void {
			this.adjustRadiusByLabels(bounds);
			var globalSettings:PiePlotGlobalSeriesSettings = PiePlotGlobalSeriesSettings(this.series[0].global);
			if (globalSettings.labelEnabled == false) return;
			if (!globalSettings.useLogicMethodOfLabelsCalc)
			{
				if (this.checkOverlap()) 
					if (this.calculateLabelsOverlapAnglesDown())
						if (this.calculateLabelsOverlapAnglesUp());
				this.calculateLabelsOverlap();
			}
			else
			{	
				this.logicMethodOfCalculateLabels();
				this.calculateLabelsOverlapLogic();
			}
		}
		
		private function logicMethodOfCalculateLabels():void
		{
			var clustersPool:Array = new Array();
			var i:int;
			var l:int = this.basicLabelsInfo.length;
			
			for (i = 0; i < l; i++)
				clustersPool.push(new Cluster(LabelInfo(this.basicLabelsInfo[i]), Math.acos(this.outerRadius/this.labelRadius) * 180 / Math.PI, this));
			
			clustersPool.sortOn("initialAngle", Array.NUMERIC);
			
			var clusters:Array = new Array();
			
			l = clustersPool.length;
			for (i = 0; i < l; i++){
				var currCluster:Cluster = clustersPool[i];
				var topCluster:Cluster = clusters.pop();
				while (topCluster != null){
					if (topCluster.checkOverlap(currCluster)){
						topCluster.merge(currCluster);
						currCluster = topCluster;
						topCluster = clusters.pop();
					} else {
						clusters.push(topCluster);
						break;
					}
				}
				clusters.push(currCluster);
			}
			
			l = clusters.length;
			for (i = 0; i < l; i++){
				clusters[i].applyPositioning();
			}
		}
		
		public function between(angle:Number, lowerBound:Number, upperBound:Number):Boolean { // верхняя не включительно
			return angle >= lowerBound && angle < upperBound; 
		}
		
		public function normalizeAngle(angle:Number, lowerBound:Number, upperBound:Number, center:Number):Number { // верхняя не включительно
			while (angle < lowerBound)
				angle += 360;
			while (angle >= upperBound)
				angle -= 360;
			if (angle == lowerBound || angle == center)
				angle += 0.01;
			return angle;
		}
		
		private function checkOverlap ():Boolean {
			for (var i:int = 0; i< this.basicLabelsInfo.length; i++){
				var labelInfo:LabelInfo = LabelInfo(this.basicLabelsInfo[i]);
				if (!calcOverlap(labelInfo))
					return true;
			}		
			return false;
		}		

		private function checkOverlapLogic ():Boolean {
			for (var i:int = 0; i< this.basicLabelsInfo.length; i++)
				for (var j:int = i; i< this.basicLabelsInfo.length; i++)
					if (calcOverlapLogic(this.basicLabelsInfo[i],this.basicLabelsInfo[j]))
						return true;
			if (calcOverlapLogic(this.basicLabelsInfo[0],this.basicLabelsInfo[this.basicLabelsInfo.length-1]))
				return true;
			return false;
		}
		
		public function getLabelAngle(index:int):Number {
			return LabelInfo(this.basicLabelsInfo[index]).angle;
		}

		private function calculateLabelsOverlap():void {
			for (var i:int = 0; i<this.basicLabelsInfo.length; i++){
				var labelInfo:LabelInfo = LabelInfo(this.basicLabelsInfo[i]);
				if (labelInfo.enabled && !calcOverlap(labelInfo)){
					if (!this.getAllowOverlap()) {
						labelInfo.enabled = false;
						labelInfo.point.enableLabel(false);
					}else {
						labelInfo.enabled = true;
						labelInfo.point.enableLabel(true);
					}
				}else{
					labelInfo.enabled = true;
					labelInfo.point.enableLabel(true);
				}
			}
			
		}
		private function calcOverlap (labeInfo:LabelInfo):Boolean{
			var position:Point = this.getLabelPosition(labeInfo,true);
			var labelRect:Rectangle = new Rectangle (position.x,position.y,labeInfo.width,labeInfo.height);
			for (var i:int = 0; i< this.basicLabelsInfo.length; i++){
				if (i!= labeInfo.index){
					var currInfo:LabelInfo = LabelInfo(this.basicLabelsInfo[i]);
					if (!currInfo.enabled) continue;
					var currPos:Point = this.getLabelPosition(currInfo,true)
					var rect:Rectangle = new Rectangle (currPos.x,currPos.y,currInfo.width,currInfo.height);
					if (labelRect.intersects(rect)) {
						return false;
					}
				}
			}
			return true;
		}
		
		private function calculateLabelsOverlapLogic():void {
			var lbl:LabelInfo;
			var lastVisible:LabelInfo;
			if (!this.getAllowOverlap() && this.basicLabelsInfo.length > 0)
			{
				for each (lbl in this.basicLabelsInfo)
					lastVisible=enabledLabel(lbl,false);
				lastVisible=enabledLabel(this.basicLabelsInfo[this.basicLabelsInfo.length-1]);
				for each (lbl in this.basicLabelsInfo)
					if (lbl!=lastVisible && !calcOverlapLogic(lbl,lastVisible))
						lastVisible=enabledLabel(lbl);
			}
			else
				for each (lbl in this.basicLabelsInfo)
					lastVisible=enabledLabel(lbl);
		}
		
		private function enabledLabel(lbl:LabelInfo, flag:Boolean=true):LabelInfo
		{
			lbl.enabled = flag;
			lbl.point.enableLabel(flag);
			return flag ? lbl : null;
		}
		
		private function calcOverlapLogic (currLabel:LabelInfo, visibleVisible:LabelInfo):Boolean{
			var currentPos:Point = this.getLabelPosition(currLabel,true);
			var labelRect:Rectangle = new Rectangle (currentPos.x,currentPos.y,currLabel.width,currLabel.height);
			var visiblePos:Point = this.getLabelPosition(visibleVisible,true)
			var rect:Rectangle = new Rectangle (visiblePos.x,visiblePos.y,visibleVisible.width,visibleVisible.height);
			return labelRect.intersects(rect);
		}
		
		private function applyHAlign(pos:Point, hAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (hAlign) {
				case HorizontalAlign.LEFT:
					pos.x -= elementSize.width + padding;
					break;
				case HorizontalAlign.RIGHT:
					pos.x += padding;
					break;
				case HorizontalAlign.CENTER:
					pos.x -= elementSize.width/2;
					break;
			}
		}
		
		private function applyVAlign(pos:Point, vAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (vAlign) {
				case VerticalAlign.TOP:
					pos.y -= elementSize.height + padding;
					break;
				case VerticalAlign.BOTTOM:
					pos.y += padding;
					break;
				case VerticalAlign.CENTER:
					pos.y -= elementSize.height/2;
					break;
			}
		}
		
		private function calculateLabelsOverlapAnglesUp():Boolean{
			var l:Number = this.basicLabelsInfo.length-1;
			
			var currentBounds:Rectangle;
			var previosBounds:Rectangle;
			
			var currentPos:Point;
			var previosPos:Point;
			
			var currentLabelInfo:LabelInfo;
			var previosLabelInfo:LabelInfo;
			
			for (var i:Number=0;i<=l;i++){
				currentLabelInfo = LabelInfo(this.basicLabelsInfo[i]);
				previosLabelInfo = i==0 ? LabelInfo(this.basicLabelsInfo[l]) : LabelInfo(this.basicLabelsInfo[i-1]);
				
				currentPos = this.getLabelPosition(currentLabelInfo,true);
				previosPos = this.getLabelPosition(previosLabelInfo,true);
				
				currentBounds = new Rectangle(currentPos.x,currentPos.y,currentLabelInfo.width,currentLabelInfo.height);
				previosBounds = new Rectangle(previosPos.x,previosPos.y,previosLabelInfo.width,previosLabelInfo.height); 
				
				if (!currentBounds.intersects(previosBounds) && currentLabelInfo.angle > previosLabelInfo.angle)
					continue;
				else {
					
					var tmpAngle:Number = Math.ceil(currentLabelInfo.angle+this.criticalAngle*180/Math.PI);
					for (var angle:Number = currentLabelInfo.angle; angle<tmpAngle;angle++){
						currentLabelInfo.angle = angle;
						
						var pos:Point = this.getLabelPosition(currentLabelInfo,true);
						currentBounds.x = pos.x;
						currentBounds.y = pos.y;
						
						if (currentBounds.intersects(previosBounds)){
							continue;
						}
						else{
							currentLabelInfo.angle = angle;
							break;
						}
					}
				}
			} 
			return this.checkOverlap();
		}
		
		private function calculateLabelsOverlapAnglesDown():Boolean {
			var l:Number = this.basicLabelsInfo.length-1;
			
			var currentBounds:Rectangle;
			var nextBounds:Rectangle;
			
			var currentPos:Point;
			var nextPos:Point;
			
			var currentLabelInfo:LabelInfo;
			var nextLabelInfo:LabelInfo;
			
			for (var i:Number=0;i<=l;i++){
				currentLabelInfo = LabelInfo(this.basicLabelsInfo[i]);
				nextLabelInfo = i==l ? LabelInfo(this.basicLabelsInfo[0]) : LabelInfo(this.basicLabelsInfo[i+1]);
				
				currentPos = this.getLabelPosition(currentLabelInfo,true);
				nextPos = this.getLabelPosition(nextLabelInfo,true);
				
				currentBounds = new Rectangle(currentPos.x,currentPos.y,currentLabelInfo.width,currentLabelInfo.height);
				nextBounds = new Rectangle(nextPos.x,nextPos.y,nextLabelInfo.width,nextLabelInfo.height); 
				
				if (!currentBounds.intersects(nextBounds) && currentLabelInfo.angle > nextLabelInfo.angle)
					continue;
				else {
					var tmpAngle:Number = Math.ceil(currentLabelInfo.angle-this.criticalAngle*180/Math.PI);
					for (var angle:Number = currentLabelInfo.angle; angle>tmpAngle;angle--){
						currentLabelInfo.angle = angle;
						
						var pos:Point = this.getLabelPosition(currentLabelInfo,true);
						currentBounds.x = pos.x;
						currentBounds.y = pos.y;
						if (currentBounds.intersects(nextBounds)){
							continue;
						}
						else{
							currentLabelInfo.angle = angle;
							break;
						}
					}
				}
			} 
			return this.checkOverlap();
		}
		
		public function getLabelPos(labelIndex:Number,need:Boolean):Point{
			return getLabelPosition(LabelInfo(this.basicLabelsInfo[labelIndex]),need);
		}
		
		public function getLabelPosition(labelInfo:LabelInfo,needAlign:Boolean):Point{
			var pos:Point = new Point();
			PiePlot2DPoint(labelInfo.point).getCenter(pos);
			if (this.is3D){
				var yRadius:Number = this.getYRadius(this.outerRadius);
				var labelPadding:Number = this.labelRadius - this.outerRadius;
				pos.x +=  (this.outerRadius + labelPadding)*Math.cos((labelInfo.angle)*Math.PI/180);
				pos.y +=  (yRadius + labelPadding)*Math.sin((labelInfo.angle)*Math.PI/180);
				pos.y+=this.get3DHeight()/2;
			} else {
				pos.x += (this.labelRadius)*Math.cos((labelInfo.angle)*Math.PI/180);
				pos.y +=  (this.labelRadius)*Math.sin((labelInfo.angle)*Math.PI/180);
			}
			
			var labelRect:Rectangle = new Rectangle (0,0,labelInfo.width,labelInfo.height);
			if (PiePlotGlobalSeriesSettings(this.series[0].global).connectorEnabled){
				if ((labelInfo.angle)>90 && (labelInfo.angle)<270)
					pos.x-=PiePlotGlobalSeriesSettings(this.series[0].global).connectorPadding;
				else
					pos.x+=PiePlotGlobalSeriesSettings(this.series[0].global).connectorPadding;
			}
			if (needAlign){
				if (Math.cos(labelInfo.angle*Math.PI/180)<0)
					this.applyHAlign(pos,HorizontalAlign.LEFT,labelRect,0);
				this.applyVAlign(pos,VerticalAlign.CENTER,labelRect,0); 
			}
			
			return pos;	
		}
		
		public function getLabelAngleInRightHalf(label:LabelInfo, y:Number):Number { // требует У центра лейбла, возвращает угол в 1 или 4 четвертях
			// если точка в левой полуокружности, надо сделать 180 - res
			var centerY:Number = this.centerPoint.y;
			y -= centerY;
			var res:Number;
			if (this.is3D){
				var yRadius:Number = this.getYRadius(this.outerRadius);
				var labelPadding:Number = this.labelRadius - this.outerRadius;
				y -= this.get3DHeight()/2;
				res = Math.asin(y / (yRadius + labelPadding)) * 180 / Math.PI;				
			} else {
				res = Math.asin(y / (this.labelRadius)) * 180 / Math.PI;
			}
			if (isNaN(res))
				return (y > 0) ? (90.01) : (-89.99);
			return res;
		} 
		
		private function getYLabelRadius():Number {
			return (this.labelRadius * ASPECT);
		}
		
		override protected function setScrollRect(newBounds:Rectangle):void { }
		
		//-----------------------------------------------------------
		//		ANIMATION
		//-----------------------------------------------------------
		
		override protected function addPointAnimation(animation:Animation, dataPoint:BasePoint):void {
			if (!animation || animation.type != AnimationType.OUTSIDE) {
				super.addPointAnimation(animation, dataPoint);
				return;
			}
			
			if (this.is3D) {
				PiePlot3DPoint(dataPoint).animation = animation;
			}else {
				this.registerPiePointAnimation(dataPoint.container, PiePlot2DPoint(dataPoint), animation);
			}
		}
		
		public function registerPiePointAnimation(container:DisplayObject, point:PiePlot2DPoint, animation:Animation):void {
			var target:PieSliceAnimationTarget = new PieSliceAnimationTarget(point);
			this.animationManager.registerCustomDisplayObject(container, this.scrollableContainer, animation, target);
		}
		
		//--- serialize
		override public function serialize(res:Object=null):Object {
			res = super.serialize(res);
			res.YRangeMax = this.cashedTokens['RangeMax'];
			res.YRangeMin = this.cashedTokens['RangeMin'];
			res.YRangeSum = this.cashedTokens['RangeSum'];
			
			res.Categories = [];
			
			var i:uint;
			for (i = 0;i<this._categoriesList.length;i++) {
				var category:BaseCategoryInfo = this._categoriesList[i];
				if (category)
					res.Categories.push(category.serialize());
			}
			
			return res;
		}
		
		override public function get selectionEventsProvider():Sprite {
			return this.sprite;
		}
		
		
		override public function deselectSelectedPoints():void {
			if (lastSelectedPoints != null && lastSelectedPoints.length > 0) {
				for each (var point:BasePoint in lastSelectedPoints) {
					if (PiePlotSettings(point.settings).exploded)
						PiePlot2DPoint(point).explodeCheck();
					point.setSelected(false);
				}
				
				this.chart.dispatchEvent(new PointsEvent(PointsEvent.MULTIPLE_POINTS_DESELECT, lastSelectedPoints));
			}
			lastSelectedPoints = null;
		}
		
		override public function finalizePointsSelection(area:Rectangle):void {
			
			if (Math.abs(area.width) < 1 && Math.abs(area.height) < 1) {
				return;
			}
			
			var pts:Array = [];
			
			for each (var series:BaseSeries in this.series) {
				for each (var point:BasePoint in series.points) {
					if (point.hitTestSelection(area, this.drawingContainer, this.selectionManager)) {
						if (!PiePlotSettings(point.settings).exploded)
							PiePlot2DPoint(point).explodeCheck();
						point.setSelected(true);
						pts.push(point);
					}
				}
			}
			lastSelectedPoints = pts;
			this.chart.dispatchEvent(new PointsEvent(PointsEvent.MULTIPLE_POINTS_SELECT, pts));
		}
		
	}
}