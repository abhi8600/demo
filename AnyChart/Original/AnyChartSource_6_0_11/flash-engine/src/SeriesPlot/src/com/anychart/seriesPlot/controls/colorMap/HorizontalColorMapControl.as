package com.anychart.seriesPlot.controls.colorMap {
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class HorizontalColorMapControl extends ColorMapControl {
		
		public static function check(data:XML):Boolean {
			return String(data.name()) == "color_swatch" 
				   && (data.@orientation == undefined ||  
				       String(data.@orientation).toLowerCase() != "vertical");
		}
		
		public function HorizontalColorMapControl() {
			super();
			this.defaultRangeItemWidth = 20;
			this.defaultRangeItemHeight = 10;
		}
		
		override protected function onBeforeDrawRanges():void {
			super.onBeforeDrawRanges();
			//this.top += (this.contentBounds.height - this.rangeItemHeight - this.labelsSpace - this.tickmarksSpace)/2 + ;
			this.top += this.normalLabelsSpace + this.normalTickmarksSpace;			
			if (this.labels != null) {
				if (this.tickmarkOnRangeCenter) {
					var w:Number = this.labels.getFirstBounds().width;
					if (w > this.rangeItemWidth)
						this.left += (w - this.rangeItemWidth)/2;
				}else {
					this.left += this.labels.getFirstBounds().width/2;
				}
			}
		}
		
		override protected function setRangeBounds(rect:Rectangle, index:int):void {
			super.setRangeBounds(rect, index);
			rect.x = this.left + this.rangeItemWidth*index;
			rect.y = this.top;
		}
		
		override protected function setLabelPosition(pos:Point, rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {
			var labelBounds:Rectangle = this.labels.getLabelBounds(index);
			pos.x = this.getX(rangeBounds, index, labelBounds.width);
			var space:Number = this.labels.padding + this.normalTickmarksSpace;
			
			if (this.labelsPosition == ColorMapLabelsPosition.NORMAL || (this.labelsPosition == ColorMapLabelsPosition.COMBINED && (labelDrawIndex%2==0)))
				pos.y = this.top - space - labelBounds.height;
			else
				pos.y = this.top + this.rangeItemHeight + space;
		}
		
		override protected function drawTickmark(rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {
			var x:Number = this.getX(rangeBounds, index, 0);
			var top:Number;
			if (this.labelsPosition == ColorMapLabelsPosition.NORMAL || (this.labelsPosition == ColorMapLabelsPosition.COMBINED && (labelDrawIndex%2==0)))
				top = this.top - this.tickmarksSize;
			else
				top = this.top + this.rangeItemHeight;
			this.container.graphics.moveTo(x, top);
			this.container.graphics.lineTo(x, top+this.tickmarksSize);
		}
		
		private function getX(rangeBounds:Rectangle, index:int, w:Number):Number {
			if (this.tickmarkOnRangeCenter)
				return rangeBounds.x + (rangeBounds.width-w)/2;
			return ((index == this.threshold.getRangeCount()) ? rangeBounds.right : rangeBounds.left) - w/2;
		}
		
		override protected function calculateLabelsSpace():void {
			this.labelsSpace = 0;
			this.normalLabelsSpace = 0;
			
			if (this.labels == null || !this.labels.enabled)
				return;
				
			switch (this.labelsPosition) {
				case ColorMapLabelsPosition.COMBINED:
					this.normalLabelsSpace = this.labels.maxEvenLabelHeight + this.labels.padding;
					this.labelsSpace = this.normalLabelsSpace;
					if ((this.threshold.getRangeCount() > 1 && this.tickmarkOnRangeCenter) || (this.threshold.getRangeCount() > 0 && !this.tickmarkOnRangeCenter))
						this.labelsSpace += this.labels.maxOddLabelHeight + this.labels.padding;
					break;
				case ColorMapLabelsPosition.NORMAL:
					this.labelsSpace = Math.max(this.labels.maxEvenLabelHeight, this.labels.maxOddLabelHeight) + this.labels.padding;
					this.normalLabelsSpace = this.labelsSpace;
					break;
					
				case ColorMapLabelsPosition.OPPOSITE:
					this.labelsSpace = Math.max(this.labels.maxEvenLabelHeight, this.labels.maxOddLabelHeight);
					break;
			}
		}
		
		override protected function calculateOptimalRangeItemHeight():Number {
			return this.defaultRangeItemHeight;
		}
		
		override protected function calculateRangeItemHeightBasedOnHeight():Number {
			return this.getContentHeight() - this.tickmarksSpace - this.labelsSpace;
		}
		
		override protected function calculateHeightBasedOnRangeItemHeight():Number {
			return this.rangeItemHeight + this.tickmarksSpace + this.labelsSpace;
		}
		
		override protected function calculateOptimalRangeItemWidth():Number {
			if (this.labels == null || !this.labels.enabled)
				return this.defaultRangeItemWidth;
				
			switch (this.labelsPosition) {
				case ColorMapLabelsPosition.COMBINED:
					if ((!this.tickmarkOnRangeCenter && this.threshold.getRangeCount()>1)
						|| 
						(this.tickmarkOnRangeCenter && this.threshold.getRangeCount() > 2))
						return .25*this.labels.maxSummIAndIPlus2LabelWidth;
					return this.defaultRangeItemWidth; 
					
				case ColorMapLabelsPosition.NORMAL:
				case ColorMapLabelsPosition.OPPOSITE:
					return this.labels.maxSummIAndIPlus1LabelWidth/2;
			}
			return NaN;
		}
		
		override protected function calculateWidthBasedOnRangeItemWidth():Number {
			var res:Number = this.rangeItemWidth*this.threshold.getRangeCount();
			if (this.labels != null && this.labels.enabled) {
				if (this.tickmarkOnRangeCenter) {
					if (this.labels.getFirstBounds().width > this.rangeItemWidth)
						res += (this.labels.getFirstBounds().width - this.rangeItemWidth)/2;
					if (this.labels.getLastBounds().width > this.rangeItemWidth)
						res += (this.labels.getLastBounds().width - this.rangeItemWidth)/2;
				}else {
					res += this.labels.getFirstBounds().width/2 + this.labels.getLastBounds().width/2;
				}
			}
			return res;
		}
		
		override protected function calculateRangeItemWidthBasedOnWidth():Number {
			var space:Number = this.getContentWidth();
			if (this.labels != null && this.labels.enabled) {
				if (this.tickmarkOnRangeCenter) {
					
					var canCalc:Boolean = false;
					
					if (this.labelsPosition == ColorMapLabelsPosition.COMBINED) {
						canCalc = this.labels.summEvenLabelWidth <= space && 
								  this.labels.summOddLabelWidth <= space;
					}else {
						canCalc = (this.labels.summEvenLabelWidth + this.labels.summOddLabelWidth) <= space;
					}
					
					var l1:Number = this.labels.getFirstBounds().width;
					var l2:Number = this.labels.getLastBounds().width;
					var n:int = this.threshold.getRangeCount();
					
					if (canCalc) {
						
						var w1:Number = space/this.threshold.getRangeCount();
						if (w1 >= l1 && w1 >= l2)
							return w1;
							
						var w2:Number = (2*space - l1)/(2*n - 1);
						if (w2 <= l1 && w2 >= l2)
							return w2;
							
						var w3:Number = (2*space - l2)/(2*n - 1);
						if (w3 >= l1 && w3 <= l2)
							return w3;
							
						var w4:Number = (2*space - l1 - l2)/(2*(n - 1));
						if (w4 <= l1 && w4 <= l2)
							return w4;
						
					}
					
					return (2*space - l1 - l2)/(2*(n - 1));
					
				}else {
					space -= this.labels.getFirstBounds().width/2 + this.labels.getLastBounds().width/2; 
				}
			}
			return space/this.threshold.getRangeCount();
		}
	}
}