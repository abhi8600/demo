package com.anychart.seriesPlot.controls.legend {
	
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.elements.marker.MarkerElementStyleState;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.thresholds.IThreshold;
	
	public class LegendAdapter {
			
		protected var plot:SeriesPlot;
		
		public function LegendAdapter(plot:SeriesPlot){
			this.plot = plot;	
		}
		
		public function destroy():void {
			this.plot = null;
		}
		
		public function getLegendDataBySource(source:String, item:XML, container:ILegendItemsContainer):void {
			switch (source) {
				case "series":
					this.getLegendSeriesData(item, container);
					break;
				case "points":
					this.getLegendPointData(item, container);
					break;
				case "thresholds":
					this.getLegendThresholdsData(item, container);
					break;
				case "auto":
					this.getLegendAutoItem(item, container);
					break;
			}
		}
		
		protected function getLegendSeriesData(item:XML, container:ILegendItemsContainer):void {
			var seriesName:String = item.@series != undefined ? SerializerBase.getString(item.@series) : null;
			var seriesCnt:uint = plot.series.length;
			var itemsBeforeSorting:Array = [];
			for (var i:int = 0;i<seriesCnt;i++) {
				var series:BaseSeries = plot.series[i];
				if (seriesName == null || series.name == seriesName) {
					var markerType:int;
					if (MarkerElementStyleState(series.marker.style.normal).isDynamicType) {
						markerType = series.marker.type == -1 ? series.markerType : series.marker.type;
					}else {
						markerType = MarkerElementStyleState(series.marker.style.normal).markerType;
					}
					var lItem:LegendAdaptiveItem = new LegendAdaptiveItem(series.color, series.hatchType, markerType, series.global.iconCanDrawMarker(), series);
					lItem.seriesType = series.type; 
					lItem.styleState = series.settings.style.normal;
					itemsBeforeSorting.push(lItem);
				} 
			}
			
			this.addItemsToContainer(item, itemsBeforeSorting, container);
		}
		
		private var sortDesc:Boolean = true;
		private var sortField:String = "%Value";
		
		private function sortFunction(item1:LegendAdaptiveItem, item2:LegendAdaptiveItem):int {
			var val1:* = item1.tag.getTokenValue(this.sortField);
			var val2:* = item2.tag.getTokenValue(this.sortField);
			if (val1 != undefined) val1 = val1.valueOf();
			if (val2 != undefined) val2 = val2.valueOf();
			if (val1 > val2) {
				return this.sortDesc ? -1 : 1;
			}else if (val1 < val2) {
				return this.sortDesc ? 1 : -1;
			}
			return 0;
		}
		
		private function addItemsToContainer(item:XML, items:Array, container:ILegendItemsContainer):void {
			
			if (item.@sort != undefined && SerializerBase.getLString(item.@sort) != 'none') {
				this.sortField = item.@sort_field != undefined ? SerializerBase.getString(item.@sort_field) : "%Value"; 
				this.sortDesc = SerializerBase.getLString(item.@sort) != 'asc';
				items.sort(this.sortFunction);
			}
			
			for (var i:int = 0;i<items.length;i++) {
				var lItem:LegendAdaptiveItem = LegendAdaptiveItem(items[i]);
				container.addItem(lItem);
			}
		}
		
		protected function getLegendPointData(item:XML, container:ILegendItemsContainer):void {
			var seriesName:String = item.@series != undefined ? SerializerBase.getString(item.@series) : null;
			var pointName:String = item.@name != undefined ? SerializerBase.getString(item.@name) : null;
			plot.resetSeriesIterator();
			
			var itemsBeforeSorting:Array = [];
			
			while (plot.hasSeries()) {
				var series:BaseSeries = plot.getNextSeries();
				if (seriesName != null && series.name != seriesName)
					continue;
					
				for (var j:uint = 0;j<series.points.length;j++) {
					var point:BasePoint = series.points[j];
					if (point && (pointName == null || pointName == point.name)) {
						if (point.threshold != null)
							point.threshold.checkBeforeDraw(point);
						
						var markerType:int;
						if (MarkerElementStyleState(point.marker.style.normal).isDynamicType) {
							if (point.marker.type == -1) {
								markerType = point.markerType;
								if (point.elementsInfo != null && point.elementsInfo[BasePoint.MARKER_INDEX] != null)
									markerType = point.elementsInfo[BasePoint.MARKER_INDEX].type;
							}else {
								markerType = point.marker.type;
							}
						}else {
							markerType =  MarkerElementStyleState(series.marker.style.normal).markerType;
						}
						
						var lItem:LegendAdaptiveItem = new LegendAdaptiveItem(point.color, point.hatchType, markerType, point.global.iconCanDrawMarker(), point); 
						lItem.seriesType = series.type; 
						lItem.styleState = point.styleState;
						itemsBeforeSorting.push(lItem);
					}
				}
			}
			
			this.addItemsToContainer(item, itemsBeforeSorting, container);
		}
		
		protected function getLegendThresholdsData(item:XML, container:ILegendItemsContainer):void {
			var thresholdName:String = item.@threshold != undefined ? SerializerBase.getLString(item.@threshold) : null;
			var thresholds:Array = plot.thresholdsList.getParsedList();
			
			for (var i:uint = 0;i<thresholds.length;i++) {
				var threshold:IThreshold = thresholds[i];
				if (thresholdName == null || thresholdName == threshold.getName().toLowerCase())
					threshold.getData(container);
			}
		}
		
		protected function getLegendAutoItem(item:XML, container:ILegendItemsContainer):void {
			this.getLegendSeriesData(item, container);
			this.getLegendThresholdsData(item, container);
		}
	}
}