package com.anychart.seriesPlot.utils {
	
	public final class CSVParser {
		
		private var rowsSep:String;
		private var rowsSepLen:uint;
		private var rowsSepPrefixFunc:Array;
		private var colsSep:String;
		private var colsSepLen:uint; 
		private var colsSepPrefixFunc:Array;
		private var content:String;
		private var contentLen:uint;
		private var ignoreTrailingSpaces:Boolean;
		
		public function init(rowsSeparator:String, colsSeparator:String, content:String, ignoreTrailingSpaces:Boolean):void{
			if (rowsSeparator == colsSeparator)
				throw new Error("Rows and cols separators must different");
			this.rowsSep = rowsSeparator;
			this.rowsSepLen = rowsSeparator.length - 1;
			this.rowsSepPrefixFunc = this.getPrefixFunction(rowsSeparator);
			this.colsSep = colsSeparator;
			this.colsSepLen = colsSeparator.length - 1;
			if (this.colsSepLen == -1 || this.rowsSepLen == -1)
				throw new Error("Rows and cols separators must be positive-length strings");
			this.colsSepPrefixFunc = this.getPrefixFunction(colsSeparator);
			this.content = content;
			this.contentLen = content.length;
			this.ignoreTrailingSpaces = ignoreTrailingSpaces;
			this.reset();
		}
		
		private function getPrefixFunction(pattern:String, checkForQuotes:Boolean = true):Array {
			
			var length:int = pattern.length;
            var result:Array = new Array(length);
            result[0] = -1;
            var k:int = -1;
            for (var i:int = 1; i < length; i++)
            {
            	var ch:String = pattern.charAt(i);
            	if (checkForQuotes && ch == "\"")
					throw new Error("There must not be any doublequotes in separators");
                while ((k > 0) && (pattern.charAt(k + 1) != ch))
                    k = result[k];
                if (pattern.charAt(k + 1) == ch)
                    k++;
                result[i] = k;
            }
			return result;
		}
		
		private var currPos:uint;
		private static const STATE_GENERAL:uint = 0;
		private static const STATE_VALUE:uint = 1;
		private static const STATE_ESCAPING:uint = 2;
		private static const STATE_LOOKING_FOR_SEPARATOR:uint = 3;
		private var finished:Boolean;
		
		public function reset():void {
			this.currPos = 0;
			this.finished = false;
		}
		
		public function next():Array {
			if (this.finished)
				return null;
			var result:Array = new Array(); // здесь будем хранить результат
			var currFieldIndex:int = 0; // номер текущего заполняемого поля
			var i:int = this.currPos - 1; // это будет счетчик текущей позиции просмотра (- 1 - из за особенностей цикла)
			var len:int; // вспомогательная, для циклов
			var j:int; // вспомогательная, для циклов
			var k:int; // вспомогательная, для циклов
			var currRowsSepPos:int = -1; // номер текущего символа в rowSep
			var currColsSepPos:int = -1; // номер текущего символа в colSep
			var state:uint = STATE_GENERAL; // состояние
			var str:String = ""; // здесь будет храниться текущая подстрока, с которой работаем
			var spacesLen:uint = 0; // используется чтобы отсекать trailing spaces при ignoreTrailingSpaces
			var nextChar:String;// вспомогательный символ
			while (++i < this.contentLen){
				var currChar:String = this.content.charAt(i);
				switch(state){
					case STATE_GENERAL:// начало ячейки
						if (currChar == "\""){ // нашли кавычку
							if (++i < this.contentLen) // проверяем, есть ли вообще следующий символ, попутно сдвигаем указатель на текущий символ на него
							 	nextChar = this.content.charAt(i);
							else // иначе умираем
								throw new Error("Unexpected end of data");
							this.currPos = i; // в любом случае началом ячейки будет этот символ
							if (nextChar == "\""){ // проверяем, это у нас сейчас было начало экранировки, или просто кавычки.
								// если кавычки, то переходим в состояние STATE_VALUE
								state = STATE_VALUE;	
							} else {// иначе переходим в состояние STATE_ESCAPING и откатываем сдвиг указателя
								state = STATE_ESCAPING;
								i--;
							}
							break;
						}
						
						while ((currColsSepPos > -1) && (this.colsSep.charAt(currColsSepPos + 1) != currChar)) // проверяем на colSep по Кнуту
							currColsSepPos = this.colsSepPrefixFunc[currColsSepPos];
						if (this.colsSep.charAt(currColsSepPos + 1) == currChar)
							currColsSepPos++;
						if (currColsSepPos == this.colsSepLen){ // дошли, наконец, до конца разделителя ячеек
							result[currFieldIndex++] = ""; // пишем результат (пустая строка должна быть) в соответствующую ячейку
							str = "";
							currColsSepPos = -1;
							currRowsSepPos = -1;
							this.currPos = i + 1;
							break;
						}
						
						while ((currRowsSepPos > -1) && (this.rowsSep.charAt(currRowsSepPos + 1) != currChar)) // проверяем на rowSep по Кнуту
							currRowsSepPos = this.rowsSepPrefixFunc[currRowsSepPos];
						if (this.rowsSep.charAt(currRowsSepPos + 1) == currChar)
							currRowsSepPos++;
						if (currRowsSepPos == this.rowsSepLen){ // дошли, наконец, до конца разделителя строк
							currColsSepPos = -1;
							currRowsSepPos = -1;
							this.currPos = i + 1;
							if (currFieldIndex > 0){ // если это не пустая строка получилась - тогда возвращаем наружу ответ
								result[currFieldIndex] = "";
								return result;
							} else { // наша песня хороша, начинай с начала
								str = "";
								break;
							}
						}
						
						if (!this.ignoreTrailingSpaces || (currChar != " " && currChar != "\t")){// если пробелы - часть данных или мы нашли не пробел
							this.currPos = i;
							state = STATE_VALUE;
						}
						break;		
					case STATE_VALUE: // внутри данных ячейки
						while ((currColsSepPos > -1) && (this.colsSep.charAt(currColsSepPos + 1) != currChar)) // проверяем на colSep по Кнуту
							currColsSepPos = this.colsSepPrefixFunc[currColsSepPos];
						if (this.colsSep.charAt(currColsSepPos + 1) == currChar)
							currColsSepPos++;
						if (currColsSepPos == this.colsSepLen){ // дошли, наконец, до конца разделителя ячеек
							len = i - this.currPos - this.colsSepLen;
							if (this.ignoreTrailingSpaces)
								len -= spacesLen;
							result[currFieldIndex++] = this.content.substr(this.currPos, len); // пишем результат в соответствующую ячейку
							str = "";
							currColsSepPos = -1;
							currRowsSepPos = -1;
							this.currPos = i + 1;
							spacesLen = 0;
							state = STATE_GENERAL;
							break;
						}
						
						while ((currRowsSepPos > -1) && (this.rowsSep.charAt(currRowsSepPos + 1) != currChar)) // проверяем на rowSep по Кнуту
							currRowsSepPos = this.rowsSepPrefixFunc[currRowsSepPos];
						if (this.rowsSep.charAt(currRowsSepPos + 1) == currChar)
							currRowsSepPos++;
						if (currRowsSepPos == this.rowsSepLen){ // дошли, наконец, до конца разделителя строк
							len = i - this.currPos - this.rowsSepLen;
							if (this.ignoreTrailingSpaces)
								len -= spacesLen;
							if (currFieldIndex == 0 && len <= 0){ // мы обманулись, приняв начало многосимвольного сепаратора за начало данных, начинаем с начала
								str = "";
								state = STATE_GENERAL;
								currColsSepPos = -1;
								currRowsSepPos = -1;
								this.currPos = i + 1;
								spacesLen = 0;
								break;
							} else {
								result[currFieldIndex] = this.content.substr(this.currPos, len);
								this.currPos = i + 1;
								return result;
							}
						}
						
						if (this.ignoreTrailingSpaces && (currChar == " " || currChar == "\t"))// если пробелы - не часть данных и мы нашли пробел
							spacesLen++;
						else
							spacesLen = 0;
						break;
					case STATE_ESCAPING: // находимся внутри экранировки, проглатываем все, кроме кавычек
						if (currChar == "\""){ // нашли кавычку
							if (++i < this.contentLen) // проверяем, есть ли вообще следующий символ, попутно сдвигаем указатель на текущий символ на него
							 	nextChar = this.content.charAt(i);
							else { // иначе завершаем работу (мы, типа, закончили строку кавчыкой и данные тоже закончились:( )
								len = i - this.currPos - 1;
								result[currFieldIndex] = this.content.substr(this.currPos, len);
								this.currPos = i;
								this.finished = true;
								return result;
							}
							if (nextChar == "\""){ // проверяем, это у нас сейчас было конец экранировки, или просто кавычки.
								// если кавычки, то сбрасываем в str часть строки до кавычки и обновляем this.currPos
								str += this.content.substr(this.currPos, i - this.currPos);
								this.currPos = i + 1;
							} else {// иначе переходим в состояние STATE_LOOKING_FOR_SEPARATOR, скидываем данные в ячейку и откатываем сдвиг указателя
								currColsSepPos = -1;
								currRowsSepPos = -1;
								state = STATE_LOOKING_FOR_SEPARATOR;
								result[currFieldIndex++] = str + this.content.substr(this.currPos, i - this.currPos - 1);
								str = "";
								i--;
							}
							break;
						}
						break;
					case STATE_LOOKING_FOR_SEPARATOR: // ищем конец ячейки, игнорируя любые другие символы
						while ((currColsSepPos > -1) && (this.colsSep.charAt(currColsSepPos + 1) != currChar)) // проверяем на colSep по Кнуту
							currColsSepPos = this.colsSepPrefixFunc[currColsSepPos];
						if (this.colsSep.charAt(currColsSepPos + 1) == currChar)
							currColsSepPos++;
						if (currColsSepPos == this.colsSepLen){ // дошли, наконец, до конца разделителя ячеек, уходим на исходную
							str = "";
							currColsSepPos = -1;
							currRowsSepPos = -1;
							this.currPos = i + 1;
							spacesLen = 0;
							state = STATE_GENERAL;
							break;
						}
						
						while ((currRowsSepPos > -1) && (this.rowsSep.charAt(currRowsSepPos + 1) != currChar)) // проверяем на rowSep по Кнуту
							currRowsSepPos = this.rowsSepPrefixFunc[currRowsSepPos];
						if (this.rowsSep.charAt(currRowsSepPos + 1) == currChar)
							currRowsSepPos++;
						if (currRowsSepPos == this.rowsSepLen){ // дошли, наконец, до конца разделителя строк
							this.currPos = i + 1;
							return result;
						}
				}
			}
			len = this.contentLen - this.currPos;
			if ((state == STATE_ESCAPING || state == STATE_VALUE) && len != 0){
				result[currFieldIndex] = str + this.content.substr(this.currPos, len);
				this.currPos = this.contentLen;
			}
			this.finished = true;
			if (result.length == 0){
				return null;
			} else {
				return result;
			}
		}
	}
}
