package com.anychart.seriesPlot.thresholds {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal class CollectionBasedThreshold extends AutomaticThreshold {
		protected var collectedPoints:Array = [];
		
		override public function checkBeforeDraw(point:BasePoint):void {
			var index:int = this.findBounds(point);
			if (index != -1) {
				point.thresholdItem = this.intervals[index];
				this.intervals[index].points.push(point);
				this.setPointColor(point, this.colorPalette.getItemAt(index));
			}
		}
		
		override public function reset():void {
			super.reset();
		}
		
		override public function checkAfterDeserialize(point:BasePoint):void {
			var valString:String = point.getPointTokenValue(this.autoValue);
			if (valString == "") return;			
			var val:Number = Number(valString);
			if (isNaN(val)) return;
			if (val < this.minValue)
				this.minValue = val;
			if (val > this.maxValue)
				this.maxValue = val;
			if (!isNaN(val)) {
				var info:PointInformation = new PointInformation();
				info.point = point;
				info.pointValue = val;
				this.collectedPoints.push(info);
			}
		}
		
		protected function doUnknownAction(val1:Number, val2:Number, toLower:Boolean):Number {
	        var tmp:Number = val1 + (0.5 * (val2 - val1));
	        if (tmp == 0)
	        	return 0;
	        var tmp1:int = int(Math.log(Math.abs(tmp))/Math.log(10));
	        var tmp2:Number = Math.pow(10, Number(tmp1 - 1));
	        tmp /= tmp2;
            tmp = (toLower) ? Math.floor(tmp) : Math.ceil(tmp);
	        return tmp * tmp2;
		}
	}
}