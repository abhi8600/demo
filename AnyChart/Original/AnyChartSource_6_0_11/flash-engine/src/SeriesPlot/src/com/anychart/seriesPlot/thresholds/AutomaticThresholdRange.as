package com.anychart.seriesPlot.thresholds {
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class AutomaticThresholdRange implements IThresholdItemFormatable,ILegendTag {
		public var start:Number;
		public var end:Number;
		public var points:Array = [];
		
		public var autoValue:String;
		
		public function contains(value:Number):Boolean {
			return value >= start && value < end;
		}
		
		public function getFormatedTokenValue(point:BasePoint, token:String):* {
			if (token == "%RangeMin") return this.start;
			if (token == "%RangeMax") return this.end;
			if (token == "%Value") return point.getTokenValue(this.autoValue);
			return "";
		}
		 
		public function hasToken(token:String):Boolean {
			return token == "%Value" || token == "%RangeMin" || token == "%RangeMax";
		}
		
		public function getCustomAttribute(token:String):* { return null; }
		public function isCustomAttribute(token:String):Boolean { return false; }
		
		public final function getTokenValue(token:String):* {
			if (token == "%RangeMin") return this.start;
			if (token == "%RangeMax") return this.end;
			return "";
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		public function setHover():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setHover();
			}
		}
		
		public function setNormal():void {
			for (var i:uint = 0;i<this.points.length;i++) {
				ILegendTag(this.points[i]).setNormal();
			}
		}
	}
}