package com.anychart.seriesPlot.updateAnimation {
	import com.anychart.animation.interpolation.InterpolationFactory;
	import com.anychart.animation.interpolation.Interpolator;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.utils.clearInterval;
	import flash.utils.setInterval;

	public class TargetPointAnimatedUpdater {
		
		private var point_:BasePoint;
		private var animationInterval_:int;
		private var interpolator_:Interpolator;
		private var fields_:Array;
		private var currentState_:Object;
		private var animationDuration_:Number;
		
		private static const DEFAULT_ANIMATION_DURATION:Number = 500;
		private static const DEFAULT_ANIMATION_INTERPOLATION_TYPE:String = "back";
		
		public function TargetPointAnimatedUpdater(point:BasePoint, newValue:Object, settings:Object) {
			this.point_ = point;
			this.interpolator_ = InterpolationFactory.create(settings.interpolation_type || DEFAULT_ANIMATION_INTERPOLATION_TYPE);
			this.fields_ = [];
			
			this.animationDuration_ = settings.duration || DEFAULT_ANIMATION_DURATION;
			
			this.currentState_ = {};
			
			var baseValues:Object = point.getCurrentValuesAsObject();
			
			for (var field:String in newValue) {
				if (!point.canAnimate(field)) continue;
				
				var pointFieldAnimation:PointFieldAnimation = new PointFieldAnimation();
				pointFieldAnimation.fieldName = field;
				pointFieldAnimation.startValue = baseValues[field];
				pointFieldAnimation.diff = newValue[field] - baseValues[field];
				this.fields_.push(pointFieldAnimation);
			}
			
			this._isFinalized = false;
		}
		
		public function canAnimate():Boolean { return this.fields_.length > 0; }
		
		private var _isFinalized:Boolean;
		public function isFinalized():Boolean { return this._isFinalized; }
		
		public function animate(time:Number):void {
			this._isFinalized = time >= this.animationDuration_;
			
			if (this._isFinalized) {
				time = this.animationDuration_;
			}
			
			for (var i:int = 0;i<this.fields_.length;i++) {
				var f:PointFieldAnimation = this.fields_[i];
				this.currentState_[f.fieldName] = this.interpolator_.interpolate(time, f.startValue, f.diff, this.animationDuration_);
			}
			
			this.point_.updateValue(this.currentState_);
		}
		
		public function setFinal():void {
			for (var i:int = 0;i<this.fields_.length;i++) {
				var f:PointFieldAnimation = this.fields_[i];
				this.currentState_[f.fieldName] = f.startValue + f.diff;
			}
			
			this.point_.updateValue(this.currentState_);
		}
	}
}
import com.anychart.animation.interpolation.Interpolator;

class PointFieldAnimation {
	public var startValue:Number;
	public var diff:Number;
	public var fieldName:String;
}