package com.anychart.seriesPlot.templates {
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.utils.XMLUtils;
	
	public class SeriesPlotTemplatesManager extends BaseTemplatesManager {
		
		override public function getBaseNodeName():String {
			return "chart";
		}
		
		override public function mergeTemplates(baseTemplate:XML, template:XML):XML {
			var res:XML = super.mergeTemplates(baseTemplate, template);
			
			var nodeName:String = this.getBaseNodeName();
			var templateData:XML = baseTemplate[nodeName][0].data[0];
			var chartData:XML = template[nodeName][0].data[0];
			
			if (templateData != null || chartData != null) {
				var data:XML = XMLUtils.mergeNamedChildrenByName(templateData, chartData, "series");
				if (data != null)
					res[nodeName][0].data[0] = data;
			}
			
			return res;
		}
		
		override public function mergeWithChart(template:XML, chart:XML):XML {
			var res:XML = super.mergeWithChart(template, chart);
			var nodeName:String = this.getBaseNodeName();
			var templateData:XML = template[nodeName][0].data[0];
			var chartData:XML = chart.data[0];
			
			if (chartData != null) {
				if (templateData != null) {
					res.data[0] = XMLUtils.merge(templateData, chartData, "data", "series");
					var seriesCnt:int = chartData.series.length();
					for (var i:int = 0;i<seriesCnt;i++) {
						var seriesNode:XML = chartData.series[i];
						if (seriesNode.@name != undefined && templateData.series.(@name == seriesNode.@name).length() > 0) {
							var seriesTemplate:XML = templateData.series.(@name == seriesNode.@name)[0];
							res.data[0].series[i] = XMLUtils.merge(seriesTemplate, seriesNode, "series", "point");
						}else {
							res.data[0].series += seriesNode; 
						}
					}
				}else {
					res.data[0] = chartData;
				}
			}
			return res;
		}
		
		override protected function mergeChartNode(template:XML, chart:XML, res:XML):void {
			super.mergeChartNode(template, chart, res);
			
			var dataPlotSettings:XML = XMLUtils.merge(template.data_plot_settings[0], chart.data_plot_settings[0]);
			if (dataPlotSettings != null)
				res.data_plot_settings[0] = dataPlotSettings;
				
			var childList:XMLList;
			var child:XML;
				
			var palettes:XML = XMLUtils.mergeNamedChildren(template.palettes[0], chart.palettes[0]);
			if (palettes != null) {  
				res.palettes[0] = palettes;
				childList = res.palettes[0].children();
				for each (child in childList) {
					if (child.@name != undefined)
						child.@name = String(child.@name).toLowerCase();
				}
			}
				
			var thresholds:XML = XMLUtils.mergeNamedChildren(template.thresholds[0], chart.thresholds[0]);
			if (thresholds != null) {
				res.thresholds[0] = thresholds;
				childList = res.thresholds[0].children();
				for each (child in childList)
					if (child.@name != undefined)
						child.@name = String(child.@name).toLowerCase();
			}
		}
		
		override protected function applyDefaults(res:XML, defaults:XML):void {
			super.applyDefaults(res, defaults);
			if (res == null || defaults == null) return;
			
			var defaultColorSwatch:XML = defaults.color_swatch[0];
			
			if (defaultColorSwatch != null) { 
				if (res.chart_settings[0] != null && res.chart_settings[0].controls[0] != null) {
					
					var colorSwatches:XMLList = res.chart_settings[0].controls[0].color_swatch;
					var cnt:int = colorSwatches.length();
					
					for (var i:int = 0;i<cnt;i++)
						colorSwatches[i] = XMLUtils.merge(defaultColorSwatch, colorSwatches[i], "color_swatch");
				}
			}
		}
	}
}