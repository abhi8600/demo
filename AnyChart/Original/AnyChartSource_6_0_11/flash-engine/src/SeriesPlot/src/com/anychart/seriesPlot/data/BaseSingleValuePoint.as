package com.anychart.seriesPlot.data {
	import com.anychart.serialization.SerializerBase;
	
	public class BaseSingleValuePoint extends BasePoint {
		
		public var y:Number;
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.deserializeYValue(data);
		}
		
		protected function deserializeYValue(data:XML):void {
			if (data.@y == undefined) {
				this.isMissing = true;
			} else {
				this.y = SerializerBase.getNumber(data.@y);
				if (this.y < 0)
					this.isMissing = true;
			}
		}	
				
		//-------------------------------------------------
		//				Formatting 
		//-------------------------------------------------
		
		override public function resetTokens():void {
			this.cashedTokens['YPercentOfSeries'] = null;
			this.cashedTokens['YPercentOfTotal'] = null;
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%Value') return this.y;
			if (token == '%YValue') return this.y;
			if (token == '%YPercentOfSeries') {
				if (this.cashedTokens['YPercentOfSeries'] == null) {
					this.cashedTokens['YPercentOfSeries'] = (this.series.getSeriesTokenValue("%SeriesYSum")!=0) ? this.y/this.series.getSeriesTokenValue("%SeriesYSum")*100 :0;
				}
				return this.cashedTokens['YPercentOfSeries'];
			}
			if (token == '%YPercentOfTotal') {
				if (this.cashedTokens['YPercentOfTotal'] == null) {
					this.cashedTokens['YPercentOfTotal'] = (this.plot.getTokenValue('%DataPlotYSum')!=0) ? this.y/this.plot.getTokenValue('%DataPlotYSum')*100 :0;
				}
				return this.cashedTokens['YPercentOfTotal'];
			}
			return super.getPointTokenValue(token);
		}	
		
		//-------------------------------------------------
		//				IObjectSerializable 
		//-------------------------------------------------
		
		override public function serialize(res:Object=null):Object {
			res = super.serialize(res);
			res.YValue = this.y;
			res.YPercentOfSeries = this.getPointTokenValue("%YPercentOfSeries");
			res.YPercentOfTotal = this.getPointTokenValue("%YPercentOfTotal");
			return res;
		}
	}
}