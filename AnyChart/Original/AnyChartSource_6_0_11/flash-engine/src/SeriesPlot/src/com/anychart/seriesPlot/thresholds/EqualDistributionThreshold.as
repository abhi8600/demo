package com.anychart.seriesPlot.thresholds {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class EqualDistributionThreshold extends CollectionBasedThreshold {
		
		override public function initialize():void {
			super.initialize();
			this.collectedPoints.sortOn("pointValue",Array.NUMERIC);
			
			var rangeCount:uint = this.rangeCount;
			
			if (this.collectedPoints.length == 0) {
				this.buildBoundsArray();
			}else {
				if (rangeCount > this.collectedPoints.length) {
					this.rangeCount = rangeCount = this.collectedPoints.length;
				}
				if (rangeCount < 2) {
					this.rangeCount = rangeCount;
					this.minValue = this.doUnknownAction(this.minValue, this.minValue, true);
					this.maxValue = this.doUnknownAction(this.maxValue, this.maxValue, false);
					this.buildBoundsArray();
				}else {
					var num:Number = this.collectedPoints.length/rangeCount;
					var a:Number = num;
					
					var val:Number = this.collectedPoints[0].pointValue;
					var valExt1:Number = this.collectedPoints[Math.round(a-1)].pointValue;
					var valExt2:Number = this.collectedPoints[Math.round(a)].pointValue;
					
					var r:AutomaticThresholdRange = new AutomaticThresholdRange();
					r.start = this.doUnknownAction(val, val, true);
					var tmp:Number = this.doUnknownAction(valExt1, valExt2, true);
					r.end = tmp;
					r.autoValue = this.autoValue;
					this.intervals[0] = r;
					
					for (var i:int = 1; i < (rangeCount - 1); i++) 
		            {
		                a += num;
		                this.intervals[i] = new AutomaticThresholdRange();
		                this.intervals[i].autoValue = this.autoValue;
		                this.intervals[i].start = tmp;
		                tmp = this.doUnknownAction(this.collectedPoints[Math.round(a - 1)].pointValue, this.collectedPoints[Math.round(a)].pointValue, true);
		                this.intervals[i].end = tmp;
		            }
		            this.intervals[this.rangeCount - 1] = new AutomaticThresholdRange();
		            this.intervals[this.rangeCount - 1].start = tmp;
		            this.intervals[this.rangeCount - 1].autoValue = this.autoValue;
		            this.intervals[this.rangeCount - 1].end = this.doUnknownAction(this.collectedPoints[this.collectedPoints.length - 1].pointValue,this.collectedPoints[this.collectedPoints.length - 1].pointValue, false);
				}
			}
		}
	}
}