package com.anychart.seriesPlot.data.drawing {
	
	public final class DrawingMode {
		public static const DRAW_TO_SERIES_GROUP_CONTAINER:uint = 1;
		public static const DRAW_TO_PERSONAL_CONTAINER:uint = 2;
	}
}