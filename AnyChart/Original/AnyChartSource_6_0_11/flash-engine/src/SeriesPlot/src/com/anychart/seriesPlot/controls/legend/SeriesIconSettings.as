package com.anychart.seriesPlot.controls.legend {
	import com.anychart.controls.legend.IconSettings;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	
	/**
	 * Legend icon settings for series-based plot
	 */
	public class SeriesIconSettings extends IconSettings {
		
		public var isSimpleBox:Boolean;
		public var plot:SeriesPlot;
		
		public var isDynamicSeriesType:Boolean;
		public var seriesType:uint;
		
		public function SeriesIconSettings(plot:SeriesPlot) {
			super();
			this.plot = plot;
			this.isSimpleBox = false;
			this.isDynamicSeriesType = true;
		}
		
		override public function createCopy(settings:IconSettings = null):IconSettings {
			if (settings == null)
				settings = new SeriesIconSettings(this.plot);
			super.createCopy(settings);
			var seriesIcon:SeriesIconSettings = SeriesIconSettings(settings);
			seriesIcon.isSimpleBox = this.isSimpleBox;
			seriesIcon.isDynamicSeriesType = this.isDynamicSeriesType;
			seriesIcon.seriesType = this.seriesType;
			return settings;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			if (data.@type != undefined) this.isSimpleBox = SerializerBase.getEnumItem(data.@type) == "box";
			if (data.@series_type != undefined) {
				if (SerializerBase.getEnumItem(data.@series_type) == "%SeriesType") {
					this.isDynamicSeriesType = true;
				}else {
					this.isDynamicSeriesType = false;
					this.seriesType = this.plot.getSeriesType(data.@series_type);
				}
			}
		}
	}
}