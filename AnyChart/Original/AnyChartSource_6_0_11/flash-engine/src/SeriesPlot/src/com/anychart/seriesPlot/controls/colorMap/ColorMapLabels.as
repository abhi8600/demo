package com.anychart.seriesPlot.controls.colorMap {
	import com.anychart.formatters.FormatsParser;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.thresholds.AutomaticThreshold;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class ColorMapLabels extends BaseTextElement {
		
		public var control:ColorMapControl;
		
		public var padding:Number;
		
		public function ColorMapLabels() {
			this.padding = 5;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (data.format[0]) {
				this.text = this.getCDATAString(data.format[0]);
				this.isDynamicText = FormatsParser.isDynamic(this.text);
				if (this.isDynamicText)
					this.dynamicText = FormatsParser.parse(this.text);
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
			this.rotation = 0 - this.rotation;
		}
		
		//-----------------------------------------------------------------
		//					SPACES CALCULATION
		//-----------------------------------------------------------------
		
		public var maxEvenLabelWidth:Number;
		public var maxEvenLabelHeight:Number;
		public var summEvenLabelWidth:Number;
		public var summEvenLabelHeight:Number;
		public var maxOddLabelWidth:Number;
		public var maxOddLabelHeight:Number;
		public var summOddLabelWidth:Number;
		public var summOddLabelHeight:Number;
		
		public var maxSummIAndIPlus1LabelWidth:Number;
		public var maxSummIAndIPlus2LabelWidth:Number;
		
		public var maxSummIAndIPlus1LabelHeight:Number;
		public var maxSummIAndIPlus2LabelHeight:Number;
		
		private var infos:Array;
		
		public function initialize():void {
			this.infos = [];
			this.maxEvenLabelHeight = 0;
			this.maxEvenLabelWidth = 0;
			this.summEvenLabelHeight = 0;
			this.summEvenLabelWidth = 0;
			this.maxOddLabelHeight = 0;
			this.maxOddLabelWidth = 0;
			this.summOddLabelHeight = 0;
			this.summOddLabelWidth = 0;
			this.maxSummIAndIPlus1LabelWidth = 0;
			this.maxSummIAndIPlus2LabelWidth = 0;
			this.maxSummIAndIPlus1LabelHeight = 0;
			this.maxSummIAndIPlus2LabelHeight = 0;
			
			this.threshold = this.control.threshold;
			
			if (this.control.tickmarkOnRangeCenter)
				this.initCenterLabels();
			else
				this.initBoundLabels();
		}
		
		private function initCenterLabels():void {
			var cnt:int = threshold.getRangeCount();
			var i:int;
			
			if (this.control.inverted) {
				for (i = (cnt-1);i>=0;i--)
					this.initCenterLabel(i);
			}else {
				for (i = 0;i<cnt;i++)
					this.initCenterLabel(i);
			}
			
			var w1:Number;
			var w2:Number;
			for (i = 0;i<(cnt-1);i++) {
				w1 = TextElementInformation(this.infos[i]).rotatedBounds.width;
				w2 = TextElementInformation(this.infos[i+1]).rotatedBounds.width;
				this.maxSummIAndIPlus1LabelWidth = Math.max(this.maxSummIAndIPlus1LabelWidth, w1+w2);
				
				w1 = TextElementInformation(this.infos[i]).rotatedBounds.height;
				w2 = TextElementInformation(this.infos[i+1]).rotatedBounds.height;
				this.maxSummIAndIPlus1LabelHeight = Math.max(this.maxSummIAndIPlus1LabelHeight, w1+w2);
			}
			
			if (cnt > 2) {
				for (i = 0;i<cnt-2;i++) {
					w1 = TextElementInformation(this.infos[i]).rotatedBounds.width;
					w2 = TextElementInformation(this.infos[i+2]).rotatedBounds.width;
					this.maxSummIAndIPlus2LabelWidth = Math.max(this.maxSummIAndIPlus2LabelWidth, w1+w2);
					
					w1 = TextElementInformation(this.infos[i]).rotatedBounds.height;
					w2 = TextElementInformation(this.infos[i+2]).rotatedBounds.height;
					this.maxSummIAndIPlus2LabelHeight = Math.max(this.maxSummIAndIPlus2LabelHeight, w1+w2);
				}
			}
		}
		
		private function initCenterLabel(index:int):void {
			this.startValue = this.threshold.getRangeStart(index);
			this.endValue = this.threshold.getRangeEnd(index);
			this.value = this.startValue + (this.endValue - this.startValue)/2;
			this.createInfo(index%2==0);
		}
		
		private function initBoundLabels():void {
			var cnt:int = threshold.getRangeCount();
			var i:int;
			if (this.control.inverted) {
				for (i = cnt;i>=0;i--)
					this.initBoundLabel(i);
			}else {
				for (i = 0;i<=cnt;i++)
					this.initBoundLabel(i);
			}
			
			/* Эксперимент в трешхолде...
			пока активна старая версия...
			это не удалять!!!
			var i:int;
			if (this.control.inverted) {
				for (i = threshold.getRangeCount();i>=0;i--)
					this.initBoundLabel(i);
			}else {
				for (i = 0;i<=threshold.getRangeCount();i++)
					this.initBoundLabel(i);
			}
			// перенес, ибо значение getRangeCount() выше может меняться.
			var cnt:Number=threshold.getRangeCount();*/
			
			var w1:Number;
			var w2:Number;
			for (i = 0;i<cnt;i++) {
				w1 = TextElementInformation(this.infos[i]).rotatedBounds.width;
				w2 = TextElementInformation(this.infos[i+1]).rotatedBounds.width;
				this.maxSummIAndIPlus1LabelWidth = Math.max(this.maxSummIAndIPlus1LabelWidth, w1+w2);
				
				w1 = TextElementInformation(this.infos[i]).rotatedBounds.height;
				w2 = TextElementInformation(this.infos[i+1]).rotatedBounds.height;
				this.maxSummIAndIPlus1LabelHeight = Math.max(this.maxSummIAndIPlus1LabelHeight, w1+w2);
			}
			if (cnt >= 2) {
				for (i = 0;i<=cnt-2;i++) {
					w1 = TextElementInformation(this.infos[i]).rotatedBounds.width;
					w2 = TextElementInformation(this.infos[i+2]).rotatedBounds.width;
					this.maxSummIAndIPlus2LabelWidth = Math.max(this.maxSummIAndIPlus2LabelWidth, w1+w2);
					
					w1 = TextElementInformation(this.infos[i]).rotatedBounds.height;
					w2 = TextElementInformation(this.infos[i+2]).rotatedBounds.height;
					this.maxSummIAndIPlus2LabelHeight = Math.max(this.maxSummIAndIPlus2LabelHeight, w1+w2);
				}
			}
		}
		
		private function initBoundLabel(index:int):void {
			this.value = (index != this.threshold.getRangeCount()) ? this.threshold.getRangeStart(index) : this.threshold.getRangeEnd(index-1);
			this.startValue = this.endValue = this.value;
			this.createInfo(index%2==0);
		}
		
		private var threshold:AutomaticThreshold;
		private var startValue:Number;
		private var endValue:Number;
		private var value:Number;
		
		private function createInfo(isEven:Boolean):void {
			var info:TextElementInformation = this.createInformation();
			info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : this.text;
			this.getBounds(info);
			
			this.infos.push(info);
			
			if (isEven) {
				this.summEvenLabelWidth += info.rotatedBounds.width;
				this.summEvenLabelHeight += info.rotatedBounds.height;
				this.maxEvenLabelWidth = Math.max(this.maxEvenLabelWidth, info.rotatedBounds.width);
				this.maxEvenLabelHeight = Math.max(this.maxEvenLabelHeight, info.rotatedBounds.height);
			}else {
				this.summOddLabelWidth += info.rotatedBounds.width;
				this.summOddLabelHeight += info.rotatedBounds.height;
				this.maxOddLabelWidth = Math.max(this.maxOddLabelWidth, info.rotatedBounds.width);
				this.maxOddLabelHeight = Math.max(this.maxOddLabelHeight, info.rotatedBounds.height);
			}
		}
		
		private function getTokenValue(token:String):* {
			var res:String = this.threshold.getTokenValue(token);
			if (res == "") {
				if (token == "%RangeMin") return this.startValue;
				if (token == "%RangeMax") return this.endValue;
				if (token == "%Value") return this.value;
			}
			return res;
		}
		
		private function isDateTimeToken(token:String):Boolean { return false; }
		
		//-----------------------------------------------------------------
		//					Drawing
		//-----------------------------------------------------------------
		
		public function getFirstBounds():Rectangle {
			return this.getLabelBounds(0);
		}
		
		public function getLastBounds():Rectangle {
			return this.getLabelBounds(this.infos.length-1);
		}
		
		public function getLabelBounds(index:uint):Rectangle {
			return TextElementInformation(this.infos[index]).rotatedBounds;
		}
		
		public function drawLabel(g:Graphics, pos:Point, index:uint):void {
			var info:TextElementInformation = this.infos[index];
			this.draw(g, pos.x, pos.y, info, 0,0);
		}

	}
}