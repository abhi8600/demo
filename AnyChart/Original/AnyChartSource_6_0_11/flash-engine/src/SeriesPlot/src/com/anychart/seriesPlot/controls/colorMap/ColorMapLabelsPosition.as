package com.anychart.seriesPlot.controls.colorMap {
	internal final class ColorMapLabelsPosition {
		public static const NORMAL:uint = 0;
		public static const OPPOSITE:uint = 1;
		public static const COMBINED:uint = 2;
	}
}