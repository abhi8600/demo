package {
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.describeType;
	
	import com.anychart.preloader.light.Preloader;
	import com.anychart.preloader.light.ErrorMessage;

	public class AnyChart51HTMLPreloader extends Sprite {
		
		private var loaderMovie:DisplayObject;
		private var loader:Preloader;
		private var swfLoader:Loader;
		private var contentMovie:DisplayObject;
		
		private var preloaderInitText:String = "Initializing... ";
		private var preloaderLoadingText:String = "Loading... ";
		
		private var targetSWFFile:String = "AnyChart.swf";
		
		public static var instance:AnyChart51HTMLPreloader;
		
		public function AnyChart51HTMLPreloader(){
			
			instance = this;
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, this.resizeHandler);
			
			this.loader = new Preloader();
			this.loaderMovie = this.loader.initialize(this.getStageBounds()); 
			this.addChild(this.loaderMovie);
			
			var params:Object = this.loaderInfo.parameters;
			for (var j:String in params) {
				switch (j.toLowerCase()) {
					case 'preloaderinittext':
					case 'preloaderinit':
						this.preloaderInitText = params[j];
						break;
					case 'preloaderloadingtext':
					case 'preloaderloading':
						this.preloaderLoadingText = params[j];
						break;
					case 'swffile':
						this.targetSWFFile = params[j];
						break;
				}
			}
			this.loader.showAnimated(this.preloaderInitText);
			
			this.swfLoader = new Loader();
			this.swfLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, this.ioErrorEventHandler);
			this.swfLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.securityErrorEventHandler);
			this.swfLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, this.progressEventHandler);
			this.swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.completeEventHandler);
			
			try {
				this.loader.initProgress(this.preloaderLoadingText);
				this.swfLoader.load(new URLRequest(this.targetSWFFile));
			}catch (securityError:SecurityError) {
				this.securityErrorEventHandler(new SecurityErrorEvent(SecurityErrorEvent.SECURITY_ERROR));
			}catch (ioError:IOError) {
				this.ioErrorEventHandler(new IOErrorEvent(IOErrorEvent.IO_ERROR));
			}
		}
		
		private function getStageBounds():Rectangle {
			return new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
		}
		
		private function ioErrorEventHandler(event:IOErrorEvent):void {
			this.swfLoader.close();
			this.showError(ErrorMessage.IO_ERROR);
		}
		
		private function securityErrorEventHandler(event:SecurityErrorEvent):void {
			this.swfLoader.close();
			this.showError(ErrorMessage.SECURITY_ERROR,ErrorMessage.SECURITY_ERROR_URL);
		}
		
		private function progressEventHandler(event:ProgressEvent):void {
			this.loader.setProgress(event.bytesLoaded/event.bytesTotal);
		}
		
		private var isAnyChart4:Boolean;
		private function completeEventHandler(event:Event):void {
			this.clear();
			this.isAnyChart4 = true;
			this.contentMovie = this.swfLoader.content;
			this.addChild(this.contentMovie);
			if (String(describeType(this.contentMovie).@name).indexOf("AnyChartRunnerBase") == -1 &&
				String(describeType(this.contentMovie).@base).indexOf("AnyChartRunnerBase") == -1)
				this.createAnyChart4Engine();
			else
				this.createAnyChart5Engine();
		}
		
		private function createAnyChart4Engine():void {
			var tmpS:Object = {};
			
			var j:String;
			
			var content:Object = Object(this.contentMovie);
			
			for (j in this.loaderInfo.parameters) {
				switch (j.toLowerCase()) {
					case 'preloaderinittext':
					case 'preloaderloadingtext':
					case 'swffile':
						break;
					case 'inittext':
						content['preloaderInitText'] = this.loaderInfo.parameters[j];
						break;
					case 'xmlloadingtext':
						content['xmlPreloaderPrefix'] = this.loaderInfo.parameters[j];
						break;
					case 'resourcesloadingtext':
						content['resourcesPrelaoderPrefix'] = this.loaderInfo.parameters[j];
						break;
					case 'xmlfile':
						content['XMLFile'] = this.loaderInfo.parameters[j];
						break;
					case 'dataxml':
						content['XMLRawData'] = this.loaderInfo.parameters[j];
						break;
					case 'nodatatext':
						content['noDataText'] = this.loaderInfo.parameters[j];
						break;
					case 'waitingfordatatext':
						content['waitinForDataText'] = this.loaderInfo.parameters[j];
						break;
					case 'templatesloadingtext':
						content['templatesPrelaoderPrefix'] = this.loaderInfo.parameters[j];
						break;
					case '__externalobjid':
						content['ExternalObjId'] = this.loaderInfo.parameters[j];
						break;
					case '__jsresize':
						content['useHTMLResizing'] = this.loaderInfo.parameters[j] == '1';
						break;
					case 'dispatchmouseevents':
						content['dispatchMouseEvents'] = (this.loaderInfo.parameters[j] == '1');
					default:
						tmpS[j] = this.loaderInfo.parameters[j];
						break;
				}
			}
			
			for (j in tmpS) {
				if (content['XMLFile'].charAt(content['XMLFile'].length - 1) != "?")
					content['XMLFile'] += "&";
				content['XMLFile'] += j+"="+escape(tmpS[j]);
			}
			content['initEngine'](this.stage);
		}
		
		private function createAnyChart5Engine():void {
			this.isAnyChart4 = false;
			Object(this.contentMovie).createEngine(this,this.loaderInfo);
		}
		
		private function clear():void { 
			if (this.loaderMovie != null) {
				this.removeChild(this.loaderMovie);
				this.loader = null;
				this.loaderMovie = null;
			}
			if (this.errorMessageSprite != null) {
				this.removeChild(this.errorMessageSprite);
				this.errorMessageSprite = null;
			}
			this.graphics.clear();
		}
		
		private var errorMessage:String;
		private var errorMessageSprite:DisplayObject;
		
		private function showError(message:String, url:String = null):void {
			this.errorMessage = message;
			this.clear();
			this.errorMessageSprite = ErrorMessage.showErrorMessage(this.getStageBounds(), message, url);
			this.addChild(this.errorMessageSprite);
		}
		
		private function resizeHandler(event:Event):void {
			if (this.contentMovie != null) {
				if (!this.isAnyChart4)
					this.contentMovie['resizeHandler'](event);
			}else if (this.loader != null)
				this.loader.resize(this.getStageBounds());
			else
				this.showError(this.errorMessage);
		}
	}
}
