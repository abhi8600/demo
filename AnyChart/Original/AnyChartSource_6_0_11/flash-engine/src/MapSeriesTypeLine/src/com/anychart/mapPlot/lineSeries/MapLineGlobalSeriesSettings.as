package com.anychart.mapPlot.lineSeries {
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.styles.states.StyleState;
	
	public final class MapLineGlobalSeriesSettings extends GlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:MapLineSeries = new MapLineSeries();
			series.type = SeriesType.LINE;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "line_series";
		}
		
		override public function getStyleNodeName():String {
			return "line_style";
		}
		
		override public function getStyleStateClass():Class {
			return LineStyleState;
		}
		
	}
}