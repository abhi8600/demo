package com.anychart.radarPolarPlot.series.bubbleSeries {
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	public class BubbleSeriesSettings extends BaseSeriesSettings {
		
		//----------------------------------------------
		//
		//			Base series settings override
		//
		//----------------------------------------------
		
		//----------------------------------------------
		//clone
		//----------------------------------------------
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new BubbleSeriesSettings();
			super.createCopy(settings);
			return settings;
		}
	}
}