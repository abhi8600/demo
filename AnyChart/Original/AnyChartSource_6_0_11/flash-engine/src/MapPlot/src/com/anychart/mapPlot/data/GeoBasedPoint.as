package com.anychart.mapPlot.data {
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class GeoBasedPoint extends BasePoint {
		public var linearGeo:Point;
		public var pixGeo:Point;
		
		public var space:BaseGeoSpace;
		
		public var geoX:Number;
		public var geoY:Number;
		
		public var link:String;
		
		public function GeoBasedPoint() {
			this.linearGeo = new Point();
			this.pixGeo = new Point();
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@link != undefined)
				this.link = SerializerBase.getString(data.@link);
			
			this.geoX = this.deserializeLongitude(data);
			this.geoY = this.deserializeLatitude(data);
			
			this.deserializeValue(data);
		}
		
		protected function deserializeLatitude(data:XML):Number {
			if (data.@y != undefined)
				return -SerializerBase.getNumber(data.@y);
			if (data.@lat != undefined)
				return -SerializerBase.getNumber(data.@lat);
			if (data.@latitude != undefined) 
				return -SerializerBase.getNumber(data.@latitude);
			return NaN;
		}
		
		protected function deserializeLongitude(data:XML):Number {
			if (data.@x != undefined)
				return SerializerBase.getNumber(data.@x);
			if (data.@long != undefined)
				return SerializerBase.getNumber(data.@long);
			if (data.@longitude != undefined)
				return SerializerBase.getNumber(data.@longitude);
			return NaN;
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "y" ||
				   field == "x";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				x: this.geoX,
				y: this.geoY
			}
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.x != undefined) this.geoX = newValue.x;
			if (newValue.y != undefined) this.geoY = -newValue.y;
			
			this.space.linearize(this.geoX, this.geoY, this.linearGeo);
		}
		
		protected function deserializeValue(data:XML):void {
		}
		
		public function applyProjection():void {
			this.space.linearize(this.geoX, this.geoY, this.linearGeo);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			pos.x = this.pixGeo.x;
			pos.y = this.pixGeo.y;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.space.transformLinearized(this.linearGeo, this.pixGeo);
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == "%Value") return this.name;
			return super.getPointTokenValue(token);
		}
		
	}
}