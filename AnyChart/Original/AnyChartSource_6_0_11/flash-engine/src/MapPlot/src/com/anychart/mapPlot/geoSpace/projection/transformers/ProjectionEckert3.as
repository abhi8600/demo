package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionEckert3 extends TopAndBottomFixedProjection {
		
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
    		
			var num13:Number = (x * Math.PI) / 180;
	        var num14:Number = (num2 * Math.PI) / 180;
	        var num15:Number = 2 / Math.sqrt(22.43597501544853);
	        var num16:Number = 4 / Math.sqrt(22.43597501544853);
	        var t:Number = 1 - ((4 * (num14 / Math.PI)) * (num14 / Math.PI));
	        if (t < 0) t = 0;
	        x = (num15 * num13) * (1 + Math.sqrt(t));
	        num2 = num16 * num14;
	        x *= 57.295779513082323;
	        num2 *= 57.295779513082323;
			pt.x = x;
			pt.y = num2;
		}
	}
}