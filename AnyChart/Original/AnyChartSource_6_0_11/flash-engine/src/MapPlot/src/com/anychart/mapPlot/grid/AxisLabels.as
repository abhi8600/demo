package com.anychart.mapPlot.grid {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTextElement;
	
	internal final class AxisLabels extends BaseTextElement {
		public var position:uint;
		public var padding:Number;
		
		public var format:uint;
		
		public function AxisLabels() {
			this.position = AxisLabelsPosition.FAR;
			this.format = AxisLabelsFormat.SEXAGESIMAL;
			this.isDynamicText = false;
			this.padding = 10;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (data.@position != undefined) {
				switch (SerializerBase.getEnumItem(data.@position)) {
					case "near": this.position = AxisLabelsPosition.NEAR; break;
					case "onequarter": this.position = AxisLabelsPosition.ONE_QUARTER; break;
					case "center": this.position = AxisLabelsPosition.CENTER; break;
					case "threequarters": this.position = AxisLabelsPosition.THREE_QUARTERS; break;
					case "far": this.position = AxisLabelsPosition.FAR; break;
				}
			}
			if (data.@format != undefined) {
				switch (SerializerBase.getEnumItem(data.@format)) {
					case "decimal": this.format = AxisLabelsFormat.DECIMAL; break;
					case "sexagesimal": this.format = AxisLabelsFormat.SEXAGESIMAL; break;
				}
			}
			if (data.@padding != undefined)
				this.padding = SerializerBase.getNumber(data.@padding);
		}
	}
}