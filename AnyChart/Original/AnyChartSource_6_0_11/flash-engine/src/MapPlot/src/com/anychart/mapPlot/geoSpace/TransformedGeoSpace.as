package com.anychart.mapPlot.geoSpace {
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class TransformedGeoSpace extends BaseGeoSpace {
		public var tx:Number;
		public var ty:Number;
		public var scaleX:Number;
		public var scaleY:Number;
		
		public var linearLeftTop:Point;
		public var linearRightBottom:Point;
		
		public function TransformedGeoSpace(tx:Number,
											ty:Number, 
											scaleX:Number, 
											scaleY:Number) {
			super();
			this.tx = tx;
			this.ty = ty;
			this.scaleX = scaleX;
			this.scaleY = scaleY;
			
			this.linearLeftTop = new Point(Number.MAX_VALUE, Number.MAX_VALUE);
			this.linearRightBottom = new Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
		}
		
		public function initBounds(pixRect:Rectangle):void {
			
			//set proportions
			var geoScale:Number = Math.abs(this.linearGeoBounds.width/this.linearGeoBounds.height);
			var pixScale:Number = pixRect.width/pixRect.height;
			this.mapPixBounds = new Rectangle();

			if (geoScale > pixScale) {
				this.mapPixBounds.width = pixRect.width;
				this.mapPixBounds.height = pixRect.width/geoScale;
			}else {
				this.mapPixBounds.height = pixRect.height;
				this.mapPixBounds.width = pixRect.height*geoScale;
			}
			
			this.mapPixBounds.x = pixRect.x + (pixRect.width - this.mapPixBounds.width)/2;
			this.mapPixBounds.y = pixRect.y + (pixRect.height - this.mapPixBounds.height)/2;
			
			this.setPixelRange(this.mapPixBounds);
		}
		
	}
}