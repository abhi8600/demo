package com.anychart.mapPlot.labels
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class SquareCoordinator
	{
		public var squares:Object = {};
        private var squareSize:uint;
        
        public function SquareCoordinator(squareSize:uint = 45) {
        	this.squareSize = squareSize;
        }

        public function addBox(box:Rectangle, id:uint):void {
        	var squaresCutsByBox:Array = getSuaresByBox(box);
        	var ln:uint = squaresCutsByBox.length;
            for (var i:uint = 0; i < ln; i++)
            	addId(squaresCutsByBox[i], id);
        }

        public function getIdsByBox(box:Rectangle):Array {
        	var result:Array = [];
        	var squareCollection:Array = getSuaresByBox(box);
        	var ln:uint = squareCollection.length;
        	var i:uint, j:uint, k:uint, ln2:uint, contains:Boolean;
            for (i = 0; i < ln; i++) {
            	var squareBoxes:Array = getSquare(squareCollection[i]);
            	if (squareBoxes == null) continue;
            	for (j = 0; j < squareBoxes.length; j++) {
            		contains = false;
            		ln2 = result.length;
            		for (k = 0; k < ln2; k++) {
            			if (result[k] == squareBoxes[j]) {
            				contains = true;
            				break;
        				}
            		}
            		if (!contains)
            			result.push(squareBoxes[j]);
            	}
            }
            return result;
        }

        public function getSquare(square:Point):Array {
        	return squares[square.toString()];
        }

        public function getSuaresByBox(box:Rectangle):Array {
            var squareCollection:Array = [];
            var atNowX:Number = box.topLeft.x;
            var atNowY:Number = box.topLeft.y;
            var squarePoint:Point = getSquareFromCoords(atNowX, atNowY);
            atNowY = squarePoint.y * squareSize;
            var brY:Number = box.bottomRight.y;
            var brX:Number = box.bottomRight.x;
            var tlX:Number = box.topLeft.x;
            while (atNowY <= brY) {
                squarePoint = getSquareFromCoords(tlX, atNowY);
                atNowX = squarePoint.x * squareSize;
                while (atNowX <= brX) {
                    squarePoint = getSquareFromCoords(atNowX, atNowY);
                    squareCollection.push(squarePoint);
                    atNowX += squareSize;
                }
                atNowY += squareSize;
            }
            return squareCollection;
        }


        public function setSquareSize(size:int):void {
            this.squareSize = size;
        }

        public function getSquareFromCoords(x:Number, y:Number):Point {
            return new Point(
                (int)((Math.ceil(x) - Math.ceil(x) % squareSize) / squareSize),
                (int)((Math.ceil(y) - Math.ceil(y) % squareSize) / squareSize));
        }

        public function addId(square:Point, id:uint):void {
        	var squareName:String = square.toString();
            if (squares[squareName] != undefined)
            	squares[squareName].push(id);
            else 
            	squares[squareName] = [id];
        }

        public function getCount():uint {
        	var i:uint = 0;
        	for (var s:String in squares) i++;
        	return i;
        }
        
        internal function clear():void {
        	squares = {};
        }

	}
}