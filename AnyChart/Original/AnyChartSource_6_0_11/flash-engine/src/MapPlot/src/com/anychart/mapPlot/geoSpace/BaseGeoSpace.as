package com.anychart.mapPlot.geoSpace {
	import com.anychart.mapPlot.geoSpace.projection.Projection;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class BaseGeoSpace {
		protected var xScale:MapScale;
		protected var yScale:MapScale;
		
		public var geoBounds:Rectangle;
		public var linearGeoBounds:Rectangle;
		public var projection:Projection;
		
		public var mapPixBounds:Rectangle;
		
		public function BaseGeoSpace() {
			this.xScale = new MapScale();
			this.yScale = new MapScale();
		}
		
		protected function boundX(x:Number):Number {
			return x;
		}
		
		protected function boundY(y:Number):Number {
			return y;
		}
		
		public function checkX(x:Number):Number {
			if (x < -180) return -180;
			if (x > 180) return 180;
			return x;
		}
		
		public function checkY(y:Number):Number {
			if (y < -90) return -90;
			if (y > 90) return 90;
			return y;
		}
		
		public function linearize(geoX:Number, geoY:Number, linearizedPt:Point):void {
			geoX = this.boundX(geoX);
			geoY = this.boundY(geoY);
			this.projection.transform(geoX, geoY, linearizedPt);
		}
		
		public function transformLinearized(linearizedPt:Point, pixPt:Point):void {
			pixPt.x = this.xScale.transform(linearizedPt.x);
			pixPt.y = this.yScale.transform(linearizedPt.y);
		}
		
		public function setPixelRange(bounds:Rectangle):void {
			this.xScale.setPixelRange(bounds.left, bounds.right);
			this.yScale.setPixelRange(bounds.top, bounds.bottom);
		}
		
		public function contains(geoX:Number, geoY:Number):Boolean {
			return this.geoBounds.contains(geoX, geoY);
		}
		
		public function drawLine(g:Graphics, start:Point, end:Point, moveTo:Boolean):void {
			this.projection.drawLine(this,g, start, end,moveTo);
		}
		
		public function transform(geoX:Number, geoY:Number, pixPt:Point):void {
			this.linearize(geoX, geoY, pixPt);
			this.transformLinearized(pixPt, pixPt);
		}
		
		public function linearizeLine(start:Point, end:Point, linearPoints:Array):void {
			this.projection.interpolateLine(start, end, linearPoints);
		}
		
		public function calculateProjection(showLabels:Boolean = false):void {
			if (isNaN(this.projection.centerX))
				this.projection.centerX = this.geoBounds.x + this.geoBounds.width/2;
				
			if (isNaN(this.projection.centerY))
				this.projection.centerY = this.geoBounds.y + this.geoBounds.height/2;

			this.linearGeoBounds = new Rectangle();
			this.projection.setLinearGeoBounds(this.geoBounds, this.linearGeoBounds);
			
			this.setLinearScales();
		}
		
		public function setLinearScales():void {
			this.xScale.minimum = this.linearGeoBounds.left;
			this.xScale.maximum = this.linearGeoBounds.right;
			this.yScale.minimum = this.linearGeoBounds.top;
			this.yScale.maximum = this.linearGeoBounds.bottom;
		}
	}
}