package com.anychart.mapPlot.labels {

	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	internal final class LabelsDisplayManagerRegionBounds extends LabelsManager {

		override public function registerLabel(labelContainer:DisplayObject, labelBounds:Rectangle, regionPixBounds:Rectangle):void {
			labelContainer.visible = regionPixBounds.containsRect(labelBounds);
		}

		override public function recheckLabel(labelContainer:DisplayObject, labelBounds:Rectangle, regionPixBounds:Rectangle):void {
			labelContainer.visible = regionPixBounds.containsRect(labelBounds);
/*
if (labelContainer is Sprite) {
	Sprite(labelContainer.parent).graphics.lineStyle(1, 0xFF0000);
	Sprite(labelContainer.parent).graphics.drawRect(
		labelBounds.x,
		labelBounds.y,
		labelBounds.width,
		labelBounds.height)

	Sprite(labelContainer.parent).graphics.lineStyle(1, 0x00FF00);
	Sprite(labelContainer.parent).graphics.drawRect(
		regionPixBounds.x,
		regionPixBounds.y,
		regionPixBounds.width,
		regionPixBounds.height)
}
//*/
		}
		
	}
}