package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionHammerAitoff extends ProjectionTransformer {
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
    		
			var num17:Number = (x * Math.PI) / 180;
	        var d:Number = (num2 * Math.PI) / 180;
	        var num19:Number = Math.sqrt(1 + (Math.cos(d) * Math.cos(num17 / 2)));
	        x = ((2 * Math.cos(d)) * Math.sin(num17 / 2)) / num19;
	        num2 = Math.sin(d) / num19;
	        x *= 57.295779513082323;
	        num2 *= 57.295779513082323;
	        
	        pt.x = x;
	        pt.y = num2;
		}
	}
}