package com.anychart.mapPlot.labels
{
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	internal class LabelsDisplayManagerNonOverlapBase extends LabelsManager {

		protected var lables:Array = [];
		protected var squareCoordinator:SquareCoordinator = new SquareCoordinator();
		
		override public function registerLabel(labelContainer:DisplayObject, labelBounds:Rectangle, regionPixBounds:Rectangle):void {
			lables.push(new LabelInfo(labelContainer, labelBounds, regionPixBounds)); 
		}

		protected function isCuttingWithPlaced(labelNum:uint):Boolean {
            var box:Rectangle = LabelInfo(lables[labelNum]).labelBounds;
            var lablesInSector:Array = squareCoordinator.getIdsByBox(box);
            var ln:uint = lablesInSector.length;
            for (var i:uint = 0; i < ln; i++)
            	if (i != labelNum && box.intersects(
            		LabelInfo(lables[lablesInSector[i]]).labelBounds)) return true;
            	//if (i != labelNum && box.intersects(LabelInfo(lables[i]).labelBounds)) return true;
            return false;
		}
		
	}
}