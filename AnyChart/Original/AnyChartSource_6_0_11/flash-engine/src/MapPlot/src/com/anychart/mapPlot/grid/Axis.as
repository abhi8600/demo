package com.anychart.mapPlot.grid {
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.mapPlot.geoSpace.MapScale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Axis implements ISerializableRes {
		
		public var enabled:Boolean;
		public var beforeMap:Boolean;
		
		private var majorGridLine:Stroke;
		private var minorGridLine:Stroke;
		private var zeroLine:Stroke;
		public var labels:AxisLabels;
		
		protected var scale:MapScale;
		protected var space:GeoSpace;
		private var g:Graphics;
		private var labelsG:Graphics;
		
		private var maxLabelSpace:Number;
		public var nearSpace:Number;
		public var farSpace:Number;
		public var startSpace:Number;
		public var endSpace:Number;
		
		public function Axis() {
			this.beforeMap = false;
			this.enabled = true;
			this.nearSpace = 0;
			this.farSpace = 0;
			this.maxLabelSpace = 0;
			this.startSpace = 0;
			this.endSpace = 0;
		}
		
		private var grid:MapGrid;
		
		public function initialize(grid:MapGrid):void {
			this.grid = grid;
			if (this.beforeMap) {
				this.g = grid.beforeMapSprite.graphics;
				this.labelsG = grid.beforeMapLabels.graphics;
			}else {
				this.g = grid.underMapSprite.graphics;
				this.labelsG = grid.underMapLabels.graphics;
			}
			this.space = grid.space;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			if (!this.enabled) return;
			if (data.@order != undefined)
				this.beforeMap = SerializerBase.getEnumItem(data.@order) == "beforemap";
			
			if (SerializerBase.isEnabled(data.major_grid_line[0])) {
				this.majorGridLine = new Stroke();
				this.majorGridLine.deserialize(data.major_grid_line[0]);
			}
			
			if (SerializerBase.isEnabled(data.minor_grid_line[0])) {
				this.minorGridLine = new Stroke();
				this.minorGridLine.deserialize(data.minor_grid_line[0]);
			}
			
			if (SerializerBase.isEnabled(data.zero_grid_line[0])) {
				this.zeroLine = new Stroke();
				this.zeroLine.deserialize(data.zero_grid_line[0]);
			}
			
			if (SerializerBase.isEnabled(data.labels[0])) {
				this.labels = new AxisLabels();
				this.labels.deserialize(data.labels[0], resources);
			}
		}
		
		public function draw():void {
			this.previousLabel = null;
			if (this.majorGridLine != null || this.minorGridLine != null || this.labels != null) {
				for (var i:int = 0;i<=this.scale.majorIntervalsCount;i++) {
					var majorValue:Number = this.scale.getMajorIntervalValue(i);
					if (this.majorGridLine != null) {
						this.majorGridLine.apply(g);
						this.drawLine(g, majorValue, true);
						g.lineStyle();
					}
					
					if (this.labels != null)
						this.drawLabel(majorValue);
					
					if (this.minorGridLine != null && i != this.scale.majorIntervalsCount) {
						for (var j:int = 1;j<this.scale.minorIntervalsCount;j++) {
							this.minorGridLine.apply(g);
							this.drawLine(g, scale.getMinorIntervalValue(majorValue, j), true);
							g.lineStyle();
						}
					}
				}
			}
			
			if (this.scale.contains(0) && this.zeroLine != null) {
				this.zeroLine.apply(this.grid.zeroLinesContainer.graphics);
				this.drawLine(this.grid.zeroLinesContainer.graphics, 0, true);
				this.grid.zeroLinesContainer.graphics.lineStyle();
			}
		}
		
		public function drawLine(g:Graphics, value:Number, moveTo:Boolean = false, back:Boolean = false):void {}
		
		//------------------------------------------------------------
		//				LABELS
		//------------------------------------------------------------
		
		private var previousLabel:Rectangle;
		
		private function drawLabel(value:Number):void {
			var info:TextElementInformation = this.labels.createInformation();
			info.formattedText = this.formatLabel(value);
			this.labels.getBounds(info);
			var pos:Point = new Point();
			this.setLabelPosition(value, info, pos);
			
			var bound:Rectangle = new Rectangle(pos.x, pos.y, info.rotatedBounds.width, info.rotatedBounds.height);
			
			if (this.previousLabel == null) {
				this.previousLabel = bound;
			}else {
				if (bound.intersects(this.previousLabel))
					return; 
				
				this.previousLabel = bound;
			}
			
			this.labels.draw(this.labelsG, pos.x, pos.y, info, 0, 0);
		}
		
		protected function setLabelPosition(value:Number, info:TextElementInformation, pos:Point):void {
		}
		
		private function formatLabel(value:Number):String {
			return this.labels.format == AxisLabelsFormat.DECIMAL ? (Math.round(value*100)/100).toString() : this.getStringGeoCoords(value);
		}
		
		protected function getStringGeoCoords(value:Number):String { return null; }
		
		public final function calculateSpace():void {
			if (this.labels == null) return;
			if (this.labels.position == AxisLabelsPosition.NEAR || this.labels.position == AxisLabelsPosition.FAR) {
				this.maxLabelSpace = 0;
				
				for (var i:uint = 0;i<=this.scale.majorIntervalsCount;i++) {
					var info:TextElementInformation = this.labels.createInformation();
					info.formattedText = this.formatLabel(this.scale.getMajorIntervalValue(i));
					this.labels.getBounds(info);
					var labelSpace:Number = this.getLabelSpace(info.rotatedBounds);
					this.maxLabelSpace = Math.max(this.maxLabelSpace, labelSpace);
					
					if (i == 0)
						this.startSpace = this.getPerpendicularSpace(info.rotatedBounds)/2;
					else if (i == this.scale.majorIntervalsCount)
						this.endSpace = this.getPerpendicularSpace(info.rotatedBounds)/2;
				}
				
				this.maxLabelSpace += this.labels.padding;
				
				if (this.labels.position == AxisLabelsPosition.NEAR)
					this.nearSpace = this.maxLabelSpace;
				else
					this.farSpace = this.maxLabelSpace;
			}
		}
		
		protected function getLabelSpace(labelSize:Rectangle):Number { return 0; }
		protected function getPerpendicularSpace(labelSize:Rectangle):Number { return 0; }
	}
}