package com.anychart.mapPlot {
	import com.anychart.IAnyChart;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.label.ItemLabelElement;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;

	internal final class RegionElementsContainer implements IMainElementsContainer {
		
		private var plot:MapPlot;
		
		public function get root():IAnyChart {
			return this.plot.root;
		}
		
		public function RegionElementsContainer(plot:MapPlot) {
			this.plot = plot;
		}

		public function getLabelsContainer(label:ItemLabelElement):Sprite {
			return this.plot.mapLabelsContainer;
		}
		
		public function get tooltipsContainer():DisplayObjectContainer {
			return this.plot.tooltipsContainer;
		}
		
		public function get markersContainer():Sprite {
			return this.plot.markersContainer;
		}
		
	}
}