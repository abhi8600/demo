package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	
	internal final class ProjectionBonne extends ProjectionTransformer {
		
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
			var num2:Number = geoY;
			
			var num30:Number = (x * Math.PI) / 180;
	        var num31:Number = (num2 * Math.PI) / 180;
	        var y:Number = centerY;
	        if (Math.abs(y) < 0.001)
	            y = 0.001;

	        y *= 0.017453292519943295;
	        var num33:Number = ((1 / Math.tan(y)) + y) - num31;
	        var num34:Number = (num30 * Math.cos(num31)) / num33;
	        x = num33 * Math.sin(num34);
	        num2 = (1 / Math.tan(y)) - (num33 * Math.cos(num34));
	        x *= 57.295779513082323;
	        num2 *= 57.295779513082323;
			
			pt.x = x;
			pt.y = num2;
		}
	}
}