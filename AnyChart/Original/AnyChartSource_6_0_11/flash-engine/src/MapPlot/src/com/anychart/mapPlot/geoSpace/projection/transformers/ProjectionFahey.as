package com.anychart.mapPlot.geoSpace.projection.transformers  {
	import flash.geom.Point;
	
	internal final class ProjectionFahey extends ProjectionTransformer {
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			var x:Number = geoX - centerX;
    		var num2:Number = geoY;
			var num6:Number = (x * Math.PI) / 180;
	        var num7:Number = (num2 * Math.PI) / 180;
	        var num8:Number = Math.cos(0.6108652381980153);
	        var num9:Number = Math.tan(num7 / 2);
	        x = (num6 * num8) * Math.sqrt(1 - (num9 * num9));
	        num2 = (1 + num8) * num9;
	        pt.x = x*57.295779513082323;
	        pt.y = num2*57.295779513082323;
		}
	}
}