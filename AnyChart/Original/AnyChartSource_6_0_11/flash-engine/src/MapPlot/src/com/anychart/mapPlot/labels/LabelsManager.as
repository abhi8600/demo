package com.anychart.mapPlot.labels {
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public class LabelsManager {
		
		public function registerLabel(labelContainer:DisplayObject, 
									  labelBounds:Rectangle,
									  regionPixBounds:Rectangle):void {
		}
		
		public function recheckLabel(labelContainer:DisplayObject, 
									 labelBounds:Rectangle,
									 regionPixBounds:Rectangle):void {
		}
		
		public function process():void {}
		
		public static function create(dispayMode:String):LabelsManager {
			if (dispayMode == null)
				return new LabelsDisplayManagerRegionBounds();
			switch (dispayMode) {
				case "always": return new LabelsDispayManagerForAlways();
				case "nonoverlap": return new LabelsDisplayManagerNonOverlap();
				case "regionboundsnonoverlap":  return new LabelsDisplayManagerRegBoundsNonOverlap();
				default: return new LabelsDisplayManagerRegionBounds(); 
			}
		}
	}
}