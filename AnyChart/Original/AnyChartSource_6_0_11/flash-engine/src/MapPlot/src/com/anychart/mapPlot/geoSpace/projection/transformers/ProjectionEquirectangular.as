package com.anychart.mapPlot.geoSpace.projection.transformers {
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class ProjectionEquirectangular extends ProjectionTransformer {
		
		override public function get needExtraPoints():Boolean { return false; }
		
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
			pt.x = geoX-centerX;
			pt.y = geoY-centerY;
		}
		
		override public function setLinearBounds(geoBounds:Rectangle, linearGeoBounds:Rectangle):void {
			linearGeoBounds.x = geoBounds.x-centerX;
			linearGeoBounds.y = geoBounds.y-centerY;
			linearGeoBounds.width = geoBounds.width;
			linearGeoBounds.height = geoBounds.height;
		}
		
		override public function drawLine(g:Graphics, space:BaseGeoSpace, start:Point, end:Point, moveTo:Boolean):void {
			var pixPt:Point = new Point();
			space.transform(start.x, start.y, pixPt);
			if (moveTo) g.moveTo(pixPt.x, pixPt.y);
			else g.lineTo(pixPt.x, pixPt.y);
			space.transform(end.x, end.y, pixPt);
			g.lineTo(pixPt.x, pixPt.y);
		}
		
		override public function interpolateLine(start:Point, end:Point, linearPts:Array):void {
			var pixPt:Point = new Point();
			this.transform(start.x, start.y, pixPt);
			linearPts.push(pixPt);
			this.transform(end.x, end.y, pixPt);
			linearPts.push(pixPt.clone());
		}
	}
}