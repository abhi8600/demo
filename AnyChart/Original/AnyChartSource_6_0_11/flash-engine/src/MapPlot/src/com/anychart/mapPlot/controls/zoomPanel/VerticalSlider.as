package com.anychart.mapPlot.controls.zoomPanel {

	internal final class VerticalSlider extends Slider {
		override protected function setSliderBounds():void {
			this.sliderBounds.x = this.bounds.width/2 - 3;
			this.sliderBounds.width = 6;
			this.sliderBounds.height = this.bounds.height;
			
			this.thumb.bounds.width = this.bounds.width;
			this.thumb.bounds.height = 9;
			
			this.dragBounds.x = 0;
			this.dragBounds.width = 0;
			this.dragBounds.y = 0;
			this.dragBounds.height = this.bounds.height - 9;
		}
		
		override protected function getGradientAngle():Number {
			return 90;
		}
		
		override public function get value():Number {
			return 1-(this.thumb.y/(this.bounds.height - 9));
		}
		
		override public function set value(newValue:Number):void {
			this.thumb.y = (1-newValue)*(this.bounds.height-9);
		}
	}
}