/**
 * $Id: Decrypt.java,v 1.5 2008-01-10 18:42:24 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * servlet to support decryption of encrypted fields outside of our firewall
 * - called via DecryptIntermedate.jsp:
 *   <script src="http://localhost:8080/decrypt/Decrypt?v=field1:7kip773qCyFzN0F1kzt69A==,field2:AXYvwtbXEUG1aDjn1HJoTQ=="></script>
 * - note that we need to do this as a 'script' file because it is the only way to allow cross-domain updating of fields on a screen
 */

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.*;
import pf.stk.*; 

public class Decrypt extends HttpServlet
{
	private static Logger logger = Logger.getLogger(Decrypt.class);
	private static final long serialVersionUID = 1L;
    private static String sfiPath;
    private static STKToolKit stk;
    private static LicenseArea license;
    private static byte[] charset;
    private static boolean initialized = false;

    public void init (ServletConfig config) throws ServletException
    {
    	ServletContext context = config.getServletContext();
    	sfiPath = context.getInitParameter("SecretKeyFile");
    	try
    	{
   	 		stk = STKToolKit.getInstance();
   	 		license = new LicenseArea();
   	 		stk.GetLic(stk.GetLicPath(), license);
   	 		logger.info("LICENSE verified, days left on License: " + license.getDaysLeftString());
   	 		charset = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/\\.:;@#$%&*()_-+=<>? ").getBytes("UTF8");
   	 		initialized = true;
    	}
        catch (UnsupportedEncodingException ex)
        {
        	logger.error(ex.getMessage());
        }
		catch (AlgorithmException ex)
		{
			// error occured
			logger.error(ex.getMessage());
		}
		catch (STKToolKitException ex)
		{
			logSTKError(ex);
		}
		catch(java.lang.UnsatisfiedLinkError ex)
		{
			logger.error(ex.getMessage());
		}
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        doGet(request, response);
    }
    
    /**
     * decrypt a set of values
     * - http(s)://host[:port]/decrypt/Decrypt?v=a,b,c,d,e,f
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	/*
    	 * sleep for testing the async nature of this
    	try
    	{
    		Thread.sleep(3000);
    	}
    	catch(Exception e)
    	{
    	}
    	*/
		response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
		response.addHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
		response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
		response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
    	response.setContentType("text/javascript");
    	PrintWriter writer = response.getWriter();
    	
    	if (!initialized)
    	{
    		logger.error("Servlet not initialized");
    		writer.close();
    		return;
    	}
    	
    	String queryString = request.getParameter("v"); // v=field|value,field|value,field|Svalue
    	if (queryString == null)
    	{
    		logger.warn("Null query string");
    		writer.close();
    		return;
    	}
    	// crack the string on commas
    	logger.debug("QueryString: " + queryString);
    	StringTokenizer st = new StringTokenizer(queryString, ",");
       	List<String> fields = new ArrayList<String>();
       	List<String> values = new ArrayList<String>();
    	while (st.hasMoreTokens())
    	{
    		String token = st.nextToken();
    		// split the token on a | (field|value)
    		String[] items = token.split("\\|");
    		if (items.length != 2)
    			continue;
    		String cleartext = decrypt(items[1]);
    		if (cleartext == null)
    			continue;
    		fields.add(items[0]);
    		values.add(cleartext);
    	}
    	
    	// return the string 'parent.decryptFields(new Array("field1","field2",...), new Array("value1","value2",...));'
    	StringBuilder sb = new StringBuilder();
    	sb.append("parent.decryptFields(new Array(");
    	boolean comma = false;
    	for (String field : fields)
    	{
    		if (comma)
    			sb.append(',');
       		sb.append('"');
    		sb.append(field);
    		sb.append('"');
    		comma = true;
    	}
    	sb.append("), new Array(");
       	comma = false;
       	for (String value : values)
    	{
    		if (comma)
    			sb.append(',');
    		sb.append('"');
    		sb.append(value);
    		sb.append('"');
    		comma = true;
    	}
    	sb.append("));");
    	writer.println(sb.toString());
    	writer.flush();
    	writer.close();
    }
    
	/**
	 * Decrypt a string using a password - AES, base64 encoded
	 * 
	 * @param text
	 * @param password
	 * @return	cleartext
	 * @throws Exception
	 */
	private String decrypt(String encryptedText)
	{
		if (encryptedText == null)
			return null;
		try
		{
		    WorkArea workArea = new WorkArea(); 
			stk.GetSfiWa(license, sfiPath, workArea);
			byte[] ciphrText = new String(encryptedText).getBytes("UTF8");
			byte[] clearText = new byte[ciphrText.length];
			stk.ChrCrypt(clearText, ciphrText, Algorithms.DECRYPT, charset, workArea);
			return new String(clearText);
		}
        catch (UnsupportedEncodingException ex)
        {
        	logger.error(ex.getMessage());
        }
		catch (AlgorithmException ex)
		{
			logger.error(ex.getMessage());
		}
		catch (STKToolKitException ex)
		{
			logSTKError(ex);
		}
		return null;
	}
	
	private static void logSTKError(STKToolKitException ex)
	{
		switch(ex.getErrorCode())
		{
		case ReturnCodes.LIC_DIFFCOMP:   /* license for a different computer       */
			logger.error("LICENSE NOT verified: Not for this computer");
			break;
		case ReturnCodes.LIC_DIFFPLAT:   /* license for a different platform       */
			logger.error("LICENSE NOT verified: Not for this platform");
			break;
		case ReturnCodes.LIC_DIFFPROD:   /* license for a different product        */
			logger.error("LICENSE NOT verified: Not for this product");
			break;
		case ReturnCodes.LIC_EXPIRED :   /* license expired                        */
			logger.error("LICENSE NOT verified: License has expired");
			break;
		case ReturnCodes.LIC_COMPANY :   /* license invalid - check company        */
			logger.error("LICENSE NOT verified: Check company name");
			break;
		case ReturnCodes.LIC_TRIAL   :   /* license invalid - check trial date     */
			logger.error("LICENSE NOT verified: Check trial date");
			break;
		case ReturnCodes.LIC_COMPID  :   /* license invalid - check computer id    */
			logger.error("LICENSE NOT verified: Check computer ID");
			break;
		case ReturnCodes.LIC_ISSUE   :   /* license invalid - check issue #        */
			logger.error("LICENSE NOT verified: Check issue");
			break;
		case ReturnCodes.LIC_CONTROL :   /* license invalid - check control code   */
			logger.error("LICENSE NOT verified: Check control code");
			break;
		case ReturnCodes.LIC_PLATFORM:   /* license invalid - check platform       */
			logger.error("LICENSE NOT verified: Check platform");
			break;
		case ReturnCodes.LIC_PRODUCT :   /* license invalid - check product        */
			logger.error("LICENSE NOT verified: Check product");
			break;
		case ReturnCodes.LIC_SECCODE :   /* license invalid - check security code  */
			logger.error("LICENSE NOT verified: Check security code");
			break;
		case ReturnCodes.GETLIC_CTLOPENERR :  /* PFDMAINT.DAT file not opened OK   */
		case ReturnCodes.GETLIC_CTLREADERR :  /* PFDMAINT.DAT file not read OK     */
			logger.error("LICENSE NOT verified: Check PFDMAINT.DAT file.  The PFDMAINT.DAT file must be in the current directory, or the environment variable PFMAINTDIR must be set.                      ");
			break;
		case ReturnCodes.LIC_INVALID :   /* license invalid                        */
		default:
			logger.error("LICENSE NOT verified: Information is invalid");
		break;
		}
	}
}