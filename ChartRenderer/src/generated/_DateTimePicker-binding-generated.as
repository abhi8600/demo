

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import mx.controls.RadioButtonGroup;
import flash.display.*;
import flash.debugger.*;
import flash.printing.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.controls.Button;
import mx.controls.DateField;
import flash.xml.*;
import mx.controls.NumericStepper;
import flash.system.*;
import mx.containers.HBox;
import flash.profiler.*;
import flash.external.*;
import flash.net.*;
import mx.controls.RadioButton;
import flash.utils.*;
import flash.text.*;
import mx.binding.*;
import flash.media.*;
import flash.ui.*;
import mx.styles.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property am (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'am' moved to '_3116am'
	 */

    [Bindable(event="propertyChange")]
    public function get am():mx.controls.RadioButton
    {
        return this._3116am;
    }

    public function set am(value:mx.controls.RadioButton):void
    {
    	var oldValue:Object = this._3116am;
        if (oldValue !== value)
        {
            this._3116am = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "am", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property ampm (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'ampm' moved to '_2998057ampm'
	 */

    [Bindable(event="propertyChange")]
    public function get ampm():mx.controls.RadioButtonGroup
    {
        return this._2998057ampm;
    }

    public function set ampm(value:mx.controls.RadioButtonGroup):void
    {
    	var oldValue:Object = this._2998057ampm;
        if (oldValue !== value)
        {
            this._2998057ampm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ampm", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property date (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'date' moved to '_3076014date'
	 */

    [Bindable(event="propertyChange")]
    public function get date():mx.controls.DateField
    {
        return this._3076014date;
    }

    public function set date(value:mx.controls.DateField):void
    {
    	var oldValue:Object = this._3076014date;
        if (oldValue !== value)
        {
            this._3076014date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "date", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property dummy (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'dummy' moved to '_95945896dummy'
	 */

    [Bindable(event="propertyChange")]
    public function get dummy():mx.controls.Button
    {
        return this._95945896dummy;
    }

    public function set dummy(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._95945896dummy;
        if (oldValue !== value)
        {
            this._95945896dummy = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dummy", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property hours (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'hours' moved to '_99469071hours'
	 */

    [Bindable(event="propertyChange")]
    public function get hours():mx.controls.NumericStepper
    {
        return this._99469071hours;
    }

    public function set hours(value:mx.controls.NumericStepper):void
    {
    	var oldValue:Object = this._99469071hours;
        if (oldValue !== value)
        {
            this._99469071hours = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "hours", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property minutes (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'minutes' moved to '_1064901855minutes'
	 */

    [Bindable(event="propertyChange")]
    public function get minutes():mx.controls.NumericStepper
    {
        return this._1064901855minutes;
    }

    public function set minutes(value:mx.controls.NumericStepper):void
    {
    	var oldValue:Object = this._1064901855minutes;
        if (oldValue !== value)
        {
            this._1064901855minutes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "minutes", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property pm (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'pm' moved to '_3581pm'
	 */

    [Bindable(event="propertyChange")]
    public function get pm():mx.controls.RadioButton
    {
        return this._3581pm;
    }

    public function set pm(value:mx.controls.RadioButton):void
    {
    	var oldValue:Object = this._3581pm;
        if (oldValue !== value)
        {
            this._3581pm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pm", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property seconds (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'seconds' moved to '_1970096767seconds'
	 */

    [Bindable(event="propertyChange")]
    public function get seconds():mx.controls.NumericStepper
    {
        return this._1970096767seconds;
    }

    public function set seconds(value:mx.controls.NumericStepper):void
    {
    	var oldValue:Object = this._1970096767seconds;
        if (oldValue !== value)
        {
            this._1970096767seconds = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "seconds", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property timeBox (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'timeBox' moved to '_1313954882timeBox'
	 */

    [Bindable(event="propertyChange")]
    public function get timeBox():mx.containers.HBox
    {
        return this._1313954882timeBox;
    }

    public function set timeBox(value:mx.containers.HBox):void
    {
    	var oldValue:Object = this._1313954882timeBox;
        if (oldValue !== value)
        {
            this._1313954882timeBox = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "timeBox", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property ADHOC (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'ADHOC' moved to '_62126361ADHOC'
	 */

    [Bindable(event="propertyChange")]
    private function get ADHOC():String
    {
        return this._62126361ADHOC;
    }

    private function set ADHOC(value:String):void
    {
    	var oldValue:Object = this._62126361ADHOC;
        if (oldValue !== value)
        {
            this._62126361ADHOC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADHOC", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property selectedDate (public)
	 * - generated setter
	 * - original getter left as-is
	 * - original public setter 'selectedDate' moved to '_1754951447selectedDate'
	 */

    [Bindable(event="propertyChange")]
    public function set selectedDate(value:Date):void
    {
    	var oldValue:Object = this.selectedDate;
        if (oldValue !== value)
        {
            this._1754951447selectedDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "selectedDate", oldValue, value));
        }
    }



}
