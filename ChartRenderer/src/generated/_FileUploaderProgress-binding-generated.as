

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import mx.controls.Button;
import mx.controls.ProgressBar;
import mx.controls.Label;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property fileNameLabelID (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fileNameLabelID' moved to '_2085518616fileNameLabelID'
	 */

    [Bindable(event="propertyChange")]
    public function get fileNameLabelID():mx.controls.Label
    {
        return this._2085518616fileNameLabelID;
    }

    public function set fileNameLabelID(value:mx.controls.Label):void
    {
    	var oldValue:Object = this._2085518616fileNameLabelID;
        if (oldValue !== value)
        {
            this._2085518616fileNameLabelID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fileNameLabelID", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property progBar (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'progBar' moved to '_309433767progBar'
	 */

    [Bindable(event="propertyChange")]
    public function get progBar():mx.controls.ProgressBar
    {
        return this._309433767progBar;
    }

    public function set progBar(value:mx.controls.ProgressBar):void
    {
    	var oldValue:Object = this._309433767progBar;
        if (oldValue !== value)
        {
            this._309433767progBar = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "progBar", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property progressCancelID (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'progressCancelID' moved to '_419244642progressCancelID'
	 */

    [Bindable(event="propertyChange")]
    public function get progressCancelID():mx.controls.Button
    {
        return this._419244642progressCancelID;
    }

    public function set progressCancelID(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._419244642progressCancelID;
        if (oldValue !== value)
        {
            this._419244642progressCancelID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "progressCancelID", oldValue, value));
        }
    }



}
