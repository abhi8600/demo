

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import mx.controls.Label;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import com.birst.flex.dashboards.components.*;
import mx.styles.*;
import flash.system.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property dropdownLabel (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'dropdownLabel' moved to '_1409953123dropdownLabel'
	 */

    [Bindable(event="propertyChange")]
    public function get dropdownLabel():mx.controls.Label
    {
        return this._1409953123dropdownLabel;
    }

    public function set dropdownLabel(value:mx.controls.Label):void
    {
    	var oldValue:Object = this._1409953123dropdownLabel;
        if (oldValue !== value)
        {
            this._1409953123dropdownLabel = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dropdownLabel", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property promptValues (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'promptValues' moved to '_1146337370promptValues'
	 */

    [Bindable(event="propertyChange")]
    public function get promptValues():XML
    {
        return this._1146337370promptValues;
    }

    public function set promptValues(value:XML):void
    {
    	var oldValue:Object = this._1146337370promptValues;
        if (oldValue !== value)
        {
            this._1146337370promptValues = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "promptValues", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property isSetViaBookmark (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'isSetViaBookmark' moved to '_1816058516isSetViaBookmark'
	 */

    [Bindable(event="propertyChange")]
    public function get isSetViaBookmark():Boolean
    {
        return this._1816058516isSetViaBookmark;
    }

    public function set isSetViaBookmark(value:Boolean):void
    {
    	var oldValue:Object = this._1816058516isSetViaBookmark;
        if (oldValue !== value)
        {
            this._1816058516isSetViaBookmark = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isSetViaBookmark", oldValue, value));
        }
    }



}
