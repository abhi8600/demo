

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import flash.filters.*;
import flash.profiler.*;
import com.birst.flex.core.CoreApp;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import mx.controls.TextArea;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import flash.ui.*;
import flash.media.*;
import mx.controls.List;
import flash.xml.*;
import mx.styles.*;
import flash.system.*;
import mx.controls.ComboBox;
import mx.modules.ModuleLoader;
import flash.errors.*;
import com.birst.flex.core.expression.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property expr (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'expr' moved to '_3127797expr'
	 */

    [Bindable(event="propertyChange")]
    public function get expr():mx.controls.TextArea
    {
        return this._3127797expr;
    }

    public function set expr(value:mx.controls.TextArea):void
    {
    	var oldValue:Object = this._3127797expr;
        if (oldValue !== value)
        {
            this._3127797expr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "expr", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fnDescr (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fnDescr' moved to '_748776423fnDescr'
	 */

    [Bindable(event="propertyChange")]
    public function get fnDescr():mx.controls.TextArea
    {
        return this._748776423fnDescr;
    }

    public function set fnDescr(value:mx.controls.TextArea):void
    {
    	var oldValue:Object = this._748776423fnDescr;
        if (oldValue !== value)
        {
            this._748776423fnDescr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fnDescr", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property funcCombo (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'funcCombo' moved to '_185648854funcCombo'
	 */

    [Bindable(event="propertyChange")]
    public function get funcCombo():mx.controls.ComboBox
    {
        return this._185648854funcCombo;
    }

    public function set funcCombo(value:mx.controls.ComboBox):void
    {
    	var oldValue:Object = this._185648854funcCombo;
        if (oldValue !== value)
        {
            this._185648854funcCombo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "funcCombo", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property funcList (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'funcList' moved to '_1379747202funcList'
	 */

    [Bindable(event="propertyChange")]
    public function get funcList():mx.controls.List
    {
        return this._1379747202funcList;
    }

    public function set funcList(value:mx.controls.List):void
    {
    	var oldValue:Object = this._1379747202funcList;
        if (oldValue !== value)
        {
            this._1379747202funcList = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "funcList", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property subjectAreaModule (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'subjectAreaModule' moved to '_1756737445subjectAreaModule'
	 */

    [Bindable(event="propertyChange")]
    public function get subjectAreaModule():mx.modules.ModuleLoader
    {
        return this._1756737445subjectAreaModule;
    }

    public function set subjectAreaModule(value:mx.modules.ModuleLoader):void
    {
    	var oldValue:Object = this._1756737445subjectAreaModule;
        if (oldValue !== value)
        {
            this._1756737445subjectAreaModule = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subjectAreaModule", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property app (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'app' moved to '_96801app'
	 */

    [Bindable(event="propertyChange")]
    private function get app():CoreApp
    {
        return this._96801app;
    }

    private function set app(value:CoreApp):void
    {
    	var oldValue:Object = this._96801app;
        if (oldValue !== value)
        {
            this._96801app = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "app", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fnList (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'fnList' moved to '_1270837882fnList'
	 */

    [Bindable(event="propertyChange")]
    private function get fnList():Array
    {
        return this._1270837882fnList;
    }

    private function set fnList(value:Array):void
    {
    	var oldValue:Object = this._1270837882fnList;
        if (oldValue !== value)
        {
            this._1270837882fnList = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fnList", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property exprRequest (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'exprRequest' moved to '_90546906exprRequest'
	 */

    [Bindable(event="propertyChange")]
    public function get exprRequest():String
    {
        return this._90546906exprRequest;
    }

    public function set exprRequest(value:String):void
    {
    	var oldValue:Object = this._90546906exprRequest;
        if (oldValue !== value)
        {
            this._90546906exprRequest = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exprRequest", oldValue, value));
        }
    }



}
