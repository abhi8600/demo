package 
{

import mx.resources.ResourceBundle;

[ExcludeClass]

public class en_US$expressions_example_properties extends ResourceBundle
{

    public function en_US$expressions_example_properties()
    {
		 super("en_US", "expressions_example");
    }

    override protected function getContent():Object
    {
        var content:Object =
        {
            "Let": "Let(Dim [AvgPrice] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Avg: UnitPrice] from [All]) Dim [TotalQuantity] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Sum: Quantity] from [All]), IIF([AvgPrice]&lt;25,25,[AvgPrice])*[TotalQuantity])",
            "Positional Calculation": "[OrderDate: Sum: Quantity]{[Time.Year]}\n\n[OrderDate: Sum: Quantity]{[Time.Year]=2012}\n\n[OrderDate: Sum: Quantity]{[Time.Total]}\n\n[OrderDate: Sum: Quantity]{[Time.Year]-=1}",
            "Rsum": "Rsum(3,[OrderDate: Sum: Quantity])",
            "Format": "Format([Time.Date],'dd-MM-yy')",
            "Substring": "Substring([Products.ProductName],0,3)",
            "Ln": "Ln([OrderDate: Sum: Quantity])",
            "ToUpper": "ToUpper([Products.ProductName])",
            "PTile": "PTile([OrderDate: Sum: Quantity])",
            "GetPromptValue": "[OrderDate: Sum: Quantity]*Float(GetPromptValue('MyPrompt','2012'))",
            "DateDiff": "DateDiff(Day,[Order_Details.OrderDate],[Order_Details.ShippedDate])",
            "Median": "Median([OrderDate: Sum: Quantity] By [OrderDate: Sum: Quantity])",
            "Sparse": "Sparse([Time.Year])",
            "Position": "Position('Chai',[Products.ProductName])",
            "Abs": "Abs([OrderDate: Sum: Quantity])",
            "AND": "[OrderDate: Sum: Quantity] WHERE [Categories.CategoryName] = 'Beverages' AND [Categories.CategoryName] = 'Seafood'",
            "ToLower": "ToLower([Products.ProductName])",
            "OR": "[OrderDate: Sum: Quantity] WHERE [Categories.CategoryName] = 'Beverages' OR [Categories.CategoryName] = 'Seafood'",
            "RowNumber": "RowNumber()",
            "Rank": "Rank([OrderDate: Sum: Quantity])",
            "Replace": "Replace([Products.ProductName],'Tofu','Soft Tofu')",
            "ArcTan": "ArcTan([OrderDate: Sum: Quantity])",
            "Tan": "Tan([OrderDate: Sum: Quantity])",
            "NumRows": "NumRows()",
            "LookupValue": "LookupValue(0,[Products.ProductName],1,Select [Products.ProductName], [OrderDate: Sum: Quantity] From [All])\n\nLookupValue(0,[Products.ProductName],1,[OrderDate: Sum: Quantity],Select [Products.ProductName], [OrderDate: Sum: Quantity] From [All] Where [OrderDate: Sum: Quantity]>%0)",
            "LookupRow": "LookupRow(0,0,Select [OrderDate: Sum: Quantity] From [All])\n\nLookupRow(0,0,[OrderDate: Sum: Quantity],Select [OrderDate: Sum: Quantity] From [All] Where [OrderDate: Sum: Quantity]>=%0)",
            "Sqrt": "Sqrt([OrderDate: Sum: Quantity])",
            "ReplaceAll": "Replaceall([Products.CategoryName],'P.*e','Organic Produce')",
            "IFNull": "IFNull([OrderDate: Sum: Quantity],0)",
            "Find": "Find(0,[Products.ProductName],1,Select [Products.ProductName], [OrderDate: Sum: Quantity] From [All])",
            "ToTime": "TOTIME([Time.TimeStamp],'hours,minutes,seconds','%d:%02d:%02d')\n\nTOTIME(1000000000,'days,hours,minutes','%d days %d hours %d minutes') -> 11 days 13 hours 46 minutes",
            "Exp": "Exp([OrderDate: Sum: Quantity])",
            "IsNaN": "IsNaN([OrderDate: Sum: Quantity])",
            "DateAdd": "DateAdd(Day,2,[Order_Details.OrderDate])",
            "Ceiling": "Ceiling([OrderDate: Sum: Quantity])",
            "Stat": "Stat(Median,1,Select [Products.ProductName],[OrderDate: Sum: Quantity] From [All])\n\nStat(Sum,1,[OrderDate: Sum: Quantity],Select [Categories.CategoryName],[OrderDate: Sum: Quantity] from [All] Where [OrderDate: Sum: Quantity]>=%0)/[OrderDate: Sum: Quantity]{[Categories.Total]}",
            "ArcCos": "ArcCos([OrderDate: Sum: Quantity])",
            "DRank": "DRank([OrderDate: Sum: Quantity])",
            "IsInf": "IsInf([OrderDate: Sum: Quantity])",
            "Cos": "Cos([OrderDate: Sum: Quantity])",
            "Trim": "Trim([Products.ProductName])",
            "Length": "Length([Products.ProductName])",
            "Log": "Log([OrderDate: Sum: Quantity])",
            "Floor": "Floor([OrderDate: Sum: Quantity])",
            "ArcSin": "ArcSin([OrderDate: Sum: Quantity])",
            "DatePart": "DatePart(Day,[Order_Details.OrderDate])",
            "Sin": "Sin([OrderDate: Sum: Quantity])",
            "GetPromptFilter": "[OrderDate: Sum: Quantity] WHERE GetPromptFilter('MyPrompt')",
            "Random": "Random()",
            "IIF": "IIF([OrderDate: Sum: Quantity]>0,100,[OrderDate: Sum: Quantity])"
        };
        return content;
    }
}



}
