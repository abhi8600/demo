

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import mx.controls.Button;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import mx.core.UIComponent;
import mx.styles.*;
import flash.system.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property okButton (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'okButton' moved to '_1641788370okButton'
	 */

    [Bindable(event="propertyChange")]
    public function get okButton():mx.controls.Button
    {
        return this._1641788370okButton;
    }

    public function set okButton(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._1641788370okButton;
        if (oldValue !== value)
        {
            this._1641788370okButton = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "okButton", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property alertTitle (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'alertTitle' moved to '_1909662300alertTitle'
	 */

    [Bindable(event="propertyChange")]
    public function get alertTitle():String
    {
        return this._1909662300alertTitle;
    }

    public function set alertTitle(value:String):void
    {
    	var oldValue:Object = this._1909662300alertTitle;
        if (oldValue !== value)
        {
            this._1909662300alertTitle = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "alertTitle", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property message (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'message' moved to '_954925063message'
	 */

    [Bindable(event="propertyChange")]
    public function get message():String
    {
        return this._954925063message;
    }

    public function set message(value:String):void
    {
    	var oldValue:Object = this._954925063message;
        if (oldValue !== value)
        {
            this._954925063message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property alertIcon (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'alertIcon' moved to '_1462752267alertIcon'
	 */

    [Bindable(event="propertyChange")]
    public function get alertIcon():Class
    {
        return this._1462752267alertIcon;
    }

    public function set alertIcon(value:Class):void
    {
    	var oldValue:Object = this._1462752267alertIcon;
        if (oldValue !== value)
        {
            this._1462752267alertIcon = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "alertIcon", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property caller (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'caller' moved to '_1367775349caller'
	 */

    [Bindable(event="propertyChange")]
    public function get caller():UIComponent
    {
        return this._1367775349caller;
    }

    public function set caller(value:UIComponent):void
    {
    	var oldValue:Object = this._1367775349caller;
        if (oldValue !== value)
        {
            this._1367775349caller = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "caller", oldValue, value));
        }
    }



}
