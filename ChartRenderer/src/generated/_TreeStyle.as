
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;

[ExcludeClass]

public class _TreeStyle
{
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='TreeDisclosureOpen', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='1552', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_TreeDisclosureOpen_474193029:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='TreeFolderOpen', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='1554', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_TreeFolderOpen_913697156:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='TreeNodeIcon', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='1550', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_TreeNodeIcon_713478543:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='TreeFolderClosed', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='1553', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_TreeFolderClosed_878749290:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='TreeDisclosureClosed', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='1551', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_TreeDisclosureClosed_924631335:Class;

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration("Tree");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration("Tree", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.disclosureOpenIcon = _embed_css_Assets_swf_TreeDisclosureOpen_474193029;
                this.folderClosedIcon = _embed_css_Assets_swf_TreeFolderClosed_878749290;
                this.folderOpenIcon = _embed_css_Assets_swf_TreeFolderOpen_913697156;
                this.disclosureClosedIcon = _embed_css_Assets_swf_TreeDisclosureClosed_924631335;
                this.verticalAlign = "middle";
                this.defaultLeafIcon = _embed_css_Assets_swf_TreeNodeIcon_713478543;
                this.paddingLeft = 2;
                this.paddingRight = 0;
            };
        }
    }
}

}
