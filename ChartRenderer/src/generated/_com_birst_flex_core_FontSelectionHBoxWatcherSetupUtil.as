






package
{
import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
[Mixin]
public class _com_birst_flex_core_FontSelectionHBoxWatcherSetupUtil extends Sprite
    implements mx.binding.IWatcherSetupUtil
{
    public function _com_birst_flex_core_FontSelectionHBoxWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import com.birst.flex.core.FontSelectionHBox;
        (com.birst.flex.core.FontSelectionHBox).watcherSetupUtil = new _com_birst_flex_core_FontSelectionHBoxWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import mx.core.UIComponentDescriptor;
        import mx.core.DeferredInstanceFromClass;
        import mx.collections.ArrayCollection;
        import mx.utils.ObjectProxy;
        import __AS3__.vec.Vector;
        import mx.binding.IBindingClient;
        import mx.core.ClassFactory;
        import mx.rpc.events.ResultEvent;
        import mx.controls.CheckBox;
        import mx.core.IFactory;
        import mx.core.DeferredInstanceFromFunction;
        import mx.utils.UIDUtil;
        import mx.rpc.events.FaultEvent;
        import flash.events.EventDispatcher;
        import com.birst.flex.webservice.WebServiceResult;
        import mx.rpc.soap.WebService;
        import mx.controls.Alert;
        import mx.containers.HBox;
        import mx.binding.BindingManager;
        import mx.core.IDeferredInstance;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import mx.events.PropertyChangeEvent;
        import mx.core.mx_internal;
        import mx.events.FlexEvent;
        import mx.core.UIComponent;
        import mx.events.ListEvent;
        import flash.events.Event;
        import mx.controls.ComboBox;
        import com.birst.flex.webservice.FontFamilyWebService;

        // writeWatcher id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[2] = new mx.binding.PropertyWatcher("mKey",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=2 size=3
        [
        bindings[0],
        bindings[1],
        bindings[2]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=5 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[5] = new mx.binding.StaticPropertyWatcher("BOLD_ITALIC_CLIENT",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=5 size=1
        [
        bindings[3]
        ]
,
                                                                 null
);

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.PropertyWatcher("resourceManager",
            {
                unused: true
            }
,         // writeWatcherListeners id=0 size=3
        [
        bindings[0],
        bindings[1],
        bindings[2]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[1] = new mx.binding.FunctionReturnWatcher("getString",
                                                                     target,
                                                                     function():Array { return [ target.mKey, "CH_TextTypePlain" ]; },
            {
                change: true
            }
,
                                                                     [bindings[0]],
                                                                     null
);

        // writeWatcher id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[3] = new mx.binding.FunctionReturnWatcher("getString",
                                                                     target,
                                                                     function():Array { return [ target.mKey, "CH_TextTypeBold" ]; },
            {
                change: true
            }
,
                                                                     [bindings[1]],
                                                                     null
);

        // writeWatcher id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[4] = new mx.binding.FunctionReturnWatcher("getString",
                                                                     target,
                                                                     function():Array { return [ target.mKey, "CH_TextTypeItalic" ]; },
            {
                change: true
            }
,
                                                                     [bindings[2]],
                                                                     null
);

        // writeWatcher id=6 shouldWriteSelf=false class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true


        // writeWatcherBottom id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[2].updateParent(target);

 





        // writeWatcherBottom id=5 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        import com.birst.flex.core.FontSelectionHBox;
        watchers[5].updateParent(com.birst.flex.core.FontSelectionHBox);

 





        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].updateParent(target);

 





        // writeWatcherBottom id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 1 0 parentWatcher
        watchers[1].parentWatcher = watchers[0];
        watchers[0].addChild(watchers[1]);

 





        // writeWatcherBottom id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 3 0 parentWatcher
        watchers[3].parentWatcher = watchers[0];
        watchers[0].addChild(watchers[3]);

 





        // writeWatcherBottom id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 4 0 parentWatcher
        watchers[4].parentWatcher = watchers[0];
        watchers[0].addChild(watchers[4]);

 





        // writeWatcherBottom id=6 shouldWriteSelf=false class=flex2.compiler.as3.binding.FunctionReturnWatcher

 





    }
}

}
