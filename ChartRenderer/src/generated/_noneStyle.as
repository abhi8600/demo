
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;
import com.hillelcoren.assets.skins.BlankSkin;

[ExcludeClass]

public class _noneStyle
{

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration(".none");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration(".none", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.upSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.fontWeight = "normal";
                this.selectedDownSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.downSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.overSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.paddingTop = 0;
                this.showComma = "yes";
                this.selectedUpSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.selectedFontWeight = "bold";
                this.paddingLeft = 0;
                this.paddingBottom = 0;
                this.paddingRight = 0;
                this.selectedOverSkin = com.hillelcoren.assets.skins.BlankSkin;
            };
        }
    }
}

}
