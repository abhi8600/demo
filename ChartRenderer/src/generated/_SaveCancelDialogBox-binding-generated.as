

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property helpImageClz (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'helpImageClz' moved to '_1678802889helpImageClz'
	 */

    [Bindable(event="propertyChange")]
    private function get helpImageClz():Class
    {
        return this._1678802889helpImageClz;
    }

    private function set helpImageClz(value:Class):void
    {
    	var oldValue:Object = this._1678802889helpImageClz;
        if (oldValue !== value)
        {
            this._1678802889helpImageClz = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "helpImageClz", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property isMultiSelectEdit (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'isMultiSelectEdit' moved to '_315037515isMultiSelectEdit'
	 */

    [Bindable(event="propertyChange")]
    public function get isMultiSelectEdit():Boolean
    {
        return this._315037515isMultiSelectEdit;
    }

    public function set isMultiSelectEdit(value:Boolean):void
    {
    	var oldValue:Object = this._315037515isMultiSelectEdit;
        if (oldValue !== value)
        {
            this._315037515isMultiSelectEdit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isMultiSelectEdit", oldValue, value));
        }
    }



}
