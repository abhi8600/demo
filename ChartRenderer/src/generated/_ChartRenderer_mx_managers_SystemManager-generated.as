package
{

import flash.display.LoaderInfo;
import flash.text.Font;
import flash.text.TextFormat;
import flash.system.ApplicationDomain;
import flash.system.Security;
import flash.utils.getDefinitionByName;
import flash.utils.Dictionary;
import mx.core.IFlexModule;
import mx.core.IFlexModuleFactory;
import mx.core.EmbeddedFontRegistry;
import mx.core.FlexVersion;
import mx.managers.SystemManager;

/**
 *  @private
 */
[ExcludeClass]
public class _ChartRenderer_mx_managers_SystemManager
    extends mx.managers.SystemManager
    implements IFlexModuleFactory
{
    public function _ChartRenderer_mx_managers_SystemManager()
    {
        FlexVersion.compatibilityVersionString = "3.0.0";
        super();
    }

    override     public function create(... params):Object
    {
        if (params.length > 0 && !(params[0] is String))
            return super.create.apply(this, params);

        var mainClassName:String = params.length == 0 ? "ChartRenderer" : String(params[0]);
        var mainClass:Class = Class(getDefinitionByName(mainClassName));
        if (!mainClass)
            return null;

        var instance:Object = new mainClass();
        if (instance is IFlexModule)
            (IFlexModule(instance)).moduleFactory = this;
        if (params.length == 0)
            EmbeddedFontRegistry.registerFonts(info()["fonts"], this);
        return instance;
    }

    override    public function info():Object
    {
        return {
            compiledLocales: [ "en_US" ],
            compiledResourceBundleNames: [ "SharedResources", "collections", "containers", "controls", "core", "dashboard", "effects", "errors", "expressions", "expressions_descr", "expressions_example", "formatters", "logging", "messaging", "rpc", "sharable", "skins", "styles", "utils", "validators" ],
            creationComplete: "setup()",
            currentDomain: ApplicationDomain.currentDomain,
            fonts:       {
"_sans" : {regular:true, bold:false, italic:false, boldItalic:false}
,
"embedVendana" : {regular:true, bold:false, italic:false, boldItalic:false}
}
,
            layout: "absolute",
            mainClassName: "ChartRenderer",
            mixins: [ "_ChartRenderer_FlexInit", "_DividedBoxStyle", "_alertButtonStyleStyle", "_SWFLoaderStyle", "_FormStyle", "_headerDateTextStyle", "_SwatchPanelStyle", "_TitleWindowStyle", "_todayStyleStyle", "_windowStylesStyle", "_FormItemLabelStyle", "_TextInputStyle", "_dateFieldPopupStyle", "_ApplicationControlBarStyle", "_FormItemStyle", "_ComboBoxStyle", "_dataGridStylesStyle", "_headerDragProxyStyleStyle", "_popUpMenuStyle", "_HSliderStyle", "_ProgressBarStyle", "_DragManagerStyle", "_NumericStepperStyle", "_ContainerStyle", "_windowStatusStyle", "_TextAreaStyle", "_swatchPanelTextFieldStyle", "_RadioButtonStyle", "_underlineStyle", "_textAreaHScrollBarStyleStyle", "_MenuBarStyle", "_comboDropdownStyle", "_CheckBoxStyle", "_ButtonStyle", "_DateFieldStyle", "_HDividedBoxStyle", "_linkButtonStyleStyle", "_richTextEditorTextAreaStyleStyle", "_ControlBarStyle", "_actionsMenuButtonStyle", "_textAreaVScrollBarStyleStyle", "_DateChooserStyle", "_globalStyle", "_ListBaseStyle", "_AlertStyle", "_ApplicationStyle", "_ToolTipStyle", "_CursorManagerStyle", "_opaquePanelStyle", "_errorTipStyle", "_MenuStyle", "_TreeStyle", "_DataGridStyle", "_activeTabStyleStyle", "_PanelStyle", "_ScrollBarStyle", "_facebookStyle", "_plainStyle", "_activeButtonStyleStyle", "_macMailStyle", "_CalendarLayoutStyle", "_DataGridItemRendererStyle", "_ColorPickerStyle", "_weekDayStyleStyle", "_noneStyle", "_com_birst_flex_core_FileUploaderWatcherSetupUtil", "_com_birst_flex_core_expression_ExprBuilderDialogBoxWatcherSetupUtil", "_com_birst_flex_core_DateTimePickerWatcherSetupUtil", "_com_birst_flex_core_BirstAlertWatcherSetupUtil", "_com_birst_flex_core_FontSelectionHBoxWatcherSetupUtil", "_com_birst_flex_dashboards_components_PromptControlWatcherSetupUtil", "_com_birst_flex_dashboards_components_PromptDisplayWatcherSetupUtil", "_com_birst_flex_core_ConfirmationDialogWatcherSetupUtil" ]
        }
    }


    /**
     *  @private
     */
    private var _preloadedRSLs:Dictionary; // key: LoaderInfo, value: RSL URL

    /**
     *  The RSLs loaded by this system manager before the application
     *  starts. RSLs loaded by the application are not included in this list.
     */
    override     public function get preloadedRSLs():Dictionary
    {
        if (_preloadedRSLs == null)
           _preloadedRSLs = new Dictionary(true);
        return _preloadedRSLs;
    }

    /**
     *  Calls Security.allowDomain() for the SWF associated with this IFlexModuleFactory
     *  plus all the SWFs assocatiated with RSLs preLoaded by this IFlexModuleFactory.
     *
     */
    override     public function allowDomain(... domains):void
    {
        Security.allowDomain(domains);

        for (var loaderInfo:Object in _preloadedRSLs)
        {
            if (loaderInfo.content && ("allowDomainInRSL" in loaderInfo.content))
            {
                loaderInfo.content["allowDomainInRSL"](domains);
            }
        }
    }

    /**
     *  Calls Security.allowInsecureDomain() for the SWF associated with this IFlexModuleFactory
     *  plus all the SWFs assocatiated with RSLs preLoaded by this IFlexModuleFactory.
     *
     */
    override     public function allowInsecureDomain(... domains):void
    {
        Security.allowInsecureDomain(domains);

        for (var loaderInfo:Object in _preloadedRSLs)
        {
            if (loaderInfo.content && ("allowInsecureDomainInRSL" in loaderInfo.content))
            {
                loaderInfo.content["allowInsecureDomainInRSL"](domains);
            }
        }
    }


}

}
