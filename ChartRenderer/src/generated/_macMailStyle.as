
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;
import com.hillelcoren.assets.skins.MacMailSkin;

[ExcludeClass]

public class _macMailStyle
{

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration(".macMail");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration(".macMail", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.upSkin = com.hillelcoren.assets.skins.MacMailSkin;
                this.fontWeight = "normal";
                this.selectedDownSkin = com.hillelcoren.assets.skins.MacMailSkin;
                this.downSkin = com.hillelcoren.assets.skins.MacMailSkin;
                this.overSkin = com.hillelcoren.assets.skins.MacMailSkin;
                this.paddingTop = 0;
                this.selectedUpSkin = com.hillelcoren.assets.skins.MacMailSkin;
                this.paddingLeft = 8;
                this.textSelectedColor = 0xffffff;
                this.paddingBottom = 0;
                this.paddingRight = 8;
                this.selectedOverSkin = com.hillelcoren.assets.skins.MacMailSkin;
            };
        }
    }
}

}
