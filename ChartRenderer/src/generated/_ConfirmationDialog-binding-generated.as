

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import mx.controls.Button;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import mx.core.UIComponent;
import mx.styles.*;
import flash.system.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property noButton (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'noButton' moved to '_165416019noButton'
	 */

    [Bindable(event="propertyChange")]
    public function get noButton():mx.controls.Button
    {
        return this._165416019noButton;
    }

    public function set noButton(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._165416019noButton;
        if (oldValue !== value)
        {
            this._165416019noButton = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noButton", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property yesButton (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'yesButton' moved to '_1256381689yesButton'
	 */

    [Bindable(event="propertyChange")]
    public function get yesButton():mx.controls.Button
    {
        return this._1256381689yesButton;
    }

    public function set yesButton(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._1256381689yesButton;
        if (oldValue !== value)
        {
            this._1256381689yesButton = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "yesButton", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property message (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'message' moved to '_954925063message'
	 */

    [Bindable(event="propertyChange")]
    public function get message():String
    {
        return this._954925063message;
    }

    public function set message(value:String):void
    {
    	var oldValue:Object = this._954925063message;
        if (oldValue !== value)
        {
            this._954925063message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property caller (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'caller' moved to '_1367775349caller'
	 */

    [Bindable(event="propertyChange")]
    public function get caller():UIComponent
    {
        return this._1367775349caller;
    }

    public function set caller(value:UIComponent):void
    {
    	var oldValue:Object = this._1367775349caller;
        if (oldValue !== value)
        {
            this._1367775349caller = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "caller", oldValue, value));
        }
    }



}
