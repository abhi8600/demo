

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import mx.containers.Canvas;
import com.anychart.AnyChartFlex;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property Chart (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'Chart' moved to '_65071038Chart'
	 */

    [Bindable(event="propertyChange")]
    public function get Chart():com.anychart.AnyChartFlex
    {
        return this._65071038Chart;
    }

    public function set Chart(value:com.anychart.AnyChartFlex):void
    {
    	var oldValue:Object = this._65071038Chart;
        if (oldValue !== value)
        {
            this._65071038Chart = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Chart", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property canvas (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'canvas' moved to '_1367706280canvas'
	 */

    [Bindable(event="propertyChange")]
    public function get canvas():mx.containers.Canvas
    {
        return this._1367706280canvas;
    }

    public function set canvas(value:mx.containers.Canvas):void
    {
    	var oldValue:Object = this._1367706280canvas;
        if (oldValue !== value)
        {
            this._1367706280canvas = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "canvas", oldValue, value));
        }
    }



}
