package 
{

import mx.resources.ResourceBundle;

[ExcludeClass]

public class en_US$errors_properties extends ResourceBundle
{

    public function en_US$errors_properties()
    {
		 super("en_US", "errors");
    }

    override protected function getContent():Object
    {
        var content:Object =
        {
            "-105": "We are currently processing data. Please come back later. {0}",
            "-103": "Data is unavailable to satisfy a query. Please contact your administrator. {0}",
            "-133": "Your last action would have resulted in a trellis chart with too many instances. Please add a filter on one or more of the attributes in your report first and then choose the desired visualization type again.",
            "-102": "A query or expression contains an unknown column. Please contact your administrator. {0}",
            "-101": "A query or expression contains incorrect syntax. Please contact your administrator. {0}",
            "-131": "Drill Map Name already exists",
            "-100": "We is unable to navigate a query. Please contact your administrator. {0}",
            "-130": "Drill From Column already exists in another Drill Map. {0}",
            "-3": "The query was cancelled successfully.",
            "-2": "An internal error occurred. Please contact your administrator.",
            "-119": "No default bookmark was found.",
            "-1": "Your user session timed out. Please log back in. {0}",
            "-117": "Query Generation not permitted for the given logical query. {0}",
            "-116": "Invalid Google Analytics column combination. {0}",
            "-115": "The required session variable is unavailable. {0}",
            "-114": "The pivot table is too wide. {0}",
            "-113": "The query has timed out.  Please contact your administrator. {0}",
            "-111": "An error has occurred when trying to render the report. Please contact your administrator. {0}",
            "-110": "An internal error occurred.  Please contact your administrator.",
            "-129": "We have detected a possible incorrect security filter configuration",
            "-128": "Space is being swapped currently",
            "AL_title": "Error",
            "-201": "Cannot parse file: {0}",
            "-200": "File not found: {0}",
            "-123": "We have detected possibly incorrectly formatted processed data. Please contact your administrator.",
            "-109": "The report or dashboard file was not found. {0}",
            "-108": "You do not have the correct permissions for this operation. {0}",
            "-107": "A query return too large of a result set. Please limit or modify the query. {0}"
        };
        return content;
    }
}



}
