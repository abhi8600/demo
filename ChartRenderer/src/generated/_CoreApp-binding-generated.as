

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property mainBundle (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'mainBundle' moved to '_20958309mainBundle'
	 */

    [Bindable(event="propertyChange")]
    public function get mainBundle():String
    {
        return this._20958309mainBundle;
    }

    public function set mainBundle(value:String):void
    {
    	var oldValue:Object = this._20958309mainBundle;
        if (oldValue !== value)
        {
            this._20958309mainBundle = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mainBundle", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property retrieveLocaleInfo (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'retrieveLocaleInfo' moved to '_1649879940retrieveLocaleInfo'
	 */

    [Bindable(event="propertyChange")]
    public function get retrieveLocaleInfo():Boolean
    {
        return this._1649879940retrieveLocaleInfo;
    }

    public function set retrieveLocaleInfo(value:Boolean):void
    {
    	var oldValue:Object = this._1649879940retrieveLocaleInfo;
        if (oldValue !== value)
        {
            this._1649879940retrieveLocaleInfo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "retrieveLocaleInfo", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property localeDateTimeFormat (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'localeDateTimeFormat' moved to '_1073113196localeDateTimeFormat'
	 */

    [Bindable(event="propertyChange")]
    public function get localeDateTimeFormat():String
    {
        return this._1073113196localeDateTimeFormat;
    }

    public function set localeDateTimeFormat(value:String):void
    {
    	var oldValue:Object = this._1073113196localeDateTimeFormat;
        if (oldValue !== value)
        {
            this._1073113196localeDateTimeFormat = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "localeDateTimeFormat", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property localeDateFormat (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'localeDateFormat' moved to '_1431706431localeDateFormat'
	 */

    [Bindable(event="propertyChange")]
    public function get localeDateFormat():String
    {
        return this._1431706431localeDateFormat;
    }

    public function set localeDateFormat(value:String):void
    {
    	var oldValue:Object = this._1431706431localeDateFormat;
        if (oldValue !== value)
        {
            this._1431706431localeDateFormat = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "localeDateFormat", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property processingTimezoneUTC (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'processingTimezoneUTC' moved to '_1146801704processingTimezoneUTC'
	 */

    [Bindable(event="propertyChange")]
    public function get processingTimezoneUTC():String
    {
        return this._1146801704processingTimezoneUTC;
    }

    public function set processingTimezoneUTC(value:String):void
    {
    	var oldValue:Object = this._1146801704processingTimezoneUTC;
        if (oldValue !== value)
        {
            this._1146801704processingTimezoneUTC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "processingTimezoneUTC", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property displayTimezoneUTC (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'displayTimezoneUTC' moved to '_220285751displayTimezoneUTC'
	 */

    [Bindable(event="propertyChange")]
    public function get displayTimezoneUTC():String
    {
        return this._220285751displayTimezoneUTC;
    }

    public function set displayTimezoneUTC(value:String):void
    {
    	var oldValue:Object = this._220285751displayTimezoneUTC;
        if (oldValue !== value)
        {
            this._220285751displayTimezoneUTC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "displayTimezoneUTC", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property processingTimezoneOffset (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'processingTimezoneOffset' moved to '_1941280735processingTimezoneOffset'
	 */

    [Bindable(event="propertyChange")]
    public function get processingTimezoneOffset():int
    {
        return this._1941280735processingTimezoneOffset;
    }

    public function set processingTimezoneOffset(value:int):void
    {
    	var oldValue:Object = this._1941280735processingTimezoneOffset;
        if (oldValue !== value)
        {
            this._1941280735processingTimezoneOffset = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "processingTimezoneOffset", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property displayTimezoneOffset (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'displayTimezoneOffset' moved to '_23225166displayTimezoneOffset'
	 */

    [Bindable(event="propertyChange")]
    public function get displayTimezoneOffset():int
    {
        return this._23225166displayTimezoneOffset;
    }

    public function set displayTimezoneOffset(value:int):void
    {
    	var oldValue:Object = this._23225166displayTimezoneOffset;
        if (oldValue !== value)
        {
            this._23225166displayTimezoneOffset = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "displayTimezoneOffset", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property isSelfScheduleEnabled (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'isSelfScheduleEnabled' moved to '_539063860isSelfScheduleEnabled'
	 */

    [Bindable(event="propertyChange")]
    public function get isSelfScheduleEnabled():Boolean
    {
        return this._539063860isSelfScheduleEnabled;
    }

    public function set isSelfScheduleEnabled(value:Boolean):void
    {
    	var oldValue:Object = this._539063860isSelfScheduleEnabled;
        if (oldValue !== value)
        {
            this._539063860isSelfScheduleEnabled = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isSelfScheduleEnabled", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property useNewQueryLang (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'useNewQueryLang' moved to '_1372900413useNewQueryLang'
	 */

    [Bindable(event="propertyChange")]
    public function get useNewQueryLang():Boolean
    {
        return this._1372900413useNewQueryLang;
    }

    public function set useNewQueryLang(value:Boolean):void
    {
    	var oldValue:Object = this._1372900413useNewQueryLang;
        if (oldValue !== value)
        {
            this._1372900413useNewQueryLang = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "useNewQueryLang", oldValue, value));
        }
    }



}
