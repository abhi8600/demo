
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;
import mx.skins.halo.DateChooserYearArrowSkin;
import mx.skins.halo.DateChooserMonthArrowSkin;
import mx.skins.halo.DateChooserIndicator;

[ExcludeClass]

public class _DateChooserStyle
{

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration("DateChooser");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration("DateChooser", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.nextYearSkin = mx.skins.halo.DateChooserYearArrowSkin;
                this.backgroundColor = 0xffffff;
                this.todayStyleName = "todayStyle";
                this.headerStyleName = "headerDateText";
                this.rollOverIndicatorSkin = mx.skins.halo.DateChooserIndicator;
                this.cornerRadius = 4;
                this.nextMonthSkin = mx.skins.halo.DateChooserMonthArrowSkin;
                this.selectionIndicatorSkin = mx.skins.halo.DateChooserIndicator;
                this.prevMonthSkin = mx.skins.halo.DateChooserMonthArrowSkin;
                this.todayColor = 0x818181;
                this.weekDayStyleName = "weekDayStyle";
                this.headerColors = [0xe1e5eb, 0xf4f5f7];
                this.todayIndicatorSkin = mx.skins.halo.DateChooserIndicator;
                this.prevYearSkin = mx.skins.halo.DateChooserYearArrowSkin;
            };
        }
    }
}

}
