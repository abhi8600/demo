

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import mx.styles.*;
import flash.system.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property TYPE_INT (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_INT' moved to '_107586890TYPE_INT'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_INT():String
    {
        return FlexField._107586890TYPE_INT;
    }

    static public function set TYPE_INT(value:String):void
    {
    	var oldValue:Object = FlexField._107586890TYPE_INT;
        if (oldValue !== value)
        {
            FlexField._107586890TYPE_INT = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_INT", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_DOUBLE (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_DOUBLE' moved to '_933310582TYPE_DOUBLE'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_DOUBLE():String
    {
        return FlexField._933310582TYPE_DOUBLE;
    }

    static public function set TYPE_DOUBLE(value:String):void
    {
    	var oldValue:Object = FlexField._933310582TYPE_DOUBLE;
        if (oldValue !== value)
        {
            FlexField._933310582TYPE_DOUBLE = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_DOUBLE", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_VARCHAR (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_VARCHAR' moved to '_1554468568TYPE_VARCHAR'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_VARCHAR():String
    {
        return FlexField._1554468568TYPE_VARCHAR;
    }

    static public function set TYPE_VARCHAR(value:String):void
    {
    	var oldValue:Object = FlexField._1554468568TYPE_VARCHAR;
        if (oldValue !== value)
        {
            FlexField._1554468568TYPE_VARCHAR = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_VARCHAR", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_BOOLEAN (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_BOOLEAN' moved to '_1382566915TYPE_BOOLEAN'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_BOOLEAN():String
    {
        return FlexField._1382566915TYPE_BOOLEAN;
    }

    static public function set TYPE_BOOLEAN(value:String):void
    {
    	var oldValue:Object = FlexField._1382566915TYPE_BOOLEAN;
        if (oldValue !== value)
        {
            FlexField._1382566915TYPE_BOOLEAN = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_BOOLEAN", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_TIMESTAMP (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_TIMESTAMP' moved to '_501384655TYPE_TIMESTAMP'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_TIMESTAMP():String
    {
        return FlexField._501384655TYPE_TIMESTAMP;
    }

    static public function set TYPE_TIMESTAMP(value:String):void
    {
    	var oldValue:Object = FlexField._501384655TYPE_TIMESTAMP;
        if (oldValue !== value)
        {
            FlexField._501384655TYPE_TIMESTAMP = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_TIMESTAMP", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_DATE (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_DATE' moved to '_959935085TYPE_DATE'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_DATE():String
    {
        return FlexField._959935085TYPE_DATE;
    }

    static public function set TYPE_DATE(value:String):void
    {
    	var oldValue:Object = FlexField._959935085TYPE_DATE;
        if (oldValue !== value)
        {
            FlexField._959935085TYPE_DATE = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_DATE", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property TYPE_VARIABLE (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'TYPE_VARIABLE' moved to '_949218785TYPE_VARIABLE'
	 */

    [Bindable(event="propertyChange")]
    static public function get TYPE_VARIABLE():String
    {
        return FlexField._949218785TYPE_VARIABLE;
    }

    static public function set TYPE_VARIABLE(value:String):void
    {
    	var oldValue:Object = FlexField._949218785TYPE_VARIABLE;
        if (oldValue !== value)
        {
            FlexField._949218785TYPE_VARIABLE = value;
            var eventDispatcher:IEventDispatcher = FlexField.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FlexField, "TYPE_VARIABLE", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property control (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'control' moved to '_951543133control'
	 */

    [Bindable(event="propertyChange")]
    public function get control():Object
    {
        return this._951543133control;
    }

    public function set control(value:Object):void
    {
    	var oldValue:Object = this._951543133control;
        if (oldValue !== value)
        {
            this._951543133control = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "control", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property text (public)
	 * - generated setter
	 * - original getter left as-is
	 * - original public setter 'text' moved to '_3556653text'
	 */

    [Bindable(event="propertyChange")]
    public function set text(value:String):void
    {
    	var oldValue:Object = this.text;
        if (oldValue !== value)
        {
            this._3556653text = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "text", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property type (public)
	 * - generated setter
	 * - original getter left as-is
	 * - original public setter 'type' moved to '_3575610type'
	 */

    [Bindable(event="propertyChange")]
    public function set type(value:String):void
    {
    	var oldValue:Object = this.type;
        if (oldValue !== value)
        {
            this._3575610type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property editable (public)
	 * - generated setter
	 * - original getter left as-is
	 * - original public setter 'editable' moved to '_1602416228editable'
	 */

    [Bindable(event="propertyChange")]
    public function set editable(value:Boolean):void
    {
    	var oldValue:Object = this.editable;
        if (oldValue !== value)
        {
            this._1602416228editable = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "editable", oldValue, value));
        }
    }



    private static var _staticBindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher();

    public static function get staticEventDispatcher():IEventDispatcher
    {
        return _staticBindingEventDispatcher;
    }
}
