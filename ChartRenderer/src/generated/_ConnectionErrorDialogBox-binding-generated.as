

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import mx.controls.Button;
import mx.controls.Text;
import mx.controls.TextArea;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property closeBttn (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'closeBttn' moved to '_483007388closeBttn'
	 */

    [Bindable(event="propertyChange")]
    public function get closeBttn():mx.controls.Button
    {
        return this._483007388closeBttn;
    }

    public function set closeBttn(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._483007388closeBttn;
        if (oldValue !== value)
        {
            this._483007388closeBttn = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "closeBttn", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property errorMessage (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'errorMessage' moved to '_1203236063errorMessage'
	 */

    [Bindable(event="propertyChange")]
    public function get errorMessage():mx.controls.TextArea
    {
        return this._1203236063errorMessage;
    }

    public function set errorMessage(value:mx.controls.TextArea):void
    {
    	var oldValue:Object = this._1203236063errorMessage;
        if (oldValue !== value)
        {
            this._1203236063errorMessage = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "errorMessage", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property message (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'message' moved to '_954925063message'
	 */

    [Bindable(event="propertyChange")]
    public function get message():mx.controls.Text
    {
        return this._954925063message;
    }

    public function set message(value:mx.controls.Text):void
    {
    	var oldValue:Object = this._954925063message;
        if (oldValue !== value)
        {
            this._954925063message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, value));
        }
    }



}
