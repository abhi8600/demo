package 
{

import mx.resources.ResourceBundle;

[ExcludeClass]

public class en_US$sharable_properties extends ResourceBundle
{

    public function en_US$sharable_properties()
    {
		 super("en_US", "sharable");
    }

    override protected function getContent():Object
    {
        var content:Object =
        {
            "AL_cancel": "Cancel",
            "WC_okay": "OK",
            "AL_ok": "Ok",
            "FU_uploading": "Uploading",
            "MIL_failLoad": "Error loading module {0} Reason:\n {1}",
            "FOD_Open": "Select",
            "EX_editBttn": "Edit",
            "EX_exprDnD": "Drag and drop from the subject area and function list to build the expression.",
            "UM_noLicense": "Not licensed for chart type",
            "LK_failJS": "Error calling javascript function: ExternalInterface not available.",
            "//": "WC_failCon=We were unable to contact the server. This could be due to a network problem or a timed out session.\n\nReason:\n {0}\t",
            "FU_upComplete": "Image uploaded",
            "FU_upload": "Upload",
            "SH_HELP_WINDOW": "_blank",
            "EX_title": "Expression Builder",
            "FU_browse": "Browse...",
            "SH_confirmation": "Confirmation",
            "LK_failFunc": "Error calling javascript function: {0}",
            "failOverridenVariables": "Failed to retrieve variables.",
            "AL_no": "No",
            "CR_endAll": "End all requests?",
            "FU_succ": "Success",
            "FOD_Cancel": "Cancel",
            "EX_dt": "Date and Time",
            "FU_failBrowse": "Unable to browse for files",
            "SH_HELP_BXE_URL": "Help/BXE/index.htm?#{0}.htm",
            "EX_mt": "Math",
            "failLoadReportDefaults": "Failed to retrieve default report properties.  Verify that a .default report file exists in the shared directory.",
            "FU_failUp": "Upload failed",
            "EX_st": "String",
            "EX_pr": "Prompts",
            "WC_detail": "Details",
            "CR_clickToEnd": "Click to end all requests.",
            "FAIL_sortEntsOrdersLen": "Number of sort entities != number of sort order",
            "AL_yes": "Yes",
            "FOD_Title": "Report Selection",
            "RP_cancel": "Cancel",
            "FX_failVar": "Cannot get variable list.",
            "SH_HELP_FULL_URL": "Help/Full/index.htm?#{0}.htm",
            "FU_uploaded": "Uploaded",
            "FU_tooBig": "File size is bigger than maximum limit of {0} bytes.",
            "AL_save": "Save",
            "EX_ot": "Other",
            "WC_close": "Close",
            "WC_sendSupp": "To help us diagnose the problem, please copy and paste the error message and email to support. Thank you.",
            "WC_failCon": "Sorry, your browser had trouble connecting to the server.  Please try again. If the problem persists, log out and back in.",
            "EX_op": "Operators",
            "EX_rp": "Rank and Ptile",
            "EX_lk": "Lookups",
            "RP_save": "Save",
            "CR_clickToCancel": "Click here to cancel",
            "EX_rl": "Report Level",
            "EX_lg": "Logical",
            "DGC_selRow": "Select a row for deletion",
            "RP_open": "Open"
        };
        return content;
    }
}



}
