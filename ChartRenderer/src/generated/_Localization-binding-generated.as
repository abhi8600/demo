

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;

class BindableProperty
    implements flash.events.IEventDispatcher
{
	/**
	 * generated bindable wrapper for property ADHOC (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'ADHOC' moved to '_62126361ADHOC'
	 */

    [Bindable(event="propertyChange")]
    static public function get ADHOC():String
    {
        return Localization._62126361ADHOC;
    }

    static public function set ADHOC(value:String):void
    {
    	var oldValue:Object = Localization._62126361ADHOC;
        if (oldValue !== value)
        {
            Localization._62126361ADHOC = value;
            var eventDispatcher:IEventDispatcher = Localization.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(Localization, "ADHOC", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property SHARABLE (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'SHARABLE' moved to '_38751136SHARABLE'
	 */

    [Bindable(event="propertyChange")]
    static public function get SHARABLE():String
    {
        return Localization._38751136SHARABLE;
    }

    static public function set SHARABLE(value:String):void
    {
    	var oldValue:Object = Localization._38751136SHARABLE;
        if (oldValue !== value)
        {
            Localization._38751136SHARABLE = value;
            var eventDispatcher:IEventDispatcher = Localization.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(Localization, "SHARABLE", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property SCHEDULER (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'SCHEDULER' moved to '_1669082981SCHEDULER'
	 */

    [Bindable(event="propertyChange")]
    static public function get SCHEDULER():String
    {
        return Localization._1669082981SCHEDULER;
    }

    static public function set SCHEDULER(value:String):void
    {
    	var oldValue:Object = Localization._1669082981SCHEDULER;
        if (oldValue !== value)
        {
            Localization._1669082981SCHEDULER = value;
            var eventDispatcher:IEventDispatcher = Localization.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(Localization, "SCHEDULER", oldValue, value));
            }
        }
    }


    //    IEventDispatcher implementation
    //
    private var _bindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher(flash.events.IEventDispatcher(this));

    public function addEventListener(type:String, listener:Function,
                                     useCapture:Boolean = false,
                                     priority:int = 0,
                                     weakRef:Boolean = false):void
    {
        _bindingEventDispatcher.addEventListener(type, listener, useCapture,
                                                 priority, weakRef);
    }

    public function dispatchEvent(event:flash.events.Event):Boolean
    {
        return _bindingEventDispatcher.dispatchEvent(event);
    }

    public function hasEventListener(type:String):Boolean
    {
        return _bindingEventDispatcher.hasEventListener(type);
    }

    public function removeEventListener(type:String,
                                        listener:Function,
                                        useCapture:Boolean = false):void
    {
        _bindingEventDispatcher.removeEventListener(type, listener, useCapture);
    }

    public function willTrigger(type:String):Boolean
    {
        return _bindingEventDispatcher.willTrigger(type);
    }

    private static var _staticBindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher();

    public static function get staticEventDispatcher():IEventDispatcher
    {
        return _staticBindingEventDispatcher;
    }
}
