

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.dashboards.components.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property colName (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'colName' moved to '_948547627colName'
	 */

    [Bindable(event="propertyChange")]
    public function get colName():String
    {
        return this._948547627colName;
    }

    public function set colName(value:String):void
    {
    	var oldValue:Object = this._948547627colName;
        if (oldValue !== value)
        {
            this._948547627colName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "colName", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property isDefault (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'isDefault' moved to '_965025207isDefault'
	 */

    [Bindable(event="propertyChange")]
    public function get isDefault():Boolean
    {
        return this._965025207isDefault;
    }

    public function set isDefault(value:Boolean):void
    {
    	var oldValue:Object = this._965025207isDefault;
        if (oldValue !== value)
        {
            this._965025207isDefault = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isDefault", oldValue, value));
        }
    }



}
