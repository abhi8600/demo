






package
{
import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
[Mixin]
public class _com_birst_flex_core_DateTimePickerWatcherSetupUtil extends Sprite
    implements mx.binding.IWatcherSetupUtil
{
    public function _com_birst_flex_core_DateTimePickerWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import com.birst.flex.core.DateTimePicker;
        (com.birst.flex.core.DateTimePicker).watcherSetupUtil = new _com_birst_flex_core_DateTimePickerWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import mx.core.UIComponentDescriptor;
        import mx.controls.RadioButtonGroup;
        import mx.core.DeferredInstanceFromClass;
        import mx.utils.ObjectProxy;
        import __AS3__.vec.Vector;
        import mx.binding.IBindingClient;
        import com.flexoop.utilities.dateutils.DaylightSavingTimeUS;
        import mx.core.ClassFactory;
        import mx.core.IFactory;
        import mx.utils.ObjectUtil;
        import mx.controls.Button;
        import mx.core.DeferredInstanceFromFunction;
        import mx.controls.DateField;
        import mx.utils.UIDUtil;
        import flash.events.EventDispatcher;
        import mx.core.Application;
        import mx.controls.NumericStepper;
        import mx.containers.HBox;
        import mx.binding.BindingManager;
        import mx.controls.RadioButton;
        import mx.core.IDeferredInstance;
        import mx.events.NumericStepperEvent;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import mx.events.PropertyChangeEvent;
        import mx.core.mx_internal;
        import mx.events.FlexEvent;
        import mx.events.DropdownEvent;
        import mx.events.CalendarLayoutChangeEvent;
        import flash.events.Event;

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.PropertyWatcher("ampm",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=0 size=2
        [
        bindings[0],
        bindings[1]
        ]
,
                                                                 propertyGetter
);


        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].updateParent(target);

 





    }
}

}
