
/**
 * 	Generated by mxmlc 2.0
 *
 *	Package:	com.birst.flex.core.expression
 *	Class: 		ExprBuilderDialogBox
 *	Source: 	C:\Users\rmcginty\Projects\birst\FlexAdhoc\src\com\birst\flex\core\expression\ExprBuilderDialogBox.mxml
 *	Template: 	flex2/compiler/mxml/gen/ClassDef.vm
 *	Time: 		2013.11.13 15:53:14 PST
 */

package com.birst.flex.core.expression
{

import com.birst.flex.core.SaveCancelDialogBox;
import flash.accessibility.*;
import flash.debugger.*;
import flash.display.*;
import flash.errors.*;
import flash.events.*;
import flash.external.*;
import flash.filters.*;
import flash.geom.*;
import flash.media.*;
import flash.net.*;
import flash.printing.*;
import flash.profiler.*;
import flash.system.*;
import flash.text.*;
import flash.ui.*;
import flash.utils.*;
import flash.xml.*;
import mx.binding.*;
import mx.binding.IBindingClient;
import mx.containers.HBox;
import mx.containers.HDividedBox;
import mx.containers.VBox;
import mx.controls.ComboBox;
import mx.controls.Label;
import mx.controls.List;
import mx.controls.TextArea;
import mx.core.ClassFactory;
import mx.core.DeferredInstanceFromClass;
import mx.core.DeferredInstanceFromFunction;
import mx.core.IDeferredInstance;
import mx.core.IFactory;
import mx.core.IPropertyChangeNotifier;
import mx.core.UIComponentDescriptor;
import mx.core.mx_internal;
import mx.events.FlexEvent;
import mx.events.ListEvent;
import mx.modules.ModuleLoader;
import mx.styles.*;



		[ResourceBundle("expressions")]
		[ResourceBundle("expressions_descr")]
		[ResourceBundle("expressions_example")]
	


public class ExprBuilderDialogBox
	extends com.birst.flex.core.SaveCancelDialogBox
	implements mx.binding.IBindingClient
{

/**
 * @private
 **/
	public var _ExprBuilderDialogBox_Label3 : mx.controls.Label;

	[Bindable]
/**
 * @private
 **/
	public var expr : mx.controls.TextArea;

	[Bindable]
/**
 * @private
 **/
	public var fnDescr : mx.controls.TextArea;

	[Bindable]
/**
 * @private
 **/
	public var funcCombo : mx.controls.ComboBox;

	[Bindable]
/**
 * @private
 **/
	public var funcList : mx.controls.List;

	[Bindable]
/**
 * @private
 **/
	public var subjectAreaModule : mx.modules.ModuleLoader;




private var _documentDescriptor_ : mx.core.UIComponentDescriptor = 
new mx.core.UIComponentDescriptor({
  type: com.birst.flex.core.SaveCancelDialogBox
  ,
  propertiesFactory: function():Object { return {
    height: 500,
    width: 750,
    childDescriptors: [
      new mx.core.UIComponentDescriptor({
        type: mx.containers.HDividedBox
        ,
        propertiesFactory: function():Object { return {
          percentHeight: 100.0,
          percentWidth: 100.0,
          childDescriptors: [
            new mx.core.UIComponentDescriptor({
              type: mx.containers.VBox
              ,
              propertiesFactory: function():Object { return {
                percentHeight: 100.0,
                percentWidth: 35.0,
                childDescriptors: [
                  new mx.core.UIComponentDescriptor({
                    type: mx.modules.ModuleLoader
                    ,
                    id: "subjectAreaModule"
                    ,
                    stylesFactory: function():void {
                      this.backgroundAlpha = 0.1;
                      this.backgroundColor = 16777215;
                    }
                    ,
                    propertiesFactory: function():Object { return {
                      percentWidth: 100.0,
                      percentHeight: 100.0
                    }}
                  })
                ]
              }}
            })
          ,
            new mx.core.UIComponentDescriptor({
              type: mx.containers.VBox
              ,
              propertiesFactory: function():Object { return {
                percentHeight: 100.0,
                percentWidth: 65.0,
                childDescriptors: [
                  new mx.core.UIComponentDescriptor({
                    type: mx.controls.TextArea
                    ,
                    id: "expr"
                    ,
                    propertiesFactory: function():Object { return {
                      percentHeight: 55.0,
                      percentWidth: 100.0
                    }}
                  })
                ,
                  new mx.core.UIComponentDescriptor({
                    type: mx.containers.HBox
                    ,
                    propertiesFactory: function():Object { return {
                      percentWidth: 100.0,
                      percentHeight: 45.0,
                      childDescriptors: [
                        new mx.core.UIComponentDescriptor({
                          type: mx.containers.VBox
                          ,
                          stylesFactory: function():void {
                            this.verticalGap = 1;
                          }
                          ,
                          propertiesFactory: function():Object { return {
                            percentWidth: 30.0,
                            percentHeight: 100.0,
                            childDescriptors: [
                              new mx.core.UIComponentDescriptor({
                                type: mx.controls.Label
                                ,
                                stylesFactory: function():void {
                                  this.paddingTop = 0;
                                }
                                ,
                                propertiesFactory: function():Object { return {
                                  text: "Function"
                                }}
                              })
                            ,
                              new mx.core.UIComponentDescriptor({
                                type: mx.controls.ComboBox
                                ,
                                id: "funcCombo"
                                ,
                                events: {
                                  change: "__funcCombo_change"
                                }
                                ,
                                propertiesFactory: function():Object { return {
                                  percentWidth: 100.0
                                }}
                              })
                            ,
                              new mx.core.UIComponentDescriptor({
                                type: mx.controls.List
                                ,
                                id: "funcList"
                                ,
                                events: {
                                  change: "__funcList_change"
                                }
                                ,
                                propertiesFactory: function():Object { return {
                                  percentWidth: 100.0,
                                  percentHeight: 100.0,
                                  dragEnabled: true
                                }}
                              })
                            ]
                          }}
                        })
                      ,
                        new mx.core.UIComponentDescriptor({
                          type: mx.containers.VBox
                          ,
                          stylesFactory: function():void {
                            this.verticalGap = 1;
                          }
                          ,
                          propertiesFactory: function():Object { return {
                            percentWidth: 70.0,
                            percentHeight: 100.0,
                            childDescriptors: [
                              new mx.core.UIComponentDescriptor({
                                type: mx.controls.Label
                                ,
                                stylesFactory: function():void {
                                  this.paddingTop = 0;
                                }
                                ,
                                propertiesFactory: function():Object { return {
                                  text: "Description"
                                }}
                              })
                            ,
                              new mx.core.UIComponentDescriptor({
                                type: mx.controls.TextArea
                                ,
                                id: "fnDescr"
                                ,
                                stylesFactory: function():void {
                                  this.backgroundColor = 16777190;
                                }
                                ,
                                propertiesFactory: function():Object { return {
                                  percentHeight: 100.0,
                                  percentWidth: 100.0,
                                  editable: false
                                }}
                              })
                            ]
                          }}
                        })
                      ]
                    }}
                  })
                ]
              }}
            })
          ]
        }}
      })
    ,
      new mx.core.UIComponentDescriptor({
        type: mx.controls.Label
        ,
        id: "_ExprBuilderDialogBox_Label3"
        ,
        propertiesFactory: function():Object { return {
          height: 20,
          percentWidth: 100.0
        }}
      })
    ]
  }}
})

    /**
     * @private
     **/
	public function ExprBuilderDialogBox()
	{
		super();

		mx_internal::_document = this;

		//	our style settings



		//	properties
		this.resizable = true;
		this.height = 500;
		this.width = 750;
		this.minHeight = 400;
		this.minWidth = 600;
		this.helpUrl = "expression_builder_dialog_box";

		//	events
		this.addEventListener("initialize", ___ExprBuilderDialogBox_SaveCancelDialogBox1_initialize);

	}

    /**
     * @private
     **/
	override public function initialize():void
	{
 		mx_internal::setDocumentDescriptor(_documentDescriptor_);

		var bindings:Array = _ExprBuilderDialogBox_bindingsSetup();
		var watchers:Array = [];

		var target:ExprBuilderDialogBox = this;

		if (_watcherSetupUtil == null)
		{
			var watcherSetupUtilClass:Object = getDefinitionByName("_com_birst_flex_core_expression_ExprBuilderDialogBoxWatcherSetupUtil");
			watcherSetupUtilClass["init"](null);
		}

		_watcherSetupUtil.setup(this,
					function(propertyName:String):* { return target[propertyName]; },
					bindings,
					watchers);

		for (var i:uint = 0; i < bindings.length; i++)
		{
			Binding(bindings[i]).execute();
		}

		mx_internal::_bindings = mx_internal::_bindings.concat(bindings);
		mx_internal::_watchers = mx_internal::_watchers.concat(watchers);


		super.initialize();
	}


			import mx.core.IToolTip;
			import com.birst.flex.webservice.WebServiceResult;
			import mx.rpc.events.ResultEvent;
			import com.birst.flex.webservice.WebServiceClient;
			import mx.events.CollectionEvent;
			import mx.collections.ArrayCollection;
			import mx.events.ModuleEvent;
			import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
			import mx.events.DragEvent;
			import mx.core.Application;
			import com.birst.flex.core.CoreApp;
			import com.birst.flex.core.DragDropHelper;
			import mx.managers.DragManager;
			import mx.core.UIComponent;
			import mx.managers.ToolTipManager;
			import mx.events.CloseEvent;
			import flash.events.MouseEvent;
			
			[Bindable]
			private var app:CoreApp = CoreApp(Application.application);
			
			[Bindable]
			private var fnList:Array = [];
			
			[Bindable]
			public var exprRequest:String = 'evaluateExpression';
			
			private var errorTip:IToolTip = null;
			private var borderColor:String = null;
			
			private var parentTextArea:TextArea;
			
			private function init():void{
				//load subject area
				subjectAreaModule.addEventListener(ModuleEvent.READY, onSubjectAreaReady);
				subjectAreaModule.url = "/swf/SubjectAreaModule.swf?noAddColumn=true";
				subjectAreaModule.loadModule();
				
				//drag and drop
				expr.addEventListener(DragEvent.DRAG_ENTER, onDragEnterExpr);
				expr.addEventListener(DragEvent.DRAG_DROP, onDragDropExpr);
				
				//function list
				this.funcList.addEventListener(mx.events.CollectionEvent.COLLECTION_CHANGE, this.onFuncListCollectionChange);
				var initList:Array = ['DatePart', 'DateDiff', 'DateAdd'];
				this.funcList.dataProvider = initList;
				
				fnList = [ { label: getSharable('EX_dt'), 
							 list: initList },
						   { label: getSharable('EX_lg'), 
						   	 list: ['IIF', 'IFNull', 'IsNaN', 'IsInf'] },
						   { label: getSharable('EX_lk'), 
						   	 list: ['Function', 'FunctionLookup', 'LookupValue', 'LookupRow', 'Find', 'Stat'] },
						   { label: getSharable('EX_mt'), 
						   	 list: ['Abs', 'Ceiling', 'Floor', 'Sqrt', 'Ln', 'Log', 'Exp', 'Sin', 'Cos', 'Tan', 'ArcSin', 'ArcCos', 'ArcTan', 'Random', 'Pow', 'Rsum', 'Median'] },
						   { label: getSharable('EX_op'), 
						     list:  [ {label: '=', t: 'op', k: 'OP_EQ'}, 
									  {label: '<', t: 'op', k: 'OP_LT'},
									  {label: '>', t: 'op', k: 'OP_GT'},
									  {label:'<>', t: 'op', k: 'OP_NEQ'},
									  {label: '<=', t: 'op', k: 'OP_LT_EQ'},  
									  {label: '>=', t: 'op', k: 'OP_GT_EQ'} ,  
									  {label: 'Like', t: 'op'},  
									  {label: 'Not Like', t: 'op'},  
									  {label: 'Is Null', t: 'op'},  
									  {label:'Is Not Null', t: 'op'},
									  {label:'AND', t: 'op'},
									  {label:'OR', t: 'op'}]},
						   { label: getSharable('EX_ot'), 
						   	 list: ['Positional Calculation', 'Let'] },
						   { label: getSharable('EX_pr'), 
						   	 list: ['GetPromptValue', 'GetPromptFilter'] },
						   { label: getSharable('EX_rp'), 
						   	 list: ['DRank', 'PTile', 'Rank'] },
						   { label: getSharable('EX_rl'), 
						   	 list: ['Transform', 'RowNumber', 'NumRows', 'Sparse'] },
						   { label: getSharable('EX_st'), 
						     list: ['Substring', 'Length', 'Position', 'Trim', 'Replace', 'ReplaceAll', 'Format', 'ToLower', 'ToUpper'] }
						   ];	
			}
			
			private function onFuncComboChange(event:Event):void{
				this.fnDescr.htmlText = '';
				var selected:Object = (this.funcCombo.dataProvider as ArrayCollection).getItemAt(this.funcCombo.selectedIndex);
				this.funcList.dataProvider = selected.list;
				this.onFuncListCollectionChange();
			}
			
			private function onFuncListChange(event:Event, selectedIndex:int = 0):void{
				var list:List = this.funcList;
				
				var selectedItem:Object = list.selectedItem;
				var key:String = String(selectedItem);
				if(selectedItem.hasOwnProperty('t') && selectedItem.t == 'op'){
					key = selectedItem.hasOwnProperty('k') ? selectedItem.k : selectedItem.label;
				}
				
				var text:String = '';
				
				var expr:String = resourceManager.getString('expressions', key);
				if(expr != null){
					text += '<i><b>' + expr + '</b></i><br><br>';
				}
				var description:String = resourceManager.getString('expressions_descr', key);
				if(description != null){
					text += description + '<br><br>';
				}
				var example:String = resourceManager.getString('expressions_example', key);
				if(example != null){
					text += example;
				}
				
				this.fnDescr.htmlText = text;
			}
			
			private function onFuncListCollectionChange(event:Event = null):void{
				this.funcList.selectedIndex = 0;
				this.onFuncListChange(event);
			}
			
			private function getSharable(key:String):String{
				return resourceManager.getString('sharable', key);
			}
			
			private function onSubjectAreaReady(event:ModuleEvent):void{
				var sa:Object = Object(subjectAreaModule.child);
				if(sa.hasOwnProperty('noAddCol')){
					sa.noAddCol = true;
				}
			}
			
			private function onDragEnterExpr(event:DragEvent):void{
				if(event.dragInitiator == this.funcList){
					var target:UIComponent = event.currentTarget as UIComponent;
					DragManager.acceptDragDrop(target);
					return;
				} 
				DragDropHelper.acceptIfSubjectAreaNode(event, true);	
			}
			
			private function onDragDropExpr(event:DragEvent):void{
				var target:TextArea = event.target as TextArea;
				
				DragDropHelper.revertBorder(target);
				
				if(event.dragInitiator == this.funcList){
					var func:Object = event.draggedItem;
					var source:Object = event.dragSource;
					var item:Object = source.dataForFormat("items");
					if(item && item.length > 0){
						if(item[0].hasOwnProperty('t') && item[0].t == 'op'){
							this.appendToExprTextArea(' ' + item[0].label + ' ');	
						}else{
							if(item == 'Positional Calculation'){
								this.appendToExprTextArea('{');	
							}else{
								this.appendToExprTextArea(item + '(');
							}
						}
					}
				}else{
					var node:XML = DragDropHelper.getSubjectAreaTreeLeaf(event);
					if(node){
						var nExpr:String = ' [' + node.@v + ']';
						if(!app.useNewQueryLang){
							var prefix:String = (node.@t == 'a') ? 'DC{' : 'M{';
							nExpr = prefix + node.@v + '}';
						}
						this.appendToExprTextArea(nExpr);
					}
				}
			}
			
			private function appendToExprTextArea(text:String):void{
				expr.text += text;
				expr.selectionBeginIndex = expr.selectionEndIndex = expr.length;	
				expr.setFocus();
			}
			
			override public function poppedUpHandler(event:DialogPopUpEvent):void{
				// make sure that the error tool tip is removed
				cancelButton.addEventListener(MouseEvent.CLICK, destroyToolTip);
				this.addEventListener(CloseEvent.CLOSE, destroyToolTip);
				
				parentTextArea = TextArea(event.menuItem.expr);
				if(parentTextArea){
					expr.text = parentTextArea.text;
				}
				
				if(event.menuItem.requestOp){
					this.exprRequest = event.menuItem.requestOp;
				}
			}
			
			override public function saveButtonClickHandler(event:Event):void{
				if(expr.text){ 
					var ws:WebServiceClient = new WebServiceClient('Adhoc', exprRequest, 'ExprBuilderDialogBox', 'saveButtonClickHandler');
					ws.setResultListener(saveHandler);
					ws.execute(expr.text);
				}else{
					finishSave();
				}
			}

			private function finishSave():void{
				if(parentTextArea){
					parentTextArea.text = expr.text;
				}
				
				super.saveButtonClickHandler(null);	
			}
			
			private function saveHandler(result:ResultEvent):void{
				var rs:WebServiceResult = WebServiceResult.parse(result);
				
				destroyToolTip();
				if (!rs.isSuccess()){
					// if there is an error, create an error tool tip
					var pt:Point = new Point(expr.x, expr.y);
					pt = expr.contentToGlobal(pt);
					errorTip = ToolTipManager.createToolTip(rs.getErrorMessage(),
															pt.x, pt.y + expr.height, "errorTipBelow") as IToolTip;					
					(errorTip as UIComponent).setStyle("styleName", "errorTip");
					expr.setStyle("borderColor", 0xFF0000);
					return;
				}
				finishSave();
			}
			
			// destroy the error tool tip
			private function destroyToolTip(event:Event=null):void{
				if (errorTip != null)
					ToolTipManager.destroyToolTip(errorTip);
				errorTip = null;
				expr.setStyle("borderColor", borderColor);
			}
			
		



    //	supporting function definitions for properties, events, styles, effects
/**
 * @private
 **/
public function ___ExprBuilderDialogBox_SaveCancelDialogBox1_initialize(event:mx.events.FlexEvent):void
{
	init();
}

/**
 * @private
 **/
public function __funcCombo_change(event:mx.events.ListEvent):void
{
	onFuncComboChange(event);
}

/**
 * @private
 **/
public function __funcList_change(event:mx.events.ListEvent):void
{
	onFuncListChange(event);
}


	//	binding mgmt
    private function _ExprBuilderDialogBox_bindingsSetup():Array
    {
        var result:Array = [];
        var binding:Binding;

        binding = new mx.binding.Binding(this,
            function():String
            {
                var result:* = (resourceManager.getString('sharable', 'EX_title'));
                var stringResult:String = (result == undefined ? null : String(result));
                return stringResult;
            },
            function(_sourceFunctionReturnValue:String):void
            {
				
                this.title = _sourceFunctionReturnValue;
            },
            "this.title");
        result[0] = binding;
        binding = new mx.binding.Binding(this,
            function():Object
            {
                return (this.fnList);
            },
            function(_sourceFunctionReturnValue:Object):void
            {
				
                funcCombo.dataProvider = _sourceFunctionReturnValue;
            },
            "funcCombo.dataProvider");
        result[1] = binding;
        binding = new mx.binding.Binding(this,
            function():String
            {
                var result:* = (resourceManager.getString('sharable', 'EX_exprDnD'));
                var stringResult:String = (result == undefined ? null : String(result));
                return stringResult;
            },
            function(_sourceFunctionReturnValue:String):void
            {
				
                _ExprBuilderDialogBox_Label3.text = _sourceFunctionReturnValue;
            },
            "_ExprBuilderDialogBox_Label3.text");
        result[2] = binding;

        return result;
    }

    private function _ExprBuilderDialogBox_bindingExprs():void
    {
        var destination:*;
		[Binding(id='0')]
		destination = (resourceManager.getString('sharable', 'EX_title'));
		[Binding(id='1')]
		destination = (this.fnList);
		[Binding(id='2')]
		destination = (resourceManager.getString('sharable', 'EX_exprDnD'));
    }

    /**
     * @private
     **/
    public static function set watcherSetupUtil(watcherSetupUtil:IWatcherSetupUtil):void
    {
        (ExprBuilderDialogBox)._watcherSetupUtil = watcherSetupUtil;
    }

    private static var _watcherSetupUtil:IWatcherSetupUtil;




    /**
     * @private
     **/
    mx_internal var _bindings : Array = [];
    /**
     * @private
     **/
    mx_internal var _watchers : Array = [];
    /**
     * @private
     **/
    mx_internal var _bindingsByDestination : Object = {};
    /**
     * @private
     **/
    mx_internal var _bindingsBeginWithWord : Object = {};

}

}
