
package com.birst.flex.core.expression
{
import com.birst.flex.core.SaveCancelDialogBox;
import flash.accessibility.*;
import flash.debugger.*;
import flash.display.*;
import flash.errors.*;
import flash.events.*;
import flash.external.*;
import flash.filters.*;
import flash.geom.*;
import flash.media.*;
import flash.net.*;
import flash.printing.*;
import flash.profiler.*;
import flash.system.*;
import flash.text.*;
import flash.ui.*;
import flash.utils.*;
import flash.xml.*;
import mx.binding.*;
import mx.controls.ComboBox;
import mx.controls.List;
import mx.controls.TextArea;
import mx.core.ClassFactory;
import mx.core.DeferredInstanceFromClass;
import mx.core.DeferredInstanceFromFunction;
import mx.core.IDeferredInstance;
import mx.core.IFactory;
import mx.core.IPropertyChangeNotifier;
import mx.core.mx_internal;
import mx.modules.ModuleLoader;
import mx.styles.*;
import mx.containers.HBox;
import mx.containers.HDividedBox;
import mx.containers.VBox;
import mx.controls.Label;
import com.birst.flex.core.SaveCancelDialogBox;


		[ResourceBundle("expressions")]
		[ResourceBundle("expressions_descr")]
		[ResourceBundle("expressions_example")]
	
public class ExprBuilderDialogBox extends com.birst.flex.core.SaveCancelDialogBox
{
	public function ExprBuilderDialogBox() {}

	[Bindable]
	public var subjectAreaModule : mx.modules.ModuleLoader;
	[Bindable]
	public var expr : mx.controls.TextArea;
	[Bindable]
	public var funcCombo : mx.controls.ComboBox;
	[Bindable]
	public var funcList : mx.controls.List;
	[Bindable]
	public var fnDescr : mx.controls.TextArea;

	mx_internal var _bindings : Array;
	mx_internal var _watchers : Array;
	mx_internal var _bindingsByDestination : Object;
	mx_internal var _bindingsBeginWithWord : Object;

include "C:/Users/rmcginty/Projects/birst/FlexAdhoc/src/com/birst/flex/core/expression/ExprBuilderDialogBox.mxml:11,249";

}}
