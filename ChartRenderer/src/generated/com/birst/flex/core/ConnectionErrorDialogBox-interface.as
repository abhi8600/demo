
package com.birst.flex.core
{
import flash.accessibility.*;
import flash.debugger.*;
import flash.display.*;
import flash.errors.*;
import flash.events.*;
import flash.external.*;
import flash.filters.*;
import flash.geom.*;
import flash.media.*;
import flash.net.*;
import flash.printing.*;
import flash.profiler.*;
import flash.system.*;
import flash.text.*;
import flash.ui.*;
import flash.utils.*;
import flash.xml.*;
import mx.binding.*;
import mx.containers.TitleWindow;
import mx.controls.Button;
import mx.controls.Text;
import mx.controls.TextArea;
import mx.core.ClassFactory;
import mx.core.DeferredInstanceFromClass;
import mx.core.DeferredInstanceFromFunction;
import mx.core.IDeferredInstance;
import mx.core.IFactory;
import mx.core.IPropertyChangeNotifier;
import mx.core.mx_internal;
import mx.styles.*;
import mx.containers.TitleWindow;
import mx.containers.HBox;

public class ConnectionErrorDialogBox extends mx.containers.TitleWindow
{
	public function ConnectionErrorDialogBox() {}

	[Bindable]
	public var message : mx.controls.Text;
	[Bindable]
	public var errorMessage : mx.controls.TextArea;
	[Bindable]
	public var closeBttn : mx.controls.Button;

	mx_internal var _bindings : Array;
	mx_internal var _watchers : Array;
	mx_internal var _bindingsByDestination : Object;
	mx_internal var _bindingsBeginWithWord : Object;

include "C:/Users/rmcginty/Projects/birst/FlexAdhoc/src/com/birst/flex/core/ConnectionErrorDialogBox.mxml:4,22";

}}
