






package
{
import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
[Mixin]
public class _com_birst_flex_core_expression_ExprBuilderDialogBoxWatcherSetupUtil extends Sprite
    implements mx.binding.IWatcherSetupUtil
{
    public function _com_birst_flex_core_expression_ExprBuilderDialogBoxWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import com.birst.flex.core.expression.ExprBuilderDialogBox;
        (com.birst.flex.core.expression.ExprBuilderDialogBox).watcherSetupUtil = new _com_birst_flex_core_expression_ExprBuilderDialogBoxWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import mx.core.UIComponentDescriptor;
        import com.birst.flex.core.CoreApp;
        import mx.core.DeferredInstanceFromClass;
        import mx.collections.ArrayCollection;
        import mx.utils.ObjectProxy;
        import __AS3__.vec.Vector;
        import mx.events.CloseEvent;
        import mx.controls.TextArea;
        import flash.events.MouseEvent;
        import mx.binding.IBindingClient;
        import com.birst.flex.core.SaveCancelDialogBox;
        import mx.rpc.events.ResultEvent;
        import mx.core.ClassFactory;
        import mx.containers.HDividedBox;
        import mx.core.IFactory;
        import mx.events.DragEvent;
        import mx.containers.VBox;
        import mx.core.DeferredInstanceFromFunction;
        import mx.controls.List;
        import mx.utils.UIDUtil;
        import mx.core.Application;
        import flash.events.EventDispatcher;
        import com.birst.flex.core.DragDropHelper;
        import com.birst.flex.webservice.WebServiceResult;
        import mx.core.IToolTip;
        import mx.containers.HBox;
        import mx.binding.BindingManager;
        import mx.events.ModuleEvent;
        import mx.managers.DragManager;
        import mx.controls.Label;
        import com.birst.flex.webservice.WebServiceClient;
        import mx.managers.ToolTipManager;
        import mx.core.IDeferredInstance;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import mx.events.PropertyChangeEvent;
        import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
        import mx.core.mx_internal;
        import mx.events.FlexEvent;
        import mx.core.UIComponent;
        import mx.events.ListEvent;
        import mx.events.CollectionEvent;
        import flash.events.Event;
        import mx.modules.ModuleLoader;
        import mx.controls.ComboBox;

        // writeWatcher id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[2] = new mx.binding.PropertyWatcher("fnList",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=2 size=1
        [
        bindings[1]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.PropertyWatcher("resourceManager",
            {
                unused: true
            }
,         // writeWatcherListeners id=0 size=2
        [
        bindings[0],
        bindings[2]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[1] = new mx.binding.FunctionReturnWatcher("getString",
                                                                     target,
                                                                     function():Array { return [ "sharable", "EX_title" ]; },
            {
                change: true
            }
,
                                                                     [bindings[0]],
                                                                     null
);

        // writeWatcher id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[3] = new mx.binding.FunctionReturnWatcher("getString",
                                                                     target,
                                                                     function():Array { return [ "sharable", "EX_exprDnD" ]; },
            {
                change: true
            }
,
                                                                     [bindings[2]],
                                                                     null
);


        // writeWatcherBottom id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[2].updateParent(target);

 





        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].updateParent(target);

 





        // writeWatcherBottom id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 1 0 parentWatcher
        watchers[1].parentWatcher = watchers[0];
        watchers[0].addChild(watchers[1]);

 





        // writeWatcherBottom id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 3 0 parentWatcher
        watchers[3].parentWatcher = watchers[0];
        watchers[0].addChild(watchers[3]);

 





    }
}

}
