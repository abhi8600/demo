

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import mx.collections.ArrayCollection;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property dateFormats (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'dateFormats' moved to '_517623758dateFormats'
	 */

    [Bindable(event="propertyChange")]
    public function get dateFormats():mx.collections.ArrayCollection
    {
        return this._517623758dateFormats;
    }

    public function set dateFormats(value:mx.collections.ArrayCollection):void
    {
    	var oldValue:Object = this._517623758dateFormats;
        if (oldValue !== value)
        {
            this._517623758dateFormats = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateFormats", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property timeFormats (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'timeFormats' moved to '_26344399timeFormats'
	 */

    [Bindable(event="propertyChange")]
    public function get timeFormats():mx.collections.ArrayCollection
    {
        return this._26344399timeFormats;
    }

    public function set timeFormats(value:mx.collections.ArrayCollection):void
    {
    	var oldValue:Object = this._26344399timeFormats;
        if (oldValue !== value)
        {
            this._26344399timeFormats = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "timeFormats", oldValue, value));
        }
    }



}
