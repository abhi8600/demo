






package
{
import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
[Mixin]
public class _com_birst_flex_dashboards_components_PromptDisplayWatcherSetupUtil extends Sprite
    implements mx.binding.IWatcherSetupUtil
{
    public function _com_birst_flex_dashboards_components_PromptDisplayWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import com.birst.flex.dashboards.components.PromptDisplay;
        (com.birst.flex.dashboards.components.PromptDisplay).watcherSetupUtil = new _com_birst_flex_dashboards_components_PromptDisplayWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import mx.core.UIComponentDescriptor;
        import com.birst.flex.core.CoreApp;
        import mx.core.DeferredInstanceFromClass;
        import mx.collections.XMLListCollection;
        import com.birst.flex.core.Util;
        import mx.collections.ArrayCollection;
        import mx.utils.ObjectProxy;
        import __AS3__.vec.Vector;
        import mx.controls.Tree;
        import mx.collections.ArrayList;
        import mx.binding.IBindingClient;
        import flash.events.MouseEvent;
        import com.birst.flex.core.ReportRendererEvent;
        import mx.rpc.events.ResultEvent;
        import mx.core.ClassFactory;
        import mx.core.IFactory;
        import mx.effects.Fade;
        import mx.containers.VBox;
        import mx.controls.Button;
        import mx.core.DeferredInstanceFromFunction;
        import mx.controls.List;
        import mx.utils.UIDUtil;
        import mx.core.Application;
        import flash.events.EventDispatcher;
        import com.birst.flex.webservice.WebServiceResult;
        import mx.controls.Alert;
        import mx.binding.BindingManager;
        import com.birst.flex.dashboards.events.PromptControlEvent;
        import com.birst.flex.core.DateTimePicker;
        import com.birst.flex.webservice.WebServiceClient;
        import mx.core.IDeferredInstance;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import mx.events.PropertyChangeEvent;
        import mx.core.mx_internal;
        import mx.events.FlexEvent;
        import mx.core.UIComponent;
        import com.birst.flex.core.Tracer;
        import flash.events.Event;
        import com.birst.flex.core.DynamicFlowBox;

        // writeWatcher id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[3] = new mx.binding.PropertyWatcher("app",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=3 size=1
        [
        bindings[3]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher shouldWriteChildren=true
        watchers[4] = new mx.binding.FunctionReturnWatcher("getLocString",
                                                                     target,
                                                                     function():Array { return [ "PD_apply" ]; },
            {
                localeChange: true
            }
,
                                                                     [bindings[3]],
                                                                     null
);

        // writeWatcher id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[1] = new mx.binding.StaticPropertyWatcher("flowLayout",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=1 size=1
        [
        bindings[1]
        ]
,
                                                                 null
);

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.PropertyWatcher("width",
            {
                widthChanged: true
            }
,         // writeWatcherListeners id=0 size=1
        [
        bindings[0]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[2] = new mx.binding.StaticPropertyWatcher("flowDirection",
            {
                propertyChange: true
            }
,         // writeWatcherListeners id=2 size=1
        [
        bindings[2]
        ]
,
                                                                 null
);


        // writeWatcherBottom id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[3].updateParent(target);

 





        // writeWatcherBottom id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.FunctionReturnWatcher
        // writeEvaluationWatcherPart 4 3 parentWatcher
        watchers[4].parentWatcher = watchers[3];
        watchers[3].addChild(watchers[4]);

 





        // writeWatcherBottom id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        import com.birst.flex.dashboards.components.PromptDisplay;
        watchers[1].updateParent(com.birst.flex.dashboards.components.PromptDisplay);

 





        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].updateParent(target);

 





        // writeWatcherBottom id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        import com.birst.flex.dashboards.components.PromptDisplay;
        watchers[2].updateParent(com.birst.flex.dashboards.components.PromptDisplay);

 





    }
}

}
