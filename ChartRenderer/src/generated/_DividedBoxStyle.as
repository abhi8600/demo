
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;

[ExcludeClass]

public class _DividedBoxStyle
{
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='mx.skins.BoxDividerSkin', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='635', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_mx_skins_BoxDividerSkin_701755532:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='mx.skins.cursor.HBoxDivider', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='637', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_mx_skins_cursor_HBoxDivider_8108817:Class;
    [Embed(_resolvedSource='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', symbol='mx.skins.cursor.VBoxDivider', source='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$Assets.swf', original='Assets.swf', _line='639', _pathsep='true', _file='C:/Users/rmcginty/Development/flex_sdk_3.5/frameworks/libs/framework.swc$defaults.css')]
    private static var _embed_css_Assets_swf_mx_skins_cursor_VBoxDivider_1107979945:Class;

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration("DividedBox");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration("DividedBox", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.dividerThickness = 3;
                this.dividerColor = 0x6f7777;
                this.dividerAffordance = 6;
                this.verticalDividerCursor = _embed_css_Assets_swf_mx_skins_cursor_VBoxDivider_1107979945;
                this.dividerSkin = _embed_css_Assets_swf_mx_skins_BoxDividerSkin_701755532;
                this.horizontalDividerCursor = _embed_css_Assets_swf_mx_skins_cursor_HBoxDivider_8108817;
                this.dividerAlpha = 0.75;
                this.verticalGap = 10;
                this.horizontalGap = 10;
            };
        }
    }
}

}
