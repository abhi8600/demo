package 
{

import mx.resources.ResourceBundle;

[ExcludeClass]

public class en_US$expressions_descr_properties extends ResourceBundle
{

    public function en_US$expressions_descr_properties()
    {
		 super("en_US", "expressions_descr");
    }

    override protected function getContent():Object
    {
        var content:Object =
        {
            "Let": "Allows re-use of logic in complex expressions by declaring expression-level variables, initializing them and re-using them in the expression.",
            "Positional Calculation": "Allows a measure to be calculated at a level different than the report grain.",
            "Rsum": "Returns the trailing sum of values, based on the window size, at the current report grain for the window provided.",
            "Format": "Formats a number or date to the particular format provided.",
            "Substring": "Returns a portion of a given string and  assumes a zero based index.",
            "Ln": "Returns the natural logarithm of a given number in a specified base.",
            "Like": "Used to search for a specified pattern in a column.",
            "ToUpper": "Returns a character expression after converting data to upper case.",
            "OP_GT_EQ": "Greater than or equal to",
            "PTile": "Returns the percentile rank of each row within the result set.",
            "OP_GT": "Greater than",
            "GetPromptValue": "A place holder for a user defined value taken from a dashboard prompt. An optional second argument (varchar or null) can be used in the event that the first argument is not a currently defined prompt.",
            "DateDiff": "Returns the integer value difference between a first date and second date given a date part. Accepted date parts: Year, Month, Day, Hour, Minute and Second.",
            "Median": "Returns the value in the middle of all measure values at the report grain. An optional break by attribute denoted with \"By\" can be used.",
            "Sparse": "Allows you to show dimension column values for which there is no fact data. Works with one dimension only.",
            "Position": "Returns the position of a second string within the first.",
            "Pow": "Returns x to the power of y.",
            "Abs": "Returns the absolute value of a number.",
            "AND": "Returns true if all filter conditions are met.",
            "ToLower": "Returns a character expression after converting data to lower case.",
            "OR": "Returns true if either filter condition is met.",
            "RowNumber": "Returns the row number for the current result set.",
            "Rank": "Returns the rank of each row within the result set.",
            "Replace": "Allows a static string value as the search criteria to replace a value.",
            "ArcTan": "Returns the arc tangent of a number between -π/2 and π/2.",
            "Tan": "Returns the tangent of a given angle.",
            "OP_NEQ": "Search condition that is successful when the column on the left does not match the string value. ",
            "NumRows": "Returns the total number of rows in the current result set.",
            "Function": "Custom functions execute a script over an entire result set to return a result. It works by declaring a return data type (the type of the function) and providing both a script and a logical query. The script is executed for each row of the result set. The variable [Result] is populated with the return value.",
            "OP_LT": "Less than",
            "LookupValue": "Allows you to look up a value in a row of another result set by the value of another column in the same row. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.",
            "Not Like": "Used to search for the non-existence of a specified pattern in a column.",
            "LookupRow": "Allows you to look up a value in a row of another result set using a specific row index. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.",
            "Sqrt": "Returns the square root of a given positive number.",
            "ReplaceAll": "Allows a regular expression as the search criteria to replace a value.",
            "IFNull": "Allows an expression to return a different result if it is null.",
            "Find": "Similar to LookupValue, except the lookup value does not need an exact match. If the match is exact that row is returned, otherwise, the first row that is greater than the lookup value is used. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.",
            "ToTime": "Converts an integer value into it's time representation since midnight 1/1/1970 (epoch time). The field parameter is a comma separated string listing the fields to include (weeks, days, hours, minutes, seconds, etc.). Format represents the standard Java output string format.",
            "Exp": "Returns e raised to the power of the number given",
            "IsNaN": "Evaluates an expression and returns true if it is not a number and false if it is. The result can be used as a conditional check.",
            "OP_LT_EQ": "Less than or equal to",
            "DateAdd": "Returns a date after adding (or subtracting if the number is negative) a given integer value date part to a date.",
            "Ceiling": "Returns the closest integer greater than or equal to the given number.",
            "Stat": "Allows you to generate aggregated statistics based on another result set. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.",
            "ArcCos": "Returns the arc cosine of a number between ο and π.",
            "DRank": "Dense Rank returns the rank of each row within the result set, without any gaps in the ranking.",
            "IsInf": "Evaluates an expression and returns true if the value is +/- infinity. The result can be used as a conditional check.",
            "Cos": "Returns the cosine of a given angle.",
            "OP_EQ": "Equals",
            "Is Null": "Condition is satisfied if the column value contains a null value, or an expression evaluates to null.",
            "Trim": "Trims leading and trailing spaces from a string.",
            "Length": "Returns the number of characters in a given string.",
            "Log": "Returns the logarithm of a given number in a specified base.",
            "Floor": "Returns the closest integer less than or equal to a given number.",
            "Transform": "Directly manipulate or create from scratch the report-level result set. Only one transform is allowed per logical query. The transform functions just like an ETL services script. It operates row-by-row over the original result set from the query, using WRITERECORD to output new rows as results. The entire result set is re-written as a result of the TRANSFORM statement. All the input and output columns need to be identical.",
            "ArcSin": "Returns the arc sine of a number between -π/2 and π/2.",
            "GetPromptFilter": "A place holder for a dashboard prompt.",
            "DatePart": "Returns an integer value representing a given a date part. Accepted date parts: Year, Month, Day, Hour, Minute and Second.",
            "Sin": "Returns the sine of a given angle.",
            "Is Not Null": "Condition is satisfied if the column value is not null, or an expression does not evaluate to null.",
            "Random": "Returns a double from 0.0 to 1.0. This function ignores parameter values.",
            "IIF": "Returns one result if the specified condition is true and a different one if it is false.",
            "FunctionLookup": "Custom functions execute a script over an entire result set to return a result. It works by declaring a return data type (the type of the function) and providing both a script and a logical query. The script is executed for each row of the result set. Additionally, a lookup parameter can be added and will run the function only on rows that satisfy the filter for the targeted column being equal to the value set. The variable [Result] is populated with the return value."
        };
        return content;
    }
}



}
