package 
{

import mx.resources.ResourceBundle;

[ExcludeClass]

public class en_US$expressions_properties extends ResourceBundle
{

    public function en_US$expressions_properties()
    {
		 super("en_US", "expressions");
    }

    override protected function getContent():Object
    {
        var content:Object =
        {
            "Let": "Let(variable declarations,expression body)",
            "Positional Calculation": "{[logical column]}",
            "Rsum": "Rsum(window size,[logical column])",
            "Format": "Format([logical column],format string)",
            "Substring": "Substring([logical column],start index,end index)",
            "Ln": "Ln([logical column])",
            "Like": "Like",
            "ToUpper": "ToUpper([logical column])",
            "OP_GT_EQ": ">=",
            "PTile": "PTile([logical column])",
            "OP_GT": ">",
            "GetPromptValue": "GetPromptValue(prompt name)",
            "DateDiff": "DateDiff(date part,[logical column],[logical column])",
            "Median": "Median([logical column] By [logical column])",
            "Sparse": "Sparse([logical column])",
            "Position": "Position(search string,[logical column])",
            "Pow": "Pow([logical column],[logical column])",
            "Abs": "Abs([logical column])",
            "AND": "And",
            "ToLower": "ToLower([logical column])",
            "OR": "Or",
            "RowNumber": "RowNumber()",
            "Rank": "Rank([logical column])",
            "Replace": "Replace([logical column],string to replace,replacement string)",
            "ArcTan": "ArcTan([logical column])",
            "Tan": "Tan([logical column])",
            "OP_NEQ": "&lt;>",
            "NumRows": "NumRows()",
            "Function": "Function(return data type,script statement block,logical query)",
            "OP_LT": "&lt;",
            "LookupValue": "LookupValue(index of column to lookup,value to lookup,index of column to return,logical query)",
            "Not Like": "Not Like",
            "LookupRow": "LookupRow(row index,index of column to return,logical query)",
            "Sqrt": "Sqrt([logical column])",
            "ReplaceAll": "ReplaceAll([logical column],regular expression,replacement string)",
            "IFNull": "IFNull([logical column],if NULL)",
            "Find": "Find(index of column to lookup,value to lookup,index of column to return,logical query)",
            "ToTime": "ToTime([logical column],fields,format)",
            "Exp": "Exp([logical column])",
            "IsNaN": "IsNaN([logical column])",
            "OP_LT_EQ": "&lt;=",
            "DateAdd": "DateAdd(date part,increment,[logical column])",
            "Ceiling": "Ceiling([logical column])",
            "Stat": "Stat(aggregation,index of column to aggregate,logical query)",
            "ArcCos": "ArcCos([logical column])",
            "DRank": "DRank([logical column])",
            "IsInf": "IsInf([logical column])",
            "Cos": "Cos([logical column])",
            "OP_EQ": "=",
            "Is Null": "Is Null",
            "Trim": "Trim([logical column])",
            "Length": "Length([logical column])",
            "Log": "Log([logical column])",
            "Floor": "Floor([logical column])",
            "Transform": "Transform(script block)",
            "ArcSin": "ArcSin([logical column])",
            "GetPromptFilter": "GetPromptFilter(prompt name)",
            "DatePart": "DatePart(date part,[logical column])",
            "Sin": "Sin([logical column])",
            "Is Not Null": "Is Not Null",
            "Random": "Random()",
            "IIF": "IIF(condition,if TRUE,if FALSE)",
            "FunctionLookup": "FunctionLookup(return data type,lookup parameter,script statement block,logical query)"
        };
        return content;
    }
}



}
