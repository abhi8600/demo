
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;
import com.hillelcoren.assets.skins.FacebookSkin;

[ExcludeClass]

public class _facebookStyle
{

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration(".facebook");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration(".facebook", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.upSkin = com.hillelcoren.assets.skins.FacebookSkin;
                this.fontWeight = "normal";
                this.selectedDownSkin = com.hillelcoren.assets.skins.FacebookSkin;
                this.downSkin = com.hillelcoren.assets.skins.FacebookSkin;
                this.overSkin = com.hillelcoren.assets.skins.FacebookSkin;
                this.paddingTop = 0;
                this.selectedUpSkin = com.hillelcoren.assets.skins.FacebookSkin;
                this.paddingLeft = 2;
                this.textSelectedColor = 0xffffff;
                this.paddingBottom = 0;
                this.paddingRight = 5;
                this.selectedOverSkin = com.hillelcoren.assets.skins.FacebookSkin;
            };
        }
    }
}

}
