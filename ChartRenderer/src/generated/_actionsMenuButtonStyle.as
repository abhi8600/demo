
package 
{

import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.core.mx_internal;
import mx.styles.CSSStyleDeclaration;
import mx.styles.StyleManager;
import com.hillelcoren.assets.skins.BlankSkin;

[ExcludeClass]

public class _actionsMenuButtonStyle
{

    public static function init(fbs:IFlexModuleFactory):void
    {
        var style:CSSStyleDeclaration = StyleManager.getStyleDeclaration(".actionsMenuButton");
    
        if (!style)
        {
            style = new CSSStyleDeclaration();
            StyleManager.setStyleDeclaration(".actionsMenuButton", style, false);
        }
    
        if (style.defaultFactory == null)
        {
            style.defaultFactory = function():void
            {
                this.upSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.selectedDownSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.downSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.overSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.paddingTop = 0;
                this.selectedUpSkin = com.hillelcoren.assets.skins.BlankSkin;
                this.paddingLeft = 0;
                this.paddingBottom = 0;
                this.paddingRight = 4;
                this.selectedOverSkin = com.hillelcoren.assets.skins.BlankSkin;
            };
        }
    }
}

}
