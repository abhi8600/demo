

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import mx.controls.Button;
import mx.controls.TextInput;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import mx.styles.*;
import flash.system.*;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property filePath (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'filePath' moved to '_735662143filePath'
	 */

    [Bindable(event="propertyChange")]
    public function get filePath():mx.controls.TextInput
    {
        return this._735662143filePath;
    }

    public function set filePath(value:mx.controls.TextInput):void
    {
    	var oldValue:Object = this._735662143filePath;
        if (oldValue !== value)
        {
            this._735662143filePath = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "filePath", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property uploadBttn (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'uploadBttn' moved to '_243603475uploadBttn'
	 */

    [Bindable(event="propertyChange")]
    public function get uploadBttn():mx.controls.Button
    {
        return this._243603475uploadBttn;
    }

    public function set uploadBttn(value:mx.controls.Button):void
    {
    	var oldValue:Object = this._243603475uploadBttn;
        if (oldValue !== value)
        {
            this._243603475uploadBttn = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "uploadBttn", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property resourceFile (static private)
	 * - generated setter
	 * - generated getter
	 * - original static private var 'resourceFile' moved to '_384797014resourceFile'
	 */

    [Bindable(event="propertyChange")]
    static private function get resourceFile():String
    {
        return FileUploader._384797014resourceFile;
    }

    static private function set resourceFile(value:String):void
    {
    	var oldValue:Object = FileUploader._384797014resourceFile;
        if (oldValue !== value)
        {
            FileUploader._384797014resourceFile = value;
            var eventDispatcher:IEventDispatcher = FileUploader.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FileUploader, "resourceFile", oldValue, value));
            }
        }
    }



    private static var _staticBindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher();

    public static function get staticEventDispatcher():IEventDispatcher
    {
        return _staticBindingEventDispatcher;
    }
}
