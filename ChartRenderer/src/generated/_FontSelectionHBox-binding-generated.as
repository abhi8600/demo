

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import com.birst.flex.core.*;
import flash.filters.*;
import flash.profiler.*;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import mx.controls.CheckBox;
import flash.accessibility.*;
import mx.binding.*;
import String;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import mx.styles.*;
import flash.system.*;
import mx.controls.ComboBox;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property fontFamily (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontFamily' moved to '_1224696685fontFamily'
	 */

    [Bindable(event="propertyChange")]
    public function get fontFamily():mx.controls.ComboBox
    {
        return this._1224696685fontFamily;
    }

    public function set fontFamily(value:mx.controls.ComboBox):void
    {
    	var oldValue:Object = this._1224696685fontFamily;
        if (oldValue !== value)
        {
            this._1224696685fontFamily = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontFamily", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fontFamilyChanged (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontFamilyChanged' moved to '_706174303fontFamilyChanged'
	 */

    [Bindable(event="propertyChange")]
    public function get fontFamilyChanged():mx.controls.CheckBox
    {
        return this._706174303fontFamilyChanged;
    }

    public function set fontFamilyChanged(value:mx.controls.CheckBox):void
    {
    	var oldValue:Object = this._706174303fontFamilyChanged;
        if (oldValue !== value)
        {
            this._706174303fontFamilyChanged = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontFamilyChanged", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fontSize (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontSize' moved to '_365601008fontSize'
	 */

    [Bindable(event="propertyChange")]
    public function get fontSize():mx.controls.ComboBox
    {
        return this._365601008fontSize;
    }

    public function set fontSize(value:mx.controls.ComboBox):void
    {
    	var oldValue:Object = this._365601008fontSize;
        if (oldValue !== value)
        {
            this._365601008fontSize = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontSize", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fontSizeChanged (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontSizeChanged' moved to '_1633643428fontSizeChanged'
	 */

    [Bindable(event="propertyChange")]
    public function get fontSizeChanged():mx.controls.CheckBox
    {
        return this._1633643428fontSizeChanged;
    }

    public function set fontSizeChanged(value:mx.controls.CheckBox):void
    {
    	var oldValue:Object = this._1633643428fontSizeChanged;
        if (oldValue !== value)
        {
            this._1633643428fontSizeChanged = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontSizeChanged", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fontWeight (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontWeight' moved to '_734428249fontWeight'
	 */

    [Bindable(event="propertyChange")]
    public function get fontWeight():mx.controls.ComboBox
    {
        return this._734428249fontWeight;
    }

    public function set fontWeight(value:mx.controls.ComboBox):void
    {
    	var oldValue:Object = this._734428249fontWeight;
        if (oldValue !== value)
        {
            this._734428249fontWeight = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontWeight", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property fontWeightChanged (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'fontWeightChanged' moved to '_918904077fontWeightChanged'
	 */

    [Bindable(event="propertyChange")]
    public function get fontWeightChanged():mx.controls.CheckBox
    {
        return this._918904077fontWeightChanged;
    }

    public function set fontWeightChanged(value:mx.controls.CheckBox):void
    {
    	var oldValue:Object = this._918904077fontWeightChanged;
        if (oldValue !== value)
        {
            this._918904077fontWeightChanged = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fontWeightChanged", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property mKey (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'mKey' moved to '_3322546mKey'
	 */

    [Bindable(event="propertyChange")]
    private function get mKey():String
    {
        return this._3322546mKey;
    }

    private function set mKey(value:String):void
    {
    	var oldValue:Object = this._3322546mKey;
        if (oldValue !== value)
        {
            this._3322546mKey = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mKey", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property BOLD_ITALIC_CLIENT (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'BOLD_ITALIC_CLIENT' moved to '_1510336384BOLD_ITALIC_CLIENT'
	 */

    [Bindable(event="propertyChange")]
    static public function get BOLD_ITALIC_CLIENT():String
    {
        return FontSelectionHBox._1510336384BOLD_ITALIC_CLIENT;
    }

    static public function set BOLD_ITALIC_CLIENT(value:String):void
    {
    	var oldValue:Object = FontSelectionHBox._1510336384BOLD_ITALIC_CLIENT;
        if (oldValue !== value)
        {
            FontSelectionHBox._1510336384BOLD_ITALIC_CLIENT = value;
            var eventDispatcher:IEventDispatcher = FontSelectionHBox.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FontSelectionHBox, "BOLD_ITALIC_CLIENT", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property _cachedFamilies (static private)
	 * - generated setter
	 * - generated getter
	 * - original static private var '_cachedFamilies' moved to '_99660899_cachedFamilies'
	 */

    [Bindable(event="propertyChange")]
    static private function get _cachedFamilies():Array
    {
        return FontSelectionHBox._99660899_cachedFamilies;
    }

    static private function set _cachedFamilies(value:Array):void
    {
    	var oldValue:Object = FontSelectionHBox._99660899_cachedFamilies;
        if (oldValue !== value)
        {
            FontSelectionHBox._99660899_cachedFamilies = value;
            var eventDispatcher:IEventDispatcher = FontSelectionHBox.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(FontSelectionHBox, "_cachedFamilies", oldValue, value));
            }
        }
    }



    private static var _staticBindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher();

    public static function get staticEventDispatcher():IEventDispatcher
    {
        return _staticBindingEventDispatcher;
    }
}
