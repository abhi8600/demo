






package
{
import flash.display.Sprite;
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
[Mixin]
public class _com_birst_flex_dashboards_components_PromptControlWatcherSetupUtil extends Sprite
    implements mx.binding.IWatcherSetupUtil
{
    public function _com_birst_flex_dashboards_components_PromptControlWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import com.birst.flex.dashboards.components.PromptControl;
        (com.birst.flex.dashboards.components.PromptControl).watcherSetupUtil = new _com_birst_flex_dashboards_components_PromptControlWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import com.birst.flex.core.VerticalLabel;
        import mx.core.UIComponentDescriptor;
        import com.birst.flex.core.CoreApp;
        import mx.core.DeferredInstanceFromClass;
        import com.birst.flex.core.Util;
        import mx.collections.ArrayCollection;
        import mx.utils.ObjectProxy;
        import __AS3__.vec.Vector;
        import mx.controls.Tree;
        import mx.binding.IBindingClient;
        import com.birst.flex.core.FlexField;
        import mx.events.ToolTipEvent;
        import mx.core.ClassFactory;
        import mx.core.IFactory;
        import mx.controls.CheckBox;
        import mx.core.DeferredInstanceFromFunction;
        import mx.controls.List;
        import mx.controls.DateField;
        import mx.utils.UIDUtil;
        import mx.events.SliderEvent;
        import mx.core.Application;
        import flash.events.EventDispatcher;
        import mx.controls.Alert;
        import mx.events.IndexChangedEvent;
        import mx.containers.HBox;
        import mx.binding.BindingManager;
        import com.birst.flex.dashboards.events.PromptControlEvent;
        import com.birst.flex.core.DateTimePicker;
        import mx.controls.Label;
        import mx.managers.ToolTipManager;
        import mx.controls.RadioButton;
        import mx.core.IDeferredInstance;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import mx.containers.Canvas;
        import mx.events.PropertyChangeEvent;
        import mx.core.mx_internal;
        import mx.controls.TextInput;
        import mx.events.FlexEvent;
        import mx.controls.sliderClasses.Slider;
        import mx.containers.Box;
        import mx.events.DropdownEvent;
        import mx.core.UIComponent;
        import com.birst.flex.core.Tracer;
        import flash.events.Event;
        import mx.controls.HScrollBar;
        import mx.controls.HSlider;
        import mx.controls.ComboBox;

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.StaticPropertyWatcher("application",
            null
,         // writeWatcherListeners id=0 size=1
        [
        bindings[0]
        ]
,
                                                                 null
);

        // writeWatcher id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[1] = new mx.binding.PropertyWatcher("promptControlDirection",
            null
,         // writeWatcherListeners id=1 size=1
        [
        bindings[0]
        ]
,
                                                                 null
);


        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        import mx.core.Application;
        watchers[0].updateParent(mx.core.Application);

 





        // writeWatcherBottom id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].addChild(watchers[1]);

 





    }
}

}
