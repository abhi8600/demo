

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import flash.filters.*;
import flash.profiler.*;
import com.birst.flex.core.CoreApp;
import flash.external.*;
import flash.display.*;
import flash.net.*;
import flash.debugger.*;
import flash.utils.*;
import flash.printing.*;
import flash.text.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import mx.binding.*;
import flash.ui.*;
import flash.media.*;
import flash.xml.*;
import com.birst.flex.dashboards.components.*;
import mx.styles.*;
import flash.system.*;
import com.birst.flex.core.DynamicFlowBox;
import flash.errors.*;

class BindableProperty
{
	/**
	 * generated bindable wrapper for property flowbox (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'flowbox' moved to '_765803043flowbox'
	 */

    [Bindable(event="propertyChange")]
    public function get flowbox():com.birst.flex.core.DynamicFlowBox
    {
        return this._765803043flowbox;
    }

    public function set flowbox(value:com.birst.flex.core.DynamicFlowBox):void
    {
    	var oldValue:Object = this._765803043flowbox;
        if (oldValue !== value)
        {
            this._765803043flowbox = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flowbox", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property additionalFilters (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'additionalFilters' moved to '_1580145108additionalFilters'
	 */

    [Bindable(event="propertyChange")]
    public function get additionalFilters():Array
    {
        return this._1580145108additionalFilters;
    }

    public function set additionalFilters(value:Array):void
    {
    	var oldValue:Object = this._1580145108additionalFilters;
        if (oldValue !== value)
        {
            this._1580145108additionalFilters = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "additionalFilters", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property app (private)
	 * - generated setter
	 * - generated getter
	 * - original private var 'app' moved to '_96801app'
	 */

    [Bindable(event="propertyChange")]
    private function get app():CoreApp
    {
        return this._96801app;
    }

    private function set app(value:CoreApp):void
    {
    	var oldValue:Object = this._96801app;
        if (oldValue !== value)
        {
            this._96801app = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "app", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property hasGlobalPrompt (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'hasGlobalPrompt' moved to '_407147169hasGlobalPrompt'
	 */

    [Bindable(event="propertyChange")]
    public function get hasGlobalPrompt():Boolean
    {
        return this._407147169hasGlobalPrompt;
    }

    public function set hasGlobalPrompt(value:Boolean):void
    {
    	var oldValue:Object = this._407147169hasGlobalPrompt;
        if (oldValue !== value)
        {
            this._407147169hasGlobalPrompt = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "hasGlobalPrompt", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property ignoreVariablesOnXml (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'ignoreVariablesOnXml' moved to '_1551949581ignoreVariablesOnXml'
	 */

    [Bindable(event="propertyChange")]
    public function get ignoreVariablesOnXml():Boolean
    {
        return this._1551949581ignoreVariablesOnXml;
    }

    public function set ignoreVariablesOnXml(value:Boolean):void
    {
    	var oldValue:Object = this._1551949581ignoreVariablesOnXml;
        if (oldValue !== value)
        {
            this._1551949581ignoreVariablesOnXml = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ignoreVariablesOnXml", oldValue, value));
        }
    }

	/**
	 * generated bindable wrapper for property flowLayout (static private)
	 * - generated setter
	 * - generated getter
	 * - original static private var 'flowLayout' moved to '_185191928flowLayout'
	 */

    [Bindable(event="propertyChange")]
    static private function get flowLayout():Boolean
    {
        return PromptDisplay._185191928flowLayout;
    }

    static private function set flowLayout(value:Boolean):void
    {
    	var oldValue:Object = PromptDisplay._185191928flowLayout;
        if (oldValue !== value)
        {
            PromptDisplay._185191928flowLayout = value;
            var eventDispatcher:IEventDispatcher = PromptDisplay.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(PromptDisplay, "flowLayout", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property flowDirection (static private)
	 * - generated setter
	 * - generated getter
	 * - original static private var 'flowDirection' moved to '_1568302607flowDirection'
	 */

    [Bindable(event="propertyChange")]
    static private function get flowDirection():String
    {
        return PromptDisplay._1568302607flowDirection;
    }

    static private function set flowDirection(value:String):void
    {
    	var oldValue:Object = PromptDisplay._1568302607flowDirection;
        if (oldValue !== value)
        {
            PromptDisplay._1568302607flowDirection = value;
            var eventDispatcher:IEventDispatcher = PromptDisplay.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(PromptDisplay, "flowDirection", oldValue, value));
            }
        }
    }

	/**
	 * generated bindable wrapper for property promptCreatedMinimized (static public)
	 * - generated setter
	 * - generated getter
	 * - original static public var 'promptCreatedMinimized' moved to '_590296830promptCreatedMinimized'
	 */

    [Bindable(event="propertyChange")]
    static public function get promptCreatedMinimized():Boolean
    {
        return PromptDisplay._590296830promptCreatedMinimized;
    }

    static public function set promptCreatedMinimized(value:Boolean):void
    {
    	var oldValue:Object = PromptDisplay._590296830promptCreatedMinimized;
        if (oldValue !== value)
        {
            PromptDisplay._590296830promptCreatedMinimized = value;
            var eventDispatcher:IEventDispatcher = PromptDisplay.staticEventDispatcher;
            if (eventDispatcher != null)
            {
                eventDispatcher.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(PromptDisplay, "promptCreatedMinimized", oldValue, value));
            }
        }
    }



    private static var _staticBindingEventDispatcher:flash.events.EventDispatcher =
        new flash.events.EventDispatcher();

    public static function get staticEventDispatcher():IEventDispatcher
    {
        return _staticBindingEventDispatcher;
    }
}
