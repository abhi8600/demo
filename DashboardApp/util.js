/*jshint smarttabs:true, bitwise:false, sub:true, strict:false*/

Ext.ns('Birst.Utils');

Ext.define('Birst.Utils.User', {
	singleton: true,

	userName: null,
	userMode: null,
	lastLoginDate: null,
	selectedSpace: null,
	enableCopy: null,
	showFreeTrialPage: null,
	canCreateNewSpace: null,
	enableUserManagement: null,
	showTemplate: null,
	sizeOfSpaceOwned: null,
	externalDBEnabled: null,
	optOutVisualizerBanner : null

});

Ext.define( 'Birst.Utils.Dt', {
	singleton : true,
	getDateFromDateTime : function( dateTimeString ) {
		//String example: 2013-11-27 17:41:26Z
		//new Date(year, month, day, hours, minutes, seconds, milliseconds)
		var t = dateTimeString.split(/[- :]/);
		var date = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5].replace(/[A-Z]/,'')); //Remove timezone
		return date;
	},
	getFormattedDate : function( date ) {
		return date.toLocaleString();
	}
} );

Ext.define('Birst.Utils.Page', {
	singleton: true,

	parseUrl: function(url) {
		"use strict";
		if (!url) {
			url = document.URL;
			}

		var regex = /^((http[s]?|ftp):\/)?\/?([^:\/\s]+)(:([^\/]*))?((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(\?([^#]*))?(#(.*))?$/i;

		return regex.exec(url);
	},

	getPageName: function(url) {
		"use strict";
		return Birst.Utils.Page.parseUrl(url)[8];
	}


});

Ext.define('Birst.Utils.Validation', {
	singleton: true,

	textfieldValidationRegEx255L: /^[^\\.\/<>:|*?\"]{1,255}$/
	/*"*/

});

/**
 * Simplified plugin that allows to add items to the Ext Panel's title header
 *
 */
Ext.define('Birst.Util.Plugin.HeaderItems', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.biheaderx',

	panelHeaderItems: [],
	onItemAdd :function () {
	"use strict";
		this.header = this.panel.getHeader();
		this.header.add(this.panelHeaderItems);
	},

	init: function(p) {
		this.panel = p;
		this.callParent();
		p.on('render', this.onItemAdd, this);
	}

});


/**
 *  Reusable confirmation modal dialog.
 *  User confirmation logic (handler) can be executed in any scope.
 */
Ext.define('Birst.Util,ConfirmDialog', {
	extend: 'Ext.window.Window',
	dfltTitle: 'Confirm',
	dfltMessage: 'Please click OK to confirm. Click Cancel to abort.',
	maxWidth: 680,
	bodyPadding: Ext.isIE ? '15 15 22 15' : 15,
	closable:false,
	draggable: false,
	resizable: false,
	cls:'xb-modal-window',
	bodyCls: 'xb-panel-body-default',
	modal:true,
	listeners : {
		afterrender : function(w) {
			var header = w.header;
			header.setHeight(36);
		}
	},
	config: {
	   message: null,
	   title: null,
	   submitButtonText: null,
	   callee: null,
	   handler: null
	},

	constructor: function(config){
	   this.title = config.title ;
	   this.message = config.msg;
	   this.callee = config.scope;
	   this.handler = config.fn;
	   this.submitButtonText = config.btnText;
	   this.callParent(arguments);
	},

	initComponent: function(args) {
		this.title = this.title || this.dfltTitle;
		this.callee =  this.callee || this;
		this.handler =  this.handler || function (ev, te, p){
			this.destroy();
		};

		this.tools = this.tools || [{
			xtype: 'button',
			iconCls: 'closeModalIcon',
			cls: 'xb-modwin-close-btn',
			overCls: 'xb-modwin-close-btn-over',
			tooltip: Birst.LM.Locale.get('LB_CANCEL')
		}];

		// tools close button handler
		this.tools[0].handler = function(ev, te, p){
				this.ownerCt.ownerCt.destroy();
		};

		this.items = this.items ||  [{
			xtype: 'label',
			cls: 'mw-l',
			name: 'BlfName',
			html: this.message || this.dfltMessage
		}];

		this.dockedItems = this.dockedItems || [{
			xtype: 'toolbar',
			ui: 'footer',
			dock: 'bottom',
			height: 60,
			layout: {
				pack: 'center'
			},
			items: ['->',
				{
				   minWidth: 80,
				   cls: 'xb-modwin-gray-btn',
				   overCls: 'xb-gray-btn-over',
				   pressedCls: 'xb-footer-btn-press'
				},
				{
					minWidth: 80,
					text: this.getSubmitButtonText(),
					cls: 'xb-modwin-submit-btn',
					overCls: 'xb-submit-btn-over',
					pressedCls: 'xb-footer-btn-press'
			 }]
		}];

		// button handlers
		if (this.dockedItems[0].items.length > 2 ) {
			this.dockedItems[0].items[1].handler = function () {
						   this.close();
			};
			this.dockedItems[0].items[1].scope = this;
			this.dockedItems[0].items[1].text = Birst.LM.Locale.get('LB_CANCEL_CP');
			this.dockedItems[0].items[1].handler = function () {
						   this.close();
			};
			this.dockedItems[0].items[2].handler = function () {
						   // call a user passed handler or our own handler
						   this.getHandler().call(this.getCallee(), this);
						   this.close();
			};
			this.dockedItems[0].items[2].scope = this;
			this.dockedItems[0].items[2].text = this.getSubmitButtonText();
		} else {

			 this.dockedItems[0].items[1].handler = function () {
						   this.close();
			};
			this.dockedItems[0].items[1].scope = this;
			this.dockedItems[0].items[1].text = this.getSubmitButtonText();
			this.dockedItems[0].items[1].handler = function () {
						   this.close();
			};

		}

		this.callParent(arguments);
	}
});

/**
 * Standard Error Message Modal
 */
Ext.define('Birst.Util,ErrorDialog', {
	extend: 'Birst.Util,ConfirmDialog',
	cls:'xb-errmsg-modal-window',
	dfltTitle: 'An Error Occurred',
	initComponent: function() {

	   // Only need to specify styles, the superclass will take care on the functionality side of things
	   this.tools = [{
			xtype: 'button',
			iconCls: 'closeModalIcon',
			cls: 'xb-errmsg-modwin-close-btn',
			overCls: 'xb-errmsg-modwin-close-btn-over',
			tooltip: 'Close'
		}];

	   this.dockedItems = this.dockedItems || [{
			xtype: 'toolbar',
			ui: 'footer',
			dock: 'bottom',
			height: 60,
			layout: {
				pack: 'center'
			},
			items: ['->',{
				minWidth: 60,
				cls: 'xb-errmsg-modwin-confirm-btn',
				overCls: 'xb-errmsg-modwin-confirm-btn-over',
				pressedCls: 'xb-footer-btn-press'
			}]
		}];

	   this.callParent(arguments);
	}

 });

Ext.define('Birst.Utils.Safe', {
	singleton: true,

	getFilteredValue: function(val) {
		"use strict";
		return Ext.util.Format.stripTags(val);
	},

	getEscapedValue: function(val) {
		"use strict";
		return Ext.htmlEncode(val);
	},

	getDecodedValue: function(val) {
		"use strict";
		return Ext.String.htmlDecode(val);
	}
});

Ext.define('Birst.Utils.HiddenValues', {
	singleton: true,

	getValue: function(key) {
		var el = document.getElementById(key);
		if (el) {
			if(el.value && el.value != ""){
				return el.value;
			}
		}else{
			return null;
		}
		return null;
	}
});

/**
 * Generic tab bar control with support for custom horizontal scrolls.
 */

Ext.define('Birst.tab.Bar', {
	extend: 'Ext.panel.Header',
	isTabBar: true,
	defaultType: 'tab',
	plain: false,
	renderTpl: [
		'<div id="{id}-body" class="{baseCls}-body {bodyCls}<tpl if="ui"> {baseCls}-body-{ui}<tpl for="uiCls"> {parent.baseCls}-body-{parent.ui}-{.}</tpl></tpl>"<tpl if="bodyStyle"> style="{bodyStyle}"</tpl>>',
			'{%this.renderContainer(out,values)%}',
		'</div>',
		'<div id="{id}-strip" class="{baseCls}-strip<tpl if="ui"> {baseCls}-strip-{ui}<tpl for="uiCls"> {parent.baseCls}-strip-{parent.ui}-{.}</tpl></tpl>"></div>'
	],
	baseCls: Ext.baseCSSPrefix + 'tab-bar',
		childEls: [
		'body', 'strip'
	],

	initComponent: function() {
		var me = this;
		if (me.plain) {
			me.setUI(me.ui + '-plain');
		}
		me.addClsWithUI(me.dock);

		me.callParent(arguments);

		Ext.merge(me.layout, me.initialConfig.layout);
		me.layout.align = (me.orientation == 'vertical') ? 'left' : 'top';
		var overflowHdlr = new Ext.layout.container.boxOverflow.Scroller(me.layout);
		overflowHdlr.scrollIncrement = 100;
		me.layout.overflowHandler = overflowHdlr;
		me.remove(me.titleCmp);
		delete me.titleCmp;

		Ext.apply(me.renderData, {
			bodyCls: me.bodyCls
		});
	},

	getLayout: function() {
		var me = this;
		me.layout.type = (me.dock === 'top' || me.dock === 'bottom') ? 'hbox' : 'vbox';
		return me.callParent(arguments);
	},

	onAdd: function(tab) {
		tab.position = this.dock;
		this.callParent(arguments);
	},

	onRemove: function(tab) {
		var me = this;

		if (tab === me.previousTab) {
			me.previousTab = null;
		}
		me.callParent(arguments);
	},

	afterComponentLayout : function(width) {
		var me = this,
			needsScroll = me.needsScroll;

		me.callParent(arguments);
		me.strip.setWidth(width);

		if (needsScroll) {
			me.layout.overflowHandler.scrollToItem(me.activeTab);
		}
		delete me.needsScroll;
	}

});

/**
 * Button tab container built on the concept of Ext.tab.Panel but doesn't requires the specialized card layout
 * capitalizing on the generic Hbox layout instead.
 */
Ext.define('Birst.btntab.Panel', {
	extend: 'Ext.panel.Panel',

	itemCls: Ext.baseCSSPrefix + 'tabpanel-child',

	removePanelHeader: true,

	deferredRender : true,

	initComponent: function() {
		var me = this;
		var dockedItems = [].concat(me.dockedItems || []);

		me.layout = new Ext.layout.container.HBox(Ext.apply({
						owner: me,
						deferredRender: me.deferredRender,
						itemCls: me.itemCls
						},
		  me.layout));



		me.bttnBar = new Birst.tab.Bar(Ext.apply({
						dock: 'top',
						plain: 'true',
						border: 'true',
						tabPanel: me
						},
		   me.bttnBar));

		dockedItems.push(me.bttnBar);
		me.dockedItems = dockedItems;
		me.callParent(arguments);
	},

	onAdd: function(btn, index) {
		var me = this;
		me.bttnBar.insert(btn);
}
});

/**
 *  Conviniently set or unset loading mask for a specified component
 */
var SetLoadingMask = function (cmp) {

	Ext.getCmp(cmp).setLoading(
				{
					msg:'',
					msgCls: "bx-spinner-small"
				});
};

var UnSetLoadingMask = function (cmp) {

	Ext.getCmp(cmp).setLoading(false);
};

Birst.Utils.EventMsg = function(){
	var eventmsgCt;
	function createBox(t,s){
		return '<div class="evntmsg"><h3>' + t + '</h3><p>' + s + '</p></div>';
	}

	return {
		show: function(title, format){
			if(!eventmsgCt){
				eventmsgCt = Ext.DomHelper.insertFirst(document.body, {id:'evntmsg-div'}, true);
			}
			//var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
			var mbox = Ext.DomHelper.append(eventmsgCt, createBox(title, ''), true);
			mbox.hide();
			mbox.slideIn('t').ghost("t", { delay: 2800, remove: true});
		}
	};

}();

Birst.Utils.Cookie = {
	savePromptViewStyleCookie: function(isNP) {
		var val = "promptView:";
		val += isNP ? "NP" : "Classic";
				
		//5 year ahead
		var fiveYears = new Date((new Date()).getTime() + 5 * 365 * 24 * 60 * 60 * 1000);		
		Ext.util.Cookies.set('BirstDashboards', val, fiveYears);
	},
	
	checkPromptViewStyleByCookie: function(){
		var cookie = Ext.util.Cookies.get('BirstDashboards');
		if(cookie){
			Birst.dashboards.doNP = cookie.indexOf('promptView:NP') >= 0;
		}else{
			Birst.dashboards.doNP = false;
		}
	}
};

Birst.Utils.QueryString = {
	params: null,
	
	getParamValue: function(key){
		if(!this.params){
			this.params = Ext.Object.fromQueryString(window.location.search);
		}
		return this.params[key];
	},
	
	isParamTrue: function(key){
		var val = this.getParamValue(key);
		return val === "true";
	}
};


var detectPepperFlash = function () {

	/*
	 * known plugin file types (win,mac,linux)
	 */
	var PPRLIB_REGEX = /^(lib)?pep(per)?flashplayer/i;
	/*
	 * PPAPI VER (todo)
	 */
	var PPRLIB_VER;

	if (navigator.plugins) {
		for (var i = 0, count = navigator.plugins.length; i < count; i++) {
			var plugin = navigator.plugins[i].filename;
			var has_pepper = PPRLIB_REGEX.test(plugin);
			if (has_pepper) {
				return true;
			}
		}
	 }
	 return false;
};
