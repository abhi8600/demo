var BirstOpenLayerPlugin = function(){
	this.map = null;
	this.markers = null;
	
	this.countryMarkerMap = {
		'Argentina': [-37.090698, -63.584808],
		'Austria': [47.696510, 13.345770],
		'Belgium': [50.501011, 4.476840],
		'Brazil': [-14.242920, -54.387829],
		'Canada': [56.954681, -98.308968],
		'Denmark': [56.276089, 9.516950],
		'Finland': [64.950142, 26.067390],
		'France': [46.710670, 1.718190],
		'Germany': [51.164181, 10.454150],
		'Ireland': [53.419609, -8.240550],
		'Italy': [42.503819, 12.573470],
		'Mexico': [23.625740, -101.956253],
		'Norway': [64.556534, 12.666170],
		'Poland': [51.918919, 19.134300],
		'Spain': [39.894890, -2.988310],
		'Sweden': [62.198448, 17.551420],
		'Switzerland' : [46.813202, 8.223950],
		'UK' : [54.314072, -2.230010],
		'USA' : [37.167931, -95.845016],
		'Venezuela': [6.472790, -66.589043]
	};
};

BirstOpenLayerPlugin.prototype.addMarkers = function(prompts){
	var numMarkers = 0;
	
	if(this.markers){
		this.markers.clearMarkers();
	}
	
	this.markers = this.markers || new OpenLayers.Layer.Markers( "Markers" );

	for ( var key in prompts){
		var vals = prompts[key];
		for (var i = 0; vals && i < vals.length; i++){
			var latLon = this.countryMarkerMap[vals[i]];
			if(latLon){
				var size = new OpenLayers.Size(21,25);
				var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
				var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);
				var m = new OpenLayers.LonLat(latLon[1],latLon[0]);
				this.markers.addMarker(new OpenLayers.Marker(m));
				numMarkers++;
			}
		}
	}
	if(numMarkers > 0){
		this.map.addLayer(this.markers);
		var newBound = this.markers.getDataExtent();
		this.map.zoomToExtent(newBound);
	}
	return numMarkers;
};

BirstOpenLayerPlugin.prototype.renderMap = function(el, prompts){
	this.map = new OpenLayers.Map(el);
	var wms = new OpenLayers.Layer.WMS(
				  "OpenLayers WMS",
				  "http://vmap0.tiles.osgeo.org/wms/vmap0",
				  {'layers':'basic'} );
	this.map.addLayer(wms);
	
	if(this.addMarkers(prompts) == 0){
		this.map.zoomToMaxExtent();
	}
};

BirstOpenLayerPlugin.prototype.renderOnInitialized = function(el, prompts){
	var that = this;
	this.loadScript('http://openlayers.org/api/OpenLayers.js', function(){
		that.renderMap(el, prompts);
	});
};

BirstOpenLayerPlugin.prototype.loadScript = function loadScript(url, callback){
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
};

BirstOpenLayerPlugin.prototype.render = function(el, prompts){
	if(!this.map){
		this.renderMap(el, prompts);
	}else{
		this.addMarkers(prompts);
	}
};