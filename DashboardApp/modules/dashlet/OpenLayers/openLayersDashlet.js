define(['openLayers'], function(){
	var BirstOpenLayerPlugin = function(data, dashletMgr){
			this.map = null;
			this.markers = null;
			this.dashletMgr = dashletMgr;
			this.countryMarkerMap = JSON.parse(data);
		};
		
		BirstOpenLayerPlugin.prototype.addMarkers = function(prompts){
			var numMarkers = 0;
			
			if(this.markers){
				this.markers.clearMarkers();
			}
			
			this.markers = this.markers || new OpenLayers.Layer.Markers( "Markers" );
			
			var that = this;
			
			for ( var key in prompts){
				var vals = prompts[key].selectedValues;
				for (var i = 0; vals && i < vals.length; i++){
					var latLon = this.countryMarkerMap[vals[i]];
					if(latLon){
						var size = new OpenLayers.Size(21,25);
						var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
						var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);
						var m = new OpenLayers.LonLat(latLon[1],latLon[0]);
						var marker = new OpenLayers.Marker(m);
						
						marker.events.register('mousedown', marker, function(evt) {
						 	that.dashletMgr.drillToDashboardPage('Bugs', '14766', '');
						});
						/*
						marker.events.register('mousedown', marker, function(evt) {
						 	that.dashletMgr.setFilters([
						 		{ key: 'Products.CategoryName', value: ['Beverages'] },
						 		{ key: 'Time.Year', value: ['1996'] }
						 	]);
						});*/
						this.markers.addMarker(marker);
						numMarkers++;
					}
				}
			}
			if(numMarkers > 0){
				this.map.addLayer(this.markers);
				var newBound = this.markers.getDataExtent();
				this.map.zoomToExtent(newBound);
			}
			return numMarkers;
		};
		
		BirstOpenLayerPlugin.prototype.renderMap = function(el, prompts){
			this.map = new OpenLayers.Map(el);
			var wms = new OpenLayers.Layer.WMS(
						  "OpenLayers WMS",
						  "http://vmap0.tiles.osgeo.org/wms/vmap0",
						  {'layers':'basic'} );
			this.map.addLayer(wms);
			
			if(this.addMarkers(prompts) == 0){
				this.map.zoomToMaxExtent();
			}
		};
		
		BirstOpenLayerPlugin.prototype.render = function(el, prompts){
			if(!this.map){
				this.renderMap(el, prompts);
			}else{
				this.addMarkers(prompts);
			}
		};
		
		return {clz : BirstOpenLayerPlugin};
});