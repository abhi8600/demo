/* globals define */

define(['visualizer-app', 'visualizer-templates' ], function( v ) {
	var VisualizerIframePlugin = function(vizFile, dashletManager) { /* instead of opening a report */
		this.dashletManager = dashletManager;
		this.vizJson = JSON.parse(vizFile);

		//console.log('vizFile', vizFile);
		//console.log('DM', dashletManager);

		// then do scope.fileSystem.render(this.vizJson) in render

		//var something = angular.injector(['visualizer']).get(some dependency);
		//something.callSomething(prompts)
	};

	VisualizerIframePlugin.prototype.render = function(elementId, prompts) {
		var that = this;

		//console.log('prompts: ', prompts)

		var element = document.getElementById(elementId);

		element.innerHTML = '<iframe id="' + elementId + '-frame" style="height: 100%; width: 100%; overflow: scroll" frameborder="0" src="../visualizer/index.html#/reportView"></iframe>';

		var iframeLoaded = false;

		function sendReport(json) {
			//if ( !iframeLoaded ) {
				var iframeWindow = $(element).find('iframe').get(0).contentWindow;

				iframeWindow.postMessage(JSON.stringify({ report: json, prompts: prompts }), '*');
			//}
		}

		$(element).find('iframe').on('load', function() {
			sendReport(that.vizJson);
			//iframeLoaded = true;
		});

		var trySendingReport = setInterval(function() {
			sendReport(that.vizJson);
			//iframeLoaded = true;

			//$.postMessage( 'render', this.vizJson, $(element).find('iframe').get(0).contentWindow );
		}, 1000);

		setTimeout(function() {
			clearInterval(trySendingReport);
		}, 10000);
	};
	
	return { clz : VisualizerIframePlugin };
});