/* globals define */

define(['moment', 'visualizer' ], function( moment ) {
	'use strict';
	var ids = [];
	var visualizerInstanceNumber = 0;
	window.moment = moment;

	var VisualizerPlugin = function(vizFile, dashletManager) { /* instead of opening a report */
		this.dashletManager = dashletManager;
		this.vizJson = vizFile;
	};

	VisualizerPlugin.prototype.render = function(elementId, prompts) {
		window.assert = window.chai.assert;
		var $ = window.jQuery;

		var element = document.getElementById( elementId );

		if( ids.indexOf( elementId ) !== -1) {
			//Maybe handle refresh?
			$(element).find('[report-view]').scope().$broadcast('prompts', prompts);
			return;
		} else {
			ids.push( elementId );
		}

		element.innerHTML = '<div ng-view><div report-view></div></div>'; // could hide this until css loads if there were a noticeable delay

		var that = this;

		angular.module('visualizer-plugin', ['visualizer']).run(function($location) {
			$location.path('/visualizer/reportView');

			var reportViewLoaded = setInterval(function() {
				var scope = $(element).find('[report-view]').scope();
				scope.visualizerInstanceNumber = visualizerInstanceNumber++;

				if ( scope !== undefined ) {
					clearInterval(reportViewLoaded);
					try {
						scope.fileSystem.render(that.vizJson);
						scope.updateChart();
					} catch(e) {
						console.log('error', e);
					}
				}
			}, 500);

		});

		angular.bootstrap( element, [ 'templates-app', 'visualizer', 'visualizer-plugin' ] ); // this works, order of modules is not important
	};

	function loadCss(url) {
		var link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = url;
		document.getElementsByTagName('head')[0].appendChild(link);
	}

	loadCss('../visualizer/assets/birst-ui-0.0.1.css');

	return { clz : VisualizerPlugin };
});