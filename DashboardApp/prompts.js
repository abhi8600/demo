Ext.ns("Birst.dashboards");

//This is used only for text metrics to measure the width of texts in the list prompts
/*
Birst.dashboards.measureDataStore = Ext.create('Ext.data.ArrayStore', { autoDestroy: true, idIndex: 0, fields: [ 'Label', 'Value']});
Birst.dashboards.globalMeasureGrid = Ext.create('Ext.grid.Panel',
												{width: 100,
												 height: 100,
												 hidden: true,
												 title: 'Simpsons',
    											 store: Birst.dashboards.measureDataStore,
    											 columns: [{ text: 'Label', dataIndex: 'Label'}],
												 renderTo: Ext.getBody()});
*/
Birst.dashboards.globalMeasureGrid =
	Ext.create('Ext.form.Label', { text: 'test', renderTo: Ext.getBody(), cls: 'x-grid-cell-inner', hidden: true});

Ext.define('Birst.dashboards.PromptsController', {
	prompts: null,
	promptControls: null,
	promptControlsChangedParamName: null,
	promptCollections: null,
	promptsContainer: null,
	promptSection: null,
	pageController: null,
	me: null,
	applyOnCreate: null,
	sessionVarsToRetrieve: null,
	dashletPromptControls: null,
	dropDownPrompts: null,

	constructor: function(prompts, promptsContainer, pageController){
		"use strict";
		this.promptsContainer = promptsContainer;
		this.pageController = pageController;
		this.prompts = [];
		if (prompts) {
			for (var i = 0; i < prompts.length; i++) {
				this.prompts.push(prompts[i]);
			}
		}
		this.promptControls = new Ext.util.HashMap();
		this.dashletPromptControls = new Ext.util.HashMap();
		this.promptControlsChangedParamName = new Ext.util.HashMap();
		this.promptCollections = [];
		this.sessionVarsToRetrieve = [];
		this.dropDownPrompts = {};
		return this;
	},

	reset: function(){
		"use strict";
		for (var i = 0; i < this.prompts.length; i++) {
			var p = this.prompts[i];
			var name = getXmlChildValue(p, 'ParameterName');
			var pc = this.promptControls.get(name);
			if(pc){
				pc.clearSelectedValues();
			}
		}
		this.promptsContainer.removeAll();
	},

	checkSessionVars: function(defaultOpt){
		"use strict";
		if(defaultOpt && defaultOpt.indexOf('V{') !== -1){
			if(!Birst.core.SessionVars[defaultOpt]){
				this.sessionVarsToRetrieve.push(defaultOpt);
			}
		}
	},

	createPrompts: function(applyOnComplete) {
		"use strict";
		var me = this;
		this.applyOnComplete = applyOnComplete;
		this.sessionVarsToRetrieve = [];
		var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
		if (this.prompts.length > 0) {

			// Remove previous prompts, if any
			this.promptsContainer.removeAll();
	    	var hidden = false;
			var params = Ext.Object.fromQueryString(window.location.search);
			if (params['birst.hideDashboardPrompts'] && params['birst.hideDashboardPrompts'] === 'true') {
				// do nothing
			}
			else {
				this.showPrompts();
			}

			// create prompts
			var promptCount = 0;
			var i, p;
			for (i = 0; i < this.prompts.length; i++) {
				p = this.prompts[i];
				var type = getXmlChildValue(p, 'PromptType');
				var name = getXmlChildValue(p, 'ParameterName');
				var isExpression = getXmlChildValue(p, 'IsExpression') == 'true';

				//Don't recreate dashlet prompts
				if(this.dashletPromptControls.containsKey(name)){
					continue;
				}

				var promptControl = null;
				if (type === 'Query') {
					promptControl = Ext.create('Birst.dashboards.QueryPrompt', p);
				}else if (type === 'Value') {
					promptControl = Ext.create('Birst.dashboards.ValuePrompt', p);
				}else if (type === 'List') {
					promptControl = Ext.create('Birst.dashboards.ListPrompt', p);
				}else if (type == 'Slider'){
					promptControl = Ext.create('Birst.dashboards.SliderPrompt', p);

					//Check if value for session variables need to be retrieved
					this.checkSessionVars(getXmlChildValue(p, 'SliderDefaultStart'));
					this.checkSessionVars(getXmlChildValue(p, 'SliderDefaultEnd'));
				}

				if (promptControl) {
					this.promptControls.add(name, promptControl);
					promptCount = promptCount + 1;

					//If the parameter name changed, we use this for lookup in cases of drill down
					if(isExpression){
						var columnName = getXmlChildValue(p, 'ColumnName');
						if(columnName !== name){
							this.promptControlsChangedParamName.add(columnName, promptControl);
						}
					}
				}

				//Check if value for session variables need to be retrieved
				this.checkSessionVars(getXmlChildValue(p, 'DefaultOption'));
			}

			if (ctrlr.filtersOnOpen) {
				this.applyFilters(ctrlr.filtersOnOpen);
				ctrlr.filtersOnOpen = null;
			}

			var promptControlsValues = this.promptControls.getValues();
			var parentPrompt;
			for (i = 0; i < promptControlsValues.length; i++) {
				p = promptControlsValues[i];

				var parent = getXmlChildValue(p.prompt, 'immediateParentFilterParam');
				if (parent) {
					parentPrompt = this.promptControls.get(parent);
					if (parentPrompt) {
						parentPrompt.addChildPrompt(p);
					}
				}
				else {
					parent = p.prompt.getElementsByTagName('Parents');
					if (parent && parent.length > 0) {
						for (var j = 0; j < parent.length; j++) {
							var item = parent[j];
							parentPrompt = this.promptControls.get(getXmlChildValue(item, 'Parent'));
							if (parentPrompt) {
								parentPrompt.addChildPrompt(p);
								p.isChild = true;
							}
						}
					}
				}
			}

			for (i = 0; i < promptControlsValues.length; i++) {
				p = promptControlsValues[i];
				if (p.hasChildPrompts()) {
					p.addParentPromptListener(this.parentPromptUpdated, this);
				}
			}

			if (promptCount === 0) {
				this.hidePrompts();
			}

			this.fillPrompts();
			return (promptCount > 0);
		}
	},

	parentPromptUpdated: function(prompt, options) {
		"use strict";
		if (prompt.childPrompts && prompt.childPrompts.length > 0) {
			var promptsStr = '<Prompts>';
			promptsStr = this.addParentPromptsString(promptsStr, prompt);
			promptsStr = this.addPromptAndChildrenString(promptsStr, prompt);
			promptsStr += '</Prompts>';

			Birst.core.Webservice.adhocRequest('getFilteredFilledOptions',
												[{key: 'prompts', value: promptsStr}],
												this,
										  		function(response, options) {
										  			this.parse(response.responseXML);
//													this.applyPrompts();
												});
		}

	},

	addPromptAndChildrenString: function(promptsStr, prompt) {
		promptsStr += xml2Str(prompt.getSelectedValues());
		if (prompt.childPrompts && prompt.childPrompts.length > 0) {
			for (var i = 0; i < prompt.childPrompts.length; i++) {
				promptsStr = this.addPromptAndChildrenString(promptsStr, prompt.childPrompts[i]);
			}
		}
		return promptsStr;
	},

	//Create the prompt collection panel which will hold the prompts visually
	//Excludes embedded/dashlet prompts
	createPromptCollection: function(colName, container, prompt){
		colName = colName ? colName : ' ';

		var pWidth = !isNaN(prompt.maxWidth) ? prompt.maxWidth : 100;
		for(var i = 0; i < this.promptCollections.length; i++){
			var coll = this.promptCollections[i];
			if(coll.birstColl === colName){
				coll.promptsWidth += pWidth;
				return coll;
			}
		}

		var	coll = Ext.create('Ext.panel.Panel', {
					layout: {
						type: Birst.dashboards.doNP ? 'vbox' : 'hbox',
						align: 'stretch',
						pack: 'start'
					},
					cls: 'promptCollection',
					floatable: false,
					collapseDirection: Birst.dashboards.doNP ? 'top' : 'right',
					titleCollapse: true,
					collapsible: true,
					width: pWidth,
					height: '100%',
					margin: '0 5 0 0',
					title: colName,
					listeners: {
						beforerender: function(that, eOpts){
							that.setWidth(that.promptsWidth);
						}
					}

		});
		coll.birstColl = colName;
		coll.promptsWidth = pWidth;
		this.promptCollections.push(coll);

		return coll;

        },
	addParentPromptsString: function(promptsStr, prompt) {
		var parentPrompt;
		var parent = getXmlChildValue(prompt.prompt, 'immediateParentFilterParam');
		if (parent) {
			parentPrompt = this.promptControls.get(parent);
			if (parentPrompt) {
				promptsStr += xml2Str(parentPrompt.getSelectedValues());
				promptsStr = this.addParentPromptsString(promptsStr, parentPrompt);
			}
		}
		else {
			parent = prompt.prompt.getElementsByTagName('Parents');
			if (parent && parent.length > 0) {
				for (var j = 0; j < parent.length; j++) {
					var item = parent[j];
					parentPrompt = this.promptControls.get(getXmlChildValue(item, 'Parent'));
					if (parentPrompt) {
						promptsStr += xml2Str(parentPrompt.getSelectedValues());
						promptsStr = this.addParentPromptsString(promptsStr, parentPrompt);
					}
				}
			}
		}
		return promptsStr;
	},

	setAsDashletPrompt: function(pc){
		var parameterName = pc.getParameterName();
		this.dashletPromptControls.add(parameterName, pc);
		if(this.dashletPromptControls.getCount() == this.promptControls.getCount()){
			this.hidePrompts();
		}

		//If the prompts was previously mistakenly added to the prompt drawer due to xml attr @isInDashlet = true
		//remove it from the prompt drawer
		if(this.possiblyUsedInDashlets){
			var promptListing = this.possiblyUsedInDashlets[parameterName];
			if(promptListing){
				this.promptsContainer.remove(promptListing);
				delete this.possiblyUsedInDashlets[parameterName];
				//Mark it as used in dashlet
				pc.isDashletPrompt = true;
			}
		}
	},

	hidePrompts: function(){
		this.promptsContainer.hide();
		this.pageController.npPromptSection && this.pageController.npPromptSection.hide();
		this.pageController.leftPromptContainer && this.pageController.leftPromptContainer.hide();
	},

	showPrompts: function(){
		this.promptsContainer.show();
		this.pageController.npPromptSection && this.pageController.npPromptSection.show();
		if(this.pageController.leftPromptContainer){
			this.pageController.leftPromptContainer.show();
			this.pageController.showLeftPromptContainerByCfg();
		}else{
			this.pageController.createLeftSidePromptIndicator();
		}
	},

	toggleSelected: function(elem, highlight){
		//TODO: move to CSS
		if(highlight){
			//elem.style.fontWeight = 'bold';
			elem.style.borderColor = "#bcbcbc";
			elem.style.borderStyle = "solid";
			elem.style.borderWidth = 1;
			elem.style.borderRightStyle = 'none';
			elem.style.backgroundColor = '#f5f5f5';
		}else{
			elem.style.fontWeight = 'normal';
			elem.style.borderStyle = '';
			elem.style.borderColor = '';
			elem.style.borderRightStyle = '';
			elem.style.backgroundColor = '';
		}
	},


	renderPromptsToContainer: function(){
		//List the prompts on the left prompt container
		if(Birst.dashboards.doNP){
			var promptCollHash = {};
			this.currentPromptWindow = null;
			var that = this;
			var selectedElem = null;
			this.promptBodyClicked = false;

			//Function to close our open dialogs
			this.closeOpenDialogs = function(){
				this.currentPromptWindow && this.currentPromptWindow.hide();
			};

			//!toggle color
			this.untoggleSelected = function(){
				(selectedElem != null) && this.toggleSelected(selectedElem, false);
			};

			this.closeOpenDialogsUntoggleSelected = function(force){
				if(!this.promptBodyClicked || force === true){
					this.closeOpenDialogs();
					this.untoggleSelected();
				}
				this.promptBodyClicked = false;
			};

			//click on the body closes this dialog box
			document.body.addEventListener('click', this.closeOpenDialogsUntoggleSelected.bind(this));

			//Possibly used in dashlets
			this.possiblyUsedInDashlets = {};

			var applyBttn = null;
			for (var i = 0; i < this.prompts.length; i++) {
				var p = this.prompts[i];
				var name = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(p, 'ParameterName'));
				var visibleName =  Birst.Utils.Safe.getEscapedValue(getXmlChildValue(p, 'VisibleName'));

				//Don't recreate dashlet prompts
				if(this.dashletPromptControls.containsKey(name)){
					continue;
				}

				var prompt = this.promptControls.get(name);
				if (prompt && !prompt.isInvisible){
					//Apply button?
					if(!applyBttn){
						applyBttn = this.createApplyButton();
						var applyContainer = Ext.create('Ext.container.Container', {
														 layout: {
														 	type: 'hbox',
														 	align: 'stretch'
														 },
														 width: '100%',
													   	 margin: '1 1 1 5',
													   	 padding: 5,
													   	 height: 40,
													   	 cls: 'applyContainer',
														 items: [
														 	{  xtype: 'component', html: '<h4>Filters</h4>', flex: 1},
														 	applyBttn
														 ]
											 });
					   	this.promptsContainer.add(applyContainer);
					}

					var promptColl = prompt.getCollectionName()
					if(promptColl && !promptCollHash[promptColl]){
						promptCollHash[promptColl] = 1;
						var collCmp =
							Ext.create('Ext.Component',
									   { html: '<h5>' + promptColl + '</h5>',
									   	 width: '100%',
								   	 	 margin: '2 2 2 10',
								   	 	 padding: 5
									   });
						this.promptsContainer.add(collCmp);
					}

					var onRenderInnerPrompt = function(prompt){
						var that = this;
						return function(panel, eOpts){
							prompt.create(panel, that);
						}
					}.bind(this);

					var cmp =
						Ext.create('Ext.panel.Panel',
								   { //html: '<span>' + visibleName + '</span><span style="float:right;" class="entypo">&#9656;</span>',
								   	 title: visibleName,
								   	 width: '100%',
								   	 collapsible: true,
								   	 collapsed: true,
								   	 margin: '2 5 2 20',
								   	 //margin: 0,
								   	 padding: 0,
								   	 scope: this,
								   	 listeners: {
        								afterrender: {
												fn: onRenderInnerPrompt(prompt)
											}
        						  	 },
        						  	 cls: 'leftPromptListItem'
    								});
    				this.promptsContainer.add(cmp);

    				//This could possibly be removed later
    				if(p.getAttribute('isInDashlet') === 'true'){
    					this.possiblyUsedInDashlets[name] = cmp;
    				}
    			}
			}
		}else{
			this.renderPromptsToContainerClassic();
		}
	},

	//This is the floating popup prompt panel when you click on prompt card
	createFloatingPromptCard: function(prompt){
		var that = this;
		var applyBttn = that.promptCardApplyButton(false);
		var width = prompt.getCardMaxWidth();
		var promptWin = Ext.create('Ext.panel.Panel',
				{
					width: width,
					maxHeight: 330,
					floating: true,
					cls: 'floatingPrompt',
					closable: false,
					closeAction: 'hide',
					renderTo: Ext.getBody(),
					listeners: {
						afterrender: {
							fn: function(panel, eOpts){
								 if(!prompt.createInCard(panel)){
								 	prompt.create(panel, eOpts);
								 }
								 panel.add(applyBttn);
								 that.closeOpenDialogs();
								 that.currentPromptWindow = panel;

								 panel.body.on('click', function(evt){
								 					evt.stopPropagation();
								 					//hmm... stopPropagation() doesn't work?
								 					//set marker for now
								 					this.promptBodyClicked = true;
								 				});
								 prompt.beforeShow && prompt.beforeShow(panel, eOpts);
							}
						},
						beforeshow: {
							fn: function(panel, eOpts){
								that.closeOpenDialogs();
								that.currentPromptWindow = panel;
								try{
									prompt.beforeShow && prompt.beforeShow(panel, eOpts);
								}catch(e){
									//Retry as extjs gives us e = null exception... why extjs?
									prompt.beforeShow && prompt.beforeShow(panel, eOpts);
								}
							}
						}

					 }
				});
		promptWin.showApplyButton = function(show){
							if(show){
								applyBttn.show();
								//promptWin.setSize(width, 330);
							}else{
								applyBttn.hide();
								//promptWin.setSize(width, 275);
							}
						};
		return promptWin;
	},

	promptCardApplyButton: function(hidden){
		var applyBttn = this.createApplyButton(hidden, true);
		applyBttn.setWidth(46);
		var container = Ext.create('Ext.container.Container',
			{
				layout: {
					type: 'hbox',
					pack: 'end'
				},
				margin: '5 5 1 5',
				padding: 5,
				items: [
					   {  xtype: 'component', html: '', flex: 2},
					   applyBttn
				]
		 });
		 return container;
	},

	//Orange apply button to apply prompts
	createApplyButton: function(hidden, isCard){
		var applyBttn = Ext.create('Ext.button.Button', {
			width: 48,
			//cls: 'xb-modwin-submit-btn',
            overCls: 'xb-submit-btn-over',
            //pressedCls: 'xb-footer-btn-press',
			//align: 'left',
			//border: false,
			cls: 'submitBttn',
			text: Birst.LM.Locale.get('LB_APPLY'),
			scope: this,
			hidden: hidden,
			handler: function(me) {
				this.closeOpenDialogsUntoggleSelected(true);
				this.promptsContainer.suspendLayouts();
				for (var i = 0; i < this.prompts.length; i++) {
					var p = this.prompts[i];
					var name = getXmlChildValue(p, 'ParameterName');
					var visibleName =  getXmlChildValue(p, 'VisibleName');
					var prompt = this.promptControls.get(name);
					if (prompt && !(p.getAttribute('isInDashlet') === 'true') && !prompt.isInvisible){
						if(isCard === true){
							prompt.applyInCard();
						}else{
							prompt.toggleSelectedCheckBoxes && prompt.toggleSelectedCheckBoxes(null, null, false);
						}

					}
				}
				this.promptsContainer.resumeLayouts(true);
				this.applyPrompts(true, false, true);
			}

		});

		return applyBttn;
	},

	renderPromptsToContainerClassic: function(){
		"use strict";
		var doPromptCollection = true;
		this.promptSection = Ext.create('Ext.panel.Panel', {
			layout: {
				type: Birst.dashboards.doNP ? 'vbox' : 'hbox',
				align: 'stretch',
				pack: 'start'
			},
			overflowX: 'auto',
			width: '100%',
			border: false,
			region: 'center',
			floatable: false,
			listeners: {
				scope: this,
				afterrender: function(me){
					for (var i = 0; i < this.prompts.length; i++) {
						var p = this.prompts[i];
						var name = getXmlChildValue(p, 'ParameterName');
						var prompt = this.promptControls.get(name);
						if(doPromptCollection){
							if (prompt && !(prompt.control && p.getAttribute('isInDashlet') === 'true') && !prompt.isInvisible){
								var collection = this.createPromptCollection(prompt.getCollectionName(), me, prompt);
								prompt.create(collection, this);
							}
						}else{
							if(prompt){
								prompt.create(me, this);
							}
						}
					}

					if(doPromptCollection){
						for(var i = 0; i < this.promptCollections.length; i++){
							var coll = this.promptCollections[i];
							me.add(coll);
						}
					}
				}
			},
			bodyStyle:{
				'background': 'rgba(255, 255, 255, 0.1)'
				//'opacity' : 0.8
				//'background' : Birst.DashboardApp.getStyleBGColor('backgroundColor1', 'backgroundColor2')
			}
		});

		this.promptsContainer.add(this.promptSection);

		this.promptsContainer.add(Ext.create('Ext.panel.Panel', {
			layout: {
				type: 'hbox',
				pack: 'start'
			},
			width: '100%',
			region: 'south',
			border: false,
			margin: '15 0 6 0',
			items: [{
				xtype: 'panel',
				layout: 'hbox',
				border: false,
				minHeight: 38,
				//cls: 'promptApplyButton',
				items: [{
					xtype: 'button',
					height: 33,
					cls: 'xb-modwin-submit-btn',
                    overCls: 'xb-submit-btn-over',
                    pressedCls: 'xb-footer-btn-press',
					align: 'left',
					border: false,
					text: Birst.LM.Locale.get('LB_APPLY'),
					scope: this,
					handler: function(me) {
						this.applyPrompts(true, false, true);
					}
				}
				        ]
			}],
			bodyStyle:{
				'background': 'rgba(255, 255, 255, 0.1)'
				//'background-color' : Birst.DashboardApp.getColorSetting('backgroundColor1')
			}
		}));

		if (Birst.dashboards.Page.getPagePromptAutoExp().expand) {
			this.promptsContainer.expand();
		}
	},

	fillPrompts: function() {
		"use strict";
		if(this.sessionVarsToRetrieve.length > 0){
			this.getEvaluateExpression();
		}else{
			this.getFillOptions();
		}
	},

	getEvaluateExpression: function(){
		"use strict";
		var exprs = '<Expressions>';
		for(var i = 0; i < this.sessionVarsToRetrieve.length; i++){
			exprs += '<Expression Expression="' + this.sessionVarsToRetrieve[i] + '"/>';
		}
		exprs += '</Expressions>';

		Birst.core.Webservice.dashboardRequest('evaluateExpression',
												[{key: 'expression', value: exprs}],
												this,
												function(response, options){
													var exprs = response.responseXML.getElementsByTagName('Expression');
													for (var i = 0; exprs && i < exprs.length; i++){
														var key = exprs[i].getAttribute('Expression');
														var value = exprs[i].getAttribute('Value');
														Birst.core.SessionVars[key] = value;
													}
													this.getFillOptions();
												});
	},

	getFillOptions: function() {
		"use strict";
		//Fill the prompts via webservice call
		var promptsStr = '<Prompts>';
		if (this.prompts.length > 0) {
			for (var i = 0; i < this.prompts.length; i++) {
				var p = this.prompts[i];
				if(p.childNodes.length > 0){ //Filter out empty <Prompt ref=XXX/>
					promptsStr += xml2Str(p);
				}
			}
		}
		promptsStr += '</Prompts>';

		Birst.core.Webservice.adhocRequest('getFilledOptions',
											[{key: 'prompts', value: promptsStr}],
											this,
									  		function(response, options) {
									  			this.parse(response.responseXML);
									  			this.renderPromptsToContainer();
												if (this.applyOnComplete){
													this.applyOnComplete = false;
													//This calls getDashboardRenderedReportFromXML()
													this.applyPrompts();
												}else{
													//This is the first time the page was rendered, calls getDashboardRenderedReport()
										  			if (this.hasChildPrompts()) {
										  				this.updateChildPrompts();
										  			}
										  			else {
										  				this.pageController.renderToMainContainer(this.getPromptsData());
										  			}
												}
											},null,null,true);
	},

	hasChildPrompts: function() {
		"use strict";
		var promptControlsValues = this.promptControls.getValues();
		for (var i = 0; i < promptControlsValues.length; i++) {
			var prompt = promptControlsValues[i];
			if (prompt.hasChildPrompts()) {
				return true;
			}
		}
		return false;
	},

	updateChildPrompts: function(updateOnly, useSessionVars) {
		"use strict";
		var promptsStr = '<Prompts>';
		var promptControlsValues = this.promptControls.getValues();
		for (var i = 0; i < promptControlsValues.length; i++) {
			var prompt = promptControlsValues[i];
			if (prompt.hasChildPrompts()) {
				promptsStr += xml2Str(prompt.getSelectedValues());
				for (var j = 0; j < prompt.childPrompts.length; j++) {
					promptsStr += xml2Str(prompt.childPrompts[j].getSelectedValues());
				}

			}
		}
		promptsStr += '</Prompts>';

		Birst.core.Webservice.adhocRequest('getFilteredFilledOptions',
												[{key: 'prompts', value: promptsStr}],
												this,
										  		function(response, options) {
										  			this.parse(response.responseXML);
										  			if(updateOnly){
										  				this.pageController.updateDashlets(useSessionVars);
										  			}else{
										  				this.pageController.renderToMainContainer(this.getPromptsData());
										  			}
												});
	},

	/**
	 * Returns the prompt xml string to submit to server and set the text label / card in the prompt summary area
	 * @param {} useSelectedValues
	 * @param {} ignorePromptList
	 * @return {}
	 */
	getPromptsData: function(useSelectedValues, ignorePromptList){
		return Birst.dashboards.doNP ? this.getPromptsDataNP(useSelectedValues, ignorePromptList) : this.getPromptsDataOriginal(useSelectedValues, ignorePromptList);
	},

	/**
	 * Returns the propmpt xml string + original / classic version of the prompt summary
	 * @param {} useSelectedValues
	 * @param {} ignorePromptList
	 * @return {}
	 */
	getPromptsDataOriginal: function(useSelectedValues, ignorePromptList){
		"use strict";
		var promptsData = '<Prompts>';
		var promptSummary = '';
		for (var i = 0; i < this.prompts.length; i++) {
			var p = this.prompts[i];
			var name = getXmlChildValue(p, 'ParameterName');
			var label = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(p, 'VisibleName'));
			var operator = getXmlChildValue(p, 'Operator');
			var isInvisible = getXmlChildValue(p, 'Invisible') === 'true';

			if (! label) {
				label = name;
			}
			if (ignorePromptList) {
				if (ignorePromptList.indexOf(label) >= 0) {
					continue;
				}
			}
			var prompt = this.promptControls.get(name);
			if (prompt) {
				var selectedValues = prompt.getSelectedValues(useSelectedValues);
				promptsData = promptsData + xml2Str(selectedValues);
				if(!isInvisible){
					var str = this.appendPromptSummaryString(prompt, label, operator, selectedValues);
					promptSummary += str;
				}
			}
		}

		if (!ignorePromptList) {
			var id = this.pageController.button.getId() + '-promptSummary';
			var summary = Ext.getCmp(id);
 			if (summary) {
				summary.setText(promptSummary, false); // prevent double escaping
				if (promptSummary.length > 0) {
					Ext.create('Ext.tip.ToolTip',{
				        target: id,
				        html: promptSummary
					});
				}
			}
		}

		promptsData = promptsData + '</Prompts>';
		return promptsData;
	},

	getPromptsObjectDefintion: function(){
		var keyValues = {};
		for (var i = 0; i < this.prompts.length; i++) {
			var p = this.prompts[i];
			var name = getXmlChildValue(p, 'ParameterName');

			/*
			if (ignorePromptList) {
				if (ignorePromptList.indexOf(label) >= 0) {
					continue;
				}
			}*/

			var prompt = this.promptControls.get(name);
			if (prompt) {
				var values = prompt.getSelectedValuesArray();
				if(values.length > 0){
					var values = {
						operator : prompt.isRangeSlider && prompt.isRangeSlider() ?  'between' : getXmlChildValue(p, 'Operator'),
						multiSelectType : getXmlChildValue(p, 'multiSelectType'),
						columnName: getXmlChildValue(p, 'ColumnName'),
						dataType: getXmlChildValue(p, 'Class'),
						selectedValues : values
					};
					keyValues[name] = values;
				}
			}
		}
		return keyValues;
	},



	getUrlParameters: function(rootUrl){
		if(this.prompts.length == 0 || !rootUrl){
			return rootUrl;
		}

		var urlStr = rootUrl;
		var questionMarkIdx = rootUrl.indexOf('?');

		//Add ? at end if it is not there
		if(!(questionMarkIdx > 0  && questionMarkIdx === urlStr.length - 1)){
			urlStr += '?';
		}

		for (var i = 0; i < this.prompts.length; i++) {
			var p = this.prompts[i];
			var label = getXmlChildValue(p, 'VisibleName');
			var name = getXmlChildValue(p, 'ParameterName');

			if (! label) {
				label = name;
			}

			/*
			if (ignorePromptList) {
				if (ignorePromptList.indexOf(label) >= 0) {
					continue;
				}
			}*/

			var prompt = this.promptControls.get(name);
			if (prompt) {
				var values = prompt.getSelectedValuesArray();
				if(values.length > 0){
					urlStr += name + '=';
					for(var y = 0; y < values.length; y++){
						urlStr += values[y];
						if(y < values.length - 1){
							urlStr += ';';
						}
					}
					if(i < this.prompts.length - 1){
						urlStr += '&';
					}
				}
			}
		}
		return urlStr;
	},

	appendPromptSummaryString: function(prompt, label, operator, selectedValues){
		var promptSummary = '';
		var parent = selectedValues.getElementsByTagName('selectedValues');
		if (parent) {
			parent = parent[0];
			var children = parent.childNodes;
			var between = '';
			if (children.length > 1) {
				promptSummary += '(';
				between = getXmlChildValue(selectedValues, 'multiSelectType');
			}
			for (var j = 0; j < children.length; j++) {
				var val = getXmlValue(children[j]);
				if (val === 'NO_FILTER' || val === '') {
					continue;
				}

				val = prompt.getValueLabel(val);

				if(prompt.isRangeSlider && prompt.isRangeSlider()){
					if(j == 0){
						promptSummary += val + ' <= ';
					}else{
						promptSummary += label + ' <= ' + val;
					}
				}else{
					if (j > 0) {
						promptSummary = promptSummary + ' ' + between + ' ';
					}

					promptSummary += label;
					promptSummary += ' ';
					promptSummary += operator;
					promptSummary += ' ';
					promptSummary += val;
					promptSummary += '; ';
				}
			}
			if (children.length > 1) {
				promptSummary += ')';
			}
		}
		return promptSummary;
	},

	/**
	 * Returns the propmpt xml string + prompt card version of the prompt summary
	 * @param {} useSelectedValues
	 * @param {} ignorePromptList
	 * @return {}
	 */
	getPromptsDataNP: function(useSelectedValues, ignorePromptList){
		"use strict";

		var id = this.pageController.button.getId() + '-promptSummary';
		var summaryContainer = Ext.getCmp(id);
		summaryContainer.removeAll();

		var promptsData = '<Prompts>';
		var promptSummary = '';
		for (var i = 0; i < this.prompts.length; i++) {
			var p = this.prompts[i];
			var name = getXmlChildValue(p, 'ParameterName');
			var label = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(p, 'VisibleName'));
			var operator = getXmlChildValue(p, 'Operator');
			var isInvisible = getXmlChildValue(p, 'Invisible') === 'true';

			if (! label) {
				label = name;
			}
			if (ignorePromptList) {
				if (ignorePromptList.indexOf(label) >= 0) {
					continue;
				}
			}
			var prompt = this.promptControls.get(name);
			if (prompt) {
				prompt.promptsController = this;

				var selectedValues = prompt.getSelectedValues(useSelectedValues);
				promptsData = promptsData + xml2Str(selectedValues);

				//Don't display the prompt card for invisible prompts
				if(isInvisible){
					continue;
				}

				var parent = selectedValues.getElementsByTagName('selectedValues');
				if (parent) {
					var promptStr = '';
					parent = parent[0];
					var children = parent.childNodes;
					var between = '';
					if (children.length > 1) {
						between = getXmlChildValue(selectedValues, 'multiSelectType');
					}
					for (var j = 0; j < children.length; j++) {
						var val = getXmlValue(children[j]);
						if (val === 'NO_FILTER' || val === '') {
							continue;
						}

						val = prompt.getValueLabel(val);

						if(prompt.isRangeSlider && prompt.isRangeSlider()){
							if(j == 0){
								promptStr += val + ' <= ';
							}else{
								promptStr += label + ' <= ' + val;
							}
						}else{
							if(j == 0){
								promptStr += label;
								promptStr += ' ';
								promptStr += operator;
								promptStr += ' ';
								promptStr += val;
							}else{
								promptStr += ',' + val;
							}
						}
					}

					//Action: user clicks on prompt cards'
					var arrowFunc = function(prompt){
						return function(bttn, evt){
							this.closeOpenDialogsUntoggleSelected();
							summaryContainer.remove(bttn);
							prompt.resetAll();
						}.bind(this);
					}.bind(this);

					//Action: user clicks on prompt card
					var clickFunc = function(p){
						return function(bttn, evt){
							this.closeOpenDialogs();
							this.promptBodyClicked = true;
							var loc = bttn.getPosition();
							var w = this.dropDownPrompts[p.visibleName];
							if(w){
								this.currentPromptWindow = w;
							}else{
								w = this.createFloatingPromptCard(p);
								this.dropDownPrompts[p.visibleName] = w;
								w.showApplyButton && w.showApplyButton(true);
							}
							var xLoc = loc[0];
							var dlgWidth = p.getCardMaxWidth();
							if( (xLoc + dlgWidth) > window.innerWidth){
								xLoc = window.innerWidth - dlgWidth;
							}
							w.showAt(xLoc, loc[1] + 30);
						}.bind(this);
					}.bind(this);

					if(promptStr !== ''){
						var button = Ext.create('Ext.button.Split', {
							text: promptStr,
							tooltip: promptStr,
							height: 30,
							maxWidth: 250,
							shrinkWrap: 1,
							disabled: prompt.isDashletPrompt,
							overCls: prompt.isDashletPrompt ? 'disabledPromptCard' : 'promptCardOver',
							cls: prompt.isDashletPrompt ? 'disabledPromptCard' : 'promptCard',
							arrowHandler: arrowFunc(prompt),
							handler: clickFunc(prompt)
						});
						summaryContainer.add(button);
					}else{
						promptSummary += promptStr;
					}
				}
			}
		}

		if (!ignorePromptList && !Birst.dashboards.doNP) {
			var id = this.pageController.button.getId() + '-promptSummary';
			var summary = Ext.getCmp(id);
 			if (summary) {
				summary.setText(promptSummary);
			}
		}

		promptsData = promptsData + '</Prompts>';
		return promptsData;
	},

	applyPromptsImmediately: function() {
		"use strict";
		this.applyPrompts(true, false, true);
	},

	applyPrompts: function(userClicked, useSessionVars, isApplyButton) {
		"use strict";

		this.updatePromptSessionVars();
		//Check if there are parent child prompts which we have to update before
		//applying. See DASH-428
		if(!isApplyButton && this.hasChildPrompts()){
			this.updateChildPrompts(true, useSessionVars);
			return;
		}

		this.pageController.updateDashlets(useSessionVars);
	},

	parse: function(xmlDoc) {
		"use strict";
		var promptValuesArray = xmlDoc.getElementsByTagName('PromptValues');
		if(promptValuesArray){
			var metrics = new Ext.util.TextMetrics(Birst.dashboards.globalMeasureGrid);
			var child = promptValuesArray[0].firstChild;
			while (child) {
				var promptId = null;
				if (child.nodeType === 1 && child.tagName === 'PromptValue') {
					var grandChild = child.firstChild;
					if (grandChild.tagName === 'PromptID') {
						promptId = getXmlChildValue(child, 'PromptID');
						var pc = this.promptControls.get(promptId);
						if (! pc) {
							child = child.nextSibling;
							continue;
						}

						var selValArray = pc.getSelectedValuesArray();
						var setSelectedValue = (!selValArray || selValArray.length === 0);
						var type = getXmlChildValue(pc.prompt, 'PromptType');
						if (type === 'Value' || type === 'List') {
							pc.setSelectedValue(pc.getDefaultOption());
						}

						//We only measure the text width of list prompts
						var doMeasureColWidth = type === 'Query';
						var expandIntoButtons = pc.isCheckBoxRadio();

						if(doMeasureColWidth){
							pc.maxWidth = expandIntoButtons ? 0 : metrics.getWidth(pc.visibleName) + 65;
						}

						grandChild = grandChild.nextSibling;
						if (grandChild) {
							pc.clearStore();
						}

						//Slider values
						var idx = 0;
						var sliderDefaultValues = [];

						//Non query slider prompts won't have any values, we set the default values here
						if(pc.isSlider && !pc.isQuery() && !grandChild){
							var defs = pc.getDefaultValuesForPointType();
							var valsToSet = pc.isRangeSlider() ? [defs.startPt, defs.endPt] : [defs.startPt];
							pc.setSelectedValuesByArray(valsToSet);
						}

						var maxStringLen = 0;
						var defaultValue = pc.getDefaultOption();
						var matchedDefault = false;
						while (grandChild) {
							if (grandChild.nodeType === 1 && grandChild.tagName === 'Value') {
								// Fixed DASH-144
								var label = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(grandChild, 'Label'));
								var value = getXmlChildValue(grandChild, 'Value');
								var greatGrandChild = grandChild.firstChild;
								if (label && value && pc){
									//Fix for DASH-151, set the width of the query / list prompt's width based on data
									if(doMeasureColWidth && metrics){
										if(expandIntoButtons === true){
											pc.maxWidth += metrics.getWidth(label) + 39;
										}else{
											if(label.length - maxStringLen > 2){
												//Additional padding for width, we prolly don't need this if the css is correct for the label used for metrics
												var width = metrics.getWidth(label) + 65;
												if(width > pc.maxWidth){
													pc.maxWidth = width;
												}
												maxStringLen = label.length;
											}
										}
									}
									pc.addToStore({Label: label, Value: value});
									idx++;
									if(setSelectedValue){
										//BPD-20884 - Date values should be, eg - 1996-06-04
										//but for some reason values are returned with extra chars, eg - 1996-06-04T00:00Z
										//We cut off the rest.
										if(pc.isDate && value.length > 10){
											value = value.substr(0, 10);
										}

										if(pc.isSlider){
											if(pc.isRangeSlider()){
												if(value === pc.getDefaultStart()){
													sliderDefaultValues.push(value);
													pc.setDefaultStartIdx(idx);
													pc.setSelectedValue(value);
												}
												if(value === pc.getDefaultEnd()){
													pc.setDefaultEndIdx(idx);
													sliderDefaultValues.push(value);
													if(sliderDefaultValues.length < 3){
														pc.setSelectedValuesByArray(sliderDefaultValues);
													}
												}
											}else{
												if(value === defaultValue){
													pc.setDefaultStartIdx(idx);
													pc.setSelectedValue(value);
												}
											}
										}else{
											if(this.matchDefaultOptions(value, pc)){
												matchedDefault = true;
												pc.appendSelectedValue(value);
											}
										}
									} else {
										// Oddly Acorn lowercases boolean types but ptompt values are case sensitive (part of DASH-208 testing)
										if(setSelectedValue && defaultValue && (value == 'True' || value == 'False' ) &&  value.toLowerCase() === defaultValue.toLowerCase()) {
											pc.setSelectedValue(value);
										}
									}
								}
							}
							grandChild = grandChild.nextSibling;
						}
						pc.checkBoxControlRefresh && pc.checkBoxControlRefresh();
					}
				}

				// DASH-208/209 if prompt doesn't have default value set
				if (setSelectedValue && !pc.getDefaultOption() && !pc.isSlider && !matchedDefault) {
					var st = pc.getStore();
					pc.setSelectedValue(st.getAt(0).data.Value);
				}
				child = child.nextSibling;
			}
		}
		if (this.promptControls) {
			var prompts = this.promptControls.getValues();
			for (var i = 0; i < prompts.length; i++) {
				prompts[i].setFillingComplete();
			}
		}
	},
	
	getDefaultOptions: function(pc){
		if(pc.isMultiSelect()){
			var mvOpts = pc.getMVDefaultOption();
			if(mvOpts.length > 0){
				return mvOpts;
			}else{
				var defOpt = pc.getDefaultOption();
				return [defOpt];
			}
		}else{
			var defOpt = pc.getDefaultOption();
			return [defOpt];
		}
	},
	
	matchDefaultOptions: function(value, pc){
		var defaultOptions = this.getDefaultOptions(pc);
		for(var i = 0; i < defaultOptions.length; i++){
			if(value === defaultOptions[i]){
				return true;
			}
		}
		
		return false;
	},

	addPrompts: function(newPrompts) {
		"use strict";
		var hasExistingPrompts = false;
		var child = newPrompts.firstChild;
		var newlyCreatedPrompts = [];
		while (child) {
			if (child.nodeName === 'Prompt') {
				var name = getXmlChildValue(child, 'ParameterName');
				var prompt = this.promptControls.get(name);
				if(!prompt){
					prompt = this.promptControlsChangedParamName.get(name);
				}
				if (prompt) {
					var selectedValue = child.getElementsByTagName('selectedValues');
					prompt.setSelectedValues(selectedValue, true);
					hasExistingPrompts = true;
				}
				else {
					this.prompts.push(child);

					if (this.promptSection) {
						// add new prompt
						var type = getXmlChildValue(child, 'PromptType');
						var promptControl = null;
						if (type === 'Query') {
							promptControl = Ext.create('Birst.dashboards.QueryPrompt', child);
						}
						else
						if (type === 'Value') {
							promptControl = Ext.create('Birst.dashboards.ValuePrompt', child);
						}
						else if (type === 'List') {
							promptControl = Ext.create('Birst.dashboards.ListPrompt', p);
						}

						if (promptControl) {
							this.promptControls.add(name, promptControl);
							newlyCreatedPrompts.push(promptControl);
						}
					}
				}
			}
			child = child.nextSibling;
		}

		if (this.promptSection) {
			//Make the call to retrieve the new prompts data from the server only if we need to,
			//and show the prompt section because we may have hidden it if there were only
			//embedded prompts.
			//Existing prompts - click the apply button - DASH-421
			if(newlyCreatedPrompts.length > 0){
				//Fill the prompts via webservice call
				var promptsStr = '<Prompts>';
				for (var i = 0; i < newlyCreatedPrompts.length; i++) {
					promptsStr += xml2Str(newlyCreatedPrompts[i].prompt);
				}
				promptsStr += '</Prompts>';

				Birst.core.Webservice.adhocRequest('getFilledOptions',
													[{key: 'prompts', value: promptsStr}],
													this,
											  		function(response, options) {
											  			var newlyCreatedPrompts = options.additionalParam;
											  			this.parse(response.responseXML);

											  			for (var i = 0; i < newlyCreatedPrompts.length; i++) {
											  				newlyCreatedPrompts[i].create(this.promptSection, this);
											  			}
											  			this.showPrompts();
											  			this.applyPrompts();
													},
													null,
													newlyCreatedPrompts,null,true);

			}else if(hasExistingPrompts){
				this.applyPrompts();
			}
		}else {
			this.createPrompts(true);
		}
	},

	isPromptSessionVarsInSync: function(){
		"use strict";
		var keys = this.promptControls.getKeys();
		for(var i = 0; i < keys.length; i++){
			var pc = this.promptControls.get(keys[i]);
			var key = pc.getParameterName();
			var sessVals = Birst.core.PromptVariables.get(key);

			if(sessVals){
				var pcVals = pc.getSelectedValuesArray();
				var isSingleValue = !pc.isMultiSelect();

				if(!isSingleValue && pcVals.length !== sessVals.length){
					return false;
				}

				for(var j = 0; j < sessVals.length; j++){
					var equals = sessVals[j] === pcVals[j];

					if(j === 0 && isSingleValue){
						return equals;
					}

					if(!equals){
						return false;
					}
				}

			}
		}

		return true;
	},

	updatePromptSessionVars: function(){
		"use strict";
		var keys = this.promptControls.getKeys();
		for(var i = 0; i < keys.length; i++){
			var pc = this.promptControls.get(keys[i]);
			if(pc.isVariableToUpdate()){
				var selectedVals = pc.getSelectedValuesArray();
				if(selectedVals.length > 0){
					var paramName = pc.getParameterName();
					Birst.core.PromptVariables.remove(paramName);
					Birst.core.PromptVariables.add(paramName, selectedVals);
				}
			}
		}
	},

	setPromptValuesBySessionVars: function(){
		"use strict";
		var keys = this.promptControls.getKeys();
		for(var i = 0; i < keys.length; i++){
			var pc = this.promptControls.get(keys[i]);
			if(pc.isReportPrompt()){
				continue;
			}

			var key = pc.getParameterName();
			var values = Birst.core.PromptVariables.get(key);
			if(values){
				var selectedValues = pc.setSelectedValuesByArray(values);
				pc.setSelectedValueInControl([selectedValues]);
			}

		}
	},

	applyFilters: function(filters) {
		"use strict";
		// they don't seem to support replacing + with space
		filters = filters.replace(/\+/g, ' ');
		var newFilters = Ext.Object.fromQueryString(filters);
		var keys = this.promptControls.getKeys();
		var filterKeys = Ext.Object.getKeys(newFilters);
		for (var j = 0; j < keys.length; j++) {
			var pc = this.promptControls.get(keys[j]);
			if (filterKeys.indexOf(pc.getParameterName()) >= 0) {
				var selectedValues = pc.setSelectedValuesByArray([newFilters[pc.getParameterName()]]);
					pc.setSelectedValueInControl([selectedValues]);
			}
		}
	}

});

Ext.define('Birst.dashboards.Prompt', {
	prompt: null,
	st: null,
	control: null,
	isFilling: false,
	isParent: false,
	isChild: false,
	isInvisible: false,
	parentPromptListenerFn: null,
	parentPromptListenerScope: null,
	parentPromptListenerOptions: null,
	childPrompts: null,
	parameterName: null,
	toolTip: null,
	visibleName: null,
	isDashletPrompt: false,
	isDate: false,
	isDateTime: false,

	constructor: function(prompt) {
		"use strict";
		this.prompt = prompt;
		this.isFilling = true;
		this.isInvisible = false;
		this.isDashletPrompt = false;
		this.isChild = false;

		var invisibleEls = this.prompt.getElementsByTagName('Invisible');
		if(invisibleEls && invisibleEls.length > 0){
			this.isInvisible = getXmlValue(invisibleEls[0], 'Invisible') === 'true';
		}

		this.parameterName =  Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'ParameterName'));
		this.visibleName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'VisibleName'));
		this.toolTip = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'ToolTip'));

		var clazz = getXmlChildValue(this.prompt, 'Class');
		this.isDate = clazz === 'Date';
		this.isDateTime = clazz === 'DateTime';
	},

	//Replicate prompt in the prompt summary drop down panel
	createInCard: function(panel){
		return false;
	},

	//When user clicks on apply in prompt card
	applyInCard: function(){
		return false;
	},

	getCardMaxWidth: function(){
		return this.maxWidth ? (this.maxWidth < 230 ? 230 : this.maxWidth) : 230;
	},

	addToolTip: function(ctrl){
		"use strict";
		if(this.toolTip){
			var control = ctrl ? ctrl : this.control;
			Ext.create('Ext.tip.ToolTip', { target: control.getEl(), html: this.toolTip});
		}
	},

	setFillingComplete: function() {
		"use strict";
		this.isFilling = false;
	},

	addParentPromptListener: function(fn, scope, options) {
		"use strict";
		this.parentPromptListenerFn = fn;
		this.parentPromptListenerScope = scope;
		this.parentPromptListenerOptions = options;
	},

	addChildPrompt: function(childPrompt) {
		"use strict";
		if (!this.childPrompts) {
			this.childPrompts = [];
		}
		this.childPrompts.push(childPrompt);
	},

	hasChildPrompts: function() {
		"use strict";
		return (this.childPrompts && this.childPrompts.length > 0);
	},

	getValueLabel: function(value) {
		if (this.st) {
			// has store - use it
				var index = this.st.findExact('Value', value);
				if (index >= 0) {
					var r = this.st.getAt(index);
					return r.data.Label;
				}
		}

		return value;
	},

	getStore: function() {
		"use strict";
		if (!this.st) {
			this.st = this.createStore();
		}
		return this.st;
	},

	createStore: function(){
		var store = Ext.create('Ext.data.ArrayStore', {
				autoDestroy: true,
				idIndex: 0,
				fields: [
					'Label',
					'Value'
				]
			});

		if (this.prompt && this.isAddNoSelectionEntry()) {
			var text = getXmlChildValue(this.prompt, 'text');
			if (! text) {
				text = 'All';
			}
			store.add({Label: text, Value: 'NO_FILTER', Index: 0});
		}
		return store;
	},

	addToStore: function(s) {
		"use strict";

		var store = this.getStore();
		var idx = store.count();
		s.Index = idx;
		store.add(s);
	},

	hasStore: function() {
		"use strict";
		return true;
	},

	clearStore: function() {
		"use strict";
		this.getStore().removeAll(false);
		if (this.prompt && this.isAddNoSelectionEntry()) {
			var text = getXmlChildValue(this.prompt, 'text');
			if (!text) {
				text = 'All';
			}
			this.st.add({Label: text, Value: 'NO_FILTER'});
		}
	},

	getDefaultOption: function(){
		"use strict";

		var defaultOpt = getXmlChildValue(this.prompt, 'DefaultOption');
		if(defaultOpt && defaultOpt.indexOf('V{') !== -1){
			defaultOpt = Birst.core.SessionVars[defaultOpt];
			if (defaultOpt.length > 0 && defaultOpt.charAt(0) == '\'' && defaultOpt.charAt(defaultOpt.length - 1) == '\'')
				defaultOpt = defaultOpt.substring(1, defaultOpt.length - 1);
		}

		//BPD-20884.
		if(this.isDate === true){
			if(defaultOpt && defaultOpt.length > 0){
				var spaceIdx = defaultOpt.indexOf(" ");
				if(spaceIdx > 0){
					defaultOpt = defaultOpt.substring(0, spaceIdx);
				}
			}
		}

		return defaultOpt;
	},
	
	getMVDefaultOption: function(){
		var ops = [];
		var mvDef = this.prompt.getElementsByTagName('MVDefaultOptions');
		if (mvDef && mvDef.length > 0) { 
			mvDef = mvDef[0];
			var valXml = mvDef.getElementsByTagName('Value');
			for(var i = 0; i < valXml.length; i++){
				var v = getXmlValue(valXml[i]);
				ops.push(v);
			}
		}
		return ops;
	},

	isReadOnly: function(){
		"use strict";
		return getXmlChildValue(this.prompt, 'ReadOnly') === 'true';
	},

	isAddNoSelectionEntry: function(){
		"use strict";
		return getXmlChildValue(this.prompt, 'addNoSelectionEntry') === 'true';
	},

	isVariableToUpdate: function(){
		"use strict";
		var isVar = getXmlChildValue(this.prompt, 'variableToUpdate');
		return (isVar && isVar === 'true');
	},

	isReportPrompt: function(){
		"use strict";
		return getXmlChildValue(this.prompt, 'IsReportPrompt') != null;
	},

	isMultiSelect: function(){
		"use strict";
		var multiSelectType = getXmlChildValue(this.prompt, 'multiSelectType');
		return (multiSelectType === 'OR' || multiSelectType === 'AND');
	},

	isCheckBoxRadio: function (){
		"use strict";
		var expand = getXmlChildValue(this.prompt, 'ExpandIntoButtons');
		var compare = expand === "true";
		return compare;
	},

	getCollectionName: function(){
		"use strict";
		return getXmlChildValue(this.prompt, 'Collection');
	},

	getParameterName: function(){
		"use strict";
		return getXmlChildValue(this.prompt, 'ParameterName');
	},

	appendSelectedValueElement: function(selectedValues, value){
		"use strict";
		var data = selectedValues.ownerDocument.createElement('selectedValue');
		data.appendChild(selectedValues.ownerDocument.createTextNode(value));
		selectedValues.appendChild(data);
	},

	getSelectedValue: function() {
		"use strict";
		return null;
	},

	getSelectedValuesXml: function(){
		"use strict";
		if(this.prompt){

		}
		return null;
	},

	getSelectedValuesArray: function(){
		"use strict";
		var v = [];
		if(this.prompt){
			var svs = this.prompt.getElementsByTagName('selectedValues');
			if (svs && svs.length > 0 && svs[0].firstChild) {
				var child = svs[0].firstChild;
				while (child) {
					v.push(getXmlValue(child, 'selectedValue'));
					child = child.nextSibling;
				}
			}
		}
		return v;
	},

	setSelectedValuesByArray: function(values){
		"use strict";
		if(values && values.length > 0){
			var selectedValues = this.clearAddNewSelectedValuesTag();
			for(var i = 0; i < values.length; i++){
				this.appendSelectedValueElement(selectedValues, values[i]);
			}
			this.setSelectedValueInControl(this.prompt.getElementsByTagName('selectedValues'));
			return selectedValues;
		}
		return null;
	},

	getSelectedValues: function(useSelectedValues) {
		"use strict";
		var child, i, data;
		var v = ['NO_FILTER'];
		var selectedValueAlreadySet = false;
		if (this.prompt) {
			var svs = this.prompt.getElementsByTagName('selectedValues');
			if (svs && svs.length > 0 && svs[0].firstChild) {
				v = [];
				child = svs[0].firstChild;
				while (child) {
					var val = getXmlValue(child, 'selectedValue');
					//val =  Ext.String.htmlEncode(val);
					//The above encodes &"'<> but our server side encoding doesn't encode ', hence this solution leaving out '
					val = val.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
					v.push(val);
					child = child.nextSibling;
				}
				selectedValueAlreadySet = true;
			}
		}
		var controlSelectedValue = this.getSelectedValue();
		if (controlSelectedValue && controlSelectedValue.length > 0){
			if(!selectedValueAlreadySet || useSelectedValues === false){
				v = controlSelectedValue;
			}
		}

		var selectedValues = this.prompt.getElementsByTagName('selectedValues');
		if (! selectedValues || selectedValues.length === 0) {
			selectedValues = this.prompt.ownerDocument.createElement('selectedValues');
			this.prompt.appendChild(selectedValues);
		}
		else {
			selectedValues = selectedValues[0];
		}

		child = selectedValues.firstChild;
		while (child) {
			selectedValues.removeChild(child);
			child = selectedValues.firstChild;
		}


		if (this.hasStore()) {
			var indices = [];
			for (i = 0; i < v.length; i++) {
				var index = this.getStore().findExact('Value', v[i]);
				if (index >= 0) {
					indices.push(index);
				}
			}
			if (indices.length === 0) {
				return this.prompt;
			}

			for (i = 0; i < indices.length; i++) {
				var r = this.getStore().getAt(indices[i]);

				data = selectedValues.ownerDocument.createElement('selectedValue');
				data.appendChild(selectedValues.ownerDocument.createTextNode(r.data.Value));
				selectedValues.appendChild(data);
			}
		}
		else {
			for (i = 0; i < v.length; i++) {
				data = selectedValues.ownerDocument.createElement('selectedValue');
				data.appendChild(selectedValues.ownerDocument.createTextNode(v[i]));
				selectedValues.appendChild(data);
			}
		}


		return this.prompt;
	},

	// protected... do not call outside
	setSelectedValueInControl: function(selectedValues) {
		"use strict";
		return false;
	},

	appendSelectedValue: function(selectedValue){
		"use strict";
		
		if(selectedValue){
			var selectedValues = this.prompt.getElementsByTagName('selectedValues');
			if(selectedValues && selectedValues.length > 0){
				selectedValues = selectedValues[0];
				var data = selectedValues.ownerDocument.createElement('selectedValue');
				data.appendChild(selectedValues.ownerDocument.createTextNode(selectedValue));
				selectedValues.appendChild(data);
			}else{
				this.setSelectedValue(selectedValue);
			}
		}
	},
	
	setSelectedValue: function(selectedValue){
		"use strict";
		if(selectedValue){
			var selectedValues = this.clearAddNewSelectedValuesTag();
			var data = selectedValues.ownerDocument.createElement('selectedValue');
			data.appendChild(selectedValues.ownerDocument.createTextNode(selectedValue));
			selectedValues.appendChild(data);
		}
	},

	//2nd parameter updateXml is used to update the prompt xml
	//because the selectedValue may not be from the prompt xml, eg in addPrompts()
	setSelectedValues: function(selectedValue, updateXml) {
		"use strict";
		var setInControl = this.setSelectedValueInControl(selectedValue);
		if (setInControl === false || (updateXml === true && setInControl == true)) {
			// save in prompt
			if (selectedValue && selectedValue.length > 0) {
				var selectedValues = this.clearAddNewSelectedValuesTag();
				var items = [];
				var child = selectedValue[0].firstChild;
				while (child) {
					if (child.tagName === 'selectedValue') {
						var text = getXmlValue(child);
						if (text) {
							var data = selectedValues.ownerDocument.createElement('selectedValue');
							data.appendChild(selectedValues.ownerDocument.createTextNode(text));
							selectedValues.appendChild(data);
						}
					}
					child = child.nextSibling;
				}
			}
		}
	},

	clearAddNewSelectedValuesTag: function(){
		"use strict";
		var selectedValues = this.clearSelectedValues();
		if (! selectedValues || selectedValues.length === 0) {
			selectedValues = this.prompt.ownerDocument.createElement('selectedValues');
			this.prompt.appendChild(selectedValues);
		}
		return selectedValues;
	},

	clearSelectedValues: function(){
		"use strict";
		var selectedValues = this.prompt.getElementsByTagName('selectedValues');
		if(selectedValues && selectedValues.length > 0){
			selectedValues = selectedValues[0];
			var child = selectedValues.firstChild;
			while (child) {
				selectedValues.removeChild(child);
				child = selectedValues.firstChild;
			}
		}
		return selectedValues;
	},

	createParentContainer: function(layout) {
		"use strict";
		return Ext.create('Ext.panel.Panel', {
			layout: layout? layout : 'hbox',
			border: false,
			cls: 'promptParent'//,
			//collapsedDirection: 'right',
			//collapsible: true,
		});
	},

	resetAll: function(){
		this.resetInlinePrompt();
		this.control && this.control.reset();
		this.clearSelectedValues();
		this.promptsController.applyPromptsImmediately();
	},

	resetInlinePrompt: function(){
	},

	onTypeAhead: function(store, ctrl){
      	store.suspendEvents();
      	store.clearFilter(true);
      	store.resumeEvents();
      	store.filter({
          	property: 'Label',
          	anyMatch: true,
          	value   : ctrl.getValue()
      	});
	}

});

Ext.define('Birst.dashboards.QueryPrompt', {
	extend: 'Birst.dashboards.Prompt',
	type: null,
	promptsController: null,
	maxWidth: -1,
	createOnFill: false,
	alreadyFilled: false,
	promptParent: null,
	executeImmediately: false,
	multiSelect: true,
	create: function(section, pc, executeImmediately) {
		"use strict";

		if (this.control) {
			return;
		}

		this.promptsController = pc;
		this.promptParent = section;
		this.executeImmediately = executeImmediately;

		if (this.isCheckBoxRadio() && !this.alreadyFilled) {
			this.createOnFill = true;
			return;
		}
		var parent = this.createParentContainer(Birst.dashboards.doNP ? 'vbox' : null);

		var selectedValues = this.getSelectedValues();
		var expandIntoButtons = getXmlChildValue(this.prompt, 'ExpandIntoButtons');
		var multiSelectType = getXmlChildValue(this.prompt, 'multiSelectType');
		var isMultiSelect = this.multiSelect = (multiSelectType === 'OR' || multiSelectType === 'AND');
		var store = this.getStore();

		if (expandIntoButtons === 'true') {
			var controlType = 'Ext.form.field.Radio';
			if (isMultiSelect) {
				controlType = 'Ext.form.field.Checkbox';
			}
			var sizingControl = Ext.create(controlType, {});
			sizingControl.on('afterrender', function(me, eOpts){
				var parent = eOpts;
				var textMetric = new Ext.util.TextMetrics(parent.getEl());
				textMetric.bind(me.getEl());
				var multiSelectType = getXmlChildValue(this.prompt, 'multiSelectType');
				var isMultiSelect = (multiSelectType === 'OR' || multiSelectType === 'AND');
				var selectedValues = this.getSelectedValues();
				var store = this.getStore();
				var items = [];
				var widths = [];
				var width = 0;
				var num = store.getCount();
				var start = 0;
				if (isMultiSelect) {
					start = 1;
				}
				for (var i = start; i < num; i++) {
					var item = store.getAt(i);
					var label = item.get('Label');
					var itemWidth = textMetric.getWidth(label) + 30;
					widths.push(itemWidth);
					width = Math.max(width, itemWidth);
					items.push({
						boxLabel: label,
						inputValue: item.get('Value'),
						name: this.parameterName
					});
				}
				if (isMultiSelect) {
					this.type = 'Checkbox';
					this.control = Ext.create('Ext.form.CheckboxGroup', {
						fieldLabel: Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'VisibleName')),
						labelAlign: 'top',
						height: 42,
						columns: widths,
						items: items,
						width: '100%'
					});
				}
				else {
					this.type = 'Radio';
					this.control = Ext.create('Ext.form.RadioGroup', {
						fieldLabel: Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'VisibleName')),
						labelAlign: 'top',
						columns: widths,
						shrinkWrap: false,
						height: 42,
						width: '100%',
						items: items
					});
				}

				parent.setWidth(width * num);
				parent.setHeight(42);

				parent.add(this.control);
				parent.remove(me);
				parent.doLayout();

				if (executeImmediately === true) {
					this.control.on('change', this.promptsController.applyPromptsImmediately, this.promptsController);
				}
				if (this.parentPromptListenerFn) {
					this.control.on('change', function(group, newValue, oldValue, eOpts) {
						if (this.parentPromptListenerFn) {
							this.setSelectedValuesByArray(this.getSelectedValuesArray());
							this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
						}
					}, this);
				}

				if (selectedValues) {
					this.setSelectedValueInControl(selectedValues.getElementsByTagName('selectedValues'));
				}

			}, this, parent);
			parent.add(sizingControl);
		} else {
			if (isMultiSelect) {
				if(Birst.dashboards.doNP && !executeImmediately){
					this.createControlAsCheckBox(executeImmediately, parent, false, isMultiSelect);
				} else {
					this.createGridPanel(executeImmediately);
				}
			} else {
				if(Birst.dashboards.doNP && !executeImmediately){
					this.createControlAsCheckBox(executeImmediately, parent, false, isMultiSelect);
				} else {
					this.createComboBox(executeImmediately);
				}
			}
		}

		if (selectedValues) {
			this.setSelectedValueInControl(selectedValues.getElementsByTagName('selectedValues'));
		}

		if(!Birst.dashboards.doNP || executeImmediately){
			parent.add(this.control);
		}
		section.add(parent);
	},

	createInCard: function(section){
		var parent = this.createParentContainer('vbox');
		var multiSelectType = getXmlChildValue(this.prompt, 'multiSelectType');
		var isMultiSelect = (multiSelectType === 'OR' || multiSelectType === 'AND');
		if (isMultiSelect) {
			this.createControlAsCheckBox(false, parent, true, isMultiSelect);
		}else {
			if(Birst.dashboards.doNP){
				this.createControlAsCheckBox(false, parent, true, isMultiSelect);
			}else{
				this.createComboBox(executeImmediately);
			}
		}
		section.add(parent);

		return true;
	},

	applyInCard: function(){
		if(this.cardCtrl){
			if(this.control){
				this.beforeShow && this.beforeShow(null, null, true);
				this.toggleSelectedCheckBoxes && this.toggleSelectedCheckBoxes(null, null, false);
			}else{

			}
		}
		return true;
	},

	//Create a multi select grid panel
	createGridPanel: function(executeImmediately){
		this.type = 'Grid';

		var store = this.getStore();

		this.control = Ext.create('Ext.grid.Panel', {
		    store: store,
		    columns: [ { text: Birst.dashboards.doNP && !executeImmediately ? '' : Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'VisibleName')),
				dataIndex: 'Label',
				layout: {
				    type: 'vbox',
                    align: 'stretch'
				},
				margin: 1,
				flex: 1,
				border: false,
				resizable: false,
				sortable: false,
				menuDisabled: true}],
			layout: 'fit',
			height: Birst.dashboards.doNP ? 150 : '100%',
			border: false,
			maxHeight: 150,
			width: executeImmediately ? '100%' : this.maxWidth > 0 ? this.maxWidth : 150,
			padding: 11,
			frameHeader: false,
			cls: executeImmediately ? 'prompt-embedded' : '',
			hidden: this.isInvisible,
			columnLines: false,
			listeners: {
				scope: this,
				afterrender: function(){
					this.addToolTip();
				}
			}
		});

		this.control.getSelectionModel().setSelectionMode('MULTI');
		this.control.getSelectionModel().bindStore(store);
		if (executeImmediately === true) {
			this.control.on('itemclick', this.promptsController.applyPromptsImmediately, this.promptsController);
		}
		if (this.parentPromptListenerFn) {
			this.control.on('itemclick', function(grid, record, item, index, e, eOpts) {
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);
		}
	},

	//Based on the store values, generate the array list of checkboxes
	getCheckBoxRadioItems: function(isMultiSelect, isPromptCard){
		var items = [];
	    var store = this.getStore();
	    var num = store.getCount();
		var start = 1;
		var itemName = (!isMultiSelect && isPromptCard) ? this.parameterName + 'Card' : this.parameterName;

		for (var i = start; i < num; i++) {
			var item = store.getAt(i);
			var label = item.get('Label');
			items.push({
				boxLabel: label,
				inputValue: item.get('Value'),
				name: itemName,
				cls: 'chkbox',
				width: '100%'
			});
		}

		return items;
	},
	
	//The main driver func for creating the prompt as checkboxes instead of a
	//list.
	createControlAsCheckBox: function(executeImmediately, parent, isPromptCard, isMultiSelect){
		this.type = 'Checkbox';
		var promptCardCtrl = isPromptCard === true ? {} : null;

		var autoComp = this.createAutoCompleteComboBox(parent, isPromptCard);
		var dataItems = this.getCheckBoxRadioItems(isMultiSelect, isPromptCard);
		var container = this.createCheckBoxGroupContainer(parent, dataItems, isMultiSelect, isPromptCard);

		if(promptCardCtrl){
			this.cardCtrl = container.list;
			promptCardCtrl.control = container.list;
			promptCardCtrl.selected = container.selected;
		}

		//Set the selected values, event listeners for parent prompt changes &
		//applying prompt immediately when in dashlet
		var isValueSet = false;
		var selectedValues = this.getSelectedValues();
		if (selectedValues) {
			isValueSet = this.setSelectedValueInControl(selectedValues.getElementsByTagName('selectedValues'));
		}

		if (executeImmediately === true) {
			this.control.on('change', this.promptsController.applyPromptsImmediately, this.promptsController);
		}

		if (this.parentPromptListenerFn) {
			this.control.on('change', function(group, newValue, oldValue, eOpts) {
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);
		}
		
		if(this.isChild){
			//Called when the checkboxes / radio buttons need to be refreshed
			//as the child of a parent prompt
			this.checkBoxControlRefresh = function(){
				autoComp.store = null;
				parent.removeAll();
				
				this.control = null;
				this.toggleSelectedCheckBoxes = false;
				this.createControlAsCheckBox(executeImmediately, parent, isPromptCard, isMultiSelect);
			}
		}

		//When the panel is shown, the selected section will show the selected checkboxes.
		//The selected checkboxes in the main checkboxes area will be invisible. Unchecking the selected checkboxes
		//will remove it and set the corresponding main checkbox visible
		var selectedCtrlGroup = container.selected;
		if(selectedCtrlGroup){
			var removeText = container.removeText;
			removeText.on('click', this.removeAllCtrlGroup(selectedCtrlGroup, removeText, isPromptCard));

			promptCardCtrl && (this.beforeShow = this.toggleSelectedCheckBoxesFn(this.control, selectedCtrlGroup, removeText, isMultiSelect));
			if(this.control){
				!this.toggleSelectedCheckBoxes && (this.toggleSelectedCheckBoxes = this.toggleSelectedCheckBoxesFn(this.control, selectedCtrlGroup, removeText, isMultiSelect));
				//In which case value(s) were set, render the selected checkboxes/radio buttons
				if(isValueSet){
					this.renderSelectedControlGroup(selectedCtrlGroup, removeText, this.control.getChecked());
				}
			}

			//Once the user selects and item from the autocomplete search box, add it to the list
			if(promptCardCtrl){
				autoComp.on('select', this.autoCompleteSelectFn(promptCardCtrl.control, selectedCtrlGroup, removeText));
			}else{
				autoComp.on('select', this.autoCompleteSelectFn(this.control, selectedCtrlGroup, removeText));
			}
		}

		//Tooltip
	    var that = this;
	    parent.on('afterrender', function(){ that.addToolTip(parent); });
	},

	removeAllCtrlGroup: function(selectedCtrlGroup, removeText, isCard){
		return function(){
			var boxes = selectedCtrlGroup.getChecked();
			for(var i = 0; i < boxes.length; i++){
				var realBox = boxes[i].sBoxPtr;
				realBox.setValue(false);
				realBox.setVisible(true);
			}
			selectedCtrlGroup.removeAll();
			selectedCtrlGroup.setVisible(false);
			removeText.setVisible(false);
			if(isCard == true){
				this.resetInlinePrompt();
			}
		}.bind(this);
	},

	autoCompleteSelectFn: function(control, selectedCtrlGroup, removeText){
		return function(combo, records, eOpts ){
			selectedCtrlGroup.setVisible(true);
			removeText.setVisible(true);
			for(var i = 0; i < records.length; i++){
				var r = records[i];
				var idx = r.raw.Index - 1;
				var val = control.items.getAt(idx);
				val.setValue(true);
				this.addCheckBoxField(val, selectedCtrlGroup);
			}
			combo.clearValue();
			this.getStore().clearFilter();
		}.bind(this);
	},

	//Based on the parameters passed in and whether this.cardCtrl (prompt card dialog ) or this.control (prompt in drawer), we try to sync
	//both ui controls.
	toggleSelectedCheckBoxesFn: function(control, selectedCtrlGroup, removeText, isMultiSelect){
		return function(panel, eOpts, useCardCtrl){

				var selectedBoxes = null;

				//User clicks apply button in prompt card
				if(useCardCtrl === true && this.cardCtrl){
					var values = [];
					var valsObj = {};
					valsObj[this.parameterName] = values;

					selectedBoxes = this.cardCtrl.getChecked();
					if(isMultiSelect){
						for(var i = 0; i < selectedBoxes.length; i++){
							values.push(selectedBoxes[i].inputValue);
						}

						var selectedVals = selectedCtrlGroup.getChecked();
						for(var i = 0; i < selectedVals.length; i++){
							values.push(selectedVals[i].inputValue);
						}
					}else{
						if(selectedBoxes.length > 0){
							//values.push(selectedBoxes[0].inputValue);
							valsObj[this.parameterName] = selectedBoxes[0].inputValue;
						}
					}
					try{
						this.control.setValue(valsObj);
					}catch(e){
						//Hmm... why can't we do this extjs? concurrency? timing?
						//console.log(e);
					}
				}else{
					selectedBoxes = [];
					if(this.cardCtrl){
						if(this.control){
							if(panel){ // User using prompt card
								selectedBoxes = this.control.getChecked();
								var values = [];
								var valsObj = {};
								var id = isMultiSelect ? this.parameterName : this.parameterName + 'Card';
								valsObj[id] = values;
								for(var i = 0; i < selectedBoxes.length; i++){
									values.push(selectedBoxes[i].inputValue);
								}
								this.cardCtrl.setValue(valsObj);
								selectedBoxes = this.cardCtrl.getChecked();
								for(var y = 0; y < selectedBoxes.length; y++){
									selectedBoxes[y].setVisible(false);
								}
							}else{
								if(this.control){
									selectedBoxes = this.control.getChecked();
								}
							}
						}else {
							//User didn't open prompt drawer, only displayed the prompt card
							var selectedValues = this.getSelectedValues();
							if(selectedValues){
								this.setSelectedValueInControl(selectedValues.getElementsByTagName('selectedValues'), this.cardCtrl);
							}
							selectedBoxes = this.cardCtrl.getChecked();
						}
					}else{
						//User just using the prompt drawer, not prompt card
						if(this.control){
							selectedBoxes = this.control.getChecked();
						}
					}
				}

				this.renderSelectedControlGroup(selectedCtrlGroup, removeText, selectedBoxes);
		}.bind(this);
	},

	//Render the selected ('Remove all filter values... [x] A, [x] B') checkboxes / radio buttons based on
	//the # of selectedBoxes
	renderSelectedControlGroup: function(selectedCtrlGroup, removeText, selectedBoxes){
		selectedCtrlGroup.removeAll();

		var visible = selectedBoxes.length > 0;
		selectedCtrlGroup.setVisible(visible);
		removeText.setVisible(visible);

		for(var i = 0; selectedBoxes && i < selectedBoxes.length; i++){
			var val = selectedBoxes[i];
			this.addCheckBoxField(val, selectedCtrlGroup);
		}
	},

	addCheckBoxField: function(val, selectedCtrlGroup){
		var ctrlGroupType = selectedCtrlGroup.getXType();
		var fieldType = ctrlGroupType === 'checkboxgroup' ? 'Ext.form.field.Checkbox' : 'Ext.form.field.Radio';
		var chkFunc = function(val, selectedCtrlGroup){
			if(ctrlGroupType === 'checkboxgroup'){
				//checkbox
				return function(chkbox, newVal, oldVal, eOpts){
					if(newVal == false){
						selectedCtrlGroup.remove(chkbox);
						val.setValue(false);
						val.setVisible(true);
					}
				};
			}else{
				//radio
				return function(chkbox, newVal, oldVal, eOpts){
					if(newVal == false){
						selectedCtrlGroup.remove(chkbox);
						val.setValue(false);
						val.setVisible(true);
						var selected = (selectedCtrlGroup.birstParamName) ? this.cardCtrl.getChecked() : this.control.getChecked();
						if(selected.length > 0){
							var sel = selected[0];
							var v = Ext.create(fieldType, {
								width: '100%',
								cls: 'chkbox',
								boxLabel: sel.boxLabel,
								name: sel.name,
								inputValue: sel.inputValue,
								checked: true,
								sBoxPtr: sel
							});
							v.on('change', chkFunc(sel, selectedCtrlGroup).bind(this));
							selectedCtrlGroup.add(v);
							sel.setVisible(false);
						}
					}
				};
			}
		};

		val.setVisible(false);
		var chkBox = Ext.create(fieldType, {
			width: '100%',
			cls: 'chkbox',
			boxLabel: val.boxLabel,
			name: val.name,
			inputValue:  val.inputValue,
			checked: true,
			sBoxPtr: val
		});
		chkBox.on('change', chkFunc(val, selectedCtrlGroup).bind(this));
		selectedCtrlGroup.add(chkBox);
	},

	//Create the prompt control as a combo box (drop down) single select
	createComboBox: function(executeImmediately){
		this.type = 'ComboBox';
		this.control = Ext.create('Ext.form.ComboBox', {
			fieldLabel: Birst.Utils.Safe.getEscapedValue(getXmlChildValue(this.prompt, 'VisibleName')),
			valueField: 'Value',
			displayField: 'Label',
			store: this.getStore(),
			queryMode: 'local',
			labelAlign: 'top',
			typeAhead: true,
			forceSelection: true,
			editable: false,
			padding: 10,
			hidden: this.isInvisible,
			width: executeImmediately ? '100%' : this.maxWidth > 0 ? this.maxWidth - 20 : 140,
			listConfig: {
				cls: 'prompt-list'
			},
			listeners: {
				scope: this,
				afterrender: function(){
					this.addToolTip();
				}
			}
		});

		if (executeImmediately === true) {
			this.control.on('select', this.promptsController.applyPromptsImmediately, this.promptsController);
		}
		if (this.parentPromptListenerFn) {
			this.control.on('select', function(grid, record, item, index, e, eOpts) {
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);
		}
	},

	createCheckBoxGroupContainer: function(parent, items, isMultiSelect, isPromptCard){
		if(!Birst.dashboards.doNP){
			return;
		}

		var groupType = isMultiSelect === false ? 'Ext.form.RadioGroup' : 'Ext.form.CheckboxGroup';

		var removeText = Ext.create('Ext.button.Button', {
			text: Birst.LM.Locale.get('FLT_REMOVEALL'),
			margin: '5 0 0 0',
			padding: 0,
			hidden: true,
			cls: 'chkRemoveBttn',
			overCls: 'chkRemoveBttnOver'
		});

		//Selected area
		var selectedCtrl = Ext.create(groupType, {
			vertical: true,
			columns: 1,
			width: '100%',
			hidden: true,
			cls: 'selectedChkBox'
		});

		if(isPromptCard === true){
			selectedCtrl.birstParamName = this.parameterName + 'Card';
		}

		//List of values area
		var listCtrl = Ext.create(groupType, {
			vertical: true,
			columns: 1,
			items: items,
			cls: 'promptChkBox',
			width: '100%'
		});

		if(!this.control && !isPromptCard){
			//This is the control in the inlined section
			this.control = listCtrl;
			this.selectedCtrl = selectedCtrl;
		}

		//Enclosing container of previous two.
		var container = Ext.create('Ext.container.Container', {
			overflowY: 'auto',
			items:[removeText, selectedCtrl, listCtrl],
			maxHeight: 255,
			width: '100%', //this.maxWidth < 230 ? 230 : this.maxWidth,
			padding: '0 0 5 8',
			margin: 0
		});

		parent.add(container);

		return {selected: selectedCtrl, removeText: removeText, list: listCtrl, container: container};

	},

	createAutoCompleteComboBox: function(parent, isPromptCard){
		if(!Birst.dashboards.doNP){
			return;
		}

		var searchIcon = Ext.create('Ext.Component', {
			html: '', //'<span class="entypoSearch">&#128269;</span>',
			height: '100%',
			padding: '0 0 0 0',
			cls: 'entypoSearchCt'
		});

		var that = this;
		var searchBox = Ext.create('Ext.form.ComboBox', {
			hideTrigger: true,
			queryMode: 'local',
    		displayField: 'Label',
    		valueField: 'Index',
    		emptyText: 'Search',
    		emptyCls: 'promptSearch',
    		store: this.getStore(),
    		width: '100%',
    		padding: 0,
    		margin: 0,
    		border: 0,
    		typeAhead: true,
    		onTypeAhead: function() {
				that.onTypeAhead(this.store, this);
		    }
		});

		var container = Ext.create('Ext.container.Container', {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items:[searchIcon, searchBox],
			width: '100%', //this.maxWidth < 230 ? 230 : this.maxWidth,
			height: 30,
			cls: 'promptSearchBox',
			border: 1,
			padding: 0,
			margin: 0
		});

		parent.add(container);
		return searchBox;
	},

	resetAll: function(){
		this.resetInlinePrompt();
		this.control && this.control.reset();
		this.clearSelectedValues();
		this.promptsController.applyPromptsImmediately();
	},

	//Clear out all selected checkboxes in the inlined prompt
	resetInlinePrompt: function(){
		if(Birst.dashboards.doNP){
			if(this.control){
				var selecteds = this.control.getChecked();
				for(var i = 0; i < selecteds.length; i++){
					selecteds[i].setVisible(true);
					if(!this.multiSelect){
						selecteds[i].setValue(false);
					}
				}
				this.control.setValue && this.control.setValue([]);
			}

			//Radio
			if(!this.multiSelect && this.cardCtrl){
				var cardSels = this.cardCtrl.getChecked();
				if(cardSels.length > 0){
					cardSels[0].setValue(false);
				}
			}
			this.toggleSelectedCheckBoxes && this.toggleSelectedCheckBoxes();
		}
	},

	clearStore: function() {
		this.callParent();
		if (this.type && this.control) {
			if (this.type === 'ComboBox') {
				this.control.setValue('NO_FILTER');
			}
			else if (this.type === 'Grid'){
				this.control.getSelectionModel().deselectAll(true);
			}
		}
	},

	addParentPromptListener: function(fn, scope, options) {
		this.callParent([fn, scope, options]);
		if (this.control) {
			if (this.type === 'ComboBox') {
				this.control.on('select', function(combo, records, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
			}
			else if (this.type === 'Grid') {
				this.control.on('itemclick', function(grid, record, item, index, e, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
			}
			else if (this.type === 'Radio' || this.type === 'Checkbox') {
				this.control.on('change', function(group, newValue, oldValue, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
			}
		}
	},

	setSelectedValueInControl: function(selectedValues, uiControl) {
		"use strict";

		var ctrl = uiControl ? uiControl : this.control;

		if (ctrl && selectedValues && selectedValues.length > 0) {
			var items = [];
			var child = selectedValues[0].firstChild;
			while (child) {
				if (child.tagName === 'selectedValue') {
					var text = getXmlValue(child);
					if (text) {
						var index = this.getStore().findExact('Value', text);
						if (index >= 0) {
							items.push(this.getStore().getAt(index));
						}
					}
				}
				child = child.nextSibling;
			}
			if (this.type === 'ComboBox') {
				ctrl.select(items);
			}
			else if (this.type === 'Grid') {
				var selectionModel = ctrl.getSelectionModel();
				selectionModel.select(items);
			}
			else if (this.type === 'Checkbox' || this.type === 'Radio') {
				var o = {};
				var data = [];
				for (var i = 0; i < items.length; i++) {
					data.push(items[i].get('Value'));
				}
				o[this.parameterName] = data;
				ctrl.setValue(o);
			}
			return true;
		}
		else if (ctrl) {
			ctrl.select(this.getStore().getAt(0));
		}
		return false;
	},

	setFillingComplete: function() {
		this.callParent();
		this.alreadyFilled = true;

		if (this.createOnFill) {
			this.create(this.promptParent, this.promptsController, this.executeImmediately);
		}

		if (this.control) {
			var selectedValues = this.getSelectedValues();
			if (selectedValues) {
				this.setSelectedValueInControl(selectedValues.getElementsByTagName('selectedValues'));
			}
		}
	},

	getSelectedValuesArray: function(){
		if(this.control){
			var value = this.getSelectedValue();
			return (this.type === 'ComboBox') ? [value] : value;
		}else{
			return this.callParent();
		}
	},

	getSelectedValue: function() {
		"use strict";
		var ctrl = (this.control) ? this.control : this.cardCtrl;
		if (ctrl) {
			var arr = [];
			if (this.type === 'ComboBox') {
				arr.push(ctrl.getValue());
				return arr;
			}
			else if (this.type === 'Grid') {
				var models = ctrl.getSelectionModel().getSelection();
				for (var i = 0; i < models.length; i++) {
					var data = models[i].getData(false);
					arr.push(data.Value);
				}
				return arr;
			}
			else if (this.type === 'Checkbox' || this.type === 'Radio') {
				var o = ctrl.getValue();
				if (o) {
					var ret = o[this.parameterName];
					if (ret instanceof Array) {
						return ret;
					}
					return (ret == undefined) ? ['NO_FILTER'] : [ret];
				}
			}

		}
	},

	hasStore: function(){
		return !Birst.dashboards.doNP;
	}
});

Ext.define('Birst.dashboards.ListPrompt', {
	extend: 'Birst.dashboards.QueryPrompt',

	constructor: function(prompt) {
		this.callParent([prompt]);

		var metrics = new Ext.util.TextMetrics(Birst.dashboards.globalMeasureGrid);
		this.maxWidth = metrics.getWidth(this.visibleName) + 65;

		var listValue = this.prompt.getElementsByTagName('ListValue');
		if (listValue && listValue.length > 0) {
			listValue = listValue[0];
			while (listValue) {
				var label = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(listValue, 'Label'));
				var width = metrics.getWidth(label) + 65;
				if(width > this.maxWidth){
					this.maxWidth = width;
				}
				this.addToStore({Label: label, Value: getXmlChildValue(listValue, 'Value')});
				listValue = listValue.nextSibling;
			}
		}
		this.alreadyFilled = true;
	}

});


Ext.define('Birst.dashboards.ValuePrompt', {
	extend: 'Birst.dashboards.Prompt',
	isDate: false,
	re: /[0-9]{4}\-[0-9]{2}\-[0-9]{2}/,
	promptsController: null,
	clazz: null,
	cardCtrl: null,
	constructor: function(prompt) {
		this.callParent([prompt]);

		this.clazz = getXmlChildValue(this.prompt, 'Class');
		this.isDate = this.clazz === 'Date' || this.clazz === 'DateTime';

		//Meaurement of width of prompt based on the prompt title
		var metrics = new Ext.util.TextMetrics(Birst.dashboards.globalMeasureGrid);
		this.maxWidth = metrics.getWidth(this.visibleName) + 65;
		this.maxWidth = this.maxWidth < 150 ? 150 : this.maxWidth;
	},

	create: function(section, pc, executeImmediately) {
		"use strict";
		if (this.control) {
			return;
		}

		this.promptsController = pc;

		var defVal = this.getDefaultOption();

	    if (this.isDate) {
	    	this.control = this.createDateTimeControl(executeImmediately);

	    	//If we're embedded, apply prompts on user pressing ENTER
			if (executeImmediately === true) {
				this.control.on('select', this.promptsController.applyPromptsImmediately, this.promptsController);

				var us = this;
				this.control.on('keypress', function(that, e, eOpts){
					if(e.getKey() == e.ENTER){
						e.preventDefault();
						us.promptsController.applyPromptsImmediately();
					}
				});
			}

			if (this.parentPromptListenerFn) {
				this.control.on('select', function(datePicker, date, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
				this.control.on('change', function(ctrl, newValue, oldValue, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
			}

			if (defVal){
 				this.control.setValue(this.displayDate(defVal));
			}
			else {
				this.setControlWithSelectedValue(this.control);
			}
	    }
	    else {
			this.control =  Birst.dashboards.doNP ?
								this.createAutoCompleteTextField(executeImmediately) : this.createTextFieldControl(executeImmediately);

			//If we're embedded, apply prompts on user pressing ENTER
			if (executeImmediately === true) {
				var us = this;
				this.control.on('keypress', function(that, e, eOpts){
					if(e.getKey() == e.ENTER){
						e.preventDefault();
						us.promptsController.applyPromptsImmediately();
					}
				});

				//User selects one of the autocomplete options
				if(Birst.dashboards.doNP){
					this.control.on('select', function(that, e, eOpts){
						us.promptsController.applyPromptsImmediately();
					});
				}
			}

			if (this.parentPromptListenerFn) {
				this.control.on('change', function(combo, records, eOpts) {
					if (this.parentPromptListenerFn) {
						this.setSelectedValuesByArray(this.getSelectedValuesArray());
						this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
					}
				}, this);
			}

			if (defVal){
				this.control.setValue(defVal);
			}
			else {
				this.setControlWithSelectedValue(this.control);
		   }
		}
		var parent = this.createParentContainer();
		parent.add(this.control);
		section.add(parent);
	},

	createDateTimeControl: function(executeImmediately, inCard){
		var ctrl = Ext.create('Ext.form.field.Date', {
					fieldLabel: Birst.dashboards.doNP ? '' : this.visibleName,
					labelAlign: 'top',
		    		width: Birst.dashboards.doNP ? inCard ? 210 : '100%' : this.maxWidth - 3,
		    		format: 'm/d/Y',
		    		disabled: this.isReadOnly(),
		    		padding: Birst.dashboards.doNP ? 10 : 2,
		    		hidden: this.isInvisible,
		    		useStrict: false,
		    		enableKeyEvents: executeImmediately,
		    		listeners: {
		    			scope: this,
		    			afterrender: function() {
		    				this.addToolTip();
		    				}
		    			}
					});
		ctrl.altFormats = 'Y-m-d|Y-m-d H:i:s|' + ctrl.altFormats;
		return ctrl;
	},

	createTextFieldControl: function(executeImmediately, inCard){
		var ctrl = Ext.create('Ext.form.field.Text', {
					enableKeyEvents: executeImmediately,
					fieldLabel: Birst.dashboards.doNP ? '' : this.visibleName,
					labelAlign: 'top',
					maxHeight: 50,
					width: Birst.dashboards.doNP ? inCard ? 210 : '95%' : this.maxWidth - 3,
					disabled: this.isReadOnly(),
					hidden: this.isInvisible,
					padding: Birst.dashboards.doNP ? 10 : 2,
					listeners: {
						scope: this,
						afterrender: function(){
							this.addToolTip();
						}
					}
				});
		return ctrl;
	},

	createAutoCompleteTextField: function(executeImmediately, inCard){
		var hasPopulated = false;
		var that = this;
		this.store = Ext.create('Ext.data.ArrayStore', {
													autoDestroy: true,
													idIndex: 0,
													fields: [
														'Label',
														'Value'
													]
												});

		var ctrl = Ext.create('Ext.form.ComboBox', {
			hideTrigger: true,
			queryMode: 'local',
    		displayField: 'Label',
    		valueField: 'Value',
    		store: this.store,
    		width: Birst.dashboards.doNP ? inCard ? 210 : '95%' : this.maxWidth - 3,
    		disabled: this.isReadOnly(),
			hidden: this.isInvisible,
    		padding: Birst.dashboards.doNP ? 10 : 2,
    		labelAlign: 'top',
    		maxHeight: 50,
    		enableKeyEvents: executeImmediately,
    		typeAhead: true,
    		onTypeAhead: function() {
    			if(this.store.count() == 0 && !hasPopulated){
    				hasPopulated = true;
    				that.populateStore();
    			}else{
			      	that.onTypeAhead(this.store, this);
    			}
		    },
		    listeners: {
				scope: this,
				afterrender: function(){
					this.addToolTip();
				}
			}
		});

		return ctrl;
	},

	//Call webservice AdhocWS.getFilledOptions() to populate our store.
	populateStore: function(){
		var promptsStr = '<Prompts>';
		var promptDef = xml2Str(this.prompt);
		promptDef = promptDef.replace('<PromptType>Value', '<PromptType>Query');
		promptsStr += promptDef;
		promptsStr += '</Prompts>';

		var loadFn = function(response, options){
			var promptValuesArray = response.responseXML.getElementsByTagName('PromptValues');
			if(promptValuesArray){
				var child = promptValuesArray[0].firstChild;
				while (child) {
					var promptId = null;
					if (child.nodeType === 1 && child.tagName === 'PromptValue') {
						var grandChild = child.firstChild;
						if (grandChild.tagName === 'PromptID') {
							grandChild = grandChild.nextSibling;
						};
						while (grandChild) {
							if (grandChild.nodeType === 1 && grandChild.tagName === 'Value') {
								var label = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(grandChild, 'Label'));
								var value = getXmlChildValue(grandChild, 'Value');
								var greatGrandChild = grandChild.firstChild;
								if (label && value){
									this.store.add({Label: label, Value: value});
								}
							}
							grandChild = grandChild.nextSibling;
						}
						child = child.nextSibling;
					}
				}
			}
		};
		Birst.core.Webservice.adhocRequest('getFilledOptions', [{key: 'prompts', value: promptsStr}], this, loadFn, null, null, true);
	},

	createInCard: function(panel){
		if(this.cardCtrl){
			return false;
		}

		if (this.isDate){
			this.cardCtrl = this.createDateTimeControl(false, true);
			this.setControlWithSelectedValue(this.cardCtrl);
		}else{
		 	this.cardCtrl = this.createTextFieldControl(false, true);
		 	this.setControlWithSelectedValue(this.cardCtrl);
		}


		panel.add(this.cardCtrl);
		return true;
	},

	//User clicks on apply in prompt card, we set the corresponding value in the embedded control
	applyInCard: function(){
		if(this.cardCtrl){
			var val = this.getSelectedValueByControl(this.cardCtrl);
			if(val.length > 0){
				if(this.isDate){
					this.control.setValue(this.displayDate(val[0]));
				}else{
					this.control.setValue(val);
				}
			}
		}
	},

	setControlWithSelectedValue: function(ctrl){
		var selectedValue = this.getSelectedValues();
		if (selectedValue) {
			var selVal =  getXmlChildValue(selectedValue,'selectedValue')
			if(this.isDate){
				selVal = this.displayDate(selVal);
			}
			if (selVal !== 'NO_FILTER') {
				ctrl.setValue(selVal);
			}
		}
	},

	addParentPromptListener: function(fn, scope, options) {
		this.callParent([fn, scope, options]);
		if (this.control) {
			this.control.on('change', function(combo, records, eOpts) {
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);

			// only for date types
			this.control.on('select', function(datePicker, date, eOpts) {
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);
		}
	},

	hasStore: function() {
		"use strict";
		return false;
	},

	getSelectedValue: function() {
		"use strict";
		return this.getSelectedValueByControl(this.control);
	},

	getSelectedValueByControl: function(ctrl){
		if (ctrl) {
			var arr = [];
			var val = ctrl.getValue();
			if (this.isDate) {
				//val is date
				val = Ext.Date.format(val, 'Y-m-d');
			}
			arr.push(val);
			return arr;
		}
	},

	setSelectedValueInControl: function(selectedValues) {
		"use strict";
		if (this.control && selectedValues && selectedValues.length > 0) {
			var child = selectedValues[0].firstChild;
		    if (child && child.tagName === 'selectedValue') {
				var text = getXmlValue(child);
				if (text) {
						if (this.isDate) {
							this.control.setValue(this.displayDate(text));
						}
						else {
							this.control.setValue(text);
						}
				}
 			}
 			return true;
     	}
		return false;
	},

	displayDate: function(date) {
		// if in format YYYY-mm-dd HH:..., strip off everything after dd
		var result = this.re.exec(date);
		if (result && result.index >= 0) {
			return  date.substr(result.index, 10);
		}
		else {
			return date;
		}

	}
});

Ext.define('Birst.dashboards.SliderPrompt', {
	extend: 'Birst.dashboards.Prompt',
	type: null,
	promptsController: null,
	defaultStartIdx: -1,
	defaultEndIdx: -1,
	_isRange: null,
	_isQuery: null,
	isSlider: true,

	constructor: function(prompt) {
		this.callParent([prompt]);
		this.maxWidth = parseInt(getXmlChildValue(this.prompt, 'SliderWidth')) + 20; //20 for additional buffer BPD-20526
	},

	create: function(section, pc, executeImmediately) {
		"use strict";
		if (this.control) {
			return;
		}

		this.promptsController = pc;

		this.control = this.createSlider(executeImmediately);

    	if (executeImmediately === true) {
    		var obj = this;
			this.control.on('change', function(){
					obj.setSelectedValuesByArray(obj.getSelectedValuesArray());
					obj.promptsController.applyPromptsImmediately();
			}, this.promptsController);
		}else{
			this.control.on('change', function(ctrl, index, thumb) {
				this.setSelectedValuesByArray(this.getSelectedValuesArray());
				if (this.parentPromptListenerFn) {
					this.setSelectedValuesByArray(this.getSelectedValuesArray());
					this.parentPromptListenerFn.call(this.parentPromptListenerScope, this, this.parentPromptListenerOptions);
				}
			}, this);
		}


		if(this.control){
			var parent = this.createParentContainer('vbox');
			if(!Birst.dashboards.doNP){
				var textTitle = getXmlChildValue(this.prompt, 'VisibleName');
				var sliderTitle = Ext.create('Ext.form.Label', { text: textTitle, cls: 'sliderTitle' });
				parent.add(sliderTitle);
			}
			parent.add(this.control);
			section.add(parent);
		}
	},

	createSlider: function(executeImmediately){
		var ctrl = null;

		var defVal = this.getDefaultOption();
	    var isReadOnly = getXmlChildValue(this.prompt, 'ReadOnly');
	    var isDisplayAllLabels = getXmlChildValue(this.prompt, 'SliderDispLabels') === 'All';
	    var width = parseInt(getXmlChildValue(this.prompt, 'SliderWidth'));

	    var store = this.getStore();

	    if(this.isQuery()){
	    	var offset = this.isAddNoSelectionEntry() ? 1 : 0;
	    	var numValues = this.getStore().count() - offset;

	    	//Default config for query slider
	    	var obj = this;
	    	var cfg =  {
	    		width: executeImmediately || Birst.dashboards.doNP ? '100%' : width,
			   	padding: 5,
			   	margin: 5,
			   	increment: 1,
			   	useTips: true,
			   	tipText: function(thumb){
						var idxObj = obj.getStore().getAt(thumb.value);
						return idxObj.data.Value;
			   	},
			   	minValue: 1,
			   	maxValue: numValues
	    	};

	    	if(this.isRangeSlider() && numValues > 1){
	    		var rangeVals = [offset, numValues];
	    		if(this.defaultStartIdx >= 0){
	    			rangeVals[0] = this.defaultStartIdx;
	    		}
	    		if(this.defaultEndIdx >= 0){
	    			rangeVals[1] = this.defaultEndIdx;
	    		}
	    		cfg.values = rangeVals;
	    		ctrl = Ext.create('Ext.slider.Multi', cfg);
	    	}else{
	    		cfg.value = (this.defaultStartIdx >= 0) ? this.defaultStartIdx : 0;
	    		ctrl = Ext.create('Ext.slider.Single', cfg);
	    	}
	    }else{
	    	var defs = this.getDefaultValuesForPointType();

			if(defs.endPt >= defs.startPt){
				var cfg =  {
	    			width: executeImmediately || Birst.dashboards.doNP? '100%' : width,
			   		padding: 5,
			   		margin: 5,
			   		increment: defs.increments,
			   		minValue: defs.min,
			   		maxValue: defs.max
	    		};

	    		if(this.isRangeSlider()){
	    			cfg.values = [defs.startPt, defs.endPt]
	    			ctrl = Ext.create('Ext.slider.Multi', cfg);
	    		}else{
	    			cfg.value = defs.startPt;
	    			ctrl = Ext.create('Ext.slider.Single', cfg);
	    		}
			}else{
				//pop up error?
			}
	    }

	    //Tooltip
	    var that = this;
	    ctrl.on('afterrender', function(){ that.addToolTip(ctrl); });

	    return ctrl;
	},

	createInCard: function(panel){
		if(this.cardCtrl){
			return false;
		}

		var parent = this.createParentContainer('vbox');
		this.cardCtrl = this.createSlider(false);
		parent.add(this.cardCtrl);

		panel.add(parent);
		return true;
	},

	//User clicks on apply in prompt card, we set the corresponding value in the embedded control
	applyInCard: function(){
		if(this.cardCtrl){
			if(this.control){
				this.copyValues(this.cardCtrl, this.control);
			}else{
				this.setSelectedValuesByArray(this.getSelectedValuesArray(this.cardCtrl));
			}
		}
	},

	beforeShow: function(panel, eOpts){
		if(this.cardCtrl && this.control){
			this.copyValues(this.control, this.cardCtrl);
		}
	},

	getDefaultValuesForPointType: function(){
		var values = { startPt: 0, endPt : 0, increments: 0 };

		if(!this.isQuery()){
			var startPt = getXmlChildValue(this.prompt, 'SliderStartPt');
	    	var endPt = getXmlChildValue(this.prompt, 'SliderEndPt');

			//Predefined increments
			var increments = getXmlChildValue(this.prompt, 'SliderIncrement');
			if(!increments || increments == 0){
				increments = 1;
			}

			try{
	    		values.min = values.startPt = parseInt(startPt, 10);
	    		values.max = values.endPt = parseInt(endPt, 10);
	    		values.increments = parseInt(increments, 10);
			}catch(e){
				//need to pop up error?
				values.startPt = values.endPt = values.increment = 0;
			}

			if(this.isRangeSlider()){
				var defStart = this.getDefaultStart();
	   			if(defStart != null && defStart != undefined && defStart >= values.startPt && defStart <= values.endPt){
	    			values.startPt = parseInt(defStart, 10);
	    		}

	    		var defEnd = this.getDefaultEnd();
	    		if(defEnd != null && defEnd != undefined && defEnd >= values.startPt && defEnd <= values.endPt){
	    			values.endPt = parseInt(defEnd, 10);
    			}
	   		}else{
	   			values.startPt = (this.defaultStartIdx >= 0) ? this.defaultStartIdx : values.startPt;
	    	}
		}

		return values;
	},

	isRangeSlider: function(){
		if(this._isRange !== null){
			return this._isRange;
		}

		this._isRange = getXmlChildValue(this.prompt, 'SliderType') === 'Range';
		return this._isRange;
	},

	isQuery: function(){
		if(this._isQuery !== null){
			return this._isQuery;
		}

		this._isQuery = getXmlChildValue(this.prompt, 'SliderEndPts') === 'Query-Based';
		return this._isQuery;
	},

	setDefaultStartIdx: function(idx){
		this.defaultStartIdx = idx;
	},

	setDefaultEndIdx: function(idx){
		this.defaultEndIdx = idx;
	},

	getDefaultStart: function(){
		return this.getDefaultValue('SliderDefaultStart');
	},

	getDefaultEnd: function(){
		return this.getDefaultValue('SliderDefaultEnd');
	},

	getDefaultValue: function(key){
		"use strict";
		var defaultOpt = getXmlChildValue(this.prompt, key);
		if(defaultOpt && defaultOpt.indexOf('V{') !== -1){
			return Birst.core.SessionVars[defaultOpt];
		}
		return defaultOpt;
	},

	getSelectedValuesArray: function(uicontrol){
		var ctrl = (uicontrol) ? uicontrol : this.control;
		if(ctrl){
			var vals = [];
			if(this.isQuery()){
				if(this.isRangeSlider()){
					var idx = ctrl.thumbs[0].value;
					vals.push(this.getStore().getAt(idx).data.Value);
					idx = ctrl.thumbs[1].value;
					vals.push(this.getStore().getAt(idx).data.Value);
				}else{
					var idx = ctrl.thumbs[0].value;
					vals.push(this.getStore().getAt(idx).data.Value);
				}
			}else{
				if(this.isRangeSlider()){
					vals.push(ctrl.thumbs[0].value);
					vals.push(ctrl.thumbs[1].value);
				}else{
					vals.push(ctrl.thumbs[0].value);
				}
			}
			return vals;
		}else{
			return this.callParent();
		}
	},

	copyValues: function(fromSlider, toSlider){
		if(this.isRangeSlider()){
			var vals = [];
			vals.push(fromSlider.thumbs[0].value);
			vals.push(fromSlider.thumbs[1].value);
			toSlider.setValue(vals);
		}else{
			toSlider.setValue(fromSlider.thumbs[0].value);
		}
	},

	hasStore: function(){
		return this.isQuery();
	}
});
