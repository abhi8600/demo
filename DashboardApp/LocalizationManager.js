Ext.ns("Birst.LM");

Ext.define('Birst.LM.Locale', {
    singleton: true,
    resPrefix: null,
    Resources: {},
    TouchGlobalResource: function(TextRes) {
 
    	var txtress = TextRes.split("\n");
	    var _res = {};      
	    try 
	      {
		      txtress.map(function(ln){	    
		    	  if (ln.length == 0) { return;}
		           var res = ln.match(/(\w*)\=(.*)/);
		           if (res) {
		        	   _res[res[1]] = res[2];
		           }
		      });      
	      } 
	      catch(e) 
	      {
	    	  console.log('An exception occurred while processing localization file: ' + e);
	      }
	      
	    // todo: validation?	     
	    this.Resources = _res;
	    return;  
    },
    
    load: function(scope, callback, file){
    	
		var el = document.getElementById('birst-locale-plchldr');
		var locale;
		var fileName;
		var resRoot; //varies if served from flash or html5 page 
		
		if (el) {
		    locale = el.value;
		}
		else {
		    locale = "en_US";
		}
		
		// property file		
        if (!file) {
			Birst.LM.Locale.resPrefix = window.location.pathname.split(/\/|\?|&|=|\./g)[2];				
			if (file) {
				fileName = file;		
			} else {
				fileName = Birst.LM.Locale.resPrefix + 'Properties.txt';
			}
			resRoot = 'resources\/locale';
			var url = resRoot+'/'+locale+'/'+fileName;		
		} else {
			// flash page
			resRoot = '\/html\/resources\/locale';
			fileName = file+'Properties.txt'; // module whose properties file to use  
			var url =  resRoot+'/'+locale+'/'+fileName; 
		}
		this.request(url, 
			function(response){
			    // success
				Birst.LM.Locale.TouchGlobalResource(response.responseText);
				if(callback){
					callback.call(scope);
				}

			}, 
			function(response){
			  // fallback to the default locale			  
			  var url =  resRoot+'/'+'en_US/'+fileName;
			  this.request(url, 
			      function(response){
				  // success
					Birst.LM.Locale.TouchGlobalResource(response.responseText);
					if(callback){
						callback.call(scope);
					}					
				  },
				  function(response){ 	  
					  console.log(response.status === 404 ? 'Localization file for a selected language was not found' : 'Failed loading a localization file: '+ url ); 					  		
 				   });			  
		    });	 
    },
    request:function(url,succFunc, failFunc ){
    	
		Ext.Ajax.request({
		    url: url , 
		    method: 'GET',
		    scope: this,
		    headers: { 'Content-Type': 'text/plain; charset=utf-8' },		 
		    timeout: 50000,
		    success: function (response, options) {		    			         
		    	if (succFunc)
		    		succFunc.call(options.scope, response);
 		    },
		    failure: function (response, options) {		
		    	if (failFunc)
		    		failFunc.call(options.scope, response);
	 		    }
		});    			
    },
    //TODO - this getXX functions need to fall back to en_US values if the locale we're looking for
    //doesn't exist
    get: function(prop){    
    	var value = this.Resources[prop];
    	return  (value) ? value : '';
    },
    getNBS: function(prop){
    	var value = this.Resources[prop];
    	return (value) ? value.replace(/ /g,'&nbsp;') : '';
    }
});

