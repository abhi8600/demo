Ext.define('Dashboard.view.ManageReportSchedules', {
    extend: 'Ext.window.Window',
    alias: 'widget.mngschedules',
    id:'bid-win-mngschedules',
    title: 'REPORT SCHEDULES',
    layout: 'fit',
    modal: true,
    cls: 'xb-modal-window',
    closable:false,
    autoShow: true,
    schedStore:null,
    width: 780,
    maxHeight:520,
    minHeight:520,
    minWidth:460,
    editJobDetails:null,
    isManage: true,
    listeners : {
        afterrender : function(w) {
            var header = w.header;
            header.setHeight(38);
            if (this.isManage) {
                w.setTitle(Birst.LM.Locale.get('SCHEDL_TITLE'));
            } else {
                w.setTitle(Birst.LM.Locale.get('SCHEDL_TITLE_CREATE'));
            }
        }
    },
    config: {
        mode: null // manage (0) create (1)
    },
    constructor: function(config){
        this.mode = config.mode || "manage";
        this.editJobDetails = {};
        if (!(this.mode == "manage")) {
            this.editJobDetails.reportFullName = config.path.replace(/\//g,'\\') +'\\'+config.name;
            this.editJobDetails.name = config.name;
            this.editJobDetails.prompts = config.prompts;
            this.editJobDetails.pagenm =  config.pagenm;
            var pathelemnts = config.path.split('/');
            this.editJobDetails.dashbnm = config.dashbnm;
        }
        this.callParent(arguments);
    },
    initComponent: function() {

         this.isManage = this.getMode() == "manage" ? true : false;
         //Birst.Utils.EventMsg.show('Manage Schedules launched');
         schedStore = Ext.create('Ext.data.ArrayStore', {
            autoDestroy: false,
            idIndex: 0,
            fields: [
                {name: 'ScheduleName'},
                {name: 'ReportName'},
                {name: 'CreatedDate'},
                {name: 'Enabled'},
                {name: 'Run', type: 'bool'},
                {name: 'JobID'},
                {name: 'F'},
                {name: 'M'},
                {name: 'P'},
                {name: 'S'},
                {name: '_changed'}
                ]
        });

        var gridMenu = Ext.create('Ext.menu.Menu', {
            id: 'id-menu-mngschedules',
            cls: 'bx-menu-mngschedules',
            items: [{
              text: 'Edit'
            },{
              text: 'Delete'
            },{
              text: 'Disable'
            },{
              text: 'Run now'
           }]
          });

        var scheduleItems = [];
        if (this.isManage) {
            scheduleItems.push(this.manageViewConfig());
            scheduleItems.push(this.editorViewConfig());
        } else {
            scheduleItems.push(this.editorViewConfig());
        }

        this.items = [{
            xtype: 'panel',
            layout: {
                type: 'card'
            },
            id: 'id-reportschedules-main',
            bodyCls: 'xb-panel-body-default',
            defaults: {
                border: false
            },
            items: scheduleItems
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            ui: 'footer',
            dock: 'bottom',
            height: 60,
            cls: 'xb-docked-base-cl',
            layout: {
                pack: 'center'
            },
            items: ['->',{
                id: 'etbtn-goback',
                cls: 'xb-modwin-gray-btn',
                overCls: 'xb-gray-btn-over',
                minWidth: 80,
                pressedCls: 'xb-footer-btn-press',
                text: Birst.LM.Locale.get('LB_BACK'),
                listeners: {
                    scope: this,
                    afterrender: function (btn) {
                       if (this.isManage) {
                            btn.animate({
                                to: {
                                      opacity: 0
                                },
                                duration: 0});
                       } else {
                           btn.setText('CANCEL');
                       }
                    }
                },
                scope:this,
                handler: function (btn) {
                         if (!this.isManage) {
                             this.destroy();
                             return;
                         }
                         Ext.getCmp('idDoneButton').setDisabled(false);
                         var actvPanel = Ext.getCmp('id-reportschedules-editview');
                         actvPanel.animate({
                                to: {
                                  opacity: 0
                                },
                                duration: 400,
                                listeners: {
                                  afteranimate: function () {
                                            var actvPanel = Ext.getCmp('id-reportschedules-manageview');
                                            actvPanel.animate({
                                                to: {
                                                    opacity: 100
                                                },
                                                duration: 800});
                                            Ext.getCmp('bid-win-mngschedules').header.setTitle('REPORT SCHEDULES');
                                            Ext.getCmp('id-reportschedules-main').getLayout().setActiveItem(0);
                                            Ext.getCmp('rs-edit-btn').setDisabled(true);
                                            Ext.getCmp('rs-run-btn').setDisabled(true);
                                            Ext.getCmp('rs-delete-btn').setDisabled(true);
                                            btn.animate({ //set Visible
                                                to: {
                                                  opacity: 0
                                                },
                                                duration: 400});

                                  }
                                }
                        });
                    },
                hidden:false
            },{
                xtype: 'button',
                minWidth: 80,
                text: Birst.LM.Locale.get('LB_DONE'),
                id:'idDoneButton',
                cls: 'xb-modwin-submit-btn',
                overCls: 'xb-submit-btn-over',
                pressedCls: 'xb-footer-btn-press',
                scope: this,
                handler: function () {
                    if (!this.isManage) {
                        if (!this.validateAndSubmitEditChanges()){
                              return;
                        }
                        else {
                            this.destroy();
                            return;
                        }
                    }
                    var dlgcntx= Ext.getCmp('id-reportschedules-main').getLayout();
                    switch(dlgcntx.activeItem.id)
                    {
                        case "id-reportschedules-manageview":
                            this.submitJobsRunStatus();
                            this.destroy();
                            break;

                        case "id-reportschedules-editview":
                             if (!this.validateAndSubmitEditChanges()){
                                 return;
                             }
                             var actvPanel = Ext.getCmp('id-reportschedules-editview');
                             actvPanel.animate({
                                    to: {
                                      opacity: 0
                                    },
                                    duration: 400,
                                    listeners: {
                                      afteranimate: function () {
                                                var actvPanel = Ext.getCmp('id-reportschedules-manageview');
                                                actvPanel.animate({
                                                    to: {
                                                        opacity: 100
                                                    },
                                                    duration: 800});
                                                Ext.getCmp('id-reportschedules-main').getLayout().setActiveItem(0);
                                                Ext.getCmp('bid-win-mngschedules').header.setTitle('REPORT SCHEDULES');
                                                Ext.getCmp('etbtn-goback').animate({
                                                    to: {
                                                      opacity: 0
                                                    },
                                                    duration: 0});
                                      }
                                    }
                            });

                            break;

                        default:
                            this.destroy();
                    }
                }
             }]
         }];
        this.callParent(arguments);
    },
    tools:[{
        xtype: 'button',
        iconCls: 'closeModalIcon',
        cls: 'xb-modwin-close-btn',
        overCls: 'xb-modwin-close-btn-over',
        tooltip: Birst.LM.Locale.get('LB_CLOSE_TIP'),
        handler: function(ev, te, p){
            this.ownerCt.ownerCt.destroy();
        }
    }],
    manageViewConfig: function () {

      var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

      return {
                xtype: 'panel',
                id: 'id-reportschedules-manageview',
                border: 0,
                maxHeight: 520,
                minHeight: 520,
                bodyPadding: '0 15 15 15',
                hideMode:'offsets',
                fieldDefaults: {
                    anchor: '100%',
                    msgTarget: 'under'

                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                listeners: {
                    scope: this,
                    activate: function(me) {
                        Ext.getCmp('rs-edit-btn').setDisabled(true);
                        Ext.getCmp('rs-run-btn').setDisabled(true);
                        Ext.getCmp('rs-delete-btn').setDisabled(true);
                        Birst.core.Webservice.schedulerRequest('getAllJobs',
                        [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                          {key: 'type', value: "Report"},
                          {key: 'onlyCurrentExecuting', value: "false"},
                          {key: 'userID', value: Birst.Utils.User.userId},
                          {key: 'filterOnUser', value: "true"}
                        ],
                        this,
                        function(response) {
                            schedStore.clearData();
                            Ext.getCmp('id-schedules-grid').getView().refresh();
                            var jobs = response.responseXML.getElementsByTagName('ScheduledJobList');
                            var versionNode = response.responseXML.getElementsByTagName('PrimarySchedulerName');
                            var version;
                            if (versionNode && versionNode[0]) {
                                version = getXmlValue(versionNode[0]);
                            }
                            this.editJobDetails.release = version;
                            if (jobs) {
                                var job = jobs[0].firstChild;
                                while (job != null) {
                                    if (job.nodeName !== "ScheduledJob") {
                                        job = job.nextSibling;
                                        continue;
                                    }
                                    var name = getXmlChildValue(job, 'Name');
                                    var report = getXmlChildValue(job, 'Summary');
                                    var date = getXmlChildValue(job, 'CreatedDate');
                                    var jid = getXmlChildValue(job, 'Id');
                                    var run = getXmlChildValue(job, 'Run');
                                    var migration = getXmlChildValue(job, 'SchedulerMigration');
                                    var primary = getXmlChildValue(job, 'PrimaryScheduler');
                                    var scheduler = getXmlChildValue(job, 'SchedulerDisplayName');

                                    var cronXml = getXmlChildValue(job.getElementsByTagName('Schedule')[0],'Cron');
                                    var cron = this.decodeCron(cronXml);

                                    var frequencyTxt ='';
                                    switch(cron.mode)
                                    {
                                        case 0:  // Daily
                                            frequencyTxt = "Daily";
                                            break;

                                        case 1:  // Weekly
                                            cron.data.forEach(function(i) {
                                                var dowchkbxCtl = Ext.getCmp("day"+i+"ChkbxId");
                                                var dayVal =  dowchkbxCtl.boxLabel.valueOf();
                                                var dayAbrv = dayVal == "Thursday" ? dayVal.substring(0,2) : dayVal.substring(0,3);
                                                frequencyTxt = frequencyTxt.length == 0 ? frequencyTxt + dayAbrv : frequencyTxt+', ' + dayAbrv;
                                            });
                                            break;

                                        case 2:  // Monthly

                                            var day = cron.data[0] != 'L' ? this.ordinaryNumberSfx(cron.data[0]) : "Last Day";
                                            frequencyTxt = day + " of Month";
                                            break;
                                        default:

                                    }
                                    if (cron.ampm == "am" ) {
                                        frequencyTxt = frequencyTxt + " at " + cron.hour +":"+cron.min+"am";
                                    } else {
                                        frequencyTxt = frequencyTxt + " at " + cron.hour +":"+cron.min+"pm";
                                    }
                                    schedStore.add({ ScheduleName: name, ReportName: report, CreatedDate: date, JobID: jid, Run: run, _changed: false, F: frequencyTxt, P: primary, M: migration, S: scheduler });
                                    job = job.nextSibling;
                                }
                            }
                        },
                        function() {
                            alert('failure!');
                        },null,true);
                    }
                },
                items: [
                     Ext.create('Ext.grid.Panel', {
                     store: schedStore,
                     id: 'id-schedules-grid',
                     hideHeaders: false,
                     //height: 360,
                     maxHeight: 420,
                     autoScroll: true,
                     layout: 'fit',
                     bodyPadding: '0 0 15 0 ',
                     scroll: true,
                     multiSelect: true,
                     cls: 'x-birst-grid',
                     columns: [
                                { header: 'Name', dataIndex: 'ScheduleName', sortable: true, flex:0.2, menuDisabled:true,
                                            renderer : function(value, metadata, record) {
                                                metadata.tdAttr = 'data-qtip="' + value + '"';
                                                return value;
                                           }
                                },
                                { header: 'Report Name', dataIndex: 'ReportName', sortable: true, flex:0.25, menuDisabled:true,
                                            renderer : function(value, metadata, record) {
                                                 record.get('ScheduleName');
                                                 var valueAbrv = value.replace("V{CatalogDir}","").split("\\");
                                                 valueAbrv = valueAbrv[valueAbrv.length-1];
                                                 metadata.tdAttr = 'data-qtip="'+value.replace("V{CatalogDir}","")+'"';
                                                 return valueAbrv;
                                            }
                                },
                                { header: 'Frequency', dataIndex: 'F', sortable: true, flex:0.25, menuDisabled:true,
                                            renderer : function(value, metadata, record) {
                                                 metadata.tdAttr = 'data-qtip="' + value + '"';
                                                 return value;
                                            }
                                },
                                { header: 'Created Date', dataIndex: 'CreatedDate', sortable: true, width: 0.25, menuDisabled:true},
                                { header: 'Enabled',
                                           xtype: 'checkcolumn',
                                           dataIndex: 'Run',
                                           width: 55,
                                           stopSelection: false,
                                           menuDisabled:true,
                                           listeners: {
                                                    checkchange: function(column, recordIndex, checked){
                                                        var rec = schedStore.getAt(recordIndex);
                                                        // toogle state Ex
                                                        rec.set('_changed', !rec.get('_changed'));
                                                        //console.log('click columncheck: (recIndx=) (checked=) (changed=) ' + recordIndex + ' '+ checked + ' '+ rec.get('_changed') );
                                                    }
                                           },
                                           renderer: function(value, metadata, record) {
                                                    var primry = record.get('P');
                                                    if (primry == "false") {
                                                        return '';
                                                    }
                                                    else {
                                                        //wdgt's default renderer
                                                        return (new Ext.ux.CheckColumn()).renderer(value);
                                                    }
                                           }
                                  }
                     ],
                     viewConfig: {
                            listeners: {
                                scope: this,
                                itemclick: function (dataview, record, item, index, e) {
                                    var selected = dataview.getSelectionModel().getSelection();
                                    var editBtn = Ext.getCmp('rs-edit-btn');
                                    var runBtn = Ext.getCmp('rs-run-btn');
                                    var deleteBtn = Ext.getCmp('rs-delete-btn');

                                    var isPrimary = true;
                                    var isMigration = false;
                                    this.editJobDetails.restrictedMode = "none";

                                    var ctl = Ext.getCmp('rs-edit-btn');
                                    delete ctl.tooltip;
                                    ctl.tooltip = Ext.create('Ext.tip.ToolTip', {
                                            target: ctl.getEl(),
                                            html: 'Edit'
                                        });
                                    Ext.getCmp('idEditbtnLabel').setText('Edit');

                                    Ext.each(selected, function (item) {
                                        if (item.data.M === 'true') {
                                            isMigration = true;
                                        }
                                        if (item.data.P === 'false') {
                                            isPrimary = false;
                                        }
                                    });

                                    if (selected.length == 1) {
                                        if (isPrimary) {
                                            runBtn.setDisabled(false);
                                            deleteBtn.setDisabled(false);
                                            editBtn.setDisabled(false);
                                        } else if
                                            (!isMigration && !isPrimary )
                                             {
                                                // no migration and is not primary - set editor in r/o mode
                                                editBtn.setDisabled(false);
                                                Ext.getCmp('idEditbtnLabel').setText('View');
                                                var ctl = Ext.getCmp('rs-edit-btn');
                                                delete ctl.tooltip;
                                                ctl.tooltip = Ext.create('Ext.tip.ToolTip', {
                                                    target: ctl.getEl(),
                                                    html: 'View'
                                                });
                                                runBtn.setDisabled(true);
                                                deleteBtn.setDisabled(true);
                                                this.editJobDetails.restrictedMode = "readonly";
                                            }
                                            else if (isMigration && !isPrimary ) {
                                                // upgrade mode
                                                editBtn.setDisabled(false);
                                                runBtn.setDisabled(true);
                                                deleteBtn.setDisabled(true);
                                                this.editJobDetails.restrictedMode = "upgrade";
                                            }
                                            else {
                                                editBtn.setDisabled(true);
                                                runBtn.setDisabled(true);
                                                deleteBtn.setDisabled(true);
                                           }
                                    } else if (selected.length > 0 ) {
                                        editBtn.setDisabled(true);
                                        if (isMigration || isPrimary) {
                                            runBtn.setDisabled(true);
                                            deleteBtn.setDisabled(true);
                                        } else {
                                            runBtn.setDisabled(false);
                                            deleteBtn.setDisabled(false);
                                        }
                                        Ext.getCmp('idEditbtnLabel').setText('Edit');

                                    } else {
                                        editBtn.setDisabled(true);
                                        runBtn.setDisabled(true);
                                        deleteBtn.setDisabled(true);
                                    }
                                },
                                render: function(view) {
                                    /* view.tip = Ext.create('Ext.tip.ToolTip', {
                                        target: view.el,
                                        delegate: view.itemSelector,
                                        trackMouse: true,
                                        html: '',
                                        listeners: {
                                            beforeshow: function updateTipBody(tip) {
                                                var gridColums = view.getGridColumns();
                                                var record = view.getRecord(Ext.get(tip.triggerElement));
                                                var v = view.getRecord(Ext.get(tip.triggerElement)).get('ScheduleName');
                                                tip.update(v);
                                            }
                                        }
                                     }); */
                                }
                            },
                            getRowClass: function(record, index, rowParams, store) {
                                return record.get('Run') === false ? 'disabled' : '';
                            }
                     },
                     listeners: {
                            scope: this,
                            beforeitemcontextmenu: function(view, record, item, index, e) {
                                /*
                                e.stopEvent();
                                gridMenu.showAt(e.getXY());
                                */
                            },

                            beforerender: function (me,eop) {
                                var header = me.header;
                                me.columns[0].setHeight(28);
                            }
                     },
                     dockedItems: [{
                            xtype: 'toolbar',
                            ui: 'footer',
                            dock: 'top',
                            height: 80,
                            cls: 'xb-docked-base-cl',
                            padding: "0 0 10 0",
                            layout: {
                                pack: 'center'
                            },
                            items: ['->', {
                                xtype:'button',
                                text: '&#10150;',
                                cls: 'entypoBttn',
                                id: 'rs-run-btn',
                                disabled: true,
                                padding: "0 10 5 5",
                                border: 0,
                                tooltip: 'Run Now',
                                scope: this,
                                handler: function() {
                                  this.handleRunNow();
                                }
                            },{
                                xtype: 'label',
                                text: Birst.LM.Locale.get('SCHEDL_RUN'),
                                width: 50
                            },{
                                xtype: 'label',
                                text: '',
                                width: 10
                            },
                            {
                                xtype:'button',
                                text: '&#9998;',
                                cls: 'entypoBttn',
                                id: 'rs-edit-btn',
                                disabled: true,
                                padding: "0 10 5 5",
                                border: 0,
                                //tooltip: 'Edit',
                                scope: this,
                                listeners: {
                                     afterrender: function (me) {
                                         me.tooltip =   Ext.create('Ext.tip.ToolTip', {
                                                    target: me.getEl(),
                                                    html: 'Edit'
                                             });
                                    }
                                },
                                handler: function() {
                                     // change to edit view
                                     var panel = Ext.getCmp('id-reportschedules-manageview');
                                     Ext.getCmp('bid-win-mngschedules').header.setTitle('EDIT SCHEDULE');
                                     if (this.editJobDetails.restrictedMode == "readonly") {
                                         Ext.getCmp('idDoneButton').setDisabled(true);
                                     }
                                     panel.animate({
                                            to: {
                                              opacity: 0
                                            },
                                            duration: 400,
                                            listeners: {
                                              afteranimate: function () {
                                                    var actvPanel = Ext.getCmp('id-reportschedules-editview');
                                                    actvPanel.animate({
                                                        to: {
                                                            opacity: 100
                                                        },
                                                        duration: 800});
                                                    Ext.getCmp('id-reportschedules-main').getLayout().setActiveItem(1);
                                                    Ext.getCmp('etbtn-goback').animate({
                                                        to: {
                                                          opacity: 100
                                                        },
                                                        duration: 400});
                                              }
                                            }
                                      });
                                      /*
                                      Ext.get('id-reportschedules-manageview').slideOut('t', {
                                                //easing: 'easeOut',
                                                duration: 2000
                                               });  */
                                      //Ext.getCmp('id-reportschedules-main').getLayout().setActiveItem(1);
                                }
                            },{
                                xtype: 'label',
                                id: 'idEditbtnLabel',
                                text: Birst.LM.Locale.get('SCHEDL_EDIT'),
                                width: 50
                            },{
                                xtype: 'label',
                                text: '',
                                width: 10
                            },{
                                xtype:'button',
                                text: '&#59177;',
                                cls: 'entypoBttn',
                                id: 'rs-delete-btn',
                                disabled: true,
                                padding: "0 10 5 5",
                                border: 0,
                                tooltip: 'Delete',
                                handler: function(me) {
                                      Ext.getCmp('rs-edit-btn').setDisabled(true);
                                      Ext.getCmp('rs-run-btn').setDisabled(true);
                                      me.setDisabled(true);
                                      this.handleDeleteSched();
                                },
                                scope:this

                            },{
                                xtype: 'label',
                                text: Birst.LM.Locale.get('SCHEDL_DELETE'),
                                width: 50
                            }
                        ]
                     }] //docketitems
                 })
            ]// items
        }
    },
    editorViewConfig: function () {
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var comprssStore = Ext.create('Ext.data.Store', {
                fields: ['name', 'value'],
                id: 'etComprssStoreId',
                data: [
                    { "name": "NONE", "value": "None" /*Birst.LM.Locale.get('SCHEDL_COMP_NONE')*/ },
                    { "name": "GZIP", "value": "GZIP"  },
                    { "name": "ZIP", "value": "ZIP" }
                ]
        });

        var comprssCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etComprssCombxId',
                fieldLabel: 'File Compression',
                inputWidth: 80,
                labelSeparator:'',
                //matchFieldWidth: true,
                store: comprssStore,
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 2px 0px 170px',
                baseBodyCls: 'bx-baseBody',
                fieldBodyCls: 'bx-fieldBody',
                listeners: {
                    //scope: me,
                    select: function(cb) {
                        //connModeSelected = cb.value;
                        },
                        afterrender: function(cb){
                        cb.select(comprssStore.getAt(0));
                    }
                },
                listConfig: {
                    maxWidth: 80
                }
        });

        var dayOfMonthCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etDayOfMonthCombxId',
                fieldLabel: 'Day of Month',
                inputWidth: 100,
                matchFieldWidth: true,
                store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'value'],
                        data: []
                }),
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 2px 0px 170px',
                baseBodyCls: 'bx-baseBody',
                fieldBodyCls: 'bx-fieldBody',
                listeners: {
                    //scope: me,
                    select: function(cb) {

                    },
                    afterrender: function(cb){
                        cb.select(cb.getStore().getAt(0));
                    }
                },
                listConfig: {
                    maxWidth:  100,
                    maxHeight: 100
                }
        });

        var domstore = dayOfMonthCombo.getStore();
        for (var i=1; i <= 31; i++) {
            domstore.add({ name: i.toString(), value: i.toString() });
        }
        domstore.add({ name: 'L', value: "Last Day" });

        var timeOfDayHourCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etTimeOfDayHourCombxId',
                fieldLabel: 'Time of Day',
                inputWidth: 100,
                matchFieldWidth: true,
                store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'value'],
                        data: []
                }),
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 2px 0px 170px',
                baseBodyCls: 'bx-baseBody',
                fieldBodyCls: 'bx-fieldBody',
                listeners: {
                    //scope: me,
                    select: function(cb) {

                    },
                    afterrender: function(cb){
                        cb.select(cb.getStore().getAt(0));
                    }
                },
                listConfig: {
                    maxWidth:  100,
                    maxHeight: 100
                }
        });

        var hstore = timeOfDayHourCombo.getStore();
        for (var i=1; i <= 12; i++) {
            hstore.add({ name: i.toString(), value: i.toString() });
        }

        var timeOfDayMinuteCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etTimeOfDayMinuteCombxId',
                fieldLabel: '',
                inputWidth: 100,
                matchFieldWidth: true,
                store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'value'],
                        data: [
                            { "name": "00", "value": "00"},
                            { "name": "15", "value": "15" },
                            { "name": "30", "value": "30" },
                            { "name": "45", "value": "45" }
                        ]
                }),
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 10px 0px 0px',
                listeners: {
                    //scope: me,
                    select: function(cb) {

                    },
                    afterrender: function(cb){
                        cb.select(cb.getStore().getAt(0));
                    }
                },
                listConfig: {
                    maxWidth:  100,
                    maxHeight: 100
                }
        });

        var timeOfDayAmPmCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etTimeOfDayAmPmCombxId',
                fieldLabel: '',
                inputWidth: 100,
                //matchFieldWidth: true,
                store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'value'],
                        data: [
                            { "name": "am", "value": "AM"},
                            { "name": "pm", "value": "PM" }
                        ]
                }),
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 10px 0px 0px',
                listeners: {
                    //scope: me,
                    select: function(cb) {

                    },
                    afterrender: function(cb){
                        cb.select(cb.getStore().getAt(0));
                    }
                },
                listConfig: {
                    maxWidth:  100,
                    maxHeight: 100
                }
        });

        var timeOfDayTZCombo = Ext.create('Ext.form.ComboBox', {
                name: 'rd-sp-class',
                id: 'etTimeOfDayTZCombxId',
                fieldLabel: '',
                inputWidth: 100,
                //matchFieldWidth: true,
                store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'value'],
                        data: [
                            { "name": "CST", "value": "CST"},
                            { "name": "PST", "value": "PST" },
                            { "name": "EST", "value": "EST" },
                            { "name": "MST", "value": "MST" }
                        ]
                }),
                queryMode: 'local',
                displayField: 'value',
                valueField: 'name',
                typeAhead: false,
                editable: false,
                cls: 'columnSelector',
                style: 'margin:0px 10px 0px 0px',
                listeners: {
                    //scope: me,
                    select: function(cb) {

                    },
                    afterrender: function(cb){
                        cb.select(cb.getStore().getAt(0));
                    }
                },
                listConfig: {
                    maxWidth:  100,
                    maxHeight: 100
                }
        });

        return {    // S. E D I T O R //
            xtype: 'panel',
            id: 'id-reportschedules-editview',
            border: 0,
            autoScroll: true,
            maxHeight: '80%',
            minHeight: '80%',
            fieldDefaults: {
                labelWidth: 130,
                anchor: '100%'
            },
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            bodyPadding: '15 15 15 15',
            listeners: {
                scope: this,
                activate: function (me) {
                    if (!this.isManage) {
                        this.populateShceduleEditForm();
                        return;
                    }
                    var sched = Ext.getCmp('id-schedules-grid').getView().getSelectionModel().getSelection().pop();
                    if (sched) {
                            Birst.core.Webservice.schedulerRequest('getJobDetails',
                            [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                              {key: 'jobType', value: "Report"},
                              {key: 'jobID', value: sched.get('JobID')}
                            ],
                            this,
                            function(response) {
                                this.editJobDetails.jobID = sched.get('JobID');
                                this.editJobDetails.spaceID = Birst.Utils.User.spaceId;
                                var sn = Ext.ComponentQuery.query('textfield[name=etSchedNameFld]')[0];
                                sn.setValue(sched.get('ScheduleName'));
                                this.populateShceduleEditForm(response.responseXML);
                            },
                            function() {

                                alert('failure!');

                            },null,true);
                    }
                }//evt activate
            },
            items: [{
                xtype:'fieldset',
                title: 'General',
                defaultType: 'textfield',
                collapsible: true,
                layout: 'anchor',
                ui: 'gray',
                defaults: {
                    anchor: '100%',
                    labelSeparator:'',
                    style: {
                        'margin-bottom':'12px',
                        'padding-left' :'16px'
                    }
                },
                items: [{
                    fieldLabel: 'Schedule Name',
                    afterLabelTextTpl: required,
                    name: 'etSchedNameFld',
                    allowBlank:false,
                    //labelStyle: 'font-size: 12px; color: #656565 !important',
                    width: 130,
                    regex:   /^[^\\.\/<>:|*?"]{1,255}$/,//"
                    regexText:  "Text is limited to 255 characters and the following characters are not allowed"+'.,<,>,:,\",/,\,|,?,*',
                    padding: '6 0 6 0',
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'
                },{
                    fieldLabel: 'Report Path',
                    disabled: true,
                    name: 'etReportPathFld',
                    id: 'etReportPathFldId',
                    padding: '6 0 0 0',
                    style: {
                        'margin-bottom':'6px'
                    },
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'
                },
                {
                    xtype: 'label',
                    disabled: true,
                    text: 'Prompts:',
                    id: 'etPromptFldId',
                    //maxWidth: 60,
                    width: '100%',
                    margin: '0 0 8 130',
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody',
                    style:  {
                        'color': '#B2B2B2',
                        'font-size': '11px',
                        'font-style': 'italic',
                        'font-weight': 'normal',
                        'padding-bottom':'16px'
                    },
                    listeners: {
                        resize: function(me, n, o) {
                             var fldCtl = me;
                             //console.log('tipTxt : '+ fldCtl.tipTxt);
                             var promptsTxt =  fldCtl.tipTxt ? fldCtl.tipTxt : fldCtl.text;
                             if (fldCtl.tip){
                                 fldCtl.tip.destroy();
                             }
                             var measure = Ext.util.TextMetrics.measure('etPromptFldId',promptsTxt);
                             var maxW = fldCtl.ownerCt.getWidth() - 200;
                             if (maxW < measure.width) {
                                var dpx = measure.width - maxW;
                                var len = promptsTxt.length;
                                var achar = promptsTxt.charAt(len/2) ;
                                var charM = Ext.util.TextMetrics.measure('etPromptFldId', achar);
                                var newLen = len - Math.floor(dpx/charM.width) - 24;
                                var promptsTxtTrn = promptsTxt.substring(0,newLen) + "...";
                                fldCtl.setText(promptsTxtTrn);
                                fldCtl.tip = Ext.create('Ext.tip.ToolTip',{
                                     target: 'etPromptFldId',
                                     html: fldCtl.tipTxt
                                });
                            } else {
                                fldCtl.setText(promptsTxt);
                            }
                        }
                    }
                }
                ,{
                    xtype: 'checkboxgroup',
                    fieldLabel: 'Delivery Format',
                    afterLabelTextTpl: required,
                    margin: '18 0 0 0',
                    columns: 4,
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody',
                    baseCls: 'bx-chkgrp-base',
                    formItemCls:'bx-chkgrp-formItem',
                    overCls:'bx-chkgrp-over',

                    items: [
                            {boxLabel: 'InlineHTML', name: 'dlv-format', id: 'htmlChkbxId'},
                            {boxLabel: 'Excel',      name: 'dlv-format', id: 'excelChkbxId'},
                            {boxLabel: 'PDF',        name: 'dlv-format', id: 'pdfChkbxId'},
                            {boxLabel: 'PPT',        name: 'dlv-format', id: 'pptChkbxId'},
                            {boxLabel: 'RTF/Word',   name: 'dlv-format', id: 'rtfChkbxId'},
                            {boxLabel: 'CSV',        name: 'dlv-format', id: 'csvChkbxId',
                                        handler: function(cb,val){
                                            cb.next("textfield[name=delmFldId]").setDisabled(!val);
                                            cb.next("textfield[name=fedngFldId]").setDisabled(!val);
                                        }
                            },
                            {xtype: 'textfield',
                                name: 'delmFldId',
                                fieldLabel: 'Delimiter',
                                labelWidth: 60,
                                fieldCls: 'cvsdelimiter-field',
                                labelSeparator:'',
                                disabled: true
                            },
                            {xtype: 'textfield',
                                name: 'fedngFldId',
                                fieldLabel: 'File ending',
                                labelSeparator:'',
                                labelWidth: 80,
                                fieldCls: 'cvsdelimiter-field',
                                disabled: true
                            }
                    ]
                },{
                    fieldLabel: 'Attachment Name',
                    name: 'etAttchmentNameFld',
                    regex:   /^[^\\.\/<>:|*?"]{1,255}$/,//"
                    regexText:  "Text is limited to 255 characters and the following characters are not allowed"+'.,<,>,:,\",/,\,|,?,*',
                    margin: '16 0 18 0',
                    labelStyle: 'font-weight:bold; width: 108px;',
                    baseBodyCls: Ext.isChrome ? 'bx-baseBodyChr'   : 'bx-baseBody',
                    fieldBodyCls: Ext.isChrome ? 'bx-fieldBodyChr' : 'bx-fieldBody'
                },
                    comprssCombo
                ]
            },{
                xtype:'fieldset',
                title: 'Email',
                collapsible: true,
                collapsed: false,
                defaultType: 'textfield',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelSeparator:'',
                    style: {
                        'margin-bottom':'12px'
                    },
                    listeners: {
                            change: function(field, newVal, oldVal) {
                            }
                    }
                },
                style: {
                  'margin-bottom':'12px',
                  'margin-top':'12px'
                },
                listeners: {
                   beforecollapse:  function(p) {
                       //return false;
                   },
                   beforeexpand: function(p) {
                      //return false;
                    }
                },
                items:[{
                    fieldLabel: 'To',
                    name: 'etEmailToFld',
                    afterLabelTextTpl: required,
                    allowBlank:false,
                    padding: '0 0 6 0',
                    validator: function(v) {
                        var emails = v.split(";");
                        var isValid = true;
                        emails.forEach(function(e) {
                            e = e.trim();
                            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;//"
                            if (!re.test(e)) {
                                  isValid = false;
                                  return false;
                            }
                        });

                        if (isValid) {
                            return true;
                        } else {
                            return  "Email is not valid ";
                        }
                    },
                    listeners: {
                        afterrender: function(me){
                            Ext.create('Ext.tip.ToolTip', { target: me.getEl(), html: "email addresses separated by \';\'"});
                        }
                    },
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'
                },{
                    fieldLabel: 'From',
                    name: 'etEmailFromFld',
                    afterLabelTextTpl: required,
                    allowBlank:false,
                    value: 'delivery@birst.com',
                    padding: '0 0 6 0',
                    vtype:'email',
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'

                },{
                    fieldLabel: 'Subject',
                    name: 'etEmailsubjectFld',
                    value: 'Report From Birst',
                    padding: '0 0 6 0',
                    regex:   /^[^\\.\/<>:|*?"]{1,255}$/,//"
                    regexText:  "Text is limited to 255 characters and the following characters are not allowed"+'.,<,>,:,\",/,\,|,?,*',
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'
                },{
                    xtype: 'htmleditor',
                    name: 'etEmailBodyFld',
                    fieldLabel: 'Body',
                    cls: 'bx-htmleditor',
                    //enableFontSize: true,
                    enableFont: true,
                    height: 220,
                    anchor: '100%',
                    baseBodyCls: 'bx-baseBody',
                    fieldBodyCls: 'bx-fieldBody'
                },
                {
                    xtype: 'checkbox',
                    boxLabel: 'Include a link to this report at the bottom of the email',
                    name: 'dashblinkChkbx',
                    padding: '0 0 6 130',
                    disabled: true,
                    listeners: {
                        scope: this,
                        change: function(c) {
                              var emailBodyCtl = Ext.ComponentQuery.query('htmleditor[name=etEmailBodyFld]')[0];
                              var emailBodyTxt = emailBodyCtl.getValue();
                              var page = "page="+this.editJobDetails.pagenm;
                              var dashboard = "dashboard="+this.editJobDetails.dashbnm;
                              var spaceid = Birst.Utils.User.spaceId;
                              var url = window.location.origin + window.location.pathname;
                              url += '?birst.module=dashboard';
                              url += '&birst.spaceId=' + Birst.Utils.User.spaceId;
                              url += '&birst.dashboard=' + this.editJobDetails.dashbnm;
                              url += '&birst.page=' + this.editJobDetails.pagenm;
                              url += '&birst.renderType=HTML';
                              var urlLink = '&nbsp;<br>&nbsp;<a href="' +  url + '">'+ 'Link to Dashboard' + '</a> &nbsp; <br>';
                              if (c.getValue()) {
                                  emailBodyTxt += urlLink;
                                  emailBodyCtl.setValue(emailBodyTxt);
                              } else {
                                  var linkEsc = urlLink.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                                  var linkReg = new RegExp(linkEsc, 'g');
                                  var emailBodyEsc = emailBodyTxt.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '""').replace(/&apos;/g, '\'');
                                  var textRepl = emailBodyEsc.replace(linkReg,"");
                                  emailBodyCtl.setValue(textRepl);
                              }
                        }
                    }
                }]

            },{
                xtype:'fieldset',
                title: 'Schedule',
                defaultType: 'textfield',
                collapsible: true,
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelSeparator:''
                },
                items: [{
                    xtype: 'panel',
                    id: 'bid-reccurane-panel',
                    border: 0,
                    bodyPadding: 5,
                    fieldDefaults: {
                        anchor: '100%'
                        //msgTarget: 'under'
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'radiogroup',
                        fieldLabel: 'Schedule',
                        columns: 3,
                        defaults: {
                            margin: '0 5 0 0',
                            labelSeparator:''
                        },
                        baseBodyCls: 'bx-baseBody',
                        fieldBodyCls: 'bx-fieldBody',
                        items: [
                                {xtype: 'radiofield', boxLabel: 'Daily',   name: 'rd-schedule', id:'dailyRdId', checked: true, inputValue: 'daily'},
                                {xtype: 'radiofield', boxLabel: 'Weekly',  name: 'rd-schedule', id:'weeklyRdId',inputValue: 'weekly'},
                                {xtype: 'radiofield', boxLabel: 'Monthly', name: 'rd-schedule', id:'monthlyRdId',inputValue: 'monthly'}
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                 var value = newValue["rd-schedule"];
                                 switch(value)
                                 {
                                    case "daily":
                                        Ext.getCmp("etWeeklyOrMonthlyPanelId").setVisible(false);
                                        break;

                                    case "weekly":
                                        Ext.getCmp("etWeeklyOrMonthlyPanelId").setVisible(true);
                                        var weekormonth = Ext.getCmp("etWeeklyOrMonthlyPanelId").getLayout().setActiveItem(0);
                                        break;
                                    case "monthly":
                                        Ext.getCmp("etWeeklyOrMonthlyPanelId").setVisible(true);
                                        var weekormonth = Ext.getCmp("etWeeklyOrMonthlyPanelId").getLayout().setActiveItem(1);

                                        break;
                                    default:
                                }
                            }
                        }
                    },{
                        xtype: 'panel',
                        id: 'etWeeklyOrMonthlyPanelId',
                        hidden: true,
                        width: '100%',
                        layout: {
                            type: 'card'
                        },
                        bodyCls: 'xb-panel-body-default',
                        defaults: {
                            border: false
                        },
                        items:[{
                            xtype: 'panel',
                            id: 'idWeeklyPanel',
                            width: '100%',
                            border: 0,
                            items:[{
                                xtype: 'checkboxgroup',
                                width: '100%',
                                fieldLabel: 'Day of Week',
                                afterLabelTextTpl: required,
                                baseBodyCls: 'bx-baseBody',
                                fieldBodyCls: 'bx-fieldBody',
                                style: {
                                  'margin-bottom':'12px',
                                  'margin-top':'12px'
                                },
                                columns: 4,
                                items: [
                                        {boxLabel: 'Monday',     name: 'dow-select', id: 'day2ChkbxId'},
                                        {boxLabel: 'Tuesday',    name: 'dow-select', id: 'day3ChkbxId'},
                                        {boxLabel: 'Wednesday',  name: 'dow-select', id: 'day4ChkbxId'},
                                        {boxLabel: 'Thursday',   name: 'dow-select', id: 'day5ChkbxId'},
                                        {boxLabel: 'Friday',     name: 'dow-select', id: 'day6ChkbxId'},
                                        {boxLabel: 'Saturday',   name: 'dow-select', id: 'day7ChkbxId'},
                                        {boxLabel: 'Sunday',     name: 'dow-select', id: 'day1ChkbxId'}
                                ]
                            }]
                        },{
                            xtype: 'panel',
                            id: 'idMonthyPanel',
                            border: 0,
                            width: '100%',
                            style: {
                              'margin-bottom':'12px',
                              'margin-top':'12px'
                            },
                            hideMode:'offsets',
                            fieldDefaults: {
                                    anchor: '100%',
                                    msgTarget: 'under'
                            },
                            items: [dayOfMonthCombo]
                            }]
                    },{
                        xtype: 'panel',
                        id: 'idTimeOfDayPanel',
                        border: 0,
                        fieldDefaults: {
                                anchor: '100%',
                                msgTarget: 'under'
                        },
                        items:[{
                                xtype: 'panel',
                                layout: 'column',
                                columns: 4,
                                bodyPadding: '0 10 0 0',
                                items:[
                                       timeOfDayHourCombo,
                                       timeOfDayMinuteCombo,
                                       timeOfDayAmPmCombo,
                                       timeOfDayTZCombo]
                        }]

                    }]
                }]
            }]

          }

    },
    handleRunNow: function() {
        var schs = Ext.getCmp('id-schedules-grid');
        var arrList=[], selected = Ext.getCmp('id-schedules-grid').getView().getSelectionModel().getSelection();
            Ext.each(selected, function (record) {
                                    arrList.push(record.data);
                    });
        arrList.forEach(function(rec) {
            var schedName = rec.ScheduleName;
            Birst.core.Webservice.schedulerRequest('runNow',
                [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                  {key: 'type', value: 'Report'},
                  {key: 'typeID', value: rec.JobID}
                ],
                null,
                function(response){
                    Ext.create('Birst.Util,ConfirmDialog', {
                        title: '<span> '+ Birst.LM.Locale.get('LB_CONFIRMATION') + '</span>',
                        msg: Birst.LM.Locale.get('SCHEDL_RUNNOW_SUCCS'),
                        btnText: Birst.LM.Locale.get('LB_OK'),
                        dockedItems:  [{
                            xtype: 'toolbar',
                            ui: 'footer',
                            dock: 'bottom',
                            height: 60,
                            layout: {
                                pack: 'center'
                            },
                            items: ['->', {
                                 minWidth: 80,
                                 cls: 'xb-modwin-submit-btn',
                                 overCls: 'xb-submit-btn-over',
                                 pressedCls: 'xb-footer-btn-press'
                            }]
                        }]
                    }).show();
                },function(response){
                    Ext.create('Birst.Util,ErrorDialog', {
                        msg: Ext.String.format(Birst.LM.Locale.get('SCHEDL_RUN_ERR'), schedName),
                        btnText: Birst.LM.Locale.get('LB_OK')
                    }).show();
                },null,true);
        });
    },
    handleDeleteSched: function() {
        var schs = Ext.getCmp('id-schedules-grid');
        var arrList=[], selected = Ext.getCmp('id-schedules-grid').getView().getSelectionModel().getSelection();
            Ext.each(selected, function (record) {
                                    arrList.push(record.data);
                    });
        Ext.create('Birst.Util,ConfirmDialog', {
            title: '<span> '+Birst.LM.Locale.get('LB_CONFIRMATION')+ '</span>',
            msg: Birst.LM.Locale.get('SCHEDL_CONF_DEL'),
            btnText: Birst.LM.Locale.get('LB_OK'),
            scope:this,
            fn: function () {
                 arrList.forEach(function(rec) {
                    var schedName = rec.ScheduleName;
                    Birst.core.Webservice.schedulerRequest('removeJob',
                        [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                          {key: 'jobType', value: 'Report'},
                          {key: 'jobID', value: rec.JobID}
                        ],
                        null,
                        function(response){
                            schedStore.remove(selected);
                        },function(response){
                            Ext.create('Birst.Util,ErrorDialog', {
                                msg: Ext.String.format(Birst.LM.Locale.get('SCHEDL_DEL_ERR'), schedName),
                                btnText: Birst.LM.Locale.get('LB_OK')
                            }).show();
                        },null,true);
                });
            }
        }).show();
   },
   submitJobsRunStatus: function(){
       // bulk schedule enable/disable status update - submit only if original value has been actually changed
       var reguestParams = '<ScheduledJobList>';
       var itemsToUpdate = 0;
       schedStore.data.each(function(record, index, totalItems ) {

            if (record.get('_changed') && record.get('P') === "true"){
                reguestParams += '<ScheduledJob>';
                reguestParams += '<Id>' + record.get('JobID')+ '</Id>';
                reguestParams += '<Type>' + 'Report'+ '</Type>';
                reguestParams += '<Run>' + record.get('Run')+ '</Run>';
                reguestParams += '</ScheduledJob>';
                itemsToUpdate++;
            }
       });
       reguestParams += '</ScheduledJobList>';
       if (itemsToUpdate > 0) {
           Birst.core.Webservice.schedulerRequest('updateJobsRunStatus',
                       [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                         {key: 'jobsRunStatusXmlString', value: reguestParams}
                       ],
                       this,
                       function (response) {

                       },
                       function (response) {

                       },
                   null,true);
           }
   },
   populateShceduleEditForm: function(responseXml) {

        if (!responseXml) {
            this.populateDefaults();
            return;
        }

        var details = responseXml.getElementsByTagName('JobDetails');
        if (details && details[0]) {
            var prop = details[0];
            var fileName = getXmlChildValue(prop, 'FileName');
            var report = getXmlChildValue(prop, 'Report').replace("V{CatalogDir}","").split("\\");
            this.editJobDetails.reportFullName = getXmlChildValue(prop, 'Report').replace("V{CatalogDir}","");
            report = report[report.length-1];
            var compression = getXmlChildValue(prop, 'Compression');
            var reportType = getXmlChildValue(prop, 'ReportType');
            var toReport = getXmlChildValue(prop, 'ToReport');
            var TriggerReport = getXmlChildValue(prop, 'TriggerReport');
            var csvSeparator = getXmlChildValue(prop, 'CsvSeparator');
            var csvExtension = getXmlChildValue(prop, 'CsvExtension');
            var createdAdminMode = getXmlChildValue(prop, 'CreatedAdminMode');
            var toList = responseXml.getElementsByTagName('ToList');
            var emails='';
            if (toList) {
                  var child  = toList[0].firstChild;
                  while (child) {
                      if (child.tagName === 'Email') {
                          emails += (emails == '' ? getXmlValue(child) : ' ; ' + getXmlValue(child));
                      }
                      child = child.nextSibling;
                  }
            }
            toList = emails;
            var subject = getXmlChildValue(prop, 'Subject');
            var body = getXmlChildValue(prop, 'Body');
            var from = getXmlChildValue(prop, 'From');
            Ext.ComponentQuery.query('textfield[name=etReportPathFld]')[0].setValue(fileName);

            // delivery format
            ["html","excel","pdf","ppt","csv","rtf"].forEach(function(item){
                    Ext.ComponentQuery.query("checkboxfield[id="+item+"ChkbxId]")[0].setValue(false);
            });

            var repList = reportType.split(",");
            repList.forEach(function(item) {
                //console.log(item);
                Ext.ComponentQuery.query("checkboxfield[id="+item+"ChkbxId]")[0].setValue(true);
            });
            Ext.ComponentQuery.query("textfield[name=delmFldId]")[0].setValue(csvSeparator);
            Ext.ComponentQuery.query("textfield[name=fedngFldId]")[0].setValue(csvExtension);

            //! File compression
            var fcb = Ext.getCmp('etComprssCombxId');
                var fcs = fcb.getStore();
                var idx;
                if ((idx = fcs.find('name',compression)) > 0) {
                   fcb.select(fcs.getAt(idx));
                } else {
                    fcb.select(fcs.getAt(0));
                  }

            Ext.ComponentQuery.query("textfield[name=etAttchmentNameFld]")[0].setValue(fileName);

            //! Email section
            Ext.ComponentQuery.query("textfield[name=etEmailToFld]")[0].setValue(toList.replace(/\s/g, ''));
            Ext.ComponentQuery.query("textfield[name=etEmailFromFld]")[0].setValue(from);
            Ext.ComponentQuery.query("textfield[name=etEmailsubjectFld]")[0].setValue(subject);
            Ext.ComponentQuery.query("htmleditor[name=etEmailBodyFld]")[0].setValue(body);
        }

        // Prompts
        this.displayPrompts(responseXml);

        //! Schedule section
        var details = responseXml.getElementsByTagName('Schedule');
        if (details && details[0]) {
            var prop = details[0];
            var timezone = getXmlChildValue(prop, 'TimeZone');
            var timezonecombx = Ext.ComponentQuery.query("combobox[id=etTimeOfDayTZCombxId]")[0];
            timezonecombx.setValue(timezone);
            var enable = getXmlChildValue(prop, 'Enable');
            var cronXml = getXmlChildValue(prop, 'Cron');
            var cron = this.decodeCron(cronXml);
            this.setCronFieldCtls(cron);
            this.editJobDetails.scheduleId = getXmlChildValue(prop, 'ScheduleId');
        }
    },
    parsePrompts: function(promptsDoc){
        if (!promptsDoc) return;

        var doc = promptsDoc;
        var prompts = doc.getElementsByTagName('Prompts');

        if (prompts && prompts[0]) {
            var prompt = prompts[0].firstChild;
            if (prompt) {
                var promptsTxt = '<applyAllPrompts>true</applyAllPrompts>';
            }
            while (prompt) {
                promptsTxt += '<Prompt>';
                if (prompt.nodeType === 1 && prompt.tagName === 'Prompt') {
                    var parameterName = '';
                    parameterName = getXmlChildValue(prompt, 'ParameterName');
                    promptsTxt += '<ParameterName>' + parameterName  + '</ParameterName>';
                    var selectedValues = prompt.getElementsByTagName('selectedValues');
                    var values='';
                    if (selectedValues) {
                        values += '<selectedValues>';
                        var value = selectedValues[0].firstChild;
                        while (value) {
                            values += '<selectedValue>';
                            if (value.nodeType === 1 && value.tagName === 'selectedValue') {
                                values += getXmlValue(value);
                            }
                            values += '</selectedValue>';
                            value = value.nextSibling;
                        }
                        values += '</selectedValues>';
                    }
                    promptsTxt  += values;
                }
                promptsTxt += '</Prompt>';
                prompt = prompt.nextSibling;
            }
        }
        return promptsTxt;
    },
    displayPrompts: function (promptsDoc) {
        if (!promptsDoc)
            return;
        var promptsTxt = '';
        var promptsXml = promptsDoc.getElementsByTagName('Prompts');
        this.editJobDetails.promptsXml = promptsXml[0];
        var prompts = promptsXml;
        if (prompts && prompts.length > 0) {
            var prompt = prompts[0].firstChild;
            while (prompt) {
                if (prompt.nodeType === 1 && prompt.tagName === 'Prompt') {
                    var parameterName = '';
                    parameterName = getXmlChildValue(prompt, 'ParameterName');
                    var selectedValues = prompt.getElementsByTagName('selectedValues');
                    var values = '';
                    if (selectedValues) {
                        var value = selectedValues[0].firstChild;
                        while (value) {
                            if (value.nodeType === 1 && value.tagName === 'selectedValue') {
                                var val = getXmlValue(value) == 'NO_FILTER' ? 'All' : getXmlValue(value);
                                values += (values == '' ? val : ',' + val);
                            }
                            value = value.nextSibling;
                        }
                    }
                    var thisPrompt = parameterName+'='+values;

                    promptsTxt += (promptsTxt == '' ? thisPrompt : ';' + thisPrompt);
               }
                prompt = prompt.nextSibling;
            }
        }
        promptsTxt = "Prompts: " + promptsTxt;
        var fldCtl = Ext.getCmp('etPromptFldId');
        fldCtl.tipTxt = promptsTxt;
        fldCtl.setText('');
        if (fldCtl.tip){
                fldCtl.tip.destroy();
        }
        var measure = Ext.util.TextMetrics.measure('etPromptFldId',promptsTxt);
        var maxW = fldCtl.ownerCt.getWidth() - 260;
        if (maxW < measure.width) {
            var dpx = measure.width - maxW;
            var len = promptsTxt.length;
            var achar = promptsTxt.charAt(len/2) ;
            var charM = Ext.util.TextMetrics.measure('etPromptFldId', achar);
            var newLen = len - Math.floor(dpx/charM.width);
            var promptsTxtTrn = promptsTxt.substring(0,newLen) + "...";

            fldCtl.setText(promptsTxtTrn);
            fldCtl.tip = Ext.create('Ext.tip.ToolTip',{
                target: 'etPromptFldId',
                html: promptsTxt
            });
        } else {
            Ext.getCmp('etPromptFldId').setText(promptsTxt);
        }
        return promptsTxt;
    },
    populateDefaults: function(){

        Ext.ComponentQuery.query("checkboxfield[id=htmlChkbxId]")[0].setValue(true);
        Ext.ComponentQuery.query("textfield[name=etEmailFromFld]")[0].setValue('delivery@birst.com');
        Ext.ComponentQuery.query("textfield[name=etEmailsubjectFld]")[0].setValue('Report From Birst');

        Ext.ComponentQuery.query('textfield[name=etAttchmentNameFld]')[0].setValue(this.editJobDetails.name);
        Ext.ComponentQuery.query('textfield[name=etReportPathFld]')[0].setValue(this.editJobDetails.reportFullName);

        var docm = Str2Xml(this.editJobDetails.prompts);
        var child = docm.firstChild;
        var prompts = this.displayPrompts(Str2Xml(this.editJobDetails.prompts));
        Ext.getCmp('etPromptFldId').setText(prompts);

        Ext.ComponentQuery.query("combobox[id=etTimeOfDayTZCombxId]")[0].setValue('CST');
        var defaultCron = this.decodeCron('0 0 1 ? * * *');
        this.setCronFieldCtls(defaultCron);

        Ext.ComponentQuery.query("checkbox[name=dashblinkChkbx]")[0].setDisabled(false);
    },

    validateAndSubmitEditChanges: function(){

        if (!this.validateEditor()) {
            return false;
        }
        var schedName = Ext.ComponentQuery.query('textfield[name=etSchedNameFld]')[0].getValue();
        var reportName = Ext.ComponentQuery.query('textfield[name=etReportPathFld]')[0].getValue();
        var fileName =  Ext.ComponentQuery.query('textfield[name=etAttchmentNameFld]')[0].getValue();
        var emailToList = Ext.ComponentQuery.query('textfield[name=etEmailToFld]')[0].getValue();
        var emailSubject = Ext.ComponentQuery.query('textfield[name=etEmailsubjectFld]')[0].getValue();
        var emailFrom = Ext.ComponentQuery.query('textfield[name=etEmailFromFld]')[0].getValue();

        var emailBody = Ext.ComponentQuery.query('htmleditor[name=etEmailBodyFld]')[0].getValue();

        var reportTypeConcat = '';
        ["html","excel","pdf","ppt","csv","rtf"].forEach(function(item){
            var checked = Ext.ComponentQuery.query("checkboxfield[id="+ item +"ChkbxId]")[0].getValue();
            if (checked) {
                reportTypeConcat += reportTypeConcat == '' ? item : ','+ item;
            }
        });
        var compression = Ext.getCmp('etComprssCombxId').getValue();
        var csvDelim='';
        var fileEnding='';
        if (Ext.getCmp("csvChkbxId").getValue() === true) {
            csvDelim = Ext.ComponentQuery.query("textfield[name=delmFldId]")[0].getValue();
            fileEnding = Ext.ComponentQuery.query("textfield[name=fedngFldId]")[0].getValue();
        }

        var timeZone =  Ext.getCmp("etTimeOfDayTZCombxId").getValue();

        //! cron params: [d, w [dow],m [dom], ampm, hour, min]
        var cronStr = this.encodeCron();

        var reguestParams = '<ScheduledReport>';
        reguestParams +=  '<Report>' + this.editJobDetails.reportFullName + '</Report>';
        reguestParams +=  '<Name>'+ schedName + '</Name>';
        reguestParams +=  '<FileName>'+ fileName  +'</FileName>';
        if (this.isManage && this.editJobDetails.promptsXml ) {
            reguestParams +=  xml2Str(this.editJobDetails.promptsXml);
        } else {
            var promptsTxt = this.parsePrompts(Str2Xml(this.editJobDetails.prompts));
            reguestParams += '<Prompts>' + promptsTxt + '</Prompts>';
        }
        reguestParams +=  '<ToList> <Email>' + emailToList +'</Email> </ToList>';
        reguestParams +=  '<Subject>' + emailSubject + '</Subject>';
        reguestParams +=  '<From>' + emailFrom + '</From>';

        reguestParams +=  '<Body> <![CDATA[' + emailBody + ']]></Body>';
        reguestParams +=  '<ReportType>' + reportTypeConcat + '</ReportType>';

        reguestParams +=  '<Compression>'+ compression + '</Compression>';
        reguestParams +=  '<CsvSeparator>' + csvDelim + '</CsvSeparator>';
        reguestParams +=  '<CsvExtension>' + fileEnding + '</CsvExtension>';
        reguestParams +=  '<CreatedAdminMode>false</CreatedAdminMode>';

        var scheduleList = '<ScheduleList>';
        scheduleList += '<Schedule>';
        scheduleList += '<Cron>' + cronStr + '</Cron>';
        //scheduleList += '<Cron>' + '0 0 1 ? * * *' + '</Cron>';
        scheduleList += '<TimeZone>' + timeZone + '</TimeZone>';
        scheduleList += '<Enable>true</Enable>';
        if (this.isManage) {
            scheduleList += '<ScheduleId>' + this.editJobDetails.scheduleId + '</ScheduleId>';
            scheduleList += '<JobId>'+ this.editJobDetails.jobID + '</JobId>';
        }
        scheduleList += '</Schedule>';
        scheduleList += '</ScheduleList>';

        reguestParams += scheduleList;
        reguestParams += '</ScheduledReport>';

        if (this.isManage && this.editJobDetails.restrictedMode == "upgrade") {
            // prepare copy params
            var sched = Ext.getCmp('id-schedules-grid').getView().getSelectionModel().getSelection().pop();
            var scheduler = sched.get('S');

            var copyReguestParams  =  '<CopyJobInfo>';
            copyReguestParams     +=  '<JobId>' + this.editJobDetails.jobID + '</JobId>';
            copyReguestParams     +=  '<FromScheduler>'+ scheduler + '</FromScheduler>';
            copyReguestParams     +=  '<Type>'+ 'Report'  +'</Type>';
            copyReguestParams     +=  '<DeleteFrom>'+ 'true'  +'</DeleteFrom>';
            copyReguestParams     +=  '<MultipleJobIDs>'+'false'+ '</MultipleJobIDs>';
            copyReguestParams     += '</CopyJobInfo>';
        }

        if (this.isManage) {
        if (this.editJobDetails.restrictedMode == "none") {
                // update job
                Birst.core.Webservice.schedulerRequest('updateJob',
                       [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                         {key: 'userID', value: Birst.Utils.User.userId},
                         {key: 'jobType', value:'Report' },
                         {key: 'jobID', value: this.editJobDetails.jobID},
                         {key: 'jobOptionsXmlString', value: reguestParams}
                       ],
                       this,
                       function (response) {

                       },
                       function (response) {

                       },
                       null,true
               );
            } else {
                // upgrade job
                Ext.create('Birst.Util,ConfirmDialog', {
                    msg:  Ext.String.format('Saving changes to this report schedule will migrate it to release version {0}. After saving, you will no longer be able to modify the report schedule using prior release versions.', this.editJobDetails.release),
                    btnText: Birst.LM.Locale.get('LB_OK'),
                    scope: this,
                    fn: function() {
                        Birst.core.Webservice.schedulerRequest('copyJob',
                           [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                             {key: 'userID', value: Birst.Utils.User.userId},
                             {key: 'jobType', value:'Report' },
                             {key: 'copyOptionsXmlString', value: copyReguestParams},
                             {key: 'jobOptionsXmlString', value: reguestParams}
                           ],
                           this,
                           function (response) {
                                 // success, go back to manage schedules (this will laso take care of refreshing the data in grid)
                                 var actvPanel = Ext.getCmp('id-reportschedules-editview');
                                 actvPanel.animate({
                                        to: {
                                          opacity: 0
                                        },
                                        duration: 400,
                                        listeners: {
                                          afteranimate: function () {
                                                    var actvPanel = Ext.getCmp('id-reportschedules-manageview');
                                                    actvPanel.animate({
                                                        to: {
                                                            opacity: 100
                                                        },
                                                        duration: 800});
                                                    Ext.getCmp('id-reportschedules-main').getLayout().setActiveItem(0);
                                                    Ext.getCmp('bid-win-mngschedules').header.setTitle('REPORT SCHEDULES');
                                                    Ext.getCmp('etbtn-goback').animate({
                                                        to: {
                                                          opacity: 0
                                                        },
                                                        duration: 0});
                                          }
                                        }
                                });
                           },
                           function (response) {
                                var err_type, err_msg;
                                var stat = response.responseXML.getElementsByTagName('CreateNewSpaceResponse');
                                var res = stat[0].firstChild;
                                while (res != null) {
                                    err_type = getXmlChildValue(res, 'ErrorCode');
                                    err_msg = getXmlChildValue(res, 'ErrorMessage');
                                    res = res.nextSibling;
                                }
                                Ext.create('Birst.Util,ErrorDialog', {
                                        msg: Ext.String.format('Failed to migrate schedule {0} to current release version. ErrorMsg: {1} ErrCode: {2}', schedName, err_msg, err_type),
                                        btnText: Birst.LM.Locale.get('LB_OK')
                                }).show();
                           },
                           null,true
                       )
                  }
                }).show();
                return false;
            }
        } else {
            // add job
            Birst.core.Webservice.schedulerRequest('addJob',
                   [ {key: 'spaceID', value: Birst.Utils.User.spaceId},
                     {key: 'userID', value: Birst.Utils.User.userId},
                     {key: 'jobType', value:'Report' },
                     {key: 'jobOptionsXmlString', value: reguestParams}
                   ],
                   this,
                   function (response) {

                   },
                   function (response) {

                   },
                   null,true
            );
        }
       return true;
    },
    validateEditor: function(){
        var validated = false;
        var ctl = Ext.ComponentQuery.query('textfield[name=etSchedNameFld]')[0];

        if (!Ext.ComponentQuery.query('textfield[name=etSchedNameFld]')[0].isValid() ||
            !Ext.ComponentQuery.query('textfield[name=etEmailFromFld]')[0].isValid() ||
            !Ext.ComponentQuery.query('textfield[name=etEmailToFld]')[0].isValid()) {
            return false;
        }
        // delivery format
        ["html","excel","pdf","ppt","csv","rtf"].some(function(item){
                if (Ext.ComponentQuery.query("checkboxfield[id="+item+"ChkbxId]")[0].getValue() == true) {
                    validated = true;
                    return true; //stop looping
                }
        });

        if (!validated) {
            Ext.create('Birst.Util,ErrorDialog', {
                msg: "Report Settings: Please select Delivery Formats",
                btnText: 'OK'
            }).show();
            return false;
        }
        validated = false; //continue validating other fields

        if (Ext.getCmp("weeklyRdId").getValue()) {
            for (var i = 1; i< 8; i++) {
                if (Ext.getCmp("day"+i+"ChkbxId").getValue() == true) {
                    validated = true;
                     break;
                }
            }
        } else {
            validated = true;
        }

        if (!validated) {
            Ext.create('Birst.Util,ErrorDialog', {
                msg: "Schedule Settings: Please select day(s) for weekly interval",
                btnText: 'OK'
            }).show();
            return false;
        }
        return true;
    },
    decodeCron: function(cron){
        var cronAtrr = cron.split(" ");
        var ampm, hour, mode, data=[];

        if (cronAtrr[2] >= 12) {
            ampm = 'pm';
            hour = cronAtrr[2] == "12" ? 12 : cronAtrr[2] - 12;
        } else {
            ampm = 'am';
            hour = cronAtrr[2] == "0" ? 12 : cronAtrr[2];
        }

        if (cronAtrr[3] != '?' ) {
            mode = 2; // Monthly
            data[0] = cronAtrr[3];
        } else
            if (cronAtrr[5] != '*' ) {
                mode = 1; // Weekly
                weekdays = cron.split("*")[1].trim(" ").split(",");
                weekdays.forEach(function(item) {
                     data.push(item);
                });
            } else {
                mode = 0; // Daily
            }
        return {
                 sec: 0,
                 min: cronAtrr[1] == "0" ? "00" : cronAtrr[1],
                 hour: hour,
                 ampm: ampm,
                 mode:mode,
                 data:data
        };
    },
    encodeCron: function(cronXml){
        var cronStr = "0";
        cronStr += " " + Ext.getCmp('etTimeOfDayMinuteCombxId').getValue();
        var ampm = Ext.getCmp('etTimeOfDayAmPmCombxId').getValue();
        var hour = Ext.getCmp('etTimeOfDayHourCombxId').getValue();
        if (ampm == "am") {
            cronStr += " " + (hour == "12" ? 0 : hour);
        } else {
            // pm
            var inth =  parseInt(hour) + 12;
            cronStr += " " + (hour == "12" ? 12 : inth.toString());
        }

        if (Ext.getCmp('dailyRdId').getValue() == true )
        {
            cronStr += " " + "?" + " * * * ";
        } else
            if (Ext.getCmp('weeklyRdId').getValue() == true )
            {
                cronStr += " " + "? * ";
                var hours='';
                for (var i = 1; i< 8; i++) {

                    if (Ext.getCmp("day"+i+"ChkbxId").getValue() == true) {
                        hours += (hours != '' ?
                                "," + i
                                : i);
                    }
                }
                cronStr += hours + " *";
            } else {
                // // monthly
                cronStr += " " + Ext.getCmp('etDayOfMonthCombxId').getValue();
                cronStr += " * ? *";
            }
        return cronStr;
    },
    ordinaryNumberSfx: function(num) {

        return  [ "1st" , "2nd" , "3rd" , "4th" , "5th" , "6th" ,
                  "7th" , "8th" , "9th" , "10th", "11th", "12th",
                  "13th", "14th", "15th", "16th", "17th", "18th",
                  "19th", "20th", "21st", "22nd", "23rd", "24th",
                  "25th", "26th", "27th", "28th", "29th", "30th",
                  "31st"
                ][ num -1];
    },
    setCronFieldCtls: function(cron){

        var hourscombx = Ext.ComponentQuery.query("combobox[id=etTimeOfDayHourCombxId]")[0];
        hourscombx.setValue(cron.hour);

        var mincombx = Ext.ComponentQuery.query("combobox[id=etTimeOfDayMinuteCombxId]")[0];
        mincombx.setValue(cron.min);

        var ampmcombx = Ext.ComponentQuery.query("combobox[id=etTimeOfDayAmPmCombxId]")[0];
        ampmcombx.setValue(cron.ampm);

        var timezonecombx = Ext.ComponentQuery.query("combobox[id=etTimeOfDayTZCombxId]")[0];
        var weeklymonthly = Ext.getCmp("etWeeklyOrMonthlyPanelId");
        var rdCtlToCheck;
        switch(cron.mode)
        {
            case 0:  // Daily
                weeklymonthly.setVisible(false);
                rdCtlToCheck = Ext.getCmp("dailyRdId");
                rdCtlToCheck.setValue(true);
                break;
            case 1:  // Weekly
                weeklymonthly.setVisible(true);
                rdCtlToCheck = Ext.getCmp("weeklyRdId");
                rdCtlToCheck.setValue(true);
                cron.data.forEach(function(i){
                    Ext.getCmp("day"+i+"ChkbxId").setValue(true);
                });
              break;
            case 2:  // Monthly
                weeklymonthly.setVisible(true);
                rdCtlToCheck = Ext.getCmp("monthlyRdId");
                rdCtlToCheck.setValue(true);
                Ext.getCmp("etDayOfMonthCombxId").setValue(cron.data[0]);
              break;
            default:
        }
     }
});