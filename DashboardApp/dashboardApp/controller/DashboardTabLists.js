Ext.ns("Birst.dashboards");

Ext.define('Dashboard.controller.DashboardTabLists', {
	extend: 'Ext.app.Controller',

	pageController: null,
	pageToOpen: null,
	dashboardToOpen: null,
	filtersOnOpen: null,

	refs: [
		{
			selector: 'viewport #tabsContainer',
			ref: 'tabsContainer'
		},
		{
			selector: 'viewport #pageContainer',
			ref: 'pageContainer'
		},
		{
			selector: 'viewport #promptsContainer',
			ref: 'promptsContainer'
		},
		{
			selector: 'viewport #pagesTabContainer',
			ref: 'pagesTabContainer'
		},
		{
			selector: 'viewport #pagePromptContainer',
			ref: 'pagePromptContainer'
		}
	],

	init: function() {
		this.control({
			'#tabsContainer': {
				render: this.onTabsRendered
			}
		});
		var params = Ext.Object.fromQueryString(window.location.search);
		if (params['birst.page']) {
			var page = decodeURIComponent(Ext.String.htmlDecode(params['birst.page']).replace(/\+/g, ' '));
			if (page.split(',').length > 1) {
				page = page.split(',')[0];
			}
			this.pageToOpen = page;
		}

		if (params['birst.dashboard']) {
			var dashb = decodeURIComponent(Ext.String.htmlDecode(params['birst.dashboard']).replace(/\+/g, ' '));
			if (dashb.split(',').length > 1) {
				dashb = dashb.split(',')[0];
			}
			this.dashboardToOpen = dashb;
		}
		
		if(params['birst.dashboardParams']){
			var separator =  params['birst.dashParamsSeparator'] ? params['birst.dashParamsSeparator'] : ';';
			var filterParams = decodeURIComponent(Ext.String.htmlDecode(params['birst.dashboardParams']).replace(/\+/g, ' '));
			filterParams = filterParams.split(separator).join("&");
			this.filtersOnOpen = filterParams;
		}else{
			this.filtersOnOpen = window.location.search;
		}
	},

	navigateToDash: function(dash, page, filters) {
		this.pageToOpen = page;
		this.filtersOnOpen = filters;
		var tabsContainer = this.getTabsContainer();
		var tabs = tabsContainer.child();
		if (tabs.activeTab.title !== dash) {
			var items = tabs.items;
			items.each(function(item, index, len) {
				if (item.title === dash) {
					tabs.setActiveTab(item);
					return false;
				}
				return true;
			});
		}
		else {
			// tab is correct - go to page
			this.openPageToOpen();
		}
	},

	openPageToOpen: function() {
		var pagesTabContainer = this.getPagesTabContainer();
		var bttnGroup = pagesTabContainer.getChildByElement('pagesBttnGroup', false);
		var child = bttnGroup.child().items.getAt(0).child();
		while (child) {
			if (child.getText && child.getText() === this.pageToOpen) {
				child.toggle();
				this.onPageClick(child, null);
				break;
			}
			child = child.nextSibling();
		}
	},

	onTabsRendered: function(){
		//alert('tabs container was rendered');
		var tabs = this.getTabsContainer();
	},

	setButtonActiveState:function(button) {
		var bttItems = Ext.getCmp('pagesBttnGroup').items;
		bttItems.each(function(bttn){
			if (bttn.getText() !== button.getText()) {
				bttn.isLastActive = false;
			} else {
				bttn.isLastActive = true;
			}
		});
	},

	onPageClick: function(button, event){
		var page = button.page;

		if(page){
			var uuid = page.getAttribute('uuid');
			var path = page.getAttribute('dir') + '/' + page.getAttribute('name') + '.page';
			var needToGetPage = page.getAttribute('partial') === 'true';

			var mainContainer = this.getPagePromptContainer();
			this.setButtonActiveState(button);

			if(!button.pagePromptContainer || event=='refresh'){
				var promptItem = null;

				if(Birst.dashboards.doNP){
					//This is the left side prompts parent container
					promptItem = Ext.create('Ext.Panel', {
						layout: {
							type: 'vbox'
						},
						cls: 'npPromptContainer',
						hidden: true,
						region: 'west',
						border: true,
						height: '100%',
						width: 300,
						minWidth: 300,
						split: true,
						overflowY: 'auto'
					});
				}else{
					promptItem = Ext.create('Ext.Panel', {
							//xtype: 'panel',
							height: 230,
							width: '100%',
							hidden: true,
							autoScroll: true,
							cls: 'prompts',
							border: false,
							titleCollapse: true,
							collapsible: true,
							collapsed: true,
							hideCollapseTool: true,
							title: 'Prompts',
							layout: {
								type: 'border'
							},
							tools: [
							{
								type: 'expand',
								cls: 'promptExpand'
							},
							{
								xtype: 'label',
								width: '90%',
								cls: 'promptSummary',
								id: button.getId() + '-promptSummary'
								}
							],
							bodyPadding: '2 10 0 0',
							padding: '2 10 0 0',
							listeners: {
								render: {
								fn: function(me, eOpts) {
										me.header.on('render', function(hdr, opts) {
											var el = hdr.getEl();
											el.addClsOnOver('hover');
										});
									}
								}
							}
					});
				}

				if(Birst.dashboards.doNP){
					var clickToExpTool = Ext.create('Ext.button.Button', {
										iconCls: 'filterBttnIcon',
										//text: '&#59212;',
										cls: 'filterBttn',
										//width: 20,
										//height: 30,
										padding: "5 10 5 5",
										border: 0,
										tooltip: 'Filters'
									});

					var summaryTabs = Ext.create('Ext.toolbar.Toolbar', {
						width: '100%',
						height: '100%',
						cls: 'promptSummary',
						id: button.getId() + '-promptSummary',
						enableOverflow: true,
						margin: '0 35 0 0',
						layout: {
							overflowHandler: 'Scroller'
						},
						listeners: {
							render: function(){
								this.layout.overflowHandler.scrollIncrement = 100;
							}
						}
					});

					var npPanel = Ext.create('Ext.panel.Panel', {
						layout: {
							type: 'border'
						},
						border: true,
						items:[{
								xtype: 'panel',
								region: 'north',
								//cls: 'promptSummaryBox',
								height: 30,
								width: '100%',
								layout: 'hbox',
								items: [
									clickToExpTool,
									/*
									{
										xtype: 'container',
										width: '100%',
										height: '100%',
										cls: 'promptSummary',
										id: button.getId() + '-promptSummary'
									}*/
									summaryTabs
								]
							},
							{
								xtype: 'panel',
								region: 'center',
								layout: {
									type: 'fit',
									align: 'stretch',
									pack: 'start'
								},
								//cls: 'Dashboard',
								cls: 'Dashboard borders',
								items:[
								{
									autoScroll: true,
									layout: {
										type: 'fit'
									},

									flex: 1
								}]
							}
						]
					});

					if(event == 'refresh' && button.summaryTab){
						button.summaryTab.removeAll();
					}

					button.summaryTab = summaryTabs;
					button.pagePromptContainer = npPanel;
					button.clickToExpandTool = clickToExpTool;
				}else{
					var pagePanel = Ext.create('Ext.panel.Panel', {
							layout: {
								type: Birst.dashboards.doNP ? 'hbox' : 'vbox',
								align: 'stretch',
								pack: 'start'
							},
							items:[
							{
								autoScroll: true,
								layout: {
									type: 'fit'
								},
								cls: 'Dashboard',
								flex: 1
							}]
					});
					button.pagePromptContainer = pagePanel;
					button.pagePromptContainer.insert(0, promptItem);
				}

				mainContainer.add(button.pagePromptContainer);
				mainContainer.getLayout().setActiveItem(button.pagePromptContainer);

				var container = button.pagePromptContainer.getComponent(1);
				var promptsContainer = promptItem; //button.pagePromptContainer.getComponent(0);

				var getPageFunc =
					function(page){
						if(page){
								container.removeAll();
								promptsContainer.removeAll();
								page.path = path;
								button.pageController = Ext.create('Birst.dashboards.PageController', page, container, promptsContainer, button, button.pagePromptContainer);
								if(this.filtersOnOpen){
									button.pageController.filtersOnOpen = this.filtersOnOpen;
								}
								this.pageController = button.pageController;
								button.pageController.render();
						}
					};

				if(needToGetPage || event=='refresh' ){
					Birst.dashboards.Page.getPageByUidPath(uuid, path, getPageFunc, this);
				}else{
					var pageObj = Ext.create('Birst.dashboards.Page');
					pageObj.parse(button.page);
					getPageFunc.call(this, pageObj);
				}
			}else{
				this.pageController = button.pageController;
				mainContainer.getLayout().setActiveItem(button.pagePromptContainer);
				var applyPrompts = false;
				if(!button.pageController.promptsController.isPromptSessionVarsInSync()){
					button.pageController.promptsController.setPromptValuesBySessionVars();
					applyPrompts = true;
				}
				if (this.filtersOnOpen) {
					button.pageController.promptsController.applyFilters(this.filtersOnOpen);
					this.filtersOnOpen = null;
					applyPrompts = true;
				}
				if (applyPrompts) {
					button.pageController.promptsController.applyPrompts(false, true);
				}
			}


		}
	},

	parse: function(resultXML){
		var result = resultXML.getElementsByTagName(Birst.core.Webservice.isNamespaceImportant() ? 'ns:getDashbardListFromDBResponse' : 'getDashbardListFromDBResponse');
		if (result.length > 0) {
			var webserviceResult = getXmlChildValue(result[0], Birst.core.Webservice.isNamespaceImportant() ? 'ns:return' : 'return');
			var xml = Str2Xml(webserviceResult);
			var dashboards = xml.getElementsByTagName('Dashboards');
			var params = Ext.Object.fromQueryString(window.location.search);
			var dash = null;

			if (this.dashboardToOpen) {
			//if (params['birst.dashboard']) {
				dash = this.dashboardToOpen; // decodeURIComponent(Ext.String.htmlDecode(params['birst.dashboard']).replace('+', ' '));
			}

			var tabs = [];
			var firstPage = null;
			var child = dashboards[0].firstChild;
			while (child) {
				//console.debug("dashboard is " + dashboard);
				//console.debug("dash title is " + dashboard.get("name"));
				var name = child.getAttribute("name");
				var obj = { title: name, dashboard: child, itemId: name, listeners : {
					removeTab : function( ) {
						this.destroy();
					}
				} };

				if(this.areAllPagesInvisible(child)){
					obj.hidden = (this.areAllPagesInvisible(child) && Birst.core.DashboardSettings.get('EditDashboard')=="false");
				}else{
					if(!firstPage){
						firstPage = child.getAttribute("name");
					}
				}

				if (dash && name === dash) {
					firstPage = child.getAttribute("name");
				}

				tabs.push(obj);

				child = child.nextSibling;
			}

			if(Birst.core.DashboardSettings.get('EditDashboard') === "true"){
				var obj = { title: '+', tabConfig: { cls : 'x-btn-add-dash' }, dashboard: null, tooltip: Birst.LM.Locale.get( 'LB_ADD_DASH' ), itemId: 'birstAddDash'};
				tabs.push(obj);
			}

			var tabPanel = Ext.create('Dashboard.view.DashboardTabPanel', {
				activeTab: 0,
				tabBar : {
					width: '100%'
				},
				plain: true,
				items: tabs,
				listeners: {
					scope: this,
					tabchange: function(tabPanel, newCard, oldCard, options) {
						if(newCard.itemId == 'birstAddDash'){
							this.showAddDashboard();
						}else{
							this.displayPageButtons(newCard);
						}
					}
				}
			});

			if(Birst.core.DashboardSettings.get('EditDashboard') === "true"){
				var that = this;
				tabPanel.on('render', function(me){
					me.getEl().on('contextmenu', that.dashboardTabContextMenuFn(tabPanel));

					//When we do not have any dashboards created and only have '+' displayed
					//Add the click handler to '+' for option to create dashboards
					if(me.items.length == 1){
						var i = me.items.get(0);
						if(i.itemId == 'birstAddDash'){
							i.tab.on('click', function(){
								that.showAddDashboard();
							});
						}
					}
				});
			}

			var tabsContainer = this.getTabsContainer();
			tabsContainer.add(tabPanel);
			if(firstPage){
				//var firstCard = tabPanel.child("[itemId="+"\'"+firstPage+"\']"); // DASH-505
				var firstCard = tabPanel.items.map[firstPage];
				tabPanel.setActiveTab(firstCard);
				this.displayPageButtons(firstCard);
			}
		}
	},

	showAddDashboard: function(){
		var headerDiv = document.getElementById('idHeaderContainer');
		if(headerDiv){
			heightOffset = headerDiv.clientHeight;
		}
		this.editPage( null, null, null, heightOffset );
	},

	//Dashboard tab management
	dashboardTabContextMenuFn: function(tabPanel){
		var that = this;
		return function(evt, me){
			evt.preventDefault();

			var dashboardName = me.textContent;

			var rect = me.getBoundingClientRect();
			var contextMenu = Ext.create( 'Ext.menu.Menu', {
					cls : 'x-db-page-menu',
					items: [
							{
								text : Birst.LM.Locale.get( 'DB_MENU_RENAME' ),
								iconCls : 'icon-edit',
								handler : function( e ) {
									that.showConfirmRenameDashboard(tabPanel, dashboardName);
								}
							},
							{
								text: Birst.LM.Locale.get( 'DB_MENU_DELETE' ),
								iconCls: 'icon-delete',
								handler: function(){
									that.showConfirmDeleteDashboard(tabPanel, dashboardName);
								}
							}
					]
			});
			contextMenu.showAt(rect.left, rect.top + 30);
		}
	},

	showConfirmRenameDashboard: function(tabPanel, dashboardName){
		var nameTxtBox = Ext.create('Ext.form.field.Text', {
			fieldLabel: Birst.LM.Locale.get('RD_new'),
			allowBlank: false,
			width: 295,
			regex: /^[^\\.<>:"/\\\\|\\?\\*]{1,64}$/,
			regexText: Birst.LM.Locale.get('DT_regexError'),
			maxLength: 64,
			enforceMaxLength: true,
			allowOnlyWhitespace: false
		});

		var copyFn = function(){
			var newName = nameTxtBox.getValue();
			if(!newName || newName == '' || !nameTxtBox.validate()){
				return false;
			}
			Birst.core.Webservice.dashboardRequest('changeDashboardName',
				[
					{key: 'oldName', value: dashboardName},
					{key: 'newName', value: newName}
				],
				this,
				function(response, options) {
					var tabBar = tabPanel.getTabBar();
					var tab = tabBar.items.findBy(function(item, key){
						return item.text === dashboardName;
					});
					if(tab){
						tab.setText(newName);
						tab.card.dashboard.setAttribute('name', newName);
					}
				}
			);
			return true;
		}.bind(this);

		this.showConfirmDlg('db_win_confirm_copy', copyFn, nameTxtBox, 'DT_rename', 'LB_SAVE', 'LB_CANCEL');
	},

	showConfirmDeleteDashboard: function(tabPanel, dashboardName){
		var yesHandler = function(){
			Birst.core.Webservice.dashboardRequest('deleteDashboard',
				[{key: 'dashboardName', value: dashboardName}],
				this,
				function(response, options) {
					var item = tabPanel.items.getByKey(dashboardName);
					tabPanel.remove(item);
				}
			);

			return true;
		}.bind(this);

		var htmlContent = {
			html : '<p>' + Birst.LM.Locale.get( 'MSG_CONFIRM_DEL_DASH' ) + '</p>'
		};

		this.showConfirmDlg('db_win_confirm_delete', yesHandler, htmlContent);
	},

	areAllPagesInvisible: function(dashboard){
		var page = dashboard.firstChild;
		while (page ) {
			if (page.nodeName === 'Page') {
				var invisibleEls = page.getElementsByTagName('Invisible');
				if(invisibleEls && invisibleEls.length > 0){
					var isInvisible = getXmlValue(invisibleEls[0], 'Invisible') === 'true';
					if(!isInvisible){
						return false;
					}
				}
				else {
					return false;
				}
			}
			page = page.nextSibling;
		}
		return true;
	},

	getIframeSrc: function(page, dashboard){
		var action = page !== '' ? '&birst.openPageForEditHTML=true' : '&birst.addNewPageHTML=true';
		var iframeSrc =  '/FlexModule.aspx?birst.module=dashboard&embedded=true&birst.embedded=true&birst.hideDashboardNavigation=true';
		iframeSrc += action  + '&birst.page=' + encodeURIComponent(page) + '&birst.dashboard=' + encodeURIComponent(dashboard);
		return iframeSrc;
	},

	editPage : function( dashboardTab, page, button, heightOffset ) {
		var that = this,
			pageName = page ? page.getAttribute( 'name' ) : '',
			dashboardName = dashboardTab ? dashboardTab.title : '',
			w = new Ext.Window( {
				title : (dashboardName || pageName) ? dashboardName + '/' + pageName : Birst.LM.Locale.get('LB_NEW_DASH'),
				tools : [
					{ xtype: 'button', text: 'X', cls: 'btn-close', handler : function() { w.close() } }
				],
				width : window.innerWidth,
				height: window.innerHeight - heightOffset,
				cls:'xb-modal-window',
				bodyCls: 'xb-panel-body-default',
				modal:true,
				draggable: false,
				resizable: false,
				closable: false,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items : [
					{
						xtype : "component",
						margin: 5,
						flex: 1,
						layout: 'fit',
						autoEl : {
							tag : "iframe",
							src : that.getIframeSrc(pageName, dashboardName)
						}
					}
				],
				listeners : {
					afterrender : function(w) {
						var header = w.header;
						header.setHeight(36);
					}
				}
			} );

		var closeFn = function(){
			if( button ) {
				that.onPageClick(button, "refresh");
			} else {
				window.location.reload();
			}
			w.close();
		};

		w.showAt(0, heightOffset);

		var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
		var eventer = window[eventMethod];
		var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

		// Listen to message from child window
		eventer(messageEvent,function(e) {
			if(e.data == 'birstEditDashDone'){
				closeFn();
			}
		},false);
	},

	// Right click menu page management
	contextMenuEditFn : function( page, dashboardTab, button ) {
		var that = this;
		return function(evt, me){
			evt.preventDefault();
			var heightOffset = 0;
			var headerDiv = document.getElementById('idHeaderContainer');
			if(headerDiv){
				heightOffset = headerDiv.clientHeight;
			}
			var contextMenu = Ext.create( 'Ext.menu.Menu', {
				cls : 'x-db-page-menu',
				items: [
					{
						text: Birst.LM.Locale.get( 'DB_MENU_EDIT' ),
						iconCls: 'icon-edit',
						handler: function(){
							button.toggle();
							that.editPage( dashboardTab, page, button, heightOffset );
						}
					},
					{
						text : Birst.LM.Locale.get( 'DB_MENU_COPY' ),
						iconCls : 'icon-copy',
						handler : function( e ) {
							that.showConfirmCopy( button, dashboardTab );
						}
					},
					{
						text : Birst.LM.Locale.get( 'DB_MENU_DELETE' ),
						iconCls : 'icon-delete',
						handler : function( e ) {
							that.showConfirmDelete( button );
						}
					}
				]
			});
			contextMenu.showAt(this.getLeft(), this.getTop() + 30);
		};
	},

	getPagePath : function( page ) {
		return page.getAttribute( 'dir' ) + '/' + page.getAttribute( 'name' ) + '.page';
	},

	deletePage : function( button ) {
		var page = button.page,
			path = this.getPagePath( page );

		Birst.core.Webservice.dashboardRequest('deletePage',
			[{key: 'path', value: path}],
			this,
			function(response, options) {
				var pageContainer = button.pagePromptContainer,
					btnGroup = button.findParentByType( 'buttongroup' ),
					wasPressed = button.pressed;

				if( pageContainer ) {
					pageContainer.destroy();
				}
				button.fireEvent( 'removeBtn' );
				btnGroup.remove( button.id, true );
				//button.destroy();
				if( wasPressed && btnGroup.items.length > 0 ) {
					var firstBtn = btnGroup.items.items[0];
					firstBtn.toggle();
					this.onPageClick( firstBtn, 'click' );
				}
				button.destroy();
			}
		);
	},

	showConfirmDelete : function( button ) {
		var yesHandler = function(){
			this.deletePage(button);
			return true;
		}.bind(this);

		var htmlContent = {
			html : '<p>' + Birst.LM.Locale.get( 'MSG_CONFIRM_DELETE' ) + '</p>'
		};

		this.showConfirmDlg('db_win_confirm_delete', yesHandler, htmlContent);
	},

	showConfirmDlg: function (id,  yesHandler, content, locTitle, locYes, locNo) {
		var titleVal = (locTitle) ? Birst.LM.Locale.get(locTitle) : Birst.LM.Locale.get( 'LB_CONFIRMATION' );
		var yesVal = (locYes) ? Birst.LM.Locale.get(locYes) : Birst.LM.Locale.get( 'LB_YES' );
		var noVal = (locNo) ? Birst.LM.Locale.get(locNo) : Birst.LM.Locale.get( 'LB_NO' );

		var win = Ext.create( 'Ext.window.Window', {
			title : titleVal,
			closable : false,
			cls : 'xb-modal-window xb-confirm confirm-delete',
			maximizable : false,
			resizable : false,
			draggable : false,
			id : id,
			width : 320,
			modal : true,
			listeners : {
				afterrender: function ( w ) {
					var header = w.header;
					header.setHeight( 36 );
				}
			},
			buttons : [
				{
					text : yesVal,
					cls : 'xb-modwin-submit-btn',
					handler : function(){
						if(yesHandler()){
							win.close();
						}
					}
				},
				{
					text : noVal,
					cls : 'xb-modwin-submit-btn',
					handler : function() {
						win.close();
					}
				}
			],
			items: [ content ]
		} );
		win.show();
	},

	showConfirmCopy : function( button, dashboardTab ) {
		var page = button.page;
		var pageName = page.getAttribute('name');
		var copyPageName = Birst.LM.Locale.get('CPN_copyOf') + ' ' + pageName;

		var nameTxtBox = Ext.create('Ext.form.field.Text', {
			fieldLabel: Birst.LM.Locale.get('RD_new'),
			allowBlank: false,
			value: copyPageName,
			width: 295,
			regex: /^[^\\.<>:"/\\\\|\\?\\*]{1,64}$/,
			regexText: Birst.LM.Locale.get('DT_regexError'),
			maxLength: 64,
			enforceMaxLength: true,
			allowOnlyWhitespace: false
		});

		var copyFn = function(){
			var newPageName = nameTxtBox.getValue();
			if(!newPageName || newPageName == '' || !nameTxtBox.validate()){
				return false;
			}
			Birst.core.Webservice.dashboardRequest('copyPage',
				[
					{key: 'dir', value: page.getAttribute('dir') },
					{key: 'fromPage', value: page.getAttribute('name')},
					{key: 'toPageName', value: newPageName}
				],
				this,
				function(response, options) {
					//Insert the new page button next to the page button
					var btnGroup = button.findParentByType( 'buttongroup' );
					if(btnGroup){
						var xmlObj = response.responseXML;
						var pageXml = xmlObj.getElementsByTagName('Page');
						if(pageXml.length > 0){
							var btn = this.createPageButton(dashboardTab, pageXml[0]);
							btnGroup.insert(btnGroup.items.indexOf(button) + 1, btn);
							dashboardTab.dashboard.appendChild(pageXml[0]);
						}
					}
				}
			);
			return true;
		}.bind(this);

		this.showConfirmDlg('db_win_confirm_copy', copyFn, nameTxtBox, 'DT_copy', 'LB_SAVE', 'LB_CANCEL');
	},

	onRenderCtxEditFn: function(buttonObj, page, dashboardTab){
		var fn = this.contextMenuEditFn(page, dashboardTab, buttonObj);
		buttonObj.on('render', function(me){
			me.getEl().on('contextmenu', fn);
		});
	},

	refreshActivePage : function() {

	},

	displayPageButtons: function( dashboardTab ){
		var firstButton = null;
		var that = this;
		//Pages tab
		var pageBttnGroup = {
			xtype: 'buttongroup',
			id: 'pagesBttnGroup',
			cls: 'pagesBttnGroup'
		};
		pageBttnGroup.items = [];

		var hmap = new Ext.util.HashMap();

		var page = dashboardTab.dashboard.firstChild;
		while (page) {
			if (page.nodeName === 'Page') {
				var pname = page.getAttribute('name');

				var buttonObj = this.createPageButton(dashboardTab, page);

				if (dashboardTab.pagesHash && dashboardTab.pagesHash.containsKey(pname) ){

					var pref = dashboardTab.pagesHash.get(pname);
					if (pref.pagePromptContainer) {
						buttonObj.pagePromptContainer = pref.pagePromptContainer;
						buttonObj.pageController = pref.pageController;

					}
					if (pref.isLastActive === true)
						firstButton = buttonObj;
				}

				if (!firstButton && buttonObj.hidden === false){
					firstButton = buttonObj;
				}
				if (this.pageToOpen && this.pageToOpen === pname) {
					firstButton = buttonObj;
				}
				pageBttnGroup.items.push(buttonObj);

				hmap.add(page.getAttribute('name'),buttonObj);
			}
			page = page.nextSibling;
		}
		if(Birst.core.DashboardSettings.get('EditDashboard') === "true"){
			var whitelist = [ 'ricks@birst.com', 'dvictoria@birst.com', 'sfoo@birst.com', 'rmcginty@birst.com', 'rmcginty', 'jtherell@birst.com', 'jsaiyed@birst.com', 'bborna@birst.com' ];
			var isWhiteListed = whitelist.indexOf( Birst.core.DashboardSettings.get( 'Username' ) ) !== -1;
			var addBtn = Ext.create( 'Ext.Button', {
				text : '+',
				scope : this,
				cls : 'x-btn-add-page',
				tooltip : Birst.LM.Locale.get( 'LB_ADD_PAGE' ),
				handler : function() {
					var params = document.location.search;
					if( ( params && params.indexOf( 'newdash' ) !== -1 ) && isWhiteListed ) {
						document.location.href = '/Dashboards2.0/#/dashboards/' + encodeURI( dashboardTab.title ) + '/page';
					} else {
						that.editPage( dashboardTab, undefined, undefined, 0 );
					}
				}
			} );
			pageBttnGroup.items.push(addBtn);
		}
		dashboardTab.pagesHash = hmap;

		var pagesTabContainer = this.getPagesTabContainer();
		pagesTabContainer.removeAll();

		var pageBttnPanel = Ext.create('Birst.btntab.Panel',{
			items: pageBttnGroup
		});

		pagesTabContainer.add(pageBttnPanel);

		if (firstButton) {
			firstButton.toggle();
			this.onPageClick(firstButton, "click");
		}
	},

	createPageButton: function(dashboardTab, page){
		var pname = page.getAttribute('name');

		var isInvisible = false;
		var invisibleEls = page.getElementsByTagName('Invisible');
		if(invisibleEls && invisibleEls.length > 0){
			isInvisible = getXmlValue(invisibleEls[0], 'Invisible') === 'true' && Birst.core.DashboardSettings.get('EditDashboard')=="false";
		}

		var buttonObj = Ext.create('Ext.Button', {
			text: pname,
			page: page,
			scope: this,
			hidden: isInvisible,
			handler: this.onPageClick,
			toggleGroup: dashboardTab.dashboard.getAttribute("name"),
			listeners : {
				removeBtn : function() {
					dashboardTab.dashboard.removeChild( this.page );
					if( !dashboardTab.dashboard.hasChildNodes() ) {
						dashboardTab.dashboard.parentNode.removeChild( dashboardTab.dashboard );
						dashboardTab.fireEvent( 'removeTab' );
					}
				}
			}
		} );

		//Show edit dashboard page option on right click context menu
		if(Birst.core.DashboardSettings.get('EditDashboard') === "true"){
			this.onRenderCtxEditFn(buttonObj, page, dashboardTab);
		}
		buttonObj.dashbname = dashboardTab.title;

		return buttonObj;
	},

	addNewPage : function() {
		//TODO: Add Page
	}

});
