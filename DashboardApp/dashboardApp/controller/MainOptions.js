Ext.define('Dashboard.controller.MainOptions', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            selector: 'viewport #optionsContainer',
            ref: 'optionsContainer'
        }
    ],

    views: ['ManageReportSchedules'],

    init: function(){
     this.control({
         '#optionsContainer': {
             render: this.onRendered
         }
     });
    },

    onRendered: function(container, opts){
        /*
        var undo = Ext.create('Ext.Button', {
            id: 'undo',
            text: 'undo',
            handler: function(){

            }
        });
        container.add(undo);
        */

        var toggleNP = Ext.create('Ext.Button', {
            id: 'togglePromptView',
            cls: 'icon-toggle',
            iconCls: Birst.dashboards.doNP ? 'icon-toggle-classic' : 'icon-toggle-np',
            tooltip: Birst.dashboards.doNP ? Birst.LM.Locale.getNBS('PR_CLASSIC') : Birst.LM.Locale.getNBS('PR_NP'),
            handler: function(){
                Birst.Utils.Cookie.savePromptViewStyleCookie(!Birst.dashboards.doNP);
                document.location.reload(true);
            }
        });
        container.add(toggleNP);


        var sched = Ext.create('Ext.Button', {
                id: 'id-manage-reps-schedules',
                scope: this,
                cls: 'mngSchedules',
                tooltip: Birst.LM.Locale.get('SCHEDL_TIP'),
                handler: function(){
                    var view = Ext.widget('mngschedules');
                }
        });
        container.add(sched);

        var refresh = Ext.create('Ext.Button', {
            text: Birst.LM.Locale.get('LB_REFRESH'),
            id: 'refresh',
            scope: this,
            tooltip: Birst.LM.Locale.get('LB_REFRESH'),
            handler: function(){
                var tabCtrl = this.application.getController('DashboardTabLists');
                if (tabCtrl.pageController) {
                    tabCtrl.pageController.refresh();
                } else {
                    Ext.create('Birst.Util,ErrorDialog', {
                        msg: 'page undefined',
                        btnText: 'OK'
                    }).show();
                }
                return;
            }
        });
        container.add(refresh);

    }

});