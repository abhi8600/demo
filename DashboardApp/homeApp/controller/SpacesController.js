var BirstRS = /rs=true$/.test(document.URL);
Ext.define('DefaultHome.controller.SpacesController', {

	extend: 'Ext.app.Controller',

	views: ['SpacesListPanel', 'SpaceOptionsPanel'],

	// views the controller cares about
	refs: [
		{
			selector: 'viewport #idHeaderContainer',
			ref: 'headerContainer'
		},
		{
			selector: 'viewport #idLoggedUserOptionsContainer',
			ref: 'loggedUserContainer'
		},
		{
			selector: 'viewport #idMySpacesMainContainer',
			ref: 'spacesMainContainer'
		},
		{
			selector: 'viewport #idSpacesListPanel',
			ref: 'spacesListContainer'
		},
		{
			selector: 'viewport #idSpaceOptionsContainer',
			ref: 'spaceOptionsContainer'
		},
		{
			selector: 'spaceOpts',
			ref: 'spaceOptionsPanel'
		},
		{
			selector: 'panel[id=copySpace]',
			ref: 'copySpace'
		},
		{
			selector: 'panel[id=removeFromSpace]',
			ref: 'removeFromSpace'
		},
		{
			selector: 'panel[id=inviteAccept]',
			ref: 'inviteAccept'
		}
	],

	selectedSpaceDesc: {
		spaceId: null,
		spaceName: null
	},

	// properties returned from the server
	spaceSettings: {
		spaceName: null,
		spaceID: null,
		owner: null,
		ownerUserName: null,
		administrator: null,
		adhoc: null,
		dashboards: null,
		quickDashboard: null,
		enableAdhoc: null,
		enableDashboards: null,
		enableQuickDashBoards: null,
		availabilityCode: null,
		spaceMode: null,
		adminAccess: {
			manageCatalog: null,
			publish: null,
			useTemplatetrue: null,
			servicesAdmin: null,
			copySpace: null,
			customFormula: null,
			shareSpace: null,
			quartzSchedulerEnabled: null,
			showOldScheduler: null
		},
		isAdminAccess: null
	},

	store: null,

	model: null,

	grid: null,

	opts: null,

	init: function () {

		store = Ext.create('Ext.data.ArrayStore', {
			autoDestroy: false,
			idIndex: 0,
			fields: [
				'SpaceName',
				'SpaceID',
				'OwnerUsername',
				'Owner',
				'AvailabilityCode',
				'IsInvite',
				'TrialVisualizer'
			]
		});
		this.store = store; //make the store public (needed for jasmine tests)
		grid = Ext.create('Ext.grid.Panel', {
			store: store,
			hideHeaders: true,
			autoScroll: true,
			scroll: true,
			multiSelect: false,
			cls: 'spaceList',
			columns: [
				{ text: 'type', dataIndex: 'Owner', sortable: true, width: 50, renderer: function (v) { return '<p/>'; }  },
				{ text: 'Space', dataIndex: 'SpaceName', sortable: true, flex: 1 },
				{ text: 'User', dataIndex: 'OwnerUsername', sortable: true, width: 150 },
				{ text: 'visualizer', dataIndex: 'TrialVisualizer', sortable: true, width: 66,
					renderer : function( v ) {
						if( v === true ) {
							return '<img class="spaceListItemVis alt="Visualizer" src="resources/images/red-ribbon.svg" />';
						}
					}
				}
			],
			viewConfig: {
				listeners: {
					scope: this,
					itemclick: function (dataview, record, item, index, e) {
						this.processSpaceSelected(record);
					}
				},
				getRowClass: function(record, index) {
					var c = record.get('Owner');
					if (c == "true") {
						return 'owner';
					}
					else {
						if (!record.get('IsInvite')) {
							return 'user';
						}
						else
							return 'invite';
					}
				}
			}

		});

		// Create the opts & nav (my-spaces's right-hand pane) panel

		opts = Ext.widget('spaceOpts');

		grid.on('rowselect',
				function (rowIndex, columnIndex, e) {
					// Todo:
				},
				this);

		this.control({
			'#idMySpacesMainContainer': {
				render: this.onSpacesRender
			},
			'#idSpacesListPanel': {
				render: this.onSpacesListContainerRender
			},
			'idSpaceOptionsContainer': {
				render: this.onSpacesOptionsContainerRender
			},
			'spaceOpts': {
				render: function () { /*alert('View was rendred.'); */ }
			},
			'panel[id=copySpace]': {
				click: function () {
					this.onCopySpace();
				}
			},
			'panel[id=deleteSpace]': {
				click: function () {
					this.onDeleteSpace();
				}
			},
			'button[name=addSpaceBtn]': {
				click: function () {
					this.onCreateNewSpaceNew(this);
				}
			},
			'panel[id=removeFromSpace]': {
				click: function () {
					this.onRemoveFromSpace();
				}
			},
			'button[id=inviteAccept]': {
				click: function () {
					this.onInviteAccept(this);
				}
			},
			'button[id=inviteDecline]': {
				click: function () {
					this.onInviteDecline(this);
				}
			},
			'button[name=freetrialnewsSpaceBtn]': {
				click: function () {
					this.onCreateNewSpaceNew(this);
				}
			}
		});
	},

	onSpacesRender: function () {
		var spaces = this.getSpacesMainContainer();
	},

	onSpacesListContainerRender: function () {
		var spaces = this.getSpacesListContainer();
		spaces.add(grid);
		var optsp = this.getSpaceOptionsContainer();
		optsp.add(opts);
	},

	onSpacesOptionsContainerRender: function () {
		var optsp = this.getSpaceOptionsContainer();

	},
	start: function () {

		this.getSpacesFromService();
	},

	hanleAllSpacesResponse: function (xmlDoc) {

		// Selected space
		var response = xmlDoc.getElementsByTagName('GetAllSpacesResponse');

		if (response) {
			var option = response[0].firstChild;
			while (option != null) {

				Birst.Utils.User.userName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(option, 'UserName'));
				Birst.Utils.User.userMode = getXmlChildValue(option, 'UserMode');
				Birst.Utils.User.lastLoginDate = getXmlChildValue(option, 'LastLoginDate');
				Birst.Utils.User.selectedSpace = getXmlChildValue(option, 'Selected');
				Birst.Utils.User.enableCopy = getXmlChildValue(option, 'EnableCopy');
				Birst.Utils.User.canCreateNewSpace = getXmlChildValue(option, 'canCreateNewSpace');
				Birst.Utils.User.enableUserManagement = getXmlChildValue(option, 'EnableUserManagement');
				Birst.Utils.User.showTemplate = getXmlChildValue(option, 'showTemplate');
				Birst.Utils.User.sizeOfSpaceOwned = getXmlChildValue(option, 'SizeOfSpaceOwned');
				Birst.Utils.User.externalDBEnabled =  getXmlChildValue(option, 'ExternalDBEnabled');
				//TODO: Impelement Banner & Banner Service Call for Opt Out Action
				Birst.Utils.User.optOutVisualizerBanner = getXmlChildValue(option, 'OptOutVisualizerBanner') === 'true';



				if (getXmlChildValue(option, 'ShowFreeTrialPage') == 'true' || getXmlChildValue(option, 'ShowDiscoveryFreeTrialPage') == 'true') {
					Birst.Utils.User.showFreeTrial = 'true';
				} else {
					Birst.Utils.User.showFreeTrial = 'false';
				}

				if (getXmlChildValue(option, 'ShowDiscoveryFreeTrialPage') == 'true') {
					Birst.Utils.User.isDiscoveryFreeTrial = true;
				} else {
					Birst.Utils.User.isDiscoveryFreeTrial = false;
				}

				if (Birst.Utils.User.canCreateNewSpace == 'true') {
					Ext.getCmp('bid-header-addspc-btn').show();
				}
				else {
					Ext.getCmp('bid-header-addspc-btn').hide();
				}
				option = option.nextSibling;
			}

			Ext.getCmp('headerMenuLabel').setText(Birst.Utils.User.userName);
			Ext.DomHelper.insertBefore( 'idHomeMainContainer', {
				tag : 'span',
				cls : 'lastLogin',
				html : Birst.LM.Locale.get( 'LB_LAST_LOGIN' ) + ': ' + Birst.Utils.User.lastLoginDate
			} );
		}

		if (Birst.Utils.User.showFreeTrial == 'true') {
			Ext.getCmp('bid-header-addspc-btn').hide();
			var ctl = Ext.getCmp ('freeTrialUserPane');
			if (ctl) {
				ctl.show();
				if (Birst.Utils.User.isDiscoveryFreeTrial) {
					var ttlinktext = '<a style="text-decoration:none;" href="http://www.birst.com/learn/free-trial/tutorials" target="_blank">'+ Birst.LM.Locale.get('DISCOVERY_FREETRIAL_TUTORIAL_LINK') +'</a>';
					var wlcmtext = '<p style="text-decoration:none;">' + Birst.LM.Locale.get('DISCOVERY_FREETRIAL_WELLCOME_MSG') +'</p>';
					ctl.items.getAt(0).setText(ttlinktext, false);
					ctl.items.getAt(1).setText(wlcmtext, false);
				}
			}
		}

		// Add spaces to store
		var spaceName;
		var spaceId;
		var ownerName;
		var availCode;
		var cntInvites;
		var trialVisualizer;

		store.removeAll();

		Ext.suspendLayouts();


		var invites = xmlDoc.getElementsByTagName('Invites');
		if (invites && invites[0] ) {
			var space  = invites[0].firstChild;
			cntInvites = 0;
			while (space != null) {
				spaceName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(space, 'SpaceName'));
				spaceId = getXmlChildValue(space, 'SpaceID');
				ownerName = getXmlChildValue(space, 'InviteUserEmail');
				if (spaceName && spaceId && ownerName) {
					//store.add({ SpaceName: spaceName, SpaceID: spaceId, OwnerUsername: ownerName, Owner: false, AvailabilityCode: -1 });
					store.add({ SpaceName: spaceName, SpaceID: spaceId, OwnerUsername: ownerName, Owner: false, AvailabilityCode: -1, IsInvite:true });
				}
				space = space.nextSibling;
				cntInvites++;

			}
		}

		if (xmlDoc.getElementsByTagName('serverConfig') != false) {
			Birst.Utils.User.isApplianceMode = getXmlValue(xmlDoc.getElementsByTagName('IsApplianceMode')[0]);
			Birst.Utils.User.allowOverrideSpaceSchema = getXmlValue(xmlDoc.getElementsByTagName('AllowOverrideSpaceSchema')[0]);
		}
		if (Birst.Utils.User.isApplianceMode && Birst.Utils.User.isApplianceMode == "true"  ){
			Birst.Utils.User.isApplianceMode = true;
		}else{
			Birst.Utils.User.isApplianceMode = false;
		}
		if (Birst.Utils.User.allowOverrideSpaceSchema && Birst.Utils.User.allowOverrideSpaceSchema == "true"  ){
			Birst.Utils.User.allowOverrideSpaceSchema = true;
		}else{
			Birst.Utils.User.allowOverrideSpaceSchema = false;
		}


		var spaces = xmlDoc.getElementsByTagName('SpacesList');
		if (spaces && spaces.length > 0) {
			var spc = spaces[0].firstChild;
			var cntSpcs = 0;

			while (spc != null) {
				spaceName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(spc, 'SpaceName'));
				spaceId = getXmlChildValue(spc, 'SpaceID');
				ownerName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(spc, 'OwnerUsername'));
				owner = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(spc, 'Owner'));
				availCode = getXmlChildValue(spc, 'AvailabilityCode');
				//TODO: Remove || When Complete
				trialVisualizer = getXmlChildValue( spc, 'TrialVisualizer' ) === 'true';
				if (spaceName && spaceId && ownerName && owner && availCode) {
					store.add({ SpaceName: spaceName, SpaceID: spaceId, OwnerUsername: ownerName, Owner: owner, AvailabilityCode: availCode, IsInvite:false, TrialVisualizer : trialVisualizer });
				}
				spc = spc.nextSibling;
				// is this a selected space
				if (cntSpcs == parseInt( Birst.Utils.User.selectedSpace, 10 ) ) {
					this.selectedSpaceDesc.spaceName = spaceName;
					this.selectedSpaceDesc.spaceId = spaceId;
				}
				cntSpcs++;
			}
		}


		Ext.resumeLayouts(true);

		// set corresponding row in grid
		grid.getView().select( parseInt( Birst.Utils.User.selectedSpace, 10 ) + cntInvites );

		// update space nav/space options view
		// this.getSpaceOptionsPanel().updateView(this.spaceSettings, Birst.Utils.User);

		// get additional details for a logged user and a selected space
		Birst.core.Webservice.acornRequest('GetSpaceDetails',
			[
				{ key: 'spaceID', value: this.selectedSpaceDesc.spaceId },
				{ key: 'spaceName', value: this.selectedSpaceDesc.spaceName }
			],
			this,
			function (response, options) {
				this.handleSpaceDetailsResponse(response.responseXML);
			}
		);

		if ( !Birst.Utils.User.isDiscoveryFreeTrial && detectPepperFlash() && !this.pepperFlashPopped ){
			this.popupPepperFlashBanner();
		} else if( Birst.Utils.User.isDiscoveryFreeTrial && !Birst.Utils.User.optOutVisualizerBanner ) {
			this.popupVisualizerTrialBanner();
		}

	},

	setOptOutTrialBanner : function( option ) {
		Birst.core.Webservice.acornRequest('setVisualizerOptOut',
			[
				{ key: 'optOut', value: option }
			],
			this,
			//success
			function (response, options) {
				//Opt out complete
			},
			//fail
			function () {
				alert( 'An error occured processing the opt out reeuest' );
			},
			undefined,
			undefined,
			true
		);
	},

	popupVisualizerTrialBanner : function() {
		var that = this,
			modal = Ext.DomHelper.append( document.getElementsByTagName( 'body')[0], {
			cls : 'modal-container',
			children : [
				{
					cls : 'modal-window modal-visualizer',
					tag : 'aside',
					children : [
						{
							tag : 'img',
							src : 'resources/images/red-ribbon.svg',
							cls : 'vz-ribbon',
							alt : ''
						},
						{
							cls : 'modal-overflow',
							children : [
								{
									tag : 'header',
									children : [
										{
											tag : 'span',
											html : 'close',
											cls : 'modal-close txt-replace'
										},
										{
											tag : 'hgroup',
											cls : 'txt-replace',
											children : [
												{
													tag : 'h1',
													html : Birst.LM.Locale.get( 'LB_VISUALIZER' )
												},
												{
													tag : 'h2',
													html : Birst.LM.Locale.get( 'LB_QUICKLY_VISUALIZE' )
												},
											]
										}
									]
								},
								{
									tag : 'p',
									html : Birst.LM.Locale.get( 'MSG_VISUALIZER_TRIAL' )
								},
								{
									cls : 'modal-visualizer-splash'
								},
								{
									tag : 'input',
									type : 'checkbox',
									id : 'opt-out-vis-banner',
									name : 'opt-out-vis-banner'
								},
								{
									tag : 'label',
									htmlFor : 'opt-out-vis-banner',
									html : Birst.LM.Locale.get( 'LB_DONT_SHOW' )
								}
							]
						}
					]
				}
			]
		} );

		function handleResize() {
			var mWin = modal.querySelector( '.modal-window' );
			var halfHeight = parseInt( mWin.offsetHeight / 2, 10 );
			mWin.setAttribute( 'style', 'margin-top: -' + halfHeight + 'px' );
		}

		window.setTimeout( function() {
			modal.className += ' modal-fade';
			handleResize();
		}, 1 );

		window.addEventListener( 'resize', handleResize, false );

		var _modal = Ext.get( modal );

		_modal.on( 'click', function( e ) {
			var testClass = e.target.className;
			if( testClass.indexOf( 'modal-container' ) >= 0 || testClass.indexOf( 'modal-close' ) >=0 ) {
				_modal.destroy();
			}
		}, this );

		_modal.on( 'change', function( e ) {
			if( e.target.checked ) {
				this.setOptOutTrialBanner( 'true' );
			} else {
				this.setOptOutTrialBanner( 'false' );
			}
		}, this, { delegate : '#opt-out-vis-banner' } );
	},

	popupPepperFlashBanner : function() {
		var regex = /Login.aspx/;
		if( regex.test( document.referrer ) ) {
			Ext.create('widget.uxNotification', {
				position: 't',
				cls: 'ux-notification-light',
				closable: true,
				title: '',
				width: 400,
				iconCls: 'ux-notification-icon-information',
				items: [{
					xtype: 'label',
					html: '<p style="text-decoration:none;text-align:center">'+Birst.LM.Locale.get('PEPPR_WARNING_MSG')+'</p>'
				},{
					xtype: 'label',
					html: '<a style="text-decoration:none;" href="../Help/full/Disabling_the_Chrome_Pepperflash_Plug_In.htm" target="_blank">'+ 'Disabling the Chrome Pepper Flash Plug-In'+'</a>',
					padding: '10 0 0 10'
				}],
				autoDestroyDelay: 6000,
				slideInDelay: 800,
				slideDownDelay: 500,
				slideDownAnimation: 'easeIn',
				paddingY: 49
			}).show();
			this.pepperFlashPopped = true;
		}
	},

	// recieved response from server
	handleSpaceDetailsResponse: function (xmlDoc) {

		// parse a response and update the view accordingly

		var response = xmlDoc.getElementsByTagName('GetSpaceDetailsResponse');
		var spaceSettings = {};

		spaceSettings.adminAccess = {};
		this.spaceSettings = {};
		if (response) {
			var option = response[0].firstChild;
			while (option != null) {
				spaceSettings.spaceName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(option, 'SpaceName'));
				spaceSettings.spaceID = getXmlChildValue(option, 'SpaceID');
				spaceSettings.owner = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(option, 'Owner'));
				spaceSettings.ownerUserName = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(option, 'OwnerUsername'));
				spaceSettings.administrator = getXmlChildValue(option, 'Administrator');
				spaceSettings.adhoc = getXmlChildValue(option, 'Adhoc');
				spaceSettings.dashboards = getXmlChildValue(option, 'Dashboards');
				spaceSettings.enableAdhoc = getXmlChildValue(option, 'EnableAdhoc');
				spaceSettings.enableDashboards = getXmlChildValue(option, 'EnableDashboards');
				spaceSettings.enableVisualizer = getXmlChildValue(option, 'EnableVisualizer') === 'true';
				spaceSettings.spaceComments =  Birst.Utils.Safe.getEscapedValue(getXmlChildValue(option, 'SpaceComments'));
				spaceSettings.lastUpload = getXmlChildValue(option, 'LastUpload');
				spaceSettings.spaceSize = getXmlChildValue(option, 'SpaceSize');
				spaceSettings.availabilityCode = getXmlChildValue(option, 'AvailabilityCode');
				spaceSettings.denyAddSpace = getXmlChildValue(option, 'DenyAddSpace');

				option = option.nextSibling;
			}
		}
		this.spaceSettings = spaceSettings;
		this.selectedSpaceDesc.spaceName = spaceSettings.spaceName;
		this.selectedSpaceDesc.spaceId = spaceSettings.spaceID;

		var adminacc = xmlDoc.getElementsByTagName('AdminAccess');
		if (adminacc && adminacc.length > 0) {
			spaceSettings.isAdminAccess = true;
		}else{
			spaceSettings.isAdminAccess = false;
		}

		// unmask view
		this.getSpaceOptionsContainer().unmask();

		// update space nav/space options view
		this.getSpaceOptionsPanel().updateView(this.spaceSettings, Birst.Utils.User);
	},

	searchStoreByField: function(fieldName,fieldValue) {
		var index = store.find(fieldName, fieldValue);
		if (index >= 0) {
				var record = store.getAt(index);
				return record;
		}

		return null;
	},
	processSpaceSelected: function (record) {

		// Store the id of the selected space for future processing (e.g., needed by invite)
		this.selectedSpaceDesc.spaceName = record.data.SpaceName;
		this.selectedSpaceDesc.spaceId = record.data.SpaceID;
		this.selectedSpaceDesc.spaceOwner = record.data.OwnerUsername; //an email if an invite

		// Mask view/buttons while request is being processed
		//if (record.data.AvailabilityCode != -1) {
		if (record.get('IsInvite')) {
			this.getSpaceOptionsPanel().createInviteView(record);
		} else {
			this.getSpaceOptionsContainer().mask();
			this.getSpaceDetailsFromService(record);
		}

	},

	getSpacesFromService: function () {

		Birst.core.Webservice.acornRequest('GetAllSpaces',
			[],
			this,
			function (response, options) {
				this.hanleAllSpacesResponse(response.responseXML);
			}
		);
	},

	getSpaceDetailsFromService: function (record) {

		Birst.core.Webservice.acornRequest('GetSpaceDetails',
			[
				{ key: 'spaceID', value: record.data.SpaceID },
				{ key: 'spaceName', value: record.data.SpaceName }
			],
			this,
			function (response, options) {
				this.handleSpaceDetailsResponse(response.responseXML);
			}
		);
	},

	onCopySpace: function () {

		var cps = Ext.create('Ext.panel.Panel', {
			plain: true,
			border: 0,
			bodyCls: 'xb-panel-body-default',
			fieldDefaults: {
				anchor: '100%',
				msgTarget: 'under'
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},

			items: [
				{
					xtype: 'label',
					cls: 'xb-modw-label-caption',
					text: Birst.LM.Locale.get('DLG_COPYSPACE_MSG_LB'),
					padding:  '0 0 12 0'
				},
				{
					xtype: 'textfield',
					fieldLabel: Birst.LM.Locale.get('DLG_COPYSPACE_NAME_LB'),
					labelAlign: 'top',
					labelSeparator: '',
					labelStyle: 'font-size: 14px; color: #656565 !important',
					width:100,
					name: 'newSpaceNameFld',
					regex:   /^[^\\.\/<>:|*?"]{1,255}$/, //"
					regexText: Ext.String.format(Birst.LM.Locale.get('VLD_SPACENAME_MAXLEN_LB'),255)+'.,<,>,:,\",/,\,|,?,*',
					allowBlank: false,
					padding: '0 0 4 0'

				},
				{
					xtype: 'textfield',
					fieldLabel: Birst.LM.Locale.get('LB_SCHEMA'),
					name: 'copySpaceSchemaFld',
					labelAlign: 'top',
					hidden: Birst.Utils.User.isApplianceMode && Birst.Utils.User.allowOverrideSpaceSchema && Birst.Utils.User.showFreeTrial != 'true' ? false : true,
					labelSeparator: '',
					labelStyle: 'font-size: 14px; color: #656565 !important',
					width: 100,
					allowBlank: true,
					padding: '0 0 4 0',
					//reqex: Birst.Utils.Validation.textfieldValidationRegEx255L,
					regex: /^[^\\.\/<>:|*?"]{1,255}$/, //"
					regexText: Ext.String.format(Birst.LM.Locale.get('VLD_SPACENAME_MAXLEN_LB'), 255) + '.,<,>,:,\",/,\,|,?,*',
					listeners: {
						afterrender: function(el){
							Ext.create('Ext.tip.ToolTip', { target: el.getEl(), html: Birst.LM.Locale.get('DLG_SPACE_SCHEMAFIELD_TOOLTIP')});
						}
					}
				},
				{
					xtype: 'textarea',
					fieldLabel: Birst.LM.Locale.get('DLG_COPYSPACE_COMMENTS_LB'),
					labelAlign: 'top',
					labelSeparator: '',
					labelStyle: 'font-size: 14px; color: #656565 !important',
					width:100,
					name: 'newSpaceCommentsFld',
					padding: '0 0 4 0',
					flex: 1,
					regex: /[*]{0,400}$/,
					regexText: Ext.String.format(Birst.LM.Locale.get('VLD_COMMENTS_MAXLEN_LB'), 400)
				}
			]
		});

		var win = Ext.create('Ext.window.Window', {
			// UX design - has a primary and secondary title parts

			title: '<span> '+Birst.LM.Locale.get('DLG_COPYSPACE_TITLE_LB')+': <span class="cl-title-secondary">' + Birst.Utils.Safe.getEscapedValue(this.selectedSpaceDesc.spaceName)+ '</span> </span>',
			closable:false,
			resizable: false,
			draggable: false,
			id: 'bid-sp-mod-dlg',
			cls: 'xb-modal-window',
			bodyPadding: 15,
			minHeight: 400,
			width: 680,
			layout: 'fit',
			modal:true,
			listeners : {
				afterrender : function(w) {
					var header = w.header;
					header.setHeight(36);
				}
			},
			items: cps,
			tools:[{
				xtype: 'button',
				iconCls: 'closeModalIcon',
				cls: 'xb-modwin-close-btn',
				overCls: 'xb-modwin-close-btn-over',
				tooltip: Birst.LM.Locale.get('DLG_COPYSPACE_BTN_CLS_LB'),
				handler: function(ev, te, p){
					this.ownerCt.ownerCt.destroy();
				}
			}],
			dockedItems: [{
				xtype: 'toolbar',
				ui: 'footer',
				dock: 'bottom',
				height: 60,
				cls: 'xb-docked-base-cl',
				layout: {
					pack: 'center'
				},
				items: ['->',{
					xtype: 'button',
					minWidth: 80,
					text: Birst.LM.Locale.get('DLG_COPYSPACE_TITLE_LB'),
					cls: 'xb-modwin-submit-btn',
					overCls: 'xb-submit-btn-over',
					pressedCls: 'xb-footer-btn-press',
					scope: this,
					handler: function () {

						if (!Ext.ComponentQuery.query('textfield[name=newSpaceNameFld]')[0].isValid())
							return; // validation failed
						var schemaName="";
						if (Ext.ComponentQuery.query('textfield[name=copySpaceSchemaFld]')[0].isVisible() ) {
							schemaName = Ext.ComponentQuery.query('textfield[name=copySpaceSchemaFld]')[0].getValue();
						}

						Birst.core.Webservice.acornRequest('CreateSpaceCopy',
							[
								{ key: 'spaceName', value: Ext.ComponentQuery.query('textfield[name=newSpaceNameFld]')[0].getValue() },
								{ key: 'schemaName', value: schemaName},
								{ key: 'copyComments', value: Ext.ComponentQuery.query('textarea[name=newSpaceCommentsFld]')[0].getValue() }
							],
							this,
							function (response, options) {
								var err_type, err_msg;
								var stat = response.responseXML.getElementsByTagName('CreateSpaceCopyResult');
								var res = stat[0].firstChild;
								while (res != null) {
									err_type = getXmlChildValue(res, 'ErrorType');
									err_msg = getXmlChildValue(res, 'ErrorMessage');
									res = res.nextSibling;
								}
								if (err_type && err_type != 0){
									// display error message
									var win = Ext.create('Birst.Util,ErrorDialog', {
										msg: err_msg,
										btnText: 'OK'
									});
									win.show();
								}
								else
									this.getSpacesFromService();

								Ext.ComponentQuery.query('window[id=bid-sp-mod-dlg]')[0].destroy();
							}, null, null, false
						);
					}
				}]
			}]
		});
		win.show();
	},

	onDeleteSpace: function () {

		var win = Ext.create('Birst.Util,ConfirmDialog', {
			title: '<span> '+Birst.LM.Locale.get('DLG_DELETESPACE_TITLE_LB')+' <span class="cl-title-secondary">' + Birst.Utils.Safe.getEscapedValue(this.selectedSpaceDesc.spaceName)+ '</span> </span>',
			msg: Birst.LM.Locale.get('DLG_DELETESPACE_MSG_LB'),
			fn: this.doDeleteSpace,
			btnText: Birst.LM.Locale.get('DLG_DELETESPACE_BTNDEL_LB'),
			scope: this
		});
		win.show();
	},

	doDeleteSpace: function () {

		Birst.core.Webservice.acornRequest('DeleteSpace',
			[],
			this,
			function (response, options) {
				this.getSpacesFromService();
			}, null, null, true
		);
	},

	onRemoveFromSpace: function () {

		var win = Ext.create('Birst.Util,ConfirmDialog', {
			title: Birst.LM.Locale.get('DLG_REMOVESPACE_TITLE_LB') +' '+this.selectedSpaceDesc.spaceName,
			msg:  Birst.LM.Locale.get('DLG_REMOVESPACE_MSG_LB'),
			fn: this.doRemoveFromSpace,
			btnText: Birst.LM.Locale.get('LB_REMOVE'),
			scope: this
		});
		win.show();
	},

	doRemoveFromSpace: function () {

	Birst.core.Webservice.acornRequest('RemoveMember',
		[],
		this,
		function (response, options) {
			this.getSpacesFromService();
		}, null, null, true
		);
	},

	onCreateNewSpaceNew: function (me) {

		var clustStore;
		var externalDBEnabled = Birst.Utils.User.externalDBEnabled ==  "true" ? true : false;
		var clustSelected;
		var connModeSelected="Birst";

		// Simple navigation scheme using bbar buttons
		// Update: As per new UX design, the 'next' button has now two roles: navigation and submit
		var wizardNav = function (wizard, direction) {
			var wizLayout = wizard.getLayout();
			// Submit
			if (!wizLayout.getNext() && direction == "next") {
				wizSubmit(wizard);
				return;
			}

			// external cloud selection validation
			if (externalDBEnabled && connModeSelected == "EXTERNAL" && direction == "next" && !clustSelected && wizLayout.getNext() && wizLayout.getPrev()) {
				Ext.create('Birst.Util,ErrorDialog', {
					msg: Birst.LM.Locale.get('DLG_ADDSPACE_CLOUDNOTSELECTED_MSG'),
					btnText: Birst.LM.Locale.get('LB_OK')
				}).show();
				return;
			}

			// Navigate
			wizLayout[direction]();

			if (externalDBEnabled && connModeSelected != "EXTERNAL" && wizLayout.getNext() && wizLayout.getPrev()) {
				wizLayout[direction]();
			}

			Ext.getCmp('wzbtn-prev').setDisabled(!wizLayout.getPrev());

			if (!wizLayout.activeItem.prev())
				Ext.getCmp('wzbtn-prev').hide();
			else
				Ext.getCmp('wzbtn-prev').show();

			if (!wizLayout.activeItem.next()) {
				Ext.getCmp('wzbtn-next').setText(Birst.LM.Locale.get('LB_DONE'));
			}
			else {
				Ext.getCmp('wzbtn-next').setText(Birst.LM.Locale.get('LB_NEXT'));
			}
		};

		var cloudtypes = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [
				{ "name": "BIRST", "value": Birst.LM.Locale.get('DLG_ADDSPACE_CLOUDSELECT_BIRST') },
				{ "name": "EXTERNAL", "value": Birst.LM.Locale.get('DLG_ADDSPACE_CLOUDSELECT_EXTERNAL') }
			]
		});

		var cloudCombo = Ext.create('Ext.form.ComboBox', {
			name: 'rd-sp-class',
			id: 'id-cloud-select-combo',
			store: cloudtypes,
			queryMode: 'local',
			displayField: 'value',
			valueField: 'name',
			typeAhead: false,
			forceSelection: true,
			disabled: externalDBEnabled ? false : true,
			editable: false,
			cls: 'columnSelector',
			style: 'margin:0px 2px 0px 170px',
			listeners: {
				scope: me,
				afterrender: function(cb){
					cb.select(cloudtypes.getAt(0));
					Ext.create('Ext.tip.ToolTip', { target: cb.getEl(), html: Birst.LM.Locale.get('DLG_ADDSPACE_CLOUDSELECT_TOOLTIP')});
				},
				select: function(cb) {
					connModeSelected = cb.value;
				}
			},
			listConfig: {
				cls: 'prompt-list'
			}
		});

		var wizardItems = [];
		if (Birst.Utils.User.showFreeTrial != 'true') {
			wizardItems.push({
				xtype: 'panel',
				id: 'bid-wzstep-1',
				border: 0,
				bodyPadding: 0,
				fieldDefaults: {
					anchor: '100%'
				},
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
						{ xtype: 'panel',
							border: 0,
							bodyPadding: 0,
							fieldDefaults: {
								anchor: '100%'
							},
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [
								{
									xtype: 'label',
									text: Birst.LM.Locale.get('DLG_ADDSPACE_MSG_LB'),
									cls: 'xb-modw-label-caption'
								},
								cloudCombo
							]
						},
						{ xtype: 'panel',
							plain: true,
							border: 0,
							bodyPadding: 5,
							fieldDefaults: {
								anchor: '100%'
							},
							layout: {
								type: 'vbox',
								align: 'stretch'
							},
							items: [{
								xtype: 'panel',
								plain: true,
								border: 0,
								bodyPadding: Ext.isIE ? '15 0 15 0' : 0,
								fieldDefaults: {
									anchor: '100%'
								},
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [{
									xtype: 'radio',
									boxLabel: Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_AUTOMATIC_LB'),
									boxLabelCls: 'bx-form-cb-label',
									name: 'rd-sp-type',
									inputValue: 'auto',
									checked: true,
									id: 'bid-sp-rdb-auto',
									flex: 0.4
								},{
									xtype: 'label',
									html: '<p class="xb-rdb-label-block">' + Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_AUTOMATIC_DESCR_LB') + '</p>',
									style: 'padding: 20 0 5 8',
									flex: 0.6
								}]
							},{
								xtype: 'panel',
								border: 0,
								bodyPadding: Ext.isIE ? '0 0 15 0' : 0,
								fieldDefaults: {
									anchor: '100%'
								},
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [{
									xtype: 'radio',
									boxLabel: Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_DISCOVERY_LB'),
									boxLabelCls: 'bx-form-cb-label',
									name: 'rd-sp-type',
									inputValue: 'auto',
									id: 'bid-sp-rdb-discovery',
									flex: 0.4
								},{
									xtype: 'label',
									html: '<p class="xb-rdb-label-block">' + Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_DISCOVERY_DESCR_LB') + '</p>',
									style: 'padding: 20 0 5 8',
									flex: 0.6
								}]
							},{
								xtype: 'panel',
								border: 0,
								bodyPadding: Ext.isIE ? '0 0 15 0' : 0,
								fieldDefaults: {
									anchor: '100%'
								},
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [{
									xtype: 'radio',
									boxLabel: Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_ADVANCED_LB'),
									boxLabelCls: 'bx-form-cb-label',
									name: 'rd-sp-type',
									inputValue: 'auto',
									id: 'bid-sp-rdb-advanced',
									flex: 0.4
								},{
									xtype: 'label',
									html: '<p class="xb-rdb-label-block">' + Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_ADVANCED_DESCR_LB') + '</p>',
									style: 'padding: 20 0 5 8',
									flex: 0.6
								}]
							},{
								xtype: 'panel',
								border: 0,
								bodyPadding: Ext.isIE ? '0 0 15 0' : 0,
								hidden: true,
								fieldDefaults: {
									anchor: '100%'
								},
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [{
									xtype: 'radio',
									boxLabel: Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_USAGETRACK_LB'),
									boxLabelCls: 'bx-form-cb-label-i',
									name: 'rd-sp-type',
									inputValue: 'auto',
									id: 'bid-sp-rdb-spacetracking',
									flex: 0.4
								},{
									xtype: 'label',
									html: '<p class="xb-rdb-label-block-i">' + Birst.LM.Locale.get('DLG_ADDSPACE_CHKB_USAGETRACK_DESCR_LB') + '</p>',
									style: 'padding: 20 0 5 8',
									flex: 0.6
								}],
								listeners: {
									scope: this,
									afterrender: function (me) {
										if (Birst.Utils.User.enableUserManagement == 'true') {
											me.show();
										}
									}
								}
							}
						]
						}
				]
			});

			// cluster selection step
			var renderRedshift = function () {
				if (externalDBEnabled) {
					clustStore = Ext.create('Ext.data.ArrayStore', {
						autoDestroy: false,
						idIndex: 0,
						fields: [
							'Type',
							'Name',
							'Description',
							'DBConnString',
							'DatabaseType'
					]
					});

					wizardItems.push({
						xtype: 'panel',
						id: 'bid-wzstep-2',
						border: 0,
						bodyPadding: 5,
						hideMode:'offsets',
						fieldDefaults: {
							anchor: '100%',
							msgTarget: 'under'

						},
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [{
							xtype: 'label',
							text: Birst.LM.Locale.get('DLG_ADDSPACE_CLOUDSELECT_MSG'),
							cls: 'xb-modw-label-caption',
							padding: Ext.isIE ? '16 4 25 4' : '10 4 25 4'

						},
							Ext.create('Ext.grid.Panel', {
								store: clustStore,
								id: 'id-conn-grid-panel',
								hideHeaders: false,
								height: 300,
								autoScroll: true,
								scroll: true,
								multiSelect: false,
								cls: 'spaceList',
								columns: [
											{ text: 'Name', dataIndex: 'Name', sortable: true, width: 100 },
											{ text: 'Database', dataIndex: 'DatabaseType', sortable: true, width: 150 },
											{ text: 'Connection', dataIndex: 'DBConnString', sortable: true, flex: 1 }
								],
								viewConfig: {
									listeners: {
										scope: this,
										itemclick: function (dataview, record, item, index, e) {
											clustSelected = record;
										}
									}
								},
								listeners: {
									beforerender: function (me,eop) {
											var header = me.header;
											me.columns[0].setHeight(28);
									}
								}
							})
						],
						listeners: {
							scope: this,
							activate: function (me) {
								if (clustStore.data.length == 0 && connModeSelected == "EXTERNAL") {
									Birst.core.Webservice.acornRequest('getSpaceTypes',
										[],
										me,
										function (response, options) {
											var types = response.responseXML.getElementsByTagName('SpaceTypes');
											if (types) {
												var type = types[0].firstChild;
												while (type != null) {
													var typeId = getXmlChildValue(type, 'Type');
													var name = !getXmlChildValue(type, 'Name') ? '' : getXmlChildValue(type, 'Name');
													var connStr = getXmlChildValue(type, 'DatabaseConnectString');
													var dbType = getXmlChildValue(type, 'DatabaseType');
													//console.log('typeId: ' + typeId + ' connStr: ' + connStr);
													clustStore.add({ Type: typeId, Name: name, DatabaseType: dbType, DBConnString: connStr });
													type = type.nextSibling;
												}
											}
											Ext.getCmp('id-conn-grid-panel').getView().refresh();
										}, null, null, true
									);
								}
							}
						}
					});
				}
			}();
		}

		wizardItems.push({
			xtype: 'panel',
			id: 'bid-wzstep-3',
			border: 0,
			bodyPadding: 5,
			//width: 320,
			fieldDefaults: {
				anchor: '100%',
				msgTarget: 'under'

			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'label',
				text: Birst.LM.Locale.get('DLG_ADDSPACE_NAMECOMMNTS_LB'),
				cls: 'xb-modw-label-caption',
				padding: Ext.isIE ? '16 4 5 4' : '10 4 5 4'
			}, {
				xtype: 'textfield',
				fieldLabel: Birst.LM.Locale.get('LB_NAME'),
				name: 'addNewSpaceNameFld',
				labelAlign: 'top',
				labelSeparator: '',
				labelStyle: 'font-size: 14px; color: #656565 !important',
				minWidth: 160,
				allowBlank: false,
				padding: Ext.isIE ? '16 4 5 4' : '10 4 5 4',
				//reqex: Birst.Utils.Validation.textfieldValidationRegEx255L,
				regex: /^[^\\.\/<>:|*?"]{1,255}$/, //"
				regexText: Ext.String.format(Birst.LM.Locale.get('VLD_SPACENAME_MAXLEN_LB'), 255) + '.,<,>,:,\",/,\,|,?,*'
			},
			{
				xtype: 'textfield',
				fieldLabel: Birst.LM.Locale.get('LB_SCHEMA'),
				name: 'addNewSpaceSchemaFld',
				labelAlign: 'top',
				hidden: Birst.Utils.User.isApplianceMode && Birst.Utils.User.allowOverrideSpaceSchema && Birst.Utils.User.showFreeTrial != 'true' ? false : true,
				labelSeparator: '',
				labelStyle: 'font-size: 14px; color: #656565 !important',
				minWidth: 160,
				allowBlank: true,
				padding: Ext.isIE ? '16 4 5 4' : '10 4 5 4',
				//reqex: Birst.Utils.Validation.textfieldValidationRegEx255L,
				regex: /^[^\\.\/<>:|*?"]{1,255}$/, //"
				regexText: Ext.String.format(Birst.LM.Locale.get('VLD_SPACENAME_MAXLEN_LB'), 255) + '.,<,>,:,\",/,\,|,?,*',
				listeners: {
					scope: me,
					afterrender: function(el){
						Ext.create('Ext.tip.ToolTip', { target: el.getEl(), html: Birst.LM.Locale.get('DLG_SPACE_SCHEMAFIELD_TOOLTIP')});
					}
				}
			},
			{
				xtype: 'textarea',
				fieldLabel: Birst.LM.Locale.get('LB_COMMENTS'),
				name: 'addNewSpaceCommentsFld',
				labelAlign: 'top',
				labelStyle: 'font-size: 14px; color: #656565 !important',
				labelSeparator: '',
				padding: Ext.isIE ? '16 4 5 4' : '10 4 5 4',
				minWidth: 160,
				flex: 1,
				regex: /[*]{0,400}$/,
				regexText: Ext.String.format(Birst.LM.Locale.get('VLD_COMMENTS_MAXLEN_LB'), 400)
			}]
		});


		var cpl = Ext.create('Ext.panel.Panel', {
			layout: {
				type: 'card',
				deferredRender: true
			},
			id: 'bid-sp-mod-newspacewiz',
			bodyPadding: 15,
			bodyCls: 'xb-panel-body-default',
			defaults: {
				border: false
			},
			dockedItems: [{
				xtype: 'toolbar',
				ui: 'footer',
				dock: 'bottom',
				height: 60,
				layout: {
					pack: 'center'
				},
				items: ['->',
					{
						id: 'wzbtn-prev',
						cls: 'xb-modwin-gray-btn',
						overCls: 'xb-gray-btn-over',
						pressedCls: 'xb-footer-btn-press',
						text: Birst.LM.Locale.get('LB_BACK'),
						handler: function (btn) {
							wizardNav(btn.up("panel"), "prev");
						},
						hidden: true
					},
					{
						id: 'wzbtn-next',
						cls: 'xb-modwin-submit-btn',
						overCls: 'xb-submit-btn-over',
						pressedCls: 'xb-footer-btn-press',
						text:  Birst.Utils.User.showFreeTrial != 'true' ? Birst.LM.Locale.get('LB_NEXT') : Birst.LM.Locale.get('LB_DONE'),
						handler: function (btn) {
							wizardNav(btn.up("panel"), "next");
						}
					}]
			}],
			items: wizardItems
		});

		var win = Ext.create('Ext.window.Window', {
			title: Birst.LM.Locale.get('DLG_ADDSPACE_TITLE_LB'),
			closable: false,
			cls: 'xb-modal-window',
			maximizable: false,
			resizable: true,
			draggable: false,
			id: 'bid-sp-mod-newspace-dlg',
			width: 720,
			modal: true,
			listeners: {
				afterrender: function (w) {
					var header = w.header;
					header.setHeight(36);
				}
			},
			tools: [{
				xtype: 'button',
				iconCls: 'closeModalIcon',
				cls: 'xb-modwin-close-btn',
				overCls: 'xb-modwin-close-btn-over',
				tooltip: Birst.LM.Locale.get('LB_CANCEL'),
				handler: function (ev, te, p) {
					this.ownerCt.ownerCt.destroy();
				}
			}],
			items: cpl
		});
		win.show();

		var wizSubmit = function (wizard) {

			var spaceMode;
			var spaceType;
			var schemaName="";
			var wizLayout = wizard.getLayout();

			if (Ext.ComponentQuery.query('textfield[name=addNewSpaceNameFld]')[0].getValue() == "")
				return; // user must enter a name

			if (Ext.ComponentQuery.query('textfield[name=addNewSpaceSchemaFld]')[0].isVisible() ) {
				schemaName = Ext.ComponentQuery.query('textfield[name=addNewSpaceSchemaFld]')[0].getValue();
			}

			if (!Ext.ComponentQuery.query('textfield[name=addNewSpaceCommentsFld]')[0].isValid() || !Ext.ComponentQuery.query('textfield[name=addNewSpaceNameFld]')[0].isValid())
				return; // exceeded length limit

			if (Birst.Utils.User.showFreeTrial != 'true') {
				if (Ext.getCmp('bid-sp-rdb-auto').getValue()) {
					spaceMode = "1";
				}

				if (Ext.getCmp('bid-sp-rdb-discovery').getValue()) {
					spaceMode = "3";
				}

				if (Ext.getCmp('bid-sp-rdb-advanced').getValue()) {
					spaceMode = "2";
				}

				if (Ext.getCmp('bid-sp-rdb-spacetracking').getValue()) {
					spaceMode = "-1";
				}

				if (externalDBEnabled) {
					var r = clustSelected;
					spaceType = !clustSelected ? "-1" : clustSelected.data.Type;
				} else {
					spaceType = "-1";
				}
			} else {
				spaceMode = "3";
				spaceType = "-1";
			}

			if (spaceMode != "-1") {
				Birst.core.Webservice.acornRequest('CreateNewSpace',
				[
					{ key: 'spaceName', value: Ext.ComponentQuery.query('textfield[name=addNewSpaceNameFld]')[0].getValue() },
					{ key: 'schemaName', value: schemaName},
					{ key: 'spaceComments', value: Ext.ComponentQuery.query('textarea[name=addNewSpaceCommentsFld]')[0].getValue() },
					{ key: 'spaceMode', value: spaceMode },
					{ key: 'useTemplate', value: 'false' },
					{ key: 'queryLanguageVersion', value: '1' },
					{ key: 'templateID' },
					{ key: 'spaceType', value: spaceType }
				],
				me,
				function (response, options) {
					var err_type, err_msg;
					var stat = response.responseXML.getElementsByTagName('CreateNewSpaceResponse');
					var res = stat[0].firstChild;
					while (res != null) {
						err_type = getXmlChildValue(res, 'ErrorType');
						err_msg = getXmlChildValue(res, 'ErrorMessage');
						res = res.nextSibling;
					}
					if (err_type && err_type != 0) {
						// display error message
						var win = Ext.create('Birst.Util,ErrorDialog', {
							msg: err_msg,
							btnText: 'OK'
						});
						win.show();
						Ext.ComponentQuery.query('window[id=bid-sp-mod-newspace-dlg]')[0].destroy();
					}
					else {
						window.open('/FlexModule.aspx?birst.module=admin', '_self');
					}
				}, null, null, true
			);

				Ext.ComponentQuery.query('window[id=bid-sp-mod-newspace-dlg]')[0].destroy();

			} else {
				Birst.core.Webservice.acornRequest('GetTemplates',
				[{ key: 'categoryName', value: 'Other' },
					{ key: 'sortExpression' },
					{ key: 'rate', value: 'false' }
				],
				me,
				function (response, options) {
					var err_type, err_msg;
					var stat = response.responseXML.getElementsByTagName('GetTemplatesResponse');
					var res = stat[0].firstChild;
					while (res != null) {
						err_type = getXmlChildValue(res, 'ErrorType');
						err_msg = getXmlChildValue(res, 'ErrorMessage');
						res = res.nextSibling;
					}
					if (err_type && err_type != 0) {
						// display error message
						var win = Ext.create('Birst.Util,ErrorDialog', {
							msg: err_msg,
							btnText: 'OK'
						});
						win.show();
						Ext.ComponentQuery.query('window[id=bid-sp-mod-newspace-dlg]')[0].destroy();
						return null;
					}
					else {
						// if template exists
						var tmplId;
						var tmpls = response.responseXML.getElementsByTagName('TemplateDetails');
						if (tmpls && tmpls.length > 0) {
							var tmpl = tmpls[0].firstChild;
							while (tmpl != null) {
								var name = Birst.Utils.Safe.getEscapedValue(getXmlChildValue(tmpl, 'Name'));
								if (name == 'Usage Tracking') {
									tmplId = getXmlChildValue(tmpl, 'TemplateID');
									tmplAutoMode = getXmlChildValue(tmpl, 'Automatic');
									break;
								}
								tmpl = tmpl.nextSibling;
							}
						}
						if (tmplId) {
							Birst.core.Webservice.acornRequest('CreateNewSpace',
								[
									{ key: 'spaceName', value: Ext.ComponentQuery.query('textfield[name=addNewSpaceNameFld]')[0].getValue() },
									{ key: 'schemaName', value: schemaName},
									{ key: 'spaceComments', value: Ext.ComponentQuery.query('textarea[name=addNewSpaceCommentsFld]')[0].getValue() },
									{ key: 'spaceMode', value: tmplAutoMode == 'true' ? '1' : '2' },
									{ key: 'useTemplate', value: 'true' },
									{ key: 'queryLanguageVersion', value: '1' },
									{ key: 'templateID', value: tmplId },
									{ key: 'spaceType', value: '-1' }
								],
								me,
								function (response, options) {
									var err_type, err_msg;
									var stat = response.responseXML.getElementsByTagName('CreateNewSpaceResponse');
									var res = stat[0].firstChild;
									while (res != null) {
										err_type = getXmlChildValue(res, 'ErrorType');
										err_msg = getXmlChildValue(res, 'ErrorMessage');
										res = res.nextSibling;
									}
									if (err_type && err_type != 0) {
										// display error message
										var win = Ext.create('Birst.Util,ErrorDialog', {
											msg: err_msg,
											btnText: 'OK'
										});
										win.show();
										Ext.ComponentQuery.query('window[id=bid-sp-mod-newspace-dlg]')[0].destroy();
									}
									else {
										window.open('/FlexModule.aspx?birst.module=admin', '_self');
									}
								}, null, null, true 
							);
						} else {
							Ext.ComponentQuery.query('window[id=bid-sp-mod-newspace-dlg]')[0].destroy();
							var win = Ext.create('Birst.Util,ErrorDialog', {
								msg: Birst.LM.Locale.get('MSG_USGTEMPL_NOTFOUND_LB'),
								btnText: 'OK'
							});
							win.show();
						}
					}
				}, null, null, true);
			}
		};
	},

	onInviteAccept: function(me) {
		Birst.core.Webservice.acornRequest('SendInvitationResponse',
			[
				{ key: 'accept', value: 'true' },
				{ key: 'spaceID', value: this.selectedSpaceDesc.spaceId },
				{ key: 'userEmail', value: this.selectedSpaceDesc.spaceOwner }
			],
			me,
			function (response, options) {
				var err_type, err_msg;
				var stat = response.responseXML.getElementsByTagName('SendInvitationResponseResponse');
				var res = stat[0].firstChild;
				while (res != null) {
					err_type = getXmlChildValue(res, 'ErrorType');
					err_msg = getXmlChildValue(res, 'ErrorMessage');
					res = res.nextSibling;
				}
				if (err_type && err_type != 0){
					// display error message
					var win = Ext.create('Birst.Util,ErrorDialog', {
						msg: err_msg,
						btnText: 'OK'
					});
					win.show();
				} else {
					this.getSpaceOptionsContainer().mask();
					this.getSpacesFromService();

				}
			}, null, null, true
		);

	},

	onInviteDecline: function(me) {
		Ext.create('Birst.Util,ConfirmDialog', {
				title: '<span> '+ Birst.LM.Locale.get('SPACEOPT_INV_DECLINE_LB')+' <span class="cl-title-secondary">' + Birst.Utils.Safe.getEscapedValue(this.selectedSpaceDesc.spaceName)+ '</span> </span>',
				msg: Birst.LM.Locale.get('SPACEOPT_INV_DECLINE_CONFIRM_LB'),
				fn: function(){
						Birst.core.Webservice.acornRequest('SendInvitationResponse',
						[
							{ key: 'accept', value: 'false' },
							{ key: 'spaceID', value: this.selectedSpaceDesc.spaceId },
							{ key: 'userEmail', value: this.selectedSpaceDesc.spaceOwner }
						],
						me,
						function (response, options) {
							var err_type, err_msg;
							var stat = response.responseXML.getElementsByTagName('SendInvitationResponseResponse');
							var res = stat[0].firstChild;
							while (res != null) {
								err_type = getXmlChildValue(res, 'ErrorType');
								err_msg = getXmlChildValue(res, 'ErrorMessage');
								res = res.nextSibling;
							}
							if (err_type && err_type != 0){
								var win = Ext.create('Birst.Util,ErrorDialog', {
									msg: err_msg,
									btnText: Birst.LM.Locale.get('LB_DELETE')
								});
								win.show();
							} else {
								this.getSpaceOptionsContainer().mask();
								this.getSpacesFromService();

							}
						}, null, null, true
						);
					},
				btnText: Birst.LM.Locale.get('SPACEOPT_INV_DECLINE_LB'),
				scope: this
				}).show();
	}
  });


