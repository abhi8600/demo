Ext.define('DefaultHome.view.SpacesListPanel', {

    extend: 'Ext.grid.Panel',

    alias: 'widget.spacesGrid',

    initComponent: function () {
        //alert('init view:DefaultHome.view.SpacesListPanel');

        this.stripeRows = false;
        this.store = 'SoapWebService';
        this.padding = 0;
        this.bodyPadding = 0;
        this.margin = 0;
        this.columns = [
                    { text: 'Space', dataIndex: 'SpaceName', sortable: true },
                    { text: 'User', dataIndex: 'OwnerUsername', sortable: true },
                    { text: 'Owner', dataIndex: 'Owner', sortable: true },
                    { text: 'Visualizer', dataIndex: 'TrialVisualizer', sortable : true }
            ];

        this.callParent(argument);
    }



});