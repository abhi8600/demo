Ext.define('DefaultHome.view.SpaceOption', {
	extend: 'Ext.panel.Panel',
	border: false,
	cls: null,
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	iconCls: null,
	headerLabel: null,
	description: null,

	constructor: function(iconCls, headerLabel, description, href, isDisabled ) {
		this.iconCls = iconCls;
		this.headerLabel = headerLabel;
		this.description = description;
		this.cls = 'spaceOption';
		this.href = href;

		var config = {
				border: false,
				cls: this.cls,
				header: false,
				id: iconCls,
				height: 75,
				disable: isDisabled != null ? isDisabled : false,
				layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [
				{
					xtype: 'panel',
					border: false,
					cls: 'iconHolder',
					flex: 1,
					style: {
					valign: 'middle'
					},
					items: [{
					xtype: 'box',
					cls: this.iconCls + 'Icon',
					itemId: this.iconCls + 'Icon',
					height: '30px'
					}]
				},
				{
					xtype: 'panel',
					itemId: this.iconCls + 'Item',
					height: '100%',
					cls: 'border-left',
					flex: 7,
					border: false,
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'box',
							html: '<p>' + this.headerLabel + '</p>',
							cls: isDisabled != null && isDisabled == true ? 'H2d' : 'H2'
						},
						{
							xtype: 'box',
							html: '<p>' + this.description + '</p>',
							cls: 'p'
						}
					]
				}
			]
		};
		this.callParent([config]);


		this.on('render', function(me, eOpts) {
			var el = Ext.get(this.el.dom);
			// prevent a hover effect if disabled
			if (!( isDisabled != null && isDisabled == true ))
				el.addClsOnOver('hover');

			el.on('click', function(e, t, eOpts) {

				if (this.href) {
					var fly = new Ext.dom.Element.Fly(e.currentTarget);
					if (! fly.hasCls('disabled'))
						window.open(href, '_self');
				}else{
					this.fireEvent('click');
				}
			},this);
		});
	}
});


Ext.define('DefaultHome.view.InviteOption', {
	extend: 'Ext.panel.Panel',
	border: false,
	cls:null,
	constructor: function() {
		this.cls = 'spaceOption';
		var config = {
						xtype: 'panel',
						border: false,
						header: false,
						height: 180,
						autoScroll: false,
						layout: {
							type: 'vbox',
							align: 'center'
						},
						items: [
								{
									xtype: 'box',
									html: '<p>' +  Birst.LM.Locale.get('SPACEOPT_INV_MESSAGE_LB') + '</p>',
									cls: 'H2',
									padding:'30 0 0 0'
								},
								{
									xtype:'container',
									border: false,
									margin: '16 1 1 1',
									flex: 1,
									layout: {
										type: 'hbox',
										align: 'center'
									},
									items: [
										{
											xtype:'container',
											border: false,
											height:'100%',
											items: [{
												xtype: 'button',
												id: 'inviteAccept',
												text: Birst.LM.Locale.get('SPACEOPT_INV_ACCEPT_LB'),
												cls: 'xb-modwin-submit-btn',
												overCls: 'xb-submit-btn-over',
												pressedCls: 'xb-footer-btn-press',
												margin: Ext.isIE ? '2 10 2 2' : '2 8 2 2',
												width:80
											}]
										},{
											xtype:'container',
											border: false,
											height:'100%',
											items: [{ xtype: 'button',
												id:'inviteDecline',
												text: Birst.LM.Locale.get('SPACEOPT_INV_DECLINE_LB'),
												cls: 'xb-modwin-gray-btn',
												overCls: 'xb-gray-btn-over',
												pressedCls: 'xb-footer-btn-press',
												margin: '2 2 2 6',
												width:80
											}]
										}]
								}]

						};
		this.callParent([config]);
	}
});

Ext.define('DefaultHome.view.SpaceWait', {
	extend: 'Ext.panel.Panel',
	border: false,
	cls:null,
	constructor: function() {
		this.cls = 'spaceOption';
		var config = {
				xtype: 'panel',
				border: false,
				header: false,
				height: 180,
				autoScroll: false,
				layout: {
					type: 'vbox',
					align: 'center'
				},
				items: [{
					xtype: 'box',
					html: '<p style="text-align:center">' +  Birst.LM.Locale.get('SPACE_WAIT') + '</p>',
					cls: 'H2',
					padding:'30 15 15 15'
				} ]
			};
		this.callParent([config]);
	}
});

