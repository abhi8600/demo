Ext.define('DefaultHome.view.SpaceOptionsPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.spaceOpts',
	border: false,
	flex: 1,
	layout: {

		type: 'auto',
		align: 'stretch'
	},
	items: [
		{
			xtype: 'displayfield',
			id: 'spaceName',
			height: 'auto',
			cls: 'H1',
			width: '100%'
		},
		{
			xtype: 'displayfield',
			id: 'spaceDescription',
			height: 'auto',
			autoScroll: true,
			cls: 'p',
			width: '100%'
		},
		{
			xtype: 'panel',
			itemId: 'spaceInfoPanel',
			id: 'spaceInfoPanel',
			border: false,
			height: 'auto',
			width: '100%',
			margin: '0 0 10 0',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [
				{
					xtype: 'displayfield',
					itemId: 'spaceOwner',
					cls: 'spaceInfo',
					margin: '0 30 0 0'
				},
				{
					xtype: 'displayfield',
					itemId: 'spaceProcessedDate',
					cls: 'spaceInfo',
					margin: '0 30 0 0'
				},
				{
					xtype: 'displayfield',
					itemId: 'spaceSize',
					cls: 'spaceInfo'
				}
			]
		},
		{
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
				},
			height:'auto',
			width: '100%',
			itemId: 'spaceAction',
			cls: 'spaceActionsTable'
		}

	],
	initComponent: function () {
		this.callParent(arguments);
	},

	cleanView: function (){
		var spaceInfoPanel = this.getComponent('spaceInfoPanel');
		spaceInfoPanel.removeAll();
		this.getComponent('spaceDescription').setValue('');
		spaceInfoPanel.add(
		{
			xtype: 'displayfield',
			itemId: 'spaceOwner',
			cls: 'spaceInfo',
			margin: '0 30 0 0'
		},
		{
			xtype: 'displayfield',
			itemId: 'spaceProcessedDate',
			cls: 'spaceInfo',
			margin: '0 30 0 0'
		},
		{
			xtype: 'displayfield',
			itemId: 'spaceSize',
			cls: 'spaceInfo'
		});

		var spaceAction = this.getComponent('spaceAction');
		spaceAction.removeAll();
	},

	updateView: function (spaceCfg, userCfg) {
		var viewControls = [];
		this.cleanView();
		this.getComponent('spaceName').setValue(spaceCfg.spaceName);
		this.getComponent('spaceDescription').setValue(spaceCfg.spaceComments);
		var spaceInfoPanel = this.getComponent('spaceInfoPanel');
		spaceInfoPanel.getComponent('spaceOwner').setValue(Birst.LM.Locale.get('SPACEINFO_OWNER_LB')+': ' + spaceCfg.ownerUserName);
		var lastUploadTxt = spaceCfg.lastUpload ? ( Birst.LM.Locale.get('SPACEINFO_LASTPROCESSED_LB') +': ' +  spaceCfg.lastUpload) : Birst.LM.Locale.get('SPACEINFO_LASTPROCESSED_LB') +': ';
		spaceInfoPanel.getComponent('spaceProcessedDate').setValue(lastUploadTxt);
		var spaceSizeTxt = spaceCfg.spaceSize ? (Birst.LM.Locale.get('SPACEINFO_SPACESIZE_LB')+': ' +  spaceCfg.spaceSize) : Birst.LM.Locale.get('SPACEINFO_SPACESIZE_LB')+': ';
		spaceInfoPanel.getComponent('spaceSize').setValue(spaceSizeTxt);
		// SpaceSize info for Acorn 5.3.0 and higher
		if (Birst.Utils.User.sizeOfSpaceOwned){
			Ext.getCmp('idMySpacesMainContainer').setTitle('<span>'+Birst.LM.Locale.get('MYSPACES_LB')+'<span id="idSpaceSizeReadout"> ('+Birst.LM.Locale.get('SPACETOTALSIZE_LB')+': ' + Birst.Utils.User.sizeOfSpaceOwned+ ') </span> </span> ');
		}
		var spaceAction = this.getComponent('spaceAction');
		if (spaceCfg.availabilityCode == "1") {
			this.createSpaceWaitView(spaceCfg);
			this.updateLayout();
			return;
		}

		var dashboardCtl = spaceAction.getComponent('dashboard');

		if (spaceCfg.dashboards == 'true' && spaceCfg.enableDashboards == 'true') {
			if (!dashboardCtl) {
				dashboardCtl = Ext.create('DefaultHome.view.SpaceOption',
					'dashboard',
					Birst.LM.Locale.get('SPACEOPT_DASHBOARDS_LB'),
					Birst.LM.Locale.get('SPACEOPT_DASHBOARDS_DESCR_LB'),
					'/FlexModule.aspx?birst.module=dashboard'
				);
				spaceAction.insert(0,dashboardCtl);
			}
		} else if (spaceCfg.dashboards == 'true' && spaceCfg.enableDashboards != 'true') {
			spaceAction.remove('dashboard');
		} else {
			spaceAction.remove('dashboard');
		}

		var visualizerCtl = spaceAction.getComponent( 'visualizer' );
		if( spaceCfg.enableVisualizer ) {
			if( !visualizerCtl ) {
				visualizerCtl = Ext.create( 'DefaultHome.view.SpaceOption',
					'visualizer',
					Birst.LM.Locale.get( 'SPACEOPT_VISUALIZER_LB' ),
					Birst.LM.Locale.get( 'SPACEOPT_VISUALIZER_DESCR_LB'),
					'/Visualizer/visualizer.aspx?standalone=true'
				);
				spaceAction.insert( 1, visualizerCtl );
			}
		}

		var designerCtl = spaceAction.getComponent('designer');
		if (spaceCfg.adhoc == 'true' && spaceCfg.enableAdhoc == 'true') {
			if (!designerCtl) {
				designerCtl = Ext.create('DefaultHome.view.SpaceOption',
					'designer',
					Birst.LM.Locale.get('SPACEOPT_DESIGNER_LB'),
					Birst.LM.Locale.get('SPACEOPT_DESIGNER_DESCR_LB'),
					'/FlexModule.aspx?birst.module=designer'
				);
			spaceAction.add(designerCtl);
			}
		} else if (spaceCfg.adhoc == 'true' && spaceCfg.enableAdhoc != 'true') {
			spaceAction.remove('designer');
		} else {
			spaceAction.remove('designer');
		}

		var adminCtl = spaceAction.getComponent('admin');
		var copyCtl = spaceAction.getComponent('copySpace');
		var deleteCtl = spaceAction.getComponent('deleteSpace');

		if (spaceCfg.denyAddSpace == 'true') {
			spaceAction.remove(copyCtl);
		}

		if (!spaceCfg.isAdminAccess) {

			// shared space with admin access right for this user
			if (copyCtl) {
				spaceAction.remove(copyCtl);
			}

			if (adminCtl) {
				spaceAction.remove(adminCtl);
			}

			if (deleteCtl) {
				spaceAction.remove(deleteCtl);
			}

		} else {

			if (!adminCtl) {
				adminCtl = Ext.create('DefaultHome.view.SpaceOption',
					'admin',
					Birst.LM.Locale.get('SPACEOPT_ADMIN_LB'),
					Birst.LM.Locale.get('SPACEOPT_ADMIN_DESCR_LB'),
					'/FlexModule.aspx?birst.module=admin'
				);
				spaceAction.add(adminCtl);
			}

			if (!copyCtl && spaceCfg.denyAddSpace != 'true') {
				copyCtl = Ext.create('DefaultHome.view.SpaceOption',
					'copySpace',
					Birst.LM.Locale.get('SPACEOPT_COPYSPACE_LB'),
					Birst.LM.Locale.get('SPACEOPT_COPYSPACE_DESCR_LB'),null
				);
				spaceAction.add(copyCtl);
			}

			if (spaceCfg.owner == 'true'){
				if (!deleteCtl) {
					deleteCtl =  Ext.create('DefaultHome.view.SpaceOption',
						'deleteSpace',
						Birst.LM.Locale.get('SPACEOPT_DELETESPACE_LB'),
						Birst.LM.Locale.get('SPACEOPT_DELETESPACE_DESCR_LB'),null
					);
					spaceAction.add(deleteCtl);
				}
			} else {
				if (deleteCtl) {
					spaceAction.remove(deleteCtl);
				}
			}
		}

		if (Birst.Utils.User.enableUserManagement == 'true' && Birst.Utils.User.showFreeTrial != 'true') {
			Ext.getCmp('bid-header-usermng-btn').show();
		}

		if (spaceCfg.denyAddSpace == 'true') {
			Ext.getCmp('bid-header-addspc-btn').hide();
		}
		else {
			Ext.getCmp('bid-header-addspc-btn').show();
		}

		var removeFromCtl = spaceAction.getComponent('removeFromSpace');

		// TODO: If trial period is over and visualizer is live we need to change this
		if ( spaceCfg.owner == 'false' ) {
			if (removeFromCtl) {
				spaceAction.remove(removeFromCtl);
			}
			if( !( Birst.Utils.User.isDiscoveryFreeTrial && spaceCfg.enableVisualizer ) ) {
				// insert last
				var last = spaceAction.items.length;
				removeFromCtl =  Ext.create('DefaultHome.view.SpaceOption',
					'removeFromSpace',
					Birst.LM.Locale.get('SPACEOPT_REMOVESPACE_LB'),
					Birst.LM.Locale.get('SPACEOPT_REMOVESPACE_DESCR_LB'),null
				);
				spaceAction.insert(last,removeFromCtl);
			}
		} else {

			if (removeFromCtl) {
				spaceAction.remove(removeFromCtl);
			}
		}
		this.updateLayout();
	},

	createInviteView: function(irec) {
		this.cleanView();
		this.getComponent('spaceName').setValue(irec.data.SpaceName);
		var spaceInfoPanel = this.getComponent('spaceInfoPanel');
		spaceInfoPanel.getComponent('spaceOwner').setValue('owner: ' + irec.data.OwnerUsername);
		var spaceAction = this.getComponent('spaceAction');
		var inviteAcceptCtl = spaceAction.getComponent('inviteAccept');



		if (!inviteAcceptCtl) {
				inviteAcceptCtl = Ext.create('DefaultHome.view.InviteOption');
				spaceAction.insert(0,inviteAcceptCtl);
		}

		/*
		if (!inviteAcceptCtl) {
			inviteAcceptCtl = Ext.create('DefaultHome.view.InviteOption',
				'inviteAccept',
				'Accepts',
				'Accepts user invitation to share the space.', null
			);
				spaceAction.insert(0,inviteAcceptCtl);
		}


		var inviteDeclineCtl = spaceAction.getComponent('inviteDecline');

		if (!inviteDeclineCtl) {
			inviteDeclineCtl = Ext.create('DefaultHome.view.SpaceOption',
				'inviteDecline',
				'Decline',
				'Decline user invitation to share the space.', null
			);
			spaceAction.insert(0,inviteDeclineCtl);
		}

		*/
	},

	createSpaceWaitView: function() {
		var spaceAction = this.getComponent('spaceAction');
		spaceAction.removeAll();
		var spaceWaitCtl = Ext.create('DefaultHome.view.SpaceWait');
		spaceAction.insert(0,spaceWaitCtl);
	}
});

