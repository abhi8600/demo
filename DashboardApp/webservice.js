Ext.ns("Birst.core");

function xml2Str(xmlNode) {
	"use strict";
   try {
	  // Gecko- and Webkit-based browsers (Firefox, Chrome), Opera.
	  return (new XMLSerializer()).serializeToString(xmlNode);
  }
  catch (e) {
	 try {
		// Internet Explorer.
		return xmlNode.xml;
	 }
	 catch (e1) {
		//Other browsers without XML Serializer
		alert('Xmlserializer not supported');
	 }
   }
   return false;
}

function loadXmlFile(filename, callback){
	"use strict";
	try{
		var xmlDoc = document.implementation.createDocument("","",null);

	}catch(e){
		try{

		}catch (e1){
			alert('load file unsupported');
		}
	}
}

function Str2Xml(str) {
	"use strict";
	if (window.DOMParser) {
		var parser = new DOMParser();
		return parser.parseFromString(str, 'text/xml');
	}

	var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	xmlDoc.async = false;
	xmlDoc.loadXML(str);
	return xmlDoc;
}

function getXmlValue(node) {
	"use strict";
	var child = node.firstChild;
	if (child && child.nodeName === '#text') {
		if (Ext.isIE) {
			return child.nodeValue;
		}
		return child.wholeText;

		}

	return null;
}

function setXmlValue(node,value) {
	"use strict";
	if (node && node.length) {
	   var prop = node[0].firstChild;
		  if (prop) {
			 prop.nodeValue = value;
			   return true;
		  }
	}
	return false;
}

function getXmlChildValue(node, childName) {
	"use strict";
	var child = node.getElementsByTagName(childName);
	if (child && child.length > 0) {
		return getXmlValue(child[0]);
	}
	return null;
}


Ext.define('Birst.core.Webservice', {
	statics: {
		escapeXml: function(str){
			"use strict";
			return (str) ? str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') : '';
		},

		unescapeXml: function(str) {
			"use strict";
			return (str) ? str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '""').replace(/&apos;/g, '\'') : '';
		},

		adminUrl: null,
		smiroot: null,
		locale:null,
		extraParams: null,
		bingMaps: null,
		schedulerRoot: null,
		noheader: null,
		gaAcount: null,
		cookieProvider: null,
		token: null,
		asyncRequests: {},
		createdTimer: false,

		createEnvelope: function(service, fnName, args, ns) {
			"use strict";
			var soap = '<SOAP-ENV:Envelope  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns:';
			soap += fnName + ' xmlns:ns="' + ns + '">';

			//arguments
			//accepts empty value, e.g., args = { {key: 'param1'}, {key: 'param2', value: 'val2'}, ...}
			for(var i = 0; i < args.length > 0; i++){
				var key = args[i].key;
				var value = args[i].value;
				if(key){
					soap += '<ns:' + key + '>' + Birst.core.Webservice.escapeXml(value) + '</ns:' + key + '>';
				}
			}

			soap += '</ns:' + fnName + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';

			return soap;
		},

		createCookieProvider: function() {
			"use strict";
			if (!Birst.core.Webservice.cookieProvider) {
				Birst.core.Webservice.cookieProvider = Ext.create('Ext.state.CookieProvider', {
					prefix: ''
				});
				Ext.state.Manager.setProvider(Birst.core.Webservice.cookieProvider);
			}
		},
		
		getCookie: function(cookieName) {
		    var cookies = document.cookie.split(';');
		    for(var i = 0; i < cookies.length; i++){
		    	var cKV = cookies[i].split('=');
		    	if(cKV.length > 0 && cKV[0].trim() == cookieName){
		    		return cKV[1];
		    	}
		    }
		   
		    return undefined;
		},

		getAntiCSRFToken: function() {
			"use strict";
			if (!this.token) {
				this.token = this.getCookie("XSRF-TOKEN");
			}
			return this.token;
		},

		request: function(url, ns, soapAction, service, fnName, args, scope, successFn, failFn, additionalParam, defErrMsg, maskself, cookie){
			"use strict";
			var envelope = Birst.core.Webservice.createEnvelope(service, fnName, args, ns);
			Birst.core.Webservice.createCookieProvider();

			//var isMask = true;

			var headers = {
				'Content-Type': 'text/xml; charset=utf-8',
				'SOAPAction': soapAction,
				'tns': 'urn:',
				'X-XSRF-TOKEN': this.getAntiCSRFToken()
			};
			if (cookie) {
				headers.Cookie = cookie;
			}
			Birst.core.Webservice.internalRequest(envelope, headers, url, scope, additionalParam, successFn, failFn, defErrMsg, maskself);
		},
		
		internalRequest: function(envelope, headers, url, scope, additionalParam, successFn, failFn, defErrMsg, maskself) {
			Ext.Ajax.request({
				url: url,
				method: 'POST',
				//cors: true,
				headers: headers,
				scope : scope,
				params : envelope,
				timeout: 300000,
				additionalParam: additionalParam,
				success : function(response, options){
					if (!maskself) {Ext.getBody().unmask();}
					if (response.status === 302) {
						window.open("/Login.aspx", "_self");
						return;
					}
					try {
						var error = response.responseXML.getElementsByTagName('Error');
						if (error && error.length > 0) {
							var errorMessage = getXmlChildValue(error[0], 'ErrorMessage');
							if (errorMessage === 'Unable to continue. Please login again') {
								window.open("/Login.aspx", "_self");
							}

							if ((defErrMsg == undefined || defErrMsg) && errorMessage != null ) {
								Ext.create('Birst.Util,ErrorDialog', {
										msg: errorMessage,
										btnText: 'OK'
								}).show();
							}
						}
					}
					catch (err) {

					}
					var bwsr = response.responseXML.getElementsByTagName('com.successmetricsinc.WebServices.BirstWebServiceResult');
					if (bwsr && bwsr.length > 0) {
						var errorCode = getXmlChildValue(bwsr[0], 'ErrorCode');
						if (errorCode === '-4') {
							// asynch response
							var asynchId = getXmlChildValue(bwsr[0], 'AsynchResultId');
							Birst.core.Webservice.addAsyncJobPolling(asynchId, options, successFn, failFn, defErrMsg);
							return;
						}
						if (errorCode !== '0') {
							if (failFn) {
								failFn.call(scope, response, options);
							}
							else {
								var errorMessage = getXmlChildValue(bwsr[0], 'ErrorMessage');
								if (errorMessage) {
									Ext.create('Birst.Util,ErrorDialog', {
										msg: errorMessage,
										btnText: 'OK'
									}).show();
								}
								else {
									Ext.create('Birst.Util,ErrorDialog', {
										msg: 'General failure. Error code:' +  errorCode,
										btnText: 'OK'
									}).show();
								}
							}
							return;
						}
					}
					if(successFn){
						successFn.call(scope, response, options);
					}
				},
				failure : (failFn) ?
					function(response, options){

						if (!maskself) {
							Ext.getBody.unmask();
						}

						if (response.status === 401) {
							window.open("/Login.aspx", "_self");
						}
						failFn.call(scope, response, options);
					}
						:
					function(response, options) {
						Ext.getBody().unmask();
						if (response.status === 401) {
							window.open("/Login.aspx", "_self");
						}

						var errtextmsg;
						if (response.status === 408 || response.status === 500) {
							errtextmsg = 'Server did not respond in a timely fashion or internal error has occured. Please try again later.';
						}
						else {
							errtextmsg = 'General failure. Request could not be completed. Please try again later.';
						}

						Ext.create('Birst.Util,ErrorDialog', {
							msg: errtextmsg,
							btnText: 'OK',
							handler: function () {
								window.open("/html/home.aspx", '_self');
							}
						}).show();
					}
			});

			if (!maskself) {
				Ext.getBody().mask('');
			}
		},

		pollRequest: function() {
			"use strict";
			Ext.Ajax.request({
				url: '/PollSession.aspx'
			});
		},

		dashboardRequest: function(fnName, args, scope, successFn, failFn, additionalParam){
			"use strict";
			var serviceUpperCase = 'Dashboard';
			var url = '/SMIWeb/services/' + serviceUpperCase + '.' + serviceUpperCase + 'HttpSoap11Endpoint';
			Birst.core.Webservice.request(url, 'http://dashboard.WebServices.successmetricsinc.com', 'urn:' + fnName, 'dashboard', fnName, args, scope, successFn, failFn, additionalParam);
		},
		adhocRequest: function(fnName, args, scope, successFn, failFn, additionalParam, mask){
			"use strict";
			var serviceUpperCase = 'Adhoc';
			var url = '/SMIWeb/services/' + serviceUpperCase + '.' + serviceUpperCase + 'HttpSoap11Endpoint';
			Birst.core.Webservice.request(url, 'http://adhoc.WebServices.successmetricsinc.com', 'urn:' + fnName, 'adhoc', fnName, args, scope, successFn, failFn, additionalParam,null,mask);
		},

		schedulerRequest: function (fnName, args, scope, successFn, failFn, additionalParam, defErrMsg, mask) {
			"use strict";
			var serviceUpperCase = 'SchedulerAdmin';
			var url = '/Scheduler/services/' + serviceUpperCase + '.' + serviceUpperCase + 'HttpSoap11Endpoint';
			Birst.core.Webservice.request(url, 'http://admin.webServices.scheduler.birst.com', 'urn:' + fnName, 'dashboard', fnName, args, scope, successFn, failFn, additionalParam);
		},

		acornRequest: function (fnName, args, scope, successFn, failFn, additionalParam, defErrMsg, mask) {
			"use strict";
			Birst.core.Webservice.request('/AdminService.asmx', 'http://www.birst.com/', 'http://www.birst.com/'+ fnName, 'acorn', fnName, args, scope, successFn, failFn, additionalParam, defErrMsg, mask);
		},

		clientInitRequest: function(fnName, args, scope, successFn, failFn, additionalParam){
			"use strict";
			if (false) { //! Birst.core.Webservice.smiroot) {
				// login to smiweb
				Birst.core.Webservice.acornRequest('createSMIWebSession', [], this,
					function(respose, options) {
						Birst.core.Webservice.acornRequest('getClientVariables', [], this,
							function(response, options) {
								// parse name value pairs
							var serviceUpperCase = 'ClientInitData';
							var url = '/SMIWeb/services/' + serviceUpperCase + '.' + serviceUpperCase + 'HttpSoap11Endpoint';
							Birst.core.Webservice.request(url, 'http://' + serviceUpperCase + '.WebServices.successmetricsinc.com', 'urn:' + fnName, 'clientInitData', fnName, args, scope, successFn, failFn, additionalParam);
							});
						});
			}
			else {
				var serviceUpperCase = 'ClientInitData';
				var url = '/SMIWeb/services/' + serviceUpperCase + '.' + serviceUpperCase + 'HttpSoap11Endpoint';
				Birst.core.Webservice.request(url, 'http://' + serviceUpperCase + '.WebServices.successmetricsinc.com', 'urn:' + fnName, 'clientInitData', fnName, args, scope, successFn, failFn, additionalParam);
			}
		},

		parseNameValuePairs: function (xmlDoc) {
			"use strict";
			var response = xmlDoc.getElementsByTagName('getClientVariablesResult');
			if (response) {
				var child = response[0].firstChild;
				while (child) {
					if (child.tagName === 'NameValuePair') {
						var name = getXmlChildValue(child, 'Name');
						var value = getXmlChildValue(child, 'Value');
						if (name === 'url') {
							Birst.core.Webservice.adminUrl = value;
							}
						else if (name === 'smiroot') {
							Birst.core.Webservice.smiroot = value;
							}
						else if (name === 'schedulerRoot') {
							Birst.core.Webservice.schedulerRoot = value;
							}
						else if (name === 'locale') {
							Birst.core.Webservice.locale = value;
							}
						else if (name === 'bingMaps') {
							Birst.core.Webservice.bingMaps = value;
							}
						else if (name === 'noheader') {
							Birst.core.Webservice.noheader = value;
							}
						else if (name === 'gaAccount') {
							Birst.core.Webservice.gaAcount = value;
							}
						else if (name === 'extraParams') {
							Birst.core.Webservice.noheader = value;
						}
					}
					child = child.nextSibling;
				}

			}
		},

		isNamespaceImportant: function() {
			"use strict";
			return !(Ext.isChrome || Ext.isSafari || Ext.isWebKit);
		},
		
		addAsyncJobPolling: function(id, options, successFn, failFn, defErrorMsg) {
			var o = {};
			o.options = options;
			o.successFn = successFn;
			if (failFn)
				o.failFn = failFn;
			if (defErrorMsg)
				o.defErrorMsg = defErrorMsg;
			Birst.core.Webservice.asyncRequests[id] = o;
			
			if (!Birst.core.Webservice.createdTimer) {
				Birst.core.Webservice.createdTimer = window.setInterval(Birst.core.Webservice.asynchPoll, 3000);
			}
		},
		
		removeAsynchJobPolling: function(id) {
			delete Birst.core.Webservice.asyncRequests[id];
		},
		
		asynchPoll : function () {
			// args = { {key: 'param1'}, {key: 'param2', value: 'val2'}, ...}
			var args = [];
			for (var keys in Birst.core.Webservice.asyncRequests)
				args.push({key: 'asynchronousJobIds', value: keys});
			if (args.length > 0) {
				Birst.core.Webservice.adhocRequest('isReportRenderJobComplete', args, this, Birst.core.Webservice.jobsComplete, null, null, true);
			}
		},
		
		jobsComplete: function(xmlDoc) {
			var response = xmlDoc.responseXML.getElementsByTagName('Finished');
			if (response) {
				var child = response[0].firstChild;
				while (child) {
					if (child.tagName == 'AsyncJob') {
						var id = child.getAttribute('Id');
						if (child.getAttribute('InvalidId') == 'true') {
							Birst.core.Webservice.removeAsynchJobPolling(id);
						}
						else if (child.getAttribute('IsComplete') == 'true') {
							var o = Birst.core.Webservice.asyncRequests[id];
							if (o) {
								Birst.core.Webservice.internalRequest(o.options.params, o.options.headers, o.options.url, o.options.scope, o.options.additionalParam, o.successFn, o.failFn, o.defErrMsg, true);
								Birst.core.Webservice.removeAsynchJobPolling(id);
							}
						}
					}
					child = child.nextSibling;
				}
			}
		}
	}
});

