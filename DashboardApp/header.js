Ext.ns("Birst.marginals");

Ext.define('Birst.marginals.header', {
	extend: 'Ext.panel.Panel',

	autoHeight: null,
	bodyBorder: null,
	layout: 'border',
	menu: null,
	toolTip: null,

	constructor: function(config){
		var params = Ext.Object.fromQueryString(window.location.search);
		if (params['birst.embedded'] && params['birst.embedded'] === 'true') {
			config.hidden = true;
		}
		config.autoHeight = true;
		config.bodyBorder = false;
		config.border = false;
		config.height = 50;
		config.cls = 'header';

		var pageName = Birst.Utils.Page.getPageName();
		var headerItems = [];
		headerItems.push({
			text: Birst.LM.Locale.get('GLNB_HOME'),
			scope: this,
			border: false,
			margin: '0 50 0 0',
//            cls: pageName === 'home.aspx' ? 'selected' : '',
			handler: function () {
				window.open("/html/home.aspx", '_self');
				}
			});

		if (pageName !== 'home.aspx') {
			headerItems.push({
				text: Birst.LM.Locale.get('GLNB_DASHBOARDS'),
				itemId: 'dashboards',
				scope: this,
				border: false,
				margin: '0 50 0 0',
//				cls: pageName === 'dashboards.aspx' ? 'selected' : '',
				handler: function () {
					window.open("/FlexModule.aspx?birst.module=dashboard", '_self');
				}
			});
			headerItems.push({
				text: Birst.LM.Locale.get('GLNB_VISUALIZER'),
				itemId: 'visualizer',
				scope: this,
				border: false,
				hidden: true,
				margin: '0 50 0 0',
//				cls: pageName === 'dashboards.aspx' ? 'selected' : '',
				handler: function () {
					window.open("/Visualizer/visualizer.aspx?standalone=true", '_self');
				}
			});
			headerItems.push({
				text: Birst.LM.Locale.get('GLNB_DESIGNER'),
				itemId: 'designer',
//				cls: pageName === 'designer.aspx' ? 'selected' : '',
				border: false,
				margin: '0 50 0 0',
				scope: this,
				handler: function () {
					window.open("/FlexModule.aspx?birst.module=designer", '_self');
					}
				});
			headerItems.push({
				text: Birst.LM.Locale.get('GLNB_ADMIN'),
				itemId: 'admin',
				scope: this,
				border: false,
				margin: '0 50 0 0',
				handler: function () {
					window.open("/FlexModule.aspx?birst.module=admin", '_self');
					}
				});

		}

		//Fico white labels
		var supportUrl = Birst.Utils.HiddenValues.getValue('BirstSupportUrl');
		var helpUrl =  Birst.Utils.HiddenValues.getValue('BirstHelpUrl');
		var logOutUrl =  Birst.Utils.HiddenValues.getValue('BirstLogoutUrl');

		this.menu = Ext.create('Ext.menu.Menu', {
			floating: true,
			region: 'east',
			flex: 0.4,
			id: 'id-header-menu',
			iconCls: 'icon-userMenu',
			header: false,

			items: [
					{
						text: Birst.LM.Locale.get('GLNB_USERMENU_SETTINGS'),
						handler: function() {
							window.open("/Account.aspx", '_self');
							},
						iconCls: 'icon-settings'
					},
					{
						text: Birst.LM.Locale.get('GLNB_USERMENU_SUPPORT'),
						handler: function() {
							window.open(supportUrl, '_blank');
							},
						iconCls: 'icon-support',
						hidden: !supportUrl
					},
					{
						text: Birst.LM.Locale.get('GLNB_USERMENU_HELP'),
						handler: function() {
							window.open(helpUrl, '_blank');
							},
						iconCls: 'icon-help',
						hidden: !helpUrl
					},
					{
						text: Birst.LM.Locale.get('GLNB_USERMENU_LOGOUT'),
						handler: function() {
							window.open("/Logout.aspx", '_self');
							},
						iconCls: 'icon-logout',
						hidden : !logOutUrl
					}

			]
		});

		headerItems.push({
			text: Birst.LM.Locale.get('GLNB_USER'),
			id: 'headerMenuLabel',
			cls: 'headerPerson',
			border: false,
			iconCls: 'headerMenu',
			iconAlign: 'right',
			margin: '0 12 0 50',
			menu: this.menu,
			menuAlign: 'tr-br?'
		});


		config.items = [
			 { xtype: 'panel',
			   region: 'west',
			   itemId: 'imgPanel',
			   border: false,
			   flex: 0.4,
			   cls: 'cr-birst-logo',
			   items: [
				 Ext.create('Ext.Img', {
					 itemId: 'image',
					 border: false,
					 src: '/Logo.aspx'
				 })
				]
			 },
			 {	xtype: 'panel',
				region: 'center',
				itemId: 'centerPanel',
				flex: 0.4,
				padding: '0 40 0 0',
				border: false,
				layout: {
					type: 'vbox',
					pack: 'center',
					align: 'center',
					flex: 1
				 },
				 items: {
					 xtype: 'label',
					 cls: 'headerSpaceName',
					 itemId: 'headerSpaceName',
					 width: "90%"
				 },
				 hidden: (pageName === 'home.aspx')
			 },
			Ext.create('Ext.toolbar.Toolbar', {
				height: 46,
				border: false,
				bodyBorder: false,
				items: headerItems,
				region: 'east',
				id: 'headerToolbar',
				cls: 'headerToolbar'
			  })
		];         // fi north panel items

		this.callParent([config]);

		this.on('afterrender', function(me, eOpts) {
			if (! this.toolTip) {
				var spaceNameLabel = this.getSpaceNameLabel();
				if (spaceNameLabel) {
					this.toolTip = Ext.create('Ext.tip.ToolTip', {
						target: spaceNameLabel.getEl(),
						html: ''
					});

				}

				Birst.core.Webservice.acornRequest( 'getAppConfigParameters', [], this, function(response, options) {
					var strings = response.responseXML.getElementsByTagName( 'string' );
					var vizFeatureFlag = false;
					for( var i = 0, ln = strings.length; i < ln; i ++ ) {
						var arr = getXmlValue( strings[i] ).split( '=' );
						if( arr[0] === 'EnableVisualizerForAll' ){
							if( arr[1] === 'true' ) {
								vizFeatureFlag = true;
							}
							break;
						}
					}

					Birst.core.Webservice.acornRequest('GetLoggedInSpaceDetails',
						[],
						this,
						function (response, options) {
							var spaceName = response.responseXML.getElementsByTagName('SpaceName');
							var spaceId = response.responseXML.getElementsByTagName('SpaceID');
							var freetrial =  response.responseXML.getElementsByTagName('FreeTrialUser');
							var enableVisualizer = response.responseXML.getElementsByTagName('EnableVisualizer');

							if (freetrial && freetrial.length > 0) {
								freetrial =  getXmlValue(freetrial[0]) == "true" ? true : false;
							}
							else {
								freetrial = false;
							}

							if (enableVisualizer && enableVisualizer.length > 0) {
								enableVisualizer =  getXmlValue( enableVisualizer[0] ) == 'true' ? true : false;
							} else {
								enableVisualizer = false;
							}
							var appversion = response.responseXML.getElementsByTagName('AppVersion');
							if (appversion && appversion[0]){
								appversion = getXmlValue(appversion[0]);
							}
							else
								appversion = '';

							var hostname = response.responseXML.getElementsByTagName('HostName');
							if (hostname && hostname[0]){
								hostname = getXmlValue(hostname[0]);
							}
							else
								hostname = '';

							var isrepoadmin = getXmlValue(response.responseXML.getElementsByTagName('SuperUser')[0]) == "true" ? true : false;

							// settings menu opt
							Ext.getCmp('id-header-menu').items.getAt(0).setVisible(!freetrial);

							if (spaceName && spaceName.length > 0) {
								spaceName = getXmlValue(spaceName[0]);
							}
							else {
								spaceName = '';
							}

							if (spaceId && spaceId.length > 0) {
								spaceId = getXmlValue(spaceId[0]);
							}
							else {
								spaceId = '';
							}

							var spaceInfo = { spaceId: spaceId,
											  appVersion: appversion,
											  hostName: hostname,
											  isAdmin: isrepoadmin
											};
							this.setSpaceInfo(spaceName, spaceInfo);
							var toolbar = this.getComponent('headerToolbar');
							this.hideItemByTagName(response.responseXML, 'designer', 'EnableAdhoc');
							this.hideItemByTagName(response.responseXML, 'dashboards', 'EnableDashboards');
							var enableAdmin = response.responseXML.getElementsByTagName('AdminAccess');
							if (!enableAdmin || enableAdmin.length === 0) {
								if (toolbar) {
									var designer = toolbar.getComponent('admin');
									if (designer) {
										designer.hide();
									}
								}
							}

							if( !( !enableVisualizer || !vizFeatureFlag ) ) {
								if (toolbar) {
									var visualizer = toolbar.getComponent( 'visualizer' );
									if ( visualizer ) {
										visualizer.show();
									}
								}
							}

							var userId =  response.responseXML.getElementsByTagName('UserID');
							if (userId && userId.length > 0) {
								userId =  getXmlValue(userId[0]);
							}
							else {
								userId = null;
						   }

						   Birst.Utils.User.userId = userId;
						   Birst.Utils.User.spaceId = spaceId;
						},null,null,null,true
					);
				},null,null,null,true );
				}
			}, this);


		//Delay running the pollrequest till later.
		var task = new Ext.util.DelayedTask(function(){
						Ext.TaskManager.start({
							run: Birst.core.Webservice.pollRequest,
							interval: 8 * 60 * 1000
						});
				   });
		task.delay(30*1000);
	 },

	 hideItemByTagName: function(xml, itemId, tagName) {
		 var toolbar = this.getComponent('headerToolbar');
		 if (toolbar) {
			 var designer = toolbar.getComponent(itemId);
			 var enableAdhoc = xml.getElementsByTagName(tagName);
			 if (enableAdhoc && enableAdhoc.length > 0) {
				if (getXmlValue(enableAdhoc[0]) !== 'true') {
					if (designer) {
						designer.hide();
					}
				}
			}
			else {
				if (designer) {
					designer.hide();
				}
			}
		}
	 },

	 setSpaceInfo: function(spaceName, spaceInfo) {
		 var spaceNameFld = this.getSpaceNameLabel();
		 spaceNameFld.setText(spaceName);
		 if (this.toolTip) {
			 var tiptxt = spaceName + '<br>';
			 if (spaceInfo.isAdmin) {
				 tiptxt += 'ID: '+ spaceInfo.spaceId + '<br>' +
						   'Server: ' + spaceInfo.hostName + '<br>' +
						   'Version: ' + spaceInfo.appVersion;
			 }
			 this.toolTip.update(tiptxt);
		 }
	 },

	 getSpaceNameLabel: function() {
		 var panelFld = this.getComponent('centerPanel');
		 if (panelFld) {
			 return panelFld.getComponent('headerSpaceName');
		 }

		 return null;
	 },

	 refreshLogo: function() {
		 var imgPanel = this.getComponent('imgPanel');
		 if (imgPanel) {
			 var img = imgPanel.getComponent('image');
			 if (img) {
				 img.setSrc('');
				 img.setSrc('/Logo.aspx');
			 }
		 }
	 }
});
