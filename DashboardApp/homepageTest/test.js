Ext.require('Ext.app.Application');
Ext.Loader.setConfig({ enabled: true });

var Application = null;

Ext.onReady(function() {
	
	try {
		    Application = Ext.create('Ext.app.Application', {
		        name: 'DefaultHome',
		        appFolder: '../appHome', 
		        autoCreateViewport: false,
		        controllers: [
		            'SpacesController'
		        ],
		
		        launch: function() {
		        	//jasmine.getEnv().addReporter(new jasmine.HtmlReporter());
		            jasmine.getEnv().addReporter(new jasmine.TrivialReporter());
		            jasmine.getEnv().execute();

		        }
		    });
        }
	catch (e){
		
	   alert(e); 
	}
	
	
	
});