describe("Page Initialization", function() {

    it("ExtJS4 is loaded", function() {
        expect(Ext).toBeDefined();
        expect(Ext.getVersion()).toBeTruthy();
        expect(Ext.getVersion().major).toEqual(4);
    });

    it("Homepage Application instance is created and loaded ",function(){
        expect(DefaultHome).toBeDefined();
    });
    
     it("Space Controller is loaded",function(){
    	
    	expect(Application.getController('SpacesController')).toBeDefined();
        
     });    

});