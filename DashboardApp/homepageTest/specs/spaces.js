describe("Test homepage space controller", function() {
	var spaceCtlr = null, 
		spacesStore = null,
		spaceCnt = null,
        viewport = null;
	//

	 
	beforeEach(function(){
		
	    if (!spaceCtlr) {
	    	spaceCtlr=Application.getController('SpacesController');
	    	spacesStore = spaceCtlr.store; 
	    	spaceCnt = spacesStore.getCount();  
	    	expect(spaceCnt).toBe(0);
 	    	
	        // we must set here the viewport in order for the controller to work normally	
            viewport = Ext.create('DefaultHome.view.Viewport');
	    	spaceCtlr.start();
	    }
	
	    expect(spaceCtlr).toBeTruthy();
	
	    waitsFor(
            function(){ 
            	 spaceCnt = spacesStore.getCount();           	 
                 return (spaceCnt > 0) ? true: false; 
             },
             "space loading never completed",
             2000
        );
	    
        waitsFor(
            function(){ 
            	 var copySpaceBtn = spaceCtlr.getSpaceOptionsContainer().down('[id=copySpace]');         	 
                 return (copySpaceBtn) ? true: false; 
             },
             "view was not rendered",
             3000
        );

   	});

 	
	it("Spaces should have been loaded",function(){
	        
		   expect(spaceCnt).toBeGreaterThan(1);
	 });

     // controller should respond to AddSpace button's' click event   	 
	 it("Has invoked the AddSpace click handler", function() {
		  
	     // Todo: chain with andCallThrough to still track the call but also to delegate 
         // the call to the actual implementation. Here we are injecting a fake 
         // click handler - the objective of this test is primarily to verify DOM/query selectors 
         // used by the Controller to subscribe for the View�s events (in a week MVC decoupling a controller can break if the design changes).
         spaceCtlr.onCreateNewSpace = 
        	jasmine.createSpy("AddSpace Spy").andCallFake(        		
        		function() {            
        });
        
        var addSpaceBtn = viewport.down('button[name=addSpaceBtn]');
        addSpaceBtn.fireEvent('click');
        
        expect(spaceCtlr.onCreateNewSpace).toHaveBeenCalled();
     });

 
	 it("Has invoked the CopySpace callback", function() {
		  
        spaceCtlr.onCopySpace = 
        	jasmine.createSpy("CopySpace Spy").andCallFake(
        		function() {            
        });
                
        var copySpaceBtn = spaceCtlr.getSpaceOptionsContainer().down('[id=copySpace]');
        copySpaceBtn.fireEvent('click');
        expect(spaceCtlr.onCopySpace).toHaveBeenCalled();
                
     }); 

});