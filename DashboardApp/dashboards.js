Ext.ns("Birst.dashboards");

//Temporary - test for url flag to enable new prompt configuration
Birst.dashboards.RS = /rs=true$/.test(document.URL);

Ext.define('Birst.dashboards.Dashlet', {
	title: null,
	path: null,
	enableExcel: null,
	viewMode: null,
	enableAdhoc: null,
	enableSelfSchedule: null,
	enableCSV: null,
	enableViewSelector: null,
	applyAllPrompts: null,
	ignorePromptList: null,
	enableSelectorsCtrl: null,
	enablePPT: null,
	enablePivotCtrl: null,
	enablePDF: null,
	enableSchedule: null,
	enableVisualizer: null,
	isExternalIframe: false,
	externalUrl: '',
	pluginProperties: null,
	isInternalPlugin: false,
	enableDescriptionMode: false,
	dashletDescription: null,

	constructor: function(title, path){
		"use strict";
		this.title = title;
		this.path = path;
		this.pluginProperties = null;
		return this;
	},

	parse: function(xmlObj){
		"use strict";
		var dbs = Birst.core.DashboardSettings;
		var enableDownload = dbs.get( 'EnableDownload' ) === 'true';
		this.path = getXmlChildValue(xmlObj, 'Path');
		this.title = getXmlChildValue(xmlObj, 'Title');
		this.enableExcel = getXmlChildValue(xmlObj, 'EnableExcel') === 'true' && enableDownload;
		this.viewMode = getXmlChildValue(xmlObj, 'ViewMode');
		this.enableAdhoc = getXmlChildValue(xmlObj, 'EnableAdhoc') === 'true';
		this.enableSelfSchedule = getXmlChildValue(xmlObj, 'EnableSelfSchedule') === 'true' && dbs.get( 'SelfScheduleAllowed' ) === 'true';
		this.enableCSV = getXmlChildValue(xmlObj, 'EnableCSV') === 'true' && enableDownload;
		this.enableViewSelector = getXmlChildValue(xmlObj, 'EnableViewSelector') === 'true';
		this.applyAllPrompts = getXmlChildValue(xmlObj, 'ApplyAllPrompts') === 'true';
		this.enableSelectorsCtrl = getXmlChildValue(xmlObj, 'EnableSelectorsCtrl') === 'true';
		this.enablePPT = getXmlChildValue(xmlObj, 'EnablePPT') === 'true' && enableDownload;
		this.enablePivotCtrl = getXmlChildValue(xmlObj, 'EnablePivotCtrl') === 'true';
		this.enablePDF = getXmlChildValue(xmlObj, 'EnablePDF') === 'true' && enableDownload;
		this.enableSchedule = getXmlChildValue(xmlObj, 'EnableSelfSchedule') === 'true' && dbs.get( 'SelfScheduleAllowed' ) === 'true';
		this.enableVisualizer = getXmlChildValue(xmlObj, 'EnableVisualizer') === 'true';
		this.isExternalIframe = getXmlChildValue(xmlObj, 'IsExternalIframe') === 'true';
		this.externalUrl = getXmlChildValue(xmlObj, 'Url');
		this.enableDescriptionMode = getXmlChildValue(xmlObj, 'enableDashletDescriptionMode') === 'true' && Birst.core.DashboardSettings.get('EnableAsyncReportGeneration') == 'true';
		this.dashletDescription = getXmlChildValue(xmlObj, 'DashletDescription');
		var pluginExt = getXmlChildValue(xmlObj, 'PluginExt');
		if(pluginExt && pluginExt.length > 0){
			this.pluginProperties = { ext: pluginExt };
		}

		var x = xmlObj.getElementsByTagName('ignorePromptList');
		if (x && x.length > 0) {
			var ignorePrompts = x[0].getElementsByTagName('item');
			if (ignorePrompts && ignorePrompts.length > 0) {
				var item = ignorePrompts[0];
				this.ignorePromptList = [];
				while (item) {
					if (item.nodeName === 'item') {
						var name = getXmlValue(item);
						this.ignorePromptList.push(name);
					}
					item = item.nextSibling;
				}
			}
		}
	}
});

Ext.define('Birst.dashboards.Layout', {
	direction: 'vertical',
	percentWidth: 100,
	percentHeight: 100,
	layouts: null,
	dashlet: null,
	prompt: null,

	constructor: function(){
		"use strict";
		this.layouts = [];
		this.percentHeight = 100;
		this.percentWidth = 100;
	},

	parse: function(xmlObj){
		"use strict";
		if(xmlObj && xmlObj.tagName === 'Layout'){
			this.direction = xmlObj.getAttribute('direction');
			this.percentWidth = xmlObj.getAttribute('percentWidth');
			this.percentHeight = xmlObj.getAttribute('percentHeight');

			if(xmlObj.hasChildNodes()){
				for(var i = 0; i < xmlObj.childNodes.length; i++){
					var child = xmlObj.childNodes[i];
					if(child.tagName === 'dashlet'){
						var d = Ext.create('Birst.dashboards.Dashlet');
						d.parse(child);
						this.dashlet = d;
						return;
					}else if (child.tagName === 'Layout'){
						var l = Ext.create('Birst.dashboards.Layout');
						l.parse(child);
						this.layouts.push(l);
					}else if (child.tagName === 'Prompt'){ // Embedded prompts
						this.prompt = child.getAttribute('ref');
						return;
					}
				}
			}
		}
	}
});

Ext.define('Birst.dashboards.PageController', {
	page: null,
	container: null,
	promptsContainer: null,
	promptsController: null,
	button:null,
	dashlets: null,
	viewMode: null,
	pagePromptContainer: null,
	leftPromptContainer: null,
	leftPCVisible: false,
	dashletCtrlMaxWin: null,
	npPromptSection: null,
	constructor: function(pageObj, containerObj, promptsContainerObj, button, pagePromptContainer){
	"use strict";
		this.page = pageObj;
		this.container = containerObj;
		this.promptsContainer = promptsContainerObj;
		this.pagePromptContainer = pagePromptContainer;
		if(Birst.dashboards.doNP){
			this.npPromptSection = pagePromptContainer.getComponent(0);
		}
		this.button = button;
		this.dashlets = [];
		this.leftPCVisible = false;
		this.promptsController = Ext.create('Birst.dashboards.PromptsController', this.page.prompts, this.promptsContainer, this);
		var params = Ext.Object.fromQueryString(window.location.search);
		if (params['birst.hideDashboardPrompts'] && params['birst.hideDashboardPrompts'] === 'true') {
			this.promptsController.hidePrompts();
		}
		else {
			if (this.page && this.page.prompts.length > 0) {
				this.promptsContainer.show();
			}else{
				this.promptsController.hidePrompts();
			}
		}
		this.viewMode = 'default';
	},

	render: function(){
	"use strict";
		if(this.container && this.page) {
			if(this.page.layout){
				if (this.page.prompts.length > 0){

					if(Birst.dashboards.doNP){
						this.createLeftSidePromptIndicator();
					}

					var apply = this.promptsController.createPrompts(false);
					if(!this.promptsController.isPromptSessionVarsInSync()){
						this.promptsController.setPromptValuesBySessionVars();
					}
					if(this.filtersOnOpen){
						this.promptsController.applyFilters(this.filtersOnOpen);
						this.filtersOnOpen = null;
					}
					if (apply === false) {
						this.renderToMainContainer();
					}
				}else{
					this.renderToMainContainer();
				}
			}
		}
	},

	hideNPPrompts: function(){
		this.npPromptSection && this.npPromptSection.hide();
	},

	leftSideArrowId: function(){
		return this.pagePromptContainer.getId() + '-arrowIndicator';
	},

	//Create the left prompt drawer once
	createLeftSidePromptIndicator: function(){
		if(!Birst.dashboards.doNP){
			return;
		}

		if(!this.leftPromptContainer){
			var that = this;
			this.leftPromptContainer = Ext.create('Ext.Component', {
				floating: true,
				width: 30,
				height: '100%',
				border: 1,
				//renderTo: Ext.getBody(),
				html: '<h id="' + this.leftSideArrowId() + '" style="position:absolute;top:45%;left:40%;font-weight:bold;" class="entypo">&#59238;</h>',
				listeners: {
					afterrender: {
						fn: function(panel, eOpts){
							panel.alignTo(that.container, "tl");
						}
					},
					el: {
						'mouseover' : function(me){
							var tgt = (me.currentTarget) ? me.currentTarget : me.target;
							tgt.style.opacity = tgt.children[0].style.opacity = 0.5;
						},
						'mouseout' : function(me){
							var tgt = (me.currentTarget) ? me.currentTarget : me.target;
							tgt.style.opacity = tgt.children[0].style.opacity = 0;
						},
						'click' : function(me){
							var tgt = (me.currentTarget) ? me.currentTarget : me.target;
							tgt.style.opacity = tgt.children[0].style.opacity = 0;
							that.toggleLeftPromptContainer();
						}
					}
				},

				style: {
					backgroundColor: '#d3d3d3',
					border: 1,
					opacity: 0
				},

				bodyStyle: {
					opacity: 0
				}

			});
			this.pagePromptContainer.add(this.leftPromptContainer);
			this.leftPromptContainer.show();

			if(this.button.clickToExpandTool){
				this.button.clickToExpandTool.on('click', this.toggleLeftPromptContainer, this);
			}

			this.container.on('resize', function(){
				that.leftPromptContainer.alignTo(that.container, "tl");
			});
		}
	},

	closeVisualizer : function( e, el, o ) {
		o.vis.launched = false;
		e.preventDefault();
		this.destroy();
	},

	/* launch visualizer in IFrame */
	launchVisualizer : function( dashlet ) {
		var that = this;
		return function( btn, event ){
			if( btn.btnEl.dom.getAttribute( 'disabled' ) === null ) {
				var	body = document.getElementsByTagName( 'body' )[0],
					visWindow,
					url = '/Visualizer/index.html?reportName=' + encodeURIComponent(dashlet.path) + '&pivot=true';
				if( !that.visualizer || !that.visualizer.launched ) {
					that.visualizer = {};
					visWindow = Ext.DomHelper.append( body, {
						tag : 'div',
						cls : 'visWinWrap',
						children : [
							{
								cls : 'visWinHeader',
								children : [
									{
										tag : 'a',
										href : '#',
										cls : 'visWinClose',
										id : 'visWinClose',
										html : 'x',
										title : 'Close Visualizer'
									}
								]
							},
							{
								cls : 'visWinInner',
								children : [
									{
										tag : 'iframe',
										src : url,
										cls : 'visWinFrame'
									}
								]
							}
						]
					} );
				}
				var vw = new Ext.Element( visWindow );
				vw.addListener( 'click', that.closeVisualizer, vw, { delegate : '.visWinClose', vis : that.visualizer } );
				that.visualizer.launched = true;
			}
		};
	},
	/* end, launch visualizer in IFrame */

	//Open or close the left prompt drawer
	toggleLeftPromptContainer: function(){
		if(this.pagePromptContainer.items.getCount() < 3){
			this.pagePromptContainer.insert(0, this.promptsContainer);
			this.leftPCVisible = true;
		}else{
			if(this.leftPCVisible){
				this.promptsContainer.hide();
				this.promptsController.closeOpenDialogs();
				this.promptsController.untoggleSelected();
			}else{
				this.promptsContainer.show();
			}
			this.leftPCVisible = !this.leftPCVisible;
		}
		this.toggleArrow(this.leftPCVisible);
		this.leftPromptContainer.alignTo(this.container, "tl");
	},
	
	showLeftPromptContainerByCfg: function(){
		if(Birst.dashboards.doNP && this.page.self.defaultPromptsOpen === true){
			if(!this.leftPCVisible){
				this.toggleLeftPromptContainer();
			}
		}
	},

	toggleArrow: function(left){
		var el = document.getElementById(this.leftSideArrowId());
		if(el){
			el.innerHTML = (left) ? '&#59237;' : '&#59238;';
		}
	},

	//Render to the main area - dashlets, reports, etc
	renderToMainContainer: function(promptData){
	"use strict";
		if(this.page.layout){
			var layoutType = this.page.layout.direction === 'vertical' ? 'vbox' : 'hbox';
			var rootLayoutContainer = this.doLayout(this.page.layout, 'Main', promptData, layoutType, 100);
			this.container.add(rootLayoutContainer);
		}
	},

	//Based on the layout data, position and position each dashlets in the main area accordingly accordingly
	//Each created dashlet will render its report as they are rendered
	doLayout: function(layoutObj, description, promptData, layoutType, flex){
	"use strict";
		this.dashlets = [];

		var container = null;

		var i;

		if(layoutObj){
			var hasDashlet = layoutObj.dashlet !== null;
			var hasPrompt = layoutObj.prompt !== null;
			if (! layoutType) {
				layoutType = layoutObj.direction === 'vertical' ? 'vbox' : 'hbox';
			}
			if (hasDashlet || hasPrompt) {
				layoutType = 'fit';
			}

			var params = {
				layout: {
					type: layoutType,
					align: 'stretch'
				},
				width: layoutObj.percentWidth + '%',
				height: layoutObj.percentHeight + '%',
				border: false
			};
			if (flex) {
				params.flex = parseInt(flex, 10);
			}
			container = Ext.create('Ext.container.Container', params);

			if(Birst.core.oldStyle){
				container.bodyStyle = {
					'border-style': 'none',
					'background': 'rgba(255, 255, 255, 0.1)'
				};
			}

			//Create the dashlet panel
			if(layoutObj.dashlet){
				var dashletObj = layoutObj.dashlet;

				//Dashlet panel toolbar export section

				var toolsExp = [];
				var tools = [];

				var CsvSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" class="pivot-svg"><g class="group"><polygon class="pivot-bg" points="0.2,15.8 0.2,0.2 11.9,0.2 15.8,4.1 15.8,15.8"/><path class="pivot-border" d="M11.8 0.5l3.7 3.7v11.3h-15v-15H11.8 M12 0H0v16h16V4L12 0L12 0z"/><path class="pivot-text" d="M5.8 11.2c-0.3 0.1-0.8 0.3-1.4 0.3c-1.5 0-2.7-1-2.7-2.7c0-1.7 1.1-2.8 2.8-2.8c0.7 0 1.1 0.1 1.3 0.2 L5.7 6.7C5.4 6.6 5 6.5 4.6 6.5c-1.3 0-2.1 0.8-2.1 2.2c0 1.3 0.8 2.2 2.1 2.2c0.4 0 0.9-0.1 1.1-0.2L5.8 11.2z"/><path class="pivot-text" d="M6.6 10.5c0.3 0.2 0.8 0.4 1.2 0.4c0.7 0 1.1-0.4 1.1-0.9c0-0.5-0.3-0.8-1-1.1C7.1 8.5 6.6 8.1 6.6 7.3 c0-0.8 0.7-1.5 1.7-1.5c0.6 0 1 0.1 1.2 0.3L9.3 6.7c-0.2-0.1-0.5-0.3-1-0.3c-0.7 0-1 0.4-1 0.8c0 0.5 0.3 0.8 1.1 1 c0.9 0.4 1.4 0.8 1.4 1.6c0 0.8-0.6 1.6-1.9 1.6c-0.5 0-1.1-0.2-1.4-0.3L6.6 10.5z"/><path class="pivot-text" d="M11.8 11.3L10.1 6h0.8l0.8 2.7c0.2 0.7 0.4 1.4 0.6 2h0c0.2-0.6 0.4-1.3 0.6-2L13.8 6h0.7l-1.9 5.4H11.8z"/></g></svg>';
				this.addDashletHeaderToolButton( layoutObj.dashlet.enableCSV, toolsExp, 'csvExport', 'csvExport', CsvSvg, 'EXP_CSV', function(event, target, owner, tool) {
					if (this.dashletCtrlMaxWin) {
						this.dashletCtrlMaxWin.exportReport('csv', this.dashletCtrlMaxWin.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}else{
						dashletPanel.dashletControl.exportReport('csv', dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}
				} );

				var excelSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" class="pivot-svg"><g class="group"><polygon class="pivot-bg" points="0.2,15.8 0.2,0.2 11.9,0.2 15.8,4.1 15.8,15.8"/><path class="pivot-border" d="M11.8 0.5l3.7 3.7v11.3h-15v-15H11.8 M12 0H0v16h16V4L12 0L12 0z"/><path class="pivot-text" d="M4.9 8.8H2.8v1.9h2.3v0.6h-3V6H5v0.6H2.8v1.7h2.1V8.8z"/><path class="pivot-text" d="M9 11.3l-0.7-1.2C8 9.7 7.8 9.4 7.6 9.1h0C7.5 9.4 7.3 9.7 7 10.2l-0.6 1.2H5.6l1.6-2.7L5.7 6h0.8l0.7 1.3 c0.2 0.4 0.4 0.6 0.5 0.9h0C7.9 7.8 8 7.6 8.2 7.2L8.9 6h0.8L8.1 8.6l1.7 2.8H9z"/><path class="pivot-text" d="M14.1 11.2c-0.3 0.1-0.8 0.3-1.4 0.3c-1.5 0-2.7-1-2.7-2.7c0-1.7 1.1-2.8 2.8-2.8c0.7 0 1.1 0.1 1.3 0.2 L14 6.7c-0.3-0.1-0.6-0.2-1.1-0.2c-1.3 0-2.1 0.8-2.1 2.2c0 1.3 0.8 2.2 2.1 2.2c0.4 0 0.9-0.1 1.1-0.2L14.1 11.2z"/></g></svg>';
				this.addDashletHeaderToolButton( layoutObj.dashlet.enableExcel, toolsExp, '', 'xlsExport', excelSvg, 'EXP_XLS', function(event, target, owner, tool) {
					if (this.dashletCtrlMaxWin) {
						this.dashletCtrlMaxWin.exportReport('xls', this.dashletCtrlMaxWin.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}else{
						dashletPanel.dashletControl.exportReport('xls', dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}
				} );

				var PdfSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" class="pivot-svg"><g class="group"><polygon class="pivot-bg" points="0.2,15.8 0.2,0.2 11.9,0.2 15.8,4.1 15.8,15.8"/><path class="pivot-border" d="M11.8 0.5l3.7 3.7v11.3h-15v-15H11.8 M12 0H0v16h16V4L12 0L12 0z"/><path class="pivot-text" d="M1.9 6C2.2 6 2.6 5.9 3.2 5.9c0.7 0 1.2 0.2 1.5 0.4C5 6.6 5.2 7 5.2 7.5C5.2 8 5 8.4 4.8 8.6 C4.4 9 3.8 9.2 3.1 9.2c-0.2 0-0.4 0-0.6 0v2.2H1.9V6z M2.6 8.6c0.2 0 0.3 0.1 0.6 0.1c0.8 0 1.4-0.4 1.4-1.2 c0-0.7-0.5-1.1-1.3-1.1c-0.3 0-0.5 0-0.7 0.1V8.6z"/><path class="pivot-text" d="M6.1 6C6.5 6 7.1 5.9 7.6 5.9c1 0 1.7 0.2 2.2 0.7c0.5 0.4 0.8 1.1 0.8 1.9c0 0.9-0.3 1.6-0.8 2.1 c-0.5 0.5-1.3 0.8-2.4 0.8c-0.5 0-0.9 0-1.3-0.1V6z M6.8 10.8c0.2 0 0.4 0 0.7 0c1.5 0 2.3-0.8 2.3-2.3c0-1.3-0.7-2.1-2.2-2.1 c-0.4 0-0.6 0-0.8 0.1V10.8z"/><path class="pivot-text" d="M11.5 6h2.9v0.6h-2.2v1.8h2v0.6h-2v2.4h-0.7V6z"/></g></svg>';
				this.addDashletHeaderToolButton( layoutObj.dashlet.enablePDF, toolsExp, '', 'pdfExport', PdfSvg, 'EXP_PDF', function(event, target, owner, tool) {
					if (this.dashletCtrlMaxWin) {
						this.dashletCtrlMaxWin.exportReport('pdf', this.dashletCtrlMaxWin.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}else{
						dashletPanel.dashletControl.exportReport('pdf', dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}
				} );

				var PptSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" class="pivot-svg"><g class="group"><polygon class="pivot-bg" points="0.2,15.8 0.2,0.2 11.9,0.2 15.8,4.1 15.8,15.8"/><path class="pivot-border" d="M11.8 0.5l3.7 3.7v11.3h-15v-15H11.8 M12 0H0v16h16V4L12 0L12 0z"/><path class="pivot-text" d="M1.9 6C2.2 6 2.6 5.9 3.2 5.9c0.7 0 1.2 0.2 1.5 0.4C5 6.6 5.2 7 5.2 7.5C5.2 8 5 8.4 4.8 8.6 C4.4 9 3.8 9.2 3.1 9.2c-0.2 0-0.4 0-0.6 0v2.2H1.9V6z M2.6 8.6c0.2 0 0.3 0.1 0.6 0.1c0.8 0 1.4-0.4 1.4-1.2 c0-0.7-0.5-1.1-1.3-1.1c-0.3 0-0.5 0-0.7 0.1V8.6z"/><path class="pivot-text" d="M6.1 6C6.5 6 6.9 5.9 7.5 5.9c0.7 0 1.2 0.2 1.5 0.4C9.3 6.6 9.4 7 9.4 7.5C9.4 8 9.3 8.4 9 8.6 C8.7 9 8.1 9.2 7.4 9.2c-0.2 0-0.4 0-0.6 0v2.2H6.1V6z M6.8 8.6c0.2 0 0.3 0.1 0.6 0.1c0.8 0 1.4-0.4 1.4-1.2 c0-0.7-0.5-1.1-1.3-1.1c-0.3 0-0.5 0-0.7 0.1V8.6z"/><path class="pivot-text" d="M11.4 6.6H9.8V6h4v0.6h-1.6v4.8h-0.7V6.6z"/></g></svg>';
				this.addDashletHeaderToolButton( layoutObj.dashlet.enablePPT, toolsExp, '', 'pptExport', PptSvg, 'EXP_PPT', function(event, target, owner, tool) {
					if (this.dashletCtrlMaxWin) {
						this.dashletCtrlMaxWin.exportReport('ppt', this.dashletCtrlMaxWin.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}else{
						dashletPanel.dashletControl.exportReport('ppt', dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
					}
				} );

				var schedSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" class="pivot-svg"><g><path class="ss0" d="M8 2c3.3 0 6 2.7 6 6s-2.7 6-6 6s-6-2.7-6-6S4.7 2 8 2 M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8s8-3.6 8-8S12.4 0 8 0 L8 0z"/></g><path class="ss1" d="M8 0.8"/><polyline class="ss2" points="6.3,9.6 8,8 8,4.1"/></svg>';
				this.addDashletHeaderToolButton( layoutObj.dashlet.enableSchedule, toolsExp, '', 'schdlBtn', schedSvg, 'SCHEDL_TIP_CREATE', function(event, target, owner, tool) {
					var path = getXmlChildValue(dashletPanel.dashletControl.adhocReportXml,'Path');
					var name = getXmlChildValue(dashletPanel.dashletControl.adhocReportXml,'Name');
					var prompts = dashletPanel.dashletControl.promptsController.getPromptsData(false, dashletPanel.dashletControl.dashlet.ignorePromptList);
					var view = Ext.widget('mngschedules',{mode:'create',path:path, name:name, prompts: prompts, pagenm: this.button.getText(), dashbnm: this.button.dashbname});
				} );

				var visSvg = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox="0 0 21 16" xml:space="preserve" class="pivot-svg vz-svg"><g><g><path class="vz0" d="M12.8 16L7.2 0h2.3L14 13.9h0L18.7 0h2.2l-5.7 16H12.8z"/></g></g><g class="vz1"><g><g><polygon class="vz2" points="10.6,12.6 4.3,12.6 4.3,16 11.8,16"/></g></g></g><g class="vz1"><g><g><polygon class="vz3" points="9.1,8.4 5.9,8.4 5.9,11.8 10.3,11.9"/></g></g></g><g class="vz1"><g><g><polygon class="vz4" points="7.7,4.2 2.6,4.2 2.6,7.7 8.9,7.7"/></g></g></g><g class="vz1"><g><g><polygon class="vz5" points="6.2,0 0,0 0,3.5 7.4,3.5"/></g></g></g></svg>';
				var enableVis = dashletObj.enableVisualizer && ( Birst.core.DashboardSettings.get( 'EnableVisualizer' ) == 'true' );
				this.addDashletHeaderToolButton( enableVis, toolsExp, 'vzExport',  'vzExport', visSvg, 'VZ_TT', this.launchVisualizer( dashletObj ) );

				var renderSummarySvg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="22 20 16 16" style="enable-background:new 22 20 16 16;" xml:space="preserve"><polygon style="fill:#555555;" points="35,27.3 30.7,31.3 26.3,27.3 "/><rect x="28.5" y="25.1" style="fill:#555555;" width="4.4" height="1.9"/><rect x="28.5" y="23.2" style="fill:#777777;" width="4.4" height="1.6"/><rect x="28.5" y="21.6" style="fill:#999999;" width="4.4" height="1.3"/><g><g><g><rect x="21.9" y="32" style="fill:#555555;" width="14.7" height="1.9"/></g><polygon style="fill:#555555;" points="35.7,31 38.2,32.9 35.7,34.8"/></g><g><g><rect x="22.8" y="21.6" style="fill:#555555;" width="1.9" height="13.1"/></g><polygon style="fill:#555555;" points="21.9,22.6 23.8,20.1 25.7,22.6"/></g></g></svg>';
				var enableRenderSummary = dashletObj.enableDescriptionMode && (Birst.core.DashboardSettings.get('EnableAsyncReportGeneration') == 'true');
				this.addDashletHeaderToolButton( enableRenderSummary, toolsExp, 'summaryRender', 'summaryRender', renderSummarySvg, 'SMR_GEN', function(event, target, owner, tool) {
					dashletPanel.dashletControl.loadAsyncReportAndRender(dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
				});
				
				toolsExp.every(function(e){tools.push(e); return true;});

				tools.push({ xtype: 'button', cls: 'maximize', type: 'maximize', tooltip: Birst.LM.Locale.get('LB_MAXM'), scope: this,
					handler : function(event, target, owner, tool){
						var tools = toolsExp.slice(0);
						tools.push({
							xtype: 'button',
							cls: 'closeButton',
							overCls: 'xb-modwin-close-btn-over',
							tooltip: 'Close',
							scope: this,
							handler: function(ev, te, p){
								dashletPanel.dashletControl.pager.setCurrentPage(this.dashletCtrlMaxWin.pager.getCurrentPage());
								dashletPanel.dashletControl.dashletControlExpanded = null;
								dashletPanel.dashletControl.renderByReportXml2(dashletPanel.dashletControl.promptsController.getPromptsData(), true, this.dashletCtrlMaxWin.adhocReportXml, this.dashletCtrlMaxWin.getSelectorData());
								this.dashletCtrlMaxWin.maxPanel.destroy();
								this.dashletCtrlMaxWin = null;
							}
						});

						var currPage = dashletPanel.dashletControl.pager.getCurrentPage();
						var maxPanel = Ext.create('Ext.window.Window', {
							title: layoutObj.dashlet.title,
							id:Ext.id()+'--maxPanel',
							layout: 'border',
							closable:false,
							cls: 'maximized-dashlet',
							items: [
								{
									xtype: 'panel',
									id: Ext.id()+'--selectorPanel',
									border: false,
									header: false,
									width: '100%',
									layout: 'hbox',
									region: 'north',
									itemId: 'selectorPanel'
								},
								{
									xtype: 'panel',
									id:  Ext.id()+'--DashContainer',
									width: '100%',
									autoScroll: false,
									region: 'center',
									layout: 'fit',
									listeners: {
										scope: this,
										afterrender: function(me) {
											this.dashletCtrlMaxWin = Ext.create('Birst.dashboards.DashletController', layoutObj.dashlet, this.page, maxPanel, me, this.promptsController);
											this.dashletCtrlMaxWin.maxPanel = maxPanel;
											this.dashletCtrlMaxWin.pager.setCurrentPage(currPage);
											this.dashletCtrlMaxWin.renderer.xmlDoc = dashletPanel.dashletControl.renderer.xmlDoc;
											this.dashletCtrlMaxWin.adhocReportXml = dashletPanel.dashletControl.adhocReportXml;
											this.dashletCtrlMaxWin.cachedSelectorsXml = dashletPanel.dashletControl.cachedSelectorsXml;
											dashletPanel.dashletControl.dashletControlExpanded = this.dashletCtrlMaxWin;
											this.dashletCtrlMaxWin.dashletControlExpanded = dashletPanel.dashletControl;
											this.dashletCtrlMaxWin.renderByReportXml2(dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList), true, this.dashletCtrlMaxWin.adhocReportXml, dashletPanel.dashletControl.getSelectorData());
										}
									}
								}
							],
							tools: tools
						});
						maxPanel.show();
						maxPanel.toggleMaximize();
						if( dashletPanel.dashletControl.vzDisabled ) {
							var vzBtn = Ext.ComponentQuery.query( '[type=vzExport]' );
							for( var i = 0, ln = vzBtn.length; i < ln; i ++ ) {
								vzBtn[i].fireEvent( 'disableme' );
							}
						}
					}
				});

				var dashletPanelConfig = {
					title: layoutObj.dashlet.title,
					layout: 'border',
					flex: 1,
					padding: 3,
					cls: 'dashlet',
					items: [{
						xtype: 'panel',
						border: false,
						header: false,
						width: '100%',
						layout: 'hbox',
						region: 'north',
						itemId: 'selectorPanel'
					},
					{
						xtype: 'panel',
						width: '100%',
						autoScroll: false,
						region: 'center',
						layout: 'fit',
						listeners: {
							scope: this,
							afterrender: function(me) {
								var dashletCtrl = Ext.create('Birst.dashboards.DashletController', layoutObj.dashlet, this.page, dashletPanel, me, this.promptsController);
								dashletPanel.dashletControl = dashletCtrl;
								this.dashlets.push(dashletCtrl);
								dashletCtrl.renderByPath(dashletPanel.dashletControl.promptsController.getPromptsData(false,layoutObj.dashlet.ignorePromptList));
							}
						}
					}],
					listeners: {
						scope: this,
						afterrender: function(me){
							// var test = me.el.dom.querySelector( '.pivot-ico' );
							// test.contentDocument.addEventListener( 'click', function(){
							// 	console.log( 'test' );
							// }, false );
							if(Birst.core.oldStyle){
								var headerId = me.getHeader().getId();
								var header = document.getElementById(headerId);
								header.style.borderTopRightRadius =  header.style.borderTopLeftRadius = 4;
								header.style.lineHeight = '80%';
								Birst.DashboardApp.setStyleBGColor(header, 'dashletHeaderColor1', 'dashletHeaderColor2');

								var headerTextSpan = document.getElementById(headerId + '_hd-textEl');
								Birst.DashboardApp.setStyleFont(headerTextSpan, 'dashletHeaderFont');
								me.getHeader().setHeight(parseInt(Birst.core.DashboardSettings.get('dashletHeaderHeight'), 10));
							}


							var params = Ext.Object.fromQueryString(window.location.search);
							if (params['birst.viewMode']) {
								this.setViewMode(params['birst.viewMode'] === 'borderless' ? 'headerBorderless' :
									(params['birst.viewMode'] === 'headerless' ? 'headerless' : 'full'));
							}
							else {
								this.setViewMode(this.viewMode);
							}
						}
					},
					tools: tools
				};

				if(Birst.core.oldStyle){
					dashletPanelConfig.bodyStyle =  {
							'background': 'rgba(256, 256, 256, 1)',
							'border-bottom-right-radius' : '4px',
							'border-bottom-left-radius' : '4px'
					};
				}

				var dashletPanel = Ext.create('Ext.Panel', dashletPanelConfig);
				container.add(dashletPanel);
			}else if (layoutObj.prompt){
				// render prompt instead
				var containerCfg = {
						layout: {
						type: 'fit'
					},
					flex: parseInt(flex, 10),
					width: layoutObj.percentWidth + '%',
					height: layoutObj.percentHeight + '%'
				};

				if(Birst.core.oldStyle){
					containerCfg.bodyStyle = {
						'background': 'rgba(256, 256, 256, 1)'
					};
				}
				container = Ext.create('Ext.container.Container', containerCfg);

				for (i = 0; i < this.promptsController.prompts.length; i++) {
					var p = this.promptsController.prompts[i];
					var ref = p.getAttribute('ref');
					if (ref === layoutObj.prompt && p.getAttribute('isInDashlet') === 'true') {
						var name = getXmlChildValue(p, 'ParameterName');
						var title = getXmlChildValue(p, 'VisibleName');
						var type = getXmlChildValue(p, 'PromptType');
						var promptControl = this.promptsController.promptControls.get(name);

						if (promptControl) {
							this.promptsController.setAsDashletPrompt(promptControl);
							if (!promptControl.control) {
								promptControl.create(container, this.promptsController, true);
							}
							else {
								if(promptControl.isCheckBoxRadio()){
									promptControl.control.hide();
								}else{
									container.add(promptControl.control);
								}
							}
							if (type === 'Query') {
								container.on('resize', function(me, width, height) {
									var clientWidth = me.el.dom.clientWidth;
									var clientHeight = me.el.dom.clientHeight;
									promptControl.control.maxHeight = clientHeight;
									promptControl.control.setSize(clientWidth, clientHeight);
									}, this);
							}
							break;
						}

					}
				}
			}

			//console.log('doLayout() layoutObject has ' + layoutObj.layouts.length + ' children');
			var size = layoutObj.layouts.length;
			for(i = 0; i < size; i++){
				var l = layoutObj.layouts[i];
				var childLayoutType = 'fit';
				if (l.layouts.length > 1) {
					childLayoutType = l.direction === 'vertical' ? 'vbox' : 'hbox';
				}
				//console.log('rendering ' + l);
				var childFlex = layoutObj.direction === 'vertical' ? l.percentHeight : l.percentWidth;
				var childContainer = this.doLayout(l, 'children ' + i, promptData, childLayoutType, childFlex);
				if(childContainer){
					container.add(childContainer);
				}
			}
		}

		return container;
	},

	addDashletHeaderToolButton: function(isEnabled, toolsExp, type, cls, svg, ttLocaleKey, clickHandler){
		var tool = null;
		if(isEnabled){
			toolsExp.push({
				type: type,
				xtype: 'button',
				cls: cls,
				tooltipType : 'title',
				tooltip: Birst.LM.Locale.get(ttLocaleKey),
				scope: this,
				handler: clickHandler,
				html: svg,
				listeners : {
					disableme : function() {
						this.setTooltip( Birst.LM.Locale.get( 'TIP_VISUALIZER_DISABLED' ) );
						this.setDisabled( true );
					},
					enableme : function(){
						this.setTooltip( Birst.LM.Locale.get( ttLocaleKey ) );
						this.setDisabled( false );
					}
				}
			});
			toolsExp.push({xtype: 'tbspacer'});
		}

		return tool;
	},

	updateDashlets: function(useSessionVariables){
	"use strict";
		var promptsXml = this.promptsController.getPromptsData(useSessionVariables);

		for(var i = 0; i < this.dashlets.length; i++){
			var dashletCtrl = this.dashlets[i];
			if (dashletCtrl.dashlet.enableDescriptionMode && Birst.core.DashboardSettings.get('EnableAsyncReportGeneration') == 'true' && dashletCtrl.adhocReportXml == null) {
				continue;
			}
			if (dashletCtrl.dashlet.ignorePromptList) {
				var pData = this.promptsController.getPromptsData(useSessionVariables, dashletCtrl.dashlet.ignorePromptList);
				if (dashletCtrl.dashletControlExpanded) {
					dashletCtrl.dashletControlExpanded.renderByReportXml(pData);
				}
				else {
					dashletCtrl.renderByReportXml(pData);
				}

			}
			else {
				if (dashletCtrl.dashletControlExpanded) {
					dashletCtrl.dashletControlExpanded.renderByReportXml(promptsXml);
				}
				else {
					dashletCtrl.renderByReportXml(promptsXml);
				}
			}
		}
	},

	refresh: function(){
	"use strict";
		var tabCtrl = Dashboard.getApplication().getController('DashboardTabLists');
		tabCtrl.onPageClick(this.button,'refresh');
		//this.container.removeAll();
		//this.promptsController.reset();
		//this.promptsContainer.removeAll();
		//this.render();
		return;
	},

	setViewMode: function(mode){
	"use strict";
		for(var i = 0; i < this.dashlets.length; i++){
			var dashletCtrl = this.dashlets[i];
			dashletCtrl.setViewMode(mode);
		}
		this.viewMode = mode;
	}
});

Ext.define('Birst.dashboards.Page', {
	name: null,
	path: null,
	uuid: null,
	isPartial: false,
	layout: null,
	xmlDoc: null,
	prompts: null,
	dragging: null,
	width: null,
	height: null,

	constructor: function(){
	"use strict";
		this.isPartial = false;
	},

	loadPageIfPartial: function(){
	"use strict";
	},

	parse: function(xmlObj){
	"use strict";
		this.xmlDoc = xmlObj;

		this.self.clickToOpenClosePrompts = false;
		this.self.defaultPromptsOpen  = false;

		var pageXml = xmlObj.nodeName == 'Page' ? xmlObj : xmlObj.getElementsByTagName('Page')[0];
		if(pageXml){
			this.name = pageXml.getAttribute('name');
			this.uuid = pageXml.getAttribute('uuid');

			//Parse layout
			var layouts = pageXml.getElementsByTagName('Layout');
			if (layouts && layouts.length > 0){
				//alert(xml2Str(layouts[0]));
				this.layout = Ext.create('Birst.dashboards.Layout');
				this.layout.parse(layouts[0]);
			}

			this.prompts = pageXml.getElementsByTagName('Prompt');

			if (xmlObj.getElementsByTagName('ClickToOpenClosePrompts')[0] && xmlObj.getElementsByTagName('ClickToOpenClosePrompts')[0].textContent)
			{
				if ( xmlObj.getElementsByTagName('ClickToOpenClosePrompts')[0].textContent == "true") {
					this.self.clickToOpenClosePrompts = true;
				}
			}

			if (xmlObj.getElementsByTagName('DefaultPromptsOpen')[0] && xmlObj.getElementsByTagName('DefaultPromptsOpen')[0].textContent)
			{
				if ( xmlObj.getElementsByTagName('DefaultPromptsOpen')[0].textContent == "true") {
					this.self.defaultPromptsOpen = true;
				}
			}
		}
	},

	statics: {
		getPageByUidPath: function(uid, path, successFn, thisScope){
		"use strict";
			Birst.core.Webservice.dashboardRequest('getPage',
				[{key: 'uuid', value: uid}, {key: 'pagePath', value: path}],
				thisScope,
				function(response, options) {
					var page = Ext.create('Birst.dashboards.Page');
					page.parse(response.responseXML);
					successFn.call(thisScope, page);
				}
			);
		},
		getPagePromptAutoExp: function() {
			return { 'expand' : this.defaultPromptsOpen};
		},
		defaultPromptsOpen: false,
		clickToOpenClosePrompts: false
	}
});

Ext.define('Birst.core.AnyChartTracker', {
	singleton: true,

	map: new Ext.util.HashMap(),
	loc: new Ext.util.HashMap(),

	add: function(id, renderer, rect) {
	"use strict";
		this.map.add(id, renderer);

		//For IE9 disabling of our custom visual filtering
		if(rect){
			var key = renderer.panelId;
			var target = this.loc.get(key);
			if(target){
				target.push({chart: id, rect: rect});
			}else{
				this.loc.add(key, [ {chart: id, rect: rect} ]);
			}
		}
	},

	getRenderer: function(id) {
	"use strict";
		return this.map.get(id);
	},

	//This is used to check if the mouse click touches on an AnyChart for
	//disabling our dragtracker which interferes with AnyChart visual filtering drag
	//select.
	pointIsInCharts: function(x, y, renderer){
		if(this.loc.getCount() == 0){
			return false;
		}
		var target = this.loc.get(renderer.panelId);
		var offsetX = renderer.panel.body.getLeft(false);
		var offsetY = renderer.panel.body.getTop(false);

		if(target){
			for(var i = 0; i < target.length; i++){
				var value = target[i].rect;
				var maxWidth = value.x + offsetX + parseInt(value.width, 10);
				var maxHeight =  value.y + offsetY + parseInt(value.height, 10);
				if( (value.x <= x) && (x  <=  maxWidth) && (value.y <= y) && (y <= maxHeight) ){
					return true;
				}
			}
		}

		return false;
	}
});

Ext.define('Birst.core.Renderer', {
	requires: ['Ext.dd.DragTracker', 'Ext.util.Region'],
	xmlDoc: null,
	panel: null,
	tracker: null,
	dashletCtrl: null,
	maxX: null,
	maxY: null,
	panelId: null,

	constructor: function(panel, dashletCtrl){
	"use strict";
		this.maxX = 0;
		this.maxY = 0;
		this.panel = panel;
		this.panelId = panel.id;
		this.dashletCtrl = dashletCtrl;
	},

	renderChartInFrame: function(frameXml, xOrigin, yOrigin, surface) {
	"use strict";
		var frameX = frameXml.getAttribute('x');
		var frameY = frameXml.getAttribute('y');
		if (frameX) {
			frameX = parseInt(frameX, 10);
		} else {
			frameX = 0;
		}

		if (frameY) {
			frameY = parseInt(frameY, 10);
		} else {
			frameY = 0;
		}

		var child = frameXml.firstChild;

		while(child){
			if(child.tagName === 'chart' || child.tagName === 'gauge'){
				this.renderChart(child, frameX + xOrigin, frameY + yOrigin, surface);
			}
			else if (child.tagName === 'frame') {
				this.renderChartInFrame(child, frameX + xOrigin, frameY + yOrigin, surface);
			}
			child = child.nextSibling;
		}
	},

	renderChart: function(chartXml, xOrigin, yOrigin, surface, scale){
	"use strict";
		var sprite;
		var anychartXml = chartXml.firstChild;
		if(anychartXml && anychartXml.tagName === 'anychart'){
			var chartWidth = chartXml.getAttribute('width');
			var chartHeight = chartXml.getAttribute('height');
			var x = parseInt(chartXml.getAttribute('x'), 10) + xOrigin;
			var y = parseInt(chartXml.getAttribute('y'), 10) + yOrigin;
			if (scale) {
				var sz = this.calculateChartSize(surface, x, y);
				if(sz){
					chartWidth = sz.width;
					chartHeight = sz.height;
				}
			}

			//Fixup for DASH-368. Client side html5 only for now. Could be server side if Flex is ok
			//with the additional parameter
			var series = chartXml.getElementsByTagName('series');
			for(var i = 0; series && i < series.length; i++){
				var se = series[i];
				if(se.getAttribute('type') == 'Spline'){
					se.setAttribute('missing_points', 'no_paint');
				}
			}

			var chartType = 'gauge';
			if (chartXml.nodeName !== 'gauge') {
				var chartTypeXml = chartXml.getElementsByTagName('chart');
				chartType = chartTypeXml[0].getAttribute('plot_type');
			}
			if (chartType === 'Scatter') {
				// do more to determine if its a bubble chart
				var seriesList = chartXml.getElementsByTagName('series');
				if (seriesList && seriesList.length > 0) {
					var series = seriesList[0];
					var isBubble = false;
					while(series && !isBubble) {
						isBubble = (series.getAttribute('type') === 'Bubble');
						series = series.nextSibling;
					}

					if (isBubble) {
						chartType = 'Bubble';
					}
				}
			}
			if (chartType === 'Radar' || chartType === 'Map') {
				sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
				var label = Ext.create('Ext.form.Label', {
					text: Ext.String.format(Birst.LM.Locale.get('CHART_NOTSUPPORTED'),chartType),
					cls: 'chartError',
					y: '50%',
					anchor: '100%'
				});
				sprite.add(label);
			}
			else if(chartType === 'UMap'){
				sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
				var bingMaps = Ext.create('Birst.dashboards.BingMaps', sprite.body.dom.id, chartTypeXml[0], this);
				bingMaps.render();
			}else{
				var chartData = xml2Str(anychartXml);
				AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
				AnyChart.enabledChartMouseEvents = true;

				var chart = new AnyChart();
				chart.width =  chartWidth;
				chart.height = chartHeight;
				
				this.maxX = Math.max(this.maxX, chart.width + x);
				this.maxY = Math.max(this.maxY, chart.height + y);
				chart.setData(chartData);

				chart.addEventListener('pointClick', this.handleChartClick);
				//chart.addEventListener('pointSelect', this.handleChartClick);
				var obj = this;
				if (this.isVisualFilter()) {
					chart.addEventListener('multiplePointsSelect', function(e) {
											obj.handleSelectionChange(e, chart);
					});
				}
				sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
				chart.write(sprite.body.dom.id);

				//We add the chart's location for each renderer so we can check if we are clicking
				//on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
				//visual filtering.
				Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: chartWidth, height: chartHeight} );
				
				if(scale){
					surface.on('resize', function(){
						var sz = this.calculateChartSize(surface, x, y);
						if(sz){
							chart.setSize(sz.width , sz.height);
							sprite.setSize(sz.width, sz.height);
						}
					}.bind(this));
				}

			}

		}
	},

	calculateChartSize: function(surface, x, y){
		var r = surface.el.getRegion();
		if (r.right != r.left && r.top != r.bottom) {
			var w = surface.el.dom.offsetWidth - surface.el.dom.clientWidth;
			var h = surface.el.dom.offsetHeight - surface.el.dom.clientHeight;
			var chartWidth = r.right - r.left - w - x - 15;
			var chartHeight = r.bottom - r.top - h - y - 15;
			surface.setAutoScroll(false);
			return {width: chartWidth, height: chartHeight};
		}
		return null;
	},
	
	createChartContainer: function(surface, x, y, chartWidth, chartHeight){
	"use strict";
		var sprite = Ext.create('Ext.panel.Panel', {
					x: x,
					y: y,
					width: parseInt(chartWidth, 10),
					height: parseInt(chartHeight, 10),
					layout: 'absolute',
					border: false
		});
		surface.add(sprite);
		return sprite;
	},

	handleMultipleChartSelect: function(event) {
	"use strict";
		// multiple items selected
		var e = event;
	},

	handleChartClick: function(event) {
	"use strict";
		if (event && event.data && event.data.Attributes && event.data.Attributes.DrillType) {
			if (event.data.Attributes.DrillType === 'Drill To Dashboard') {
				var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
				if (ctrlr) {
					var dashParts = event.data.Attributes.targetURL.split(',');
					if (dashParts.length > 1) {
						ctrlr.navigateToDash(dashParts[0], dashParts[1], event.data.Attributes.filters);
					}
				}

			}
			else if (event.data.Attributes.DrillType === 'Drill Down') {
				var me = Birst.core.AnyChartTracker.getRenderer(event.target.id);
				Birst.core.Webservice.dashboardRequest('drillDown',
				   [{key: 'xmlReport', value: xml2Str(me.dashletCtrl.adhocReportXml)},
					{key: 'drillCols', value: event.data.Attributes.drillBy},
					{key: 'filters', value: event.data.Attributes.filters}],
				   event,
				   function(response, options) {
						var me = Birst.core.AnyChartTracker.getRenderer(this.target.id);
						var xml = response.responseXML;
						var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
						me.dashletCtrl.adhocReportXml = xmlReportsArray[0];
						if (me.dashletCtrl.dashlet.enableDescriptionMode) {
							me.dashletCtrl.setupReportForAsync();
						}
						var newPrompts = xml.getElementsByTagName('Prompts');
						if (newPrompts.length > 0) {
							me.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
						}
						else {
							if (me.dashletCtrl.promptsController) {
								me.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
							}
							else {
								me.dashletCtrl.renderByReportXml('<Prompts/>');
							}
						}
				  });
			}

		}
	},

	requestFilterPrompt: function(newPrompts){
		if (newPrompts && newPrompts.getCount() > 0) {
			var keys = newPrompts.getKeys();
			var name = '';
			for (i = 0; i < keys.length; i++) {
				if (i > 0) {
					name = name + ',';
				}
				name = name + keys[i];
			}

			Birst.core.Webservice.adhocRequest('getFilterPrompts',
										[ {key: 'column', value: name}
										],
										this,
										function(response, options) {
											var newPrompts = response.responseXML.getElementsByTagName('prompts');
											var numExistingPrompts = 0;
											var numNewPrompts = 0;

											if (newPrompts.length > 0) {
												var values = options.additionalParam;
												var prompt = newPrompts[0].firstChild;
												while (prompt) {
													var column = getXmlChildValue(prompt, 'ParameterName');
													var promptControl = this.dashletCtrl.promptsController.promptControls.get(column);
													var value = values.get(column);

													var promptToDelete = null;
													if (value) {
														if(promptControl){
															promptControl.setSelectedValuesByArray(value);
															numExistingPrompts++;
															promptToDelete = prompt;
														}else{
															var selectedValues = prompt.getElementsByTagName('selectedValues');
															if (! selectedValues || selectedValues.length === 0) {
																selectedValues = prompt.ownerDocument.createElement('selectedValues');
																prompt.appendChild(selectedValues);
															}
															else {
																selectedValues = selectedValues[0];
															}

															var child = selectedValues.firstChild;
															while (child) {
																selectedValues.removeChild(child);
																child = selectedValues.firstChild;
															}

															var test = {};

															for (var i = 0; i < value.length; i++) {
																if (test[value[i]])
																	continue;

																test[value[i]] = 1;
																var data = selectedValues.ownerDocument.createElement('selectedValue');
																data.appendChild(selectedValues.ownerDocument.createTextNode(value[i]));
																selectedValues.appendChild(data);
															}
															numNewPrompts++;
														}
													}

													prompt = prompt.nextSibling;
													if(promptToDelete){
														newPrompts[0].removeChild(promptToDelete);
													}
												}

												if(numNewPrompts > 0){
													this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
												}else{
													if(numExistingPrompts > 0){
														this.dashletCtrl.promptsController.applyPrompts();
													}
												}
											}
										},
										null,
										newPrompts);


		}
	},

	handleSelectionChange: function(e, chart){
		if (!e || !e.data || !e.data.points || !e.data.points.length) {
			return;
		}

		var pts = e.data.points;

		if (!pts.length) {
			return;
		}

		var newPrompts = new Ext.util.HashMap();

		for(var i = 0; i < pts.length; i++){
			var point = pts[i];
			if(point.hasOwnProperty('Attributes')){
				var attr = point.Attributes;
				for(var y = 0; y < 10; y++){
					if(!attr.hasOwnProperty('column' + y)){
						break;
					}

					var dim = attr['dimension' + y];
					if(dim == 'EXPR'){
						continue;
					}

					var col = attr["column" + y];
					var name = dim + "." + col;
					var value = attr["value" + y];

					var valArray = newPrompts.get(name);
					if (! valArray) {
						valArray = [];
						newPrompts.add(name, valArray);
					}
					valArray.push(value);
				}
			}
		}

		this.requestFilterPrompt(newPrompts);
	},
	/*
	 * 	private function handleMultipleChartSelect(event:AnyChartMultiplePointsEvent):void {
			Tracer.debug("multiple chart select " + event);
			var pts:Array = event.points;
			var columns:Array = [];
			for each (var pt:AnyChartPointInfo in pts) {
				for (var i:uint = 0; i < MAX_CATEGORIES; i++) {
					if (pt.pointCustomAttributes.hasOwnProperty("column" + i) == false)
						break;

					var dim:String = pt.pointCustomAttributes["dimension" + i];

					//We don't do filtering on expressions.
					if(dim == 'EXPR'){
						continue;
					}

					var col:String = pt.pointCustomAttributes["column" + i];
					var name:String = dim + "." + col;
					var columnData:Object = columns[name];
					if (columnData == null) {
						columnData = [];
						columnData["columnType"] = "dimension";
						columnData["dimension"] = dim;
						columnData["column"] = col;
						columnData["datetimeFormat"] = pt.pointCustomAttributes["datetimeFormat" + i];
						columnData["values"] = [];
						columns[name] = columnData;
					}
					columnData["values"].push(pt.pointCustomAttributes["value" + i]);
				}
			}
			sendSelectionEvent(columns);
		}

		private function sendSelectionEvent(columns:Array):void {
			var data:XML = XML("<Filters/>");
			for each (var c:Array in columns) {
				var filter:XML = XML("<Filter/>");
				if (c["dimension"] == "EXPR")
					continue;

				var cType:XML = XML("<ColumnType>" + c["columnType"] + "</ColumnType>");
				var dim:XML   = XML("<Dimension>" + c["dimension"] + "</Dimension>");
				var col:XML   = XML("<Column>" + c["column"] + "</Column>");

				filter.appendChild(cType);
				filter.appendChild(dim);
				filter.appendChild(col);

				//If it is a datetime object, it will have a format
				var fmt:String = c["datetimeFormat"];
				if (fmt){
					filter.appendChild(new XML("<datetimeFormat>" + fmt + "</datetimeFormat>"));
				}

				var val:XML = XML("<selectedValues/>");
				for (var j:int = 0; j < c["values"].length; j++) {
					var v:XML = XML("<selectedValue>" + c["values"][j] + "</selectedValue>");
					val.appendChild(v);
				}
				filter.appendChild(val);
				data.appendChild(filter);
			}

			if (parent){
				parent.dispatchEvent(new ReportRendererEvent(ReportRendererEvent.RECTANGLE_SELECT, true, false, data));
			}
		}

	 */
	renderText: function(textXml, surface, xOrigin, yOrigin) {
		"use strict";
		var xPos, yPos;
		var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

		this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
		this.maxY = Math.max(this.maxY, textFld.height + textFld.y);
		var item = {
			type: 'rect',
			width: textFld.width,
			height: textFld.height,
			x: textFld.x,
			y: textFld.y,
			text: textFld.text
		};
		if (textXml.getAttribute('transparent') !== 'true') {
			item.fill = textFld.backColor;

		}
		surface.add(item);
		var bottomBorderColor = textXml.getAttribute('bottomBorderColor');
		var bottomBorderWidth = textXml.getAttribute('bottomBorderWidth');
		var bottomBorderStyle = textXml.getAttribute('bottomBorderStyle');
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				yPos = textFld.y + parseInt(textFld.height, 10);
				surface.add( {
					type: 'path',
					path: "M" + textFld.x + " " + yPos + "l" + parseInt(textFld.width, 10) + " 0",
					"stroke-width": bottomBorderWidth,
					stroke: bottomBorderColor
					}).show(true);
			}
		}
		bottomBorderColor = textXml.getAttribute('leftBorderColor');
		bottomBorderWidth = textXml.getAttribute('leftBorderWidth');
		bottomBorderStyle = textXml.getAttribute('leftBorderStyle');
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				yPos = textFld.y;
				surface.add( {
					type: 'path',
					path: "M" +textFld. x + " " + textFld.y + "l 0 " + parseInt(textFld.height, 10),
					"stroke-width": bottomBorderWidth,
					stroke: bottomBorderColor
					}).show(true);
			}
		}
		bottomBorderColor = textXml.getAttribute('rightBorderColor');
		bottomBorderWidth = textXml.getAttribute('rightBorderWidth');
		bottomBorderStyle = textXml.getAttribute('rightBorderStyle');
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				xPos = parseInt(textFld.x, 10) + parseInt(textFld.width, 10);
				surface.add( {
					type: 'path',
					path: "M" + xPos + " " + textFld.y + "l 0 " + parseInt(textFld.height, 10),
					"stroke-width": bottomBorderWidth,
					stroke: bottomBorderColor
					}).show(true);
			}
		}
		bottomBorderColor = textXml.getAttribute('topBorderColor');
		bottomBorderWidth = textXml.getAttribute('topBorderWidth');
		bottomBorderStyle = textXml.getAttribute('topBorderStyle');
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				yPos = textFld.y + parseInt(textFld.height, 10);
				surface.add( {
					type: 'path',
					path: "M" + textFld.x + " " + textFld.y + "l " + parseInt(textFld.width, 10) + " 0",
					"stroke-width": bottomBorderWidth,
					stroke: bottomBorderColor
					}).show(true);
			}
		}
		textFld.y = parseInt(textFld.y, 10) + (parseInt(textFld.height, 10)/2);
		var sprite = surface.add( {
			type: 'text',
			text: textFld.text,
			fill: textFld.textColor,
			width: textFld.width,
			height: textFld.height,
			x: textFld.textX,
			y: textFld.y,
			"font-family": textFld.fontName,
			"font-size": textFld.fontSize,
			"font-style": 'normal',
			"font-weight": textFld.fontWeight,
			"text-anchor": textFld.anchor
			}
			).show(true);
		var link = textXml.getAttribute('link');
		if (link) {
			sprite.on("click", this.onClick, this, textXml);
			sprite.addCls('clickable');
			}

	},

	renderTextAsHTML: function(textXml, panel, xOrigin, yOrigin) {
		"use strict";
		var width = parseInt(textXml.getAttribute('width'), 10);
		var height = parseInt(textXml.getAttribute('height'), 10);
		var x = parseInt(textXml.getAttribute('x'), 10) + xOrigin;
		var y = parseInt(textXml.getAttribute('y'), 10) + yOrigin;
		var item = {
			html: Birst.core.Webservice.unescapeXml(textXml.getAttribute('value')),
			width: width,
			height: height,
			x: x,
			y: y
		};
		var htmlFld = Ext.create('Ext.Component', item);
		panel.add(htmlFld);
		return htmlFld;
	},

	renderTextAsDiv: function(textXml, panel, xOrigin, yOrigin) {
		"use strict";
		var xPos, yPos;
		var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

		this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
		this.maxY = Math.max(this.maxY, textFld.height + textFld.y);
		var item = {
					width: textFld.width,
					height: textFld.height,
					x: textFld.x,
					y: textFld.y,
					text: textFld.text
		};
		var style = {
			"font-family": textFld.fontName,
			"font-size": textFld.fontSize + 'px',
			"font-style": 'normal',
			"font-weight": textFld.fontWeight,
			"text-align": textFld.alignment,
			"color": textFld.textColor
			};

		if (textXml.getAttribute('transparent') !== 'true') {
			style["background-color"] = textFld.backColor;
		}
		else {
			style["background-color"] = "transparent";
		}
		var bottomBorderColor = textXml.getAttribute('bottomBorderColor');
		var bottomBorderWidth = textXml.getAttribute('bottomBorderWidth');
		var bottomBorderStyle = textXml.getAttribute('bottomBorderStyle');
		style["border-bottom"] = "0px none";
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				style["border-bottom"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
			}
		}
		bottomBorderColor = textXml.getAttribute('leftBorderColor');
		bottomBorderWidth = textXml.getAttribute('leftBorderWidth');
		bottomBorderStyle = textXml.getAttribute('leftBorderStyle');
		style["border-left"] = "0px none";
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				style["border-left"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
			}
		}
		bottomBorderColor = textXml.getAttribute('rightBorderColor');
		bottomBorderWidth = textXml.getAttribute('rightBorderWidth');
		bottomBorderStyle = textXml.getAttribute('rightBorderStyle');
		style["border-right"] = "0px none";
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				style["border-right"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
			}
		}
		bottomBorderColor = textXml.getAttribute('topBorderColor');
		bottomBorderWidth = textXml.getAttribute('topBorderWidth');
		bottomBorderStyle = textXml.getAttribute('topBorderStyle');
		style["border-top"] = "0px none";
		if (bottomBorderWidth) {
			bottomBorderWidth = parseInt(bottomBorderWidth, 10);
			if (bottomBorderWidth > 0) {
				style["border-top"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
			}
		}

		if (textFld.isUnderline) {
			style["text-decoration"] = 'underline';
		}

		item.style = style;
		var link = textXml.getAttribute('link');
		var label = Ext.create('Ext.form.Label', item);
		panel.add(label);
		if (link) {
			label.on('afterrender', function(me, eOpts) {
				me.getEl().on('click', this.onClick, this, eOpts);
			}, this, textXml);
			label.addCls('clickable');
		}
		return label;
	},

	renderButton: function(textXml, panel, xOrigin, yOrigin) {
	"use strict";
		var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

		this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
		this.maxY = Math.max(this.maxY, textFld.height + textFld.y);

		var btn = Ext.create('Ext.button.Button', {
			text: textFld.text,
			width: textFld.width,
			height: textFld.height,
			x: textFld.x,
			y: textFld.y,
			style: {
			"font-family": textFld.fontName,
			"font-size": textFld.fontSize,
			"font-style": 'normal',
			"font-weight": textFld.fontWeight,
			"text-anchor": textFld.anchor
			}
		});
		btn.on("click", this.onClick, this, textXml);
		btn.on("afterrender", function(me, eOpts) {
			var id = me.getId();
			var spanId = id + '-btnInnerEl';
			var el = me.getEl();
			if (el) {
				var span = el.getById(spanId);
				if (span) {
					span.setStyle('font-family', eOpts.fontName);
					span.setStyle('font-size', eOpts.fontSize);
					span.setStyle('font-weight', eOpts.fontWeight);
					span.setStyle('color', eOpts.textColor);
				}
			}
		}, this, textFld);
		panel.add(btn);
	},

	renderImage:  function(textXml, panel, xOrigin, yOrigin) {
		"use strict";
		var x = textXml.getAttribute('x');
		var y = textXml.getAttribute('y');
		x = parseInt(x, 10) + xOrigin;
		y = parseInt(y, 10) + yOrigin;
		var width  = textXml.getAttribute('width');
		var height = textXml.getAttribute('height');
		width = parseInt(width, 10);
		height = parseInt(height, 10);
		var ibackcolor = textXml.getAttribute('backcolor');
		var link = textXml.getAttribute('link');

		var properties = {
			x: x,
			y: y,
			width: width,
			height: height,
			src: '/SMIWeb/' + getXmlValue(textXml),
			style: {
				'background-color': ibackcolor
			}
		};

		if (link && link !== '') {
			properties.cls = 'clickable';
		}
		var img = Ext.create('Ext.Img', properties);

		if (link && link !== '') {
			img.on("afterrender", function(me, eOpts) {
				var el = me.getEl();
				if (el) {
					el.on('click', this.onClick, this, textXml);
				}

			}, this, textXml);
		}

		panel.add(img);
	},

	onClick: function(e, t, eOpts) {
	"use strict";
		var link = new String(eOpts.getAttribute('link'));
		if (link.indexOf('javascript:AdhocContent.DrillThru(') === 0) {
			// drill or navigate
			var parms = link.substring(link.indexOf('('), link.length);
			var paramArray = parms.split(',');
			for (var i = 0; i < paramArray.length; i++) {
				paramArray[i] = Ext.String.trim(paramArray[i]);
			}
			if (paramArray[1] === "'Drill Down'") {
				// drill down
			// send this.xmlDoc, paramArray[2], paramArray[3]
				Birst.core.Webservice.dashboardRequest('drillDown',
					[
						{key: 'xmlReport', value: xml2Str(this.dashletCtrl.adhocReportXml)},
						{key: 'drillCols', value: paramArray[3].slice(1, -1)},
						{key: 'filters', value: paramArray[2].slice(1, -1)}
					],
					this,
					function(response, options) {
						var xml = response.responseXML;
						var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
						this.dashletCtrl.adhocReportXml = xmlReportsArray[0];
						if (this.dashletCtrl.dashlet.enableDescriptionMode) {
							this.dashletCtrl.setupReportForAsync();
						}
						var newPrompts = xml.getElementsByTagName('Prompts');
						if (newPrompts.length > 0) {
							this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
						}
						else {
							if (this.dashletCtrl.promptsController) {
								this.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
							}
							else {
								this.dashletCtrl.renderByReportXml('<Prompts/>');
							}
						}
				});
			}
			else if (paramArray[2] === "'Drill To Dashboard'") {
				var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
				if (ctrlr) {
					ctrlr.navigateToDash(paramArray[0].slice(2, -1), paramArray[1].slice(1, -1), paramArray[3].slice(1, -1));
				}
			}

		}
		else if (link) {
			var dest = eOpts.getAttribute('target');
			if (!dest) {
				dest = '_blank';
			}
			window.open(link, dest);
		}
	},

	clearPanel: function(){
	"use strict";
		var cell = this.panel.body.dom;
		if ( cell.hasChildNodes() ) {
			while ( cell.childNodes.length >= 1 ) {
				cell.removeChild( cell.firstChild );
			}
		}

		this.panel.removeAll();
	},

	renderInMaximizedPanel: function(maxPanel){
	"use strict";
		var refPanel = this.panel;
		this.panel = maxPanel;
		this.render(this.xmlDoc, true);
		this.panel = refPanel;
	},


	parse: function(doc){
	"use strict";
		this.clearPanel();
		this.xmlDoc = doc;
		this.render(this.xmlDoc);
	},

	calcMaxDims: function(item, xOffset, yOffset) {
	"use strict";
		if (item.tagName === 'frame') {
			var frameX = item.getAttribute('x');
			var frameY = item.getAttribute('y');
			if (frameX) {
				frameX = parseInt(frameX, 10);
			} else {
				frameX = 0;
			}

			if (frameY) {
				frameY = parseInt(frameY, 10);
			} else {
				frameY = 0;
			}

			var child = item.firstChild;

			while(child){
				this.calcMaxDims(child, frameX + xOffset, frameY + yOffset);
				child = child.nextSibling;
			}
		}
		else {
			var x = item.getAttribute('x');
			var y = item.getAttribute('y');
			x = parseInt(x, 10) + xOffset;
			y = parseInt(y, 10) + yOffset;
			var width  = parseInt(item.getAttribute('width'), 10);
			var height = parseInt(item.getAttribute('height'), 10);
			if (isFinite(width) && isFinite(x)) {
				this.width = Math.max(this.width, width + x);
			}
			if (isFinite(height) && isFinite(y)) {
				this.height = Math.max(this.height, height + y);
			}
		}
	},

	render: function(doc, dontResize){
		"use strict";
		var parent = this.panel;
		this.panel = Ext.create('Ext.panel.Panel', {
			autoScroll: true,
			layout: 'absolute',
			width: parent.getWidth(),
			height: parent.getHeight()
		});

		if (this.isVisualFilter()) {
			this.panel.on('afterrender', this.createDragSelectTracker, this);
		}

		try {
		this.width = 0; //doc.getAttribute('width');
		this.height = 0; //doc.getAttribute('height');
		if (dontResize) {
			this.width = doc.getAttribute('width');
			this.height = doc.getAttribute('height');
		}
		var numItems = 0;
		var numCharts = 0;

		var child = doc.firstChild;
		while(child){
			this.calcMaxDims(child, 0, 0);
			if (child.tagName ==='chart' || child.tagName ==='gauge') {
				numItems += 1;
				numCharts += 1;
			}
			else if (child.tagName ==='frame' ||
					child.tagName === 'text' ||
					child.tagName === 'html' ||
					child.tagName === 'adhocButton' ||
					child.tagName === 'image') {
				numItems += 1;
			}
			child = child.nextSibling;
		}

		var scaleChart = false;
		if (numItems <= 3 && numCharts === 1) {
			scaleChart = true;
		}


		child = doc.firstChild;
		while(child){
			if(child.tagName === 'text'){
				this.renderTextAsDiv(child, this.panel, 0, 0);
			}
			else if (child.tagName === 'html') {
				this.renderTextAsHTML(child, this.panel, 0, 0);
			}
			else if (child.tagName === 'frame') {
				this.renderFrame(child, 0, 0);
			}
			else if (child.tagName === 'adhocButton') {
				this.renderButton(child, this.panel, 0, 0);
			}
			else if (child.tagName === 'image') {
				this.renderImage(child, this.panel, 0, 0);
			}
			child = child.nextSibling;
		}

		}
		catch (err) {

		}

		parent.add(this.panel);
		this.panel.show();
		child = doc.firstChild;
		while(child){
			if(child.tagName === 'chart' || child.tagName === 'gauge'){
				this.renderChart(child, 0, 0, this.panel, scaleChart);
			}
			else if (child.tagName === 'frame') {
				this.renderChartInFrame(child, 0, 0, this.panel);
			}
			child = child.nextSibling;
		}
		this.panel = parent;
		if (! dontResize) {
			this.panel.setSize(this.maxX, this.maxY);
		}
	},
	
	renderFrame: function(child, x, y) {
				var grandChild = child.firstChild;
				var xOrigin = child.getAttribute('x');
				var yOrigin = child.getAttribute('y');
				if (xOrigin) {
					xOrigin = parseInt(xOrigin, 10);
				} else {
					xOrigin = 0;
				}
				xOrigin += x;

				if (yOrigin) {
					yOrigin = parseInt(yOrigin, 10);
				} else {
					yOrigin = 0;
				}
				yOrigin += y;
				
				var label = null;
				var cellType = null;
				var childCounts = 0;
				while (grandChild) {
					if(grandChild.tagName === 'text'){
						label = this.renderTextAsDiv(grandChild, this.panel, xOrigin, yOrigin);
						childCounts++;
					}
					else if (grandChild.tagName === 'html') {
						label = this.renderTextAsHTML(grandChild, this.panel, xOrigin, yOrigin);
						childCounts++;
					}
					else if (grandChild.tagName === 'adhocButton') {
						this.renderButton(grandChild, this.panel, xOrigin, yOrigin);
					}
					else if (grandChild.tagName === 'net.sf.jasperreports.crosstab.cell.type') {
						cellType = getXmlValue(grandChild);
					}
					else if (grandChild.tagName === 'image') {
						this.renderImage(grandChild, this.panel, xOrigin, yOrigin);
					}
					else if (grandChild.tagName === 'frame') {
						this.renderFrame(grandChild, xOrigin, yOrigin);
					}
					grandChild = grandChild.nextSibling;
				}
				if (label && (cellType === 'Data' || cellType === 'RowHeader' || cellType === 'CrosstabHeader' || cellType === 'ColumnHeader')) {
					var textWidth = parseInt(child.getAttribute('width'), 10);
					var textHeight = Math.max(parseInt(child.getAttribute('height'), 10), label.height);
					if (childCounts > 1) {
						textWidth = label.width + (textWidth - ((label.x - xOrigin) + label.width));
					}

					if (cellType === 'CrosstabHeader') {
						textHeight = label.height;
					}
					else if (childCounts > 1) {
						textHeight = label.height + parseInt(child.getAttribute('height'), 10) - ((label.y - yOrigin) + label.height);
					}

					label.setWidth(Math.min(label.width, textWidth));
					label.setHeight(textHeight);
				}
	},

	cancelClick: function() {
		return !this.tracker.dragging;
	},

	createDragSelectTracker: function() {
		var thisRenderer = this;
		this.tracker = Ext.create('Ext.dd.DragTracker', {
			autoStart: false,
//			constrainTo: this.panel,
			delegate: this.panel.body,
			preventDefault: true,
			trackerPanel: null,
			renderer: this,
			panel: this.panel,

			listeners: {
				beforedragstart: function(that, e, eOpts) {
					var p = e.getXY();

					//This drag tracker interferes with chart visual filtering
					//if we point at any chart area, we do not run this drag tracker
					if(Birst.core.AnyChartTracker.pointIsInCharts(p[0], p[1], thisRenderer)){
						return false;
					}

					//console.log('onBeforeStart: currentTarget is ' + e.currentTarget.id + ' panel is ' + this.panel.body.id);
					if (that.dragTarget && that.dragTarget.id === that.panel.body.id)
					{

						var r = that.el.getRegion();
						var w = that.dragTarget.offsetWidth - that.dragTarget.clientWidth;
						var h = that.dragTarget.offsetHeight - that.dragTarget.clientHeight;
						if (p[0] < (r.right-w) && p[1] < (r.bottom-h)) {
							return true;
						}
					}
					return false;
				}
			},

			onStart: function(e) {
					// Flag which controls whether the cancelClick method vetoes the processing of the DataView's containerclick event.
					// On IE (where else), this needs to remain set for a millisecond after mouseup because even though the mouse has
					// moved, the mouseup will still trigger a click event.
					this.dragging = true;

					this.trackerPanel = Ext.create('Ext.window.Window', {
						header: false,
						style: {
							 background: '#ffcc',
							 opacity: 0.6
							},
						//bodyCls: 'visual-selector',
						x: this.startXY[0],
						y: this.startXY[1],
						width: 1,
						height: 1,
						border: false,
						layout: 'absolute',
						resizable: false,
						minWidth: 1,
						minHeight: 1
						}).show();
			},

			onDrag: function(e) {
				var minX = Math.min(e.xy[0], this.startXY[0]);
				var minY = Math.min(e.xy[1], this.startXY[1]);
				var maxX = Math.max(e.xy[0], this.startXY[0]);
				var maxY = Math.max(e.xy[1], this.startXY[1]);
				Ext.apply(this.dragRegion, {
					top: minY,
					left: minX,
					right: maxX,
					bottom: maxY
				});

				this.trackerPanel.setSize(maxX - minX, maxY - minY);
				this.trackerPanel.setPosition(minX, minY);

//				this.dragRegion.constrainTo(bodyRegion);
			},

			onEnd: Ext.Function.createDelayed(function(e) {
				this.dragging = false;
				this.trackerPanel.close();
				this.renderer.handleDragSelect(this.dragRegion, this.dragTarget.scrollTop, this.dragTarget.scrollLeft);
			}, 1)
		});
		this.tracker.initEl(this.panel.body);
	},

	handleDragSelect: function(region, scrollTopOffset, scrollLeftOffset) {
		"use strict";
		var i;
		// offset region by this.panel.body
		var offsetX = this.panel.body.getLeft(false) - scrollLeftOffset;
		var offsetY = this.panel.body.getTop(false) - scrollTopOffset;
		var minX = region.left - offsetX;
		var minY = region.top - offsetY;
		var maxX = region.right - offsetX;
		var maxY = region.bottom - offsetY;

		var elementArray = this.findIntersectingTextFields(minX, minY, maxX, maxY, this.xmlDoc);

		// find columns and getFilterPrompt
		var newPrompts = new Ext.util.HashMap();
		for (i = 0; i < elementArray.length; i++) {
			if (getXmlChildValue(elementArray[i], 'columnType') === 'Dimension') {
				var column = getXmlChildValue(elementArray[i], 'dimension') + '.' + getXmlChildValue(elementArray[i], 'column');
				var value = getXmlChildValue(elementArray[i], 'valueToUse');
				if (!value) {
					value = elementArray[i].getAttribute('value');
				}
				var valArray = newPrompts.get(column);
				if (! valArray) {
					valArray = [];
					newPrompts.add(column, valArray);
				}
				valArray.push(value);
			}
		}

		this.requestFilterPrompt(newPrompts);
	},

	findIntersectingTextFields: function(minX, minY, maxX, maxY, parentDoc) {
	"use strict";
		var elementArray = [];

		var child = parentDoc.firstChild;
		while(child){
			if(child.tagName === 'text'){
				var x = parseInt(child.getAttribute('x'), 10);
				var y = parseInt(child.getAttribute('y'), 10);
				var width  = parseInt(child.getAttribute('width'), 10);
				var height = parseInt(child.getAttribute('height'), 10);
				if (x <= maxX && y <= maxY && x + width > minX && y + height > minY) {
					elementArray.push(child);
				}
			}
			else if (child.tagName === 'frame') {
				var grandChild = child.firstChild;
				var xOrigin = child.getAttribute('x');
				var yOrigin = child.getAttribute('y');
				if (xOrigin) {
					xOrigin = parseInt(xOrigin, 10);
				} else {
					xOrigin = 0;
				}

				if (yOrigin) {
					yOrigin = parseInt(yOrigin, 10);
				} else {
					yOrigin = 0;
				}
				var childElements = this.findIntersectingTextFields(minX - xOrigin, minY - yOrigin, maxX - xOrigin, maxY - yOrigin, child);
				elementArray = elementArray.concat(childElements);
			}
			child = child.nextSibling;
		}

		return elementArray;
	},
	isVisualFilter: function(){
		var rectangleSelect = this.dashletCtrl.page.xmlDoc.getElementsByTagName('RectangleSelect');
		if(rectangleSelect && rectangleSelect.length > 0){
			return getXmlValue(rectangleSelect[0]) === 'false' ? false : true;
		}
		return true;
   }
});

Ext.define('Birst.core.text', {
	x: null,
	y: null,
	width: null,
	height: null,
	textColor: null,
	backColor: null,
	isBold: null,
	isItalic: null,
	fontName: null,
	fontSize: null,
	text: null,
	isUnderline: null,
	alignment: null,
	anchor: null,
	textX: null,
	fontWeight: null,

	constructor: function(textXml, xOrigin, yOrigin) {
	"use strict";
		this.x = textXml.getAttribute('x');
		this.y = textXml.getAttribute('y');
		this.x = parseInt(this.x, 10) + xOrigin;
		this.y = parseInt(this.y, 10) + yOrigin;
		this.width  = textXml.getAttribute('width');
		this.height = textXml.getAttribute('height');
		this.width = parseInt(this.width, 10);
		this.height = parseInt(this.height, 10);

		this.textColor = textXml.getAttribute('color');
		this.backColor = textXml.getAttribute('backcolor');
		this.isBold = textXml.getAttribute('bold') === 'true';
		this.isItalic = textXml.getAttribute('italic') === 'true';
		this.fontName = textXml.getAttribute('face');
		this.fontSize = textXml.getAttribute('size');
		this.text = textXml.getAttribute('value');
		this.isUnderline = textXml.getAttribute('underline') === 'true';
		this.alignment = textXml.getAttribute('alignment');
		this.textX = this.x;
		if (this.alignment === 'left') {
			this.anchor = 'start';
		} else if (this.alignment === 'right') {
			this.anchor = 'end';
			this.textX = parseInt(this.x, 10) + parseInt(this.width, 10);
		}
		else {
			this.anchor = 'middle';
			this.textX = parseInt(this.x, 10) + (parseInt(this.width, 10) / 2);
		}
		this.fontWeight = 400;
		if (this.isBold) {
			this.fontWeight = 800;
		}
	}
});

Ext.define('Birst.dashboards.DashletController', {
	dashlet: null,
	page: null,
	panel: null,
	renderer: null,
	adhocReportXml: null,
	promptsController: null,
	cachedSelectorsXml:null,
	viewSelectorCombo: null,
	columnSelectors: null,
	pager: null,
	pluginObj: null,
	extIframe: null,
	constructor: function(dashletObj, pageObj, panel, renderToPanel, promptsController){
	"use strict";
		this.dashlet = dashletObj;
		this.page = pageObj;
		this.panel = panel;
		this.promptsController = promptsController;
		this.dashletPanel = renderToPanel;

		this.renderer = Ext.create('Birst.core.Renderer', renderToPanel, this);
		this.pager = Ext.create('Birst.dashlet.Pager', this);
		this.panel.addDocked(this.pager);
		return this;
	},

	onResponseRender: function(response, options){
		"use strict";

		var index;
		this.dashletPanel.setLoading(false);
		var xmlDoc = response.responseXML;
		var hasSubReport = xmlDoc.getElementsByTagName( 'com.successmetricsinc.adhoc.AdhocSubreport' ).length > 0;
		if( hasSubReport && this.dashlet.enableVisualizer ) {
			var vzBtn = this.panel.query( '[type=vzExport]' );
			for( var i = 0, ln = vzBtn.length; i < ln; i++ ) {
				vzBtn[i].fireEvent( 'disableme' );
				this.vzDisabled = true;
			}
		}
		var adhocXml = xmlDoc.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
		var idx;
		if(adhocXml && adhocXml.length > 0) {

			for (idx=0; idx < adhocXml.length; idx++) {
				var createdBy = adhocXml[idx].getAttribute('createdBy');
				if (createdBy != null && createdBy != '') {
					this.adhocReportXml = adhocXml[idx];
					if (this.dashlet.enableDescriptionMode) {
						this.setupReportForAsync();
					}
					break;
				}
			}
			if (idx == adhocXml.length ) {
				this.adhocReportXml = adhocXml[0];
				if (this.dashlet.enableDescriptionMode) {
					this.setupReportForAsync();
				}
			}
		}

		var xmlReportRenderer = xmlDoc.getElementsByTagName('reportRenderer');
		if (xmlReportRenderer && xmlReportRenderer.length > 0) {
			var numPages = getXmlChildValue(xmlReportRenderer[0], 'numberOfPages');
			var currentPage = getXmlChildValue(xmlReportRenderer[0], 'page');

			try {
				numPages = parseInt(numPages, 10);
			}
			catch (err) {
				numPages = 1;
			}
			this.pager.setCount(numPages);

			try {
				currentPage = parseInt(currentPage, 10);
			}
			catch (err) {
				currentPage = 0;
			}
			this.pager.setCurrentPage(currentPage);

			this.pager.onLoad();
		}
		
		this.handleSelectorsResponse(xmlDoc);

		var xmlReportsArray = xmlDoc.getElementsByTagName('report');
		if(xmlReportsArray){
			this.renderer.parse(xmlReportsArray[0]);
		}
		
		if (this.dashlet.enableDescriptionMode) {
			var msg = Ext.String.format(Birst.LM.Locale.get('AR_Complete'), this.dashlet.title);
			var title = Birst.LM.Locale.get('AR_Information');
			var ok = Birst.LM.Locale.get('LB_OK');
        	Ext.create('Birst.Util,ErrorDialog', {
                        msg: msg,
                        title: title,
                        btnText: ok
                    }).show();

		}

	},
	
	handleSelectorsResponse: function(xmlDoc){
		var selectors = xmlDoc.getElementsByTagName('Selectors');
		if ((selectors && selectors.length > 0 && (this.dashlet.enableViewSelector || this.dashlet.enableSelectorsCtrl)) || (this.dashletControlExpanded && this.dashlet.enableViewSelector) ) {

			if (!this.cachedSelectorsXml){
				this.cachedSelectorsXml = selectors;
			}

			var selectorPanel = this.panel.getComponent('selectorPanel');

			if (this.dashletControlExpanded) {
				selectors = this.selectorsLastState ? Str2Xml(this.selectorsLastState).getElementsByTagName('Selectors') : selectors;
			}
			var includeChartSelector = getXmlChildValue(selectors[0], 'IncludeChartSelector');
			if (this.dashlet.enableViewSelector && includeChartSelector === 'true') {
				if (this.dashletControlExpanded) {
					selectorPanel.removeAll();
				}

				var chartType = getXmlChildValue(selectors[0], 'ChartType');
				var value = chartType;

				if (this.dashletControlExpanded) {
					if (!chartType && this.cachedSelectorsXml) {
						// determine type based on cached data and resolve conflicts
						// in maximized dashlet mode resolve the chartType null case - group of chart types to show in the dropdown - the "default"
						// or "chart" ones (composite type or any other than default).
						var chartTypeCached =  getXmlChildValue(this.cachedSelectorsXml[0], 'ChartType');
						// if in an original report the chart type wasn't one of the following then we show the chart group or otherwsie we show the default group
						if ( chartTypeCached !== 'Pie' && chartTypeCached !== 'Bar' && chartTypeCached !== 'Column' && chartTypeCached !== 'Line' ) {
							chartType = 'chart';
						}else{
							chartType = 'default';
						}
						value = 'table'; //a null chartType indicates we are in the table view so we will set the selection in the dropdown to table
					}
				}

				var viewSelectorTypes = [];
				if (chartType === 'Pie' || chartType === 'Bar' || chartType === 'Column' || chartType === 'Line' || chartType == 'default' ) {
					viewSelectorTypes.push([Birst.LM.Locale.get('CHART_PIE'), 'Pie']);
					viewSelectorTypes.push([Birst.LM.Locale.get('CHART_BAR'), 'Bar']);
					viewSelectorTypes.push([Birst.LM.Locale.get('CHART_COL'), 'Column']);
					viewSelectorTypes.push([Birst.LM.Locale.get('CHART_LINE'), 'Line']);
				}
				else {
					if (chartTypeCached === undefined) {
						// composite or any other type not in the default group
						if (!chartType) {
							value = 'table';
						} else {
							value = 'chart';
						}
					}

					viewSelectorTypes.push([Birst.LM.Locale.get('CHART_CHART'), 'chart']);
				}
				viewSelectorTypes.push([Birst.LM.Locale.get('CHART_TABLE'), 'table']);

				var viewSelectorStore = Ext.create('Ext.data.ArrayStore', {
					fields: [
						{name: 'label', type: 'string'},
						{name: 'value', type: 'string'}
					],
					data: viewSelectorTypes
				});

				this.viewSelectorCombo = Ext.create('Ext.form.ComboBox', {
					hideLabel: true,
					store: viewSelectorStore,
					queryMode: 'local',
					displayField: 'label',
					cls: 'columnSelector',
					valueField: 'value',
					forceSelection: true,
					editable: false
				});

				index = viewSelectorStore.find('value', value);

				if (selectorPanel) {
					selectorPanel.add(this.viewSelectorCombo);
				}

				this.viewSelectorCombo.select(viewSelectorStore.getAt(index));

				this.viewSelectorCombo.on('select', function(combo, records, eOpts) {
					this.renderByReportXml(this.getPromptData());
				}, this);
			}

			var child = selectors[0].firstChild;
			while (child) {
				if (child.tagName === 'ColumnSelectors' && this.dashlet.enableSelectorsCtrl) {
					if (!this.columnSelectors) {
						this.columnSelectors = new Ext.util.HashMap();
					}

					var colSelectorData = [];
					var selected = getXmlChildValue(child, 'Selected');
					var grandChild = child.firstChild;
					var colSelectorName = getXmlChildValue(child, 'Column');
					while(grandChild) {
						if (grandChild.tagName === 'ColumnSelector') {
							var label = getXmlChildValue(grandChild, 'Label');
							var dimCol = getXmlChildValue(grandChild, 'ID');
							colSelectorData.push([label, dimCol]);
						}

						grandChild = grandChild.nextSibling;
					}

					var colSelectorStore = Ext.create('Ext.data.ArrayStore', {
						fields: [
							{name: 'label', type: 'string'},
							{name: 'value', type: 'string'},
							{name: 'selector'}
						],
						data: colSelectorData
					});

					var colSelectorCombo = Ext.create('Ext.form.ComboBox', {
						hideLabel: true,
						itemId: getXmlChildValue(child, 'Column'),
						store: colSelectorStore,
						queryMode: 'local',
						displayField: 'label',
						valueField: 'value',
						cls: 'columnSelector',
						forceSelection: true,
						editable: false
					});

					this.columnSelectors.add(xml2Str(child), colSelectorCombo);
					index = colSelectorStore.find('value', selected);

					if (selectorPanel) {
						selectorPanel.add(colSelectorCombo);
					}

					colSelectorCombo.select(colSelectorStore.getAt(index));

					colSelectorCombo.on('select', function(combo, records, eOpts) {
						this.renderByReportXml(this.getPromptData());
					}, this);
				}
				child = child.nextSibling;
			}

			if (this.dashletControlExpanded) {
				delete this.dashletControlExpanded;
			}
		}
	},

	onResponseError: function(response, options){
		this.dashletPanel.setLoading(false);
	},

	getPromptData: function() {
	"use strict";
		return this.promptsController.getPromptsData(true);
	},

	hasSelectors: function(){
	"use strict";
		if (this.viewSelectorCombo || this.columnSelectors ) {
			return true;
		}
		return false;
	},

	getSelectorData: function() {
	"use strict";
		var selectorData = '<Selectors>';
		var chartType;
		if (this.viewSelectorCombo) {
			selectorData = selectorData + '<IncludeChartSelector>true</IncludeChartSelector>';
			chartType = this.viewSelectorCombo.getValue();
			if (chartType === 'table') {
				selectorData = selectorData + '<ChartType/>';
			}
			else {
				selectorData = selectorData + '<ChartType>';
				selectorData = selectorData + chartType;
				selectorData = selectorData + '</ChartType>';
				if (chartType !== 'chart') {
					// need to set chart type in the reportXML
					var chart = this.getDefaultChart();
					if (chart) {
						var c = chart.getElementsByTagName('Chart');
						if (c) {
							var cType = c[0].getElementsByTagName('ChartType');
							if (cType) {
								c[0].removeChild(cType[0]);
							}

							cType = this.adhocReportXml.ownerDocument.createElement('ChartType');
							cType.appendChild(this.adhocReportXml.ownerDocument.createTextNode(chartType));
							c[0].appendChild(cType);
						}
					}
				}
			}

			if (chartType === 'table') {
				this.updateXMLReportConfProperty('IncludeTable','true');
			}
			else {
				this.updateXMLReportConfProperty('IncludeTable','false');
			}
		}
		if (this.columnSelectors) {
			this.columnSelectors.each(function(selector, combo, length) {
				var cData = combo.getValue();
				var doc = Str2Xml(selector);
				selector = doc.firstChild;
				var sel = selector.getElementsByTagName('Selected');
				if (sel) {
					sel[0].removeChild(sel[0].firstChild);
				}
				sel[0].appendChild(doc.createTextNode(cData));
				selectorData = selectorData + xml2Str(selector);
				return true;
			},this);
		}
		selectorData = selectorData + '</Selectors>';
		return selectorData;
	},

	getDefaultChart: function() {
	"use strict";
		var defaultChartIndex = getXmlChildValue(this.adhocReportXml, 'DefaultChartIndex');
		if (defaultChartIndex) {
			var entities = this.adhocReportXml.getElementsByTagName('Entities');
			if (entities) {
				var children = entities[0].childNodes;
				for (var i = 0; i < children.length; i++) {
					if (children[i].tagName === 'com.successmetricsinc.adhoc.AdhocChart') {
						var id = getXmlChildValue(children[i], 'ReportIndex');
						if (id === defaultChartIndex) {
							return children[i];
						}
					}
				}
			}
		}

		return null;
	},

	setLoading: function(){
		this.dashletPanel.setLoading({msg: '',  cls: 'dashMask', msgCls: 'dashMaskMsg'});
	},

	renderByPath: function(promptsXml){
	"use strict";
		if(this.dashlet.isExternalIframe && this.dashlet.externalUrl && this.dashlet.externalUrl.length > 0){
			this.dashletPanel.removeAll();
			this.extIframe = null;
			this.renderIframe();
		}else if (this.dashlet.enableDescriptionMode) {
			this.renderDescription(this.dashlet.dashletDescription);
			Birst.core.Webservice.adhocRequest('getReportColumnSelectors',
				[ {key: 'name', value: this.dashlet.path}
				],
				this,
				function(response, options) {
					this.handleSelectorsResponse(response.responseXML);
				}, this.onResponseError, null, true);
		}else if (this.dashlet.pluginProperties){
			this.renderPlugin();
		}else{
			this.actualRenderByPath(promptsXml);
		}
	},
	
	actualRenderByPath: function(promptsXml) {
			SetLoadingMask(this.dashletPanel.id);
			Birst.core.Webservice.adhocRequest('getDashboardRenderedReport',
				[
					{key: 'name', value: this.dashlet.path},
					{key: 'prompts', value: promptsXml},
					{key: 'page', value: this.pager ? this.pager.getCurrentPage().toString() : '0'},
					{key: 'applyAllPrompts', value: 'true'},
					{key: 'pagePath', value: this.page.path},
					{key: 'guid', value: this.page.uuid}
				],
				this,
				this.onResponseRender,this.onResponseError,null,true);
	},
	
	loadAsyncReportAndRender: function(promptsXml) {
		if (!this.adhocReportXml) {
			// load report
			Birst.core.Webservice.adhocRequest('openReport', [ {key: 'filename', value: this.dashlet.path} ],
				this, function(response, options){
					var xmlDoc = response.responseXML;
					var adhocXml = xmlDoc.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
					
					var idx;
					if(adhocXml && adhocXml.length > 0) {
			
						for (idx=0; idx < adhocXml.length; idx++) {
							var createdBy = adhocXml[idx].getAttribute('createdBy');
							if (createdBy != null && createdBy != '') {
								this.adhocReportXml = adhocXml[idx];
								if (this.dashlet.enableDescriptionMode) {
									this.setupReportForAsync();
								}
								break;
							}
						}
						if (idx == adhocXml.length ) {
							this.adhocReportXml = adhocXml[0];
							if (this.dashlet.enableDescriptionMode) {
								this.setupReportForAsync();
							}
						}
					}
					this.renderByReportXml(promptsXml, 0);
				}, this.onResponseError, null, true);
		}
		else {
			if (this.dashlet.enableDescriptionMode) {
				this.setupReportForAsync();
			}
			this.renderByReportXml(promptsXml, 0);
		}
	},

	renderIframe: function(){
		var urlParams = this.promptsController.getUrlParameters(this.dashlet.externalUrl);
		var promptsArray = this.promptsController.getPromptsObjectDefintion();
		
		var iframeId = 'iframe-' + this.dashletPanel.id;
		var iframeMsgPkt = JSON.stringify({
			operation: 'render',
			id : iframeId,
			prompts: promptsArray
		});
		
		
		if (this.extIframe){
			this.extIframe.onload = function(e){
				this.contentWindow.postMessage(iframeMsgPkt, urlParams);
			};
			this.extIframe.src = urlParams;
		}else{
			var iframeComp = Ext.create('Ext.Component', {
											title: this.dashlet.title,
											style: {
												width: '100%',
												height: '100%'
											},
											html: '<iframe id="' + iframeId + '" src="' + urlParams + '" width="100%" height="100%" class="extIframe"></iframe>',
											listeners: {
												scope: this,
												render: {
													fn: function(me) {
														this.extIframe = document.getElementById(iframeId);
														this.extIframe.onload = function(e){
															this.contentWindow.postMessage(iframeMsgPkt, urlParams);
														};
													}
												}
											}
										});
			this.dashletPanel.add(iframeComp);
			
			//Listen to callbacks from the iframe
			window.addEventListener('message', this.receivedIframeMessage.bind(this));
		}
	},
	
	receivedIframeMessage: function(evt){
		if(evt.data){
			try{
				var response = JSON.parse(evt.data);
				if(response){
					if(response.id === this.extIframe.id){
						if(response.operation === 'drillToDashboard' && response.dashboard && response.page){
							this.createInterfaceObject().drillToDashboardPage(response.dashboard, response.page, response.filters);
						}else if(response.operation === 'setFilters' && response.filters){
							this.createInterfaceObject().setFilters(response.filters);
						}
					}
				}
			}catch(e){
				if(console && console.error){
					console.error("Parse error of data from iframe: " + e);
				}
			}
		}
	},
	
	createInterfaceObject: function(dashletCtrl){
		var that = this;
		return {
			drillToDashboardPage: function(dashboard, page, filter){
				var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
				if (ctrlr && dashboard && page) {
					ctrlr.navigateToDash(dashboard, page, filter);
				}
			},
	
			setFilters: function(filters){
				if(filters && filters instanceof Array && filters.length > 0){
					var promptsKV = new Ext.util.HashMap();
					for(var i = 0; i < filters.length; i++){
						var f = filters[i];
						if(f.key, f.value){
							promptsKV.add(f.key, f.value);
						}
					}
					that.renderer.requestFilterPrompt(promptsKV);
				}
			}
		}
	},

	renderDescription: function(description) {
		var item = {
			html: '<span style="position:absolute;left:0;top:50%;height:50%;width:100%;text-align:center;">' + Birst.core.Webservice.unescapeXml(description) + '</span>' +
			'<span style="position:absolute;top:60%;left:0;height:40%;width:100%;text-align:center;"> <font-size:smaller>This report is in Description mode. Please export the report or ' +
				'<u><font color="blue">click here</font></u> to initiate rendering</font></span>',
			width: this.dashletPanel.width,
			height: '50%',
			x: 0,
			y: 0,
			listeners: {
				click: {
					fn: this.renderDescriptionReport,
					scope: this
				}
			},
			cls: 'dashboardDescription'
		};
		var htmlFld = Ext.create('Ext.button.Button', item);
		this.dashletPanel.add(htmlFld);
	},
	
	renderDescriptionReport: function() {
		this.loadAsyncReportAndRender(this.promptsController.getPromptsData(false,this.dashlet.ignorePromptList));
	},
	
	renderPlugin: function(){
		var el = this.dashletPanel.id + '-body';
		var promptsArray = this.promptsController.getPromptsObjectDefintion();
		if(!this.pluginObj){
			var pluginInfo = Birst.DashletPlugins.FileExtension2Plugin.get(this.dashlet.pluginProperties.ext);
			if(pluginInfo){
				//Retrieve the data file associated with this plugin
				if(this.dashlet.path){
					var onResponsePlugin = function(response, options){
								var xmlDoc = response.responseXML;
								var data = {};
								var dataEl = xmlDoc.getElementsByTagName('data');
								if(dataEl && dataEl.length > 0){
									data = getXmlValue(dataEl[0]);
								}
								var prefix = pluginInfo.subDir && pluginInfo.subDir.length > 0 ? pluginInfo.subDir + '/' : '';
								var module = prefix + pluginInfo.module;
								require([module], function(plugin){
									if(plugin.clz){
										this.pluginObj = new plugin.clz(data, this.createInterfaceObject());
										//console.log( JSON.stringify( promptsArray ) );
										this.pluginObj.render(el, promptsArray);
								}
						}.bind(this));
					}.bind(this);

					Birst.core.Webservice.adhocRequest('getDashletPluginReportFile',
						[
							{key: 'name', value: this.dashlet.path},
							{key: 'pagePath', value: this.page.path},
							{key: 'guid', value: this.page.uuid},
							{key: 'logUsage', value: 'true'},
							{key: 'isDashletEdit', value: 'false'}
						],
						this,
						onResponsePlugin,
						this.onResponseError, null, true
					);

				}
			}
		}else{
			this.pluginObj.render(el, promptsArray);
		}
	},

	renderByReportXml: function(promptsXml, usePageNo){
		"use strict";
		if(this.dashlet.isExternalIframe && this.dashlet.externalUrl && this.dashlet.externalUrl.length > 0){
			this.renderIframe();
		}else if (this.dashlet.pluginProperties){
			this.dashletPanel.removeAll();
			this.renderPlugin();
		}else if (this.dashlet.enableDescriptionMode && !this.adhocReportXml) {
			this.renderDescription(this.dashlet.dashletDescription);
		}else{
			if (!this.adhocReportXml)
				return;
			SetLoadingMask(this.dashletPanel.id);
			var selectorData = this.getSelectorData();
			var reportStr = xml2Str(this.adhocReportXml);
			Birst.core.Webservice.adhocRequest('getDashboardRenderedReportFromXML',
				[
					{key: 'xmlData', value: reportStr},
					{key: 'prompts', value: promptsXml},
					{key: 'page', value: usePageNo && this.pager ? this.pager.getCurrentPage().toString() : '0'},
					{key: 'dashletPrompts', value: selectorData},
					{key: 'applyAllPrompts', value: 'true'},
					{key: 'pagePath', value: this.page.path},
					{key: 'guid', value: this.page.uuid}
				],
				this,
				this.onResponseRender,this.onResponseError,null,true
			);
		}
	},

	renderByReportXml2: function(promptsXml, usePageNo, reportXml, selectors){
		"use strict";
		if(this.dashlet.isExternalIframe && this.dashlet.externalUrl && this.dashlet.externalUrl.length > 0){
			this.renderIframe();
		} else if( this.dashlet.pluginProperties ) {
			this.renderPlugin();
		}else if (this.dashlet.enableDescriptionMode && !this.adhocReportXml) {
			this.renderDescription(this.dashlet.dashletDescription);
		} else {
			if (!this.adhocReportXml)
				return;
			SetLoadingMask(this.dashletPanel.id);
			var selectorData = selectors;
			this.selectorsLastState = selectorData;
			var reportStr = xml2Str(reportXml);
			Birst.core.Webservice.adhocRequest('getDashboardRenderedReportFromXML',
				[
					{key: 'xmlData', value: reportStr},
					{key: 'prompts', value: promptsXml},
					{key: 'page', value: usePageNo && this.pager ? this.pager.getCurrentPage().toString() : '0'},
					{key: 'dashletPrompts', value: selectorData},
					{key: 'applyAllPrompts', value: 'true'},
					{key: 'pagePath', value: this.page.path},
					{key: 'guid', value: this.page.uuid}
				],
				this,
				this.onResponseRender,this.onResponseError,null,true
			);
		}
	},

	getCookie: function(cookieName) {
		return document.cookie.split(';').reduce(function(prev, c) {
			var arr = c.split('=');
			return (arr[0].trim() === cookieName) ? arr[1] : prev;
		}, undefined);
	},

	getAntiCSRFToken: function() {
		"use strict";
		if (!this.token) {
			this.token = this.getCookie("XSRF-TOKEN");
		}
		return this.token;
	},

	setupReportForAsync: function() {
		if (!this.adhocReportXml) {
			return;
		}
		
		var node = this.adhocReportXml.getElementsByTagName('AsynchReportGeneration');
		if (node && node.length > 0) {
			setXmlValue(node, 'true');
		}
	},
	
	exportRep: function(response, options) {
		var xmlDoc = response.responseXML;
		var adhocXml = xmlDoc.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
		var idx;
		if(adhocXml && adhocXml.length > 0) {

			for (idx=0; idx < adhocXml.length; idx++) {
				var createdBy = adhocXml[idx].getAttribute('createdBy');
				if (createdBy != null && createdBy != '') {
					this.adhocReportXml = adhocXml[idx];
					if (this.dashlet.enableDescriptionMode) {
						this.setupReportForAsync();
					}
					break;
				}
			}
			if (idx == adhocXml.length ) {
				this.adhocReportXml = adhocXml[0];
				if (this.dashlet.enableDescriptionMode) {
					this.setupReportForAsync();
				}
			}
		}
		
		this.exportReport(options.additionalParam.type, options.additionalParam.promptsXml);
		
	},
	
	exportReport: function(type, promptsXml) {
	"use strict";
		if (!this.adhocReportXml) {
			Birst.core.Webservice.adhocRequest('openReport',
				[
					{key: 'name', value: this.dashlet.path}
				],
				this,
				this.exportRep,this.onResponseError,{ type: type, promptsXml: promptsXml},true);
			
			return;
		}
		Birst.core.Webservice.adhocRequest('preExport',
			[
					{key: 'exportType', value: type},
					{key: 'reportXML', value: xml2Str(this.adhocReportXml)},
					{key: 'reportName', value: ''},
					{key: 'promptsXML', value: promptsXml},
					{key: 'applyAllPrompts', value: 'true'},
					{key: 'dashboardParams', value: ''},
					{key: 'dashboardParamsSeparator', value: ''},
					{key: 'dashletParams', value: this.getSelectorData()}
			], 
			this,
			this.postPreExport, this.onResponseError, {type: type});
	},
	
	postPreExport: function(response, options) {
		var id = response.responseXML.getElementsByTagName('AsynchResultId');
		if (id.length > 0) {
			var type = options.additionalParam.type;
			var inputForm = new Ext.FormPanel({
				standardSubmit: true,
				//url: '/SMIWeb/ExportServlet.jsp',
				//api: 'submit',
				method: 'POST',
				//headers: {
				//	'Content-Type': 'application/x-www-form-urlencoded'
				//},
				defaultType: 'textfield',
				items: [
						{name: 'birst.exportType', value: type},
						{name: 'birst.jasperPrintId', value: getXmlValue(id[0])},
						{name: 'birst.anticsrftoken', value: this.getAntiCSRFToken()}
						],
				hidden: true,
				listeners: {
					render: {
					fn: function(me) {
						me.submit({
							url: '/SMIWeb/ExportServlet.jsp',
							headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
						});
						Ext.defer(function() {Ext.destroy(me); }, 100);
					}
				}
			}

		});
		inputForm.render(Ext.getBody());
			
		}
	},

	exportReportNew: function(type, promptsXml) {
		"use strict";
		var reportStr = this.base64Encode(xml2Str(this.adhocReportXml));
		var selectorData = this.base64Encode(this.getSelectorData());

		Ext.Ajax.request({
			url: '/SMIWeb/ExportServlet.jsp',
			method: 'POST',
			//scope: this,
			params: {
				'birst.exportType': type,
				'birst.reportXML': reportStr,
				'birst.prompts': this.base64Encode(promptsXml),
				'birst.xmlEncoded': 'true',
				'birst.applyAllPrompts': 'true',
				'birst.dashletParams': selectorData,
				'birst.anticsrftoken': this.getAntiCSRFToken()
			},
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			defaultType: 'textfield',
			success: function (response, options) {
				console.log('succs');
			},
			failure: function (response, options) {
				console.log('fail');
			}
		});
	},

	base64Encode: function(s) {
		if (!s)
			return s;
		return Base64.encode(s);
	},

	setViewMode: function(mode){
	"use strict";
		if (mode === 'default') {
			mode = this.dashlet.viewMode;
		}

		switch(mode){
			case 'headerless' :{
				this.panel.getHeader().hide();
				this.panel.setBorder(true);
			}
			break;

			case 'headerBorderless' :{
				this.panel.getHeader().hide();
				this.panel.setBorder(false);
			}
			break;

			default:
				this.panel.getHeader().show();

				//Bug in extjs 4.1, we have to explicitly remove those borderless css classes
				this.removeBorderCssClass();
				this.panel.setBorder('1px');
		}
	},

	removeBorderCssClass: function(){
	"use strict";
		this.panel.body.removeCls('x-docked-noborder-right');
		this.panel.body.removeCls('x-docked-noborder-left');
		this.panel.body.removeCls('x-docked-noborder-top');
		this.panel.body.removeCls('x-docked-noborder-bottom');

		this.panel.header.removeCls('x-docked-noborder-right');
		this.panel.header.removeCls('x-docked-noborder-left');
		this.panel.header.removeCls('x-docked-noborder-top');
		this.panel.header.removeCls('x-docked-noborder-bottom');
	},

	updateXMLReportConfProperty: function(name,value){
	"use strict";
		var propNode= this.adhocReportXml.getElementsByTagName(name);
		setXmlValue(propNode,value);
	}
});

Ext.define('Birst.dashlet.Pager', {
	extend: 'Ext.toolbar.Paging',
	count: null,
	currentPage: null,
	dashletController: null,

	constructor: function(dc) {
		this.dashletController = dc;
		this.dock = 'bottom';
		this.currentPage = 0;
		this.count = 1;
		this.callParent([{buttonAlign: 'middle', cls: 'dashlet-pager'}]);
	},

	getPagingItems: function() {
		var ret = this.callParent();
		ret.pop();
		ret.pop();
		ret.unshift({xtype: 'component', flex: 0.5});
		ret.push({xtype: 'component', flex: 0.5});
		return ret;
	},

	setCurrentPage: function(c) {
	"use strict";
		this.currentPage = c;
	},

	getCurrentPage: function() {
	"use strict";
		return this.currentPage;
	},

	setCount: function(c) {
	"use strict";
		this.count = c;
	},

	getCount: function() {
	"use strict";
		return this.count;
	},

	updateInfo : function(){
	"use strict";
	},

	onLoad : function(){
	"use strict";
		var me = this,
			currPage,
			pageCount,
			afterText,
			count,
			isEmpty;

		count = me.getCount();
		isEmpty = count === 0;
		if (!isEmpty && count > 1) {
			me.show();
			currPage = me.getCurrentPage() + 1;
			pageCount = me.getCount();
			afterText = Ext.String.format(me.afterPageText, isNaN(pageCount) ? 1 : pageCount);
		} else {
			me.hide();
			currPage = 1;
			pageCount = 1;
			afterText = Ext.String.format(me.afterPageText, 0);
		}

		Ext.suspendLayouts();
		me.child('#afterTextItem').setText(afterText);
		me.child('#inputItem').setDisabled(isEmpty).setValue(currPage);
		me.child('#first').setDisabled(currPage === 1 || isEmpty);
		me.child('#prev').setDisabled(currPage === 1  || isEmpty);
		me.child('#next').setDisabled(currPage === pageCount  || isEmpty);
		me.child('#last').setDisabled(currPage === pageCount  || isEmpty);
		me.updateInfo();
		Ext.resumeLayouts(true);

	},

	onPagingBlur : function(e){
	"use strict";
		this.child('#inputItem').setValue(this.getCurrentPage());
	},

	readPageFromInput : function(){
	"use strict";
		var v = this.child('#inputItem').getValue(),
			pageNum = parseInt(v, 10);

		if (!v || isNaN(pageNum)) {
			this.child('#inputItem').setValue(this.getCurrentPage() + 1);
			return false;
		}
		return pageNum;
	},

	onPagingKeyDown : function(field, e){
	"use strict";
		var me = this,
			k = e.getKey(),
			increment = e.shiftKey ? 10 : 1,
			pageNum;

		if (k === e.RETURN) {
			e.stopEvent();
			pageNum = me.readPageFromInput();
			if (pageNum !== false) {
				pageNum -= 1;
				pageNum = Math.min(Math.max(1, pageNum), me.getCount() - 1);
				if(me.fireEvent('beforechange', me, pageNum) !== false){
					me.loadPage(pageNum);
				}
			}
		} else if (k === e.HOME || k === e.END) {
			e.stopEvent();
			pageNum = k === e.HOME ? 1 : me.getCount() - 1;
			field.setValue(pageNum);
		} else if (k === e.UP || k === e.PAGE_UP || k === e.DOWN || k === e.PAGE_DOWN) {
			e.stopEvent();
			pageNum = me.readPageFromInput();
			if (pageNum) {
				if (k === e.DOWN || k === e.PAGE_DOWN) {
					increment *= -1;
				}
				pageNum += increment;
				if (pageNum >= 1 && pageNum <= me.getCount()) {
					field.setValue(pageNum);
				}
			}
		}
	},

	// @private
	beforeLoad : function(){
		"use strict";
		if(this.rendered && this.refresh){
			this.refresh.disable();
		}
	},

	/**
	 * Move to the first page, has the same effect as clicking the 'first' button.
	 */
	moveFirst : function(){
		"use strict";
		if (this.fireEvent('beforechange', this, 1) !== false){
			this.loadPage(0);
		}
	},

	/**
	 * Move to the previous page, has the same effect as clicking the 'previous' button.
	 */
	movePrevious : function(){
		"use strict";
		var me = this,
			prev = me.getCurrentPage() - 1;

		if (prev >= 0) {
			if (me.fireEvent('beforechange', me, prev) !== false) {
				me.loadPage(prev);
			}
		}
	},

	/**
	 * Move to the next page, has the same effect as clicking the 'next' button.
	 */
	moveNext : function(){
		"use strict";
		var me = this,
			total = me.getCount(),
			next = me.getCurrentPage() + 1;

		if (next < total) {
			if (me.fireEvent('beforechange', me, next) !== false) {
				me.loadPage(next);
			}
		}
	},

	/**
	 * Move to the last page, has the same effect as clicking the 'last' button.
	 */
	moveLast : function(){
		"use strict";
		var me = this,
			last = me.getCount() - 1;

		if (me.fireEvent('beforechange', me, last) !== false) {
			me.loadPage(last);
		}
	},

	loadPage: function(page) {
	"use strict";
		this.currentPage = page;
		this.dashletController.renderByReportXml(this.dashletController.promptsController.getPromptsData(), true);
	}
});

Ext.define('Birst.dashboards.BingMaps', {
	width: 0,
	height: 0,
	apiKey: 'AmgA_JU8O0xHVGR43nD1MgKGCRif6NjLNkwJj7r31ZlNm5z7iVjJXt1wCuykqgDk',
	divId: '',
	infoBox: null,
	map: null,
	data: null,
	nameY: null,
	nameYToolTip: null,
	polygons: null,
	gradientRuleList: null,
	colors: null,
	bounds: null,
	visualSelect: false, //we're visual selecting?
	selectStartLoc: null, //visual select start point
	selectEndLoc: null, //visual select end point
	selectPolygon: null, //the polygon used as the selection box
	markers: null,
	nameAttributes: null, //name -> attributes map
	renderer: null,
	canvasIdNumber: 0,
	customPolyColors: null,

	statics: {
		isScriptLoaded: false,
		isLoading: false,
		URL: 'https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1'
	},

	constructor: function(divId, data, renderer){
	"use strict";
		this.divId = divId;
		this.data = data;
		this.renderer = renderer;
		this.nameY = {};
		this.nameYToolTip = {};
		this.nameDrill = {};
		this.customPolyColors = {};
		this.polygons = [];
		this.colors = [];
		this.bounds = [];
		this.visualSelect = false;
		this.selectStartLoc = this.selectEndLoc = this.selectPolygon = null;
		this.markers = [];
		this.nameAttributes = {};
		this.canvasIdNumber = 0;
		this.canvasLayer = {};

		this.parseColorSettings();
		return this;
	},

	//Load the Bing Maps script on demand
	loadScript: function(onLoadSuccess, onLoadError){
		if(!Birst.dashboards.BingMaps.isScriptLoaded){
			Ext.Loader.loadScript({url: Birst.dashboards.BingMaps.URL,
				onLoad: function(){
					//Second call, make to retrieve veapicore.js
					Ext.Loader.loadScript({ url: Microsoft.Maps.Globals.jsPath + 'veapicore.js',
											onLoad: onLoadSuccess});
				}});
			Birst.dashboards.BingMaps.isLoading = true;
		}
	},

	render: function(){
		//Wait for the Bing Maps code to be loaded before we render
		if(!Birst.dashboards.BingMaps.isScriptLoaded){
			if(!Birst.dashboards.BingMaps.isLoading){
				this.loadScript(this.renderOnLoaded.bind(this));
			}else{
			    var that = this;
				//poll for when the script is done loading
				var interval = setInterval(function(){
					if(Birst.dashboards.BingMaps.isScriptLoaded){
						that.renderOnLoaded();
						clearInterval(interval);
					}
				}, 1000);
			}
		}else{
			this.renderOnLoaded();
		}
	},

	renderOnLoaded: function(){
	"use strict";
		Birst.dashboards.BingMaps.isScriptLoaded = true;
		if(!this.map){
			this.markers = [];

			var obj = this;
			Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function() {
						obj.map = new Microsoft.Maps.Map(document.getElementById(obj.divId),  {
										credentials: obj.apiKey,
										mapTypeId : Microsoft.Maps.MapTypeId.road,
										enableSearchLogo: false,
										enableClickableLogo: false,
										customizeOverlays: true,
										showBreadcrumb: false,
										theme: new Microsoft.Maps.Themes.BingTheme()
						});

						Microsoft.Maps.Events.addHandler(obj.map, 'click', obj.onClick.bind(obj));
						if(true){ //false to disable filtering
							Microsoft.Maps.Events.addHandler(obj.map, 'mousedown', obj.onMouseDown.bind(obj));
							Microsoft.Maps.Events.addHandler(obj.map, 'mouseup', obj.onMouseUp.bind(obj));
							Microsoft.Maps.Events.addHandler(obj.map, 'mousemove', obj.onMouseMove.bind(obj));
						}
						
						obj.canvasLayer = new Microsoft.Maps.EntityCollection();
        				Microsoft.Maps.Events.addHandler(obj.canvasLayer, 'entityadded', function (e) {
            				if (e.entity._canvasID) {
            					e.entity.renderCanvas();
            				}
        				});
        				obj.map.entities.push(obj.canvasLayer);
        				
        				Microsoft.Maps.Events.addHandler(obj.map, 'maptypechanged', function(e) {
							for(var i = 0; i < obj.canvasLayer.getLength(); i++){
								var marker = obj.canvasLayer.get(i);
								if (marker._canvasID) {
            						marker.renderCanvas();
            					}
							}
						});
						
        				obj.parseMarkersGetPlacemarks();
					}
			});
		}

	},

	//When user clicks on a marker or polygon / region, we check if it is a drill to
	//dashboard.
	onClick: function(e){
		var evt = e.originalEvent;
		if(evt){
			var type = e.targetType;
			if(type === 'pushpin' || type === 'polygon'){
				var polygon = e.target.trueEntity;
				var drillCfg = this.nameDrill[polygon.drillKey];
				if(drillCfg){
					var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
					if (ctrlr) {
						var dashParts = drillCfg.targetURL.split(',');
						if (dashParts.length > 1) {
							ctrlr.navigateToDash(dashParts[0], dashParts[1], drillCfg.filters);
						}
					}
				}
			}
		}
	},

	//Returns us the lat/lon location from the mouse clicked point
	pointToLocation: function(e){
		var point = new Microsoft.Maps.Point(e.getX(), e.getY());
		var loc = loc = this.map.tryPixelToLocation(point);
		return new Microsoft.Maps.Location(loc.latitude, loc.longitude);
	},

	//When user clicks on map and ctrl key enabled, we check if are doing a visual filtering
	onMouseDown: function(e){
		var evt = e.originalEvent;
		if(evt.ctrlKey && e.targetType === 'map'){
			this.visualSelect = e.handled = true;
			this.selectStartLoc = this.pointToLocation(e);
		}else{
			e.handled = false;
		}
	},

	//After the user has visually select by releasing mouse, we check if any of our
	//markers are within the bounding box. Collect and filter on them
	onMouseUp: function(e){
		var evt = e.originalEvent;
		if(this.visualSelect && this.selectPolygon){
			var locs = this.selectPolygon.getLocations();

			//Sometimes user select from the bottom up instead of top down
			var noFlip = this.selectStartLoc.latitude > this.selectEndLoc.latitude;
			var topLeft = noFlip ? this.selectStartLoc : this.selectEndLoc;
			var bottomRight = noFlip ? this.selectEndLoc : this.selectStartLoc;

			var selectedMarkers = [];
			for(var i = 0; i < this.markers.length; i++){
				var marker = this.markers[i];
				var loc = marker.getLocation();
				var lon = loc.longitude;
				var lat = loc.latitude;
				if(topLeft.longitude > bottomRight.longitude){
					if(topLeft.longitude >= lon && bottomRight.longitude <= lon &&
						topLeft.latitude >= lat && bottomRight.latitude <= lat) {
							selectedMarkers.push(marker);
						}
				}else {
					if(topLeft.longitude <= lon && bottomRight.longitude >= lon &&
						topLeft.latitude >= lat && bottomRight.latitude <= lat){
							selectedMarkers.push(marker);
						}
				}
			}

			if(selectedMarkers.length > 0){
				var newPrompts = new Ext.util.HashMap();
				for (var n = 0; n < selectedMarkers.length; n++){
					var key = selectedMarkers[n].drillKey;
					var attributes = this.nameAttributes[key];
					if(attributes){
						// find columns and getFilterPrompts
						var column = attributes['dimension0'] + '.' + attributes['label0'];
						var value = attributes['value0'];

						var valArray = newPrompts.get(column);
						if (! valArray) {
							valArray = [];
							newPrompts.add(column, valArray);
						}
						valArray.push(value);
					}
				}
				this.renderer.requestFilterPrompt(newPrompts);
			}
		}

		//Reset
		this.visualSelect = false;
		this.selectStartLoc = this.selectEndLoc = null;
		if(this.selectPolygon){
			this.map.entities.remove(this.selectPolygon);
			this.selectPolygon = null;
		}
		e.handled = false;
	},

	//As the mouse moves when we are visual filtering, create a new polyline rectangle each time
	onMouseMove: function(e){
		var evt = e.originalEvent;
		if(evt.ctrlKey && this.visualSelect){
			e.handled = true;

			if(this.selectPolygon){
				this.map.entities.remove(this.selectPolygon);
			}

			this.selectEndLoc = this.pointToLocation(e);
			if(this.selectEndLoc){
				var topRightLoc = new Microsoft.Maps.Location(this.selectStartLoc.latitude, this.selectEndLoc.longitude);
				var bottomLeftLoc = new Microsoft.Maps.Location(this.selectEndLoc.latitude, this.selectStartLoc.longitude);
				var polys = [this.selectStartLoc, topRightLoc, this.selectEndLoc, bottomLeftLoc, this.selectStartLoc];
				this.selectPolygon = new Microsoft.Maps.Polyline(polys, {strokeThickness: 1}); //new Microsoft.Maps.Polygon(polys, null);
				this.map.entities.push(this.selectPolygon);
			}
		}else{
			e.handled = false;
		}
	},

	parseBubbleScatterMarkers: function(){
		if(this.data){
			//resetOverlaysList();

			//Bubble vars
			var points = [];
			var minRadius = 1;
			var maxRadius = 10;
			var minSize = 1;
			var maxSize = 2;
			var values = [];

			var series = [];
			var dt = this.data.getElementsByTagName('data');
			if(dt.length > 0){
				series = dt[0].getElementsByTagName('series');
			}

			for (var i = 0; i < series.length; i++){
				var s = series[i];
				var seriesName = s.getAttribute('name');
				var type = s.getAttribute('type');
				if(type == 'Marker'){
					var markers = s.getElementsByTagName('point');
					for(var x = 0; x < markers.length; x++){
						this.createMarker(markers[x], seriesName);
					}
				}else if(type == 'Bubble'){
					var point = s.getElementsByTagName('point');
					if(point.length > 0){
						var pt = point[0];
						var lat = parseFloat(pt.getAttribute('y'));
						var lon = parseFloat(pt.getAttribute('x'));
						var latLon = new Microsoft.Maps.Location(lat , lon);
						var rgbAttr = pt.getAttribute('color');
						var size = parseFloat(pt.getAttribute('size'));
						if(i == 0){
							minSize = maxSize = size;
						}
						if(size > maxSize){
							maxSize = size;
						}else if (size < minSize){
							minSize = size;
						}
						var obj = { name:seriesName, ll: latLon, sz: size, pt: pt, color: rgbAttr };
						values.push(size);
						points.push(obj);
					}
				}
			}//for(var i..

			//Bubble
			if(points.length == 1){
				this.addCircle(points[0], minRadius);
			}else{
				var range = maxSize - minSize;
				points.sort(function(a, b){ return b.sz - a.sz;});
				for(var y = 0; y < points.length; y++){
					var data = points[y];
					var ratio = ((parseFloat(data.sz) - minSize) / range);
					var radiusSize = (data.sz <= 0) ? minSize :  ratio * maxRadius;
					if(radiusSize < minRadius){
						radiusSize = minRadius;
					}
					radiusSize = Math.round(radiusSize);
					this.addCircle(data, radiusSize);
				}

			}

			this.centerOnBounds();
			if(this.bounds.length == 1){
				this.map.setView({zoom: 17});
			}
		}
	},

	centerOnBounds: function(){
		if(this.bounds.length > 0){
			this.map.setView({ bounds:  Microsoft.Maps.LocationRect.fromLocations(this.bounds) });
		}
	},

	addCircle: function(data, radiusSize){
		var dim = (radiusSize * 10) + 28;
		var loc = data.ll;

		var anchorPt = new Microsoft.Maps.Point(0, 10);
		var marker = null;
		if(data.color && data.color != ''){
			var radius = radiusSize * 2.3;
			marker = this.augmentColorIcon(data.color, loc, { width: dim, height: dim, anchor: anchorPt }, data.name, radius, radius * 3);
			this.canvasLayer.push(marker);
		}else{
			var options = {icon: '/html/resources/images/pin_blue_' + radiusSize + '.png', width: dim, height: dim, anchor: new Microsoft.Maps.Point(30, 30)};
			marker = new Microsoft.Maps.Pushpin(loc, options);
			this.map.entities.push(marker);

			var title = data.name.replace(/\n/g, '<br/>');
			var info = new Microsoft.Maps.Infobox(loc, {title: title, description: '', pushpin: marker});
			this.map.entities.push(info);
		}

		this.parseDrillAttributes(data.pt, data.name);
		marker.drillKey = data.name;

		this.markers.push(marker);
		this.bounds.push(loc);
	},

	createMarker: function(markerXml, seriesName){
		var options = { anchor: new Microsoft.Maps.Point(0,0)};
		var lat = parseFloat(markerXml.getAttribute('y'));
		var lon = parseFloat(markerXml.getAttribute('x'));
		var loc = new Microsoft.Maps.Location(lat , lon);
		var title = seriesName.replace(/\n/g, '<br/>');

		var marker = this.createCustomColoredMarkerIcon(markerXml, loc, options, seriesName);
		if (!marker){
			marker = new Microsoft.Maps.Pushpin(loc, options);
			this.map.entities.push(marker);
			var info = new Microsoft.Maps.Infobox(loc, {title: title, description: '', pushpin: marker});
			this.map.entities.push(info);
		}

		this.parseDrillAttributes(markerXml, seriesName);
		marker.drillKey = seriesName;

		this.markers.push(marker);
		this.bounds.push(loc);
	},

	generateUniqueCanvasId: function() {
		var canvasID = 'canvasElm' + this.canvasIdNumber;
		this.canvasIdNumber++;

		if (window[canvasID]) {
			return this.generateUniqueCanvasId();
		}

		return canvasID;
	},

	createCustomColoredMarkerIcon: function(markerXml, loc, options, seriesName){
		var rgbAttr = markerXml.getAttribute('color');
		var customColored = (rgbAttr && rgbAttr != '');
		if (customColored){
			marker = this.augmentColorIcon(rgbAttr, loc, options, seriesName);
			this.canvasLayer.push(marker);
			return marker;
		}
		return null;
	},

	augmentColorIcon: function(color, loc, options, tooltip, radius, dim){
		var uniqueId = this.generateUniqueCanvasId();
		dim = (dim) ? dim : 24;
		radius = (radius) ? radius : 12;

		tool = tooltip.replace(/\n/g, '&#xa;');

		options.htmlContent = '<div style="pointer-events:all" data-tip="' + tooltip + '"><canvas id="' + uniqueId + '" width="' + dim + '" height="' + dim + '" class="colorMarker"></canvas></div>';
		options.icon = '  ';

		var pushpin = new Microsoft.Maps.Pushpin(loc, options);
		pushpin._canvasID = uniqueId;
		pushpin.renderCanvas = function(){
			var c = document.getElementById(uniqueId);
			if(c){
				//Draw circular icon
				var context = c.getContext('2d');
				context.clearRect(0, 0, c.width, c.height);
				var centerX = c.width / 2;
				var centerY = c.height / 2;
				context.beginPath();
				context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
				context.fillStyle = color;
				context.fill();
				context.lineWidth = 2;
				context.strokeStyle = '#ffffff';
				context.stroke();

			}
		};

		Microsoft.Maps.Events.addHandler(pushpin, 'mouseover', function(evt){
				throw "StopBingThemeMouseOverEvent";
		});

		return pushpin;
	},

	parseColorSettings: function(){
	"use strict";
		if(this.data){
			var clrs = [];
			var plts = this.data.getElementsByTagName('palettes');
			if(plts.length > 0){
				var plt = plts[0].getElementsByTagName('palette');
				if(plt.length > 0){
					var grad = plt[0].getElementsByTagName('gradient');
					if(grad.length > 0){
						clrs = grad[0].getElementsByTagName('key');
					}
				}
			}

			var defaultColors = false;
			for(var i = 0; !defaultColors && i < clrs.length; i++){
				var key = clrs[i];
				var colorStr = key.getAttribute('color');
				if(colorStr === 'green'){
					//hack here - green indicates default values, ie no user defined color palette
					defaultColors = true;
					continue;
				}
				var rgbValue = this.rgbString2int(colorStr);
				this.colors.push(rgbValue);
			}
		}
	},

	rgbString2int: function(str){
		"use strict";
		if (!str) { return -1; }
		if (str.length === 0) { return -1; }

		if (str.length > 1){
			var startIdx = str.indexOf("Rgb(");
			if (startIdx === 0){
				var endIdx = str.indexOf(")", 4);
				if(endIdx > 0){
					var rgbString = str.substring(startIdx + 4, endIdx);
					var rgbValues = rgbString.split(',');
					if(rgbValues.length === 3){
						var r = parseInt(rgbValues[0], 10);
						var g = parseInt(rgbValues[1], 10);
						var b = parseInt(rgbValues[2], 10);
						return r << 16 | g << 8 | b << 0;
					}
				}
			}
		}
		return parseInt(str, 10);
	},

	rgbString2Array: function(str){
		"use strict";
		if (!str) { return -1; }
		if (str.length === 0) { return -1; }

		if (str.length > 1){
			var startIdx = str.indexOf("Rgb(");
			if (startIdx === 0){
				var endIdx = str.indexOf(")", 4);
				if(endIdx > 0){
					var rgbString = str.substring(startIdx + 4, endIdx);
					var rgbValues = rgbString.split(',');
					if(rgbValues.length === 3){
						var r = parseInt(rgbValues[0], 10);
						var g = parseInt(rgbValues[1], 10);
						var b = parseInt(rgbValues[2], 10);
						return [r, g, b];
					}
				}
			}
		}
		return null;
	},

	parseDrillAttributes: function(p, name){
		var hasDrill = false;
		var attrs = p.getElementsByTagName('attributes');
		for(var z = 0;  z < attrs.length; z++){
			var a = attrs[z].getElementsByTagName('attribute');

			var nameAttrVals = {};
			var drillCfg = {};
			for(var i = 1; i < a.length; i++){
				var ab = a[i];
				var attrName = ab.getAttribute('name');
				var attrVal = getXmlValue(ab);
				if ( attrName === 'DrillType'){
					if(attrVal === 'Drill To Dashboard'){
						drillCfg[attrName] = attrVal;
					}
				}else if ( attrName === 'targetURL' || attrName === 'filters'){
					drillCfg[attrName] = attrVal;
				}
				nameAttrVals[attrName] = attrVal;
			}

			this.nameAttributes[name] = nameAttrVals;

			if(drillCfg.DrillType){
				this.nameDrill[name] = drillCfg;
				hasDrill = true;
			}
		}
		return hasDrill;
	},

	parseMarkersGetPlacemarks: function(){
	"use strict";
		if(this.data){
			var mapSeries = null;
			var dataPlotSettings = this.data.getElementsByTagName('data_plot_settings');
			if(dataPlotSettings && dataPlotSettings.length > 0){
				mapSeries = dataPlotSettings[0].getElementsByTagName('map_series');
				if(mapSeries && mapSeries.length > 0){
					mapSeries = mapSeries[0];
				}else{
					mapSeries = null;
				}
			}

			if(!mapSeries){
				return;
			}

			var source = mapSeries.getAttribute('source');

			var polygonKeys = [];
			var dt = this.data.getElementsByTagName('data');
			if(dt && dt.length > 0){
				var series = dt[0].getElementsByTagName('series');
				for(var x = 0; x < series.length; x++){
					var type = series[x].getAttribute('type');
					if(type === 'Bar'){
						polygonKeys.push(series[x]);
					}
				}
			}

			var doPolyRangeColors = true;
			var polySize = polygonKeys.length;
			if(source !== "none" && polySize > 0){
				for (var i = 0; i < polySize; i++){
					var s = polygonKeys[i];
					var points = s.getElementsByTagName('point');
					var count = points ? points.length : 0;
					if(count > 0){
						for(var y = 0; y < count; y++){
							var p = points[y];
							var name = p.getAttribute('name');
							if(!this.nameY[name]){
								this.nameY[name] = p.getAttribute('y');
								var attrs = p.getElementsByTagName('attributes');
								for(var z = 0;  z < attrs.length; z++){
									var a = attrs[z].getElementsByTagName('attribute');
									if(a.length > 0){
										if(a[0].getAttribute('name') === 'umap'){
											var attr = getXmlValue(a[0]);
											this.nameYToolTip[name] = attr;
											this.parseDrillAttributes(p, name);
										}
									}
								}
							}

							//Custom column colors from a column?
							var rgbAttr = p.getAttribute('color');
							var customColor = (rgbAttr && rgbAttr != '');
							if (customColor){
								var rgbColor = this.rgbString2Array(rgbAttr);
								if(rgbColor){
									this.customPolyColors[name] = rgbColor;
									doPolyRangeColors = false;
								}
							}
						}
					}
				}

				this.parsePlacemarks(doPolyRangeColors);
			}else{

				//this.removeGradientBar();
				this.parseBubbleScatterMarkers();

			}
			this.centerOnBounds();
		}
	},

	parsePlacemarks: function(doPolyRangeColors){
		var placemarks = this.data.getElementsByTagName('placemarks');
		if(placemarks.length > 0){
			var minValue = 0;
			var maxValue = 0;
			var values = [];
			var polys = placemarks[0].getElementsByTagName('pm');
			for(var i = 0; i < polys.length; i++){
				var pol = polys[i];
				var key = pol.getAttribute('id');

				//Custom colors?
				var rgbAttr = pol.getAttribute('color');
				var customColored = (rgbAttr && rgbAttr != '');

				var coords = pol.getElementsByTagName('coords');
				for(var y = 0; y < coords.length; y++){
					var latlngs = [];
					var cordsStr = getXmlValue(coords[y]);
					var cordPairs = cordsStr.split(' ');
					for(var z = 0; z < cordPairs.length; z++){
						var cordPairsStr = cordPairs[z];
						var latlon = cordPairsStr.split(',');
						var lat = parseFloat(latlon[0]);
						var lon = parseFloat(latlon[1]);
						var latlng = new Microsoft.Maps.Location(lat, lon);
						latlngs.push(latlng);
						this.bounds.push(latlng);
					}
					var value = this.nameY[key];
					var numValue = parseFloat(value);
					if(numValue){
						if(i === 0){
							minValue = maxValue = numValue;
						}else{
							if(numValue < minValue){
								minValue = numValue;
							}
							if(numValue > maxValue){
								maxValue = numValue;
							}
						}
						values.push(numValue);
					}
					this.createPolygon(key, latlngs, numValue);
				}
			}

			if(doPolyRangeColors !== false){
				this.calculateGradientFromMinMax(minValue, maxValue);
				this.colorRangePolygons(values);
			}

			this.parseBubbleScatterMarkers();
		}
	},

	calculateGradientFromMinMax: function(minValue, maxValue){
	"use strict";
		if(this.colors.length >= 2){
			var rules = [];
			var prev;

			var rangeIncrement = (maxValue - minValue) / this.colors.length;
			var borderValues = [];
			borderValues.push(0);
			var rangeValue = minValue;

			for(var i = 0; i < this.colors.length; i++){
				var color = this.colors[i];
				//last one
				if(i === this.colors.length - 1){
					prev.maxColor = color;
					prev.maxValue = maxValue;
					borderValues.push(maxValue);
					continue;
				}

				var g = Ext.create('Birst.dashboards.GradientRule');
				g.minColor = color;
				if(i === 0){
					g.minValue = minValue;
				}
				if(prev){
					prev.maxColor = color;
				}
				g.maxValue = rangeValue + rangeIncrement;
				prev = g;
				rules.push(g);

				borderValues.push(rangeValue);
				rangeValue += rangeIncrement;
			}
			this.gradientRuleList = Ext.create('Birst.dashboards.GradientRuleList', rules);
			/*
			if(this.gradientBar){
				this.gradientBar.borders = borderValues;
				this.gradientBar.drawGradientRule(this.gradientRuleList);
			}*/
		}else{
			var range = maxValue - minValue;
			var n1 = minValue + (range / 4);
			var n2 = n1 + (range / 4);
			this.calculateGradient(minValue, n1, n2, maxValue);
		}
	},

	createPolygon: function(key, latlngs, value){
	"use strict";
		var color = this.customPolyColors[key];
		var fillColor = null;
		if(color){
			fillColor = new Microsoft.Maps.Color(100, color[0], color[1], color[2]);
		}else{
			fillColor = new Microsoft.Maps.Color(100,100,0,100);
		}
		var options = { fillColor: fillColor,
						strokeColor: new Microsoft.Maps.Color(100,0,100,100),
						strokeThickness: 1 };
		var polygon = new Microsoft.Maps.Polygon(latlngs, options);
		polygon.amount = value;
		polygon.drillKey = key;

		var obj = this;
		Microsoft.Maps.Events.addHandler(polygon, 'mouseover', function(evt){ obj.infoBoxVisibility(evt, true); });
		Microsoft.Maps.Events.addHandler(polygon, 'mouseout', function(evt){ obj.infoBoxVisibility(evt, false); });

		var tt = key;
		var ttvalue = this.nameYToolTip[key];
		var value = this.nameY[key];

		if(ttvalue){
			tt += ": " + ttvalue;
		} else if(value){
			tt += ": " + value;
		}

		var pos = latlngs[0];
		var infoboxOffset = new Microsoft.Maps.Point(0, -50);
		var infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(pos.latitude, pos.longitude),
			{title: tt, description: '', polygon: polygon, offset: infoboxOffset} );
		this.map.entities.push(infobox);
		this.map.entities.push(polygon);
		this.polygons.push(polygon);
	},

	infoBoxVisibility: function(evt, visibility){
	"use strict";
		var polygon = evt.target;

		var infoBox = polygon.getInfobox();
		if(infoBox){
			if(visibility){
				var point = new Microsoft.Maps.Point(evt.getX(), evt.getY());
				var location = this.map.tryPixelToLocation(point);
				infoBox.setLocation(location);
			}
			infoBox.setOptions({visible: visibility});
		}
	},

	calculateGradient: function(n0, n1, n2, n3){
	"use strict";
		var g1 = Ext.create('Birst.dashboards.GradientRule');
		g1.minValue = n0;
		g1.maxValue = n1;
		//g1.minColor = 0x008000;
		g1.minColor = 0x00cc99;
		g1.maxColor = 0xffff00;

		var g2 = Ext.create('Birst.dashboards.GradientRule');
		g2.maxColor = 0xff7700;
		g2.maxValue = n2;

		var g3 = Ext.create('Birst.dashboards.GradientRule');
		g3.maxValue = n3;
		g3.maxColor = 0xff0000;

		this.gradientRuleList = Ext.create('Birst.dashboards.GradientRuleList', [g1, g2, g3]);
		/*
		if(this.gradientBar){
			this.gradientBar.borders = [0, n1, n2, n3];
			this.gradientBar.drawGradientRule(gradientRuleList);
		}*/
	},

	colorRangePolygons:function (values){
	"use strict";
		if(values.length > 0){
			var gradientControl = this.gradientRuleList.applyGradientToValueList(values);
			for(var i = 0; i < this.polygons.length; i++) {
				var p = this.polygons[i];
				if (!isNaN(p.amount)) {
					var colorRgb = gradientControl.rgbColorForValue(p.amount);
					p.setOptions({fillColor: new Microsoft.Maps.Color(100, colorRgb.r, colorRgb.g, colorRgb.b)});
					/*
					var style:MarkerStyle = createDefaultMarkerStyle();
					style.fillRGB = color;
					style.fillAlpha = 0.6;
					p.setStyle(style);
					*/
				}
			}
		}
	}
});

Ext.define('Birst.dashboards.GradientUtil', {
	statics: {
		linearScale: function(srcDict, dstDict, src, dstStepCount) {
			"use strict";
			var minSrc = srcDict['min'];
			var maxSrc = srcDict['max'];
			var minDst = dstDict['min'];
			var maxDst = dstDict['max'];

			var srcNorm = src;
			var dst;
			if (minSrc === maxSrc) {
				dst = minDst;
			} else {
				dst = minDst + (
				(srcNorm - minSrc) / (maxSrc - minSrc) * (maxDst - minDst));
			}
			var result = NaN;
			if (minDst < maxDst) {
				result = Math.min(Math.max(minDst, dst), maxDst);
			} else {
				result = Math.max(Math.min(minDst, dst), maxDst);
			}
			if (dstStepCount === 0) {
				return result;
			} else if (dstStepCount < 0) {
				//throw exception
			} else {
				var step = (maxDst - minDst) / (dstStepCount + 0.0);
				var stepNum = Math.round(Birst.dashboards.GradientUtil.linearScale(dstDict, {'min': 0, 'max': dstStepCount}, result));
				return minDst + stepNum * step;
			}
		},

		arrayMinMax: function(values){
			"use strict";
			if (values.length === 0) {
			return [];
			}
			var overallMinValue = values[0];
			var overallMaxValue = values[0];
			for(var i = 0; i < values.length; i++){
			var v = values[i];
			if (v < overallMinValue) {
				overallMinValue = v;
			}
			if (v > overallMaxValue) {
				overallMaxValue = v;
			}
			}

			return [overallMinValue, overallMaxValue];
		}
  }
});

Ext.define('Birst.dashboards.Range', {
	statics: {
		// Use this value if the start of a range is undefined.
		MIN_PERCENT: 0,
		// Use this value if the end of a range is undefined.
		MAX_PERCENT: 100
	},

	// Start of a range
	minValue: NaN,
	// End of a range
	maxValue: NaN,

	// Start of a color range
	minColor: NaN,

	// Start of a color range
	maxColor: NaN,

	// Access to min/maxPercent will be overridden in subclasses,
	// hence they are not public.
	_minPercent: NaN, /** Backing for minPercent. */
	_maxPercent: NaN, /** Backing for maxPercent. */

	constructor: function(divId, data){
		"use strict";
		this.minValue = NaN;
		this.maxValue = NaN;
		this.minColor = NaN;
		this.maxColor = NaN;
		this.minPercent = NaN;
		this.maxPercent = NaN;
	},

	setMinPercent: function(value) {
	"use strict";
		this._minPercent = value;
	},

   setMaxPercent: function(value) {
	"use strict";
		this._maxPercent = value;
	},

	getValueDict: function() {
		"use strict";
		return {
			'min': this.minValue,
			'max': this.maxValue
		};
	},

	getRgbColorDict: function() {
		"use strict";
		var result = {};

		result['r'] = {
			'min': this.rChannel(this.minColor),
			'max': this.rChannel(this.maxColor)
		};
		result['g'] = {
			'min': this.gChannel(this.minColor),
			'max': this.gChannel(this.maxColor)
		};
		result['b'] = {
			'min': this.bChannel(this.minColor),
			'max': this.bChannel(this.maxColor)
		};
		return result;
	},

	rChannel: function(value){
		"use strict";
		return value >> 16 & 0xFF;
	},

	gChannel: function(value){
		"use strict";
		return value >> 8 & 0xFF;
	},

	bChannel: function(value) {
		"use strict";
		return value >> 0 & 0xFF;
	}
});

Ext.define('Birst.dashboards.GradientRule', {
	extend: 'Birst.dashboards.Range',

	constructor: function(){
		this.callParent(arguments);
	},

	/**
	* Start of a percentage range. Values are forced to be greater than
	* MIN_PERCENT.
	*/
	setMinPercent: function(value) {
		"use strict";
		if (value < MIN_PERCENT) {
			value = MIN_PERCENT;
		}
		this._minPercent = value;
	},

	/**
	* End of a percentage range. Values are forced to be less than
	* MAX_PERCENT.
	*/
   setMaxPercent: function(value) {
		"use strict";
		if (value > MAX_PERCENT) {
			value = MAX_PERCENT;
		}
		this._maxPercent = value;
	},


	/**
	* Create a new instance of this class from an XML config
	*
	* @param dataValues An array of numbers
	*
	* @return A new GradientControl objects with this object's
	* gradient config applied to the specified values
	*/
	applyGradientToValueList: function(dataValues){
		"use strict";
		var arrayMinMax = Birst.dashboards.GradientUtil.arrayMinMax(dataValues);
		if (arrayMinMax.length === 0) {
		return null;
		}
		this.minValue = arrayMinMax[0];
		this.maxValue = arrayMinMax[1];

		// TODO: should these be called twice?

		// Try reading values from children
		this.fillValueColorFromChildren();
		// If still unfilled, try reading values from the siblings
		this.fillValueColorFromSiblings();
		// If still unfilled, try reading values from the parent
		this.fillValueColorFromParent(this);

		var gradients = this.createValueColorGradients(this);
		return  Ext.create('Birst.dashboards.GradientControl', gradients);
	},

	fillValueColorFromChildren: function(){
	},

	fillValueColorFromSiblings: function(){
	},

	fillValueColorFromParent: function(parent) {
	},


	// If min or max absolute values are not specified,
	// calculate them based on the percentage values
	// and on the min/max values taken from the parent.
	createValueColorGradients: function(parent){
		"use strict";
		var percentRangeDict = {
			'min': 0,
			'max': 100
		};
		var flatGradient = this.createFlatGradient();
		var dstDict = {
			'min': parent.minValue,
			'max': parent.maxValue
		};
		if (isNaN(flatGradient.minValue)) {
			flatGradient.minValue = Birst.dashboards.GradientUtil.linearScale(percentRangeDict, dstDict, minPercent, 0);
		}
		if (isNaN(flatGradient.maxValue)) {
			flatGradient.maxValue = Birst.dashboards.GradientUtil.linearScale(percentRangeDict, dstDict, maxPercent, 0);
		}

		return [flatGradient];
	},

	// Create a gradient with absolute values for use in a GradientControl.
   createFlatGradient: function(){
		"use strict";
		var result = Ext.create('Birst.dashboards.ValueColorGradient');
		result.minValue = this.minValue;
		result.maxValue = this.maxValue;
		result.minColor = this.minColor;
		result.maxColor = this.maxColor;
		return result;
	},

	// Duplicate the min values of another GradientRule object
	fillMin: function(other){
		"use strict";
		if (isNaN(this.minValue)) {
			this.minValue = other.minValue;
		}
		if (isNaN(this.minColor)) {
			this.minColor = other.minColor;
		}
		// Do not transfer percentage between levels
	},

	// Duplicate the max values of another GradientRule object
	fillMax: function(other){
		"use strict";
		if (isNaN(this.maxValue)) {
			this.maxValue = other.maxValue;
		}
		if (isNaN(this.maxColor)) {
			this.maxColor = other.maxColor;
		}
		// Do not transfer percentage between levels
	},

	// Duplicate the max values of another GradientRule object
	// as the min values for this object
   fillFromPrev: function(prev, fillPercent){
		"use strict";
		if (isNaN(this.minPercent) && fillPercent) {
		//minValue won't be overriden by this
			this.minPercent = prev.maxPercent;
		}
		if (isNaN(this.minValue)) {
			this.minValue = prev.maxValue;
		}
		if (isNaN(this.minColor)) {
			this.minColor = prev.maxColor;
		}
	},

	// Duplicate the min values of another GradientRule object
	// as the max values for this object
	fillFromNext: function(next, fillPercent){
		if (isNaN(this.maxPercent) && fillPercent) {
			//maxValue won't be overriden by this
			this.maxPercent = next.minPercent;
		}
		if (isNaN(this.maxValue)) {
			this.maxValue = next.minValue;
		}

		if (isNaN(this.maxColor)) {
			this.maxColor = next.minColor;
		}
	}
});

Ext.define('Birst.dashboards.GradientRuleList', {
	extend: 'Birst.dashboards.GradientRule',
	ruleList: null,
	constructor: function(ruleList){
		this.callParent(arguments);
		this.ruleList = ruleList;
	},

	fillValueColorFromChildren: function(){
	"use strict";
		// Depth first.
		for(var i = 0; i < this.ruleList.length; i++){
			var g = this.ruleList[i];
			g.fillValueColorFromChildren();
		}

		if (this.ruleList.length > 0) {
			this.fillMin(this.ruleList[0]);
			this.fillMax(this.ruleList[this.ruleList.length-1]);
		}
	},

	// Read value and color borders from the parent
	fillValueColorFromParent: function(parent){
		this.callParent(parent);
		if (this.ruleList.length === 0) {
			return;
		}
		// Breadth first
		this.ruleList[0].fillMin(this);
		this.ruleList[this.ruleList.length-1].fillMax(this);

		for(var i = 0; i < this.ruleList.length; i++){
			var g = this.ruleList[i];
			g.fillValueColorFromParent(this);
		}
	},

	// Read value and color borders from the neighboring gradients.
	// Use 0% and 100% for the start and end of the ranges if not specified.
	fillValueColorFromSiblings: function() {
		"use strict";
		var fantomFirstEntry = Ext.create('Birst.dashboards.GradientRule');
		fantomFirstEntry.maxPercent = Birst.dashboards.Range.MIN_PERCENT;

		var fantomLastEntry = Ext.create('Birst.dashboards.GradientRule');
		fantomLastEntry.minPercent = Birst.dashboards.Range.MAX_PERCENT;

		var paddedGradientList = [fantomFirstEntry].concat(this.ruleList).concat([fantomLastEntry]);

		for (var i = 1; i< paddedGradientList.length - 1; i++) {
			var g = paddedGradientList[i];

			var prev  = paddedGradientList[i-1];
			var next = paddedGradientList[i+1];

			g.fillFromPrev(prev, true);
			g.fillFromNext(next, true);

			g.fillValueColorFromSiblings();
		}
	},

	createValueColorGradients: function(parent) {
		var oneGradientList = this.callParent(arguments);
		var flatGradient = oneGradientList[0];
		var flatGradientList = [];
		for(var i = 0; i < this.ruleList.length; i++){
			var g = this.ruleList[i];
			var childFlatGradients = g.createValueColorGradients(flatGradient);
			flatGradientList = flatGradientList.concat(childFlatGradients);
		}
		return flatGradientList;
	}
});

Ext.define('Birst.dashboards.ValueColorGradient', {
	extend: 'Birst.dashboards.Range',

	constructor: function() {
		this.callParent(arguments);
	},

	/**
	* Apply the scaling calculations to translate the specfied number
	* into a color within this object's range.
	*/
   colorForValue: function(someValue){
		"use strict";
		var valueDict = this.getValueDict();
		var rgbColorDict = this.getRgbColorDict();
		var r = Birst.dashboards.GradientUtil.linearScale( valueDict, rgbColorDict['r'], someValue, 0);
		var g = Birst.dashboards.GradientUtil.linearScale(valueDict, rgbColorDict['g'], someValue, 0);
		var b = Birst.dashboards.GradientUtil.linearScale(valueDict, rgbColorDict['b'], someValue, 0);
		return this.rgbToInt(r, g, b);
	},

	rgbColorForValue: function(someValue){
	"use strict";
		var valueDict = this.getValueDict();
		var rgbColorDict = this.getRgbColorDict();
		var r = Birst.dashboards.GradientUtil.linearScale( valueDict, rgbColorDict['r'], someValue, 0);
		var g = Birst.dashboards.GradientUtil.linearScale(valueDict, rgbColorDict['g'], someValue, 0);
		var b = Birst.dashboards.GradientUtil.linearScale(valueDict, rgbColorDict['b'], someValue, 0);
		r = Math.floor(r);
		g = Math.floor(g);
		b = Math.floor(b);
		return { r: r, g: g, b: b};
	},

	containsValue: function(someValue){
		"use strict";
		return someValue < this.maxValue;
	},

	numFlatGradients: function() {
		"use strict";
		return 1;
	},

	flatGradientAt: function(i){
		"use strict";
		return this;
	},

	rgbToInt: function(r, g, b) {
		"use strict";
		return r << 16 | g << 8 | b << 0;
	}
});

Ext.define('Birst.dashboards.GradientControl', {
	extend:  'Birst.dashboards.ValueColorGradient',

	// A list of ValueColorGradients storing specific
	// gradient rules.
	flatGradientList: null,

	constructor: function(flatGradientList){
		"use strict";
		this.flatGradientList = flatGradientList;
	},

	numFlatGradients: function(){
		"use strict";
		return this.flatGradientList.length;
	},

	// Return the gradient at the specified index
	flatGradientAt: function(i){
		"use strict";
		return this.flatGradientList[i];
	},

	/**
	* Find the gradient into whose range the specified value falls
	* and use it to calculate the color.
	*
	* @param value A number to translate into a color.
	*/
	colorForValue: function(value){
		"use strict";
		for (var i = 0; i< this.numFlatGradients(); i++) {
			var gradient = this.flatGradientAt(i);
			if (gradient.containsValue(value)) {
				return gradient.colorForValue(value);
			}
		}
		// The value is greater than last gradient's max
		// - delegate to last gradient.
		var lastGradient = this.flatGradientAt(this.numFlatGradients()-1);
		return lastGradient.colorForValue(value);
	},

	rgbColorForValue: function(value){
		"use strict";
		for (var i = 0; i< this.numFlatGradients(); i++) {
			var gradient = this.flatGradientAt(i);
			if (gradient.containsValue(value)) {
				return gradient.rgbColorForValue(value);
			}
		}
		// The value is greater than last gradient's max
		// - delegate to last gradient.
		var lastGradient = this.flatGradientAt(this.numFlatGradients()-1);
		return lastGradient.rgbColorForValue(value);
	}

});

function debug(log_txt) {
	if (typeof window.console !== 'undefined') {
		console.log(log_txt);
	}
}

