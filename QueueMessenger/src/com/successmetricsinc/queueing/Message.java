package com.successmetricsinc.queueing;


public class Message {
	public static final String MESSAGE_DATA = "MessageData";
	public static final String ASYNC_REPORT_GENERATION_DATA = "AsynchReportGenerationData";
		
	private String spaceId;
	private String userName;
	private String inputData;
	private String resultData;
	
	public String getResultData() {
		return resultData;
	}

	public void setResultData(String resultData) {
		this.resultData = resultData;
	}

	private String id;
	private String releaseNumber;
	
	public Message(){		
	}
	
	private Message(String userName, String spaceID)
	{			
		this.userName = userName;
		this.spaceId = spaceID;		
	}	
	
	public static Message createMessage(String userName, String spaceID)
	{
		return new Message(userName, spaceID);
	}	
	
	public String getUserName() {
		return userName;
	}
	
	public String getSpaceId() {
		return spaceId;
	}
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public void setSpaceId(String spaceId){
		this.spaceId = spaceId;
	}

	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public String getReleaseNumber() {
		return releaseNumber;
	}

	public void setReleaseNumber(String releaseNumber) {
		this.releaseNumber = releaseNumber;
	}
}
