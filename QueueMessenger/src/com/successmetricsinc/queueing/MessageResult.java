package com.successmetricsinc.queueing;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;


public class MessageResult {

	private boolean success;
	public int ErrorCode = -1;
	private String ErrorMessage;
	public static final String MESSAGE_RESULT = "MessageResult";
	public static final String SUCCESS = "Success";
	public static final String ERROR_CODE = "ErrorCode";
	public static final String ERROR_MESSAGE = "ErrorMessage";
	private static final Logger logger = Logger.getLogger(MessageResult.class);
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public int getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(int errorCode) {
		ErrorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return ErrorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}
	
	public OMElement getMessageResultElement()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement messageResult = fac.createOMElement(MESSAGE_RESULT, ns);
		XmlUtils.addContent(fac, messageResult, SUCCESS, success, ns);
		XmlUtils.addContent(fac, messageResult, ERROR_CODE, ErrorCode, ns);
		XmlUtils.addContent(fac, messageResult, ERROR_MESSAGE, ErrorMessage, ns);
		return messageResult;
	}
	
	public static MessageResult parseAsyncResult(String resultFile) throws Exception
	{
		MessageResult result = new MessageResult();
		FileInputStream fis = null;
		XMLStreamReader parser;
		try {
			fis = new FileInputStream(new File(resultFile));
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(fis, "UTF-8");
			
			for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
				String name = parser.getLocalName();
				if (name.equals(MESSAGE_RESULT)) {
					for (XMLStreamIterator msgChildIter = new XMLStreamIterator(parser); msgChildIter.hasNext(); ) {
						String elName = parser.getLocalName();
						if (SUCCESS.equals(elName)) {
							result.success = XmlUtils.getBooleanContent(parser);
						} else if (ERROR_CODE.equals(elName)) {
							result.ErrorCode = XmlUtils.getIntContent(parser);
						} else if (ERROR_MESSAGE.equals(elName)) {
							result.ErrorMessage = XmlUtils.getStringContent(parser);
						}
					}
				}
			}
		}
		catch (Exception e) {
			logger.error(e, e);
		}
		finally {
			if (fis != null)
			{
				try
				{
					fis.close();
				}
				catch(Exception e)
				{
					logger.error(e, e);
				}
			}
				
		}
		return result;
	}
}
