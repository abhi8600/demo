package com.successmetricsinc.queueing.consumer;

import org.apache.axiom.om.OMElement;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.MessageResult;

public abstract class QueueConsumer {
	
	protected Message message;
	protected MessageResult result;
	
	public QueueConsumer(Message message, int consumerIndex)
	{
		this.message = message;
	}
	
	abstract public MessageResult consume() throws Exception;
	
	abstract public OMElement getMessageResultElement() throws Exception;
	
	abstract public void postConsumption() throws Exception;
	
}
