package com.successmetricsinc.queueing.consumer;

import java.lang.reflect.Constructor;
import org.apache.log4j.Logger;

import com.successmetricsinc.queueing.Message;

public class QueueConsumerFactory {
	private static final Logger logger = Logger.getLogger(QueueConsumerFactory.class);
	
	public static QueueConsumer createQueueConsumer(Class<? extends QueueConsumer> clzz, Message msg, int consumerIndex) {
		try {		
			if (clzz != null) {
				Class[] args = new Class[] { Message.class, int.class };
				Constructor con = clzz.getDeclaredConstructor(args);
				return (QueueConsumer)con.newInstance(msg, consumerIndex);
			}
		} catch (Exception nsme) {
			logger.error(nsme, nsme);
		}

		return null;
	}
}
