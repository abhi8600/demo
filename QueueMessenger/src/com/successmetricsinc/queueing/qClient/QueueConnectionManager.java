package com.successmetricsinc.queueing.qClient;

import java.util.HashMap;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;

import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.utils.QClientUtil;

public abstract class QueueConnectionManager {
	private static Map<Queue, QueueConnection> queueToConnectionMap = new HashMap<>();
	private static Map<Topic, TopicConnection> topicToConnectionMap = new HashMap<>();
	
	private static QueueConnection getConnectionForQueue(Queue queue, boolean forceNewConnection) throws MessengerException
	{
		if (!forceNewConnection)
		{
			if (queueToConnectionMap.containsKey(queue))
				return (queueToConnectionMap.get(queue));
		}
		ConnectionFactory factory = QClientConnectionFactory.getConnectionFactory();
		try
		{
			QueueConnection conn = ((QueueConnectionFactory)factory).createQueueConnection();
			String clientId = QClientUtil.getClientId();
			conn.setClientID(clientId);
			conn.start();
			queueToConnectionMap.put(queue, conn);
			return conn;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}		
	}
	
	public static QueueSession getSessionForQueue(Queue queue) throws MessengerException
	{
		synchronized (queueToConnectionMap) {
			QueueConnection conn = getConnectionForQueue(queue, false);
			try
			{
				QueueSession session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
				return session;
			}
			catch(JMSException ex){
				conn = getConnectionForQueue(queue, true);
				try
				{
					QueueSession session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
					return session;
				}
				catch (JMSException jmse)
				{
					throw new MessengerException(jmse);
				}
			}
		}
	}
	
	private static TopicConnection getConnectionForTopic(Topic topic, boolean forceNewConnection) throws MessengerException
	{
		if (!forceNewConnection)
		{
			if (topicToConnectionMap.containsKey(topic))
				return (topicToConnectionMap.get(topic));
		}
		ConnectionFactory factory = QClientConnectionFactory.getConnectionFactory();
		try
		{
			TopicConnection conn = ((TopicConnectionFactory)factory).createTopicConnection();
			String clientId = QClientUtil.getClientId();
			conn.setClientID(clientId);
			conn.start();
			topicToConnectionMap.put(topic, conn);
			return conn;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}		
	}
	
	public static TopicSession getSessionForTopic(Topic topic) throws MessengerException
	{
		synchronized (topicToConnectionMap) {
			TopicConnection conn = getConnectionForTopic(topic, false);
			try
			{
				TopicSession session = conn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
				return session;
			}
			catch(JMSException ex){
				conn = getConnectionForTopic(topic, true);
				try
				{
					TopicSession session = conn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
					return session;
				}
				catch (JMSException jmse)
				{
					throw new MessengerException(jmse);
				}
			}
		}
	}
	
	public static void closeConnectionForQueue(Queue queue)
	{
		if (queueToConnectionMap.containsKey(queue))
		{
			try
			{
				queueToConnectionMap.get(queue).close();
			}
			catch (JMSException jmse)
			{
			}
		}
	}
	
	public static void closeConnectionForTopic(Topic topic)
	{
		if (topicToConnectionMap.containsKey(topic))
		{
			try
			{
				topicToConnectionMap.get(topic).close();
			}
			catch (JMSException jmse)
			{
			}
		}
	}
	
	public static void closeAllConnections()
	{
		if (queueToConnectionMap != null)
		{
			for (Queue queue : queueToConnectionMap.keySet())
			{
				closeConnectionForQueue(queue);
			}
		}
		if (topicToConnectionMap != null)
		{
			for (Topic topic : topicToConnectionMap.keySet())
			{
				closeConnectionForTopic(topic);
			}
		}
	}
}
