package com.successmetricsinc.queueing.qClient;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.QueueSession;
import javax.jms.TopicSession;

import org.apache.qpid.amqp_1_0.jms.impl.QueueImpl;
import org.apache.qpid.amqp_1_0.jms.impl.TopicImpl;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.utils.MessageUtil;
import com.successmetricsinc.queueing.qClient.utils.QClientUtil;

public class ConsumerImpl implements Consumer {

	private MessageConsumer consumer;
	private QueueSession queueSession;
	private TopicSession topicSession;
	private TopicMessageListener topicMessageListener;
	
	public static Consumer create(Queue queue) throws MessengerException{
		try{
			QueueSession session = QueueConnectionManager.getSessionForQueue(queue);
			MessageConsumer consumer = session.createConsumer(new QueueImpl(queue.name()));
			ConsumerImpl consumerImpl = new ConsumerImpl();
			consumerImpl.consumer = consumer;
			consumerImpl.queueSession = session;
			return consumerImpl;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}
	}
	
	public static Consumer create(Topic topic) throws MessengerException{
		try
		{
			TopicSession session = QueueConnectionManager.getSessionForTopic(topic);
			MessageConsumer consumer = session.createSubscriber(new TopicImpl(QClientUtil.getTopicName(topic)));
			ConsumerImpl consumerImpl = new ConsumerImpl();
			consumerImpl.consumer = consumer;
			consumerImpl.topicSession = session;			
			return consumerImpl;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}
	}
	
	
	@Override
	public Message retreiveNextMessage(long waitTimeInMilliseconds) throws MessengerException {
		try {
			javax.jms.Message msg = consumer.receive(waitTimeInMilliseconds);			
			if(msg instanceof MapMessage){
				return MessageUtil.formatMapMessage((MapMessage)msg);
			}			
			return null; 
		} catch (JMSException ex) {
			throw new MessengerException(ex);
		}
	}
		

	@Override
	public void subscribeToTopic(Subscriber subscriber) throws MessengerException {
		if(this.topicMessageListener == null){
			topicMessageListener = new TopicMessageListener();
			try {
				consumer.setMessageListener(topicMessageListener);
			} catch (JMSException e) {
				throw new MessengerException(e);
			}
		}
		topicMessageListener.addSubscriber(subscriber);
	}

	@Override
	public void close() throws MessengerException {		
		try{
			if(queueSession != null){
				queueSession.close();
			}
			
			if(topicSession != null){
				topicSession.close();
			}
		}
		catch (JMSException e) {
			throw new MessengerException(e);
		}
	}
}
