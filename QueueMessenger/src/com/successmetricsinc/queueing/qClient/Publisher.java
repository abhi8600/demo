package com.successmetricsinc.queueing.qClient;


import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public interface Publisher {

	public String publish(Message message) throws MessengerException;
	public void close() throws MessengerException;
}
