package com.successmetricsinc.queueing.qClient.Exceptions;

public class MessengerException extends Exception {
	
	
	/**
	 * @author gsingh
	 */
	private static final long serialVersionUID = 1L;
	
	public MessengerException(){
		super();
	}
	
	public MessengerException(String message){
		super(message);
	}
	
	public MessengerException(Throwable cause){
		super(cause);
	}

	public MessengerException(String message, Throwable cause){
		super(message, cause);
	}
	
}
