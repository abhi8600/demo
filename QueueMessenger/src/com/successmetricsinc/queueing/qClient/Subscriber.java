package com.successmetricsinc.queueing.qClient;

import com.successmetricsinc.queueing.Message;

public interface Subscriber {
	
	public void onRecieve(Message message);
}
