package com.successmetricsinc.queueing.qClient;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public interface Consumer {
	
	public Message retreiveNextMessage(long waitTimeInMilliseconds) throws MessengerException;
	public void subscribeToTopic(Subscriber subscriber) throws MessengerException;
	public void close() throws MessengerException;

}
