package com.successmetricsinc.queueing.qClient;

import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.QueueSession;
import javax.jms.TopicSession;

import org.apache.qpid.amqp_1_0.jms.impl.QueueImpl;
import org.apache.qpid.amqp_1_0.jms.impl.TopicImpl;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.utils.QClientUtil;

public class PublisherImpl implements Publisher {

	private MessageProducer producer;
	private QueueSession queueSession;
	private TopicSession topicSession;	
	
	public PublisherImpl(){		
	}

	public static Publisher create(Queue queue) throws MessengerException{
		try
		{
			QueueSession session = QueueConnectionManager.getSessionForQueue(queue);
			MessageProducer producer = session.createProducer(new QueueImpl(queue.name()));
			PublisherImpl publisherImpl = new PublisherImpl();
			publisherImpl.producer = producer;
			publisherImpl.queueSession = session;			
			return publisherImpl;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}
	}
	
	public static Publisher create(Topic topic) throws MessengerException{
		try
		{
			TopicSession session = QueueConnectionManager.getSessionForTopic(topic);	
			MessageProducer producer = session.createProducer(new TopicImpl(QClientUtil.getTopicName(topic)));
			//producer.setTimeToLive(QClientUtil.getMessageExpiration()); // Needs to find another way to expire. This does not work
			PublisherImpl publisherImpl = new PublisherImpl();
			publisherImpl.producer = producer;
			publisherImpl.topicSession = session;
			return publisherImpl;
		}
		catch(JMSException ex){
			throw new MessengerException(ex);
		}		
	}
	
	@Override
	public String publish(Message message) throws MessengerException {	
		synchronized(this){
			try{
				String id = message.getId();
				if(id == null || id.trim().length() == 0){
					id = UUID.randomUUID().toString();
				}
				
				MapMessage mapMessage = null;
				if(queueSession != null){
					mapMessage = queueSession.createMapMessage();
				}else{
					mapMessage = topicSession.createMapMessage();	
				}
				
				mapMessage.setString("spaceId", message.getSpaceId());
				mapMessage.setString("inputData", message.getInputData());
				mapMessage.setString("resultData", message.getResultData());
				mapMessage.setString("username", message.getUserName());								
				mapMessage.setString("id", id);				
				mapMessage.setString("releaseNumber", message.getReleaseNumber());
				producer.send(mapMessage);
				return id;
			}catch(JMSException ex){
				throw new MessengerException(ex);
			}
		}
	}

	@Override
	public void close() throws MessengerException {		
		try{
			if(queueSession != null){
				queueSession.close();
			}
			
			if(topicSession != null){
				topicSession.close();
			}
		}
		catch (JMSException e) {
			throw new MessengerException(e);
		}
	}
	
}
