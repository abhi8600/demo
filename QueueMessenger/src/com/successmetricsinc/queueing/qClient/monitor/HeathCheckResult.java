package com.successmetricsinc.queueing.qClient.monitor;


public class HeathCheckResult {

	public enum HealthStatus{		
		WORKING,
		DOWN,
		STOPPED
	}

	private HealthStatus status;
	
	public HeathCheckResult(HealthStatus status){
		this.status = status;
	}
	
	public boolean isHealthy(){
		return status == HealthStatus.WORKING;
	}
	
	public HealthStatus getState(){
		return status;
	}
}
