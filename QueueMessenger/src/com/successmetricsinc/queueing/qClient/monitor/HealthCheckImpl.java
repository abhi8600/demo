package com.successmetricsinc.queueing.qClient.monitor;

import java.util.UUID;

import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.successmetricsinc.queueing.qClient.QClientConnectionFactory;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class HealthCheckImpl implements HealthCheck, ExceptionListener {

	private static final Logger logger = Logger.getLogger(HealthCheckImpl.class);
	private static final long WAIT_ON_CONSUMER = 20*60*1000;	
	private QueueConnection tempConn;
	private MessageProducer tempProducer;
	private MessageConsumer tempConsumer;
	private QueueSession queueSession;
	
	@Override
	public HeathCheckResult check(long waitTimeOnMessageInMilliseconds) throws MessengerException {
		try{

			String id = UUID.randomUUID().toString();
			TextMessage message = queueSession.createTextMessage(id);
			tempProducer.send(message);
			Message receivedMessage = tempConsumer.receive(waitTimeOnMessageInMilliseconds > 0 ? waitTimeOnMessageInMilliseconds : WAIT_ON_CONSUMER);
			if(receivedMessage instanceof TextMessage){
				TextMessage received = (TextMessage)receivedMessage;
				if(id.equals(received.getText())){
					return new HeathCheckResult(HeathCheckResult.HealthStatus.WORKING);
				}
			}
			return new HeathCheckResult(HeathCheckResult.HealthStatus.DOWN);
		}catch(JMSException ex){
			throw new MessengerException(ex);
		}

	}

	public static HealthCheck create() throws MessengerException{
		ConnectionFactory factory = QClientConnectionFactory.getConnectionFactory();
		return create(factory);
	}
	
	public static HealthCheck create(ConnectionFactory factory)
			throws MessengerException {

		try{
			QueueConnection conn = ((QueueConnectionFactory)factory).createQueueConnection();
			conn.start();			
			QueueSession session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			TemporaryQueue tQueue = session.createTemporaryQueue();
			MessageProducer producer = session.createProducer(tQueue);
			MessageConsumer consumer = session.createConsumer(tQueue);
			HealthCheckImpl hCheck = new HealthCheckImpl();
			hCheck.tempConn = conn;
			hCheck.tempProducer = producer;
			hCheck.tempConsumer = consumer;
			hCheck.queueSession = session;
			conn.setExceptionListener((ExceptionListener) hCheck);
			return hCheck;
		}catch(JMSException ex){
			throw new MessengerException(ex);
		}
	}

	@Override
	public void onException(JMSException arg0) {
		logger.error("Exception in health check connection " + arg0.getMessage());
	}

	@Override
	public void close(){
		if(tempConn != null){
			try {
				tempConn.close();
			} catch (JMSException e) {}
		}		
	}

}
