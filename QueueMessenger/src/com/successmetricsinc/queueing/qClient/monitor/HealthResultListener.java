package com.successmetricsinc.queueing.qClient.monitor;

import com.successmetricsinc.queueing.qClient.monitor.HeathCheckResult.HealthStatus;

public interface HealthResultListener {
	
	void onStateChange(HealthStatus newHealthStatus, HealthStatus oldHealthStatus);
}
