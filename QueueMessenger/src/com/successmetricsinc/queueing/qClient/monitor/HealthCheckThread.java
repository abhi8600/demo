package com.successmetricsinc.queueing.qClient.monitor;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.monitor.HeathCheckResult.HealthStatus;

public class HealthCheckThread implements Runnable {

	private long pingWaitTime;
	private HealthCheck healthCheck;
	private boolean pause = false;
	private HeathCheckResult healthCheckResult = null;
	private List<HealthResultListener> listeners = new ArrayList<HealthResultListener>();
	private static final long DEFALT_PING_WAIT = 5*1000; // 5 seconds
	
	public HealthCheckThread(long pingTimeInMilliseconds){
		setPingTime(pingTimeInMilliseconds);		
	}
	
	private void setPingTime(long pingTimeInMilliseconds){
		pingWaitTime = pingTimeInMilliseconds < 0 ? DEFALT_PING_WAIT : pingTimeInMilliseconds;
	}
	
	@Override
	public void run() {		
		while(!pause){
			try
			{
				Thread.sleep(pingWaitTime);
				if(healthCheck == null || healthCheckResult.getState() == HeathCheckResult.HealthStatus.DOWN){
					healthCheck = HealthCheckImpl.create();
				}
				setHealthCheckResult(healthCheck.check(-1));				
			}catch (Exception ex){
				setHealthCheckResult(new HeathCheckResult(HeathCheckResult.HealthStatus.DOWN));
				try {
					if(healthCheck != null){
						healthCheck.close();
					}
				} catch (MessengerException e) {
				}				
			}
		}
	}
	
	public void addHealthResultListener(HealthResultListener listener){
		listeners.add(listener);
	}
	
	private void setHealthCheckResult(HeathCheckResult newState){
		HeathCheckResult oldState = healthCheckResult;
		healthCheckResult = newState;
		if(healthCheckResult != null && oldState != null &&
				(healthCheckResult.getState() != oldState.getState())){
			dispatchOnStateChange(newState.getState(), oldState.getState());
		}
	}
	
	public void dispatchOnStateChange(HealthStatus newHealthStatus, HealthStatus oldHealthStatus){
		if(listeners != null && listeners.size() > 0){
			for(HealthResultListener listener : listeners){
				listener.onStateChange(newHealthStatus, oldHealthStatus);
			}
		}
	}
	
	public HeathCheckResult getHealthState(){
		return healthCheckResult;
	}
	
	public void changeRunState(boolean run){
		this.pause = !run;
	}
}
