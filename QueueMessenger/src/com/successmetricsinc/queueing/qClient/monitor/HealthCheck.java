package com.successmetricsinc.queueing.qClient.monitor;


import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public interface HealthCheck {
	
	HeathCheckResult check(long waitTimeOnMessageInMilliseconds) throws MessengerException;
	void close() throws MessengerException;
}
