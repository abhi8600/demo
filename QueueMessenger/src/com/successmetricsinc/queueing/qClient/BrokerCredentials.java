package com.successmetricsinc.queueing.qClient;

public class BrokerCredentials {	
	private String host;
	private int port;
	
	public BrokerCredentials(String host, int port){
		this.host = host;
		this.port = port;
	}
	
	public String getHost(){
		return this.host;
	}
	
	public int getPort(){
		return this.port;
	}
}
