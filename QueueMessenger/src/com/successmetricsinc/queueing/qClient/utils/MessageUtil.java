package com.successmetricsinc.queueing.qClient.utils;

import javax.jms.JMSException;
import javax.jms.MapMessage;

import com.successmetricsinc.queueing.Message;

public class MessageUtil {

	public static Message formatMapMessage(MapMessage mapMessage) throws JMSException{
		Message message = new Message();
		message.setSpaceId(mapMessage.getString("spaceId"));
		message.setInputData(mapMessage.getString("inputData"));
		message.setResultData(mapMessage.getString("resultData"));
		message.setUserName(mapMessage.getString("username"));				
		message.setId(mapMessage.getString("id"));
		message.setReleaseNumber(mapMessage.getString("releaseNumber"));
		return message;
	}

}
