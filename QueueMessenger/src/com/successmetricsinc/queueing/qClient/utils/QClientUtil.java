package com.successmetricsinc.queueing.qClient.utils;

import java.util.UUID;

import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;

public class QClientUtil {

	private static final long DEFAULT_DURATION_MESSAGE_EXPIRATION = 3*24*60*60*1000; // 3 days
	private static long messageExpireDuration=-1;
	private static final String topicPrefix = "topic://";
	
	
	public static String getClientId(){
		return UUID.randomUUID().toString();
	}
	
	/***
	 * Return message expiration duration in milliseconds
	 * @return
	 */
	public static long getMessageExpiration(){
		if(messageExpireDuration > 0){
			return messageExpireDuration;
		}
		return DEFAULT_DURATION_MESSAGE_EXPIRATION;
	}
	
	public static void setMessageExpiration(long timeInMilliseconds){
		messageExpireDuration = timeInMilliseconds;
	}
	
	public static String getTopicName(Topic topic){
		String topicName = topic.name();
		topicName = topicName.startsWith(topicPrefix) ? topicName : (topicPrefix + topicName);
		return topicName;
	}
}
