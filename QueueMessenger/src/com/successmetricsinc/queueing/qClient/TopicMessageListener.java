package com.successmetricsinc.queueing.qClient;

import java.util.HashSet;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.successmetricsinc.queueing.qClient.utils.MessageUtil;

public class TopicMessageListener implements MessageListener {

	private Set<Subscriber> topicSubscribers;
	@Override
	public void onMessage(Message msg) {
		if(topicSubscribers != null){			
			com.successmetricsinc.queueing.Message formattedMessage;
			try {
				formattedMessage = MessageUtil.formatMapMessage((MapMessage)msg);
				for(Subscriber subscriber : topicSubscribers){
					subscriber.onRecieve(formattedMessage);
				}
			} catch (JMSException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public void addSubscriber(Subscriber subscriber) {
		if(topicSubscribers == null){
			topicSubscribers = new HashSet<Subscriber>();
		}		
		topicSubscribers.add(subscriber);
	}
	
}
