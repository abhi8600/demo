package com.successmetricsinc.queueing.qClient;

import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class MessengerFactory {

	public enum Queue {
		NOW_REPORT
	}

	public enum Topic {
		NOW_REPORTGEN_RESULT
	}

	public static Publisher getPublisher(Queue queue) throws MessengerException{

		if(queue == Queue.NOW_REPORT){
			return PublisherImpl.create(queue);
		}

		throw new MessengerException("Invalid queue");
	}

	public static Consumer getConsumer(Queue queue) throws MessengerException{
		if(queue == Queue.NOW_REPORT){
			return ConsumerImpl.create(queue);
		}

		throw new MessengerException("Invalid queue");
	}

	public static Publisher getPublisher(Topic topic) throws MessengerException{

		if(topic == Topic.NOW_REPORTGEN_RESULT){
			return PublisherImpl.create(topic);
		}

		throw new MessengerException("Invalid topic");
	}

	public static Consumer getConsumer(Topic topic) throws MessengerException{
		if(topic == Topic.NOW_REPORTGEN_RESULT){
			return ConsumerImpl.create(topic);
		}

		throw new MessengerException("Invalid topic");
	}

}
