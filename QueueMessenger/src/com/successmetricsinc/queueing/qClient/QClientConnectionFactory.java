package com.successmetricsinc.queueing.qClient;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.qpid.amqp_1_0.jms.impl.ConnectionFactoryImpl;

import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class QClientConnectionFactory {

	private static ConnectionFactory _instance;
	private static BrokerCredentials brokerCredentials;
	
	public static ConnectionFactory getConnectionFactory() throws MessengerException{
		if(_instance == null){
			if(brokerCredentials != null){
				_instance = getConnectionFactory(brokerCredentials.getHost(), brokerCredentials.getPort());
			}
			else{
				throw new MessengerException("Unable to instantiate connection factory");
			}
		}
		return _instance; 
	}
	
	
	public static ConnectionFactory getConnectionFactory(Properties properties) throws MessengerException{
		if(_instance == null){
			try{
				Context context = new InitialContext(properties);
				return (ConnectionFactory) context.lookup("qpidConnectionFactory");
			}catch(NamingException ex){
				throw new MessengerException(ex);
			}
		}
		return _instance;
	}
	
	public static ConnectionFactory getConnectionFactory(String host, int port){
		if(_instance == null){
			return new ConnectionFactoryImpl(host, port,null,null);			
		}
		return _instance;
	}
	
	public static void setUpDefaultCredentials(BrokerCredentials credentials){
		brokerCredentials = credentials;
	}
}
