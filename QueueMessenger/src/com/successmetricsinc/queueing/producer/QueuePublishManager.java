package com.successmetricsinc.queueing.producer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.MessengerFactory;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.Publisher;
import com.successmetricsinc.queueing.qClient.QueueConnectionManager;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class QueuePublishManager {
	private Map<Queue, Integer> queueToNoOfPublishersMap = new HashMap<>();
	private Map<Queue, Publisher[]> queueToPublishersMap = new HashMap<>();
	private Map<Queue, Integer> queueToNextPublisherIndexMap = new HashMap<>();
	
	private Map<Topic, Integer> topicToNoOfPublishersMap = new HashMap<>();
	private Map<Topic, Publisher[]> topicToPublishersMap = new HashMap<>();
	private Map<Topic, Integer> topicToNextPublisherIndexMap = new HashMap<>();
	
	
	private static final Logger logger = Logger.getLogger(QueuePublishManager.class);
	private ConcurrentHashMap<String, Object> publisherQueueLocks = new ConcurrentHashMap<String, Object>();
	private ConcurrentHashMap<String, Object> publisherTopicLocks = new ConcurrentHashMap<String, Object>();
	
	public boolean configurePublishersForQueue(Queue queue, int noPublishers)
	{
		queueToNoOfPublishersMap.put(queue, noPublishers);
		queueToPublishersMap.put(queue, null);
		queueToNextPublisherIndexMap.put(queue, 0);
		return true;
	}
	
	public boolean configurePublishersForTopic(Topic topic, int noPublishers)
	{
		topicToNoOfPublishersMap.put(topic, noPublishers);
		topicToPublishersMap.put(topic, null);
		topicToNextPublisherIndexMap.put(topic, 0);
		return true;
	}
	
	
	public boolean initializePublishers(Queue queue) throws MessengerException
	{
		if (queueToNoOfPublishersMap.containsKey(queue))
		{
			int noPublishers = queueToNoOfPublishersMap.get(queue);
			Publisher[] publishers = new Publisher[noPublishers];
			for (int i=0; i<noPublishers; i++)
			{
				publishers[i] = MessengerFactory.getPublisher(queue);
				publisherQueueLocks.putIfAbsent(getQueueLockKey(queue,i), new Object());
			}
			queueToPublishersMap.put(queue, publishers);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean initializePublishers(Topic topic) throws MessengerException
	{
		if (topicToNoOfPublishersMap.containsKey(topic))
		{
			int noPublishers = topicToNoOfPublishersMap.get(topic);
			Publisher[] publishers = new Publisher[noPublishers];
			for (int i=0; i<noPublishers; i++)
			{
				publishers[i] = MessengerFactory.getPublisher(topic);
				publisherTopicLocks.putIfAbsent(getTopicLockKey(topic, i), new Object());
			}
			topicToPublishersMap.put(topic, publishers);
			return true;
		}
		else
		{
			return false;
		}
	}

	
	public String publishMessage(Queue queue, Message message) throws MessengerException
	{
		return publishMessage(queue, message, 0);
	}
	
	public String publishMessage(Queue queue, Message message, int retryCount) throws MessengerException
	{
		int currPubIndex = -1;
		synchronized(queueToNextPublisherIndexMap)
		{
			//round robin
			currPubIndex = queueToNextPublisherIndexMap.get(queue);
			int nextPubIndex = currPubIndex + 1;
			if (nextPubIndex == queueToNoOfPublishersMap.get(queue))
				nextPubIndex = 0;
			queueToNextPublisherIndexMap.put(queue, nextPubIndex);
		}
		Publisher[] publishers = queueToPublishersMap.get(queue);

		synchronized(getPublisherQueueLock(queue, currPubIndex)){
			Publisher publisher = publishers[currPubIndex];			
			int count =0;
			boolean retry = false;
			do{
				try
				{
					if(retry){
						publisher = MessengerFactory.getPublisher(queue);
						publishers[currPubIndex] = publisher;
						retry = false;
					}
					return publisher.publish(message);					
				} catch(MessengerException ex){	
					logger.error(ex.getMessage());
					count++;
					retry = true;
				}
			}while(retry && count < retryCount);
		}
		throw new MessengerException("Unable to publish to queue after retries");
	}
	
	private Object getPublisherQueueLock(Queue queue, int index){
		return publisherQueueLocks.get(getQueueLockKey(queue, index));
	}
	
	private Object getPublisherTopicLock(Topic topic, int index){
		return publisherTopicLocks.get(getTopicLockKey(topic, index));
	}
	
	private String getQueueLockKey(Queue queue, int index){
		return getLockKey(queue.name(), index); 
	}
	
	private String getTopicLockKey(Topic topic, int index){
		return getLockKey(topic.name(), index); 
	}
	
	private static String getLockKey(String name, int index){
		return name + "-" + index;
	}
	
	public String publishMessage(Topic topic, Message message) throws MessengerException
	{
		return publishMessage(topic, message, 0);
	}
	
	public String publishMessage(Topic topic, Message message, int retryCount) throws MessengerException
	{
		int currPubIndex = -1;
		synchronized(topicToNextPublisherIndexMap)
		{
			//round robin
			currPubIndex = topicToNextPublisherIndexMap.get(topic);
			int nextPubIndex = currPubIndex + 1;
			if (nextPubIndex == topicToNoOfPublishersMap.get(topic))
				nextPubIndex = 0;
			topicToNextPublisherIndexMap.put(topic, nextPubIndex);
		}
		Publisher[] publishers = topicToPublishersMap.get(topic);
		
		synchronized(getPublisherTopicLock(topic, currPubIndex)){
			Publisher publisher = publishers[currPubIndex];			
			int count =0;
			boolean retry = false;
			do{
				try
				{
					if(retry){
						publisher = MessengerFactory.getPublisher(topic);
						publishers[currPubIndex] = publisher;
						retry = false;
					}
					return publisher.publish(message);					
				} catch(MessengerException ex){
					logger.error(ex.getMessage());
					count++;
					retry = true;
				}
			}while(retry && count < retryCount);
		}
		throw new MessengerException("Unable to publish to topic after retries");
	}
	
	public void closePublishers(Topic topic) throws MessengerException{
		Publisher[] publishers = topicToPublishersMap.get(topic);
		if(publishers != null && publishers.length > 0)
		{
			for(Publisher publisher : publishers)
			{
				try
				{
					publisher.close();
				}
				catch(MessengerException ex){					
				}
			}
		}
	}
	
	public void closePublishers(Queue queue) throws MessengerException{
		if (queueToPublishersMap.containsKey(queue))
		{
			Publisher[] existingPublishers = queueToPublishersMap.get(queue);
			if(existingPublishers != null && existingPublishers.length > 0){
				for(Publisher publisher : existingPublishers)
				{
					try
					{
						publisher.close();
					}
					catch(MessengerException ex){					
					}
				}
			}			
		}
	}
	
	public void closeAllPublishers() throws MessengerException{
		if(queueToPublishersMap != null && queueToPublishersMap.size() > 0){
			for(Queue queue : queueToPublishersMap.keySet()){
				closePublishers(queue);
				QueueConnectionManager.closeConnectionForQueue(queue);
			}
		}
		
		if(topicToPublishersMap != null && topicToPublishersMap.size() > 0){
			for(Topic topic : topicToPublishersMap.keySet()){				
				closePublishers(topic);
				QueueConnectionManager.closeConnectionForTopic(topic);
			}
		}
	}
}
