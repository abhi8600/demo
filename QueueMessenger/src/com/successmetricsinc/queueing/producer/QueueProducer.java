/**
 * 
 */
package com.successmetricsinc.queueing.producer;

import com.successmetricsinc.queueing.Message;

/**
 * @author agarrison
 *
 */
public class QueueProducer {

	/** 
	 * Posts a message to the queue
	 * @param msg the message to post
	 * @return	the queue id
	 */
	public static String postMessage(Message msg) {
		// post the message to the queue
		return "id";
	}
}
