﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antlr.Runtime.Tree;
using Antlr.Runtime;

namespace ExpressionParser
{
    public class LogicalExpression
    {
        private LogicalExpressionLexer lel = null;
        private LogicalExpressionParser lep = null;
        private CommonTree tree;
        private Dictionary<string, string> columnMap;
        Dictionary<string, DataType> typeMap;
        private string error;
        private string result;
        private DataType expressionType;
        private List<string> logicalColumns;
        private string tableName;
        private string physicalTableName;
        private int topn = -1;
        private List<string> projectionListColumns;
        public static int SQL_GEN_DEFAULT = 0;
        public static int SQL_GEN_MYSQL = 1;
        public static int SQL_GEN_ORACLE = 2;
        public static int SQL_GEN_BIRST = 3;
        public static int SQL_GEN_PARACCEL = 4;
        public static int SQL_GEN_HANA = 5;
        public string physicalFilter;
        private int sqlGen = SQL_GEN_DEFAULT;
        private bool newQueryLanguageOperations = true; // if true, 0 based SUBSTRING and POSITION, SUBSTRING arguments like Java, if false, SQL like

        public LogicalExpression(string query, string tableName, string physicalTableName, Dictionary<string, string> columnMap, 
            Dictionary<string, DataType> typeMap, int topn, int sqlGenType, bool newQueryLanguageOperations)
        {
            this.columnMap = columnMap;
            this.typeMap = typeMap;
            this.tableName = tableName;
            this.physicalTableName = physicalTableName;
            this.topn = topn;
            this.sqlGen = sqlGenType;
            this.newQueryLanguageOperations = newQueryLanguageOperations;
            try
            {
                lel = new LogicalExpressionLexer(new ANTLRStringStream(query));
                CommonTokenStream cts = new CommonTokenStream(lel);
                lep = new LogicalExpressionParser(cts);
                AstParserRuleReturnScope<CommonTree, IToken> le = lep.query();
                logicalColumns = new List<string>();
                tree = (CommonTree)le.Tree;
                try
                {
                    result = generateCode();
                    expressionType = new DataType("None", 0);
                }
                catch (UnrecognizedLogicalColumnException ex)
                {
                    error = "Unrecognized logical column: " + ex.name;
                }
                catch (UnrecognizedTableException ex)
                {
                    error = "Unrecognized table: " + ex.name;
                }
                catch (InvalidQueryException ex)
                {
                    error = "Invalid query: " + ex.name;
                }
                catch (InvalidDataTypeException ex)
                {
                    error = "Invalid data type: " + ex.name;
                }
            }
            catch (RecognitionException e)
            {
		        error = "Error at position: " + e.CharPositionInLine + " (" + e.Message + ")";
            }
        }

        public LogicalExpression(string expr, Dictionary<string, string> columnMap, Dictionary<string, DataType> typeMap, int sqlGenType, bool newQueryLanguageOperations)
        {
            this.columnMap = columnMap;
            this.typeMap = typeMap;
            this.sqlGen = sqlGenType;
            this.newQueryLanguageOperations = newQueryLanguageOperations;
            try
            {
                lel = new LogicalExpressionLexer(new ANTLRStringStream(expr));
                CommonTokenStream cts = new CommonTokenStream(lel);
                lep = new LogicalExpressionParser(cts);
                AstParserRuleReturnScope<CommonTree, IToken> le = lep.expression();
                logicalColumns = new List<string>();
                tree = (CommonTree)le.Tree;
                try
                {
                    result = generateCode();
                    expressionType = getType();
                    if (expressionType.Name != "Varchar")
                        expressionType.Width = 0;
                    if (expressionType.Name == "Boolean")
                    {
                        if (sqlGenType == SQL_GEN_BIRST)
                            result = "CAST(" + result + " AS INTEGER)";
                        else
                            result = "CAST(" + result + " AS INT)";
                        expressionType.Name = "Integer";
                        expressionType.Width = 0;
                    }
                }
                catch (UnrecognizedLogicalColumnException ex)
                {
                    error = "Unrecognized logical column: " + ex.name;
                }
                catch (InvalidDataTypeException ex)
                {
                    error = "Invalid data type: " + ex.name;
                }
            }
            catch (RecognitionException e)
            {
		        error = "Error at position: " + e.CharPositionInLine + " (" + e.Message + ")";
            }
        }

        public bool HasError
        {
            get { return (error != null || lel.HasError); }
        }

        public string Error
        {
            get { return (error != null ? error : lel.ErrorMessage); }
        }

        public string GeneratedCode
        {
            get { return (error == null ? result : null); }
        }

        public DataType Type
        {
            get { return (error == null ? expressionType : null); }
        }

        public List<string> LogicalColumns
        {
            get { return (logicalColumns); }
        }

        public List<string> ProjectionListColumns
        {
            get { return projectionListColumns; }
        }

        private string generateCode()
        {
            if (tree == null)
                return (null);
            // Prune any top level parenthesis
            while (tree.Type == LogicalExpressionParser.PAREN)
            {
                if (tree.Children.Count == 1)
                    tree = (CommonTree)tree.GetChild(0);
            }
            return (generateCode(tree));
        }

        private bool constant = true;

        private static string dateOpCode(int type)
        {
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    return "YEAR";
                case LogicalExpressionParser.QUARTER:
                    return "QUARTER";
                case LogicalExpressionParser.MONTH:
                    return "MONTH";
                case LogicalExpressionParser.WEEK:
                    return "WEEK";
                case LogicalExpressionParser.DAY:
                    return "DAY";
                case LogicalExpressionParser.HOUR:
                    return "HOUR";
                case LogicalExpressionParser.MINUTE:
                    return "MINUTE";
                case LogicalExpressionParser.SECOND:
                    return "SECOND";
            }
            return null;
        }

        private string generateCode(ITree tree)
        {
            switch (tree.Type)
            {
                case LogicalExpressionParser.SELECT:
                    return getSelect(tree);
                case LogicalExpressionParser.DELETE:
                    return getDelete(tree);
                case LogicalExpressionParser.UPDATE:
                    return getUpdate(tree);
                case LogicalExpressionParser.PAREN:
                    return ('(' + generateCode(tree.GetChild(0)) + ')');
                case LogicalExpressionParser.INTEGER:
                    return (tree.Text);
                case LogicalExpressionParser.FLOAT:
                    return (tree.Text);
                case LogicalExpressionParser.STRING:
                    if (sqlGen == SQL_GEN_DEFAULT)
                        return ('N'+tree.Text);
                    else
                    return (tree.Text);
                case LogicalExpressionParser.DATETIME:
                    if (tree.Text.ToLower() == "now")
                    {
                        if (sqlGen == SQL_GEN_MYSQL)
                            return ("NOW()");
                        else if (sqlGen == SQL_GEN_ORACLE)
                            return ("sysdate");
                        else if (sqlGen == SQL_GEN_HANA)
                            return ("CURRENT_DATE");
                        return ("GETDATE()");
                    }
                    return ("'" + tree.Text.Substring(1, tree.Text.Length - 2) + "'");
                case LogicalExpressionParser.BOOLEAN:
                    return (tree.Text);
                case LogicalExpressionParser.NULL:
                    return "NULL";
                case LogicalExpressionParser.PLUS:
                    ITree cl = tree.GetChild(0);
                    ITree cr = tree.GetChild(1);
                    if (cl.Type == LogicalExpressionParser.STRING && cr.Type == LogicalExpressionParser.STRING)
                        if (sqlGen == SQL_GEN_DEFAULT)
                            return ('N'+cl.Text.Substring(0, cl.Text.Length - 1) + cr.Text.Substring(1));
                        else
                        return (cl.Text.Substring(0, cl.Text.Length - 1) + cr.Text.Substring(1));
                    DataType dl = getType(cl);
                    DataType dr = getType(cr);
                    /*
                     * MySQL uses CONCAT(....)
                     */
                    if (sqlGen == SQL_GEN_MYSQL)
                    {
                        if (dl.Name == "Varchar" || dr.Name == "Varchar")
                            return "CONCAT(CAST(IFNULL(" + generateCode(cl) + ",'') AS CHAR CHARACTER SET utf8),CAST(IFNULL(" + generateCode(cr) + ",'') AS CHAR CHARACTER SET utf8))";
                        return generateCode(cl) + "+" + generateCode(cr);
                    }
                    else if (sqlGen == SQL_GEN_ORACLE)
                    {
                        if (dl.Name == "Varchar" || dr.Name == "Varchar")
                            return "CONCAT(NVL(" + generateCode(cl) + ",''),NVL(" + generateCode(cr) + ",''))";
                        return generateCode(cl) + "+" + generateCode(cr);
                    }
                    else if (sqlGen == SQL_GEN_HANA)
                    {
                        if (dl.Name == "Varchar" && dr.Name != "Varchar")
                            return ("IFNULL(" + generateCode(cl) + ",'') || TO_CHAR(" + generateCode(cr) + ")");
                        if (dl.Name != "Varchar" && dr.Name == "Varchar")
                            return ("TO_CHAR(" + generateCode(cl) + ") || IFNULL(" + generateCode(cr) + ",'')");
                        if (dl.Name == "Varchar" && dr.Name == "Varchar")
                        {
                            return (cl.Type == LogicalExpressionParser.STRING ? generateCode(cl) : "IFNULL(" + generateCode(cl) + ",'')") + " || " +
                                (cr.Type == LogicalExpressionParser.STRING ? generateCode(cr) : "IFNULL(" + generateCode(cr) + ",'')");
                        }
                    }
                    /*
                     * For SQL Server 2005 need to surround varchars with ISNULLs because if one of the columns
                     * is null, then the query will return no rows otherwise
                     */
                    if (dl.Name == "Varchar" && dr.Name != "Varchar")
                        return ("ISNULL(" + generateCode(cl) + ",'')+CAST(" + generateCode(cr) + " AS VARCHAR)");
                    if (dl.Name != "Varchar" && dr.Name == "Varchar")
                        return ("CAST(" + generateCode(cl) + " AS VARCHAR)+ISNULL(" + generateCode(cr) + ",'')");
                    if (dl.Name == "Varchar" && dr.Name == "Varchar")
                    {
                        return (cl.Type == LogicalExpressionParser.STRING ? generateCode(cl) : "ISNULL(" + generateCode(cl) + ",'')") + "+" +
                            (cr.Type == LogicalExpressionParser.STRING ? generateCode(cr) : "ISNULL(" + generateCode(cr) + ",'')");
                    }
                    return (generateCode(cl) + "+" + generateCode(cr));
                case LogicalExpressionParser.MINUS:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "-" + generateCode(cr));
                case LogicalExpressionParser.NEGATE:
                    cl = tree.GetChild(0);
                    return ("-" + generateCode(cl));
                case LogicalExpressionParser.ISNULL:
                    cl = tree.GetChild(0);
                    return (generateCode(cl) + " IS NULL");
                case LogicalExpressionParser.ISNOTNULL:
                    cl = tree.GetChild(0);
                    return (generateCode(cl) + " IS NOT NULL");
                case LogicalExpressionParser.DIV:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "/" + generateCode(cr));
                case LogicalExpressionParser.MOD:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "%" + generateCode(cr));
                case LogicalExpressionParser.BITAND:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    if (sqlGen == SQL_GEN_ORACLE)
                        return ("BITAND(" + generateCode(cl) + "," + generateCode(cr) + ')');
                    else
                        return (generateCode(cl) + "&" + generateCode(cr));
                case LogicalExpressionParser.MULT:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "*" + generateCode(cr));
                case LogicalExpressionParser.EQUALS:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "=" + generateCode(cr));
                case LogicalExpressionParser.LT:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "<" + generateCode(cr));
                case LogicalExpressionParser.GT:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + ">" + generateCode(cr));
                case LogicalExpressionParser.LTEQ:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "<=" + generateCode(cr));
                case LogicalExpressionParser.GTEQ:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + ">=" + generateCode(cr));
                case LogicalExpressionParser.NOTEQUALS:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + "<>" + generateCode(cr));
                case LogicalExpressionParser.AND:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + " AND " + generateCode(cr));
                case LogicalExpressionParser.OR:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return (generateCode(cl) + " OR " + generateCode(cr));
                case LogicalExpressionParser.DATEDIFF:
                    StringBuilder result = new StringBuilder();
                    if (sqlGen == SQL_GEN_MYSQL)
                    {
                        return getMySQLDateDiff(tree.GetChild(0).Type, generateCode(tree.GetChild(1)), generateCode(tree.GetChild(2)));
                    }
                    else if (sqlGen == SQL_GEN_ORACLE)
                    {
                        return getOracleDateDiff(tree.GetChild(0).Type, generateCode(tree.GetChild(1)), generateCode(tree.GetChild(2)));
                    }
                    else if (sqlGen == SQL_GEN_HANA)
                    {
                        return getHanaDateDiff(tree.GetChild(0).Type, generateCode(tree.GetChild(1)), generateCode(tree.GetChild(2)));
                    }
                    else
                        result.Append("DATEDIFF(");
                    result.Append(dateOpCode(tree.GetChild(0).Type));
                    result.Append("," + generateCode(tree.GetChild(1)) + "," + generateCode(tree.GetChild(2)) + ")");
                    return (result.ToString());
                case LogicalExpressionParser.DATEADD:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_MYSQL)
                        result.Append("TIMESTAMPADD(");
                    else if (sqlGen == SQL_GEN_ORACLE)
                    {
                        return getOracleDateADD(tree.GetChild(0).Type, generateCode(tree.GetChild(1)), generateCode(tree.GetChild(2)));
                    }
                    else if (sqlGen == SQL_GEN_HANA)
                    {
                        return getHanaDateADD(tree.GetChild(0).Type, generateCode(tree.GetChild(1)), generateCode(tree.GetChild(2)));
                    }
                    else
                        result.Append("DATEADD(");
                    result.Append(dateOpCode(tree.GetChild(0).Type));
                    result.Append(",CAST(" + generateCode(tree.GetChild(1)) + " AS " + (sqlGen == SQL_GEN_MYSQL ? "SIGNED " : "") + "INTEGER),");
                    result.Append(generateCode(tree.GetChild(2)) + ")");
                    return (result.ToString());
                case LogicalExpressionParser.DATEPART:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_MYSQL)
                    {
                        result.Append(dateOpCode(tree.GetChild(0).Type));
                        result.Append("(" + generateCode(tree.GetChild(1)) + ")");
                    }
                    else if (sqlGen == SQL_GEN_ORACLE)
                    {
                        return getOracleDatePart(tree.GetChild(0).Type, generateCode(tree.GetChild(1)));
                    }
                    else if (sqlGen == SQL_GEN_HANA)
                    {
                        return getHanaDatePart(tree.GetChild(0).Type, generateCode(tree.GetChild(1)));
                    }
                    else
                    {
                        result.Append("DATEPART(");
                        result.Append(dateOpCode(tree.GetChild(0).Type));
                        result.Append("," + generateCode(tree.GetChild(1)) + ")");
                    }
                    return (result.ToString());
                case LogicalExpressionParser.SUBSTR:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_ORACLE)
                        result.Append("SUBSTR(");
                    else
                        result.Append("SUBSTRING(");
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    if (newQueryLanguageOperations)
                    {
                        result.Append(generateCode(cl) + ",(" + generateCode(cr) + ")+1");
                        if (tree.ChildCount > 2)
                            result.Append(",(" + generateCode(tree.GetChild(2)) + ")-(" + generateCode(cr) + ")");
                    }
                    else
                    {
                        result.Append(generateCode(cl) + "," + generateCode(cr));
                        if (tree.ChildCount > 2)
                            result.Append("," + generateCode(tree.GetChild(2)));
                    }
                    return (result.ToString() + ")");
                case LogicalExpressionParser.POSITION:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_MYSQL)
                        result.Append("(LOCATE(");
                    else if (sqlGen == SQL_GEN_ORACLE || sqlGen == SQL_GEN_HANA)
                        result.Append("(INSTR(");
                    else if (sqlGen == SQL_GEN_BIRST)
                        result.Append("(POSITION(");
                    else
                        result.Append("(CHARINDEX(");
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    if (sqlGen == SQL_GEN_ORACLE || sqlGen == SQL_GEN_HANA)
                        result.Append(generateCode(cr) + "," + generateCode(cl));
                    else
                        result.Append(generateCode(cl) + "," + generateCode(cr));
                    if (newQueryLanguageOperations)
                        return (result.ToString() + ")-1)");
                    else
                        return (result.ToString() + "))");
                case LogicalExpressionParser.LENGTH:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_MYSQL || sqlGen == SQL_GEN_ORACLE || sqlGen == SQL_GEN_BIRST || sqlGen == SQL_GEN_HANA)
                        result.Append("LENGTH(");
                    else
                        result.Append("LEN(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.TOUPPER:
                    result = new StringBuilder();
                    result.Append("UPPER(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.TOLOWER:
                    result = new StringBuilder();
                    result.Append("LOWER(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.SIN:
                    result = new StringBuilder();
                    result.Append("SIN(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.COS:
                    result = new StringBuilder();
                    result.Append("COS(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.TAN:
                    result = new StringBuilder();
                    result.Append("TAN(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.ARCSIN:
                    result = new StringBuilder();
                    result.Append("ASIN(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.ARCCOS:
                    result = new StringBuilder();
                    result.Append("ACOS(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.ARCTAN:
                    result = new StringBuilder();
                    result.Append("ATAN(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.ABS:
                    result = new StringBuilder();
                    result.Append("ABS(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.CEILING:
                    result = new StringBuilder();
                    result.Append("CEILING(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.FLOOR:
                    result = new StringBuilder();
                    result.Append("FLOOR(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.SQRT:
                    result = new StringBuilder();
                    result.Append("SQRT(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.LN:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_PARACCEL || sqlGen == SQL_GEN_HANA)
                        result.Append("LN(");
                    else
                        result.Append("LOG(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.LOG:
                    result = new StringBuilder();
                    if (sqlGen == SQL_GEN_PARACCEL || sqlGen == SQL_GEN_HANA)
                        result.Append("LOG(");
                    else
                        result.Append("LOG10(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    if (sqlGen == SQL_GEN_HANA)
                    {
                        result.Append(",10");
                    }
                    return (result.ToString() + ")");
                case LogicalExpressionParser.EXP:
                    result = new StringBuilder();
                    result.Append("EXP(");
                    cl = tree.GetChild(0);
                    result.Append(generateCode(cl));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.POWER:
                    result = new StringBuilder();
                    result.Append("POWER(");
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    result.Append(generateCode(cl));
                    result.Append(',');
                    result.Append(generateCode(cr));
                    return (result.ToString() + ")");
                case LogicalExpressionParser.GETPROMPTVALUE:
                    cl = tree.GetChild(0);
                    return ("V{P[" + cl.Text + "]}");
                case LogicalExpressionParser.GETVARIABLE:
                    cl = tree.GetChild(0);
                    return ("V{" + cl.Text.Substring(1, cl.Text.Length - 2) + "}");
                case LogicalExpressionParser.IDENT_NAME:
                case LogicalExpressionParser.LEVEL:
                    string name = tree.Text.Substring(1, tree.Text.Length - 2);
                    if (columnMap != null && !columnMap.ContainsKey(name))
                        throw new UnrecognizedLogicalColumnException(name);
                    logicalColumns.Add(name);
                    constant = false;
                    if (columnMap != null)
                        return columnMap[name];
                    else
                        return null;
                case LogicalExpressionParser.CONDITIONAL:
                    // Factor out top level filter if Infobright
                    if (sqlGen == SQL_GEN_MYSQL && tree == this.tree)
                    {
                        int numConditions = 0;
                        for (int i = 0; i < tree.ChildCount; i++)
                        {
                            if (tree.GetChild(i).Type == LogicalExpressionParser.CONDITION)
                                numConditions++;
                        }
                        if (numConditions == 1)
                        {
                            if (tree.ChildCount == 1 || tree.GetChild(1).Type == LogicalExpressionParser.NULL ||
                                (tree.GetChild(1).Type == LogicalExpressionParser.INTEGER && tree.GetChild(1).Text == "0"))
                            {
                                physicalFilter = generateCode(tree.GetChild(0).GetChild(0));
                                return generateCode(tree.GetChild(0).GetChild(1));
                            }
                        }
                    }
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < tree.ChildCount; i++)
                    {
                        if (i > 0)
                            sb.Append(' ');
                        if (tree.GetChild(i).Type == LogicalExpressionParser.CONDITION)
                            sb.Append(generateCode(tree.GetChild(i)));
                        else
                            sb.Append("ELSE " + generateCode(tree.GetChild(i)));
                    }
                    return ("CASE " + sb.ToString() + " END");
                case LogicalExpressionParser.CONDITION:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    return ("WHEN " + generateCode(cl) + " THEN " + generateCode(cr));
                case LogicalExpressionParser.CAST:
                    cl = tree.GetChild(0); // type
                    cr = tree.GetChild(1); // value
                    return getCastOperation(cl, cr);
                default:
                    break;
            }
            return (null);
        }

        public string getCastOperation(ITree cast, ITree value)
        {
            string type = null;
            switch (cast.Type)
            {
                case LogicalExpressionParser.VARCHARTYPE:
                    type = "VARCHAR(30)";
                    break;
                case LogicalExpressionParser.INTEGERTYPE:
                    if (sqlGen == SQL_GEN_MYSQL)
                        type = "SIGNED";
                    else
                        type = "INTEGER";
                    break;
                case LogicalExpressionParser.FLOATTYPE:
                    if (sqlGen == SQL_GEN_MYSQL || sqlGen == SQL_GEN_HANA)
                        throw new InvalidDataTypeException("FLOAT");
                    type = "FLOAT";
                    break;
                case LogicalExpressionParser.DATETYPE:
                    type = "DATE";
                    break;
                case LogicalExpressionParser.DATETIMETYPE:
                    type = "DATETIME";
                    break;
                default:
                    throw new InvalidDataTypeException(cast.Text);
            }
            return "CAST(" + generateCode(value) + " AS " + type + ")";
        }

        public DataType getCastType(ITree cast)
        {
            switch (cast.Type)
            {
                case LogicalExpressionParser.VARCHARTYPE:
                    return new DataType("Varchar", 30);
                case LogicalExpressionParser.INTEGERTYPE:
                    return new DataType("Integer", 20);
                case LogicalExpressionParser.FLOATTYPE:
                    return new DataType("Float", 20);
                case LogicalExpressionParser.DATETYPE:
                    return new DataType("Date", 20);
                case LogicalExpressionParser.DATETIMETYPE:
                    return new DataType("DateTime", 20); ;
                default:
                    throw new InvalidDataTypeException(tree.Text);
            }
        }

        public bool isConstant
        {
            get { return (constant); }
        }

        public class DataType
        {
            public string Name;
            public int Width;

            public DataType(string Name, int Width)
            {
                this.Name = Name;
                this.Width = Width;
            }

            public override string ToString()
            {
                if (Name == "Varchar")
                    return ("Varchar(" + Width + ")");
                return (Name);
            }
        }

        private DataType typeMath(DataType dtl, DataType dtr)
        {
            if (dtl.Name == "Integer" && dtr.Name == "Integer")
                return (new DataType("Integer", 20));
            if ((dtl.Name == "Float" && (dtr.Name == "Float" || dtr.Name == "Integer")) ||
                (dtr.Name == "Float" && (dtl.Name == "Float" || dtl.Name == "Integer")))
                return (new DataType("Float", 20));
            if ((dtl.Name == "Number" && (dtr.Name == "Integer" || dtr.Name == "Float" || dtr.Name == "Number")) ||
                (dtr.Name == "Number" && (dtl.Name == "Integer" || dtl.Name == "Float" || dtl.Name == "Number")))
                return new DataType("Number", 20);
            return (new DataType("Varchar", dtl.Width + dtr.Width));
        }

        private DataType getType()
        {
            if (tree == null)
                return (null);
            return (getType(tree));
        }

        private DataType getType(ITree tree)
        {
            switch (tree.Type)
            {
                case LogicalExpressionParser.PAREN:
                    return (getType(tree.GetChild(0)));
                case LogicalExpressionParser.INTEGER:
                    return (new DataType("Integer", 10));
                case LogicalExpressionParser.FLOAT:
                    return (new DataType("Float", 15));
                case LogicalExpressionParser.STRING:
                    return (new DataType("Varchar", tree.Text.Length - 2));
                case LogicalExpressionParser.DATETIME:
                    return (new DataType("DateTime", 20));
                case LogicalExpressionParser.NULL:
                    return (new DataType("Any", 0));
                case LogicalExpressionParser.SIN:
                case LogicalExpressionParser.COS:
                case LogicalExpressionParser.TAN:
                case LogicalExpressionParser.ARCSIN:
                case LogicalExpressionParser.ARCCOS:
                case LogicalExpressionParser.ARCTAN:
                case LogicalExpressionParser.ABS:
                case LogicalExpressionParser.SQRT:
                case LogicalExpressionParser.LN:
                case LogicalExpressionParser.LOG:
                case LogicalExpressionParser.EXP:
                case LogicalExpressionParser.POWER:
                    return (new DataType("Float", 15));
                case LogicalExpressionParser.CEILING:
                case LogicalExpressionParser.FLOOR:
                    return (new DataType("Integer", 10));
                case LogicalExpressionParser.BOOLEAN:
                case LogicalExpressionParser.EQUALS:
                case LogicalExpressionParser.LT:
                case LogicalExpressionParser.GT:
                case LogicalExpressionParser.LTEQ:
                case LogicalExpressionParser.GTEQ:
                case LogicalExpressionParser.NOTEQUALS:
                case LogicalExpressionParser.AND:
                case LogicalExpressionParser.OR:
                case LogicalExpressionParser.ISNULL:
                case LogicalExpressionParser.ISNOTNULL:
                    return (new DataType("Boolean", 5));
                case LogicalExpressionParser.PLUS:
                    ITree cl = tree.GetChild(0);
                    ITree cr = tree.GetChild(1);
                    DataType dtl = getType(cl);
                    DataType dtr = getType(cr);
                    if (dtl.Name == "DateTime" || dtr.Name == "DateTime" || dtl.Name == "Date" || dtr.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean" || dtr.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    return (typeMath(dtl, dtr));
                case LogicalExpressionParser.MINUS:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if (dtl.Name == "Varchar" || dtr.Name == "Varchar")
                        throw new InvalidDataTypeException("Varchar");
                    if (dtl.Name == "DateTime" || dtr.Name == "DateTime" || dtl.Name == "Date" || dtr.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean" || dtr.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    return (typeMath(dtl, dtr));
                case LogicalExpressionParser.NEGATE:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar")
                        throw new InvalidDataTypeException("Varchar");
                    if (dtl.Name == "DateTime" || dtl.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    dtl = getType(cl);
                    return (dtl);
                case LogicalExpressionParser.DIV:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if (dtl.Name == "Varchar" || dtr.Name == "Varchar")
                        throw new InvalidDataTypeException("Varchar");
                    if (dtl.Name == "DateTime" || dtr.Name == "DateTime" || dtl.Name == "Date" || dtr.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean" || dtr.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    return (typeMath(dtl, dtr));
                case LogicalExpressionParser.MOD:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if (dtl.Name == "Varchar" || dtr.Name == "Varchar")
                        throw new InvalidDataTypeException("Varchar");
                    if (dtl.Name == "DateTime" || dtr.Name == "DateTime" || dtl.Name == "Date" || dtr.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean" || dtr.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    return (typeMath(dtl, dtr));
                case LogicalExpressionParser.BITAND:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if (dtl.Name != "Integer" && dtr.Name != "Integer")
                        throw new InvalidDataTypeException("Not Integer");
                    return (new DataType("Integer", 20));
                case LogicalExpressionParser.MULT:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if (dtl.Name == "Varchar" || dtr.Name == "Varchar")
                        throw new InvalidDataTypeException("Varchar");
                    if (dtl.Name == "DateTime" || dtr.Name == "DateTime" || dtl.Name == "Date" || dtr.Name == "Date")
                        throw new InvalidDataTypeException("Date/DateTime");
                    if (dtl.Name == "Boolean" || dtr.Name == "Boolean")
                        throw new InvalidDataTypeException("Boolean");
                    return (typeMath(dtl, dtr));
                case LogicalExpressionParser.DATEDIFF:
                    string t1 = getType(tree.GetChild(1)).Name;
                    string t2 = getType(tree.GetChild(2)).Name;
                    if ((t1 == "DateTime" || t1 == "Date" || t1 == "Any") && (t2 == "DateTime" || t2 == "Date" || t2 == "Any"))
                        return (new DataType("Integer", 20));
                    throw new InvalidDataTypeException("DATEDIFF");
                case LogicalExpressionParser.DATEADD:
                    t1 = getType(tree.GetChild(1)).Name;
                    t2 = getType(tree.GetChild(2)).Name;
                    if ((t2 == "DateTime" || t2 == "Date" || t2 == "Any") && ((t1 == "Integer") || (t1 == "Varchar"))) // we do a CAST to INTEGER
                        return (new DataType("DateTime", 20));
                    throw new InvalidDataTypeException("DATEADD");
                case LogicalExpressionParser.DATEPART:
                    t1 = getType(tree.GetChild(1)).Name;
                    if (t1 == "DateTime" || t1 == "Date" || t1 == "Any")
                        return (new DataType("Integer", 20));
                    throw new InvalidDataTypeException("DATEPART");
                case LogicalExpressionParser.SUBSTR:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    DataType dtp = getType(tree.GetChild(2));
                    if ((dtl.Name == "Varchar" || dtl.Name == "Any") && (dtr.Name == "Integer" || dtr.Name == "Any") &&
                        (tree.ChildCount == 2 || (dtp.Name == "Integer") || dtp.Name == "Any"))
                        return (dtl);
                    throw new InvalidDataTypeException("SUBSTRING");
                case LogicalExpressionParser.LENGTH:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar" || dtl.Name == "Any")
                        return (new DataType("Integer", 20));
                    throw new InvalidDataTypeException("LENGTH");
                case LogicalExpressionParser.TOUPPER:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar" || dtl.Name == "Any")
                        return dtl;
                    throw new InvalidDataTypeException("TOUPPER");
                case LogicalExpressionParser.TOLOWER:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar" || dtl.Name == "Any")
                        return dtl;
                    throw new InvalidDataTypeException("TOLOWER");
                case LogicalExpressionParser.GETPROMPTVALUE:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar")
                        return (new DataType("Varchar", 20));
                    throw new InvalidDataTypeException("GETPROMPTVALUE");
                case LogicalExpressionParser.GETVARIABLE:
                    cl = tree.GetChild(0);
                    dtl = getType(cl);
                    if (dtl.Name == "Varchar")
                        return (new DataType("Varchar", 20));
                    throw new InvalidDataTypeException("GETVARIABLE");
                case LogicalExpressionParser.POSITION:
                    cl = tree.GetChild(0);
                    cr = tree.GetChild(1);
                    dtl = getType(cl);
                    dtr = getType(cr);
                    if ((dtl.Name == "Varchar" || dtl.Name == "Any") && (dtr.Name == "Varchar" || dtr.Name == "Any"))
                        return (new DataType("Integer", 20));
                    throw new InvalidDataTypeException("POSITION");
                case LogicalExpressionParser.IDENT_NAME:
                case LogicalExpressionParser.LEVEL:
                    string name = tree.Text.Substring(1, tree.Text.Length - 2);
                    if (typeMap != null && !typeMap.ContainsKey(name))
                        throw new UnrecognizedLogicalColumnException(name);
                    if (typeMap != null)
                        return typeMap[name];
                    else
                        return new DataType("Any", 0);
                case LogicalExpressionParser.CONDITIONAL:
                    DataType curType = null;
                    for (int i = 0; i < tree.ChildCount; i++)
                    {
                        if (tree.GetChild(i).Type == LogicalExpressionParser.CONDITION)
                        {
                            DataType condType = getType(tree.GetChild(i).GetChild(0));
                            if (condType.Name != "Boolean" && condType.Name != "Any")
                                throw new InvalidDataTypeException(condType.Name);
                        }
                        DataType newType = getType(tree.GetChild(i));
                        if (curType == null)
                            curType = newType;
                        else if (newType.Name != "Any")
                        {
                            if (newType.Name == "Varchar")
                            {
                                curType.Name = newType.Name;
                                curType.Width = Math.Max(curType.Width, newType.Width);
                            }
                            else if (curType.Name == "Varchar" && newType.Name != "Varchar")
                            {
                                curType.Width = Math.Max(curType.Width, newType.Width);
                            }
                            else if (curType.Name == "DateTime" &&
                                !(newType.Name == "DateTime" || newType.Name == "Date" || newType.Name == "Varchar"))
                            {
                                throw new InvalidDataTypeException("DateTime");
                            }
                            else if ((newType.Name == "DateTime" || newType.Name == "Date") &&
                                !(curType.Name == "DateTime" || curType.Name == "Date" || curType.Name == "Varchar"))
                            {
                                throw new InvalidDataTypeException("DateTime");
                            }
                            else if (curType.Name == "Integer" && newType.Name == "Float")
                            {
                                curType = newType;
                            }
                        }
                    }
                    return (curType);
                case LogicalExpressionParser.CONDITION:
                    cr = tree.GetChild(1);
                    return (getType(cr));
                case LogicalExpressionParser.CAST:
                    return getCastType(tree.GetChild(0));
                default:
                    break;
            }
            return (null);
        }

        private string getSelect(ITree tree)
        {
            projectionListColumns = new List<string>();
            ITree plist = tree.GetChild(0);
            string[] projectionList = getProjectionList(plist, null);
            ITree from = tree.GetChild(1);
            string fromtable = from.Text.Substring(1, from.Text.Length - 2);
            if (fromtable != tableName)
                throw new UnrecognizedTableException(fromtable);
            int index = 2;
            string where = null;
            string orderby = null;
            while (index < tree.ChildCount)
            {
                ITree clause = tree.GetChild(index);
                if (clause.Type == LogicalExpressionParser.WHERE)
                {
                    if (getType(clause.GetChild(0)).Name != "Boolean")
                        throw new InvalidQueryException(clause.GetChild(0).Text);
                    where = generateCode(clause.GetChild(0));
                }
                else if (clause.Type == LogicalExpressionParser.ORDERBY)
                {
                    string[] oblist = getProjectionList(clause.GetChild(0), plist);
                    orderby = oblist[0];
                    if (clause.ChildCount > 1)
                    {
                        if (clause.GetChild(1).Type == LogicalExpressionParser.ASCENDING)
                            orderby += " ASC";
                        else
                            orderby += " DESC";
                    }
                }
                index++;
            }
            bool hasOrderBy = (orderby != null);
            bool hasWhere = (where != null);
            bool useTop = false;
            bool useLimit = false;
            bool useRowNum = false;
            bool createSubQueryForRowNum = false;
            if (topn >= 0)
            {
                if (supportsLimit())
                    useLimit = true;
                else if (supportsRowNum())
                    useRowNum = true;
                else useTop = true; //default SQL Server

                createSubQueryForRowNum = (useRowNum && hasOrderBy); //if query has top and order by then create subquery
            }
            string query = "SELECT " + (useTop ? "TOP " + topn + " " : "") + projectionList[0] + " FROM " + physicalTableName + (where != null ? " WHERE " + where : "") +
                (useRowNum && !createSubQueryForRowNum ? (hasWhere ? " AND " : " WHERE ") + " ROWNUM <= " + topn : "") +
                (projectionList[1] != null && projectionList[1].Length > 0 ? " GROUP BY " + projectionList[1] : "") +
                (hasOrderBy ? " ORDER BY " + orderby : "") + (useLimit ? " LIMIT " + topn : "");
            if (createSubQueryForRowNum)
            {
                query = "SELECT * FROM (" + query + ") WHERE ROWNUM <= " + topn;
            }
            return query;
        }
        
        private string getUpdate(ITree tree)
        {
            projectionListColumns = new List<string>();
            ITree plist = tree.GetChild(0);
            string[] projectionList = getSetProjectionList(plist);
            ITree from = tree.GetChild(1);
            string fromtable = from.Text.Substring(1, from.Text.Length - 2);
            if (fromtable != tableName)
                throw new UnrecognizedTableException(fromtable);
            int index = 2;
            string where = null;
            while (index < tree.ChildCount)
            {
                ITree clause = tree.GetChild(index);
                if (clause.Type == LogicalExpressionParser.WHERE)
                {
                    if (getType(clause.GetChild(0)).Name != "Boolean")
                        throw new InvalidQueryException(clause.GetChild(0).Text);
                    where = generateCode(clause.GetChild(0));
                }
                index++;
            }
            return "UPDATE " + physicalTableName + " SET " + projectionList[0] + " FROM " + physicalTableName + (where != null ? " WHERE " + where : "");
        }

        private string getDelete(ITree tree)
        {
            ITree from = tree.GetChild(0);
            string fromtable = from.Text.Substring(1, from.Text.Length - 2);
            if (fromtable != tableName)
                throw new UnrecognizedTableException(fromtable);
            int index = 1;
            string where = null;
            while (index < tree.ChildCount)
            {
                ITree clause = tree.GetChild(index);
                if (clause.Type == LogicalExpressionParser.WHERE)
                {
                    if (getType(clause.GetChild(0)).Name != "Boolean")
                        throw new InvalidQueryException(clause.GetChild(0).Text);
                    where = generateCode(clause.GetChild(0));
                }
                index++;
            }
            return "DELETE FROM " + physicalTableName + (where != null ? " WHERE " + where : "");
        }

        private string[] getProjectionList(ITree plist, ITree verifyList)
        {
            StringBuilder projectionList = new StringBuilder();
            StringBuilder groupBy = new StringBuilder();
            bool hasAgg = false;
            for (int i = 0; i < plist.ChildCount; i++)
            {
                ITree child = plist.GetChild(i);
                if (child.Type == LogicalExpressionParser.IDENT_NAME || child.Type == LogicalExpressionParser.LEVEL || child.Type == LogicalExpressionParser.LEVEL_SET)
                {
                    string name = child.Text.Substring(1, child.Text.Length - 2);
                    if (projectionList.Length > 0)
                    {
                        projectionList.Append(',');
                        groupBy.Append(',');
                    }
                    if (columnMap != null && !columnMap.ContainsKey(name))
                        throw new UnrecognizedLogicalColumnException(name);
                    if (verifyList != null)
                    {
                        bool found = false;
                        for (int j = 0; j < verifyList.ChildCount; j++)
                        {
                            ITree vchild = verifyList.GetChild(j);
                            if (vchild.Text == child.Text)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            throw new InvalidQueryException("Order by columns must be in select list: " + name);
                    }
                    projectionList.Append(columnMap[name]);
                    if (verifyList == null)
                        projectionListColumns.Add(name);
                    groupBy.Append(columnMap[name]);
                }
                else if (child.Type == LogicalExpressionParser.SUM
                    || child.Type == LogicalExpressionParser.AVG
                    || child.Type == LogicalExpressionParser.MIN
                    || child.Type == LogicalExpressionParser.MAX
                    || child.Type == LogicalExpressionParser.COUNT)
                {
                    string name = child.GetChild(0).Text;
                    name = name.Substring(1, name.Length - 2);
                    hasAgg = true;
                    if (columnMap != null && !columnMap.ContainsKey(name))
                        throw new UnrecognizedLogicalColumnException(name);
                    if (verifyList != null)
                    {
                        bool found = false;
                        for (int j = 0; j < verifyList.ChildCount; j++)
                        {
                            ITree vchild = verifyList.GetChild(j);
                            if (vchild.Text == child.Text)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            throw new InvalidQueryException("Order by columns must be in select list: " + name);
                    }
                    if (projectionList.Length > 0)
                    {
                        projectionList.Append(',');
                    }
                    if (verifyList == null)
                        projectionListColumns.Add(name);
                    projectionList.Append(child.Text + "(" + columnMap[name] + ")");
                }
            }
            string[] result = new string[2];
            result[0] = projectionList.ToString();
            if (hasAgg)
                result[1] = groupBy.ToString();
            return result;
        }

        private string[] getSetProjectionList(ITree plist)
        {
            StringBuilder projectionList = new StringBuilder();
            for (int i = 0; i < plist.ChildCount; i++)
            {
                ITree node = plist.GetChild(i);
                if (node.Text != "=")
                    throw new InvalidQueryException("Update must be of the form: UPDATE [A]=val FROM [TABLE]");
                ITree child = node.GetChild(0);
                if (child.Type == LogicalExpressionParser.IDENT_NAME || child.Type == LogicalExpressionParser.LEVEL || child.Type == LogicalExpressionParser.LEVEL_SET)
                {
                    string name = child.Text.Substring(1, child.Text.Length - 2);
                    if (projectionList.Length > 0)
                    {
                        projectionList.Append(',');
                    }
                    if (columnMap != null && !columnMap.ContainsKey(name))
                        throw new UnrecognizedLogicalColumnException(name);
                    string val = generateCode(node.GetChild(1));
                    projectionList.Append(columnMap[name]+'='+val);
                    projectionListColumns.Add(name);
                }
                else
                    throw new InvalidQueryException("Update must be of the form: UPDATE [A]=val FROM [TABLE]");
            }
            string[] result = new string[1];
            result[0] = projectionList.ToString();
            return result;
        }

        /**
         * variation of the Oracle one, try to use built in scalar functions where they exist
         */
        public static string getHanaDateDiff(int type, string p_date1, string p_date2)
        {
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    return "YEAR(" + p_date2 + ") - YEAR(" + p_date1 + ")";
                case LogicalExpressionParser.QUARTER:
                    string year_quarter_diff = "(YEAR(" + p_date2 + ") * 4 - YEAR(" + p_date1 + ") * 4)";
                    string curr_quarter_diff = "(CEIL(MONTH(" + p_date2 + ") / 3) - CEIL(MONTH(" + p_date1 + ") / 3))";
                    return year_quarter_diff + " + " + curr_quarter_diff;
                case LogicalExpressionParser.MONTH:
                    string month1 = "(YEAR(" + p_date1 + ") * 12 + MONTH(" + p_date1 + "))";
                    string month2 = "(YEAR(" + p_date2 + ") * 12 + MONTH(" + p_date2 + "))";
                    return month2 + " - " + month1;
                case LogicalExpressionParser.WEEK:                    
                    return "(TO_NUMBER(SUBSTRING(ISOWEEK(" + p_date2 + "),7)) - TO_NUMBER(SUBSTRING(ISOWEEK(" + p_date1 + "),7))) + 52 * (YEAR(" + p_date2 + ") - YEAR(" + p_date1 + "))";
                case LogicalExpressionParser.DAY:
                    return "DAYS_BETWEEN(" + p_date1 + "," + p_date2 + ")";
                case LogicalExpressionParser.HOUR:
                    return "FLOOR(SECONDS_BETWEEN(" + p_date1 + "," + p_date2 + ") / 3600)";
                case LogicalExpressionParser.MINUTE:
                    return "FLOOR(SECONDS_BETWEEN(" + p_date1 + "," + p_date2 + ") / 60)";
                case LogicalExpressionParser.SECOND:
                    return "FLOOR(SECONDS_BETWEEN(" + p_date1 + "," + p_date2 + "))";
                default:
                    throw new InvalidQueryException("Bad DATEDIFF type: " + type);
            }
        }

        public static string getMySQLDateDiff(int type, string p_date1, string p_date2)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.MONTH:
                    result = "PERIOD_DIFF(DATE_FORMAT(" + p_date2 + ",'%Y%m'),DATE_FORMAT(" + p_date1 + ",'%Y%m'))";
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "FLOOR(TIMESTAMPDIFF(DAY," + p_date1 + "," + p_date2 + ")/7) + ";
                    result = result + "CASE WHEN DAYOFWEEK(" + p_date1 + ")>DAYOFWEEK(" + p_date2 + ") THEN 1 ELSE 0 END";
                    break;
                case LogicalExpressionParser.QUARTER:
                    result = "(YEAR(" + p_date2 + ") - YEAR(" + p_date1 + "))*4 + (QUARTER(" + p_date2 + ")-QUARTER(" + p_date1 + "))";
                    break;
                case LogicalExpressionParser.YEAR:
                    result = "YEAR(" + p_date2 + ") - YEAR(" + p_date1 + ")";
                    break;
                default:
                    result = "TIMESTAMPDIFF(" + dateOpCode(type) + "," + p_date1 + "," + p_date2 + ")";                    
                    break;
            }
            return result;
        }

        public static string getOracleDateDiff(int type, string p_date1, string p_date2)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    string year1 = "TO_CHAR(" + p_date1 + ",'YYYY')";
                    string year2 = "TO_CHAR(" + p_date2 + ",'YYYY')";
                    result =  year2  + "-"  + year1;
                    break;
                case LogicalExpressionParser.QUARTER:
                    string year_quarter_diff = "(TO_CHAR(" + p_date2 + ",'YYYY')*4) - (TO_CHAR(" + p_date1 + ",'YYYY')*4)";
                    string curr_quarter_diff = "(CEIL(TO_CHAR(" + p_date2 + ",'MM')/3)) - (CEIL(TO_CHAR(" + p_date1 + ",'MM')/3))";
                    result = year_quarter_diff + "+" + curr_quarter_diff;
                    break;
                case LogicalExpressionParser.MONTH:
                    string month1 = "(TO_CHAR(" + p_date1 + ",'YYYY')*12 + TO_NUMBER(TO_CHAR(" + p_date1 + ",'MM')))";
                    string month2 = "(TO_CHAR(" + p_date2 + ",'YYYY')*12 + TO_NUMBER(TO_CHAR(" + p_date2 + ",'MM')))";
                    result =  month2 + "-" + month1 ;
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "TO_NUMBER(TO_CHAR(" + p_date2 + ", 'WW')) - TO_NUMBER(TO_CHAR( " + p_date1 + ", 'WW')) + 52 * (TO_NUMBER(TO_CHAR(" + p_date2 + ", 'YYYY')) - TO_NUMBER(TO_CHAR( " + p_date1 + ", 'YYYY')))";
                    break;
                case LogicalExpressionParser.DAY:
                    result = "TRUNC(" + p_date2 + "-" + p_date1 + ")";
                    break;
                case LogicalExpressionParser.HOUR:
                    result = "TRUNC((" + p_date2 + "-" + p_date1 + ")*24)";
                    break;
                case LogicalExpressionParser.MINUTE:
                    result = "TRUNC((" + p_date2 + "-" + p_date1 + ")*24*60)";
                    break;
                case LogicalExpressionParser.SECOND:
                    result = "TRUNC((" + p_date2 + "-" + p_date1 + ") * 24 * 60 * 60)";
                    break;
            }
            return result;
        }

        public static string getHanaDateADD(int type, string cnt, string p_date1)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    result = "ADD_YEARS(" + p_date1 + "," + cnt + ")";
                    break;
                case LogicalExpressionParser.QUARTER:
                    result = "ADD_MONTHS(" + p_date1 + "," + cnt + " * 3)";
                    break;
                case LogicalExpressionParser.MONTH:
                    result = "ADD_MONTHS(" + p_date1 + "," + cnt + ")";
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "ADD_DAYS(" + p_date1 + "," + cnt + " * 7)";
                    break;
                case LogicalExpressionParser.DAY:
                    result = "ADD_DAYS(" + p_date1 + "," + cnt + ")";
                    break;
                case LogicalExpressionParser.HOUR:
                    result = "ADD_SECONDS(" + p_date1 + ",(" + cnt + "*60*60))";
                    break;
                case LogicalExpressionParser.MINUTE:
                    result = "ADD_SECONDS(" + p_date1 + ",(" + cnt + "*60))";
                    break;
                case LogicalExpressionParser.SECOND:
                    result = "ADD_SECONDS(" + p_date1 + ",(" + cnt + "))";
                    break;
            }
            return result;
        }

        public static string getOracleDateADD(int type, string cnt, string p_date1)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    result = "ADD_MONTHS(" + p_date1 + "," + cnt + " * 12)";
                    break;
                case LogicalExpressionParser.QUARTER:
                    result = "ADD_MONTHS(" + p_date1 + "," + cnt + " * 3)";
                    break;
                case LogicalExpressionParser.MONTH:
                    result = "ADD_MONTHS(" + p_date1 + "," + cnt + ")";
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "(" + p_date1 + "+" + cnt + " * 7)";
                    break;
                case LogicalExpressionParser.DAY:
                    result = "(" + p_date1 + "+" + cnt + ")";
                    break;
                case LogicalExpressionParser.HOUR:
                    result = "(" + p_date1 + "+" + cnt + "/24)";
                    break;
                case LogicalExpressionParser.MINUTE:
                    result = "(" + p_date1 + "+" + cnt + "/(24*60))";
                    break;
                case LogicalExpressionParser.SECOND:
                    result = "(" + p_date1 + "+" + cnt + "/(24*60*60))";
                    break;
            }
            return result;
        }

        public static string getOracleDatePart(int type, string p_date1)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    result = "TO_CHAR(" + p_date1 + ",'YYYY')";
                    break;
                case LogicalExpressionParser.QUARTER:
                    result = "CEIL(TO_CHAR(" + p_date1 + ",'MM')/3)";
                    break;
                case LogicalExpressionParser.MONTH:
                    result = "TO_CHAR(" + p_date1 + ",'MM')";
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "TO_CHAR(" + p_date1 + ",'WW')";
                    break;
                case LogicalExpressionParser.DAY:
                    result = "TO_CHAR(" + p_date1 + ",'DD')";
                    break;
                case LogicalExpressionParser.HOUR:
                    result = "TO_CHAR(" + p_date1 + ",'HH')";
                    break;
                case LogicalExpressionParser.MINUTE:
                    result = "TO_CHAR(" + p_date1 + ",'MI')";
                    break;
                case LogicalExpressionParser.SECOND:
                    result = "TO_CHAR(" + p_date1 + ",'SS')";
                    break;
            }
            return result;
        }

        public static string getHanaDatePart(int type, string p_date1)
        {
            string result = "";
            switch (type)
            {
                case LogicalExpressionParser.YEAR:
                    result = "YEAR(" + p_date1 + ")";
                    break;
                case LogicalExpressionParser.QUARTER:
                    result = "CEIL(TO_CHAR(" + p_date1 + ",'MM')/3)"; //Quarter for HANA returns string value hence using the oracle alternative
                    break;
                case LogicalExpressionParser.MONTH:
                    result = "MONTH(" + p_date1 + ")";
                    break;
                case LogicalExpressionParser.WEEK:
                    result = "TO_NUMBER(SUBSTRING(ISOWEEK(" + p_date1 + "),7))"; //Week for HANA starts on Monday and was returning different values
                    break;
                case LogicalExpressionParser.DAY:
                    result = "TO_CHAR(" + p_date1 + ",'DD')"; //Day function does not exist HANA hence using the oracle alternative
                    break;
                case LogicalExpressionParser.HOUR:
                    result = "HOUR(" + p_date1 + ",'HH')";
                    break;
                case LogicalExpressionParser.MINUTE:
                    result = "MINUTE(" + p_date1 + ",'MI')";
                    break;
                case LogicalExpressionParser.SECOND:
                    result = "SECOND(" + p_date1 + ",'SS')";
                    break;
            }
            return result;
        }

        private bool supportsRowNum()
        {
            return (sqlGen == SQL_GEN_ORACLE);
        }

        private bool supportsLimit()
        {
            return (sqlGen == SQL_GEN_MYSQL || sqlGen == SQL_GEN_PARACCEL || sqlGen == SQL_GEN_BIRST);
        }

        private bool supportsTOP()
        {
            return (sqlGen == SQL_GEN_DEFAULT);
        }
    }
}
