grammar LogicalExpression;

options 
{
	output=AST;
	ASTLabelType=CommonTree;
	language=CSharp3;
}

tokens
{
	NEGATE;
	STRING;
	CONDITIONAL;
	CONDITION;
	PAREN;
	CAST;
	PROJECTIONLIST;
}

@rulecatch
{
catch(Exception e)
{
throw (e);
}
}

@members {
	int nesting = 0;
	int MAX_NESTING = 600;

	new void PopFollow()
	{
		base.PopFollow();
		nesting--;
	}

	new void PushFollow(BitSet fset)
	{
		if (++nesting > MAX_NESTING)
			throw new RecognitionException("Syntax Error or Too Many Levels of Nesting");
		base.PushFollow(fset);
	}
}

@header {
using System;
using System.Text;
using System.Collections;
}

@lexer::members
{

List<RecognitionException> exceptions = new List<RecognitionException>();

public override void ReportError(RecognitionException e)
{
    exceptions.Add(e);
}

public bool HasError
{
	get { return exceptions.Count > 0; }
}

public string ErrorMessage
{
	get { return this.GetErrorMessage(exceptions[0] as RecognitionException, this.TokenNames); }
}

public string ErrorPosition
{
	get { return this.GetErrorHeader(exceptions[0] as RecognitionException); }
}

class DFA : Antlr.Runtime.DFA
{

   public override int Predict(IIntStream input)
   {
	            int mark = input.Mark(); // remember where decision started in input
	            int s = 0; // we always start at s0
	            int lastIndex = 0;
	        	int currentIndex = 0;
	        	int iterationCounter = 0;
	        	const int MAX_ITERATIONS_ALLOWED = 1000;
	            try
	            {
	                for (; ; )
	                {
	                    int specialState = special[s];
	                    lastIndex = input.Index;
	                    if (specialState >= 0)
	                    {
	                        s = SpecialStateTransition(this, specialState, input);
	                        if (s == -1)
	                        {
	                            NoViableAlt(s, input);
	                            return 0;
	                        }
	                        input.Consume();
	                        continue;
	                    }
	                    if (accept[s] >= 1)
	                    {
	                        return accept[s];
	                    }
	                    // look for a normal char transition
	                    char c = (char)input.LA(1); // -1 == \uFFFF, all tokens fit in 65000 space
	                    if (c >= min[s] && c <= max[s])
	                    {
	                        int snext = transition[s][c - min[s]]; // move to next state
	                        if (snext < 0)
	                        {
	                            // was in range but not a normal transition
	                            // must check EOT, which is like the else clause.
	                            // eot[s]>=0 indicates that an EOT edge goes to another
	                            // state.
	                            if (eot[s] >= 0)
	                            {  // EOT Transition to accept state?
	                                if (debug)
	                                   // Console.Error.WriteLine("EOT transition");
	                                s = eot[s];
	                                input.Consume();
	                                // TODO: I had this as return accept[eot[s]]
	                                // which assumed here that the EOT edge always
	                                // went to an accept...faster to do this, but
	                                // what about predicated edges coming from EOT
	                                // target?
	                                continue;
	                            }
	                            NoViableAlt(s, input);
	                            return 0;
	                        }
	                        s = snext;
	                        input.Consume();
	                        /* Allow 1000 iterations on the same character then exit */
	                        currentIndex = input.Index;
	                        if (currentIndex == lastIndex)
	                        {
	                            iterationCounter++;
	                            if (iterationCounter > MAX_ITERATIONS_ALLOWED)
	                            {
	                                NoViableAlt(s, input);
	                                return 0;
	                            }
	                        }
	                        else
	                        {
	                            iterationCounter = 0;
	                        }
	                        continue;
	                    }
	                    if (eot[s] >= 0)
	                    {  // EOT Transition?
	                        s = eot[s];
	                        input.Consume();
	                        continue;
	                    }
	                    if (c == unchecked((char)TokenTypes.EndOfFile) && eof[s] >= 0)
	                    {  // EOF Transition to accept state?
	                        return accept[eof[s]];
	                    }
	                    // not in range and not EOF/EOT, must be invalid symbol
	                    NoViableAlt(s, input);
	                    return 0;
	                }
	            }
	            finally
	            {
	                input.Rewind(mark);
	            }
	     }
	}

}

public query	:	selectquery | deletequery | updatequery;

selectquery
	:	SELECT projectionlist from where? orderby? EOF -> ^(SELECT projectionlist from where? orderby?);
	
deletequery
	:	DELETE from where? EOF -> ^(DELETE from where?);

updatequery
	:	UPDATE setlist from where? EOF -> ^(UPDATE setlist from where?);

from	:	FROM! (LEVEL | LEVEL_SET | IDENT_NAME);

where	:	WHERE logicalExpression -> ^(WHERE logicalExpression);

orderby	:	ORDERBY projectionlist direction? -> ^(ORDERBY projectionlist direction?);

direction
	:	ASCENDING | DESCENDING;

projectionlist
	:	projection (',' projection)* -> ^(PROJECTIONLIST projection*);

setlist
	:	logicalExpression (',' logicalExpression)* -> ^(PROJECTIONLIST logicalExpression*);

projection
	:	ident | 
	aggrule '(' ident ')' -> ^(aggrule ident);
	
ident	:	IDENT_NAME | LEVEL;

aggrule	:	SUM | AVG | COUNT | MIN | MAX;

public expression
	: 	logicalExpression EOF!; 
		
logicalExpression
	:	booleanAndExpression (OR^ booleanAndExpression )* ;

OR 	: 	'||' | (('O'|'o') ('R'|'r'));
	
booleanAndExpression
	:	equalityExpression (AND^ equalityExpression)* ;

AND 	: 	'&&' | (('A'|'a') ('N'|'n') ('D'|'d'));

equalityExpression
	:	nullExpression ((EQUALS|NOTEQUALS)^ nullExpression)*;

EQUALS	
	:	'=' | '==';
NOTEQUALS 
	:	'!=' | '<>';

nullExpression
	:	relationalExpression ((ISNULL|ISNOTNULL)^)?;

ISNULL:
	('I'|'i') ('S'|'s') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
ISNOTNULL
	:
	('I'|'i') ('S'|'s') WS ('N'|'n') ('O'|'o') ('T'|'t') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');

relationalExpression
	:	additiveExpression ( (LT|LTEQ|GT|GTEQ)^ additiveExpression )*;

LT	:	'<';
LTEQ	:	'<=';
GT	:	'>';
GTEQ	:	'>=';

additiveExpression
	:	multiplicativeExpression ( (PLUS|MINUS)^ multiplicativeExpression )*;

PLUS	:	'+';
MINUS	:	'-';

multiplicativeExpression 
	:	powerExpression ( (MULT|DIV|MOD|BITAND)^ powerExpression )*;
	
MULT	:	'*';
DIV	:	'/';
MOD	:	'%';

BITAND:	'&';

powerExpression 
	:	unaryExpression ( POW^ unaryExpression )*;
	
POW	:	'^';

unaryExpression
	:	primaryExpression
    	|	NOT^ primaryExpression
    	|	MINUS primaryExpression -> ^(NEGATE primaryExpression);
  
NOT	:	'!' | ('N'|'n') ('O'|'o') ('T'|'t');

primaryExpression
	:	'(' logicalExpression ')' -> ^(PAREN logicalExpression)
	|	cast
	|	function
	|	conditional
	|	value ;

value	
	: 	INTEGER
	|	FLOAT
	| 	DATETIME
	|	BOOLEAN
	|	STRING
	|	IDENT_NAME
	|	LEVEL
	|	NULL;

VARCHARTYPE	:	('V'|'v') ('A'|'a') ('R'|'r') ('C'|'c') ('H'|'h') ('A'|'a') ('R'|'r');
INTEGERTYPE	:	('I'|'i') ('N'|'n') ('T'|'t') ('E'|'e') ('G'|'g') ('E'|'e') ('R'|'r');
FLOATTYPE	:	('F'|'f') ('L'|'l') ('O'|'o') ('A'|'a') ('T'|'t');
DATETYPE	:	('D'|'a') ('A'|'a') ('T'|'t') ('E'|'e');
DATETIMETYPE	:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e');

STRING
    	:  	'\'' ( EscapeSequence | (options {greedy=false;} : ~('\u0000'..'\u001f' | '\\' | '\'' ) ) )* '\'';

INTEGER	
	:	('0'..'9')+;

FLOAT
	:	('0'..'9')* '.' ('0'..'9')+;

DATETIME
 	:	'#' (~'#')* '#' | (('N'|'n') ('O'|'o') ('W'|'w'));

BOOLEAN
	:	('T'|'t') ('R'|'r') ('U'|'u') ('E'|'e')
	|	('F'|'f') ('A'|'a') ('L'|'l') ('S'|'s') ('E'|'e');

NULL	:	('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');

SELECT	:	('S'|'s') ('E'|'e') ('L'|'l') ('E'|'e') ('C'|'c') ('T'|'t');
FROM	:	('F'|'f') ('R'|'r') ('O'|'o') ('M'|'m');
WHERE	:	('W'|'w') ('H'|'h') ('E'|'e') ('R'|'r') ('E'|'e');
ORDERBY	:	('O'|'o') ('R'|'r') ('D'|'d') ('E'|'e') ('R'|'r') ' ' ('B'|'b') ('Y'|'y');
DELETE	:	('D'|'d') ('E'|'e') ('L'|'l') ('E'|'e') ('T'|'t') ('E'|'e');
UPDATE	:	('U'|'u') ('P'|'p') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e');

SUM	:	('S'|'s') ('U'|'u') ('M'|'m');
AVG	:	('A'|'a') ('V'|'v') ('G'|'g');
COUNT	:	('C'|'c') ('O'|'o') ('U'|'u') ('N'|'n') ('T'|'t');
MIN	:	('M'|'m') ('I'|'i') ('N'|'n');
MAX	:	('M'|'m') ('A'|'a') ('X'|'x');

/*LEFT_PAREN: 	'(';
RIGHT_PAREN: 	')';*/
	
function
	:	datediff | datepart | dateadd | substring | length | position | toupper | tolower | getpromptvalue | getvariable | mathmethod;

datediff
	:	DATEDIFF^ '('! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression ')'!;
	
DATEDIFF:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('D'|'d') ('I'|'i') ('F'|'f') ('F'|'f');

dateadd
	:	DATEADD^ '('! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression ')'!;
	
DATEADD:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('A'|'a') ('D'|'d') ('D'|'d');

datepart
	:	DATEPART^ '('! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ')'!;
	
DATEPART:	 ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('P'|'p') ('A'|'a') ('R'|'r') ('T'|'t');

YEAR	:	('Y'|'y') ('E'|'e') ('A'|'a') ('R'|'r');	
QUARTER	:	('Q'|'q') ('U'|'u') ('A'|'a') ('R'|'r') ('T'|'t') ('E'|'e') ('R'|'r');
MONTH	:	('M'|'m') ('O'|'o') ('N'|'n') ('T'|'t') ('H'|'h');
WEEK	:	('W'|'w') ('E'|'e') ('E'|'e') ('K'|'k');
DAY	:	('D'|'d') ('A'|'a') ('Y'|'y');
	
HOUR	:	('H'|'h') ('O'|'o') ('U'|'u') ('R'|'r');	
MINUTE	:	('M'|'m') ('I'|'i') ('N'|'n') ('U'|'u') ('T'|'t') ('E'|'e');
SECOND	:	('S'|'s') ('E'|'e') ('C'|'c') ('O'|'o') ('N'|'n') ('D'|'d');

SIN	:	('S'|'s') ('I'|'i') ('N'|'n');
COS	:	('C'|'c') ('O'|'o') ('S'|'s');
TAN	:	('T'|'t') ('A'|'a') ('N'|'n');
ARCSIN	:	('A'|'a') ('R'|'r') ('C'|'c') ('S'|'s') ('I'|'i') ('N'|'n');
ARCCOS	:	('A'|'a') ('R'|'r') ('C'|'c') ('C'|'c') ('O'|'o') ('S'|'s');
ARCTAN	:	('A'|'a') ('R'|'r') ('C'|'c') ('T'|'t') ('A'|'a') ('N'|'n');
ABS	:	('A'|'a') ('B'|'b') ('S'|'s');
CEILING	:	('C'|'c') ('E'|'e') ('I'|'i') ('L'|'l') ('I'|'i') ('N'|'n') ('G'|'g');
FLOOR	:	('F'|'f') ('L'|'l') ('O'|'o') ('O'|'o') ('R'|'r');
SQRT	:	('S'|'s') ('Q'|'q') ('R'|'r') ('T'|'t');
LN	:	('L'|'l') ('N'|'n');
LOG	:	('L'|'l') ('O'|'o') ('G'|'g');
EXP	:	('E'|'e') ('X'|'x') ('P'|'p');
POWER	:	('P'|'p') ('O'|'o') ('W'|'w');

mathmethod
	:	sin | cos | tan | arcsin | arccos | arctan | abs | ceiling | floor | squareroot | ln | log | exp | power;

sin	:	SIN^ '('! logicalExpression ')'!;
cos	:	COS^ '('! logicalExpression ')'!;
tan	:	TAN^ '('! logicalExpression ')'!;
arcsin	:	ARCSIN^ '('! logicalExpression ')'!;
arccos	:	ARCCOS^ '('! logicalExpression ')'!;
arctan	:	ARCTAN^ '('! logicalExpression ')'!;
abs	:	ABS^ '('! logicalExpression ')'!;
ceiling	:	CEILING^ '('! logicalExpression ')'!;
floor	:	FLOOR^ '('! logicalExpression ')'!;
squareroot	:	SQRT^ '('! logicalExpression ')'!;
ln	:	LN^ '('! logicalExpression ')'!;
log	:	LOG^ '('! logicalExpression ')'!;
exp	:	EXP^ '('! logicalExpression ')'!;
power	:	POWER^ '('! logicalExpression ','! logicalExpression ')'!;
	
substring
	:	SUBSTR^ '('! logicalExpression ','! logicalExpression (','! logicalExpression)? ')'!;
	
SUBSTR:	 	('S'|'s') ('U'|'u') ('B'|'b') ('S'|'s') ('T'|'t') ('R'|'r') ('I'|'i') ('N'|'n') ('G'|'g');

length	:	LENGTH^ '('! logicalExpression ')'!;
	
LENGTH:	 	('L'|'l') ('E'|'e') ('N'|'n') ('G'|'g') ('T'|'t') ('H'|'h');

position
	:	POSITION^ '('! logicalExpression ','! logicalExpression ')'!;
	
TOUPPER:	('T'|'t') ('O'|'o') ('U'|'u') ('P'|'p') ('P'|'p') ('E'|'e') ('R'|'r');
TOLOWER:	('T'|'t') ('O'|'o') ('L'|'l') ('O'|'o') ('W'|'w') ('E'|'e') ('R'|'r');

toupper:	TOUPPER^ '('! logicalExpression ')'!;

tolower:	TOLOWER^ '('! logicalExpression ')'!;	
	
getpromptvalue
	:	GETPROMPTVALUE^ '('! STRING ')'!;
	
GETPROMPTVALUE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('P'|'p') ('R'|'r') ('O'|'o') ('M'|'m') ('P'|'p') ('T'|'t') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');
	
getvariable
	:	GETVARIABLE^ '('! STRING ')'!;
	
GETVARIABLE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('V'|'v') ('A'|'a') ('R'|'r') ('I'|'i') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
	
POSITION:	('P'|'p') ('O'|'o') ('S'|'s') ('I'|'i') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');

fragment IDENT
	:	('a'..'z' | 'A'..'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ';' | '>' | '<' | '0'..'9' | '\u0080'..'\uFFFF')+;
	
IDENT_NAME
	:	'[' IDENT ']';
	
LEVEL
	:	'[' IDENT '.' IDENT ']';

LEVEL_SET
	:	'[' IDENT '.' IDENT (',' IDENT '.' IDENT)+ ']';

conditional
	:	condition+ (ELSE logicalExpression)? END -> ^(CONDITIONAL condition* logicalExpression?);
	
condition
	:	IF logicalExpression THEN logicalExpression -> ^(CONDITION logicalExpression logicalExpression);
	
IF	:	('I'|'i') ('F'|'f');
THEN	:	('T'|'t') ('H'|'h') ('E'|'e') ('N'|'n');
ELSE	:	('E'|'e') ('L'|'l') ('S'|'s') ('E'|'e');
END	:	('E'|'e') ('N'|'n') ('D'|'d');

cast	:	INTEGERTYPE '(' logicalExpression ')' -> ^(CAST INTEGERTYPE logicalExpression) |
		FLOATTYPE '(' logicalExpression ')' -> ^(CAST FLOATTYPE logicalExpression) |
		DATETIMETYPE '(' logicalExpression ')' -> ^(CAST DATETIMETYPE logicalExpression) |
		DATETYPE '(' logicalExpression ')' -> ^(CAST DATETYPE logicalExpression) |
		VARCHARTYPE '(' logicalExpression ')' -> ^(CAST VARCHARTYPE logicalExpression);

ASCENDING
	:	('A'|'a') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;
	
DESCENDING
	:	('D'|'d') ('E'|'e') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;

fragment EscapeSequence 
	:	'\\'
  	(	
  		'n' 
	|	'r' 
	|	't'
	|	'\'' 
	|	'\\'
	|	UnicodeEscape
	);

fragment UnicodeEscape
    	:    	'u' HexDigit HexDigit HexDigit HexDigit ;

fragment HexDigit 
	: 	('0'..'9'|'a'..'f'|'A'..'F') ;

/* Ignore white spaces */	
WS	
	:  (' '|'\r'|'\t'|'\u000C'|'\n') {$channel=Hidden;};
