﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    class InvalidQueryException : Exception
    {
        public string name;

        public InvalidQueryException(string name)
        {
            this.name = name;
        }
    }
}
