﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    class UnrecognizedLogicalColumnException : Exception
    {
        public string name;

        public UnrecognizedLogicalColumnException(string name)
        {
            this.name = name;
        }
    }
}
