﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    class UnrecognizedTableException : Exception
    {
        public string name;

        public UnrecognizedTableException(string name)
        {
            this.name = name;
        }
    }
}
