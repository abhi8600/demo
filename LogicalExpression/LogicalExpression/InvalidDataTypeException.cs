﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParser
{
    class InvalidDataTypeException : Exception
    {
        public string name;

        public InvalidDataTypeException(string name)
        {
            this.name = name;
        }
    }
}
