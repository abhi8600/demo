﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExpressionParser;

namespace LogicalExpressionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> columnMap = new Dictionary<string, string>();
            Dictionary<string, LogicalExpression.DataType> typeMap = 
                new Dictionary<string, LogicalExpression.DataType>();
            columnMap.Add("datecolumn", "TABLE.dim$column");
            typeMap.Add("datecolumn", new LogicalExpression.DataType("Date", 20));
            columnMap.Add("string-/column", "TABLE.dim$column");
            typeMap.Add("string-/column", new LogicalExpression.DataType("Varchar", 20));
            columnMap.Add("stringcolumn", "TABLE.dim$column");
            typeMap.Add("stringcolumn", new LogicalExpression.DataType("Varchar", 20));
            columnMap.Add("intcolumn", "TABLE.dim$intcolumn");
            typeMap.Add("intcolumn", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("Order_Details.CustomerID", "TABLE.dim$cid");
            typeMap.Add("Order_Details.CustomerID", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("Time.Day ID", "TABLE.dim$did");
            typeMap.Add("Time.Day ID", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("Order_Details.Customers1200947", "TABLE.key");
            typeMap.Add("Order_Details.Customers1200947", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("CustomerID", "TABLE.cid");
            typeMap.Add("CustomerID", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("OrderID", "TABLE.oid");
            typeMap.Add("OrderID", new LogicalExpression.DataType("Integer", 20));
            columnMap.Add("OrderDate", "TABLE.od");
            typeMap.Add("OrderDate", new LogicalExpression.DataType("DateTime", 0));
            columnMap.Add("CategoryName", "TABLE.cn");
            typeMap.Add("CategoryName", new LogicalExpression.DataType("Varchar", 20));
            //LogicalExpression le = new LogicalExpression("IF (1=1 OR 3=3) AND 7=7 THEN (1+3.0)*4/(DATEDIFF(hour,[datecolumn],#1/2/2002#)) IF 2=2 THEN 4 ELSE 5 END", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("Substring([stringcolumn],1,2)", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("Length([stringcolumn])-Position('@',[stringcolumn])-1", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("[string-/column]+' '+[string-/column]+' '+[intcolumn]", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("IF [intcolumn]>0 THEN 'test' IF [intcolumn]=0 THEN 'test2' END", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("IF [intcolumn] IS NULL THEN 'test' IF [intcolumn]=0 THEN 'test2' END", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("IF [datecolumn] IS NULL THEN 'N' ELSE NULL END", columnMap, typeMap, LogicalExpression.SQL_GEN_MYSQL, false);
            //LogicalExpression le = new LogicalExpression("IF [datecolumn] IS NULL THEN 1 ELSE 0 END", columnMap, typeMap, LogicalExpression.SQL_GEN_MYSQL, false);
            //LogicalExpression le = new LogicalExpression("(If [datecolumn]<=GetPromptValue('End Date') And ([datecolumn]>GetPromptValue('End Date')   Or [datecolumn] Is Null) And [stringcolumn]='Open'  And [datecolumn]>=GetPromptValue('Period Start Date') And [datecolumn]<DateAdd(Day,1,GetPromptValue('Period End Date')) Then [intcolumn] Else 0 End)-(If [stringcolumn]='Won' And [datecolumn]>=GetPromptValue('Period Start Date') And [datecolumn]<DateAdd(Day,1,GetPromptValue('Period End Date'))  And ([datecolumn]>=GetPromptValue('Period Start Date') And [datecolumn]<DateAdd(Day,1,GetPromptValue('Period End Date'))) And [datecolumn]>=GetPromptValue('Start Date') And [datecolumn]<GetPromptValue('End Date') And ([datecolumn]>=GetPromptValue('Start Date') Or [datecolumn] Is Null) Then [intcolumn] Else 0 End)", columnMap, typeMap, LogicalExpression.SQL_GEN_MYSQL, false);
            //LogicalExpression le = new LogicalExpression("(If [datecolumn]<=GetPromptValue('End Date') And ([datecolumn]>GetPromptValue('End Date')   Or [datecolumn] Is Null) And [stringcolumn]='Open'  And [datecolumn]>=GetPromptValue('Period Start Date') And [datecolumn]<DateAdd(Day,1,GetPromptValue('Period End Date')) Then [intcolumn] Else 0 End)", columnMap, typeMap, LogicalExpression.SQL_GEN_MYSQL, false);
            //LogicalExpression le = new LogicalExpression("DATEPART(week,[datecolumn])%10", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("DATEDIFF(day,#1/1/2009#,Now)", columnMap, typeMap, LogicalExpression.SQL_GEN_DEFAULT);
            //LogicalExpression le = new LogicalExpression("5+3 AND 2+1", columnMap, typeMap);
            //LogicalExpression le = new LogicalExpression("Select [stringcolumn],SUM([intcolumn]) from [test]", "test", "dbo.testtable", columnMap, typeMap, -1);
            //LogicalExpression le = new LogicalExpression("Select [stringcolumn],SUM([intcolumn]) from [test] where [stringcolumn]+1", "test", "dbo.testtable", columnMap, typeMap, -1);
            //LogicalExpression le = new LogicalExpression("Select [stringcolumn],SUM([intcolumn]) from [test] where [stringcolumn]='test' order by [stringcolumn]", "test", "dbo.testtable", columnMap, typeMap, 100, -1, true);
            //LogicalExpression le = new LogicalExpression("Select [stringcolumn],SUM([intcolumn]) from [test.test] where [stringcolumn]='test' order by [stringcolumn] Desc", "test.test", "dbo.testtable", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select [stringcolumn],SUM([intcolumn]) from [test.test] order by SUM([intcolumn]) Desc", "test.test", "dbo.testtable", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select [Order_Details.CustomerID] from [Order_Details.Customers,Time.Day]", "Order_Details.Customers,Time.Day", "dbo.testtable", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select [Time.Day ID],Count([Order_Details.Customers1200947]) from [Order_Details.Customers,Time.Day]", "Order_Details.Customers,Time.Day", "dbo.testtable", columnMap, typeMap, 100);            
            //LogicalExpression le = new LogicalExpression("Select [CustomerID],COUNT([OrderID]) FROM [Order_Details.Orders] WHERE [OrderDate]<#1/1/1995#", "Order_Details.Orders", "dbo.orders", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select COUNT([OrderID]) FROM [Order_Details.Orders]", "Order_Details.Orders", "dbo.orders", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select [CustomerID],COUNT([OrderID]) FROM [Order_Details.Orders] WHERE Count([OrderID])>1", "Order_Details.Orders", "dbo.orders", columnMap, typeMap, 100);
            //LogicalExpression le = new LogicalExpression("Select [Order_Details.CustomerID] FROM [Order_Details.Customers,Time.Day] where [Order_Details.CustomerID]='ANATR'", "Order_Details.Customers,Time.Day", "dbo.dw_sf_customers.day", columnMap, typeMap, 100, -1, true);
            //LogicalExpression le = new LogicalExpression("Update [CategoryName]='test' FROM [Categories] where [CategoryName]='Beverages'", "Categories", "ST_Categories", columnMap, typeMap, 100, -1, true);
            //LogicalExpression le = new LogicalExpression("Select [Categories.CategoryID] FROM [Categories.Categories, Time.Day]", "Categories", "ST_Categories", columnMap, typeMap, 100, -1, true);
            LogicalExpression le = new LogicalExpression("Select [Categories.Categories-1697024394],[Categories.CategoryID],[Time.Day ID],[Time.Week ID],[Time.Month ID],[Time.Quarter ID],[# CategoryID],[# Distinct CategoryID] FROM [Categories.Categories,Time.Day]", "Categories", "ST_Categories", columnMap, typeMap, 100, -1, true);
            if (le.HasError)
                Console.WriteLine(le.Error);
            else
                Console.WriteLine(le.GeneratedCode+":"+le.Type);
            Console.Read();
        }
    }
}
