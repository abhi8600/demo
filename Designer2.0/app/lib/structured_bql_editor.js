(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function($) {

    function autocomplete(completionsFor, events, initialString, placeholder) {
        initialString = initialString || "";
        var container = $("<div>");
        var topInput = $("<input>").attr('type', 'text').attr('placeholder', placeholder).val(initialString).appendTo(container);
        setTimeout(function() {
            topInput.focus();
        }, 0);
        var completionsContainer = $("<div>").addClass('completionsContainer').appendTo(container);

        var completions = completionsFor(initialString);

        completionsContainer.on('click', function (ev) {
            ev.stopPropagation();
        });

        $(document).on('click', function (ev) {
            completionsContainer.hide();
        });

        topInput.on('click', function (ev) {
            ev.stopPropagation();
        });

        topInput.on('focus', function() {
            completionsContainer.show();
        });

        topInput.on('keydown', function(ev) {
            var key = ev.which;
            if (key === 27) events.escape();
            if (key === 8) return backspace();
            if (key === 40) {
                selectDown();
                return false;
            }
            if (key === 38) {
                selectUp();
                return false;
            }
            if (key === 32) {
                var completionsAfterSpace = completionsFor(topInput.val() + " ");
                if (completions.length === 1 && completionsAfterSpace.length === 0) {
                    enter();
                }
            }
            if(key === 39 && topInput[0].selectionStart == topInput.val().length && topInput[0].selectionEnd == topInput.val().length) { 
                events.onRight();
                return false;
            }
            if(key === 37 && topInput[0].selectionStart == 0 && topInput[0].selectionEnd == 0) {
                events.onLeft();
                return false;
            }
            if (key === 13) enter();
            if (key === 93) console.log('getPosition() =', getPosition());
        });

        topInput.on('keypress', function(ev) {
            // Let the keypress be handled by the browser, then update based on new text
            setTimeout(updateText, 0);
        });

        updateText();

        return container;

        function getPosition() {
            // This function replaces the previous `position` variable, and it is needed
            // to unify both mouse and keyboard interactions with completions.
            var pos = -1;
            while (++pos < completions.length) {
                if (completions[pos].elem.css('background-color') !== 'rgb(255, 255, 255)') { return pos; }
            }
            return -1; // No highlights found
        }

        function updateText() {
            completionsContainer.empty();
            completions = completionsFor(topInput.val());
            if (completions.length === 0) {
                completionsContainer.append($("<span>").text("No completions"));
            } else {
                completions.map(function(completion) {
                    completion.highlight(topInput.val());
                    completion.elem.on('mousedown', function() {
                        events.finish(completion.value);
                    }).appendTo(completionsContainer);
                });
            }
        }

        function scrollToPosition() {
            var currPosition = getPosition();
            var containerHeight = completionsContainer.height();
            var elemOffset = completions[currPosition].elem.position().top;
            var elemHeight = completions[currPosition].elem.height();
            var diff = 0;
            if (elemOffset < 0) {
                diff = elemOffset;
            }
            if (elemOffset + elemHeight > containerHeight) {
                diff = 20 + elemOffset + elemHeight - containerHeight;
            }
            completionsContainer[0].scrollTop += diff;
        }

        function selectDown() {
            var currPosition = getPosition();
            if (currPosition === completions.length - 1) return;
            if (currPosition !== -1) completions[currPosition].selected(false);
            completions[currPosition+1].selected(true);
            scrollToPosition();
        }

        function selectUp() {
            var currPosition = getPosition();
            if (currPosition === -1) return;
            completions[currPosition].selected(false);
            if (currPosition === 0) return;
            completions[currPosition-1].selected(true);
            scrollToPosition();
        }

        function enter() {
            var currPosition = getPosition();
            if (completions.length === 0) return;
            if (completions.length === 1) return events.finish(completions[0].value);
            if (currPosition > -1) return events.finish(completions[currPosition].value);;
            return events.finish(completions[0].value);
        }

        function backspace() {
            if (topInput.val() === "") {
                if(events.back) {
                    events.back();
                }
                return false;
            }

            // Let the backspace be handled by the browser, then update based on new text
            setTimeout(updateText, 0);
            return true;
        }

    }


    function simpleCompletion(val) {
        return {
            elem: $("<div>").css({
                width: '100%',
                lineHeight: '30px'
            }).text(val),
            selected: function(isSelected) {
                this.elem.css('font-weight', isSelected ? 'bold' : 'normal');
            },
            value: val
        };
    }


    function testCompletionsFor(str) {
        var vals = ["foo", "bar", "baz"];
        var ret = [];
        vals.map(function(val) {
            if (val.indexOf(str) > -1) {
                ret.push(simpleCompletion(val));
            }
        });
        return ret;
    }


    return autocomplete;
}

},{}],2:[function(require,module,exports){
module.exports = function($) {

    function Completion(elem, highlight, selected, value, allowSpace) {
        // selected should be a method, called by completion.selected(boolean)
        this.elem = elem;
        this.value = value;
        this.selected = selected;
        this.highlight = highlight;
        this.allowSpace = allowSpace;
    }

    function stringCompletion(completion) {
        var elem = $("<div>").addClass('completion').addClass(completion.className);
        var icon = $('<i>').addClass(completion.type + '-icon');
        if (completion.className === 'operator-completion') {
            icon.addClass(completion.nickname);
        }
        elem.append(icon);
        var wordSpan = $("<span>").text(completion.string).appendTo(elem);
        
        var typeAnnotation = $("<div>");
        typeAnnotation.text(completion.type);
        elem.append(typeAnnotation);
        
        var newCompletion = new Completion(
            elem,
            function(str) {
                if(str.length === 0) {
                    wordSpan.empty().text(completion.string);
                } else {
                    wordSpan.empty();
                    boldMatches(wordSpan, completion.string, str);
                }
            },
            function(isSelected) {
                elem.css('font-weight', isSelected ? 'bold' : 'normal');
                elem.css('background-color', isSelected ? '#ebf8ff' : '#fff');
            },
            completion,
            completion.allowSpace
        );

        elem.on('mouseenter', function () {
            $('.completion').css('font-weight', 'normal').css('background-color', '#fff');
            newCompletion.selected(true);
        });

        return newCompletion;
    }

    function boldMatches(span, full, part) {
        var idx = -1, match;
        var lowerFull = full.toLowerCase(), lowerPart = part.toLowerCase();
        while((match = lowerFull.indexOf(lowerPart, idx)) > -1) {
            span[0].appendChild(document.createTextNode(full.substring(idx, match)));
            span.append($("<b>").text(full.substring(match, match + part.length)));
            idx = match + part.length;
        }
        span[0].appendChild(document.createTextNode(full.substring(idx)));
    }

    return {
        Completion: Completion,
        stringCompletion: stringCompletion
    };
};

},{}],3:[function(require,module,exports){
module.exports = function($) {
    var C = require("./completion")($);

    return {
        autocomplete: require("./autocomplete")($),
        Completion: C.Completion,
        stringCompletion: C.stringCompletion
    };
}

},{"./autocomplete":1,"./completion":2}],4:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {

    var Fns = require("./functions");

    function placeholderFor(arg) {
        if(arg.length) {
            return "Argument";
        }
        switch(arg) {
        case "int":
            return "Number";
        case "float":
            return "Number";
        case "string":
            return "String";
        case "bool":
            return "Condition";
        case "date":
            return "Date";
        case "datetime":
            return "Date and Time";
        case "datepart":
            return "Date Component";
        case "datetimepart":
            return "Date and Time Component";
        default:
            throw "Unexpected arg type: \"" + arg + "\"";
        }
    }

    function functionProductions() {
        var functionsBySignature = {};
        Fns.functions.map(function(fn) {
            var expTypes = ["int", "float", "string", "bool", "date", "datetime"];
            var signature = fn.args.map(function(arg) {
                if (expTypes.indexOf(arg) > -1 || arg.every && arg.every(function(it) {
                    return expTypes.indexOf(it) > -1;
                })) {
                    return "expression";
                } else if (arg === "datepart" || arg === "datetimepart") {
                    return arg;
                } else {
                    throw "Unexpected arg type: \"" + arg + "\"";
                }
            });
            var sigString = JSON.stringify(signature);
            if (!(sigString in functionsBySignature)) {
                functionsBySignature[sigString] = [];
            }
            functionsBySignature[sigString].push(fn);
        });

        return Object.keys(functionsBySignature).map(function(sigString) {
            var fns = functionsBySignature[sigString];
            var signature = JSON.parse(sigString);
            var nameToken = G.listToken(
                fns.map(function(fn) {
                    return {
                        type: 'function',
                        className: 'function-completion',
                        value: fn.label,
                        string: fn.label
                    };
                }), 2
            );
            var components = [nameToken, G.string("(")];
            for (var ii = 0; ii < signature.length; ii++) {
                if (ii > 0) components.push(G.string(","));
                var component = signature[ii] === "expression" && bql.expression || signature[ii] === "datepart" && bql.datepart || signature[ii] === "datetimepart" && bql.datetimepart;

                components.push(component);
            }
            components.push(G.string(")"));
            return new G.Production(components, function() {
                var children = [].slice.call(arguments, 0);
                var fn = children.shift();
                // remove parens
                children.shift();
                children.pop();
                // remove commas
                children = children.filter(function(child, ii) {
                    return ii % 2 === 0;
                });

                return new BqlNode({
                    type: "function",
                    value: fn.value
                }, children);
            }, function(children) {
                var fnName = children[0].value;

                var fn = Fns.functions.filter(function(fn) { return fn.label === fnName.value; })[0];
                var placeholders = [null, null]; // fnName itself and '('
                fn.args.map(function(arg, ii) {
                    if(ii !== 0) placeholders.push(null) // ','
                    
                    if(fn.placeholders) {
                        placeholders.push(fn.placeholders[ii]);
                    } else {
                        placeholders.push(placeholderFor(arg));
                    }
                });
                placeholders.push(null); // ')'
                return placeholders;
            });
        });
    }

    var operator = G.listToken(
        Fns.operators.map(function(op) {
            return {
                type: 'operator',
                className: 'operator-completion',
                string: op.label,
                value: op.label,
                nickname: op.nickname
            };
        }), 5);

    var statToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "Stat",
        string: "Stat"
    }], 2);
    var paramListOrQuery = new G.NonTerminal();
    paramListOrQuery.initialize([
        new G.Production([bql.query], function(query) {
            return {
                query: query,
                params: []
            };
        }, function() { return ["Query"] }),
        new G.Production([bql.expression, G.string(","), paramListOrQuery], function(exp, _, queryParams) {
            return {
                query: queryParams.query,
                params: [exp].concat(queryParams.params)
            };
        }, function() { return ["Parameter", null, "Parameters and Query"]; })
    ], true);
    var statProduction = new G.Production([
        statToken,
        G.string("("), bql.statType,
        G.string(","), bql['int'],
        G.string(","), paramListOrQuery,
        G.string(")")
    ], function(statFn, _, statType, _, index, _, queryParams) {
        var args = [statType, index].concat(queryParams.params).concat([queryParams.query]);
        return new BqlNode({
            type: 'function',
            value: 'stat'
        }, args);
    }, function() { 
        return [null, null, "Aggregation", null, "Column Index", null, "Parameters and Query"];
    });

    var medianToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "Median",
        string: "Median"
    }], 2);
    var rankBy = new G.NonTerminal([
        new G.Production([], function() { return []; }),
        new G.Production([G.string("by"), G.separatedList(bql.expression, G.string(","), false, function(vals, seps) { return vals; })], function(_, list) {
            return list;
        })
    ], true);
    var medianProduction = new G.Production([
        medianToken, 
        G.string("("), 
        bql.expression,
        rankBy,
        G.string(")")
    ], function(medianFn, _, arg, ranks, _) {
        if(ranks.length) {
            arg = BqlNode.fromRankBy(arg, ranks);
        } 
        return new BqlNode({
            type: 'function',
            value: 'median'
        }, [arg]);
    }, function() { return [null, null, "Argument", null, null]; });

    var rsumToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "RSum",
        string: "RSum"
    }], 2);
    var rsumProduction = new G.Production([
        rsumToken, 
        G.string("("), 
        bql['int'],
        G.string(","),
        bql.expression,
        rankBy,
        G.string(")")
    ], function(rsumFn, _, wind, _, arg, ranks, _) {
        if(ranks.length) {
            arg = BqlNode.fromRankBy(arg, ranks);
        } 
        return new BqlNode({
            type: 'function',
            value: 'rsum'
        }, [wind, arg]);
    }, function() { return [null, null, "Window", null, "Summand", null, null]; });

    bql.operator.initialize([new G.Production([operator], function(op) {
        return op;
    })], true);

    bql.bqlFunction.initialize(functionProductions().concat([medianProduction, rsumProduction /*, statProduction */]), true);
};

},{"./functions":9}],5:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var measures = subjectArea.measures;
    var attributes = subjectArea.attributes;

    function parseMeasureOrAttribute(str, idx) {
        var nextBra = str.indexOf("[", idx);
        if(nextBra == -1) return null;
        if(!str.substring(idx, nextBra).match(/^\s*$/)) return null;

        var nextKet = str.indexOf("]", nextBra);
        if(nextKet == -1) return null;

        var attribOrMeasureString = str.substring(nextBra+1, nextKet);
        return {string: attribOrMeasureString, index: nextKet+1};
    }

    var measure = G.listToken(

        measures.filter(function(measure) {
            return measure.type === 'measure';
        }).map(function(measure) {
            return {
                string: measure.label,
                type: 'measure',
                className: 'measure-completion',
                measure: measure,
                dateType: BqlNode.defaultDateType(measure),
                aggregation: BqlNode.defaultAggregation(measure),
                timeSeries: null
            };
        }), 4,
        function(str, idx) {
            var val = parseMeasureOrAttribute(str, idx);
            if(!val) return null;
            var measureParts = subjectArea.bqlMeasureMap[ val.string ];
            if(!measureParts) return null;

            var token = {
                string: measureParts.measure.label,
                type: 'measure',
                className: 'measure-completion',
                measure: measureParts.measure,
                dateType: measureParts.dateType,
                aggregation: measureParts.aggregation,
                timeSeries: measureParts.timeSeries
            };

            return {
                index: val.index,
                value: token
            };
        }
    );
    measure.customRenderer = function(node) {
        var span = $("<span>").css('position', 'relative').text(node.value.string);
        var popup;

        var aggregationsGroupContainer = $("<div>");
        if (node.value.measure.aggregations.length > 1) {
            aggregationsGroupContainer.addClass('custom-renderer').append($("<div>").addClass('title').text("Aggregation"));
            var aggregationsGroup = $('<div>').appendTo(aggregationsGroupContainer);
            node.value.measure.aggregations.map(function(agg) {
                aggregationsGroup.append($("<label>").append(
                    $("<input type=radio name=aggregationsRadio>").val(agg).attr('checked', agg === node.value.aggregation).on('change', function() {
                        node.value.aggregation = agg;
                    }),
                    $("<span>").text(agg)
                ));
            });
        }
        var dateTypeGroupContainer = $("<div>");
        var dateTypesWithoutLoadDate = node.value.measure.dateTypes.filter(function(type) {
            return type !== "By Load Date";
        });
        if (dateTypesWithoutLoadDate.length > 1) {
            dateTypeGroupContainer.addClass('custom-renderer').append($("<div>").addClass('title').text("Analyze By Date"));
            var dateTypeGroup = $('<div>').appendTo(dateTypeGroupContainer);
            dateTypesWithoutLoadDate.map(function(dt) {
                dateTypeGroup.append($("<label>").append(
                    $("<input type=radio name=dateTypeRadio>").val(dt).attr('checked', dt === node.value.dateType).on('change', function() {
                        node.value.dateType = dt;
                    }),
                    $("<span>").text(dt)
                ));
            });
        }

        var buttons = $("<div class='finish-container'>").append($('<div class="title">'), $("<button class='btn btn-primary'>").text('DONE').click(function() {
            popup.hide();
        }));

        var grammarWidth = function() {
            var width = 90;

            if (!aggregationsGroupContainer.is(':empty')) {
                width = width + 200;
            }
            if (!dateTypeGroupContainer.is(':empty')) {
                width = width + 200;
            }
            return width;
        };

        popup = $("<div>").addClass('grammar-popup')
            .append(
                aggregationsGroupContainer,
                dateTypeGroupContainer,
                buttons
            )
            .css('width', grammarWidth)
            .appendTo(span)
            .hide()
            .on('click', function(ev) {
                ev.stopPropagation();
            });

        span.append($("<span>").text(" "), $("<i>").addClass("down-arrow-icon").css({
            'cursor': 'pointer',
            margin: '0 0 0 4px',
            backgroundSize: 10
        }).click(function() {
            popup.show();
            return false;
        }));

        return span;
    };

    var attribute = G.listToken(
        attributes.map(function(attribute) {
            return {
                string: attribute.label,
                type: 'attribute',
                className: 'attribute-completion',
                attribute: attribute
            };
        }), 3,
        function(str, idx) {
            var val = parseMeasureOrAttribute(str, idx);
            if(!val) return null;
            
            var attribute = _.filter(subjectArea.attributes, function(attribute) {
                return _.last(attribute.path) === val.string;
            })[0];
            if(!attribute) return null;

            var token = {
                string: attribute.label,
                type: 'attribute',
                className: 'attribute-completion',
                attribute: attribute
            };
            return {index: val.index, value: token};
        }
    );

    bql.measure.initialize([new G.Production([measure], function(m) {
        return BqlNode.fromMeasure(m.measure, m.aggregation, m.dateType, m.timeSeries);
    })], true);

    bql.attribute.initialize([new G.Production([attribute], function(a) {
        return BqlNode.fromAttribute(a.attribute);
    })], true);
}

},{}],6:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var dateConstant = G.stringToken(
        function(str) {
            var match = str.match(/^#([^#]*)#?$/);
            if(!match) {
                return null;
            }

            var dateString = match[1];
            return {
                type: 'datetime',
                className: 'datetime-completion',
                string: '#' + dateString + '#',
                value: '#' + dateString + '#'
            };
        }, '##', 0,
        function(str, idx) {
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "#") return null;
            for(idx++; idx < str.length; idx++) {
                if(str.charAt(idx) === "#") {
                    var innerString = str.substring(start+1, idx);
                    var tok = {
                        type: 'datetime',
                        className: 'datetime-completion',
                        string: '#' + innerString + '#',
                        value: '#' + innerString + '#'
                    };
                    return {value: tok, index: idx+1};
                }
            }
            return null;
        }
    );
    var dateNow = G.listToken(
        ["NOW", "NOWDATE"].map(function(time) {
            return {
                string: time,
                type: 'datetime',
                className: 'datetime-completion',
                value: time
            };
        }), 0
    );

    bql.date.initialize([
        new G.Production([dateConstant], function(dt) {
            return BqlNode.fromDateTime(dt.value);
        }),
        new G.Production([dateNow], function(dt) {
            return BqlNode.fromDateTime(dt.value);
        })
    ], true);
}

},{}],7:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var dateToken = G.listToken(["Year", "Month", "Day"].map(function(part) {
        return {
            type: "datepart",
            className: 'datepart-completion',
            string: part,
            value: part
        };
    }), 1);

    var dateTimeToken = G.listToken(["Year", "Month", "Day", "Hour", "Minute", "Second"].map(function(part) {
        return {
            type: "datepart",
            className: 'datepart-completion datepart-time-completion',
            string: part,
            value: part
        };
    }), 1);

    bql.datepart.initialize([
        new G.Production([dateToken], function(it) {
            return new BqlNode({
                type: "datepart",
                value: it.value
            }, []);
        })
    ], true);

    bql.datetimepart.initialize([
        new G.Production([dateTimeToken], function(it) {
            return new BqlNode({
                type: "datetimepart",
                value: it.value
            }, []);
        })
    ], true);
}

},{}],8:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var expressionList = G.separatedList(
        bql.suffixedExpression,
        bql.operator,
        false,
        function(vals, ops) {
            var combined = [vals.shift()];
            while (ops.length) {
                combined.push(ops.shift());
                combined.push(vals.shift());
            }
            return toNode(combined);

            function toNode(lst) {
                if (lst.length === 1) {
                    return lst[0];
                } else {
                    var nextOp = 1;
                    for (var ii = 3; ii < lst.length; ii += 2) {
                        if (lst[ii].precedence <= lst[nextOp].precedence) {
                            nextOp = ii;
                        }
                    }
                    return new BqlNode({
                        type: "operator",
                        value: lst[nextOp].value
                    }, [toNode(lst.slice(0, nextOp)), toNode(lst.slice(nextOp + 1))]);
                }
            }

        }
    );

    bql.expression.initialize([new G.Production([expressionList], Function.id)], true);
}

},{}],9:[function(require,module,exports){



var bql_functions = [];
var bql_ops = [];

function defn(name, group, type, doc, argTypes, placeholders) {
    var fn = {
        group: group,
        label: name,
        type: type,
        doc: doc,
        args: argTypes,
        placeholders: placeholders
    };
    bql_functions.push(fn);
}

function defop(name, group, type, doc, argTypes, precedence, nickname) {
    var fn = {
        group: group,
        label: name,
        type: type,
        doc: doc,
        args: argTypes,
        precedence: precedence,
        nickname: nickname
    };
    bql_ops.push(fn);
}

var dateGroup = "Date Functions";
var mathGroup = "Arithmetic Functions";
var controlGroup = "Control Functions";
var cmpGroup = "Comparison Functions";
var otherGroup = "Other Functions";


var anyType = ["bool", "string", "int", "float", "date", "datetime"];

defn("IIF", controlGroup, anyType, "Immediate IF. Allows an expression to return one result if the specified condition is true and a different one if it is false. Can be nested.", 
     ["bool", anyType, anyType],
     ["Condition", "When True", "When False"]);
defn("IFNULL", controlGroup, anyType, "Allows an expression to return a different result if it is null.", 
     [anyType, anyType],
     ["Argument", "Default Value"]);
defn("IsNaN", controlGroup, "bool", "Evaluates whether an expression is a number", [anyType]);
defn("Abs", mathGroup, "float", "Absolute value of a measure value", ["float"]);
defn("Ceiling", mathGroup, "int", "Returns the closest integer greater than or equal to the given number.", ["float"]);
defn("Floor", mathGroup, "int", "Returns the closest integer less than or equal to a given number.", ["float"]);
defn("Sqrt", mathGroup, "float", "Returns the square root of a given positive number.", ["float"]);
defn("Exp", mathGroup, "float", "Returns e raised to the power of the number given.", ["float"]);
defn("Pow", mathGroup, "float", "Raise x to the power of y.", ["float", "float"]);
//defn("RSum", mathGroup, "float", "Returns the trailing sum of values, based on the window size, at the current report grain for the window provided.", ["int", ["float", "int"]]);
//defn("Median", mathGroup, "float", "Returns the value in the middle of all measure values at the report grain." /* " An optional break by attribute denoted with \"By\" can be used." */, [anyType]);
defn("DateDiff", dateGroup, "int", "Returns the difference in dates as an integer value. (NULLS are interpreted as 0. )", 
     ["datetimepart" /*Year/Month/Day/Hour/Minute/Second*/, "date", "date"],
     ["Date Part", "First Date", "Second Date"]
    );
defn("DateAdd", dateGroup, "date", "Add or subtract an integer from a date.", 
     ["datepart" /*Year/Month/Day*/, "int", "date"],
     ["Date Part", "Number", "Date"]
    );
defn("Format", otherGroup, "string", "Formats a number or date to the particular format provided.", 
     ["float", "string"],
     ["Number", "Format String"]
    );
defn("ToLower", otherGroup, "string", "Returns a character expression after converting data to lower case.", ["string"]);
defn("ToUpper", otherGroup, "string", "Returns a character expression after converting data to upper case.", ["string"]);
defn("Substring", otherGroup, "string", "Returns a portion of a given string and  assumes a zero based index.\n" +
                                        "Note: First position is 0, ending position is the position after the last that should be included.", 
     ["string", "int", "int"],
    ["String", "Start Index", "End Index"]);
defn("Length", otherGroup, "int",  "Returns the number of characters in a string. This can also be used in a Birst ETL script to return the number of elements in a List object.", ["string"]);
defn("Trim", otherGroup, "string", "Trims leading and trailing spaces. Can be used in a constant to pad a value.", ["string"]);
// NOT is disabled for now
// defn("NOT", cmpGroup, "bool", "true if its argument is false and vice versa", ["bool"]);

defop("OR", cmpGroup, "bool", "true if either of its arguments are true", ["bool", "bool"], 1, 'or');

defop("AND", cmpGroup, "bool", "true if both of its arguments are true", ["bool", "bool"], 2, 'and');

defop("<>", cmpGroup, "bool", "compare two values, returning true if the value on the left is unequal to the value on the right", [["float","int"], ["float","int"]], 3, 'not-equal');
defop("=", cmpGroup, "bool", "compare two values, returning true if the value on the left is equal to the value on the right", [["float","int"], ["float","int"]], 3, 'equal');
defop("<", cmpGroup, "bool", "compare two values, returning true if the value on the left is less than the value on the right", [["float","int"], ["float","int"]], 3, 'less-than');
defop(">", cmpGroup, "bool", "compare two values, returning true if the value on the left is greater than the value on the right", [["float","int"], ["float","int"]], 3, 'greater-than');
defop("<=", cmpGroup, "bool", "compare two values, returning true if the value on the left is less than or equal to the value on the right", [["float","int"], ["float","int"]], 3, 'less-or-equal');
defop(">=", cmpGroup, "bool", "compare two values, returning true if the value on the left is greater than or equal to the value on the right", [["float","int"], ["float","int"]], 3, 'greater-or-equal');

defop("/", mathGroup, "float", "Divide two numbers", [["float","int"], ["float","int"]], 4, 'divide');
defop("*", mathGroup, "float", "Multiply two numbers", [["float","int"], ["float","int"]], 4, 'multiply');

defop("+", mathGroup, "float", "Add two numbers", [["float","int"], ["float","int"]], 5, 'add');
defop("-", mathGroup, "float", "Subtract two numbers", [["float","int"], ["float","int"]], 5, 'subtract');

module.exports = {
    functions: bql_functions,
    operators: bql_ops
};

/*
// defn("Add", "list", "Allows you to add a value to end of an existing list.", ["list", "int"]);
// defn("AddParentChild", "Adds a single parent-child pair to the current list of pairs.", ["ParentID","ChildID"]);
// defn("Alt", "To use new query language syntax in spaces using classic query language.", alt:{expression});
defn("ArcCos", mathGroup, "float", "Returns the arc cosine of a number between ο and π.", ["float"]);
defn("ArcSin", mathGroup, "float", "Returns the arc sine of a number between -π/2 and π/2.", ["float"]);
defn("ArcTan", mathGroup, "float", "Returns the arc tangent of a number between -π/2 and π/2.", ["float"]);
defn("Float", mathGroup, "float", "Convert the argument to a Float", ["string"])
defn("Integer", mathGroup, "int", "Convert the argument to an Integer", ["string"])
defn("IsInf", mathGroup, "bool", "Evaluates an expression and returns true if the value is +/- infinity. The result can be used as a conditional check.", ["float"]);
defn("Cos", mathGroup, "float", "Returns the cosine of a given angle.", ["float"]);
//defn("Year", dateGroup, "datepart", "Select the year", []);
//defn("Month", dateGroup, "datepart", "Select the month", []);
//defn("Day", dateGroup, "datepart", "Select the day", []);
//defn("Year", dateGroup, "dateortimepart", "Select the year", []);
//defn("Month", dateGroup, "dateortimepart", "Select the month", []);
//defn("Day", dateGroup, "dateortimepart", "Select the day", []);
//defn("Hour", dateGroup, "dateortimepart", "Select the hour", []);
//defn("Minute", dateGroup, "dateortimepart", "Select the minute", []);
//defn("Second", dateGroup, "dateortimepart", "Select the second", []);
defn("Date", dateGroup, "date", "Convert the argument to a Date", ["string"])
defn("DateTime", dateGroup, "datetime", "Convert the argument to a DateTime", ["string"])
//defn("DatePart", dateGroup, "int", "Returns integers from dates.", ["datetimepart","date"]);
defn("GetWeekID", dateGroup, "int", "This function allows you to return a unique integer signifying the week based on a date or datetime value.", ["date"]);
defn("GetDayID", dateGroup, "int", "This function allows you to return a unique integer signifying the day based on a date or datetime value.", ["date"]);
defn("GetMonthID", dateGroup, "int", "This function allows you to return a unique integer signifying the month based on a date or datetime value.", ["date"]);
defn("DRank", otherGroup, "int", "Dense Rank returns the rank of each row within the result set, without any gaps in the ranking.", ["float"]);
//defn("Find", type, "Similar to LookupValue, except the lookup value does not need an exact match. If the match is exact that row is returned, otherwise, the first row that is greater than the lookup value is used. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.", ["int", "expression", "int", "query"]);
//defn("GetPromptFilter", "A place holder for a dashboard prompt.", ["name"]);
//defn("GetPromptValue", "A place holder for a user defined value taken from a dashboard prompt. An optional second argument (such as null or a string such as '2012') can be used in the event that the first argument is not a currently defined prompt. The second argument can also be supplied by a variable using GetVariable(). Note that all values are returned as strings (except null) and must be cast to numeric data types if they are to be used in arithmetic functions.", ["name", "constant"]);
//defn("GetVariable", "Retrieves the value of a variable.", ["name"]);
*/


/*
  GetLevelAttribute

  Provides additional data either on the entire tree or on a given record that is being processed. Two options are supported.

  Option 1: GetLevelAttribute('NumLevels') // Returns the maximum depth of items in the tree.

  Option 2: GetLevelAttribute('CurrentDepth') // Returns the depth of the current item in the tree.

  

  GetLevelValue

  For the current pair, GetLevelValue(n) returns the ID n levels deep into the hierarchy. If there are fewer than n levels, it returns the ID of the lowest level available. Hence, the IDs will repeat for levels lower than their depth.

*/


/*
  Function()

  Allows for creation of a custom function that can be used in any logical query including an expression.

  Fucntion({datatype of return value}, {calculation/script} COMPLETE [Result]= {calculation from script} END COMPLETE)

  Function (Float, Dim [Var1] as Float = 0.0 Dim [Var2] as Float = 0.0 Complete [Result] = [Var1] - [Var2] End Complete)

  FunctionLookup

  Custom functions execute a script over an entire result set to return a result. It works by declaring a return data type (the type of the function) and providing both a script and a logical query. The script is executed for each row of the result set. Additionally, a lookup parameter can be added and will run the function only on rows that satisfy the filter for the targeted column being equal to the value set. The variable [Result] is populated with the return value.

  FunctionLookup(return data type,lookup parameter,script statement block,logical query)
*/


/*
  IsNaN

  Evaluates an expression and returns true if it is not a number and false if it is. The result can be used as a conditional check.

  IsNaN(evaluation,result if TRUE, result if FALSE)  

  IIF(IsNaN([OrderDate: Sum: Quantity]),0,[OrderDate: Sum: Quantity])


  ETL Script Example: Length([myList]) - 1

  Let

  Allows re-use of logic in complex expressions by declaring expression-level variables, initializing them and re-using them in the expression.

  Let(variable declarations,expression body)

  Let(Dim [AvgPrice] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Avg: UnitPrice] from [All]) Dim [TotalQuantity] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Sum: Quantity] from [All]), IIF([AvgPrice]<25,25,[AvgPrice])*[TotalQuantity])

  Ln(x)

  Returns the natural logarithm of a given number in a specified base.

  Ln([logical column])

  Ln([OrderDate: Sum: Quantity])

  Log(x)

  Returns the logarithm of a given number in a specified base.

  Log([logical column])

  Log([OrderDate: Sum: Quantity])

  LookupRow

  Use over LookupValue if the result set will be in the same order as the existing set.

  LookupRow (index of the row to return, index of the column to return, SELECT [Query] from [ALL])

  LookupRow (0,1, Select [OrderDetail.OrderID], [OrderDate: Sum: Discount] from [ALL])

  LookupValue

  Lookup based on value in the current result set to get a dimension or measure value from another resultset. Looks for EXACT MATCH.

  LookupValue (column number from the query, [value in the current query to look at], column number of the new query, Select [new query] from [All])

  lookupvalue(0,[Products.CategoryName],1,Select [Products.CategoryName],[OrderDate: Sum: Quantity] from [All])


  NextChild

  Once a set of parent-child pairs have been added, the list can then be iterated over one by one to flatten. NextChild positions the current record to the next available pair. If there are no more pairs left, it returns false.

  

  

  NumRows

  Returns the total number of rows in the current result set. (This function is not available in ETL Services.)

  NumRows()

  NumRows()

  Order By

  Sort function.

  Select [query] from [All] Order By [dimension]/[measure] Ascending/Asc/Descending/Desc

  Select [Product.Category Name], [Time.Year],[Unit Sales] from [All] Order By [Product.Category Name] Ascending, [Time.Year] Descending

  Position

  Returns the position of a second string within the first. You can specify the starting index to begin the search as an optional third parameter.

  Position('search string', [dimension], starting position number to begin search)

  Position(‘the’, [Varchar], 10) 

  (In this example, it would start looking at position 10.)


  PTile

  Returns the percentile rank of each row within the result set.

  PTile([measure])

  PTile([OrderDate: Sum: Quantity])

  Random()

  Returns a double from 0.0 to 1.0.  This function ignores parameter values.

  Random()

  Random()

  Rank

  Returns the rank of each row within the result set.

  Rank([logical column])

  Rank([OrderDate: Sum: Quantity])

  Select [Time.Year],[Product.Category Name],Rank([Unit Sales] By [Time.Year]) ‘Sales Rank’ from [All] where [Time.Year]=2011 Or [Time.Year]=2012 Display by [Time.Year] Ascending, [Sales Rank] Ascending

  RemoveAll

  Removes all elements from a list.

  

  

  RemoveAt

  Allows you to remove an item from a given position in a list.

  RemoveAt([ListVariable],[IndexToRemove])

  

  Replace

  Allows a static string value as the search criteria to replace a value.

  Replace(original string/[dimension],part to replace,replacement string)

  Replace([Products.CategoryName], 'ages', 'age')

  ReplaceAll

  Allows a regular expression as the search criteria to replace a value.

  ReplaceAll(original string/[dimension],[expression],replacement string)

  ReplaceAll([Products.CategoryName],'P.*e','Organic Produce')

  RowNumber

  Returns the row number for the current result set.  (This function is not available in ETL Services.)

  [RownumVar]=RowNumber()

  RowNumber()


  Sin

  Returns the sine of a given angle.

  Sin([logical column])

  Sin([OrderDate: Sum: Quantity])

  Sparse

  Allows you to show dimension column values for which there is no fact data. As of 5.4, works with multiple dimensions. See About the Sparse Function for more information.

  Sparse([dimension])

  Sparse([Time.Year])


  STAT

  Aggregate stats based on another result set. Valid aggregations are median, avg, min, max, sum, count, countdistinct, and stddev. 

  Stat(aggregation, index, [query])

  Stat(Median, 1,Select [Products.CategoryName], [OrderDate: Sum: Quantity]  from [All])


  Substring([Products.CategoryName],20,22)

  (The example above returns 3 characters starting at position 20.)

  Tan

  Returns the tangent of a given angle.

  Tan([logical column])

  Tan([OrderDate: Sum: Quantity])


  Top n

  Filter by Top n results.

  SELECT TOP x [query] FROM [ALL]

  Select Top 20 [Product.Product Name],[Unit Sales] from [All] Order By [Unit Sales] Descending

  ToTime

  Allows integer values to be converted into a time format, for example: HH:mm:ss. Supported in ETL scripts and column expressions.

  ToTime(1000000000, 'hours,minutes,seconds', '%d:%02d:%02d')

  ToTime(Integer([Order_Details.OrderID]+[Order_Details.ProductID]*1000),'HH:mm:ss:SSS')


  Transform

  Directly manipulate or create from scratch the report-level result set. Only one Transform is allowed per logical query. The Transform functions just like an ETL services script. It operates row-by-row over the original result set from the query, using WRITERECORD to output new rows as results. The entire result set is re-written as a result of the TRANSFORM statement. All the input and output columns need to be identical.

  Transform({script})

  Transform(Dim [Odd] As Integer = 0 If (Integer([Odd]/2))*2 = [Odd] Then
  [Sum: Quantity]=[Sum: Quantity]  WriteRecord End If [Odd] = [Odd] + 1)

  Trend

  Uses the Least Squares method to extrapolate values based on a linear trend.

  Trend({index of column to return},{index of column to lookup},{value to lookup},{logical query})

  TREND(1,0,[Time.Month Seq Number],SELECT [Time.Month Seq Number], [OrderDate: Sum: Quantity] FROM [ALL] WHERE [Time.Month Seq Number] >= 1136 AND [Time.Month Seq Number] <= 1148)


*/

},{}],10:[function(require,module,exports){
module.exports = function($, subjectArea, BqlNode) {

    var G = require("../grammar")($);


    var bql = {
        // a full query, like "SELECT [Time.Year] FROM [all]"
        query: new G.NonTerminal(),

        // comes after FROM, typically [all]
        fromClause: new G.NonTerminal(),

        // WHERE expression
        whereClause: new G.NonTerminal(),
        
        // DISPLAY WHERE expression
        displayWhereClause: new G.NonTerminal(),

        // the list of columns in the SELECT clause of a query
        projectionList: new G.NonTerminal(),
        
        // Any BQL expression: a number, a column, a function application, two subexpressions combined with an operator, etc.
        expression: new G.NonTerminal(),
        
        // An expression with possible suffixes (like IS NULL) but no operators
        suffixedExpression: new G.NonTerminal(),

        // An expression without operators or suffixes (or a full-fledged expression in parens)
        simpleExpression: new G.NonTerminal(),
        
        // "(" expression ")"
        groupedExpression: new G.NonTerminal(),
        
        // Built in Bql Function, e.g. IIF, IsNaN, etc.
        bqlFunction: new G.NonTerminal(),

        // Infix operators: +, -, /, OR, AND, etc.
        operator: new G.NonTerminal(),
        
        // Year, Month, Day (used in some functions like datediff)
        datepart: new G.NonTerminal(),

        // Mean, Median, StdDev, etc
        statType: new G.NonTerminal(),

        // Year, Month, Day, Hour, Minute, Second (used in some functions like datediff)
        datetimepart: new G.NonTerminal(),
        
        // A measure from the Subject Area, like Revenue.
        measure: new G.NonTerminal(),
        
        // an attribute from the Subject Area, like [Time.Year]
        attribute: new G.NonTerminal(),
        
        // e.g. 100
        'int': new G.NonTerminal(),

        // e.g. 121.1, or 100.0 (BQL distinguishes floats from ints, unlike JS)
        'float': new G.NonTerminal(),

        // e.g. 'BQL is great!'
        string: new G.NonTerminal(),

        // e.g. #2010-01-01#, or NOW
        date: new G.NonTerminal(),
        
        // e.g. %0, %1
        parameter: new G.NonTerminal()
    };

    require("./query")(bql, $, subjectArea, BqlNode, G);
    require("./expression")(bql, $, subjectArea, BqlNode, G);
    require("./suffixedExpression")(bql, $, subjectArea, BqlNode, G);
    require("./simpleExpression")(bql, $, subjectArea, BqlNode, G);
    require("./groupedExpression")(bql, $, subjectArea, BqlNode, G);
    require("./bqlFunction")(bql, $, subjectArea, BqlNode, G);
    require("./datePart")(bql, $, subjectArea, BqlNode, G);
    require("./column")(bql, $, subjectArea, BqlNode, G);
    require("./number")(bql, $, subjectArea, BqlNode, G);
    require("./string")(bql, $, subjectArea, BqlNode, G);
    require("./date")(bql, $, subjectArea, BqlNode, G);
    require("./statType")(bql, $, subjectArea, BqlNode, G);
    return bql;
};

},{"../grammar":21,"./bqlFunction":4,"./column":5,"./date":6,"./datePart":7,"./expression":8,"./groupedExpression":11,"./number":12,"./query":13,"./simpleExpression":14,"./statType":15,"./string":16,"./suffixedExpression":17}],11:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    bql.groupedExpression.initialize([new G.Production([G.string("("), bql.expression, G.string(")")], function(lParen, val, rParen) {
        return val;
    })], true);
}

},{}],12:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {

    var intConstant = G.stringToken(
        function(str) {
            var n = parseInt(str, 10);
            if (isNaN(n)) return null;
            if(str.indexOf(".") !== -1) return null;
            return {
                type: 'int',
                className: 'number-completion',
                string: str
            };
        }, "0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            while(idx < str.length && str.charAt(idx).match(/[-0-9]/)) idx++;
            var candidate = str.substring(start, idx);
            if(isNaN(parseInt(candidate, 10))) return null;

            return {value: {type: 'int', className: 'number-completion', string: candidate},
                    index: idx};
        }
    );

    var floatConstant = G.stringToken(
        function(str) {
            var n = parseInt(str, 10);
            if (isNaN(n)) return null;
            if(str.indexOf(".") === -1) return null;
            return {
                type: 'float',
                className: 'number-completion',
                string: str
            };
        }, "0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            while(idx < str.length && str.charAt(idx).match(/[-0-9.]/)) idx++;
            var candidate = str.substring(start, idx);
            if(candidate.indexOf(".") === -1) return null;
            if(isNaN(parseInt(candidate, 10))) return null;

            return {value: {type: 'float', className: 'number-completion', string: candidate},
                    index: idx};
        }
    );

    var paramConstant = G.stringToken(
        function(str) {
            if(str.charAt(0) !== "%") return null;
            var n;
            if(str.length === 1) {
                n = 0;
            } else {
                n = parseInt(str.substring(1), 10);
                if (isNaN(n)) return null;
                if(n < 0 || n !== Math.floor(n)) return null;
            }
            return {
                type: 'parameter',
                className: 'parameter-completion',
                string: str
            };
        }, "%0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "%") return null;
            idx++;

            while(idx < str.length && str.charAt(idx).match(/[0-9]/)) idx++;
            var candidate = str.substring(start+1, idx);
            var n = parseInt(candidate, 10);
            if (isNaN(n)) return null;
            if(n < 0 || n !== Math.floor(n)) return null;

            return {value: {type: 'parameter', className: 'parameter-completion', string: candidate},
                    index: idx};
        }
    );

    bql['int'].initialize([new G.Production([intConstant], function(ic) {
            return BqlNode.fromInt(parseInt(ic.string, 10));
    })], true);

    bql['float'].initialize([new G.Production([floatConstant], function(fc) {
            return BqlNode.fromFloat(parseInt(fc.string, 10));
    })], true);

    bql.parameter.initialize([new G.Production([paramConstant], function(pc) {
            return BqlNode.fromParameter(pc.string);
    })], true);

}

},{}],13:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    
    bql.query.initialize([
        new G.Production([G.string("SELECT"), bql.projectionList, G.string("FROM"), bql.fromClause, G.optional(bql.whereClause), G.optional(bql.displayWhereClause)], 
                       function(_, projlist, _, fromClause, whereClause, displayWhereClause) {
                           return BqlNode.fromQuery(projlist, fromClause, BqlNode.fromWhereClause(whereClause), BqlNode.fromDisplayWhereClause(displayWhereClause));
                       })
        ], true);
    
    // For now fromClause is just [all]
    bql.fromClause.initialize([
        new G.Production([G.string("[all]")], function(_) {
            return BqlNode.fromFromClause("[all]");
        })
    ], true);

    bql.whereClause.initialize([
        new G.Production([G.string("WHERE"), bql.expression], function(_, exp) {
            return exp;
        })
    ], true);

    bql.displayWhereClause.initialize([
        new G.Production([G.string("DISPLAY WHERE"), bql.expression], function(_, exp) {
            return exp;
        })
    ], true);
        

    var projList = G.separatedList(bql.expression, G.string(","), false, function(exps, commas) { return BqlNode.fromProjectionList(exps); });

    bql.projectionList.initialize([
        new G.Production([projList], Function.id)
    ], true);
        
}


},{}],14:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    bql.simpleExpression.initialize([
        new G.Production([bql['int']], Function.id),
        new G.Production([bql['float']], Function.id),
        new G.Production([bql.bqlFunction], Function.id),
        new G.Production([bql.measure], Function.id),
        new G.Production([bql.attribute], Function.id),
        new G.Production([bql.string], Function.id),
        new G.Production([bql.date], Function.id),
        new G.Production([bql.groupedExpression], Function.id),
        new G.Production([bql.parameter], Function.id)
    ], true);
}

},{}],15:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var statToken = G.listToken(["Median", "Min", "Max", "Avg", "Sum", "Count", "CountDistinct", "StdDev"].map(function(part) {
        return {
            type: "statType",
            className: 'statType-completion',
            string: part,
            value: part
        };
    }), 1);

    bql.statType.initialize([
        new G.Production([statToken], function(it) {
            return new BqlNode({
                type: "statType",
                value: it.value
            }, []);
        })
    ], true);

}

},{}],16:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    function stringToStringToken(str) {
        var match = str.match(/^"((?:[^"]|"")*)"?$/);
        if (match) {
            var innerString = match[1].replace(/""/g, '"');
            return {
                type: 'string',
                className: 'string-completion',
                string: '"' + innerString + '"',
                value: innerString
            };
        }
        return null;
    }
    var stringConstant = G.stringToken(
        stringToStringToken, '""', 0,
        function(str, idx) {
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "'") return null;
            while(true) {
                idx++;
                if(idx >= str.length) return null;
                if(str.charAt(idx) === "'") {
                    var innerString = str.substring(start+1, idx).replace(/\\(.)/g, "\\1");
                    var tok = {
                        type: 'string',
                        className: 'string-completion',
                        string: '"' + innerString + '"',
                        value: innerString
                    };
                    return {value: tok, index: idx+1};
                }
                if(str.charAt(idx) === '\\') {
                    idx++;
                    if(idx >= str.length) return null;
                } 
            }
        }
    );

    bql.string.initialize([new G.Production([stringConstant], function(sc) {
        return BqlNode.fromString(sc.value);
    })], true);
}

},{}],17:[function(require,module,exports){
module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var optionalSuffix = new G.NonTerminal([
        new G.Production([], function() {
            return undefined;
        }),
        new G.Production([G.listToken([{
            type: "suffix",
            className: 'suffix-completion',
            string: "IS NULL"
        }, {
            type: "suffix",
            className: 'suffix-completion',
            string: "IS NOT NULL"
        }], 2)], function(it) {
            return {
                type: "suffix",
                value: it.string
            };
        })
    ], true);

    bql.suffixedExpression.initialize([
        new G.Production([bql.simpleExpression, optionalSuffix], function(simple, suffix) {
            if (suffix) {
                return new BqlNode(suffix, [simple]);
            } else {
                return simple;
            }
        }),
    ], true);

}

},{}],18:[function(require,module,exports){
// An AST is composed of Nodes and Leafs. A Node corresponds to a
// particular production of a nonterminal, while a leaf corresponds to
// a terminal.
//
// for example, if the grammar is A -> aA; A -> b then aab will correspond to a tree like this:
//
//        Node: A -> aA
//          /      \
//    Leaf: a     Node: A -> aA
//                /          \
//             Leaf: a      Node: A -> b
//                             \
//                           Leaf: b


function Node(nonterminal, production, children) {
    this.nonterminal = nonterminal;
    this.production = production;
    this.children = children;
}

function Leaf(terminal, value, initialFlag) {
    this.terminal = terminal;
    this.value = value;
    this.initialFlag = initialFlag;
}


// use a list of numbers to navigate through the tree's children,
// returning the Leaf or Node at those coordinates.
Node.prototype.getChild = function(path) {
    if (path.length === 0) return this;
    if (path.length === 1) return this.children[path[0]];
    return this.children[path[0]].getChild(path.slice(1));
};

// Replace the node at <path> with the node <value>. Return the result.
//
// Note: you should always use this like
//
// ast = ast.setChild(path, value),
//
// because when path is [] then the old value of ast will be
// unchanged.
Node.prototype.setChild = function(path, value) {
    if (path.length === 0) {
        return value;
    } else if (path.length === 1) {
        this.children[path[0]] = value;
        return this;
    } else {
        this.children[path[0]].setChild(path.slice(1), value);
        return this;
    }
};

Node.prototype.initial = function() {
    return this.nonterminal.initial();
}

Leaf.prototype.initial = function() {
    return this.terminal.initial();
}

// Check if the node is still in its initial state
Node.prototype.isInitial = function() {
    return this.nonterminal.productions.indexOf(this.production) === 0 && this.children.every(function(child) {
        return child.isInitial();
    });
};

Leaf.prototype.isInitial = function() {
    return this.value === this.terminal.defaultValue;
};

Node.prototype.isEmpty = function() {
    return this.production.length === 0 || this.children.every(function(child) {
        return child.isEmpty();
    });
};

Leaf.prototype.isEmpty = function() {
    return false;
};

// move backward and then up the tree from the given path, returning a
// list of all nodes visited
Node.prototype.nodesBefore = function(path) {
    var list = [];
    go(this, []);

    function go(node, nodePath) {
        if (nodePath.length === path.length) {
            return;
        } else {
            var next = path[nodePath.length];
            go(node.children[next], nodePath.concat([next]), false);
            for (var ii = next - 1; ii >= 0; ii--) {
                list.push({
                    node: node.children[ii],
                    path: nodePath.concat([ii])
                });
            }
        }
    }
    return list;
};

Node.prototype.firstPath = function() {
    return [];
}

// move forward and then up the tree from the given path, returning a
// list of all nodes visited
Node.prototype.nodesAfter = function(path) {
    var list = [];
    go(this, []);

    function go(node, nodePath) {
        if (nodePath.length === path.length) {
            return;
        } else {
            var next = path[nodePath.length];
            go(node.children[next], nodePath.concat([next]), false);
            for (var ii = next + 1; ii < node.children.length; ii++) {
                list.push({
                    node: node.children[ii],
                    path: nodePath.concat([ii])
                });
            }
        }
    }
    return list;
};

Node.prototype.lastPath = function() {
    return [this.children.length-1]
}

Node.prototype.lastTokenPath = function() {
    for(var ii = this.children.length-1; ii >= 0; ii--) {
        if(this.children[ii] instanceof Leaf) {
            return [ii];
        } 
        var ch = this.children[ii].lastTokenPath();
        if(ch.length) return [ii].concat(ch);
    }
    return [];
}

Node.prototype.nextPath = function(path) {
    var nextPaths = this.nodesAfter(path);
    if(nextPaths.length === 0) {
        return this.lastPath();
    } else {
        return nextPaths[0].path;
    }
}

Node.prototype.prevPath = function(path) {
    var prevPaths = this.nodesBefore(path);
    if(prevPaths.length === 0) {
        return this.firstPath();
    } else {
        var underNodes = this.nodesUnder(prevPaths[0].path);
        return underNodes[ underNodes.length-1 ].path;
    }
}

Node.prototype.prevTokenPath = function(path) {
    var self = this;
    var prevPaths = this.nodesBefore(path);
    prevPaths = prevPaths.concatMap(function(nodePath) { return self.nodesUnder(nodePath.path).reverse(); });
    prevPaths = prevPaths.filter(function(nodePath) { return nodePath.node instanceof Leaf; });
    if(prevPaths.length === 0) {
        return this.firstPath();
    } else {
        return prevPaths[ 0 ].path;
    }
}

Node.prototype.nextTokenPath = function(path) {
    var self = this;
    var nextPaths = this.nodesAfter(path);
    nextPaths = nextPaths.concatMap(function(nodePath) { return self.nodesUnder(nodePath.path); });
    nextPaths = nextPaths.filter(function(nodePath) { return nodePath.node instanceof Leaf; });
    if(nextPaths.length === 0) {
        return this.lastTokenPath();
    } else {
        return nextPaths[ 0 ].path;
    }
}

Node.prototype.backspacePath = function(path) {
    var ret = this.topEquivalentPath(this.prevTokenPath(this.topEquivalentPath(path)));
    while(ret.length && ret[ret.length-1] === 0) {
        ret.pop();
    }
    return this.topEquivalentPath(ret);
}

// Return the next node after path, if there is a node after path and
// if it is in its initial state
Node.prototype.nextInitialPath = function(path) {
    var nextPaths = this.nodesAfter(path);
    if (!nextPaths.length) return null;
    if (!nextPaths[0].node.isInitial()) return null;

    return nextPaths[0].path;
};

// List of all nodes and leaves under the given one, in preorder traversal
Node.prototype.nodesUnder = function(path) {
    var list = [];
    go(this.getChild(path), path);

    function go(node, nodePath) {
        list.push({
            node: node,
            path: nodePath
        });
        if (!(node instanceof Leaf)) {
            node.children.map(function(child, ii) {
                go(child, nodePath.concat([ii]));
            });
        }
    }
    return list;
};

Node.prototype.evaluate = function() {
    return this.production.evaluator.apply({},
        this.children.map(function(child) {
            return child.evaluate();
        })
    );
};

Leaf.prototype.evaluate = function() {
    return this.value;
};

// Find the topmost (closest to tree root) path which contains the
// same leaf nodes as the given path.
Node.prototype.topEquivalentPath = function(path) {
    var best = null;
    for (var ii = 0; ii < path.length; ii++) {
        best = best || path.slice(0, ii);
        var nextNode = this.getChild(best);
        var nextComponent = path[ii];
        var containsOtherLeafs = !nextNode.children.every(function(child, jj) {
            return (jj === nextComponent) || child.isEmpty();
        });
        if (containsOtherLeafs) {
            best = null;
        }
    }

    if (best) return best;
    return path;
};

// The set of tokens that may be the next ones after path, taking into
// account any following empty nodes
Node.prototype.followingTokens = function(path) {
    var child = this.getChild(path);
    if (child instanceof Leaf) {
        return [{
            topPath: path,
            relPath: [],
            token: child.terminal,
            zipper: function(leaf) {
                return leaf;
            }
        }];
    }
    var tokens = child.nonterminal.firstTokens(path);
    if (child.isEmpty()) {
        var nodesAfter = this.nodesAfter(path);
        for (var ii = 0; ii < nodesAfter.length; ii++) {
            var node = nodesAfter[ii].node;
            var nodePath = nodesAfter[ii].path;
            if (!node.isInitial()) break;

            if (node instanceof Leaf) {
                tokens.push({
                    topPath: nodePath,
                    relPath: [],
                    token: node.terminal,
                    zipper: function(leaf) {
                        return leaf;
                    }
                });
            } else {
                tokens = tokens.concat(node.nonterminal.firstTokens(nodePath));
            }

            if (!node.isEmpty()) break;
        }
    }
    return tokens;
};



// Utility functions - compare paths within an AST

function lessPath(left, right) {
    var len = Math.min(left.length, right.length);
    for (var ii = 0; ii < len; ii++) {
        if (left[ii] > right[ii]) return false;
        if (left[ii] < right[ii]) return true;
    }
    return right.length > len;
}

function equalPath(left, right) {
    return !lessPath(left, right) && !lessPath(right, left);
}

module.exports = {
    Leaf: Leaf,
    Node: Node,
    equalPath: equalPath,
    lessPath: lessPath
};

},{}],19:[function(require,module,exports){
module.exports = function($) {

    var PP = require("./pretty_printer")($);
    var G = require("./grammar");
    var A = require("./ast");
    var Auto = require("../autocomplete")($);

    // Prevent chrome from going back when backspace is held down.
    $(document).off('keydown.preventBack').on('keydown.preventBack', function(ev) {
        if(ev.keyCode === 8) {
            if(!$(ev.target).is("input, textarea")) {
                ev.preventDefault();
            }
        }
    });

    function prettyPrint(width, node, renderLeaf, path, completionWidget) {
        return PP.pp(width, PP.group.apply({}, go(node, [], null)));

        function go(node, nodePath, placeholder) {
            if (path && A.equalPath(nodePath, path)) {
                return [PP.span(completionWidget(placeholder))];
            }
            if (node instanceof A.Leaf) {
                return [PP.span(
                    renderLeaf(node, nodePath, placeholder)
                )];
            } else {
                var children = [];
                var placeholders = node.production.placeholders && node.production.placeholders(node.children);
                for (var ii = 0; ii < node.children.length; ii++) {
                    if (ii !== 0) children.push(PP.blank());
                    children = children.concat(go(node.children[ii], nodePath.concat([ii]), placeholders ? placeholders[ii] : placeholder));
                }
                return node.nonterminal.grouped ? [PP.group.apply({}, children)] : children;
            }
        }
    }


    function editAtPath(container, ast, onChange, path, continuationMethod, prefilledString) {
        var nextPath = function(ast, path) { return ast.nextPath(path); }
        var backspacePath = function(ast, path) { return ast.backspacePath(path); }
        var prevTokenPath = function(ast, path) { return ast.topEquivalentPath(ast.prevTokenPath(path)) };

        if(!continuationMethod) {
            continuationMethod = nextPath;
        }
        onChange(ast);
        var tokens = ast.followingTokens(path);
        if(tokens.length === 1 && !prefilledString) {
            var defaultVal = tokens[0].token.defaultValue;
            if(defaultVal.type === 'syntax') {
                defaultVal.token = tokens[0].token;
                defaultVal.zipper = tokens[0].zipper;
                defaultVal.topPath = tokens[0].topPath;
                defaultVal.relPath = tokens[0].relPath;
                continueWith(defaultVal, continuationMethod);
                return;
            }
        }

        var completionWidget = function(placeholder) {
            return Auto.autocomplete(tokenCompletions(tokens), {
                back: path.length > 0 ? onBack : null,
                escape: onEscape,
                finish: onFinish,
                onLeft: onLeft,
                onRight: onRight
            }, prefilledString, placeholder);
        }


        container.find('.pp').remove();
        container.append(prettyPrint(container.width(), ast, function(node, nodePath, placeholder) {
            var span;
            if(node.terminal.customRenderer) {
                span = node.terminal.customRenderer(node);
            } else if(node.initialFlag && placeholder) {
                span = $("<span>").css({fontSize: '90%', fontStyle: 'italic'}).text(placeholder);
            } else {
                span = $("<span>").text(node.value.string);
            }
            return span.on('click', function() {
                editAtPath(container, ast, onChange, ast.topEquivalentPath(nodePath), nextPath);
            });
        }, path, completionWidget));

        function onBack() {
            var currPath = ast.topEquivalentPath(path);
            ast = ast.setChild(currPath, ast.getChild(currPath).initial());
            var prevPath = ast.backspacePath(currPath);
            ast = ast.setChild(prevPath, ast.getChild(prevPath).initial());
            editAtPath(container, ast, onChange, prevPath, backspacePath);
        }

        function onEscape() {
            editAtPath(container, ast, onChange, ast.topEquivalentPath(ast.lastTokenPath()), nextPath);
        }

        function onFinish(val) {
            continueWith(val, nextPath);
        }

        function onLeft() {
            var leftPath = ast.topEquivalentPath(ast.prevTokenPath(path));
            editAtPath(container, ast, onChange, leftPath, prevTokenPath);
        }

        function onRight() {
            var rightPath = ast.topEquivalentPath(ast.nextTokenPath(path));
            editAtPath(container, ast, onChange, rightPath, nextPath);
        }

        function continueWith(val, method) {
            ast = ast.setChild(val.topPath, val.zipper(new A.Leaf(val.token, val)))
            var fullPath = val.topPath.concat(val.relPath);
            var next = method(ast, fullPath);
            editAtPath(container, ast, onChange, next, method);
        }
    }


    function tokenCompletions(tokens) {
        tokens = tokens.sort(function(left, right) {
            return right.token.priority - left.token.priority;
        });
        return function(string) {

            var completions = [],
                completionsStartsWith = [],
                completionsContains = [];
            tokens.map(function(tokenWithPath) {
                var tokenVals = tokenWithPath.token.stringFunction(string);
                tokenVals = tokenVals.map(function(tokenVal) {
                    tokenVal.token = tokenWithPath.token;
                    tokenVal.zipper = tokenWithPath.zipper;
                    tokenVal.topPath = tokenWithPath.topPath;
                    tokenVal.relPath = tokenWithPath.relPath;
                    if (tokenVal.string.toLowerCase().indexOf(string.toLowerCase()) === 0) {
                        completionsStartsWith.push(tokenVal);
                    } else {
                        completionsContains.push(tokenVal);
                    }
                    return tokenVal;
                });
            });
            completions = completionsStartsWith.concat(completionsContains);

            return completions.map(function(completion) {
                return Auto.stringCompletion(completion);
            });
        };
    }

    function stringToken(stringFunction, defaultString, priority, parser) {
        return new G.Terminal(
            function(str) {
                var value = stringFunction(str);
                if (value) return [value];
                return [];
            },
            stringFunction(defaultString),
            priority,
            true,
            parser
        );
    }

    function listToken(values, priority, parser) {
        return new G.Terminal(
            function(str) {
                var exactMatch = values.filter(function(value) {
                    return value.string.toLowerCase() === str.toLowerCase();
                });
                var containingValues = values.filter(function(value) {
                    return value.string.toLowerCase().indexOf(str.toLowerCase()) > -1;
                });
                return exactMatch.concat(containingValues.filter(function(val) {
                    return exactMatch.indexOf(val) === -1;
                }));
            },
            values[0],
            priority,
            false,
            parser || parseValuesCaseInsensitive(values)
        );
    }


    function parseValuesCaseInsensitive(values) {
        return function(str, idx) {
            var matchingStrings = values.map(function(val) { return val.string.toLowerCase(); });
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;

            var strIdx = 0;
            while(true) {
                var stopMatching = false;
                if(idx === str.length) {
                    stopMatching = true;
                } else {
                    var nextChar = str.charAt(idx).toLowerCase();
                    var nextMatches = matchingStrings.filter(function(match) {
                        return match.length > strIdx && match.charAt(strIdx) === nextChar;
                    });
                    stopMatching = (nextMatches.length === 0);
                }
                if(stopMatching) {
                    var exactMatches = matchingStrings.filter(function(match) {
                        return match.length === strIdx
                    });
                    if(exactMatches.length) {
                        var matchingValue = values.filter(function(val) { return val.string.toLowerCase() === exactMatches[0].toLowerCase() })[0]
                        return {index: idx, value: matchingValue};
                    } else {
                        return null;
                    }
                }
                matchingStrings = nextMatches;
                strIdx++;
                idx++;
            }
        }
    }

    function constantString(str) {
        return listToken([{
            type: 'syntax',
            className: 'syntax-completion',
            string: str
        }], 100);
    }

    function optional(elem) {
        return new G.NonTerminal([
            new G.Production([], function() { return undefined; }),
            new G.Production([elem], Function.id)
        ], true);
    }

    function separatedList(elem, sep, allowEmpty, finalize) {
        var tail = new G.NonTerminal([
            new G.Production([], function() {
                return {
                    values: [],
                    separators: []
                };
            })
        ], false);
        tail.productions.push(
            new G.Production([sep, elem, tail], function(sep, val, tailVal) {
                tailVal.values.unshift(val);
                tailVal.separators.unshift(sep);
                return tailVal;
            })
        );

        return new G.NonTerminal((allowEmpty ? [new G.Production([], function() {
            return finalize([], []);
        })] : []).concat([
            new G.Production([elem, tail], function(val, tailVal) {
                tailVal.values.unshift(val);
                return finalize(tailVal.values, tailVal.separators);
            })
        ]), true);
    }


    return {
        string: constantString,
        listToken: listToken,
        stringToken: stringToken,
        optional: optional,
        separatedList: separatedList,
        editAtPath: editAtPath
    };

    /*
      function runTests() {
      var t1 = new Terminal("t1", function(){}, "");

      var p1 = new Production([]);
      var p2 = new Production([t1]);
      var g1 = new NonTerminal([p1, p2]);

      var g1Init = g1.initial();
      assert(g1Init instanceof Node, "Initial form for grammar 1 should be a Node");
      assert(g1Init.isInitial(), "Initial form for grammar 1 should be initial");
      assert(g1Init.isEmpty(), "Initial form for grammar 1 should be empty");

      var g2 = new NonTerminal([p2]);
      var g2Init = g2.initial();
      assert(g2Init instanceof Node, "Initial form for grammar 2 should be a Node");
      assert(g2Init.isInitial(), "Initial form for grammar 2 should be initial");
      assert(!g2Init.isEmpty(), "Initial form for grammar 2 should not be empty");

      g1.productions[1] = new Production([t1, g1]);
      assert(g1Init instanceof Node, "Initial form for grammar 1 should be a Node");
      assert(g1Init.isInitial(), "Initial form for grammar 1 should be initial");
      assert(g1Init.isEmpty(), "Initial form for grammar 1 should be empty");
      }


      runTests();
    */

};

},{"../autocomplete":3,"./ast":18,"./grammar":20,"./pretty_printer":22}],20:[function(require,module,exports){
var assert = require("assert");
var AST = require("./ast");


// A NonTerminal is a letter on the left side of a grammar production, like A -> aA, A -> b.
// It is represented with a list of all its right-side productions.

function NonTerminal(productions, grouped) {
    if (arguments.length) {
        this.initialize(productions, grouped);
    }
}

function Terminal(stringFunction, defaultValue, priority, isRequired, parser) {
    this.stringFunction = stringFunction;
    this.defaultValue = defaultValue;
    this.priority = priority;
    this.isRequired = isRequired;
    this.parser = parser;
}

// A Production is just a (possibly-empty) list of components, which are grammar elements (NonTerminals or Terminals)

function Production(components, evaluator, placeholders) {
    assert(components.every(function(component) {
        return (component instanceof NonTerminal) || (component instanceof Terminal);
    }), "Components should be one of the two Grammar types (Terminal or NonTerminal)");
    assert(evaluator, "Evaluator is required for any Production");

    this.components = components;
    this.evaluator = evaluator;
    this.placeholders = placeholders;
}

NonTerminal.prototype.initialize = function(productions, grouped) {
    assert(productions.every(function(production) {
        return production instanceof Production;
    }), "First argument to NonTerminal should be a list of Productions");

    this.productions = productions;

    // Grouped indicates whether to group this nonterminal for pretty-printing
    this.grouped = grouped;
};

// Initial / default value for a particular nonterminal. Note that the
// *first* production is chosen, so initial values are determined by
// the order in which productions are defined. Note also that a poor
// choice of initial productions could lead to infinite recursion
// here!
NonTerminal.prototype.initial = function() {
    var production = this.productions[0];

    var initialChildren = production.components.map(function(component) {
        return component.initial();
    });

    return new AST.Node(this, production, initialChildren);
};

Terminal.prototype.initial = function() {
    return new AST.Leaf(this, this.defaultValue, true);
};

// Return the initial value only if it is an empty tree (i.e. no terminals in the expansion).
// otherwise known as an epsilon production
NonTerminal.prototype.empty = function() {
    var init = this.initial();
    if (init.isEmpty()) {
        return init;
    }
    return null;
};

Terminal.prototype.empty = function() {
    return null;
};

function streamEnd(success, failure) {
    failure();
}

function streamAppend(s1, s2) {
    return function(success, failure) {
        s1(function(val, idx, tail) {
            success(val, idx, streamAppend(tail, s2));
        }, function() {
            s2(success, failure);
        });
    };
}

function streamSeq(s, fs) {
    return function(success, failure) {
        s(function(val, idx, tail) {
            fs(val, idx)(function(val, idx, tail2) {
                success(val, idx, streamAppend(tail2, streamSeq(tail, fs)));
            }, function() {
                streamSeq(tail, fs)(success, failure);
            });
        }, failure);
    };
}

function streamMap(s, f) {
    return function(success, failure) {
        s(function(val, idx, tail) {
            success(f(val), idx, streamMap(tail, f));
        }, failure);
    };
}

function streamReturn(val, idx) {
    return function(success /*, failure*/) {
        success(val, idx, streamEnd);
    };
}

NonTerminal.prototype.parse = function(str) {
    var ret = null;
    var trimmed = str.trim();
    this.tryParse(trimmed, 0)(function succ(val, idx, tail) {
        if (idx === trimmed.length) {
            ret = val;
        } else {
            tail(succ, function() {});
        }
    }, function() {});
    return ret;
};

NonTerminal.prototype.tryParse = function(str, index) {
    var NT = this;
    return this.productions.map(tryParseProduction).reduceRight(streamAppend, streamEnd);

    function tryParseProduction(production) {
        var parseComponents = production.components.reduce(function(init, last) {
            return streamSeq(init, function(initVals, idx, tail) {
                return streamMap(last.tryParse(str, idx), function(val) {
                    return initVals.concat([val]);
                });
            });
        }, streamReturn([], index));

        return streamMap(parseComponents, function(children) {
            return new AST.Node(NT, production, children);
        });
    }
};

Terminal.prototype.tryParse = function(str, index) {
    var self = this;
    return function(success, failure) {
        var result = self.parser(str, index);
        if (!result) {
            failure();
            return;
        }
        success(new AST.Leaf(self, result.value), result.index, streamEnd);
    };
};

// Tokens which can be at the start of this nonterminal. e.g., if you have
// A -> a
// A -> BC
// B -> b
// C -> C
//
// then firstTokens would include a and b, but not c.
//
// Besides the token itself, we return three other values:
//
//   zipper: turns a Terminal produced from the given token, into a Node corresponding to this top-level NonTerminal
//   relPath: path from the top of the resulting Node to the terminal
//   topPath: passed in and returned unchanged, this represents the path to the NonTerminal within the larger AST
NonTerminal.prototype.firstTokens = function(topPath) {

    return go(this, [], function(node) {
        return node;
    });

    function go(grammar, path, zipper) {
        if (grammar instanceof Terminal) {
            return [{
                topPath: topPath,
                relPath: path,
                token: grammar,
                zipper: zipper
            }];
        }

        return grammar.productions.concatMap(productionFirstTokens);

        function productionFirstTokens(production) {
            var tokens = [];
            var finished = false;
            production.components.map(function(component, ii) {
                if (finished) return;
                tokens = tokens.concat(go(component, path.concat([ii]), function(node) {
                    var children = production.components.map(function(component, jj) {
                        if (ii === jj) {
                            return node;
                        } else {
                            return component.initial();
                        }
                    });
                    return zipper(new AST.Node(grammar, production, children));
                }));
                if (!component.empty()) {
                    finished = true;
                }
            });
            return tokens;
        }
    }
};

module.exports = {
    Terminal: Terminal,
    NonTerminal: NonTerminal,
    Production: Production
};

},{"./ast":18,"assert":24}],21:[function(require,module,exports){
module.exports = function($) {

    var G = require("./grammar.js");
    //var A = require("./ast.js");
    var E = require("./editor.js")($);

    return {
        Production: G.Production,
        NonTerminal: G.NonTerminal,
        Terminal: G.Terminal,
        string: E.string,
        listToken: E.listToken,
        stringToken: E.stringToken,
        optional: E.optional,
        separatedList: E.separatedList,
        editAtPath: E.editAtPath
    };
};

},{"./editor.js":19,"./grammar.js":20}],22:[function(require,module,exports){
module.exports = function($) {

    var blankWidth = 6;
    var indentWidth = 15;

    function blankSpace() {
        return $("<span>").css({display: 'inline-block'}).html("&nbsp").width(blankWidth);
    }

    function indenter(w) {
        return $("<span>").css({display: 'inline-block'}).html("&nbsp").width(w*indentWidth);
    }

    function spanWidth(s) {
        var hiddenDiv = $("<div>").css({position: 'absolute', left: "-3000px"}).appendTo($("body"));
        hiddenDiv.append(s);
        var ret = s.width();
        hiddenDiv.detach();
        return ret;
    }

    function span(s) {
        var w = spanWidth(s);
        return {
            width: w,
            print: function(left, limit, breakSpaces, indent) {
                return {left: left + w,  render: [s], indent: indent};
            }
        };
    }

    function blank() {
        return {
            width: blankWidth,
            print: function(left, limit, breakSpaces, indent) {
                if(breakSpaces) {
                    return {left: indent*indentWidth, indent: indent, render: [$("<br>"), indenter(indent)]};
                } else {
                    return {left: left + blankWidth, indent: indent, render: [blankSpace()]};
                }
            }
        };
    }

    function group() {
        var lst = [].slice.call(arguments);
        var w = 0;
        lst.map(function(it) { w += it.width; });
        return {
            width: w,
            print: function(left, limit, breakSpaces, indent) {
                var breakChildren = left + w > limit;
                var render = [];
                lst.map(function(it) {
                    var ret = it.print(left, limit, breakChildren, indent+1);
                    left = ret.left;
                    render.push.apply(render, ret.render);
                });
                return {left: left, indent: indent, render: render};
            }
        };
    }

    function pp(w, it) {
        var container = $("<div>").addClass('pp');
        it.print(0, w, false, 0).render.map(function(elem) {
            container.append(elem);
        });
        return container;
    }

    return {
        group: group,
        blank: blank,
        span: span,
        pp: pp
    };
};

},{}],23:[function(require,module,exports){
'use strict';

Array.prototype.concatMap = function(fn) {
    return [].concat.apply([], this.map(fn));
};

Function.id = function(x) { return x; };

function makeEditorPopup($, containerFn) {
    var popup, backdrop;

    function closePopup() {
        popup.removeClass('open');
        $(window).off('resize.bqlEditorResize');
        setTimeout(function() {
            drawer.detach();
        }, 600);
    }

    var editor = containerFn(closePopup);
    var drawer = $('<div>').addClass('drawer editor-container').append(editor);

    popup = $("<div>")
        .addClass('drawer-container')
        .append(drawer)
        .appendTo($("body"));

    setTimeout(function() {
        popup.addClass('open');
    }, 1);

    backdrop = $("<div>")
        .addClass('modal-backdrop')
        .click(closePopup)
        .appendTo(popup);
}

window.openBqlEditor = function($, BqlNode, subjectArea, bql, name, expressionIsMeasure, saveBql, checkBql) {
    var expr = (require("./bql/grammar")($, subjectArea, BqlNode)).expression;
    var G = require("./grammar")($);
    var ast = bql ? expr.parse(bql) : expr.initial();
    if(!ast) {
        console.log("Can't parse BQL expression:", bql);
        bql = null;
        ast = expr.initial();
    }

    makeEditorPopup($, function(closePopup) {
        var container = $("<div>").addClass('editor').append($('<div>').append($("<h3>").text('Expression Builder').append($("<span>").text(" (beta)").css({fontSize: 16, color: '#aaa'}))));
        var errorNameReadout = $('<div>').addClass('name-message').text('');
        var inputContainer = $("<div>").addClass("name-container clearfix")
            .append(errorNameReadout)
            .append($('<div>').addClass('title').text('Name: '))
            .appendTo(container);
        var nameInput = $("<input>").addClass("form-control").attr('type', 'text')
            .blur(function() {
                console.log(nameInput.val());
            })
            .appendTo(inputContainer);
        if(name) {
            nameInput.val(name);
        }

        var newAst = null, errorMessage = null, checkId = 1;

        var saveButton = $('<button>').addClass("btn btn-primary pull-right")
			.append($("<i>").addClass("save-icon pull-left"))
            .append($("<span>").text("DONE"))
            .click(function() {
                var newName = nameInput.val();
                closePopup();
                saveBql(newName, newAst, expressionIsMeasure);
            });
        var closeButton = $('<button>').addClass("btn btn-default pull-right")
            .append($("<span>").text("Cancel"))
            .click(closePopup);

        var errorReadout = $("<div>").addClass("pull-right bql-error-message").text("");

        function updateSaveButtonDisabledState() {
            var sanitized = nameInput.val().replace(/[\]\[.]/g, '');
            if (nameInput.val() !== sanitized) { nameInput.val(sanitized); }

            if (nameInput.val() === '') {
                saveButton.attr('disabled', 'disabled');
                errorNameReadout.text('(Name Required)');
            }

            if (errorMessage) {
                saveButton.attr('disabled', 'disabled');
                errorReadout.text(errorMessage);
            }

            if (nameInput.val() !== '' && errorMessage) {
                saveButton.attr('disabled', 'disabled');
                errorReadout.text(errorMessage);
                errorNameReadout.text('');
            }

            if (nameInput.val() !== '' && !errorMessage) {
                saveButton.removeAttr('disabled');
                errorNameReadout.text('');
                errorReadout.text('');
            }
        }
        updateSaveButtonDisabledState();
        nameInput.on('keyup', function() { updateSaveButtonDisabledState(); });

        var editorContainer = $("<div>")
            .addClass('editor-main')
            .append($('<div>').addClass('title').text('Code: '))
            .appendTo(container);

        var category = $('<div>')
            .addClass('expression-type-container')
            .append($('<div>').addClass('title').text('Category: '))
            .append($('<div>').addClass('expression-type')
                .append($('<div>').addClass('radio')
                    .append($('<label>')
                        .append($('<input>')
                            .attr('type', 'radio')
                            .attr('name', 'category-radio')
                            .val('measure')
                            .prop('checked', expressionIsMeasure))
                        .append('<span>Measure</span>')
                    )
                )
                .append($('<div>').addClass('radio')
                    .append($('<label>')
                        .append($('<input>')
                            .attr('type', 'radio')
                            .attr('name', 'category-radio')
                            .val('attribute')
                            .prop('checked', !expressionIsMeasure))
                        .append('<span>Attribute</span>')
                    )
                )
            )
            .appendTo(container);

        category.on('change', function() {
            expressionIsMeasure = category.find(":checked").val() === 'measure';
        });
        var buttonContainer = $('<div>').addClass('button-bar-container').appendTo(container);
        $('<div>').addClass('button-bar clearfix').append(saveButton, closeButton, errorReadout).appendTo(buttonContainer);

        // use setTimeout because the rendering relies on container width
        setTimeout(function() {
            G.editAtPath(editorContainer, ast, function(val) {
                newAst = val;
                var bql = newAst.evaluate().expressionToString();
                var myId = ++checkId;
                checkBql(bql, function(code, message) {
                    // protect against out-of-order ajax call returns
                    if(myId !== checkId) { return; }

                    if(code == 0) {
                        errorMessage = null;
                    } else {
                        console.log("BQL error code:", code);
                        errorMessage = message;
                    }
                    updateSaveButtonDisabledState();
                }, function(err) { console.log(err); });
            }, ast.topEquivalentPath(ast.lastTokenPath()));
        }, 0);

        return container;
    });

};

},{"./bql/grammar":10,"./grammar":21}],24:[function(require,module,exports){
// http://wiki.commonjs.org/wiki/Unit_Testing/1.0
//
// THIS IS NOT TESTED NOR LIKELY TO WORK OUTSIDE V8!
//
// Originally from narwhal.js (http://narwhaljs.org)
// Copyright (c) 2009 Thomas Robinson <280north.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// when used in node, this will actually load the util module we depend on
// versus loading the builtin util module as happens otherwise
// this is a bug in node module loading as far as I am concerned
var util = require('util/');

var pSlice = Array.prototype.slice;
var hasOwn = Object.prototype.hasOwnProperty;

// 1. The assert module provides functions that throw
// AssertionError's when particular conditions are not met. The
// assert module must conform to the following interface.

var assert = module.exports = ok;

// 2. The AssertionError is defined in assert.
// new assert.AssertionError({ message: message,
//                             actual: actual,
//                             expected: expected })

assert.AssertionError = function AssertionError(options) {
  this.name = 'AssertionError';
  this.actual = options.actual;
  this.expected = options.expected;
  this.operator = options.operator;
  if (options.message) {
    this.message = options.message;
    this.generatedMessage = false;
  } else {
    this.message = getMessage(this);
    this.generatedMessage = true;
  }
  var stackStartFunction = options.stackStartFunction || fail;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, stackStartFunction);
  }
  else {
    // non v8 browsers so we can have a stacktrace
    var err = new Error();
    if (err.stack) {
      var out = err.stack;

      // try to strip useless frames
      var fn_name = stackStartFunction.name;
      var idx = out.indexOf('\n' + fn_name);
      if (idx >= 0) {
        // once we have located the function frame
        // we need to strip out everything before it (and its line)
        var next_line = out.indexOf('\n', idx + 1);
        out = out.substring(next_line + 1);
      }

      this.stack = out;
    }
  }
};

// assert.AssertionError instanceof Error
util.inherits(assert.AssertionError, Error);

function replacer(key, value) {
  if (util.isUndefined(value)) {
    return '' + value;
  }
  if (util.isNumber(value) && (isNaN(value) || !isFinite(value))) {
    return value.toString();
  }
  if (util.isFunction(value) || util.isRegExp(value)) {
    return value.toString();
  }
  return value;
}

function truncate(s, n) {
  if (util.isString(s)) {
    return s.length < n ? s : s.slice(0, n);
  } else {
    return s;
  }
}

function getMessage(self) {
  return truncate(JSON.stringify(self.actual, replacer), 128) + ' ' +
         self.operator + ' ' +
         truncate(JSON.stringify(self.expected, replacer), 128);
}

// At present only the three keys mentioned above are used and
// understood by the spec. Implementations or sub modules can pass
// other keys to the AssertionError's constructor - they will be
// ignored.

// 3. All of the following functions must throw an AssertionError
// when a corresponding condition is not met, with a message that
// may be undefined if not provided.  All assertion methods provide
// both the actual and expected values to the assertion error for
// display purposes.

function fail(actual, expected, message, operator, stackStartFunction) {
  throw new assert.AssertionError({
    message: message,
    actual: actual,
    expected: expected,
    operator: operator,
    stackStartFunction: stackStartFunction
  });
}

// EXTENSION! allows for well behaved errors defined elsewhere.
assert.fail = fail;

// 4. Pure assertion tests whether a value is truthy, as determined
// by !!guard.
// assert.ok(guard, message_opt);
// This statement is equivalent to assert.equal(true, !!guard,
// message_opt);. To test strictly for the value true, use
// assert.strictEqual(true, guard, message_opt);.

function ok(value, message) {
  if (!value) fail(value, true, message, '==', assert.ok);
}
assert.ok = ok;

// 5. The equality assertion tests shallow, coercive equality with
// ==.
// assert.equal(actual, expected, message_opt);

assert.equal = function equal(actual, expected, message) {
  if (actual != expected) fail(actual, expected, message, '==', assert.equal);
};

// 6. The non-equality assertion tests for whether two objects are not equal
// with != assert.notEqual(actual, expected, message_opt);

assert.notEqual = function notEqual(actual, expected, message) {
  if (actual == expected) {
    fail(actual, expected, message, '!=', assert.notEqual);
  }
};

// 7. The equivalence assertion tests a deep equality relation.
// assert.deepEqual(actual, expected, message_opt);

assert.deepEqual = function deepEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'deepEqual', assert.deepEqual);
  }
};

function _deepEqual(actual, expected) {
  // 7.1. All identical values are equivalent, as determined by ===.
  if (actual === expected) {
    return true;

  } else if (util.isBuffer(actual) && util.isBuffer(expected)) {
    if (actual.length != expected.length) return false;

    for (var i = 0; i < actual.length; i++) {
      if (actual[i] !== expected[i]) return false;
    }

    return true;

  // 7.2. If the expected value is a Date object, the actual value is
  // equivalent if it is also a Date object that refers to the same time.
  } else if (util.isDate(actual) && util.isDate(expected)) {
    return actual.getTime() === expected.getTime();

  // 7.3 If the expected value is a RegExp object, the actual value is
  // equivalent if it is also a RegExp object with the same source and
  // properties (`global`, `multiline`, `lastIndex`, `ignoreCase`).
  } else if (util.isRegExp(actual) && util.isRegExp(expected)) {
    return actual.source === expected.source &&
           actual.global === expected.global &&
           actual.multiline === expected.multiline &&
           actual.lastIndex === expected.lastIndex &&
           actual.ignoreCase === expected.ignoreCase;

  // 7.4. Other pairs that do not both pass typeof value == 'object',
  // equivalence is determined by ==.
  } else if (!util.isObject(actual) && !util.isObject(expected)) {
    return actual == expected;

  // 7.5 For all other Object pairs, including Array objects, equivalence is
  // determined by having the same number of owned properties (as verified
  // with Object.prototype.hasOwnProperty.call), the same set of keys
  // (although not necessarily the same order), equivalent values for every
  // corresponding key, and an identical 'prototype' property. Note: this
  // accounts for both named and indexed properties on Arrays.
  } else {
    return objEquiv(actual, expected);
  }
}

function isArguments(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
}

function objEquiv(a, b) {
  if (util.isNullOrUndefined(a) || util.isNullOrUndefined(b))
    return false;
  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) return false;
  //~~~I've managed to break Object.keys through screwy arguments passing.
  //   Converting to array solves the problem.
  if (isArguments(a)) {
    if (!isArguments(b)) {
      return false;
    }
    a = pSlice.call(a);
    b = pSlice.call(b);
    return _deepEqual(a, b);
  }
  try {
    var ka = objectKeys(a),
        kb = objectKeys(b),
        key, i;
  } catch (e) {//happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates
  // hasOwnProperty)
  if (ka.length != kb.length)
    return false;
  //the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  //~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i])
      return false;
  }
  //equivalent values for every corresponding key, and
  //~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!_deepEqual(a[key], b[key])) return false;
  }
  return true;
}

// 8. The non-equivalence assertion tests for any deep inequality.
// assert.notDeepEqual(actual, expected, message_opt);

assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
  if (_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'notDeepEqual', assert.notDeepEqual);
  }
};

// 9. The strict equality assertion tests strict equality, as determined by ===.
// assert.strictEqual(actual, expected, message_opt);

assert.strictEqual = function strictEqual(actual, expected, message) {
  if (actual !== expected) {
    fail(actual, expected, message, '===', assert.strictEqual);
  }
};

// 10. The strict non-equality assertion tests for strict inequality, as
// determined by !==.  assert.notStrictEqual(actual, expected, message_opt);

assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
  if (actual === expected) {
    fail(actual, expected, message, '!==', assert.notStrictEqual);
  }
};

function expectedException(actual, expected) {
  if (!actual || !expected) {
    return false;
  }

  if (Object.prototype.toString.call(expected) == '[object RegExp]') {
    return expected.test(actual);
  } else if (actual instanceof expected) {
    return true;
  } else if (expected.call({}, actual) === true) {
    return true;
  }

  return false;
}

function _throws(shouldThrow, block, expected, message) {
  var actual;

  if (util.isString(expected)) {
    message = expected;
    expected = null;
  }

  try {
    block();
  } catch (e) {
    actual = e;
  }

  message = (expected && expected.name ? ' (' + expected.name + ').' : '.') +
            (message ? ' ' + message : '.');

  if (shouldThrow && !actual) {
    fail(actual, expected, 'Missing expected exception' + message);
  }

  if (!shouldThrow && expectedException(actual, expected)) {
    fail(actual, expected, 'Got unwanted exception' + message);
  }

  if ((shouldThrow && actual && expected &&
      !expectedException(actual, expected)) || (!shouldThrow && actual)) {
    throw actual;
  }
}

// 11. Expected to throw an error:
// assert.throws(block, Error_opt, message_opt);

assert.throws = function(block, /*optional*/error, /*optional*/message) {
  _throws.apply(this, [true].concat(pSlice.call(arguments)));
};

// EXTENSION! This is annoying to write outside this module.
assert.doesNotThrow = function(block, /*optional*/message) {
  _throws.apply(this, [false].concat(pSlice.call(arguments)));
};

assert.ifError = function(err) { if (err) {throw err;}};

var objectKeys = Object.keys || function (obj) {
  var keys = [];
  for (var key in obj) {
    if (hasOwn.call(obj, key)) keys.push(key);
  }
  return keys;
};

},{"util/":26}],25:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],26:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

},{"./support/isBuffer":25,"inherits":27}],27:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}]},{},[23])