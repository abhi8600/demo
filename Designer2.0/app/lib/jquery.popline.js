/*
 Core based on:

  jquery.popline.js 0.0.1

  Version: 0.0.1

  jquery.popline.js is an open source project, contribute at GitHub:
  https://github.com/kenshin54/popline.js

  (c) 2013 by kenshin54
*/

;(function($) {

  var LEFT = -2, UP = -1, RIGHT = 2, DOWN = 1, NONE = 0;

  var isIMEMode = false;
  $(document).on('compositionstart', function() {
    isIMEMode = true;
  });
  $(document).on('compositionend', function() {
    isIMEMode = false;
  });

  var toggleBox = function(event) {
    //this avoids false clicks, if clicking outside official input, bail out
    if ( $(".chart-input").is(":visible") !== true) {
      return;
    }
    if ($.popline.utils.isNull($.popline.current)) {
      return;
    }
    var isTargetOrChild = $.contains($.popline.current.target.get(0), event.target) || $.popline.current.target.get(0) === event.target;
    var isBarOrChild = $.contains($.popline.current.bar.get(0), event.target) || $.popline.current.bar.get(0) === event.target;
    if ((isTargetOrChild || isBarOrChild) ) {
      var bar = $.popline.current.bar;
      if (bar.is(":hidden") || bar.is(":animated")) {
        bar.stop(true, true);
        var pos = new Position().mouseup(event);
        pos.left = event.clientX + (event.target.offsetWidth-bar.width())/2;
        $.popline.current.show(pos);
      }
    }else {
      $.popline.hideAllBar();
    }
  };

  var targetEvent = {
    mousedown: function() {
      $.popline.current = $(this).data("popline");
      $.popline.hideAllBar();
    },
    keyup: function(event) {
      var popline = $(this).data("popline");
      if (!isIMEMode && window.getSelection().toString().length > 0 && !popline.keepSlientWhenBlankSelected()) {
        var pos = new Position().keyup(event);
        $.popline.current.show(pos);
      }else {
        $.popline.current.hide();
      }
    },
    keydown: function() {
      $.popline.current = $(this).data("popline");
      var rects = window.getSelection().getRangeAt(0).getClientRects();
      if (rects.length > 0) {
        $(this).data('lastKeyPos', $.popline.boundingRect());
      }
    }
  };

  var Position = function() {
    var target= $.popline.current.target, bar = $.popline.current.bar, positionType = $.popline.current.settings.position;

    var positions = {
      "fixed": {
        mouseup: function(event) {
          var rect = window.getSelection().getRangeAt(0).getBoundingClientRect();
          var left = event.pageX - bar.width() / 2;
          var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
          if (left < 0) {
            left = 10;
          }
          var top = scrollTop + rect.top - bar.outerHeight() - 10;
          return {left: left, top: top};
        },
        keyup: function() {
          var left = null, top = null;
          var rect = $.popline.getRect(), keyMoved = $.popline.current.isKeyMove();
          if (keyMoved === DOWN || keyMoved === RIGHT) {
            left = rect.right - bar.width() / 2;
          }else if (keyMoved === UP || keyMoved === LEFT) {
            left = rect.left - bar.width() / 2;
          }
          var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
          top = scrollTop + rect.top - bar.outerHeight() - 10;
          return {left: left, top: top-20};
        }
      },
      "relative": {
        mouseup: function(event) {
          var left = event.pageX - bar.width() / 2;
          if (left < 0) {
            left = 10;
          }
          var top = event.pageY - bar.outerHeight() - parseInt(target.css('font-size'), 10) / 2;
          return {left: left, top: top-25};
        },
        keyup: function() {
          var left = null, top = null;
          var rect = $.popline.getRect(), keyMoved = $.popline.current.isKeyMove();
          if (keyMoved === DOWN || keyMoved === RIGHT) {
            left = rect.right - bar.width() / 2;
            top = $(document).scrollTop() + rect.bottom - bar.outerHeight() - parseInt(target.css("font-size"), 10);
          } else if (keyMoved === UP || keyMoved === LEFT) {
            left = rect.left - bar.width() / 2;
            top = $(document).scrollTop() + rect.top - bar.outerHeight();
          }
          return {left: left, top: top};
        }
      }
    };

    return positions[positionType];
  };

  $.fn.popline = function(options) {

    /*if ($.popline.utils.browser.ie) {
      return;
    }*/

    var _arguments = arguments;
    this.each(function() {
      if (_arguments.length >= 1 && typeof(_arguments[0]) === "string" && $(this).data("popline")) {
        var func = $(this).data("popline")[_arguments[0]];
        if (typeof(func) === "function") {
          func.apply($(this).data("popline"), Array.prototype.slice.call(_arguments, 1));
        }
      } else if (!$(this).data("popline")) {
        var popline = new $.popline(options, this);
      }
    });

    if (!$(document).data("popline-global-binded")) {
      $(document).mouseup(function(event){
        var _this = this;
        setTimeout(function() {
          toggleBox.call(_this, event);
        }, 1);
      });
      $(document).data("popline-global-binded", true);
    }
  };

  $.popline = function(options, target) {
    this.settings = $.extend(true, {}, $.popline.defaults, options);
    this.setPosition(this.settings.position);
    this.target = $(target);
    this.init();
    $.popline.addInstance(this);
  };

  $.extend($.popline, {

    defaults: {
      zIndex: 9999,
      mode: "edit",
      enable: null,
      disable: null,
      position: "fixed",
      keepSlientWhenBlankSelected: true
    },

    instances: [],

    current: null,

    prototype: {
      init: function() {
        this.bar = $("<ul class='popline " + this.settings.className + "' style='z-index:" + this.settings.zIndex + "'></ul>").appendTo("body");
        this.bar.data("popline", this);
        this.target.data("popline", this);
        var me = this;

        var isEnable = function(array, name) {
          if (array === null) {
            return true;
          }
          for (var i = 0, l = array.length; i < l; i++) {
            var v = array[i];
            if (typeof(v) === "string" && name === v) {
              return true;
            }else if ($.isArray(v)) {
              if (isEnable(v, name)) {
                return true;
              }
            }
          }
          return false;
        };


        var isDisable = function(array, name) {
          if (array === null) {
            return false;
          }
          for (var i = 0, l = array.length; i < l; i++) {
            var v = array[i];
            if (typeof(v) === "string" && name === v) {
              return true;
            }else if ($.isArray(v)) {
              if ((v.length === 1 || !$.isArray(v[1])) && isDisable(v, name)) {
                return true;
              }else if (isDisable(v.slice(1), name)) {
                return true;
              }
            }
          }
          return false;
        };

        var poplineClickHandler = function() {
          $.popline.current.hide();
          $.popline.current.target.find('input').trigger("click");
          $.popline.current.target.find('input').trigger("mouseup");
        };

        var makeButtons = function(parent, buttons) {
          for (var name in buttons) {
            var button = buttons[name];
            var mode = $.popline.utils.isNull(button.mode) ? $.popline.defaults.mode : button.mode;

            if (mode !== me.settings.mode || !isEnable(this.settings.enable, name) || isDisable(this.settings.disable, name)) {
              continue;
            }
            var $button = $("<li><span class='popline-button'></span></li>");

            $button.addClass("popline-button popline-" + name + "-button");

            if (button.iconClass) {
              $button.children(".popline-button").append("<i class='" + button.iconClass + "'></i>");
            }

            if (button.text) {
              $button.children(".popline-button").append("<span class='text " + (button.textClass || '') + "'>" + button.text + "</span>");
            }

            if (button.bgColor) {
              $button.css({'background-color': button.bgColor});
            }

            if (button.fontFamily) {
                $button.css({'font-family': button.fontFamily});
            }


            if ($.isFunction(button.beforeShow)) {
              this.beforeShowCallbacks.push({name: name, callback: button.beforeShow});
            }

            if ($.isFunction(button.afterHide)) {
              this.afterHideCallbacks.push({name: name, callback: button.afterHide});
            }

            $button.appendTo(parent);

            if (button.buttons) {
              var $subbar = $("<ul class='subbar'><span class='back'><i class='glyphicon glyphicon-chevron-left'></i><div>Back</div></span> </ul>");
              
              $subbar.find('.back').on("click", poplineClickHandler);

              $button.append($subbar);
              makeButtons.call(this, $subbar, button.buttons);
              $button.click(function(event) {
                var _this = this;
                if (!$(this).hasClass("boxed")) {
                  me.switchBar($(this), function() {
                    $(_this).siblings("li").hide().end()
                         .children(".popline-button").hide().end()
                         .children("ul").show().end();
                  });
                  event.stopPropagation();
                }
              });
            } else if($.isFunction(button.action)) {
              $button.click((function(button) {
                  return function(event) {
                    button.action.call(this, event, me);
                  };
                })(button)
              );
            }
            $button.mousedown(function(event) {
              if (!$(event.target).is("input")) {
                event.preventDefault();
              }
            });
            $button.mouseup(function(event) {
              event.stopPropagation();
            });
          }
        };

        makeButtons.call(this, this.bar, $.popline.buttons);

        this.target.bind(targetEvent);

        this.bar.on("mouseenter", "li", function() {
          if (!($(this).hasClass("boxed"))) {
            $(this).addClass("hover");
          }
        });
        this.bar.on("mouseleave", "li", function() {
          if (!($(this).hasClass("boxed"))) {
            $(this).removeClass("hover");
          }
        });
      },

      show: function(options) {
        for (var i = 0, l = this.beforeShowCallbacks.length; i < l; i++) {
          var obj = this.beforeShowCallbacks[i];
          var $button = this.bar.find("li.popline-" + obj.name + "-button");
          obj.callback.call($button, this);
        }
        // paint color button with the chosen color
        $(".glyphicon.glyphicon-th").css('color',this.target.children(0).css('color'));
        this.bar.css('top', options.top + "px").css('left', options.left + "px").stop(true, true).fadeIn();
      },

      hide: function() {
        var _this = this;
        if (this.bar.is(":visible") && !this.bar.is(":animated")) {
          this.bar.fadeOut(function(){
            _this.bar.find("li").removeClass("boxed").show();
            _this.bar.find(".subbar").hide();
            _this.bar.find(".textfield").hide();
            _this.bar.find(".popline-button").show();
            for (var i = 0, l = _this.afterHideCallbacks.length; i < l; i++) {
              var obj = _this.afterHideCallbacks[i];
              var $button = _this.bar.find("li.popline-" + obj.name + "-button");
              obj.callback.call($button, _this);
            }
          });
        }
      },

      destroy: function() {
        this.target.unbind(targetEvent);
        this.target.removeData("popline");
        this.target.removeData("lastKeyPos");
        this.bar.remove();
      },

      switchBar: function(button, hideFunc, showFunc) {
        if (typeof(hideFunc) === "function") {
          var _this = this;
          var position = parseInt(_this.bar.css('left'), 10) + _this.bar.width() / 2;
          _this.bar.animate({ opacity: 0, marginTop: -_this.bar.height() + 'px' }, function() {
            hideFunc.call(this);
            button.removeClass('hover').addClass('boxed').show();
            _this.bar.css("margin-top", _this.bar.height() + "px");
            _this.bar.css("left", position - _this.bar.width() / 2 + "px");
            if (typeof(showFunc) === "function") {
              _this.bar.animate({ opacity: 1, marginTop: 0 }, showFunc);
            } else {
              _this.bar.animate({ opacity: 1, marginTop: 0 });
            }
          });
        }
      },

      keepSlientWhenBlankSelected: function() {
        if (this.settings.keepSlientWhenBlankSelected && $.trim(window.getSelection().toString()) === ""){
          return true;
        }else {
          return false;
        }
      },

      isKeyMove: function() {
        var lastKeyPos = this.target.data('lastKeyPos');
        var currentRect = $.popline.boundingRect();
        if ($.popline.utils.isNull(lastKeyPos)) {
          return null;
        }
        if (currentRect.top === lastKeyPos.top && currentRect.bottom !== lastKeyPos.bottom) {
          return DOWN;
        }
        if (currentRect.bottom === lastKeyPos.bottom && currentRect.top !== lastKeyPos.top) {
          return UP;
        }
        if (currentRect.right !== lastKeyPos.right) {
          return RIGHT;
        }
        if (currentRect.left !== lastKeyPos.left) {
          return LEFT;
        }
        return NONE;
      },

      setPosition: function(position) {
        this.settings.position = position === "relative" ? "relative" : "fixed";
      },

      beforeShowCallbacks: [],

      afterHideCallbacks: []

    },

    hideAllBar: function() {
      for (var i = 0, l = $.popline.instances.length; i < l; i++) {
        $.popline.instances[i].hide();
      }
    },

    addInstance: function(popline){
      $.popline.instances.push(popline);
    },

    boundingRect: function(rects) {
      if ($.popline.utils.isNull(rects)) {
        rects = window.getSelection().getRangeAt(0).getClientRects();
      }
      return {
        top: parseInt(rects[0].top, 10),
        left: parseInt(rects[0].left, 10),
        right: parseInt(rects[rects.length -1].right, 10),
        bottom: parseInt(rects[rects.length - 1].bottom, 10)
      };
    },

    webkitBoundingRect: function() {
      var rects = window.getSelection().getRangeAt(0).getClientRects();
      var wbRects = [];
      for (var i = 0, l = rects.length; i < l; i++) {
        var rect = rects[i];
        if (rect.width === 0) {
          continue;
        } else if ((i === 0 || i === rects.length - 1) && rect.width === 1) {
          continue;
        } else {
          wbRects.push(rect);
        }
      }
      return $.popline.boundingRect(wbRects);
    },

    getRect: function() {
      if ($.popline.utils.browser.firefox || $.popline.utils.browser.opera) {
        return $.popline.boundingRect();
      } else if ($.popline.utils.browser.chrome || $.popline.utils.browser.safari) {
        return $.popline.webkitBoundingRect();
      }
    },

    utils: {
      isNull: function(data) {
        if (typeof(data) === "undefined" || data === null) {
          return true;
        }
        return false;
      },
      randomNumber: function() {
        return Math.floor((Math.random() * 10000000) + 1);
      },
      trim: function(string) {
        return string.replace(/^\s+|\s+$/g, '');
      },
      browser: {
        chrome: navigator.userAgent.match(/chrome/i) ? true : false,
        safari: navigator.userAgent.match(/safari/i) && !navigator.userAgent.match(/chrome/i) ? true : false,
        firefox: navigator.userAgent.match(/firefox/i) ? true : false,
        opera: navigator.userAgent.match(/opera/i) ? true : false,
        ie: navigator.userAgent.match(/msie/i) ? true : false,
        webkit: navigator.userAgent.match(/webkit/i) ? true : false
      },
      findNodeWithTags: function(node, tags) {
        if (!$.isArray(tags)) {
          tags = [tags];
        }
        while (node) {
          if (node.nodeType !== 3) {
            var index = tags.indexOf(node.tagName);
            if (index !== -1) {
              return node;
            }
          }
          node = node.parentNode;
        }
        return null;
      }
    },

    addButton: function(button) {
      $.extend($.popline.buttons, button);
    },

    buttons: {}

  });

})(jQuery);

//add buttons we need  to do: add button for font selection
(function($) {
    var colors = [
        '#FF0000',
        '#FFFF00',
        '#9CBE5A',
        '#00AE52',
        '#07A8EC',
        '#002463',
        '#7349A5',
        '#000000',
        '#C3C3C3'
    ];

    var fonts = [
        'Helvetica, Arial, sans-serif',
        '"Arial Black", Gadget, sans-serif',
        '"Comic Sans MS", cursive, sans-serif',
        'Impact, Charcoal, sans-serif',
        '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
        'Tahoma, Geneva, sans-serif',
        '"Trebuchet MS", Helvetica, sans-serif'
    ];

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    }

    function colorToHex(color) {
        if (color.substr(0, 1) === '#') {
            return color;
        }
        var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

        var red = parseInt(digits[2], 10);
        var green = parseInt(digits[3], 10);
        var blue = parseInt(digits[4], 10);

        return '#' + componentToHex(red) + componentToHex(green) + componentToHex(blue);
    }

    var getColorButtons = function (){
        var buttons = {};

        // buttons['picker'] = {
        //   bgColor: '#eeeeee',
        //   text: 'X',
        //   action: function(event, me) {
        //     event.preventDefault();
        //     event.stopPropagation();
        //     console.log(event, me);

        //   }
        // };

        $(colors).each(function (index, color) {
            buttons['color' + index] = {
                bgColor: color,
                text: '&nbsp',
                action: function (event,me) {
                    me.target.find('input').css('color', colorToHex($(this).css('background-color')));
                }
            };
        });

        return buttons;
    };

    var getFontButtons = function (){
        var buttons = {};

        $(fonts).each(function (index, font) {
            buttons['font' + index] = {
                text: 'A',
                fontFamily:font,
                action: function (event,me) {
                    me.target.find('input').css('font-family', $(this).css('font-family'));
                }
            };
        });
        return buttons;
    };

    $.popline.addButton({
        font: {
            iconClass: "glyphicon glyphicon-font",
            mode: "edit",
            buttons: getFontButtons()
        },

        fontDown: {
            iconClass: "popline-icon-sizedown",
            mode: "edit",
            action: function(event, me) {
            var currentFont = parseInt(me.target.find('input').css('font-size'), 10);
            if(currentFont>6) {
              currentFont--;
            }
            me.target.find('input').css('font-size', currentFont+"px");
          }
        },

        fontUp: {
            iconClass: "popline-icon-sizeup",
            mode: "edit",
            action: function(event, me) {
                var currentFont = parseInt(me.target.find('input').css('font-size'), 10);
                currentFont++;
                me.target.find('input').css('font-size', currentFont+"px");
            }
        },

        color: {
            iconClass: "popline-icon-color",
            mode: "edit",
            buttons: getColorButtons()
        },

        bold: {
            iconClass: "popline-icon-bold",
            mode: "edit",
            action: function(event, me) {
                me.target.find('input').css('font-weight') === 'bold' ? me.target.find('input').css('font-weight','normal') : me.target.find('input').css('font-weight','bold');
            }
        },

        italic: {
            iconClass: "popline-icon-italic",
            mode: "edit",
            action: function(event, me) {
                me.target.find('input').css('font-style') === 'italic' ? me.target.find('input').css('font-style','normal') : me.target.find('input').css('font-style','italic');
            }
        },

        close: {
            iconClass: "popline-icon-close",
            mode: "edit",
            action: function() {
                $.popline.current.hide();
            }
        }
    });
})(jQuery);
