//called by layout.js to support special cases of charts where there's only 1 measure or only 1 category
(function() {
    function specialCaseChart(HighchartsBuilder, stage, resultSets, width, height, errorHandler) {
        var axis, groups;

        //generate quick chart here bypassing real chart, since only used when 1 measure and no attribs
        var container = $("<div>").width(width).height(height).css({
            'position': 'absolute',
            'left': 0,
            top: 0
        });

        var builder = new HighchartsBuilder();
        /* single Measure along Y */
        if (stage.allSeries().length) {
            if (stage.allSeries().length > 1) {
                errorHandler({
                    basicText: visualizer.misc.errors.ERR_CAT_B4_MULT_MSR,
                    moreInfoText: null
                });
            }
            builder.xCategories([stage.allSeries()[0].column.name], false, null, null, null, errorHandler);
            if (resultSets[0].rows.length === 0) {
                errorHandler({
                    basicText: 'Error processing query return.',
                    moreInfoText: 'No result set'
                });
            }

            groups = [{
                stacks: [{
                    points: [{
                        y: resultSets[0].rows[0][0]
                    }]
                }]
            }];

            axis = builder.addYAxis("", true);
            var val = resultSets[0].rows[0][0];
            if (val > 0) {
                axis.max = val;
                axis.min = 0;
            } else {
                axis.max = 0;
                axis.min = val;
            }
        } else {

            /* single Category along X */
            var category = _.find(stage.allColumns(), function(column) {
                return~ resultSets[0].displayNames[0].indexOf(column.name);
            });

            builder.xCategories(_.pluck(resultSets[0].rows, 0), false, true, category.name, true, errorHandler);

            groups = [];

            var Cats = resultSets[0].rows;

            _.each(Cats, function(element) {
                //do not include null categories
                if (element[0] !== null) {
                    groups.push({
                        stacks: [{
                            points: [{
                                y: 0
                            }]
                        }]
                    });
                }
            });

            axis = builder.addYAxisNoMax("", true);
        }

        builder.addColumnSeriesOnAxis("Total", 'column', groups, axis);
        builder.turnTooltipOff();

        var chart = $("<div>").highcharts(builder.generateConfig());

        container.append(chart);

        function resizeSingle(newWidth, newHeight) {
            container.width(newWidth).height(newHeight);
            var totalMargin = 94; //hardcoded for now

            var plotWidth = (newWidth - totalMargin);

            var hc = chart.highcharts();

            var w = plotWidth + hc.chartWidth - hc.plotWidth;
            var h = newHeight;

            hc.setSize(w, h);
            chart.width(w).height(h);

        }

        // Resize after container is added to DOM
        setTimeout(function() {
            resizeSingle(width, height);
        }, 0);
        return {
            container: container,
            resize: resizeSingle
        };

    }
    window.specialCaseChart = specialCaseChart;
})();
