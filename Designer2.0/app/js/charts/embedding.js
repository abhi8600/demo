(function() {
    function embeddingService(chartStateService, HighchartsBuilder) {

        function Embedding(stage, resultSets, inverted, width, height, errorHandler) {
            this.stage = stage;
            this.resultSets = resultSets;
            this.inverted = inverted;
            this.tagsOff = [];

            this.dimensionValues = this.calculateDimensionValues();

            var trellisedResults = this.trellisResultSets();

            this.builders = _.map(trellisedResults, function(row, ii) {
                var isLeft = ii === 0;
                var isRight = ii === trellisedResults.length - 1;

                return _.map(row, function(resultSets, jj) {
                    var isTop = jj === 0;
                    var isBottom = jj === row.length - 1;

                    var horizontalTrellisValue = this.dimensionValues.HorizontalTrellis ? this.dimensionValues.HorizontalTrellis[ii] : "";
                    var verticalTrellisValue = this.dimensionValues.VerticalTrellis ? this.dimensionValues.VerticalTrellis[jj] : "";

                    return this.addTrellisChart(isLeft, isRight, isTop, isBottom, resultSets, horizontalTrellisValue, verticalTrellisValue, errorHandler);
                }, this);
            }, this);

            this.normalizeYAxes();
            this.setWidths(width);
            this.setHeights(height);
        }

        Embedding.prototype = {

            trellisResultSets: function() {
                var validSeries = this.stage.allValidSeries();

                var cols;
                if (this.dimensionValues.HorizontalTrellis) {
                    cols = _.map(this.dimensionValues.HorizontalTrellis, function(trellisValue) {
                        return _.map(this.resultSets, function(resultSet, ii) {
                            var columns = this.stage.getSeriesColumns(validSeries[ii]);
                            var hTrellisInd = _.indexOf(_.pluck(columns, 'dim'), 'HorizontalTrellis');

                            return {
                                rows: _.filter(resultSet.rows, function(row) {
                                    return row[hTrellisInd] === trellisValue;
                                }),
                                columns: resultSet.columns
                            };
                        }, this);
                    }, this);
                } else {
                    cols = [this.resultSets];
                }

                if (this.dimensionValues.VerticalTrellis) {
                    cols = _.map(cols, function(resultSets) {
                        return _.map(this.dimensionValues.VerticalTrellis, function(trellisValue) {
                            return _.map(resultSets, function(resultSet, ii) {
                                var columns = this.stage.getSeriesColumns(validSeries[ii]);
                                var vTrellisInd = _.indexOf(_.pluck(columns, 'dim'), 'VerticalTrellis');

                                return {
                                    rows: _.filter(resultSet.rows, function(row) {
                                        return row[vTrellisInd] === trellisValue;
                                    }),
                                    columns: resultSet.columns
                                };
                            }, this);
                        }, this);
                    }, this);
                } else {
                    cols = cols.map(function(it) {
                        return [it];
                    });
                }

                return cols;
            },

            addTrellisChart: function(isLeft, isRight, isTop, isBottom, resultSets, horizontalTrellisValue, verticalTrellisValue, errorHandler) {
                this.gradientsUsed = 0;

                var builder = new HighchartsBuilder();

                builder.inverted(this.inverted);
                builder.title(isTop ? "" + horizontalTrellisValue : "");

                this.setXAxis(builder, !(isLeft && isRight), isBottom, errorHandler);
                this.addSeries(builder, resultSets, isLeft, verticalTrellisValue);

                return builder;
            },

            normalizeYAxes: function() {
                var yMaxima = _.map(this.builders[0][0].yAxes, function() {
                    return 0;
                });
                var yMinima = _.map(this.builders[0][0].yAxes, function() {
                    return 0;
                });

                _.each(_.flatten(this.builders), function(builder) {
                    _.each(builder.yAxes, function(axis, i) {
                        if (builder._stackType === 'normal') {
                            yMaxima[i] = Math.max(yMaxima[i], builder.yAxisMax(axis));
                            yMinima[i] = Math.min(yMaxima[i], builder.yAxisMin(axis));
                        } else if (builder._stackType === 'percent') {
                            yMaxima[i] = 100;
                            yMinima[i] = 0;
                        }
                    });
                });


                var allNegative = _.max(yMaxima) === 0;
                var allPositive = _.min(yMinima) === 0;

                // Use a constant for simplicity right now; but it would be better to vary by browser height.
                var maxNumberOfTicks = 11;

                // Round up to a number like 250, 1000, 500000, etc.

                function roundNumberBaseTen(n) {
                    var exponent = Math.floor(Math.log(n) / Math.log(10));
                    var mantissa = n / Math.pow(10, exponent);

                    if (mantissa <= 2.0) {
                        mantissa = 2.0;
                    }

                    if (mantissa <= 2.5) {
                        mantissa = 2.5;
                    } else if (mantissa <= 5) {
                        mantissa = 5;
                    } else {
                        mantissa = 10;
                    }

                    return mantissa * Math.pow(10, exponent);
                }

                // Simply rounding up the tickSize to an even number can leave a lot of
                // extra ticks at the top. Remove any extra ticks that are beyond the range of
                // all the charts.

                var axisTicks = _.map(yMaxima, function(m, ii) {
                    var oneTick,
                        tickSize,
                        nTicks;

                    var axisMax = Math.max(yMaxima[ii], -yMinima[ii]);

                    if (allNegative || allPositive) {
                        oneTick = axisMax / (maxNumberOfTicks - 1);
                        tickSize = roundNumberBaseTen(oneTick);
                        nTicks = 1 + Math.ceil(axisMax / tickSize);
                    } else {
                        oneTick = 2 * axisMax / (maxNumberOfTicks - 1);
                        tickSize = roundNumberBaseTen(oneTick);
                        nTicks = 1 + 2 * Math.ceil(axisMax / tickSize);
                    }

                    return {
                        tickSize: tickSize,
                        nTicks: nTicks
                    };
                });
                var nTicks = _.max(_.pluck(axisTicks, 'nTicks'));

                _.each(_.flatten(this.builders), function(builder) {
                    _.each(builder.yAxes, function(axis, ii) {
                        var tickSize = axisTicks[ii].tickSize;
                        if (allPositive || allNegative) {
                            axis.tickPositioner = function() {
                                return _.map(_.range(nTicks), function(n) {
                                    return allPositive ? tickSize * n : tickSize * (n - nTicks + 1);
                                });
                            };
                        } else {
                            axis.tickPositioner = function() {
                                return _.map(_.range(nTicks), function(n) {
                                    return tickSize * (n - (nTicks - 1) / 2);
                                });
                            };
                        }
                    });
                });
            },

            setWidths: function(width) {
                var len = this.builders.length;

                _.each(_.flatten(this.builders), function(builder) {
                    builder.width(Math.max(200, Math.floor(width / len)));
                }, this);
            },

            setHeights: function(height) {
                var len = this.builders[0].length;

                _.each(_.flatten(this.builders), function(builder) {
                    builder.height(Math.max(200, Math.floor(height / len)));
                }, this);
            },

            generateConfigs: function(stage, errorHandler) {
                var numberOfTrellisCharts = _.flatten(this.builders).length;

                if (numberOfTrellisCharts > 25) {
                    errorHandler({
                        basicText: visualizer.misc.errors.ERR_TOOMANYTRELLIS,
                        moreInfoText: '(' + numberOfTrellisCharts + ') charts.'
                    }, true);

                    return [];
                }

                return _.map(this.builders, function(columnBuilders) {
                    return _.invoke(columnBuilders, 'generateConfig', stage);
                });
            },

            // this groupBy groups by all dimensions at the same time,
            // so if there are 16 results and it is given 2 dimensions, it will make 4 groups of 4,
            // whereas if it is given 3 dimensions it would make 8 groups of 2

            groupBy: function(dims, columns, rows) {
                var groups = {};

                var indices = _.map(dims, function(dim) {
                    return _.findIndex(columns, function(col) {
                        return col.dim === dim;
                    });
                });

                _.each(rows, function(row) {
                    // this is a tuple of column values from all the columns that we're grouping by
                    var tuple = _.map(indices, function(index) {
                        return row[index];
                    });
                    var key = JSON.stringify(tuple);

                    groups[key] = groups[key] || {
                        tuple: tuple,
                        rows: []
                    };
                    groups[key].rows.push(row);
                });
                //console.log('dim vals', this.dimensionValues);
                var allDimensionValues = _.map(dims, function(dim) {
                    return this.dimensionValues[dim];
                }, this);
                return _.map(_.cartesianProduct(allDimensionValues), function(tuple) {
                    var key = JSON.stringify(tuple);

                    return {
                        tuple: tuple,
                        rows: groups[key] ? groups[key].rows : []
                    };
                });
            },

            setVisibility: function(tag, isVisible, chart) {
                if (isVisible) {
                    this.tagsOff = _.without(this.tagsOff, tag);
                } else {
                    if (!_.contains(this.tagsOff, tag)) {
                        this.tagsOff.push(tag);
                    }
                }

                var seriesOff = [];

                _.each(this.tagsOff, function(tag) {
                    seriesOff = _.union(seriesOff, this.builders[0][0].seriesNumbersForTag(tag));
                }, this);

                _.each(_.range(chart.series.length), function(n) {
                    if (_.contains(seriesOff, chart.series[n].options.originalOrdering)) {
                        chart.series[n].setVisible(false, false);
                    } else {
                        chart.series[n].setVisible(true, false);
                    }
                });

                chart.redraw(false);
            },

            setHighlight: function(tag, isHighlit, chart) {
                var seriesToHighlight = this.builders[0][0].seriesNumbersForTag(tag);

                _.each(chart.series, function(serie) { // IMPORTANT: Do not use the series index here because the index changes when .update is called!
                    if (!_.contains(seriesToHighlight, serie.options.originalOrdering)) {
                        return;
                    }

                    if (_.contains(['bar', 'column'], serie.type)) {
                        _.each(serie.data, function(point) {
                            point.options.oldColor = point.options.oldColor || point.options.color;
                            point.update({
                                color: isHighlit ? "#ccc" : point.options.oldColor,
                                oldColor: point.options.oldColor,
                                index: point.options.index
                            }, false);
                        });
                    } else if (_.contains(['scatter'], serie.type)) {
                        _.each(serie.data, function(point) {
                            point.options.marker.oldColor = point.options.marker.oldColor || point.options.marker.fillColor;
                            var newMarker = angular.copy(point.options.marker);
                            if (isHighlit) {
                                newMarker.fillColor = "#ccc";
                            } else {
                                newMarker.fillColor = newMarker.oldColor;
                            }
                            point.update({
                                marker: newMarker
                            }, false);
                        });
                    } else {
                        serie.options.oldColor = serie.options.oldColor || serie.options.color;

                        // .update erases all information on the serie, so all properties must be restored if we want to maintain them,
                        // such as the index... we may want more flexible code here that copies all the old properties back to the series
                        // and then make our changes on top of that base, rather than assuming there is nothing we want to keep
                        /*
                    var updatedSerieOptions = _.extend({}, serie.options, {
                        color: isHighlit ? "#ccc" : serie.options.oldColor, oldColor: serie.options.oldColor,
                        //index: serie.options.index // not needed, this is preserved...
                    });

                    //console.log('new serie.index', serie.index);

                    //console.log(serie.index, serie.options.index, updatedSerieOptions.index);*/

                        serie.update({
                            color: isHighlit ? "#ccc" : serie.options.oldColor,
                            oldColor: serie.options.oldColor,
                            index: serie.options.index
                        }, false);
                    }
                });

                // update all the series in the right display order.

                // again, this update is destroying all the information on each series
                //_.invoke(chart.series, 'update');

                _.each(_.sortBy(chart.series, function(serie) {
                    return serie.options.originalOrdering;
                }), function(serie) {
                    serie.update({
                        index: serie.options.index
                    }, false);
                });

                chart.redraw(false);
            },

            calculateDimensionValues: function() {
                var dimensionValueSets = {};
                var dimensionValues = {};
                var allSeries = this.stage.allValidSeries();
                this.seriesCount = allSeries.length;
                //console.log('allSeries = ', allSeries)
                _.each(this.resultSets, function(seriesResults, ii) {
                    var columns = this.stage.getSeriesColumns(allSeries[ii]);
                    _.each(columns, function(column, columnIndex) {
                        dimensionValueSets[column.dim] = dimensionValueSets[column.dim] || {};

                        _.each(seriesResults.rows, function(row) {
                            //do not add a dimension if its category is null

                            if (!dimensionValueSets[column.dim][row[columnIndex]]) {
                                dimensionValueSets[column.dim][row[columnIndex]] = true;

                                dimensionValues[column.dim] = dimensionValues[column.dim] || [];
                                dimensionValues[column.dim].push(row[columnIndex]);
                            }
                        }, this);
                    }, this);
                }, this);


                var previousDimensionValues = chartStateService.getPreviousDimensionValues();
                _.each(_.keys(dimensionValues), function(dim) {
                    if (!_.subset(previousDimensionValues[dim] || [], dimensionValues[dim])) {
                        previousDimensionValues[dim] = dimensionValues[dim];
                    }
                });
                chartStateService.setPreviousDimensionValues(previousDimensionValues);
                this.previousDimensionValues = previousDimensionValues;
                return dimensionValues;
            },

            addSeries: function(builder, resultSets, isFirst, verticalTrellisValue) {
                var seriesByAxis = this.stage.validSeriesByAxis();
                var seriesCount = 0;

                _.each(seriesByAxis, function(axisSeries, ii) {
                    var nextSeriesCount = seriesCount + axisSeries.series.length;
                    var trellisTitle = ii === seriesByAxis.length - 1 ? verticalTrellisValue + "<br>" : "";
                    this.addSeriesForAxis(builder, axisSeries.axis, axisSeries.series, resultSets.slice(seriesCount, nextSeriesCount), isFirst, trellisTitle);
                    seriesCount = nextSeriesCount;
                }, this);
            },

            addSeriesForAxis: function(builder, axis, series, resultSets, isFirst, verticalTrellisValue) {
                var axisName = _.map(series, function(serie) {
                    return serie.column.name;
                }).join("    -    ");
                var newAxis = builder.addYAxis("" + verticalTrellisValue + axisName, isFirst);

                _.each(series, function(serie, ii) {
                    this.addSerieToAxis(builder, serie, resultSets[ii], newAxis);
                }, this);
            },

            addSerieToAxis: function(builder, serie, resultSet, builderAxis) {
                var serieNumber = _.indexOf(this.stage.allValidSeries(), serie);

                if (serie.type === 'column' || serie.type === 'bar' || serie.type === 'area' || serie.type === 'areaspline') {
                    builder._stackType = serie.colorDistribution === 'Percent' ? 'percent' : 'normal';
                } else {
                    builder._stackType = 'normal';
                }

                switch (serie.type) {
                    case 'bar':
                    case 'column':
                        this.addColumnSerieToAxis(builder, serie, resultSet, builderAxis, serieNumber);
                        break;
                    case 'scatter':
                    case 'points':
                    case 'bubble':
                        this.addScatterSerieToAxis(builder, serie, resultSet, builderAxis, serieNumber);
                        break;
                    default:
                        this.addLinearSerieToAxis(builder, serie, resultSet, builderAxis, serieNumber);
                        break;
                }

                var columns = this.stage.getSeriesColumns(serie);

                if (_.contains(_.pluck(columns, 'dim'), 'Color')) {
                    if (this.stage.Color.expression.isMeasure()) {
                        this.gradientsUsed += 1;
                    }
                }
            },

            addColumnSerieToAxis: function(builder, serie, resultSet, builderAxis, serieNumber) {
                var columns = this.stage.getSeriesColumns(serie);

                var allDims = _.pluck(columns, 'dim');
                var hTrellisInd = _.indexOf(allDims, 'HorizontalTrellis');
                var vTrellisInd = _.indexOf(allDims, 'VerticalTrellis');

                var measureInd = _.indexOf(allDims, 'measure');
                var colorInd = _.indexOf(allDims, 'Color');
                //var colorIndAll = _.indexOf(allDims, 'Color');
                var dims = _.without(allDims, 'HorizontalTrellis', 'VerticalTrellis');

                var groupDims = _.filter(dims, function(dim) {
                    return serie.seriesDimensions[dim] === true && (colorInd > -1) && serie.colorDistribution === 'Group' && !this.stage[dim].expression.isMeasure();
                }, this);

                var stackDims = _.filter(dims, function(dim) {
                    return serie.seriesDimensions[dim] === true && (colorInd > -1) && (serie.colorDistribution === 'Stack' || serie.colorDistribution === 'Percent') && !this.stage[dim].expression.isMeasure();
                }, this);

                if (colorInd > -1) {
                    var colorMax = _.max(this.dimensionValues.Color);
                } else {
                    var seriesColor = chartStateService.getColorForSeries(this.seriesCount, serieNumber, 0);
                }

                //but column series are sent as a single object, and then split into series in the builder

                //var stackGroups = this.groupBy(groupDims, columns, groupTuples.rows);

                //console.log('stackGroups', stackGroups);

                var groups = _.map(this.groupBy(['Category'], columns, resultSet.rows), function(groupTuples) {
                    return {
                        stacks: _.map(this.groupBy(groupDims, columns, groupTuples.rows), function(stackTuples) {
                            return {
                                points: _.map(this.groupBy(stackDims, columns, stackTuples.rows), function(pointTuples) {
                                    if (pointTuples.rows.length === 0) {
                                        return {
                                            y: 0
                                        };
                                    }

                                    var y = pointTuples.rows[0][measureInd];
                                    var color;
                                    var tags = [JSON.stringify(['series', serieNumber])];

                                    if (colorInd > -1) {
                                        if (this.stage.Color.expression.isMeasure()) {
                                            color = chartStateService.getColorGradient(this.gradientsUsed, pointTuples.rows[0][colorInd] / colorMax);
                                        } else {
                                            color = chartStateService.getColorForSeries(this.seriesCount, serieNumber, _.indexOf(this.previousDimensionValues.Color, pointTuples.rows[0][colorInd]));
                                            tags.push(JSON.stringify(['Color', pointTuples.rows[0][colorInd]]));
                                        }
                                    } else {
                                        color = seriesColor;
                                    }

                                    //console.log('colorValue', pointTuples.rows[0][colorInd]);

                                    return {
                                        y: y,
                                        color: color,
                                        colorValue: pointTuples.rows[0][colorInd],
                                        tags: tags,
                                        hTrellisValue: pointTuples.rows[0][hTrellisInd],
                                        vTrellisValue: pointTuples.rows[0][vTrellisInd]
                                    };
                                }, this)
                            };
                        }, this)
                    };
                }, this);

                builder.addColumnSeriesOnAxis(serie.column.name, serie.type, groups, builderAxis);
            },

            addScatterSerieToAxis: function(builder, serie, resultSet, builderAxis, serieNumber) {
                var columns = this.stage.getSeriesColumns(serie);
                var dims = _.pluck(columns, 'dim');
                var measureInd = _.indexOf(dims, 'measure');

                window.assert(measureInd > -1, "Measure and category should always be present in a result set");

                var categoryInd = _.indexOf(dims, 'Category');

                window.assert(categoryInd > -1, "Measure and category should always be present in a result set");

                var colorInd = _.indexOf(dims, 'Color');
                var shapeInd = _.indexOf(dims, 'Shape');
                var sizeInd = _.indexOf(dims, 'Size');
                var hTrellisInd = _.indexOf(dims, 'HorizontalTrellis');
                var vTrellisInd = _.indexOf(dims, 'VerticalTrellis');

                if (colorInd > -1) {
                    var colorMax = _.max(this.dimensionValues.Color);
                }

                if (sizeInd > -1) {
                    var sizeMax = _.max(_.map(this.dimensionValues.Size, Math.abs));
                }

                var groupDims = _.filter(_.without(dims, 'measure', 'Category', 'HorizontalTrellis', 'VerticalTrellis'), function(dim) {
                    return !this.stage[dim].expression.isMeasure();
                }, this);

                _.each(this.groupBy(groupDims, columns, resultSet.rows), function(tupleRows) {
                    var tags = _.map(groupDims, function(dim, ii) {
                        return JSON.stringify([dim, tupleRows.tuple[ii]]);
                    });

                    tags.push(JSON.stringify(['series', serieNumber]));

                    var points = _.map(tupleRows.rows, function(row) {
                        var x = row[categoryInd];
                        var y = row[measureInd];
                        var color,
                            shape,
                            size,
                            negative = false;

                        if (colorInd > -1) {
                            if (this.stage.Color.expression.isMeasure()) {
                                color = chartStateService.getColorGradientObject(this.gradientsUsed, row[colorInd] / colorMax);
                            } else {
                                color = _.hexToRgb(chartStateService.getColorForSeries(this.seriesCount, serieNumber, _.indexOf(this.previousDimensionValues.Color, row[colorInd])));
                            }
                        } else {
                            color = _.hexToRgb(chartStateService.getColorForSeries(this.seriesCount, serieNumber, 0));
                        }

                        if (shapeInd > -1) {
                            shape = chartStateService.getShapeForSeries(this.seriesCount, serieNumber, _.indexOf(this.previousDimensionValues.Shape, row[shapeInd]));
                        }

                        if (sizeInd > -1) {
                            size = 2 + 28 * Math.abs(row[sizeInd]) / sizeMax;
                            negative = row[sizeInd] < 0;
                        }

                        return {
                            x: x,
                            y: y,
                            size: size,
                            negative: negative,
                            sizeValue: row[sizeInd],
                            shape: shape,
                            shapeValue: row[shapeInd],
                            color: "rgba(" + [color.r, color.g, color.b, "0.5"] + ")",
                            colorValue: row[colorInd],
                            hTrellisValue: row[hTrellisInd],
                            vTrellisValue: row[vTrellisInd]
                        };
                    }, this);

                    builder.addScatterSeriesOnAxis(serie.column.name, points, builderAxis, tags);
                }, this);
            },

            addLinearSerieToAxis: function(builder, serie, resultSet, builderAxis, serieNumber) {
                var columns = this.stage.getSeriesColumns(serie);
                var groupDims = _.without(_.pluck(columns, 'dim'), 'Category', 'measure', 'HorizontalTrellis', 'VerticalTrellis');

                _.each(this.groupBy(groupDims, columns, resultSet.rows), function(tupleRows) {
                    var tags = _.map(groupDims, function(dim, ii) {
                        return JSON.stringify([dim, tupleRows.tuple[ii]]);
                    });

                    tags.push(JSON.stringify(['series', serieNumber]));

                    var colorInd = _.indexOf(groupDims, 'Color');

                    var color;

                    if (colorInd > -1) {
                        color = chartStateService.getColorForSeries(this.seriesCount, serieNumber, _.indexOf(this.previousDimensionValues.Color, tupleRows.tuple[colorInd]));
                    } else {
                        color = chartStateService.getColorForSeries(this.seriesCount, serieNumber, 0);
                    }

                    //var categoryInd = dimensionIndex(columns, 'Category');
                    var measureInd = dimensionIndex(columns, 'measure');
                    var hTrellisInd = dimensionIndex(columns, 'HorizontalTrellis');
                    var vTrellisInd = dimensionIndex(columns, 'VerticalTrellis');
                    var colorInd2 = dimensionIndex(columns, 'Color');

                    var yValues = _.map(this.groupBy(['Category'], columns, tupleRows.rows), function(xRows) {
                        var old = false;

                        if (old) {
                            if (xRows.rows.length) {
                                return xRows.rows[0][measureInd];
                            } else {
                                return 0;
                            }
                        } else {
                            if (xRows.rows.length) {
                                return {
                                    y: xRows.rows[0][measureInd],
                                    hTrellisValue: xRows.rows[0][hTrellisInd],
                                    vTrellisValue: xRows.rows[0][vTrellisInd],
                                    colorValue: xRows.rows[0][colorInd2]
                                };
                            } else {
                                return {
                                    y: 0
                                };
                            }
                        }
                    });

                    // console.log('addLinearSerieToAxis yValues', yValues);

                    builder.addLinearSeriesOnAxis(serie.column.name, serie.type, color, yValues, builderAxis, tags);
                }, this);
            },

            setXAxis: function(builder, isTrellis, showLabels, errorHandler) {
                window.assert(this.stage.Category, "Category must be set for X axis");

                if (this.stage.Category.expression.isMeasure()) {
                    builder.xLinear(showLabels);
                } else {
                    //console.log('this.dimensionValues', this);
                    builder.xCategories(this.dimensionValues.Category, isTrellis, null, null, showLabels, errorHandler);
                }
            }
        };

        function dimensionIndex(columns, dim) {
            return _.indexOf(_.pluck(columns, 'dim'), dim);
        }

        return Embedding;
    }


    angular.module("visualizer").factory('Embedding', ['chartStateService', 'HighchartsBuilder', embeddingService]);

})();
