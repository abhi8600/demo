// visualizer.legends returns an array of legends, one for each of the visual variables used by the chart,
// each of which can be a "legend" (visual variable legend) or a "seriesLegend"

visualizer.legends = function(chartStateService) {

    function colorLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        var usedGradients = 0;
        var colorIsMeasure = stage.Color && stage.Color.expression.isMeasure();

        var legends = [];
        var colorlessSeries = [];
        _.each(series, function(serieColumns, ii) {
            var seriesName = stage.allValidSeries()[ii].column.name;
            if (_.contains(_.pluck(serieColumns, 'dim'), 'Color')) {
                var legendLabel = stage.Color.name + " (" + seriesName + "): ";
                if (colorIsMeasure) {
                    legends.push(
                        legendContainer(legendLabel, gradientLegend('Color', _.max(_.map(dimensionValues.Color, Math.abs)), usedGradients))
                    );
                    usedGradients++;
                } else {
                    legends.push(
                        legendContainer(legendLabel, categoryLegend('Color', dimensionValues.Color, previousDimensionValues.Color, series.length, ii, setVisibility, setHighlight, stage.Color.displayFiltered || []))
                    );
                }
            } else {
                colorlessSeries.push({
                    number: ii,
                    name: seriesName
                });
            }
        });
        if (colorlessSeries.length) {
            // Color is not being used as a dimension, so we should show a series key.
            legends.push(seriesLegend(colorlessSeries, series.length, setVisibility, setHighlight));
        }
        return legends;
    }

    function shapeLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        if (!dimensionValues.Shape) {
            return [];
        }

        var legends = [];
        _.each(series, function(serieColumns, ii) {
            var seriesName = stage.allValidSeries()[ii].column.name;
            if (_.contains(_.pluck(serieColumns, 'dim'), 'Shape')) {
                var legendLabel = stage.Shape.name + " (" + seriesName + "): ";
                legends.push(
                    legendContainer(legendLabel, categoryLegend('Shape', dimensionValues.Shape, previousDimensionValues.Shape, series.length, ii, setVisibility, setHighlight, stage.Shape.displayFiltered || []))
                );
            }
        });
        return legends;
    }

    function sizeLegends(dimensionValues, stage) {
        if (!dimensionValues.Size) {
            return [];
        } else {
            return [legendContainer(
                stage.Size.name + ":",
                gradientLegend('Size', _.max(_.map(dimensionValues.Size, Math.abs)))
            )];
        }
    }

    // A legend for a data series.  For example, "Series 1: (blue), Series 2: (green)"

    function seriesLegend(colorlessSeries, totalSeries, setVisibility, setHighlight) {
        var icons = _.map(colorlessSeries, function(serie) {
            return colorIcon(chartStateService.getColorForSeries(totalSeries, serie.number, 0));
        });
        var seriesIcons = _.map(icons, function(icon, ii) {
            var seriesNumber = colorlessSeries[ii].number;
            return seriesVisibilityToggle(JSON.stringify(["series", seriesNumber]), setVisibility, setHighlight, true,
                $("<span>").append(icon, centeredText(colorlessSeries[ii].name)));
        });
        var contents = $("<div>");
        _.each(seriesIcons, function(icon) {
            contents.append(icon);
        });
        return legendContainer("Series: ", contents);
    }

    // legend for a color gradient

    function gradientLegend(dim, maxVal, gradientNumber) {
        var width = 120;
        var numbers = $("<div>").css({
            'position': 'relative',
            textAlign: 'left'
        });
        var numberStopCount = 3;
        _.map(_.range(numberStopCount), function(ii) {
            var numberStop =
                $("<span>")
                .text(formatNumber(maxVal * ii / (numberStopCount - 1)))
                .appendTo(numbers);
            if (ii > 0) {
                // First numberStop is not absolute positioned, to give div height
                numberStop.css({
                    'position': 'absolute',
                    left: ii * width / (numberStopCount - 1),
                    top: 0
                });
            }
            setTimeout(function() {
                // Center the number horizontally, after it has been added to DOM
                numberStop.css('marginLeft', -numberStop.width() / 2);
            }, 0);
        });

        var gradient;

        switch (dim) {
            case "Color":
                gradient = colorGradient(gradientNumber, width);
                break;
            case "Size":
                gradient = sizeGradient(width);
                break;
            default:
                throw "Unhandled dimension type in gradientLegend: " + dim;
        }

        return $("<div>").css('padding', '0 20px').append(
            gradient,
            numbers
        );
    }

    // used to generate legends for color (hue) and shape

    function categoryLegend(dim, values, previousValues, totalSeries, seriesNumber, setVisibility, setHighlight, valuesFiltered) {
        var icons;

        switch (dim) {
            case "Color":
                icons = _.map(values, function(val) {
                    return colorIcon(chartStateService.getColorForSeries(totalSeries, seriesNumber, _.indexOf(previousValues, val)));
                });
                break;
            case "Shape":
                icons = _.map(values, function(val) {
                    return shapeIcon(chartStateService.getShapeForSeries(totalSeries, seriesNumber, _.indexOf(previousValues, val)));
                });
                break;
            default:
                throw "Unhandled dimension type in categoryLegend: " + dim;
        }

        var legendDiv = $("<div>");

        _.map(icons, function(icon, ii) {
            var tag = JSON.stringify([dim, values[ii]]);
            var legendItem = seriesVisibilityToggle(tag, setVisibility, setHighlight, !_.contains(valuesFiltered, values[ii]),
                $("<span>").append(icon, centeredText(values[ii])));
            legendDiv.append(legendItem);
        });

        return legendDiv;
    }

    function colorGradient(gradientNumber, width) {
        var stops = _.map(_.range(20), function(n) {
            return chartStateService.getColorGradient(gradientNumber, n / 20) + " " + (n * 5) + "%";
        });

        var gradient = "linear-gradient(" + ["90deg"].concat(stops).join(",") + ")";

        return $("<span>").css({
            display: 'inline-block',
            width: width,
            height: 24,
            background: gradient
        }).html("&nbsp;");
    }

    function sizeGradient(width) {
        return $("<span>").css({
            display: 'inline-block',
            borderRight: width + 'px solid #999',
            borderTop: '24px solid transparent'
        });
    }

    function colorIcon(color) {
        return $("<span>").css({
            display: 'inline-block',
            verticalAlign: 'middle',
            width: 14,
            height: 14,
            margin: '0 4px',
            backgroundColor: color
        });
    }

    function shapeIcon(shape) {
        var icon = $("<span>").css({
            'display': 'inline-block',
            margin: '0 4px'
        });
        var renderer = new window.Highcharts.Renderer(icon[0], 18, 18);
        renderer.symbol(shape || 'circle', 4, 4, 12, 12).add().attr('fill', '#555');
        return icon;
    }

    function legendContainer(title, contents) {
        return $("<div>").addClass('legend-container').append(centeredText(title, true), contents);
    }

    // returns a series toggle switch that...

    // it seems that the tag is used to identify the series to toggle, and that's where a problem is

    function seriesVisibilityToggle(tag, setVisibility, setHighlight, visible, span) {
        span.css('cursor', 'pointer').css('display', 'inline-block').on('click', function() {
            visible = !visible;
            setVisibility(tag, visible);
            span.css('color', visible ? '#777' : '#bbb');
        }).hover(function() {
            setHighlight(tag, true);
        }, function() {
            setHighlight(tag, false);
        });
        span.css('color', visible ? '#777' : '#bbb');
        if (!visible) {
            setVisibility(tag, false);
        }
        return span;
    }

    function centeredText(text, isTitle) {
        var paddingRight = isTitle ? '4px' : '8px'; // why do we need this padding?  doesn't seem like a normal part of centering text

        text = isTitle ? text.split('.')[text.split('.').length - 1].replace('_', ' ') : text;

        return $("<span>").css({
            'vertical-align': 'middle',
            'padding-right': paddingRight
        }).text(text);
    }

    function formatNumber(val) {
        var suffixes = [
            [Math.pow(10, 12), "T"],
            [Math.pow(10, 9), "B"],
            [Math.pow(10, 6), "M"],
            [Math.pow(10, 3), "K"],
            [1, ""]
        ];
        for (var ii = 0; ii < suffixes.length; ii++) {
            if (val > suffixes[ii][0]) {
                var part = val / suffixes[ii][0];
                var decimals = part < 100 ? 1 : 0;
                return window.Highcharts.numberFormat(part, decimals) + suffixes[ii][1];
            }
        }
        return window.Highcharts.numberFormat(val);
    }

    return function(dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        var series = stage.getAllColumnsPerSeriesQuery();

        return [].concat(
            colorLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight)
        ).concat(
            shapeLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight)
        ).concat(
            sizeLegends(dimensionValues, stage, setVisibility, setHighlight)
        );
    };
};


angular.module("visualizer").factory('legendService', ['chartStateService', visualizer.legends]);
