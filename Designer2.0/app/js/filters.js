'use strict';

/* Filters */

visualizer.filters.subjectAreaDisplayName = function() {
    return function(columnText) {
        if (typeof columnText === 'undefined') {
            return '';
        }
        return columnText.replace(/\./g, ' ').replace(/_/g, ' ');
    };
};

visualizer.filters.subjectAreaSearchName = function() {
    return function(columnText) {
        return columnText.split('.')[columnText.split('.').length - 1].replace(/_/g, ' ');
    };
};

visualizer.filters.popoverThreshold = function() {
    // This filter allows for popovers to only be shown if the displayName
    // is over a certain number of characters (threshold).
    // If threshold is set to the string 'omit', then the popover will
    // always be suppressed.
    return function(displayName, threshold) {
        if (displayName === null) {
            return '';
        }
        var thresh = 25; // by default, hide all tooltips of length less than this
        if (threshold === 'report expression') {
            thresh = 16; // report expressions have the +/- toggle, thus lower threshold
        } else if (!isNaN(threshold)) {
            thresh = threshold;
        }
        if (typeof displayName === 'undefined' || threshold === 'omit') {
            return '';
        }
        return displayName.length > thresh ? displayName : '';
    };
};

visualizer.filters.characters = function() {
    // Truncates and adds ellipsis to long inputs.  If `input` is greater
    // than `chars` characters long, returns the first `chars - 1` characters
    // plus three periods.
    return function(input, chars) {
        if (isNaN(chars)) {
            return input;
        }
        if (chars <= 0) {
            return '';
        }
        if (input && input.length > chars) {
            return input.substring(0, chars - 1) + '...';
        }
        return input;
    };
};

visualizer.filters.timeFromNow = function () {
    return function (date) {
        return window.moment(date).fromNow();
    };
};

visualizer.filters.highlightPartial = function() {
    return function(partial, search) {
        var regex_search = new RegExp(_.regexEscape(search), 'i');
        var result = regex_search.exec(partial);
        if (!result) {
            return _.htmlEscape(partial);
        }
        return [
            _.htmlEscape(partial.substring(0, result.index)),
            "<b>" + _.htmlEscape(partial.substring(result.index, result.index + search.length)) + "</b>",
            _.htmlEscape(partial.substring(result.index + search.length))
        ].join("");
    };
};

visualizer.filters.stagingAreaDisplayName = function() {
    return function(columnText) {
        return columnText.slice(columnText.indexOf('.') + 1).replace(/_/g, ' ');
    };
};

visualizer.filters.tableDisplayName = function() {
    return function(columnText) {
        return _.last(columnText.split(': ')).replace(/_/g, ' ');
    };
};

visualizer.filters.cssClass = function() {
    return function(text) {
        return text.toLowerCase().replace(/\s/g, '-').replace(/\(|\)/g, '');
    };
};

visualizer.filters.displayRoundNumber = function() {
    return function(number) {
        return Math.floor(number);
    };
};

visualizer.filters.setDecimals = function() {
    return function(number, variable) {
        return _.isNumber(number) ? number.toFixed(variable || 2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : number;
    };
};

visualizer.filters.formatNumberWithCommas = function() {
    return function(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
};

visualizer.filters.capitalizeFirstLetter = function() {
    return function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
};

visualizer.filters.lowerCaseFirstLetter = function() {
    return function(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    };
};


visualizer.filters.range = function() {
    var MAX_ONLY = 'max';
    var MIN_ONLY = 'min';
    var MIN_TO_MAX = true;
    var OUTER = 'outer';

    return function(list, min, max, type) {
        var newList = [];
        var item, i;

        type = type || true;

        function testMin(item) {
            if (min === undefined) {
                return true;
            } else {
                return item >= min;
            }
        }

        function testMax(item) {
            if (max === undefined) {
                return true;
            } else {
                return item <= max;
            }
        }

        for (i = 0; i < list.length; ++i) {
            item = list[i];

            if (type === MIN_TO_MAX) {
                if (testMin(item) && testMax(item)) {
                    newList.push(item);
                }
            } else if (type === MIN_ONLY) {
                if (!testMin(item)) {
                    newList.push(item);
                }
            } else if (type === MAX_ONLY) {
                if (!testMax(item)) {
                    newList.push(item);
                }
            } else if (type === OUTER) {
                if (!(testMin(item) && testMax(item))) {
                    newList.push(item);
                }
            }
        }

        return newList;
    };
};

visualizer.filters.orderObjectBy = function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function(a, b) {
            return (a[field] > b[field]);
        });
        if (reverse) {
            filtered.reverse();
        }
        return filtered;
    };
};

angular.module('visualizer').
filter('subjectAreaDisplayName', visualizer.filters.subjectAreaDisplayName).
filter('subjectAreaSearchName', visualizer.filters.subjectAreaSearchName).
filter('popoverThreshold', visualizer.filters.popoverThreshold).
filter('characters', visualizer.filters.characters).
filter('timeFromNow', visualizer.filters.timeFromNow).
filter('highlightPartial', visualizer.filters.highlightPartial).
filter('stagingAreaDisplayName', visualizer.filters.stagingAreaDisplayName).
filter('tableDisplayName', visualizer.filters.tableDisplayName).
filter('formatNumberWithCommas', visualizer.filters.formatNumberWithCommas).
filter('displayRoundNumber', visualizer.filters.displayRoundNumber).
filter('setDecimals', visualizer.filters.setDecimals).
filter('capitalizeFirstLetter', visualizer.filters.capitalizeFirstLetter).
filter('lowerCaseFirstLetter', visualizer.filters.lowerCaseFirstLetter).
filter('range', visualizer.filters.range).
filter('cssClass', visualizer.filters.cssClass).
filter('orderObjectBy', visualizer.filters.orderObjectBy);
