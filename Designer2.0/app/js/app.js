/*jshint -W079 */
'use strict';

// Declare app level module which depends on filters, and services

var visualizer = {
    config: {},
    directives: {},
    services: {},
    webservices: {},
    models: {},
    controllers: {},
    filters: {},
    misc: {}
};

visualizer.config = {
    routes: function($routeProvider) {
        $routeProvider.when('/reportName/:reportName', {
            templateUrl: 'partials/designer.html'
        });
        $routeProvider.when('/', {
            templateUrl: 'partials/designer.html'
        });
        $routeProvider.when('/login', {
            templateUrl: 'partials/login.html'
        });
        $routeProvider.when('/reportView', {
            templateUrl: 'partials/reportView.html'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }
};

angular.module('visualizer', [
    'ngRoute',
    'ngAnimate',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.sortable',
    'mgcrea.ngStrap',
    'templates-app'
]).
config(['$routeProvider', visualizer.config.routes]);
