(function() {

    var alertDiv;
    alertDiv = $("<div>")
        .addClass("alert alert-dismissable am-fade alert-success")
        .css({
            display: 'block',
            position: 'absolute',
            right: 20,
            top: 20,
            width: 400
        })
        .append(
            $("<button>").attr('type', 'button').addClass('close').text('x').click(function() {
                alertDiv.remove();
            })
    );
    var alertContent = $("<span>").appendTo(alertDiv);

    $(".primary").append(alertDiv);

    var alertServiceCSA = function() {

        return {
            set: function(contents) {
                alertContent.empty().append(contents);
                $(".primary").append(alertDiv);
            },

            clear: function() {
                alertDiv.remove();
            }
        };
    };

    angular.module('visualizer')
        .service('alertServiceCSA', [alertServiceCSA]);

})();
