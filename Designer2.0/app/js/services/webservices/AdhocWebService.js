(function() {

    var $ = window.$;

    if (angular && angular.element) {
        //$ = angular.element;
    }

    function convertNumericTextToNumber(text) {
        if (text === '') {
            return null;
        } else if (!isNaN(text)) {
            return Number(text);
        } else {
            return text;
        }
    }

    var javaSqlTypes = {
        '-7': 'BIT',
        '-6': 'TINYINT',
        '5': 'SMALLINT',
        '4': 'INTEGER',
        '-5': 'BIGINT',
        '6': 'FLOAT',
        '7': 'REAL',
        '8': 'DOUBLE',
        '2': 'NUMERIC',
        '3': 'DECIMAL',
        '12': 'VARCHAR',
        '-1': 'LONGVARCHAR',
        '91': 'DATE',
        '92': 'TIME',
        '93': 'TIMESTAMP',
        '-2': 'BINARY',
        '-3': 'VARBINARY',
        '-4': 'LONGVARBINARY',
        '0': 'NULL',
        '1111': 'OTHER',
        '2000': 'JAVA_OBJECT',
        '2001': 'DISTINCT',
        '2002': 'STRUCT',
        '2003': 'ARRAY',
        '2004': 'BLOB',
        '2005': 'CLOB',
        '2006': 'REF',
        '70': 'DATALINK',
        '16': 'BOOLEAN',
        '-8': 'ROWID',
        '-15': 'NCHAR',
        '-9': 'NVARCHAR',
        '-16': 'LONGNVARCHAR',
        '2011': 'NCLOB'
    };


    function javaSqlType(typeEnumIndex) {
        return javaSqlTypes[typeEnumIndex + ''];
    }

    function AdhocWebService($http, $q, chartStateService) {

        this.$http = $http;
        this.$q = $q;
        this.chartStateService = chartStateService;

        if (this.$http === undefined && angular && angular.injector) {
            this.$http = angular.injector(['ng']).get('$http');
        }

        this.keepAlive = false;
    }

    AdhocWebService.prototype.post = function(urn, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        var timedOut = false;
        var completed = false;

        function successWrapper(xml) {
            completed = true;

            if (!timedOut && typeof success === 'function') {
                success(xml);
            }
        }

        function errorWrapper(xml) {
            completed = true;

            if (!timedOut && typeof error === 'function') {
                error(xml);
            }
        }

        setTimeout(function() {
            if (completed) {
                return;
            }

            timedOut = true;

            if (typeof error === 'function') {
                error('query timed out');
            }
        }, 60000);

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': urn,
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(successWrapper || noOp).error(errorWrapper || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: successWrapper,
                error: errorWrapper
            });
        } else {
            error();
        }
    };

    AdhocWebService.prototype.baseUrl = '/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint/';


    //note--in rare cases reports end with legacy .jasper so change to .AdhocReport on the fly
    AdhocWebService.prototype.getReport = function(reportName, success, error) {
        this.post(
            '"urn:openReport"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:openReport xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:filename>' + _.htmlEscape(reportName.replace('.jasper', '.AdhocReport')) + '</ns:filename>' +
            '</ns:openReport>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {
                try {
                    if (typeof(xml) !== 'string') {
                        xml = $.parseXML(xml);
                    } else {
                        // dont mess with XML, it's already XML
                    }
                } catch (e) {
                    error(e.toString());
                }

                success(xml, reportName);
            },
            error
        );
    };


    AdhocWebService.prototype.getVisualizerData = function(reportName, success, error) {
        this.post(
            '"urn:getVisualizerData"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:getVisualizerData xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:filename>' + _.htmlEscape(reportName) + '</ns:filename>' +
            '</ns:getVisualizerData>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log(xml);

                var getVisualizerDataResult = $(xml).find('Result').get(1);

                //console.log('resultText1', getVisualizerDataResult);

                var $getVisualizerDataResult = $(getVisualizerDataResult);

                //console.log('resultText', $getVisualizerDataResult.text());

                var columns = [];

                $getVisualizerDataResult.find('Col').each(function(i, column) {
                    var $column = $(column);

                    //console.log('found column', i, column);

                    columns.push({
                        column: $column.attr('column'),
                        alias: $column.attr('alias')
                    });
                });

                var json = {
                    query: $getVisualizerDataResult.attr('query'),
                    columns: columns
                    /*
                     rows: jsonRows,
                     columnNames: columnNames,
                     displayNames: displayNames,
                     dataTypes: dataTypes,
                     length: jsonRows.length,
                     hasMoreRows: $executeQueryInSpaceResult.find('hasMoreRows').first().text()*/
                };

                success(json);
            },

            error
        );
    };


    // WARNING: SavedExpression('name') is sensitive to the use of single vs double quotes.
    // Also, be sure to send columns as [Order_Date: Sum: Revenue] not "Revenue" or incorrect data will be returned.

    AdhocWebService.prototype.getColumnDefinition = function(column, success, error) {
        this.post(
            '"urn:getColumnDefinition"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:getColumnDefinition xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:column>' + _.htmlEscape(column) + '</ns:column>' +
            '</ns:getColumnDefinition>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log('$xml', xml);

                //console.log(xml);

                var columnDefinition = $(xml).find('Result').get(0);

                var $columnDefinition = $(columnDefinition);

                //console.log('columnDefinition', columnDefinition);

                var json = {
                    columnType: $columnDefinition.find('ColumnType').text(),
                    name: $columnDefinition.find('Name').text(),
                    column: $columnDefinition.find('Column').text(),
                    originalColumn: $columnDefinition.find('OriginalColumn').text(),
                    isMeasureExpression: $columnDefinition.find('IsMeasureExpression').text().toLowerCase() === "true",
                    expressionAggRule: $columnDefinition.find('ExpressionAggRule').text(),
                    visualizationPanel: $columnDefinition.find('VisualizationPanel').text()
                };

                //console.log('json', json);

                success(json);
            },

            error
        );
    };

    AdhocWebService.prototype.executeQuery = function(query, success, error) {
        //console.log('adhoc baseurl = ', this.baseUrl);

        this.post(
            '"urn:executeQuery"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:executeQuery xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:queryString>' + _.htmlEscape(query) + '</ns:queryString>' +
            '<ns:maxNumberOfRowsReturned>10000</ns:maxNumberOfRowsReturned>' +
            '</ns:executeQuery>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                //<Result>1.6313725490196078</Result>
                var executeQueryInSpaceResult = $(xml).find('Result').first().find('Result');
                var json;
                var jsonRows = [];
                var columnNames = [];
                var displayNames = [];
                var dataTypes = [];
                var warehouseColumnDataTypes = [];
                var columnFormats = [];

                var $executeQueryInSpaceResult = $(executeQueryInSpaceResult);

                $executeQueryInSpaceResult.find('dataTypes').first().children().each(function(index, childElement) {
                    //dataTypes.push($(childElement).prop('tagName') + ' ' + $(childElement).text());
                    dataTypes.push(javaSqlType($(childElement).text()));
                });

                $executeQueryInSpaceResult.find('warehouseColumnDataTypes').first().children().each(function(index, childElement) {
                    //dataTypes.push($(childElement).prop('tagName') + ' ' + $(childElement).text());
                    warehouseColumnDataTypes.push($(childElement).text());
                });

                $executeQueryInSpaceResult.find('columnFormats').first().children().each(function(index, childElement) {
                    //dataTypes.push($(childElement).prop('tagName') + ' ' + $(childElement).text());
                    columnFormats.push($(childElement).text());
                });

                $executeQueryInSpaceResult.find('rows').first().find('ArrayOfString').each(function(index, arrayOfString) {
                    var jsonRow = [];

                    var strings = $(arrayOfString).find('string');
                    //is it always an array of string?
                    strings.each(function(index, string) {
                        var originalText = $(string).text();
                        var textOrNumber = convertNumericTextToNumber(originalText);
                        var textNumberOrDate = warehouseColumnDataTypes[index] === 'Date' ? window.moment(textOrNumber).format('YYYY-MM-DD') : textOrNumber;
                        if (!textNumberOrDate) {
                            if (dataTypes[index] === 'VARCHAR' && textNumberOrDate === null) {
                                textNumberOrDate = '(is missing)';
                            } else if (textNumberOrDate === null) {
                                textNumberOrDate = null;
                            }
                        }
                        jsonRow.push(textNumberOrDate);
                    });

                    // MOVE ME!!!

                    for (var i = strings.length; i < dataTypes.length; ++i) {
                        var fillValue;

                        if (dataTypes[i] !== 'VARCHAR') {
                            fillValue = null;
                        } else {
                            fillValue = '(is missing)';
                        }

                        jsonRow.push(fillValue);
                    }

                    jsonRows.push(jsonRow);
                });

                $executeQueryInSpaceResult.find('columnNames').first().find('string').each(function(index, string) {
                    columnNames.push($(string).text());
                });
                $executeQueryInSpaceResult.find('displayNames').first().find('string').each(function(index, string) {
                    displayNames.push($(string).text());
                });

                json = {
                    rows: jsonRows,
                    columnNames: columnNames,
                    displayNames: displayNames,
                    dataTypes: dataTypes,
                    warehouseColumnDataTypes: warehouseColumnDataTypes,
                    columnFormats: columnFormats,
                    hasMoreRows: $executeQueryInSpaceResult.find('hasMoreRows').first().text()
                };

                success(json);
            },

            error
        );
    };

    AdhocWebService.prototype.saveFile = function(directory, filename, file, success, error) {
        filename = !~filename.indexOf('.viz.dashlet') ? filename + '.viz.dashlet' : filename;

        if (typeof file !== 'string') {
            file = JSON.stringify(file);
        }

        this.post(
            '"urn:saveFile"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:saveFile xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:path>' + _.htmlEscape(directory) + '</ns:path>' +
            '<ns:filename>' + _.htmlEscape(filename) + '</ns:filename>' +
            '<ns:data><![CDATA[' + file + ']]></ns:data>' +
            '</ns:saveFile>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                // var file = $(xml).find('Result').text();
                // if (file) {
                //   file = JSON.parse(file);
                // }

                success();

            },

            error
        );
    };

    AdhocWebService.prototype.readFile = function(directory, filename, success, error) {
        filename = !~filename.indexOf('.viz.dashlet') ? filename + '.viz.dashlet' : filename;

        this.post(
            '"urn:readFile"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<SOAP-ENV:Body>' +
            '<ns:readFile xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:path>' + _.htmlEscape(directory) + '</ns:path>' +
            '<ns:filename>' + _.htmlEscape(filename) + '</ns:filename>' +
            '</ns:readFile>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                var file = $(xml).find('Result').text();

                success(file);
            },

            error
        );
    };

    ///make services call to get user name
    AdhocWebService.prototype.postClient = function(urn, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: '/SMIWeb/services/ClientInitData.ClientInitDataHttpSoap11Endpoint/',
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': urn,
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(success || noOp).error(error || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: '/SMIWeb/services/ClientInitData.ClientInitDataHttpSoap11Endpoint/',
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: success,
                error: error
            });
        } else {
            error();
        }
    };

    AdhocWebService.prototype.getUserName = function(success, error) {
        this.postClient(
            '"urn:getDashboardSettings"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body/>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    if (e.toString().substring(0, 19) === 'Error: Invalid XML:') {
                        error(visualizer.misc.errors.ERR_LOGGED_OUT);
                    } else {
                        error(e.toString());
                    }
                }

                var UserName = $(xml).find('Username').text();
                //this value comes from the 'FreeTrial' row in the XML; Gurinder will make sure val returned right
                var FreeTrial = $(xml).find('IsDiscoverFreeTrial').text();

                success([UserName, FreeTrial]);
            },

            error
        );
    };

    ///make services call to AdminService.asmx

    AdhocWebService.prototype.postAdminService = function(SOAPAction, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: '/AdminService.asmx',
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': SOAPAction,
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(success || noOp).error(error || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: '/AdminService.asmx',
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: success,
                error: error
            });
        } else {
            error();
        }
    };

    //get user preferences in terms of showing help

    AdhocWebService.prototype.getPreferences = function(success, error) {
        this.postAdminService(
            '"http://www.birst.com/getVisualizerHelpPreference"',
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bir="http://www.birst.com/">' +
            '<soapenv:Header/>' +
            '<soapenv:Body>' +
            '<bir:getVisualizerHelpPreference/>' +
            '</soapenv:Body>' +
            '</soapenv:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                var preference = $(xml).find('string');

                success(preference.text());
            },

            error
        );
    };

    AdhocWebService.prototype.setPreferences = function(isTrue) {
        this.postAdminService(
            '"http://www.birst.com/setVisualizerHelpPreference"',
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bir="http://www.birst.com/">' +
            '<soapenv:Header/>' +
            '<soapenv:Body>' +
            '<bir:setVisualizerHelpPreference>' +
            '<bir:value>' + isTrue + '</bir:value>' +
            '</bir:setVisualizerHelpPreference>' +
            '</soapenv:Body>' +
            '</soapenv:Envelope>'
        );
    };


    //get nav menu perms

    AdhocWebService.prototype.getNavBarAccess = function(success, error) {
        this.postAdminService(
            '"http://www.birst.com/GetLoggedInSpaceDetails"',
            '<SOAP-ENV:Envelope  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:GetLoggedInSpaceDetails xmlns:ns="http://www.birst.com/">' +
            '</ns:GetLoggedInSpaceDetails>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                var adminInNav = $(xml).find('Administrator').text() === 'true' ? true : false;
                var DesignerInNav = $(xml).find('EnableAdhoc').text() === 'true' ? true : false;
                var DashboardInNav = $(xml).find('EnableDashboards').text() === 'true' ? true : false;
                var spaceName = $(xml).find('SpaceName').text();
                var navBar = {
                    'adminInNav': adminInNav,
                    'DesignerInNav': DesignerInNav,
                    'DashboardInNav': DashboardInNav,
                    'spaceName': spaceName
                };

                success(navBar);
            },

            error
        );
    };

    AdhocWebService.prototype.evaluateExpression = function(bql, success, error) {
        this.post(
            '"urn:evaluateExpression"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:evaluateExpression xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
            '<ns:expr>' + _.htmlEscape(bql) + '</ns:expr>' +
            '</ns:evaluateExpression>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }
                success($(xml).find("ErrorCode").text(), $(xml).find("ErrorMessage").text());
            },

            error
        );
    };

    visualizer.webservices.AdhocWebService = AdhocWebService;

})();

angular.module('visualizer')
    .service('AdhocWebService', ['$http', '$q', 'chartStateService', visualizer.webservices.AdhocWebService]);
