(function() {

    var $ = window.$;

    if (angular && angular.element) {
        //$ = angular.element;
    }

    function PreferencesService(configuration) {
        configuration = configuration || {};

        this.$http = configuration.httpService;
        this.$q = configuration.promiseService;

        if (this.$http === undefined && angular && angular.injector) {
            this.$http = angular.injector(['ng']).get('$http');
        }

        this.keepAlive = false;
    }

    PreferencesService.prototype.post = function(urn, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        var timedOut = false;
        var completed = false;

        function successWrapper(xml) {
            completed = true;

            if (!timedOut && typeof success === 'function') {
                success(xml);
            }
        }

        function errorWrapper(xml) {
            completed = true;

            if (!timedOut && typeof error === 'function') {
                error(xml);
            }
        }

        setTimeout(function() {
            if (completed) {
                return;
            }

            timedOut = true;

            // if (window.$scope) {
            //     window.$scope.$broadcast('error', {
            //         basicText: visualizer.misc.errors.ERR_QUERY_TO,
            //         moreInfoText: ''
            //     });

            //     window.$scope.$apply();
            // }

            if (typeof error === 'function') {
                error('query timed out');
            }
        }, 60000);

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': urn,
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(successWrapper || noOp).error(errorWrapper || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: successWrapper,
                error: errorWrapper
            });
        } else {
            error();
        }
    };

    PreferencesService.prototype.baseUrl = '/PreferencesService.asmx';

    PreferencesService.prototype.getUserPreferences = function(success, error) {
        this.post(
            '"http://www.birst.com/getUserPreferences"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<tns:getUserPreferences xmlns:tns="http://www.birst.com/">' +
            '<tns:preferenceKeys>' +
            '<tns:string>Designer_LastUsedFolder</tns:string>' +
            '</tns:preferenceKeys>' +
            '</tns:getUserPreferences>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                success($(xml).find('UserPreference'));
            },

            error
        );
    };

    window.PreferencesService = PreferencesService;

})();

angular.module('visualizer')
    .service('PreferencesService', ['$http', '$q', window.PreferencesService]);
