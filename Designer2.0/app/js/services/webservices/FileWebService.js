(function() {

    var $ = window.$;

    if (angular && angular.element) {
        //$ = angular.element;
    }

    function FileWebService(configuration) {
        configuration = configuration || {};

        this.$http = configuration.httpService;
        this.$q = configuration.promiseService;

        if (this.$http === undefined && angular && angular.injector) {
            this.$http = angular.injector(['ng']).get('$http');
        }

        this.keepAlive = false;
    }

    FileWebService.prototype.post = function(urn, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        var timedOut = false;
        var completed = false;

        function successWrapper(xml) {
            completed = true;

            if (!timedOut && typeof success === 'function') {
                success(xml);
            }
        }

        function errorWrapper(xml) {
            completed = true;

            if (!timedOut && typeof error === 'function') {
                error(xml);
            }
        }

        setTimeout(function() {
            if (completed) {
                return;
            }

            timedOut = true;

            if (typeof error === 'function') {
                error('query timed out'); // do not change, error handlers depend on this
            }
        }, 60000);

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': urn,
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(successWrapper || noOp).error(errorWrapper || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: successWrapper,
                error: errorWrapper
            });
        } else {
            error();
        }
    };

    FileWebService.prototype.baseUrl = '/SMIWeb/services/File.FileHttpSoap11Endpoint/';

    FileWebService.prototype.getVisibleFolderStructure = function(writeable, success, error) {
        writeable = writeable || 'false';

        this.post(
            '"urn:getVisibleFolderStructure"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:getVisibleFolderStructure xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:writeable>' + writeable + '</ns:writeable>' +
            '</ns:getVisibleFolderStructure>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                var json = {};

                function addDirectories(parent, $directory) {
                    parent.children = parent.children || [];

                    _.each($directory.children('DirectoryNode').get(), function(directoryNode) {
                        var $directoryNode = $(directoryNode);

                        var directory = {
                            name: $directoryNode.attr('n'),
                            path: $directoryNode.attr('p'),
                            open: false
                        };

                        addDirectories(directory, $directoryNode.children('ch'));

                        parent.children.push(directory);
                    });
                }

                var top = $(xml).find('Result');

                addDirectories(json, top);

                json = json.children[0];
                json.name = 'My Reports';
                json.path = '/';
                json.open = true;

                success(json);
            },

            error
        );
    };

    FileWebService.prototype.getDirectory = function(directory, writeable, success, error) {
        directory = directory || '/';
        writeable = writeable || 'false';

        this.post(
            '"urn:getDirectory"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:getDirectory xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:dir>' + _.htmlEscape(directory) + '</ns:dir>' +
            '<ns:writeable>' + writeable + '</ns:writeable>' +
            '</ns:getDirectory>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                var directory = [];

                var fileNodes = $(xml).find('Result FileNode ch').children('FileNode');

                _.each(fileNodes, function(node) {
                    var $node = $(node);

                    // ignore if not directory and also not viz.dashlet file
                    if (!~$node.attr('n').indexOf('.viz.dashlet') && $node.attr('dir') !== 'true') {
                        return;
                    }

                    var fileNode = {
                        fileName: $node.attr('n'),
                        reportName: $node.attr('l'),
                        type: $node.attr('dir') === 'true' ? 'folder' : 'file',
                        owner: $node.attr('CreatedBy'),
                        modified: $node.attr('m'),
                        dashboard: $node.attr('dash'),
                        write: $node.attr('w'),
                        editing: false
                    };

                    directory.push(fileNode);
                });

                success(directory);
            },

            error
        );
    };

    FileWebService.prototype.getDirectoryWithSuffix = function(directory, writeable, suffix, success, error) {
        directory = directory || 'shared';
        writeable = writeable || 'false';
        suffix = suffix || 'viz';

        this.post(
            '"urn:getDirectoryWithSuffix"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:getDirectoryWithSuffix xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:dir>' + _.htmlEscape(directory) + '</ns:dir>' +
            '<ns:writeable>' + _.htmlEscape(writeable) + '</ns:writeable>' +
            '<ns:validSuffixes>' + suffix + '</ns:validSuffixes>' +
            '</ns: getDirectoryWithSuffix>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }
                console.log('this method doesnt work yet', xml);

                // var directory = [ ];

                // var fileNodes = $(xml).find('Result FileNode ch').children('FileNode');

                // _.each(fileNodes, function(node) {
                //     var $node = $(node);

                //     var fileNode = {
                //         file: $node.attr('n'),
                //         name: $node.attr('l'),
                //         type: $node.attr('dir') === 'true' ? 'folder' : 'file',
                //         owner: $node.attr('CreatedBy'),
                //         modified: $node.attr('m'),
                //         dashboard: $node.attr('dash'),
                //         write: $node.attr('w')
                //     };

                //     directory.push(fileNode);
                // });

                //success(directory);
            },

            error
        );
    };

    FileWebService.prototype.createDirectory = function(directory, newFolderName, success, error) {
        directory = directory || 'shared';

        this.post(
            '"urn:createDirectory"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:createDirectory xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:dirName>' + _.htmlEscape(newFolderName) + '</ns:dirName>' +
            '<ns:relativeFilePath>' + _.htmlEscape(directory) + '</ns:relativeFilePath>' +
            '</ns:createDirectory>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log(xml);

                success();
            },
            error
        );
    };

    FileWebService.prototype.renameFile = function(directory, oldName, newName, success, error) {
        directory = directory || 'shared';
        newName = !~newName.indexOf('.viz.dashlet') ? newName + '.viz.dashlet' : newName;

        this.post(
            '"urn:renameFile"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:renameFile xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:newName>' + _.htmlEscape(newName) + '</ns:newName>' +
            '<ns:oldRelativeFilePath>' + _.htmlEscape(directory + '/' + oldName) + '</ns:oldRelativeFilePath>' +
            '</ns:renameFile>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log(xml);

                success();
            },
            error
        );
    };

    FileWebService.prototype.moveFiles = function(sourceDirectory, sourceFile, targetDirectory, success, error) {
        sourceFile = !~sourceFile.indexOf('.viz.dashlet') ? sourceFile + '.viz.dashlet' : sourceFile;

        this.post(
            '"urn:moveFiles"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:moveFiles xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:sourcePaths>' + _.htmlEscape(sourceDirectory + '/' + sourceFile) + '</ns:sourcePaths>' +
            '<ns:targetDirectory>' + _.htmlEscape(targetDirectory.substring(1)) + '</ns:targetDirectory>' +
            '</ns:moveFiles>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log(xml);

                success();
            },
            error
        );
    };

    FileWebService.prototype.deleteFile = function(directory, filename, success, error) {
        directory = directory || 'shared';

        this.post(
            '"urn:deleteFiles"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:deleteFiles xmlns:ns="http://file.WebServices.successmetricsinc.com">' +
            '<ns:relativeFilePaths>' + _.htmlEscape(directory + '/' + filename) + '</ns:relativeFilePaths>' +
            '</ns:deleteFiles>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {

                try {
                    xml = $.parseXML(xml);
                } catch (e) {
                    error(e.toString());
                }

                //console.log(xml);

                success();
            },
            error
        );
    };


    window.FileWebService = FileWebService;

})();

angular.module('visualizer')
    .service('FileWebService', ['$http', '$q', window.FileWebService]);
