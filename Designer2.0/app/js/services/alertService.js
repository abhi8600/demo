'use strict';

visualizer.services.alertService = function($rootScope, $alert) {
    var maxQueueLength = 9;

    // This defines a mapping from the 'categories' of alerts we invoke,
    // to a smaller set of visual styles.  This allows us to have a more
    // fine grained definition of alerts, without being too visually
    // complicated for the user.
    this.alertTypeMapping = {
        success: 'success',
        info: 'success',
        warning: 'warning',
        warn: 'warning',
        danger: 'danger',
        error: 'danger'
    };

    this.alertQueue = [];

    // This function is called for each distinct area that alerts will be displayed.  It
    // returns a function which is exposed as the alert-creation interface.
    //
    // This interface can be called 4 different ways, depending on how much information
    // needs to be conveyed:
    //
    // 1.  alertService.alert('Content here');
    // 2.  alertService.alert(['Content here', 'uniqueIdHere']);
    // 3.  alertService.alert(['Content here', 'uniqueIdHere', 'alert_type']);
    // 4.  alertService.alert({
    //         title: 'Title here',
    //         content: 'Content here',
    //         id: 'uniqueIdHere',
    //         type: '[success|warning|danger]',
    //         duration: [seconds],
    //     });
    this.alert = function(alertConfig) {
        if (alertConfig instanceof Array) {
            if (alertConfig.length === 3) {
                alertConfig = {content: alertConfig[0], id: alertConfig[1], type: alertConfig[2]};
            } else {
                alertConfig = {content: alertConfig[0], id: alertConfig[1]};
            }
        } else if (typeof alertConfig === 'string') {
            alertConfig = {content: alertConfig};
        }

        // If an alert with this ID already exists, clear the older one
        if (typeof alertConfig.id !== 'undefined') { this.clearById(alertConfig.id); }

        alertConfig = _.extend({
            container: '[alerts-chart]',
            duration: 0,
            type: 'error',
            timestamp: new Date().getTime()
        }, alertConfig);

        alertConfig.type = this.alertTypeMapping[alertConfig.type];
        $alert(alertConfig);
        $rootScope.$broadcast('alertService-newAlert', {maxNotifications: maxQueueLength});
        $rootScope.$broadcast('alertService-newAlert-' + alertConfig.type);

        this.alertQueue.unshift(alertConfig);
        if (this.alertQueue.length > maxQueueLength) {
            this.alertQueue.pop();
        }
    };

    this.clearAll = function(area) {
        // When called with no arguments, all alerts in all areas will be
        // removed.  When a specific `area` is passed, all the alerts in
        // that area will be removed.
        if (typeof area === 'undefined') {
            $rootScope.$broadcast('alertService-clearAll');
            $('[alerts-chart], [alerts-sa]').empty();
            this.alertQueue = [];
        } else {
            $('[alerts-' + area + ']').empty();
            for (var i = 0; i < this.alertQueue.length; i++) {
                if (this.alertQueue[i].container === '[alerts-' + area + ']') {
                    this.alertQueue.splice(i, 1);
                }
            }
        }
    };

    this.clearById = function(alertId) {
        // Given an alert id (with no '#' prepended), remove that alert from the
        // DOM, and remove it from the alertQueue.
        $('[alerts-chart] #' + alertId + ', [alerts-sa] #' + alertId).remove();
        for (var i = 0; i < this.alertQueue.length; i++) {
            if (this.alertQueue[i].id === alertId) {
                this.alertQueue.splice(i, 1);
                return false;
            }
        }
    };

    this.clearByType = function (type) {
        // Clears all alerts of the given type.
        for (var i = 0; i < this.alertQueue.length; i++) {
            if (this.alertQueue[i].type === type) {
                var alertId = this.alertQueue[i].id;
                if (alertId) {
                    this.clearById(alertId);
                } else {
                    this.alertQueue.splice(i, 1);
                }
                i--;
            }
        }
    };

    this.queueContainsError = function () {
        // Returns false if there are no errors, otherwise returns the index
        // of the first error.
        for (var i = 0; i < this.alertQueue.length; i++) {
            if (this.alertQueue[i].type === 'danger') {
                return i;
            }
        }
        return false;
    };
};

angular.module('visualizer').service('alertService', ['$rootScope', '$alert', visualizer.services.alertService]);
