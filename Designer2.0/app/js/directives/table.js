'use strict';
visualizer.directives.tableView = function(formatting) {
    return {
        replace: false,
        templateUrl: 'partials/designer/table.html',
        controller: function($scope, $element) {
            var results = {};
            var sortLock = {};
            var numberRowsToShow = 0;

            var findColumn = function(index) {
                if ($scope.stage.Columns && $scope.stage.Columns.length) {
                    return $scope.stage.Columns[index];
                }
            };

            var infiniteScroll = function() {
                var $container, $table, $tableHeight, $containerBottom, remaining;
                $container = $element.find('.table-container');
                $table = $element.find('table');
                $containerBottom = $container.height() + $container.scrollTop();
                $tableHeight = $table.height();
                remaining = $tableHeight - $containerBottom + 22; // 22 pixels of extra space, padding-bottom + borders
                if (remaining < 200) {
                    //console.log('add rows', results);
                    var length = $scope.table.results.rows.length;
                    var newRows = angular.copy(results.rows);
                    var newLength = length + numberRowsToShow;
                    newRows.splice(newLength);
                    $scope.table.results.rows = newRows;
                    $scope.$apply();
                }
            };

            $element.find('.table-container').on('scroll', infiniteScroll);

            $scope.table = {
                exportFormatting: function() {
                    // this method is for debugging in console and should be removed - access formatting from service, not from scope
                    return formatting.export($scope.table.formatting);
                },
                updateFormatting: function() {
                    this.formatting = formatting.getAllFormatting().table;
                },
                editMode: false,
                formatting: formatting.getAllFormatting().table,
                getFormattingOptions: function(key) {
                    return formatting.getFormattingOptions('table', key);
                },
                setTableFormatting: function() {
                    formatting.setFormatting($scope.table.formatting, 'table');
                },
                findColumn: function(index) {
                    return findColumn(index);
                },
                toggleEditMode: function() {
                    $scope.table.editMode = !$scope.table.editMode;
                },
                getStyles: function(key, index) {
                    var styles = {};
                    if (key === 'background' && $scope.table.formatting[key].switchState !== false) {
                        var color = index % 2 === 0 ? $scope.table.formatting[key].properties.backgroundColor.value : $scope.table.formatting[key].properties.altColor.value;
                        return { 'background-color': color };
                    } else if (key && $scope.table.formatting[key].switchState !== false) {
                        _.each($scope.table.formatting[key].properties, function(style) {
                            if (typeof style.key === 'object') {
                                var newKey = style.key[style.value];
                                styles[newKey] = style.value;
                            } else if (typeof style.value === 'number') {
                                styles[style.key] = style.value + 'px';
                            } else if (style.key === 'font-family') {
                                styles[style.key] = '\'' + style.value + '\', sans-serif';
                            } else {
                                styles[style.key] = style.value;
                            }
                        });
                        return styles;
                    } else if ($scope.table.formatting[key].switchState === false) {
                        if (key === 'borders') {
                            return {
                                'border-width': 0,
                                'border-color': 'transparent',
                            };
                        } else if (key === 'background') {
                            return {
                                'background-color': 'transparent'
                            };
                        }
                    }
                },
                renderTable: function(resultSet) {
                    var columns = resultSet[0].columnNames.length;
                    results = angular.copy(resultSet[0]);
                    results.alignment = new Array(columns);
                    results.showDropdown = new Array(columns);
                    results.columns = $scope.stage.Columns;
                    _.each(results.columnNames, function(col) {
                        if (!sortLock[col]) {
                            sortLock[col] = false;
                        }
                    });
                    _.each(results.dataTypes, function(type, columnIndex) {
                        var columnResults = _.pluck(results.rows, columnIndex);
                        if (_.every(columnResults, function(data) {
                            return _.isNumber(data) || _.isNull(data);
                        })) {
                            results.alignment[columnIndex] = 'right';
                            _.each(columnResults, function(number, rowIndex) {
                                if (number !== null) {
                                    results.rows[rowIndex][columnIndex] = Math.round(number * 100) / 100;
                                }
                            });
                        } else {
                            results.alignment[columnIndex] = 'left';
                        }
                    });
                    _.each(results.rows, function(row) {
                        _.each(row, function(item, index) {
                            if (results.dataTypes[index] === 'VARCHAR' && item === null) {
                                item = '';
                            } else if (item === null) {
                                item = '';
                            }
                        });
                    });
                    numberRowsToShow = Math.floor((($element.height() * 2) - 64) / 36); // 1.5 * available height, minus th height, divided by tr height
                    var initialRows = angular.copy(results.rows);
                    initialRows.splice(numberRowsToShow);
                    $scope.table.results = angular.copy(results);
                    $scope.table.results.rows = initialRows;
                },
                sort: function(index) {
                    var column = findColumn(index);
                    // if there is no sorting lock
                    if (sortLock[results.columnNames[index]] === false) {
                        if (column && !column.properties.sortDirection) {
                            _.each($scope.stage.internalSortOrder, function(col) {
                                if (!sortLock[col.name] && !sortLock[col.toBql]) {
                                    $scope.stage.internalSortOrder = _.without($scope.stage.internalSortOrder, col);
                                    col.properties.sortDirection = null;
                                }
                            });
                            $scope.stage.addToSortOrder(column);
                        } else if (column) {
                            var sort = column.properties.sortDirection;
                            column.properties.sortDirection = sort === 'ASC' ? 'DESC' : 'ASC';
                        }
                        // if there is a sorting lock
                    } else {
                        //var bql = $scope.stage.getBQL(column);
                    }
                    $scope.updateChart();
                },
                lockSorting: function(index) {
                    sortLock[results.columnNames[index]] = true;
                },
                removeColumn: function(index) {
                    var col = findColumn(index);
                    $scope.removeColumn(col, $scope.chartType);
                    $scope.updateChart();
                },
                filter: function(index) {
                    var column = findColumn(index);
                    var elem = $element.closest('[designer]');
                    var $filter = elem.find('[menu-parent="Filters"]');
                    setTimeout(function() {
                        if (!$filter.hasClass('active-stage-parent')) {
                            $filter.click();
                        }
                        elem.find('[column-name="' + column.name + '"]').find('[ng-click="filterDialog(column)"]').click();
                    }, 100);
                },
                toggleTableDropdown: function(index) {
                    $scope.table.results.showDropdown[index] = !$scope.table.results.showDropdown[index];
                },
                drop: _.memoizeObject(function(index) {
                    return {
                        "over": function() {
                            $element.find('th').eq(index).addClass('move');
                        },
                        "out": function() {
                            $element.find('th').eq(index).removeClass('move');
                        },
                        start: function() {},
                        drop: function(column) {
                            $element.find('th').eq(index).removeClass('move');
                            var oldIndex = _.indexOf(results.columnNames, column);
                            if (oldIndex !== index) {
                                $scope.stage.tableSwapItems(oldIndex, index);
                                $scope.updateChart();
                            }
                        }
                    };
                })
            };
        }
    };
};

angular.module('visualizer').
directive('tableView', ['formatting', visualizer.directives.tableView ]);
