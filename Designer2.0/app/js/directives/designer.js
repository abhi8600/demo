/* Designer Directive */
visualizer.directives.designer = function(queryService, chartStateService, $q, globalStateService, AdhocWebService, adhocConverter, ReportImporter, alertService, $timeout) {
    return {
        scope: true,
        controller: function($scope, $element, $routeParams) {

            // Feature Flags - add keys to this object, and reference them when you
            // need to detect if a feature is enabled.
            // The value of $scope.flag.{your_flag} will be set to query parameter value.
            $scope.flags = {
                table: false,
                messageCenter: false,
                loaders: true
            };
            for (var paramKey in $routeParams) {
                $scope.flags[paramKey] = $routeParams[paramKey];
            }

            // fallback for iframe
            function getURLParameterByName(name) {
                var match = (new RegExp('[?&]' + name + '=([^&]*)')).exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }

            $scope.loaders = {};

            $scope.reportName = $routeParams.reportName || getURLParameterByName('reportName');

            //console.log('trying to get ', $scope.reportName);
            console.log("BPD-23409 - reportName is", $scope.reportName);

            var firstSubjectArea = true;

            (function visualizerInit() {
                //if 'dashlet' is in URL, hide the header cause we're running in iframe
                if (globalStateService.determineLaunchState() === 'pivot') {
                    $scope.hideHeader = true;
                }

                // Shared state, controlled by directives on subtemplates
                $scope.customExpressionObject = [];
                // The subject area currently selected
                $scope.subjectArea = null;

                // The set of columns that have been staged for charting
                $scope.stage = new visualizer.Stage();
                $scope.updateStage = function(newStage) {
                    $scope.stage = newStage;
                };
                $scope._ = _;
                $scope.chartType = 'column';
                $scope.launchMode = globalStateService.getProductLaunchedAs();

                $scope.trashBucket = {
                    columns: [],
                    dragToTrashMode: false
                };
            })();

            function pivotControlInit() {
                setTimeout(function() {
                    $element.find('[menu-parent="Recent"]').click();
                }, 100);
                globalStateService.getReportData().then(function(report) {
                    try {
                        try {
                            if (_.indexOf(chartStateService.getNativeChartTypes(), report.chartType) > -1) {
                                chartStateService.setCurrentChartType(report.chartType);
                            }
                        } catch (f) {
                            console.log('Failed to set chart type: ', f);
                        }

                        ReportImporter.import(report, $scope);
                    } catch (e) {
                        console.log("BPD-23409 - error thrown during report import", e.toString());
                    }
                });
            }

            function standaloneInit() {
                setTimeout(function() {
                    AdhocWebService.getUserName(
                        function(data) {
                            var userName = data[0];
                            globalStateService.setMyName(userName);
                            $element.find("#myName").text(userName);

                            var isTrialUser = data[1];
                            if (isTrialUser === 'true') {
                                globalStateService.setProductLaunchedAs('standalone_trial');
                            }
                            if (globalStateService.hasSavePermissions()) {
                                $scope.hasSavePermissions = true;
                            }
                            if (globalStateService.hasBQLEnabled()) {
                                $scope.hasEditorPermissions = true;
                            }
                            $scope.safeApply();
                        },
                        function(errorCause) {
                            if (errorCause === 'query timed out') {
                                // $scope.$broadcast('error', {
                                //     basicText: visualizer.misc.errors.ERR_QUERY_TO,
                                //     moreInfoText: ''
                                // });
                                alertService.alert([visualizer.misc.errors.ERR_QUERY_TO, 'error-standaloneOtherError']);
                            } else if (errorCause === visualizer.misc.errors.ERR_LOGGED_OUT) {
                                // $scope.$broadcast('error', {
                                //     basicText: errorCause,
                                //     moreInfoText: ''
                                // });
                                alertService.alert([visualizer.misc.errors.ERR_LOGGED_OUT, 'error-loggedOut']);
                            } else {
                                // $scope.$broadcast('error', {
                                //     basicText: errorCause,
                                //     moreInfoText: ''
                                // });
                                alertService.alert([errorCause, 'error-standaloneOtherError']);
                            }
                            $scope.$apply();
                        }
                    );
                }, 100);
            }

            if ($scope.reportName && globalStateService.useInternalWebServices()) {
                var deferred = $q.defer();
                var pivotMode = $routeParams.pivot;
                if (!pivotMode) {
                    standaloneInit();
                }

                globalStateService.setReportData(deferred.promise);

                if ($scope.reportName && $scope.reportName.match(/\.viz\.dashlet$/)) {
                    var parts = $scope.reportName.split("/");
                    var fileName = parts.pop();
                    var folder = parts.join("/") + "/";
                    // fileSystem is not on scope yet, so use a setTimeout I guess
                    setTimeout(function() {
                        $scope.fileSystem.open(folder, fileName);
                    }, 10);
                } else {
                    try {
                        AdhocWebService.getReport($scope.reportName,
                            // read in report later, use later once subj area done populating

                            function(xml, reportName) {
                                var convertedFile = adhocConverter.convert(xml, reportName);
                                var data = convertedFile.report;
                                var errorMsg = convertedFile.errorMsg;

                                //console.log('success: ', data);
                                deferred.resolve(data);
                                if (globalStateService.runningFirstTime()) {
                                    pivotControlInit();
                                }

                                setTimeout(function() {
                                    if (errorMsg && errorMsg.basicText) {
                                        //$scope.$broadcast('error', errorMsg);
                                        alertService.alert([errorMsg.basicText, 'error-convertedFile']);
                                        $scope.safeApply();
                                    }
                                }, 500);
                            },

                            function(errorCause) {
                                if (errorCause === 'query timed out') {
                                    // $scope.$broadcast('error', {
                                    //     basicText: visualizer.misc.errors.ERR_QUERY_TO,
                                    //     moreInfoText: ''
                                    // });
                                    alertService.alert([visualizer.misc.errors.ERR_QUERY_TO, 'error-getReportQueryTimedOut']);
                                } else {
                                    // $scope.$broadcast('error', {
                                    //     basicText: visualizer.misc.errors.UNKNOWN_ERROR,
                                    //     moreInfoText: ''
                                    // });
                                    alertService.alert([errorCause, 'error-getReportOtherError']);
                                }

                                $scope.$apply();

                                //console.log('error during request for columns from report ' + $scope.reportName);
                            }
                        );
                    } catch (e) {
                        console.log('error while attempting to load columns from report ' + $scope.reportName);
                    }
                }
            } else if (globalStateService.determineLaunchState() === 'standalone' || globalStateService.determineLaunchState() === 'dev') {
                standaloneInit();
            }

            $scope.loadAnim = function(toggle, queryVariation) {
                var queryRunning = queryVariation ? 'query-running' : 'first-load';
                if (toggle === 'On') {
                    if ($("#loadAnimDiv").length === 0) {
                        $("body").append('<div class="modal-backdrop fade in loadAnim ' + queryRunning + '" id="loadAnimDiv"><div><div width="60" height="60" class="birst-spinner"></div></div></div>');
                    } else {
                        $("#loadAnimDiv").show();
                    }
                } else {
                    $("#loadAnimDiv").hide();
                }
            };

            $scope.errorHandler = function(errorObject, eraseChart) {
                //$scope.$broadcast('error', errorObject);
                alertService.alert({
                    content: errorObject.basicText,
                    extra: errorObject.moreInfoText,
                    id: 'error-errorHandlerOtherError'
                });
                if (eraseChart) {
                    if ($scope.flags.loaders) {
                        // Depending on whether errors are thrown "upstream" or "downstream" from
                        // the current scope, need to both $emit and $broadcast.
                        $scope.$broadcast('loader-chart-hide');
                        $scope.$emit('loader-chart-hide');
                    }
                    $scope.$broadcast('blankChart', []);
                }
            };

            $scope.safeApply = function() {
                var phase = this.$parent.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    return;
                } else {
                    $scope.$apply();
                }
            };

            this.setSubjectArea = function(subjectArea) {
                if (!$scope.fileSystem.loadingFile && !firstSubjectArea) {
                    $scope.clearStage();
                    firstSubjectArea = false;
                }
                $scope.fileSystem.loadingFile = false;
                $scope.subjectArea = subjectArea;
                $scope.subjectAreaConcat = [].concat($scope.subjectArea.measures).concat($scope.subjectArea.attributes).concat($scope.subjectArea.uncategorized).concat(chartStateService.reportExpressions);
            };

            var queryHandle = null;

            $scope.updateChart = function() {
                setTimeout(function() {

                    $scope.$broadcast('blankChart');

                    var canceled = false;

                    if (queryHandle) {
                        queryHandle();
                    }

                    queryHandle = function() {
                        canceled = true;
                    };

                    if ($scope.stage.allColumns().length > 1) {
                        if ($scope.flags.loaders) {
                            $scope.$broadcast('loader-chart-show');
                            alertService.alert({
                                container: '[alerts-chart]',
                                content: visualizer.misc.feedback.FEEDBACK_RENDERING_CHART,
                                type: 'success',
                                id: 'success-rendering-chart',
                                duration: 3
                            });

                            $scope.loaders.chartTimeout = $timeout(function () {
                                // Set 30 second filtering timeout.  Note that the error alert
                                // triggers the removal of the loader.
                                alertService.alert({
                                    container: '[alerts-chart]',
                                    content: visualizer.misc.errors.ERR_CHART_TIMEOUT,
                                    id: 'error-rendering-chart'
                                });
                            }, 60000);
                        } else {
                            $scope.animTimeout = setTimeout(function() {
                                $scope.loadAnim('On', true);
                                //fail safe -- if for some reason the spinner fails to be turned off after 5 seconds, kill it
                                setTimeout(function() {
                                    $scope.loadAnim('Off', true);
                                }, 5000);
                            }, 500);
                        }
                    }

                    queryService.runQueriesForStage($scope.stage, function(resultSets) {
                        if (!canceled) {
                            queryHandle = null;

                            if ($scope.chartType !== 'table' && resultSets && resultSets[0] && resultSets[0].rows.length > 999) {
                                $scope.$broadcast('blankChart', []);

                                // $scope.$broadcast('error', {
                                //     basicText: visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER,
                                //     moreInfoText: ''
                                // });
                                alertService.alert([visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER, 'error-resultSetTooLarge']);

                                $scope.safeApply();
                            } else {
                                $scope.stage.addMinMax(resultSets);
                                $scope.stage.addDataTypeToColumns(resultSets);
                                $scope.$broadcast('updateChart', resultSets);
                            }
                        }
                    }, $scope);

                    //Update Color grouping and colorDistributionControl
                    $scope.stage.serieColorGroupings();

                }, 0);
            };

            $scope.$on('runNewQueries', function() {
                $scope.updateChart();
            });

            $scope.$watch(function() { return chartStateService.reportExpressions; }, function() {
                if($scope.subjectArea) {
                    $scope.subjectAreaConcat = [ ].concat($scope.subjectArea.measures).concat($scope.subjectArea.attributes).concat($scope.subjectArea.uncategorized).concat(chartStateService.reportExpressions);
                }
            }, true);

            $scope.deleteReportExpression = function(reportExpression) {
                chartStateService.reportExpressions = chartStateService.reportExpressions.filter(function(it) {
                    return it !== reportExpression;
                });
                // remove any instances of the report expression from
                // "recently used".  We don't expect there to be any
                // instances in stage, because that is checked for
                // before calling delete
                $scope.trashBucket.columns = $scope.trashBucket.columns.filter(function(column) {
                    return column.reportExpressionId !== reportExpression.reportExpressionId;
                });
                $scope.subjectAreaConcat = [ ].concat($scope.subjectArea.measures).concat($scope.subjectArea.attributes).concat($scope.subjectArea.uncategorized).concat(chartStateService.reportExpressions);
            };

            $scope.editReportExpression = function(reportExpression) {
                window.openBqlEditor($, visualizer.BqlNode, $scope.subjectArea, reportExpression.expression.expressionToString(), reportExpression.label, reportExpression.category === 'measure', function(newName, newColumn, isMeasure) {
                    newName = makeUniqueReportExpressionName(newName, reportExpression);
                    reportExpression.label = newName;
                    reportExpression.displayName = newName;
                    reportExpression.expression = newColumn.evaluate();
                    reportExpression.category = isMeasure ? 'measure' : 'attribute';
                    $scope.stage.allColumnsAndUnvisualized().map(function(col) {
                        if(col.reportExpressionId === reportExpression.reportExpressionId) {
                            col.name = newName;
                            col.expression = reportExpression.expression;
                        }
                    });
                    $scope.$apply();
                    $scope.updateChart();
                }, function() { AdhocWebService.evaluateExpression.apply(AdhocWebService, arguments); });
            };

            this.stageColumn = function(column) {
                if (!column.disabled) {
                    $scope.$broadcast('stageColumn', column);
                }
            };

            $scope.$on('stageColumn', function (event, column, dim, type) {
                $scope.stage.addColumn( column, dim, type || chartStateService.getCurrentChartType() );
                $scope.updateChart();
                $scope.searchVariable.string = '';
                $scope.searchGrouped = [ [], [], [] ];
            });

            function makeUniqueReportExpressionName(newName, existing) {
                var usedNames = chartStateService.reportExpressions.filter(function(exp) { return exp !== existing; }).map(function(exp) {
                    return exp.label;
                });
                // if the name is already used, add a (1) (2) etc.
                var origName = newName, idx = 1;
                while(usedNames.indexOf(newName) > -1) {
                    newName = origName + " (" + idx++ + ")";
                }
                return newName;
            }

            $scope.openBqlEditor = function() {
                window.openBqlEditor($, visualizer.BqlNode, $scope.subjectArea, null, null, true, function(newName, newColumn, isMeasure) {
                    newName = makeUniqueReportExpressionName(newName);
                    chartStateService.reportExpressions.push({
                        label: newName,
                        displayName: newName,
                        type: 'report expression',
                        expression: newColumn.evaluate(),
                        category: isMeasure ? 'measure' : 'attribute',
                        reportExpressionId: chartStateService.reportExpressionId()
                    });

                    $scope.subjectAreaConcat = [].concat($scope.subjectArea.measures).concat($scope.subjectArea.attributes).concat($scope.subjectArea.uncategorized).concat(chartStateService.reportExpressions);
                    $scope.$apply();
                }, function() {
                    AdhocWebService.evaluateExpression.apply(AdhocWebService, arguments);
                });
            };
        }
    };
};

angular.module('visualizer')
    .directive('designer', ['queryService', 'chartStateService', '$q', 'globalStateService', 'AdhocWebService', 'adhocConverter', 'ReportImporter', 'alertService', '$timeout', visualizer.directives.designer]);
