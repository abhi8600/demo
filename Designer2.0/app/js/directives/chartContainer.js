visualizer.directives.chartContainer = function() {
    return {
        replace: true,
        templateUrl: 'partials/designer/chartContainer.html'
    };
};

angular.module('visualizer').
directive('chartContainer', [visualizer.directives.chartContainer]);
