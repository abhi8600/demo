visualizer.directives.vizMenu = function() {
    return {
        link: function(scope, element) {
            scope.navCurrentZIndex = 100;
            // subject area search variable
            scope.searchVariable = {
                string: ''
            };
            scope.activeSearch = false;
            scope.menuHandler = function(event) {
                var $target = element.find(event.target);
                // various targets that should not be part of menuHandler's logic
                if ($target.attr('menu-back') !== undefined ||
                    $target.parent().attr('menu-back') !== undefined ||
                    $target.hasClass('apply') ||
                    $target.hasClass('reset') ||
                    $target.closest('div[date-picker]').length) {
                    return;
                }
                var activeMenusSubjectArea = element.find('[subject-areas] [menu-child].active');
                var activeMenusStaging = element.find('[staging] [menu-child].active');

                // if you're in staging and click into subject area, close subject area
                if (activeMenusSubjectArea.length && $target.closest('[staging]').length) {
                    activeMenusSubjectArea.removeClass('active');
                }
                // if you're in subject area and click into staging, close staging and staging child menus
                if (activeMenusStaging.length && $target.closest('[subject-areas]').length) {
                    activeMenusStaging.removeClass('active');
                    activeMenusStaging.closest('.active-stage-parent').removeClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
                }
                // auto close sub-menu after clicking a "measure item", which is the lowest level to be in
                if ($target.attr('measure-item') !== undefined || $target.parent().attr('measure-item') !== undefined) {
                    $target.closest('[menu-child="nested"]').removeClass('active');
                    return;
                }

                var parent = $target.closest('[menu-parent]');
                // specific rules for staging menu logic
                if (parent.closest('[staging]').length) {
                    if (!parent.children('[menu-child="nested"]').length) {
                        if (parent.attr('menu-parent') === 'Summary') {
                            activeMenusStaging.removeClass('active');
                            activeMenusStaging.closest('.active-stage-parent').removeClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
                            return;
                        } else if (parent.hasClass('active-stage-parent') && !$target.hasClass('column-icon-preview')) {
                            if ($target.closest('[menu-child]').length) {
                                return;
                            }
                            //console.log('preview open')
                            parent.removeClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
                            parent.find('.active').removeClass('active');
                            return;
                        } else {
                            element.find('.active-stage-parent').removeClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
                            parent.addClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
                            element.prev('[toolbar]').find('input[search]').blur();
                        }
                    }
                }

                // open the child menu
                var child = parent.children('[menu-child]');
                child.addClass('active').css('z-index', scope.navCurrentZIndex++);

                if ($target.hasClass('column-icon-preview')) {
                    element.find('[staging] [column-name="' + $target.prev().html() + '"] [menu-parent="Chart-Types"] [menu-child]').addClass('active').css('z-index', scope.navCurrentZIndex++);
                    return;
                }

                // if navigating from one stage section to another, close the first after opening the second
                if (activeMenusStaging.length && $target.closest('[staging]>[menu-parent]>.stage-button').length) {
                    activeMenusStaging.removeClass('active');
                }

            };

            scope.returnToMenu = function() {
                var activeMenusStaging = element.find('[staging] [menu-child].active');

                element.find('[menu-child].active', '[subject-areas]').removeClass('active');
                activeMenusStaging.removeClass('active');
                activeMenusStaging.closest('.active-stage-parent').removeClass('active-stage-parent').children('.stage-button').children('i').toggleClass('unselected selected');
            };

            scope.closeMenu = function(event) {
                var $target = element.find(event.target);
                var $parent = $target.closest('[ng-click="closeMenu($event)"]').parent();
                var $grandParent = $parent.parent();

                $parent.removeClass('active');
                // logic specific for staging
                if ($grandParent.hasClass('active-stage-parent')) {
                    $grandParent.removeClass('active-stage-parent');
                }
            };

            scope.$on('openDisplayAggregationsInStage', function(event, colName) {
                // Close currently active menu
                element.find("[menu-child].active").removeClass("active"); 
                element.find(".active-stage-parent").removeClass(".active-stage-parent").children(".stage-button").children("i").toggleClass("unselected selected");
                
                var columnListing = element.find('[staging] [column-name="' + colName + '"]').filter('[staging-type="measure"]');
                // Open the particular column
                columnListing.closest('[menu-parent]').children('[menu-child]').addClass('active').css('z-index', scope.navCurrentZIndex++);
                // Now open the sub-menu for display aggregations
                columnListing.find('[menu-parent="Display Aggregation"] [menu-child]').addClass('active').css('z-index', scope.navCurrentZIndex++);
            });
        }
    };
};

visualizer.directives.menuChild = function() {
    return {
        transclude: true,
        template: '<div><div menu-back ng-click="closeMenu($event)"><i class="glyphicon glyphicon-chevron-left"></i>back</div><div ng-transclude></div></div>',
        replace: true
    };
};

angular.module('visualizer')
    .directive('vizMenu', visualizer.directives.vizMenu)
    .directive('menuChild', visualizer.directives.menuChild);
