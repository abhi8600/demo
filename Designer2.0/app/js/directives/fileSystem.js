
'use strict';
visualizer.directives.fileSystem = function(chartStateService, AdhocWebService, FileWebService, PreferencesService, alertService, formatting) {

    var migrations = [
        {
            version: 2,
            update: function(file) {
                return JSON.stringify(JSON.parse(file, function(k, v) {
                    if(k === 'stage') {
                        return visualizer.Stage.fromJSON(v);
                    }
                    if(v && v.export) {
                        v.expression = visualizer.BqlNode.fromBqlExportColumn(v);
                        delete v.export;
                    }
                    return v;
                }));
            }
        },
        {
            version: 3,
            update: function(file) {
                return JSON.stringify(JSON.parse(file, function(k, v) {
                    if(k && k._class === 'Stage') {
                        v = visualizer.Stage.fromJSON(v);
                        _.each(v.allColumns(), function(column) {
                            var filter = column.filter || column.displayFilter;
                            if (filter && filter.type === 'range' && filter.subType === '=') {
                                filter.subType = 'Between';
                            }
                            if (filter && filter.type === 'date' && filter.subType === '=' && filter.items[0].operator === '>' && filter.items[1].operator === '<') {
                                filter.subType = 'Between';
                            }
                        });
                        return v;
                    } else if(v && v._class === 'BqlNode') {
                        return visualizer.BqlNode.fromJSON(v);
                    } else {
                        return v;
                    }
                }));
            }
        }
    ];

    return {
        link: function(scope, element) {
            var visualizerVersion = 2;
            var fileWebService = FileWebService;
            var adhocWebService = AdhocWebService;
            var preferencesService = PreferencesService;

            var error = function(cause) {
                if (cause === 'query timed out') {
                    // scope.$broadcast('error', {
                    //     basicText: visualizer.misc.errors.ERR_QUERY_TO,
                    //     moreInfoText: ''
                    // });
                    alertService.alert([visualizer.misc.errors.ERR_QUERY_TO, 'error-fileSystemOtherError']);
                    scope.safeApply();
                } else {
                    console.log('file system get directory error!');

                    // scope.$broadcast('error', {
                    //     basicText: visualizer.misc.errors[cause] || 'An unknown error occurred.',
                    //     moreInfoText: ''
                    // });
                    var loggedOut;
                    var errorId = 'error-fileSystemMiscError';
                    if (cause.toString().substring(0, 19) === 'Error: Invalid XML:') {
                        loggedOut = visualizer.misc.errors.ERR_LOGGED_OUT;
                        errorId = 'error-loggedOut';
                    }
                    alertService.alert([
                        loggedOut || visualizer.misc.errors[cause] || 'An unknown error occurred.',
                        errorId
                    ]);
                    scope.safeApply();
                }
            };

            var addColumns = function(file) {
                scope.updateStage(file.stage);
                scope.stage.updateSortOrder();
                var MAX = _.max(_.map(scope.stage.allColumnsAndUnvisualized(), function(col) { return col.uniqueId; }));
                for (var i = 0; i < MAX; i++ ) {
                    _.uniqueId();
                }
            };

            var updateBasicStageProperties = function(file) {
                chartStateService.setPreviousDimensionValues(file.previousDimensionValues);
                if(file.reportExpressions) {
                    chartStateService.reportExpressions = file.reportExpressions;
                }
            };

            var addFormatting = function(file) {
                if (file.title) {
                    chartStateService.setTitle(file.title);
                }
                if (file.formatting && file.formatting.title) {
                    chartStateService.setChartFormat('Title', file.formatting.title);
                }
                if (file.formatting && file.formatting.xAxis) {
                    chartStateService.setChartFormat('XAxis', file.formatting.xAxis);
                }
                if (file.formatting && file.formatting.yAxis) {
                    chartStateService.setChartFormat('YAxis', file.formatting.yAxis);
                }
                if (file.formatting && file.formatting.table) {
                    formatting.import(file.formatting.table);
                    scope.table.updateFormatting();
                }
            };

            var prepareForSaving = function(file) {
                for (var key in file) {
                    if (key === '$$hashKey') {
                        delete file[key];
                    } else if (typeof file[key] === 'object') {
                        prepareForSaving(file[key]);
                    }
                }
            };

            var createFile = function() {
                var file = {
                    version: visualizerVersion,
                    subjectArea: scope.lastSubjectArea || "Default Subject Area",
                    title: chartStateService.getTitle(),
                    guidedMode: scope.chartType,
                    stage: scope.stage.toJSON(),
                    previousDimensionValues: chartStateService.getPreviousDimensionValues(),
                    formatting: {
                        title: chartStateService.getChartFormat('Title'),
                        xAxis: chartStateService.getChartFormat('XAxis'),
                        yAxis: chartStateService.getChartFormat('YAxis'),
                        legend: null,
                        tooltip: null,
                        table: formatting.export(scope.table && scope.table.formatting)
                    },
                    reportExpressions: chartStateService.reportExpressions,
                    undoStack: scope.undoStack
                };
                prepareForSaving(file);
                return file;
            };

            window.LOG_FILE = function() {
                console.log(JSON.stringify(createFile()));
            };

            var blankFile = JSON.stringify(createFile());
            var currentFile = angular.copy(blankFile);

            var existingFileNameInCurrentDirectory = function(file) {
                return _.find(scope.fileSystem.currentDirectoryList, function(fileNode) {
                    return fileNode.type === 'file' && fileNode.reportName === file;
                });
            };

            var fileNameCheck = function(fileName) {
                if (fileName === undefined || !fileName.length) {
                    scope.fileSystem.saveValidationMessage = 'Invalid report name';
                    return;
                } else if (fileName.match(/[\/\\\?\%\*\:\"<\>\.\|]/)) {
                    scope.fileSystem.saveValidationMessage = 'Invalid report name';
                    return;
                } else if (existingFileNameInCurrentDirectory(fileName)) {
                    scope.fileSystem.saveValidationMessage = 'Report already exists';
                    return;
                } else if (!fileName.match(/[0-9a-zA-Z]/)) {
                    scope.fileSystem.saveValidationMessage = 'Report name must contain at least one letter or number';
                    return;
                } else {
                    return true;
                }
            };

            scope.fileSystem = {

                isNavigatorOpen: false,
                activeDirectory: '/shared',
                modal: {},

                render: function(file, fileName) {
                    var version = JSON.parse(file).version;
                    scope.fileSystem.loadingFile = true;

                    // first-version files use an object for version, normalize it to an integer 1.
                    version = isNaN(version) ? 1 : version;
                    for(var ii = 0; ii < migrations.length; ii++) {
                        if(migrations[ii].version > version) {
                            file = migrations[ii].update( file );
                        }
                    }
                    file = JSON.parse(file, function(k, v) {
                        if(v && v._class === 'Stage') {
                            return visualizer.Stage.fromJSON(v);
                        } else if(v && v._class === 'BqlNode') {
                            return visualizer.BqlNode.fromJSON(v);
                        } else {
                            return v;
                        }
                    });
                    scope.clearStage(true);
                    scope.reportName = fileName || null;
                    addColumns(file);
                    addFormatting(file);
                    if (file.guidedMode) {
                        scope.chartType = file.guidedMode;
                        chartStateService.setCurrentChartType(file.guidedMode);
                    } else {
                        console.log('warning: no chart type set in file.');
                    }
                    if (file.subjectArea) {
                        scope.selectedSubjectArea = file.subjectArea;
                    }
                    updateBasicStageProperties(file);
                    //scope.updateChart();
                },

                open: function(directory, fileName) {
                    adhocWebService.readFile(directory, fileName, function(file) {

                        currentFile = file;
                        scope.fileSystem.render(file, fileName);

                    }, error);
                },

                save: function(directory, fileName, file) {
                    file = file || JSON.stringify(createFile());
                    scope.fileSystem.navigatorMode = 'save';

                    var finalizeSave = function(directory, fileName, file) {
                        adhocWebService.saveFile(directory, fileName, file, resetCheck, error);
                        scope.reportName = fileName;
                        scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        scope.fileSystem.saveValidationMessage = null;
                        scope.fileSystem.closeNavigator();
                    };

                    var resetCheck = function() {
                        if (scope.fileSystem.nextAction === 'reset') {
                            scope.clearStage(true);
                        }
                    };

                    if (!fileName && !scope.reportName) {
                        scope.fileSystem.openNavigator();
                    } else if (fileName && !fileNameCheck(fileName)) {
                        // filename check doesn't pass, don't do anything
                        if (scope.fileSystem.saveValidationMessage === 'Report already exists') {
                            scope.fileSystem.modal = {
                                showModal: true,
                                title: 'File Overwrite',
                                text: 'Would you like to overwrite ' + fileName + '?',
                                successButtonText: 'Overwrite',
                                successCallback: function() {
                                    finalizeSave(directory, fileName, file);
                                    scope.fileSystem.closeModal();
                                },
                                cancelCallback: function() {
                                    scope.fileSystem.closeModal();
                                    scope.fileSystem.saveValidationMessage = null;
                                }
                            };
                        }
                        return;
                    } else if (!fileName && !directory && scope.reportName) {
                        adhocWebService.saveFile(scope.fileSystem.activeDirectory, scope.reportName, file, resetCheck, error);
                        scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        scope.fileSystem.navigatorMode = 'open';
                        currentFile = file;
                    } else if (fileName && directory) {
                        finalizeSave(directory, fileName, file);
                        currentFile = file;
                    } else {
                        scope.fileSystem.openNavigator();
                    }
                },

                copy: function(fileNode) {
                    adhocWebService.readFile(scope.fileSystem.activeDirectory, fileNode.reportName, function(file) {

                        scope.fileSystem.save(scope.fileSystem.activeDirectory, fileNode.fileName, file);
                        scope.safeApply();

                    }, error);
                },

                moveTo: function() {
                    console.log('move file');
                },

                startRename: function(file) {
                    file.editing = !file.editing;
                    file.newName = file.reportName;
                },

                confirmRename: function(file) {
                    if (!file.fileName && !file.reportName && !file.modified) {
                        // create new folder
                        fileWebService.createDirectory(scope.fileSystem.activeDirectory, file.newName, function() {
                            scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        }, error);
                    } else {
                        // actually rename
                        var oldName = file.fileName;

                        file.editing = !file.editing;
                        scope.reportName = file.newName;
                        file.fileName = file.type === 'file' ? file.newName + '.viz.dashlet' : file.newName;
                        //file.fileName = getUniqueFileName(file);

                        fileWebService.renameFile(scope.fileSystem.activeDirectory, oldName, file.fileName, scope.fileSystem.openDirectory, error);
                    }
                },

                delete: function(file) {
                    scope.fileSystem.activeFile = file;
                    scope.fileSystem.modal = {
                        showModal: true,
                        title: 'Delete Confirmation',
                        text: 'This action will remove your file!',
                        successButtonText: 'Delete',
                        successCallback: function() {
                            scope.fileSystem.closeModal();

                            fileWebService.deleteFile(scope.fileSystem.activeDirectory, scope.fileSystem.activeFile.fileName, function() {
                                scope.fileSystem.currentDirectoryList = _.without(scope.fileSystem.currentDirectoryList, scope.fileSystem.activeFile);
                                scope.safeApply();
                            }, error);
                        },
                        cancelCallback: scope.fileSystem.closeModal
                    };
                },

                closeModal: function() {
                    scope.fileSystem.modal.showModal = false;
                },

                hasFileChanged: function() {
                    var newFile = JSON.stringify(createFile());
                    if (!_.isEqual(newFile, currentFile)) {
                        return true;
                    } else {
                        return false;
                    }
                },

                clearCurrentFile: function() {
                    currentFile = angular.copy(blankFile);
                },

                openNavigator: function(mode) {
                    var fileHasChanged = scope.fileSystem.hasFileChanged();

                    if (fileHasChanged && scope.reportName) {
                        scope.fileSystem.modal = {
                            showModal: true,
                            title: 'Would you like to save?',
                            text: 'Do you want to save the changes you have made to ' + (scope.reportName || 'this unsaved report') + '?',
                            successButtonText: 'Save',
                            successCallback: function() {
                                scope.fileSystem.closeModal();
                                scope.fileSystem.save();
                            },
                            cancelButtonText: 'Don\'t Save',
                            cancelCallback: scope.fileSystem.closeModal
                        };
                    }

                    if (mode === 'open') {
                        scope.fileSystem.navigatorMode = 'open';
                    }

                    setTimeout(function() {
                        scope.fileSystem.isNavigatorOpen = true;
                        element.find('[file-navigator]').scrollTop(0);
                    }, 50);

                    fileWebService.getVisibleFolderStructure(false, function(folderStructure) {

                        scope.fileSystem.folderStructure = folderStructure;
                        //scope.fileSystem.activeDirectory = scope.fileSystem.getUserPreferences(); ?
                        scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        scope.safeApply();

                    }, error);
                },

                closeNavigator: function() {
                    scope.fileSystem.isNavigatorOpen = false;
                },

                getUserPreferences: function() {
                    preferencesService.getUserPreferences(function($preferences) {
                        scope.fileSystem.activeDirectory = $preferences.children('Value').text();
                    }, error);
                },

                openDirectory: function(directory) {
                    directory = directory || scope.fileSystem.activeDirectory;

                    fileWebService.getDirectory(directory, false, function(files) {
                        scope.fileSystem.currentDirectoryList = files;
                        scope.safeApply();
                    }, error);
                },

                setDirectory: function(directory) {
                    directory.open = directory.open === true ? true : false;

                    if (scope.fileSystem.activeDirectory !== directory.path) {
                        scope.fileSystem.activeDirectory = directory.path;
                        scope.fileSystem.currentDirectoryList = scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        scope.safeApply();
                    }
                },

                openFile: function(file, $event) {
                    var $target = $($event.target);

                    if (!$target.is('TR') && ($target.is('I') || $target.hasClass('button') || $target.parent().hasClass('button') || $target.parent().hasClass('inline-edit'))) {
                        return;
                    }
                    if (file.type === 'folder') {
                        scope.fileSystem.activeDirectory = scope.fileSystem.activeDirectory === '/' ? '/' + file.reportName : scope.fileSystem.activeDirectory + '/' + file.reportName;

                        var folderClicked = _.recursiveSearch(scope.fileSystem.folderStructure, 'children', 'path', scope.fileSystem.activeDirectory);
                        folderClicked.open = true;

                        scope.fileSystem.openDirectory(scope.fileSystem.activeDirectory);
                        scope.safeApply();
                    } else {
                        if (scope.fileSystem.navigatorMode === 'save') {
                            scope.fileSystem.newFileName = file.reportName;
                            return;
                        }
                        scope.fileSystem.isNavigatorOpen = false;
                        scope.fileSystem.open(scope.fileSystem.activeDirectory, file.reportName);
                    }
                },

                newFolder: function() {
                    scope.fileSystem.currentDirectoryList.unshift({
                        fileName: null,
                        reportName: null,
                        newName: '(new folder)',
                        type: 'folder',
                        owner: null,
                        modified: null,
                        dashboard: null,
                        write: 'true',
                        editing: true
                    });
                },
                createFile: createFile,

                getSortType: function() {
                    return 'Recent';
                },

                // dropInFolder: _.memoizeObject(function(folder) {
                //     return {
                //         "over": function() {
                //             elem.find('[folder="' + folder.$$hashKey + '"]').addClass('drag-hover-state');
                //         },
                //         "out": function() {
                //             elem.find('[folder="' + folder.$$hashKey + '"]').removeClass('drag-hover-state');
                //         },
                //         start: function() {},
                //         drop: function(file) {
                //             if (file.type === 'folder') {
                //                 alert('dont move folders yet');
                //                 return;
                //             }
                //             elem.find('[folder="' + folder.$$hashKey + '"]').removeClass('drag-hover-state');
                //             fileWebService.moveFiles(scope.fileSystem.activeDirectory, file.fileName, folder.path, function() {
                //                 scope.fileSystem.currentDirectoryList = _.without(scope.fileSystem.currentDirectoryList, file);
                //                 scope.safeApply();
                //             }, error);
                //         }
                //     };
                // })
            };
        }
    };
};

visualizer.directives.fileSystemUi = function() {
    return {
        replace: true,
        templateUrl: 'partials/designer/fileSystemUI.html'
    };
};

angular.module('visualizer').
directive('fileSystem', ['chartStateService', 'AdhocWebService', 'FileWebService', 'PreferencesService', 'alertService', 'formatting', visualizer.directives.fileSystem]).
directive('fileSystemUi', visualizer.directives.fileSystemUi);
