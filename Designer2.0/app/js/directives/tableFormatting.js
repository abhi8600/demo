angular.module('visualizer').
directive('tableFormatting', function() {
    return {
        replace: true,
        templateUrl: 'partials/designer/tableFormatting.html'
    };
});
