visualizer.directives.stagingRow = function () {
    return {
        require: '^?designer',
        replace: true,
        templateUrl: 'partials/designer/stagingRow.html',
        scope: true,

        link: function (scope, elem, attrs) {
            //scope.editingMode = false;
            scope.column = scope.$eval(attrs.stagingRow);
            scope.$watch(function() {
                return scope.$eval(attrs.stagingRow);
            }, function(val) { scope.column = val; });
            scope.serie = scope.$eval(attrs.serie);
            scope.stagingType = attrs.stagingType;
            scope.columnPropertiesOpen = true;
        }
    };
};

angular.module('visualizer').
    directive('stagingRow', visualizer.directives.stagingRow );