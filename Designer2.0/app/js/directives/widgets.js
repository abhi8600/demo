visualizer.directives.toggleSwitchApple = function() {
    var iOSToggleCounter = 100;

    return {
        restrict: 'EA',
        replace: true,
        scope: {
            'model': '=',
            'default': '=',
            'onChange': '&'
        },
        template: '<div class="switch-apple" ng-click="toggle()"><input type="checkbox" name="{{iOSToggleCounter}}" ng-model="model"><label for="{{iOSToggleCounter}}"></label></div>',
        link: function($scope, $element, attrs) {
            // model starts true unless attribute default="value" is present in html
            if ($scope.
                default !== undefined) {
                $scope.model = $scope.
                default;
            } else if ($scope.model !== undefined) {
                $scope.model = $scope.model;
            } else {
                $scope.model = true;
            }
            $scope.$watch(function() {
                return $scope.$eval(attrs.model);
            }, function(val) {
                if (!val) {
                    return;
                }
                $scope.model = val;
            });

            $scope.iOSToggleCounter = iOSToggleCounter++;

            $scope.toggle = function() {
                // use a setTimeout to delay onchange until after model changes have propagated. This is awful =/
                setTimeout(function() {
                    $scope.$apply(function() {
                        $scope.onChange();
                    });
                }, 0);
                $scope.model = $scope.model ? !$scope.model : true;
            };
        }
    };
};

visualizer.directives.sortWidget = function() {
    var sortCounter = 100;

    return {
        replace: true,
        templateUrl: 'partials/designer/sortWidget.html',
        scope: true,

        link: function(scope, elem, attrs) {
            scope.column = scope.$eval(attrs.sortWidget);
            scope.index = scope.$eval(attrs.index);
            scope.sortCounter = sortCounter++;
            scope.$watch(function() {
                return scope.$eval(attrs.sortWidget);
            }, function(val) {
                scope.column = val;
            });
            scope.$watch(function() {
                return scope.$eval(attrs.index);
            }, function(val) {
                scope.index = val;
            });
        }
    };
};

visualizer.directives.tableResize = function() {
    return {
        scope: true,
        link: function(scope, element) {
            var LEFT_BUTTON = 1;
            var drag = false;
            var startX,
                newX,
                startWidth,
                childSpan,
                spanWidth,
                rightSibling,
                rightSiblingWidth,
                rightSiblingSpan,
                rightSiblingSpanWidth,
                containerWidth,
                widthOfColumns,
                widthDifference,
                distanceAtOverflow;

            element.on('mousedown', function(event) {
                var mouseButtonClicked = event.which;

                if (mouseButtonClicked !== LEFT_BUTTON) {
                    return;
                }

                startX = event.pageX;
                startWidth = element.closest('th').width();
                childSpan = element.siblings('.table-column-title');
                spanWidth = childSpan.width();
                rightSibling = element.closest('th').next();
                rightSiblingWidth = rightSibling.width();
                rightSiblingSpan = rightSibling.find('.table-column-title');
                rightSiblingSpanWidth = rightSiblingSpan.width();
                drag = true;
                distanceAtOverflow = 0;

            });

            $(document).on('mouseup', function() {
                drag = false;
            });

            $(document).on('mousemove', function(event) {
                if (drag) {
                    newX = event.pageX - startX;
                    containerWidth = _.widthWithoutScrollbar('.table-container') - 1;
                    widthOfColumns = 0;
                    element.closest('tr').children('th').each(function() {
                        widthOfColumns += $(this).outerWidth();
                    });
                    widthDifference = widthOfColumns - containerWidth;

                    if (widthDifference >= 0) {
                        distanceAtOverflow = distanceAtOverflow === 0 ? newX - widthDifference : distanceAtOverflow;

                        element.closest('th').width(startWidth + newX);

                        if (childSpan.data('autoWidth') > (spanWidth + newX)) {
                            childSpan.width(spanWidth + newX);
                        } else {
                            childSpan.width(childSpan.data('autoWidth'));
                        }

                        rightSibling.width(rightSiblingWidth + (distanceAtOverflow - newX));

                        if (rightSiblingSpan.data('autoWidth') > (rightSiblingWidth + newX)) {
                            rightSiblingSpan.width(rightSiblingWidth + newX);
                        } else {
                            rightSiblingSpan.width(rightSiblingSpan.data('autoWidth'));
                        }
                    } else {
                        element.closest('th').width(startWidth + newX);

                        if (childSpan.data('autoWidth') > (spanWidth + newX)) {
                            childSpan.width(spanWidth + newX);
                        } else {
                            childSpan.width(childSpan.data('autoWidth'));
                        }
                    }
                }
            });
        }
    };
};

visualizer.directives.tableAutoWidth = function() {
    return {
        scope: true,
        link: function(scope, element, attrs) {
            var minColumnWidth = 80;
            var autoWidth;

            scope.$watch('$viewContentLoaded', function() {
                var widestData = 0;
                _.each(element.closest('table').find('span.index' + attrs.index), function(span) {
                    if ($(span).width() > widestData) {
                        widestData = $(span).width();
                    }
                });
                var newWidth = Math.max(minColumnWidth, widestData - 40);
                var childSpan = element.find('.table-column-title');
                autoWidth = childSpan.outerWidth();
                childSpan.data('autoWidth', autoWidth);
                childSpan.width(newWidth);
            });
        }
    };
};

visualizer.directives.colorPicker = function() {
    return {
        replace: true,
        template: '<span ng-click="togglePicker($event)"><div ng-show="showPicker" class="colors"><div ng-repeat="color in palette" ng-click="setColor(color)" ng-style="getStyle(color)" ng-class="color"></div><input type="text" /></div></span>',
        scope: {
            'colorPicker': '=',
            'palette': '='
        },
        link: function(scope, element) {
            //console.log(scope.palette);
            scope.showPicker = false;

            scope.$watch(function() {
                return scope.colorPicker;
            }, function(val) {
                if (!val) {
                    return;
                }
                scope.colorPicker = val;
                updateColor();
            }, true);

            function updateColor() {
                if (scope.colorPicker.value === 'transparent') {
                    element.addClass('transparent-icon');
                    element.css('background-color', scope.colorPicker.value);
                } else {
                    element.css('background-color', scope.colorPicker.value);
                }
            }
            updateColor();

            scope.getStyle = function(color) {
                return { 'background-color': color };
            };

            scope.setColor = function(color) {
                scope.colorPicker.value = color.toString();
                updateColor();
                //scope.togglePicker();
            };

            scope.togglePicker = function(e) {
                if (e) {
                    e.stopPropagation();
                }
                scope.showPicker = !scope.showPicker;
            };

            element.closest('body').on('click', function() {
                if (scope.showPicker) {
                    scope.showPicker = false;
                    scope.$apply();
                }
            });

            element.find('input').spectrum({
                color: scope.colorPicker.value,
                showInitial: true,
                change: function(color) {
                    scope.colorPicker.value = color.toString();
                    updateColor();
                    scope.togglePicker();
                    // this is a dirty hack, sorry future readers :(
                    element.click().click();
                }
            });
        }
    };
};


angular.module('visualizer').
directive('toggleSwitchApple', visualizer.directives.toggleSwitchApple).
directive('sortWidget', visualizer.directives.sortWidget).
directive('tableResize', visualizer.directives.tableResize).
directive('tableAutoWidth', visualizer.directives.tableAutoWidth).
directive('colorPicker', visualizer.directives.colorPicker);
