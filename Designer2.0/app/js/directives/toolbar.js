'use strict';

visualizer.directives.toolbar = function ($routeParams, $window) {
    return {
        replace: true,
        templateUrl: 'partials/designer/toolbar.html',
        controller: function ($scope) {
            $scope.showFilePath = false;
            $scope.reopenHelpScreen = function () {
                $scope.showHelpButton = "OK, let's go!";
                $scope.showHelp = true;
            };

            $scope.dashboardName = typeof $routeParams.dbname === 'undefined' ? '' : $routeParams.dbname;
            $scope.dashboardPage = typeof $routeParams.dbpage === 'undefined' ? '' : $routeParams.dbpage;

            $scope.publishToDashboards = function (activeDirectory, reportName) {
                var dashboard_url,
                    page = '',
                    name = encodeURIComponent($scope.dashboardName),
                    dir = encodeURIComponent(activeDirectory.replace(/\//g, ':')),
                    report = encodeURIComponent(':' + reportName);
                if ($scope.dashboardPage !== 'new') {
                    page = encodeURIComponent($scope.dashboardPage) + '/';
                }
                dashboard_url = '/Dashboards2.0/#/dashboards/' + name + '/page/' + page + 'add/' + dir + report;
                //console.log('dashboard URL:', dashboard_url);
                $window.location.href = dashboard_url;
            };
        }
    };
};

angular.module('visualizer').
    directive('toolbar', ['$routeParams', '$window', visualizer.directives.toolbar]);
