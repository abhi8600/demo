visualizer.directives.dropConfigs = function(chartStateService, alertService, $timeout) {
    return {
        link: function(scope, elem) {
            // Memoize this function or angular gets in an infinite loop :)
            var chartType = chartStateService.getCurrentChartType();
            scope.dropDim = _.memoize(function(dim) {
                return {
                    "over": function() {
                        var targetElement = elem.find("[menu-parent=" + dim + "]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        var targetElement = elem.find("[menu-parent=" + dim + "]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    "drop": function(column) {
                        var targetElement = elem.find("[menu-parent=" + dim + "]");
                        targetElement.removeClass('drag-hover-state');
                        if (!column || _.isArray(column)) {
                            return;
                        }
                        if (dim === 'Columns') {
                            if (!scope.stage.isColumnInStage(column, true)) {
                                scope.stage.addColumn(column, null, 'table');
                                scope.updateChart();
                                return;
                            } else {
                                return;
                            }
                        }
                        if (column.expression.isMeasure() && (dim === 'Shape' || dim === 'Trellis') ||
                            column.expression.isAttribute() && dim === 'Size') {
                            // scope.$broadcast('error', {
                            //     basicText: '"' + column.name + visualizer.misc.errors.ERR_CANT_BE_DIM + dim + '.' //' Please move it to a different bucket, remove it, or try dragging to one of the suggested chart types on the right.'
                            // });
                            alertService.alert([
                                '"' + column.name + visualizer.misc.errors.ERR_CANT_BE_DIM + dim + '.',
                                'error-cannotBeDimension'
                            ]);
                            return;
                        }
                        if (dim === 'Trellis') {
                            if (scope.stage.HorizontalTrellis && scope.stage.HorizontalTrellis !== column) {
                                column.properties.trellisDirection = 'Vertical';
                                scope.stage.addColumn(column, 'VerticalTrellis');
                            } else {
                                column.properties.trellisDirection = 'Horizontal';
                                scope.stage.addColumn(column, 'HorizontalTrellis');
                            }
                        } else {
                            delete column.properties.trellisDirection;
                            scope.stage.addColumn(column, dim, chartType);
                        }
                        scope.updateChart();
                        if (column.dragSource === 'trash') {
                            scope.trashBucket.columns = _.filter(scope.trashBucket.columns, function(trashColumn) {
                                return trashColumn.name !== column.name;
                            });
                        }
                        if (dim === 'Category') {
                            if (column.expression.isMeasure()) {
                                chartStateService.setCurrentChartType("bubble");
                                _.map(scope.stage.allSeries(), function(series) {
                                    series.type = "bubble";
                                });
                            } else if (chartStateService.getCurrentChartType() === 'bubble') {
                                chartStateService.setCurrentChartType("column");
                                _.map(scope.stage.allSeries(), function(series) {
                                    series.type = "column";
                                });
                            }
                        }
                        scope.stage.updateSortOrder();
                        scope.searchVariable.string = '';
                        scope.searchGrouped = [
                            [],
                            [],
                            []
                        ];
                        delete column.dragSource;
                    }
                };
            });

            scope.dropAxis = _.memoizeObject(function(axis) {
                return {
                    "over": function() {
                        var targetElement = elem.find("[menu-parent=Stage-Measures]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        var targetElement = elem.find("[menu-parent=Stage-Measures]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    start: function() { //console.log("start measure");
                    },
                    drop: function(column) {
                        var targetElement = elem.find("[menu-parent=Stage-Measures]");
                        targetElement.removeClass('drag-hover-state');
                        if (!column) {
                            return;
                        }
                        if (column.expression.isAttribute()) {
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_ATTR_AS_MSR1 + column.name + visualizer.misc.errors.ERR_ATTR_AS_MSR2
                            // });
                            alertService.alert([
                                visualizer.misc.errors.ERR_ATTR_AS_MSR1 + column.name + visualizer.misc.errors.ERR_ATTR_AS_MSR2,
                                'error-attrAsMeasure'
                            ]);
                            return;
                        }
                        if (column.dragSource === 'trash') {
                            scope.trashBucket.columns = _.filter(scope.trashBucket.columns, function(trashColumn) {
                                return trashColumn.name !== column.name;
                            });
                        }
                        if (!scope.stage.measures.length) {
                            scope.stage.addAxis();
                        }
                        if (!axis) {
                            axis = scope.stage.measures[0];
                        }
                        scope.stage.addAxisColumn(column, axis, chartStateService.getCurrentChartType());
                        scope.searchVariable.string = '';
                        scope.searchGrouped = [
                            [],
                            [],
                            []
                        ];
                        scope.updateChart();
                        delete column.dragSource;
                    }
                };
            });

            scope.dropFilter = _.memoizeObject(function() {
                return {
                    "over": function() {
                        var targetElement = elem.find("[menu-parent=Filters]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        var targetElement = elem.find("[menu-parent=Filters]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    start: function() {},
                    drop: function(column) {

                        var targetElement = elem.find("[menu-parent=Filters]");
                        targetElement.removeClass('drag-hover-state');
                        if (!column) {
                            return;
                        }

                        if (column.expression.isNotBasicColumn() && !scope.stage.isColumnInStage(column, true)) {
                            console.log('here');
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_CANT_FILTER3
                            // });
                            alertService.alert([
                                visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_CANT_FILTER3,
                                'error-attrAsMeasure'
                            ]);
                            return;
                        }
                        if (scope.stage.allColumns().length === 0) {
                            console.log('here');
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_CANT_FILTER2
                            // });
                            alertService.alert([
                                visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_CANT_FILTER2,
                                'error-cantFilter'
                            ]);
                            return;
                        }
                        if (scope.chartType !== 'table' && !scope.stage.Category || _.every(scope.stage.allColumns(), function(column) {
                            return column.expression.isMeasure();
                        })) {
                            console.log('here');
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_NEED_CATEGORY
                            // });
                            alertService.alert([
                                visualizer.misc.errors.ERR_CANT_FILTER1 + column.name + visualizer.misc.errors.ERR_NEED_CATEGORY,
                                'error-cantFilterNeedCategory'
                            ]);
                            return;
                        }
                        if (column.dragSource === 'trash') {
                            console.log('here');
                            scope.trashBucket.columns = _.filter(scope.trashBucket.columns, function(trashColumn) {
                                return trashColumn.name !== column.name;
                            });
                        }

                        if (scope.flags.loaders) {
                            scope.$broadcast('loader-sa-show');
                            alertService.alert({
                                container: '[alerts-sa]',
                                content: visualizer.misc.feedback.FEEDBACK_LOADING_FILTER,
                                id: 'success-rendering-sa',
                                type: 'success',
                                duration: 3
                            });

                            scope.loaders.saTimeout = $timeout(function () {
                                // Set 30 second filtering timeout.  Note that the error alert
                                // triggers the removal of the loader.
                                alertService.alert({
                                    container: '[alerts-sa]',
                                    content: visualizer.misc.errors.ERR_FILTER_TIMEOUT,
                                    id: 'error-rendering-sa'
                                });
                            }, 30000);
                        }

                        column.measuresToFilter = {};
                        scope.stage.addFilter(column, 'filter');

                        var $filter = elem.find('[menu-parent="Filters"]');
                        setTimeout(function() {
                            if (!$filter.hasClass('active-stage-parent')) {
                                $filter.click();
                            }
                            elem.find('[column-name="' + column.name + '"]').find('[ng-click="filterDialog(column)"]').click();
                        }, 100);

                        scope.searchVariable.string = '';
                        scope.searchGrouped = [
                            [],
                            [],
                            []
                        ];
                        //scope.updateChart();
                        delete column.dragSource;
                    }
                };
            });

            scope.dropSorting = _.memoizeObject(function() {
                return {
                    "over": function() {
                        var targetElement = elem.find("[menu-parent=Sorting]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        var targetElement = elem.find("[menu-parent=Sorting]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    start: function() { //console.log("start filter");
                    },
                    drop: function(column) {
                        var targetElement = elem.find("[menu-parent=Sorting]");
                        targetElement.removeClass('drag-hover-state');
                        if (!column) {
                            return;
                        }
                        if (!scope.stage.isColumnInStage(column, true)) {
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_SORT_DIRECT
                            // });
                            alertService.alert([visualizer.misc.errors.ERR_SORT_DIRECT, 'error-cantSortFromSA']);
                            return;
                        }
                        if (!column.properties.sortDirection && (column !== scope.stage.HorizontalTrellis || column !== scope.stage.VerticalTrellis)) {
                            scope.stage.addToSortOrder(column);
                        }
                        var $sorting = elem.find('[menu-parent="Sorting"]');
                        if (!$sorting.hasClass('active-stage-parent')) {
                            setTimeout(function() {
                                $sorting.click();
                            }, 100);
                        }
                        scope.updateChart();
                        delete column.dragSource;
                    }
                };
            });

            scope.dropTrellis = _.memoize(function(dim) {
                return {
                    "over": function() {
                        var targetElement = dim === 'Vertical' ? elem.find("[menu-parent='Trellis'] [vertical-trellis]") : elem.find("[menu-parent='Trellis'] [horizontal-trellis]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        var targetElement = dim === 'Vertical' ? elem.find("[menu-parent='Trellis'] [vertical-trellis]") : elem.find("[menu-parent='Trellis'] [horizontal-trellis]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    start: function() {
                        //console.log("start filter");
                    },
                    cancel: function() {
                        var targetElement = dim === 'Vertical' ? elem.find("[menu-parent='Trellis'] [vertical-trellis]") : elem.find("[menu-parent='Trellis'] [horizontal-trellis]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    drop: function(column) {
                        var targetElement = dim === 'Vertical' ? elem.find("[menu-parent='Trellis'] [vertical-trellis]") : elem.find("[menu-parent='Trellis'] [horizontal-trellis]");
                        targetElement.removeClass('drag-hover-state');
                        if (scope.stage[dim + 'Trellis'] === column) {
                            return;
                        } else {
                            var temp = scope.stage.HorizontalTrellis;
                            scope.stage.HorizontalTrellis = scope.stage.VerticalTrellis;
                            scope.stage.VerticalTrellis = temp;

                            scope.stage.updateSortOrder();
                            scope.updateChart();
                        }
                    }
                };
            });

            scope.dropIntoTrash = _.memoizeObject(function() {
                return {
                    "start": function(a, column) {
                        elem.find('[search-input] input').blur();
                        if (column.dragSource === 'stage' || column.dragSource === 'sort' || column.dragSource === 'filter') {
                            scope.trashBucket.dragToTrashMode = true;
                        }
                    },
                    "over": function() {
                        elem.find('[trash-bucket] .trash-drop').addClass('trash-active');
                    },
                    "out": function() {
                        scope.trashBucket.dragToTrashMode = false;
                        elem.find('[trash-bucket] .trash-drop').removeClass('trash-active');
                    },
                    "cancel": function(column) {
                        scope.trashBucket.dragToTrashMode = false;
                        elem.find('[trash-bucket] .trash-drop').removeClass('trash-active');
                        delete column.dragSource;
                    },
                    "drop": function(column) {
                        scope.trashBucket.dragToTrashMode = false;
                        elem.find('[trash-bucket] .trash-drop').removeClass('trash-active');

                        if (column.dragSource === 'stage') {
                            scope.removeColumn(column, scope.chartType);
                        } else if (column.dragSource === 'filter') {
                            scope.stage.removeFilter(column, scope.chartType);
                        } else if (column.dragSource === 'sort') {
                            scope.stage.removeFromSortOrder(column);
                        }
                        scope.updateChart();
                        delete column.dragSource;
                    }
                };
            });

            scope.dropIntoRecent = _.memoizeObject(function() {
                return {
                    "start": function(a, column) {
                        elem.find('[search-input] input').blur();
                        if (column.dragSource === 'stage' || column.dragSource === 'sort' || column.dragSource === 'filter') {
                            scope.trashBucket.dragToTrashMode = true;
                        }
                    },
                    "over": function() {
                        var targetElement = elem.find("[menu-parent=Trash]");
                        targetElement.addClass('drag-hover-state');
                    },
                    "out": function() {
                        scope.trashBucket.dragToTrashMode = false;
                        var targetElement = elem.find("[menu-parent=Trash]");
                        targetElement.removeClass('drag-hover-state');
                    },
                    "cancel": function(column) {
                        scope.trashBucket.dragToTrashMode = false;
                        var targetElement = elem.find("[menu-parent=Trash]");
                        targetElement.removeClass('drag-hover-state');
                        delete column.dragSource;
                    },
                    "drop": function(column) {
                        scope.trashBucket.dragToTrashMode = false;
                        var targetElement = elem.find("[menu-parent=Trash]");
                        targetElement.removeClass('drag-hover-state');

                        if (column.dragSource === 'stage') {
                            scope.removeColumn(column, scope.chartType);
                        } else if (column.dragSource === 'filter') {
                            scope.stage.removeFilter(column, scope.chartType);
                        } else if (column.dragSource === 'sort') {
                            scope.stage.removeFromSortOrder(column);
                        }
                        scope.updateChart();
                        delete column.dragSource;
                    }
                };
            });
        }
    };
};

angular.module('visualizer').
directive('dropConfigs', ['chartStateService', 'alertService', '$timeout', visualizer.directives.dropConfigs]);
