visualizer.directives.filterRow = function (queryService) {
    return {
        require: '^?designer',
        replace: true,
        templateUrl: 'partials/designer/filterRow.html',
        scope: true,

        link: function (scope, elem, attrs) {
            //scope.editingMode = false;
            scope.column = scope.$eval(attrs.filterRow);
            scope.$watch(function() {
                return scope.$eval(attrs.filterRow);
            }, function(val) { scope.column = val; });

            scope.serie = scope.$eval(attrs.serie);
            scope.stagingType = attrs.stagingType;

            // This isn't the best implementation. filterTypes uses index to reference from text to lookup. filterInfo and filterInfoReverse are built from filterTypes.
            scope.filterTypes = {
                text: {
                    range: ['Between', 'Equal To', 'Greater Than', 'Less Than', 'Outside', 'Greater Than or Equal To', 'Less Than or Equal To', 'Not Equal To', '(is missing)', '(is not missing)'],
                    items: ['Equal To', 'Not Equal To', 'Contains', 'Does Not Contain', '(is missing)', '(is not missing)'],
                    date: ['Equal To', 'Greater Than', 'Less Than', 'Between', '(is missing)', '(is not missing)']
                },
                lookup: {
                    range: ['Between', '=', '>', '<', 'Outside', '>=', '<=', '<>', ' IS NULL ', ' IS NOT NULL '],
                    items: ['=', '<>', ' LIKE ', ' NOT LIKE ', ' IS NULL ', ' IS NOT NULL '],
                    date: ['=', '>', '<', 'Between', ' IS NULL ', ' IS NOT NULL ']
                }
            };

            scope.filterInfo = {};
            _.each(scope.filterTypes.text, function(type, key) {
                scope.filterInfo[key + 'TypesMap'] = _.object(type, scope.filterTypes.lookup[key]);
            });

            scope.filterInfoReverse = { };
            _.each(scope.filterInfo, function(value, key) {
                scope.filterInfoReverse[key] = _.invert(value);
            });
        },

        controller: function ($scope) {
            $scope.filterDialog = function (column) {
                var filterData = {
                    column: column,
                    filterInfo: $scope.filterInfo, // remove this line
                    filterTypes: $scope.filterTypes
                };
                var savedFilter, savedDisplayFilter;

                if (column.properties.visualized) {
                    filterData.allColumns = $scope.stage.allColumns($scope.chartType);
                } else {
                    filterData.allColumns = $scope.stage.allColumnsAndUnvisualized($scope.chartType);
                }

                var removeFilter = filterData.allColumns.map(function (col) {
                    if (col === column) {
                        savedFilter = col.filter;
                        savedDisplayFilter = col.displayFilter;
                        col.filter = null; // temporarily setting to null, no need to proxy through stage.removeFilter()
                        col.displayFilter = null; // temporarily setting to null, no need to proxy through stage.removeFilter()
                        return col;
                    } else {
                        return col;
                    }
                });

                // Below, we're retrieving the list of values available to filter on.
                // To do so we may need to remove an existing filter on the column we're trying to filter, otherwise
                // we would only be able to further narrow our filter.  We want to be able to narrow or expand the filter.
                var queryWithoutFilter = queryService.queryString( removeFilter, removeFilter, $scope.stage.sortBQLString());

                queryService.runCachedQuery(queryWithoutFilter, function (data) {
                    if ($scope.flags.loaders) {
                        $scope.$emit('loader-sa-hide');
                    }
                    if (savedFilter || savedDisplayFilter) {
                        filterData.column.filter = savedFilter;
                        filterData.column.displayFilter = savedDisplayFilter;
                    }
                    filterData.resultSet = data;
                    filterData.filterInfoReverse = $scope.filterInfoReverse;
                    $scope.filterData = filterData;
                    $scope.safeApply();
                }, $scope.errorHandler);

            };
        }
    };
};

angular.module('visualizer').
    directive('filterRow', visualizer.directives.filterRow );