'use strict';

// A loader for an area of Visualizer can be triggered by $broadcasting
// the string 'loader-{area}-show', and likewise hidden by $broadcasting
// the string 'loader-{area}-hide'.

visualizer.directives.loader = function(alertService, $q, $timeout) {
    return {
        link: function (scope, element, attrs) {
            scope.loaders[attrs.area + 'Timeout'] = $q.defer();

            function showLoader() {
                element.addClass('loading');
            }

            function hideLoader() {
                $timeout.cancel(scope.loaders[attrs.area + 'Timeout']);
                scope.loaders[attrs.area + 'Timeout'] = $q.defer();
                alertService.clearById('success-rendering-' + attrs.area);
                element.removeClass('loading');
            }

            scope.$on('loader-{area}-show'.replace('{area}', attrs.area), showLoader);
            scope.$on('loader-{area}-hide'.replace('{area}', attrs.area), hideLoader);
            scope.$on('loader-all-hide', hideLoader);
            
            // Whenever an error is thrown, hide all loaders.
            scope.$on('alertService-newAlert-danger', function () {
                setTimeout(function () {
                    hideLoader();
                }, 500);
            });
        }
    };
};

angular.module('visualizer').directive('loader', ['alertService', '$q', '$timeout', visualizer.directives.loader]);
