visualizer.directives.errorDialog = function() {
    return {
        replace: true,
        templateUrl: 'partials/designer/errorDialog.html',
        controller: function($scope) {
            $scope.isErrorVisible = false;
            $scope.showErrorMessage = false;
            $scope.moreOrLess = 'more';

            /*
             Name:                 name of error, not used for anything right now, just internal tracking
             Severity:             0-3, 0=info, 3=critical
             basicText:            basic message to user
             moreInfoText:         "more info" message to user displayed optionally
             hyperlinkText:        text of hyperlink to take remedial action
             hyperlinkFunction:    function call for remedial action
             recallError:          if true, force error dialog to close (e.g. if we know we'd previously sent in error)
             */

            $scope.error = {
                Name: null,
                Severity: 0,
                basicText: null,
                moreInfoText: null,
                hyperlinkText: null,
                hyperlinkFunction: null,
                recallError: null,
                timeout: null
            };

            $scope.toggleError = function() {
                $scope.showErrorMessage = !$scope.showErrorMessage;
            };

            $scope.closeErrorDialog = function() {
                $scope.isErrorVisible = false;
                $scope.error = {};
                $scope.moreOrLess = 'less';
            };

            $scope.toggleMoreOrLess = function() {
                if (!$scope.error) {
                    return;
                }

                switch ($scope.moreOrLess) {
                    case 'more':
                        $scope.moreOrLess = 'less';
                        $scope.errorMessage = $scope.error.basicText + " - " + $scope.error.moreInfoText;
                        break;
                    default:
                        $scope.moreOrLess = 'more';
                        $scope.errorMessage = $scope.error.basicText;
                }
            };
        },

        link: function(scope) {

            scope.$on('error', function(event, errorProperties) {
                if (errorProperties.recallError === true) {
                    scope.closeErrorDialog();
                    return;
                } else {
                    scope.moreOrLess = 'less';
                    scope.toggleMoreOrLess();
                    scope.isErrorVisible = true;

                    scope.error = scope.error || {};

                    scope.error.basicText = errorProperties.basicText;
                    scope.error.moreInfoText = errorProperties.moreInfoText;

                    scope.errorMessage = scope.error.basicText;
                }

                if (errorProperties.timeout) {
                    setTimeout(scope.closeErrorDialog, errorProperties.timeout);
                }

                scope.safeApply();
            });
        }
    };
};

angular.module('visualizer').directive('errorDialog', visualizer.directives.errorDialog);
