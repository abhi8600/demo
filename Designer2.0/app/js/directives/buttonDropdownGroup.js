visualizer.directives.buttonDropdownGroup = function() {
    return {
        replace: true,
        scope: {
            model: '=',
            options: '=',
            type: '@'
        },
        templateUrl: 'partials/designer/buttonDropdownGroup.html',
        link: function(scope) {
            scope.optionsLength = _.size(scope.options.text[scope.type]);
            scope.showMoreOptions = false;
            scope.toggleMoreOptions = function() {
                scope.showMoreOptions = !scope.showMoreOptions;
            };
            scope.setModel = function(index) {
                scope.model = scope.options.lookup[scope.type][index];
                scope.showMoreOptions = false;
            };
        }
    };
};

angular.module('visualizer').
directive('buttonDropdownGroup', visualizer.directives.buttonDropdownGroup);
