visualizer.directives.filterSummary = function() {
    return {
        replace: true,
        scope: true,
        templateUrl: 'partials/designer/filter-summary.html',
        link: function(scope, elem, attrs) {
            scope.column = scope.$eval(attrs.filterSummary);
        }
    };
};

angular.module('visualizer').
directive('filterSummary', visualizer.directives.filterSummary);