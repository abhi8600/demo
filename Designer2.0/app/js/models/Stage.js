/* jshint expr:true */
visualizer.Stage = function() {
    this.clear();
};

visualizer.Stage.dimensions = ['Category', 'Color', /*'Pattern',*/ 'Shape', 'Size', 'HorizontalTrellis', 'VerticalTrellis'];

visualizer.Stage.columnSort = function(leftColumn, rightColumn) {
    var ls = leftColumn.expression.expressionToString();
    var rs = rightColumn.expression.expressionToString();
    if (leftColumn.filter) {
        ls += JSON.stringify(leftColumn.filter);
    }
    if (rightColumn.filter) {
        rs += JSON.stringify(rightColumn.filter);
    }
    return ls < rs ? -1 : (rs < ls ? 1 : 0);
};

visualizer.Stage.fromJSON = function(json) {
    var stage = new visualizer.Stage();
    Object.keys(json).map(function(key) {
        stage[key] = json[key];
    });
    var allColumns = stage.allColumnsAndUnvisualized();
    stage.internalSortOrder = stage.internalSortOrder.filter(function(x) {
        return x > -1;
    }).map(function(index) {
        return allColumns[index];
    });
    stage.externalSortOrder = stage.externalSortOrder.filter(function(x) {
        return x > -1;
    }).map(function(index) {
        return allColumns[index];
    });
    return stage;
};

visualizer.Stage.prototype = {
    tableAllColumns: function() {
        return this.Columns;
    },
    tableSwapItems: function(old_index, new_index) {
        if (new_index >= this.Columns.length) {
            var k = new_index - this.Columns.length;
            while ((k--) + 1) {
                this.Columns.push(undefined);
            }
        }
        this.Columns.splice(new_index, 0, this.Columns.splice(old_index, 1)[0]);
        return this;
    },
    tableAllColumnsAndUnvisualized: function() {
        var cols = [].concat(this.Columns);
        _.each(this.unused, function(col) {
            cols.push(col);
        });
        return cols;
    },
    tableRemoveColumn: function(col) {
        this.Columns = _.without(this.Columns, col);
    },
    convertStageTo: function(chartType) {
        if (chartType !== 'table') {
            this.RecentlyUsed = [];
            _.each(this.Columns, function(col) {
                var ignoreFilters = true;
                var ignoreTableBucket = true;
                if (!this.isColumnNameInStage(col, ignoreFilters, ignoreTableBucket)) {
                    this.addColumn(col);
                }
            }, this);
            this.Columns = null;
            return this.RecentlyUsed;
        } else if (chartType === 'table') {
            this.Columns = [];

            _.each(this.allColumns(), function(col) {
                if (col.expression.isAttribute() || col.category === 'attribute') {
                    this.Columns.unshift(col);
                } else {
                    this.Columns.push(col);
                }
            }, this);
        }
    },
    toJSON: function() {
        var allColumns = this.allColumnsAndUnvisualized();

        function getIndex(col) {
            return allColumns.indexOf(col);
        }
        var ret = {
            _class: "Stage",
            measures: this.measures,

            // For internal/external sort order, they should share
            // objects with dimensions - not have their own copies of
            // the columns. So we serialize the index within
            // allColumns.
            internalSortOrder: this.internalSortOrder.map(getIndex),
            externalSortOrder: this.externalSortOrder.map(getIndex),
            colorDistribution: this.colorDistribution,
            colorDistributionControl: this.colorDistributionControl,
            unused: this.unused
        };
        visualizer.Stage.dimensions.map(function(dim) {
            ret[dim] = this[dim];
        }, this);
        ret.Columns = this.Columns;
        return ret;
    },

    allColumns: function(chartType) {
        if (chartType === 'table') {
            return this.tableAllColumns();
        }
        var cols = _.concatMap(this.measures, function(measureAxis) {
            return _.pluck(measureAxis.series, 'column');
        });
        this.mapDimensions(function(dim) {
            if (this[dim]) {
                cols.push(this[dim]);
            }
        });
        // Sort the columns so that rearranging the columns
        // does not cause a new query to run.
        cols = cols.sort(visualizer.Stage.columnSort);
        return cols;
    },

    unvisualizedColumns: function() {
        return this.unused;
    },

    allColumnsAndUnvisualized: function(chartType) {
        if (chartType === 'table') {
            return this.tableAllColumnsAndUnvisualized();
        }
        var cols = this.allColumns();
        _.each(this.unused, function(col) {
            cols.push(col);
        });

        cols = cols.sort(visualizer.Stage.columnSort);

        return cols;
    },

    internalSortOrder: [],
    externalSortOrder: [],
    updateSortOrder: function() {
        this.externalSortOrder = [];

        if (this.HorizontalTrellis || this.VerticalTrellis) {
            if (this.HorizontalTrellis && !this.HorizontalTrellis.properties.sortDirection) {
                this.HorizontalTrellis.properties.sortDirection = 'ASC';
            }
            if (this.VerticalTrellis && !this.VerticalTrellis.properties.sortDirection) {
                this.VerticalTrellis.properties.sortDirection = 'ASC';
            }
            this.externalSortOrder = this.HorizontalTrellis ? [].concat([this.HorizontalTrellis]) : [];
            this.externalSortOrder = this.VerticalTrellis ? this.externalSortOrder.concat([this.VerticalTrellis]) : this.externalSortOrder;
        }

        this.externalSortOrder = this.externalSortOrder.concat(this.internalSortOrder);
    },
    addToSortOrder: function(column) {
        column.properties.sortDirection = column.properties.sortDirection || 'ASC';
        this.internalSortOrder.push(column);
        this.updateSortOrder();
    },
    removeFromSortOrder: function(column) {
        column.properties.sortDirection = null;
        this.internalSortOrder = _.without(this.internalSortOrder, column);
        this.updateSortOrder();
    },
    sortBQLString: function() {
        var dataSort = _.filter(this.externalSortOrder, function(column) {
            return !column.expression.isNotBasicColumn();
        });
        var displaySort = _.filter(this.externalSortOrder, function(column) {
            return column.expression.isNotBasicColumn();
        });
        var dataSortString, displaySortString;

        if (!dataSort.length) {
            dataSortString = '';
        } else {
            dataSortString = ' ORDER BY ' + _.map(dataSort, function(column) {
                return column.expression.expressionToString() + ' ' + column.properties.sortDirection;
            }).join(', ');
        }

        if (!displaySort.length) {
            displaySortString = '';
        } else {
            displaySortString = ' DISPLAY BY ' + _.map(displaySort, function(column) {
                return '[EXPR.' + column.name + column.uniqueId + '] ' + column.properties.sortDirection;
            }).join(', ');
        }
        return dataSortString + displaySortString;
    },
    getBQL: function(column) {
        return column.expression.expressionToString().replace(/\[|\]/g, '');
    },

    allFilters: function(chartType) {
        return _.filter(this.allColumnsAndUnvisualized(chartType), function(item) {
            return item.filter || item.displayFilter;
        });
    },

    clear: function() {
        _.map(visualizer.Stage.dimensions, function(dim) {
            this[dim] = null;
        }, this);
        this.measures = [];
        this.Columns = [];
        this.unused = [];
        this.internalSortOrder = [];
        this.externalSortOrder = [];
        this.colorDistributionControl = true;
        this.colorDistribution = 'Stack';
    },

    mapDimensions: function(fn) {
        return _.map(visualizer.Stage.dimensions, fn, this);
    },

    updateAxesMinMax: function() {
        _.each(this.getAxes(), function(axis) {
            this.updateAxisMinMax(axis);
        }, this);
    },

    updateAxisMinMax: function(axis) {
        var seriesMinimums = _.pluck(axis.series, 'min');
        var seriesMaximums = _.pluck(axis.series, 'max');

        //console.log('seriesMinimums', seriesMinimums)

        axis.min = seriesMinimums.length ? _.min(seriesMinimums) : 0;
        axis.max = seriesMaximums.length ? _.min(seriesMaximums) : 0;

        //console.log('setting axis min/max to ', axis.min, axis.max, 'after removal');
    },

    removeColumn: function(col, chartType) {
        if (chartType === 'table') {
            this.tableRemoveColumn(col);
        }

        if (col === this.Color) {
            this.setColorGrouping('Stack');
        }

        var oldDimension = this.mapDimensions(function(dim) {
            if (this[dim] === col) {
                this[dim] = null;
                return dim;
            }
        });

        oldDimension = _.compact(oldDimension);

        if (oldDimension.length === 1) {
            return oldDimension[0];
        }

        _.each(this.measures, function(axis) {
            axis.series = _.filter(axis.series, function(serie) {
                return serie.column !== col;
            });

            this.updateAxisMinMax(axis);
        }, this);

        this.unused = _.without(this.unused, col);
    },

    removeFilter: function(column, chartType) {
        if (column.properties.visualized === true) {
            column.filter = null;
            column.displayFilter = null;
            column.properties.isFilterOn = false;
        } else {
            this.removeColumn(column, chartType);
        }
        this.updateSortOrder();
    },

    isColumnInStage: function(col, ignoreFilter, ignoreTableBucket) {
        var columnExists = false;
        // dimensions bucket test
        this.mapDimensions(function(dim) {
            if (this[dim] === col) {
                columnExists = true;
            }
        });
        // measures bucket test
        _.map(this.measures, function(measureAxis) {
            _.each(measureAxis.series, function(serie) {
                if (serie.column === col) {
                    columnExists = true;
                }
            });
        });
        // Columns bucket test
        if (_.contains(this.Columns, col) && !ignoreTableBucket) {
            columnExists = true;
        }
        // filters bucket test
        if (_.contains(this.unused, col) && !ignoreFilter) {
            columnExists = true;
        }
        return columnExists;
    },

    isColumnNameInStage: function(column) {
        var columnExists = false;
        // dimensions bucket test
        this.mapDimensions(function(dim) {
            if (this[dim] && this[dim].expression.expressionToString() === column.expression.expressionToString()) {
                columnExists = true;
            }
        });
        // measures bucket test
        _.map(this.measures, function(measureAxis) {
            _.each(measureAxis.series, function(serie) {
                if (serie.column.expression.expressionToString() === column.expression.expressionToString()) {
                    columnExists = true;
                }
            });
        }); // columns bucket test
        _.map(this.Columns, function(col) {
            if (col.expression.expressionToString() === column.expression.expressionToString()) {
                columnExists = true;
            }
        });
        // filters bucket test
        _.map(this.unused, function(filter) {
            if (filter.expression.expressionToString() === column.expression.expressionToString()) {
                columnExists = true;
            }
        });
        return columnExists;
    },

    findColumnInStage: function(column) {
        var foundColumn;

        // dimensions bucket test
        this.mapDimensions(function(dim) {
            if (this[dim] && this[dim].expression.expressionToString() === column.expression.expressionToString()) {
                foundColumn = this[dim];
            }
        });
        // measures bucket test
        _.map(this.measures, function(measureAxis) {
            _.each(measureAxis.series, function(serie) {
                if (serie.column.expression.expressionToString() === column.expression.expressionToString()) {
                    foundColumn = serie.column;
                }
            });
        });
        // colmuns bucket test
        _.map(this.Columns, function(col) {
            if (col.expression.expressionToString() === column.expression.expressionToString()) {
                foundColumn = col;
            }
        });
        // filters bucket test
        _.map(this.unused, function(filter) {
            if (filter.expression.expressionToString() === column.expression.expressionToString()) {
                foundColumn = filter;
            }
        });

        return foundColumn;
    },

    assignColumnUniqueId: function(column) {
        if (!column) {
            return;
        }

        if (!('uniqueId' in column)) {
            column.uniqueId = _.uniqueId();
        }

        return column.uniqueId;
    },

    // addColumn is called with a column and an optional dimension.
    // If the dimension is present, we add the column to that
    // particular dimension. If dimension is absent, the column is
    // added to the first slot available.
    // chartType signifies how to plot the series: line, area, scatter...
    addColumn: function(col, dim, chartType, options, action) {
        if (!col || _.isArray(col)) {
            //console.log('EXPECTED COLUMN in addColumn');
            return;
        } else {
            //console.log('stage.addColumn, ', col);
        }
        var foundColumn;

        col.properties.visualized = options && options.visualize !== undefined ? options.visualized : true;
        col.properties.isFilterOn = options && options.isFilterOn !== undefined ? options.isFilterOn : true;

        this.assignColumnUniqueId(col);

        if (dim === 'filter') {
            if (!this.isColumnNameInStage(col)) {
                //console.log('column ', col, ' not in stage!  adding as filter.');
                col.properties.visualized = options && options.visualized || false;
                this.unused.push(col);
            } else {
                //console.log('column ', col, ' is already in stage!  not adding as filter!');
                foundColumn = this.findColumnInStage(col);

                if (foundColumn.filter) {
                    foundColumn.filter.items = foundColumn.filter.items.concat(col.filter.items);
                } else {
                    foundColumn.filter = col.filter;
                }
                foundColumn.properties.isFilterOn = true;
            }
            return;
        }

        if (dim === 'prompt') {
            if (!this.isColumnNameInStage(col)) {
                //console.log('column ', col, ' not in stage!  adding as filter.');
                col.properties.visualized = options && options.visualized || false;
                this.unused.push(col);
            } else {
                //console.log('column ', col, ' is already in stage!  not adding as filter!');
                foundColumn = this.findColumnInStage(col);

                if (action === 'add') {
                    foundColumn.prompts = col.prompts;
                } else if (action === 'remove') {
                    foundColumn.prompts = null;
                }

                foundColumn.properties.isFilterOn = true;
            }
            return;
        }

        if (chartType === 'table') {
            if (col.expression.isAttribute() || col.category === 'attribute') {
                this.Columns.unshift(col);
                this.addColumn(col);
            } else {
                this.Columns.push(col);
                this.addSeries(col, chartType, options ? options.axis : undefined, options ? options.stackType : undefined);
            }
            return;
        }

        var oldDimension = this.removeColumn(col);

        if (dim) {
            var oldColumn = this[dim];

            this[dim] = col;

            if (oldColumn) {
                this.addColumn(oldColumn, oldDimension);
            }
        } else {
            if (col.expression.isAttribute() || col.category === 'attribute') {
                var finished = false;

                this.mapDimensions(function(dim) {
                    if (finished || dim === 'Size' || !this.shouldShowDimension(chartType, dim)) {
                        return;
                    }
                    if (!this[dim]) {
                        finished = true;
                        this[dim] = col;
                    }
                });

                if (!finished) {
                    if (!this.RecentlyUsed) {
                        this.RecentlyUsed = [];
                    }
                    this.RecentlyUsed.push(col);
                }
            } else {
                if (chartType === 'scatter' && this.allSeries().length > 0 && !this.Category) {
                    this.Category = col;
                } else {
                    this.addSeries(col, chartType, options ? options.axis : undefined, options ? options.stackType : undefined);
                }
            }
        }
    },

    addFilter: function(column, options) {
        this.addColumn(column, 'filter', null, options);
    },

    updatePrompt: function(column, options, action) {
        this.addColumn(column, 'prompt', null, options, action);
    },

    // need to wait for response from query before assigning an axis

    addSeriesWithTemporaryAxis: function(col, seriesPlotType, stackType) { // (1) someone adds a new series with no assigned axis
        col.properties.visualized = true; // why not default to true?

        if (!('uniqueId' in col)) {
            col.uniqueId = _.uniqueId();
        }

        var temporaryAxis = this.addAxis();

        temporaryAxis.temporary = true;

        return this.addAxisColumn(col, temporaryAxis, seriesPlotType, stackType);
    },

    addSeries: function(col, seriesPlotType, axis, stackType) {
        if (!axis) { // add to a temporary axis
            return this.addSeriesWithTemporaryAxis(col, seriesPlotType, stackType);
        } else {
            return this.addAxisColumn(col, axis, seriesPlotType, stackType);
        }
    },

    addMinMax: function(resultSets) { // (2) queryService returns and calls this with the resultSets so we can find the min/max
        // stage.measures[0].series is an array of { column: column, seriesDimensions: ..., type: }
        var allSeries = this.allSeries();
        //no point of doing this if there are no series, so just exit
        if (this.allSeries().length < 1) {
            return;
        }
        //this.getAllColumnsPerSeriesQuery ? or this.allSeries?
        _.each(resultSets, function(resultSet, i) {
            // we want to set min/max for the series based on results from the rows...
            // min and max are for the measure

            // it happens to be that the first column in each resultSet is the measure, because of how this.getSeriesColumns is written
            // which is used by this.allSeriesColumnsPerQuery

            var measureValues = _.pluck(resultSet.rows, 0);

            allSeries[i].min = _.min(measureValues);
            allSeries[i].max = _.max(measureValues);

            //console.log('min, max', allSeries[i].min, allSeries[i].max);
        }, this);

        this.updateAxesMinMax();
        this.assignSeriesFromTemporaryAxes();
    },

    addDataTypeToColumns: function(resultSets) {
        var allColumns = this.allColumns();

        _.each(allColumns, function(column) {
            if (!column.properties.type) {
                _.each(resultSets, function(resultSet) {
                    _.each(resultSet.columnNames, function(columnName, index) {
                        if (_.last(this.getBQL(column).split('.')) === columnName) {
                            column.properties.type = resultSet.dataTypes[index];
                            column.properties.warehouseDataType = resultSet.warehouseColumnDataTypes[index];
                        }
                    }, this);
                }, this);
            }
        }, this);
    },

    getAxes: function() {
        return this.measures;
    },

    assignSeriesFromTemporaryAxes: function() { // (3) queryService calls assignUnassignedAxes (probably someone else should but....)
        var that = this;

        function isGoodFitAxis(series, axis) {
            return series.max > (0.2 * axis.max) && series.min > (0.2 * axis.min) &&
                series.max < (5 * axis.max) && series.min < (5 * axis.min);
        }

        function getGoodFitAxis(series) {
            var goodFitAxis; // eventually try for best fit

            _.each(that.getAxes(), function(axis) {
                if (isGoodFitAxis(series, axis)) {
                    goodFitAxis = axis;
                    return false;
                }
            });

            return goodFitAxis;
        }

        _.each(that.getAxes(), function(axis) {
            if (axis.temporary) { // there should only be one of these
                _.each(axis.series, function(series) { // again, probably only one, but just in case...
                    var goodFitAxis = getGoodFitAxis(series);

                    if (goodFitAxis && goodFitAxis !== axis) {
                        //console.log('moving to existing non-temp axis');
                        that.moveSeries(series, axis, goodFitAxis);
                    } else {
                        //console.log('making temp axis permanent');
                        axis.temporary = false; // let next series possibly use it as well
                    }
                });

                if (!axis.series.length) {
                    that.removeAxis(axis);
                }
            }
        });
    },

    moveSeries: function(serieToMove, originalAxis, newAxis) {
        originalAxis.series = _.filter(originalAxis.series, function(serie) {
            return serie !== serieToMove;
        });
        newAxis.series.push(serieToMove);
    },

    changeChartType: function(chartType) {
        _.each(this.allSeries(), function(element, index) {
            this.allSeries()[index].type = chartType;
        }, this);

        // hack solution, but it works and I can't think of a better quick fix at the moment

        var $injector = angular.injector(['visualizer']);

        $injector.invoke(function(chartStateService) {
            chartStateService.setCurrentChartType(chartType);
        });
        /*
        setInterval(function() {
            var $injector = angular.injector(['visualizer']);

            $injector.invoke(function(chartStateService) {
                //console.log('currentChartTypeX', chartStateService.getCurrentChartType());
            });
        }, 1000);*/

        /*
        var chartStateService = $injector.get('chartStateService');

        chartStateService.setCurrentChartType(chartType);*/
    },

    dimAllowedForType: function(type, dim) {
        if (!type || !dim) {
            return;
        }
        if (type === 'column' || type === 'bar' || type === 'area' || type === 'areaspline' || type === 'line' || type === 'spline') {
            return _.contains(['Category', 'Color', 'HorizontalTrellis', 'VerticalTrellis'], dim);
        }
        if (type === 'bubble') {
            return _.contains(['Category', 'Color', 'Size', 'HorizontalTrellis', 'VerticalTrellis'], dim);
        }
        if (type === 'points') {
            return _.contains(['Category', 'Color', 'Shape', 'HorizontalTrellis', 'VerticalTrellis'], dim);
        }
        if (type === 'scatter') {
            return _.contains(['Category', 'Color', 'Shape', 'Size', 'HorizontalTrellis', 'VerticalTrellis'], dim);
        }
        if (type === 'pie') {
            return _.contains(['Category', 'HorizontalTrellis', 'VerticalTrellis'], dim);
        }
        if (type === 'table') {
            return _.contains(['Columns'], dim);
        }
        throw "Expected type to be line, column, bar, or scatter";
    },

    addAxisColumn: function(col, axis, chartType, stackType) {
        col.properties.visualized = true;
        //console.log('adding axis column');
        if (!('uniqueId' in col)) {
            col.uniqueId = _.uniqueId();
        }

        if (!this.measures.length) {
            this.addAxis();
        }

        if (!axis) {
            axis = this.measures[0];
        }

        this.removeColumn(col);

        var seriesDimensions = {};

        // by default, for the new axis, group by each dimension (rather than stacking/disabling)

        this.mapDimensions(function(dim) {
            if (dim !== 'Category' && dim !== 'HorizontalTrellis' && dim !== 'VerticalTrellis') {
                seriesDimensions[dim] = true;
            }
        });

        if (!stackType) {
            stackType = 'Stack';
        }

        var seriesColumn = {
            column: col,
            type: chartType,
            seriesDimensions: seriesDimensions,
            colorDistribution: stackType
        };

        axis.series.push(seriesColumn);

        return seriesColumn;
    },

    mergeAxes: function(axes, scope) {
        var that = this;

        if (!axes) {
            axes = this.getAxes().slice(); // make a copy so when we add an axis we don't iterate over it to remove axes
        }

        var mergedAxis = this.addAxis();

        _.each(axes, function(axis) {
            _.each(axis.series, function(series) {
                mergedAxis.series.push(series);
            });

            that.removeAxis(axis);
        });

        scope.safeApply();
        scope.updateChart();

        console.log('this.axes', this.getAxes());
    },

    unmergeAxes: function(axes, scope) {
        var that = this;

        if (!axes) {
            axes = this.getAxes();
        }

        _.each(axes, function(axis) {
            if (axis.series && axis.series.length > 1) {
                _.each(axis.series.slice(1), function(series) {
                    var newAxis = that.addAxis();

                    newAxis.series.push(series);
                });

                axis.series = axis.series.slice(0, 1);
            }
        });

        scope.safeApply();
        scope.updateChart();
    },

    hasSharedAxes: function() {
        var axes = this.getAxes();

        return _.any(axes, function(axis) {
            return axis.series && axis.series.length > 1;
        });
    },

    hasMultipleAxes: function() {
        var numAxesWithSeries = 0;

        var axes = this.getAxes();

        _.each(axes, function(axis) {
            if (axis.series.length >= 1) {
                numAxesWithSeries += 1;
            }
        });

        if (numAxesWithSeries > 1) {
            return true;
        } else {
            return false;
        }
    },

    toggleMergeAxes: function(scope) {
        if (this.hasSharedAxes()) {
            this.unmergeAxes(undefined, scope);
        } else {
            this.mergeAxes(undefined, scope);
        }
    },

    getSeriesDimension: function(series, dim) {
        if (!this[dim] || !this[dim].expression.isValid()) {
            return false;
        }
        if (dim === 'Category') {
            if (series.type === 'scatter' || series.type === 'bubble' || this[dim].expression.isAttributeOrCustomExpression()) {
                return series.seriesDimensions[dim];
            } else {
                return false;
            }
        }
        if (dim === 'HorizontalTrellis' || dim === 'VerticalTrellis') {
            return this[dim] && this[dim].expression.isValid();
        }
        if (!this.dimAllowedForType(series.type, dim)) {
            return false;
        }
        if (_.contains(['line', 'spline', 'area', 'areaspline'], series.type) && dim === 'Color' && this.Color.expression.isMeasure()) {
            return false;
        }
        return series.seriesDimensions[dim];
    },

    getSeriesColumns: function(series) {
        if (!series.column.expression.isValid()) {
            //console.log('invalid expression');
            return undefined;
        }

        if (this.getSeriesDimension(series, 'Category') === false) {
            //console.log('no category');
            return undefined;
        }

        var ret = [{
            series: series,
            dim: 'measure',
            column: series.column
        }, {
            series: series,
            dim: 'Category',
            column: this.Category
        }];

        if (this.HorizontalTrellis && this.getSeriesDimension(series, 'HorizontalTrellis')) {
            ret.push({
                series: series,
                dim: 'HorizontalTrellis',
                column: this.HorizontalTrellis
            });
        }
        if (this.VerticalTrellis && this.getSeriesDimension(series, 'VerticalTrellis')) {
            ret.push({
                series: series,
                dim: 'VerticalTrellis',
                column: this.VerticalTrellis
            });
        }

        _.map(_.keys(series.seriesDimensions), function(dim) {
            if (this.getSeriesDimension(series, dim) !== false) {
                ret.push({
                    series: series,
                    dim: dim,
                    column: this[dim]
                });
            }
        }, this);

        //console.log('ret', ret)

        ret = ret.sort(function(l, r) {
            return visualizer.Stage.columnSort(l.column, r.column);
        });

        //console.log('ret', ret)

        return ret;
    },

    serieColorGroupings: function() {
        var chartTypes = _.map(this.allSeries(), function(serie) {
            return serie.type;
        });
        if (_.contains(chartTypes, 'column') || _.contains(chartTypes, 'bar')) {
            this.colorDistributionControl = true;
            return ['Stack', 'Group', 'Percent'];
        } else if (_.contains(chartTypes, 'area') || _.contains(chartTypes, 'areaspline')) {
            this.colorDistributionControl = true;
            if (this.colorDistribution === 'Group') {
                this.setColorGrouping('Stack');
            }
            return ['Stack', 'Percent'];
        } else {
            this.colorDistributionControl = false;
            return;
        }
    },
    colorDistributionControl: true,
    colorDistribution: 'Stack',
    getColorGrouping: function() {
        return this.colorDistribution;
    },

    setColorGrouping: function(grouping) {
        // over ride all series with selected grouping type:
        var allSeries = this.allSeries();

        _.each(allSeries, function(serie) {
            if (serie.type === 'column' || serie.type === 'bar') {
                serie.colorDistribution = grouping;
                this.colorDistribution = grouping;
            } else if (serie.type === 'area' || serie.type === 'areaspline') {
                if (grouping === 'Group') {
                    serie.colorDistribution = 'Stack';
                    this.colorDistribution = 'Stack';
                } else {
                    serie.colorDistribution = grouping;
                    this.colorDistribution = grouping;
                }
            } else {
                serie.colorDistribution = 'Stack';
                this.colorDistribution = 'Stack';
            }
        }, this);
    },

    allSeries: function() {
        // this.measures is an array of axes, and we're returning the axis.series, which means plural series
        // as opposed to the perhaps intended singular series... series is in fact an array
        // but we're flattening it with concatMap so this function returns a flat array
        return _.concatMap(this.measures, function(measure) {
            return measure.series;
        });
    },

    allValidSeries: function() {
        return _.filter(this.allSeries(), function(serie) {
            return this.getSeriesColumns(serie) !== undefined;
        }, this);
    },

    getAllColumnsPerSeriesQuery: function() {
        //console.log('this.allSeries', this.allSeries());

        var seriesColumns = _.map(this.allSeries(), this.getSeriesColumns, this);
        // but allSeries really means allAxes?  maybe not... allSeries returns measure.series

        var definedSeriesColumns = _.filter(seriesColumns, function(seriesColumn) {
            return seriesColumn !== undefined; // does this do anything???
        });

        return definedSeriesColumns;
    },

    getCategoryColumn: function() {
        return this.Category;
    },

    setCategoryColumn: function(column) {
        if (!column) {
            console.log('EXPECTED COLUMN in setCategoryColumn');
        }

        this.assignColumnUniqueId(column);

        this.addColumn(column, 'Category');

        return column;
    },

    validSeriesByAxis: function() {
        // 4 nested returns all smashed into a couple lines?  really??

        return _.filter(_.map(this.measures, function(axis) {
                return {
                    axis: axis,
                    series: _.filter(axis.series, function(it) {
                        return this.getSeriesColumns(it) !== undefined;
                    }, this)
                };
            }, this), function(axisSeries) {
                return axisSeries.series.length > 0;
            },
            this
        );
    },

    seriesAxis: function(series) {
        return _.filter(this.measures, function(measure) {
            return _.contains(measure.series, series);
        })[0];
    },

    //creates new Y axis and returns its object so column could be added to it as single transaction

    addAxis: function() {
        var newAxis = {
            name: "New Axis",
            series: []
        };

        this.measures.push(newAxis);

        return newAxis;
    },

    removeAxis: function(axis) {
        this.measures = _.without(this.measures, axis);
    },


    guidedModeRecommendation: function(dim, chartType) {
        var self = this;

        function attr() {
            if (self[dim] && (self[dim].expression.isAttribute() || self[dim].category === "attribute")) {
                return {
                    ok: true
                };
            } else if (self[dim]) {
                return {
                    ok: false,
                    message: "replace with Attribute"
                };
            } else {
                return {
                    ok: false,
                    message: "add Attribute"
                };
            }
        }

        function measure() {
            if (self[dim] && (self[dim].expression.isMeasure() || self[dim].category === "measure")) {
                return {
                    ok: true
                };
            } else if (self[dim]) {
                return {
                    ok: false,
                    message: "replace with Measure"
                };
            } else {
                return {
                    ok: false,
                    message: "add Measure"
                };
            }
        }

        function attr_or_measure() {
            if (self[dim]) {
                return {
                    ok: true
                };
            } else {
                return {
                    ok: false,
                    message: "add Attr. or Meas."
                };
            }
        }

        function nothing() {
            if (self[dim]) {
                return {
                    ok: false,
                    message: "please remove"
                };
            }
            return {
                ok: true
            };
        }
        // function any() {
        //     return {ok: true};
        // }

        function option(x) {
            if (!self[dim]) {
                return {
                    ok: true
                };
            } else {
                return x;
            }
        }
        switch (dim) {
            case "Category":
                if (_.contains(["scatter", "bubble"], chartType)) {
                    return attr_or_measure();
                } else {
                    return attr();
                }
                break;
            case "Color":
                if (_.contains(["line", "spline", "area", "areaspline"], chartType)) {
                    return option(attr());
                } else if (_.contains(["column", "bar", "points", "scatter"], chartType)) {
                    return option(attr_or_measure());
                } else if (_.contains(["bubble"], chartType)) {
                    return attr();
                } else {
                    return nothing();
                }
                break;
            case "Shape":
                if (_.contains(["points"], chartType)) {
                    return option(attr());
                } else if (_.contains(["scatter"], chartType)) {
                    return attr();
                } else {
                    return nothing();
                }
                break;
            case "Size":
                if (_.contains(["bubble"], chartType)) {
                    return measure();
                } else if (_.contains(["points", "scatter"], chartType)) {
                    return option(measure());
                } else {
                    return nothing();
                }
                break;
        }
    },

    shouldShowDimension: function(chartType, dim) {
        if (this[dim]) {
            return true;
        }
        if (this.dimAllowedForType(chartType, dim)) {
            return true;
        }
        return _.any(this.allSeries(), function(serie) {
            return this.dimAllowedForType(serie.type, dim);
        }, this);
    },

    chartRecommendations: function(column, chartType) {
        if (this.allSeries().length) {
            // Override the guided mode chart type with the actual chart type, if available
            chartType = this.allSeries()[0].type;
        }
        var possibilities = [];
        var isMeasure = column.expression.isMeasure() || column.category === 'measure';
        var lineType = _.contains(["spline", "line"], chartType);
        var areaType = _.contains(["areaspline", "area"], chartType);
        var columnType = _.contains(["column", "bar"], chartType);
        //var pointType = _.contains(["scatter", "points", "bubble"], chartType);
        var stage = this;

        function newSeriesPossibilities() {
            if (!_.contains(["pie", "table"], chartType)) {
                possibilities.push(seriesPossibility(chartType));
            }

            !lineType && possibilities.push(seriesPossibility("line"));
            !columnType && possibilities.push(seriesPossibility("column"));
            !areaType && possibilities.push(seriesPossibility("area"));
        }

        function trellisPossibilities() {
            if (!this.HorizontalTrellis) {
                possibilities.push(possibility(chartType, "HorizontalTrellis", "trellis"));
            } else if (!this.VerticalTrellis) {
                possibilities.push(possibility(chartType, "VerticalTrellis", "trellis"));
            }
        }

        function linePossibilities(type) {
            if (!isMeasure && !stage.Color && !stage.Size && !stage.Shape) {
                possibilities.push(colorPossibility(type, "split"));
            }
        }

        function areaPossibilities(type) {
            if (!isMeasure && !stage.Color && !stage.Size && !stage.Shape) {
                possibilities.push(colorPossibility(type, "stack"));
            }
        }

        function columnPossibilities(type) {
            if (!stage.Color && !stage.Size && !stage.Shape) {
                if (isMeasure) {
                    possibilities.push(possibility(type, "Color"));
                } else {
                    possibilities.push(colorPossibility(type, "stack"));
                    possibilities.push(colorPossibility(type, "split"));
                }
            }
        }

        function pointPossibilities() {
            // bubble - size + color(attr)
            // points - color + shape
            // scatter - size + color + shape
            var by_type = {
                bubble: [],
                points: [],
                scatter: []
            };
            var c = !! stage.Color,
                sh = !! stage.Shape,
                si = !! stage.Size;
            if (!c && sh && si) {
                if (isMeasure) {
                    by_type.scatter.push(possibility("scatter", "Color"));
                } else {
                    by_type.scatter.push(colorPossibility("scatter", "split"));
                }
            }
            if (c && !sh && si && !isMeasure) {
                by_type.scatter.push(possibility("scatter", "Shape"));
            }
            if (c && sh && !si && isMeasure) {
                by_type.scatter.push(possibility("scatter", "Size"));
            }
            if (c && !sh && !si) {
                if (isMeasure) {
                    if (chartType === "scatter") {
                        by_type.scatter.push(possibility("scatter", "Size"));
                    } else {
                        by_type.bubble.push(possibility("bubble", "Size"));
                    }
                } else {
                    if (chartType === "scatter") {
                        by_type.scatter.push(possibility("scatter", "Shape"));
                    } else {
                        by_type.points.push(possibility("points", "Shape"));
                    }
                }
            }
            if (!c && sh && !si) {
                if (isMeasure) {
                    if (chartType === "scatter") {
                        by_type.scatter.push(possibility("scatter", "Color"));
                    } else {
                        by_type.points.push(possibility("points", "Color"));
                    }
                } else {
                    if (chartType === "scatter") {
                        by_type.scatter.push(colorPossibility("scatter", "split"));
                    } else {
                        by_type.points.push(colorPossibility("points", "split"));
                    }
                }
            }
            if (!c && !sh && si && !isMeasure) {
                by_type.bubble.push(colorPossibility("bubble", "split"));
            }
            if (!c && !sh && !si && !isMeasure) {
                by_type.points.push(colorPossibility("points", "split"));
                by_type.points.push(possibility("points", "shape"));
            }

            var same = by_type[chartType] || [];
            var other = _.without(['points', 'bubble', 'scatter'], chartType).concatMap(function(type) {
                return by_type[type];
            });

            possibilities = same.concat(possibilities).concat(other);
        }

        function seriesPossibility(type) {
            return {
                className: 'preview-' + type + '-add-series',
                caption: type + ": add series",
                action: function() {
                    stage.addSeriesWithTemporaryAxis(column, type);
                }
            };
        }

        function colorPossibility(type, action) {
            return {
                className: 'preview-' + type + '-add-color-' + action,
                caption: type + ": " + (action === 'split' ? "split by color" : "add color and stack"),
                action: function() {

                    stage.addColumn(column, "Color");
                    if (action === 'split') {
                        stage.setColorGrouping('Group');
                    } else {
                        stage.setColorGrouping('Stack');
                    }
                    _.map(stage.allSeries(), function(serie) {
                        serie.seriesDimensions.Color = true;
                    });
                    stage.changeChartType(type);
                }
            };
        }

        function possibility(type, dim, displayDim) {
            displayDim = displayDim || dim.toLowerCase();
            return {
                className: 'preview-' + type + '-add-' + displayDim,
                caption: type + ": add " + displayDim,
                action: function() {
                    stage.addColumn(column, dim);
                    stage.changeChartType(type);
                }
            };
        }

        lineType && linePossibilities(chartType);
        areaType && areaPossibilities(chartType);
        columnType && columnPossibilities(chartType);

        if (isMeasure) {
            newSeriesPossibilities();
        }
        if (!isMeasure) {
            trellisPossibilities();
        }

        !lineType && linePossibilities("line");
        !areaType && areaPossibilities("area");
        !columnType && columnPossibilities("column");

        pointPossibilities();

        return possibilities;
    }
};
