visualizer.models.columnCreator = function(BqlNode, AdhocWebService) {

    function determineExpression(protoColumn) {
        if (protoColumn.type === 'saved expression') {
            if (!protoColumn.subtype) {
                AdhocWebService.getColumnDefinition(protoColumn.value, function(columnDefinition) {
                    protoColumn.subtype = columnDefinition.isMeasureExpression ? 'measure' : 'attribute';
                    return;
                });
            }
            return new BqlNode(protoColumn, []);
        } else if (protoColumn.expression instanceof BqlNode) {
            return protoColumn.expression;
        } else if (protoColumn.type === 'measure') {
            return BqlNode.fromMeasure(protoColumn);
        } else if (protoColumn.type === 'attribute') {
            return BqlNode.fromAttribute(protoColumn);
        }
    }

    function createColumn(name, category, expression, reportExpressionId, filter, displayFilter, properties, extend) {
        if (_.isString(expression)) {
            expression = BqlNode.parseFromString(expression);
        }
        var column = {
            name: name,
            filter: filter || undefined,
            displayFilter: displayFilter || undefined,
            properties: properties || {
                isFilterOn: false,
                visualized: false,
                sortDirection: null,
                type: null
            },
            category: category || null,
            expression: expression,
            displayAggregation: undefined,
            measuresToFilter: {}
        };
        if(reportExpressionId) {
            column.reportExpressionId = reportExpressionId;
        }

        if (extend) {
            column = _.extend(column, extend);
        }

        return column;
    }

    function createFromProto(protoColumn) {
        return createColumn(protoColumn.displayName, protoColumn.category, determineExpression(protoColumn), protoColumn.reportExpressionId);
    }

    return {
        createColumn: createColumn,
        createFromProto: createFromProto
    };
};

angular.module('visualizer').service('columnCreator', ['BqlNode', 'AdhocWebService', visualizer.models.columnCreator]);
