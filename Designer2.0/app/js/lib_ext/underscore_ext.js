_.mixin({
    'containsOnly': function(list, set) {
        var notInSet = _.filter(list, function(item) {
            return !_.contains(set, item);
        });

        return notInSet.length === 0;
    },

    'endsWith': function(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    },

    'concatMap': function(arr, fn, ctxt) { // maps then flattens, or intends to
        return [].concat.apply([], _.map(arr, fn, ctxt));
    },
    'trueKeys': function(object) {
        return _.keysWhere(object, true);
    },

    'truthyKeys': function(object) {
        return _.keysWhere(object, function(key) {
            return key === true;
        });
    },

    'widthWithoutScrollbar': function(selector) {
        var tempDiv = $("<div/>");
        $(selector).append(tempDiv);
        var elemwidth = tempDiv.width();
        tempDiv.remove();
        return elemwidth;
    },

    'hexToRgb': function(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    },

    'hslToRgb': function(h, s, l) {
        var r, g, b;

        function hue2rgb(p, q, t) {
            if (t < 0) {
                t += 1;
            }
            if (t > 1) {
                t -= 1;
            }
            if (t < 1 / 6) {
                return p + (q - p) * 6 * t;
            }
            if (t < 1 / 2) {
                return q;
            }
            if (t < 2 / 3) {
                return p + (q - p) * (2 / 3 - t) * 6;
            }
            return p;
        }

        function decToHex(dec) {
            var hex = Math.round(dec * 255).toString(16);
            if (hex.length === 1) {
                return '0' + hex;
            } else if (hex.length === 2) {
                return hex;
            } else {
                console.log('error with hex conversion: ' + dec + ' should be between 0 and 1.');
                return '00';
            }
        }

        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }

        return '#' + decToHex(r) + decToHex(g) + decToHex(b);
    },

    'rgbToHsl': function(r, g, b) {
        r = r / 255;
        g = g / 255;
        b = b / 255;
        var max = Math.max(r, g, b),
            min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max === min) {
            h = s = 0; // achromatic
        } else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }
            h /= 6;
        }
        return [h, s, l];
    },

    'regexEscape': function(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    },

    'queryAlias': function(text) {
        return text.replace(/[-'"+<>{}#%]/g, "");
    },

    'recursiveSearch': function(list, childKey, searchKey, searchValue) {
        var found = false;
        _.each(list[childKey], function(child) {
            if (child[searchKey] === searchValue) {
                found = child;
            } else {
                found = _.recursiveSearch(child, childKey, searchKey, searchValue);
            }
        });
        return found;
    },

    'keysWhere': function(object) {
        var iteratorFunctionsOrValues = Array.prototype.slice.call(arguments, 1);
        var fnOrValue;

        return _.filter(_.keys(object), function(key) {
            for (var j = 0; j < iteratorFunctionsOrValues.length; ++j) {
                fnOrValue = iteratorFunctionsOrValues[j];

                if (typeof(fnOrValue) === 'function') {
                    if (fnOrValue(object[key])) {
                        return true;
                    }
                } else {
                    if (object[key] === fnOrValue) {
                        return true;
                    }
                }
            }
        });
    },

    'whereNotAny': function(list, properties) {
        var newList = [],
            keep;

        if (!list) {
            return [];
        } else if (!properties) {
            return list;
        }

        _.each(list, function(item) {
            keep = true;

            _.each(_.keys(properties), function(property) {
                if (item[property] === properties[property]) {
                    keep = false;
                }
            });

            if (keep) {
                newList.push(item);
            }
        });

        return newList;
    },

    'whereNotAll': function(list, properties) {
        var newList = [];
        var numProperties = _.keys(properties).length;

        if (!list) {
            return [];
        } else if (!properties) {
            return list;
        }

        _.each(list, function(item) {
            var matches = 0;

            _.each(_.keys(properties), function(property) {
                if (item[property] === properties[property]) {
                    ++matches;
                }
            });

            if (matches !== numProperties) {
                newList.push(item);
            }
        });

        return newList;
    },

    'whereHasAll': function(list, properties) {
        var newList = [],
            keep;

        if (!list) {
            return [];
        } else if (!properties) {
            return list;
        }

        _.each(list, function(item) {
            keep = true;

            _.each(_.keys(properties), function(property) {
                if (item[property] === undefined) {
                    keep = false;
                }
            });

            if (keep) {
                newList.push(item);
            }
        });

        return newList;
    },

    'replace': function(list, iteratorFnOrValue, replaceWith) {
        var testFn;

        if (typeof(iteratorFnOrValue) === 'function') {
            testFn = iteratorFnOrValue;
        } else {
            testFn = function(value) {
                return value === iteratorFnOrValue;
            };
        }

        for (var i = 0; i < list.length; i++) {
            if (testFn(list[i])) {
                if (typeof(replaceWith) === 'function') {
                    list[i] = replaceWith(list[i]);
                } else {
                    list[i] = replaceWith;
                }
            }
        }

        return list;
    },

    'htmlEscape': function(str) {
        return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '\'')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    },

    'htmlUnescape': function(str) {
        var e = document.createElement('div');
        e.innerHTML = str;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    },

    'xmlEscapeUnicodeEntities': function(str) {
        return str.replace(/[^\x00-\xef]/g, function(chr) {
            return "&#" + chr.charCodeAt(0) + ";";
        });
    },

    'sum': function(list) {
        var total = 0;

        if (_.every(list, _.isNumber)) {
            _.each(list, function(elem) {
                total += elem;
            });
        }

        return total;
    },

    // Takes an array of arrays (sets), returns the cartesian product of those arrays (sets)
    // [[1,2,3],[a,b,c]] -> [[1,a],[1,b],[1,c],[2,a],...,[3,c]]

    cartesianProduct: function recur(sets) { // naming support: http://stackoverflow.com/questions/12303989/cartesian-product-of-multiple-arrays-in-javascript
        if (sets.length === 0) {
            return [[]];
        } else {
            // recursively generate the cartesian product of a smaller array of sets,
            // which will be the set of "tails" that can be added to each element in the first set

            var tails = recur(sets.slice(1));

            // for each element in the first set, loop over all tails to generate all combinations (loosely defined)

            return _.concatMap(sets[0], function(element) {
                return _.map(tails, function(tail) { // each tail is also a set
                    return [element].concat(tail);
                });
            });
        }
    },

    // Similar to _.memoize, but the cache is an association list
    // (array of [key,value] pairs) instead of an object. This allows
    // us to memoize functions which take object or array references.
    memoizeObject: function(fn, resolver) {
        var cache = [];

        var memoizedFunction = function() {
            var key;

            if (resolver) {
                key = resolver.apply(this, arguments);
            } else {
                key = arguments[0];
            }

            var cached = _.filter(cache, function(row) {
                return row[0] === key;
            })[0];

            if (cached) {
                return cached[1];
            }

            var row = [key, fn.apply(this, arguments)];

            cache.push(row);

            return row[1];
        };

        memoizedFunction.cache = cache;

        return memoizedFunction;
    },

    // is small a subset of big?
    subset: function(big, small) {
        return _.difference(small, big).length === 0;
    },

    namespace: function(ns, globalObject) {
        function getNamespace(ns, globalObject) {
            globalObject = globalObject || this;

            var components = ns.split('.');
            var currentNs = globalObject;

            for (var i = 0; i < components.length; ++i) {
                var component = components[i];

                currentNs[component] = currentNs[component] || {};
                currentNs = currentNs[component];
            }

            return currentNs;
        }

        if (!globalObject) {
            return getNamespace(ns);
        } else {
            return getNamespace(ns, globalObject);
        }
    }

});
