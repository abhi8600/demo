'use strict';

Array.prototype.concatMap = function(fn) {
    return [].concat.apply([], this.map(fn));
};

Function.id = function(x) { return x; };

function makeEditorPopup($, containerFn) {
    var popup, backdrop;

    function closePopup() {
        popup.removeClass('open');
        $(window).off('resize.bqlEditorResize');
        setTimeout(function() {
            drawer.detach();
        }, 600);
    }

    var editor = containerFn(closePopup);
    var drawer = $('<div>').addClass('drawer editor-container').append(editor);

    popup = $("<div>")
        .addClass('drawer-container')
        .append(drawer)
        .appendTo($("body"));

    setTimeout(function() {
        popup.addClass('open');
    }, 1);

    backdrop = $("<div>")
        .addClass('modal-backdrop')
        .click(closePopup)
        .appendTo(popup);
}

window.openBqlEditor = function($, BqlNode, subjectArea, bql, name, expressionIsMeasure, saveBql, checkBql) {
    var expr = (require("./bql/grammar")($, subjectArea, BqlNode)).expression;
    var G = require("./grammar")($);
    var ast = bql ? expr.parse(bql) : expr.initial();
    if(!ast) {
        console.log("Can't parse BQL expression:", bql);
        bql = null;
        ast = expr.initial();
    }

    makeEditorPopup($, function(closePopup) {
        var container = $("<div>").addClass('editor').append($('<div>').append($("<h3>").text('Expression Builder').append($("<span>").text(" (beta)").css({fontSize: 16, color: '#aaa'}))));
        var errorNameReadout = $('<div>').addClass('name-message').text('');
        var inputContainer = $("<div>").addClass("name-container clearfix")
            .append(errorNameReadout)
            .append($('<div>').addClass('title').text('Name: '))
            .appendTo(container);
        var nameInput = $("<input>").addClass("form-control").attr('type', 'text')
            .blur(function() {
                console.log(nameInput.val());
            })
            .appendTo(inputContainer);
        if(name) {
            nameInput.val(name);
        }

        var newAst = null, errorMessage = null, checkId = 1;

        var saveButton = $('<button>').addClass("btn btn-primary pull-right")
			.append($("<i>").addClass("save-icon pull-left"))
            .append($("<span>").text("DONE"))
            .click(function() {
                var newName = nameInput.val();
                closePopup();
                saveBql(newName, newAst, expressionIsMeasure);
            });
        var closeButton = $('<button>').addClass("btn btn-default pull-right")
            .append($("<span>").text("Cancel"))
            .click(closePopup);

        var errorReadout = $("<div>").addClass("pull-right bql-error-message").text("");

        function updateSaveButtonDisabledState() {
            var sanitized = nameInput.val().replace(/[\]\[.]/g, '');
            if (nameInput.val() !== sanitized) { nameInput.val(sanitized); }

            if (nameInput.val() === '') {
                saveButton.attr('disabled', 'disabled');
                errorNameReadout.text('(Name Required)');
            }

            if (errorMessage) {
                saveButton.attr('disabled', 'disabled');
                errorReadout.text(errorMessage);
            }

            if (nameInput.val() !== '' && errorMessage) {
                saveButton.attr('disabled', 'disabled');
                errorReadout.text(errorMessage);
                errorNameReadout.text('');
            }

            if (nameInput.val() !== '' && !errorMessage) {
                saveButton.removeAttr('disabled');
                errorNameReadout.text('');
                errorReadout.text('');
            }
        }
        updateSaveButtonDisabledState();
        nameInput.on('keyup', function() { updateSaveButtonDisabledState(); });

        var editorContainer = $("<div>")
            .addClass('editor-main')
            .append($('<div>').addClass('title').text('Code: '))
            .appendTo(container);

        var category = $('<div>')
            .addClass('expression-type-container')
            .append($('<div>').addClass('title').text('Category: '))
            .append($('<div>').addClass('expression-type')
                .append($('<div>').addClass('radio')
                    .append($('<label>')
                        .append($('<input>')
                            .attr('type', 'radio')
                            .attr('name', 'category-radio')
                            .val('measure')
                            .prop('checked', expressionIsMeasure))
                        .append('<span>Measure</span>')
                    )
                )
                .append($('<div>').addClass('radio')
                    .append($('<label>')
                        .append($('<input>')
                            .attr('type', 'radio')
                            .attr('name', 'category-radio')
                            .val('attribute')
                            .prop('checked', !expressionIsMeasure))
                        .append('<span>Attribute</span>')
                    )
                )
            )
            .appendTo(container);

        category.on('change', function() {
            expressionIsMeasure = category.find(":checked").val() === 'measure';
        });
        var buttonContainer = $('<div>').addClass('button-bar-container').appendTo(container);
        $('<div>').addClass('button-bar clearfix').append(saveButton, closeButton, errorReadout).appendTo(buttonContainer);

        // use setTimeout because the rendering relies on container width
        setTimeout(function() {
            G.editAtPath(editorContainer, ast, function(val) {
                newAst = val;
                var bql = newAst.evaluate().expressionToString();
                var myId = ++checkId;
                checkBql(bql, function(code, message) {
                    // protect against out-of-order ajax call returns
                    if(myId !== checkId) { return; }

                    if(code == 0) {
                        errorMessage = null;
                    } else {
                        console.log("BQL error code:", code);
                        errorMessage = message;
                    }
                    updateSaveButtonDisabledState();
                }, function(err) { console.log(err); });
            }, ast.topEquivalentPath(ast.lastTokenPath()));
        }, 0);

        return container;
    });

};
