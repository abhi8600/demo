module.exports = function($) {

    function Completion(elem, highlight, selected, value, allowSpace) {
        // selected should be a method, called by completion.selected(boolean)
        this.elem = elem;
        this.value = value;
        this.selected = selected;
        this.highlight = highlight;
        this.allowSpace = allowSpace;
    }

    function stringCompletion(completion) {
        var elem = $("<div>").addClass('completion').addClass(completion.className);
        var icon = $('<i>').addClass(completion.type + '-icon');
        if (completion.className === 'operator-completion') {
            icon.addClass(completion.nickname);
        }
        elem.append(icon);
        var wordSpan = $("<span>").text(completion.string).appendTo(elem);
        
        var typeAnnotation = $("<div>");
        typeAnnotation.text(completion.type);
        elem.append(typeAnnotation);
        
        var newCompletion = new Completion(
            elem,
            function(str) {
                if(str.length === 0) {
                    wordSpan.empty().text(completion.string);
                } else {
                    wordSpan.empty();
                    boldMatches(wordSpan, completion.string, str);
                }
            },
            function(isSelected) {
                elem.css('font-weight', isSelected ? 'bold' : 'normal');
                elem.css('background-color', isSelected ? '#ebf8ff' : '#fff');
            },
            completion,
            completion.allowSpace
        );

        elem.on('mouseenter', function () {
            $('.completion').css('font-weight', 'normal').css('background-color', '#fff');
            newCompletion.selected(true);
        });

        return newCompletion;
    }

    function boldMatches(span, full, part) {
        var idx = -1, match;
        var lowerFull = full.toLowerCase(), lowerPart = part.toLowerCase();
        while((match = lowerFull.indexOf(lowerPart, idx)) > -1) {
            span[0].appendChild(document.createTextNode(full.substring(idx, match)));
            span.append($("<b>").text(full.substring(match, match + part.length)));
            idx = match + part.length;
        }
        span[0].appendChild(document.createTextNode(full.substring(idx)));
    }

    return {
        Completion: Completion,
        stringCompletion: stringCompletion
    };
};
