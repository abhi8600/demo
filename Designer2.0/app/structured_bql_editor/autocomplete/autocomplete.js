module.exports = function($) {

    function autocomplete(completionsFor, events, initialString, placeholder) {
        initialString = initialString || "";
        var container = $("<div>");
        var topInput = $("<input>").attr('type', 'text').attr('placeholder', placeholder).val(initialString).appendTo(container);
        setTimeout(function() {
            topInput.focus();
        }, 0);
        var completionsContainer = $("<div>").addClass('completionsContainer').appendTo(container);

        var completions = completionsFor(initialString);

        completionsContainer.on('click', function (ev) {
            ev.stopPropagation();
        });

        $(document).on('click', function (ev) {
            completionsContainer.hide();
        });

        topInput.on('click', function (ev) {
            ev.stopPropagation();
        });

        topInput.on('focus', function() {
            completionsContainer.show();
        });

        topInput.on('keydown', function(ev) {
            var key = ev.which;
            if (key === 27) events.escape();
            if (key === 8) return backspace();
            if (key === 40) {
                selectDown();
                return false;
            }
            if (key === 38) {
                selectUp();
                return false;
            }
            if (key === 32) {
                var completionsAfterSpace = completionsFor(topInput.val() + " ");
                if (completions.length === 1 && completionsAfterSpace.length === 0) {
                    enter();
                }
            }
            if(key === 39 && topInput[0].selectionStart == topInput.val().length && topInput[0].selectionEnd == topInput.val().length) { 
                events.onRight();
                return false;
            }
            if(key === 37 && topInput[0].selectionStart == 0 && topInput[0].selectionEnd == 0) {
                events.onLeft();
                return false;
            }
            if (key === 13) enter();
            if (key === 93) console.log('getPosition() =', getPosition());
        });

        topInput.on('keypress', function(ev) {
            // Let the keypress be handled by the browser, then update based on new text
            setTimeout(updateText, 0);
        });

        updateText();

        return container;

        function getPosition() {
            // This function replaces the previous `position` variable, and it is needed
            // to unify both mouse and keyboard interactions with completions.
            var pos = -1;
            while (++pos < completions.length) {
                if (completions[pos].elem.css('background-color') !== 'rgb(255, 255, 255)') { return pos; }
            }
            return -1; // No highlights found
        }

        function updateText() {
            completionsContainer.empty();
            completions = completionsFor(topInput.val());
            if (completions.length === 0) {
                completionsContainer.append($("<span>").text("No completions"));
            } else {
                completions.map(function(completion) {
                    completion.highlight(topInput.val());
                    completion.elem.on('mousedown', function() {
                        events.finish(completion.value);
                    }).appendTo(completionsContainer);
                });
            }
        }

        function scrollToPosition() {
            var currPosition = getPosition();
            var containerHeight = completionsContainer.height();
            var elemOffset = completions[currPosition].elem.position().top;
            var elemHeight = completions[currPosition].elem.height();
            var diff = 0;
            if (elemOffset < 0) {
                diff = elemOffset;
            }
            if (elemOffset + elemHeight > containerHeight) {
                diff = 20 + elemOffset + elemHeight - containerHeight;
            }
            completionsContainer[0].scrollTop += diff;
        }

        function selectDown() {
            var currPosition = getPosition();
            if (currPosition === completions.length - 1) return;
            if (currPosition !== -1) completions[currPosition].selected(false);
            completions[currPosition+1].selected(true);
            scrollToPosition();
        }

        function selectUp() {
            var currPosition = getPosition();
            if (currPosition === -1) return;
            completions[currPosition].selected(false);
            if (currPosition === 0) return;
            completions[currPosition-1].selected(true);
            scrollToPosition();
        }

        function enter() {
            var currPosition = getPosition();
            if (completions.length === 0) return;
            if (completions.length === 1) return events.finish(completions[0].value);
            if (currPosition > -1) return events.finish(completions[currPosition].value);;
            return events.finish(completions[0].value);
        }

        function backspace() {
            if (topInput.val() === "") {
                if(events.back) {
                    events.back();
                }
                return false;
            }

            // Let the backspace be handled by the browser, then update based on new text
            setTimeout(updateText, 0);
            return true;
        }

    }


    function simpleCompletion(val) {
        return {
            elem: $("<div>").css({
                width: '100%',
                lineHeight: '30px'
            }).text(val),
            selected: function(isSelected) {
                this.elem.css('font-weight', isSelected ? 'bold' : 'normal');
            },
            value: val
        };
    }


    function testCompletionsFor(str) {
        var vals = ["foo", "bar", "baz"];
        var ret = [];
        vals.map(function(val) {
            if (val.indexOf(str) > -1) {
                ret.push(simpleCompletion(val));
            }
        });
        return ret;
    }


    return autocomplete;
}
