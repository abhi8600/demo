module.exports = function($, subjectArea, BqlNode) {

    var G = require("../grammar")($);


    var bql = {
        // a full query, like "SELECT [Time.Year] FROM [all]"
        query: new G.NonTerminal(),

        // comes after FROM, typically [all]
        fromClause: new G.NonTerminal(),

        // WHERE expression
        whereClause: new G.NonTerminal(),
        
        // DISPLAY WHERE expression
        displayWhereClause: new G.NonTerminal(),

        // the list of columns in the SELECT clause of a query
        projectionList: new G.NonTerminal(),
        
        // Any BQL expression: a number, a column, a function application, two subexpressions combined with an operator, etc.
        expression: new G.NonTerminal(),
        
        // An expression with possible suffixes (like IS NULL) but no operators
        suffixedExpression: new G.NonTerminal(),

        // An expression without operators or suffixes (or a full-fledged expression in parens)
        simpleExpression: new G.NonTerminal(),
        
        // "(" expression ")"
        groupedExpression: new G.NonTerminal(),
        
        // Built in Bql Function, e.g. IIF, IsNaN, etc.
        bqlFunction: new G.NonTerminal(),

        // Infix operators: +, -, /, OR, AND, etc.
        operator: new G.NonTerminal(),
        
        // Year, Month, Day (used in some functions like datediff)
        datepart: new G.NonTerminal(),

        // Mean, Median, StdDev, etc
        statType: new G.NonTerminal(),

        // Year, Month, Day, Hour, Minute, Second (used in some functions like datediff)
        datetimepart: new G.NonTerminal(),
        
        // A measure from the Subject Area, like Revenue.
        measure: new G.NonTerminal(),
        
        // an attribute from the Subject Area, like [Time.Year]
        attribute: new G.NonTerminal(),
        
        // e.g. 100
        'int': new G.NonTerminal(),

        // e.g. 121.1, or 100.0 (BQL distinguishes floats from ints, unlike JS)
        'float': new G.NonTerminal(),

        // e.g. 'BQL is great!'
        string: new G.NonTerminal(),

        // e.g. #2010-01-01#, or NOW
        date: new G.NonTerminal(),
        
        // e.g. %0, %1
        parameter: new G.NonTerminal()
    };

    require("./query")(bql, $, subjectArea, BqlNode, G);
    require("./expression")(bql, $, subjectArea, BqlNode, G);
    require("./suffixedExpression")(bql, $, subjectArea, BqlNode, G);
    require("./simpleExpression")(bql, $, subjectArea, BqlNode, G);
    require("./groupedExpression")(bql, $, subjectArea, BqlNode, G);
    require("./bqlFunction")(bql, $, subjectArea, BqlNode, G);
    require("./datePart")(bql, $, subjectArea, BqlNode, G);
    require("./column")(bql, $, subjectArea, BqlNode, G);
    require("./number")(bql, $, subjectArea, BqlNode, G);
    require("./string")(bql, $, subjectArea, BqlNode, G);
    require("./date")(bql, $, subjectArea, BqlNode, G);
    require("./statType")(bql, $, subjectArea, BqlNode, G);
    return bql;
};
