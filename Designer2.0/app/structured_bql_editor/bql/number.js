module.exports = function(bql, $, subjectArea, BqlNode, G) {

    var intConstant = G.stringToken(
        function(str) {
            var n = parseInt(str, 10);
            if (isNaN(n)) return null;
            if(str.indexOf(".") !== -1) return null;
            return {
                type: 'int',
                className: 'number-completion',
                string: str
            };
        }, "0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            while(idx < str.length && str.charAt(idx).match(/[-0-9]/)) idx++;
            var candidate = str.substring(start, idx);
            if(isNaN(parseInt(candidate, 10))) return null;

            return {value: {type: 'int', className: 'number-completion', string: candidate},
                    index: idx};
        }
    );

    var floatConstant = G.stringToken(
        function(str) {
            var n = parseInt(str, 10);
            if (isNaN(n)) return null;
            if(str.indexOf(".") === -1) return null;
            return {
                type: 'float',
                className: 'number-completion',
                string: str
            };
        }, "0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            while(idx < str.length && str.charAt(idx).match(/[-0-9.]/)) idx++;
            var candidate = str.substring(start, idx);
            if(candidate.indexOf(".") === -1) return null;
            if(isNaN(parseInt(candidate, 10))) return null;

            return {value: {type: 'float', className: 'number-completion', string: candidate},
                    index: idx};
        }
    );

    var paramConstant = G.stringToken(
        function(str) {
            if(str.charAt(0) !== "%") return null;
            var n;
            if(str.length === 1) {
                n = 0;
            } else {
                n = parseInt(str.substring(1), 10);
                if (isNaN(n)) return null;
                if(n < 0 || n !== Math.floor(n)) return null;
            }
            return {
                type: 'parameter',
                className: 'parameter-completion',
                string: str
            };
        }, "%0", 1,
        function(str, idx) {
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "%") return null;
            idx++;

            while(idx < str.length && str.charAt(idx).match(/[0-9]/)) idx++;
            var candidate = str.substring(start+1, idx);
            var n = parseInt(candidate, 10);
            if (isNaN(n)) return null;
            if(n < 0 || n !== Math.floor(n)) return null;

            return {value: {type: 'parameter', className: 'parameter-completion', string: candidate},
                    index: idx};
        }
    );

    bql['int'].initialize([new G.Production([intConstant], function(ic) {
            return BqlNode.fromInt(parseInt(ic.string, 10));
    })], true);

    bql['float'].initialize([new G.Production([floatConstant], function(fc) {
            return BqlNode.fromFloat(parseInt(fc.string, 10));
    })], true);

    bql.parameter.initialize([new G.Production([paramConstant], function(pc) {
            return BqlNode.fromParameter(pc.string);
    })], true);

}
