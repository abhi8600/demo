module.exports = function(bql, $, subjectArea, BqlNode, G) {
    
    bql.query.initialize([
        new G.Production([G.string("SELECT"), bql.projectionList, G.string("FROM"), bql.fromClause, G.optional(bql.whereClause), G.optional(bql.displayWhereClause)], 
                       function(_, projlist, _, fromClause, whereClause, displayWhereClause) {
                           return BqlNode.fromQuery(projlist, fromClause, BqlNode.fromWhereClause(whereClause), BqlNode.fromDisplayWhereClause(displayWhereClause));
                       })
        ], true);
    
    // For now fromClause is just [all]
    bql.fromClause.initialize([
        new G.Production([G.string("[all]")], function(_) {
            return BqlNode.fromFromClause("[all]");
        })
    ], true);

    bql.whereClause.initialize([
        new G.Production([G.string("WHERE"), bql.expression], function(_, exp) {
            return exp;
        })
    ], true);

    bql.displayWhereClause.initialize([
        new G.Production([G.string("DISPLAY WHERE"), bql.expression], function(_, exp) {
            return exp;
        })
    ], true);
        

    var projList = G.separatedList(bql.expression, G.string(","), false, function(exps, commas) { return BqlNode.fromProjectionList(exps); });

    bql.projectionList.initialize([
        new G.Production([projList], Function.id)
    ], true);
        
}

