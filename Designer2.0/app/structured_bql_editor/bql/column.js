module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var measures = subjectArea.measures;
    var attributes = subjectArea.attributes;

    function parseMeasureOrAttribute(str, idx) {
        var nextBra = str.indexOf("[", idx);
        if(nextBra == -1) return null;
        if(!str.substring(idx, nextBra).match(/^\s*$/)) return null;

        var nextKet = str.indexOf("]", nextBra);
        if(nextKet == -1) return null;

        var attribOrMeasureString = str.substring(nextBra+1, nextKet);
        return {string: attribOrMeasureString, index: nextKet+1};
    }

    var measure = G.listToken(

        measures.filter(function(measure) {
            return measure.type === 'measure';
        }).map(function(measure) {
            return {
                string: measure.label,
                type: 'measure',
                className: 'measure-completion',
                measure: measure,
                dateType: BqlNode.defaultDateType(measure),
                aggregation: BqlNode.defaultAggregation(measure),
                timeSeries: null
            };
        }), 4,
        function(str, idx) {
            var val = parseMeasureOrAttribute(str, idx);
            if(!val) return null;
            var measureParts = subjectArea.bqlMeasureMap[ val.string ];
            if(!measureParts) return null;

            var token = {
                string: measureParts.measure.label,
                type: 'measure',
                className: 'measure-completion',
                measure: measureParts.measure,
                dateType: measureParts.dateType,
                aggregation: measureParts.aggregation,
                timeSeries: measureParts.timeSeries
            };

            return {
                index: val.index,
                value: token
            };
        }
    );
    measure.customRenderer = function(node) {
        var span = $("<span>").css('position', 'relative').text(node.value.string);
        var popup;

        var aggregationsGroupContainer = $("<div>");
        if (node.value.measure.aggregations.length > 1) {
            aggregationsGroupContainer.addClass('custom-renderer').append($("<div>").addClass('title').text("Aggregation"));
            var aggregationsGroup = $('<div>').appendTo(aggregationsGroupContainer);
            node.value.measure.aggregations.map(function(agg) {
                aggregationsGroup.append($("<label>").append(
                    $("<input type=radio name=aggregationsRadio>").val(agg).attr('checked', agg === node.value.aggregation).on('change', function() {
                        node.value.aggregation = agg;
                    }),
                    $("<span>").text(agg)
                ));
            });
        }
        var dateTypeGroupContainer = $("<div>");
        var dateTypesWithoutLoadDate = node.value.measure.dateTypes.filter(function(type) {
            return type !== "By Load Date";
        });
        if (dateTypesWithoutLoadDate.length > 1) {
            dateTypeGroupContainer.addClass('custom-renderer').append($("<div>").addClass('title').text("Analyze By Date"));
            var dateTypeGroup = $('<div>').appendTo(dateTypeGroupContainer);
            dateTypesWithoutLoadDate.map(function(dt) {
                dateTypeGroup.append($("<label>").append(
                    $("<input type=radio name=dateTypeRadio>").val(dt).attr('checked', dt === node.value.dateType).on('change', function() {
                        node.value.dateType = dt;
                    }),
                    $("<span>").text(dt)
                ));
            });
        }

        var buttons = $("<div class='finish-container'>").append($('<div class="title">'), $("<button class='btn btn-primary'>").text('DONE').click(function() {
            popup.hide();
        }));

        var grammarWidth = function() {
            var width = 90;

            if (!aggregationsGroupContainer.is(':empty')) {
                width = width + 200;
            }
            if (!dateTypeGroupContainer.is(':empty')) {
                width = width + 200;
            }
            return width;
        };

        popup = $("<div>").addClass('grammar-popup')
            .append(
                aggregationsGroupContainer,
                dateTypeGroupContainer,
                buttons
            )
            .css('width', grammarWidth)
            .appendTo(span)
            .hide()
            .on('click', function(ev) {
                ev.stopPropagation();
            });

        span.append($("<span>").text(" "), $("<i>").addClass("down-arrow-icon").css({
            'cursor': 'pointer',
            margin: '0 0 0 4px',
            backgroundSize: 10
        }).click(function() {
            popup.show();
            return false;
        }));

        return span;
    };

    var attribute = G.listToken(
        attributes.map(function(attribute) {
            return {
                string: attribute.label,
                type: 'attribute',
                className: 'attribute-completion',
                attribute: attribute
            };
        }), 3,
        function(str, idx) {
            var val = parseMeasureOrAttribute(str, idx);
            if(!val) return null;
            
            var attribute = _.filter(subjectArea.attributes, function(attribute) {
                return _.last(attribute.path) === val.string;
            })[0];
            if(!attribute) return null;

            var token = {
                string: attribute.label,
                type: 'attribute',
                className: 'attribute-completion',
                attribute: attribute
            };
            return {index: val.index, value: token};
        }
    );

    bql.measure.initialize([new G.Production([measure], function(m) {
        return BqlNode.fromMeasure(m.measure, m.aggregation, m.dateType, m.timeSeries);
    })], true);

    bql.attribute.initialize([new G.Production([attribute], function(a) {
        return BqlNode.fromAttribute(a.attribute);
    })], true);
}
