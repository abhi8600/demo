module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var expressionList = G.separatedList(
        bql.suffixedExpression,
        bql.operator,
        false,
        function(vals, ops) {
            var combined = [vals.shift()];
            while (ops.length) {
                combined.push(ops.shift());
                combined.push(vals.shift());
            }
            return toNode(combined);

            function toNode(lst) {
                if (lst.length === 1) {
                    return lst[0];
                } else {
                    var nextOp = 1;
                    for (var ii = 3; ii < lst.length; ii += 2) {
                        if (lst[ii].precedence <= lst[nextOp].precedence) {
                            nextOp = ii;
                        }
                    }
                    return new BqlNode({
                        type: "operator",
                        value: lst[nextOp].value
                    }, [toNode(lst.slice(0, nextOp)), toNode(lst.slice(nextOp + 1))]);
                }
            }

        }
    );

    bql.expression.initialize([new G.Production([expressionList], Function.id)], true);
}
