module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var optionalSuffix = new G.NonTerminal([
        new G.Production([], function() {
            return undefined;
        }),
        new G.Production([G.listToken([{
            type: "suffix",
            className: 'suffix-completion',
            string: "IS NULL"
        }, {
            type: "suffix",
            className: 'suffix-completion',
            string: "IS NOT NULL"
        }], 2)], function(it) {
            return {
                type: "suffix",
                value: it.string
            };
        })
    ], true);

    bql.suffixedExpression.initialize([
        new G.Production([bql.simpleExpression, optionalSuffix], function(simple, suffix) {
            if (suffix) {
                return new BqlNode(suffix, [simple]);
            } else {
                return simple;
            }
        }),
    ], true);

}
