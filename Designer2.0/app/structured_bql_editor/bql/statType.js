module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var statToken = G.listToken(["Median", "Min", "Max", "Avg", "Sum", "Count", "CountDistinct", "StdDev"].map(function(part) {
        return {
            type: "statType",
            className: 'statType-completion',
            string: part,
            value: part
        };
    }), 1);

    bql.statType.initialize([
        new G.Production([statToken], function(it) {
            return new BqlNode({
                type: "statType",
                value: it.value
            }, []);
        })
    ], true);

}
