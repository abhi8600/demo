// An AST is composed of Nodes and Leafs. A Node corresponds to a
// particular production of a nonterminal, while a leaf corresponds to
// a terminal.
//
// for example, if the grammar is A -> aA; A -> b then aab will correspond to a tree like this:
//
//        Node: A -> aA
//          /      \
//    Leaf: a     Node: A -> aA
//                /          \
//             Leaf: a      Node: A -> b
//                             \
//                           Leaf: b


function Node(nonterminal, production, children) {
    this.nonterminal = nonterminal;
    this.production = production;
    this.children = children;
}

function Leaf(terminal, value, initialFlag) {
    this.terminal = terminal;
    this.value = value;
    this.initialFlag = initialFlag;
}


// use a list of numbers to navigate through the tree's children,
// returning the Leaf or Node at those coordinates.
Node.prototype.getChild = function(path) {
    if (path.length === 0) return this;
    if (path.length === 1) return this.children[path[0]];
    return this.children[path[0]].getChild(path.slice(1));
};

// Replace the node at <path> with the node <value>. Return the result.
//
// Note: you should always use this like
//
// ast = ast.setChild(path, value),
//
// because when path is [] then the old value of ast will be
// unchanged.
Node.prototype.setChild = function(path, value) {
    if (path.length === 0) {
        return value;
    } else if (path.length === 1) {
        this.children[path[0]] = value;
        return this;
    } else {
        this.children[path[0]].setChild(path.slice(1), value);
        return this;
    }
};

Node.prototype.initial = function() {
    return this.nonterminal.initial();
}

Leaf.prototype.initial = function() {
    return this.terminal.initial();
}

// Check if the node is still in its initial state
Node.prototype.isInitial = function() {
    return this.nonterminal.productions.indexOf(this.production) === 0 && this.children.every(function(child) {
        return child.isInitial();
    });
};

Leaf.prototype.isInitial = function() {
    return this.value === this.terminal.defaultValue;
};

Node.prototype.isEmpty = function() {
    return this.production.length === 0 || this.children.every(function(child) {
        return child.isEmpty();
    });
};

Leaf.prototype.isEmpty = function() {
    return false;
};

// move backward and then up the tree from the given path, returning a
// list of all nodes visited
Node.prototype.nodesBefore = function(path) {
    var list = [];
    go(this, []);

    function go(node, nodePath) {
        if (nodePath.length === path.length) {
            return;
        } else {
            var next = path[nodePath.length];
            go(node.children[next], nodePath.concat([next]), false);
            for (var ii = next - 1; ii >= 0; ii--) {
                list.push({
                    node: node.children[ii],
                    path: nodePath.concat([ii])
                });
            }
        }
    }
    return list;
};

Node.prototype.firstPath = function() {
    return [];
}

// move forward and then up the tree from the given path, returning a
// list of all nodes visited
Node.prototype.nodesAfter = function(path) {
    var list = [];
    go(this, []);

    function go(node, nodePath) {
        if (nodePath.length === path.length) {
            return;
        } else {
            var next = path[nodePath.length];
            go(node.children[next], nodePath.concat([next]), false);
            for (var ii = next + 1; ii < node.children.length; ii++) {
                list.push({
                    node: node.children[ii],
                    path: nodePath.concat([ii])
                });
            }
        }
    }
    return list;
};

Node.prototype.lastPath = function() {
    return [this.children.length-1]
}

Node.prototype.lastTokenPath = function() {
    for(var ii = this.children.length-1; ii >= 0; ii--) {
        if(this.children[ii] instanceof Leaf) {
            return [ii];
        } 
        var ch = this.children[ii].lastTokenPath();
        if(ch.length) return [ii].concat(ch);
    }
    return [];
}

Node.prototype.nextPath = function(path) {
    var nextPaths = this.nodesAfter(path);
    if(nextPaths.length === 0) {
        return this.lastPath();
    } else {
        return nextPaths[0].path;
    }
}

Node.prototype.prevPath = function(path) {
    var prevPaths = this.nodesBefore(path);
    if(prevPaths.length === 0) {
        return this.firstPath();
    } else {
        var underNodes = this.nodesUnder(prevPaths[0].path);
        return underNodes[ underNodes.length-1 ].path;
    }
}

Node.prototype.prevTokenPath = function(path) {
    var self = this;
    var prevPaths = this.nodesBefore(path);
    prevPaths = prevPaths.concatMap(function(nodePath) { return self.nodesUnder(nodePath.path).reverse(); });
    prevPaths = prevPaths.filter(function(nodePath) { return nodePath.node instanceof Leaf; });
    if(prevPaths.length === 0) {
        return this.firstPath();
    } else {
        return prevPaths[ 0 ].path;
    }
}

Node.prototype.nextTokenPath = function(path) {
    var self = this;
    var nextPaths = this.nodesAfter(path);
    nextPaths = nextPaths.concatMap(function(nodePath) { return self.nodesUnder(nodePath.path); });
    nextPaths = nextPaths.filter(function(nodePath) { return nodePath.node instanceof Leaf; });
    if(nextPaths.length === 0) {
        return this.lastTokenPath();
    } else {
        return nextPaths[ 0 ].path;
    }
}

Node.prototype.backspacePath = function(path) {
    var ret = this.topEquivalentPath(this.prevTokenPath(this.topEquivalentPath(path)));
    while(ret.length && ret[ret.length-1] === 0) {
        ret.pop();
    }
    return this.topEquivalentPath(ret);
}

// Return the next node after path, if there is a node after path and
// if it is in its initial state
Node.prototype.nextInitialPath = function(path) {
    var nextPaths = this.nodesAfter(path);
    if (!nextPaths.length) return null;
    if (!nextPaths[0].node.isInitial()) return null;

    return nextPaths[0].path;
};

// List of all nodes and leaves under the given one, in preorder traversal
Node.prototype.nodesUnder = function(path) {
    var list = [];
    go(this.getChild(path), path);

    function go(node, nodePath) {
        list.push({
            node: node,
            path: nodePath
        });
        if (!(node instanceof Leaf)) {
            node.children.map(function(child, ii) {
                go(child, nodePath.concat([ii]));
            });
        }
    }
    return list;
};

Node.prototype.evaluate = function() {
    return this.production.evaluator.apply({},
        this.children.map(function(child) {
            return child.evaluate();
        })
    );
};

Leaf.prototype.evaluate = function() {
    return this.value;
};

// Find the topmost (closest to tree root) path which contains the
// same leaf nodes as the given path.
Node.prototype.topEquivalentPath = function(path) {
    var best = null;
    for (var ii = 0; ii < path.length; ii++) {
        best = best || path.slice(0, ii);
        var nextNode = this.getChild(best);
        var nextComponent = path[ii];
        var containsOtherLeafs = !nextNode.children.every(function(child, jj) {
            return (jj === nextComponent) || child.isEmpty();
        });
        if (containsOtherLeafs) {
            best = null;
        }
    }

    if (best) return best;
    return path;
};

// The set of tokens that may be the next ones after path, taking into
// account any following empty nodes
Node.prototype.followingTokens = function(path) {
    var child = this.getChild(path);
    if (child instanceof Leaf) {
        return [{
            topPath: path,
            relPath: [],
            token: child.terminal,
            zipper: function(leaf) {
                return leaf;
            }
        }];
    }
    var tokens = child.nonterminal.firstTokens(path);
    if (child.isEmpty()) {
        var nodesAfter = this.nodesAfter(path);
        for (var ii = 0; ii < nodesAfter.length; ii++) {
            var node = nodesAfter[ii].node;
            var nodePath = nodesAfter[ii].path;
            if (!node.isInitial()) break;

            if (node instanceof Leaf) {
                tokens.push({
                    topPath: nodePath,
                    relPath: [],
                    token: node.terminal,
                    zipper: function(leaf) {
                        return leaf;
                    }
                });
            } else {
                tokens = tokens.concat(node.nonterminal.firstTokens(nodePath));
            }

            if (!node.isEmpty()) break;
        }
    }
    return tokens;
};



// Utility functions - compare paths within an AST

function lessPath(left, right) {
    var len = Math.min(left.length, right.length);
    for (var ii = 0; ii < len; ii++) {
        if (left[ii] > right[ii]) return false;
        if (left[ii] < right[ii]) return true;
    }
    return right.length > len;
}

function equalPath(left, right) {
    return !lessPath(left, right) && !lessPath(right, left);
}

module.exports = {
    Leaf: Leaf,
    Node: Node,
    equalPath: equalPath,
    lessPath: lessPath
};
