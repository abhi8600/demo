
### Templates/Partials

With `gulp watch` running, any changes to a partial will generate a new `app/js/templates-app.js` (as well as an uglified version in `config.dest`).