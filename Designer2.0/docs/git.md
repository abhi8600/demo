
# Git

### Setup

Speak with **Jerome Fong** to make sure you have an account on [Birst GitLab](https://git.birst.com/), and that you have access to the `eng/Birst` project.

Install git locally (you may need to install XCode from the AppStore). You should then be able to `git clone git@git.birst.com:eng/birst.git` to get a local copy of the repository.

### Workflow

All work is committed to the `dev` branch, so do a `git checkout dev` to make sure you are working on the right branch initially. The usual `master` branch is completely empty, and currently unused.

You can follow whatever workflow is best for you - whether you use local feature branches, stashes, a Git GUI, etc. Whenever you pull, you are encouraged to do a rebase (`git pull --rebase`) such that your own commits will be replayed on top of the most recent commits to dev. This will eliminate almost all "merge commits".

A common workflow is as follows:

```bash
git pull --rebase
...make changes...
git add .
git commit -m "Awesome commit message"
git pull --rebase  # Any potential merge conflicts are resolved here
git push
```

### Commit Messages

A lot of work you do will be related to a specific [JIRA](Designer2.0/docs/jira_sprints.md) ticket. When you make a commit, prepend the ticket to a meaningful commit message. For example: `BPD-25473: Viz BQL editor, fix clicking scrollbar in IE`.