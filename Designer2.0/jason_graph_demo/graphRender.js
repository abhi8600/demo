var $ = require("./jquery.js")
var Raphael = require("raphael-browserify");

var processSubjectArea = require("./subjectArea");
var assert = require("assert");
var bqlEditor = require("./bql/editor");
var bqlUtil = require("./bql/util");

module.exports = renderGraph;

function renderGraph(series, dimMappings, left, top, width, height, paper) {
    assert(series.length > 0);

    console.log(series);

    var xr = xRange(series, width);

    series.map(function(serie) {
        var seriesType = serie.type;
        switch(seriesType) {
        case "column": return renderColumn(serie.groups, left, top, width, height, paper);
        case "line": return renderLine(serie.points, serie.attrs, xr, left, top, width, height, paper);
        case "scatter": return renderScatter(serie.points, serie.attrs, xr, left, top, width, height, paper);
        case "area": return renderArea(serie.points, serie.attrs, xr, left, top, width, height, paper);
        }
        assert(false);
    });
}

function xRange(series, width) {
    var columns = series.filter(function(serie) { return serie.type == 'column'; });
    if(columns.length) {
        assert(columns.length == 1);
        return distributeColumns(columns[0].groups, width).xRange;
    } else {
        return {min: 0, max: 1};
    }
}

function distributeColumns(groups, width) {
    assert(groups.length > 0);
    // group count
    var gc = groups.length;
    // column count
    var cc = groups[0].stacks.length;
    // spacing between columns
    var sp = 2;
    var columnWidth = (width - sp*(cc-1)*gc) / (gc*cc + gc - 1);
    var groupWidth = columnWidth*cc + sp*(cc - 1);

    var xr = {min: Math.round(groupWidth/(2*width)), max: 1 - Math.round(groupWidth/(2*width))};
    
    return {columnWidth: columnWidth, groupWidth: groupWidth, xRange: xr};
}

function adjustX(xr, x) {
    return x*(xr.max-xr.min) + xr.min;
}

function renderScatter(points, attrs, xr, left, top, width, height, paper) {
    points.map(function(point, ii) {
        if(point.rows.length > 0) {
            var row = point.rows[0];
            var x = left + width*adjustX(xr, row.x) , y = top + (1-row.y)*height;
            paper.circle(x,y,30*row.size).attr('fill', row.color);
        }
    });
}

function renderArea(points, attrs, xr, left, top, width, height, paper) {
    var path = pointsPath(points, xr, left, top, width, height);
    var br = "L" + (left + width*xr.max) + "," + (top + height);
    var bl = "L" + (left + width*xr.min) + "," + (top + height);
    
    paper.path(path + br + bl + "Z").attr('fill', points[0].rows[0].color).attr('opacity', 0.8);
}

function renderLine(points, attrs, xr, left, top, width, height, paper) {
    if(!points.length) return;
    var path = pointsPath(points, xr, left, top, width, height);
    paper.path(path).attr('stroke', attrs.color).attr('stroke-width', 1 + 10*attrs.size);
}

function pointsPath(points, xr, left, top, width, height) {
    var started = false;
    return mapPairs(points, function(point1, point2, ii) {
        if(point1.rows.length == 0 || point2.rows.length == 0) return "";
        var row1 = point1.rows[0];
        var row2 = point2.rows[0];
        var y1 = Math.floor(top + height*(1 - row1.y));
        var y2 = Math.floor(top + height*(1 - row2.y));
        var x1 = adjustX(xr, row1.x);
        var x2 = adjustX(xr, row2.x);
        if(!started) {
            started = true;
            return "M" + (left + width*x1) + "," + y1 + "L" + (left + width*x2) + "," + y2;
        } else {
            return "L" + (left + width*x2) + "," + y2;
        }
    }).join("");
}

function mapPairs(arr, fn) {
    var ret = [];
    for(var ii = 1; ii < arr.length; ii++) {
        ret.push(fn(arr[ii-1], arr[ii], ii-1));
    }
    return ret;
}

function renderColumn(groups, left, top, width, height, paper) {
    
    var distributed = distributeColumns(groups, width);
    var columnWidth = distributed.columnWidth;
    var groupWidth = distributed.groupWidth;
    var sp = 2;
    
    groups.map(function(group, ii) {
        // x value at the left side of the column group
        var gx = (groupWidth + columnWidth)*ii;
        group.stacks.map(function(stack, jj) {
            var x = Math.round(gx + (columnWidth+sp)*jj);
            var y = height;
            var w = columnWidth;
            console.log(stack);
            stack.pieces.slice(0).reverse().map(function(piece) {
                if(piece.rows.length) {
                    var row = piece.rows[0];
                    y -= row.y*height;
                    paper.rect(left + x, top + y, w, row.y*height).attr('fill', row.color).attr('stroke', row.color);
                }
            });
        });
    });

}
