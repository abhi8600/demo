var http = require("http");
var browserify = require("browserify");
var CWS = require("./CWS.js");
var qs = require("querystring");

process.on('uncaughtException', function (err) {
  console.log(err);
})

http.createServer(function(req, resp) {
    console.log(req.url);
    if(req.url === '/') {
        resp.writeHead(200, {'Content-Type': 'text/html'});
        resp.write("<!doctype html>");
        resp.write("<html><body><script type=\"text/javascript\">");
        var bundle = browserify().add("./browserMain.js").bundle();
        bundle.on('data', function(piece) {
            resp.write(piece);
        });
        bundle.on('end', function() {
            resp.write("</script>\n");
            resp.end();
        });
    } else if(req.url === '/subjectAreas'){
        CWS.subjectAreas(function(sas) {
            resp.writeHead(200, {'Content-Type': 'text/json'});
            resp.end(JSON.stringify(sas));
        });
    } else if(req.url === '/query') {
        var postData = "";
        req.on('data', function(piece) { postData += piece; });
        req.on('end', function() { 
            console.log(postData, qs.parse(postData));
            CWS.query(qs.parse(postData).q, function(res) {
                resp.writeHead(200, {'Content-Type': 'text/json'});
                resp.end(JSON.stringify(res));
            });
        });
    } else {
        resp.writeHead(404, {'Content-Type': 'text/plain'});
        resp.end("No content\n");
    }
}).listen(7500);
