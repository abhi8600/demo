
module.exports = {
    blank: blank,
    node: node,
    isAttribute: isAttribute,
    bqlString: bqlString,
    isValid: isValid,
    colName: colName
}

function isValid(node) {
    return node
        && !isBlank(node) 
        && node.children.filter(function(child) { return !isValid(child); }).length == 0;
}

function isAttribute(node) {
    return node.head[0] === 'attribute';
}

function isBlank(node) {
    return node.head[0] === 'function' && node.head[1].label === 'blank';
}

function blank(type) { 
    if(type === undefined) {
        type = ["bool", "string", "int", "float", "date", "datetime"];
    }
    return node(["function", {label: 'blank', type: type, args: []}], []); 
}

function node(head, children) {
    return {head: head, children: children};
}

function colName(expression) {
    switch(expression.head[0]) {
    case "measure":
        return expression.head[1].measureType.label;
    case "attribute":
        return expression.head[1].label;
    }
    return "(expression)";
}

function bqlString(expression) {
    switch(expression.head[0]) {
    case "measure":
        var measure = expression.head[1]; // head[1] is a column, which in this case happens to be a measure
        
        // what is a measureType.path?
        return "[" + measure.measureType.bqlString(measure.measureType.path[ measure.measureType.path.length - 1 ],
                                            measure.aggregation, measure.dateType, measure.timeSeriesType) + "]";
    case "attribute":
        return "[" + expression.head[1].path[ expression.head[1].path.length - 1 ] + "]";
    case "function":
        var substrings = expression.children.map(function(child) { return bqlString(child); });
        if(expression.head[1].bqlString) {
            return expression.head[1].bqlString.apply(this, substrings);
        } else {
            return expression.head[1].label + "(" + substrings.join(",") + ")";
        }
    case "string":
        return "'" + expression.head[1].replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'";
    case "number":
        return "" + expression.head[1];
    default:
        return "{Unhandled Expression Type}";
    }
}
