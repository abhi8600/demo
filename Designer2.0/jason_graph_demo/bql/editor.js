
var autocomplete = require("./autocomplete");
var P = require("./pretty_printer");
var bqlFunctions = require("./functions");
var $ = require("../jquery.js");
var util = require("./util");

module.exports = function(bql, subjectArea, onChange) {

    var completions = subjectArea.measures.map(function(measure) { 
        return {
            label: measure.path[ measure.path.length - 1 ],
            doc: 'Measure from subject area',
            group: 'Measures',
            value: ["measure", measure]
        }
    }).concat( subjectArea.attributes.map(function(attrib) { 
        return {
            label: attrib.label,
            doc: 'Attribute from subject area',
            group: 'Attributes',
            value: ["attribute", attrib] 
        }
    })).concat( bqlFunctions.map(function(fn) { 
        return {
            label: fn.label,
            doc: fn.doc,
            group: fn.group,
            value: ["function", fn] 
        }
    }));

    return bqlEditor(bql, subjectArea, onChange)

    function copyArray(arr) { return arr.map(function(x){ return x; }); }
    function copyObject(obj) { var ret = {}; Object.keys(obj).map(function(key) { ret[key] = obj[key]; }); return ret; }


    function set(tree, coords, val) {
        if(coords.length === 0) {
            tree.head = copyArray(val.head);
            tree.children = copyArray(val.children);
        } else {
            set(tree.children[ coords[0] ], coords.slice(1), val);
        }
    }
    
    function renderExpression(tree, editCoords) {
        return go(tree, []);
        function go(t, coords) {
            var subRenders = t.children.map(function(child, index) { return go(child, coords.concat([index])); });
            var render = printExpression(t.head, subRenders);
            
            render.span.bind("click", function() {
                editCoords(coords);
            });

            return render.pretty;
        }
        
    }


    function printFunction(label, args) {
        var parent = $("<span>").text(label);
        if(args.length == 0) return {span: parent, pretty: P.span(parent)};

        var parts = [P.span(parent), P.span($("<span>").text("(")), P.blank()];
        for(var ii = 0; ii < args.length-1; ii++) {
            parts.push(args[ii]);
            parts.push(P.span($("<span>").text(",")));
            parts.push(P.blank());
        }
        parts.push(args[args.length-1]);
        parts.push(P.span($("<span>").text(")")));

        return {
            span: parent,
            pretty: P.group.apply(this, parts)
        };
    }

    function renderEditor(tree, coords, events) {
        if($.isArray(coords) && coords.length === 0) {
            var type_completions = completions.filter(function(it) {
                // Only type checking functions for now
                if(it[0] !== "function" || tree.head[0] !== "function") return true;

                var itType = $.isArray(it[1].type) ? it[1].type : [it[1].type];
                var myType = $.isArray(tree.head[1].type) ? tree.head[1].type : [tree.head[1].type];
                return itType.some(function(t) { return myType.indexOf(t) > -1; });
            });
            return P.span(autocomplete(type_completions, events));
        }
        var nextCoord = -1;
        if($.isArray(coords)) {
            nextCoord = coords[0];
        }

        var subRenders = tree.children.map(function(child, index) {
            var isEditingIndex = index === nextCoord;
            return renderEditor(child, isEditingIndex ? coords.slice(1) : null, events);
        });
        return printExpression(tree.head, subRenders).pretty;
    }

    function printExpression(head, subRenders) {
        switch(head[0]) {
        case "measure":
            var span = $("<span>").text(head[1].measureType.label);
            return {span: span, pretty: P.span(span)};
        case "attribute":
            var span = $("<span>").text(head[1].label);
            return {span: span, pretty: P.span(span)};
        case "function":
            if(head[1].print) return head[1].print.apply(this, subRenders);
            return printFunction(head[1].label, subRenders);
        case "string":
            span = $("<span>").css('text-style', 'italic').text('"' + head[1] + '"');
            return {span: span, pretty: P.span(span)};
        case "number":
            span = $("<span>").css('text-style', 'italic').text(head[1]);
            return {span: span, pretty: P.span(span)};
        }
        throw "Unrecognized node type";
    }

    function edit(tree, coords, cont) {
        var container = $("<div>");

        var back = [];
        var forward = [];

        subEdit();

        return container;

        function subEdit() {
            container.children().remove();
            container.append(P.pp(600, renderEditor(tree, coords, {
                'continue': function(val) {
                    back.push({tree: copyObject(tree), coords: copyArray(coords), forward: copyArray(forward)});

                    var t;
                    if(val[0] === "measure") {
                        t = util.node(["measure", {measureType: val[1], 
                                  aggregation: val[1].aggregations.indexOf("Sum") > -1 ? "Sum" : val[1].aggregations[0], 
                                  dateType: val[1].dateTypes[0],
                                  timeSeriesType: null
                                 }], []);
                        
                        // TODO: allow specification of the aggregation / dateType / timeSeriesType in editor
                        
                    } else {
                        var args = val[0] === "function" ? val[1].args : [];
                        t = util.node(val, args.map(function(type){ return util.blank(type); }));
                        for(var ii = args.length - 1; ii >= 0; ii--) {
                            forward.push(coords.concat([ii]));
                        }
                    }
                    set(tree, coords, t);
                    if(forward.length) {
                        coords = forward.pop();
                        subEdit();
                    } else {
                        cont( tree );
                    }
                },
                backspace: function() {
                    if(back.length === 0) {
                        cont( tree );
                    } else {
                        var b = back.pop();
                        tree = b.tree;
                        coords = b.coords;
                        forward = b.forward;
                        subEdit();
                    }
                },
                escape: function() { 
                    cont( tree ); 
                }
            })));
        }
    }

    function bqlEditor(bql, subjectArea, onChange) {
        var elem = $("<div>");
        function inactive() {
            elem.empty().append(P.pp(600, renderExpression(bql, active) ));
        }
        function active(coords) {
            elem.empty().append(edit(bql, coords, function(val) {
                bql = val;
                onChange(bql);
                inactive();
            }));
        }
        inactive();
        return elem;
    }
}
