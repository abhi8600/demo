var treeSelect = require("./treeSelect");
var $ = require("../jquery.js");

module.exports = function(items, events) {

    var container = $("<span>").css({position: 'relative', overflow: 'scroll', backgroundColor: 'lightgoldenrodyellow'});
    var elem = $("<input>").css({backgroundColor: 'lightgoldenrodyellow'}).attr('type' ,'text').width(400).appendTo(container);
    var listView = $("<div>").appendTo(container).width(398).css({'position': 'absolute', backgroundColor: 'lightgoldenrodyellow', border: "1px solid #ccc", top: '19px', 'padding': '7px'});


    var groups = {};
    items.map(function(it) {
        groups[ it.group ] = groups[it.group] || {head: ["group", it.group], children: []};
        groups[ it.group ].children.push({head: ["item", it], children: []});
    });
    var val = "";
    var tree = treeSelect(Object.keys(groups).map(function(key) { return groups[key]; }), listView);
    setTimeout(function() {
        elem.focus();
    }, 0);
    
    elem.bind('keypress', function(ev) {
        var s = String.fromCharCode(ev.which);
        var curr = elem.val();
        if(s != "") {
            val = curr + s;
            elem.val(val);
            tree.filter(val);
        }
        return false;
    });
    elem.bind('keydown', function(ev) {
        var key = ev.keyCode;
        if(key === 27) { // escape
            events.escape();
        } else if(key === 8) { // backspace
            if(elem.val() == "") {
                events.backspace();
            } else {
                val = elem.val().substring(0, elem.val().length - 1);
                elem.val(val);
                tree.filter(val);
            }
        } else if(key === 40) { // down arrow
            tree.down();
        } else if(key === 38) { // up arrow
            tree.up();
        } else if(key === 13) { // enter
            tree.expand();
            var val = tree.selectedValue();
            if(val) events['continue'](val);
        } else {
            return true;
        }
        return false;
    });
    return container;
}
