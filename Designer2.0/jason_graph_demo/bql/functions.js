var P = require("./pretty_printer");
var $ = require("../jquery.js");

var bql_functions = [];

function defn(name, group, type, doc, argTypes, opts) {
    var fn = {
        group: group,
        label: name,
        type: type,
        doc: doc,
        args: argTypes
    };
    if(opts && opts.print) {
        fn.print = opts.print;
    }
    if(opts && opts.bqlString) {
        fn.bqlString = opts.bqlString;
    }
    bql_functions.push(fn);
}

function binOp(str) {
    return {
        bqlString: function(left, right) {
            return "(" + left + " " + str + " " + right + ")";
        },
        print: function(left, right) {
            var span = $("<span>").text(str);
            return {
                span: span,
                pretty: P.group(P.span($("<span>").text("(")), P.blank(), left, P.blank(), P.span(span), P.blank(), right, P.blank(), P.span($("<span>").text(")")))
            };
        }
    }
}

var dateGroup = "Date Functions";
var mathGroup = "Arithmetic Functions";
var controlGroup = "Control Functions";
var cmpGroup = "Comparison Functions";
var otherGroup = "Other Functions";


var anyType = ["bool", "string", "int", "float", "date", "datetime"];

defn("Abs", mathGroup, "float", "Absolute value of a measure value", ["float"]);
// defn("Add", "list", "Allows you to add a value to end of an existing list.", ["list", "int"]);
// defn("AddParentChild", "Adds a single parent-child pair to the current list of pairs.", ["ParentID","ChildID"]);
// defn("Alt", "To use new query language syntax in spaces using classic query language.", alt:{expression});
defn("ArcCos", mathGroup, "float", "Returns the arc cosine of a number between ο and π.", ["float"]);
defn("ArcSin", mathGroup, "float", "Returns the arc sine of a number between -π/2 and π/2.", ["float"]);
defn("ArcTan", mathGroup, "float", "Returns the arc tangent of a number between -π/2 and π/2.", ["float"]);
defn("Float", mathGroup, "float", "Convert the argument to a Float", ["string"])
defn("Integer", mathGroup, "int", "Convert the argument to an Integer", ["string"])
defn("Ceiling", mathGroup, "int", "Returns the closest integer greater than or equal to the given number.", ["float"]);
defn("IsInf", mathGroup, "bool", "Evaluates an expression and returns true if the value is +/- infinity. The result can be used as a conditional check.", ["float"]);
defn("Cos", mathGroup, "float", "Returns the cosine of a given angle.", ["float"]);
defn("exp", mathGroup, "float", "Returns e raised to the power of the number given.", ["float"]);
defn("Floor", mathGroup, "int", "Returns the closest integer less than or equal to a given number.", ["float"]);
defn("+", mathGroup, "float", "Add two numbers", [["float","int"], ["float","int"]], binOp("+"));
defn("-", mathGroup, "float", "Subtract two numbers", [["float","int"], ["float","int"]], binOp("-"));
defn("/", mathGroup, "float", "Divide two numbers", [["float","int"], ["float","int"]], binOp("/"));
defn("*", mathGroup, "float", "Multiply two numbers", [["float","int"], ["float","int"]], binOp("*"));
defn("Year", dateGroup, "datepart", "Select the year", []);
defn("Month", dateGroup, "datepart", "Select the month", []);
defn("Day", dateGroup, "datepart", "Select the day", []);
defn("Year", dateGroup, "dateortimepart", "Select the year", []);
defn("Month", dateGroup, "dateortimepart", "Select the month", []);
defn("Day", dateGroup, "dateortimepart", "Select the day", []);
defn("Hour", dateGroup, "dateortimepart", "Select the hour", []);
defn("Minute", dateGroup, "dateortimepart", "Select the minute", []);
defn("Second", dateGroup, "dateortimepart", "Select the second", []);
defn("Date", dateGroup, "date", "Convert the argument to a Date", ["string"])
defn("DateTime", dateGroup, "datetime", "Convert the argument to a DateTime", ["string"])
defn("DateAdd", dateGroup, "date", "Add or subtract an integer from a date.", ["datepart" /*Year/Month/Day*/, "int", "date"]);
defn("DateDiff", dateGroup, "int", "Returns the difference in dates as an integer value. (NULLS are interpreted as 0. )", ["dateortimepart" /*Year/Month/Day/Hour/Minute/Second*/, "date", "date"]);
defn("DatePart", dateGroup, "int", "Returns integers from dates.", ["dateortimepart","date"]);
defn("GetWeekID", dateGroup, "int", "This function allows you to return a unique integer signifying the week based on a date or datetime value.", ["date"]);
defn("GetDayID", dateGroup, "int", "This function allows you to return a unique integer signifying the day based on a date or datetime value.", ["date"]);
defn("GetMonthID", dateGroup, "int", "This function allows you to return a unique integer signifying the month based on a date or datetime value.", ["date"]);
defn("DRank", otherGroup, "int", "Dense Rank returns the rank of each row within the result set, without any gaps in the ranking.", ["float"]);
//defn("Find", type, "Similar to LookupValue, except the lookup value does not need an exact match. If the match is exact that row is returned, otherwise, the first row that is greater than the lookup value is used. Parameters can be added before the logical query and referenced in the logical query using the %index syntax, starting with %0.", ["int", "expression", "int", "query"]);
defn("Format", otherGroup, "string", "Formats a number or date to the particular format provided.", ["float", "string"]);
//defn("GetPromptFilter", "A place holder for a dashboard prompt.", ["name"]);
//defn("GetPromptValue", "A place holder for a user defined value taken from a dashboard prompt. An optional second argument (such as null or a string such as '2012') can be used in the event that the first argument is not a currently defined prompt. The second argument can also be supplied by a variable using GetVariable(). Note that all values are returned as strings (except null) and must be cast to numeric data types if they are to be used in arithmetic functions.", ["name", "constant"]);
//defn("GetVariable", "Retrieves the value of a variable.", ["name"]);
defn("IFNULL", controlGroup, anyType, "Allows an expression to return a different result if it is null.", [anyType, anyType]);
defn("IIF", controlGroup, anyType, "Immediate IF. Allows an expression to return one result if the specified condition is true and a different one if it is false. Can be nested.", ["bool", anyType, anyType]);
defn("OR", cmpGroup, "bool", "true if either of its arguments are true", ["bool", "bool"], binOp("OR"));
defn("AND", cmpGroup, "bool", "true if both of its arguments are true", ["bool", "bool"], binOp("AND"));
defn("NOT", cmpGroup, "bool", "true if its argument is false and vice versa", ["bool"]);
defn("<", cmpGroup, "bool", "compare two values, returning true if the value on the left is less than the value on the right", [["float","int"], ["float","int"]], binOp("<"));
defn(">", cmpGroup, "bool", "compare two values, returning true if the value on the left is greater than the value on the right", [["float","int"], ["float","int"]], binOp(">"));
defn("<=", cmpGroup, "bool", "compare two values, returning true if the value on the left is less than or equal to the value on the right", [["float","int"], ["float","int"]], binOp("<="));
defn(">=", cmpGroup, "bool", "compare two values, returning true if the value on the left is greater than or equal to the value on the right", [["float","int"], ["float","int"]], binOp(">="));

module.exports =  bql_functions;

/*
 GetLevelAttribute

 Provides additional data either on the entire tree or on a given record that is being processed. Two options are supported.

 Option 1: GetLevelAttribute('NumLevels') // Returns the maximum depth of items in the tree.

 Option 2: GetLevelAttribute('CurrentDepth') // Returns the depth of the current item in the tree.

 

 GetLevelValue

 For the current pair, GetLevelValue(n) returns the ID n levels deep into the hierarchy. If there are fewer than n levels, it returns the ID of the lowest level available. Hence, the IDs will repeat for levels lower than their depth.

 */


/*
 Function()

 Allows for creation of a custom function that can be used in any logical query including an expression.

 Fucntion({datatype of return value}, {calculation/script} COMPLETE [Result]= {calculation from script} END COMPLETE)

 Function (Float, Dim [Var1] as Float = 0.0 Dim [Var2] as Float = 0.0 Complete [Result] = [Var1] - [Var2] End Complete)

 FunctionLookup

 Custom functions execute a script over an entire result set to return a result. It works by declaring a return data type (the type of the function) and providing both a script and a logical query. The script is executed for each row of the result set. Additionally, a lookup parameter can be added and will run the function only on rows that satisfy the filter for the targeted column being equal to the value set. The variable [Result] is populated with the return value.

 FunctionLookup(return data type,lookup parameter,script statement block,logical query)
 */


/*
 IsNaN

 Evaluates an expression and returns true if it is not a number and false if it is. The result can be used as a conditional check.

 IsNaN(evaluation,result if TRUE, result if FALSE)  

 IIF(IsNaN([OrderDate: Sum: Quantity]),0,[OrderDate: Sum: Quantity])

 Length

 Returns the number of characters in a string. This can also be used in a Birst ETL script to return the number of elements in a List object.

 Length([dimension])

 Length([Products.CategoryName])

 ETL Script Example: Length([myList]) - 1

 Let

 Allows re-use of logic in complex expressions by declaring expression-level variables, initializing them and re-using them in the expression.

 Let(variable declarations,expression body)

 Let(Dim [AvgPrice] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Avg: UnitPrice] from [All]) Dim [TotalQuantity] As Float = LookupValue(0,[Categories.CategoryName],1,Select [Categories.CategoryName],[OrderDate: Sum: Quantity] from [All]), IIF([AvgPrice]<25,25,[AvgPrice])*[TotalQuantity])

 Ln(x)

 Returns the natural logarithm of a given number in a specified base.

 Ln([logical column])

 Ln([OrderDate: Sum: Quantity])

 Log(x)

 Returns the logarithm of a given number in a specified base.

 Log([logical column])

 Log([OrderDate: Sum: Quantity])

 LookupRow

 Use over LookupValue if the result set will be in the same order as the existing set.

 LookupRow (index of the row to return, index of the column to return, SELECT [Query] from [ALL])

 LookupRow (0,1, Select [OrderDetail.OrderID], [OrderDate: Sum: Discount] from [ALL])

 LookupValue

 Lookup based on value in the current result set to get a dimension or measure value from another resultset. Looks for EXACT MATCH.

 LookupValue (column number from the query, [value in the current query to look at], column number of the new query, Select [new query] from [All])

 lookupvalue(0,[Products.CategoryName],1,Select [Products.CategoryName],[OrderDate: Sum: Quantity] from [All])

 Median

 Returns the value in the middle of all measure values at the report grain. An optional break by attribute denoted with "By" can be used.

 Median([logical column] By [logical column])

 Median([OrderDate: Sum: Quantity] By [OrderDate: Sum: Quantity])

 NextChild

 Once a set of parent-child pairs have been added, the list can then be iterated over one by one to flatten. NextChild positions the current record to the next available pair. If there are no more pairs left, it returns false.

 

 

 NumRows

 Returns the total number of rows in the current result set. (This function is not available in ETL Services.)

 NumRows()

 NumRows()

 Order By

 Sort function.

 Select [query] from [All] Order By [dimension]/[measure] Ascending/Asc/Descending/Desc

 Select [Product.Category Name], [Time.Year],[Unit Sales] from [All] Order By [Product.Category Name] Ascending, [Time.Year] Descending

 Position

 Returns the position of a second string within the first. You can specify the starting index to begin the search as an optional third parameter.

 Position('search string', [dimension], starting position number to begin search)

 Position(‘the’, [Varchar], 10) 

 (In this example, it would start looking at position 10.)

 Pow

 Raise x to the power of y.

 Pow(x,y)

 Pow(4,2)

 PTile

 Returns the percentile rank of each row within the result set.

 PTile([measure])

 PTile([OrderDate: Sum: Quantity])

 Random()

 Returns a double from 0.0 to 1.0.  This function ignores parameter values.

 Random()

 Random()

 Rank

 Returns the rank of each row within the result set.

 Rank([logical column])

 Rank([OrderDate: Sum: Quantity])

 Select [Time.Year],[Product.Category Name],Rank([Unit Sales] By [Time.Year]) ‘Sales Rank’ from [All] where [Time.Year]=2011 Or [Time.Year]=2012 Display by [Time.Year] Ascending, [Sales Rank] Ascending

 RemoveAll

 Removes all elements from a list.

 

 

 RemoveAt

 Allows you to remove an item from a given position in a list.

 RemoveAt([ListVariable],[IndexToRemove])

 

 Replace

 Allows a static string value as the search criteria to replace a value.

 Replace(original string/[dimension],part to replace,replacement string)

 Replace([Products.CategoryName], 'ages', 'age')

 ReplaceAll

 Allows a regular expression as the search criteria to replace a value.

 ReplaceAll(original string/[dimension],[expression],replacement string)

 ReplaceAll([Products.CategoryName],'P.*e','Organic Produce')

 RowNumber

 Returns the row number for the current result set.  (This function is not available in ETL Services.)

 [RownumVar]=RowNumber()

 RowNumber()

 Rsum

 Returns the trailing sum of values, based on the window size, at the current report grain for the window provided.

 Rsum(window size,[logical column])

 Rsum(10,[OrderDate: Sum: Quantity])

 Sin

 Returns the sine of a given angle.

 Sin([logical column])

 Sin([OrderDate: Sum: Quantity])

 Sparse

 Allows you to show dimension column values for which there is no fact data. As of 5.4, works with multiple dimensions. See About the Sparse Function for more information.

 Sparse([dimension])

 Sparse([Time.Year])

 sqrt(x)

 Returns the square root of a given positive number.

 sqrt([measure])

 sqrt([OrderDate: Sum: Discount])

 STAT

 Aggregate stats based on another result set. Valid aggregations are median, avg, min, max, sum, count, countdistinct, and stddev. 

 Stat(aggregation, index, [query])

 Stat(Median, 1,Select [Products.CategoryName], [OrderDate: Sum: Quantity]  from [All])

 Substring

 Returns a portion of a given string and  assumes a zero based index.

 SUBSTRING([dimension], starting position number, ending position number)

 Note: First position is 0, ending position is the position after the last that should be included.

 Substring([Products.CategoryName],20,22)

 (The example above returns 3 characters starting at position 20.)

 Tan

 Returns the tangent of a given angle.

 Tan([logical column])

 Tan([OrderDate: Sum: Quantity])

 ToLower

 Returns a character expression after converting data to lower case.

 ToLower([dimension])

 ToLower([Products.CategoryName])

 Top n

 Filter by Top n results.

 SELECT TOP x [query] FROM [ALL]

 Select Top 20 [Product.Product Name],[Unit Sales] from [All] Order By [Unit Sales] Descending

 ToTime

 Allows integer values to be converted into a time format, for example: HH:mm:ss. Supported in ETL scripts and column expressions.

 ToTime(1000000000, 'hours,minutes,seconds', '%d:%02d:%02d')

 ToTime(Integer([Order_Details.OrderID]+[Order_Details.ProductID]*1000),'HH:mm:ss:SSS')

 ToUpper

 Returns a character expression after converting data to upper case.

 ToUpper([dimension])

 ToUpper([Products.CategoryName])

 Transform

 Directly manipulate or create from scratch the report-level result set. Only one Transform is allowed per logical query. The Transform functions just like an ETL services script. It operates row-by-row over the original result set from the query, using WRITERECORD to output new rows as results. The entire result set is re-written as a result of the TRANSFORM statement. All the input and output columns need to be identical.

 Transform({script})

 Transform(Dim [Odd] As Integer = 0 If (Integer([Odd]/2))*2 = [Odd] Then
 [Sum: Quantity]=[Sum: Quantity]  WriteRecord End If [Odd] = [Odd] + 1)

 Trend

 Uses the Least Squares method to extrapolate values based on a linear trend.

 Trend({index of column to return},{index of column to lookup},{value to lookup},{logical query})

 TREND(1,0,[Time.Month Seq Number],SELECT [Time.Month Seq Number], [OrderDate: Sum: Quantity] FROM [ALL] WHERE [Time.Month Seq Number] >= 1136 AND [Time.Month Seq Number] <= 1148)

 Trim

 Trims leading and trailing spaces. Can be used in a constant to pad a value.

 Trim([dimension])
 [constant name] = Trim(string value with spaces)

 Trim([Products.CategoryName])
 [BevConstant] = Trim('Beverages         ')

 */
