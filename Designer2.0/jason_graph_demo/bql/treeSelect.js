var $ = require("../jquery.js");

function buildElem(container, item, level) {
    var elem = $("<div>").appendTo(container).css('position', 'relative');
    elem.css('paddingLeft', level*10);
    var number;
    var filter;
    var tt;
    if(item[0] === 'group') {
        elem.css('textDecoration', 'underline');
        elem.text(item[1]);
        number = $("<span>").css({backgroundColor: 'lightgoldenrodyellow', color: 'lightgrey', 'float': 'right'}).text("0").appendTo(elem);
        filter = function(){ return false; }
    } else if(item[0] === 'item') {
        filter = function(str) {
            if(str === "") {
                elem.empty().append($("<span>").text( item[1].label ));
                return false;
            }
            var index = item[1].label.toLowerCase().indexOf( str.toLowerCase() );
            if(index === -1) return true;
            var prev = 0;

            elem.empty();
            while(index !== -1) {
                elem.append( $("<span>").text( item[1].label.substring(prev, index) ) );
                elem.append( $("<b>").text( item[1].label.substring(index, index + str.length) ) );
                prev = index + str.length;
                index = item[1].label.toLowerCase().indexOf( str.toLowerCase(), index+1 );
            }
            elem.append( $("<span>").text( item[1].label.substring(prev) ) );
            
            return false;
        }
        filter("");
    } else {
        // constant
        filter = function(str) {
            if(item[0] === 'number') {
                if(str.match(/^[0-9]+(\.[0-9]*)?$/)) {
                    elem.empty().text("the number " + str);
                    return false;
                } else {
                    return true;
                }
            } else {
                if(str.length) {
                    elem.empty().append($("<i>").text('"' + str + '"'));
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
    return {
        expanded: false,
        setCount: function(count) {
            number && number.text(count);
        },
        filter: filter,
        visible: function(b){ 
            if(b) { 
                elem.show(); 
            } else { 
                elem.hide(); 
            } 
        },
        select: function(b) {
            if(b) {
                tt && tt.remove();
                if(item[1] && item[1].doc) {
                    tt = tooltip( item[1].doc, elem ).css({'backgroundColor': '#ccc', color: '#333'});
                }
                elem.css({backgroundColor: '#336', color: '#fff'});
            } else {
                tt && tt.remove();
                tt = null;
                elem.css({backgroundColor: 'lightgoldenrodyellow', color: '#333'});
            }
        }
    }
}

function tooltip(content, parent) {
    var ret = $("<div>").css({
        'position': 'absolute',
        'visibility': 'hidden',
        width: 200,
        padding: '5px 10px',
        top: 0
    }).html(content).appendTo(document.body);
    ret.css('right', -ret.outerWidth());
    ret.appendTo(parent);
    ret.css('visibility', 'visible');
    return ret;
}

module.exports = function(trees, container) {

    var filterString = "";
    var selection = -1;
    var visibleNodeList = [];
    trees.unshift({head: ["string"], children: []});
    trees.unshift({head: ["number"], children: []});
    var annotatedTrees = trees.map(function(tree) { return buildTree(tree, 0) });
    updateTrees();

    function buildTree(tree, level) {
        var elem = buildElem(container, tree.head, level);
        var ret = {
            head: tree.head,
            children: tree.children.map(function(child) { return buildTree(child, level+1); }),
            elem: elem
        };

        return ret;
    }
    
    function updateTrees() {
        visibleNodeList = [];
        annotatedTrees.map(function(node) { update(node, false); });
        function update(node, hidden) {
            var filtered = node.elem.filter(filterString);
            var visible = !filtered && !hidden;
            node.elem.visible(visible);
            if(visible) visibleNodeList.push(node);
            var childrenNotFiltered = node.children.reduce(function(sum, child) { return sum + update(child, hidden || !node.elem.expanded); }, 0);
            node.elem.setCount(childrenNotFiltered);
            if(node.head[0] === "item") {
                return filtered ? 0 : 1;
            } else {
                return childrenNotFiltered;
            }
        }
    }

    function up() {
        if(selection === -1) return;
        visibleNodeList[ selection ].elem.select(false);
        selection--;
        if(selection === -1) return;
        visibleNodeList[ selection ].elem.select(true);
    }

    function down() {
        if(selection === visibleNodeList.length-1) return;
        if(selection !== -1) visibleNodeList[ selection ].elem.select(false);
        selection++;
        visibleNodeList[ selection ].elem.select(true);
    }

    function deselect() {
        if(selection === -1) return;
        visibleNodeList[ selection ].elem.select(false);
        selection = -1;
    }

    function expand() {
        if(selection === -1) return;
        visibleNodeList[ selection ].elem.expanded = !visibleNodeList[ selection ].elem.expanded;
        updateTrees();
    }

    function filter(str) {
        deselect();
        filterString = str;
        updateTrees();
    }

    function selectedValue() {
        if(selection === -1) return undefined;

        var selected = visibleNodeList[selection];
        switch(selected.head[0]) {
        case "group":
            return undefined;
        case "string":
            return ["string", filterString];
        case "number":
            return ["number", Number(filterString)];
        case "item":
            return selected.head[1].value;
        default:
            throw "unexpected item type";
        }
    }

    return {
        filter: filter,
        up: up,
        down: down,
        deselect: deselect,
        expand: expand,
        selectedValue: selectedValue
    }

}
