var xml2js = require("xml2js");
var http = require("http");


/*
var globalPort = '443';
var globalURL = 'app2101.bws.birst.com';
var globalSpaceId = '380d2e26-a81f-42a7-b22f-c0ca5214afe0';
var globalUsername = 'gjakobson@birst.com';
var globalPassword = 'Birst123$';
var globalToken = null;
*/
var globalURL = '10.37.129.4';
var globalSpaceId = '432ee5ed-381e-4216-a116-44af4c31b67d';
var globalUsername = 'accountadmin@birst.com';
var globalPassword = 'birst@123';
var globalPort = '6105';
var globalToken = null;

function soapUp(xml) {
    return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bir="http://www.birst.com/">\
              <soapenv:Header/>\
              <soapenv:Body>'
                + xml +
             '</soapenv:Body>\
           </soapenv:Envelope>';
        
}

// Allows to search for a given name in the JSON version of an XML document
function searchTree(json, name) {
    if(typeof json !== "object") return [];
    if(Array.isArray(json)) {
        return [].concat.apply([], json.map(function(child) { return searchTree(child, name); }));
    }
    if(name in json) {
        return [json[name]];
    }
    return [].concat.apply([], Object.keys(json).map(function(key) {
        return searchTree(json[key], name);
    }));
}

function toJSON(xml) {
    var typeMap = {
        m: 'measure',
        a: 'attribute',
        f: 'folder',
        s: 'saved expression'
    };

    return [].concat.apply([], searchTree(xml, 'n').map(function(ns) {
        ns = Array.isArray(ns) ? ns : [ns];
        return ns.map(function(n) {
            return {
                type: typeMap[n.$.t],
                label: n.$.l,
                value: n.$.v,
                valid: n.$.vl,
                visible: n.$.visible === undefined ? true : n.$.visible,
                reportLabel: n.$.rl,
                description: n.$.d,
                children: [].concat.apply([], searchTree(n, 'c').map(toJSON))
            };
        });
    }));
}

//Mango_copy spaceId 432ee5ed-381e-4216-a116-44af4c31b67d

function CommandWebService(configuration) {
    configuration = configuration || { };
}

CommandWebService.prototype.hostname = globalURL;
CommandWebService.prototype.port = globalPort;
CommandWebService.prototype.baseUrl = '/CommandWebService.asmx';

CommandWebService.prototype.post = function(data, success, error) {
    var req = http.request({
        method: 'POST',
        hostname: this.hostname,
        port: this.port,
        path: this.baseUrl,
        headers: {
            'Accept': 'application/xml, text/xml, */*; q=0.01',
            'Content-Type': 'text/xml'
        }
    }, function(res) {
        var response = "";
        console.log(res.statusCode);
        res.on('data', function(it) { response += it; });
        res.on('end', function() { 
            xml2js.parseString(response, function(err, xml) { success(xml); });
        });
    });
    req.write(soapUp(data));
    req.end();
};

CommandWebService.prototype.login = function(username, password, success, error) {
    if (globalToken) {
        success(globalToken);
    }

    username = username || globalUsername;
    password = password || globalPassword;
    
    this.post(
          '<bir:Login>\
            <bir:username>' + username + '</bir:username>\
            <bir:password>' + password + '</bir:password>\
          </bir:Login>',

        function(data) {
            var loginResult = searchTree(data, 'LoginResult');
            if (loginResult.length > 0) {
                success(loginResult[0][0]); // call success with authentication token
            } else {
                error(JSON.stringify(data));
            }
        },

        error
    )

    return false;
};

CommandWebService.prototype.logout = function(token, success, error) {
    this.post(
         '<bir:Logout>\
            <bir:token>' + token + '</bir:token>\
         </bir:Logout>',
        success,
        error
    );
};

CommandWebService.prototype.listSpaces = function(token, success, error) {
    this.post(
         '<bir:listSpaces>\
            <bir:token>' + token + '</bir:token>\
          </bir:listSpaces>',

        function(xml) {
            var spaces = searchTree(xml, 'UserSpace')[0].map(function(userSpace) {
                return {
                    name: userSpace.name[0],
                    owner: userSpace.owner[0],
                    id: userSpace.id[0]
                }
            });
            success(spaces);
        },

        error
    );
}

CommandWebService.prototype.executeQueryInSpace = function(token, spaceId, query, success, error) {
    this.post(
         '<bir:executeQueryInSpace>\
            <bir:token>' + token + '</bir:token>\
            <bir:query>' + query + '</bir:query>\
            <bir:spaceID>' + spaceId + '</bir:spaceID>\
        </bir:executeQueryInSpace>',

        function(xml) {
            var result = searchTree(xml, 'executeQueryInSpaceResult');

            var rows = [].concat.apply([], searchTree( 
                searchTree(result, 'rows'),
                'ArrayOfString'
            ).map(function(arrayOfString) {
                return searchTree(arrayOfString, 'string');
            }));

            var columnNames = searchTree(
                searchTree(result, 'columnNames'),
                'string'
            ).map(function(string) { return string[0] });

            var displayNames = searchTree(
                searchTree(result, 'columnNames'),
                'string'
            ).map(function(string) { return string[0]; });

            var dataTypes = searchTree(result, 'dataTypes')[0].map(function(child) {
                var tagName = Object.keys(child)[0];
                return tagName + ' ' + child[tagName][0];
            });

            success({
                rows: rows,
                columnNames: columnNames,
                displayNames: displayNames,
                dataTypes: dataTypes,
                length: rows.length,
                hasMoreRows: searchTree(result, 'hasMoreRows')[0][0]
            });
        },
        error
    );
}

CommandWebService.prototype.listCustomSubjectAreas = function(token, spaceId, success, error) { // in progress
    this.post(
         '<bir:listCustomSubjectAreas>\
            <bir:token>' + token + '</bir:token>\
            <bir:spaceID>' + spaceId + '</bir:spaceID>\
        </bir:listCustomSubjectAreas>',
        function(xml) {
            success(searchTree(searchTree(xml, 'listCustomSubjectAreasResult'), 'string')[0]);
        },
        error
    );
}

CommandWebService.prototype.queryMore = function(token, queryToken, success, error) {
    this.post(
        '<bir:queryMore>\
            <bir:token>' + token + '</bir:token>\
            <bir:queryToken>' + queryToken + '</bir:queryToken>\
        </bir:queryMore>',
        success,
        error
    );
}

CommandWebService.prototype.getSubjectAreaContentRaw = function(token, spaceId, subjectAreaName, success, error) {
    // sample spaceId = 432ee5ed-381e-4216-a116-44af4c31b67d

    this.post(
        '<bir:getSubjectAreaContent>\
            <bir:token>' + token + '</bir:token>\
            <bir:spaceID>' + spaceId + '</bir:spaceID>\
            <bir:name>' + subjectAreaName + '</bir:name>\
        </bir:getSubjectAreaContent>',
        function(result) {
            xml2js.parseString(searchTree(result, "getSubjectAreaContentResult")[0], function(err, xml) {
                success(xml)
            });
        },
        error
    );
}

function extractLabeledNodes(tree, label) {
    if(Array.isArray(tree)) return [].concat.apply([], tree.map(function(child) { return extractLabeledNodes(child, label); }));
    var ret = [];
    if(tree.label === label) ret.push(tree);
    return ret.concat.apply(ret, tree.children.map(function(child) { return extractLabeledNodes(child, label); }));
}

CommandWebService.prototype.getSubjectAreaContent = function(token, spaceId, subjectAreaName, success, error) {
    this.getSubjectAreaContentRaw(token, spaceId, subjectAreaName, function(xml) { 
        success({
            root: 'SubjectAreaContent',
            children: toJSON(xml)
        }) 
    });
}

CommandWebService.prototype.getMeasures = function(token, spaceId, subjectAreaName, success, error) {
    this.getSubjectAreaContentRaw(token, spaceId, subjectAreaName, function(xml) {
        success({
            root: 'Measures',
            children: extractLabeledNodes(toJSON(xml), 'Measures')
        })
    });
}

CommandWebService.prototype.getAttributes = function(token, spaceId, subjectAreaName, success, error) {
    this.getSubjectAreaContentRaw(token, spaceId, subjectAreaName, function(xml) {
        success({
            root: 'Attributes',
            children: extractLabeledNodes(toJSON(xml), 'Attributes')
        })
    });
}

/*
 *Example usage:

 * var CWS = new CommandWebService();
 * CWS.login(null, null, function(x) { console.log('success', x) }, function(x) { console.log('error', x ) });
 * CWS.getMeasures('b30d87c912366d6616b1821190672709', '432ee5ed-381e-4216-a116-44af4c31b67d', 'Default Subject Area', function(x) { console.log(x) }, function(y) { console.log('error', y)})
 *
 var CWS = new CommandWebService();
 var sessionToken;

 CWS.login(null, null, function(token) {
 console.log('token', token);
 sessionToken = token;
 
 CWS.getSubjectAreaContent(token, globalSpaceId, 'Retail Analysis',
 function(data) {
 });

 
 CWS.listSpaces(token, function(data) {
 console.log('spaces', data);
 });
 });
 */

function loginAnd(cb) {
    var CWS = new CommandWebService();
    if(!globalToken) {
        CWS.login(null, null, function(token) { globalToken = token; cb(); });
        //setTimeout(function(){ globalToken = null; }, 300000);
    } else {
        cb();
    }
}

function subjectAreas(cb) {
    var CWS = new CommandWebService();
    loginAnd(function() {
        CWS.listCustomSubjectAreas(globalToken, globalSpaceId, function(names) {
            var sas = {}, finished = 0;
            names = [names[0]];
            names.map(function(name) {
                CWS.getSubjectAreaContent(globalToken, globalSpaceId, name, function(subjectArea) {
                    sas[name] = subjectArea;
                    if(++finished == names.length) {
                        cb(sas);
                    }
                });
            });
        });
    });
}

function query(q, cb) {
    var CWS = new CommandWebService();
    loginAnd(function() {
        CWS.executeQueryInSpace(globalToken, globalSpaceId, q, function(result) {
            cb(result);
        });
    });
}

module.exports = {subjectAreas: subjectAreas, query: query};
