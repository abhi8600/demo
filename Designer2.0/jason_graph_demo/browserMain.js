var $ = require("./jquery.js")
var Raphael = require("raphael-browserify");

var processSubjectArea = require("./subjectArea");
var assert = require("assert");
var bqlEditor = require("./bql/editor");
var bqlUtil = require("./bql/util");

var renderGraphSelections = require("./dimensionSelector");
var subjectAreas = {};

// y will be - {name: string, columns: {type: string, column: column}}

$('body').css('margin', 0);

var selectedGraph = {
    seriesDimension: null,
    y: [],
    x: {column: null},
    hue: {column: null},
    value: {column: null},
    size: {column: null}
};

Array.prototype.concatMap = function(fn) {
    return [].concat.apply([], this.map(fn));
}


function subjectAreasReady() {
    showMeasuresAgainstTime();
}

$.get("/subjectAreas", function(res) {
    Object.keys(res).map(function(name) {
        subjectAreas[ name ] = processSubjectArea(res[name]);
    });
    
    subjectAreasReady();
});



function yearMonthToDate(str) {
    var yearMonth = str.split("/");
    return new Date(yearMonth[0], yearMonth[1]-1, 0, 0, 0, 0, 0);
}


function showMeasuresAgainstTime() {
    var timeMeasures = subjectAreas['Default Subject Area'].measures.filter(function(measure) {
        return measure.dateTypes.length;
    });
    var query = "SELECT USING OUTER JOIN " + timeMeasures.map(function(measure) {
        var agg = measure.aggregations[0];
        if(measure.aggregations.indexOf("Sum") > -1) agg = "Sum";
        
        return "[" + measure.bqlString(measure.path[measure.path.length-1], agg, measure.dateTypes[0], null) + "]";
    }).join(" , ") + ", [Time.Year/Month] FROM [ALL]";
    console.log(query);
    $.post("/query", {q: query}, function(resp) {
        var sortedRows = resp.rows.sort(function(r1, r2) {
            return yearMonthToDate(r1[ r1.length-1 ]).getTime() - yearMonthToDate(r2[ r2.length-1 ]).getTime();
        });
        timeMeasures.map(function(measure, ii) {
            var timeLine = $("<span>");
            var container = $("<div>").css({width: 650, margin: '0 auto', borderBottom: '1px dotted #ccc', cursor: 'pointer'}).append(
                $("<span>").css({
                    'font-size': '20px',
                    'line-height': '50px',
                    'display': 'inline-block',
                    'verticalAlign': 'top',
                    'min-width': '180px'
                }).text(measure.label),
                timeLine
            ).appendTo($("body")).hover(function() {
                $(this).css('backgroundColor', '#dedede');
            }, function() {
                $(this).css('backgroundColor', '#fff');
            }).on('click', function() {
                var yearMonth = subjectAreas['Default Subject Area'].attributes.filter(function(attr) {
                    return attr.path[ attr.path.length-1 ] === 'Time.Year/Month';
                })[0];
                selectedGraph.x.column = {head: ["attribute", yearMonth], children: []};
                selectedGraph.y.push({name: measure.label, columns: [{dimensions: [], type: 'line', column: {head: ["measure", {aggregation: 'Sum', measureType: measure, dateType: measure.dateTypes[0], timeSeriesType: null}], children: []}}]});
                $("body").empty();
                renderGraphSelections(subjectAreas['Default Subject Area'], selectedGraph);
            });
            var paperHeight = 20, paperWidth = 300;
            var paper = Raphael(timeLine[0], paperWidth, paperHeight);
            var numbers = sortedRows.map(function(row){
                return isNaN(row[ii]) ? 0 : Number(row[ii]);
            });
            var scale = Math.max.apply(Math, numbers);
            paper.path(
                numbers.map(function(number, jj) {
                    var cmd = (jj == 0) ? "M" : "L";
                    cmd += "" + Math.round(paperWidth*jj/numbers.length) + "," + (scale ? Math.round(paperHeight - paperHeight*number/scale) : paperHeight);
                    return cmd;
                }).join("")
            ).attr('stroke', '#333');
        });
        console.log(resp);
    });
}


/*
var width = $(window).width(), height = $(window).height();

var paper = Raphael(0, 0, width, height);
renderGraph(
    [
        ["column",
         [
             [
                 [{h: .10, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .50, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .40, color: 'red'}, {h: .3, color: 'green'}]
             ],
             [
                 [{h: .10, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .50, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .40, color: 'red'}, {h: .3, color: 'green'}]
             ],
             [
                 [{h: .10, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .50, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .40, color: 'red'}, {h: .3, color: 'green'}]
             ],
             [
                 [{h: .10, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .50, color: 'red'}, {h: .2, color: 'green'}],
                 [{h: .40, color: 'red'}, {h: .3, color: 'green'}]
             ]
         ]
        ],
        ["area",
         [{y:.2, color: 'green'},
          {y:.1,color:'blue'},
          {y:.2,color:'red'},
          {y:.5,color:'grey'}]
        ],
        
        ["line",
         [{y:.3, color: 'green'},
          {y:.2,color:'blue'},
          {y:.1,color:'red'},
          {y:.6,color:'grey'}]
        ],

        ["scatter",
         [{x: .1, y:.8, color: 'green', size: .5}, // TODO scatter works with x as measure
          {x: .4, y:.6, color:'blue', size: .7},
          {x: .6, y:.7, color:'red', size: .8},
          {x: .8, y:.7, color:'grey', size: .1}]
        ]
    ],50, 50, width-100, height-100, paper);
*/
