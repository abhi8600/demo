var $ = require("./jquery.js")
var Raphael = require("raphael-browserify");
var bqlEditor = require("./bql/editor");
var bqlUtil = require("./bql/util");
var renderSeries = require("./series");
var hpack = require("./pack");

module.exports = renderGraphSelections;

// Not including texture or shape for now
var dimensions = ['x', 'hue', 'value', 'size'];
var resizeInterval;

function renderGraphSelections(subjectArea, dims) {
    var pack;
    var elem = $("<div>").css({'width': '200px'});
    render();
    function plus() {
        return $("<span>").text("+").css({display: 'inline-block', 'float': 'right', backgroundColor: '#383', color: 'white', cursor: 'pointer'});
    }
    function newYColumn() {
        return {type: 'line', column: null, dimensions: dimensions.filter(function(dim) { return dim !== 'x' && dims[dim].column; })};
    }
    function render() {
        if(resizeInterval) clearInterval(resizeInterval);
        pack = hpack();
        pack.giveWidth(200, elem);
        renderSeries(dims, pack, function() {
            var w = 0, h = 0;
            resizeInterval = setInterval(function() {
                var bw = $("body").width();
                var bh = $("body").height();
                if(w !== bw || h !== bh) {
                    w = bw;
                    h = bh;
                    $("body").children().detach();
                    $("body").append( pack.fill( w-20 ) );
                }
            }, 200);
        });
        elem.empty();
        var ys = $("<div>")
                .css({border: '2px solid white', backgroundColor: '#eee'})
                .append($("<span>").text("y"))
                .appendTo(elem);

        ys.append(plus().click(function() {
            dims.y.push({name: 'Y Axis ' + (dims.y.length + 1), columns: [newYColumn()]});
            render();
        }));
        
        dims.y.map(function(yAxis, ii) {
            var axis = $("<div>").css({'border': '2px solid white', backgroundColor: '#ddd'})
                    .append($("<span>").text(yAxis.name))
                    .append(plus().click(function() {
                        yAxis.columns.push(newYColumn());
                        render();
                    })).appendTo(ys);
            yAxis.columns.map(function(col) {
                axis.append(bqlEditor(col.column || bqlUtil.blank(), subjectArea, function(it) { col.column = it; render(); }));
                var axisType = $("<select>").appendTo(axis).on('change', function(ev) {
                    col.type = $(this).val();
                    if(col.type === 'column') {
                        col.groupDimensions = col.dimensions;
                        col.stackDimensions = [];
                        col.dimensions = undefined;
                    } else {
                        if(col.groupDimensions) {
                            col.dimensions = col.groupDimensions.concat( col.stackDimensions );
                            col.stackDimensions = col.groupDimensions = undefined;
                        }
                    }
                    render();
                }).css({display: 'inline-block'});
                ['line', 'scatter', 'area', 'column'].map(function(type) {
                    var opt = axisType.append($("<option>").val(type).text(type));
                });
                axisType.val( col.type );

                dimensions.map(function(dim) {
                    if(dim == 'x' ||!bqlUtil.isValid(dims[dim].column)) return;
                    var toggle = $("<span>").text(dim).appendTo( axis );
                    if(col.type !== 'column') {
                        var activeDim = col.dimensions.indexOf(dim) > -1;
                        toggle.css(activeDim ? {color: 'green'} : {color: 'grey'});
                        toggle.click(function() {
                            if(activeDim) { col.dimensions.splice( col.dimensions.indexOf(dim), 1 ); }
                            else { col.dimensions.push(dim); }
                            render();
                        });
                    } else {
                        var stackDim = col.stackDimensions.indexOf(dim) > -1;
                        var groupDim = col.groupDimensions.indexOf(dim) > -1;
                        toggle.css(stackDim || groupDim ? {color: 'green'} : {color: 'grey'});
                        if(stackDim) toggle.append($("<span>").text(" - stack"));
                        if(groupDim) toggle.append($("<span>").text(" - group"));
                        
                        toggle.click(function() {
                            if(!stackDim && !groupDim) {
                                col.groupDimensions.push(dim);
                            } else if(groupDim) {
                                col.groupDimensions.splice(col.groupDimensions.indexOf(dim), 1);
                                col.stackDimensions.push(dim);
                            } else {
                                col.stackDimensions.splice(col.stackDimensions.indexOf(dim), 1);
                            }
                            render();
                        });
                    }
                });
            });
        });
        dimensions.map(function(dim) {
            $("<div>")
                .css({'border': '2px solid white', backgroundColor: '#eee'})
                .append($("<span>").text(dim))
                .append(bqlEditor(dims[dim].column || bqlUtil.blank(), subjectArea, function(it) { dims[dim].column = it; render(); }))
                .appendTo(elem);
        });
    }
    
    return elem;
}
