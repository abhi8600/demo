var $ = require("./jquery.js");

module.exports = hpack;

function hpack() {
    var parts = [];
    var ret = {
        giveWidth: function(width, contents) {
            parts.push({type: 'give', width: width, contents: contents});
            return ret;
        },
        takeWidth: function(fn) {
            parts.push({type: 'take', fn: fn});
        },
        fill: function(width) {
            var cont = $("<div>");
            parts.filter(function(part) { return part.type == 'give'; }).map(function(part) {
                width -= part.width;
            });
            parts.map(function(part) {
                if(part.type === 'give') {
                    cont.append($("<div>").css({
                        display: 'inline-block',
                        width: part.width,
                        verticalAlign: 'top'
                    }).append(part.contents));
                } else {
                    cont.append($("<div>").css({
                        display: 'inline-block',
                        width: part.width,
                        verticalAlign: 'top'
                    }).append(part.fn(width)));
                }
            });
            return cont;
        }
    }
    return ret;
}
