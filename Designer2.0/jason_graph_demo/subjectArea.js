
module.exports = function processSubjectArea(subjectArea, name) {
    var attributes = {};
    var measures = {};
    var measureBQLMap = {};
    var timeSeries = [];
    go(subjectArea, []);
    
    function go(tree, parents) {
        if (tree.children && tree.children.length) {
            tree.children.map(function (child) {
                go(child, [tree].concat(parents));
            });
        } else if (tree.type === 'measure') {
            addToMeasures(tree, parents);
        } else if (tree.type === 'attribute') {
            addToAttributes(tree, parents);
        }
    }

    function addToAttributes(tree, parents) {
        var path = [tree.value].concat(parents.map(function (p) {
            return p.label;
        })).reverse();
        path = path.slice(path.indexOf("Attributes") + 1);
        var key = JSON.stringify(path);
        attributes[key] = attributes[key] || {path: path, label: path[path.length - 1]};
    }

    function addToMeasures(tree, parents) {
        var aggregation = tree.label.replace(/^\s*/, '');
        var dateType = null;
        // TODO: This regex won't be completely reliable, should get the information about whether this folder is a date type more explicitly.
        if (parents[0].label.match(/^By /)) {
            dateType = parents.shift().label;
        }
        var path = parents.map(function (p) {
            return p.label;
        }).reverse();
        var tsIndex = path.indexOf('Time Series Measures');
        if (tsIndex !== -1) {
            var timeSeriesLabel = tree.label.match(/(.*?) (SUM|COUNT|COUNT DISTINCT|AVG|MIN|MAX)/i)[1];
            addUnique(timeSeries, timeSeriesLabel);
            measureBQLMap[ JSON.stringify([path[path.length - 1], aggregation.match(/sum|count|count distinct|avg|min|max/i)[0], dateType, timeSeriesLabel]) ] = tree.value;
            return;
        }
        path = path.slice(path.indexOf("Measures") + 1);
        var key = JSON.stringify(path);
        if (!measures[key]) {
            measures[key] = {path: path, aggregations: [aggregation], dateTypes: dateType ? [dateType] : [], label: path[path.length - 1].replace(/_/g, ' '), bqlString: measureBQL};
        }
        addUnique(measures[key].aggregations, aggregation);
        if (dateType) addUnique(measures[key].dateTypes, dateType);
        measureBQLMap[ JSON.stringify([path[path.length - 1], aggregation, dateType, null]) ] = tree.value;
    }
    
    function addUnique(list, elem) {
        if (list.indexOf(elem) === -1) list.push(elem);
    }
    function measureBQL(measureName, aggregation, dateType, timeSeriesLabel) {
        return measureBQLMap[ JSON.stringify([measureName, aggregation, dateType, timeSeriesLabel]) ];
    }

    return {
        name: name,
        attributes: Object.keys(attributes).map(function (key) {
            return attributes[key];
        }),

        measures: Object.keys(measures).map(function (key) {
            return measures[key];
        }),
        timeSeries: timeSeries
    };
}
