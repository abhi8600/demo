var $ = require("./jquery.js")
var Raphael = require("raphael-browserify");

var processSubjectArea = require("./subjectArea");
var assert = require("assert");
var bqlEditor = require("./bql/editor");
var bqlUtil = require("./bql/util");
var renderGraph = require("./graphRender");
var axes = require("./axisRender");

module.exports = function(dims, pack, complete) {
    var allValidSeries = dims.y.concatMap(function(axis) {
        return axis.columns.filter(function(col) { return bqlUtil.isValid(col.column); });
    });
    mapAsync(dims.y, function(yAxis, ii, finished) {
        var validSeries = yAxis.columns.filter(function(col) { return bqlUtil.isValid(col.column); });

        mapAsync(validSeries, function(series, jj, finished) {
            getRows(queryColumns(series, dims, series.column), finished, dims.seriesDimension, allValidSeries.indexOf(series));
        }, finished);
    }, function(resultSetsByAxis) {
        var combinedResultSets = resultSetsByAxis.concatMap(function(axisResults) {
            return axisResults.concatMap(function(rows) { return rows; });
        });
        var dimMappings = getDimMappings(dims, combinedResultSets);

        var chartSeries = [];
        var yAxisMaxima = dims.y.map(function(yAxis, ii) {
            var res = seriesForAxis(yAxis, dimMappings, resultSetsByAxis[ii], dims);
            chartSeries.push(res.series);
            return res.yMax;
        });
        var smallestRatio = 1;
        var smallestAxis = 0;
        yAxisMaxima.map(function(max, ii) { 
            if(max == 0) return;

            var ratio = axes.chooseTicks(max) / max;
            if(ratio < smallestRatio) {
                smallestRatio = ratio;
                smallestAxis = ii;
            }
        });
        yAxisMaxima = yAxisMaxima.map(function(max, ii) {
            if(max == 0) return 0;

            var ratio = axes.chooseTicks(max) / max;
            max = max*(ratio / smallestRatio);
            chartSeries[ii].map(function(serie) {
                mapOverRows(serie, function(row) {
                    row.y /= max;
                });
            });
            return max;
        });
        chartSeries = chartSeries.concatMap(function(x) { return x; });
        

        var legendsContainer = $("<div>").css('display', 'inline-block');
        Object.keys(dimMappings).map(function(key) {
            if(key == 'x' || key == 'y') return;
            if(dimMappings[key].values.length < 2) return;
            if(key === dims.seriesDimension) {
                legendsContainer.append(legendFor('series', {dim: key, values: allValidSeries.map(function(col) { return col.column.head[1].measureType.label; })}));
            } else {
                legendsContainer.append(legendFor(dims[key].name, dimMappings[key]));
            }
            legendsContainer.append($("<br>"));
        });

        var columnSeries = chartSeries.filter(function(serie) { return serie.type === 'column'; });
        if(columnSeries.length) {
            var col = columnSeries.shift();
            columnSeries.map(function(serie) {
                serie.groups.map(function(group, xInd) {
                    col.groups[xInd].stacks.push(group.stacks[0]);
                });
            });
            chartSeries = [col].concat(chartSeries.filter(function(serie) { return serie.type !== 'column'; }));
        }

        normalizeSeries(chartSeries, dimMappings);

        pack.takeWidth(function(w) {
            var h = $(document).height() - 10;
            var cont = $("<div>");
            var paper = Raphael(cont[0], w, h);
            var axisLeft = 0;
            var axisHeight = 40;
            yAxisMaxima.map(function(yMax, ii) {
                if(yMax === 0) return;

                var name = bqlUtil.colName( dims.y[ii].columns[0].column );
                axisLeft += axes.renderYAxis(name, yMax, smallestRatio, axisLeft, 0, w, h - axisHeight, paper, ii == yAxisMaxima.length-1);
            });
            axes.renderXAxis(bqlUtil.colName(dims.x.column), dimMappings.x.values, axisLeft, h - axisHeight, w - axisLeft, axisHeight, paper);
            renderGraph(chartSeries, dimMappings, axisLeft, 0, w - axisLeft, h - axisHeight, paper);
            return cont;
        });
        var legendWidth = legendsContainer.appendTo($("body")).width();
        console.log("legendWidth", legendWidth);
        legendsContainer.detach();
        pack.giveWidth(legendWidth, legendsContainer);
        complete();
    });
}

function normalizeSeries(chartSeries, dimMappings) {
    function normalizeRow(row) {
        for(var dim in dimMappings) {
            if(dim in row) {
                row[ dim ] = dimMappings[dim].mapping( row[ dim ] );
            }
        }
        row.color = Raphael.hsb( 'hue' in row ? 0.5 + 0.5*row.hue :  0.5, 1.0, .3 + .7*('value' in row ? row.value : .5));
        if(row.size === undefined) row.size = 0.3;
    }

    chartSeries.map(function(serie) {
        mapOverRows(serie, normalizeRow);
        if(serie.type !== 'column') {
            normalizeRow(serie.attrs);
        }
    });
}

function mapOverRows(serie, fn) {
    if(serie.type === 'column') {
        serie.groups.map(function(group) {
            group.stacks.map(function(stack) {
                stack.pieces.map(function(piece) {
                    piece.rows.map(function(row) {
                        fn(row);
                    });
                });
            });
        });
    } else {
        serie.points.map(function(point) {
            point.rows.map(function(row) {
                fn(row);
            });
        });
    }
}

function seriesForAxis(yAxis, dimMappings, resultSets, dims) {
    var series = yAxis.columns.concatMap(function(col, ii) {
        if(!bqlUtil.isValid(col.column)) return [];
        if(col.type === 'column') {
            var groupAttributes = col.groupDimensions
                    .map(function(dim) { return dimMappings[dim]; }).filter(function(x) { return x; });

            var stackAttributes = col.stackDimensions
                    .map(function(dim) { return dimMappings[dim]; }).filter(function(x) { return x; });

            var groups = groupByAttrs([dimMappings.x], resultSets[ii]);
            groups = groups.map(function(group) {
                var stacks = groupByAttrs(groupAttributes, group.rows);
                stacks = stacks.map(function(stack) {
                    return {
                        attrs: stack.attrs,
                        pieces: groupByAttrs(stackAttributes, stack.rows)
                    };
                });

                return {
                    attrs: group.attrs,
                    stacks: stacks
                };
            });

            return [{
                type: col.type,
                groups: groups
            }];
        } else {
            var linearAttributes = col.dimensions
                    .filter(function(dim) { return bqlUtil.isValid(dims[dim].column) && bqlUtil.isAttribute(dims[dim].column); })
                    .map(function(dim) { return dimMappings[dim]; });
            if(dims.seriesDimension)  {
                linearAttributes.push(dimMappings[ dims.seriesDimension ]);
            }
            return groupByAttrs(linearAttributes, resultSets[ii]).map(function(serie) {
                return {
                    type: col.type,
                    attrs: serie.attrs,
                    points: groupByAttrs([dimMappings.x], serie.rows)
                };
            });
        }
    });

    var yMax = 0;
    series.map(function(serie) {
        if(serie.type === 'column') {
            serie.groups.map(function(group) {
                group.stacks.map(function(stack) {
                    var stackY = 0;
                    stack.pieces.map(function(piece) {
                        piece.rows.map(function(row) {
                            stackY += Number(row.y);
                        });
                    });
                    yMax = Math.max(yMax, stackY);
                });
            });
        } else {
            serie.points.map(function(point) {
                point.rows.map(function(row) {
                    yMax = Math.max(yMax, row.y);
                });
            });
        }
    });
    
    return {yMax: yMax, series: series};
}

function tuples(arrs) {
    if(arrs.length == 0) {
        return [[]];
    }
    return [].concat.apply([], arrs[0].map(function(v1) {
        return tuples(arrs.slice(1)).map(function(tuple) { return [v1].concat(tuple); });
    }));
}

function groupByAttrs(attrs, rows) {
    var mapping = {};
    tuples(attrs.map(function(attr) { return attr.values; })).map(function(tuple) {
        var attrMap = {};
        attrs.map(function(attr, ii) {
            attrMap[attr.dim] = tuple[ii];
        });
        mapping[ JSON.stringify(tuple) ] = {attrs: attrMap, rows: []};
    });
    rows.map(function(row) {
        var rowTuple = attrs.map(function(attr) { return row[ attr.dim ]; });
        mapping[ JSON.stringify(rowTuple) ].rows.push(row);
    });
    return Object.keys(mapping).map(function(key) { return mapping[key]; });
}

function getDimMappings(dims, combinedResultSets) {
    var validDims = getValidDims(dims);
    if(dims.seriesDimension) {
        validDims.push(dims.seriesDimension);
    }
    var dimMapping = {};
    validDims.map(function(dim) { dimMapping[dim] = getAttr(dim, combinedResultSets); });
    return dimMapping;
}

function getValidDims(dims) {
    return Object.keys(dims).filter(function(dim) {
        if(dim == 'y' || dim == 'seriesDimension') return false;
        return bqlUtil.isValid(dims[dim].column);
    });
}

function queryColumns(series, dims, yColumn) {
    if(series.type === 'column') {
        var dimNames = series.stackDimensions.concat(series.groupDimensions).concat(series.measures || []);
    } else {
        dimNames = series.dimensions;
    }
    return dimNames.concat(['x'])
        .map(function(name) { return {dim: name, column: dims[name].column}; })
        .concat([{dim: 'y', column: yColumn}])
        .filter(function(dim) { return bqlUtil.isValid(dim.column); });
}

function runQuery(cols, fn) {
    var query = "SELECT USING OUTER JOIN " + cols.map(bqlUtil.bqlString).join(", ") + " FROM [All]";
    $.post("/query", {q: query}, function(resp) { fn(resp.rows); });
}

function mapAsync(arr, fn, finished) {
    var inFlight = arr.length;
    var results = [];
    arr.map(function(x, ii) {
        fn(x, ii, function(result) { 
            results[ii] = result; 
            if(--inFlight == 0) finished(results);
        });
    });
}

function getRows(cols, finish, seriesDim, seriesNumber) {
    runQuery(cols.map(function(col) { return col.column; }), function(rows) {
        finish(rows.map(function(row) {
            var obj = {};
            cols.map(function(col, ii) { 
                obj[ col.dim ] = row[ ii ];
            });
            if(seriesDim) {
                obj[ seriesDim ] = seriesNumber;
            }
            return obj;
        }));
    });
}

function getAttr(dim, rows) {
    var values = [];
    rows.map(function(row) {
        if(values.indexOf(row[ dim ]) === -1)
            values.push(row[dim]);
    });
    values.sort();
    return {dim: dim, values: values, mapping: mappingFor(values)};
}

function mappingFor(values) {
    if(values.length == 1) { 
        return function(x) { return 0.5; };
    } if(values.filter(function(it) { return isNaN(it); }).length == 0) {
        var max = Math.max.apply(Math, values);
        return function(x) { return max > 0 ? Number(x) / max : 0; }
    } else {
        return function(x) { return values.indexOf(x) / (values.length - 1); }
    }
    
}

function legendFor(name, attr) {
    console.log(name, attr);
    var legend = $("<div>").css({
        display: 'inline-block',
        overflow: 'visible',
        border: '1px solid #666',
        borderRadius: 5,
        padding: 10 ,
        position: 'relative'
    });
    legend.append($("<span>").css({
        position: 'absolute',
        fontSize: '12px',
        padding: '0 3px',
        left: 10,
        top: 3,
        backgroundColor: 'white',
        textTransform: 'uppercase'
    }).text(name));
    var isRange = attr.values.filter(function(it) { return isNaN(it); }).length == 0;
    if(isRange) {
        legend.append(gradientLegend(attr.dim, Math.max.apply(Math, attr.values)));
    } else {
        legend.append(valuesLegend(attr.dim, attr.values));
    }
    return legend;
}

function gradientLegend(dim, max) {
    var ret = $("<div>");
    ret.append(dimGradient(dim));
    ret.append(
        $("<span>").css({
            position: 'relative',
            display: 'inline-block',
            verticalAlign: 'top',
            height: 60,
            minWidth: 30
        }).append(
            $("<span>").css({paddingLeft: 10}).text(max),
            $("<span>").css({position: 'absolute', bottom:0, left: 10}).text(0)
        ));
    return ret;
}

function dimGradient(dim) {
    var container = $("<div>").width(15).height(60).css({display: 'inline-block'});
    var paper = Raphael(container[0], 15, 60);
    switch(dim) {
    case 'hue':
        paper.rect(0,0,15,60).attr('fill', '90-' + Raphael.hsb(0.5,1.0,.65) + '-' + Raphael.hsb(1.0, 1.0, .65));
        break;
    case 'value':
        paper.rect(0,0,15,60).attr('fill', '90-' + Raphael.hsb(0.5,1.0,.3) + '-' + Raphael.hsb(0.5, 1.0, 1.0));
        break;
    case 'size':
        paper.path("M0,0L11,0L1,60L0,60Z").attr('fill', Raphael.hsb(0.5,1.0,.65));
        break;
    }
    return container;
}

function valuesLegend(dim, values) {
    var ret = $("<div>");
    values.map(function(val) {
        var norm = values.indexOf(val)/(values.length-1);
        ret.append(dimLegend(dim, norm), $("<span>").text(val), $("<br>"));
    });
    return ret;
}

function dimLegend(dim, norm) {
    var ret = $("<span>").css({display: 'inline-block', width: 15, height: 15, marginRight: 10, backgroundColor: Raphael.hsb(0.5, 1.0, 0.65)});
    switch(dim) {
    case 'hue': ret.css('backgroundColor', Raphael.hsb(0.5 + norm*0.5, 1.0, .65)); break;
    case 'value': ret.css('backgroundColor', Raphael.hsb(0.5, 1.0, .3 + .7*norm)); break;
    case 'size': ret.width(1 + Math.floor(norm*10)).css('marginRight', 24 - Math.floor(norm*10)); break;
    }
    return ret;
}
