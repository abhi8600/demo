var Raphael = require("raphael-browserify");
var $ = require("./jquery.js")

function renderYAxis(name, max, ratio, left, top, w, h, paper, drawLines) {
    var tick = Math.round(max * ratio);
    var textWidth = 0;
    var texts = [];
    for(var ii = 0; ii*tick < max; ii++) {
        var txt = numString(ii*tick);
        textWidth = Math.max(textWidth, measureText(txt));
        texts.push(txt);
    }

    // Raphael doesn't draw text correctly unless the paper is in the DOM at the time, so draw it after a timeout
    setTimeout(function() {
        paper.text(left+10, top + (h/2), name).attr('font-size', '14px').transform("r270");
    }, 0);

    texts.map(function(txt, ii) {
        var y = Math.floor(h*(1 - (ii*tick / max)));
        setTimeout(function() {
            paper.text(left+textWidth+20, top + y + 2, txt).attr('text-anchor', 'end');
        }, 0);
        if(drawLines) {
            paper.rect(left+textWidth+20, top + y, w - textWidth, 1).attr('fill', '#aaa').attr('stroke', '#aaa');
        }
    });
    return textWidth + 20;
}

function renderXAxis(name, values, x, y, w, h, paper) {
    setTimeout(function() {
        paper.text(x + (w/2), y + (h/2), name).attr('font-size', '14px');
    }, 0);
}

function chooseTicks(max) {
    var lg8 = Math.log(max/8)/Math.log(10);
    
    var tens = Math.floor(lg8), part = Math.pow(10, lg8 - tens);
    
    if(part < 2.5) return Math.pow(10, tens);
    if(part < 5) return Math.pow(10, tens)*2.5;
    return Math.pow(10, tens)*5;
}

function numString(n) {
    var lg10 = Math.log(n) / Math.log(10);
    if(lg10 >= 9) return (n / 1000000000) + "G";
    if(lg10 >= 6) return (n / 1000000) + "M";
    if(lg10 >= 3) return (n / 1000) + "K";
    return "" + n;
}


var measurePaperContainer = $("<div>").css({position: 'absolute', left: -100});
var measurePaper = Raphael(measurePaperContainer[0], 1, 1);

function measureText(txt) {
    $("body").append(measurePaperContainer);
    var elem = measurePaper.text(0, 0, txt);
    var ret = elem.getBBox().width;
    elem.remove();
    return ret;
}

module.exports = {
    renderYAxis: renderYAxis,
    renderXAxis: renderXAxis,
    chooseTicks: chooseTicks
}
