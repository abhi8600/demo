'use strict';


// Includes

var gulp = require('gulp');
var clean = require('gulp-clean');
var gutil = require('gulp-util');
var changed = require('gulp-changed');
var filesize = require('gulp-filesize');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var jshintStylish = require('jshint-stylish');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var ngmin = require('gulp-ngmin');
var minifyHtml = require('gulp-minify-html');
var html2js = require('gulp-html2js');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var prefix = require('gulp-autoprefixer');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gulpKarma = require('gulp-karma')({configFile: 'config/karma.conf.js'});
var svgSprites = require('gulp-svg-sprites');

var paths = require('./paths.js');

// Currently Unused:
// var livereload = require('gulp-livereload');
// var imagemin = require('gulp-imagemin');
// var ngHtml2js = require('gulp-ng-html2js');
// var concatCss = require('gulp-concat-css');
// var notify = require('gulp-notify');


// Configuration

var config = {
    dest: 'dist',
    paths: {
        js: {
            libs: paths.libs,
            scripts: paths.scripts,
            script_overrides: ['!app/js/templates-app.js', 'gulpfile.js'],
            bql: 'app/structured_bql_editor/**/*.js'
        },
        scss: 'app/css/sass/birst-styles.scss',
        scss_watch: 'app/css/sass/**/*.scss',
        css: 'app/css/*.css',
        html: paths.html,
        partials: paths.partials,
        fonts: 'app/fonts/*',
        images: 'app/img/**/*.{png,jpg,jpeg}',
        svg: 'app/img/**/*.svg'
    }
};


// Utility

gulp.task('clean', function () {
    return gulp.src(config.dest, {read: false})
        .pipe(clean());
});

var handleError = function (err) {
    gutil.log(err);
    gutil.beep();
    this.emit('end');
};


// JavaScript, Template Cache, Testing

gulp.task('hint', function () {
    // Runs JSHint on the array of globbed paths.  The `additions`
    // variable allows for files not included in `scripts.js` to be
    // hinted, as well as preventing other files from being hinted.
    // Note: implicitly uses `.jshintrc`.
    return gulp.src(config.paths.js.scripts.concat(config.paths.js.script_overrides))
        .pipe(jshint())
        .pipe(jshint.reporter(jshintStylish));
});

gulp.task('karma', function () {
    // This version of `gulp karma` uses the "helper" branch of the gulp-karma repo.
    gulpKarma.start().then(gulpKarma.run);
    gulp.watch(config.paths.js.scripts.concat(config.paths.js.script_overrides), function () {
        gulpKarma.run();
    });
});

gulp.task('bqleditor', function () {
    // Generates the `structured_bql_editor.js` lib file.  Note that
    // the `source()` method converts the Browserify stream into the
    // type of stream Gulp expects.
    // Pass `{debug: true}` to bundle() to include the source map.
    return browserify()
        .add('./app/structured_bql_editor/main.js')
        .bundle({detectGlobals: false})
        .on('error',  handleError)
        .pipe(source('structured_bql_editor.js'))
        .pipe(gulp.dest('app/lib'));
});

gulp.task('libs', ['bqleditor'], function () {
    return gulp.src(config.paths.js.libs)
        .pipe(concat('js/libs.js'))
        .pipe(filesize())
        .pipe(uglify())
        .pipe(filesize())
        .pipe(gulp.dest(config.dest));
});

gulp.task('templates', function () {
    return gulp.src(config.paths.partials)
        .pipe(minifyHtml({empty: true, spare: true, quotes: true}))
        .pipe(html2js({outputModuleName: 'templates-app', base: 'app/'}))
        .pipe(concat('templates-app.js'))
        .pipe(gulp.dest('app/js'))
        .pipe(ngmin())
        .pipe(uglify())
        .pipe(gulp.dest(config.dest));
});

gulp.task('scripts', ['templates'], function () {
    return gulp.src(config.paths.js.scripts)
        .pipe(concat('js/scripts.js'))
        .pipe(filesize())
        .pipe(ngmin())
        .pipe(uglify({mangle: false}))
        .pipe(filesize())
        .pipe(gulp.dest(config.dest));
});


// Style

gulp.task('scss', function () {
    // Generates the `birst-styles.css` file,
    // which is used in development.
    return gulp.src(config.paths.scss)
        .pipe(sass())
        .on('error', handleError)
        .pipe(gulp.dest('app/css'));
        //.pipe(livereload());  // livereload integration "in progress"
});

gulp.task('css', ['scss'], function () {
    // Generates the minified `main.css` file,
    // which is used in production.
    return gulp.src(config.paths.css)
        .pipe(concat('styles.css'))
        .pipe(prefix('last 1 version'))
        .pipe(minifyCss())
        .pipe(rename('main.css'))
        .pipe(gulp.dest(config.dest + '/css'));
});


// HTML, Fonts, Images

gulp.task('fonts', function () {
    return gulp.src(config.paths.fonts)
        .pipe(changed(config.dest + '/fonts'))
        .pipe(gulp.dest(config.dest + '/fonts'));
});

gulp.task('html-index', function () {
    return gulp.src('release/index.html')
        .pipe(rename('index.html'))
        .pipe(gulp.dest(config.dest));
});

gulp.task('html', ['html-index'], function () {
    return gulp.src(config.paths.html)
        //.pipe(minifyHtml())
        .pipe(gulp.dest(config.dest));
});

gulp.task('images', function () {
    return gulp.src(config.paths.images)
        .pipe(changed(config.dest + '/img'))
        .pipe(gulp.dest(config.dest + '/img'));
});

gulp.task('svg', function () {
    var svgConfig = {
        className: '.svg-icsson-%f',
        svgId: 'svg-icon-%f',
        refSize: 512
    };
    return gulp.src('app/img/TopBar/*.svg')
        .pipe(svgSprites.svg(svgConfig))
        .pipe(gulp.dest(config.dest + '/img/svg'));
});


// "Compound" Tasks

gulp.task('watch', ['hint', 'scss', 'templates', 'bqleditor'], function () {
    gulp.watch(config.paths.js.scripts.concat(config.paths.js.script_overrides), ['hint']);
    gulp.watch(config.paths.scss_watch, ['scss']);
    gulp.watch(config.paths.partials, ['templates']);
    gulp.watch(config.paths.js.bql, ['bqleditor']);
});

gulp.task('build', [
    'clean',
    'html',
    'css',
    'fonts',
    'images',
    'libs',
    'scripts',
    'templates'
]);

gulp.task('default', ['clean']);




// Scratch / In Progress

// Previous way of running Karma - this uses the gulp-karma "master" branch
//
// var gulpKarma = require('gulp-karma');
// gulp.task('karma', function () {
//     // This task uses the Karma configuration file specified.
//     // Note `fake-file.txt` doesn't exist, but is necessary to force
//     // Karma to use its `files` as defined in its config.
//     return gulp.src('fake-file.txt')
//         .pipe(gulpKarma({
//             configFile: 'config/karma.conf.js',
//             action: 'watch'
//         }))
//         .on('error', function (err) {
//             throw err;
//         });
// });

// Different way to run Karma tests - without an external karma.conf.js
//
// var karma = require('karma').server;
// gulp.task('karma-alternative', function () {
//     karma.start({
//         frameworks: ['jasmine'],
//         files: [].concat(
//             config.paths.js.libs,
//             config.paths.js.scripts,
//             'test/lib/angular/angular-mocks.js',
//             'test/unit/localSettings.js',
//             'test/unit/services/adhocreports/*.js',
//             'test/unit/**/*.js',
//             config.paths.partials
//         ),
//         browsers: ['PhantomJS'],
//         singleRun: true
//     });
// });



