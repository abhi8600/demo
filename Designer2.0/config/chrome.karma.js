basePath = '../';

files = [
  JASMINE,
  JASMINE_ADAPTER,
  'app/lib/jquery-1.10.1.min.js',
  'app/lib/angular/angular.js',
  'app/lib/angular-dragdrop.js',
  'app/lib/ng-table.js',
  'app/lib/ui-*.js',
  'test/lib/angular/angular-mocks.js',
  'test/unit/localSettings.js',
  'app/lib/underscore.js',
  'app/lib/sortable.js',
  'app/js/lib_ext/underscore_ext.js',
  'app/js/services/ngProgress.js',
  'app/js/app.js',
  'app/js/services/CommandWebService.js',
  'app/js/services/ConvenientCommandWebService.js',
  'app/js/services/ChartDataTransformer.js',
  'app/js/services/HighchartBuilder.js',
  'app/js/dialogControllers.js',
  'app/js/directives/designer.js',
  'app/js/services/services.js',
  'app/js/**/*.js',
  'test/unit/**/*.js',
  'app/partials/**/*.html'
];

exclude = [
    //'app/js/**/CommandWebService.js'
    //'test/unit/services/convenientCommandWebServiceSpec.js',
    //'test/unit/services/commandWebServiceSpec.js'
]

autoWatch = true;

browsers = ['Chrome', 'PhantomJS'];

junitReporter = {
    outputFile: 'test_out/unit.xml',
    suite: 'unit'
};

reporters = ['progress', 'coverage'];

preprocessors = {
    'app/js/**/*.js': 'coverage',
    'app/partials/**/*.html': 'html2js'
};

coverageReporter = {
    type: 'html',
    dir: 'test/coverage/unit/'
}
