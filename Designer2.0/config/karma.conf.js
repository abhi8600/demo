module.exports = function(config) {
    config.set({
        basePath: '../',

        plugins: [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-ng-html2js-preprocessor'
        ],

        frameworks: ['jasmine'],

        files: [
            //JASMINE,
            //JASMINE_ADAPTER,
            'app/lib/jquery-1.10.1.min.js',
            'app/lib/angular/angular-1.2.6/angular.js',
            'app/lib/angular/angular-1.2.6/angular-route.js',
            'app/lib/angular/angular-1.2.6/angular-animate.js',
            'app/lib/angular/angular-1.2.6/angular-loader.js',
            'app/lib/angular/angular-1.2.6/angular-sanitize.js',
            'app/lib/angular/angular-1.2.6/angular-touch.js',
            'app/lib/angular-dragdrop.js',
            'app/lib/angular-strap-custom.js',
            'app/lib/chai/chai.js',
            'app/lib/ui-*.js',
            'app/lib/angular/angular-1.2.6/angular-mocks.js',
            'test/unit/localSettings.js',
            'test/fakeSubjectArea.js',
            'app/lib/lodash.min.js',
            'app/lib/sortable.js',
            'app/js/lib_ext/underscore_ext.js',
            'app/js/app.js',
            'app/js/models/*.js',
            'test/unit/services/adhocreports/*.js',
            'app/js/**/*.js',
            'test/unit/**/*.js',
            'app/partials/**/*.html'
        ],

        autoWatch: true,

        browsers: ['PhantomJS'],

        reporters: ['progress'],

        preprocessors: {
            'app/partials/**/*.html': 'html2js'
        }
    });
};
