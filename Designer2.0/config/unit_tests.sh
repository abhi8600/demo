#!/bin/bash

export PATH=/usr/local/bin:/usr/local/share/npm/bin:$PATH
export PHANTOMJS_BIN=/usr/local/share/npm/bin/phantomjs

/usr/local/share/npm/bin/karma start ~/Dev/birst/Designer2.0/config/karma.conf.js --single-run

exit $?
