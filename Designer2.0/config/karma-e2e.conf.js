basePath = '../';

files = [
    JASMINE,
    JASMINE_ADAPTER,
    //ANGULAR_SCENARIO,
    //ANGULAR_SCENARIO_ADAPTER,
    'app/lib/jquery-1.10.1.min.js',
    'app/lib/angular/angular.js',
    'app/lib/angular-dragdrop.js',
    'app/lib/underscore.js',
    'test/e2e/localSettings.js',
    'app/js/services/CommandWebService.js',
    'app/js/app.js',
    'app/js/directives/designer.js',
    'app/js/services/services.js',
    'app/js/**/*.js',
    'test/e2e/**/*.js'
];

autoWatch = false;

browsers = ['Chrome', 'PhantomJS'];

singleRun = true;

proxies = {
    '/': 'http://localhost:8000/'
};

junitReporter = {
    outputFile: 'test_out/e2e.xml',
    suite: 'e2e'
};

reporters = ['progress', 'coverage'];

preprocessors = {
    'app/js/**/*.js': 'coverage'
};

coverageReporter = {
    type: 'html',
    dir: 'test/coverage/e2e/'
}

/*
if text output is desired instead...

coverageReporter = {
  type : 'text',
  dir : 'test/coverage/e2e/',
  file : 'coverage.txt'
}
*/
