/* globals describe, it, expect, jasmine */

describe('Underscore Extensions', function() {
   describe('replace', function() {
        it('should replace the primitive value you specify with the primitive value you specify', function() {
            expect(_.replace([1,2,3], 2, 5)).toEqual([1,5,3]);
        });


        it('should replace the evaluated value you specify with the value you specify', function() {
            expect(_.replace([1,2,3,4], function(value) {
                return value % 2 === 0;
            }, 5)).toEqual([1,5,3,5]);
        });
        
        it('should replace the primitive value you specify with the evaluated value you specify', function() {
            expect(_.replace([1,2,3,4], 2, function(value) {
                return value + 9;
            })).toEqual([1,11,3,4]);
        });

        it('should replace the evaluated value you specify with the evaluated value you specify', function() {
            expect(_.replace([1,2,3,4], function(value) {
                return value % 2 === 0;
            }, function(value) {
                return value + 9;
            })).toEqual([1,11,3,13]);
        });

   });

    describe('keysWhere', function() {
        it('should return keys where the iterator function returns true', function() {
            expect( _.keysWhere({
                    'first': 'A',
                    'second': 'B',
                    'third': 'C'
                }, function(value) {
                    return value === 'A' || value === 'C';
                })
            ).toEqual(['first', 'third']);
        });
    });

    describe('whereNotAny', function() {
        it('should exclude objects where the property has the given value', function() {
            expect(
                _.whereNotAny( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3 },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'testProperty': 'exclude' }
                )
            ).toEqual(
                [
                    { 'testProperty': 'include', 'id': 1 },
                    { 'otherProperty': 'include', 'id': 3 },
                    { 'otherProperty': 'exclude', 'id': 4 },
                    { 'testProperty': 'include', 'id': 5 }
                ]
            );
        });

        it('should exclude objects where at least one property has the corresponding value', function() {
            expect(
                _.whereNotAny( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3 },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'testProperty': 'exclude', 'id': 5 }
                )
            ).toEqual(
                [
                    { 'testProperty': 'include', 'id': 1 },
                    { 'otherProperty': 'include', 'id': 3 },
                    { 'otherProperty': 'exclude', 'id': 4 }
                ]
            );
        });
    });

    describe('whereNotAll', function() {
        it('should exclude objects where the property has the given value', function() {
            expect(
                _.whereNotAll( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3 },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'testProperty': 'exclude' }
                )
            ).toEqual(
                [
                    { 'testProperty': 'include', 'id': 1 },
                    { 'otherProperty': 'include', 'id': 3 },
                    { 'otherProperty': 'exclude', 'id': 4 },
                    { 'testProperty': 'include', 'id': 5 }
                ]
            );
        });

        it('should exclude objects where the properties do have the given values', function() {
            expect(
                _.whereNotAll( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3 },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'testProperty': 'exclude', 'id': 2 }
                )
            ).toEqual(
                [
                    { 'testProperty': 'include', 'id': 1 },
                    { 'otherProperty': 'include', 'id': 3 },
                    { 'otherProperty': 'exclude', 'id': 4 },
                    { 'testProperty': 'include', 'id': 5 },
                    { 'testProperty': 'exclude', 'id': 6 }
                ]
            );
        });
    });

    describe('whereHasAll', function() {
        it('should keep objects where the property is defined', function() {
            expect(
                _.whereHasAll( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3 },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'testProperty': 'exclude' }
                )
            ).toEqual(
                [
                  { 'testProperty': 'include', 'id': 1 },
                  { 'testProperty': 'exclude', 'id': 2 },
                  { 'testProperty': 'include', 'id': 5 },
                  { 'testProperty': 'exclude', 'id': 6 }
                ]
            );
        });

        it('should keep objects where either property is defined', function() {
            expect(
                _.whereHasAll( [ { 'testProperty': 'include', 'id': 1 },
                              { 'testProperty': 'exclude', 'id': 2 },
                              { 'otherProperty': 'include', 'id': 3, 'extraProperty': 'exists' },
                              { 'otherProperty': 'exclude', 'id': 4 },
                              { 'testProperty': 'include', 'id': 5 },
                              { 'testProperty': 'exclude', 'id': 6 }
                            ],

                            { 'otherProperty': true, 'extraProperty': true }
                )
            ).toEqual(
                [
                              { 'otherProperty': 'include', 'id': 3, 'extraProperty': 'exists' }
                ]
            );
        });
    });

    describe('htmlEscape', function() {
      it('should escape html/xml characters like greater than, less than, ampersand, etc', function() {
        expect( _.htmlEscape('<http>') ).toEqual('&lt;http&gt;');
        expect( _.htmlEscape('This & that') ).toEqual('This &amp; that');
      });
    });

    describe('sum', function() {
      it('should add all numbers together in a number only array', function() {
        expect( _.sum([2,3,4]) ).toEqual(9);
        expect( _.sum([1,2,3,4,5]) ).toEqual(15);
      });
    });

    describe('memoizeObject', function() {
        it('should call the underlying function only once for each input object', function() {
            var n = 0;
            // Test function returns a different result each time
            var testFunction = _.memoizeObject(function(/*ob*/) {
                return n++;
            });
            var ob1 = {};
            var ob2 = {};
            
            expect(testFunction(ob1)).toBe(0);
            expect(testFunction(ob2)).toBe(1);
            expect(testFunction(ob1)).toBe(0);
            expect(testFunction(ob2)).toBe(1);
            expect(testFunction.cache).toEqual([[ob1, 0], [ob2, 1]]);
        });
    });

    describe('cartesianProduct', function() {
        it('should generate the cartesian product of an array of arrays', function() {
            expect(_.cartesianProduct([[1,2,3],['a','b','c'],[[4],[5],[6]]]))
                .toEqual([
                    [ 1, 'a', [ 4 ] ],
                    [ 1, 'a', [ 5 ] ],
                    [ 1, 'a', [ 6 ] ],
                    
                    [ 1, 'b', [ 4 ] ],
                    [ 1, 'b', [ 5 ] ],
                    [ 1, 'b', [ 6 ] ],

                    [ 1, 'c', [ 4 ] ],
                    [ 1, 'c', [ 5 ] ],
                    [ 1, 'c', [ 6 ] ],

                    [ 2, 'a', [ 4 ] ],
                    [ 2, 'a', [ 5 ] ],
                    [ 2, 'a', [ 6 ] ],

                    [ 2, 'b', [ 4 ] ],
                    [ 2, 'b', [ 5 ] ],
                    [ 2, 'b', [ 6 ] ],

                    [ 2, 'c', [ 4 ] ],
                    [ 2, 'c', [ 5 ] ],
                    [ 2, 'c', [ 6 ] ],

                    [ 3, 'a', [ 4 ] ],
                    [ 3, 'a', [ 5 ] ],
                    [ 3, 'a', [ 6 ] ],

                    [ 3, 'b', [ 4 ] ],
                    [ 3, 'b', [ 5 ] ],
                    [ 3, 'b', [ 6 ] ],

                    [ 3, 'c', [ 4 ] ],
                    [ 3, 'c', [ 5 ] ],
                    [ 3, 'c', [ 6 ] ]
                ]);
        });
    });

    describe('namespace', function() {
        var globalObject = { };

        var sublib = _.namespace('Birst.lib.sublib', globalObject);

        expect(sublib).toEqual({ });

        expect(globalObject.Birst.lib.sublib).toEqual(jasmine.any(Object));

        var window = (function() {
            return this;
        })();

        var oldBirst = window.Birst;

        window.Birst = undefined;

        _.namespace('Birst.lib.sublib');

        expect(window.Birst.lib.sublib).toEqual(jasmine.any(Object));

        delete window.Birst;

        window.Birst = oldBirst; // restore original object from before testing
    });
});
