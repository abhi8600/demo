/* global BqlNode */

describe('BqlNode', function() {
	
	var savedExpression = {"type":"saved expression","label":"Uniquely Named Expression","value":"SavedExpression(&apos;Uniquely Named Expression&apos;)","visible":"true","reportLabel":"SavedExpression(&apos;Uniquely Named Expression&apos;)","children":[]}

	it('should be able to produce the correct bql for a saved expression', function() {
		var bqlNode = new visualizer.BqlNode(savedExpression, []);

		expect(bqlNode.expressionToString()).toEqual("SavedExpression('Uniquely Named Expression')"); //"SavedExpression('Uniquely Named Expression')");
	});
});