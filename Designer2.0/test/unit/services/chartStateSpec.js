describe('chartState', function() {
	beforeEach(function() {
		module('visualizer');
	});

	it('should be able to get and set a chart title', inject(function(chartStateService) {
		var title = 'test title';

		chartStateService.setTitle(title);
		expect(chartStateService.getTitle()).toBe(title);
	}));

	it('should be able to get and set a chart title', inject(function(chartStateService) {
		var mode = 2;

		chartStateService.setChartAutoMode(mode);
		expect(chartStateService.getChartAutoMode()).toBe(mode);
	}));

    /*it ('should get pre-set color palette approved by BC', inject(function (chartStateService){
        var colors = ['#91c4e5', '#a7c877', '#e37e8f', '#f3b400', '#bdaadd', '#fe7f48', '#57cec7', '#c7a083', '#fe95f3', '#b9baba', '#6295b7', '#7f9c53', '#a35b67', '#b28300', '#9982bf', '#d15f2e', '#3c8f89', '#876d59', '#c65dbc', '#797a7a'];
        expect(chartStateService.getColorPalette()).toEqual(colors);
    }))*/

    it('should be able to set and get chart type as "line"', inject(function(chartStateService) {
        var chartType='line'
        chartStateService.setCurrentChartType(chartType)
        expect(chartStateService.getCurrentChartType()).toEqual(chartType)
    }));

    it('should be a valid chart format object for title', inject(function(chartStateService) {
        expect(chartStateService.getChartFormat('Title')).toEqual(jasmine.any(Object))
    }));

    it('should be a valid chart format object for XAxis', inject(function(chartStateService) {
        expect(chartStateService.getChartFormat('XAxis')).toEqual(jasmine.any(Object))
    }));

    it('should be a valid chart format object for YAxis', inject(function(chartStateService) {
        expect(chartStateService.getChartFormat('YAxis')).toEqual(jasmine.any(Object))
    }));

    it('should be a valid chart format object for Legend', inject(function(chartStateService) {
        expect(chartStateService.getChartFormat('Legend')).toEqual(jasmine.any(Object))
    }));


});
