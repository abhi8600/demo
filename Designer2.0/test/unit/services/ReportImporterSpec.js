/* global describe, it, expect, _, ReportImporter, spyOn, beforeEach */

describe('ReportImporter', function() {
	var scope = {
		trashBucket: {
			columns: []
		},
		stage: {
			getCategoryColumn: function() {
				return this.Category;
			},
			setCategoryColumn: function(column) {
				this.Category = column;

				return column;
			},
			addColumn: function() {

			},
			clear: function() {

			},
			addAxis: function(x) {
				return x;
			}
		},
		updateChart: function() {

		},
		safeApply: function() {
			
		}
	};

	var fakeChart1 = {
		chartType: 'Column',
		categories: [1],
		series: [3]
	};

	var fakeChart2 = {
		chartType: 'Line',
		categories: [1],
		series: [5]
	};

	var fakeBubbleChart = {
		chartType:  'Bubble',
		categories: [1],
		series:     [5],
		series2:    [7],
		series3:    [9]
	};

	var fakeBubbleColumns = {
		1: 1,
		5: 5,
		7: 7,
		9: 9
	};

	var fakeScatterChart = {
		chartType:  'Scatter',
		categories: [1],
		series:     [5],
		series2:    [7],
	};

	var fakeScatterColumns = {
		1: 1,
		5: 5,
		7: 7
	};

	var fakeMultiMeasureLineChart = {
		chartType:  'Line',
		categories: [1],
		series:     [3, 5, 7]
	};

	var fakeMultiMeasureScatterChart = {
		chartType:  'Scatter',
		categories: [1],
		series:     [9],
		series2:    [3, 5, 7]
	};

	var fakeReport = { };

	fakeReport.columns = [
		{
			name: 'Categories',
			columnType: 'Dimension',
			isMeasureExpression: false,
			reportIndex: 1
		},
		{
			name: 'Revenue',
			columnType: 'Measure',
			isMeasureExpression: true,
			reportIndex: 3
		},
		{
			name: 'Gross Margin',
			columnType: 'Measure',
			isMeasureExpression: true,
			reportIndex: 5
		}
	];

	fakeReport.dataFilters = [ ];

	fakeReport.charts = [ fakeChart1, fakeChart2 ];

	var fakeReport2 = _.extend({ }, fakeReport);

	fakeReport2.charts = [ fakeChart1 ];

	describe('import', function() {
		it('should attempt to add charts', function() {
			var RI = new ReportImporter();

			RI.prepareColumnForStage = function(column) {
				return column.reportIndex;
			};

			spyOn(RI, 'addChart');

			RI.import(fakeReport2, scope);

			expect(RI.addChart).toHaveBeenCalledWith(fakeChart1, {1: 1, 3: 3, 5: 5}, scope);
			expect(RI.addChart.calls.length).toEqual(1);
		});

		it('should attempt to add (the correct number of) multiple charts', function() {
			var RI = new ReportImporter();

			RI.prepareColumnForStage = function(column) {
				return column.reportIndex;
			};

			spyOn(RI, 'addChart');

			RI.import(fakeReport, scope);

			expect(RI.addChart).toHaveBeenCalledWith(fakeChart1, {1: 1, 3: 3, 5: 5}, scope);
			expect(RI.addChart).toHaveBeenCalledWith(fakeChart2, {1: 1, 3: 3, 5: 5}, scope);
			expect(RI.addChart.calls.length).toEqual(2);
		});
	});

	describe('addChart', function() {
		var localScope;

		beforeEach(function() {
			localScope = _.extend({
				stage: {
					getCategoryColumn: function() {
						return this.Category;
					},
					setCategoryColumn: function(column) {
						this.Category = column;

						return column;
					},
					addColumn: function() {

					},
					clear: function() {

					}
				},
				updateChart: function() {

				}
			}, scope);
		});

		//localScope.stage.addSeriesWithTemporaryAxis; // use getters to check state afterward instead... add things to stage

		//localScope.stage.getSeriesColumns();

		// it('should attempt to add a category column', function() {
		// 	var RI = new ReportImporter();

		// 	RI.prepareColumnForStage = function(column) {
		// 		return column.reportIndex;
		// 	};

		// 	var stage = localScope.stage;

		// 	spyOn(stage, 'getCategoryColumn');
		// 	spyOn(stage, 'setCategoryColumn');

		// 	RI.addChart(fakeChart1, {1: 1, 3: 3, 5: 5}, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(1);
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(1);
		// });

		// it('should replace the category column if requested', function() {
		// 	var RI = new ReportImporter();

		// 	RI.prepareColumnForStage = function(column) {
		// 		return column.reportIndex;
		// 	};

		// 	var stage = localScope.stage;

		// 	spyOn(stage, 'getCategoryColumn');
		// 	spyOn(stage, 'setCategoryColumn');

		// 	RI.addChart(fakeChart1, {1: 1, 3: 3, 5: 5}, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(1);
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(1);

		// 	RI.addChart(fakeChart1, {1: 'A', 3: 3, 5: 5}, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(2);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith('A');
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(2);
		// });

		// it('should be able to add a bubble chart', function() {
		// 	var RI = new ReportImporter();

		// 	RI.prepareColumnForStage = function(column) {
		// 		return column.reportIndex;
		// 	};

		// 	var stage = localScope.stage;

		// 	spyOn(stage, 'getCategoryColumn');
		// 	spyOn(stage, 'setCategoryColumn');
		// 	spyOn(stage, 'addColumn');

		// 	RI.addChart(fakeBubbleChart, fakeBubbleColumns, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(5);
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(1);

		// 	expect(stage.addColumn.calls.length).toEqual(3);
		// 	expect(stage.addColumn).toHaveBeenCalledWith(7, undefined, 'bubble', { axis: undefined });
		// 	expect(stage.addColumn).toHaveBeenCalledWith(9, 'Size');
		// 	expect(stage.addColumn).toHaveBeenCalledWith(1, 'Color');
		// });

		// it('should be able to add a scatter chart', function() {
		// 	var RI = new ReportImporter();

		// 	RI.prepareColumnForStage = function(column) {
		// 		return column.reportIndex;
		// 	};

		// 	var stage = localScope.stage;

		// 	spyOn(stage, 'getCategoryColumn');
		// 	spyOn(stage, 'setCategoryColumn');
		// 	spyOn(stage, 'addColumn');

		// 	RI.addChart(fakeScatterChart, fakeScatterColumns, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(5);
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(1);

		// 	expect(stage.addColumn.calls.length).toEqual(2);
		// 	expect(stage.addColumn).toHaveBeenCalledWith(7, undefined, 'scatter', { axis: undefined });
		// 	expect(stage.addColumn).toHaveBeenCalledWith(1, 'Color');
		// });

		// it('should be able to add a column / line / area chart', function() {
		// 	var RI = new ReportImporter();

		// 	RI.prepareColumnForStage = function(column) {
		// 		return column.reportIndex;
		// 	};

		// 	var stage = localScope.stage;

		// 	spyOn(stage, 'getCategoryColumn');
		// 	spyOn(stage, 'setCategoryColumn');
		// 	spyOn(stage, 'addColumn');

		// 	RI.addChart(fakeChart1, {1: 1, 3: 3, 5: 5}, localScope);

		// 	expect(stage.getCategoryColumn).toHaveBeenCalled();
		// 	expect(stage.getCategoryColumn.calls.length).toEqual(1);
		// 	expect(stage.setCategoryColumn).toHaveBeenCalledWith(1);
		// 	expect(stage.setCategoryColumn.calls.length).toEqual(1);

		// 	expect(stage.addColumn.calls.length).toEqual(1);
		// 	expect(stage.addColumn).toHaveBeenCalledWith(3, undefined, 'column', { axis: undefined });
		// });

		it('should be able to add a multi-layer report', function() {
			// verified manually, need to write a test for it though...
		});
	});
});