/* globals describe, beforeEach, expect, it, adhocReports, inject */

describe('adhoc report converter', function() {
    beforeEach(module('visualizer'));

    it('should be defined', inject(function(adhocConverter) {
        expect(adhocConverter.convert).toBeDefined();
    }));

    it('should have a visualized display filter on the date column Date', inject(function(adhocConverter) {
        var reportName = adhocReports.displayDate;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Date') {
                expect(column.displayFilter.items.length).toBe(2);
                expect(column.filter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should have a visualized display filter on the numeric attribute column CategoryID', inject(function(adhocConverter) {
        var reportName = adhocReports.displayNumericAttribute;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'CategoryID') {
                expect(column.displayFilter.items.length).toBe(5);
                expect(column.filter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should have a visualized display filter on the varchar attribute column CategoryName', inject(function(adhocConverter) {
        var reportName = adhocReports.displayVarcharCategoryName;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'CategoryName') {
                expect(column.displayFilter.items.length).toBe(2);
                expect(column.filter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should not support "is not null" data filter on the varchar attribute column Region', inject(function(adhocConverter) {
        var reportName = adhocReports.dataIsNotNullVarcharAttributeRegion;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Region') {
                expect(column.filter).toBe(undefined);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should not support "is not null" display filter on the varchar attribute column Region', inject(function(adhocConverter) {
        var reportName = adhocReports.displayIsNotNullVarcharAttributeRegion;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Region') {
                expect(column.filter).toBe(undefined);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should not support "is null" data filter on the varchar attribute column Region', inject(function(adhocConverter) {
        var reportName = adhocReports.dataIsNullVarcharAttributeRegion;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Region') {
                expect(column.filter).toBe(undefined);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should not support "is null" display filter on the varchar attribute column Region', inject(function(adhocConverter) {
        var reportName = adhocReports.displayIsNullVarcharAttributeRegion;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Region') {
                expect(column.filter).toBe(undefined);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should support data filter on the time attribute column Date', inject(function(adhocConverter) {
        var reportName = adhocReports.dataTimeAttributeDate;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'Date') {
                expect(column.filter.items.length).toBe(4);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should support data filter on the time attribute column Date', inject(function(adhocConverter) {
        var reportName = adhocReports.dataTimeAttributeOrderDate;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'OrderDate') {
                expect(column.filter.items.length).toBe(10);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should support data filter on the  visualized time attribute column Date', inject(function(adhocConverter) {
        var reportName = adhocReports.displayDateSavedExpressionNumericAttributeVisualized;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'num_attr') {
                expect(column.filter).toBe(undefined);
                console.log('NOTE: we can get this next one working - display filter in projection list');
                expect(column.displayFilter).toBe(undefined); // expect to be defined! change logic
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

    it('should not support data filter on the unvisualized time attribute column Date', inject(function(adhocConverter) {
        var reportName = adhocReports.displayDateSavedExpressionNumericAttributeUnvisualized;

        var report = adhocConverter.convert(reportName).report;
        _.each(report.columns, function(column) {
            if (column.name === 'num_attr6') {
                expect(column.filter).toBe(undefined);
                expect(column.displayFilter).toBe(undefined);
            } else {
                expect(column.displayFilter).toBe(undefined);
                expect(column.filter).toBe(undefined);
            }
        });
        expect(report.columns.length).toBe(2);
        expect(report.displayFilters.length).toBe(0);
        expect(report.dataFilters.length).toBe(0);
    }));

});
