describe('popoverThreshold filter', function() {

    beforeEach(function() {
        module('visualizer');
    });

    it('filters based on character length and arguments', function() {
        inject(function($compile, $rootScope) {            
            var scope = $rootScope.$new();
            var template = '<div>'+
                '<div id="default-25">{{"1234567890123456789012345" | popoverThreshold}}</div>'+
                '<div id="default-26">{{"12345678901234567890123456" | popoverThreshold}}</div>'+
                '<div id="omit">{{"123456789012345678901234567890" | popoverThreshold:"omit"}}</div>'+
                '<div id="blank">{{"" | popoverThreshold}}</div>'+
                '<div id="null">{{null | popoverThreshold}}</div>'+
                '<div id="report-exp-16">{{"1234567890123456" | popoverThreshold:"report expression"}}</div>'+
                '<div id="report-exp-17">{{"12345678901234567" | popoverThreshold:"report expression"}}</div>'+
                '<div id="other-exp-16">{{"12345678901234567" | popoverThreshold:"other expression"}}</div>'+
                '<div id="other-exp-17">{{"12345678901234567" | popoverThreshold:"other expression"}}</div>'+
                '</div>';
            var partialDOM = angular.element(template);
            
            $compile(partialDOM)(scope);
            $rootScope.$digest();
            
            expect(partialDOM.find('#default-25').html().length).toBe(0);
            expect(partialDOM.find('#default-26').html().length).toBe(26);
            expect(partialDOM.find('#omit').html().length).toBe(0);
            expect(partialDOM.find('#blank').html().length).toBe(0);
            expect(partialDOM.find('#null').html().length).toBe(0);
            expect(partialDOM.find('#report-exp-16').html().length).toBe(0);
            expect(partialDOM.find('#report-exp-17').html().length).toBe(17);
            expect(partialDOM.find('#other-exp-16').html().length).toBe(0);
            expect(partialDOM.find('#other-exp-17').html().length).toBe(0);
        });
    });
});
