/* globals describe, beforeEach, it, runs, waitsFor, expect */

describe('CommandWebService', function() {

    var injector; //

    var sessionToken;

    var spaces;

    beforeEach(function() {
        injector = angular.injector(['ng']);
    });

    describe('dependencies:', function() {
        it('$http should be able to contact a remote site', function() {

            injector.invoke(function($http) {
                var returned;

                runs(function() {
                    var testUrl = 'http://www.birst.com'; // test if testUrl has correct protocol

                    expect($http).toBeDefined();

                    $http.get(testUrl).then(function() {
                        returned = true;
                    }, function() {
                        returned = false;
                    });
                });

                waitsFor(function() {
                    return returned !== undefined;
                }, 2500);

                runs(function() {
                    expect(returned).toBeDefined();
                });
            });
        });
    });

    describe('login', function() {
        it('returns a token when given correct credentials', function() {

            runs(function() {
                injector.invoke(function($http) {
                    var CWS = new CommandWebService({
                        httpService: $http
                    });
                    CWS.login('accountadmin@birst.com', 'birst@123', function(token) {
                        sessionToken = token;
                    }, function() {
                        sessionToken = false;
                    });
                });
            });

            waitsFor(function() {
                return sessionToken !== undefined;
            }, 'token to be returned', 15000);

            runs(function() {
                expect(sessionToken).toBeTruthy();
            });

        })
    });

    describe('listSpaces', function() {
        it('should return a list of spaces', function() {
            runs(function() {
                injector.invoke(function($http) {
                    var CWS = new CommandWebService({
                        httpService: $http
                    });
                    CWS.listSpaces(sessionToken, function(_spaces) {
                        spaces = _spaces;
                    });
                });
            });

            waitsFor(function() {
                return spaces;
            }, 'token to be returned', 15000);

            runs(function() {
                expect(spaces).toBeTruthy();
            });

        });

        it('should return the spaces in the proper format', function() {
            for (var i in spaces) {
                expect(spaces[i].name).toBeDefined();
                expect(spaces[i].owner).toBeDefined();
                expect(spaces[i].id).toBeDefined();
            }
        });
    });

});
