describe('Jasmine + Angular + $http', function() {

    var injector; 

    beforeEach(function() {
        injector = angular.injector(['ng']);
    });

    it('can be tested', function() {
        injector.invoke(function($http) {
	    var n = $http;

            var complete = false;
  	    
            runs(function() {
                $http({
		  url: 'http://test.dev/#',
		  method: 'GET'
		}).then(function(res, x, y) {
		    //console.log('res', res, x, y);
                    complete = true;
                }, function(err, x, y) {
		    //console.log('something went wrong', err, x, y);
		});
            });

            waitsFor(function() {
                return complete;
            }, 'query to complete', 15000);

            runs(function() {
                expect(complete).toEqual(true);
            });

        });

    });

});