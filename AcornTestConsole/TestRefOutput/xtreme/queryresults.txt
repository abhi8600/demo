Account Class	Sum: Account Balance
Inventory	3,656,185.37
Accounts Payable	3,127,663.26
Share Capital	2,200,000
Capital Asset	1,431,176.53
Operating Revenue	939,431.67
Bank	893,260.52
Long Term Debt.	559,467
Accounts Receivable	513,702.31
Cost of Goods Sold	436,010.62
Payroll Expense	256,923.07
Current Earnings	196,904.5
Retained Earnings	102,455.97
Sales Tax Payable	67,003.27
Payroll Tax Payable	64,230.77
Other Payable	61,661.54
General & Administrative Expense	22,500.41
Amortization/Depreciation Expense	16,722.48
Interest Expense	10,255.15
Bank Service Charges	115.44
Accumulated Amortization & Depreciation	-114,938.42

Account Type
Asset
Equity
Expense
Liability
Revenue

Account Name
Accounting & Legal
Accounts Payable
Accounts Receivable
Accumulated Amortization (Building)
Accumulated Amortization (Machinery)
Advertising & Promotions
Amortization Expense (Building)
Amortization Expense (Machinery)
Bank Charges
Bike Sales - Competition
Bike Sales - Hybrid
Bike Sales - Kids
Bike Sales - Mountain
Bikes (Competition) Cost
Bikes (Competition) Inventory
Bikes (Hybrid) Cost
Bikes (Hybrid) Inventory
Bikes (Kids) Cost
Bikes (Kids) Inventory
Bikes (Mountain) Cost
Bikes (Mountain) Inventory
Buildings
Chequing Bank Account
Common Shares
Courier & Postage
Current Earnings
Federal Income Tax Payable
Gloves Cost
Gloves Inventory
Helmets Cost
Helmets Inventory
Insurance
Interest Expense
Internet
Land
Locks Cost
Locks Inventory
Long Term Notes Payable
Machinery
Miscellaneous
Office Supplies
Preferred Shares
Property Taxes
Repair & Maintenance
Retained Earnings - Previous Year
Saddles Cost
Saddles Inventory
Sales Gloves
Sales Helmets
Sales Locks
Sales Returns
Sales Saddles
Sales Tax Payable
Telephone
Utilities
Vacation Payable
Wages & Salaries


Journal Entry Type
Bill Payment
Capital Asset Purchase
Invoice
Paycheque
Purchase Order
Purchase Payment
Receipt
Sales Return
amortization
capital purchase - initial purchase
capital purchase - machinery
capitalization
closing entries
notes payable

Debit Or Credit
Credit
Debit

F0_Account Type	F1_Sum: Amount
Expense	4,708,451.15
Revenue	7,359,940.27

F0_Account Name	F1_Sum: Amount	F2_Sum: Amount
Bikes (Competition) Cost	2,102,904.62	2
Bikes (Mountain) Cost	673,979.38	5
Sales Returns	126,151.58	10
Bike Sales - Hybrid	649,571.79	6
Bike Sales - Kids	130,881.67	9
Sales Helmets	135,568.82	8
Bikes (Hybrid) Cost	276,282.41	7
Wages & Salaries	1,230,154.06	3
Bike Sales - Mountain	1,125,130.44	4
Bike Sales - Competition	5,118,996.87	1

Account Type	Amount
Revenue	7,359,940.27
Expense	4,708,451.15

Account Name	Amount	Rank
Bike Sales - Competition	5,118,996.87	1
Bikes (Competition) Cost	2,102,904.62	2
Wages & Salaries	1,230,154.06	3
Bike Sales - Mountain	1,125,130.44	4
Bikes (Mountain) Cost	673,979.38	5
Bike Sales - Hybrid	649,571.79	6
Bikes (Hybrid) Cost	276,282.41	7
Sales Helmets	135,568.82	8
Bike Sales - Kids	130,881.67	9
Sales Returns	126,151.58	10

F0_Journal Entry Type	F1_Sum: Amount
amortization	229,876.84
Bill Payment	1,296,930
Capital Asset Purchase	2,862,353.05
capitalization	4,400,000
closing entries	14,519,214.84
Invoice	12,452,039.48
notes payable	262,641.68
Paycheque	3,083,076.84
Purchase Order	11,027,528.01
Purchase Payment	4,772,201.5
Receipt	7,709,477.6
Sales Return	206,563.8

F0_Debit Or Credit	F1_Sum: Amount
Credit	31,410,951.82
Debit	31,410,951.82

Journal Entry Type	Amount
closing entries	14,519,214.84
Invoice	12,452,039.48
Purchase Order	11,027,528.01
Receipt	7,709,477.6
Purchase Payment	4,772,201.5
capitalization	4,400,000
Paycheque	3,083,076.84
Capital Asset Purchase	2,862,353.05
Bill Payment	1,296,930
notes payable	262,641.68
amortization	229,876.84
Sales Return	206,563.8

Debit Or Credit	Amount
Debit	31,410,951.82
Credit	31,410,951.82

