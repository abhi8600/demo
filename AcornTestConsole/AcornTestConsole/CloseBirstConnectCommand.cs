﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class CloseBirstConnectCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string dcconfigname { get; set; }

        public CloseBirstConnectCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command closebirstconnect.");
            spacename = preProcessStringArgument(spacename);
            dcconfigname = preProcessStringArgument(dcconfigname);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                string key = spacename + "_" + dcconfigname;
                if (ATC.bcCmdLines.ContainsKey(key))
                {
                    ATC.bcCmdLines[key].CloseMainWindow();
                    ATC.bcCmdLines.Remove(key);
                    return true;
                }                
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("closebirstconnect");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/dcconfigname=").Append(dcconfigname);
            return command.ToString();
        }
    }
}
