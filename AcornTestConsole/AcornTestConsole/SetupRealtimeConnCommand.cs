﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class SetupRealtimeConnCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string connname { get; set; }
        public string servername { get; set; }
        public string database { get; set; }
        public string driver { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string port { get; set; }
        public string logfilepath { get; set; }
        public string dcconfigname { get; set; }
        public bool localetlconnection { get; set; }
        public string localetlappdirectory { get; set; }
        public string localetlloadgroup { get; set; }

        public SetupRealtimeConnCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command setuprealtimeconn.");
            spacename = preProcessStringArgument(spacename);
            if (connname == null)
                throw new Exception("connname argument not specified for command setuprealtimeconn.");
            connname = preProcessStringArgument(connname);
            if (servername == null)
                throw new Exception("servername argument not specified for command setuprealtimeconn.");
            servername = preProcessStringArgument(servername);
            if (connname == null)
                throw new Exception("connname argument not specified for command setuprealtimeconn.");
            connname = preProcessStringArgument(connname);
            if (database == null)
                throw new Exception("database argument not specified for command setuprealtimeconn.");
            database = preProcessStringArgument(database);
            if (driver == null)
                throw new Exception("driver argument not specified for command setuprealtimeconn.");
            driver = preProcessStringArgument(driver);
            if (username == null)
                throw new Exception("username argument not specified for command setuprealtimeconn.");
            username = preProcessStringArgument(username);
            if (password == null)
                throw new Exception("password argument not specified for command setuprealtimeconn.");
            password = preProcessStringArgument(password);
            if (port == null)
                throw new Exception("port argument not specified for command setuprealtimeconn.");
            port = preProcessStringArgument(port);
            if (logfilepath == null)
                throw new Exception("logfilepath argument not specified for command setuprealtimeconn.");
            logfilepath = preProcessStringArgument(logfilepath);
            dcconfigname = preProcessStringArgument(dcconfigname);
            if (localetlconnection)
            {
                if (localetlappdirectory == null)
                    throw new Exception("localetlappdirectory argument not specified for command setuprealtimeconn.");
                localetlappdirectory = preProcessStringArgument(localetlappdirectory);
                if (localetlloadgroup == null)
                    throw new Exception("localetlloadgroup argument not specified for command setuprealtimeconn.");
                localetlloadgroup = preProcessStringArgument(localetlloadgroup);
            }
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                setupdcconfig(sp);
                //Save connection settings to repository
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void setupdcconfig(Space sp)
        {
            StreamReader reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["DCConfigLocation"] + "\\dcconfig_template.xml");
            string dcconfig = reader.ReadToEnd();
            reader.Close();
            dcconfig = dcconfig.Replace("{CONN}", connname);
            dcconfig = dcconfig.Replace("{SERVER}", servername);
            dcconfig = dcconfig.Replace("{DBNAME}", database);
            dcconfig = dcconfig.Replace("{DBTYPE}", driver);
            dcconfig = dcconfig.Replace("{DRIVER}", getDriverString(driver));
            dcconfig = dcconfig.Replace("{USER}", username);
            dcconfig = dcconfig.Replace("{PASSWD}", MainAdminForm.encrypt(password, MainAdminForm.SecretPassword));
            dcconfig = dcconfig.Replace("{PORT}", port);
            dcconfig = dcconfig.Replace("{LOGFILE}", Directory.GetCurrentDirectory() + logfilepath);

            if (localetlconnection)
            {
                reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["DCConfigLocation"] + "\\dcconfig_localetl_template.xml");
                string leconfig = reader.ReadToEnd();
                reader.Close();
                leconfig = leconfig.Replace("{LOCALETLAPPDIRECTORY}", localetlappdirectory);
                leconfig = leconfig.Replace("{LOCALETLCONNECTION}", connname);
                leconfig = leconfig.Replace("{LOCALETLLOADGROUP}", localetlloadgroup);
                dcconfig = dcconfig.Replace("{LOCALETLCONFIG}", leconfig);
            }
            else
            {
                dcconfig = dcconfig.Replace("{LOCALETLCONFIG}", "");
            }

            string dcconfigfilename = "Default".Equals(dcconfigname) ? "dcconfig.xml" : dcconfigname + "_" + "dcconfig.xml";
            string filename = sp.Directory + "\\dcconfig" + "\\" + dcconfigfilename;
            if (File.Exists(filename))
                File.Delete(filename);
            StreamWriter sw = File.CreateText(filename);
            sw.Write(dcconfig);
            sw.Close();
        }

        private static string getDriverString(string drivertype)
        {
            if (drivertype.Equals("Microsoft SQL Server", StringComparison.InvariantCultureIgnoreCase))
                return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            else if (drivertype.Equals("MySQL", StringComparison.InvariantCultureIgnoreCase)
                    || drivertype.Equals("Infobright", StringComparison.InvariantCultureIgnoreCase))
                return "com.mysql.jdbc.Driver";
            else if (drivertype.Equals("Oracle 11g", StringComparison.InvariantCultureIgnoreCase))
                return "oracle.jdbc.OracleDriver";
            else if (drivertype.Equals("Sybase IQ", StringComparison.InvariantCultureIgnoreCase))
                return "com.sybase.jdbc4.jdbc.SybDriver";
            else if (drivertype.Equals("Oracle ODBC", StringComparison.InvariantCultureIgnoreCase)
                    || drivertype.Equals("MySQL ODBC", StringComparison.InvariantCultureIgnoreCase)
                    || drivertype.Equals("DB2 ODBC", StringComparison.InvariantCultureIgnoreCase)
                    || drivertype.Equals("Microsoft SQL Server ODBC", StringComparison.InvariantCultureIgnoreCase))
                return "sun.jdbc.odbc.JdbcOdbcDriver";
            else if (drivertype.Equals("Microsoft Analysis Services (XMLA)", StringComparison.InvariantCultureIgnoreCase)
                    || drivertype.Equals("Hyperion Essbase (XMLA)", StringComparison.InvariantCultureIgnoreCase)
                || drivertype.Equals("SAP BW (XMLA)", StringComparison.InvariantCultureIgnoreCase) 
                || drivertype.Equals("Pentaho (Mondrian) Analysis Services (XMLA)", StringComparison.InvariantCultureIgnoreCase))
                return "org.olap4j.driver.xmla.XmlaOlap4jDriver";
            else
                return null;
        }
        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setuprealtimeconn");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/connname=").Append(connname);
            command.Append(" ").Append("/servername=").Append(servername);
            command.Append(" ").Append("/database=").Append(database);
            command.Append(" ").Append("/driver=").Append(driver);
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/password=").Append(password);
            command.Append(" ").Append("/port=").Append(port);
            command.Append(" ").Append("/logfilepath=").Append(logfilepath);
            command.Append(" ").Append("/dcconfigname=").Append(dcconfigname);
            return command.ToString();
        }
    }
}
