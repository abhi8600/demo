﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using System.IO;

namespace AcornTestConsole
{
    class ListTablesCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string connname { get; set; }
        public string outputfilename { get; set; }
        public string resultdir { get; set; }

        public ListTablesCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputfilename = "importresults.txt";
            resultdir = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command listtables.");
            spacename = preProcessStringArgument(spacename);
            if (connname == null)
                throw new Exception("connname argument not specified for command listtables.");
            connname = preProcessStringArgument(connname);            
            resultdir = preProcessStringArgument(resultdir);
            outputfilename = preProcessStringArgument(outputfilename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                Performance_Optimizer_Administration.MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                object[][] tables = Acorn.Utils.LiveAccessUtil.getSchemaTables(null, ATC.u, maf, sp, connname, null);
                if (tables == null)
                    throw new Exception("Could not retrive tables from liveaccess connection:" + connname);
                if (!Directory.Exists(resultdir))
                    Directory.CreateDirectory(resultdir);
                string testFolder = resultdir + "\\" + sp.Name;
                if (!Directory.Exists(testFolder))
                    Directory.CreateDirectory(testFolder);
                StreamWriter sw = null;
                if (!File.Exists(testFolder + "\\" + outputfilename))
                    sw = File.CreateText(testFolder + "\\" + outputfilename);
                else
                    sw = File.AppendText(testFolder + "\\" + outputfilename);
                for (int i = 0; i < tables.Length; i++)
                {
                    bool first = true;
                    for (int j = 0; j < tables[i].Length; j++)
                    {
                        if (!first)
                            sw.Write("\t");
                        first = false;
                        sw.Write(tables[i][j].ToString());
                    }
                    sw.WriteLine("");
                }
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("listtables");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/connname=").Append(connname);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/resultdir=").Append(resultdir);
            return command.ToString();
        }
    }
}
