﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole
{
    class RunBirstConnectTaskCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string commandargs { get; set; }
        public string dcconfigname { get; set; }
        public int maxwaitseconds { get; set; }
        public string taskerrorsfile { get; set; }
        public string taskstatusfile { get; set; }
        private int secodswaited = 0;

        public RunBirstConnectTaskCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
            maxwaitseconds = 900;//default 15 minutes wait for load
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command runbirstconnecttask.");
            spacename = preProcessStringArgument(spacename);
            if (commandargs == null)
                throw new Exception("commandargs argument not specified for command runbirstconnecttask.");
            commandargs = preProcessStringArgument(commandargs);
            dcconfigname = preProcessStringArgument(dcconfigname);
            taskerrorsfile = preProcessStringArgument(taskerrorsfile);
            taskstatusfile = preProcessStringArgument(taskstatusfile);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);

                string jnlp = Util.generateJnlpForSpace(ATC.conn, ATC.u, sp, dcconfigname);
                string jnlpfilename = sp.Directory + "\\" + sp.Name + ".jnlp";
                if (File.Exists(jnlpfilename))
                    File.Delete(jnlpfilename);
                StreamWriter sw = File.CreateText(jnlpfilename);
                sw.Write(jnlp);
                sw.Close();

                string spDirectory = ATC.testresultsdir + "\\" + sp.Name;
                if (!Directory.Exists(spDirectory))
                    Directory.CreateDirectory(spDirectory);

                String cmdFile = ATC.tempfolder + "\\" + sp.Name + ".birstconnectcmd.bat";
                File.Delete(cmdFile);
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                sw = new StreamWriter(file);
                sw.Write("\"" + System.Environment.GetEnvironmentVariable("JAVA_HOME") + "\\bin\\java.exe\"");
                sw.Write(" -cp \"" + ATC.configLocation + "\\*;" + ATC.configLocation + "\\lib\\*;" + ATC.configLocation + "\\lib\\SAP\\*;" + ATC.configLocation + "\\lib\\JDBC\\*\"");
                sw.Write(" -Djnlp.file=\"" + jnlpfilename + "\"");
                if (taskerrorsfile != null)
                {
                    if (taskerrorsfile.StartsWith("."))
                        taskerrorsfile = Directory.GetCurrentDirectory() + taskerrorsfile.Substring(1);
                    sw.Write(" -Djnlp.responseLogFile=\"" + taskerrorsfile + "\"");
                }
                DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\QA");
                sw.Write(" -Djnlp.log4j.Override=\"" + di.FullName + "\\bin\\log4j.xml\"");
                string birstConnectLogsPath = sp.Directory + "\\BirstConnectTaskLogs";
                if (!Directory.Exists(birstConnectLogsPath))
                    Directory.CreateDirectory(birstConnectLogsPath);
                sw.Write(" -DBirstConnect.logs.path=\"" + (birstConnectLogsPath + "\\").Replace("\\", "\\\\") + "\"");
                sw.Write(" com.birst.dataconductor.DataConductorCommandLine ");
                sw.Write(commandargs);
                sw.Flush();
                sw.Close();

                ProcessStartInfo startInfo = new ProcessStartInfo(cmdFile);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.WorkingDirectory = Directory.GetCurrentDirectory() ;
                Process process;
                Console.Out.Write("Running command: " + startInfo.FileName + " ");
                Console.Out.WriteLine(startInfo.Arguments);
                process = Process.Start(startInfo);
                int i = 0;
                while (!process.WaitForExit(1000))
                {
                    if (i++ < maxwaitseconds)
                        Console.Out.Write(".");
                    else
                    {
                        Console.Error.WriteLine("Giving up on birstconnect commandline execution. Waited for:" + maxwaitseconds);
                        break;
                    }
                }
                Console.Out.WriteLine("");
                int exitCode  = process.ExitCode;
                if (commandargs.Contains("-checkstatus") && exitCode == 1)
                {
                    if (secodswaited > maxwaitseconds)
                    {
                        Console.Error.WriteLine("BirstConnect giving up on polling load status, waited for " + secodswaited + " seconds.");
                        throw new Exception("Error executing Birst Connect task:" + commandargs);
                    }
                    Console.Out.WriteLine("BirstConnect exit status for load:" + exitCode + " (Currently Processing)");
                    System.Threading.Thread.Sleep(5 * 1000);
                    secodswaited += 5;
                    return execute();
                }
                Console.Out.WriteLine("BirstConnect exit status:" + exitCode + " for task:" + commandargs);
                if (!process.HasExited)
                    process.WaitForExit();
                if (taskerrorsfile != null)
                {
                    string errorStr = File.ReadAllText(taskerrorsfile);
                    errorStr.Replace(sp.Directory + "\\data", ".");
                    File.WriteAllText(taskerrorsfile, errorStr);
                }
                if (taskstatusfile != null)
                {
                    StreamWriter writer = null;
                    if (!File.Exists(taskstatusfile))
                        writer = File.CreateText(taskstatusfile);
                    else
                        writer = File.AppendText(taskstatusfile);
                    writer.Write("BirstConnect exit status:" + exitCode + " for task:" + commandargs);
                    writer.Flush();
                    writer.Close();
                    return true;
                }
                else
                {
                    if (exitCode == 0)
                    {
                        Console.Out.WriteLine("Successfully executed Birst Connect task:" + commandargs);
                        return true;
                    }
                    else
                    {
                        Console.Error.WriteLine("BirstConnect exit status:" + exitCode);
                        throw new Exception("Error executing Birst Connect task:" + commandargs);
                    }
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("runbirstconnecttask");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/commandargs=").Append(commandargs);
            command.Append(" ").Append("/dcconfigname=").Append(dcconfigname);
            if (commandargs.Contains("-checkstatus"))
                command.Append(" ").Append("/maxwaitseconds=").Append(maxwaitseconds);
            if (taskerrorsfile != null)
                command.Append(" ").Append("/taskerrorsfile=").Append(taskerrorsfile);
            if (taskstatusfile != null)
                command.Append(" ").Append("/taskstatusfile=").Append(taskstatusfile);
            return command.ToString();
        }
    }
}
