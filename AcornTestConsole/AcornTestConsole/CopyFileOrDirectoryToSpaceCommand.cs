﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole
{
    class CopyFileOrDirectoryToSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string path { get; set; }//file or directory path
        public bool directory { get; set; }//path is directory
        public string topath { get; set; }//relative path from space directory

        public CopyFileOrDirectoryToSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command copyfileordirectorytospace.");
            spacename = preProcessStringArgument(spacename);
            if (path == null)
                throw new Exception("spacename argument not specified for command copyfileordirectorytospace.");
            path = preProcessStringArgument(path);
            topath = preProcessStringArgument(topath);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (topath == null || topath.Trim().Equals(""))
                    topath = sp.Directory;
                else
                {
                    topath = sp.Directory + "\\" + topath;
                    if (!Directory.Exists(topath))
                        Directory.CreateDirectory(topath);                    
                }
                if (directory)
                {
                    if (!Directory.Exists(path))
                    {
                        throw new Exception("Path \"" + path + "\" not found");
                    }
                    DirectoryInfo di = new DirectoryInfo(path);
                    if (!Directory.Exists(topath + "\\" + di.Name))
                        Directory.CreateDirectory(topath + "\\" + di.Name);
                    foreach (FileInfo fi in  di.GetFiles())
                    {
                        File.Copy(fi.FullName, topath + "\\" + di.Name + "\\" + fi.Name, true);
                    }
                }
                else
                {
                    if (!File.Exists(path))
                    {
                        throw new Exception("Path \"" + path + "\" not found");
                    }
                    FileInfo fi = new FileInfo(path);
                    File.Copy(path, topath + "\\" + fi.Name, true);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("copyfileordirectorytospace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/path=").Append(path);
            if (directory)
                command.Append(" ").Append("/directory");
            command.Append(" ").Append("/topath=").Append(topath);
            return command.ToString();
        }
    }
}
