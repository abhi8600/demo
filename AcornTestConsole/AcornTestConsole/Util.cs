﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Performance_Optimizer_Administration;
using System.Text.RegularExpressions;
using System.Data.Odbc;
using Acorn;
using System.IO;
using System.Data;
using Acorn.DBConnection;

namespace AcornTestConsole
{
    class Util
    {
       public static void buildColumnAndTypeMapForCustomAttribute(MainAdminForm maf, Dictionary<string, string> columnMap,
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap, string[] levelArray)
        {

            if (levelArray == null || levelArray.Length != 2)
            {
                throw new Exception("Invalid HierarchyLevel");
            }

            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                bool found = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == levelArray[0] && level[1] == levelArray[1])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    continue;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (Array.IndexOf<string>(sc.TargetTypes, levelArray[0]) >= 0)
                    {
                        string name = sc.Name;
                        if (!columnMap.ContainsKey(name))
                        {
                            columnMap.Add(name, name);
                            typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                        }
                    }
                }
            }
            if (maf.timeDefinition != null && maf.timeDefinition.Name == levelArray[0] && maf.dimensionTablesList != null)
            {
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if ((dt.InheritTable == null || dt.InheritTable.Length == 0) &&
                        dt.DimensionName != null && dt.Level != null && dt.DimensionName == levelArray[0] && dt.Level == levelArray[1])
                    {
                        addToColumnAndTypeMaps(maf, dt, columnMap, typeMap);
                    }
                }
            }
        }

        public static void addToColumnAndTypeMaps(MainAdminForm maf, DimensionTable dt, Dictionary<string, string> columnMap, Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap)
        {
            DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
            foreach (DataRowView dr in drv)
            {
                try
                {
                    string colName = (string)dr["ColumnName"];
                    int width = 20;
                    DataRow dcr = maf.dimensionColumnNamesView.FindRows(new string[] { dt.DimensionName, colName })[0].Row;
                    if ((bool)dcr["Key"])
                        continue;
                    string s = (string)dcr["ColWidth"];
                    if (s.Length > 0)
                        width = int.Parse(s);
                    columnMap.Add(colName, colName);
                    typeMap.Add(colName, new ExpressionParser.LogicalExpression.DataType((string)dcr["DataType"], width));
                }
                catch (Exception)
                {
                }
            }
        }

        public static MeasureTable findMeasureTableBasedOnGrains(MainAdminForm maf, string[][] grainArray)
        {
            MeasureTable mTable = null;
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                List<MeasureTableGrain> mtGrainList = maf.getGrainInfo(mt);
                if (mtGrainList != null)
                {
                    int j = 0;
                    for (; j < mtGrainList.Count; j++)
                    {
                        if (!(grainArray[j][0].Equals(mtGrainList[j].DimensionName) && grainArray[j][1].Equals(mtGrainList[j].DimensionLevel)))
                            break;                                             
                    }
                    if (j == mtGrainList.Count)
                    {
                        mTable = mt;
                        break;
                    }
                }
            }
            return mTable;
        }

        public static StagingTable findStagingTableBasedOnGrainForDiscoveryMode(MainAdminForm maf, string grainofsource)
        {
            StagingTable sTable = null;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.getDisplayName().Equals(grainofsource))
                    return st;
            }
            return sTable;
        }

        public static StagingTable findStagingTableBasedOnGrain(MainAdminForm maf, string[][] grains)
        {
            StagingTable sTable = null;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (grains.Length == st.Levels.Length)
                {
                    bool matched = false;
                    for (int j = 0; j < st.Levels.Length; j++)
                    {
                        bool found = true;
                        for (int k = 0; k < st.Levels[j].Length; k++)
                        {
                            if (!st.Levels[j][k].Equals(grains[j][k]))
                            {
                                found = false;
                                break;
                            }
                        }
                        if (found && j == st.Levels.Length - 1)
                        {
                            matched = true;
                            break;
                        }
                    }
                    if (matched)
                    {
                        sTable = st;
                        break;
                    }
                }
            }
            return sTable;
        }

        public static void addOrUpdateCustomAttributeForCustomAttribute(bool isNewAttribute, string attributename, MainAdminForm maf, string[] levelArray, ExpressionParser.LogicalExpression lep, LogicalExpression curle, Acorn.User u, Space sp)
        {
            if (isNewAttribute)
            {
                // Make sure attribute name is unique
                bool dupe = false;
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.Name == attributename && Array.IndexOf<string>(sc.TargetTypes, levelArray[0]) >= 0)
                        {
                            dupe = true;
                            break;
                        }
                    }
                }
                
                if (!dupe && maf.getLogicalExpressions() != null)
                    foreach (LogicalExpression le in maf.getLogicalExpressions())
                    {
                        if (le.Type == LogicalExpression.TYPE_DIMENSION && le.Levels[0][0] == levelArray[0] && le.Levels[0][1] == levelArray[1] && le.Name == attributename && (lep == null || !lep.Equals(le)))
                        {
                            dupe = true;
                            break;
                        }
                    }
                if (dupe)
                {
                    throw new Exception("Duplicate Attribute Name");
                }
                //Adding New Expression
                LogicalExpression[] leExpressions = maf.getLogicalExpressions();
                List<LogicalExpression> exlist = leExpressions == null ? null : new List<LogicalExpression>(leExpressions);
                if (exlist == null)
                {
                    exlist = new List<LogicalExpression>();
                    //maf.LogicalExpressions = exlist;
                }
                int index = 0;
                for (; index < exlist.Count; index++)
                {
                    if (exlist[index] == curle)
                        break;
                }
                if (index >= exlist.Count)
                {
                    maf.setRepositoryDirectory(sp.Directory);
                    maf.updateLogicalExpression(curle, u.Username, "Custom attribute");
                }
            }
            else//update existing attribute
            {
                LogicalExpression[] leExperssions = maf.getLogicalExpressions();
                List<LogicalExpression> exlist = new List<LogicalExpression>(leExperssions);
                LogicalExpression toUpdate = null;
                foreach (LogicalExpression le in exlist)
                {
                    if (le.Type == LogicalExpression.TYPE_DIMENSION)
                    {
                        if (attributename.Equals(le.Name))
                        {
                            toUpdate = le;
                            toUpdate = curle;
                            break;
                        }
                    }
                }
            }
        }

       
        public static void buildColumnAndTypeMapForCustomMetric(MainAdminForm maf, Dictionary<string, string> columnMap,
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap, StagingTable sTable, Acorn.Space sp, String grain)
        {

            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (grain == CustomCalculations.getGrainString(st.Levels))
                {
                    MeasureGrain mg = ApplicationBuilder.getMeasureGrain(st, maf);
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                        {
                            string name = sc.Name;
                            if (!columnMap.ContainsKey(name))
                            {
                                columnMap.Add(name, name);
                                typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                            }
                        }
                    }
                    if (Acorn.Util.GENERATE_INHERITED_MEASURE_TABLES)
                    {
                        // Get columns that result from combining facts
                        // First, get all staging tables at a higher grain
                        foreach (StagingTable st2 in maf.stagingTableMod.getStagingTables())
                        {
                            if (st == st2)
                                continue;
                            MeasureGrain mg2 = ApplicationBuilder.getMeasureGrain(st2, maf);
                            if (ManageSources.isTargeted(maf, st2) && ApplicationBuilder.isLowerGrain(maf, mg2.measureTableGrains, mg.measureTableGrains, maf.dependencies, sp.Automatic) != null)
                            {
                                foreach (StagingColumn sc in st2.Columns)
                                {
                                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                                    {
                                        string name = sc.Name;
                                        if (!columnMap.ContainsKey(name))
                                        {
                                            columnMap.Add(name, name);
                                            typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    /*
                     * Generate discovery source metrics
                     */
                    if (ApplicationBuilder.getDisplayName(st) == grain)
                    {
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                            {
                                string name = sc.Name;
                                if (!columnMap.ContainsKey(name))
                                {
                                    columnMap.Add(name, name);
                                    typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                }
                            }
                        }
                        if (st.ForeignKeys != null)
                        {
                            // Traverse up a one-to-many join
                            foreach (ForeignKey fk in st.ForeignKeys)
                            {
                                StagingTable fkst = maf.stagingTableMod.findTable(fk.Source);
                                if (fkst == null)
                                    continue;
                                foreach (StagingColumn sc in fkst.Columns)
                                {
                                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                                    {
                                        string name = sc.Name;
                                        if (!columnMap.ContainsKey(name))
                                        {
                                            columnMap.Add(name, name);
                                            typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
            }
        }

        public static bool checkIfDuplicateMetric(MainAdminForm maf, string metricname, string[][] grains, ExpressionParser.LogicalExpression lep)
        {
            bool dupe = false;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.Name == metricname && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                    {
                        dupe = true;
                        break;
                    }
                }
            }
            
            if (!dupe && maf.getLogicalExpressions() != null)
                foreach (LogicalExpression le in maf.getLogicalExpressions())
                {
                    if (le.Type == LogicalExpression.TYPE_MEASURE && le.Name == metricname && equalsArray(le.Levels, grains) && (lep == null || !lep.Equals(le)))
                    {
                        dupe = true;
                        break;
                    }
                }
            return dupe;
        }

        private static bool equalsArray(string[][] array1, string[][] array2)
        {
            if (array1 == null || array2 == null)
                return false;
            if (array1.Length != array2.Length)
                return false;
            foreach (string[] str1 in array1)
            {
                foreach (string[] str2 in array2)
                {
                    if (str1 == null || str2 == null)
                        return false;
                    if (str1.Length != str2.Length)
                        return false;
                    foreach (string str11 in str1)
                    {
                        foreach (string str22 in str2)
                        {
                            if (str11 == null || str22 == null)
                                return false;
                            if (!str11.Equals(str22))
                                return false;
                        }
                    }
                }
            }
            return true;
        }

        public static string generateJnlpForSpace(QueryConnection conn, Acorn.User u, Space sp, string dcconfigname)
        {
            string key = "AZ" + "RB" + "PZ" + "WN" + "Z4" + "WH" + "BH" + "C2" + "W9" + "2I";
            string REALM = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AuthenticationRealm"];

            StreamReader reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"] + "\\launch.jnlp");
            string jnlp = reader.ReadToEnd();
            reader.Close();
            string webDavUrl = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ATCWebDAVURL"];
            string protocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ATCWebDAVProtocol"];
            string dcconfigfilename = "Default".Equals(dcconfigname) ? "dcconfig.xml" : dcconfigname + "_" + "dcconfig.xml";
            jnlp = jnlp.Replace("{CODEBASE}", protocol + webDavUrl);
            jnlp = jnlp.Replace("{UNAME}", u.Username);
            jnlp = jnlp.Replace("{PWD}", MainAdminForm.encrypt(u.Password, key));
            jnlp = jnlp.Replace("{SPACEID}", sp.ID.ToString());
            jnlp = jnlp.Replace("{REALM}", REALM);
            jnlp = jnlp.Replace("{PROXY_UNAME}", "");
            jnlp = jnlp.Replace("{PROXY_PWD}", "");
            jnlp = jnlp.Replace("{DCCONFIG_FILE_NAME}", dcconfigfilename);
            jnlp = jnlp.Replace("{SPACENAME}", sp.Name);
            // See if the user has realtime
            string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            QueryConnection adminconn = ConnectionPool.getConnection();
            List<Product> plist = Database.getProducts(adminconn, mainSchema, u.ID);
            ConnectionPool.releaseConnection(adminconn);
            bool hasRealtime = false;
            bool hasSAPConnector = false;
            bool hasLocalETL = false;
            foreach (Product p in plist)
            {
                if (p.Type == Product.TYPE_REALTIME)
                {
                    hasRealtime = true;
                }
                else if (p.Type == Product.TYPE_SAP_CONNECTOR)
                {
                    hasSAPConnector = true;
                }
                else if (p.Type == Product.TYPE_LOCAL_ETL_LOADER)
                {
                    hasLocalETL = true;
                }
            }
            if (hasRealtime)
            {
                jnlp = jnlp.Replace("{REALTIME}", "\r\n        <property name=\"jnlp.realtime\" value=\"True\"/>");
            }
            else
            {
                jnlp = jnlp.Replace("{REALTIME}", "");
            }
            if (hasSAPConnector)
            {
                jnlp = jnlp.Replace("{SAPCONNECT}", "\r\n        <property name=\"jnlp.sapconnect\" value=\"True\"/>");
            }
            else
            {
                jnlp = jnlp.Replace("{SAPCONNECT}", "");
            }
            if (hasLocalETL)
            {
                jnlp = jnlp.Replace("{LOCALETL}", "\r\n        <property name=\"jnlp.localetl\" value=\"True\"/>");
                reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"] + "\\LocalEtlResources.txt");
                string localEtlResources = reader.ReadToEnd();
                reader.Close();
                jnlp = jnlp.Replace("{LOCALETLSOURCES}", localEtlResources);
            }
            else
            {
                jnlp = jnlp.Replace("{LOCALETL}", "");
                jnlp = jnlp.Replace("{LOCALETLSOURCES}", "");
            }
            return jnlp;
        }

        public static bool spaceContainsLocalSource(MainAdminForm maf)
        {
            bool containsLocalSource = false;
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.LoadGroups != null)
                {
                    foreach (string lg in st.LoadGroups)
                    {
                        if (lg.StartsWith(Acorn.Utils.LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                        {
                            containsLocalSource = true;
                            break;
                        }
                    }
                }
            }
            return containsLocalSource;
        }

        public static string[] getDCConfigNames(Space sp)
        {
            List<string> configNames = new List<string>();
            string configDirPath = sp.Directory + "\\" + Acorn.Util.DCCONFIG_DIR;
            if (Directory.Exists(configDirPath))
            {
                string[] files = Directory.GetFiles(configDirPath, "*dcconfig.xml");
                if (files != null)
                {
                    string filename = null;
                    int index = -1;
                    for (int i = 0; i < files.Length; i++)
                    {
                        filename = new FileInfo(files[i]).Name;
                        string visiblename = "Default";
                        index = filename.IndexOf("_" + Acorn.Util.DCCONFIG_FILE_NAME);
                        if (index > 0)
                        {
                            visiblename = filename.Substring(0, index);
                        }
                        configNames.Add(visiblename);
                    }
                }
            }
            return configNames.ToArray();
        }
    }
}
