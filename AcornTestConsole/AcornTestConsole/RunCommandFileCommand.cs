﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Diagnostics;

namespace AcornTestConsole
{
    class RunCommandFileCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string commandfile { get; set; }
        public string resultfile { get; set; }
        public string resultdir { get; set; }
        public int maxwaitseconds { get; set; }
        
        public RunCommandFileCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            resultfile = "queryresults.txt";
            resultdir = ATC.testresultsdir;
            maxwaitseconds = 1200;//default 20 minutes wait for
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command runcommandfile.");
            spacename = preProcessStringArgument(spacename);
            if (commandfile == null)
                throw new Exception("commandfile argument not specified for command runcommandfile.");
            commandfile = preProcessStringArgument(commandfile);
            resultfile = preProcessStringArgument(resultfile);
            if (resultdir == null)
                throw new Exception("resultdir argument not specified for command runcommandfile (nor testresultsdir specified for ATC).");
            resultdir = preProcessStringArgument(resultdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                addDataLocationVar(sp, "DATALOCATION");
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (!Directory.Exists(resultdir))
                    Directory.CreateDirectory(resultdir);
                string testFolder = resultdir + "\\" + sp.Name;
                if (!Directory.Exists(testFolder))
                    Directory.CreateDirectory(testFolder);

                String cmdFile = ATC.tempfolder +  "\\" + sp.Name + ".tempfile.smi";
                File.Delete(cmdFile);
                String repPath = sp.Directory + "\\repository_dev.xml";
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                // passing the updated allowed packges details
                List<PackageSpaceProperties> packageSpacesList = Acorn.Util.getImportedPackageSpaces(sp, maf);
                if (packageSpacesList != null && packageSpacesList.Count > 0)
                {
                    foreach (PackageSpaceProperties packageSpaceProperties in packageSpacesList)
                    {
                        string spaceProps = "packageID=" + packageSpaceProperties.PackageID + " packageName=" + packageSpaceProperties.PackageName +
                            " spaceID=" + packageSpaceProperties.SpaceID.ToString() + " spaceDirectory=" + packageSpaceProperties.SpaceDirectory + " spaceName=" + packageSpaceProperties.SpaceName;
                        sw.WriteLine("setProperties start packageSpaceProperties " + spaceProps + "  end");
                    }
                }
                sw.WriteLine("repository \"" + repPath + "\"");
                FileStream usercmdfile = new FileStream(commandfile, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(usercmdfile);
                String line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    foreach (string varName in ATC.variableDictionary.Keys)
                    {                      
                        if (line.Contains("V{" + varName + "}"))
                        {
                            line = line.Replace("V{" + varName + "}", ATC.variableDictionary[varName]);                     
                        }
                    }
                    sw.WriteLine(line);
                }
                sr.Close();
                usercmdfile.Close();
                sw.Close();
                file.Close();

                ProcessStartInfo startInfo = new ProcessStartInfo(System.Web.Configuration.WebConfigurationManager.AppSettings["RunQueryCommand"]);
                startInfo.Arguments = "-c \"" + cmdFile + "\"";
                startInfo.Arguments += " -logfile \"" + ATC.tempfolder + "\\runquerycmdoutput.log\" > \"" + testFolder + "\\" + resultfile + "\"";
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                Process process;
                Console.Out.Write("Running command: " + startInfo.FileName + " ");
                Console.Out.WriteLine(startInfo.Arguments);
                process = Process.Start(startInfo);
                int i = 0;
                while (!process.WaitForExit(5000))
                {
                    if (i++ < maxwaitseconds)
                        Console.Out.Write(".");
                    else
                    {
                        Console.Error.WriteLine("Giving up on commandfile execution. Waited for:" + maxwaitseconds);
                        Console.Error.WriteLine("Space:" + sp.Name + " CommandFile:" + commandfile);
                        break;
                    }
                }
                Console.Out.WriteLine("");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        void addDataLocationVar(Space sp, String name)
        {
            if (!ATC.variableDictionary.ContainsKey(name))
            {
                ATC.variableDictionary.Add(name, sp.Directory + "\\data");
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("runcommandfile");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/commandfile=").Append(commandfile);
            command.Append(" ").Append("/resultfile=").Append(resultfile);
            command.Append(" ").Append("/resultdir=").Append(resultdir);
            command.Append(" ").Append("/maxwaitseconds=").Append(maxwaitseconds);
            return command.ToString();
        }

    }
}
