﻿using System;
using System.Collections.Generic;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using System.Linq;
using System.Text;
using Acorn;
using System.Diagnostics;
using System.IO;

namespace AcornTestConsole
{
    class DeleteSampleCommand : ATC_Command
    {
        public string spacename { get; set; }       
        public int maxwaitseconds { get; set; }
        MainAdminForm maf;

        public DeleteSampleCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            maxwaitseconds = 1200;//default 20 minutes wait for
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command deletesamples.");
            spacename = preProcessStringArgument(spacename);           
        }

        override public Nullable<bool> execute()
        {
            try
            {              
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                maf = Acorn.Util.setSpace(null, sp, ATC.u);
                Acorn.Command co = new Acorn.Command(sp, ATC.u, new List<string>(), null, null, null, maf);
                co.processCommand("deletesamples");

                //Set last modified date for all staging tables to current so that next time data is processed, all staging tables would be reloaded.
                List<StagingTable> stList = maf.stagingTableMod.getStagingTables();
                if ((stList != null) && (stList.Count > 0))
                {
                    foreach (StagingTable st in stList)
                    {
                        st.LastModifiedDate = DateTime.UtcNow;
                    }
                }
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }


        override public string ToString()
        {
            StringBuilder command = new StringBuilder("deletesamples");
            command.Append(" ").Append("/spacename=").Append(spacename);            
            return command.ToString();
        }

    }
}
