﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class WaitCommand : ATC_Command
    {
        public int seconds { get; set; }

        public WaitCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (seconds == 0)
                throw new Exception("invalid seconds argument specified for command wait.");
        }

        override public Nullable<bool> execute()
        {
            try
            {
                System.Threading.Thread.Sleep(seconds * 1000);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("wait");
            command.Append(" ").Append("/seconds=").Append(seconds);
            return command.ToString();
        }
    }
}
