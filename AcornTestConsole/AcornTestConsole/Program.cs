﻿using System;
using System.Collections.Generic;
using System.Linq;
using Acorn;
using System.IO;
using System.Data.Odbc;
using System.Web.Configuration;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Test.CommandLineParsing;
using Acorn.DBConnection;
using System.Runtime.ExceptionServices;

namespace AcornTestConsole
{
    public class Program : Microsoft.Test.CommandLineParsing.Command
    {
        // commandline arguments of ATC
        public string commandfile { get; set; }
        public string testresultsdir { get; set; }
        public bool force { get; set; }
        public bool parseonly { get; set; }
        
        public string configLocation = null;
        public string tempfolder;
        public string logfile;
        public string logfolder;
        public static int MAX_TIME_FOR_QUERY = 300000; // give 5 minutes for the query to execute.
        public static int MAX_TIME_FOR_COMMAND_FILE = 1200000; // give 20 minutes for the command file to execute.
        static string testUser = "qauser@successmetricsinc.com";
        static string dbConnection = "Driver={SQL Native Client};Server=localhost;Database=WebApplication;Uid=smi;Pwd=smi";
        public string SCHEMA_NAME = null;
        public SpaceConfig sc = null;
        public Acorn.User u = null;
        public QueryConnection conn = null;
        public string DEFAULT_DATABASE_TYPE = "MSSQL";
        public string DatabaseType;
        public string PsTools_Path;
        public string MemDB_Machine_Name;
        public string MemDB_UserName;
        public string MemDB_Password;
        public string MemDB_Path;
        public int  MemDB_Process_ID = -1;
        public string ATC_Tomcat_Temp_Dir = null;
        public Dictionary<String, Process> bcCmdLines = new Dictionary<string, Process>();
        public string datetimeStr = null;
        public string dateStr = null;
        public int RepositoryBuilderVersion;
        public bool abortATC { get;set; }
        public Dictionary<String, String> variableDictionary = new Dictionary<string, string>();
        void initConfigs()
        {
            //to be set by the batch file (runacorntestconsole.bat), only to be enabled when running in debug mode
            string smi_home = System.Environment.GetEnvironmentVariable("SMI_HOME");
            if (smi_home == null || smi_home.Equals(""))
                System.Environment.SetEnvironmentVariable("SMI_HOME", "C:\\SMIPerfEngine\\Main");
            string install_dir = System.Environment.GetEnvironmentVariable("INSTALL_DIR");
            if (install_dir == null || install_dir.Equals(""))
                System.Environment.SetEnvironmentVariable("INSTALL_DIR", "C:\\SMIPerfEngine\\Main");
            
            // Get the current configuration file.
            configLocation = System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"];
            if (configLocation == null || configLocation.Trim().Equals(""))
                configLocation = "c:\\workspace\\Acorn\\Acorn";
            string tUser = System.Configuration.ConfigurationManager.AppSettings["TestUser"];
            if (tUser != null && !tUser.Trim().Equals(""))
                testUser = tUser;
            
            Console.WriteLine("-------------- Initializing the configuration --------------");
            Console.WriteLine("Using Configuration files from location:" + configLocation);
            Console.WriteLine("Using User account:" + testUser);

            System.Configuration.ExeConfigurationFileMap fmap = new ExeConfigurationFileMap();
            fmap.ExeConfigFilename = configLocation + "\\web.config";
            
            // Load the web configuration from acorn project into here in a round-about mannner.
            // Currently this loads only two sections i.e: appSettings and connectionStrings 
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fmap, ConfigurationUserLevel.None);
            AppSettingsSection settings = (AppSettingsSection)config.GetSection("appSettings");

            // Use connection string from web.config
            ConnectionStringSettings connSettings = config.ConnectionStrings.ConnectionStrings["DatabaseConnection"];
            if (connSettings != null)
            {
                dbConnection = connSettings.ConnectionString;
                settings.Settings.Add("AdminConnectionString", connSettings.ConnectionString);//add ConnectionString in AppSettingsSection
            }
            
            foreach (String key in settings.Settings.AllKeys)
            {
                System.Web.Configuration.WebConfigurationManager.AppSettings[key] = settings.Settings[key].Value;
            }

            DatabaseType = DEFAULT_DATABASE_TYPE;//MSSQL
            string dbtype = System.Web.Configuration.WebConfigurationManager.AppSettings["ATC_DatabaseType"];
            if (dbtype != null && dbtype.Trim().Length != 0)
                DatabaseType = dbtype;
            string tomcat_temp_dir = System.Web.Configuration.WebConfigurationManager.AppSettings["ATC_TOMCAT_TEMP"];
            if (tomcat_temp_dir != null && !tomcat_temp_dir.Equals(""))
                ATC_Tomcat_Temp_Dir = tomcat_temp_dir;
            string birstWebService_BaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BirstWebService_BaseURL"];
            if (birstWebService_BaseURL != null && !birstWebService_BaseURL.Equals(""))
                BirstWebServices.BirstWebServiceBase.URL = birstWebService_BaseURL + "/CommandWebService.asmx";
            PsTools_Path = System.Web.Configuration.WebConfigurationManager.AppSettings["PSTOOLS_PATH"];
            MemDB_Machine_Name = System.Web.Configuration.WebConfigurationManager.AppSettings["MemDB_Machine_Name"];
            MemDB_UserName = System.Web.Configuration.WebConfigurationManager.AppSettings["MemDB_UserName"];
            MemDB_Password = System.Web.Configuration.WebConfigurationManager.AppSettings["MemDB_Password"];
            MemDB_Path = System.Web.Configuration.WebConfigurationManager.AppSettings["MemDB_Path"];
            string builderVersion = System.Web.Configuration.WebConfigurationManager.AppSettings["RepositoryBuilderVersion"];
            if (builderVersion != null && builderVersion.Length > 0)
            {
                RepositoryBuilderVersion = Int32.Parse(builderVersion);
            }
            tempfolder = Directory.GetCurrentDirectory() + "\\QA\\temp";
            if (!Directory.Exists(tempfolder))
                Directory.CreateDirectory(tempfolder);
            logfolder = Directory.GetCurrentDirectory() + "\\QA\\logs";
            if (!Directory.Exists(logfolder))
                Directory.CreateDirectory(logfolder);
            logfile = Directory.GetCurrentDirectory() + "\\QA\\logs\\birst.ATC." + System.Environment.GetEnvironmentVariable("COMPUTERNAME");
            SCHEMA_NAME = System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            Acorn.Util.loadSpaceParameters(Acorn.ConnectionPool.getConnection(), SCHEMA_NAME, configLocation);
            Space.NEW_SPACE_TYPE = Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
            sc = Acorn.Util.getSpaceConfiguration(Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]));
            Space.NEW_SPACE_PROCESSVERSION_ID = Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["ProcessingEngineForNewSpace"]);
            Console.WriteLine("Using Connection String:" + connSettings);
            Console.WriteLine("Using data directory (from web.config):" + System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"]);
            Console.WriteLine("Data directory from spaces.config:" + sc.DatabaseLoadDir);
            Console.WriteLine("Connection info for the spaces:" + sc.DatabaseConnectString);
            Console.WriteLine("Connection user for the spaces:" + sc.AdminUser);
            Console.WriteLine("Command for running the engine/Loadwarehouse (from web.config):" + System.Web.Configuration.WebConfigurationManager.AppSettings["LoadWarehouseCommand"]);
            Console.WriteLine("Command for running the query (from web.config):" + System.Web.Configuration.WebConfigurationManager.AppSettings["RunQueryCommand"]);
            Console.WriteLine("Command for Creating time dimension (from web.config):" + System.Web.Configuration.WebConfigurationManager.AppSettings["CreateTimeCommand"]);
            Console.WriteLine("Command for deleting the published data (from web.config):" + System.Web.Configuration.WebConfigurationManager.AppSettings["DeleteDataCommand"]);
            Console.WriteLine("--------------End of configuration info--------------");
            Console.WriteLine("#### Environment info: These are important for SMI engine commands to run####");
            Console.WriteLine("JAVA_HOME="+System.Environment.GetEnvironmentVariable("JAVA_HOME"));
            Console.WriteLine("SMI_HOME=" + System.Environment.GetEnvironmentVariable("SMI_HOME"));
            Console.WriteLine("INSTALL_DIR=" + System.Environment.GetEnvironmentVariable("INSTALL_DIR"));
            Console.WriteLine("TEMP Folder=" + tempfolder);
            Console.WriteLine("ATC_DatabaseType=" + System.Web.Configuration.WebConfigurationManager.AppSettings["ATC_DatabaseType"]);
            if (ATC_Tomcat_Temp_Dir != null)
                Console.WriteLine("ATC_TOMCAT_TEMP=" + ATC_Tomcat_Temp_Dir);
            if (birstWebService_BaseURL != null)
                Console.WriteLine("BirstWebService_BaseURL=" + birstWebService_BaseURL);
            if (PsTools_Path != null)
                Console.WriteLine("PsTools_Path=" + PsTools_Path);
            if (MemDB_Machine_Name != null)
                Console.WriteLine("MemDB_Machine_Name=" + MemDB_Machine_Name);
            if (MemDB_UserName != null)
                Console.WriteLine("MemDB_UserName=" + MemDB_UserName);
            if (MemDB_Password != null)
                Console.WriteLine("MemDB_Password=" + MemDB_Password);
            if (MemDB_Path != null)
                Console.WriteLine("MemDB_Path=" + MemDB_Path);
            if (RepositoryBuilderVersion > 0)
                Console.WriteLine("RepositoryBuilderVersion=" + RepositoryBuilderVersion);
            Console.WriteLine("----------------------------");
        }

        /*  This program reads the configuration defined for acorn project, they are: web.config and spaces.config
         *  Location from where these files are read can be configured using the config file which is specific to this project (AcornTestConsole.exe.config)
         *  
         *  This program will take the following parameters
         *    /commandfile=<commandfile> (absolute path is what is tested as of now)
         *    /testresultsdir=<testresultsdir> (optional) location of the directory where the test results (if any) should be put.
         *    /force (optional) if supplied, the prompting for deleting the test results directory to be switched off. i.e: directory will be  sliently cleared.
         *    /parseonly (optional) if supplied, the commandfile will only be parsed and any errors will be reported. The commands will not be executed.
         */
        [HandleProcessCorruptedStateExceptions]
        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            try
            {
                Program p = new Program();
                p.ParseArguments(args.ToList());
                p.Execute();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("ATC Execution Exception, ATC now aborting.\n" + e.Message + "\n" + e.StackTrace);
            }
        }

        public static void CurrentDomain_UnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Console.Error.WriteLine("Unhandled domain Exception \n" + (e.ExceptionObject as Exception).Message +"\n" + (e.ExceptionObject as Exception).StackTrace);
        }

        public void configureDefaultLogger()
        {
            log4net.Appender.ConsoleAppender appender = new log4net.Appender.ConsoleAppender();
            appender.Name = "AcornConsoleAppender";
            appender.Layout = new log4net.Layout.PatternLayout("%d %-5p - %m%n");
            log4net.Config.BasicConfigurator.Configure(appender);
        }

        public void configureFileLogger()
        {
            if (logfile != null)
            {
                log4net.Appender.RollingFileAppender appender = new log4net.Appender.RollingFileAppender();
                appender.File = logfile;
                appender.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Date;
                appender.DatePattern = @".yyyy-MM-dd.lo\g";
                appender.StaticLogFileName = false;
                appender.AppendToFile = true;
                appender.Layout = new log4net.Layout.PatternLayout("%d %-5p - %m%n");
                appender.ActivateOptions();
                log4net.Config.BasicConfigurator.Configure(appender);

                log4net.Appender.IAppender[] appenders = log4net.LogManager.GetRepository().GetAppenders();
                for (int i = 0; i < appenders.Length; i++)
                {
                    if (appenders[i] is log4net.Appender.ConsoleAppender)
                    {
                        log4net.Appender.ConsoleAppender cAppender = (log4net.Appender.ConsoleAppender)appenders[i];
                        cAppender.Threshold = log4net.Core.Level.Off;
                    }
                }
            }
        }

        override public void Execute()
        {
            if (commandfile == null)
            {
                displayHelp(); // displays usage
                return;
            }

            initConfigs(); // initializes ATC.exe.config and Web.config and spaces.config
            configureDefaultLogger();
            configureFileLogger();
            conn = new QueryConnection(dbConnection);
            try
            {
                conn.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not open connection:" + e.Message);
                return;
            }
            u = Acorn.Database.getUserByEmail(conn, SCHEMA_NAME, testUser);
            if (u == null)
            {
                Console.Error.WriteLine("Did not find the specified user:" + testUser);
                return;
            }
            if (testresultsdir != null)
            {
                if (force)
                {
                    Console.Out.WriteLine("Deleting the testResults folder (" + testresultsdir + ") and cleaning up all it's content");
                }
                else
                {
                    Console.Out.WriteLine("About to delete the testResults folder (" + testresultsdir + ") and cleaning up all it's content");
                    Console.Out.WriteLine("Press <enter> to continue, <ctrl+c> to break...");
                    Console.In.ReadLine();
                }
                // clear the test results directory
                if (Directory.Exists(testresultsdir))
                    Directory.Delete(testresultsdir, true);
                Directory.CreateDirectory(testresultsdir);
            }

            if (DatabaseType != DEFAULT_DATABASE_TYPE)
            {
                int index = commandfile.LastIndexOf(".");
                string DBcommandfile = commandfile.Substring(0, index) + "_" + DatabaseType + commandfile.Substring(index);
                if (File.Exists(DBcommandfile))
                    commandfile = DBcommandfile;                
            }
            StreamReader sr = new StreamReader(new FileStream(commandfile, FileMode.Open, FileAccess.Read));
            string line;
            int lineCount = 0;
            DateTime dt = DateTime.Now;
            datetimeStr = dt.ToString("yyyyMMdd_HHmmss");
            dateStr = dt.ToString("d-MMM-yyyy");
            Acorn.Background.BackgroundProcess.StartBackgroundProcess();
            List<ATC_Command> commands = new List<ATC_Command>();
            bool failedToParse = false;
            bool ignoreCommand = false;
            while ((line = sr.ReadLine()) != null)
            {
                lineCount++;
                if (line.StartsWith("#") || line.StartsWith("//") || line.Trim().Equals(""))
                    continue;
                string[] delimStr = new string[] { " /" };
                string[] cargs = line.Split(delimStr, StringSplitOptions.None);
                if (cargs.Length == 0)
                    continue;
                try
                {
                    ATC_Command command = parseCommand(cargs.ToList());
                    if (!parseonly && command is ExecuteStartCommand)
                    {
                        if (!((ExecuteStartCommand)command).dbtype.Equals(DatabaseType))
                        {
                            Console.Out.WriteLine("ExecuteStart DB specific commands for dbtype = " + ((ExecuteStartCommand)command).dbtype + " being ingored");
                            ignoreCommand = true;
                        }                        
                        continue;
                    }
                    if (!parseonly && command is ExecuteFinishCommand)
                    {
                        if (ignoreCommand && !((ExecuteFinishCommand)command).dbtype.Equals(DatabaseType))
                        {
                            ignoreCommand = false;
                            Console.Out.WriteLine("ExecuteFinish DB specific commands for dbtype = " + ((ExecuteFinishCommand)command).dbtype + " completed");
                        }
                        continue;
                    }
                    if (!parseonly && command is IgnoreStartCommand)
                    {
                        if (((IgnoreStartCommand)command).dbtype.Equals(DatabaseType))
                        {
                            Console.Out.WriteLine("Starting ignoring of DB specific commands for dbtype = " + ((IgnoreStartCommand)command).dbtype);
                            ignoreCommand = true;
                        }
                        continue;
                    }
                    if (!parseonly && command is IgnoreFinishCommand)
                    {
                        if (ignoreCommand && ((IgnoreFinishCommand)command).dbtype.Equals(DatabaseType))
                        {
                            ignoreCommand = false;
                            Console.Out.WriteLine("Completed ignoring of DB specific commands for dbtype = " + ((IgnoreFinishCommand)command).dbtype);
                        }
                        continue;
                    }
                    if (ignoreCommand)
                    {
                        Console.Out.WriteLine("ATC Ignoring Command: " + command.ToString());
                        continue;
                    }
                    commands.Add(command);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Error in Command : [" + line + "] at line #" + lineCount +
                        " of commandfile \"" + commandfile + "\" - " + e.Message);
                    failedToParse = true;
                    break;
                }
            }
            sr.Close();

            if (!parseonly && !failedToParse) // execute all commands only if commandfile is successfully parsed.
            {
                foreach (ATC_Command command in commands)
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    if (abortATC && !(command is CompareResultsCommand))
                    {
                        Console.Error.WriteLine("ATC command execution aborted, skipping command: " + command.ToString());
                        continue;
                    }
                    Nullable<bool> result = command.execute();
                    if (result.HasValue)
                    {
                        if (result.Value)
                            Console.Out.WriteLine("Successfully Completed Command : " + command.ToString());
                        else
                            Console.Error.WriteLine("Error Executing Command : " + command.ToString());
                    }
                }
            }
            else if (parseonly)
            {
                if (!failedToParse)
                    Console.Out.WriteLine("Successfully Parsed CommandFile : " + commandfile);
                else
                    Console.Error.WriteLine("Error Parsing CommandFile : " + commandfile);
            }
            
            conn.Close();
            
            foreach (Process bc in bcCmdLines.Values)
            {
                bc.CloseMainWindow();                    
            }
            if (BirstWebServices.BirstWebServiceBase.Token != null)//make sure to logout from webservices
            {
                ATC_Command command = new BirstWebServices.LogoutCommand(this);
                Nullable<bool> result = command.execute();
                if (result.HasValue)
                {
                    if (result.Value)
                        Console.Out.WriteLine("Successfully Completed Command : " + command.ToString());
                    else
                        Console.Error.WriteLine("Error Executing Command : " + command.ToString());
                }
            }
            
            Environment.Exit(0);
        }

        private string[] extractArguments(List<string> args)
        {
            string[] arguments = new string[args.Count - 1];
            for (int i = 1; i < args.Count; i++)
            {
                arguments[i - 1] = "/" + args[i].Trim();
            }
            return arguments;
        }

        ///<summary>
        ///Parses command to generate an ATC command and sets up its arguments. Throws exception if failed to generate command.
        ///</summary>
        private ATC_Command parseCommand(List<String> commandArgs)
        {
            ATC_Command command = null;
            string commandname = commandArgs[0].ToLower().Trim();
            switch (commandname)
            {
                case "executestart": command = new ExecuteStartCommand(this); break;
                case "executefinish": command = new ExecuteFinishCommand(this); break;
                case "ignorestart": command = new IgnoreStartCommand(this); break;
                case "ignorefinish": command = new IgnoreFinishCommand(this); break;
                case "createspace": command = new CreateSpaceCommand(this); break;
                case "deletespace": command = new DeleteSpaceCommand(this); break;
                case "upload": command = new UploadCommand(this); break;
                case "applyrepository": command = new ApplyRepositoryCommand(this); break;
                case "applydcconfig": command = new ApplyDCConfigCommand(this); break;
                case "publish": command = new PublishCommand(this); break;
                case "copydatafile": command = new CopyDataFileCommand(this); break;
                case "copycatalogtospace": command = new CopyCatalogToSpaceCommand(this); break;
                case "runquery": command = new RunQueryCommand(this); break;
                case "runcommandfile": command = new RunCommandFileCommand(this); break;
                case "copyrepo": command = new CopyRepoCommand(this); break;
                case "overwriterepo": command = new OverwriteRepoCommand(this); break;
                case "overwritedcconfig": command = new OverwriteDCConfigCommand(this); break;
                case "compareresults": command = new CompareResultsCommand(this); break;
                case "removepublished": command = new RemovePublishedCommand(this); break;
                case "setuprealtimeconn": command = new SetupRealtimeConnCommand(this); break;
                case "launchbirstconnect": command = new LaunchBirstConnectCommand(this); break;
                case "closebirstconnect": command = new CloseBirstConnectCommand(this); break;
                case "writetabletofile": command = new WriteTableToFileCommand(this); break;
                case "applygasettings": command = new ApplyGASettingsCommand(this); break;
                case "addcustomattribute": command = new AddCustomAttributeCommand(this); break;
                case "updatecustomattribute": command = new UpdateCustomAttributeCommand(this); break;
                case "removecustomattribute": command = new RemoveCustomAttributeCommand(this); break;
                case "addcustommetric": command = new AddCustomMetricCommand(this); break;
                case "updatecustommetric": command = new UpdateCustomMetricCommand(this); break;
                case "removecustommetric": command = new RemoveCustomMetricCommand(this); break;
                case "importcube": command = new ImportCubeCommand(this); break;
                case "listtables": command = new ListTablesCommand(this); break;
                case "importtableschema": command = new ImportTableSchemaCommand(this); break;
                case "writesourcemetadata": command = new WriteSourceMetaDataCommand(this); break;
                case "exportreporttohtml": command = new ExportReportToHtmlCommand(this); break;
                case "movefiles": command = new MoveFilesCommand(this); break;
                case "changequerylanguage": command = new ChangeQueryLanguageCommand(this); break;
                case "exportdashboardstoxml": command = new ExportDashboardsToXmlCommand(this); break;
                case "setuser": command = new SetUserCommand(this); break;
                case "setupbirstconnecttasks": command = new SetupBirstConnectTasksCommand(this); break;
                case "runbirstconnecttask": command = new RunBirstConnectTaskCommand(this); break;
                case "wait": command = new WaitCommand(this); break;
                case "setspace": command = new SetSpaceCommand(this); break;
                case "startmemdbnowait": command = new StartMemDBNoWaitCommand(this); break;
                case "startmemdbwithwait": command = new StartMemDBWithWaitCommand(this); break;
                case "stopmemdbinstancewithwait": command = new StopMemDBInstanceWithWaitCommand(this); break;
                case "stopmemdbnowait": command = new StopMemDBNoWaitCommand(this); break;
                case "logtime": command = new LogTimeCommand(this); break;
                case "importpackage": command = new ImportPackageCommand(this); break;
                case "createpackage": command = new CreatePackageCommand(this); break;
                case "deletepackage": command = new DeletePackageCommand(this); break;
                case "copyfileordirectorytospace": command = new CopyFileOrDirectoryToSpaceCommand(this); break;
                case "setvariable": command = new SetVariableCommand(this); break;
                case "setrepositoryattribute": command = new SetRepositoryAttributeCommand(this); break;
                case "importtable": command = new ImportTableCommand(this); break;
                case "movespace": command = new MoveSpace(this); break;               
                case "deletesamples": command = new DeleteSampleCommand(this); break;
                case "executedml": command = new ExecuteDMLCommand(this); break;
                
                case "birstwebservices.copyspace": command = new BirstWebServices.CopySpaceCommand(this); break;
                case "birstwebservices.login": command = new BirstWebServices.LoginCommand(this); break;
                case "birstwebservices.logout": command = new BirstWebServices.LogoutCommand(this); break;
                case "birstwebservices.getvariablesforspace": command = new BirstWebServices.GetVariablesForSpaceCommand(this); break;
                case "birstwebservices.executequeryinspace": command = new BirstWebServices.ExecuteQueryInSpaceCommand(this); break;
                case "birstwebservices.swapspacecontents": command = new BirstWebServices.SwapSpaceContentsCommand(this); break;
                case "birstwebservices.addusertospace": command = new BirstWebServices.AddUserToSpaceCommand(this); break;
                case "birstwebservices.addgrouptospace": command = new BirstWebServices.AddGroupToSpaceCommand(this); break;
                case "birstwebservices.addusertogroupinspace": command = new BirstWebServices.AddUserToGroupInSpaceCommand(this); break;
                case "birstwebservices.listusersinspace": command = new BirstWebServices.ListUsersInSpaceCommand(this); break;
                case "birstwebservices.listgroupaclsinspace": command = new BirstWebServices.ListGroupAclsInSpaceCommand(this); break;
                case "birstwebservices.clearcacheinspace": command = new BirstWebServices.ClearCacheInSpaceCommand(this); break;
                case "birstwebservices.begindataupload": command = new BirstWebServices.BeginDataUploadCommand(this); break;
                case "birstwebservices.uploaddata": command = new BirstWebServices.UploadDataCommand(this); break;
                case "birstwebservices.finishdataupload": command = new BirstWebServices.FinishDataUploadCommand(this); break;
                case "birstwebservices.getdatauploadstatus": command = new BirstWebServices.GetDataUploadStatusCommand(this); break;
                case "birstwebservices.publishdata": command = new BirstWebServices.PublishDataCommand(this); break;
                case "birstwebservices.getpublishingstatus": command = new BirstWebServices.GetPublishingStatusCommand(this); break;
                case "birstwebservices.setvariableinspace": command = new BirstWebServices.SetVariableInSpaceCommand(this); break;
                case "birstwebservices.resetpassword": command = new BirstWebServices.ResetPasswordCommand(this); break;
                case "birstwebservices.removeuserfromspace": command = new BirstWebServices.RemoveUserFromSpaceCommand(this); break;
                case "birstwebservices.removeuserfromgroupinspace": command = new BirstWebServices.RemoveUserFromGroupInSpaceCommand(this); break;
                case "birstwebservices.addacltogroupinspace": command = new BirstWebServices.AddAclToGroupInSpaceCommand(this); break;
                case "birstwebservices.addproxyuser": command = new BirstWebServices.AddProxyUserCommand(this); break;
                case "birstwebservices.copyspacecontents": command = new BirstWebServices.CopySpaceContentsCommand(this); break;
                case "birstwebservices.listgroupsinspace": command = new BirstWebServices.ListGroupsInSpaceCommand(this); break;
                case "birstwebservices.listusersingroupinspace": command = new BirstWebServices.ListUsersInGroupInSpaceCommand(this); break;
                case "birstwebservices.removegroupfromspace": command = new BirstWebServices.RemoveGroupFromSpaceCommand(this); break;
                case "birstwebservices.removeaclfromgroupinspace": command = new BirstWebServices.RemoveAclFromGroupInSpaceCommand(this); break;
                case "birstwebservices.listcreatedusers": command = new BirstWebServices.ListCreatedUsersCommand(this); break;
                case "birstwebservices.adduser": command = new BirstWebServices.AddUserCommand(this); break;
                case "birstwebservices.listproxyusers": command = new BirstWebServices.ListProxyUsersCommand(this); break;
                case "birstwebservices.removeproxyuser": command = new BirstWebServices.RemoveProxyUserCommand(this); break;
                case "birstwebservices.copycatalogdirectory": command = new BirstWebServices.CopyCatalogDirectoryCommand(this); break;
                case "birstwebservices.getdirectorycontents": command = new BirstWebServices.GetDirectoryContentsCommand(this); break;
                case "birstwebservices.getdirectorypermissions": command = new BirstWebServices.GetDirectoryPermissionsCommand(this); break;
                case "birstwebservices.setdirectorypermission": command = new BirstWebServices.SetDirectoryPermissionCommand(this); break;
                case "birstwebservices.addallowedip": command = new BirstWebServices.AddAllowedIpCommand(this); break;
                case "birstwebservices.listallowedips": command = new BirstWebServices.ListAllowedIpsCommand(this); break;
                case "birstwebservices.removeallowedip": command = new BirstWebServices.RemoveAllowedIpCommand(this); break;
                case "birstwebservices.copyfileordirectory": command = new BirstWebServices.CopyFileOrDirectoryCommand(this); break;
                case "birstwebservices.deletefileordirectory": command = new BirstWebServices.DeleteFileOrDirectoryCommand(this); break;
                case "birstwebservices.renamefileordirectory": command = new BirstWebServices.RenameFileOrDirectoryCommand(this); break;
                case "birstwebservices.createnewdirectory": command = new BirstWebServices.CreateNewDirectoryCommand(this); break;
                case "birstwebservices.createnewspace": command = new BirstWebServices.CreateNewSpaceCommand(this); break;
                case "birstwebservices.deletespace": command = new BirstWebServices.DeleteSpaceCommand(this); break;
                case "birstwebservices.copycustomsubjectarea": command = new BirstWebServices.CopyCustomSubjectAreaCommand(this); break;
                case "birstwebservices.listcustomsubjectareas": command = new BirstWebServices.ListCustomSubjectAreasCommand(this); break;
                case "birstwebservices.listspaces": command = new BirstWebServices.ListSpacesCommand(this); break;
                case "birstwebservices.deletelastdatafromspace": command = new BirstWebServices.DeleteLastDataFromSpaceCommand(this); break;
                case "birstwebservices.deletealldatafromspace": command = new BirstWebServices.DeleteAllDataFromSpaceCommand(this); break;
                case "birstwebservices.setuprealtimeconnectionforspace": command = new BirstWebServices.SetUpRealTimeConnectionForSpaceCommand(this); break;
                case "birstwebservices.setupgenericjdbcrealtimeconnectionforspace": command = new BirstWebServices.SetUpGenericJDBCRealTimeConnectionForSpaceCommand(this); break;
                case "birstwebservices.importcubemetadataintospace": command = new BirstWebServices.ImportCubeMetaDataIntoSpaceCommand(this); break;
            }
            if (command == null)
                throw new Exception("Unrecognized ATC Command : " + commandname);
            
            // initialize default values for optional parameters
            command.setDefaultArguments();
            
            // initialize/override user specified values for parameters
            // throws an exception if a mandatory (string) parameter is missing or could not be parsed
            command.processCommandArguments(extractArguments(commandArgs));
            return command;
        }

        private void displayHelp()
        {
            Console.WriteLine("Birst Acorn Test Console\n");
            Console.WriteLine("Usage: runAcornTestConsole <commandfile> [<testresultsdir>] [-force] [-parseonly]\n");
            Console.WriteLine("E.g: runAcornTestConsole c:/test/input.txt c:/test/testresult\n\n");
            Console.WriteLine("Commands Supported by Acorn Test Console are to be supplied in the <commandfile> - each on a seperate line. Specifying <testresultsdir> is mandatory if <commandfile> consists of any of runquery, runcommandfile or compareresults commands. [-force] option switches off prompt for deleting <testresultsdir>. if [-parseonly] is supplied, none of the commands will be executed, only the commandfile will be parsed.");
            Console.WriteLine("\nThe following are the supported commands by this program.");
            Console.WriteLine("\nSyntax (createspace):");
            Console.WriteLine("\tcreatespace <spacename> [<MAX_QUERY_ROWS>] [<spacemode>] [<comments>] [<querylanguageversion>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the new space [<MAX_QUERY_ROWS>] is the limit for no. of rows for executing a query (default is 10000) use spacemode=Advanced/Discovery to create advanced/discovery mode space [<comments>] is the comments for the new space to be created [<querylanguageversion>] is the query language version for new space.");
            Console.WriteLine("\nSyntax (deletespace):");
            Console.WriteLine("\tdeletespace <spacename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user");
            Console.WriteLine("\nSyntax (upload):");
            Console.WriteLine("\tupload <spacename> <datafilelocation> [<unmapColumnsNotPresent>] [<outputfilename>] [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <datafilelocation> is full path of the file containing the data to be uploaded into the space and [<unmapColumnsNotPresent>] default is false.");
            Console.WriteLine("\nSyntax (applyrepository):");
            Console.WriteLine("\tapplyrepository <spacename> <repositoryfilename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <repositoryfilename> is the repository file that is to be applied to the space.");
            Console.WriteLine("\nSyntax (applydcconfig):");
            Console.WriteLine("\tapplydcconfig <spacename> <configfile> [<dcconfigname>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <configfile> is the dcconfig file that is to be applied to the space and [<dcconfigname>] is named dcconfig file.");
            Console.WriteLine("\nSyntax (publish):");
            Console.WriteLine("\tpublish <spacename> <loadgroup> <loadnumber> <allowdimensionalcrossjoins> <date> [<subgroups>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user, valid loadgroup and loadnumber for loading the staging tables, allowdimensionalcrossjoins should be true/false and <date> is date of publishing the space and <subgroups> (optional) is the list of subgroups (comma seperated) that need to be processed.");
            Console.WriteLine("\nSyntax (copydatafile):");
            Console.WriteLine("\ncopydatafile <spacename> [<dirname>] [<filename>] <resultdir>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <dirname>/<filename> is the path/file where verified set of file(s) to be copied are available and <resultdir> is the path where the matching sourcefiles are to be copied.");
            Console.WriteLine("\nSyntax (copycatalogtospace):");
            Console.WriteLine("\ncopycatalogtospace <spacename> <catalogdir>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <catalogdir> is the directory that will be replace the catalog directory under the space's data directory.");
            Console.WriteLine("\nSyntax (runquery):");
            Console.WriteLine("\trunquery <spacename> <logicalquery> [<resultfile> [<resultdir>]]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <logicalquery> is any valid (SMIPerformanceEngine) logical query to be executed on the space. <resultfile> is optional to specify the name of resultantfile and resultdir to specify <resultfolder> other than <testresultsdir>.");
            Console.WriteLine("\nSyntax (runcommandfile):");
            Console.WriteLine("\truncommandfile <spacename> <commandfile> [<resultfile>] [<resultdir>] [<maxwaitseconds>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <commandfile> is full path of file containing a set of valid (SMIPerformanceEngine) commands to be executed on the space - each on a seperate line. <resultfile> is optional to specify the name of resultantfile and resultdir to specify <resultfolder> other than <testresultsdir>. <maxwaitseconds> is the no of secods to wait");
            Console.WriteLine("\nSyntax (copyrepo):");
            Console.WriteLine("\tcopyrepo <spacename> [<destinationdir>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and [<destinationdir>] (optional) is name of directory where the copy of the repository for the space is to be created. <destinatinodir> is mandatory if the commandline argument for <testresultsdir> is not provided otherwise <testresultsdir> will be used as <destinatinodir>.");
            Console.WriteLine("\nSyntax (overwriterepo):");
            Console.WriteLine("\toverwriterepo <spacename> <repofilename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <repofilename> is name of the external repository file which needs to be overwritten to the space repository.");
            Console.WriteLine("\nSyntax (overwritedcconfig):");
            Console.WriteLine("\toverwritedcconfig <spacename> <sourcefilename> [<configfilename>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <sourcefilename> is name of the external dcconfig file which needs to be overwritten to the space repository [<configfilename>] is the name of dcconfig file name to be saved (default is dcconfig.xml).");
            Console.WriteLine("\nSyntax (compareresults):");
            Console.WriteLine("\tcompareresults <spacename> <referencedir> <outputfilename> <resultdir> <filetype> [<ignorecase>] [<isbincomparison>] [<iszipfolder>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user, <referencedir> is full path of the reference directory containing results which are to be compared with the new results generated in <testresultsdir>. <outputfilename> is the name of the file that contains comparison results. <resultdir> is the path where actual results/sourcefiles are generated <filetype> is the type of files to be compared.<isbincomparison> is a boolean flag to compare files using FC /B , you will also need to explicitly specify <filetype>=*.* when using <isbincomparison>.<iszipfolder> specefies results folder has zips which are extracted and then compared");
            Console.WriteLine("\nSyntax (removepublished):");
            Console.WriteLine("\tremovepublished <spacename> [<deleteall>] [<restoresettings>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and [<deleteall>] specifies whether all published loads of the given spacename are to be removed (default is last load delete) and [<restoresettings>] specifies to restore previous repository and space settings (default is do not restore).");
            Console.WriteLine("\nSyntax (addcustommetric):");
            Console.WriteLine("\taddcustommetric <spacename> <metricname> <grainofsource> <aggregationrule> <formula> [<sqlgentype>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <metricname> is the name of new metric to be defined <grainofsource> is grain of table on which metric is created <aggregationrule> is the aggregation rule to be applied on metric, <formula> is the expression for obtaining the metric value and [<sqlgentype>] is integer specifying the sql code to generate (0 for MS SQL Server and 1 for MySQL).");
            Console.WriteLine("\nSyntax (updatecustommetric):");
            Console.WriteLine("\tupdatecustommetric <spacename> <metricname> <grainofsource> <aggregationrule> <formula> [<sqlgentype>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <metricname> is the name of metric to be updated <grainofsource> is grain of table on which metric is created <aggregationrule> is the aggregation rule to be applied on metric, <formula> is the expression for obtaining the metric value and [<sqlgentype>] is integer specifying the sql code to generate (0 for MS SQL Server and 1 for MySQL).");
            Console.WriteLine("\nSyntax (removecustommetric):");
            Console.WriteLine("\tremovecustommetric <spacename> <metricname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <metricname> is the name of metric to be removed.");
            Console.WriteLine("\nSyntax (addcustomattribute):");
            Console.WriteLine("\taddcustomattribute <spacename> <attributename> <hierarchylevel> <formula> [<sqlgentype>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <attributename> is the name of new attribute to be defined <hierarchylevel> is a valid Dimension.Level pair, <formula> is the expression for obtaining the attribute value and [<sqlgentype>] is integer specifying the sql code to generate (0 for MS SQL Server and 1 for MySQL).");
            Console.WriteLine("\nSyntax (updatecustomattribute):");
            Console.WriteLine("\tupdatecustomattribute <spacename> <attributename> <hierarchylevel> <formula> [<sqlgentype>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <attributename> is the name of attribute to be updated <hierarchylevel> is a valid Dimension.Level pair, <formula> is the expression for obtaining the attribute value and [<sqlgentype>] is integer specifying the sql code to generate (0 for MS SQL Server and 1 for MySQL).");
            Console.WriteLine("\nSyntax (removecustommetric):");
            Console.WriteLine("\tremovecustomattribute <spacename> <attributename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <attributename> is the name of attribute to be removed.");
            Console.WriteLine("\nSyntax (setuprealtimeconn):");
            Console.WriteLine("\tsetuprealtimeconn <spacename> <connname> <servername> <database> <driver> <username> <password> <port> <logfilepath> [<dcconfigname>] [<localetlconnection>] [<localetlappdirectory>] [<localetlloadgroup>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and the rest are realtime connection parameters for the new connection to be set up.");
            Console.WriteLine("\nSyntax (setupbirstconnecttasks):");
            Console.WriteLine("\tsetupbirstconnecttasks <spacename> <tasklistfile> [<dcconfigname>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <tasklistfile> is the file that contains tasklist portion of dcconfig.xml.");
            Console.WriteLine("\nSyntax (runbirstconnecttask):");
            Console.WriteLine("\trunbirstconnecttask <spacename> <commandargs> [<dcconfigname>] [<maxwaitseconds>] [<taskerrorsfile>] [<taskstatusfile>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user, <commandargs> is the arguments to be passed to run BirstConnect via commandline <maxwaitseconds> is the no of secods to wait for load being processed and is only used if -checkstatus argument is used (default it 900 seconds)."
                              + " <taskerrorsfile> if used, will have any messages for an upload task, <taskstatusfile> if used, will log the exitstatus of BirstConnect and will allow ATC to run even when task fails.");
            Console.WriteLine("\nSyntax (wait):");
            Console.WriteLine("\twait <seconds>");
            Console.WriteLine("\t\tWhere <seconds> is no of secods ATC should wait before executing the next command.");
            Console.WriteLine("\nSyntax (launchbirstconnect):");
            Console.WriteLine("\tlaunchbirstconnect <spacename> [<dcconfigname>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user.");
            Console.WriteLine("\nSyntax (closebirstconnect):");
            Console.WriteLine("\tclosebirstconnect <spacename> [<dcconfigname>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user.");
            Console.WriteLine("\nSyntax (applyGASettings):");
            Console.WriteLine("\tapplyGASettings <spacename> <username> <password> <profile> <startdate> <enddate> ");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <username> <password> <profile> <startdate>  <enddate> are Google Analytics login credentials.");
            Console.WriteLine("\nSyntax (writetabletofile):");
            Console.WriteLine("\twritetabletofile <spacename> [<query>] [<tablename>] <outputfilename> [<outputdirectory>] [<orderby>] [writetablestructureonly]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <tablename> is the space's table and output will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (importcube):");
            Console.WriteLine("\timportcube <spacename> <connname> <driver> <cubename> <cacheable>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <connname> is the space's (XMLA) live access connection <driver> is the name of XMLA connection driver and <cubename> is the name of the cube to be imported <cacheable> specifies whether the sources imported are cacheable or not.");
            Console.WriteLine("\nSyntax (importtable):");
            Console.WriteLine("\timporttable <spacename> <connname> <tablename> <dimensionname> <levelname> <importmeasures>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <connname> is the space's live access connection <tablename> is the name of table to import <dimensionname> is the name of the dimension into which table is to be imported <levelname> specifies level at which table is to be imported and <importmeasures> specifies whether measures are to be imported or not");
            Console.WriteLine("\nSyntax (listtables):");
            Console.WriteLine("\tlisttables <spacename> <connname> [<outputfilename>] [<resultdir>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <connname> is the space's (non XMLA) live access connection [<outputfilename>] (default is importresults.txt) is name of file to which the output is to be written and [<resultdir>] (default is testresultsdir) is the directory where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (importtableschema):");
            Console.WriteLine("\timporttableschema <spacename> <connname> <tablename> [<outputfilename>] [<resultdir>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <connname> is the space's (non XMLA) live access connection <tablename> is the comma seperated list of tables for which the schema is to be fetched [<outputfilename>] (default is importresults.txt) is name of file to which the output is to be written and [<resultdir>] (default is testresultsdir) is the directory where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (writesourcemetadta):");
            Console.WriteLine("\twritesourcemetadta <spacename> <stagingtable> <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <stagingtable> is the space's stagingtable whose metadata is to be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (exportreporttohtml):");
            Console.WriteLine("\texportreporttohtml <spacename> <reportname> <reportpath> [<emailto>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <reportname> the name of the report to be exported <reportpath> is the path where report is located and [<emailto>] is the email id to which email is to be sent.");
            Console.WriteLine("\nSyntax (movefiles):");
            Console.WriteLine("\tmovefiles <spacename> [<fromfolder>] <tofolder> [<filetype>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user <fromfolder> is path from where the files are to be copied and <tofolder> is path where the files are to be copied and <filetype> is the type of files to be copied. ");
            Console.WriteLine("\nSyntax (changequerylanguage):");
            Console.WriteLine("\tchangequerylanguage <spacename> <querylanguage>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <querylanguage> is the version of query langugage to be set for space.");
            Console.WriteLine("\nSyntax (exportdashboardstoxml):");
            Console.WriteLine("\texportdashboardstoxml <spacename> <outputdir>");
            Console.WriteLine("\t\tWhere <spacename> is the name of one of the valid spaces of the user and <outputdir> is the folder where the generated xml files are to be saved.");
            Console.WriteLine("\nSyntax (setuser):");
            Console.WriteLine("\tsetuser <username>");
            Console.WriteLine("\t\tWhere <username> is the email id of the new ATC user.");
            Console.WriteLine("\nSyntax (setspace):");
            Console.WriteLine("\tsetspace <spacename>");
            Console.WriteLine("\t\tCalls Acorn's SetSpace method to do housekeeping jobs. Where <spacename> is the name of one of the valid spaces of the user.");
            Console.WriteLine("\nSyntax (StartMemDBNoWait):");
            Console.WriteLine("\tStartMemDBNoWait <filename> [<arguments>]");
            Console.WriteLine("\t\tStarts the execution file specified by <filename> on remote machine (machine on which MemDB is running) and returns immediately. Supply arguments to file if any");
            Console.WriteLine("\nSyntax (StopMemDBNoWait):");
            Console.WriteLine("\tStopMemDBNoWait <filename>");
            Console.WriteLine("\t\tStops/Kills the execution file specified by <filename> on remote machine (machine on which MemDB is running) and returns immediately.");
            Console.WriteLine("\nSyntax (StartMemDBWithWait):");
            Console.WriteLine("\tStartMemDBWithWait <filename> [<arguments>]");
            Console.WriteLine("\t\tStarts the execution file specified by <filename> on remote machine (machine on which MemDB is running) and holds the process handle. Supply arguments to file if any");
            Console.WriteLine("\nSyntax (StopMemDBInstanceWithWait):");
            Console.WriteLine("\tStopMemDBInstanceWithWait");
            Console.WriteLine("\t\tStops the memdb instance running on remote machine (machine on which MemDB is running) and releases the process handle.");
            Console.WriteLine("\nSyntax (logtime):");
            Console.WriteLine("\tlogtime <spacename> <commandtype> <tag> <outputfilename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space <commandtype> is the name of command like CREATE,UPLOAD etc.. <tag> is the name of the tag  <outputfilename> is full path of the file containing the log.");
            Console.WriteLine("\nSyntax (importpackage):");
            Console.WriteLine("\timportpackage <spacename> <parentspacename> <parentspaceownername> <packagename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space <parentspacename> is the spacename of parent space, <parentspaceownername> is the ownername of parent space <packagename> is the name of the package to be imported from parent space.");
            Console.WriteLine("\nSyntax (createpackage):");
            Console.WriteLine("\tcreatepackage <spacename> <packagedir>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space <packagedir> is the directory which contains package.xml file");
            Console.WriteLine("\nSyntax (deletepackage):");
            Console.WriteLine("\tdeletepackage <spacename> <packagename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space <packagename> is the name of the package to delete");
            Console.WriteLine("\nSyntax (copyfileordirectorytospace):");
            Console.WriteLine("\tcopyfileordirectorytospace <spacename> <path> [<directory>] <topath>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space <path> is path of file or directory to be copied, <directory> denotes path is directory and <topath> is the path under space where file/directory is to be copied.");
            Console.WriteLine("\nSyntax (setvariable):");
            Console.WriteLine("\tsetvariable <name> <value>[<dbtype>]");
            Console.WriteLine("\t\tWhere <name> is the name of the variable, <value> is the value of specified variable and [<dbtype>] is the database name.");
            Console.WriteLine("\nSyntax (setrepositoryattribute):");
            Console.WriteLine("\tsetrepositoryattribute <spacename> <attributename> <attributevalue>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <attributename> is the name of the attribute, <attributevalue> is the value of specified attribute.");
            Console.WriteLine("\tmovespace get server=<servername> database=<databasename> uid=<uname> pwd=<passwd> dir=<outputdir> spacename=<spacename> [driver=<drivername> databasedir=<databasedir>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <databasename> is the name of the database, <uname>,<passwd>,<databasename> are db username,password and db Name. <spacename> is spacename , <driver> is Admin Database driver,<databasedir> is relative path to db");
            Console.WriteLine("Please refer MoveSpace commands for more details on usage");
            Console.WriteLine("\tmovespace put [overwrite] server=<servername> database=<databasename> uid=<uname> pwd=<passwd> dir=<outputdir> owner=<ownername>  [spaceid=<spaceid> ][driver=<drivername>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <databasename> is the name of the database, <uname>,<passwd>,<databasename> are db username,password and db Name. <spacename> is spacename , <driver> is Admin Database driver,<spaceid> is space id");
            Console.WriteLine("Please refer MoveSpace commands for more details on usage");                    
            Console.WriteLine("\tdeletesamples <spacename>");
            Console.WriteLine("\texecutedml <spacename> <query>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <query> is the query you want to execute");

            Console.WriteLine("\r\n\r\n");
            Console.WriteLine("\nSyntax (birstwebservices.login):");
            Console.WriteLine("\tbirstwebservices.login <username> <password>");
            Console.WriteLine("\t\tWhere <username> is the email id of the new ATC user and <password> is password in clear text.");
            Console.WriteLine("\nSyntax (birstwebservices.logout):");
            Console.WriteLine("\tbirstwebservices.logout");
            Console.WriteLine("\t\tLogs out user and terminates webservices session.");
            Console.WriteLine("\nSyntax (birstwebservices.getvariablesforspace):");
            Console.WriteLine("\tbirstwebservices.getvariablesforspace <spacename> <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, variablelist will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.executequeryinspace):");
            Console.WriteLine("\tbirstwebservices.executequeryinspace <spacename> [<query> | <queryfile>] <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, atleast [<query> | <queryfile>] argument should be defined and <queryfile> contains logical queries and will get precedence over <query>. <query> is a logical query to be executed on the space, result will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.swapspacecontents):");
            Console.WriteLine("\tbirstwebservices.swapSpaceContents <spacename1> <spacename2>");
            Console.WriteLine("\t\tWhere <spacename1> is the name of the first space, <spacename2> is the name of the second space.");
            Console.WriteLine("\nSyntax (birstwebservices.addusertospace):");
            Console.WriteLine("\tbirstwebservices.addusertospace <spacename> <username> <hasadmin> <hasdesigner> <hasdashboard>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <username> is the email id,  <hasadmin>|<hasdesigner>|<hasdashboard> are user access rights to space.");
            Console.WriteLine("\nSyntax (birstwebservices.addgrouptospace):");
            Console.WriteLine("\tbirstwebservices.addgrouptospace <spacename> <groupname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <groupname> is the group to be added to space.");
            Console.WriteLine("\nSyntax (birstwebservices.addusertogroupinspace):");
            Console.WriteLine("\tbirstwebservices.addusertogroupinspace <spacename> <groupname> <username>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <username> is the email id of user, <groupname> is the group name to which user will be added.");
            Console.WriteLine("\nSyntax (birstwebservices.listusersinspace):");
            Console.WriteLine("\tbirstwebservices.listusersinspace <spacename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space for which user list is requested.");
            Console.WriteLine("\nSyntax (birstwebservices.listgroupaclsinspace):");
            Console.WriteLine("\tbirstwebservices.listgroupaclsinspace <spacename> <groupname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <groupname> is the name of group in space for which acl list is requested.");
            Console.WriteLine("\nSyntax (birstwebservices.clearcacheinspace):");
            Console.WriteLine("\tbirstwebservices.clearcacheinspace <spacename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space for which clear cache is requested.");
            Console.WriteLine("\nSyntax (birstwebservices.begindataupload):");
            Console.WriteLine("\tbirstwebservices.begindataupload <spacename> <sourcename> [<uploadoptions>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <sourcename> is the name of source and optional [<uploadoptions>] is upload options seperated by comma i.e. ConsolidateIdenticalStructures=[true|false], ColumnNamesInFirstRow=[true|false], FilterLikelyNoDataRows=[true|false], LockDataSourceFormat=[true|false], IgnoreQuotesNotAtStartOrEnd=[true|false], RowsToSkipAtStart=n, RowsToSkipAtEnd=n, CharacterEncoding=enc.");
            Console.WriteLine("\nSyntax (birstwebservices.uploaddata):");
            Console.WriteLine("\tbirstwebservices.uploaddata <spacename> <sourcefile>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <sourcefile> is the absolute file path.");
            Console.WriteLine("\nSyntax (birstwebservices.finishdataupload):");
            Console.WriteLine("\tbirstwebservices.finishdataupload [<waittillfinish>] [<maxwaitseconds>]");
            Console.WriteLine("\tbirstwebservices.finishdataupload is called after the last chunk of data is uploaded for a particular data source, optional arguements - [<waittillfinish>] wait till upload is finished and check upload status till [<maxwaitseconds>] seconds.");
            Console.WriteLine("\nSyntax (birstwebservices.getdatauploadstatus):");
            Console.WriteLine("\tbirstwebservices.getdatauploadstatus Called the get the status after data upload processing is complete");
            Console.WriteLine("\nSyntax (birstwebservices.publishdata):");
            Console.WriteLine("\tbirstwebservices.publishdata [<waittillfinish>] [<maxwaitseconds>]");
            Console.WriteLine("\tbirstwebservices.publishdata Gets Birst to process the data sources that have been defined.");
            Console.WriteLine("\nSyntax (birstwebservices.publishdata) <spacename> <loadgroup> <independentmode> <date> [<subgroups>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <loadgroup> is the space load group, <independentmode> flag, <date> is the date to use as the load-date and optional <subgroups> (if defined in the space), [<waittillfinish>] wait till processing is finished and check processing status till [<maxwaitseconds>] seconds.");
            Console.WriteLine("\nSyntax (birstwebservices.getpublishingstatus):");
            Console.WriteLine("\tbirstwebservices.getpublishingstatus Called the get the status after publishing processing is complete");
            Console.WriteLine("\tbirstwebservices.setvariableinspace Sets a variable value.");
            Console.WriteLine("\nSyntax (birstwebservices.setvariableinspace) <spacename> <variablename> <query>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <variablename> is name of variable and <query> is the logical query.");
            Console.WriteLine("\nSyntax (birstwebservices.resetpassword) <username>");
            Console.WriteLine("\t\tWhere <username> is the name of the user to reset.");
            Console.WriteLine("\nSyntax (birstwebservices.removeuserfromspace):");
            Console.WriteLine("\tbirstwebservices.removeuserfromspace <spacename> <username>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <username> is name of the user.");
            Console.WriteLine("\nSyntax (birstwebservices.removeuserfromgroupinspace):");
            Console.WriteLine("\tbirstwebservices.removeuserfromgroupinspace <spacename> <groupname> <username>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <username> is the name of user, <groupname> is the group name from which user will be removed.");
            Console.WriteLine("\nSyntax (birstwebservices.addacltogroupinspace):");
            Console.WriteLine("\tbirstwebservices.addacltogroupinspace <spacename> <groupname> <aclname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <aclname> is the acl name and <groupname> is the group to which acl needs to be added for space.");
            Console.WriteLine("\nSyntax (birstwebservices.addproxyuser):");
            Console.WriteLine("\tbirstwebservices.addproxyuser <username> <proxyusername>");
            Console.WriteLine("\t\tWhere <username> is the name of the user, <proxyusername> is the name of the user who will proxy for the first user.");
            Console.WriteLine("\nSyntax (birstwebservices.copyspacecontents):");
            Console.WriteLine("\tbirstwebservices.copyspacecontents <copyfromspace> <copytospace>");
            Console.WriteLine("\t\tWhere <copyfromspace> is the name of the space to copy from , <copytospace> is the name of the space to copy to.");
            Console.WriteLine("\nSyntax (birstwebservices.listgroupsinspace):");
            Console.WriteLine("\tbirstwebservices.listgroupsinspace <spacename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space for which user group list is requested.");
            Console.WriteLine("\nSyntax (birstwebservices.listusersingroupinspace):");
            Console.WriteLine("\tbirstwebservices.listusersingroupinspace <spacename> <groupname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <groupname> is the name of group in space for which user list is requested.");
            Console.WriteLine("\nSyntax (birstwebservices.removegroupfromspace):");
            Console.WriteLine("\tbirstwebservices.removegroupfromspace <spacename> <groupname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <groupname> group will be removed for the space.");
            Console.WriteLine("\nSyntax (birstwebservices.removeaclfromgroupinspace):");
            Console.WriteLine("\tbirstwebservices.removeaclfromgroupinspace <spacename> <groupname> <aclname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <aclname> is the acl name and <groupname> is the group from which acl needs to be removed for space.");
            Console.WriteLine("\nSyntax (birstwebservices.listcreatedusers):");
            Console.WriteLine("\tbirstwebservices.listcreatedusers");
            Console.WriteLine("\nSyntax (birstwebservices.adduser):");
            Console.WriteLine("\tbirstwebservices.adduser <username> <parameters>");
            Console.WriteLine("\t\tWhere <username> is the name of the user and <parameters> are a list of name=value pairs separated by spaces. Valid names are 'password=', 'firstname=', 'lastname=', 'email=', 'title=', 'company=', 'phone=', 'city=', 'state=', 'zip=' and 'country=', for example 'password=foo firstname=John lastname=Doe'. password in clear text.");
            Console.WriteLine("\nSyntax (birstwebservices.listproxyusers):");
            Console.WriteLine("\tbirstwebservices.addproxyuser <username> <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <username> is the name of the user for which list of proxy user names will be retrieved and output will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated..");
            Console.WriteLine("\nSyntax (birstwebservices.removeproxyuser):");
            Console.WriteLine("\tbirstwebservices.removeproxyuser <username> <proxyusername>");
            Console.WriteLine("\t\tWhere <username> is the name of the user who has proxy user, <proxyusername> is the name of proxy user.");
            Console.WriteLine("\nSyntax (birstwebservices.copycatalogdirectory):");
            Console.WriteLine("\tbirstwebservices.copycatalogdirectory <copyfromspace> <copytospace> <directoryname>");
            Console.WriteLine("\t\tWhere <copyfromspace> is the name of the space to copy from , <copytospace> is the name of the space to copy to and <copycatalogdirectory and <directoryname> is the name of the directory to start the copy.");
            Console.WriteLine("\nSyntax (birstwebservices.getdirectorycontents):");
            Console.WriteLine("\tbirstwebservices.getdirectorycontents <spacename> [<directoryname>] <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, optional [<directoryname>] is the name of the directory (null value means top level), output will be written to <outputfilename> file, Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.getdirectorypermissions):");
            Console.WriteLine("\tbirstwebservices.getdirectorypermissions <spacename> [<directoryname>] <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, optional [<directoryname>] is the name of the directory (null value means top level), output will be written to <outputfilename> file, Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.setdirectorypermission):");
            Console.WriteLine("\tbirstwebservices.setdirectorypermission <spacename> <directoryname> <groupname> <permissionname> <permission>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, <directoryname> is the name of the directory, <groupname> is the name of the group, <permissionname> is the name of permission, <permission> is true/false.");
            Console.WriteLine("\nSyntax (birstwebservices.addallowedip):");
            Console.WriteLine("\tbirstwebservices.addallowedip <username> <ip>");
            Console.WriteLine("\t\tWhere <username> is the name of the user, <ip> is the ip address/CIDR netblock.");
            Console.WriteLine("\tbirstwebservices.listallowedips <username> <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <username> is the name of the user, output will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.removeallowedip):");
            Console.WriteLine("\tbirstwebservices.removeallowedip <username> <ip>");
            Console.WriteLine("\t\tWhere <username> is the name of the user, <ip> is the ip address/CIDR netblock to be removed.");
            Console.WriteLine("\nSyntax (birstwebservices.copyfileordirectory):");
            Console.WriteLine("\tbirstwebservices.copyfileordirectory <fromspace> <fileOrDir> <tospace> <toDir>");
            Console.WriteLine("\t\tWhere <fromspace> is the name of the space to copy from , <fileOrDir> is the file or directory name , <tospace> is the name of the space to copy to and <toDir> is target directory.");
            Console.WriteLine("\nSyntax (birstwebservices.deletefileordirectory):");
            Console.WriteLine("\tbirstwebservices.deletefileordirectory <spacename> <fileOrDir>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space to copy from , <fileOrDir> is the file or directory name.");
            Console.WriteLine("\nSyntax (birstwebservices.renamefileordirectory):");
            Console.WriteLine("\tbirstwebservices.renamefileordirectory <spacename> <fileOrDir> <newname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space to copy from , <fileOrDir> is the file or directory name and <newname> of the file or direcotory.");
            Console.WriteLine("\nSyntax (birstwebservices.createnewdirectory):");
            Console.WriteLine("\tbirstwebservices.createnewdirectory <spacename> <parenetdir> <newdirectoryname>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space to copy from , <parenetdir> is the name of parent folder and <newdirectoryname> is the new directory name.");
            Console.WriteLine("\nSyntax (birstwebservices.createnewspace):");
            Console.WriteLine("\tbirstwebservices.createnewspace <spacename> [<comments>] [<advanced>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, optional <comments>] and [<advanced>].");
            Console.WriteLine("\nSyntax (birstwebservices.deletespace):");
            Console.WriteLine("\tbirstwebservices.deletespace <spacename>");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space.");
            Console.WriteLine("\nSyntax (birstwebservices.copycustomsubjectarea):");
            Console.WriteLine("\tbirstwebservices.copycustomsubjectarea <fromspacename> <customsubjectareaname> <tospacename>");
            Console.WriteLine("\t\tWhere <fromspacename> is the name of the space to copy from , <customsubjectareaname> is the name of the custom subject area and <tospacename> is the name of the destination space.");
            Console.WriteLine("\nSyntax (birstwebservices.listcustomsubjectareas):");
            Console.WriteLine("\tbirstwebservices.listcustomsubjectareas <spacename> <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space, custom subject areas will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.listspaces):");
            Console.WriteLine("\tbirstwebservices.listspaces <outputfilename> [<outputdirectory>]");
            Console.WriteLine("\t\tWhere space names will be written to <outputfilename> file. Optionally supply [<outputdirectory> where outputfile is to be generated.");
            Console.WriteLine("\nSyntax (birstwebservices.deletelastdatafromspace):");
            Console.WriteLine("\tbirstwebservices.deletelastdatafromspace <spacename> [<waittillfinish>] [<maxwaitseconds>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space from which data to be deleted, optional arguements - [<waittillfinish>] wait till detele data is finished and check delete status till [<maxwaitseconds>] seconds.");
            Console.WriteLine("\nSyntax (birstwebservices.deletealldatafromspace):");
            Console.WriteLine("\tbirstwebservices.deletealldatafromspace <spacename> [<waittillfinish>] [<maxwaitseconds>]");
            Console.WriteLine("\t\tWhere <spacename> is the name of the space from which data to be deleted, optional arguements - [<waittillfinish>] wait till detele data is finished and check delete status till [<maxwaitseconds>] seconds.");
            Console.WriteLine("\tbirstwebservices.copyspace <fromspacename> <tospacename>");
            Console.WriteLine("\t\tWhere <fromspacename> is source space, <tospacename> is destination space");
            Console.WriteLine("\nSyntax (birstwebservices.setuprealtimeconnectionforspace):");
            Console.WriteLine("\tbirstwebservices.setuprealtimeconnectionforspace <spacename> <configFileName> <connectionName> <databaseType> <useDirectConnection> <host> <port> <databaseName> <userName> <password> <timeout>");
            Console.WriteLine("\nSyntax (birstwebservices.setupgenericjdbcrealtimeconnectionforspace):");
            Console.WriteLine("\tbirstwebservices.setupgenericjdbcrealtimeconnectionforspace <spacename> <configFileName> <connectionName> <useDirectConnection> <sqlType> <driverName> <connectionString> <filter> <userName> <password> <timeout>");
            Console.WriteLine("\nSyntax (birstwebservices.importcubemetadataintospace):");
            Console.WriteLine("\tbirstwebservices.importcubemetadataintospace <spacename> <connectionName> <databaseType> <cubeName> <importType> <cacheable>");
        }
    }
}