﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class ImportPackageCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string parentspacename { get; set; }
        public string parentspaceownername { get; set; }
        public string packagename { get; set; }
        
        public ImportPackageCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command importpackage.");
            spacename = preProcessStringArgument(spacename);
            if (parentspacename == null)
                throw new Exception("parentspacename argument not specified for command importpackage.");
            parentspacename = preProcessStringArgument(parentspacename);
            if (parentspaceownername == null)
                throw new Exception("parentspaceownername argument not specified for command importpackage.");
            parentspaceownername = preProcessStringArgument(parentspaceownername);
            if (packagename == null)
                throw new Exception("packagename argument not specified for command importpackage.");
            packagename = preProcessStringArgument(packagename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                Acorn.User owner = Acorn.Database.getUserByEmail(ATC.conn, ATC.SCHEMA_NAME, parentspaceownername);
                Space parentSpace = Acorn.Database.getSpace(ATC.conn, parentspacename, owner, true, ATC.SCHEMA_NAME);
                if (parentSpace == null)
                    throw new Exception("Unable to find the parentspace specified:" + parentspacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                List<PackageInfoLite> packages = Acorn.Utils.PackageUtils.getAllowedPackagesForImport(null, ATC.u, sp);
                if (packages != null && packages.Count > 0)
                {
                    bool found = false;
                    foreach (PackageInfoLite package in packages)
                    {
                        if (package.Name.Equals(packagename) && package.SpaceID == parentSpace.ID.ToString())
                        {
                            found = true;
                            List<PackageCollision> collissions = Acorn.Utils.PackageUtils.importPackage(null, ATC.u, sp, maf, parentSpace.ID, Guid.Parse(package.ID), true);
                            if (collissions != null)
                            {
                                throw new Exception("Error on importing package: " + packagename + " to childspace, Collisions detected");
                            }
                        }
                    }
                    if (!found)
                        throw new Exception("No package with name " + packagename + " found for import from parentspace: " + parentspacename);                
                }
                else
                    throw new Exception("No packages found for import from parentspace: " + parentspacename);                
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("importpackage");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/parentspacename=").Append(parentspacename);
            command.Append(" ").Append("/parentspaceownername=").Append(parentspaceownername);
            command.Append(" ").Append("/packagename=").Append(packagename);
            return command.ToString();
        }
    }
}
