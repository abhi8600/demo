﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class SetUserCommand : ATC_Command
    {
        public string username { get; set; }

        public SetUserCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command setuser.");
            username = preProcessStringArgument(username);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                ATC.u = Acorn.Database.getUserByEmail(ATC.conn, ATC.SCHEMA_NAME, username);
                if (ATC.u == null)
                {
                    throw new Exception("Did not find the specified user:" + username);                    
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setuser");
            command.Append(" ").Append("/username=").Append(username);
            return command.ToString();
        }
    }
}
