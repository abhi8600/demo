﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.Diagnostics;
using System.IO;

namespace AcornTestConsole
{
    class StopMemDBNoWaitCommand : ATC_Command
    {
        public string filename { get; set; }
        public string arguments { get; set; }

        public StopMemDBNoWaitCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (filename == null)
                throw new Exception("filename argument not specified for command stopmemdbnowait.");
            filename = preProcessStringArgument(filename);
            if (ATC.MemDB_Path == null || ATC.MemDB_Path.Trim().Length == 0)
            {
                throw new Exception("MemDB_Path not specified in AcornTestConsole.exe.config.");
            }
            if (arguments != null)
                arguments = preProcessStringArgument(arguments);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                String cmdFile = ATC.tempfolder + "\\stopmemdbnowaitcmd.bat";
                File.Delete(cmdFile);
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                sw.Write("TASKKILL.exe");
                sw.Write(" /S " + ATC.MemDB_Machine_Name);
                if (ATC.MemDB_UserName != null && ATC.MemDB_UserName.Trim().Length > 0)
                    sw.Write(" /U " + ATC.MemDB_UserName);
                if (ATC.MemDB_Password != null && ATC.MemDB_Password.Trim().Length > 0)
                    sw.Write(" /P " + ATC.MemDB_Password);
                sw.Write(" /IM \"" + filename + "\"");
                if (arguments != null && arguments.Trim().Length > 0)
                    sw.Write(" " + arguments);
                sw.Write(" /T /F /fi \"USERNAME eq NT AUTHORITY\\SYSTEM\"");
                string outFile = "\"" + ATC.tempfolder + "\\stopmemdbnowaitout.log\"";
                string errFile = "\"" + ATC.tempfolder + "\\stopmemdbnowaiterr.log\"";
                sw.Write(" > " + outFile + " 2> " + errFile);
                sw.Flush();
                sw.Close();
                ProcessStartInfo startInfo = new ProcessStartInfo(cmdFile);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.WorkingDirectory = ATC.PsTools_Path;
                Process process = Process.Start(startInfo);
                process.WaitForExit();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("stopmemdbnowait");
            command.Append(" ").Append("/filename=").Append(filename);
            if (arguments != null && !arguments.Trim().Equals(""))
                command.Append(" ").Append("/arguments=").Append(arguments);
            return command.ToString();
        }
    }
}
