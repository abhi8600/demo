﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Data.Odbc;
using System.Diagnostics;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class LaunchBirstConnectCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string dcconfigname { get; set; }

        public LaunchBirstConnectCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command launchbirstconnect.");
            spacename = preProcessStringArgument(spacename);
            dcconfigname = preProcessStringArgument(dcconfigname);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (ATC.bcCmdLines.ContainsKey(sp.Name + "_" + dcconfigname))
                    throw new Exception("BirstConnect is already running for space:" + spacename + " configname:" + dcconfigname);
                string jnlp = Util.generateJnlpForSpace(ATC.conn, ATC.u, sp, dcconfigname);
                string jnlpfilename = sp.Directory + "\\" + sp.Name + ".jnlp";
                if (File.Exists(jnlpfilename))
                    File.Delete(jnlpfilename);
                StreamWriter sw = File.CreateText(jnlpfilename);
                sw.Write(jnlp);
                sw.Close();

                ProcessStartInfo startInfo = new ProcessStartInfo("\"" + System.Environment.GetEnvironmentVariable("JAVA_HOME") + "\\bin\\java.exe\"");
                startInfo.Arguments =  " -cp \"" + ATC.configLocation + "\\*;" + ATC.configLocation + "\\lib\\*;" + ATC.configLocation + "\\lib\\SAP\\*;" + ATC.configLocation + "\\lib\\JDBC\\*\"";
                startInfo.Arguments += " -Xmx256m";
                startInfo.Arguments += " -Djnlp.file=\"" + jnlpfilename + "\"";
                DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\QA");
                startInfo.Arguments += " -Djnlp.log4j.Override=\"" + di.FullName + "\\bin\\log4j.xml\"";
                string birstConnectLogsPath = sp.Directory + "\\BirstConnectLogs";
                if (!Directory.Exists(birstConnectLogsPath))
                    Directory.CreateDirectory(birstConnectLogsPath);
                startInfo.Arguments += " -DBirstConnect.logs.path=\"" + (birstConnectLogsPath + "\\").Replace("\\", "\\\\") + "\"";
                startInfo.Arguments += " com.birst.dataconductor.DataConductorApp";
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.WorkingDirectory = Directory.GetCurrentDirectory();
                Console.Out.Write("Launching Birst Connect...");
                Console.Out.WriteLine(startInfo.FileName + startInfo.Arguments);
                ATC.bcCmdLines.Add(sp.Name + "_" + dcconfigname, Process.Start(startInfo));
                System.Threading.Thread.Sleep(60000);                
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("launchbirstconnect");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/dcconfigname=").Append(dcconfigname);
            return command.ToString();
        }
    }
}
