﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using Acorn.Utils;
using Acorn.DBConnection;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class DeletePackageCommand : ATC_Command
    {
          public string spacename { get; set; }
          public string packagename { get; set; }

          public DeletePackageCommand(Program ATC) : base(ATC)
          {
          }

         override public void setDefaultArguments()
         {
         }

         override public void processCommandArguments(string[] commandArgs)
         {
             this.ParseArguments(commandArgs);
             if (spacename == null)
                 throw new Exception("spacename argument not specified for command deletepackage.");
             spacename = preProcessStringArgument(spacename);

             if (packagename == null)
                 throw new Exception("packagename argument not specified for command deletepackage.");
             packagename = preProcessStringArgument(packagename);
         }

         override public Nullable<bool> execute()
         {
             try
             {
                 bool isPackageExist = false;
                 
                 Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                 if (sp == null)
                     throw new Exception("Unable to find the space specified:" + spacename);

                 AllPackages fromPackagesList = AllPackages.load(sp.Directory);
                 if (fromPackagesList != null && fromPackagesList.Packages != null && fromPackagesList.Packages.Length > 0)
                 {
                     foreach (PackageDetail package in fromPackagesList.Packages)
                     {
                         if (package.Name.Equals(packagename))
                         {
                             isPackageExist = true;
                             PackageUtils.deletePackage(null, ATC.u, sp, package.ID);
                             break;
                         }
                     }
                     if (!isPackageExist)
                     {
                         throw new Exception("Unable to find package specified:" + packagename);
                     }
                 }
                 else
                 {
                     throw new Exception("Unable to find any package in space specified:" + spacename);
                 }
                 return true;
             }
             catch (Exception e)
             {
                 Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                 return false;
             }
         }

         override public string ToString()
         {
             StringBuilder command = new StringBuilder("deletepackage");
             command.Append(" ").Append("/spacename=").Append(spacename);
             command.Append(" ").Append("/packagename=").Append(packagename);
             return command.ToString();
         }
    }
}
