﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class ApplyGASettingsCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string profile { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }

        public ApplyGASettingsCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command applygasettings.");
            spacename = preProcessStringArgument(spacename);
            if (username == null)
                throw new Exception("username argument not specified for command applygasettings.");
            username = preProcessStringArgument(username);
            if (password == null)
                throw new Exception("password argument not specified for command applygasettings.");
            password = preProcessStringArgument(password);
            if (profile == null)
                throw new Exception("profile argument not specified for command applygasettings.");
            profile = preProcessStringArgument(profile);
            if (start_date == null)
                throw new Exception("start_date argument not specified for command applygasettings.");
            start_date = preProcessStringArgument(start_date);
            if (end_date == null)
                throw new Exception("end_date argument not specified for command applygasettings.");
            end_date = preProcessStringArgument(end_date);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                GoogleAnalyticsSettings settings = GoogleAnalyticsSettings.getSettings(sp.Directory);
                List<GoogleAnalyticsProfile> profiles = null;
                try
                {
                    profiles = Acorn.GoogleAnalyticsUtil.getProfiles(username, password);
                }
                catch (Exception)
                {
                    throw new Exception("Unable to get profiles for username:" + username);
                }

                if (profiles == null || profiles.Count == 0)
                {
                    throw new Exception("Not enough Profiles to compare with profile " + profile);
                }
                else
                {

                    settings.Username = username;
                    settings.Password = password;
                    MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                    GoogleAnalyticsProfile gp = null;
                    foreach (GoogleAnalyticsProfile sgp in profiles)
                    {
                        if (profile.Equals(sgp.title, StringComparison.InvariantCultureIgnoreCase))
                        {
                            settings.profileTableId = sgp.tableID;
                            gp = sgp;
                            break;
                        }
                    }
                    if (gp == null)
                    {
                        throw new Exception("Profile " + "\"" + profile + "\"" + " not matching with available profiles");                        
                    }
                    settings.profile = gp;
                    try
                    {
                        settings.startDate = DateTime.Parse(start_date);
                        settings.endDate = DateTime.Parse(end_date);
                    }
                    catch (Exception)
                    {
                        settings.startDate = DateTime.Now.Subtract(TimeSpan.FromDays(90));
                        settings.endDate = DateTime.Now;
                    }
                    String configLocation = System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"];
                    if (configLocation == null || configLocation.Trim().Equals(""))
                        configLocation = "c:\\workspace\\Acorn\\Acorn";
                    settings.integrateIntoRepository(maf, configLocation);
                    settings.saveSettings(sp.Directory);
                    Acorn.Util.saveApplication(maf, sp, null, ATC.u);                    
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("applygasettings");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/profile=").Append(profile);
            command.Append(" ").Append("/start_date=").Append(start_date);
            command.Append(" ").Append("/end_date=").Append(end_date);
            return command.ToString();
        }
    }
}
