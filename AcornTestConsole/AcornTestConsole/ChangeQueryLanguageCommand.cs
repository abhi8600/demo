﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class ChangeQueryLanguageCommand : ATC_Command
    {
        public string spacename { get; set; }
        public int querylanguage { get; set; }

        public ChangeQueryLanguageCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            querylanguage = Database.NEW_QUERY_LANGUAGE;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command changequerylanguage.");
            spacename = preProcessStringArgument(spacename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                sp.QueryLanguageVersion = querylanguage;
                Database.updateSpace(ATC.conn, ATC.SCHEMA_NAME, sp);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("changequerylanguage");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/querylanguage=").Append(querylanguage);
            return command.ToString();
        }
    }
}
