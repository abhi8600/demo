﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    ///<summary>
    ///Base class for all ATC Commands
    ///</summary>
    public abstract class ATC_Command
    {
        public Program ATC = null;

        public ATC_Command(Program ATC)
        {
            this.ATC = ATC;
        }

        ///<summary>
        ///Preprocesses string arguments - removes quotes, replaces V{DATE} and V{DATETIME}
        ///</summary>
        protected string preProcessStringArgument(string value)
        {
            if (value != null)
            {
                if (value.StartsWith("\"") && value.EndsWith("\"") && value.Length >= 2)
                    value = value.Substring(1, value.Length - 2);
                value = value.Replace("V{DATETIME}", ATC.datetimeStr);
                value = value.Replace("V{DATE}", ATC.dateStr);
            }
            return value;
        }

        ///<summary>
        ///Sets command's default arguments
        ///</summary>
        abstract public void setDefaultArguments();

        ///<summary>
        ///Parses command arguments and throws exception if any mandatory argument is missing or could not be parsed
        ///</summary>
        abstract public void processCommandArguments(string[] commandArgs);

        ///<summary>
        ///Executes the task to be performed by command
        ///</summary>
        abstract public Nullable<bool> execute();

        ///<summary>
        ///Returns the command along with its arguments.
        ///</summary>
        abstract override public string ToString();
    }
}
