﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class ImportTableCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string connname { get; set; }
        public string tablename { get; set; }
        public string dimensionname { get; set; }
        public string levelname { get; set; }
        public bool importmeasures { get; set; }

        private Acorn.RARepository rap;

        public ImportTableCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command importtable.");
            spacename = preProcessStringArgument(spacename);
            if (connname == null)
                throw new Exception("connname argument not specified for command importtable.");
            connname = preProcessStringArgument(connname);
            if (tablename == null)
                throw new Exception("tablename argument not specified for command importtable.");
            tablename = preProcessStringArgument(tablename);
            if (dimensionname == null)
                throw new Exception("dimensionname argument not specified for command importtable.");
            dimensionname = preProcessStringArgument(dimensionname);
            if (levelname == null)
                throw new Exception("levelname argument not specified for command importtable.");
            levelname = preProcessStringArgument(levelname);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Acorn.Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                rap = Acorn.Util.getRepository(maf);
                object[][] tables = Acorn.Utils.LiveAccessUtil.getTableSchema(null, ATC.u, maf, sp, connname, tablename.Split(new string[] { "," }, StringSplitOptions.None), null);
                if (tables == null)
                    throw new Exception("Could not retrive tableschema from liveaccess connection:" + connname);
                if (tables != null && tables.Length > 0)
                {
                    Dictionary<string, RARepository.RAMeasureTable> mtables = new Dictionary<string, RARepository.RAMeasureTable>();
                    Dictionary<string, RARepository.RADimensionTable> dtables = new Dictionary<string, RARepository.RADimensionTable>();
                    String connType = getConnectionType(connname);
                    for (int i = 0; i < tables.Length; i++)
                    {
                        object[] row = tables[i];
                        string dbase = (string)row[0];
                        string schema = (isDBTypeMySQL(connType) ? (string)row[0] : (string)row[1]);
                        string tname = (string)row[2];
                        string cname = (string)row[3];
                        string dtype = (string)row[5];
                        int width = Convert.ToInt32(row[6]);

                        if (importmeasures)
                        {
                            RARepository.RAMeasureTable mt = null;
                            if (mtables.ContainsKey(tname))
                                mt = mtables[tname];
                            if (mt == null)
                            {
                                if (rap.MeasureTables != null)
                                {
                                    foreach (RARepository.RAMeasureTable smt in rap.MeasureTables)
                                    {
                                        if (smt.Name.Equals(tname))
                                        {
                                            mt = smt;
                                            mtables[tname] = mt;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (mt == null)
                            {
                                mt = new RARepository.RAMeasureTable();
                                mt.Cacheable = true;
                                mt.Name = tname;
                                mtables[tname] = mt;
                                mt.Type = 0;
                                mt.TTL = -1;
                                mt.Cardinality = 0;
                                mt.Source = new TableSource();
                                mt.Source.Connection = connname;
                                if (schema != null && schema.Length > 0)
                                    mt.Source.Schema = schema;
                                mt.Source.Tables = new TableSource.TableDefinition[1];
                                TableSource.TableDefinition td = new TableSource.TableDefinition();
                                td.JoinType = 0;
                                mt.Source.Tables[0] = td;
                                //mt.Mappings = new RARepository.RAMeasureColumnMapping()[];
                                td.PhysicalName = tname;
                                if (rap.MeasureTables != null)
                                {
                                    List<RARepository.RAMeasureTable> lst = rap.MeasureTables.ToList<RARepository.RAMeasureTable>();
                                    lst.Add(mt);
                                    rap.MeasureTables = lst.ToArray<RARepository.RAMeasureTable>();
                                }
                                else
                                {
                                    rap.MeasureTables = new RARepository.RAMeasureTable[] { mt };
                                }
                            }
                            string agg = "SUM";
                            addMeasureIfNecessary(cname, agg, dtype, 0);
                            bool found = false;
                            if (mt.Mappings != null)
                            {
                                foreach (RARepository.RAMeasureColumnMapping mc in mt.Mappings)
                                {
                                    if (mc.Formula.Equals(cname))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (!found)
                            {
                                RARepository.RAMeasureColumnMapping mc = new RARepository.RAMeasureColumnMapping();
                                mc.ColumnName = cname;
                                mc.Formula = "[" + cname + "]";
                                if (mt.Mappings != null)
                                {
                                    List<RARepository.RAMeasureColumnMapping> lst = mt.Mappings.ToList<RARepository.RAMeasureColumnMapping>();
                                    lst.Add(mc);
                                    mt.Mappings = lst.ToArray<RARepository.RAMeasureColumnMapping>();
                                }
                                else
                                {
                                    mt.Mappings = new RARepository.RAMeasureColumnMapping[] { mc };
                                }
                            }
                            if (levelname.Length > 0)
                            {
                                string dtname = tablename + "-" + levelname;
                                RARepository.RADimensionTable dt = null;
                                if (dtables.ContainsKey(dtname))
                                    dt = dtables[dtname];
                                RARepository.RADimension d = null;
                                if (rap.Dimensions != null)
                                {
                                    foreach (RARepository.RADimension dim in rap.Dimensions)
                                    {
                                        if (!dim.Name.Equals(dimensionname))
                                            continue;
                                        d = dim;
                                        break;
                                    }
                                }
                                // Create dimension if necessary
                                if (d == null)
                                {
                                    d = new RARepository.RADimension();
                                    d.Name = dimensionname;
                                    if (rap.Dimensions != null)
                                    {
                                        List<RARepository.RADimension> lst = rap.Dimensions.ToList<RARepository.RADimension>();
                                        lst.Add(d);
                                        rap.Dimensions = lst.ToArray<RARepository.RADimension>();
                                    }
                                    else
                                    {
                                        rap.Dimensions = new RARepository.RADimension[] { d };
                                    }
                                }

                                if (dt == null && d.Definitions != null)
                                {
                                    foreach (RARepository.RADimensionTable sdt in d.Definitions)
                                    {
                                        if (sdt.Name.Equals(dtname))
                                        {
                                            dt = sdt;
                                            dtables[dtname] = dt;
                                            break;
                                        }
                                    }
                                }

                                // Create a dimension table if necessary
                                if (dt == null)
                                {
                                    dt = new RARepository.RADimensionTable();
                                    dt.Name = dtname;
                                    dt.Cacheable = true;
                                    dtables[dtname] = dt;
                                    if (d.Definitions != null)
                                    {
                                        List<RARepository.RADimensionTable> lst = d.Definitions.ToList<RARepository.RADimensionTable>();
                                        lst.Add(dt);
                                        d.Definitions = lst.ToArray<RARepository.RADimensionTable>();
                                    }
                                    else
                                    {
                                        d.Definitions = new RARepository.RADimensionTable[] { dt };
                                    }
                                    dt.Type = 0;
                                    dt.TTL = -1;
                                    dt.Level = levelname;
                                    dt.Source = new TableSource();
                                    dt.Source.Connection = connname;
                                    dt.Source.Schema = schema;
                                    TableSource.TableDefinition td = new TableSource.TableDefinition();
                                    dt.Source.Tables = new TableSource.TableDefinition[] { td };
                                    td.JoinType = 0;
                                    td.PhysicalName = tablename;
                                }
                                found = false;
                                if (d.Columns != null)
                                {
                                    foreach (RARepository.RADimensionColumn dc in d.Columns)
                                    {
                                        if (dc.ColumnName.Equals(cname))
                                        {
                                            found = true;
                                        }
                                    }
                                }
                                if (!found)
                                {
                                    RARepository.RADimensionColumn dc = new RARepository.RADimensionColumn();
                                    dc.ColumnName = cname;
                                    if (d.Columns != null)
                                    {
                                        List<RARepository.RADimensionColumn> lst = d.Columns.ToList<RARepository.RADimensionColumn>();
                                        lst.Add(dc);
                                        d.Columns = lst.ToArray<RARepository.RADimensionColumn>();
                                    }
                                    else
                                    {
                                        d.Columns = new RARepository.RADimensionColumn[] { dc };
                                    }
                                    dc.DataType = dtype;
                                    dc.Width = width;
                                }
                                found = false;

                                if (dt.Mappings != null)
                                {
                                    foreach (RARepository.RADimensionColumnMapping dcm in dt.Mappings)
                                    {
                                        if (dcm.ColumnName.Equals(cname))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (!found)
                                {
                                    RARepository.RADimensionColumnMapping dcm = new RARepository.RADimensionColumnMapping();
                                    dcm.ColumnName = cname;
                                    dcm.Formula = tname + "." + getQuotedName(connType, cname);
                                    if (dt.Mappings != null)
                                    {
                                        List<RARepository.RADimensionColumnMapping> lst = dt.Mappings.ToList<RARepository.RADimensionColumnMapping>();
                                        lst.Add(dcm);
                                        dt.Mappings = lst.ToArray<RARepository.RADimensionColumnMapping>();
                                    }
                                    else
                                    {
                                        dt.Mappings = new RARepository.RADimensionColumnMapping[] { dcm };
                                    }
                                }
                            }
                        }
                    }
                    // Add redundant joins if both measure and dimension tables imported
                    foreach (RARepository.RAMeasureTable jmt in mtables.Values)
                    {
                        foreach (RARepository.RADimensionTable jdt in dtables.Values)
                        {
                            if (jmt.Source.Tables[0].PhysicalName.Equals(jdt.Source.Tables[0].PhysicalName))
                            {
                                bool found = false;
                                if (rap.Joins != null)
                                {
                                    foreach (RARepository.RAJoin j in rap.Joins)
                                    {
                                        if ((j.Table1.Equals(jdt.Name) && j.Table2.Equals(jmt.Name)) || (j.Table2.Equals(jdt.Name) && j.Table1.Equals(jmt.Name)))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (!found)
                                {
                                    RARepository.RAJoin nj = new RARepository.RAJoin();
                                    nj.Table1 = jdt.Name;
                                    nj.Table2 = jmt.Name;
                                    nj.JoinCondition = "";
                                    nj.JoinType = "Inner";
                                    nj.Redundant = true;
                                    if (rap.Joins != null)
                                    {
                                        List<RARepository.RAJoin> lst = rap.Joins.ToList<RARepository.RAJoin>();
                                        lst.Add(nj);
                                        rap.Joins = lst.ToArray<RARepository.RAJoin>();
                                    }
                                    else
                                    {
                                        rap.Joins = new RARepository.RAJoin[] { nj };
                                    }
                                }
                            }
                        }
                    }
                    Acorn.Util.setRepository(rap, maf, sp, null, ATC.u);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private string getQuotedName(string connType, string item)
        {
            if (item == null || item.Trim().Length == 0 || item.IndexOf(" ") < 0)
            {
                return item;
            }
            else if (isDBTypeMySQL(connType))
            {
                return "`" + item + "`";
            }
            else
            {
                return "\"" + item + "\"";
            }
        }

        private static bool isDBTypeMySQL(string connType)
        {
            return (connType != null && (connType.ToLower().Equals("mysql") || connType.ToLower().Equals("infobright") || connType.ToLower().Equals("odbcmysql")));
        }

        private string getConnectionType(string cVisibleName)
        {
            foreach (RARepository.RAConnection raConn in rap.Connections)
            {
               if (raConn.VisibleName.Equals(cVisibleName))
               {
                   return raConn.Type;
               }               
            }
            return null;
        }

        private void addMeasureIfNecessary(string name, string aggRule, string dtype, int width)
        {
            RARepository.RAMeasureColumn curm = null;
            if (rap.Measures != null)
            {
                foreach (RARepository.RAMeasureColumn m in rap.Measures)
                {
                    if (m.ColumnName.Equals(name))
                    {
                        curm = m;
                        break;
                    }
                }
            }
            if (curm == null)
            {
                curm = new RARepository.RAMeasureColumn();
                curm.ColumnName = name;
                if (rap.Measures != null)
                {
                    List<RARepository.RAMeasureColumn> lst = rap.Measures.ToList<RARepository.RAMeasureColumn>();
                    lst.Add(curm);
                    rap.Measures = lst.ToArray<RARepository.RAMeasureColumn>();
                }
                else
                {
                    rap.Measures = new RARepository.RAMeasureColumn[] { curm };
                }
            }
            curm.AggRule = aggRule;
            curm.DataType = getType(dtype);
            curm.Width = width;
        }

        private string getType(string dtype)
        {
            if (dtype.ToLower().Equals("varchar") || dtype.ToLower().Equals("nvarchar") || dtype.ToLower().Equals("nchar") || dtype.ToLower().Equals("char") || dtype.ToLower().Equals("ntext") || dtype.ToLower().Equals("varchar2"))
                return "Varchar";
            else if (dtype.ToLower().Equals("int") || dtype.ToLower().Equals("integer") || dtype.ToLower().Equals("int identity") || dtype.ToLower().Equals("smallint"))
                return "Integer";
            else if (dtype.ToLower().Equals("float"))
                return "Float";
            else if (dtype.ToLower().Equals("datetime") || dtype.ToLower().IndexOf("timestamp") != -1)
                return "DateTime";
            else if (dtype.ToLower().Equals("date"))
                return "Date";
            else if (dtype.ToLower().Equals("decimal") || dtype.ToLower().Equals("money") || dtype.ToLower().Equals("real") || dtype.ToLower().Equals("number") || dtype.ToLower().Equals("double") || dtype.ToLower().Equals("currency"))
                return "Number";
            return "Varchar";
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("importtable");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/connname=").Append(connname);
            command.Append(" ").Append("/tablename=").Append(tablename);
            command.Append(" ").Append("/dimensionname=").Append(dimensionname);
            command.Append(" ").Append("/levelname=").Append(levelname);
            if (importmeasures)
                command.Append(" ").Append("/importmeasures");
            return command.ToString();
        }
    }
}
