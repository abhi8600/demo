﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using System.IO;

namespace AcornTestConsole
{
    class LogTimeCommand : ATC_Command
    {
        public string tag { get; set; }
        public string outputFilename { get; set; }
        public string spacename { get; set; }
        public string commandtype { get; set; }

        public LogTimeCommand(Program ATC)
            : base(ATC)
        {
            // Block Will Remain Blank
        }

        override public void setDefaultArguments()
        {
            // Block Will Remain Blank
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);

            if (tag != null)
            {
                tag = preProcessStringArgument(tag);
            }

            if (String.IsNullOrEmpty(tag))
            {
                throw new Exception("tag argument not specified for command logtime.");
            }


            if (outputFilename == null)
            {
                throw new Exception("outputfilename argument not specified for command logtime.");
            }
            outputFilename = preProcessStringArgument(outputFilename);            

            if (spacename == null)
            {
                throw new Exception("spacename argument not specified for command logtime.");
            }
            spacename = preProcessStringArgument(spacename);

            if (commandtype == null)
            {
                throw new Exception("commandtype argument not specified for command logtime.");
            }
            commandtype = preProcessStringArgument(commandtype);

        }

        override public Nullable<bool> execute()
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                String fileName = outputFilename.Substring(outputFilename.LastIndexOf("\\")+1);
                String revisedFileName = ATC.DatabaseType + "_" + fileName;
                outputFilename = outputFilename.Replace(fileName, revisedFileName);
                if (!File.Exists(outputFilename))
                {
                    File.AppendAllText(outputFilename, "Dataset Name\tTask\tSpecification\tDatetime\n");
                }
                File.AppendAllText(outputFilename, spacename + "\t" + commandtype + "\t" + tag + "\t" + dateTime.ToString("yyyy-MM-dd' 'H:mm:ss.fff") + "\n");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }

        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("logtime");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/commandtype=").Append(commandtype);
            command.Append(" ").Append("/tag=").Append(tag);
            command.Append(" ").Append("/outputfilename=").Append(outputFilename);
            return command.ToString();
        }
    }
}
