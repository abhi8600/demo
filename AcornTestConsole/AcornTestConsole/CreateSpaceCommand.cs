﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using Acorn;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class CreateSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public int MAX_QUERY_ROWS { get; set; }
        public string comments { get; set; }
        public string spacemode { get; set; }
        public int querylanguageversion { get; set; }

        public CreateSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            MAX_QUERY_ROWS = 10000;
            comments = "";
            querylanguageversion = Acorn.Database.NEW_QUERY_LANGUAGE;
            spacemode = "Automatic";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command createspace."); 
            spacename = preProcessStringArgument(spacename);
            comments = preProcessStringArgument(comments);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                int spaceMode;
                switch (spacemode)
                {
                    case "Advanced"     : spaceMode = Space.SPACE_MODE_ADVANCED; break;
                    case "Discovery"    : spaceMode = Space.SPACE_MODE_DISCOVERY; break;
                    default             : spaceMode = Space.SPACE_MODE_AUTOMATIC; break;
                }
                GenericResponse response = NewSpaceService.createSpace(null, spacename, null, "ATC Created Space", spaceMode, false, querylanguageversion, null, ATC.u, -1);

                if (response.Error.ErrorType != ResponseMessages.SUCCESS)
                    throw new Exception("Cannot create space: " + spacename + " : " + response.Error.ErrorMessage);               
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("createspace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/MAX_QUERY_ROWS=").Append(MAX_QUERY_ROWS);
            if (comments != null && !comments.Trim().Equals(""))
                command.Append(" ").Append("/comments=").Append(comments);
            command.Append(" ").Append("/spacemode=").Append(spacemode);
            command.Append(" ").Append("/querylanguageversion=").Append(querylanguageversion);
            return command.ToString();
        }
    }
}
