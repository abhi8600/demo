@@ECHO OFF

if !"%JAVA_HOME%"==!"" goto SET_JAVA_HOME
goto SKIP_JAVA_HOME

:SET_JAVA_HOME
set CLEAR_JAVA_HOME=false
set JAVA_HOME=C:\Program Files\Java\jdk1.6.0_10

:SKIP_JAVA_HOME
if !"%SMI_HOME%"==!"" goto SET_SMI_HOME
goto SKIP_SMI_HOME

:SET_SMI_HOME
set CLEAR_SMI_HOME=false
set SMI_HOME=C:\SMIPerfEngine\Main

:SKIP_SMI_HOME
if !"%INSTALL_DIR%"==!"" goto SET_INSTALL_DIR
goto SKIP_INSTALL_DIR

:SET_INSTALL_DIR
set CLEAR_INSTALL_DIR=false
set INSTALL_DIR=C:\SMIPerfEngine\Main

:SKIP_INSTALL_DIR

call AcornTestConsole.exe %1 %2 %3 %4 %5 %6 %7 %8 %9

rem ECHO %JAVA_HOME%
rem ECHO %SMI_HOME%
rem ECHO %INSTALL_DIR%

if !"%CLEAR_JAVA_HOME%"==!"true" set JAVA_HOME=
if !"%CLEAR_JAVA_HOME%"==!"true" set CLEAR_JAVA_HOME=
if !"%CLEAR_SMI_HOME%"==!"true" set SMI_HOME=
if !"%CLEAR_SMI_HOME%"==!"true" set CLEAR_SMI_HOME=
if !"%CLEAR_INSTALL_DIR%"==!"true" set INSTALL_DIR=
if !"%CLEAR_INSTALL_DIR%"==!"true" set CLEAR_INSTALL_DIR=