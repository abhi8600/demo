﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class IgnoreFinishCommand : ATC_Command
    {
        public string dbtype { get; set; }

        public IgnoreFinishCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public Nullable<bool> execute()
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (dbtype == null)
                throw new Exception("dbtype argument not specified for command ignorefinish.");
            dbtype = preProcessStringArgument(dbtype);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("ignorefinish");
            command.Append(" ").Append("/dbtype=").Append(dbtype);
            return command.ToString();
        }
    }
}
