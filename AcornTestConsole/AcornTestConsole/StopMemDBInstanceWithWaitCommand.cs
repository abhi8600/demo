﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.Diagnostics;
using System.IO;

namespace AcornTestConsole
{
    class StopMemDBInstanceWithWaitCommand : ATC_Command
    {
        public StopMemDBInstanceWithWaitCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);            
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (ATC.MemDB_Process_ID == -1)
                    throw new Exception("MemDB instance handle not available.");
                String cmdFile = ATC.tempfolder + "\\stopmemdbwaitcmd.bat";
                File.Delete(cmdFile);
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                sw.Write(ATC.PsTools_Path + "\\PSKILL.exe");
                sw.Write(" -t");
                sw.Write(" \\\\" + ATC.MemDB_Machine_Name);
                if (ATC.MemDB_UserName != null && ATC.MemDB_UserName.Trim().Length > 0)
                    sw.Write(" -u " + ATC.MemDB_UserName);
                if (ATC.MemDB_Password != null && ATC.MemDB_Password.Trim().Length > 0)
                    sw.Write(" -p " + ATC.MemDB_Password);
                sw.Write(" " + ATC.MemDB_Process_ID);
                string outFile = "\"" + ATC.tempfolder + "\\stopmemdbwaitout.log\"";
                string errFile = "\"" + ATC.tempfolder + "\\stopmemdbwaiterr.log\"";
                sw.Write(" > " + outFile + " 2> " + errFile);
                sw.Flush();
                sw.Close();
                ProcessStartInfo startInfo = new ProcessStartInfo(cmdFile);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.WorkingDirectory = ATC.PsTools_Path;
                Process process = Process.Start(startInfo);
                process.WaitForExit();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("stopmemdbinstancewithwait");
            return command.ToString();
        }

    }
}
