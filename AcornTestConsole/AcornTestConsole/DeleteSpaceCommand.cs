﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using Acorn.DBConnection;

namespace AcornTestConsole
{
    class DeleteSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        
        public DeleteSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command deletespace."); 
            spacename = preProcessStringArgument(spacename);            
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                List<string> launchedBirstConnects = new List<string>();
                if (Util.spaceContainsLocalSource(maf))
                {
                    //need to launch birstconnect (for all birstconnect configs) when deleting space
                    string[] configNames = Util.getDCConfigNames(sp);
                    if (configNames != null && configNames.Length > 0)
                    {
                        foreach (string configName in configNames)
                        {
                            if (!ATC.bcCmdLines.ContainsKey(sp.Name + "_" + configName))
                            {
                                LaunchBirstConnectCommand cmd = new LaunchBirstConnectCommand(ATC);
                                cmd.setDefaultArguments();
                                cmd.processCommandArguments(new string[] { "/spacename=" + sp.Name, "/dcconfigname=" + configName });
                                cmd.execute();
                                launchedBirstConnects.Add(configName);
                            }
                        }
                    }
                }
                Acorn.Database.deleteSpace(ATC.conn, ATC.SCHEMA_NAME, sp, null);
                deleteSchema(ATC.conn, ATC.SCHEMA_NAME, sp); 
                foreach (string key in launchedBirstConnects)
                {
                    CloseBirstConnectCommand cmd = new CloseBirstConnectCommand(ATC);
                    cmd.setDefaultArguments();
                    cmd.processCommandArguments(new string[] { "/spacename=" + sp.Name, "/dcconfigname=" + key });
                    cmd.execute();
                }
                //delete space entry from spaces table
                QueryCommand command = ATC.conn.CreateCommand();
                command.CommandText = "DELETE FROM " + ATC.SCHEMA_NAME + "." + Acorn.Database.SPACE_TABLE_NAME + " WHERE [ID]='" + sp.ID + "'";
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("deletespace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            return command.ToString();
        }

        private static void deleteSchema(QueryConnection conn, string schema, Space sp)
        {
            Global.systemLog.Info("Beginning delete of schema: " + sp.ID);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.SPACE_TABLE_NAME + " WHERE [ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.USER_SPACE_INXN + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.PUBLISH_TABLE + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.SCHEDULED_LOAD_TABLE + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            // Deleting space group acl information from database
            // Deleting group_acl information for the space groups
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.GROUP_ACL_TABLE +
                " WHERE [GROUP_ID] IN " + 
                "(SELECT [GROUP_ID] FROM " + schema + "." + Acorn.Database.SPACE_GROUP_TABLE + 
                " WHERE [SPACE_ID]= '" + sp.ID + "')";
            cmd.ExecuteNonQuery();

            // Deleting group_user info for the space groups
            cmd.CommandText = "DELETE FROM " + schema + "." + Acorn.Database.GROUP_USER_TABLE +
                " WHERE [GROUP_ID] IN " +
                "(SELECT [GROUP_ID] FROM " + schema + "." + Acorn.Database.SPACE_GROUP_TABLE +
                " WHERE [SPACE_ID]= '" + sp.ID + "')";
            cmd.ExecuteNonQuery();

            // Deleting space_group 
            cmd.CommandText = "DELETE FROM " + schema + "." +  Acorn.Database.SPACE_GROUP_TABLE +
                " WHERE [SPACE_ID] = '" + sp.ID + "'";
            cmd.ExecuteNonQuery();

            // Delete the database schema
            QueryConnection tconn = null;
            try
            {
                tconn = ConnectionPool.getConnection(sp.getFullConnectString());
                QueryCommand tcmd = tconn.CreateCommand();
                if (sp.DatabaseType == "Infobright")
                {
                    tcmd.CommandText = "DROP DATABASE IF EXISTS " + sp.Schema;
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
                else if (sp.DatabaseType == Acorn.Database.ORACLE)
                {
                    if (isOraTableSpaceExists(tconn, sp.Schema))
                    {
                        string dropTablesSpaceQuery = "DROP TABLESPACE " + sp.Schema + " INCLUDING CONTENTS AND DATAFILES CASCADE CONSTRAINTS";
                        Global.systemLog.Debug("Drop tablespace query for space " + sp.ID.ToString() + " query: " + dropTablesSpaceQuery);
                        tcmd.CommandText = dropTablesSpaceQuery;
                        tcmd.ExecuteNonQuery();
                    }
                    string dropUserQuery = "DROP USER " + sp.Schema + " CASCADE";
                    Global.systemLog.Debug("Drop user query for space " + sp.ID.ToString() + " query: " + dropUserQuery);
                    tcmd.CommandText = dropUserQuery;
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
                else
                {
                    tcmd.CommandText = "DROP SCHEMA [" + sp.Schema + "]";
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(tconn);
            }
            return;
        }

        public static bool isOraTableSpaceExists(QueryConnection conn, string tablespacename)
        {
            if (!conn.isDBTypeOracle())
                return false;
            else
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM DBA_TABLESPACES WHERE TABLESPACE_NAME='" + tablespacename + "'";
                QueryReader reader = cmd.ExecuteReader();
                cmd.Dispose();
                return reader.Read();
            }
        }
    }
}
