﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class CopyCatalogToSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string catalogdir { get; set; }

        public CopyCatalogToSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command copycatalogtospace.");
            spacename = preProcessStringArgument(spacename);
            if (catalogdir == null)
                throw new Exception("catalogdir argument not specified for command copycatalogtospace.");
            catalogdir = preProcessStringArgument(catalogdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (!Directory.Exists(catalogdir))
                    throw new Exception("Catalog Directory not found - " + catalogdir);

                if (Directory.Exists(sp.Directory + "\\catalog"))
                {
                    System.Diagnostics.Process.Start("CMD.exe", "/C rmdir /S /Q \"" + sp.Directory + "\\catalog\"");
                }
                System.Diagnostics.Process.Start("CMD.exe", "/C mkdir \"" + sp.Directory + "\\catalog\"");
                System.Diagnostics.Process process = System.Diagnostics.Process.Start("CMD.exe", "/C xcopy /Y/S/E/I  \"" + catalogdir + "\" \"" + sp.Directory + "\\catalog\"");
                Console.Out.WriteLine("Waiting for the catalog to be copied");
                process.WaitForExit(10000);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("copycatalogtospace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/catalogdir=").Append(catalogdir);
            return command.ToString();
        }
    }
}
