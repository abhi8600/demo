﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class PublishCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string loadgroup { get; set; }
        public int loadnumber { get; set; }
        public bool allowdimensionalcrossjoins { get; set; }
        public string date { get; set; }
        public string subgroups { get; set; }
        public bool reprocessmodified { get; set; }

        public PublishCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            loadgroup = "ACORN";
            loadnumber = 1;
            date = "V{DATE}";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command publish."); 
            spacename = preProcessStringArgument(spacename);
            loadgroup = preProcessStringArgument(loadgroup);
            date = preProcessStringArgument(date);
            subgroups = preProcessStringArgument(subgroups);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                DateTime publishDate = DateTime.Now;
                bool res = DateTime.TryParse(date, out publishDate);
                if (!res)
                {
                    Console.Out.WriteLine("Unable to parse the datetime specified for publish:" + date + ", Using current time");
                    publishDate = DateTime.Now;
                }
                string[] subgroupArr = null;
                if (subgroups != null)
                {
                    subgroupArr = subgroups.Split(',');
                }

                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                maf.setAllowDimensionalCrossJoins(allowdimensionalcrossjoins);
                string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                ApplicationLoader al = new ApplicationLoader(maf, sp, publishDate, ATC.SCHEMA_NAME, ATC.sc.EngineCommand, System.Web.Configuration.WebConfigurationManager.AppSettings["CreateTimeCommand"], ATC.sc.DatabaseLoadDir, null, ATC.u, loadgroup, loadnumber);
                if (subgroupArr != null)
                    al.SubGroups = subgroupArr;
                al.load(true, null, false, true, true, reprocessmodified);
                
                //sleep current thread for 5 seconds and then poll stauts, abort if load failed
                Status.StatusResult result = null;
                do
                {
                    System.Threading.Thread.Sleep(5000);
                    result = Status.getLoadStatus(null, sp, loadnumber, loadgroup);
                    Console.Out.WriteLine("Publish Status : " + (result == null ? " Unknown" : result.code.ToString()));
                } while (result != null && Status.isRunningCode(result.code));
                result = filterStatusResult(result);
                if (result == null || result.code.Equals(Status.StatusCode.Failed))
                {
                    ATC.abortATC = true;
                    throw new Exception("Publish for space failed, aborting further execution of ATC commands.");
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This hides the status like "Ready, None" and instead sends Complete status
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private Status.StatusResult filterStatusResult(Status.StatusResult result)
        {
            if (result != null && !Status.isRunningCode(result.code))
            {
                result.code = (result.code == Status.StatusCode.Complete || result.code == Status.StatusCode.Failed) ? result.code : Status.StatusCode.Complete;
                return result;
            }
            else
            {
                return result;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("publish");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/loadgroup=").Append(loadgroup);
            command.Append(" ").Append("/loadnumber=").Append(loadnumber);
            if (allowdimensionalcrossjoins)
                command.Append(" ").Append("/allowdimensionalcrossjoins");
            command.Append(" ").Append("/date=").Append(date);
            if (subgroups != null)
                command.Append(" ").Append("/subgroups=").Append(subgroups);
            return command.ToString();
        }
    }
}
