﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class OverwriteRepoCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string repofilename { get; set; }

        public OverwriteRepoCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command overwriterepo.");
            spacename = preProcessStringArgument(spacename);
            if (repofilename == null)
                throw new Exception("repofilename argument not specified for command overwriterepo.");
            repofilename = preProcessStringArgument(repofilename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                string repfile = sp.Directory + "\\repository_dev.xml";
                File.Copy(repofilename, repfile, true);
                File.SetLastWriteTime(repfile, DateTime.Now);
                repfile = sp.Directory + "\\repository.xml";
                File.Copy(repofilename, repfile, true);
                File.SetLastWriteTime(repfile, DateTime.Now);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("overwriterepo");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/repofilename=").Append(repofilename);
            return command.ToString();
        }
    }
}
