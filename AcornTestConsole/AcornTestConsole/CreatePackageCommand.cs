﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using Acorn.Utils;
using Acorn.DBConnection;
using Performance_Optimizer_Administration;
using System.IO;

namespace AcornTestConsole
{
    class CreatePackageCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string packagedir { get; set; }

        public CreatePackageCommand(Program ATC) : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command createpackage.");
            spacename = preProcessStringArgument(spacename);

            if (packagedir == null)
                throw new Exception("packagedir argument not specified for command createpackage.");
            packagedir = preProcessStringArgument(packagedir);
            
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);

                if (!Directory.Exists(packagedir))
                    throw new Exception("package directory does not exist: " + packagedir);

                AllPackages fromPackagesList = AllPackages.load(packagedir);
                if (fromPackagesList != null && fromPackagesList.Packages != null && fromPackagesList.Packages.Length > 0)
                {
                    foreach (PackageDetail package in fromPackagesList.Packages)
                    {
                        PackageDetails packageDetails = new PackageDetails();
                        packageDetails .PackageObjects = Acorn.Utils.PackageUtils.getPackageObjects(null, sp, package, false);
                        packageDetails.ID = package.ID.ToString();
                        packageDetails.Name = package.Name;
                        packageDetails.Description = package.Description;
                        packageDetails.CreatedBy = ATC.u.Username;
                        packageDetails.CreatedDate = DateTime.Now;
                        packageDetails.ModifiedBy = null;
                        packageDetails.ModifiedDate = null;

                        PackageUtils.createPackageDetails(null, ATC.u, sp, packageDetails);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

              
        override public string ToString()
        {
            StringBuilder command = new StringBuilder("createpackage");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/packagedir=").Append(packagedir);
            return command.ToString();
        }
    }
}
