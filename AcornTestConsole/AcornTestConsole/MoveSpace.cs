﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using System.Diagnostics;
using System.IO;

namespace AcornTestConsole
{
    class MoveSpace : ATC_Command
    {
        public string server { get; set; }
        public string database { get; set; }
        public string uid { get; set; }
        public string pwd { get; set; }
        public string dir { get; set; }
        public string spacename { get; set; }
        public Guid spaceid { get; set; }
        public string spaceconfig { get; set; }
        public string owner { get; set; }
        public string contentdir { get; set; }
        public bool overwrite { get; set; }
        public string driver { get; set; }
        public string databasedir { get; set; }
        public bool get { get; set; }
        public bool put { get; set; }

        public MoveSpace(Program ATC)
            : base(ATC)
        {
        }

        public override void setDefaultArguments()
        {
            put = false;
            overwrite = false;
        }

        public override void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            spacename = preProcessStringArgument(spacename);
            owner = preProcessStringArgument(owner);
            if (!get && !put)
                throw new Exception("put/get argument not specified for command MoveSpace.");
            if (get && put)
                throw new Exception("put & get both arguments specified for command MoveSpace.");
            if (spacename == null && get)
                throw new Exception("spacename argument not specified for command MoveSpace.");
            if (server == null)
                throw new Exception("server argument not specified for command MoveSpace.");
            if (database == null)
                throw new Exception("database argument not specified for command MoveSpace.");
            if (uid == null)
                throw new Exception("uid argument not specified for command MoveSpace.");
            if (pwd == null)
                throw new Exception("pwd argument not specified for command MoveSpace.");
            if (dir == null)
                throw new Exception("dir argument not specified for command MoveSpace.");

            if (put)
            {
                if (owner == null)
                    throw new Exception("owner argument not specified for command MoveSpace.");
            }
        }

        public override bool? execute()
        {
            try
            {
                if (get)
                {
                    Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                    spaceid = sp.ID;
                    if (spaceid == null)
                    {
                        throw new Exception("Could not get the Space ID for " + spacename);
                    }
                    //clean up the movespace folder
                    Console.WriteLine("Empty target dir folder for MoveSpace : " + dir);
                    try
                    {
                        ClearFolder(dir);
                    }
                    catch(Exception){
                    }
                }
                String moveSpaceBatFile = ATC.tempfolder + "\\acorntest.movespace.bat";
                File.Delete(moveSpaceBatFile);
                FileStream file = new FileStream(moveSpaceBatFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
             
                sw.Write(this.ToString());
                sw.Close();
                file.Close();

                ProcessStartInfo startInfo  = new ProcessStartInfo(moveSpaceBatFile);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                
                Process process = Process.Start(startInfo);
                process.StartInfo.RedirectStandardOutput = true;
                process.OutputDataReceived += (sender, args) => Console.WriteLine("{0}", args.Data);
                process.BeginOutputReadLine();
                Console.Out.WriteLine("Waiting for the MoveSpace to complete execution");
                
                while (!process.WaitForExit(10000))
                    Console.Out.Write(".");
                if (process.ExitCode != 0)
                {
                    Console.Out.WriteLine("Move Space Exited with a Non 0 return code. Treating command as unsuccessful");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        public override string ToString()
        {
            StringBuilder command = new StringBuilder();
            command.Append(ATC.configLocation + "\\bin\\MoveSpace.exe");
            command.Append(" ").Append(get ? "get" : "put");
            command.Append(" ").Append("server=").Append(server);
            command.Append(" ").Append("database=").Append(database);
            command.Append(" ").Append("uid=").Append(uid);
            command.Append(" ").Append("pwd=").Append(pwd);
            command.Append(" ").Append("dir=").Append(dir);
           
            //if its a get and databasedir is not null, could possibly mean MoveSpace for IB
            if (databasedir != null && get)
            {
                command.Append(" ").Append("databasedir=").Append(databasedir);
            }
            if (get)
            {
                command.Append(" ").Append("spaceid=").Append(spaceid);
            }
            if (driver != null)
            {
                command.Append(" ").Append("driver=").Append(driver);
            }
            if (put)
            {
                if (put)
                {
                    //Using configured Space Config for ATC
                    command.Append(" ").Append("spaceconfig=").Append(System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"]);
                    command.Append(" ").Append("owner=").Append(owner);
                    command.Append(" ").Append("contentdir=").Append(System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"]);
                    if (overwrite)
                    {
                        command.Append(" ").Append("overwrite");
                    }
                }
            }
            return command.ToString();
        }

        private void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }
    }
}
