﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace AcornTestConsole
{
    class WriteTableToFileCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string tablename { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }
        public string orderby { get; set; }
        public string query { get; set; }
        public bool writetablestructureonly { get; set; }
        public bool skipheader { get; set; }

        public WriteTableToFileCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command writetabletofile.");
            spacename = preProcessStringArgument(spacename);
            tablename = preProcessStringArgument(tablename);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command writetabletofile.");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
            orderby = preProcessStringArgument(orderby);
            query = preProcessStringArgument(query);
            if (tablename == null && query == null)
                throw new Exception("tablename or query argument not specified for command writetabletofile.");
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);

                QueryConnection spConn = Acorn.ConnectionPool.getConnection(sp.getFullConnectString());
                QueryCommand cmd = spConn.CreateCommand();
                QueryReader reader = null;
                if (query != null)
                {
                    query = query.Replace("V{SCHEMA}", sp.Schema);
                    cmd.CommandText = query;
                }
                else
                {

                    if (!Acorn.Database.tableExists(spConn, sp.Schema, tablename))
                    {
                        Console.Error.WriteLine("Unable to find tablename specified:" + tablename);
                        return false;
                    }

                    bool useSquareBrackets = false;
                    if (spConn.isDBTypeSQLServer())
                        useSquareBrackets = true;
                    string orderbyclause = orderby != null && orderby.Length > 0 ? " ORDER BY " + orderby : "";
                    cmd.CommandText = "SELECT * FROM " + (useSquareBrackets ? "[" : "") + sp.Schema + (useSquareBrackets ? "]" : "") + "." + (useSquareBrackets ? "[" : "") + tablename + (useSquareBrackets ? "]" : "") + orderbyclause;
                }
                reader = cmd.ExecuteReader();
                List<List<string>> listRec = new List<List<string>>();
                List<string> listCol = new List<string>();
                Dictionary<string, string> colMetaData = new Dictionary<string, string>();
                bool firstRow = true;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        List<string> listCells = new List<string>();
                        if (firstRow)
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                listCol.Add(reader.GetName(i));
                                colMetaData.Add(reader.GetName(i), reader.GetDataTypeName(i));
                            }
                            firstRow = false;
                            if (writetablestructureonly)
                                break;
                        }
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (reader.IsDBNull(i))
                                listCells.Add("");
                            else
                            {
                                switch (Type.GetTypeCode(reader.GetValue(i).GetType()))
                                {
                                    case TypeCode.String: listCells.Add(reader.GetString(i)); break;
                                    case TypeCode.DateTime: listCells.Add(reader.GetDateTime(i).ToString()); break;
                                    case TypeCode.Int16: listCells.Add(reader.GetInt16(i).ToString()); break;
                                    case TypeCode.Int32: listCells.Add(reader.GetInt32(i).ToString()); break;
                                    case TypeCode.Int64: listCells.Add(reader.GetInt64(i).ToString()); break;
                                    case TypeCode.Double: listCells.Add(reader.GetDouble(i).ToString()); break;
                                    case TypeCode.Decimal: listCells.Add(reader.GetDecimal(i).ToString()); break;
                                    case TypeCode.Single: listCells.Add(reader.GetFloat(i).ToString()); break;
                                    default: listCells.Add(reader.GetString(i)); break;
                                }
                            }
                        }
                        listRec.Add(listCells);
                    }
                }
                else
                {
                    List<string> listCells = new List<string>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        listCol.Add(reader.GetName(i));
                        colMetaData.Add(reader.GetName(i), reader.GetDataTypeName(i));
                    }
                }
                reader.Close();
                cmd.Dispose();

                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                if (!skipheader)
                {
                    for (int i = 0; i < listCol.Count; i++)
                    {
                        if (writetablestructureonly)
                            sw.WriteLine(listCol[i] + " " + colMetaData[listCol[i]]);
                        else
                            sw.Write((i > 0 ? "|" : "") + listCol[i]);
                    }
                    if (listCol.Count > 0)
                        sw.WriteLine();
                }
                for (int i = 0; i < listRec.Count; i++)
                {
                    for (int j = 0; j < listCol.Count; j++)
                    {
                        sw.Write((j > 0 ? "|" : "") + (listRec[i][j] == null ? "" : listRec[i][j]));
                    }
                    sw.WriteLine();
                }
                sw.Flush();
                sw.Close();

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("writetabletofile");
            command.Append(" ").Append("/spacename=").Append(spacename);
            if (tablename != null)
                command.Append(" ").Append("/tablename=").Append(tablename);
            if (writetablestructureonly)
                command.Append(" ").Append("/writeTableStructureOnly");
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            if (orderby != null)
                command.Append(" ").Append("/orderby=").Append(orderby);
            if (query != null)
                command.Append(" ").Append("/query=").Append(query);
            return command.ToString();
        }
    }
}
