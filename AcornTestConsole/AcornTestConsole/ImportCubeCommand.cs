﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using System.IO;
using System.Text.RegularExpressions;

namespace AcornTestConsole
{
    class ImportCubeCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string connname { get; set; }
        public string driver { get; set; }
        public string cubename { get; set; }
        public bool cacheable { get; set; }
        public int importType { get; set; }        

        public ImportCubeCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            importType = Performance_Optimizer_Administration.MainAdminForm.RELATIONAL_DIMENSIONAL_STRUCTURE;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command importcube.");
            spacename = preProcessStringArgument(spacename);
            if (connname == null)
                throw new Exception("connname argument not specified for command importcube.");
            connname = preProcessStringArgument(connname);
            if (driver == null)
                throw new Exception("driver argument not specified for command importcube.");
            driver = preProcessStringArgument(driver);
            if (cubename == null)
                throw new Exception("cubename argument not specified for command importcube.");
            cubename = preProcessStringArgument(cubename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                return Acorn.Utils.BirstConnectUtil.importCubeMetaDataIntoSpace(ATC.u, sp, connname, driver, cubename, importType, cacheable);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("importcube");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/connname=").Append(connname);
            command.Append(" ").Append("/driver=").Append(driver);
            command.Append(" ").Append("/cubename=").Append(cubename);
            if (cacheable)
                command.Append(" ").Append("/cacheable");
            command.Append(" ").Append("/importType=").Append(importType);
            return command.ToString();
        }
    }
}
