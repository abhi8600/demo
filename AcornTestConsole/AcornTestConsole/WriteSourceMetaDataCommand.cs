﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class WriteSourceMetaDataCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string stagingtable { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public WriteSourceMetaDataCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command writesourcemetadata.");
            spacename = preProcessStringArgument(spacename);
            if (stagingtable == null)
                throw new Exception("stagingtable argument not specified for command writesourcemetadata.");
            stagingtable = preProcessStringArgument(stagingtable);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command writesourcemetadata.");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);

                string filename = outputdirectory + "\\" + outputfilename;
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                bool found = false;
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (st.Name.Equals(stagingtable))
                    {
                        found = true;
                        writeTableMetaData(st, filename);
                        break;
                    }
                }
                if (!found)
                    throw new Exception("Staging table " + stagingtable + " not found.");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void writeTableMetaData(StagingTable st, string filename)
        {
            StringBuilder sb = new StringBuilder();
            if (st.Columns != null)
            {
                sb.Append("ColumnName\tDataType\tWidth\tTargetTypes\r\n");
                foreach (StagingColumn sc in st.Columns)
                {
                    sb.Append(sc.Name + "\t" + sc.DataType + "\t" + sc.Width + "\t");
                    if (sc.TargetTypes != null)
                    {
                        Boolean first = true;
                        foreach (String targetType in sc.TargetTypes)
                        {
                            if (first)
                            {
                                sb.Append(targetType);
                                first = false;
                            }
                            else
                                sb.Append(",").Append(targetType);
                        }
                    }
                    sb.Append("\r\n");
                }
                sb.Append("\r\n\r\n");
            }
            File.AppendAllText(filename, sb.ToString());
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("writesourcemetadata");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/stagingtable=").Append(stagingtable);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }
    }
}
