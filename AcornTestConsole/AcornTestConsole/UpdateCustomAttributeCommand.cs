﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class UpdateCustomAttributeCommand : ATC_Command
    {
        //<spacename> <attributename> <hierarchylevel> <formula>
        public string spacename { get; set; }
        public string attributename { get; set; }
        public string hierarchylevel { get; set; }
        public string formula { get; set; }
        public int sqlgentype { get; set; }

        public UpdateCustomAttributeCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            sqlgentype = Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command updatecustomattribute.");
            spacename = preProcessStringArgument(spacename);
            if (attributename == null)
                throw new Exception("attributename argument not specified for command updatecustomattribute.");
            attributename = preProcessStringArgument(attributename);
            if (hierarchylevel == null)
                throw new Exception("hierarchylevel argument not specified for command updatecustomattribute.");
            hierarchylevel = preProcessStringArgument(hierarchylevel);
            if (formula == null)
                throw new Exception("formula argument not specified for command updatecustomattribute.");
            formula = preProcessStringArgument(formula);                     
        }

        override public Nullable<bool> execute()
        {
            FileStream fs = null;
            TextReader reader = null;
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                
                String fName = sp.Directory + "\\repository_dev.xml";
                MainAdminForm maf = new MainAdminForm();
                maf.loadRepositoryFromFile(fName);
                maf.setupTree();

                LogicalExpression[] leExpressions = maf.getLogicalExpressions();
                LogicalExpression leToUpdate = null;
                if (leExpressions != null && leExpressions.Length > 0)
                {
                    List<LogicalExpression> exlist = new List<LogicalExpression>(leExpressions);
                    int count = 0;
                    foreach (LogicalExpression le in exlist)
                    {
                        if (le.Type == LogicalExpression.TYPE_DIMENSION)
                        {
                            if (attributename.Equals(le.Name))
                            {
                                leToUpdate = le;
                                break;
                            }
                        }
                        count++;
                    }
                }
                if (leToUpdate != null)
                {
                    // Get columns and type maps
                    Dictionary<string, string> columnMap = new Dictionary<string, string>();
                    Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                        new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
                    char[] delim = { '.' };
                    String[] levelArray = hierarchylevel.Split(delim);
                    Util.buildColumnAndTypeMapForCustomAttribute(maf, columnMap, typeMap, levelArray);

                    ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(formula, columnMap, typeMap, sqlgentype, true);
                    if (lep.HasError)
                    {
                        throw new Exception(lep.Error);
                    }

                    LogicalExpression curle = new LogicalExpression();
                    curle.Name = attributename;
                    curle.Expression = formula;
                    curle.Levels = new string[][] { levelArray };
                    curle.Type = LogicalExpression.TYPE_DIMENSION;
                    Util.addOrUpdateCustomAttributeForCustomAttribute(false, attributename, maf, levelArray, lep, curle, ATC.u, sp);
                    string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                    Acorn.Util.buildApplication(maf, schema, DateTime.MinValue, -1, null, sp, null);
                    Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                }
                else
                    throw new Exception("Cannot find Attribute " + attributename);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (fs != null)
                    fs.Close();
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("updatecustomattribute");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/attributename=").Append(attributename);
            command.Append(" ").Append("/hierarchylevel=").Append(hierarchylevel);
            command.Append(" ").Append("/formula=").Append(formula);
            command.Append(" ").Append("/sqlgentype=").Append(sqlgentype);
            return command.ToString();
        }
    }
}
