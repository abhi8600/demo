﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class ApplyDCConfigCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string configfile { get; set; }
        public string dcconfigname { get; set; }

        public ApplyDCConfigCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command applyupdcconfig.");
            spacename = preProcessStringArgument(spacename);
            if (configfile == null)
                throw new Exception("configfile argument not specified for command applyupdcconfig.");
            configfile = preProcessStringArgument(configfile);
            dcconfigname = preProcessStringArgument(dcconfigname);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                
                string dcconfigfilename = "Default".Equals(dcconfigname) ? "dcconfig.xml" : dcconfigname + "_" + "dcconfig.xml";
                if (ATC.DatabaseType != ATC.DEFAULT_DATABASE_TYPE)
                {
                    int index = dcconfigfilename.LastIndexOf(".");
                    string DBdcconfigfilename = dcconfigfilename.Substring(0, index) + "_" + ATC.DatabaseType + dcconfigfilename.Substring(index);
                    if (File.Exists(DBdcconfigfilename))
                        dcconfigfilename = DBdcconfigfilename;
                }
                
                File.Copy(configfile, sp.Directory + "\\dcconfig\\" + dcconfigfilename, true);
                DateTime now = DateTime.Now;
                FileInfo fi = new FileInfo(sp.Directory + "\\dcconfig\\" + dcconfigfilename);
                fi.LastWriteTime = now;
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("applydcconfig");
            command.Append(" ").Append("/configfile=").Append(configfile);
            command.Append(" ").Append("/dcconfigname=").Append(dcconfigname);
            return command.ToString();
        }
    }
}
