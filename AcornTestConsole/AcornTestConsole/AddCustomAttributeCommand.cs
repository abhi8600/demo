﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class AddCustomAttributeCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string attributename { get; set; }
        public string hierarchylevel { get; set; }
        public string formula { get; set; }
        public int sqlgentype { get; set; }
                
        public AddCustomAttributeCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            sqlgentype = Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command addcustomattribute.");
            spacename = preProcessStringArgument(spacename);
            if (attributename == null)
                throw new Exception("attributename argument not specified for command addcustomattribute.");
            attributename = preProcessStringArgument(attributename);
            if (hierarchylevel == null)
                throw new Exception("hierarchylevel argument not specified for command addcustomattribute.");
            hierarchylevel = preProcessStringArgument(hierarchylevel);
            if (formula == null)
                throw new Exception("formula argument not specified for command addcustomattribute.");
            formula = preProcessStringArgument(formula);            
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);


                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                bool previousUberTransaction = maf.isInUberTransaction();
                maf.setInUberTransaction(true);

                // Get columns and type maps
                Dictionary<string, string> columnMap = new Dictionary<string, string>();
                Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                    new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
                char[] delim = { '.' };
                string[] levelArray = hierarchylevel.Split(delim);
                Util.buildColumnAndTypeMapForCustomAttribute(maf, columnMap, typeMap, levelArray);

                ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(formula, columnMap, typeMap, sqlgentype, true);
                if (lep.HasError)
                {
                    throw new Exception(lep.Error);                    
                }

                LogicalExpression curle = new LogicalExpression();
                curle.Name = attributename;
                curle.Expression = formula;
                curle.Levels = new string[][] { levelArray };
                curle.Type = LogicalExpression.TYPE_DIMENSION;

                Util.addOrUpdateCustomAttributeForCustomAttribute(true, attributename, maf, levelArray, lep, curle, ATC.u, sp);

                string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                Acorn.Util.buildApplication(maf, schema, DateTime.MinValue, -1, null, sp, null);
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);

                maf.setInUberTransaction(previousUberTransaction);

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }            
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("addcustomattribute");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/attributename=").Append(attributename);
            command.Append(" ").Append("/hierarchylevel=").Append(hierarchylevel);
            command.Append(" ").Append("/formula=").Append(formula);
            command.Append(" ").Append("/sqlgentype=").Append(sqlgentype);
            return command.ToString();
        }
    }
}
