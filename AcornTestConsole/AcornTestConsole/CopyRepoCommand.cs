﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class CopyRepoCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string destinationdir { get; set; }

        public CopyRepoCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            destinationdir = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command copyrepo.");
            spacename = preProcessStringArgument(spacename);
            if (destinationdir == null)
                throw new Exception("destinationdir argument not specified for command copyrepo (nor testresultsdir specified for ATC).");
            destinationdir = preProcessStringArgument(destinationdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                string testFolder = destinationdir + "\\" + sp.Name;
                if (!Directory.Exists(testFolder))
                    Directory.CreateDirectory(testFolder);
                string destPath = testFolder + "\\" + "repository.xml";
                string repPath = sp.Directory + "\\repository_dev.xml";
                File.Copy(repPath, destPath);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("copyrepo");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/destinationdir=").Append(destinationdir);
            return command.ToString();
        }
    }
}
