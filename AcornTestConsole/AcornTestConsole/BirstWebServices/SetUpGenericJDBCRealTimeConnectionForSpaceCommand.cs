﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class SetUpGenericJDBCRealTimeConnectionForSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string configFileName { get; set; }
        public string connectionName { get; set; }
        public bool useDirectConnection { get; set; }
        public string sqlType { get; set; }
        public string driverName { get; set; }
        public string connectionString { get; set; }
        public string filter { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public int timeout { get; set; }

        public SetUpGenericJDBCRealTimeConnectionForSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            configFileName = "dcconfig.xml";            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.SetUpGenericJDBCRealTimeConnectionForSpace");
            spacename = preProcessStringArgument(spacename);
            configFileName = preProcessStringArgument(configFileName);
            connectionName = preProcessStringArgument(connectionName);
            sqlType = preProcessStringArgument(sqlType);
            driverName = preProcessStringArgument(driverName);
            connectionString = preProcessStringArgument(connectionString);
            filter = preProcessStringArgument(filter);
            userName = preProcessStringArgument(userName);
            password = preProcessStringArgument(password);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                return cws.setUpGenericJDBCRealTimeConnectionForSpace(BirstWebServiceBase.Token, sp.ID.ToString(), configFileName, connectionName,
                    useDirectConnection, sqlType, driverName, connectionString, filter, userName, password, timeout);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.SetUpGenericJDBCRealTimeConnectionForSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/configFileName=").Append(configFileName);
            command.Append(" ").Append("/connectionName=").Append(connectionName);
            command.Append(" ").Append("/useDirectConnection=").Append(useDirectConnection);
            command.Append(" ").Append("/sqlType=").Append(sqlType);
            command.Append(" ").Append("/driverName=").Append(driverName);
            command.Append(" ").Append("/connectionString=").Append(connectionString);
            command.Append(" ").Append("/filter=").Append(filter);
            command.Append(" ").Append("/userName=").Append(userName);
            command.Append(" ").Append("/password=").Append(password);
            command.Append(" ").Append("/timeout=").Append(timeout);
            return command.ToString();
        }
    }
}
