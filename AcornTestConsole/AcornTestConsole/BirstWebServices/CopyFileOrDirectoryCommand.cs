﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class CopyFileOrDirectoryCommand : ATC_Command
    {
        public string fromspace { get; set; }
        public string tospace { get; set; }
        public string fileOrDir { get; set; }
        public string toDir { get; set; }

        public CopyFileOrDirectoryCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (fromspace == null)
                throw new Exception("copyfromspace argument not specified for command BirstWebServices.CopyFileOrDirectoryCommand");
            if (tospace == null)
                throw new Exception("copytospace argument not specified for command BirstWebServices.CopyFileOrDirectoryCommand");
            if (fileOrDir == null)
                throw new Exception("fileOrDir argument not specified for command BirstWebServices.CopyFileOrDirectoryCommand");
            if (toDir == null)
                throw new Exception("toDir argument not specified for command BirstWebServices.CopyFileOrDirectoryCommand");
            fromspace = preProcessStringArgument(fromspace);
            tospace = preProcessStringArgument(tospace);
            fileOrDir = preProcessStringArgument(fileOrDir);
            toDir = preProcessStringArgument(toDir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space spFrom = Acorn.Database.getSpace(ATC.conn, fromspace, ATC.u, true, ATC.SCHEMA_NAME);
                if (spFrom == null)
                    throw new Exception("Unable to find the space specified:" + tospace);
                Space spTo = Acorn.Database.getSpace(ATC.conn, tospace, ATC.u, true, ATC.SCHEMA_NAME);
                if (spTo == null)
                    throw new Exception("Unable to find the space specified:" + tospace);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                String token = cws.copyFileOrDirectory(BirstWebServiceBase.Token, spFrom.ID.ToString(), fileOrDir, spTo.ID.ToString(), toDir);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.CopyFileOrDirectory");
            command.Append(" ").Append("/copyfromspace=").Append(fromspace);
            command.Append(" ").Append("/fileOrDir=").Append(fileOrDir);
            command.Append(" ").Append("/copytospace=").Append(tospace);
            command.Append(" ").Append("/toDir=").Append(toDir);
            return command.ToString();
        }

    }
}