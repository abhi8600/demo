﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class RemoveProxyUserCommand : ATC_Command
    {
        public string username { get; set; }
        public string proxyusername { get; set; }
        
        public RemoveProxyUserCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.RemoveProxyUserCommand");
            if (proxyusername == null)
                throw new Exception("proxyusername argument not specified for command BirstWebServices.RemoveProxyUserCommand");
            username = preProcessStringArgument(username);
            proxyusername = preProcessStringArgument(proxyusername);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.removeProxyUser(BirstWebServiceBase.Token, username, proxyusername);
                Console.Out.WriteLine("RemoveProxyUserCommand executed successfully");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.RemoveProxyUser");
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/proxyusername=").Append(proxyusername);
            return command.ToString();
        }
    }
}

