﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ListProxyUsersCommand : ATC_Command
    {
        public string username { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public ListProxyUsersCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.ListProxyUsers");
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.ListProxyUsers");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
            username = preProcessStringArgument(username);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string[] pusers = cws.listProxyUsers(BirstWebServiceBase.Token, username);
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                foreach (string u in pusers)
                {
                    sw.WriteLine(u);
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ListProxyUsers");
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }

    }
}
