﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class CreateNewSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string comments { get; set; }
        public bool advanced { get; set; }

        public CreateNewSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            comments = "";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.CreateNewSpaceCommand");
            spacename = preProcessStringArgument(spacename);
            comments = preProcessStringArgument(comments);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string response = cws.createNewSpace(BirstWebServiceBase.Token, spacename, comments, !advanced);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.CreateNewSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/advanced=").Append(advanced);
            if (comments != null)
                command.Append(" ").Append("/comments=").Append(comments);
            return command.ToString();
        }

    }
}