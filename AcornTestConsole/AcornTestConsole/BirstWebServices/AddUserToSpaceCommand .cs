﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class AddUserToSpaceCommand : ATC_Command
    {
        public string username { get; set; }
        public string spacename { get; set; }
        public bool hasadmin { get; set; }
        public bool hasdesigner { get; set; }
        public bool hasdashboard { get; set; }
        
        public AddUserToSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.AddUserToSpaceCommand");
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.AddUserToSpaceCommand");
            if (!hasadmin && !hasdesigner && !hasdashboard)
                throw new Exception("At least hasAdmin, hasDesigner or hasDashboard should be set for command BirstWebServices.AddUserToSpaceCommand");
            username = preProcessStringArgument(username);
            spacename = preProcessStringArgument(spacename);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.addUserToSpace(BirstWebServiceBase.Token, username, sp.ID.ToString(), hasadmin);
                Console.Out.WriteLine("AddUserToSpaceCommand successful");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.AddUserToSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/hasAdmin=").Append(hasadmin);
            command.Append(" ").Append("/hasDesigner=").Append(hasdesigner);
            command.Append(" ").Append("/hasDashboard=").Append(hasdashboard);
            return command.ToString();
        }

    }
}
