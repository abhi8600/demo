﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ListCreatedUsersCommand : ATC_Command
    {
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public ListCreatedUsersCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.ListCreatedUsers");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string[] users = cws.listCreatedUsers(BirstWebServiceBase.Token);
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                foreach (string u in users)
                {
                    sw.WriteLine(u);
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ListCreatedUsers");
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }

    }
}
