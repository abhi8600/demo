﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class DeleteFileOrDirectoryCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string fileOrDir { get; set; }
        
        public DeleteFileOrDirectoryCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.DeleteFileOrDirectoryCommand");
            if (fileOrDir == null)
                throw new Exception("fileOrDir argument not specified for command BirstWebServices.DeleteFileOrDirectoryCommand");
            spacename = preProcessStringArgument(spacename);
            fileOrDir = preProcessStringArgument(fileOrDir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.deleteFileOrDirectory(BirstWebServiceBase.Token, sp.ID.ToString(), fileOrDir);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.DeleteFileOrDirectory");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/fileOrDir=").Append(fileOrDir);
            return command.ToString();
        }

    }
}