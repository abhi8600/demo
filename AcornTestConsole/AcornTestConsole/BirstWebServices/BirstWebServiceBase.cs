﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcornTestConsole.BirstWebServices
{
    abstract class BirstWebServiceBase
    {
        public static string URL { get; set; }
        public static string Token{ get; set; }
        public static string UploadToken { get; set; }
        public static string PublishToken { get; set; }
    }
}
