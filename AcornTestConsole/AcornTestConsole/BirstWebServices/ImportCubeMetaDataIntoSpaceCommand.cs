﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ImportCubeMetaDataIntoSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string connectionName { get; set; }
        public string databaseType { get; set; }
        public string cubeName { get; set; }
        public int importType { get; set; }
        public bool cacheable { get; set; }

        public ImportCubeMetaDataIntoSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.ImportCubeMetaDataIntoSpace");
            spacename = preProcessStringArgument(spacename);
            connectionName = preProcessStringArgument(connectionName);
            databaseType = preProcessStringArgument(databaseType);
            cubeName = preProcessStringArgument(cubeName);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                return cws.importCubeMetaDataIntoSpace(BirstWebServiceBase.Token, sp.ID.ToString(), connectionName, databaseType, cubeName, importType, cacheable);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }
        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ImportCubeMetaDataIntoSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/connectionName=").Append(connectionName);
            command.Append(" ").Append("/databaseType=").Append(databaseType);
            command.Append(" ").Append("/cubeName=").Append(cubeName);
            command.Append(" ").Append("/importType=").Append(importType);
            command.Append(" ").Append("/cacheable=").Append(cacheable);
            return command.ToString();
        }
    }
}
