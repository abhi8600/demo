﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class DeleteLastDataFromSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public bool waittillfinish { get; set; }
        public int maxwaitseconds { get; set; }
        private int secodswaited = 0;
        
        public DeleteLastDataFromSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.DeleteLastDataFromSpaceCommand");
            spacename = preProcessStringArgument(spacename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string deletetoken = cws.deleteLastDataFromSpace(BirstWebServiceBase.Token, sp.ID.ToString());
                if (waittillfinish)
                {
                    waitTillFinished(cws, deletetoken);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void waitTillFinished(CommandWebService cws, string deletetoken)
        {
            bool isFinished = false;
            do
            {
                if (secodswaited > maxwaitseconds)
                {
                    Console.Error.WriteLine("DeleteLastDataFromSpace giving up on polling data upload, waited for " + secodswaited + " seconds.");
                    throw new Exception("Error while checking finish status for BirstWebServices.DeleteLastDataFromSpace");
                }
                StatusResult result = cws.getJobStatus(BirstWebServiceBase.Token, deletetoken);
                if (result.statusCode == "Complete")
                    isFinished = true;
                if (!isFinished)
                {
                    System.Threading.Thread.Sleep(5 * 1000);
                    secodswaited += 5;
                }
            } while (!isFinished);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.DeleteLastDataFromSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/waittillfinish=").Append(waittillfinish);
            command.Append(" ").Append("/maxwaitseconds=").Append(maxwaitseconds);
            return command.ToString();
        }

    }
}