﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole.BirstWebServices
{
    class LoginCommand : ATC_Command
    {
        public string username { get; set; }
        public string password { get; set; }

        public LoginCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.Login");
            username = preProcessStringArgument(username);
            if (password == null)
                throw new Exception("password argument not specified for command BirstWebServices.Login");
            password = preProcessStringArgument(password);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string token = cws.Login(username, password);
                if (token == null)
                {
                    throw new Exception("Invalid login credentials");                    
                }
                BirstWebServiceBase.Token = token;
                Console.Out.WriteLine("Login successful, token generated : " + token);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.Login");
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/password=").Append(password);
            return command.ToString();
        }
    }
    
}
