﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class PublishDataCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string loadgroup { get; set; }
        public string subgroups { get; set; }
        public bool independentmode { get; set; }
        public string date { get; set; }
        public bool waittillfinish { get; set; }
        public int maxwaitseconds { get; set; }
        private int secodswaited = 0;
        
        public PublishDataCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            maxwaitseconds = 900;//default 15 minutes wait for load
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.PublishData");
            if (loadgroup == null)
                throw new Exception("loadgroup argument not specified for command BirstWebServices.PublishData");
            if (date == null)
                throw new Exception("date argument not specified for command BirstWebServices.PublishData");
            spacename = preProcessStringArgument(spacename);
            loadgroup = preProcessStringArgument(loadgroup);
            subgroups = preProcessStringArgument(subgroups);
            date = preProcessStringArgument(date);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                DateTime publishDate = DateTime.Now;
                bool res = DateTime.TryParse(date, out publishDate);
                if (!res)
                {
                    Console.Out.WriteLine("Unable to parse the datetime specified for publish:" + date + ", Using current time");
                    publishDate = DateTime.Now;
                }
                string[] subgroupArr = null;
                if (subgroups != null)
                {
                    subgroupArr = subgroups.Split(',');
                }
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string pToken = cws.publishData(BirstWebServiceBase.Token, sp.ID.ToString(), subgroupArr, publishDate);
                if (pToken == null)
                {
                    throw new Exception("PublishData request failed");
                }
                BirstWebServiceBase.PublishToken = pToken;
                Console.Out.WriteLine("Publish Data Started, token generated : " + pToken);
                if (waittillfinish)
                {
                    waitTillFinished(cws);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void waitTillFinished(CommandWebService cws)
        {
            bool isFinished = false;
            do
            {
                if (secodswaited > maxwaitseconds)
                {
                    Console.Error.WriteLine("PublishData giving up on polling publish status, waited for " + secodswaited + " seconds.");
                    throw new Exception("Error while checking finish status for BirstWebServices.PublishDataCommand");
                }
                string[] status = cws.getPublishingStatus(BirstWebServiceBase.Token, BirstWebServiceBase.PublishToken);
                if (status.Length == 0)
                    isFinished = true;
                else
                {
                    foreach (string s in status)
                    {
                        if (s != "Running" && s != "None")
                        {
                            isFinished = true;
                            break;
                        }
                    }
                }
                if (!isFinished)
                {
                    System.Threading.Thread.Sleep(5 * 1000);
                    secodswaited += 5;
                }
            } while (!isFinished);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.PublishData");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/independentmode=").Append(independentmode);
            command.Append(" ").Append("/date=").Append(date);
            if (subgroups != null)
                command.Append(" ").Append("/subgroups=").Append(subgroups);
            command.Append(" ").Append("/waittillfinish=").Append(waittillfinish);
            command.Append(" ").Append("/maxwaitseconds=").Append(maxwaitseconds);
            return command.ToString();
        }

    }
}
