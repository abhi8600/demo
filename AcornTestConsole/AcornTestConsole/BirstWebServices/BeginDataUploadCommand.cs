﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class BeginDataUploadCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string sourcename { get; set; }
        public string uploadoptions { get; set; }
        
        public BeginDataUploadCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.BeginDataUpload");
            if (sourcename == null)
                throw new Exception("source argument not specified for command BirstWebServices.BeginDataUpload");
            spacename = preProcessStringArgument(spacename);
            sourcename = preProcessStringArgument(sourcename);
            uploadoptions = preProcessStringArgument(uploadoptions);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string uToken = cws.beginDataUpload(BirstWebServiceBase.Token, sp.ID.ToString(), sourcename);
                if (uToken == null)
                {
                    throw new Exception("Dataupload begin request failed");
                }
                BirstWebServiceBase.UploadToken = uToken;
                string[] optionsArr = null;
                if (uploadoptions != null)
                {
                    optionsArr = uploadoptions.Split(',');
                    cws.setDataUploadOptions(BirstWebServiceBase.Token, uToken, optionsArr);
                }
                Console.Out.WriteLine("Data Upload Started, token generated : " + uToken);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.BeginDataUpload");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/sourcename=").Append(sourcename);
            if (uploadoptions != null)
                command.Append(" ").Append("/uploadoptions=").Append(uploadoptions);
            return command.ToString();
        }

    }
}
