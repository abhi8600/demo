﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ExecuteQueryInSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string query { get; set; }
        public string queryfile { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public ExecuteQueryInSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.ExecuteQueryInSpace");
            if (query == null && queryfile == null)
                throw new Exception("Atleast query/queryfile argument should be specified for command BirstWebServices.ExecuteQueryInSpace");
            spacename = preProcessStringArgument(spacename);
            queryfile = preProcessStringArgument(queryfile);
            query = preProcessStringArgument(query);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.ExecuteQueryInSpace.");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);

                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                if (queryfile != null)
                {
                    FileStream usercmdfile = new FileStream(queryfile, FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(usercmdfile);
                    String line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith("#") || line.StartsWith("//") || line.Trim().Equals(""))
                            continue;
                        executeQuery(cws, line, sp.ID.ToString(), sw);
                    }
                }
                else
                {
                    executeQuery(cws, query, sp.ID.ToString(), sw);
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void executeQuery(CommandWebService cws, string query, string spaceid, StreamWriter sw)
        {
            CommandQueryResult cqr = cws.executeQueryInSpace(BirstWebServiceBase.Token, query, spaceid);
            writeRowsToFile(cqr, sw, true);
            string queryToken = cqr.queryToken;
            while (cqr.hasMoreRows)
            {
                cqr = cws.queryMore(BirstWebServiceBase.Token, queryToken);
                writeRowsToFile(cqr, sw, false);
            }
        }

        private void writeRowsToFile(CommandQueryResult cqr, StreamWriter sw, bool printHeaders)
        {
            if (printHeaders)
            {
                foreach (string colname in cqr.columnNames)
                {
                    sw.Write(colname);
                    sw.Write("\t");
                }
                sw.Write(sw.NewLine);
            }

            string[][] results = cqr.rows;
            foreach (string[] rows in results)
            {
                foreach (string col in rows)
                {
                    sw.Write(col);
                    sw.Write("\t");
                }
                sw.Write(sw.NewLine);
            }

            sw.Write(sw.NewLine);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ExecuteQueryInSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/query=").Append(query);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }

    }
}
