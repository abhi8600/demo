﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class GetVariablesForSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public GetVariablesForSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.GetVariablesForSpace");
            spacename = preProcessStringArgument(spacename);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.GetVariablesForSpace.");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);

                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string[][] variables = cws.getVariablesForSpace(BirstWebServiceBase.Token, sp.ID.ToString());
                
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                sw.WriteLine("Variables in Space : " + sp.ID.ToString() + "\r\n");
                for (int i = 0; i < variables.Length; i++)
                    sw.WriteLine(variables[i][0] + " = " + variables[i][1]);
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.GetVariablesForSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            return command.ToString();
        }

    }
}
