﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class RemoveUserFromSpaceCommand : ATC_Command
    {
        public string username { get; set; }
        public string spacename { get; set; }
        
        public RemoveUserFromSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.RemoveUserFromSpaceCommand");
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.RemoveUserFromSpaceCommand");
            username = preProcessStringArgument(username);
            spacename = preProcessStringArgument(spacename);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.removeUserFromSpace(BirstWebServiceBase.Token, username, sp.ID.ToString());
                Console.Out.WriteLine("RemoveUserFromSpaceCommand successful");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.RemoveUserFromSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/username=").Append(username);
            return command.ToString();
        }

    }
}
