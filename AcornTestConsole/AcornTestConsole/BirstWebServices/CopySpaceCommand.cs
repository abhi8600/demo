﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;
using System.IO;
using System.Web.SessionState;
using Acorn.Utils;
using Acorn.Background;


namespace AcornTestConsole.BirstWebServices
{
    class CopySpaceCommand : ATC_Command
    {
        public string fromspacename { get; set; }
        public string tospacename { get; set; }
        public string mode { get; set; }
        public string options { get; set; }
        public string token { get; set; }

        static private string[] allOptions = { "repository", "settings-basic", "settings-rare", "settings-sso", "settings-other", "settings-permissions", "settings-membership",
                                                 "birst-connect", "custom-subject-areas", "custom-subject-areas-no-permissions", "dashboardstyles", "salesforce", "catalog",
                                                 "datastore-warehouse_tables", "datastore-warehouse", "datastore",
                                                 "datastore-staging_tables", "datastore-staging",
                                                 "datastore-warehouse_indices", "datastore-staging_indices",
                                                 "CustomGeoMaps.xml", "DrillMaps.xml", "spacesettings.xml", "SavedExpressions.xml",
                                                 "attachments", "data", "logs", "slogs", "archive", "output", "connectors",
                                                 "birst-connect:", "custom-subject-areas:", "custom-subject-areas-no-permissions:", "catalog:", "connectors:","useunloadfeature" };

        public CopySpaceCommand(Program ATC)
            : base(ATC)
        {
        }
        public override void setDefaultArguments()
        {

        }

        public override void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (fromspacename == null)
                throw new Exception("fromspacename not specified for copyspace");
            if (tospacename == null)
                throw new Exception("tospacename not specified for copyspace");
            fromspacename = preProcessStringArgument(fromspacename);
            tospacename = preProcessStringArgument(tospacename);
        }

        public override bool? execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space oldsp = Acorn.Database.getSpace(ATC.conn, fromspacename, ATC.u, true, ATC.SCHEMA_NAME);
                Space newsp = Acorn.Database.getSpace(ATC.conn, tospacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (oldsp == null || newsp == null)
                    throw new Exception("Copy Space : From Space or To Space not created.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                Console.WriteLine("Submitting a new Job Using External Web Services for Copy Space");
                String jobToken = cws.copySpace(BirstWebServiceBase.Token, oldsp.ID.ToString(), newsp.ID.ToString(), "replicate", string.Join(";",allOptions));
                Console.WriteLine("Job Token Received : " + jobToken);
                StatusResult sr = cws.getJobStatus(BirstWebServiceBase.Token, jobToken);
                while (sr.statusCode != "Complete")
                {
                    System.Threading.Thread.Sleep(60000);
                    Console.WriteLine(".");
                    sr = cws.getJobStatus(BirstWebServiceBase.Token, jobToken);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        public override string ToString()
        {
            StringBuilder command = new StringBuilder();
            command.Append("birstwebservices.copyspace").Append(" ");
            command.Append("/fromspacename=").Append(fromspacename);
            command.Append(" ").Append("/tospacename=").Append(tospacename);
            return command.ToString();
        }
    }
}