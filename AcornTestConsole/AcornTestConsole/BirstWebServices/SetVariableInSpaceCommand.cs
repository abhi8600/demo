﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class SetVariableInSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string variablename { get; set; }
        public string query { get; set; }

        public SetVariableInSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.SetVariableInSpace");
            if (variablename == null)
                throw new Exception("variablename argument not specified for command BirstWebServices.SetVariableInSpace");
            if (query == null)
                throw new Exception("query argument not specified for command BirstWebServices.SetVariableInSpace");
            spacename = preProcessStringArgument(spacename);
            variablename = preProcessStringArgument(variablename);
            query = preProcessStringArgument(query);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.setVariableInSpace(BirstWebServiceBase.Token, variablename, query, sp.ID.ToString());
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.SetVariableInSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/variablename=").Append(variablename);
            command.Append(" ").Append("/query=").Append(query);
            return command.ToString();
        }

    }
}
