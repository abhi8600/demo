﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class SetUpRealTimeConnectionForSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string configFileName { get; set; }
        public string connectionName { get; set; }
        public string databaseType { get; set; }
        public bool useDirectConnection { get; set; }
        public string host { get; set; }
        public int port { get; set; }
        public string databaseName { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public int timeout { get; set; }

         public SetUpRealTimeConnectionForSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            configFileName = "dcconfig.xml";            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.SetUpRealTimeConnectionForSpace");
            spacename = preProcessStringArgument(spacename);
            configFileName = preProcessStringArgument(configFileName);
            connectionName = preProcessStringArgument(connectionName);
            databaseType = preProcessStringArgument(databaseType);
            host = preProcessStringArgument(host);
            databaseName = preProcessStringArgument(databaseName);
            userName = preProcessStringArgument(userName);
            password = preProcessStringArgument(password);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                return cws.setUpRealTimeConnectionForSpace(BirstWebServiceBase.Token, sp.ID.ToString(), configFileName, connectionName, databaseType,
                    useDirectConnection, host, port, databaseName, userName, password, timeout);                
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.SetUpRealTimeConnectionForSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/configFileName=").Append(configFileName);
            command.Append(" ").Append("/connectionName=").Append(connectionName);
            command.Append(" ").Append("/useDirectConnection=").Append(useDirectConnection);
            command.Append(" ").Append("/databaseType=").Append(databaseType);
            command.Append(" ").Append("/host=").Append(host);
            command.Append(" ").Append("/port=").Append(port);
            command.Append(" ").Append("/databaseName=").Append(databaseName);
            command.Append(" ").Append("/userName=").Append(userName);
            command.Append(" ").Append("/password=").Append(password);
            command.Append(" ").Append("/timeout=").Append(timeout);
            return command.ToString();
        }
    }
}