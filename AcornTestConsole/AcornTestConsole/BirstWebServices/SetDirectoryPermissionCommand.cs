﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class SetDirectoryPermissionCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string directoryname { get; set; }
        public string groupname { get; set; }
        public string permissionname { get; set; }
        public bool permission { get; set; }
        
        public SetDirectoryPermissionCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.SetDirectoryPermissionCommand");
            if (directoryname == null)
                throw new Exception("directoryname argument not specified for command BirstWebServices.SetDirectoryPermissionCommand");
            if (groupname == null)
                throw new Exception("groupname argument not specified for command BirstWebServices.SetDirectoryPermissionCommand");
            if (permissionname == null)
                throw new Exception("permissionname argument not specified for command BirstWebServices.SetDirectoryPermissionCommand");
            spacename = preProcessStringArgument(spacename);
            directoryname = preProcessStringArgument(directoryname);
            groupname = preProcessStringArgument(groupname);
            permissionname = preProcessStringArgument(permissionname);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.setDirectoryPermission(BirstWebServiceBase.Token, sp.ID.ToString(), directoryname, groupname, permissionname, permission);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.SetDirectoryPermission");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/directoryname=").Append(directoryname);
            command.Append(" ").Append("/groupname=").Append(groupname);
            command.Append(" ").Append("/permissionname=").Append(permissionname);
            command.Append(" ").Append("/permission=").Append(permission);
            return command.ToString();
        }
    }
}

