﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Xml;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ListSpacesCommand : ATC_Command
    {
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public ListSpacesCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.ListSpaces");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                UserSpace[] spaces = cws.listSpaces(BirstWebServiceBase.Token);
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                Stream strm = File.Create(filename);
                StreamWriter sw = new StreamWriter(strm);
                foreach (UserSpace space in spaces)
                {
                    sw.WriteLine(space.name);
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ListSpaces");
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }

    }
}
