﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class RemoveUserFromGroupInSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string groupname { get; set; }
        public string username  { get; set; }
        
        public RemoveUserFromGroupInSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.RemoveUserFromGroupInSpaceCommand");
            if (groupname == null)
                throw new Exception("groupname argument not specified for command BirstWebServices.RemoveUserFromGroupInSpaceCommand");
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.RemoveUserFromGroupInSpaceCommand");
            spacename = preProcessStringArgument(spacename);
            groupname = preProcessStringArgument(groupname);
            username = preProcessStringArgument(username);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.removeUserFromGroupInSpace(BirstWebServiceBase.Token, username, groupname, sp.ID.ToString());
                Console.Out.WriteLine("RemoveUserFromGroupInSpaceCommand executed successfully");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.RemoveUserFromGroupInSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/groupname=").Append(groupname);
            command.Append(" ").Append("/username=").Append(username);
            return command.ToString();
        }

    }
}
