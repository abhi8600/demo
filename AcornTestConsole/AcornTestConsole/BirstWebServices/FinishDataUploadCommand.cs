﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class FinishDataUploadCommand : ATC_Command
    {
        public bool waittillfinish { get; set; }
        public int maxwaitseconds { get; set; }
        private int secodswaited = 0;

        public FinishDataUploadCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            maxwaitseconds = 900;//default 15 minutes wait for load
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                if (BirstWebServiceBase.UploadToken == null)
                    throw new Exception("Valid Begin Upload Token not found for BirstWebServices acess");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.finishDataUpload(BirstWebServiceBase.Token, BirstWebServiceBase.UploadToken);

                if (waittillfinish)
                {
                    waitTillFinished(cws);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void waitTillFinished(CommandWebService cws)
        {
            bool isFinished = false;
            do
            {
                if (secodswaited > maxwaitseconds)
                {
                    Console.Error.WriteLine("FinishDataUpload giving up on polling data upload, waited for " + secodswaited + " seconds.");
                    throw new Exception("Error while checking finish status for BirstWebServices.FinishDataUploadCommand");
                }
                string[] status = cws.getDataUploadStatus(BirstWebServiceBase.Token, BirstWebServiceBase.UploadToken);
                if (status.Length == 0)
                    isFinished = true;
                else
                {
                    foreach (string s in status)
                    {
                        if (s != "Upload is not complete")
                        {
                            isFinished = true;
                            break;
                        }
                    }
                }
                if (!isFinished)
                {
                    System.Threading.Thread.Sleep(5 * 1000);
                    secodswaited += 5;
                }
            } while (!isFinished);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.FinishDataUpload");
            command.Append(" ").Append("/waittillfinish=").Append(waittillfinish);
            command.Append(" ").Append("/maxwaitseconds=").Append(maxwaitseconds);
            return command.ToString();
        }
    }
}
