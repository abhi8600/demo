﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Xml;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class GetDirectoryPermissionsCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string directoryname { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public GetDirectoryPermissionsCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.GetDirectoryPermissions");
            spacename = preProcessStringArgument(spacename);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.GetDirectoryPermissions.");
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
            directoryname = preProcessStringArgument(directoryname);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);

                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                GroupPermission[] gp = cws.getDirectoryPermissions(BirstWebServiceBase.Token, sp.ID.ToString(), directoryname);
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                Stream strm = File.Create(filename);
                StreamWriter sw = new StreamWriter(strm);
                sw.WriteLine("GroupName" + "\t" + "CanModify" +  "\t" + "CanView");
                foreach (GroupPermission g in gp)
                {
                    sw.WriteLine(g.groupName + "\t" + g.canModify + "\t" + g.canView);
                }
                sw.Flush();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.GetDirectoryPermissions");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/directoryname=").Append(directoryname);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }
    }
}
