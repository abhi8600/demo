﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class CopySpaceContentsCommand : ATC_Command
    {
        public string copyfromspace { get; set; }
        public string copytospace { get; set; }
        
        public CopySpaceContentsCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (copyfromspace == null)
                throw new Exception("copyfromspace argument not specified for command BirstWebServices.CopySpaceContentsCommand");
            if (copytospace == null)
                throw new Exception("copytospace argument not specified for command BirstWebServices.CopySpaceContentsCommand");
            copyfromspace = preProcessStringArgument(copyfromspace);
            copytospace = preProcessStringArgument(copytospace);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space spFrom = Acorn.Database.getSpace(ATC.conn, copyfromspace, ATC.u, true, ATC.SCHEMA_NAME);
                if (spFrom == null)
                    throw new Exception("Unable to find the space specified:" + copyfromspace);
                Space spTo = Acorn.Database.getSpace(ATC.conn, copytospace, ATC.u, true, ATC.SCHEMA_NAME);
                if (spTo == null)
                    throw new Exception("Unable to find the space specified:" + copytospace);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.copySpaceContents(BirstWebServiceBase.Token, spFrom.ID.ToString(), spTo.ID.ToString());
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.CopySpaceContents");
            command.Append(" ").Append("/copyfromspace=").Append(copyfromspace);
            command.Append(" ").Append("/copytospace=").Append(copytospace);
            return command.ToString();
        }

    }
}