﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class SwapSpaceContentsCommand : ATC_Command
    {
        public string spacename1 { get; set; }
        public string spacename2 { get; set; }
        
        public SwapSpaceContentsCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename1 == null)
                throw new Exception("spacename1 argument not specified for command BirstWebServices.SwapSpaceContentsCommand");
            if (spacename2 == null)
                throw new Exception("spacename2 argument not specified for command BirstWebServices.SwapSpaceContentsCommand");
            spacename1 = preProcessStringArgument(spacename1);
            spacename2 = preProcessStringArgument(spacename2);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp1 = Acorn.Database.getSpace(ATC.conn, spacename1, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp1 == null)
                    throw new Exception("Unable to find the space specified:" + spacename1);
                Space sp2 = Acorn.Database.getSpace(ATC.conn, spacename2, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp2 == null)
                    throw new Exception("Unable to find the space specified:" + spacename2);

                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.swapSpaceContents(BirstWebServiceBase.Token, sp1.ID.ToString(), sp2.ID.ToString());
                Console.Out.WriteLine("Swap space contents successful");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.SwapSpaceContents");
            command.Append(" ").Append("/spacename1=").Append(spacename1);
            command.Append(" ").Append("/spacename2=").Append(spacename2);
            return command.ToString();
        }

    }
}
