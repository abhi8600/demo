﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class RemoveAllowedIpCommand : ATC_Command
    {
        public string username { get; set; }
        public string ip { get; set; }
        
        public RemoveAllowedIpCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.RemoveAllowedIpCommand");
            if (ip == null)
                throw new Exception("ip argument not specified for command BirstWebServices.RemoveAllowedIpCommand");
            username = preProcessStringArgument(username);
            ip = preProcessStringArgument(ip);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.removeAllowedIp(BirstWebServiceBase.Token, username, ip);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.RemoveAllowedIp");
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/ip=").Append(ip);
            return command.ToString();
        }
    }
}

