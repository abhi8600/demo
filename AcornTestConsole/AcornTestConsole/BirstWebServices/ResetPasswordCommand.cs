﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ResetPasswordCommand : ATC_Command
    {
        public string username { get; set; }
        
        public ResetPasswordCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.ResetPasswordCommand");
            username = preProcessStringArgument(username);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.resetPassword(BirstWebServiceBase.Token, username);
                Console.Out.WriteLine("ResetPasswordCommand executed successfully");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ResetPassword");
            command.Append(" ").Append("/username=").Append(username);
            return command.ToString();
        }

    }
}
