﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class UploadDataCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string sourcefile { get; set; }
        
        public UploadDataCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.UploadData");
            if (sourcefile == null)
                throw new Exception("source argument not specified for command BirstWebServices.UploadData");
            spacename = preProcessStringArgument(spacename);
            sourcefile = preProcessStringArgument(sourcefile);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                if (BirstWebServiceBase.UploadToken == null)
                    throw new Exception("Valid Begin Upload Token not found for BirstWebServices acess");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                FileStream fs = new FileStream(sourcefile, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(sourcefile).Length;
                byte[] buff = br.ReadBytes((int)numBytes);

                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.uploadData(BirstWebServiceBase.Token, BirstWebServiceBase.UploadToken, (int)numBytes, buff);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.UploadData");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/sourcefile=").Append(sourcefile);
            return command.ToString();
        }

    }
}
