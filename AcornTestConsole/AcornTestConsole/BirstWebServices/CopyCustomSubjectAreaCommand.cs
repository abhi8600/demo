﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class CopyCustomSubjectAreaCommand : ATC_Command
    {
        public string fromspacename { get; set; }
        public string customsubjectareaname { get; set; }
        public string tospacename { get; set; }

        public CopyCustomSubjectAreaCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (fromspacename == null)
                throw new Exception("fromspacename argument not specified for command BirstWebServices.CopyCustomSubjectAreaCommand");
            if (customsubjectareaname == null)
                throw new Exception("customsubjectareaname argument not specified for command BirstWebServices.CopyCustomSubjectAreaCommand");
            if (tospacename == null)
                throw new Exception("tospacename argument not specified for command BirstWebServices.CopyCustomSubjectAreaCommand");
            fromspacename = preProcessStringArgument(fromspacename);
            customsubjectareaname = preProcessStringArgument(customsubjectareaname);
            tospacename = preProcessStringArgument(tospacename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space spfrom = Acorn.Database.getSpace(ATC.conn, fromspacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (spfrom == null)
                    throw new Exception("Unable to find the space specified:" + fromspacename);
                Space spto = Acorn.Database.getSpace(ATC.conn, tospacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (spto == null)
                    throw new Exception("Unable to find the space specified:" + tospacename);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.copyCustomSubjectArea(BirstWebServiceBase.Token, spfrom.ID.ToString(), customsubjectareaname, spto.ID.ToString());
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.CopyCustomSubjectArea");
            command.Append(" ").Append("/fromspacename=").Append(fromspacename);
            command.Append(" ").Append("/tospacename=").Append(tospacename);
            command.Append(" ").Append("/customsubjectareaname=").Append(customsubjectareaname);
            return command.ToString();
        }

    }
}