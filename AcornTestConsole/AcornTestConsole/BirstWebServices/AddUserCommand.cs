﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class AddUserCommand : ATC_Command
    {
        public string username { get; set; }
        public string parameters { get; set; }
        
        public AddUserCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (username == null)
                throw new Exception("username argument not specified for command BirstWebServices.AddUserCommand");
            if (parameters == null)
                throw new Exception("parameters argument not specified for command BirstWebServices.AddUserCommand");
            username = preProcessStringArgument(username);
            parameters = preProcessStringArgument(parameters);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                cws.addUser(BirstWebServiceBase.Token, username, parameters);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.AddUser");
            command.Append(" ").Append("/username=").Append(username);
            command.Append(" ").Append("/parameters=").Append(parameters);
            return command.ToString();
        }

    }
}
