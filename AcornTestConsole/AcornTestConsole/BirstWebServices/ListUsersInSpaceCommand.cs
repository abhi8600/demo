﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Acorn;

namespace AcornTestConsole.BirstWebServices
{
    class ListUsersInSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public ListUsersInSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command BirstWebServices.ListUsersInSpace");
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command BirstWebServices.ListUsersInSpace");
            spacename = preProcessStringArgument(spacename);
            outputfilename = preProcessStringArgument(outputfilename);
            outputdirectory = preProcessStringArgument(outputdirectory);
        }
        
        override public Nullable<bool> execute()
        {
            try
            {
                if (BirstWebServiceBase.Token == null)
                    throw new Exception("Valid Token not found for BirstWebServices access.");
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (outputdirectory == null)
                    throw new Exception("No output folder is specified for results");
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                outputdirectory += "\\" + sp.Name;
                if (!Directory.Exists(outputdirectory))
                    Directory.CreateDirectory(outputdirectory);
                CommandWebService cws = new CommandWebService();
                cws.Url = BirstWebServiceBase.URL;
                string[] unames = cws.listUsersInSpace(BirstWebServiceBase.Token, sp.ID.ToString());
                string filename = outputdirectory + "\\" + outputfilename;
                if (File.Exists(filename))
                    File.Delete(filename);
                StreamWriter sw = File.CreateText(filename);
                foreach (string name in unames)
                {
                    sw.WriteLine(name);
                }
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("BirstWebServices.ListUsersInSpace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }

    }
}
