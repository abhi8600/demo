﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Text.RegularExpressions;

namespace AcornTestConsole
{
    class CompareResultsCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string referencedir { get; set; }
        public string outputfilename { get; set; }
        public string resultdir { get; set; }
        public string filetype { get; set; }
        public bool ignorecase { get; set; }

        public bool isbincomparison { get; set; } // Flag to allow FC /B option to compare zip files etc
        public bool iszipfolder { get; set; } //allow result folder to have multiple zips which are extracted and then compared


        public CompareResultsCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            filetype = "*.txt";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command compareresults.");
            spacename = preProcessStringArgument(spacename);
            if (referencedir == null)
                throw new Exception("referencedir argument not specified for command compareresults.");
            referencedir = preProcessStringArgument(referencedir);
            if (outputfilename == null)
                throw new Exception("outputfilename argument not specified for command compareresults.");
            outputfilename = preProcessStringArgument(outputfilename);
            if (resultdir == null)
                throw new Exception("resultdir argument not specified for command compareresults.");
            resultdir = preProcessStringArgument(resultdir);
            filetype = preProcessStringArgument(filetype);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                string resDir = resultdir + "\\" + sp.Name;
                if (!Directory.Exists(resDir))
                    Directory.CreateDirectory(resDir);

                string refDir = referencedir;
                if (CultureInfo.CurrentCulture.Name.Equals("en-US") && Directory.Exists(refDir + "_en_US"))
                    refDir += "_en_US";
                string origRefDir = refDir;
                if (ATC.DatabaseType != ATC.DEFAULT_DATABASE_TYPE && Directory.Exists(refDir + "_" + ATC.DatabaseType))
                {
                    refDir += "_" + ATC.DatabaseType;
                }

				if(refDir != origRefDir)
				{
					//Iterate over all files in origRefDir, see if the same file exists in refDir. If it doesn't exist, copy it over. This can be achieved with
					//copy file with override option = false.
                    
                    DirectoryInfo di = new DirectoryInfo(origRefDir);
                    foreach (FileInfo fi in di.GetFiles())
                    {
                        if (!File.Exists(refDir + "\\" + fi.Name))
                            File.Copy(origRefDir + "\\" + fi.Name, refDir + "\\" + fi.Name, false);
                    }
				}
                //Iterate over all files in resDir, see if any file that exists in refDir is not generated in resDir. If it doesn't exist, create empty file.
                //This is useful when load fails and ATC execution is aborted, it generates empty actual results file to show diff.
                DirectoryInfo rdi = new DirectoryInfo(refDir);
                foreach (FileInfo fi in rdi.GetFiles())
                {
                    if (!File.Exists(resDir + "\\" + fi.Name))
                    {
                        StreamWriter writer = File.CreateText(resDir + "\\" + fi.Name);
                        writer.WriteLine("-------------- Dummy empty actual result file created by ATC to show diff, check error console for more details -------------");
                        writer.Flush();
                        writer.Close();
                    }
                }

                // Let's create a temp file to list down the list of files to compare
                String cmpBatFile = ATC.tempfolder +  "\\acorntest.comparefiles.bat";
                File.Delete(cmpBatFile);
                FileStream file = new FileStream(cmpBatFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                String cmpAppendBatFile = ATC.tempfolder + "\\acorntest.compareallfiles.bat";
                FileStream appFile = new FileStream(cmpAppendBatFile, FileMode.Append, FileAccess.Write);
                StreamWriter appSW = new StreamWriter(appFile);

                string diffOutFile = outputfilename;
                FileInfo fInfo = new FileInfo(outputfilename);
                string dirForATI = fInfo.Directory.Name;

                if (iszipfolder)
                {
                    extractZipDirectory(resDir);
                        
                }
                foreach (string fpath in getCompareFileList(refDir, resDir, null, filetype, dirForATI, ignorecase))
                {
                 if (isbincomparison)
                    {
                        sw.WriteLine("fc /B " + fpath);
                        appSW.WriteLine("fc /B " + fpath);
                    }
                    else
                    {
                        sw.WriteLine("fc /N " + (ignorecase ? "/C " : "") + fpath);
                        appSW.WriteLine("fc /N " + (ignorecase ? "/C " : "") + fpath);
                    }

                }
                sw.Close();
                appSW.Close();
                file.Close();
                appFile.Close();

   
      

                ProcessStartInfo startInfo;
                if (!File.Exists(diffOutFile))
                    startInfo = new ProcessStartInfo(cmpBatFile, " > \"" + diffOutFile + "\"");
                else
                    startInfo = new ProcessStartInfo(cmpBatFile, " >> \"" + diffOutFile + "\"");
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                Process process;
                process = Process.Start(startInfo);
                Console.Out.WriteLine("Waiting for the compare util to be closed");
                while (!process.WaitForExit(10000))
                    Console.Out.Write(".");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private static List<String> getCompareFileList(String refPath, String resPath, List<String> buffer, String pattern,String dirForATI,bool ignoreCase)
        {
            if (buffer == null)
                buffer = new List<String>();
            DirectoryInfo dir = new DirectoryInfo(refPath);
            DirectoryInfo resDir = new DirectoryInfo(resPath);
            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                if (di.Name.ToLower().Equals("cvs"))
                    continue; // ignore all the directories with cvs as name
                // find the directory in the results path
                DirectoryInfo[] res2 = resDir.GetDirectories(di.Name);
                if (res2.Length == 0)
                {
                    Console.Error.WriteLine("COMPARE ERROR: Unable to find the matching directory for:" + di.FullName + " in the test output folder, which is expected to have created");
                    continue; // nothing to compare
                }
                // recursive call
                buffer = getCompareFileList(di.FullName, res2[0].FullName, buffer, pattern, dirForATI,ignoreCase);
            }
            FileInfo[] filesToCompare = dir.GetFiles(pattern);
            foreach (FileInfo fi in filesToCompare)
            {
                FileInfo[] resFiles = resDir.GetFiles(fi.Name);
                if (resFiles.Length == 0)
                {
                    Console.Error.WriteLine("COMPARE ERROR: Unable to find the matching file for:" + fi.FullName + " in the test output folder, which is expected to have created");
                    continue; // nothing to compare
                }
                buffer.Add("\"" + fi.FullName + "\" \"" + resFiles[0].FullName + "\"");
                //Append to ATCTestNGIntegration.xml file to be used later by ATI
                createOrAppendATIConfiguration(Path.Combine(dirForATI, "ATCTestNGIntegration.xml"), getTestSuiteName(refPath), fi.FullName, resFiles[0].FullName, ignoreCase);
            }
            return buffer;
        }

        private static void  extractZipDirectory(String resultDirectory){
          
            string[] fileEntries = Directory.GetFiles(resultDirectory);
    
            foreach(string fileName in fileEntries)
            {
                ExtractZipFile(fileName, resultDirectory);
            }
          
          
        }

        public static void ExtractZipFile(string archiveFilenameIn, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
             

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

             
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

             
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }catch(Exception){//Ignoring cannot find central directory
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; 
                    zf.Close(); 
                }
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("compareresults");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/referencedir=").Append(referencedir);
            command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            command.Append(" ").Append("/resultdir=").Append(resultdir);
            command.Append(" ").Append("/ignorecase=").Append(ignorecase);
            command.Append(" ").Append("/iszipfolder=").Append(iszipfolder);
            return command.ToString();
        }


        private static void createOrAppendATIConfiguration(string atiConfigFilePath,string testSuiteName, string expectedOutputFile, string actualOutputFile, bool ignoreCase)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement testSuiteList = null;
            XmlElement atiTestSuiteElement = null;
            if (!File.Exists(atiConfigFilePath))
            {
                testSuiteList = doc.CreateElement("TestSuiteList");
            }
            else
            {
                doc.Load(atiConfigFilePath);
                testSuiteList = doc.DocumentElement;
            }
            atiTestSuiteElement = doc.CreateElement("TestSuite");
            XmlNode nodeTestSuiteName = doc.CreateNode(XmlNodeType.Element, "TestSuiteName", "");
            nodeTestSuiteName.InnerText = testSuiteName;
            atiTestSuiteElement.AppendChild(nodeTestSuiteName);
            XmlNode nodeTestSuiteName2 = doc.CreateNode(XmlNodeType.Element, "ExpectedFile", "");
            nodeTestSuiteName2.InnerText = expectedOutputFile;
            atiTestSuiteElement.AppendChild(nodeTestSuiteName2);
            XmlNode nodeTestSuiteName3 = doc.CreateNode(XmlNodeType.Element, "ActualFile", "");
            nodeTestSuiteName3.InnerText = actualOutputFile;
            atiTestSuiteElement.AppendChild(nodeTestSuiteName3);

            XmlNode ignoreCaseNode = doc.CreateNode(XmlNodeType.Element, "IsIgnoreCase", "");
            ignoreCaseNode.InnerText = ignoreCase.ToString();
            atiTestSuiteElement.AppendChild(ignoreCaseNode);

            testSuiteList.AppendChild(atiTestSuiteElement);
            doc.AppendChild(testSuiteList);
            using (TextWriter sw = new StreamWriter(atiConfigFilePath, false, Encoding.UTF8))
            {
                doc.Save(sw);
            }
        }

        static string getTestSuiteName(string path)
        {
            DirectoryInfo dirN = new DirectoryInfo(path);
            try
            {
                if (!path.ToLower().Contains("\\test queries\\"))
                {
                    return dirN.Parent.Name;
                }
                else
                {
                    //We could have nested folders in a testsuite in that case treat them as seperate test suites
                    string testSuiteName = path.Substring(path.ToLower().IndexOf("\\test queries\\") + 14);
                    testSuiteName = Regex.Replace(testSuiteName, "[^0-9A-Za-z]+", "_");
                    return testSuiteName.Trim('_');
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not determine a valid test suiteName ", ex.Message);
                return dirN.Parent.Name;
            }
        }
    }
}
