﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class RemovePublishedCommand : ATC_Command
    {
        public string spacename { get; set; }
        public bool deleteall { get; set; }
        public bool restoresettings { get; set; }

        public RemovePublishedCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command removepublished.");
            spacename = preProcessStringArgument(spacename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                List<string> launchedBirstConnects = new List<string>();
                if (Util.spaceContainsLocalSource(maf))
                {
                    //need to launch birstconnect (for all birstconnect configs) when deleting space
                    string[] configNames = Util.getDCConfigNames(sp);
                    if (configNames != null && configNames.Length > 0)
                    {
                        foreach (string configName in configNames)
                        {
                            if (!ATC.bcCmdLines.ContainsKey(sp.Name + "_" + configName))
                            {
                                LaunchBirstConnectCommand cmd = new LaunchBirstConnectCommand(ATC);
                                cmd.setDefaultArguments();
                                cmd.processCommandArguments(new string[] { "/spacename=" + sp.Name, "/dcconfigname=" + configName });
                                cmd.execute();
                                launchedBirstConnects.Add(configName);
                            }
                        }
                    }
                }
                Acorn.DeleteDataService.deleteData(sp, deleteall, restoresettings, null, ATC.u, new DeleteDataOutput());
                Console.WriteLine("Waiting for data to be deleted...");
                System.Threading.Thread.CurrentThread.Join(60000);
                foreach (string key in launchedBirstConnects)
                {
                    CloseBirstConnectCommand cmd = new CloseBirstConnectCommand(ATC);
                    cmd.setDefaultArguments();
                    cmd.processCommandArguments(new string[] { "/spacename=" + sp.Name, "/dcconfigname=" + key });
                    cmd.execute();
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("removepublished");
            command.Append(" ").Append("/spacename=").Append(spacename);
            if (deleteall)
                command.Append(" ").Append("/deleteall");
            if (restoresettings)
                command.Append(" ").Append("/restoresettings");
            return command.ToString();
        }
    }
}
