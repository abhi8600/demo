﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Diagnostics;

namespace AcornTestConsole
{
    class RunQueryCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string logicalquery { get; set; }
        public string resultfile { get; set; }
        public string resultdir { get; set; }

        public RunQueryCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            resultfile = "queryresults.txt";
            resultdir = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command runquery.");
            spacename = preProcessStringArgument(spacename);
            if (logicalquery == null)
                throw new Exception("logicalquery argument not specified for command runquery.");
            logicalquery = preProcessStringArgument(logicalquery);
            resultfile = preProcessStringArgument(resultfile);
            if (resultdir == null)
                throw new Exception("resultdir argument not specified for command runquery (nor testresultsdir specified for ATC).");
            resultdir = preProcessStringArgument(resultdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (!Directory.Exists(resultdir))
                    Directory.CreateDirectory(resultdir);
                string testFolder = resultdir + "\\" + sp.Name;
                if (!Directory.Exists(testFolder))
                    Directory.CreateDirectory(testFolder);

                String cmdFile = ATC.tempfolder + "\\" + sp.Name + ".tempfile.smi";
                File.Delete(cmdFile);
                String repPath = sp.Directory + "\\repository_dev.xml";
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file, Encoding.UTF8);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                // passing the updated allowed packges details
                List<PackageSpaceProperties> packageSpacesList = Acorn.Util.getImportedPackageSpaces(sp, maf);
                if (packageSpacesList != null && packageSpacesList.Count > 0)
                {
                    foreach (PackageSpaceProperties packageSpaceProperties in packageSpacesList)
                    {
                        string spaceProps = "packageID=" + packageSpaceProperties.PackageID + " packageName=" + packageSpaceProperties.PackageName +
                            " spaceID=" + sp.ID.ToString() + " spaceDirectory=" + packageSpaceProperties.SpaceDirectory + " spaceName=" + packageSpaceProperties.SpaceName;
                        sw.WriteLine("setProperties start packageSpaceProperties " + spaceProps + "  end");
                    }
                }
                sw.WriteLine("repository \"" + repPath + "\"");
                sw.WriteLine("runquery \"" + logicalquery + "\"");
                sw.Close();
                file.Close();

                ProcessStartInfo startInfo = new ProcessStartInfo(System.Web.Configuration.WebConfigurationManager.AppSettings["RunQueryCommand"]);
                startInfo.Arguments = "-c \"" + cmdFile + "\"";
                startInfo.Arguments += " -logfile \"" + ATC.tempfolder + "\\runquerycmdoutput.log\" > \"" + testFolder + "\\" + resultfile + "\"";
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                Process process;
                Console.Out.Write("Running command: " + startInfo.FileName + " ");
                Console.Out.WriteLine(startInfo.Arguments);
                process = Process.Start(startInfo);
                int i = 0;
                while (!process.WaitForExit(1000))
                {
                    if (i++ < Program.MAX_TIME_FOR_QUERY / 1000)
                        Console.Out.Write(".");
                    else
                    {
                        Console.Error.WriteLine("Giving up on query execution. Waited for:" + Program.MAX_TIME_FOR_QUERY);
                        Console.Error.WriteLine("Space:" + sp.Name + " Query:" + logicalquery);                            
                        break;
                    }
                }
                Console.Out.WriteLine("");
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("runquery");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/logicalquery=").Append(logicalquery);
            command.Append(" ").Append("/resultfile=").Append(resultfile);
            command.Append(" ").Append("/resultdir=").Append(resultdir);
            return command.ToString();
        }
    }
}
