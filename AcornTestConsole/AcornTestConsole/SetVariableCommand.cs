﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class SetVariableCommand : ATC_Command
    {
        public string name { get; set; }
        public string value { get; set; }
        public string dbtype { get; set; }

        public SetVariableCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                if (dbtype == null || dbtype.Equals(""))
                {
                    if (ATC.variableDictionary.ContainsKey(name))
                         ATC.variableDictionary[name] = value;
                     else
                         ATC.variableDictionary.Add(name, value);
                    Console.Out.WriteLine("Setting Variable:" + name + "=" + value);
                }
                else if (ATC.DatabaseType.Equals(dbtype))
                {
                    if (ATC.variableDictionary.ContainsKey(name))
                        ATC.variableDictionary[name] = value;
                    else
                        ATC.variableDictionary.Add(name, value);
                    Console.Out.WriteLine("Setting Variable:" + name + "=" + value);
                }
                else
                {
                    Console.Out.WriteLine("Database type for setvariable does not match dbtype, ignoring setvariable for variable:" + name);
                }
                
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setvariable");
            command.Append(" ").Append("/name=").Append(name);
            command.Append(" ").Append("/value=").Append(value);
            if(dbtype != null && dbtype != "")
            {
                command.Append(" ").Append("/dbtype=").Append(value);
            }            
            return command.ToString();
        }
    }
}