﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class MoveFilesCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string fromfolder { get; set; }
        public string tofolder { get; set; }
        public string filetype { get; set; }

        public MoveFilesCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            fromfolder = "V{ATC_TOMCAT_TEMP}";
            tofolder = ATC.testresultsdir;
            filetype = "*";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command movefiles.");
            spacename = preProcessStringArgument(spacename);
            fromfolder = preProcessStringArgument(fromfolder);
            if (ATC.ATC_Tomcat_Temp_Dir != null)
                fromfolder = fromfolder.Replace("V{ATC_TOMCAT_TEMP}", ATC.ATC_Tomcat_Temp_Dir);
            if (tofolder == null)
                throw new Exception("tofolder argument not specified for command movefiles.");
            tofolder = preProcessStringArgument(tofolder);
            filetype = preProcessStringArgument(filetype);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (!Directory.Exists(fromfolder))
                    throw new Exception("From Folder does not exist: " + fromfolder);
                if (!Directory.Exists(tofolder))
                    Directory.CreateDirectory(tofolder);
                tofolder = tofolder + "\\" + sp.Name;
                if (!Directory.Exists(tofolder))
                    Directory.CreateDirectory(tofolder);
                DirectoryInfo dir = new DirectoryInfo(fromfolder);
                foreach (FileInfo fi in dir.GetFiles(filetype))
                {
                    fi.MoveTo(tofolder + "\\" + fi.Name);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("movefiles");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/fromfolder=").Append(fromfolder);
            command.Append(" ").Append("/tofolder=").Append(tofolder);
            command.Append(" ").Append("/filetype=").Append(filetype);            
            return command.ToString();
        }
    }
}
