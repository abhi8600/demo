﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class UpdateCustomMetricCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string metricname { get; set; }
        public string grainofsource { get; set; }
        public string aggregationrule { get; set; }
        public string formula { get; set; }
        public int sqlgentype { get; set; }

        public UpdateCustomMetricCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            sqlgentype = Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command updatecustommetric.");
            spacename = preProcessStringArgument(spacename);
            if (metricname == null)
                throw new Exception("metricname argument not specified for command updatecustommetric.");
            metricname = preProcessStringArgument(metricname);
            if (grainofsource == null)
                throw new Exception("grainofsource argument not specified for command updatecustommetric.");
            grainofsource = preProcessStringArgument(grainofsource);
            if (aggregationrule == null)
                throw new Exception("aggregationrule argument not specified for command updatecustommetric.");
            aggregationrule = preProcessStringArgument(aggregationrule);
            if (formula == null)
                throw new Exception("formula argument not specified for command updatecustommetric.");
            formula = preProcessStringArgument(formula);            
        }

        override public Nullable<bool> execute()
        {
            FileStream fs = null;
            TextReader reader = null;
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);

                String fName = sp.Directory + "\\repository_dev.xml";
                MainAdminForm maf = new MainAdminForm();
                maf.loadRepositoryFromFile(fName);
                maf.setupTree();

                //Converting grains into StagingTable Levels form
                char[] delim = { ',' };
                string[] grainArray = grainofsource.Split(delim);

                string[][] grains = new string[grainArray.Length][];
                int i = 0;
                delim = new char[] { '.' };
                foreach (string grstr in grainArray)
                {
                    if (grstr.Contains('.'))
                    {
                        grains[i] = new string[2];
                        grains[i][0] = grstr.Split(delim)[0];
                        grains[i++][1] = grstr.Split(delim)[1];
                    }
                    else
                    {
                        grains[i] = new string[1];
                        grains[i++][0] = grstr;
                    }
                }

                StagingTable sTable = null;
                if (!sp.DiscoveryMode && grainofsource.Contains('.'))
                {
                    MeasureTable mTable = Util.findMeasureTableBasedOnGrains(maf, grains);
                    if (mTable == null)
                    {
                        throw new Exception("Invalid Grain " + grainofsource);
                    }

                    //Find relative staging table based on grains
                    sTable = Util.findStagingTableBasedOnGrain(maf, grains);
                }
                else
                {
                    //Find relative staging table based on grains
                    sTable = Util.findStagingTableBasedOnGrainForDiscoveryMode(maf, grainofsource);
                }

                // Get columns and type maps
                Dictionary<string, string> columnMap = new Dictionary<string, string>();
                Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                    new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();

                Util.buildColumnAndTypeMapForCustomMetric(maf, columnMap, typeMap, sTable, sp, grainofsource);

                ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(formula, columnMap, typeMap, sqlgentype, true);
                if (lep.HasError)
                {
                    throw new Exception(lep.Error);
                }

                LogicalExpression curle = new LogicalExpression();
                curle.Name = metricname;
                curle.Expression = formula;
                if (!sp.DiscoveryMode && grainofsource.Contains('.'))
                {
                    curle.Levels = sTable.Levels;
                }
                else
                {
                    curle.Levels = grains;
                }
                curle.AggregationRule = aggregationrule;
                curle.Type = LogicalExpression.TYPE_MEASURE;

                //update existing metric
                LogicalExpression[] leExpressions = maf.getLogicalExpressions();
                if (leExpressions == null || leExpressions.Length == 0)
                    return false;
                List<LogicalExpression> exlist = new List<LogicalExpression>(leExpressions);
                LogicalExpression leToUpdate = null;
                foreach (LogicalExpression le in exlist)
                {
                    if (le.Type == LogicalExpression.TYPE_MEASURE)
                    {
                        if (metricname.Equals(le.Name))
                        {
                            leToUpdate = curle;
                            break;
                        }
                    }
                }
                
                string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                Acorn.Util.buildApplication(maf, schema, DateTime.MinValue, -1, null, sp, null);
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (fs != null)
                    fs.Close();
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("updatecustommetric");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/metricname=").Append(metricname);
            command.Append(" ").Append("/grainofsource=").Append(grainofsource);
            command.Append(" ").Append("/aggregationrule=").Append(aggregationrule);
            command.Append(" ").Append("/formula=").Append(formula);
            command.Append(" ").Append("/sqlgentype=").Append(sqlgentype);
            return command.ToString();
        }
    }
}
