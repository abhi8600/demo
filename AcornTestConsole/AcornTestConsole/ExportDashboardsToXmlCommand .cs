﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Threading;

namespace AcornTestConsole
{
    class ExportDashboardsToXmlCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string outputdir { get; set; }

        public ExportDashboardsToXmlCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command exportdashboardstoxml.");
            spacename = preProcessStringArgument(spacename);
            if (outputdir == null)
                throw new Exception("outputdir argument not specified for command exportdashboardstoxml.");
            outputdir = preProcessStringArgument(outputdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (!Directory.Exists(outputdir))
                    throw new Exception("ouputdir : " + outputdir + " does not exist");

                // Default to global if user-level pref does not exist
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, ATC.u.ID, "Locale");
                // Only query for the user-level preference
                IDictionary<string, string> timezonePref = ManagePreferences.getUserPreference(null, ATC.u.ID, "TimeZone");
                string timezone = null;
                if (timezonePref.ContainsKey("TimeZone"))
                {
                    timezone = timezonePref["TimeZone"];
                }
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SpaceConfig sc = Acorn.Util.getSpaceConfiguration(sp.Type);
                TrustedService.TrustedService ts = new TrustedService.TrustedService();
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                ts.Timeout = Timeout.Infinite;
                ts.seedCache(sp.Directory, sp.ID.ToString(), ATC.u.Username, localePref["Locale"], timezone, true, true, true, true);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("exportdashboardstoxml");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/outputdir=").Append(outputdir);
            return command.ToString();
        }
    }
}
