﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class SetupBirstConnectTasksCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string tasklistfile { get; set; }
        public string dcconfigname { get; set; }

        public SetupBirstConnectTasksCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            dcconfigname = "Default";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command setupbirstconnecttasks.");
            spacename = preProcessStringArgument(spacename);
            if (tasklistfile == null)
                throw new Exception("tasklistfile argument not specified for command setupbirstconnecttasks.");
            tasklistfile = preProcessStringArgument(tasklistfile);
            dcconfigname = preProcessStringArgument(dcconfigname);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                FileInfo fi = new FileInfo(tasklistfile);
                if (!fi.Exists)
                    throw new Exception("TaskListFile does not exist:" + tasklistfile);
                string dcconfigfilename = "Default".Equals(dcconfigname) ? "dcconfig.xml" : dcconfigname + "_" + "dcconfig.xml";
                fi = new FileInfo(sp.Directory + "\\dcconfig\\" + dcconfigfilename);
                if (!fi.Exists)
                    throw new Exception("dcconfig file does not exist:" + sp.Directory + "\\dcconfig\\" + dcconfigfilename);
                StreamReader sr = new StreamReader(sp.Directory + "\\dcconfig\\" + dcconfigfilename);
                string dcconfig = sr.ReadToEnd();
                string[] lines = dcconfig.Split('\n');
                int noLines = lines.Length;
                sr.Close();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < noLines - 2; i++)
                    sb.AppendLine(lines[i].TrimEnd());
                sr = new StreamReader(tasklistfile);
                sb.AppendLine(sr.ReadToEnd());
                sr.Close();
                sb.AppendLine(lines[noLines - 2]).Append(lines[noLines - 1]);
                StreamWriter sw = new StreamWriter(sp.Directory + "\\dcconfig\\" + dcconfigfilename);
                sw.Write(sb.ToString());
                sw.Flush();
                sw.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setupbirstconnecttasks");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/tasklistfile=").Append(tasklistfile);
            return command.ToString();
        }
    }
}
