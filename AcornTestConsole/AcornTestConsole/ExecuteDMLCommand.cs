﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace AcornTestConsole
{
    class ExecuteDMLCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string query { get; set; }

        public ExecuteDMLCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {            
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command executedml.");
            spacename = preProcessStringArgument(spacename);
            if (query == null)
                throw new Exception("query argument not specified for command executedml.");
            query = preProcessStringArgument(query);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);

                QueryConnection spConn = Acorn.ConnectionPool.getConnection(sp.getFullConnectString());
                QueryCommand cmd = spConn.CreateCommand();
                if (query != null)
                {
                    query = query.Replace("V{SCHEMA}", sp.Schema);
                    cmd.CommandText = query;
                }
                int rows = cmd.ExecuteNonQuery();
              
                cmd.Dispose();                               

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("executedml");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/query=").Append(query);
            return command.ToString();
        }
    }
}
