﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;

namespace AcornTestConsole
{
    class OverwriteDCConfigCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string sourcefilename { get; set; }
        public string configfilename { get; set; }

        public OverwriteDCConfigCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            configfilename = "dcconfig.xml";
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command overwritedcconfig.");
            spacename = preProcessStringArgument(spacename);
            if (sourcefilename == null)
                throw new Exception("sourcefilename argument not specified for command overwritedcconfig.");
            sourcefilename = preProcessStringArgument(sourcefilename);
            configfilename = preProcessStringArgument(configfilename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                string configfile = sp.Directory + "\\dcconfig\\" + configfilename;
                File.Copy(sourcefilename, configfile, true);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("overwritedcconfig");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/sourcefilename=").Append(sourcefilename);
            command.Append(" ").Append("/configfilename=").Append(configfilename);
            return command.ToString();
        }
    }
}
