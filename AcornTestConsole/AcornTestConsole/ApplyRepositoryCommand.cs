﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;
using System.Xml;
using System.Xml.Serialization;

namespace AcornTestConsole
{
    class ApplyRepositoryCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string repositoryfilename { get; set; }
        public string fixPackageID { get; set; }

        public ApplyRepositoryCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command applyrepository.");
            spacename = preProcessStringArgument(spacename);
            if (repositoryfilename == null)
                throw new Exception("repositoryfilename argument not specified for command applyrepository.");
            repositoryfilename = preProcessStringArgument(repositoryfilename);
            if (fixPackageID != null && fixPackageID.Trim().Length > 0)
            {
                fixPackageID = preProcessStringArgument(fixPackageID);
            }
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (ATC.DatabaseType != ATC.DEFAULT_DATABASE_TYPE)
                {
                    int index = repositoryfilename.LastIndexOf(".");
                    string DBrepositoryfilename = repositoryfilename.Substring(0, index) + "_" + ATC.DatabaseType + repositoryfilename.Substring(index);
                    if (File.Exists(DBrepositoryfilename))
                        repositoryfilename = DBrepositoryfilename;
                }
                if (!File.Exists(repositoryfilename))
                    throw new Exception("Error opening repositoryfile: " + repositoryfilename);
                Dictionary<string, PackageImport> oldPackageIDToNameMapping = null;
                if (fixPackageID != null && fixPackageID.Trim().ToString() == "true")
                {
                    oldPackageIDToNameMapping = getPackageIDToNameMappingBeforeChange(sp);
                }
                File.Copy(repositoryfilename, sp.Directory + "\\repository_dev.xml", true);
                DateTime now = DateTime.Now;
                FileInfo fi = new FileInfo(sp.Directory + "\\repository_dev.xml");
                fi.LastWriteTime = now;
                File.Copy(sp.Directory + "\\repository_dev.xml", sp.Directory + "\\repository.xml", true);
                fi = new FileInfo(sp.Directory + "\\repository.xml");
                fi.LastWriteTime = now;
                File.Delete(sp.Directory + "\\Variables_dev.xml");
                File.Delete(sp.Directory + "\\Variables.xml");
                File.Delete(sp.Directory + "\\VirtualColumns_dev.xml");
                File.Delete(sp.Directory + "\\VirtualColumns.xml");
                File.Delete(sp.Directory + "\\LogicalExpressions_dev.xml");
                File.Delete(sp.Directory + "\\LogicalExpressions.xml");
                File.Delete(sp.Directory + "\\Aggregates_dev.xml");
                File.Delete(sp.Directory + "\\Aggregates.xml");
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                maf.hybrid = true; // to let us know that the repository was potentially modified outside of Birst
                if (fixPackageID != null && fixPackageID.Trim().ToString() == "true")
                {
                    synchUpPackageIDs(maf, oldPackageIDToNameMapping);
                }
                if (ATC.RepositoryBuilderVersion > 0)
                {
                    maf.builderVersion = ATC.RepositoryBuilderVersion;
                }
                if (maf.builderVersion >= 25)
                {
                    //Set last modified date for all staging tables to current so that next time data is processed, all staging tables would be reloaded.
                    List<StagingTable> stList = maf.stagingTableMod.getStagingTables();
                    if ((stList != null) && (stList.Count > 0))
                    {
                        foreach (StagingTable st in stList)
                        {
                            st.LastModifiedDate = DateTime.UtcNow;
                        }
                    }
                }
                DatabaseConnection defaultConnection = maf.connection.findConnection("Default Connection");
                if (defaultConnection != null && defaultConnection.IsPartitioned)
                {
                    string[] partitionConnectionNames = defaultConnection.PartitionConnectionNames;
                    if (partitionConnectionNames != null && partitionConnectionNames.Length > 0)
                    {
                        foreach (string partitionConnectionName in partitionConnectionNames)
                        {
                            DatabaseConnection dc = maf.connection.findConnection(partitionConnectionName);
                            if (dc != null && dc.Schema != sp.Schema)
                            {
                                dc.Schema = sp.Schema;                                
                            }
                        }
                    }
                }
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private Dictionary<string, PackageImport> getPackageIDToNameMappingBeforeChange(Space sp)
        {
            if (sp != null)
            {                
                bool newRep;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (maf != null && maf.Imports != null && maf.Imports.Length > 0)
                {
                    Dictionary<string, PackageImport> response = new Dictionary<string, PackageImport>();
                    foreach (PackageImport pi in maf.Imports)
                    {
                        string packageName = pi.PackageName;
                        Guid packageID = pi.PackageID;
                        if (packageName != null && packageName.Length > 0 && packageID != null && packageID != Guid.Empty)
                        {
                            if (!response.ContainsKey(pi.PackageName))
                            {
                                PackageImport piCopy = new PackageImport();
                                piCopy.PackageID = pi.PackageID;
                                piCopy.PackageName = pi.PackageName;
                                piCopy.SpaceID = pi.SpaceID;
                        
                                response.Add(pi.PackageName, piCopy);
                            }
                        }
                    }
                    return response;
                }
            }
            return null;
        }

        private void synchUpPackageIDs(MainAdminForm maf, Dictionary<string, PackageImport> oldPackageIDToNameMapping)
        {
            if (maf != null && maf.Imports != null && maf.Imports.Length > 0 &&
                oldPackageIDToNameMapping != null && oldPackageIDToNameMapping.Count > 0)
            {
                foreach (PackageImport pi in maf.Imports)
                {
                    if (oldPackageIDToNameMapping.ContainsKey(pi.PackageName))
                    {                        
                        PackageImport oldPackageImport = oldPackageIDToNameMapping[pi.PackageName];
                        if (pi.PackageID != oldPackageImport.PackageID)
                        {
                            pi.PackageID = oldPackageImport.PackageID;
                            pi.SpaceID = oldPackageImport.SpaceID;
                        }
                    }
                }
            }
            if (fixPackageID != null && fixPackageID.Trim().Length > 0)
            {                
                string packageFileName = fixPackageID.Trim();
                if (!File.Exists(packageFileName))
                    return;
                XmlReader xreader = new XmlTextReader(packageFileName);
                // If packages.xml is in new format
                System.Xml.Serialization.XmlSerializer serializer = new XmlSerializer(typeof(AllPackages));
                AllPackages packages = null;
                try
                {
                    packages = (AllPackages)serializer.Deserialize(xreader);
                }
                catch (System.InvalidOperationException)
                {
                    Console.Error.WriteLine("Problem in loading package file name " + packageFileName);
                }
                finally
                {
                    xreader.Close();
                }
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("applyrepository");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/repositoryfilename=").Append(repositoryfilename);
            return command.ToString();
        }
    }
}
