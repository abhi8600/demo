﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Test.CommandLineParsing;
using System.Diagnostics;
using System.IO;

namespace AcornTestConsole
{
    class StartMemDBWithWaitCommand : ATC_Command
    {
        public string filename { get; set; }
        public string arguments { get; set; }

        public StartMemDBWithWaitCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (filename == null)
                throw new Exception("filename argument not specified for command startmemdbwithwait.");
            filename = preProcessStringArgument(filename);
            if (ATC.PsTools_Path == null || ATC.PsTools_Path.Trim().Length == 0 ||
                    ATC.MemDB_Path == null || ATC.MemDB_Path.Trim().Length == 0)
            {
                throw new Exception("PSTOOLS_PATH or MemDB_Path not specified in AcornTestConsole.exe.config.");
            }
            if (ATC.MemDB_Process_ID != -1)
                throw new Exception("MemDB instance is already running.");
            if (arguments != null)
                arguments = preProcessStringArgument(arguments);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                String cmdFile = ATC.tempfolder + "\\startmemdbwaitcmd.bat";
                File.Delete(cmdFile);
                FileStream file = new FileStream(cmdFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                sw.Write(ATC.PsTools_Path + "\\PSEXEC.exe");
                sw.Write(" \\\\" + ATC.MemDB_Machine_Name);
                if (ATC.MemDB_UserName != null && ATC.MemDB_UserName.Trim().Length > 0)
                    sw.Write(" -u " + ATC.MemDB_UserName);
                if (ATC.MemDB_Password != null && ATC.MemDB_Password.Trim().Length > 0)
                    sw.Write(" -p " + ATC.MemDB_Password);
                sw.Write(" -d -s");
                sw.Write(" -w \"" + ATC.MemDB_Path + "\"");
                sw.Write(" \"" + ATC.MemDB_Path + "\\" + filename + "\"");
                if (arguments != null && arguments.Trim().Length > 0)
                    sw.Write(" " + arguments);
                string outFile = "\"" + ATC.tempfolder + "\\startmemdbwaitout.log\"";
                string errFile = "\"" + ATC.tempfolder + "\\startmemdbwaiterr.log\"";
                //sw.Write(" > " + outFile + " 2> " + errFile);
                sw.Flush();
                sw.Close();
                ProcessStartInfo startInfo = new ProcessStartInfo(cmdFile);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardInput = true;
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.WorkingDirectory = ATC.tempfolder;
                Process process = Process.Start(startInfo);
                string stdErr = null, lastline =null;
                while ((stdErr = process.StandardError.ReadLine()) != null)
                {
                    lastline = stdErr;
                }
                string pid = "-1";
                int processID = -1;
                if (lastline != null && lastline.Length > 0)
                {
                    pid = lastline.Substring(lastline.LastIndexOf(" ") + 1, lastline.LastIndexOf(".") - 1 - lastline.LastIndexOf(" "));
                    int.TryParse(pid, out processID);
                }
                if (processID != -1)
                    ATC.MemDB_Process_ID = processID;
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("startmemdbwithwait");
            command.Append(" ").Append("/filename=").Append(filename);
            if (arguments != null && !arguments.Trim().Equals(""))
                command.Append(" ").Append("/arguments=").Append(arguments);
            return command.ToString();
        }
    }
}
