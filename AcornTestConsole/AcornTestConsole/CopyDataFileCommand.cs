﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using System.Globalization;

namespace AcornTestConsole
{
    class CopyDataFileCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string dirname { get; set; }
        public string filename { get; set; }
        public string resultdir { get; set; }

        public CopyDataFileCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command copydatafile.");
            spacename = preProcessStringArgument(spacename);
            if (dirname == null && filename == null)
                throw new Exception("file or dirname argument must be specified for command copydatafile");
            dirname = preProcessStringArgument(dirname);
            filename = preProcessStringArgument(filename);
            if (resultdir == null)
                throw new Exception("resultdir argument not specified for command copydatafile.");
            resultdir = preProcessStringArgument(resultdir);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                if (dirname != null)
                {
                    String sourceFolder = sp.Directory + Path.DirectorySeparatorChar + dirname;
                    String destFolder = resultdir + Path.DirectorySeparatorChar + sp.Name;
                    if (!Directory.Exists(destFolder))
                        Directory.CreateDirectory(destFolder);
                    if (!Directory.Exists(sourceFolder))
                        throw new Exception("dirname is not a valid directory : . + dirname");

                    DirectoryInfo srcdir = new DirectoryInfo(sourceFolder);
                    DirectoryInfo refdir = new DirectoryInfo(destFolder);

                    copyDataFilesFromSpace(srcdir, refdir);
                }

                if (filename != null)
                {
                    String srcFile = sp.Directory + Path.DirectorySeparatorChar + filename;
                    String destFolder = resultdir + Path.DirectorySeparatorChar + sp.Name;
                    if (!Directory.Exists(destFolder))
                        Directory.CreateDirectory(destFolder);
                    if (!File.Exists(srcFile))
                        throw new Exception("filename is not a valid file : " + filename);

                    File.Copy(srcFile, destFolder + Path.DirectorySeparatorChar + filename, true);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        private void copyDataFilesFromSpace(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
                copyDataFilesFromSpace(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                file.CopyTo(Path.Combine(target.FullName, file.Name),true);
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("copydatafile");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/dirname=").Append(dirname);
            command.Append(" ").Append("/filename=").Append(resultdir);
            command.Append(" ").Append("/resultdir=").Append(resultdir);            
            return command.ToString();
        }
    }
}
