﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using System.IO;

namespace AcornTestConsole
{
    class UploadCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string datafilelocation { get; set; }
        public bool unmapcolumnsnotpresent { get; set; }
        public string outputfilename { get; set; }
        public string outputdirectory { get; set; }

        public UploadCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            outputdirectory = ATC.testresultsdir;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command upload.");
            spacename = preProcessStringArgument(spacename);
            if (datafilelocation == null)
                throw new Exception("datafilelocation argument not specified for command upload.");
            datafilelocation = preProcessStringArgument(datafilelocation);
            if (outputfilename != null)
                outputfilename = preProcessStringArgument(outputfilename);
            if (outputdirectory != null)
                outputdirectory = preProcessStringArgument(outputdirectory);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);                
                if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
                    Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                FileInfo fi = new FileInfo(datafilelocation);
                File.Copy(fi.FullName, sp.Directory + "\\data\\" + fi.Name, true);
                ApplicationUploader au = new ApplicationUploader(maf, sp, fi.Name, true, false, false, null, false, null, ATC.u, null, 0, 0, false, null, null,false);
                System.Data.DataTable errorTable = au.uploadFile(true, unmapcolumnsnotpresent);
                string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                Acorn.Util.buildApplication(maf, schema, DateTime.Now, sp.LoadNumber + 1, null, sp, null);
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                if (errorTable.Rows.Count > 0)
                {
                    StringBuilder errorStr = new StringBuilder();
                    errorStr.Append("Error uploading file:" + fi.FullName.Replace(Directory.GetCurrentDirectory(),".") + "\r\n");
                    for (int i = 0; i < errorTable.Rows.Count; i++)
                    {
                        System.Data.DataRow dr = errorTable.Rows[i];
                        errorStr.Append(dr[0] + " " + dr[1] + dr[2] + "\r\n");
                    }
                    if (outputfilename != null && outputdirectory != null)
                    {
                        if (!Directory.Exists(outputdirectory))
                            Directory.CreateDirectory(outputdirectory);
                        outputdirectory += "\\" + sp.Name;
                        if (!Directory.Exists(outputdirectory))
                            Directory.CreateDirectory(outputdirectory);
                        string filename = outputdirectory + "\\" + outputfilename;
                        File.AppendAllText(filename, errorStr.ToString());
                        // If error is redirected to file, the upload failure is expected.
                        // Hence, not failing the upload command to keep the error console clean for ATC.
                        return true;
                    }
                    Console.Error.Write(errorStr.ToString());
                    throw new Exception("Error uploading file:" + fi.FullName);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("upload");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/datafilelocation=").Append(datafilelocation);
            if (unmapcolumnsnotpresent)
                command.Append(" ").Append("/unmapColumnsNotPresent");
            if (outputfilename != null && outputdirectory != null)
                command.Append(" ").Append("/outputfilename=").Append(outputfilename);
            if (outputfilename != null && outputdirectory != null)
                command.Append(" ").Append("/outputdirectory=").Append(outputdirectory);
            return command.ToString();
        }
    }
}
