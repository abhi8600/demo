﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;
using System.Net;

namespace AcornTestConsole
{
    class ExportReportToHtmlCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string reportname { get; set; }
        public string reportpath { get; set; }
        public string emailto { get; set; }

        public ExportReportToHtmlCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
            emailto = ATC.u.Username;
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command exportreporttohtml.");
            spacename = preProcessStringArgument(spacename);
            if (reportname == null)
                throw new Exception("reportname argument not specified for command exportreporttohtml.");
            reportname = preProcessStringArgument(reportname);
            if (reportpath == null)
                throw new Exception("reportpath argument not specified for command exportreporttohtml.");
            reportpath = preProcessStringArgument(reportpath);
            emailto = preProcessStringArgument(emailto);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);

                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);
                Acorn.TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
                // 5 Minute timeout
                ts.Timeout = 5 * 60 * 1000;
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                ts.Url = localprotocol + Acorn.Util.getSpaceConfiguration(sp.Type).LocalURL + "/SMIWeb/services/TrustedService";
                string from = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["DeliveryFrom"];
                // Default to global if user-level pref does not exist
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, ATC.u.ID, "Locale");
                // Only query for the user-level preference
                IDictionary<string, string> timezonePref = ManagePreferences.getUserPreference(null, ATC.u.ID, "TimeZone");
                string timezone = null;
                if (timezonePref.ContainsKey("TimeZone"))
                {
                    timezone = timezonePref["TimeZone"];
                }
                
                Acorn.TrustedService.ExecuteResult er = null;
                try
                {
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    er = ts.exportReport(smtp.Host, sp.Directory, sp.ID.ToString(), "atc_html", reportname, ATC.u.Username, from, "Report From Birst", null,
                        reportpath + "\\" + reportname + ".jasper", emailto.Split(';'), null, null, ATC.u.MaxDeliveredReportsPerDay, true, localePref["Locale"], timezone);
                }
                catch (System.Exception e)
                {
                    Console.Error.WriteLine("May not be able to deliver report: " + sp.ID + "/" + reportname);
                    string reason = e.GetBaseException().Message;
                    Console.Error.WriteLine("Reason for report failure: " + reason);
                    Console.Error.WriteLine("Exception: " + e + "\n" + e.StackTrace);
                    // Send to user
                    /* Can't send to them because the SMIWeb.TrustedService may have sent an email even though the comm failed on this end
                    Util.sendErrorSummaryEmail(sr.ToList[0], 
                                               "[Problem generating scheduled report] " + sr.Subject,
                                               "scheduled report based on: " + sr.ReportPath.Replace("V{CatalogDir}", ""), 
                                               reason);
                    */
                    // Send email alert to operations if it's anything but a timeout
                    if (e is WebException && !((WebException)e).Status.Equals(WebExceptionStatus.Timeout))
                    {
                        Acorn.Util.sendErrorDetailEmail("[Problem generating scheduled report] " + "Report From Birst",
                                                  "scheduled report based on: " + (reportpath + "\\" + reportname).Replace("V{CatalogDir}", ""),
                                                  "ScheduledReport.execute() raised an exception: " + reason,
                                                  e, sp, null);
                    }
                    throw e;
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("exportreporttohtml");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/reportname=").Append(reportname);
            command.Append(" ").Append("/reportpath=").Append(reportpath);
            command.Append(" ").Append("/emailto=").Append(emailto);
            return command.ToString();
        }
    }
}
