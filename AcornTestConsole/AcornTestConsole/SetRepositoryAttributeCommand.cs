﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using System.Reflection;
using Microsoft.Test.CommandLineParsing;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class SetRepositoryAttributeCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string attributename { get; set; }
        public string attributevalue { get; set; }
        public bool serverparameter { get; set; }

        public SetRepositoryAttributeCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command setrepositoryattribute.");
            spacename = preProcessStringArgument(spacename);
            if (attributename == null)
                throw new Exception("attributename argument not specified for command setrepositoryattribute.");
            attributename = preProcessStringArgument(attributename);
            if (attributevalue == null)
                throw new Exception("attributevalue argument not specified for command setrepositoryattribute.");
            attributevalue = preProcessStringArgument(attributevalue);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u);                
                Type objType = maf.GetType();
                Object obj = maf;
                if (serverparameter)
                {
                    objType = maf.serverPropertiesModule.GetType();
                    obj = maf.serverPropertiesModule;
                }
                foreach (FieldInfo field in objType.GetFields())
                {
                    if (field.Name.Equals(attributename,StringComparison.OrdinalIgnoreCase))
                    {
                        Object valObj = field.GetValue(obj);
                        {
                            if (valObj != null)
                            {
                                if (valObj is string)
                                {
                                    field.SetValue(obj, attributevalue);
                                }
                                else if (valObj is bool)
                                {
                                    field.SetValue(obj, bool.Parse(attributevalue));
                                }
                                else if (valObj is int)
                                {
                                    field.SetValue(obj, int.Parse(attributevalue));
                                }
                                else
                                {
                                    throw new Exception("attribute " + attributename + " is of type " + valObj.GetType().Name + " - cannot set value");
                                }
                            }
                            else
                            {
                                if (field.FieldType.Name.Equals("string", StringComparison.OrdinalIgnoreCase))
                                {
                                    field.SetValue(obj, attributevalue);
                                }
                                else if (field.FieldType.Name.Equals("boolean", StringComparison.OrdinalIgnoreCase))
                                {
                                    field.SetValue(obj, bool.Parse(attributevalue));
                                }
                                else if (field.FieldType.Name.Equals("int64", StringComparison.OrdinalIgnoreCase))
                                {
                                    field.SetValue(obj, int.Parse(attributevalue));
                                }
                                else
                                {
                                    throw new Exception("attribute " + attributename + " is of type " + field.FieldType.Name + " - cannot set value");
                                }
                            }
                            break;
                        }                        
                    }
                }
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setrepositoryattribute");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/attributename=").Append(attributename);
            command.Append(" ").Append("/attributevalue=").Append(attributevalue);
            if (serverparameter)
                command.Append(" ").Append("/serverparameter");
            return command.ToString();
        }
    }
}
