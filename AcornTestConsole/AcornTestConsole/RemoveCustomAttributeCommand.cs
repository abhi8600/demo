﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;
using System.IO;
using Performance_Optimizer_Administration;

namespace AcornTestConsole
{
    class RemoveCustomAttributeCommand : ATC_Command
    {
        public string spacename { get; set; }
        public string attributename { get; set; }

        public RemoveCustomAttributeCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {   
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command removecustomattribute.");
            spacename = preProcessStringArgument(spacename);
            if (attributename == null)
                throw new Exception("attributename argument not specified for command removecustomattribute.");
            attributename = preProcessStringArgument(attributename);
        }

        override public Nullable<bool> execute()
        {
            FileStream fs = null;
            TextReader reader = null;
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                
                String fName = sp.Directory + "\\repository_dev.xml";
                MainAdminForm maf = new MainAdminForm();
                maf.loadRepositoryFromFile(fName);
                maf.setupTree();

                LogicalExpression[] leExpressions = maf.getLogicalExpressions();
                LogicalExpression leToDelete = null;
                if (leExpressions != null && leExpressions.Length > 0)
                {
                    List<LogicalExpression> exlist = new List<LogicalExpression>(leExpressions);
                    foreach (LogicalExpression le in exlist)
                    {
                        if (le.Type == LogicalExpression.TYPE_DIMENSION)
                        {
                            if (attributename.Equals(le.Name))
                            {
                                leToDelete = le;
                                break;
                            }
                        }
                    }
                }
                if (leToDelete != null)
                {
                    maf.setRepositoryDirectory(sp.Directory);
                    maf.deleteLogicalExpression(leToDelete);
                    string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                    Acorn.Util.buildApplication(maf, schema, DateTime.MinValue, -1, null, sp, null);
                    Acorn.Util.saveApplication(maf, sp, null, ATC.u);
                }
                else
                    throw new Exception("Cannot find Attribute " + attributename);
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (fs != null)
                    fs.Close();
            }
        }


        override public string ToString()
        {
            StringBuilder command = new StringBuilder("removecustomattribute");
            command.Append(" ").Append("/spacename=").Append(spacename);
            command.Append(" ").Append("/attributename=").Append(attributename);
            return command.ToString();
        }
    }
}
