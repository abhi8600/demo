﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Microsoft.Test.CommandLineParsing;

namespace AcornTestConsole
{
    class SetSpaceCommand : ATC_Command
    {
        public string spacename { get; set; }

        public SetSpaceCommand(Program ATC)
            : base(ATC)
        {
        }

        override public void setDefaultArguments()
        {
        }

        override public void processCommandArguments(string[] commandArgs)
        {
            this.ParseArguments(commandArgs);
            if (spacename == null)
                throw new Exception("spacename argument not specified for command setspace.");
            spacename = preProcessStringArgument(spacename);
        }

        override public Nullable<bool> execute()
        {
            try
            {
                Space sp = Acorn.Database.getSpace(ATC.conn, spacename, ATC.u, true, ATC.SCHEMA_NAME);
                if (sp == null)
                    throw new Exception("Unable to find the space specified:" + spacename);
                Performance_Optimizer_Administration.MainAdminForm maf = Acorn.Util.setSpace(null, sp, ATC.u, true);
                Acorn.Util.saveApplication(maf, sp, null, ATC.u);                
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error Executing Command : " + this.ToString() + "\n" + e.Message + "\n" + e.StackTrace);
                return false;
            }
        }

        override public string ToString()
        {
            StringBuilder command = new StringBuilder("setspace");
            command.Append(" ").Append("/spacename=").Append(spacename);
            return command.ToString();
        }
    }
}
