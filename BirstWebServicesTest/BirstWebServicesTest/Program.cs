﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using BirstWebServicesTest.BirstWebServices;
using System.Net;
using System.IO;
using System.Xml;

/**
 * sample C# code for accessing the Birst Web Services
 *
 * Note: in order to access the web services, you need the Birst Web Services product (11) associated with your user account
 */

namespace BirstWebServicesTest
{
    class Program
    {
        private static string BASEURL = "https://login.bws.birst.com";
        private static string USERNAME = "nobody@nobody.com";
        private static string PASSWORD = "password";
        private const int CHUNK_SIZE = 10 * 1024;

        static void Main(string[] args)
        {
            Console.WriteLine("flags are: -u baseurl (e.g., https://rc.birst.com) -n username, -p password");
            string baseurl = BASEURL;
            string username = USERNAME;
            string password = PASSWORD;
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; )
                {
                    if (args[i].ToLower() == "-u" && args.Length > i + 1)
                    {
                        baseurl = args[i + 1];
                        i += 2;
                    }
                    else if (args[i].ToLower() == "-n" && args.Length > i + 1)
                    {
                        username = args[i + 1];
                        i += 2;
                    }
                    else if (args[i].ToLower() == "-p" && args.Length > i + 1)
                    {
                        password = args[i + 1];
                        i += 2;
                    }
                    else
                    {
                        i++;
                    }
                }
            }

            Console.WriteLine("Using " + baseurl + " as user " + username);
            BirstWebServices.CommandWebService cws = new BirstWebServices.CommandWebService();
            cws.Url = baseurl + "/CommandWebService.asmx";
            cws.CookieContainer = new System.Net.CookieContainer(); // necessary to support cookie based load balancing  

            string token;
            while (true)
            {
                try
                {
                    int response = showMenu();
                    if (response < 0)
                        break;
                    switch (response)
                    {
                        case 1: //addAclToGroupInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                Console.WriteLine("Enter acl tag name and press enter.");
                                string aclTag = Console.ReadLine();
                                cws.addAclToGroupInSpace(token, groupName, aclTag, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;

                        case 2: // addAllowedIps
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the username to set an allowed ip address.");
                                string u = Console.ReadLine();
                                Console.WriteLine("Enter the ip address to allow.");
                                string ip = Console.ReadLine();
                                cws.addAllowedIp(token, u, ip);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 3: // addGroupToSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                cws.addGroupToSpace(token, groupName, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 4: // addProxyUser
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the username that will be proxied for.");
                                string u = Console.ReadLine();
                                Console.WriteLine("Enter the username that will be the proxy.");
                                string proxyUserName = Console.ReadLine();
                                Console.WriteLine("Enter the expiration date.");
                                string date = Console.ReadLine();
                                cws.addProxyUser(token, u, proxyUserName, DateTime.Parse(date));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 5: // addUser
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the new username.");
                                string u = Console.ReadLine();
                                cws.addUser(token, u, "");
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 6: // addUserToGroupInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the new username.");
                                string u = Console.ReadLine();
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                cws.addUserToGroupInSpace(token, u, groupName, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 7: // addUserToSpace
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the new username.");
                                string u = Console.ReadLine();
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                cws.addUserToSpace(token, u, spaceId, getBooleanInput("Is administrator"));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 8: // uploadData
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the filename for the source.");
                                string filename = Console.ReadLine();
                                uploadData(cws, token, spaceId, filename);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 9: // clearCacheInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                cws.clearCacheInSpace(token, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;

                        case 11: // copyCatalogDirectory
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Select the FROM space.");
                                string spFromId = selectSpace(cws, token);
                                Console.WriteLine("Select the TO space.");
                                string spToId = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the directory.");
                                string dirName = Console.ReadLine();
                                waitForComplete(cws, token, cws.copyCatalogDirectory(token, spFromId, spToId, dirName));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 12: //copyCustomSubjectArea
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Select the FROM space.");
                                string spFromId = selectSpace(cws, token);
                                Console.WriteLine("Select the TO space.");
                                string spToId = selectSpace(cws, token);
                                string customSubjectAreaName = selectCustomSubjectArea(cws, token, spFromId);
                                cws.copyCustomSubjectArea(token, spFromId, customSubjectAreaName, spToId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 13: // copyFileOrDirectory
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Select the FROM space.");
                                string spFromId = selectSpace(cws, token);
                                Console.WriteLine("Select the file name or directory to copy.");
                                string fileOrDir = Console.ReadLine();
                                Console.WriteLine("Select the TO space.");
                                string spToId = selectSpace(cws, token);
                                Console.WriteLine("Select the directory to copy to.");
                                string toDir = Console.ReadLine();
                                waitForComplete(cws, token, cws.copyFileOrDirectory(token, spFromId, fileOrDir, spToId, toDir));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 14: // copySpaceContents
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Select the FROM space.");
                                string spFromId = selectSpace(cws, token);
                                Console.WriteLine("Select the TO space.");
                                string spToId = selectSpace(cws, token);
                                waitForComplete(cws, token, cws.copySpaceContents(token, spFromId, spToId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 15: // createNewDirectory
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the parent directory.");
                                string parentDir = Console.ReadLine();
                                Console.WriteLine("Enter the new directory name.");
                                string newDirectoryName = Console.ReadLine();
                                cws.createNewDirectory(token, spaceId, parentDir, newDirectoryName);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 16: // createNewSpace
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the name of the new space.");
                                string spaceName = Console.ReadLine();
                                Console.WriteLine("Enter comments.");
                                string comments = Console.ReadLine();
                                string spID = cws.createNewSpace(token, spaceName, comments, getBooleanInput("Automatic space?"));
                                Console.WriteLine("Space ID " + spID);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 17: // deleteAllDataFromSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                waitForComplete(cws, token, cws.deleteAllDataFromSpace(token, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 18: // deleteFileOrDirectory
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter file or directory to delete.");
                                string fileOrDir = Console.ReadLine();
                                cws.deleteFileOrDirectory(token, spaceId, fileOrDir);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;

                        case 21: // deleteLastDataFromSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                waitForComplete(cws, token, cws.deleteLastDataFromSpace(token, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 22: // deleteSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string jobToken = cws.deleteSpace(token, spaceId);
                                waitForComplete(cws, token, jobToken);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 23: // executeQuery
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter Query.");
                                string query = Console.ReadLine();
                                executeQuery(cws, token, spaceId, query);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 24: // getDirectoryContents
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the parent directory.");
                                string dir = Console.ReadLine();
                                Console.WriteLine(cws.getDirectoryContents(token, spaceId, dir).ToString());
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 25: //getDirectoryPermissions
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the parent directory.");
                                string dir = Console.ReadLine();
                                Console.WriteLine(cws.getDirectoryPermissions(token, spaceId, dir).ToString()); ;
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 26: //getVariablesForSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string[][] list = cws.getVariablesForSpace(token, spaceId);
                                foreach (string[] l in list)
                                {
                                    Console.Write("Variable name: ");
                                    Console.Write(l[0]);
                                    Console.Write(" value=");
                                    Console.WriteLine(l[1]);
                                }
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 27: // listAllowedIps
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the name of the user.");
                                string u = Console.ReadLine();
                                writeList(cws.listAllowedIps(token, u));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 28: // listCreatedUsers
                            token = cws.Login(username, password);
                            try
                            {
                                writeList(cws.listCreatedUsers(token));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;

                        case 31: //listCustomSubjectAreas
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                writeList(cws.listCustomSubjectAreas(token, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 32: // listGroupAclsInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the group.");
                                string groupName = Console.ReadLine();
                                writeList(cws.listGroupAclsInSpace(token, groupName, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 33: // listGroupsInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                writeList(cws.listGroupsInSpace(token, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 34: // listProxyUsers
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the name of the user.");
                                string u = Console.ReadLine();
                                writeList(cws.listProxyUsers(token, u));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 35: // listSpaces
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine(cws.listSpaces(token).ToString());
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 36: // listUsersInGroupInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                writeList(cws.listUsersInGroupInSpace(token, groupName, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 37: // listUsersInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                writeList(cws.listUsersInSpace(token, spaceId));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 38: // publishData
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                publishData(cws, token, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 41: // removeAclFromGroupInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                Console.WriteLine("Enter acl tag to remove.");
                                string aclTag = Console.ReadLine();
                                cws.removeAclFromGroupInSpace(token, groupName, aclTag, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 42: // removeAllowedIps
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter username to remove an IP address from.");
                                string u = Console.ReadLine();
                                Console.WriteLine("Enter IP address to remove.");
                                string ip = Console.ReadLine();
                                cws.removeAllowedIp(token, u, ip);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 43: // removeGroupFromSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                cws.removeGroupFromSpace(token, groupName, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 44: // removeProxyUser
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter username to remove a proxy user from.");
                                string u = Console.ReadLine();
                                Console.WriteLine("Enter proxy username to remove.");
                                string proxyUserName = Console.ReadLine();
                                cws.removeProxyUser(token, u, proxyUserName);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 45: // removeUserFromGroupInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                Console.WriteLine("Enter user to remove from group.");
                                string u = Console.ReadLine();
                                cws.removeUserFromGroupInSpace(token, u, groupName, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 46: // removeUserFromSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter user to remove from space");
                                string u = Console.ReadLine();
                                cws.removeUserFromSpace(token, u, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 47: // renameFileOrDirectory
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter file or directory to rename");
                                string fileOrDir = Console.ReadLine();
                                Console.WriteLine("Enter new name");
                                string newName = Console.ReadLine();
                                cws.renameFileOrDirectory(token, spaceId, fileOrDir, newName);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 48: // setDirectoryPermissions
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                string groupName = selectGroupFromSpace(cws, token, spaceId);
                                Console.WriteLine("Enter directory to set permission on");
                                string dir = Console.ReadLine();
                                Console.WriteLine("Enter the name of the permission");
                                string permissionName = Console.ReadLine();
                                cws.setDirectoryPermission(token, spaceId, dir, groupName, permissionName, getBooleanInput("Give user permission (Y/N)?"));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 51: //setVariableInSpace
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the variable");
                                string varName = Console.ReadLine();
                                Console.WriteLine("Enter the value or query for the variable");
                                string query = Console.ReadLine();
                                cws.setVariableInSpace(token, varName, query, spaceId);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 52: // swapSpaceContents
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the first space");
                                string spFrom = selectSpace(cws, token);
                                Console.WriteLine("Enter the second space");
                                string spTo = selectSpace(cws, token);
                                cws.swapSpaceContents(token, spFrom, spTo);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 53: //exportReportToPNG
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the space");
                                string spFrom = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the report");
                                string report = Console.ReadLine();

                                string exportToken = cws.exportReportToPNG(token, spFrom, report, null, 1, 0);
                                waitForComplete(cws, token, exportToken);
                                Console.WriteLine("Base64 encoded report png data is:");
                                string data = cws.getExportData(token, exportToken);
                                Console.WriteLine(data);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 54: //exportReportToPDF
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the space.");
                                string spFrom = selectSpace(cws, token);
                                Console.WriteLine("Enter the name of the report.");
                                string report = Console.ReadLine();

                                string exportToken = cws.exportReportToPNG(token, spFrom, report, null, 1, 0);
                                waitForComplete(cws, token, exportToken);
                                Console.WriteLine("Base64 encoded report png data is:");
                                string data = cws.getExportData(token, exportToken);
                                Console.WriteLine(data);
                                // could use System.Convert.FromBase64String here...
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 56: // setSpaceSSOPassword
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the SSO password");
                                string pw = Console.ReadLine();
                                cws.setSpaceSSOPassword(token, spaceId, pw);
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 57: // getUserReleaseType
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the username");
                                string uname = Console.ReadLine();
                                Console.WriteLine(cws.getUserRelease(token, uname));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 58: // setUserReleaseType
                            token = cws.Login(username, password);
                            try
                            {
                                Console.WriteLine("Enter the username");
                                string uname = Console.ReadLine();
                                Console.WriteLine("Enter the release");
                                string rType = Console.ReadLine();
                                cws.setUserRelease(token, uname, rType);
                                Console.WriteLine(cws.getUserRelease(token, uname));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 61: // getSpaceJNLPFile
                            token = cws.Login(username, password);
                            try
                            {
                                string spaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the config file name (e.g., <name>_dcconfig.xml)");
                                string cfile = Console.ReadLine();
                                Console.WriteLine(cws.getSpaceJNLPFile(token, spaceId, cfile));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                        case 62: // copyFile
                            token = cws.Login(username, password);
                            try
                            {
                                string fromSpaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the from file");
                                string fromFile = Console.ReadLine();
                                string toSpaceId = selectSpace(cws, token);
                                Console.WriteLine("Enter the to file");
                                string toFile = Console.ReadLine();
                                Console.WriteLine("Enter the overwrite flag (true or false)");
                                string overwrite = Console.ReadLine();
                                cws.copyFile(token, fromSpaceId, fromFile, toSpaceId, toFile, bool.Parse(overwrite));
                            }
                            finally
                            {
                                cws.Logout(token);
                            }
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

        }

        private static void publishData(CommandWebService cws, string token, string spaceId)
        {
            Console.WriteLine("Publishing");
            waitForComplete(cws, token, cws.publishData(token, spaceId, "Acorn", null, false, DateTime.Now));
        }

        private static void waitForComplete(CommandWebService cws, string token, string jobToken)
        {
            bool complete = false;
            DateTime now = DateTime.Now;
            while (complete == false)
            {
                System.Threading.Thread.Sleep(1000);
                Console.Write(".");
                complete = cws.isJobComplete(token, jobToken);
            }
            Console.WriteLine("It took " + DateTime.Now.Subtract(now).Seconds + " seconds to complete process!");

            StatusResult status = cws.getJobStatus(token, jobToken);
            Console.WriteLine(status == null ? "no status" : status.message);
        }

        private static void writeList(string[] p)
        {
            foreach (string s in p)
            {
                Console.WriteLine(s);
            }
        }

        private static void executeQuery(CommandWebService cws, string token, string spaceId, string query)
        {
            CommandQueryResult result = cws.executeQueryInSpace(token, query, spaceId);
            for (int i = 0; i < result.displayNames.Length; i++)
            {
                Console.WriteLine("Column " + i + " named " + result.displayNames[i]);
            }

            for (int i = 0; i < result.numRowsReturned; i++)
            {
                Console.Write("row " + i + ": ");
                for (int j = 0; j < result.displayNames.Length; j++)
                {
                    if (j > 0)
                        Console.Write(", ");
                    Console.Write(result.rows[i][j]);
                }
                Console.WriteLine();
            }

            if (result.hasMoreRows)
            {
                result = cws.queryMore(token, result.queryToken);
            }
        }

        private static string selectCustomSubjectArea(CommandWebService cws, string token, string spFromId)
        {
            string[] customSubjectAreas = cws.listCustomSubjectAreas(token, spFromId);
            Console.WriteLine("Select the custom subject area by typing the number at the front of the line and hitting enter.");
            return selectFromStringList(customSubjectAreas);
        }

        private static string selectGroupFromSpace(CommandWebService cws, string token, string spaceId)
        {
            string[] groups = cws.listGroupsInSpace(token, spaceId);
            Console.WriteLine("Select the group by typing the number at the front of the line and hitting enter.");
            return selectFromStringList(groups);
        }
        private static string selectFromStringList(string[] list)
        {
            int i = 0;
            foreach (string group in list)
            {
                Console.Write(i++);
                Console.Write(" ");
                Console.WriteLine(group);
            }
            string line = Console.ReadLine();
            int index = Int32.Parse(line);
            if (index < 0 || index >= list.Length)
                return null;
            return list[index];
        }

        private static string selectSpace(CommandWebService cws, string token)
        {
            UserSpace[] spaces = cws.listSpaces(token);
            Console.WriteLine("Select the space by typing the number at the front of the line and hitting enter..");
            int i = 0;
            foreach (UserSpace space in spaces)
            {
                Console.Write(i++);
                Console.Write(" ");
                Console.Write(space.name);
                Console.Write(" owned by ");
                Console.Write(space.owner);
                Console.WriteLine();
            }

            string line = Console.ReadLine();
            int index = Int32.Parse(line);
            if (index < 0 || index >= spaces.Length)
                return null;
            return spaces[index].id;
        }

        private static bool getBooleanInput(string text)
        {
            Console.WriteLine(text);
            Console.WriteLine("Press Y or N");
            ConsoleKeyInfo cki = Console.ReadKey(true);
            string key = cki.Key.ToString();
            if (key.ToLower().StartsWith("y"))
                return true;
            return false;
        }

        private static int showMenu()
        {
            return showMenu_1();
        }


        private static void uploadData(CommandWebService cws, string token, string spaceId, string filename)
        {
            string source = Path.GetFileName(filename);
            FileStream fStream = File.OpenRead(filename);
            BinaryReader reader = new BinaryReader(fStream);

            string dataUploadToken = cws.beginDataUpload(token, spaceId, source);

            Console.WriteLine();
            byte[] bytes;
            do
            {
                bytes = reader.ReadBytes(CHUNK_SIZE);
                if (bytes.Length > 0)
                {
                    try
                    {
                        // no need to base 64 encode the data.  The stubs generated by C# do the encoding for us
                        // The programming language that you are using will determine whether you need to encode the data or not.
                        /*
                         * // here is sample code of doing base 64 encoding of the data
                         * // we send the encoded data, but the size is still the length of the unencoded data
                        string data = System.Convert.ToBase64String(bytes);
                        byte[] base64Bytes = Encoding.ASCII.GetBytes(data);
                        cws.uploadData(token, dataUploadToken, bytes.Length, base64Bytes);
                         * */
                        cws.uploadData(token, dataUploadToken, bytes.Length, bytes);
                    }
                    catch (Exception) { }
                    Console.Write(".");
                }
            } while (bytes.Length > 0);

            Console.WriteLine();
            Console.WriteLine("Finishing up upload");
            cws.finishDataUpload(token, dataUploadToken);
            DateTime now = DateTime.Now;
            while (cws.isDataUploadComplete(token, dataUploadToken) == false)
            {
                System.Threading.Thread.Sleep(1000);
                Console.Write(".");
            }
            Console.WriteLine("It took " + DateTime.Now.Subtract(now).Seconds + " seconds to process the data load!");
            Console.WriteLine();
            string[] dataUploadWarnings = cws.getDataUploadStatus(token, dataUploadToken);
            if (dataUploadWarnings != null && dataUploadWarnings.Length > 0)
            {
                foreach (string s in dataUploadWarnings)
                {
                    Console.WriteLine(s);
                }
            }
        }


        private static int showMenu_1()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. Add acl to group in space");
                Console.WriteLine("2. Add allowed ips.");
                Console.WriteLine("3. Add group to space.");
                Console.WriteLine("4. Add proxy user.");
                Console.WriteLine("5. Add user.");
                Console.WriteLine("6. Add user to group in space.");
                Console.WriteLine("7. Add user to space.");
                Console.WriteLine("8. Upload data.");
                Console.WriteLine("9. Clear cache in space.");
                Console.WriteLine("0. MORE");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_2();

            return (int)Char.GetNumericValue(key);
        }

        private static int showMenu_2()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. copyCatalogDirectory");
                Console.WriteLine("2. copyCustomSubjectArea");
                Console.WriteLine("3. copyFileOrDirectory");
                Console.WriteLine("4. copySpaceContents");
                Console.WriteLine("5. createNewDirectory");
                Console.WriteLine("6. createNewSpace");
                Console.WriteLine("7. deleteAllDataFromSpace");
                Console.WriteLine("8. deleteFileOrDirectory");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_3();
            else if (key == '9')
                return showMenu_1();

            return (int)Char.GetNumericValue(key) + 10;
        }
        private static int showMenu_3()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. deleteLastDataFromSpace");
                Console.WriteLine("2. deleteSpace");
                Console.WriteLine("3. executeQuery");
                Console.WriteLine("4. getDirectoryContents");
                Console.WriteLine("5. getDirectoryPermissions");
                Console.WriteLine("6. getVariablesForSpace");
                Console.WriteLine("7. listAllowedIps");
                Console.WriteLine("8. listCreatedUsers");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_4();
            else if (key == '9')
                return showMenu_2();

            return (int)Char.GetNumericValue(key) + 20;
        }
        private static int showMenu_4()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. listCustomSubjectAreas");
                Console.WriteLine("2. listGroupAcslInSpace");
                Console.WriteLine("3. listGroupsInSpace");
                Console.WriteLine("4. listProxyUsers");
                Console.WriteLine("5. listSpaced");
                Console.WriteLine("6. listUsersInGroupInSpace");
                Console.WriteLine("7. listUsersInSpace");
                Console.WriteLine("8. publishData");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_5();
            else if (key == '9')
                return showMenu_3();

            return (int)Char.GetNumericValue(key) + 30;
        }
        private static int showMenu_5()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. removeAclFromGroupInSpace");
                Console.WriteLine("2. removeAllowedIps");
                Console.WriteLine("3. removeGroupFromSpace");
                Console.WriteLine("4. removeProxyUser");
                Console.WriteLine("5. removeUserFromGroupInSpace");
                Console.WriteLine("6. removeUserFromSpace");
                Console.WriteLine("7. renameFileOrDirectory");
                Console.WriteLine("8. setDirectoryPermissions");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_6();
            else if (key == '9')
                return showMenu_4();

            return (int)Char.GetNumericValue(key) + 40;
        }
        private static int showMenu_6()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. setVariableInSpace");
                Console.WriteLine("2. swapSpaceContents");
                Console.WriteLine("3. exportReportToPNG");
                Console.WriteLine("4. exportReportToPDF");
                Console.WriteLine("6. setSpaceSSOPassword");
                Console.WriteLine("7. getUserReleaseType");
                Console.WriteLine("8. setUserReleaseType");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '0')
                return showMenu_7();
            if (key == '9')
                return showMenu_5();

            return (int)Char.GetNumericValue(key) + 50;
        }

        private static int showMenu_7()
        {
            char key = 'a';
            do
            {
                Console.WriteLine("Select an operation to test.  Press a number key.");
                Console.WriteLine("1. getSpaceJNLPFile");
                Console.WriteLine("2. copyFile");
                Console.WriteLine("9. PREVIOUS");
                Console.WriteLine("0. NEXT");

                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Escape)
                    return -1;
                key = cki.KeyChar;
            }
            while (key < '0' && key > '9');
            if (key == '9')
                return showMenu_6();

            return (int)Char.GetNumericValue(key) + 60;
        }
    }
}
