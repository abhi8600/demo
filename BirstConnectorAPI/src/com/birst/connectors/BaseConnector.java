package com.birst.connectors;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * This class should be inherited by each connector as it provides some utility methods that are readily available to all connectors  
 * 
 * @author mpandit
 */
public abstract class BaseConnector
  	implements IConnector
{
	public static final String SOURCEFILE_NAME_PREFIX = "SourceFileNamePrefix";
	public static final String SPACE_DIR = "SpaceDir";
	public static final String SPACE_NAME = "SpaceName";
	public static final String SPACE_ID = "SpaceID";
	public static final String SAVE_AUTHENTICATION = "SaveAuthentication";
	public static final String USE_SANDBOX_URL = "UseSandBoxURL";
	public static final String SANDBOX_URL = "SandboxURL";
	public static final String ENVIRONMENT = "EnvironmentType";
	public static final String BIRST_PARAMETERS = "BirstParameters";
	protected String SourceFileNamePrefix = "";
	protected String spaceDir;
	protected String spaceID;
	protected String spaceName;
	protected boolean useSandBoxURL;
	protected String sandboxURL;
	protected String environmentType;
	protected static Map<String, String> birstParameters = new HashMap<String, String>();
	protected boolean isConnected = false;
	protected static final int MAX_CONNECTIONS_PER_HOST = 50;
	protected static final int MAX_TOTAL_CONNECTIONS = 100;
	public static String dateTimeISOStandardFormatPattern = "yyyy-MM-dd'T'HH:mm:ss";
	protected static final Date minDateValue;
	public static char[] new_lang_allowable;
	public static char[] old_lang_allowable;
	char[] allowable = new_lang_allowable;

	static
	{
		Calendar c = Calendar.getInstance();
	    c.set(1, 0, 1, 0, 0, 0);
	    c.set(14, 0);
	    minDateValue = c.getTime();

	    new_lang_allowable = new char[] {
	    			'_', ' ', ':', '$', '#', '%', '-', '/', '&', '!',
	    			'^', '@', ',', ';', '>', '<' };
	    old_lang_allowable = new char[] {
	    			'!', '@', '#', '$', '%', '^', '&', '*', '/', '<',
	    			'>', ',', '|', '=', '+', '-', '_', ';', ':' };
	}

	/**
	 * This method must be overridden by all implementing connectors. This method should be called first when connect method of connector is called. 
	 * This method extracts parameters common to all connectors such as spaceDir, spaceID etc. Since this class is supposed to be inherited by all 
	 * connectors, the inherited attributes will be available to all connectors.  
	 * 
	 * @param props ConnectionProperties to be used for connecting to connector
	 * @param metaData {@link ConnectorMetaData} object for connector
	 * @throws RecoverableException
	 * @throws UnrecoverableException
	 */
	public void connect(Properties props, ConnectorMetaData metaData)
			throws RecoverableException, UnrecoverableException
    {
		this.spaceDir = props.getProperty("SpaceDir");
		this.spaceID = props.getProperty("SpaceID");
	    this.spaceName = props.getProperty("SpaceName");
	    this.sandboxURL = props.getProperty("SandboxURL");
	    this.useSandBoxURL = Boolean.parseBoolean(props.getProperty("UseSandBoxURL"));
	    
	    if (metaData.allowsSourceFileNamePrefix())
	    	this.SourceFileNamePrefix = props.getProperty("SourceFileNamePrefix");
    }

	/**
	 * Method to retrieve any Birst specific parameters used for connector - such as SFDCClientID to be used for salesforce extraction
	 * 
	 * @param paramName
	 * @return parameterValue
	 */
	public String getBirstParameter(String paramName) 
	{
		if (birstParameters == null)
			return null;
		return birstParameters.get(paramName);		
	}
	
	protected final void setBirstParameter(String paramName, String paramValue)
	{
		if (birstParameters != null)
		{
			birstParameters.put(paramName, paramValue);
		}
	}

	/**
	 * Gets SourceFileNamePrefix for connector
	 * 
	 * @return SourceFileNamePrefix
	 */
	public String getSourceFileNamePrefix()
	{
		return this.SourceFileNamePrefix;
	}

	/**
	 * Replaces non allowed characters from string f with an underscore. This method should be used for validating columnname for {@link ExtractionObject} being extracted. 
	 * 
	 * @param f string from which the non allowable characters are to be replaced
	 * @param allowSpaces whether or not to allow spaces as valid characters
	 * @return
	 */
	protected String getAllowable(String f, boolean allowSpaces)
	{
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < f.length(); i++)
		{
			if (((f.charAt(i) >= 'a') && (f.charAt(i) <= 'z')) || ((f.charAt(i) >= 'A') && (f.charAt(i) <= 'Z')) ||
					((f.charAt(i) >= '0') && (f.charAt(i) <= '9')) || ((allowSpaces) && (f.charAt(i) == ' ')))
			{
				sb.append(f.charAt(i));
			}
			else 
			{
				boolean ok = false;
				for (char c : this.allowable) 
				{
					if (f.charAt(i) != c)
						continue;
					ok = true;
					break;
				}
				if (!ok)
					sb.append('_');
				else
					sb.append(f.charAt(i));
			}
		}
		return sb.toString();
	}
	
	public boolean ping() {
		return true;
	}
}
