package com.birst.connectors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.birst.connectors.util.XmlUtils;

/**
 * Represents metadata for connector. Each connector should define its own class that inherits this class to provide the metadata information of the connector.
 * 
 * @author mpandit
 *
 */
public abstract class ConnectorMetaData
{
	public static final String NODE_CONN_PROPERTY = "ConnectionProperty";
	public static final String NODE_METADATA_ROOT = "MetaData";

	/**
	 * Gets name of Connector (eg. SFDC or NETSUITE)
	 * @return ConnectorName
	 */
	public abstract String getConnectorName();

	/**
	 * Gets InterfaceVersion of {@link IConnector} class being implemented by the connector (normally 1.0)
	 * @return InterfaceVersion
	 */
	public abstract String getInterfaceVersion();

	/**
	 * Gets Version of Connector class implementing {@link IConnector} interface (normally 1.0)
	 * @return ConnectorVersion
	 */
	public abstract String getConnectorVersion();

	/**
	 * Gets Version of API being implemented by connector (eg. '26.0' for Salesforce and '2012_2' for NetSuite)
	 * @return ConnectorCloudVersion
	 */
	public abstract String getConnectorCloudVersion();

	/**
	 * Gets list of {@link ExtractionObject.Types} supported by connector
	 * @return list of {@link ExtractionObject.Types}
	 */
	public abstract List<ExtractionObject.Types> getSupportedTypes();

	/**
	 * Gets list of ConnectionProperties that are required for connecting to the connector
	 * @return list of {@link ConnectionProperty}
	 */
	public abstract List<ConnectionProperty> getConnectionProperties();

	/**
	 * Indicates whether the connector supports parallel extraction of objects
	 * @param extractGroupObjectsList - list of extraction object supplied  
	 * @return true if connector supports parallel extraction and false if not
	 */
	public abstract boolean supportsParallelExtraction(List<ExtractionObject> extractGroupObjectsList);

	/**
	 * Indicatees whether the connector allows using SourceFileNamePrefix parameter (must be true for all connectors except for Salesforce)
	 * @return true if connector allows using SourceFileNamePrefix parameter
	 */
	public abstract boolean allowsSourceFileNamePrefix();
	
	/**
	 * Indicatees whether the connector allows use of dynamic parameters based on some other parameter value
	 * @return true if connector requires dynamic params for Object
	 */
	public abstract boolean requiresDynamicParamForObjects();
	
	/**
	 * All connectors must use this method to get base list of ConnectionProperties from super class and then add more properties as necessary for connector for building
	 * list of ConnectionProperty that is returned when getConnectionProperties method is called.
	 * @return list of {@link ConnectionProperty} from base class
	 */
	protected static List<ConnectionProperty> getBaseConnectionProperties()
	{
		List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>();
	    ConnectionProperty sourceFileNamePrefix = new ConnectionProperty();
	    sourceFileNamePrefix.name = BaseConnector.SOURCEFILE_NAME_PREFIX;
	    sourceFileNamePrefix.isRequired = false;
	    sourceFileNamePrefix.displayIndex = 6;
	    sourceFileNamePrefix.displayLabel = "Source File Prefix";
	    connectionProperties.add(sourceFileNamePrefix);
	    ConnectionProperty useSandBoxURL = new ConnectionProperty();
	    useSandBoxURL.name = BaseConnector.USE_SANDBOX_URL;
	    useSandBoxURL.isRequired = false;
	    useSandBoxURL.displayIndex = 3;
	    useSandBoxURL.displayLabel = "Environment";
	    connectionProperties.add(useSandBoxURL);
	    ConnectionProperty saveAuthentication = new ConnectionProperty();
	    saveAuthentication.name = BaseConnector.SAVE_AUTHENTICATION;
	    saveAuthentication.isRequired = true;
	    connectionProperties.add(saveAuthentication);
	    return connectionProperties;
	}

	/**
	 * Checks whether all required ConnectionProperties are supplied
	 * @param props list of properties supplied
	 * @return true if all required properties are found in supplied list of properties and false otherwise
	 */
	public boolean containsRequiredConnectionProperties(Properties props) 
	{
		if (props == null)
			return false;
		for (ConnectionProperty property : getConnectionProperties())
		{
			if ((property.isRequired) && (!props.containsKey(property.name)))
				return false;
		}
		return true;
	}

	/**
	 * Method used by Birst Connector Framework internally. Converts {@link ConnectorMetaData} object to xml string.
	 * @param connectorMetaData metadata object for connector
	 * @return xml String for {@link ConnectorMetaData}
	 */
	public static String toXML(ConnectorMetaData connectorMetaData) 
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement metaDataRoot = factory.createOMElement(NODE_METADATA_ROOT, ns);
		List<ConnectionProperty> connectorProperties = connectorMetaData.getConnectionProperties();
		OMElement connPropElement;
		if ((connectorProperties != null) && (connectorProperties.size() > 0)) 
		{
			for (ConnectionProperty connectionProperty : connectorProperties) 
			{
				if ((connectionProperty.name.equals("SourceFileNamePrefix")) && (!connectorMetaData.allowsSourceFileNamePrefix())) 
				{
					continue;
				}
				connPropElement = factory.createOMElement(NODE_CONN_PROPERTY, ns);
				XmlUtils.addContent(factory, connPropElement, "Name", connectionProperty.name, ns);
				XmlUtils.addContent(factory, connPropElement, "Required", connectionProperty.isRequired, ns);
				XmlUtils.addContent(factory, connPropElement, "Secret", connectionProperty.isSecret, ns);
				XmlUtils.addContent(factory, connPropElement, "Encrypted", connectionProperty.isEncrypted, ns);
				XmlUtils.addContent(factory, connPropElement, "SaveToConfig", connectionProperty.saveToConfig, ns);
				XmlUtils.addContent(factory, connPropElement, "DisplayIndex", connectionProperty.displayIndex, ns);
				XmlUtils.addContent(factory, connPropElement, "DisplayLabel", connectionProperty.displayLabel, ns);
				XmlUtils.addContent(factory, connPropElement, "DisplayType", connectionProperty.displayType, ns);
				XmlUtils.addContent(factory, connPropElement, "Value", connectionProperty.value, ns);
				if(connectionProperty.options != null && !connectionProperty.options.isEmpty())
				{
					OMElement connOptions = factory.createOMElement("Options", ns);
					for(Entry<String, String> entry : connectionProperty.options.entrySet())
					{
						XmlUtils.addContent(factory, connOptions, "EnvKey",entry.getKey(), ns);
						XmlUtils.addContent(factory, connOptions, "EnvValue",entry.getValue(), ns);
					}
					connPropElement.addChild(connOptions);
				}
				
				metaDataRoot.addChild(connPropElement);
			}
		}

		List<ExtractionObject.Types> supportedTypes = connectorMetaData.getSupportedTypes();
		if ((supportedTypes != null) && (supportedTypes.size() > 0)) 
		{
			for (ExtractionObject.Types type : supportedTypes) 
			{
				connPropElement = factory.createOMElement(NODE_CONN_PROPERTY, ns);
				XmlUtils.addContent(factory, connPropElement, "Name", type.toString(), ns);
				metaDataRoot.addChild(connPropElement);
			}
		}
		return XmlUtils.convertToString(metaDataRoot);
	}
	
	/**
	 * Method used by Birst Connector Framework internally. Parses ConnectionProperties from given xml element.
	 * @param connectorMetaDataElement xml element
	 * @return list of {@link ConnectionProperty} parsed
	 */
	public static List<ConnectionProperty> parseConnectionProperties(OMElement connectorMetaDataElement)
	{
		if (connectorMetaDataElement == null)
			return null;
		List<ConnectionProperty> connectionProperties = getBaseConnectionProperties();
		for (Iterator<OMElement> resultIterator = connectorMetaDataElement.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement connPropElement = resultIterator.next();
			if (NODE_CONN_PROPERTY.equals(connPropElement.getLocalName()))
			{
				ConnectionProperty cp = new ConnectionProperty();
				boolean containsOtherProperties = false;
				for (Iterator<OMElement> connPropIterator = connPropElement.getChildElements(); connPropIterator.hasNext(); )
				{
					OMElement cpEl = connPropIterator.next();
					if ("Name".equals(cpEl.getLocalName()))
					{
						cp.name = cpEl.getText();
					}
					else if ("Required".equals(cpEl.getLocalName()))
					{
						cp.isRequired = Boolean.parseBoolean(cpEl.getText());
						containsOtherProperties = true;
					}
					else if ("Secret".equals(cpEl.getLocalName()))
					{
						cp.isSecret = Boolean.parseBoolean(cpEl.getText());
						containsOtherProperties = true;
					}
					else if ("Encrypted".equals(cpEl.getLocalName()))
					{
						cp.isEncrypted = Boolean.parseBoolean(cpEl.getText());
						containsOtherProperties = true;
					}
					else if ("SaveToConfig".equals(cpEl.getLocalName()))
					{
						cp.saveToConfig = Boolean.parseBoolean(cpEl.getText());
						containsOtherProperties = true;
					}
					else if ("Value".equals(cpEl.getLocalName()))
					{
						String value = cpEl.getText();
						if (value == null || value.trim().isEmpty())
							cp.value = null;
						else
							cp.value = cpEl.getText();
						containsOtherProperties = true;
					}
				}
				boolean found = false;
				for (ConnectionProperty connProp : connectionProperties)
				{
					if (connProp.name.equals(cp.name))
					{
						connProp.value = cp.value;
						found = true;
					}
				}
				if (!found && containsOtherProperties)
					connectionProperties.add(cp);
			}			
		}
		return connectionProperties;
	}
}