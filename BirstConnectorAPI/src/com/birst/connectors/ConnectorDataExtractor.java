package com.birst.connectors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.birst.connectors.ExtractionProgressHandler.ExtractionStatus;
import com.birst.connectors.util.Util;


public class ConnectorDataExtractor extends Thread 
{
	private IConnector connector;
	private BlockingQueue<ExtractionObject> queue;
	private String spaceDirectory;
	private String uid;
	List<ExtractionResult> extractionResults;
	
	private int thresholdRecordCount = 1000;
	private static final int maxRetries = 5;	
	private static final long retrySleepInMilliseconds = 10 * 1000;
	public static final String errorMessagePrefix = ">>> ";
	public static final String statusFileSuffix = "-status.txt";
	private static final Logger logger = Logger.getLogger(ConnectorDataExtractor.class);
	
	public ConnectorDataExtractor(IConnector connector, int threadNo, BlockingQueue<ExtractionObject> queue, String spaceDirectory, String uid, List<ExtractionResult> extractionResults) 
			throws RecoverableException, UnrecoverableException
	{
		super("ConnectorDataExtractor-" + threadNo);
		this.connector = connector;
		this.queue = queue;
		this.spaceDirectory = spaceDirectory;
		this.uid = uid;
		this.extractionResults = extractionResults;
	}
	
	public void run()
	{
		while(!queue.isEmpty())
		{
			String killFileName = Util.getKillFileName(spaceDirectory, Util.KILL_EXTRACT_FILE);
			if(Util.isTerminated(killFileName))
			{	
				writeOutputFile("killed-extract-");
				break;
			}
			ExtractionObject type = queue.poll();
			if(type == null)
			{
				if (logger.isTraceEnabled())
					logger.trace("No object found in queue");
				return;
			}
			try
			{
				BufferedWriter writer = null;
				String fileName = type.name;
				String prefix = ((BaseConnector)connector).getSourceFileNamePrefix();
				if (prefix != null && !prefix.trim().isEmpty())
					fileName = prefix + "_" + fileName;
				fileName = spaceDirectory + File.separator + "data" + File.separator + fileName + ".txt";
				String statusFileName = spaceDirectory + File.separator + "connectors" + File.separator + ((prefix != null && !prefix.trim().isEmpty()) ? prefix + "_" : "") + type.name + statusFileSuffix;
				ExtractionProgressHandler hndlr = new ExtractionProgressHandler(thresholdRecordCount, statusFileName, killFileName);
				try
				{
					logger.info("Starting extract for object : " + type.name);
					long startTime = System.currentTimeMillis();
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, false), "UTF-8"));
					//BPD-18779
					writeDoneFlag();
					ExtractionResult result = connector.fetchData(writer, type, hndlr);
					extractionResults.add(result);
					int numRows = result.getRowCount();
					long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
					logger.info("Successfully extracted object " + type.name + " : Total Time taken : " + timeTakenInSeconds + "(milliseconds)" + " : numRows : " + numRows);
					type.status = ExtractionObject.SUCCESS;
					if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Complete)
						hndlr.updateProgress(ExtractionStatus.Complete, numRows);
				}
				catch (RecoverableException re)
				{
					if (type.noRetries < maxRetries)
					{
						logger.warn("Recoverable Exception encountered extracting object " + type.name + " - "+ re.getMessage() + " (retrying)");
						type.noRetries++;
						logger.debug("Retrying extraction for object " + type.name + ": number of retries: " + type.noRetries);
						Thread.sleep(retrySleepInMilliseconds);
						queue.offer(type);
						continue;
					}
					else
					{	
						logger.error("Recoverable Exception encountered extracting object  " + type.name + " - maximum number of retries reached, marking extraction as failed : " + re.getMessage(), re);
						type.status = ExtractionObject.FAILED;
						if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Failed)
							hndlr.updateFailedProgress();
					}
				}
				catch (UnrecoverableException ure)
				{
					//replace newline with newline>>>> so last extract logs can parse error message properly
					String errorMsg = (ure.getMessage() != null ? ure.getMessage().replace("\n", "\n" + errorMessagePrefix) : null);
					logger.error("Unrecoverable Exception encountered extracting object " + type.name + " - "+ errorMsg, ure);	    
					type.status = ExtractionObject.FAILED;
					if (hndlr != null)
					{
						if(hndlr.getLoggedStatus() == ExtractionStatus.Killed)
						{
							writeOutputFile("killed-extract-");
						}
						else if(hndlr.getLoggedStatus() != ExtractionStatus.Failed)
						{
							hndlr.updateFailedProgress();
						}
					}
				}
				finally
				{
					if (writer != null)
					{
						try
						{
							logger.debug("Closing the file writer for the object " + type.name);
							writer.close();
						}
						catch (IOException ie)
						{
							logger.error(ie.getMessage(), ie);
						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.error(ex, ex);
			}
		}
	}
	
	private synchronized void writeOutputFile(String fileNamePart)
	{
		String outputFileName = null;
		try
		{
			if(uid != null)
			{	
				outputFileName = spaceDirectory + File.separator + "slogs" + File.separator + fileNamePart + uid + ".txt";
				File file = new File(outputFileName);
				if(!file.exists())
				{	
					file.createNewFile();
				}
			}
		}
		catch(Exception ex)
		{
			logger.warn("Error while creating out the success file file " +  outputFileName);
		}
	}
	
	//BPD-18779
	private synchronized void writeDoneFlag()
	{
		String connectorName = null;
		String doneFlag = null;
		
		try {
			connectorName = connector.getMetaData().getConnectorName().toLowerCase();

			if(!connectorName.contains("netsuite") || !queue.isEmpty())
				return;

			if(queue.isEmpty()){
				String tempDirName = spaceDirectory + File.separator + "tempDepData";
				File tempDir = new File(tempDirName);
				if(!tempDir.exists()) 
					tempDir.mkdir();
				
				doneFlag = tempDirName + File.separator + "_DONE";
				File file = new File(doneFlag);
				if(!file.exists())
				{	
					file.createNewFile();
				}
			}

		}catch(Exception ex)
		{
			logger.warn("Error while creating out the flag file: " +  doneFlag);
		}
		
	}
}
