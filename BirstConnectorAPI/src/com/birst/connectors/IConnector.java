package com.birst.connectors;

import java.io.Writer;
import java.util.List;
import java.util.Properties;

import com.birst.connectors.parameter.Parameter;

/**
 * Interface that must be implemented by each connector in order for connector to provide operations required by framework. 
 * 
 * @author mpandit
 */
public interface IConnector
{
	/**
	 * Attempts to connect to connector using connection properties provided. This method should perform call equivalent of 'login' using credentials provided.
	 * 
	 * @param connectionProperties properties to be used for connecting to connector
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered 
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector, such as invalid/insufficient credentials supplied for connect    
	 */
	public abstract void connect(Properties connectionProperties)
			throws RecoverableException, UnrecoverableException;

	/**
	 * This method can be called on connector even before connect is called on connector. This method returns {@link ConnectorMetaData} object that contains metadata 
	 * information about the connector. This method is called by Connector Framework to render generic UI for ConnectionProperties and ExtractionTypes.
	 * 
	 * @return {@link ConnectorMetaData} object for connector
	 * @throws UnrecoverableException
	 * 		Should be thrown for any exceptions thrown by connector when performing this operation
	 */
	public abstract ConnectorMetaData getMetaData()
			throws UnrecoverableException;

	/**
	 * This method should throw exception if an attempt to perform getCatalog operation is made without connecting to connector. This method returns catalog that contains
	 * User Friendly names of Objects that are available from connector to extract data. This catalog should not only show all Standard Objects available in connector, 
	 * but should also show all Custom Objects that the user has access to.
	 * <p>
	 * When extraction is performed for any objects selected from catalog, Birst will generate sourcefile for each selected object with the same name as object, optionally
	 * with SourceFilePrefix depending upon whether the user has selected value for SourceFilePrefix.
	 * 
	 * @return Array of String containing display name of Objects that are available to extract data from connector
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered 
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector when performing this operation    
	 */
	public abstract String[] getCatalog()
			throws RecoverableException, UnrecoverableException;

	/**
	 * This method performs data extraction for given {@link ExtractionObject} based on the type of ExtractionObject. This method should throw exception if an attempt to
	 * perform fetchData operation is made without connecting to connector.  
	 * <p>
	 * Each connector should log the time taken for this operation. This operation needs to make use of 'UseLastModified' property of ExtractionObject in order to determine
	 * whether to perform incremental extraction of data or not. In case of incremental extraction using 'LastModifiedDate' field on Object, 'lastSystemModStamp' attribute of 
	 * ExtractionObject must be updated to MAX(LastModifiedDate) found for extracted data.
	 * <p>
	 * The file being written by this operation must write column headers for object in first line followed by 
	 * rows of data extracted. Each column header and value should be separated explicitly by '|'. When a column value contains any of '\r', '\n', '"', '|' or '\', the value
	 * must be enclosed in double quotes ("") and '"', '|' and '\' must be escaped using escape character ("\")
	 * <p>
	 * In case of paging being used to extract data, after receiving response for each page, the connector must check if the extraction is 'killed' by user and abort extraction
	 * as follows :
	 * <pre>
	 * {@code}
	 * if(hndlr != null && !hndlr.shouldContinue())
	 * {
     * 		hndlr.updateProgress(ExtractionStatus.Killed, numRecords);
     * 		throw new UnrecoverableException(type.name + " Extraction Killed", new Exception(type.name + " Extraction Killed"));
     * }
	 * </pre>
	 * <p>
	 * In the same way, after processing each page received with data, connector should update the status with no. of rows extracted as follows:
	 * <pre>
	 * {@code}
	 * if (hndlr != null && (numRecords % hndlr.getThresholdRecordCount() == 0))
	 * {
     * 		hndlr.updateProgress(ExtractionStatus.Running, numRecords);
     * }
	 * </pre>
	 * 
	 * @param writer StreamWriter for writing extracted data
	 * @param extractionObject Can be any valid {@link ExtractionObject}
	 * @param extractionProgressHandler {@link ExtractionProgressHandler} object supplied by framework for updating progress of extraction in UI
	 * @return {@link ExtractionResult}
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector when performing this operation
	 */
	public abstract ExtractionResult fetchData(Writer writer, ExtractionObject extractionObject, ExtractionProgressHandler extractionProgressHandler)
			throws RecoverableException, UnrecoverableException;

	/**
	 * This operation is currently only used for ExtractionObjects of type QUERY. This operation attempts to generate label to id mapping for all columns available in ExtractionObject.
	 * 
	 * @param extractionObject the object for which the mapping is to be discovered
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector when performing this operation
	 */
	public abstract void fillObjectDetails(ExtractionObject extractionObject)
			throws RecoverableException, UnrecoverableException;

	/**
	 * This operation is currently only used for ExtractionObjects of type QUERY. This operation attempts to validate the Query syntax by issuing LIMIT 0 query against connector.
	 * 
	 * @param extractionObject the object for which the mapping is to be discovered
	 * @return true if the Query is valid and false if it is not
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector when performing this operation
	 */
	public abstract boolean validateQueryForObject(ExtractionObject extractionObject)
			throws RecoverableException, UnrecoverableException;
	
	/**
	 * This operation is currently only used for ExtractionObjects of type SAVED_OBJECT. This operation attempts to validate the custom object columns and selection criteria.
	 * 
	 * @param extractionObject the object for which the mapping is to be discovered
	 * @return true if the object columns and selection criteria are valid and false if it is not
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered
	 * @throws UnrecoverableException
	 * 		Should be thrown for any other exceptions thrown by connector when performing this operation
	 */
	public abstract boolean validateObject(ExtractionObject extractionObject)
			throws RecoverableException, UnrecoverableException;
	
	/**
	 * This operation is used to ping the connector instance.
	 * 
	 * @param N/A
	 * @return true if the instance is running
	 * @throws RecoverableException
	 * 		Should be thrown for exceptions that might be generated due to communication problems to connector, such as IOException, Birst will use retry logic 
	 * when RecoverableException is encountered
	 */
	public abstract boolean ping() 
			throws RecoverableException;
	/**
	 * This operation is used to get dynamic parameters for the connector instance.
	 * 
	 * @param properties
	 * @return List of {@link Parameter}
	 * @throws UnrecoverableException
	 */
	public abstract List<Parameter> getDynamicParameterList(Properties properties)
			throws UnrecoverableException;
	
	/**
	 * This operation is used to get dynamic parameters based on other parameter.
	 * 
	 * @param extractionObject
	 * @return List of {@link Parameter}
	 * @throws UnrecoverableException
	 */
	public abstract List<Parameter> fillDynamicParametersForObject(ExtractionObject extractionObject)
			throws RecoverableException, UnrecoverableException;
}