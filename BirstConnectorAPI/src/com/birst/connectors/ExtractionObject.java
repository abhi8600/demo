package com.birst.connectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Represents generic type for connectors for which extraction can be performed
 * 
 * @author mpandit
 *
 */
public class ExtractionObject
{
	/**
	 * Enum that defines the types of ExtractionObject. The extraction may differ for each of the type.
	 * Each connector needs to define the types of ExtractionObject it supports.
	 * 
	 * @author mpandit
	 *
	 */
	public enum Types {
		OBJECT, QUERY, SAVED_OBJECT, SAVED_SEARCH_OBJECT, SAVED_GOOGLE_ANALYTICS_QUERY, SAVED_OMNITURE_SITECATALYST_OBJECT
	}
	
	public int noRetries = 0;
	public String name;
	public String techName;
	public Types type;
	public String[] columnNames;
	public String query;
	public Boolean includeAllRecords = new Boolean(false) ;
	private Map<String, String> variableMap;
	public String recordType;
	public String objectId;
	public String url;
	public String profileID;
	public String dimensions;
	public String metrics;
	public String segmentID;
	public String filters;
	public String startDate;
	public String endDate;
	public String samplingLevel;
	public String reportSuiteID;
	public String elements;
	public String pageSize;
	public boolean useLastModified;
	public Date lastSystemModStamp;
	public Date lastUpdatedDate;
	public int status;
	public List<String> extractionGroupNames = null;
	public static final int SUCCESS = 1;
	public static final int FAILED = -1;
    public static final String EXT_GROUPS_DELIM = ",";
    public static final String EXT_GROUP_NAMES = "ExtractionGroupNames";
    public static final String EXT_GROUP_NAME = "ExtractionGroupName";
    public static final String XML_OPEN = "<";
    public static final String XML_CLOSE = ">";
    public static final String XML_END = "</";
	public String columnMappings;
	public String selectionCriteria;
	
	public ExtractionObject()
	{
		extractionGroupNames = new ArrayList<String>();
	}

	/**
	 * Adds column to this ExtractionObject
	 * 
	 * @param s columnname
	 */
	public void addColumnName(String s)
	{
		if (this.columnNames == null)
		{
			this.columnNames = new String[1];
			this.columnNames[0] = s;
		}
		else
		{
			boolean found = false;
			for (String cs : this.columnNames)
			{
				if (!s.equals(cs))
					continue;
				found = true;
				break;
			}

			if (!found)
			{
				List<String> old = new ArrayList<String>(Arrays.asList(this.columnNames));
				old.add(s);
				this.columnNames = ((String[])old.toArray(new String[old.size()]));
			}
		}
	}

	// Getters and setters.
	public Map<String, String> getVariableMap() {
		return this.variableMap;
	}
	
	public void setVariableMap(Map<String,String> variableMap) {
		this.variableMap = variableMap;
	}
	
	public String getRecordType() {
		return recordType;
	}
	
	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public String getDimensions() {
		return dimensions;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}

	public String getSegmentID() {
		return segmentID;
	}

	public void setSegmentID(String segmentID) {
		this.segmentID = segmentID;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSamplingLevel() {
		return samplingLevel;
	}

	public void setSamplingLevel(String samplingLevel) {
		this.samplingLevel = samplingLevel;
	}
	
	public String getReportSuiteID() {
		return reportSuiteID;
	}
	public void setReportSuiteID(String reportSuiteID) {
		this.reportSuiteID = reportSuiteID;
	}
	public String getElements() {
		return elements;
	}
	public void setElements(String elements) {
		this.elements = elements;
	}
	public Boolean isIncludeAllRecords() {
		return includeAllRecords;
	}
	
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}	
	public void setIncludeAllRecords(Boolean includeAllRecords) {
		this.includeAllRecords = includeAllRecords;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public String getURL() {
		return url;
	}

	public void setURL(String url) {
		this.url = url;
	}
	
	public List<String> getExtractionGroupNames() {
		return extractionGroupNames;
	}
	
	public void setExtractionGroupNames(List<String> extGroupNames) {
		extractionGroupNames = extGroupNames;
	}
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public void setExtractionGroupNames(String extGroupNames) {
		extractionGroupNames = new ArrayList<String>();
		if (extGroupNames != null) {
			String[] extGroups = extGroupNames.split(EXT_GROUPS_DELIM);
			for (String extGroup : extGroups) {
				extractionGroupNames.add(extGroup);
			}
		}
	}
	
	public String toXml() {
		List<String> extGroups = getExtractionGroupNames();
		StringBuilder sb = new StringBuilder();
		int length = extGroups.size();
		if (length > 0) {
			sb.append(buildOpenTag(EXT_GROUP_NAMES));
			for (String extGroup : extGroups) {
				sb.append(buildOpenTag(EXT_GROUP_NAME)).append(extGroup).append(buildCloseTag(EXT_GROUP_NAME));
			}
			sb.append(buildCloseTag(EXT_GROUP_NAMES));
		}
		return sb.toString();
	}
	
	private String buildOpenTag(String name) {
		StringBuilder sb = new StringBuilder();
		return (sb.append(XML_OPEN).append(name).append(XML_CLOSE)).toString();
	}
	
	private String buildCloseTag(String name) {
		StringBuilder sb = new StringBuilder();
		return (sb.append(XML_END).append(name).append(XML_CLOSE)).toString();
	}
	
	@Override
	public String toString() {
		List<String> extGroups = getExtractionGroupNames();
		StringBuilder sb = new StringBuilder();
		int length = 0;
		
		// Build delimited extraction group string.
		for (String extGroup : extGroups) {
			if (extGroup != null && extGroup.length() > 0) {
				sb.append(extGroup.trim()).append(EXT_GROUPS_DELIM);
			}
		}
		
		// Trim delimited list, if necessary.
		length = sb.length();
		if (length > 0) {
			// First character.
			if (String.valueOf(sb.charAt(0)).equals(EXT_GROUPS_DELIM)) {
				sb.deleteCharAt(0);
			}
			
			// Last character.
			sb.setLength(sb.length() - 1);
		}
		
		return sb.toString();
	}
}