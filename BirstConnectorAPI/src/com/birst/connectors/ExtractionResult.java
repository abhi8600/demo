package com.birst.connectors;

/**
 * Class that represents extraction result used to send API usage and rows fetched for current extraction.  
 * 
 * @author rchandarana
 */
public class ExtractionResult 
{
	private int rowCount;
	private int apiUsage;
	
	/**
	 * Get the total number of records fetched in current data extraction
	 * @return rowCount
	 */
	public int getRowCount() 
	{
		return rowCount;
	}
	
	/**
	 * Set the total number of records fetched in current data extraction
	 * @param rowCount
	 */
	
	public void setRowCount(int rowCount) 
	{
		this.rowCount = rowCount;
	}
	
	/**
	 * Get the API Usage for current extraction
	 * @return apiUsage
	 */
	public int getApiUsage() 
	{
		return apiUsage;
	}
	
	/**
	 * Set the API Usage for current extraction
	 * @param apiUsage
	 */
	public void setApiUsage(int apiUsage) 
	{
		this.apiUsage = apiUsage;
	}
}
