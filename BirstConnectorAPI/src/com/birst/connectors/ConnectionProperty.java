package com.birst.connectors;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Represents generic class that specifies the attribute required for connecting to a connector.
 * Attributes of this class are:
 * name - name of the ConnectionProperty (eg.username)
 * value - value provided by user for the ConnectionProperty
 * saveToConfig - whether to save this property in connector config file or not (default value is true)
 * isSecret - whether this ConnectionProperty should be shown as '***' in UI (eg. password)
 * isRequired - whether this ConnectionProperty is mandatory
 * isEncrypted - whether this ConnectionProperty is to be saved in encrypted form in config file
 * 
 * @author mpandit
 *
 */
public class ConnectionProperty
{
	public static final String DISPLAY_TYPE_UPLOAD_FILE = "upload";
	public static final String DISPLAY_TYPE_TEXT_BOX = "textbox";
	public static final String DISPLAY_TYPE_RADIO_BUTTON = "radiobutton";
	public static final String DISPLAY_TYPE_COMBO_BOX = "combobox";
	public static final String DISPLAY_TYPE_BUTTON = "button";
	public static final String DISPLAY_TYPE_TEXTAREA = "textarea";
	public String name;
	public String value;
	public boolean saveToConfig;
	public boolean isSecret;
	public boolean isRequired;
	public boolean isEncrypted;
	public int displayIndex = -1;
	public String displayLabel;
	public String displayType;
	public Map<String,String> options=new LinkedHashMap<String,String>();

	public ConnectionProperty()
	{
		this.saveToConfig = true;
	}
}