package com.birst.connectors.parameter;

import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.birst.connectors.ConnectorMetaData;
import com.birst.connectors.util.XmlUtils;

public class ParameterUtil 
{
	//Display Types
	public static final String DISPLAY_TYPE_UPLOAD_FILE = "upload";
	public static final String DISPLAY_TYPE_TEXT_BOX = "textbox";
	public static final String DISPLAY_TYPE_RADIO_BUTTON = "radiobutton";
	public static final String DISPLAY_TYPE_COMBO_BOX = "combobox";
	public static final String DISPLAY_TYPE_BUTTON = "button";
	public static final String DISPLAY_TYPE_TEXTAREA = "textarea";
	
	//XML Nodes
	public static final String NODE_DYNAMIC_PARAMETER_ROOT = "DynamicParameterList";
	public static final String NODE_DYNAMIC_PARAMETER = "DynamicParameter";
	public static final String NODE_PARAMETER_NAME = "ParameterName";
	public static final String NODE_IS_MULTISELECT = "IsMultiSelect";
	public static final String NODE_PARAMETER_GROUP_LIST = "ParameterGroupList";
	public static final String NODE_PARAMETER_GROUP = "ParameterGroup";
	public static final String NODE_GROUP_NAME = "GroupName";
	public static final String NODE_PARAMETER_VALUE_LIST = "ParameterValueList";
	public static final String NODE_PARAMTER_VALUE = "ParameterValue";
	public static final String NODE_LABEL = "Label";
	public static final String NODE_VALUE = "Value";
	
	/**
	 * Method used by Birst Connector Framework internally. Converts List of {@link Parameter} object to xml string.
	 * @param connectorMetaData metadata object for connector
	 * @return xml String for list of {@link Parameter}
	 */
	public static String toXML(List<Parameter> parameters) 
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parameterRoot = factory.createOMElement(NODE_DYNAMIC_PARAMETER_ROOT, ns);
		if ((parameters != null) && (parameters.size() > 0)) 
		{
			OMElement parameterElement;
			for (Parameter parameter : parameters) 
			{
				parameterElement = factory.createOMElement(NODE_DYNAMIC_PARAMETER, ns);
				XmlUtils.addContent(factory, parameterElement, NODE_PARAMETER_NAME, parameter.getName(), ns);
				XmlUtils.addContent(factory, parameterElement, NODE_IS_MULTISELECT, parameter.isMultiSelect(), ns);
				OMElement groupListElement = factory.createOMElement(NODE_PARAMETER_GROUP_LIST, ns);		
				Map<String, List<ParameterValue>> parameterValuesByGroup = parameter.getParameterValuesByGroup();
				if (parameterValuesByGroup != null && !parameterValuesByGroup.isEmpty())
				{
					for (Map.Entry<String, List<ParameterValue>> entry : parameterValuesByGroup.entrySet()) 
					{
						OMElement groupElement = factory.createOMElement(NODE_PARAMETER_GROUP, ns);
						String groupName = entry.getKey();
						XmlUtils.addContent(factory, groupElement, NODE_GROUP_NAME, groupName, ns);
						OMElement groupItemListElement = factory.createOMElement(NODE_PARAMETER_VALUE_LIST, ns);
					    List<ParameterValue> parameterValueList = entry.getValue();
					    for (ParameterValue paramValue : parameterValueList)
					    {
					    	OMElement groupItemElement = factory.createOMElement(NODE_PARAMTER_VALUE, ns);
					    	XmlUtils.addContent(factory, groupItemElement, NODE_LABEL, paramValue.getLabel(), ns);
					    	XmlUtils.addContent(factory, groupItemElement, NODE_VALUE, paramValue.getValue(), ns);
					    	groupItemListElement.addChild(groupItemElement);
					    }
					    groupElement.addChild(groupItemListElement);
					    groupListElement.addChild(groupElement);
					}					
				}
				parameterElement.addChild(groupListElement);
				parameterRoot.addChild(parameterElement);
			}
		}		
		return XmlUtils.convertToString(parameterRoot);
	}
}
