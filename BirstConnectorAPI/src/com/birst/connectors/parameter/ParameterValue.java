package com.birst.connectors.parameter;

/**
 * This class represents dynamic parameter value
 * @author rchandarana
 *
 */
public class ParameterValue 
{
	private String label;
	private String value;
	
	public ParameterValue()
	{
		super();
	}
	
	public ParameterValue(String label, String value)
	{
		this.label = label;
		this.value = value;
	}
	
	public String getLabel() 
	{
		return label;
	}
	
	public void setLabel(String label) 
	{
		this.label = label;
	}
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}
}
