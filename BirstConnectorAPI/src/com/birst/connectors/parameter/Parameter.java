package com.birst.connectors.parameter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents dynamic parameter
 * @author rchandarana
 *
 */
public class Parameter 
{
	private String name;
	private LinkedHashMap<String, List<ParameterValue>> parameterValuesByGroup;
	private Boolean isMultiSelect;
	
	public Parameter()
	{
		super();
	}
	
	public Parameter(String name, Boolean isMultiSelect, LinkedHashMap<String, List<ParameterValue>> parameterValuesByGroup)
	{
		this.name = name;
		this.isMultiSelect = isMultiSelect;
		this.parameterValuesByGroup = parameterValuesByGroup;
	}
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Map<String, List<ParameterValue>> getParameterValuesByGroup() 
	{
		return parameterValuesByGroup;
	}
	
	public void setParameterValuesByGroup(LinkedHashMap<String, List<ParameterValue>> parameterValuesByGroup) 
	{
		this.parameterValuesByGroup = parameterValuesByGroup;
	}
	
	public Boolean isMultiSelect() 
	{
		return isMultiSelect;
	}
	
	public void setIsMultiSelect(Boolean isMultiSelect) 
	{
		this.isMultiSelect = isMultiSelect;
	}
}
