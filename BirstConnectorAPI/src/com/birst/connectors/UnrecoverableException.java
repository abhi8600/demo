package com.birst.connectors;

import com.birst.connectors.util.BaseConnectorException;

/**
 * Exception to be thrown by connectors for any exception that cannot be fixed simply using retries. It also allows to set specific error code to take 
 * appropriate actions using it.
 * @author mpandit
 *
 */
public class UnrecoverableException extends BaseConnectorException
{
	private static final long serialVersionUID = 1L;
	private int errorCode = 0;

	public UnrecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public int getErrorCode()
	{
		return this.errorCode;
	}
}