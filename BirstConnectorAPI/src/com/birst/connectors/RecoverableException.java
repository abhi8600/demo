package com.birst.connectors;

import com.birst.connectors.util.BaseConnectorException;

/**
 * Exception to be thrown by connectors for any exception that can be due to communication problems for which retrying should fix the problem. The framework
 * will use retry logic when this exception is encountered before it gives up. 
 * @author mpandit
 *
 */
public class RecoverableException extends BaseConnectorException
{
	private static final long serialVersionUID = 1L;

	public RecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
