package com.birst.connectors.util;

import java.awt.Color;
import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * @author agarrison
 * 
 */
public class XmlUtils
{
	public static final String FONT_BOLDITALIC = "BOLDITALIC";
	public static final String FONT_ITALIC = "ITALIC";
	public static final String FONT_BOLD = "BOLD";
	public static final String FONT_PLAIN = "PLAIN";
	private static Logger logger = Logger.getLogger(XmlUtils.class);
	private static final String XML_STREAM_ERROR = "Can't convert String-ified XML to OMElement.";
	public static final String dateFormatPattern = "yyyy-MM-dd HH:mm:ss";
	public static final String XML_EOL = "&#xD;";
	private static final ThreadLocal<DateFormat> formats = new ThreadLocal<DateFormat>() {
		 @Override protected DateFormat initialValue() { return new SimpleDateFormat(dateFormatPattern); }
	};
	
	public static final String timeZoneDateFormatPattern = "yyyy-MM-dd HH:mm:ss zzz";
	private static final ThreadLocal<DateFormat> gmtFormats = new ThreadLocal<DateFormat>() {
		 @Override protected DateFormat initialValue() { 
			   SimpleDateFormat sdf = new SimpleDateFormat(timeZoneDateFormatPattern);
			   sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			   return sdf;
			 }
	};
	
	/**
	 * return a secure XML input factory
	 * - stops the processing of any ENTITIES
	 * @return
	 */
	public static XMLInputFactory getSecureXMLInputFactory()
	{
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.FALSE);
		return inputFactory;
	}
	
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param value
	 * @param ns
	 */
	public static void addContent(Element parent, String elementName, String value, Namespace ns)
	{
		Element element = new Element(elementName, ns);
		element.setText(value);
		parent.addContent(element);
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param value
	 * @param ns
	 */
	public static void addContent(Element parent, String elementName, boolean value, Namespace ns)
	{
		addContent(parent, elementName, Boolean.toString(value), ns);
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param value
	 * @param ns
	 */
	public static void addContent(Element parent, String elementName, int value, Namespace ns)
	{
		addContent(parent, elementName, Integer.toString(value), ns);
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param value
	 * @param ns
	 */
	public static void addContent(Element parent, String elementName, Color value, Namespace ns)
	{
		int color = value.getRGB() & 0xFFFFFF; // (value.getRed() * 255 + value.getGreen()) * 255 + value.getBlue();
		addContent(parent, elementName, Integer.toHexString(color), ns);
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param value
	 * @param ns
	 */
	public static void addContent(Element parent, String elementName, Font value, Namespace ns)
	{
		if (value == null)
		{
			return;
		}
		addContent(parent, elementName, encodeFont(value), ns);
	}
	/** Retrieve the style name string
	 * 
	 * @param font
	 * @return String
	 */
	public static String getStyleName(Font font)
	{
		int style = font.getStyle();
		return XmlUtils.getStyleName(style);
	}
	/** Retrieve the style name string
	 * 
	 * @param style
	 * @return String
	 */
	public static String getStyleName(int style)
	{
		if (style == Font.PLAIN)
		{
			return FONT_PLAIN;
		}
		if (style == Font.BOLD)
		{
			return FONT_BOLD;
		}
		if (style == Font.ITALIC)
		{
			return FONT_ITALIC;
		}
		return FONT_BOLDITALIC;
	}
	
	public static int convertStyleName(String style) {
		if (FONT_BOLDITALIC.equalsIgnoreCase(style))
			return Font.BOLD | Font.ITALIC;
		
		if (FONT_BOLD.equalsIgnoreCase(style))
			return Font.BOLD;
		
		if (FONT_ITALIC.equalsIgnoreCase(style))
			return Font.ITALIC;
		
		return Font.PLAIN;
	}
	/**
	 * 
	 * @param font
	 * @return String
	 */
	public static String encodeFont(Font font)
	{
		return font.getName() + "-" + getStyleName(font) + "-" + font.getSize();
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Integer
	 */
	public static Integer getIntContent(Element parent, String elementName, Namespace ns)
	{
		String value = getStringContent(parent, elementName, ns);
		if (value != null)
		{
			try {
				Float d = Float.parseFloat(value);
				if (d != null) {
					long l = Math.round(d);
					return Integer.valueOf((int)l);
				}
			}
			catch (Exception e) { }
		}
		return null;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Long
	 */
	public static Long getLongContent(Element parent, String elementName, Namespace ns)
	{
		String value = getStringContent(parent, elementName, ns);
		if (value != null)
		{
			try {
				Float d = Float.parseFloat(value);
				if (d != null) {
					long l = Math.round(d);
					return Long.valueOf(l);
				}
			}
			catch (Exception e) { }
		}
		return null;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Color
	 */
	public static Color getColorContent(Element parent, String elementName, Namespace ns)
	{
		String value = getStringContent(parent, elementName, ns);
		if (value != null)
		{
			Integer color = Integer.valueOf(value, 16);
			if (color != null)
				return new Color(color);
		}
		return null;
	}

	/**
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return String
	 */
	public static String getStringContent(Element parent, String elementName, Namespace ns)
	{
		String value = parent.getChildTextTrim(elementName, ns);
		return value;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Boolean instance
	 */
	public static Boolean getBooleanContent(Element parent, String elementName, Namespace ns)
	{
		String value = getStringContent(parent, elementName, ns);
		if (value != null)
		{
			return Boolean.valueOf(value);
		}
		return Boolean.FALSE;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Font
	 */
	public static Font getFontContent(Element parent, String elementName, Namespace ns)
	{
		String value = getStringContent(parent, elementName, ns);
		if (value != null)
		{
			return Font.decode(value);
		}
		return null;
	}
	
	public static String convertElementToString(Element root) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String returnString = null;
		try {
			new XMLOutputter(Format.getRawFormat()).output(root, new OutputStreamWriter(out, "UTF-8"));
			returnString = new String(out.toByteArray(), "UTF-8");
		} catch (IOException e) {
			logger.error(e, e);
		}
		return returnString;
	}
	
	public static String convertToString(OMElement root) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String returnString = null;
		try {
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(new OutputStreamWriter(out, "UTF-8"));
			root.serialize(writer);
			writer.flush();
			returnString = new String(out.toByteArray(), "UTF-8");
		}
		catch (Exception e) {
			logger.error(e,e);
		}
		return returnString;
	}
	
	public static String getDateString(Date date, boolean gmtFormat)
	{
		if(date != null){
			return gmtFormat ? gmtFormats.get().format(date) : formats.get().format(date); 
		}
		return null;
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, int value, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, long value, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, Date value, OMNamespace ns) {
		if (value != null) {
			XmlUtils.addContent(fac, parent, name, formats.get().format(value), ns);
		}
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, Date value, boolean gmtFormat, OMNamespace ns) {
		if (value != null && gmtFormat) {
			XmlUtils.addContent(fac, parent, name, gmtFormats.get().format(value), ns);
		}
		else
		{
			addContent(fac, parent, name, value, ns);
		}
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, boolean value, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, name, Boolean.toString(value), ns);
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, Color color, OMNamespace ns) {
		if (color != null) {
			int clr = color.getRGB() & 0xFFFFFF; // (value.getRed() * 255 + value.getGreen()) * 255 + value.getBlue();
			addContent(fac, parent, name, Integer.toHexString(clr), ns);
		}
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, Font value, OMNamespace ns) {
		if (value != null)
			XmlUtils.addContent(fac, parent, name, encodeFont(value), ns);
	}

	public static OMElement addContent(OMFactory fac, OMElement parent, String name, OMNamespace ns) {
		OMElement el = fac.createOMElement(name, ns);
		parent.addChild(el);
		return el;
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, String value, OMNamespace ns) {
		OMElement el = fac.createOMElement(name, ns);
		OMText text = fac.createOMText(value);
		el.addChild(text);
		parent.addChild(el);
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, double value, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	
	public static void addContent(OMFactory fac, OMElement parent,
			String name, UUID guid, OMNamespace ns) {
		if (guid != null) {
			XmlUtils.addContent(fac, parent, name, guid.toString(), ns);
		}
	}
	
	public static void addAttribute(OMFactory fac, OMElement element, String name, String value, OMNamespace ns){
		OMAttribute attribute = fac.createOMAttribute(name, ns, value);
		element.addAttribute(attribute);
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, OMElement value, OMNamespace ns) {
		OMElement el = fac.createOMElement(name, ns);
		el.addChild(value);
		parent.addChild(el);
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Integer
	 */
	public static Integer getIntContent(OMElement parent)
	{
		String value = XmlUtils.getStringContent(parent);
		return getIntContent(value);
	}
	
	public static Integer getIntContent(String value) {
		if (value != null)
		{
			try {
				Float d = Float.parseFloat(value);
				if (d != null) {
					long l = Math.round(d);
					return Integer.valueOf((int)l);
				}
			}
			catch (Exception e) {
				// ignore
			}
		}
		return null;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Long
	 */
	public static Long getLongContent(OMElement parent)
	{
		return getLongContent(XmlUtils.getStringContent(parent));
	}
	
	private static Long getLongContent(String value) {
		if (value != null)
		{
			Double d = Double.valueOf(value);
			if (d != null) {
				long l = Math.round(d);
				return Long.valueOf(l);
			}
		}
		return null;
	}
	
	public static Double getDoubleContent(OMElement parent) {
		return getDoubleContent(XmlUtils.getStringContent(parent));
	}
	
	public static Double getDoubleContent(String value) {
		if (value != null)
		{
			Double d = Double.valueOf(value);
			return d;
		}
		return null;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Color
	 */
	public static Color getColorContent(OMElement parent)
	{
		return getColorContent(XmlUtils.getStringContent(parent));
	}
	
	private static Color getColorContent(String value) {
		if (value != null)
		{
			Integer color = Integer.valueOf(value, 16);
			if (color != null)
				return new Color(color);
		}
		return null;
	}
	/**
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return String
	 */
	public static String getStringContent(OMElement parent)
	{
		return getStringContent(parent.getText());
	}
	private static String getStringContent(String ret) {
		if (ret != null) {
			ret = ret.trim();
			if (ret.isEmpty()) {
				ret = null;
			}
		}
		return ret;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Boolean instance
	 */
	public static Boolean getBooleanContent(OMElement parent)
	{
		return getBooleanContent(getStringContent(parent));
	}
	
	private static Boolean getBooleanContent(String value) {
		if (value != null)
		{
			return Boolean.valueOf(value);
		}
		return Boolean.FALSE;
	}
	/**
	 * 
	 * @param parent
	 * @param elementName
	 * @param ns
	 * @return Font
	 */
	public static Font getFontContent(OMElement parent)
	{
		return getFontContent(getStringContent(parent));
	}
	
	public static Font getFontContent(String value) {
		if (value != null)
		{
			return Font.decode(value);
		}
		return null;
	}
	
	public static String encode(String s) {
		if (s == null)
			return null;
		
		s = s.replaceAll("&", "&amp;").
			replaceAll(">", "&gt;").
			replaceAll("<", "&lt;").
			replaceAll("%", "&#37;").
			replaceAll("\"", "&quot;").
			replaceAll("'", "&apos;").
			replaceAll("\u221E", "&#x221E;");
		return s.replaceAll("\r\n", "\n").replaceAll("\r", "\n").replaceAll("\n", XML_EOL);
	}
	
	public static String decode(String s) {
		if (s == null)
			return null;
		
		s = s.replaceAll(XML_EOL, "\r\n");
		return s.replaceAll("&apos;", "'").
			replaceAll("&#x221E;", "\u221E").
			replaceAll("&quot;", "\"").
			replaceAll("&#37;", "%").
			replaceAll("&lt;", "<").
			replaceAll("&gt;", ">").
			replaceAll("&amp;", "&");
	}
	/**
	 * Pull the date string from the element and parse it to create a Date instance.
	 * @param parent is the XML element to be accessed.
	 * @return the Date representation of the input element's textual date.
	 */
	public static Date getDateContent(OMElement parent) {
		return getDateContent(XmlUtils.getStringContent(parent));
	}
	
	public static Date getDateContent(String value) {
		return getDateContent(value, false);
	}
	
	public static Date getDateContent(String value, boolean gmtFormat) {
		Date date = null;
		if (value != null) {
			try {
				if(gmtFormat)
				{
					date = gmtFormats.get().parse(value);	
				}
				else
				{
					date = formats.get().parse(value);
				}
			} catch (ParseException e) {
				try {
					// old way
					if(gmtFormat)
					{
						SimpleDateFormat sdf = new SimpleDateFormat();
						sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
						sdf.parse(value);
					}
					else 
					{
						date = new SimpleDateFormat().parse(value);
					}
				}
				catch (ParseException pe) {
					logger.error("XmlUtils received an unparseable date: " + value);
				}
			}
		}
		return date;
	}
	
	public static UUID getUUIDContent(OMElement el) {
		return getUUIDContent(XmlUtils.getStringContent(el));
	}
	
	public static UUID getUUIDContent(XMLStreamReader el) throws XMLStreamException {
		return getUUIDContent(XmlUtils.getStringContent(el));
	}
	
	private static UUID getUUIDContent(String stringContent) {
		try {
			return UUID.fromString(stringContent);
		}
		catch (Exception e) {
		}
		return null;
	}
	public static OMElement convertToOMElement(final String xml) {
		XMLStreamReader parser;
		try {
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(xml));
			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			return doc;
		} catch (XMLStreamException e) {
			logger.error(XML_STREAM_ERROR, e); 
		} catch (FactoryConfigurationError e) {
			logger.error(XML_STREAM_ERROR, e); 
		}
		return null;
	}
	
	public static OMElement addRootElement(final OMElement element, final String rootName) {
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		OMElement rootElement = omFactory.createOMElement(rootName, null);
		rootElement.addChild(element);
		return rootElement;
	}
	
	public static Element findOrCreateChild(Element parent, String name) {
		Element child = parent.getChild(name);
		if (child == null) {
			child = new Element(name);
			parent.addContent(child);
		}
		return child;
	}
	
	public static void addColorAttribute(Element parent, String attributeName, Color clr) {
		String color = "RGB(" + clr.getRed() + "," + clr.getGreen() + "," + clr.getBlue() + ")";
		parent.setAttribute(attributeName, color);
	}
	public static String getStringContent(XMLStreamReader parser) throws XMLStreamException {
		return getStringContent(parser.getElementText());
	}
	public static Integer getIntContent(XMLStreamReader parser) throws XMLStreamException {
		return getIntContent(getStringContent(parser));
	}
	public static Boolean getBooleanContent(XMLStreamReader parser) throws XMLStreamException {
		return getBooleanContent(getStringContent(parser));
	}
	public static Color getColorContent(XMLStreamReader parser) throws XMLStreamException {
		return getColorContent(getStringContent(parser));
	}
	public static Font getFontContent(XMLStreamReader el) throws XMLStreamException {
		return getFontContent(getStringContent(el));
	}
	public static Long getLongContent(XMLStreamReader el) throws XMLStreamException {
		return getLongContent(getStringContent(el));
	}
	
	public static Double getDoubleContent(XMLStreamReader el) throws XMLStreamException {
		return getDoubleContent(getStringContent(el));
	}
	
	public static Date getDateContent(XMLStreamReader el) throws XMLStreamException {
		return getDateContent(getStringContent(el));
	}
	
	public static Date getDateContent(XMLStreamReader el, boolean gmtFormat) throws XMLStreamException {
		return getDateContent(getStringContent(el), gmtFormat);
	}
}
