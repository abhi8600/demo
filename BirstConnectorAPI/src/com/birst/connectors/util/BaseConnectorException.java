package com.birst.connectors.util;

/**
 * Base class for all connector exceptions. Contains constants for error codes used by connectors.
 * @author mpandit
 *
 */
public class BaseConnectorException extends Exception
{
	private static final long serialVersionUID = 1L;
	public static final int SUCCESS = 0;
	public static final int ERROR_OTHER = -2;
	public static final int ERROR_CONNECTOR_LOGIN_FAILURE = -134;
	public static final int ERROR_CONNECTOR_INVALID_QUERY = -135;

	protected BaseConnectorException()
	{
	}

	protected BaseConnectorException(String s)
	{
		super(s);
	}

	protected BaseConnectorException(String s, Throwable cause)
	{
		super(s, cause);
	}

	/**
	 * Gets generic error code unless the specific error code is set by subclass
	 * @return error code
	 */
	public int getErrorCode()
	{
		return ERROR_OTHER;
	}
}
