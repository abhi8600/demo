package com.birst.connectors.util;

import java.io.File;

import org.apache.log4j.Logger;

import com.birst.connectors.ConnectorConfig;
import com.birst.connectors.IConnector;

public class Util 
{
	private static final Logger logger = Logger.getLogger(Util.class);
	
	public static final String KILL_EXTRACT_FILE = "SFDC_kill.lock";
	
	public static boolean isTerminated(String killFileName)
	{
		boolean terminate = false;
		if ((killFileName != null) && (killFileName.trim().length() > 0))
		{
			try
			{
				terminate = new File(killFileName).exists();
			}
			catch (Exception ex) {
				logger.warn("Exception while checking for terminate request: " + killFileName);
			}
		}
		return terminate;
	}
	
	public static String getKillFileName(String spaceDirectory, String name)
	{
		return spaceDirectory + File.separator + name;
	}
	
	public static void deleteFile(String killFileName)
	{		
		try
		{	
			File file = new File(killFileName);
			if(file.exists())
			{
				logger.debug("Clearing out the lock file" + killFileName);
				file.delete();
			}	
		}
		catch(Exception ex)
		{
			logger.warn("Error while clearing out the kill lock file " +  killFileName);
		}
	}
	
	public static ConnectorConfig getConnectorSettings(IConnector connector, String spaceDirectory) throws Exception{
		return ConnectorConfig.getConnectorConfig(spaceDirectory, getConnectorName(connector));			
	}
	
	public static String getConnectorName(IConnector connector) throws Exception
	{
		return connector.getMetaData().getConnectorName().toLowerCase();
	}
}
