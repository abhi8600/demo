 package com.birst.connectors;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.log4j.Logger;

import com.birst.connectors.util.Util;

/**
 * This class provides mechanism to update the extraction progress for an {@link ExtractionObject}. 
 * 
 * @author mpandit
 *
 */
public class ExtractionProgressHandler
{
	private static final Logger logger = Logger.getLogger(ExtractionProgressHandler.class);
	private int thresholdRecordCount;
	private String statusFileName;
	private String killFileName;
	private ExtractionStatus loggedStatus;

	/**
	 * Creates instance of ExtractionProgressHandler and sets the properties in it. Upon instantiation, it deletes the status file if it already exists.
	 * @param thresholdRecordCount signifies no of records to extract for which the status can be updated 
	 * @param statusFileName the name of the file in which the status is recorded
	 * @param killFileName the name of the file which is used for killing extraction
	 * @throws UnrecoverableException
	 * 		Should be thrown for any exceptions encountered when performing this operation
	 */
	public ExtractionProgressHandler(int thresholdRecordCount, String statusFileName, String killFileName)
		throws UnrecoverableException
	{
		this.thresholdRecordCount = thresholdRecordCount;
		this.statusFileName = statusFileName;
		this.killFileName = killFileName;
		deleteFileForcibly();
	}

	/**
	 * Gets no of records to extract for which the status can be updated 
	 * @return no of records to extract for which the status can be updated
	 */
	public int getThresholdRecordCount()
	{
		return this.thresholdRecordCount;
	}

	/**
	 * Updates failed status for {@link ExtractionObject}
	 * @throws UnrecoverableException
	 */
	public void updateFailedProgress() throws UnrecoverableException
	{
		updateProgress(ExtractionStatus.Failed, -1);
	}

	/**
	 * Logs status for {@link ExtractionObject} to status file
	 * @param status can be either of 'Running', 'Complete', 'Failed' or 'Killed'
	 * @param recordsExtracted no od records extracted so far
	 * @throws UnrecoverableException
	 * 		Should be thrown for any exceptions encountered when performing this operation
	 */
	public void updateProgress(ExtractionStatus status, int recordsExtracted) throws UnrecoverableException
	{
		String line = null;
		switch (status)
		{
			case Running:
				line = "Running\t" + recordsExtracted;
				break;
			case Complete:
				line = "Complete\t" + recordsExtracted;
				break;
			case Failed:
				line = "Failed\t" + recordsExtracted;
				break;
			case Killed:
				line = "Killed\t" + recordsExtracted;
				break;
		}

		boolean isWritten = false;
		int noRetries = 0;
		int maxRetries = 10;
		do
		{
			try
			{
				writeStatus(line);
				isWritten = true;
			}
			catch (UnrecoverableException ex)
			{
				try
				{
					Thread.sleep(2000L);
				}
				catch (InterruptedException localInterruptedException) {
				}
			}
		}
		while ((!isWritten) && (noRetries++ < maxRetries));
		if (!isWritten)
		{
			logger.warn("Unable to write status file - " + this.statusFileName);
		}
		this.loggedStatus = status;
	}

	/**
	 * Gets currently logged status for {@link ExtractionObject}
	 * @return current value of {@link ExtractionStatus}
	 */
	public ExtractionStatus getLoggedStatus()
	{
		return this.loggedStatus;
	}

	/**
	 * Attempts to write the status line to status file
	 * @param line
	 * @throws UnrecoverableException
	 * 		Should be thrown for any exceptions encountered when performing this operation
	 */
	private void writeStatus(String line) throws UnrecoverableException
	{
		FileWriter writer = null;
		try
		{
			File file = new File(this.statusFileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			writer = new FileWriter(file);
			writer.write(line);
			writer.flush();
		}
		catch (IOException ex)
		{
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		finally
		{
			if (writer != null)
			{
				try
				{
					writer.close();
				}
				catch (IOException ex)
				{
					logger.warn("Unable to close status file - " + this.statusFileName, ex);
				}
			}
		}
	}

	/**
	 * Deletes the status file forcibly using sleeps and retries
	 * @throws UnrecoverableException
	 * 		Should be thrown for any exceptions encountered when performing this operation
	 */
	private void deleteFileForcibly() throws UnrecoverableException
	{
		File file = new File(this.statusFileName);
		if (file.exists())
		{
			int noRetries = 0;
			int maxRetries = 10;
			boolean isDeleted = false;
			do
			{
				try
				{
					file.delete();
					isDeleted = true;
				}
				catch (Exception ex)
				{
					try
					{	
						Thread.sleep(2000L);
					}
					catch (InterruptedException localInterruptedException) {
					}
				}
			}
			while ((!isDeleted) && (noRetries++ < maxRetries));
			if (!isDeleted)
			{
				logger.warn("Unable to delete status file - " + this.statusFileName);
			}
		}
	}

	/**
	 * When called by connector, checks existence of kill file to determine whether the connector should continue to extract more data or not. 
	 * @return true if the kill file is not present and false otherwise. The connector must abort extraction immediately and throw {@link UnrecoverableException} 
	 * when this method returns false.
	 */
	public boolean shouldContinue()
	{
		String killLockFileName = null;
		try
		{
			return !Util.isTerminated(this.killFileName);
		}
		catch (Exception ex)
		{
			logger.warn("Error in looking for kill lock file " + killLockFileName);
		}
		return true;
	}

	/**
	 * Enum that defines the possible status values for an {@link ExtractionObject}
	 * @author mpandit
	 *
	 */
	public static enum ExtractionStatus
	{
		Running, Complete, Failed, Killed;
	}
}