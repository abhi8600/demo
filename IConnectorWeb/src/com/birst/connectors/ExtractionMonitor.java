package com.birst.connectors;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.birst.connectors.ControllerServices.Operation;
import com.birst.connectors.ControllerServices.Status;
import com.birst.connectors.util.FileCleanupUtil;
import com.birst.connectors.util.FileFilterDateIntervalUtil;
import com.birst.connectors.util.LogUtil;
import com.birst.connectors.util.Util;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class ExtractionMonitor extends Thread {
	IConnector connector;
	Properties connectionProperties;
	Map<String, List<ExtractionObject>> extractionObjectsForExtractionGroups; 
	int numThreads;
	String spaceID;
	String spaceDirectory;
	String uid;
	int version;
	String userID;
	String client;
	
	private String extractGroupStatusFileNamePrefix = null;
	private static final Logger logger = Logger.getLogger(ExtractionMonitor.class);
	
	public ExtractionMonitor(IConnector connector, Properties connectionProperties, String client, Map<String, List<ExtractionObject>> extractionObjectsForExtractionGroups, 
			int numThreads, String spaceID, String spaceDirectory, String uid, int version, String userID)
	{
		super("ExtractionMonitor");
		this.connector = connector;
		this.connectionProperties = connectionProperties;
		this.extractionObjectsForExtractionGroups = extractionObjectsForExtractionGroups;
		this.numThreads = numThreads;
		this.spaceID = spaceID;
		this.spaceDirectory = spaceDirectory;
		this.uid = uid;
		this.version = version;
		this.userID = userID;
		this.client = client;
	}
	
	@Override
	public void run() {
		ConnectorConfig config = null;
		boolean success = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date startTime = new Date();
		String activityID = "";
		try {
			//log activity
			Map<String, String> argumentsMap = ControllerServices.getConnectorActivityLogParams(connector, activityID, uid, Operation.extract.toString(), Status.inProgress.toString(), sdf.format(startTime), null);
			activityID = ControllerServices.logConnectorActivity(client, String.valueOf(version), uid, spaceID, spaceDirectory, argumentsMap);
			//do extraction
			config = ConnectorConfig.getConnectorConfig(spaceDirectory, Util.getConnectorName(connector));
			config.setStartExtractionTime(startTime);
			clearStatusFiles(); //clear previous status files
			success = fetchDataUsingMultipleThreads(connector, connectionProperties, extractionObjectsForExtractionGroups, numThreads, spaceDirectory, uid);
		}catch(Exception ex){
			logger.error("Error while starting extraction", ex);
		}
		finally
		{	
			String fileNamePart = success ?  "success-extract-" : "failed-extract-";
	    	writeOutputFile(spaceDirectory, uid, fileNamePart);
			cleanupTemp();
			
			if (config != null)
			{
				Date endTime = new Date();
				config.setEndExtractionTime(endTime);
				try
				{
					IConnectorServlet.updateConfigVersion(connector, config, spaceID, spaceDirectory, String.valueOf(version), userID);
					Set<ExtractionObject> uniqueExtractObjects = new HashSet<ExtractionObject>();
					for (List<ExtractionObject> lst : extractionObjectsForExtractionGroups.values())
	    			{
	    				uniqueExtractObjects.addAll(lst);
	    			}
					
					for (int i = 0; i < config.getExtractionObjects().size(); i++)
					{
						ExtractionObject obj = config.getExtractionObjects().get(i);
						for (ExtractionObject eobj : uniqueExtractObjects)
						{
							if (eobj.name.equalsIgnoreCase(obj.name))
							{
								config.getExtractionObjects().set(i, eobj);
							}
						}
					}
					config.saveConnectorConfig(spaceDirectory,  Util.getConnectorName(connector));
					//log activity
					if (activityID != null && !activityID.isEmpty())
					{
						try
						{
						    Integer.parseInt(activityID);
							Map<String, String> argumentsMap = ControllerServices.getConnectorActivityLogParams(connector, activityID, uid, Operation.extract.toString(), success? Status.completed.toString() : Status.failed.toString(), sdf.format(startTime), sdf.format(endTime));
							ControllerServices.logConnectorActivity(client, String.valueOf(version), uid, spaceID, spaceDirectory, argumentsMap); 
						}
						catch(ParseException ex)
						{
							//do not log activity
						}
					}
				}
				catch(Exception ex)
				{
					logger.error("Problem saving settigs - " + ex.getMessage(), ex);
				}
				
				writeExtractLogToFile(connector, spaceDirectory, startTime, uid);
			}
		}
	}
	
	private void cleanupTemp()
	{
		try
		{
			FileCleanupUtil util = new FileCleanupUtil(connector, spaceDirectory);
			util.cleanupTemp();
		}
		catch (UnrecoverableException ure)
		{
			logger.error("Problem encountered when performing temp files clean up for space " + spaceID, ure);
		}		
	}
	
	private void clearStatusFiles()
	{
		try
		{
			FileCleanupUtil util = new FileCleanupUtil(connector, spaceDirectory);
			util.clearStatusFiles();
		}
		catch (Exception ex)
		{
			logger.error("Problem encountered when performing status files clean up for space " + spaceID, ex);
		}	
	}
	
	public boolean fetchDataUsingMultipleThreads(IConnector connector, Properties connectionProperties, Map<String, List<ExtractionObject>> extractionObjectsForExtractionGroups, 
			int noParallelThreads, String spaceDirectory, String uid) 
			throws RecoverableException, UnrecoverableException
	{
		long startTime = System.currentTimeMillis();
		boolean allExtractGroupObjectsExtracted = true;
		Map<ExtractionObject, String> extractObjectsToExtractGroupMap = new HashMap<ExtractionObject, String>();
		String connDir = spaceDirectory + File.separator + "connectors";
		File spaceConnDir = new File(connDir);
		if (!spaceConnDir.exists() || !spaceConnDir.isDirectory())
		{
			spaceConnDir.mkdir();
		}
		extractGroupStatusFileNamePrefix = connector.getMetaData().getConnectorName() + "-ExtractGroup-";
		FilenameFilter fnf = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.startsWith(extractGroupStatusFileNamePrefix))
					return true;
				return false;
			}
		};
		List<String> extractGroups = new ArrayList<String>(); 
		extractGroups.addAll(extractionObjectsForExtractionGroups.keySet());
		Collections.sort(extractGroups);
		List<ExtractionResult> extractionResults = Collections.synchronizedList(new ArrayList<ExtractionResult>());
		for (String extractGroup : extractGroups)
		{
			long startTimeEG = System.currentTimeMillis();
			for (File f : spaceConnDir.listFiles(fnf))
			{
				f.delete();
			}
			String extractGroupName = extractGroup == null ? "All" : extractGroup;
			String extractGroupStatusFileName = spaceConnDir + File.separator + extractGroupStatusFileNamePrefix + extractGroupName + ".txt";
			File egFile = new File(extractGroupStatusFileName);
			if (!egFile.exists())
			{
				try
				{
					egFile.createNewFile();
				}
				catch(IOException ioe)
				{
					logger.error("Could not create status file:" + extractGroupStatusFileName);
				}
			}
			logger.info("Extracting Data for Extract Group: " + extractGroupName);
			List<ExtractionObject> types = new ArrayList<ExtractionObject>();
			List<ExtractionObject> extractGroupObjectsList = extractionObjectsForExtractionGroups.get(extractGroup);
			if (extractGroupObjectsList != null && extractGroupObjectsList.size() > 0)
			{
				for (ExtractionObject eobj : extractGroupObjectsList)
				{
					if (!extractObjectsToExtractGroupMap.containsKey(eobj))
					{
						types.add(eobj);
						extractObjectsToExtractGroupMap.put(eobj, extractGroupName);
					}
					else
					{
						logger.info("Extraction Object " + eobj.name + " is already extracted as part of Extract Group:" + extractObjectsToExtractGroupMap.get(eobj) + ", this object will not be extracted again");
					}
				}
				if (types.size() == 0)
				{
					logger.info("All objects that belong to Extract Group " + extractGroupName + " are already extracted as part of other Extract Group(s)");
					continue;
				}
			}
			else
			{
				logger.info("No objects found that belong to Extract Group " + extractGroupName);
				continue;
			}
			BlockingQueue<ExtractionObject> queue = new LinkedBlockingQueue<ExtractionObject>(types.size());
			queue.addAll(types);
			int noThreads = 1;
		
			if (connector.getMetaData().supportsParallelExtraction(extractGroupObjectsList))
			{
				noThreads = noParallelThreads;
			}
			ConnectorDataExtractor[] extractionThreads = new ConnectorDataExtractor[noThreads];
			for (int i = 0; i < noThreads; i++)
			{
				IConnector threadSpecificConnector = null;
				try
				{
					threadSpecificConnector = (IConnector) connector.getClass().newInstance();
					threadSpecificConnector.connect(connectionProperties);
				}
				catch (InstantiationException | IllegalAccessException ex)
				{
					logger.error(ex.getMessage(), ex);
					throw new UnrecoverableException(ex.getMessage(), ex);
				}
				extractionThreads[i] = new ConnectorDataExtractor(threadSpecificConnector, i, queue, spaceDirectory, uid, extractionResults);			
				extractionThreads[i].start();
			}
			for (int i = 0; i < noThreads; i++)
			{
				try
				{
					extractionThreads[i].join();
				}
				catch (InterruptedException ie)
				{
					logger.error(ie.getMessage(), ie);				
				}			
			}
			boolean allObjectsExtracted = true;
			for (ExtractionObject type : types)
			{
				if (type.getVariableMap() != null) {
					Map<String, String> variableMap = type.getVariableMap();
					for (String key : variableMap.keySet())
					{
						if (key.equals("query"))
						{
							type.query = variableMap.get("query");							
						}
						else if (key.equals("selectionCriteria"))
						{
							type.selectionCriteria = variableMap.get("selectionCriteria");
						}
						else if (key.equals("startDate"))
						{
							type.startDate = variableMap.get("startDate");
						}
						else if (key.equals("endDate"))
						{
							type.endDate = variableMap.get("endDate");
						}
						else if (key.equals("profileID"))
						{
							type.profileID = variableMap.get("profileID");
						}
						else if (key.equals("reportSuiteID"))
						{
							type.reportSuiteID = variableMap.get("reportSuiteID");
						}
					}
				}
			}
			for (ExtractionObject type : types)
			{
				if (type.status != ExtractionObject.SUCCESS)
				{
					allObjectsExtracted = false;
					break;
				}
			}
			if (!allObjectsExtracted)
			{
				logger.warn("Extraction failed for one or more objects for Extract Group: " + extractGroupName);
			}
			else
			{
				logger.info("Extraction successful for all objects for Extract Group: " + extractGroupName);
			}
			long timeTakenInSecondsEG = (System.currentTimeMillis() - startTimeEG);
			logger.info("Total Time taken for extracting all objects in Extract Group '" + extractGroupName + "' : " + timeTakenInSecondsEG + "(milliseconds)");
			allExtractGroupObjectsExtracted = allObjectsExtracted;
			if (!allObjectsExtracted)
			{
				break;
			}
		}
		
		int apiUsage = 0;
		for (ExtractionResult result : extractionResults)
		{
			apiUsage += result.getApiUsage();
		}
		if (apiUsage > 0)
		{
			logger.info("Total API Usage for extracting all objects is : " + apiUsage + " units");	
		}
		for (File f : spaceConnDir.listFiles(fnf))
		{
			f.delete();
		}
		if (!allExtractGroupObjectsExtracted)
		{
			logger.warn("Extraction failed for one or more Extract Groups");			
		}
		else
		{
			logger.info("Extraction successful for all objects for all Extract Groups");
		}
		long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
		logger.info("Total Time taken for extracting all Extract Groups : " + timeTakenInSeconds + "(milliseconds)");
		return allExtractGroupObjectsExtracted;
	}
	
	private synchronized void writeOutputFile(String spaceDirectory, String uid, String fileNamePart)
	{
		String outputFileName = null;
		try
		{
			if(uid != null)
			{	
				outputFileName = spaceDirectory + File.separator + "slogs" + File.separator + fileNamePart + uid + ".txt";
				File file = new File(outputFileName);
				if(!file.exists())
				{	
					file.createNewFile();
				}
			}
		}
		catch(Exception ex)
		{
			logger.warn("Error while creating out the success file file " +  outputFileName);
		}
	}
	
	private synchronized void writeExtractLogToFile(IConnector connector, String spaceDirectoryPath, Date startTime, String uid)
	{
		BufferedReader reader = null;
		BufferedWriter writer = null;
		try
		{
			if(uid != null)
			{	
				//get the log files from connector deployment
				File connectorLogDir = LogUtil.getConnectorLogDirectory();
				if (connectorLogDir != null)
				{
					FileFilterDateIntervalUtil filter = new FileFilterDateIntervalUtil(startTime);
					File sourceFiles[] = connectorLogDir.listFiles(filter);
					
					if (sourceFiles != null && sourceFiles.length > 0)
					{
						//create output file & writer
						File spaceLogDir = LogUtil.getConnectorLogDirForSpace(spaceDirectoryPath);
						String outLogFileName = connector.getMetaData().getConnectorName().toLowerCase(); //use lowercase for file name
						File outfile = new File (spaceLogDir + File.separator + outLogFileName + ".log");
						writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile, false), "UTF-8"));
						
						Pattern uidPattern = Pattern.compile("(\\,)("+uid+")"); //i.e. ,<uid>
						
						for(File sFile: sourceFiles)
						{
							reader = new BufferedReader(new FileReader(sFile));
							String line = null;
							int lineNum  = 0;
							boolean parseErrorStackTrace = false;
							boolean ignoreLine = false;
							boolean isStackTraceLine = false;
							while((line = reader.readLine()) != null)
							{
								if (lineNum > 0 && ((lineNum % LogUtil.LOG_WRITER_THRESHOLD) == 0))
		                        {
		                            writer.flush();
		                        }
								
								//try to get error messages (do not include entire error stack trace)
								//when reader gets any error line then parse stack trace to get messages(i.e. not part of any log levels)
								if (parseErrorStackTrace)
								{
									isStackTraceLine = LogUtil.isStackrace(line);
								}
								if (isStackTraceLine)
								{
									ignoreLine = true;
									if (LogUtil.isErrorMessage(line))
									{
										ignoreLine = false;
										line = line.replace(ConnectorDataExtractor.errorMessagePrefix, "");
									}
								}
								else
								{
									//UID must match If not part of error stack trace
									Matcher uidMatcher = uidPattern.matcher(line);
									if (!uidMatcher.find())
									{
										continue;
									}
									ignoreLine = LogUtil.isRejectLog(line);
									
									//current line might be stack trace (not part of any log level) so check for error lines only if not stack trace
									parseErrorStackTrace =  LogUtil.isErrorLog(line);
								}
								
								//write line if matches valid logs pattern or error stack trace message
								if (!ignoreLine)
								{
									writer.write(line);
									lineNum++;
									writer.write(System.getProperty("line.separator"));									
								}
							}
							writer.flush();	
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Error while generating last extraction logs for UID : " + uid, ex);
		}
		finally
		{
			try
			{
				if (reader != null)
				{
					reader.close();
				}
				if (writer != null)
				{
					writer.close();
				}
			}
			catch (IOException ex)
			{
				logger.error(ex.getMessage(), ex);
			}
		}
	}
}
