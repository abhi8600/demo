package com.birst.connectors;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.birst.connectors.parameter.Parameter;
import com.birst.connectors.parameter.ParameterUtil;
import com.birst.connectors.util.BaseConnectorException;
import com.birst.connectors.util.EncryptionService;
import com.birst.connectors.util.Util;
import com.birst.connectors.util.XmlUtils;


/**
 * Primary Servlet called by Birst Connector Framework to perform all connector operations. Any connector developed for Birst must have a class that implements
 * {@link IConnector} interface. The connector then must provide the fully qualified classname of the class that implements IConnector interface as init parameter
 * to IConnectorServlet defined in web.xml. IConnectorServlet will instantiate the connector class and call methods defined in IConnector interface to perform 
 * operations required by Birst from connector.
 * 
 * @author mpandit
 *
 */
public class IConnectorServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	/**
	 * use this to allow Connector Framework to support backward compatibility with older callers (ie. SMIWeb/Scheduler/TestUtility)
	 */
	public static final int BIRST_CONNECTOR_VERSION = 1;
	private static Class connectorClass;
	private static String connectorName;
	private static String connectorClient;
	private static String connectorCloudVersion;
	private static final int maxRetries = 5;
	private static final EncryptionService svc = EncryptionService.getInstance();
	private static final String NODE_CATALOG_ROOT = "Catalog";
	private static final String NODE_CATALOG_ITEM = "CatalogItem";
	private static final String NODE_EXTRACTION_OBJECT = "ExtractionObject";
	private static final String NODE_EXTRACT_GROUP = "ExtractGroup";
	private static final String NODE_OBJECT_NAME = "ObjectName";
	private static final String NODE_OBJ_NAME = "Name";
	private static final String NODE_IS_INCREMENTAL = "IsIncremental";
	private static final String NODE_OBJECT_ROOT = "Object";
	private static final String NODE_OBJECT_ID = "ObjectId";
	private static final String NODE_CONN_PROPERTY_ROOT = "ConnectionProperties";
	private static final String NODE_OBJECT_QUERY = "Query";
	private static final String NODE_OBJECT_TYPE = "Type";
	private static final String NODE_OBJECT_RECORD_TYPE = "RecordType";
	private static final String NODE_OBJECT_URL = "URL";
	private static final String NODE_OBJECT_MAPPING = "Mapping";
	private static final String NODE_OBJECT_COLUMNS = "Columns";
	private static final String NODE_OBJECT_TECH_NAME = "ObjectName";
	public static final String NODE_OBJECT_PROFILE_ID = "ProfileID";
	public static final String NODE_OBJECT_DIMENSIONS = "Dimensions";
	public static final String NODE_OBJECT_METRICS = "Metrics";
	public static final String NODE_OBJECT_SEGMENT_ID = "SegmentID";
	public static final String NODE_OBJECT_FILTERS = "Filters";
	public static final String NODE_OBJECT_START_DATE = "StartDate";
	public static final String NODE_OBJECT_END_DATE = "EndDate";
	public static final String NODE_OBJECT_SAMPLING_LEVEL = "SamplingLevel";
	public static final String NODE_OBJECT_REPORT_SUITE_ID = "ReportSuiteID";
	public static final String NODE_OBJECT_ELEMENTS = "Elements";
	public static final String NODE_OBJECT_PAGESIZE = "PageSize";
	private static final String NODE_OBJECT_SELECTION_CRITERIA = "SelectionCriteria";
	private static final String NODE_OBJECT_ISVALIDQUERY = "IsValidQuery";
	private static final String NODE_OBJECT_ISVALIDOBJECT = "IsValidObject";
	private static final String NODE_VARIABLE_OBJECT = "Object";
	private static final String NODE_VARIABLE_QUERY = "Query";
	private static final String NODE_OBJECTS_ROOT = "Objects";
	private static final String NODE_OBJECT_DYNAMICPARAM = "DynamicParam";
	public static final String NODE_INCLUDE_ALL_RECORDS="IncludeAllRecords";
    private static final String METHOD = "V";
    private static final String LEFT_PAR = "{";
    private static final String RIGHT_PAR = "}";
    private static final String KEY_VALUE_DELIM = "=";
    private static final String KEY_VALUE_PAIR_DELIM = "\\|";
	private static final String COMMA = ",";
	private static final String SINGLE_QUOTE = "'";
	private static final String SLASH = "/";
	private static final String DASH = "-";
    private static final String VALID_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String VALID_DATE_FORMAT = "yyyy-MM-dd";
    private static final String SPACE = " ";
    private static final String DATE_TIME_FILLER = "T";
    private static final int NAME_INDEX = 0;
    private static final int VALUE_INDEX = 1;
	
	private static final org.apache.log4j.Logger logger = Logger.getLogger(IConnectorServlet.class);

	private static final String CONNECTOR_CONTROLLER_URI = "ConnectorControllerURI";
	private static String connectorControllerURI = null;
	private static Map<String, String> parametersMap = null;
	private static Map<String, String> birstParametersMap = null;
	private static boolean requiresDynamicParamForObject=false;

	@Override
	public void init(ServletConfig config)
			throws ServletException
	{
		InputStream in = null;
		Properties buildProperties = new Properties();
		try
		{
			in = getClass().getClassLoader().getResourceAsStream("build.number");
			if (in != null)
			{
				buildProperties.load(in);
				logger.debug("Using properties file: build.number (found via the classpath)");
			}
		} catch (IOException e)
		{
			logger.debug("classpath properties file (build.number) not found.");
		} finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				} catch (Exception e)
				{
				}
			}
		}
		String connectorClassName = config.getInitParameter("ConnectorClassName");
	    if ((connectorClassName == null) || (connectorClassName.trim().isEmpty()))
	    	throw new ServletException("Init Parameter ConnectorClassName not defined");
	    try
	    {
	    	Class connectorClass = Class.forName(connectorClassName);
	    	Object connectorInstance = connectorClass.newInstance();
	    	if (!(connectorInstance instanceof IConnector))
	    	{
	    		throw new ServletException("Connector Class " + connectorClassName + " does not implement com.birst.connectors.IConnector interface");
	    	}
	    	if (!(connectorInstance instanceof BaseConnector))
	    	{
	    		throw new ServletException("Connector Class " + connectorClassName + " does not extend com.birst.connectors.BaseConnector class");
	    	}
	    	IConnector connector = (IConnector)connectorInstance;
	    	IConnectorServlet.connectorClass = connectorClass;
	    	connectorName = connector.getMetaData().getConnectorName();
	    	MDC.put("connectorName", connectorName);
	    	connectorCloudVersion = connector.getMetaData().getConnectorCloudVersion();
	    	MDC.put("connectorCloudVersion", connectorCloudVersion);
	    	
	    	connectorClient = connectorName + " " + connector.getMetaData().getConnectorVersion();
	    	String connectorHome = System.getProperty("connector.home");
			if (connectorHome == null)
			{
				logger.error("connector.home property not found");
				throw new ServletException("connector.home property not found");
			}
	    	File connectorsProperties = new File(connectorHome + File.separator + "conf" + File.separator + "connectors.properties");
			Properties configProperties = new Properties();
			if (connectorsProperties.exists())
			{
				logger.debug("Overriding connectors configuration properties from " + connectorsProperties);
				try
				{
					configProperties.load(new FileReader(connectorsProperties));
				}catch (IOException ioe)
				{
					logger.error("problem loading connectors.properties - " + ioe.getMessage(), ioe);
				}
				connectorControllerURI = getParameter(CONNECTOR_CONTROLLER_URI, configProperties);
				if (connectorControllerURI == null)
				{
					logger.error("ConnectorControllerURI not found, connector may not work as expected");
				}				
			}
	    	logger.info("IConnectorServlet initialized successfully using connector class " + connectorClassName);
	    	// Report results of search
			if (buildProperties.isEmpty())
			{
				logger.debug("Properties file (build.number) not found or empty after full search was performed.");
			} else
			{
				if (logger.isDebugEnabled())
					printProperties(buildProperties);
			}
	    }
	    catch (Exception ex)
	    {
	    	ex.printStackTrace();
	    	logger.error(ex.getMessage(), ex);
	    	throw new ServletException(ex.getMessage(), ex);
	    }
	    finally {
	    	clearOutMDC();
	    }
	}
	
	/**
	 * dump the properties to the log file
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}

	private String getParameter(String parameterName, Properties configProperties)
	{
		if (parameterName == null)
			return null;
		if (configProperties.containsKey(parameterName))
			return configProperties.getProperty(parameterName);
		return null;
	}
	
	public static String getConnectorControllerURI()
	{
		return connectorControllerURI;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
	    doPost(request, response);
	}

	@Override
  	protected void doPost(HttpServletRequest request, HttpServletResponse response)
  			throws ServletException, IOException
    {
		response.setContentType("text/xml;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
		String output = null;
	    String errorMessage = null;
	    int errorCode = BaseConnectorException.ERROR_OTHER;
	    try 
	    {
	    	clearOutMDC();
	    	MDC.put("connectorName", connectorName);
	    	MDC.put("connectorCloudVersion", connectorCloudVersion);
	    	if (parametersMap == null)
			{
				//read Connector Parameters and cache them
				parametersMap = ControllerServices.fillConnectorParameters(connectorClient, ControllerServices.getCurrentAPIVersionConnectionString((IConnector)connectorClass.newInstance()));
				if (parametersMap != null && parametersMap.containsKey(BaseConnector.BIRST_PARAMETERS) && parametersMap.get(BaseConnector.BIRST_PARAMETERS) != null)
				{
					birstParametersMap = new HashMap<String, String>();
					String[] birstParams = parametersMap.get(BaseConnector.BIRST_PARAMETERS).split(",");
					if (birstParams != null)
					{
						for (String param : birstParams)
						{
							String[] birstParam = param.split("=");
							if (birstParam != null && birstParam.length == 2)
							{
								birstParametersMap.put(birstParam[0], birstParam[1]);
							}
						}
					}
				}
				logger.info("Connector Parameters initialized successfully for " + connectorName + " " + connectorCloudVersion + " connector");
			}
	  		HttpSession session = request.getSession();
	  		String sessionID = session.getId();
			if (sessionID != null)
			{
				MDC.put("sessionID", sessionID);
			}
			String userID = request.getParameter("UserID");
	    	if (userID != null)
			{
				MDC.put("userID", userID);
			}
	    	String spaceID = request.getParameter("SpaceID");
	    	if (spaceID != null)
			{
				MDC.put("spaceID", spaceID);
			}
	    	String client = request.getParameter("Client");
	    	if (client != null)
			{
				MDC.put("client", client);
			}
	    	String birstConnectorVersion = request.getParameter("BirstConnectorVersion");
	    	int version = BIRST_CONNECTOR_VERSION;
	    	if (birstConnectorVersion != null && !birstConnectorVersion.trim().isEmpty())
	    	{
	    		try
	    		{
	    			version = Integer.parseInt(birstConnectorVersion.trim());
	    		}
	    		catch (NumberFormatException nfe)
	    		{
	    			logger.error("Could not parse BirstConnectorVersion:" + birstConnectorVersion.trim() + ", default version " + BIRST_CONNECTOR_VERSION + " will be used.");
	    		}
	    	}
	    	else if (version > BIRST_CONNECTOR_VERSION)
	    	{
	    		logger.warn("Caller's BirstConnectorVersion:" + version + " is higher than connector's BirstConnectorVersion:" + BIRST_CONNECTOR_VERSION);
	    	}
	    	MDC.put("birstConnectorVersion", version);
	    	String method = request.getParameter("Method");
	    	String spaceDirectory = request.getParameter("SpaceDirectory");	    	
	    	if (method != null)
		    {
		    	switch (method) 
			    {
			    	case"getMetaData":
				    {
				    	Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        ConnectorMetaData cmd = connector.getMetaData();
				        output = ConnectorMetaData.toXML(cmd);
				        break;
				    }
			    	case "connect"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        boolean isConnected = connect(connector, credentialsXML, spaceID, spaceDirectory);
				        if(!isConnected)
						{
							throw new Exception("Cannot connect to connector - " + connector.getMetaData().getConnectorName() + " - for space" + spaceID);
						}
						output = "true";
						break;
			    	}
			    	case "saveConnectorCredentials"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        boolean isConnected = connect(connector, credentialsXML, spaceID, spaceDirectory);
				        if(!isConnected)
						{
							throw new Exception("Cannot connect to connector - " + connector.getMetaData().getConnectorName() + " - for space" + spaceID);
						}
				        saveCredentials(connector, credentialsXML, spaceID, spaceDirectory, String.valueOf(version), userID);
				        output = "true";
			    		break;
			    	}
			    	case "testConnection"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        boolean isConnected = connect(connector, credentialsXML, spaceID, spaceDirectory);
				        if(!isConnected)
						{
							throw new Exception("Cannot connect to connector - " + connector.getMetaData().getConnectorName() + " - for space" + spaceID);
						}
				        output = "true";
			    		break;
			    	}
			    	case "validateConnectionPropertiesAndGetCatalog"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        String[] catalog = null;
				        if (validateConnectionPropertiesForConnector(credentialsXML, connector, spaceID, spaceDirectory))
				        {
				        	catalog = connector.getCatalog();
				        }			    		 
			    		if (catalog != null)
			    		{
			    			OMFactory factory = OMAbstractFactory.getOMFactory();
		    				OMNamespace ns = null;
		    				OMElement catalogRoot = factory.createOMElement(NODE_CATALOG_ROOT, ns);
		    				OMElement itemElement;
			    			for (String item : catalog)
			    			{
			    				itemElement = factory.createOMElement(NODE_CATALOG_ITEM, ns);
			    				itemElement.setText(item);
			    				catalogRoot.addChild(itemElement);
			    			}
			    			output = XmlUtils.convertToString(catalogRoot);
			    		}			    			
			    		break;
			    	}
			    	case "startExtract"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		String isIncrementalXml = request.getParameter("isIncrementalXml");
			    		String uid = request.getParameter("UID");
			    		MDC.put("uniqueID", uid);
			    		String numThreadsString = request.getParameter("NumThreads");
			    		String variables = request.getParameter("Variables");
			    		String extractGroupsXML = request.getParameter("ExtractGroupsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        if (validateConnectionPropertiesForConnector(credentialsXML, connector, spaceID, spaceDirectory))
				        {
				        	List<String> extractGroups = null;
				        	if (extractGroupsXML != null)
				        	{
				        		SAXBuilder builder = new SAXBuilder();
				    			Document d = builder.build(new StringReader(extractGroupsXML));
				    			Element root = d.getRootElement();
				    			List<Element> elements = root.getChildren(NODE_EXTRACT_GROUP);
				    			if (elements != null)
				    			{
				    				extractGroups = new ArrayList<String>();
				    				for (Element el : elements)
				    				{
				    					String extractGroup = el.getText();
				    					if (!extractGroups.contains(extractGroup))
				    						extractGroups.add(extractGroup);
				    				}
				    				Collections.sort(extractGroups);
				    			}
				        	}
				        	ConnectorConfig settings = ConnectorConfig.getConnectorConfig(spaceDirectory, Util.getConnectorName(connector));
				    		if(settings != null){
				    			Util.deleteFile(Util.getKillFileName(spaceDirectory, Util.KILL_EXTRACT_FILE));
				    			Map<String, List<ExtractionObject>> extractionObjectsForExtractionGroups = null;
				    			if (extractGroups != null && extractGroups.size() > 0)
				    			{
				    				extractionObjectsForExtractionGroups = settings.getExtractionObjectsForExtractionGroups(extractGroups);
				    				if (extractionObjectsForExtractionGroups.size() == 0)
				    				{
				    					String extGroupsStr = "";
				    					boolean first = true;
				    					for (String eg : extractGroups)
				    					{
				    						if (first)
				    							first = false;
				    						else
				    							extGroupsStr += ",";
				    						extGroupsStr += eg;
				    					}				    						
				    					throw new Exception("No objects found that belong to Extract Group(s): '" + extGroupsStr + "'");
				    				}
				    				else
				    				{
				    					for (String eg : extractGroups)
				    					{
				    						if (!extractionObjectsForExtractionGroups.containsKey(eg))
				    						{
				    							logger.warn("Invalid Extract Group:" + eg + ". Please check your schedule.");
				    						}
				    					}
				    				}
				    			}
				    			else
				    			{
				    				extractionObjectsForExtractionGroups = new HashMap<String, List<ExtractionObject>>();
				    				extractionObjectsForExtractionGroups.put(null, settings.getExtractionObjects());
				    			}
				    			Set<ExtractionObject> uniqueExtractObjects = new HashSet<ExtractionObject>();
				    			for (List<ExtractionObject> lst : extractionObjectsForExtractionGroups.values())
				    			{
				    				uniqueExtractObjects.addAll(lst);
				    			}
				    			if(uniqueExtractObjects != null && uniqueExtractObjects.size() > 0){
									Properties connectionProperties = extractConnectionProperties(credentialsXML, connector, spaceID, spaceDirectory);
									Map<String, String> isExtractionObjectTransactional = parseIsIncrementalXml(isIncrementalXml);
									for (ExtractionObject eobj : uniqueExtractObjects) 
							    	{
										Map<String, String> variableMap = new HashMap<String, String>();
										if (eobj.type == ExtractionObject.Types.QUERY) {
											if(variables!=null && eobj.query != null){
												variableMap.put("query", eobj.query);
												eobj.query = replaceQueryVariables(eobj.query, variables);												
											}
										}
										else if (eobj.type == ExtractionObject.Types.SAVED_OBJECT) {
											if(variables!=null && (eobj.selectionCriteria != null && !eobj.selectionCriteria.isEmpty())){
												variableMap.put("selectionCriteria", eobj.selectionCriteria);
												eobj.selectionCriteria = replaceQueryVariables(eobj.selectionCriteria, variables);												
											}
											if(variables!=null && (eobj.query != null && !eobj.query.isEmpty())){
												variableMap.put("query", eobj.query);
												eobj.query = replaceQueryVariables(eobj.query, variables);												
											}
										}
										else if (eobj.type == ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY) {
											if(variables!=null && eobj.startDate != null){
												variableMap.put("startDate", eobj.startDate);
												eobj.startDate = replaceQueryVariables(eobj.startDate, variables);												
											}
											if(variables!=null && eobj.startDate != null){
												variableMap.put("endDate", eobj.endDate);
												eobj.endDate = replaceQueryVariables(eobj.endDate, variables);												
											}
											if(variables!=null && eobj.profileID !=null){
												variableMap.put("profileID", eobj.profileID);
												eobj.profileID = replaceQueryVariables(eobj.profileID, variables);	
											}
										}
										else if (eobj.type == ExtractionObject.Types.SAVED_OMNITURE_SITECATALYST_OBJECT) {
											if(variables!=null && eobj.startDate != null){
												variableMap.put("startDate", eobj.startDate);
												eobj.startDate = replaceQueryVariables(eobj.startDate, variables);												
											}
											if(variables!=null && eobj.startDate != null){
												variableMap.put("endDate", eobj.endDate);
												eobj.endDate = replaceQueryVariables(eobj.endDate, variables);												
											}
											if(variables!=null && eobj.reportSuiteID !=null){
												variableMap.put("reportSuiteID", eobj.reportSuiteID);
												eobj.reportSuiteID = replaceQueryVariables(eobj.reportSuiteID, variables);	
											}
										}
										eobj.setVariableMap(variableMap);
							    		eobj.useLastModified = false;
							    		if (isExtractionObjectTransactional.containsKey(eobj.name))
							    		{
							    			eobj.useLastModified = Boolean.parseBoolean(isExtractionObjectTransactional.get(eobj.name));
							    		}
							    	}
									int numThreads = 4;
									try{
										numThreads = Integer.parseInt(numThreadsString);
									}
									catch(Exception ex)
									{
										logger.warn("Error parsing the num of parallel threads property. Using default value - " + numThreads);										
									}
									ExtractionMonitor et = new ExtractionMonitor(connector, connectionProperties, client, extractionObjectsForExtractionGroups, numThreads, spaceID, spaceDirectory, uid, version, userID);
									et.start();																		
								}
								else
								{
									throw new Exception("No objects selected for extraction - " + Util.getConnectorName(connector) + " for space - " + spaceID);
								}
							}
				    		else
				    		{
				    			throw new Exception("No config file found for connector - " + Util.getConnectorName(connector) + " in space - " + spaceID);
				    		}
				        }
				        else
				        {
				        	throw new Exception("Cannot connect to connector - " + Util.getConnectorName(connector) + " for space - " + spaceID);
				        }
				        output = "true";
				        break;
			    	}
			    	case "getObjectQuery"	:
			    	{
			    		String xmlData = request.getParameter("xmlData");
			    		SAXBuilder builder = new SAXBuilder();
			    		Document d = builder.build(new StringReader(xmlData));
			    		Element rootEl = d.getRootElement();
			    		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        Properties allProps = getConnectorCredentials(connector, spaceDirectory);
						allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
						allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
				        Properties inputProps = parseConnectorProperties((BaseConnector)connector, connectorPropertiesRoot);
						
						if(inputProps != null && inputProps.size() > 0){
							allProps.putAll(inputProps);
						}
						
				        Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
						ExtractionObject object = paraseQueryObjectDetails(objectRoot);
						
						boolean isFilled = false;
				        int count = 0;
						boolean retry = false;
						do{
							try{
								connector.connect(allProps);
								connector.fillObjectDetails(object);
								retry = false;
								isFilled = true;
							}
							catch(RecoverableException ex){
								count++;
								retry = count < maxRetries;
								if(retry){
									Thread.sleep(500);
									logger.warn("Retrying getObjectQuery : " +count + " ", ex);
								}
								else{
									logger.warn("Exhausted the retries for getObjectQuery : " +count + " ", ex);
								}
							}
						}while(retry);
						if (!isFilled)
						{
							throw new Exception("Problem encountered getting query object - " + Util.getConnectorName(connector) + " for space - " + spaceID);
						}
						OMFactory factory = OMAbstractFactory.getOMFactory();
						OMNamespace ns = null;			
						OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
						XmlUtils.addContent(factory, root, NODE_OBJ_NAME, object.name, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, object.columnMappings, ns);
						output = XmlUtils.convertToString(root);
			    		break;
			    	}
			    	case "getObject" :
			    	{
			    		// Get parameters.
			    		String xmlData = request.getParameter("xmlData");
			    		String xmlData1 = request.getParameter("xmlData");
			    		String variables = request.getParameter("Variables");
			    		if(variables!=null)
			    		{
			    			xmlData = replaceVariables(xmlData, variables);
			    		}
			    		SAXBuilder builder = new SAXBuilder();
			    		Document d = builder.build(new StringReader(xmlData));
			    		Document d1 = builder.build(new StringReader(xmlData1));
			    		
			    		// Parse xml data and get properties.
			    		Element rootEl = d.getRootElement();
			    		Element rootEl1 = d1.getRootElement();
			    		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
			    		Object obj = connectorClass.newInstance();
			    		IConnector connector = (IConnector)obj;
			    		Properties allProps = getConnectorCredentials(connector, spaceDirectory);
			    		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
			    		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
			    		Properties inputProps = parseConnectorProperties((BaseConnector)connector, connectorPropertiesRoot);
			    		if(inputProps != null && inputProps.size() > 0) {
			    			allProps.putAll(inputProps);
			    		}
			    		
			    		// Set up and execute validation test.
			    		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
			    		Element objectRoot1 = rootEl1.getChild(NODE_OBJECT_ROOT);
			    		ExtractionObject object = parseObjectDetails(objectRoot);
			    		ExtractionObject object1 = parseObjectDetails(objectRoot1);	
			    		
			    		boolean isFilled = false;
			    		int count = 0;
			    		boolean retry = false;
			    		String result = null;
			    		do {
			    			try {
			    				connector.connect(allProps);
			    				connector.fillObjectDetails(object);
			    				List<Parameter> dymanicParamList = connector.fillDynamicParametersForObject(object);
			    				result = ParameterUtil.toXML(dymanicParamList);
			    				retry = false;
			    				isFilled = true;
			    			}
			    			catch(RecoverableException ex) {
			    				count++;
			    				retry = count < maxRetries;
			    				if(retry) {
			    					Thread.sleep(500);
			    					logger.warn("Retrying getQuery : " + count + " ", ex);
			    				}
			    				else {
			    					logger.warn("Exhausted the retries for getQuery : " + count + " ", ex);
			    				}
			    			}
			    		} while(retry);

			    		// Check if object is valid.
			    		if (!isFilled)
			    		{
			    			throw new Exception("Problem encountered while getting " + Util.getConnectorName(connector) + " object '" + object.name + "'");
			    		}

			    		// Build response xml.
			    		OMFactory factory = OMAbstractFactory.getOMFactory();
			    		OMNamespace ns = null;			
			    		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);						
			    		XmlUtils.addContent(factory, root, NODE_OBJ_NAME, object.name, ns);
			    		XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
			    		XmlUtils.addContent(factory, root, NODE_OBJECT_TECH_NAME, object.techName, ns);
			    		XmlUtils.addContent(factory, root, NODE_OBJECT_SELECTION_CRITERIA, object.selectionCriteria, ns);
			    		if (object.columnNames != null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_COLUMNS, StringUtils.join(object.columnNames, COMMA), ns);
			    		}
			    		XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, object.columnMappings, ns);
			    		if (object1.reportSuiteID !=null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_REPORT_SUITE_ID, object1.reportSuiteID, ns);
			    		}
			    		if (object.metrics !=null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_METRICS, object.metrics, ns);
			    		}
			    		if (object.elements !=null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_ELEMENTS, object.elements, ns);
			    		}
			    		if (object1.startDate !=null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_START_DATE, object1.startDate, ns);
			    		}
			    		if (object1.endDate != null) {
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_END_DATE, object1.endDate, ns);
			    		}
			    		XmlUtils.addContent(factory, root, NODE_OBJECT_ISVALIDOBJECT, String.valueOf(isFilled), ns);
			    		if(result != null)
			    		{
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_DYNAMICPARAM, result, ns);
			    		}
			    		if (object.query != null){
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
			    		}
			    		output = XmlUtils.convertToString(root);
			    		output = StringEscapeUtils.unescapeXml(output);
			    		break;
			    	}
			    	case "validateObjectQuery"	:
			    	{
			    		String xmlData = request.getParameter("xmlData");
			    		String variables = request.getParameter("Variables");
			    		if(variables!=null){
			    			xmlData = replaceVariables(xmlData, variables);
			    		}
			    		SAXBuilder builder = new SAXBuilder();
			    		Document d = builder.build(new StringReader(xmlData));
			    		Element rootEl = d.getRootElement();
			    		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        Properties allProps = getConnectorCredentials(connector, spaceDirectory);
						allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
						allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
				        Properties inputProps = parseConnectorProperties((BaseConnector)connector, connectorPropertiesRoot);
						
						if(inputProps != null && inputProps.size() > 0){
							allProps.putAll(inputProps);
						}
						
				        Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
						ExtractionObject object = paraseQueryObjectDetails(objectRoot);
						
						boolean isValid = false;
				        int count = 0;
						boolean retry = false;
						do{
							try{
								connector.connect(allProps);
								connector.validateQueryForObject(object);
								retry = false;
								isValid = true;
							}
							catch(RecoverableException ex){
								count++;
								retry = count < maxRetries;
								if(retry){
									Thread.sleep(500);
									logger.warn("Retrying getObjectQuery : " +count + " ", ex);
								}
								else{
									logger.warn("Exhausted the retries for getObjectQuery : " +count + " ", ex);
								}
							}
						}while(retry);
						if (!isValid)
						{
							throw new Exception("Problem encountered validating query object - " + Util.getConnectorName(connector) + " for space - " + spaceID);
						}
						OMFactory factory = OMAbstractFactory.getOMFactory();
						OMNamespace ns = null;			
						OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
						XmlUtils.addContent(factory, root, NODE_OBJ_NAME, object.name, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_ISVALIDQUERY, String.valueOf(isValid), ns);
						output = XmlUtils.convertToString(root);
			    		break;
			    	}
			    	case "validateObject"	:
			    	{
			    		// Get parameters.
			    		String xmlData = request.getParameter("xmlData");
			    		String variables = request.getParameter("Variables");
			    		if(variables!=null)
			    		{
			    			xmlData = replaceVariables(xmlData, variables);
			    		}
			    		SAXBuilder builder = new SAXBuilder();
			    		Document d = builder.build(new StringReader(xmlData));
			    		
			    		// Parse xml data and get properties.
			    		Element rootEl = d.getRootElement();
			    		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        Properties allProps = getConnectorCredentials(connector, spaceDirectory);
						allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
						allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
				        Properties inputProps = parseConnectorProperties((BaseConnector)connector, connectorPropertiesRoot);
						if(inputProps != null && inputProps.size() > 0) {
							allProps.putAll(inputProps);
						}
						
						// Set up and execute validation test.
				        Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
						ExtractionObject object = parseObjectDetails(objectRoot);
						
						boolean isValid = false;
				        int count = 0;
						boolean retry = false;
						do {
							try{
								connector.connect(allProps);
								connector.validateObject(object);
								retry = false;
								isValid = true;
							}
							catch(RecoverableException ex){
								count++;
								retry = count < maxRetries;
								if(retry){
									Thread.sleep(500);
									logger.warn("Retrying validateObject : " + count + " ", ex);
								}
								else{
									logger.warn("Exhausted the retries for validateObject : " + count + " ", ex);
								}
							}
						} while(retry);
						
						// Check if object is valid.
						if (!isValid)
						{
							throw new Exception("Problem encountered while validating " + Util.getConnectorName(connector) + " object '" + object.name + "'");
						}

						// Build response xml.
						OMFactory factory = OMAbstractFactory.getOMFactory();
						OMNamespace ns = null;			
						OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);						
						XmlUtils.addContent(factory, root, NODE_OBJ_NAME, object.name, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_TECH_NAME, object.techName, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_SELECTION_CRITERIA, object.selectionCriteria, ns);
						if (object.columnNames != null) {
							XmlUtils.addContent(factory, root, NODE_OBJECT_COLUMNS, StringUtils.join(object.columnNames, COMMA), ns);
						}
						if (object.query != null)
						{
							XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
						}
						XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, object.columnMappings, ns);
						XmlUtils.addContent(factory, root, NODE_OBJECT_ISVALIDOBJECT, String.valueOf(isValid), ns);
						output = XmlUtils.convertToString(root);
			    		break;
			    	}
			    	case "getDynamicParameters"	:
			    	{
			    		String credentialsXML = request.getParameter("CredentialsXML");
			    		Object obj = connectorClass.newInstance();
				        IConnector connector = (IConnector)obj;
				        output = getDynamicParameters(connector, credentialsXML, spaceID, spaceDirectory);
			    		break;
			    	}
			    	case "getDynamicParamsforObject" :
			    	{
			    		Object objct = connectorClass.newInstance();
			    		IConnector conn = (IConnector)objct;
			    		requiresDynamicParamForObject=conn.getMetaData().requiresDynamicParamForObjects();

			    		// Get parameters.
			    		String xmlData = request.getParameter("xmlData");
			    		String objectDef = request.getParameter("ObjectDefinition");
			    		String objectDef2 = request.getParameter("ObjectDefinition");
			    		String variables = request.getParameter("Variables");
			    		
			    		if(variables!=null && requiresDynamicParamForObject )
			    		{
			    			objectDef2 = replaceVariables(objectDef2, variables);
			    		}
			    		SAXBuilder builder = new SAXBuilder();
			    		SAXBuilder builder1 = new SAXBuilder();
			    		SAXBuilder builder2 = new SAXBuilder();
			    		Document d = builder.build(new StringReader(xmlData));
			    		Document d1 = builder1.build(new StringReader(objectDef));
			    		Document d2 = builder2.build(new StringReader(objectDef2));
			    		
			    		// Parse xml data and get properties.
			    		Element rootEl = d.getRootElement();
			    		Element objRootEl = d1.getRootElement();
			    		Element objRootEl2 = d2.getRootElement();
			    		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
			    		Object obj = connectorClass.newInstance();
			    		IConnector connector = (IConnector)obj;
			    		Properties allProps = getConnectorCredentials(connector, spaceDirectory);
			    		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
			    		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
			    		Properties inputProps = parseConnectorProperties((BaseConnector)connector, connectorPropertiesRoot);
			    		if(inputProps != null && inputProps.size() > 0) {
			    			allProps.putAll(inputProps);
			    		}
			    		
			    		// Set up and execute validation test.
			    		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
			    		Element existingObjectRoot = objRootEl;
			    		Element existingObjectRoot2 = objRootEl2;
			    		ExtractionObject object = parseObjectDetails(objectRoot);
			    		ExtractionObject existingObject = parseObjectDetails(existingObjectRoot);
			    		ExtractionObject existingObject2 = parseObjectDetails(existingObjectRoot2);
			    		boolean isFilled = false;
			    		int count = 0;
			    		boolean retry = false;
			    		String result = null;
			    		do {
			    			try {
			    				if(requiresDynamicParamForObject)
			    				{
			    					connector.connect(allProps);
			    					List<Parameter> dymanicParamList = connector.fillDynamicParametersForObject(existingObject2);
			    					result = ParameterUtil.toXML(dymanicParamList);
			    				}
			    				retry = false;
			    				isFilled = true;
			    			}
			    			catch(RecoverableException ex) {
			    				count++;
			    				retry = count < maxRetries;
			    				if(retry) {
			    					Thread.sleep(500);
			    					logger.warn("Retrying getQuery : " + count + " ", ex);
			    				}
			    				else {
			    					logger.warn("Exhausted the retries for getQuery : " + count + " ", ex);
			    				}
			    			}
			    		} while(retry);

			    		// Check if object is valid.
			    		if (!isFilled)
			    		{
			    			throw new Exception("Problem encountered while getting " + Util.getConnectorName(connector) + " object '" + object.name + "'");
			    		}

			    		// Build response xml.
			    		OMFactory factory = OMAbstractFactory.getOMFactory();
			    		OMNamespace ns = null;			
			    		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
			    		if(existingObject != null)
			    		{
			    			XmlUtils.addContent(factory, root, NODE_OBJ_NAME, existingObject.name, ns);
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, existingObject.type.toString(), ns);
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_TECH_NAME, existingObject.techName, ns);
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_SELECTION_CRITERIA, existingObject.selectionCriteria, ns);
			    			if (existingObject.columnNames != null) {
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_COLUMNS, StringUtils.join(existingObject.columnNames, COMMA), ns);
			    			} if (existingObject.isIncludeAllRecords() != null){
			    				XmlUtils.addContent(factory, root, NODE_INCLUDE_ALL_RECORDS, existingObject.isIncludeAllRecords(), ns);
			    			} if (existingObject.getObjectId() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_ID, existingObject.getObjectId(), ns);
			    			} if (existingObject.getURL() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_URL, existingObject.getURL(), ns);
			    			} if (existingObject.getRecordType() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_RECORD_TYPE, existingObject.getRecordType(), ns);
			    			} if (existingObject.getProfileID() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_PROFILE_ID, existingObject.getProfileID(), ns);
			    			} if (existingObject.getDimensions() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_DIMENSIONS, existingObject.getDimensions(), ns);
			    			} if (existingObject.getMetrics() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_METRICS, existingObject.getMetrics(), ns);
			    			} if (existingObject.getSegmentID() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_SEGMENT_ID, existingObject.getSegmentID(), ns);
			    			} if (existingObject.getFilters() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_FILTERS, existingObject.getFilters(), ns);
			    			} if (existingObject.getStartDate() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_START_DATE, existingObject.getStartDate(), ns);
			    			} if (existingObject.getEndDate() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_END_DATE, existingObject.getEndDate(), ns);
			    			} if (existingObject.getSamplingLevel() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_SAMPLING_LEVEL, existingObject.getSamplingLevel(), ns);
			    			}if (existingObject.getReportSuiteID() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_REPORT_SUITE_ID, existingObject.getReportSuiteID(), ns);
			    			}if (existingObject.getElements() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_ELEMENTS, existingObject.getElements(), ns);
			    			}if (existingObject.getPageSize() != null){
			    				XmlUtils.addContent(factory, root, NODE_OBJECT_PAGESIZE, existingObject.getPageSize(), ns);
			    			}if (existingObject.query != null){
				    			XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, existingObject.query, ns);
				    		}
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, existingObject.columnMappings, ns);
			    		}
			    		if(result != null)
			    		{
			    			OMElement ResultDynamic = XmlUtils.convertToOMElement(result);
			    			XmlUtils.addContent(factory, root, NODE_OBJECT_DYNAMICPARAM, ResultDynamic, ns);
			    		}
			    		output = XmlUtils.convertToString(root);
			    		break;
			    	}
			    	case "ping":
			    	{
			    		Object obj = connectorClass.newInstance();
			    		IConnector connector = (IConnector)obj;
			    		try {
			    			output = String.valueOf(connector.ping());
			    		} catch (Exception ex) {
			    			output = "false";
			    			errorMessage = ex.getMessage();
			    		}
			    		break;
			    	}
				    default			:
				    {
				    	throw new Exception("Valid methodname not provided for IConnectorServlet: " + method);
				    }
			    }
		    }
	    }
	    catch (Exception e) {
	    	if (e instanceof BaseConnectorException)
	    	{
	    		errorCode = ((BaseConnectorException) e).getErrorCode();
	    	}
	    	logger.error(e.getMessage(), e);
	    	errorMessage = e.getMessage();
	    }
	    finally {
	    	clearOutMDC();
	    }
	    StringBuilder sb = new StringBuilder();
	    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    if (output == null)
	    {
	    	sb.append("<Root>");
	    	sb.append("<Success>false</Success>");
	    	sb.append("<Reason>").append(errorMessage).append("</Reason>");
	    	sb.append("<ErrorCode>").append(errorCode).append("</ErrorCode>");
	    	sb.append("</Root>");
	    }
	   else
	    {
		   sb.append("<Root>");
		   sb.append("<Success>true</Success>");
		   sb.append("<Result>").append(output).append("</Result>");
		   sb.append("</Root>");
	    }
	    pw.write(sb.toString());
	    pw.flush();
    }
  	
	private String replaceVariables(String xmlData, String variables) throws IOException, JDOMException {
		SAXBuilder sxBuild = new SAXBuilder();
		Document doc = sxBuild.build(new StringReader(xmlData));
		Element xml = doc.getRootElement();
		String query = null;
		Element objectElement;
		if (xml != null) {
			if((xml.getName()).equals(NODE_VARIABLE_OBJECT))
			{
				objectElement = xml;
			}
			else
			{
				objectElement = xml.getChild(NODE_VARIABLE_OBJECT);
			}
			List<Element> objectElements = objectElement.getChildren();
			if (objectElements != null) {
				for (Element element : objectElements)
				{
					query = element.getValue();
					if (query != null)
					{
						query = replaceQueryVariables(query, variables);
						element.setText(query);
					}
				}
			}
		}
		
		return XmlUtils.convertElementToString(xml);
	}
	
	private String replaceQueryVariables(String queryStr, String variables) {
		String[] nameValuePairs = variables.split(KEY_VALUE_PAIR_DELIM);
		String[] nameValues;
		for (String pair : nameValuePairs) {
			nameValues = pair.split(KEY_VALUE_DELIM);
			queryStr = queryStr.replace(formatVariableName(nameValues[NAME_INDEX]), formatVariableValue(nameValues[VALUE_INDEX]));
		}
		return queryStr;
	}
	
	private String formatVariableName(String name) {
		StringBuilder sb = new StringBuilder();
		sb.append(METHOD).append(LEFT_PAR).append(name).append(RIGHT_PAR);
		return sb.toString();
	}
	
	private String formatVariableValue(String value) {
		StringBuilder sb = new StringBuilder();
		String[] values = value.split(COMMA);
		SimpleDateFormat dateFormat;
		Date date;
		String format;
		boolean isMultiValued = (values.length > 1);
		for (String val : values) {
			if(val != null && !val.isEmpty() && !val.trim().isEmpty())
			{
				sb.append(getFormattedValue(val, isMultiValued));
				sb.append(COMMA);
			}
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
	
	private boolean isDateFormat(String value, String format, boolean isMultiValued) {
		SimpleDateFormat dateFormat;
		Date date;
		boolean isFormat;
		try {
			value = getDateTime(value, isMultiValued);
			dateFormat = new SimpleDateFormat(format);
			dateFormat.parse(value);
			isFormat = true;
		} catch (ParseException ex) {
			isFormat = false;
		}
		return isFormat;
	}
	
	private String getDateTime(String value, boolean isMultiValued) {
		if (isQuoted(value)) {
			value = value.substring(1, value.length() - 1);
		}
		return value;
	}
	
	private boolean isQuoted(String value) {
		int length = value.length();
		return value.substring(0, 1).equals(SINGLE_QUOTE) && value.substring(length - 1, length).equals(SINGLE_QUOTE);
	}
	
	private String getFormattedValue(String value, boolean isMultiValued) {
		/*** Pre-formatting. ***/
		if (value != null) {
			value = value.trim();
		} else {
			return value;
		}
		
		/*** Boolean formatting. ***/
		if (value.equalsIgnoreCase(SINGLE_QUOTE + Boolean.TRUE.toString() + SINGLE_QUOTE)) {
			return Boolean.TRUE.toString();
		}
		if (value.equalsIgnoreCase(SINGLE_QUOTE + Boolean.FALSE.toString() + SINGLE_QUOTE)) {
			return Boolean.FALSE.toString();
		}
		
		/*** DateTime formatting. ***/
		String format = VALID_DATE_TIME_FORMAT;
		if (isDateFormat(value, format, isMultiValued)) {
			return value;
		}
		
		// Format 1 support.
		format = "yyyy-MM-dd'T'HH:mm:ss.SSS";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ss.SS";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ss.SSZ";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ss.S";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ss.SZ";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ss";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:ssZ";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:s";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm:sZ";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mm";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd'T'HH:mmZ";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		
		// Format 2 support.
		format = "yyyy-MM-dd HH:mm:ss.SSS";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);

		}
		format = "yyyy-MM-dd HH:mm:ss.SSSZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SS";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SSZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.S";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);	
		}
		format = "yyyy-MM-dd HH:mm:ssZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);	
		}
		format = "yyyy-MM-dd HH:mm:s";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:sZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mmZ";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		
		// Format 3 support.
		format = "yyyy-MM-dd HH:mm:ss.SSS aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SSSZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SS aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SSZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.S aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss.SZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ss aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:ssZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:s aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm:sZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mm aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "yyyy-MM-dd HH:mmZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			value = value.replaceFirst(SPACE, DATE_TIME_FILLER);
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = VALID_DATE_FORMAT;
		if (isDateFormat(value, format, isMultiValued)) {
			return getDateTime(value, isMultiValued);
		}
		
		// Format 4 support.
		format = "MM/dd/yyyy HH:mm:ss.SSS aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss.SSSZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss.SS aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss.SSZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss.S aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss.SZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ss aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:ssZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:s aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm:sZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mm aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy HH:mmZ aa";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_TIME_FORMAT, isMultiValued);
		}
		format = "MM/dd/yyyy";
		if (isDateFormat(value, format, isMultiValued)) {
			return convertToSoqlDateTime(value, format, VALID_DATE_FORMAT, isMultiValued);
		}
		return value;
	}
	
	private String convertToSoqlDateTime(String value, String format, String validFormat, boolean isMultiValued) {
		String ConnName=null;
		SimpleDateFormat dateFormat;
		Date date;
		String convertedDate = null;
		
		try{
			Object obj = connectorClass.newInstance();
			IConnector connector = (IConnector)obj;
			ConnName = Util.getConnectorName(connector);
		}
		catch(Exception ex)
		{
		}
		if(ConnName.equalsIgnoreCase("netsuitejdbc"))
		{
			validFormat = "yyyy-MM-dd HH:mm:ss.SSS";
			try {
				value = value.replaceFirst(DATE_TIME_FILLER, SPACE);
				value = getDateTime(value, isMultiValued);
				dateFormat = new SimpleDateFormat(format);
				date = dateFormat.parse(value);
				dateFormat = new SimpleDateFormat(validFormat);
				convertedDate = dateFormat.format(date);
			}
			catch (ParseException ex) {
				value = value.replaceFirst(DATE_TIME_FILLER, SPACE);
				convertedDate = value;
			}
		}
		else
		{
			try {
				value = getDateTime(value, isMultiValued);
				dateFormat = new SimpleDateFormat(format);
				date = dateFormat.parse(value);
				dateFormat = new SimpleDateFormat(validFormat);
				convertedDate = dateFormat.format(date);
			} catch (ParseException ex) {
				convertedDate = value;
			}
		}
		return convertedDate;
	}
  	
  	private boolean validateConnectionPropertiesForConnector(String xmlData, IConnector connector, String spaceID, String spaceDirectory) throws Exception
	{
		Properties allProps = extractConnectionProperties(xmlData, connector, spaceID, spaceDirectory);
		
		// are all the properties available
		for (ConnectionProperty property : connector.getMetaData().getConnectionProperties()){
			if (property.isRequired && !allProps.containsKey(property.name)){
				logger.error("Required property not found for connector : " + Util.getConnectorName(connector) + " : " + property.name);
				throw new Exception("Required Property Not Found : " + property.name);
			}
		}
		
		int count = 0;
		boolean isConnected = false;
		boolean retry = false;
		do{
			try{
				connector.connect(allProps);
				isConnected = true;
				retry = false;
			}
			catch(RecoverableException ex){
				count++;
				retry = count < maxRetries;
				if(retry){
					Thread.sleep(500);
					logger.warn("Retrying getCatalog : " +count + " ", ex);
				}
				else{
					logger.warn("Exhausted the retries for getCatalog : " +count + " ", ex);
				}
			}
		}while(retry);
		return isConnected;
	}
  	
  	private Map<String, String> parseIsIncrementalXml(String isIncrementalXml) throws Exception
  	{
  		Map<String, String> isExtractionObjectTransactional = new HashMap<String, String>();
  		if (isIncrementalXml != null && isIncrementalXml.length() > 0)
  		{
	  		SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(isIncrementalXml));
			Element root = d.getRootElement();
			List<Element> elements = root.getChildren(NODE_EXTRACTION_OBJECT);
			for (Element childEl : elements)
			{
				isExtractionObjectTransactional.put(childEl.getChildText(NODE_OBJECT_NAME), childEl.getChildText(NODE_IS_INCREMENTAL));
			}
  		}
		return isExtractionObjectTransactional;
  	}
  	
  	private Properties extractConnectionProperties(String xmlData, IConnector connector, String spaceID, String spaceDirectory) throws Exception
  	{
  		Properties allProps = getConnectorCredentials(connector, spaceDirectory);
		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		if(xmlData != null && xmlData.trim().length() > 0){
			// get credentials from incoming request
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element root = d.getRootElement();
			Properties incomingProps = parseConnectorProperties((BaseConnector)connector, root);
			allProps.putAll(incomingProps);
		}
		return allProps;
  	}
  	
  	private Properties getConnectorCredentials(IConnector connector, String spaceDirectory) throws Exception{
		List<ConnectionProperty> connProps = getConnectorCredentialProperties(connector, spaceDirectory);
		Properties props = new Properties();
		if(connProps != null && connProps.size() > 0){
			for(ConnectionProperty connProperty : connProps){
				if(connProperty.value != null){
					props.setProperty(connProperty.name, connProperty.value);
				}
			}
		}
		return props;
	}
  	
  	private List<ConnectionProperty> getConnectorCredentialProperties(IConnector connector, String spaceDirectory) throws Exception{
		ConnectorConfig settings  = Util.getConnectorSettings(connector, spaceDirectory);
		return settings.getConnectionProperties();
	}
  	
  	/**
	 * The underlying implementation parses the dom element to parse all the properties it needs
	 * e.g. username, password, sfdcclient id etc.
	 * @param root
	 * @return
	 * @throws UnrecoverableException 
	 */
	private Properties parseConnectorProperties(BaseConnector connector, Element el) throws UnrecoverableException{
		Properties properties = new Properties();
		ConnectorMetaData connectorMetaData = connector.getMetaData();
		List<ConnectionProperty> connectorPropertiesList = connectorMetaData.getConnectionProperties();
		if(connectorPropertiesList != null)
		{
			for(ConnectionProperty connectionProperty : connectorPropertiesList)
			{
				fillInPropsIfNotNull(connector, el, connectionProperty.name, properties);
			}
		}
		return properties;
	}
	
	private void fillInPropsIfNotNull(BaseConnector connector, Element el, String propName, Properties properties){
		String propValue = getPropertyValue(connector, el, propName);
		fillInPropsIfNotNull(propName, propValue, properties);
	}
	
	private String getPropertyValue(BaseConnector connector, Element el, String propName){
		if(el != null && propName != null && propName.trim().length() > 0){
			//always use Connector Parameters from Connector DB
			if (birstParametersMap != null && birstParametersMap.containsKey(propName))
			{
				connector.setBirstParameter(propName, birstParametersMap.get(propName));
				return birstParametersMap.get(propName);				
			}
			else if (parametersMap != null && parametersMap.containsKey(propName))
			{
				return parametersMap.get(propName);				
			}
			else
			{
				return el.getChildText(propName);
			}
		}
		return null;
	}
	
	private void fillInPropsIfNotNull(String propName, String propValue, Properties properties){
		if(propValue != null && propValue.trim().length() > 0){
			properties.setProperty(propName, propValue);
		}
	}
	
	private void fillInPropsIfNotNull(BaseConnector connector, Element el, ConnectionProperty connProperty){
		if(connProperty != null){
			String propValue = getPropertyValue(connector, el, connProperty.name);
			if(propValue != null && propValue.trim().length() > 0){
				connProperty.value = propValue;
			}
		}
	}
	
	private boolean connect(IConnector connector, String credentialsXML, String spaceID, String spaceDirectory) throws Exception
	{
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(credentialsXML));
		Element root = d.getRootElement();
		Properties properties = parseConnectorProperties((BaseConnector)connector, root);
		properties.setProperty(BaseConnector.SPACE_ID, spaceID);
		properties.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		int count = 0;
		boolean retry = false;
		boolean isConnected = false;
		do{
			try{
				connector.connect(properties);
				retry = false;
				isConnected = true;
			}
			catch(RecoverableException ex){
				count++;
				retry = count < maxRetries;
				if(retry){
					Thread.sleep(500);
					logger.warn("Retrying connect : " +count + " ", ex);
				}
				else{
					logger.error("Exhausted the retries for connect : " +count + " ", ex);
				}
			}
		}while(retry);
		return isConnected;
	}
	
	private String getDynamicParameters(IConnector connector, String credentialsXML, String spaceID, String spaceDirectory) throws Exception
	{
		String result = null;
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(credentialsXML));
		Element root = d.getRootElement();
		Properties properties = parseConnectorProperties((BaseConnector)connector, root);
		properties.setProperty(BaseConnector.SPACE_ID, spaceID);
		properties.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		List<Parameter> dymanicParamList = connector.getDynamicParameterList(properties);
		result = ParameterUtil.toXML(dymanicParamList);
		return result;
	}
	
	private void saveCredentials(IConnector connector, String xmlData, String spaceID, String spaceDirectory, String version, String userName) throws Exception
	{
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element root = d.getRootElement();
		Properties properties = parseConnectorProperties((BaseConnector)connector, root);
		properties.setProperty(BaseConnector.SPACE_ID, spaceID);
		properties.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		ConnectorMetaData connectorMetaData = connector.getMetaData();
		List<ConnectionProperty> toSaveConnectionProperties = new ArrayList<ConnectionProperty>();
		for(ConnectionProperty connProperty : connectorMetaData.getConnectionProperties()){
			ConnectionProperty toSaveConnProperty = new ConnectionProperty();
			toSaveConnProperty.name = connProperty.name;
			toSaveConnProperty.isRequired = connProperty.isRequired;
			toSaveConnProperty.isEncrypted = connProperty.isEncrypted;
			toSaveConnProperty.isSecret = connProperty.isSecret;
			toSaveConnProperty.saveToConfig = connProperty.saveToConfig;
			String propValue = getPropertyValue((BaseConnector)connector, root, connProperty.name);
			if(propValue != null && propValue.trim().length() > 0){
				toSaveConnProperty.value = propValue;
			}
			toSaveConnectionProperties.add(toSaveConnProperty);
		}
		ConnectorConfig config = Util.getConnectorSettings(connector, spaceDirectory);
		config.setConnectionProperties(toSaveConnectionProperties);
		updateConfigVersion(connector, config, spaceID, spaceDirectory, version, userName);
		config.saveConnectorConfig(spaceDirectory, Util.getConnectorName(connector));
	}
	
	public static void updateConfigVersion(IConnector connector, ConnectorConfig config, String spaceID, String spaceDirectory, String version, String userName) throws Exception
	{
		String currentAPIVersion = connector.getMetaData().getConnectorCloudVersion().replace('_', '.');
		String currentAPIVersionConnectionString = ControllerServices.getCurrentAPIVersionConnectionString(connector);
		String defaultAPIVersion = ControllerServices.getDefaultAPIVersion(connectorClient, version, userName, spaceID, spaceDirectory, currentAPIVersionConnectionString);
		boolean isDefaultVersion = false;
		if (defaultAPIVersion != null)
		{
			String[] versionParts = defaultAPIVersion.substring("connector:birst://".length()).split(":");
			isDefaultVersion =  (versionParts != null && versionParts.length == 2 && versionParts[1].length() > 0 && versionParts[1].equals(currentAPIVersion));
			if (isDefaultVersion || defaultAPIVersion.equals(currentAPIVersionConnectionString))
			{
				config.setConnectorAPIVersion(null);//wipe out API version from config if it is default version
			}
			else
			{
				config.setConnectorAPIVersion(currentAPIVersionConnectionString);//write current API version to config
			}
		}
	}
	
	private ExtractionObject paraseQueryObjectDetails(Element root) throws JDOMException, IOException
	{
		String objectName = root.getChildText(NODE_OBJ_NAME);
		String objectType = root.getChildText(NODE_OBJECT_TYPE);
		String objectQuery = root.getChildText(NODE_OBJECT_QUERY);
		String objectMapping = root.getChildText(NODE_OBJECT_MAPPING);
		ExtractionObject extractionObject = new ExtractionObject();
		extractionObject.name = objectName;
		extractionObject.type = getExtractionType(objectType);
		extractionObject.query = objectQuery;
		extractionObject.columnMappings = objectMapping;
		return extractionObject;
	}
	
	private ExtractionObject parseObjectDetails(Element root) throws JDOMException, IOException {
		ExtractionObject extractionObject = new ExtractionObject();
		extractionObject.name = root.getChildText(NODE_OBJ_NAME);
		extractionObject.type = getExtractionType(root.getChildText(NODE_OBJECT_TYPE));
		extractionObject.techName = root.getChildText(NODE_OBJECT_TECH_NAME);
		if (root.getChildText(NODE_OBJECT_COLUMNS) != null) {
			extractionObject.columnNames = root.getChildText(NODE_OBJECT_COLUMNS).split(COMMA);
		}
		if (root.getChildText(NODE_OBJECT_ID) != null) {
			extractionObject.objectId = root.getChildText(NODE_OBJECT_ID);
		}
		if (root.getChildText(NODE_OBJECT_RECORD_TYPE) != null) {
			extractionObject.recordType = root.getChildText(NODE_OBJECT_RECORD_TYPE);
		}
		if (root.getChildText(NODE_OBJECT_URL) != null) {
			extractionObject.url = root.getChildText(NODE_OBJECT_URL);
		}
		if (root.getChildText(NODE_OBJECT_PROFILE_ID) != null) {
			extractionObject.profileID = root.getChildText(NODE_OBJECT_PROFILE_ID);
		}
		if (root.getChildText(NODE_OBJECT_DIMENSIONS) != null) {
			extractionObject.dimensions = root.getChildText(NODE_OBJECT_DIMENSIONS);
		}
		if (root.getChildText(NODE_OBJECT_METRICS) != null) {
			extractionObject.metrics = root.getChildText(NODE_OBJECT_METRICS);
		}
		if (root.getChildText(NODE_OBJECT_SEGMENT_ID) != null) {
			extractionObject.segmentID = root.getChildText(NODE_OBJECT_SEGMENT_ID);
		}
		if (root.getChildText(NODE_OBJECT_FILTERS) != null) {
			extractionObject.filters = root.getChildText(NODE_OBJECT_FILTERS);
		}
		if (root.getChildText(NODE_OBJECT_START_DATE) != null) {
			extractionObject.startDate = root.getChildText(NODE_OBJECT_START_DATE);
		}
		if (root.getChildText(NODE_OBJECT_END_DATE) != null) {
			extractionObject.endDate = root.getChildText(NODE_OBJECT_END_DATE);
		}
		if (root.getChildText(NODE_OBJECT_SAMPLING_LEVEL) != null) {
			extractionObject.samplingLevel = root.getChildText(NODE_OBJECT_SAMPLING_LEVEL);
		}
		if (root.getChildText(NODE_OBJECT_REPORT_SUITE_ID) != null) {
			extractionObject.reportSuiteID = root.getChildText(NODE_OBJECT_REPORT_SUITE_ID);
		}
		if (root.getChildText(NODE_OBJECT_ELEMENTS) != null) {
			extractionObject.elements = root.getChildText(NODE_OBJECT_ELEMENTS);
		}
		if (root.getChildText(NODE_OBJECT_PAGESIZE) != null) {
			extractionObject.pageSize = root.getChildText(NODE_OBJECT_PAGESIZE);
		}
		if (root.getChildText(NODE_OBJECT_QUERY) != null) {
			extractionObject.query = root.getChildText(NODE_OBJECT_QUERY);
		}
		extractionObject.columnMappings = root.getChildText(NODE_OBJECT_MAPPING);
		extractionObject.selectionCriteria = root.getChildText(NODE_OBJECT_SELECTION_CRITERIA);
		return extractionObject;
	}
	
	private static ExtractionObject.Types getExtractionType(String objectType)
	{
		if(objectType == null)
			return null;
		ExtractionObject.Types extractionType = ExtractionObject.Types.OBJECT;
		if(ExtractionObject.Types.QUERY.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.QUERY;
		}
		
		if(ExtractionObject.Types.OBJECT.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.OBJECT;
		}
		
		if(ExtractionObject.Types.SAVED_OBJECT.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_OBJECT; 
		}
		
		if(ExtractionObject.Types.SAVED_SEARCH_OBJECT.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_SEARCH_OBJECT; 
		}
		
		if(ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY; 
		}
		
		return extractionType;
	}
	
	private static void clearOutMDC()
	{
		MDC.remove("userID");
		MDC.remove("spaceID");
		MDC.remove("client");
		MDC.remove("birstConnectorVersion");
		MDC.remove("connectorName");
		MDC.remove("connectorCloudVersion");		
		MDC.remove("sessionID");		
	}
	
}