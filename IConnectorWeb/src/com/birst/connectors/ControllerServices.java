package com.birst.connectors;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;

import com.birst.connectors.util.BaseConnectorException;
import com.birst.connectors.util.XmlUtils;

public class ControllerServices {
	
	private static final Logger logger = Logger.getLogger(ControllerServices.class);
	private static final String CONNECTORCONTROLLER_APPLICATION = "ConnectorController";
	private static final String GET_DEFAULT_VERSION_SERVLET = "GetDefaultVersion";
	private static final String GET_CONNECTOR_PARAMETERS_SERVLET = "GetConnectorParameters";
	public static final String NODE_CONNECTOR_PARAM_NAME_NODE = "ParameterName";
	public static final String NODE_CONNECTOR_PARAM_VALUE_NODE = "ParameterValue";
	private static final String LOG_CONNECTOR_ACTIVITY_SERVLET = "LogConnectorActivity";
	
	public enum Status 
	{
	    none ("None"),
	    inProgress ("InProgress"),
	    completed ("Completed"),
	    failed ("Failed");

	    private final String name;       

	    private Status(String s) 
	    {
	        name = s;
	    }

	    public boolean equalsName(String otherName)
	    {
	        return (otherName == null)? false:name.equals(otherName);
	    }

	    public String toString()
	    {
	       return name;
	    }
	}
	
	public enum Operation 
	{
	    none ("None"),
	    extract ("Extract");

	    private final String name;       

	    private Operation(String s)
	    {
	        name = s;
	    }

	    public boolean equalsName(String otherName)
	    {
	        return (otherName == null)? false:name.equals(otherName);
	    }

	    public String toString()
	    {
	       return name;
	    }
	}
	
	public static Map<String, String> getConnectorDefaultArguments(String connectorClient, String birstConnectorVersion, String userID, String spaceID, String spaceDirectory) {
		Map<String, String> argumentsMap = new HashMap<String, String>();
		if (userID != null)
		{
			argumentsMap.put("UserID", userID);
		}
		if (spaceID != null)
		{
			argumentsMap.put("SpaceID", spaceID);
		}
		if (spaceDirectory != null)
		{
			argumentsMap.put("SpaceDirectory", spaceDirectory);
		}
		if (connectorClient != null)
		{
			argumentsMap.put("Client", connectorClient);
		}
		if (birstConnectorVersion != null)
		{
			argumentsMap.put("BirstConnectorVersion", birstConnectorVersion);
		}
		return argumentsMap;
	}
	
	public static Map<String, String> fillConnectorParameters(String connectorClient, String connectorString) throws Exception
	{
		Map<String, String> parametersMap = new HashMap<String, String>();
		try
		{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(connectorClient, null, null, null, null);
			argumentsMap.put("ConnectorString", connectorString);
			String result = client.callConnectorServlet(IConnectorServlet.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, GET_CONNECTOR_PARAMETERS_SERVLET, true, argumentsMap);		
			OMElement connectorParamsEl = XmlUtils.convertToOMElement(result);
			if (connectorParamsEl == null)
			{
				UnrecoverableException ure = new UnrecoverableException("Unable to get Connector Parameters for connectionstring: " + connectorString, new Exception());
				ure.setErrorCode(BaseConnectorException.ERROR_OTHER);
				throw ure;
			}
			if (connectorParamsEl != null)
			{
				for (Iterator<OMElement> connectorParamIterator = connectorParamsEl.getChildElements(); connectorParamIterator.hasNext(); )
				{
					OMElement connectorParamEl = connectorParamIterator.next();
					String paramName = null;
					String paramValue = null;
					for (Iterator<OMElement> connectorDetailIterator = connectorParamEl.getChildElements(); connectorDetailIterator.hasNext(); )
					{
						OMElement el = connectorDetailIterator.next();
						String elName = el.getLocalName();
						if (NODE_CONNECTOR_PARAM_NAME_NODE.equals(elName))
						{
							paramName = XmlUtils.getStringContent(el);
						}
						else if (NODE_CONNECTOR_PARAM_VALUE_NODE.equals(elName))
						{
							paramValue = XmlUtils.getStringContent(el);
						}
					}
					parametersMap.put(paramName, paramValue);
				}
			}
		}
		catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
		return parametersMap;		
	}

	public static String getDefaultAPIVersion(String connectorClient, String birstConnectorVersion, String userName, String spaceID, String spaceDirectory, String connectorString) throws Exception
	{
		OMElement element = null;
		try
		{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(connectorClient, birstConnectorVersion, userName, spaceID, spaceDirectory);
			argumentsMap.put("ConnectorString", connectorString);
			String result = client.callConnectorServlet(IConnectorServlet.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, GET_DEFAULT_VERSION_SERVLET, true, argumentsMap);		
			return parseConnectorStringResultElement(result);			
		}
		catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
		return null;		
	}
	
	public static String parseConnectorStringResultElement(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null)
		{
			UnrecoverableException ure = new UnrecoverableException("Unable to parse result received from Controller", new Exception());
			ure.setErrorCode(BaseConnectorException.ERROR_OTHER);
			throw ure;
		}
		boolean isSuccess = false;
		String root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					root = el.getText();					
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseConnectorException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}
		return root;
	}
	
	public static boolean parseConnectorBooleanResult(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		boolean isSuccess = false;
		OMElement root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		boolean resultValue = false;
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					resultValue = Boolean.valueOf(XmlUtils.getBooleanContent(el));								
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseConnectorException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}		
		return resultValue;
	}
	
	public static String getCurrentAPIVersionConnectionString(IConnector connector) throws Exception
	{
		String currentAPIVersion = connector.getMetaData().getConnectorCloudVersion().replace('_', '.');
		String connectorName = connector.getMetaData().getConnectorName();
		String currentAPIVersionConnectionString = "connector:birst://" + connectorName.toLowerCase() + ":" + currentAPIVersion;
		return currentAPIVersionConnectionString;
	}
	
	public static Map<String,String> getConnectorActivityLogParams(IConnector connector, String activityID, String uid, String requestType, String status, String startTime, String finishTime)
	{
		Map<String, String> connectorLogParams = new HashMap<String, String>();
		try
		{
			String currentAPIVersionConnectionString = ControllerServices.getCurrentAPIVersionConnectionString(connector);
			connectorLogParams.put("ConnectorString", currentAPIVersionConnectionString);
			connectorLogParams.put("UniqueID", uid);
			connectorLogParams.put("ActivityID", activityID);
			connectorLogParams.put("RequestType", requestType);
			connectorLogParams.put("StartTime", startTime);
			connectorLogParams.put("FinishTime", finishTime);
			connectorLogParams.put("Status", status);
		} 
		catch(Exception ex)
		{
			logger.warn("Error while setting connector activity log params");
		}
		return connectorLogParams;
	}
	
	public static String logConnectorActivity(String client, String birstConnectorVersion, String userName, String spaceID, String spaceDirectory, Map<String, String> arguments) throws Exception
	{
		try
		{
			ConnectorHttpClient httpClient = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(client, birstConnectorVersion, userName, spaceID, spaceDirectory);
			argumentsMap.putAll(arguments);
			String result = httpClient.callConnectorServlet(IConnectorServlet.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LOG_CONNECTOR_ACTIVITY_SERVLET, true, argumentsMap);		
			return parseConnectorStringResultElement(result);			
		}
		catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
		return "-1";		
	}
}
