package com.birst.connectors.util;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.birst.connectors.ConnectorDataExtractor;

public class LogUtil 
{
	private static final String [] rejectLogLevel= {"DEBUG", "TRACE"};
	private static final String [] allLogLevel= {"TRACE", "DEBUG", "INFO", "WARN", "ERROR"};
	private static final String errorLogLevel= "ERROR";
	public static Pattern[] rejectLogPattern;
	public static Pattern[] allLogPattern;
	public static Pattern errorLogPattern;
	public static Pattern errorMessagePattern;
	
	static
	{
		rejectLogPattern = LogUtil.getRejectLogLevelPattern();
		allLogPattern = LogUtil.getAllLogLevelPattern();
		errorLogPattern = LogUtil.getErrorLogLevelPattern();
		errorMessagePattern = LogUtil.getErrorMessagePattern();
	}
	
	public static final int LOG_WRITER_THRESHOLD = 100;
	
	private static Pattern[] getRejectLogLevelPattern()
	{
		if (rejectLogPattern == null)
		{
			rejectLogPattern = new Pattern[rejectLogLevel.length];
			int patternIdx = 0;
			for(String rejectLogType: rejectLogLevel)
			{
				Pattern p = Pattern.compile("(\\[)(.*?)(\\])[\\s]("+rejectLogType+")[\\s]-"); //i.e. [<some_text>] DEBUG -
				rejectLogPattern[patternIdx] = p;
				patternIdx++;
			}
		}
		
		return rejectLogPattern;
	}
	
	private static Pattern[] getAllLogLevelPattern()
	{
		if (allLogPattern == null)
		{
			allLogPattern = new Pattern[allLogLevel.length];
			int patternIdx = 0;
			for(String logType: allLogLevel)
			{
				Pattern p = Pattern.compile("(\\[)(.*?)(\\])[\\s]("+logType+")[\\s]-"); //i.e. [<some_text>] DEBUG -
				allLogPattern[patternIdx] = p;
				patternIdx++;
			}
		}
		
		return allLogPattern;
	}
	
	private static Pattern getErrorLogLevelPattern()
	{
		if (errorLogPattern == null)
		{
			errorLogPattern = Pattern.compile("(\\[)(.*?)(\\])[\\s]("+errorLogLevel+")[\\s]-"); //i.e. [<some_text>] DEBUG -
		}
		
		return errorLogPattern;
	}
	
	private static Pattern getErrorMessagePattern()
	{
		if (errorMessagePattern == null)
		{
			errorMessagePattern =  Pattern.compile(ConnectorDataExtractor.errorMessagePrefix); //i.e. >>>SomeErrorMessage
		}
		
		return errorMessagePattern;
	}
	
	public static File getConnectorLogDirectory()
	{
		String connectorHome = System.getProperty("connector.home");
		File connectorLogLocation = null;
		if(connectorHome != null)
		{
			String dirPath = connectorHome + File.separator + "logs";
			connectorLogLocation = new File(dirPath);
		}
		return connectorLogLocation;
	}
	
	public static File getConnectorLogDirForSpace(String spaceDirectory)
	{
		String spaceConnectorLogPath = spaceDirectory + File.separator + "connectorlogs";
		File outDir = new File(spaceConnectorLogPath);
		if (!outDir.exists() || !outDir.isDirectory())
		{
			outDir.mkdir();
		}
		return outDir;
	}
	
	public static boolean isErrorLog(String line)
	{
		Matcher m = errorLogPattern.matcher(line);
		return m.find();
	}
	
	public static boolean isRejectLog(String line)
	{
		boolean isRejectLog = false;
		for(Pattern logPattern: rejectLogPattern)
		{
			Matcher m = logPattern.matcher(line);
			if (m.find()) 
			{
				isRejectLog = true;
				break;
			}									
		}
		return isRejectLog;
	}
	
	//error log message should not be part of any log level and should match errorMessagePattern
	public static boolean isErrorMessage(String line)
	{
		boolean isErrorMessage = false;
		Matcher errorMatcher = errorMessagePattern.matcher(line);
		if (errorMatcher.find())
		{
			isErrorMessage = (errorMatcher.regionStart() == 0);
		}
		return isErrorMessage;
	}
	
	public static boolean isStackrace(String line)
	{
		boolean isStacktrace = true;
		for(Pattern logPattern: allLogPattern)
		{
			Matcher m = logPattern.matcher(line);
			if (m.find()) 
			{
				isStacktrace = false;
				break;
			}									
		}
		return isStacktrace;
	}
}
