package com.birst.connectors.util;

import java.io.File;

import org.apache.log4j.Logger;

import com.birst.connectors.ConnectorDataExtractor;
import com.birst.connectors.IConnector;
import com.birst.connectors.UnrecoverableException;

public class FileCleanupUtil 
{
	private IConnector connector;
	private String spaceDirectory;
	private static final Logger logger = Logger.getLogger(FileCleanupUtil.class);
	
	public FileCleanupUtil(IConnector connector, String spaceDirectory)
	{
		this.connector = connector;
		this.spaceDirectory = spaceDirectory;
	}
	
	//BPD-18779 not sure if cleaning up the temp files is actually necessary. 
	// they might help with debugging. Not calling it for now.
	public void cleanupTemp() throws UnrecoverableException
	{
		if (!connector.getMetaData().getConnectorName().toLowerCase().contains("netsuite"))
		{
			return;
		}
		// Directory path here
		String path = spaceDirectory + File.separator + "tempDepData"; 

		File folder = new File(path);
		File[] listOfFiles = folder.listFiles(); 
		if (listOfFiles != null && listOfFiles.length > 0)
		{
			for (int i = 0; i < listOfFiles.length; i++) 
			{
				if (listOfFiles[i].isFile()) 
				{
					File newFile = listOfFiles[i];
					String fileName = newFile.getName();
	
					// Destination directory
					File destFile = new File(spaceDirectory + File.separator + "data" + File.separator + fileName);
					//deleting the file if it exists
					if(destFile.exists()){
						destFile.delete();
					}
					// Move file to new directory
					boolean success = newFile.renameTo(destFile);
					if (!success) {
						logger.error("Can't copy the new file: " + fileName +" to the \"data\" folder");
					}
				}
			}
	
			File fin = new File(path);
			File[] finlist = fin.listFiles();       
			for (int n = 0; n < finlist.length; n++) {
				if (finlist[n].isFile()) {
					finlist[n].delete();
				}
			}
			fin.delete();
		}
	}
	
	public void  clearStatusFiles()
	{
		String connectorDirPath = spaceDirectory + File.separator + "connectors";
		File dir = new File(connectorDirPath);
		if (!dir.exists() || !dir.isDirectory())
		{
			return;
		}
		
		FilenameFilterUtil filter = new FilenameFilterUtil(ConnectorDataExtractor.statusFileSuffix);
		
		File files[] = dir.listFiles(filter);
		for (File file : files) 
		{
			file.delete();
		}
	}
}
