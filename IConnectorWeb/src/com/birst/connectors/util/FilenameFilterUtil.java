package com.birst.connectors.util;

import java.io.File;
import java.io.FilenameFilter;

public class FilenameFilterUtil implements FilenameFilter
{
	private String extension;
    
    public FilenameFilterUtil(String extension) 
    {
    	this.extension = extension;
    }

    public boolean accept(File dir, String name) 
    {
    	return (name.toLowerCase().endsWith(extension.toLowerCase()));
    }
}
