package com.birst.connectors.util;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileFilterDateIntervalUtil implements FilenameFilter
{ 
    String dateStart;
    SimpleDateFormat sdf;
    
    public FileFilterDateIntervalUtil(Date dateStart) 
    {
    	sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSSZ");
    	this.dateStart = sdf.format(dateStart);
    }

    public boolean accept(File dir, String name) 
    {
    	Date d = new Date(new File(dir, name).lastModified());
    	String current = sdf.format(d);
    	return ((dateStart.compareTo(current) < 0));
    }
}