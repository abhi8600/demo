﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using System.IO;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class FileInfoDateComparerTest
    {
        private List<string> files;
        private int numFiles;

        [SetUp]
        public void setup()
        {
            files = new List<string>();
            numFiles = 6;
            for (int i = 0; i < numFiles; i++)
            {
                string fileName = System.IO.Path.GetTempFileName();
                files.Add(fileName);
                System.Console.WriteLine("File Created : " + fileName);
            }

        }

        [TearDown]
        public void tearDown()
        {
            foreach (string fileName in files)
            {
                try
                {
                    File.Delete(fileName);
                }
                catch (Exception)
                {
                    // oops what to do now. Its a zero byte file. Will not harm
                    System.Console.WriteLine("Unable to delete the file : " + fileName);
                }
            }
        }

        [Test]
        public void testComparer()
        {
            int[] expectedSortOrder = new int[] { 5, 4, 3, 2, 0, 1 };
            modifyFileTimeStamps(expectedSortOrder);
            int[] actualSortOrder = getSortedFiles();
            Assert.AreEqual(expectedSortOrder, actualSortOrder);
        }

        [Test]
        public void testCompareOneFileDeleted()
        {
            int[] expectedSortOrder = new int[] { 5, 4, 3, 2, 0, 1 };
            modifyFileTimeStamps(expectedSortOrder);
            File.Delete(files[3]);
            int[] actualSortOrder = getSortedFiles();
            Assert.AreEqual(expectedSortOrder, expectedSortOrder);
        }

        private void modifyFileTimeStamps(int[] expectedSortOrder)
        {
            List<FileInfo> fileInfoList = new List<FileInfo>();
            DateTime date = DateTime.Now;
            // Put the creation time in descending order of their names

            int count = 0;
            foreach (int i in expectedSortOrder)
            {
                FileInfo fi = new FileInfo(files[i]);
                fi.Refresh();
                fi.LastWriteTime = date.AddDays(count);
                fi.Refresh();
                System.Console.WriteLine("Modified the time stamp on the following file = " + fi.ToString());
                count++;
            }
        }

        private int[] getSortedFiles()
        {
            string[] sortedFiles = Util.sortFiles(files.ToArray());
            int[] actualSortOrder = new int[files.Count];

            for (int index = 0; index < files.Count; index++)
            {
                System.Console.WriteLine("Sorted Files " + sortedFiles[index]);
                actualSortOrder[index] = files.IndexOf(sortedFiles[index]);
                System.Console.WriteLine("order :" + actualSortOrder[index]);
                printTimeStamps(sortedFiles[index]);
            }

            return actualSortOrder;
        }

        private void printTimeStamps(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            fi.Refresh();
            System.Console.WriteLine("Time stamp : " + fi.LastWriteTime + " on the file : " + fi.ToString());            
        }
    }
}