using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.IO;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Data.Odbc;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.Net.Security;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Text.RegularExpressions;
using Acorn.tests;
using System.Net.Sockets;
using System.IO.Compression;
using Acorn.WebDav;
using Acorn.DBConnection;
using Acorn.Exceptions;
using System.Management;
using System.Diagnostics;
using System.Linq;

namespace Acorn
{
    public class Util
    {
        private static string useisapi = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["isapiEnabled"];

        private static bool GENERATE_SNOWFLAKE_HIERARCHIES = true;
        private static bool GENERATE_SNOWFLAKE_JOINS_FOR_DEPENDENCIES = true;
        public static bool GENERATE_INHERITED_MEASURE_TABLES = true;
        public static string SPACE_CONFIG_FILENAME = "config/Spaces.config";
        private static string NEW_USER_SPACE_CONFIG_FILENAME = "config/NewUserSpaces.config";
        public static string OWNER_GROUP_NAME = "OWNER$";
        public static string USER_GROUP_NAME = "USER$";
        public static string LOAD_GROUP_NAME = "ACORN";
        public static int MAX_LOAD_NUMBER = 100000 - 1;
        public static string PROXY_DB_TYPE = "MS SQL Server 2005";
        public static string PROXY_DB_DRIVER_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        private static List<NewUserSpace> newUserSpaces = new List<NewUserSpace>();
        public static String loginServiceEndPoint = "/SMIWeb/services/SMIWebLogin.SMIWebLoginHttpSoap11Endpoint";
        public static String sessionServiceEndPoint = "/SMIWeb/services/SMIWebSession.SMIWebSessionHttpSoap11Endpoint";
        public static string REPOSITORY_DEV = "repository_dev.xml";
        public static string REPOSITORY_PROD = "repository.xml";
        public static string LOAD_INITIATED_TIME = "LoadInitiatedTime";
        public static string LOAD_START_TIME = "LoadStartTime";
        public static string LOAD_COMPLETION_TIME = "LoadCompletionTime";
        public static string SFDC_EXTRACT_START_TIME = "SFDCExtractStartTime";
        public static string SFDC_EXTRACT_COMPLETION_TIME = "SFDCExtractCompletionTime";
        public static string NETSUITE_EXTRACT_START_TIME = "NetsuiteExtractStartTime";
        public static string NETSUITE_EXTRACT_COMPLETION_TIME = "NetsuiteExtractCompletionTime";
        public static string DYNAMIC_GROUP_VARIABLE_NAME = "Birst$Groups";
        private static Regex smiEngineTimeFormatRegex;
        private static Regex spacesConfigCommandVersionRegex;
        private static Regex trialUserEmailAddressRegex;
        public static bool TEST_FLAG = false;
        public static MockHttpSession mockSession;
        public static readonly string SFDC_EXTRACT_LOCK_FILE = "SFDC_extract.lock";
        public static readonly string SFDC_KILL_LOCK_FILE = "SFDC_kill.lock";
        public static readonly string PUBLISH_KILL_LOCK_FILE = "Load_kill.lock";
        public static readonly string CONNECTOR_CONFIG_DIR = "connectors";
        public static readonly string DCCONFIG_DIR = "dcconfig";
        public static readonly string DCCONFIG_FILE_NAME = "dcconfig.xml";
        public const string RUNNING_TYPE_LOAD = "PROCESS_DATA";
        public static readonly string ForceComURL = "https://[^/?]+\\.(sales|cloud|visual\\.)force\\.com/.*";

        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string urlHeader = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BirstURLHeader"];
        private static string trustedIPs = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TrustedSuperUserIPs"];
        private static string trustedInterServerIPs = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TrustedInterServerIPs"];
        private static string trustedTrialRequestServerIPs = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TrustedTrialRequestServerIPs"];
        private static string termsOfServiceURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TermsOfServiceLink"];
        private static string privacyPolicyURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PrivacyPolicyLink"];
        private static string contactUsURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContactUsLink"];
        private static string copyRight = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["copyRight"];


        // redshift properties for time dim tables
        private static string s3BucketName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RedshiftTimeBucketName"];
        private static string awsAccessKeyId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsAccessKeyId"];
        private static string awsSecretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsSecretKey"];
        private static string redShiftTimeCommand = System.Web.Configuration.WebConfigurationManager.AppSettings["RedshiftCreateTimeCommand"];
        private static string sapHanaCreateTimeCommand = System.Web.Configuration.WebConfigurationManager.AppSettings["SAPHanaCreateTimeCommand"];
        private static string timeDimensionFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TimeDimensionFolder"];

        // redshift properties for upload mechanism
        private static string uploadS3BucketName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UploadRedshiftBucketName"];
        private static string uploadAwsAccessKeyId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UploadRedshiftAwsAccessKeyId"];
        private static string uploadAwsSecretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UploadRedshiftAwsSecretKey"];
        // driver naming is dependent on whether its 64 or 32 bit machine
        private static string postgresDriverName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PostgreSQLDriverName"];
        public  static string postgreSQLDriverVersion = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PostgreSQLDriverVersion"];
        private static string urlToRedirect = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["URLToRedirect"];

        //appliance mode
        private static string allowOverrideSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AllowOverrideSpaceSchema"];
        private static string isApplianceMode = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["IsApplianceMode"];

        private static string localProtocol;
        private static int sfdcDefaultVersionId;

        private static int REPOSITORY_SIZE_WARN = 10000000; // 10MB
        private static int REPOSITORY_SIZE_ERROR = 100000000; // 100MB
        public static DateTime scheduledReportReference = DateTime.MinValue;

        // session id cookie name
        public const string sessionIdName = "ASP.NET_SessionId";

        // Threshold number for max concurrent users for an account
        public static int THRESHOLD_WARN_CONCURRENT_LOGINS = 5;

        public static string DEFAULT_DATE_FORMAT = "MM'/'dd'/'yyyy"; //SQL Server
        public static readonly int SPACE_PROCESS_VERSION_UNDEFINED = -1;

        public static bool taskScheduledForSForce = false;
        public static bool taskScheduledForReport = false;
        public const string LOAD_LOCK_FILE = "load.lock";

        //unsupported file name chars 
        public static string BAD_FILE_NAME_CHARS = "[\\.]";
        public static string BAD_FILE_NAME_CHARS_STR = ".";

        // For MoveSpace
        public static string MOVE_LOCAL_PROTOCOL = null;

        // session object names
        public const string SESSION_OBJECT_PACKAGELIST_CACHE = "packageListCache";

        // feature flag names
        public const string FEATURE_FLAG_VIZ_GA = "EnableVisualizerForAll";

        //For Redshift
        public static string defaultPostgresSQLDriverVersion = "09.00.0101";

        public static bool isTaskSchedulerEnabledForReport()
        {
            return taskScheduledForReport;
        }

        public static bool isTaskSchedulerEnabledForSForce()
        {
            return taskScheduledForSForce;
        }

        public static Regex unsupportedColNameChars = new Regex("[^\\w\\s:$#%\\-&!^@,;></]");
        public static string replaceUnSupportedColChars(string colName)
        {
            return unsupportedColNameChars.Replace(colName, "_");
        }

        public static string replaceUnSupportedColChars(string colName, int queryLanguageVersion)
        {
            if (queryLanguageVersion == Acorn.Database.OLD_QUERY_LANGUAGE)
            {
                return new Regex(Performance_Optimizer_Administration.Util.OLD_QUERY_LANGUAGE_UNSUPPORTED_CHARS).Replace(colName, "_");
            }
            else
            {
                return new Regex(Performance_Optimizer_Administration.Util.NEW_QUERY_LANGUAGE_UNSUPPORTED_CHARS).Replace(colName, "_");
            }
        }

        /*
         * is the address 'ipAddress' within the netblock 'cidr'
         * - IPv4: a.b.c.d/int
         */
        private static bool IsInSubnet(string ipAddress, string cidr)
        {
            string[] parts = cidr.Split('/');
            int baseAddress = BitConverter.ToInt32(IPAddress.Parse(parts[0]).GetAddressBytes(), 0);
            int address = BitConverter.ToInt32(IPAddress.Parse(ipAddress).GetAddressBytes(), 0);
            int bits = 32;
            if (parts.Length == 2)
            {
                bits = int.Parse(parts[1]);
            }
            int mask = IPAddress.HostToNetworkOrder(-1 << (32 - bits));
            return ((baseAddress & mask) == (address & mask));
        }

        public static string getLocalProtocol()
        {
            if (localProtocol == null || localProtocol.Length == 0)
            {
                localProtocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            }

            return localProtocol;
        }

        public static UploadToken validateAndGetUploadToken(HttpRequest request, HttpResponse response)
        {
            string token = request.Params["UPLOADTOKEN"];
            if (token == null)
            {
                response.Clear();
                Global.systemLog.Warn("Upload request with no UploadToken");
                response.Status = "401 Unauthorized";
                response.StatusCode = 401;
                response.Write("error=true");
                return null;
            }

            Guid tokenID = Util.decryptToken(token);
            if (tokenID == Guid.Empty)
            {
                Global.systemLog.Warn("Bad Request - invalid uploadToken format: " + token);
                response.StatusCode = 400;
                response.Write("error=true");
                return null;
            }
            QueryConnection conn = null;
            UploadToken uploadToken = null;
            try
            {
                conn = ConnectionPool.getConnection();
                uploadToken = Database.getUploadToken(conn, mainSchema, tokenID);
                if (uploadToken == null)
                {
                    Global.systemLog.Warn("Upload request with invalid UploadToken: " + tokenID);
                    response.Write("error=true");
                    response.StatusCode = 400;
                    return null;
                }

                if (DateTime.Now > uploadToken.endOfLife)
                {
                    Global.systemLog.Warn("SSO request with invalid BirstSSOToken: " + uploadToken.endOfLife + " (timestamp too old)");
                    response.Write("error=true");
                    response.StatusCode = 400;
                    return null;
                }

                User u = Database.getUserById(conn, mainSchema, uploadToken.userID);
                if (u == null)
                {
                    Global.systemLog.Warn("Upload request failed with invalid user: " + uploadToken.userID + " (not in the database or expired products)");
                    response.Write("error=true");
                    response.StatusCode = 400;
                    return null;
                }
                if (u.Disabled)
                {
                    Global.systemLog.Warn("Upload request failed with invalid user: " + uploadToken.userID + " (disabled)");
                    response.Write("error=true");
                    response.StatusCode = 400;
                    return null;
                }
            }
            finally
            {
                if (conn != null)
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return uploadToken;
        }

        public const string KEY_REPORT_EMAIL_FROM = "reportEmailFrom";
        public const string KEY_REPORT_EMAIL_SUBJECT = "reportEmailSubject";

        public static Dictionary<string, string> getReportScheduleEmailProps(Space sp)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Dictionary<string, string> props = Database.getSpaceEmailProperties(conn, Util.getMainSchema(), sp.ID);
                if (props == null)
                {
                    props = new Dictionary<string, string>();
                }

                if (!props.ContainsKey(KEY_REPORT_EMAIL_FROM))
                {
                    props.Add(KEY_REPORT_EMAIL_FROM, getGlobalReportScheduleFrom());
                }

                if (!props.ContainsKey(KEY_REPORT_EMAIL_SUBJECT))
                {
                    props.Add(KEY_REPORT_EMAIL_SUBJECT, getGlobalReportScheduleSubject());
                }
                return props;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static string getGlobalReportScheduleFrom()
        {
            string from = System.Web.Configuration.WebConfigurationManager.AppSettings["ReportScheduleEmailFrom"];
            if (from != null && from.Trim().Length > 0)
            {
                return from;
            }
            return "delivery@birst.com";
        }

        private static string getGlobalReportScheduleSubject()
        {
            string from = System.Web.Configuration.WebConfigurationManager.AppSettings["ReportScheduleEmailSubject"];
            if (from != null && from.Trim().Length > 0)
            {
                return from;
            }
            return "Report From Birst";
        }

        // used by the AcornExecute wrapper to push information about load process started by scheduler
        public static void addRunningProcessInfoToDB(Space sp, string source, string pids, string loadInfo)
        {
            Database.addProcessingServer(mainSchema, sp.ID, Util.RUNNING_TYPE_LOAD, Util.getServerIP(), Util.getServerPort(), source, ProcessHandler.getPidsString(pids, true), loadInfo);
        }
        /*
         * is the address 'ipAddress' within the list of netblocks specified in the 'TrustedSuperUserIPs' list in web.config
         */
        public static bool IsValidIP(string ipAddress)
        {
            if (ipAddress == "::1") // XXX hack for now until we get IPv6 in here
                return true;

            if (trustedIPs == null)
                return false;
            try
            {
                string[] validIPs = trustedIPs.Split(',');
                foreach (string ip in validIPs)
                {
                    if (IsInSubnet(ipAddress, ip))
                        return true;
                }
            }
            catch (Exception) { }
            return false;

        }

        /*
         * is the address 'ipAddress' within any of the netblocks in the list 'ipList'
         */
        public static bool IsValidIP(string ipAddress, List<String> ipList)
        {
            // null or zero sized list means no restrictions
            if (ipList == null || ipList.Count == 0)
                return true;
            try
            {
                foreach (string ip in ipList)
                {
                    if (ipAddress.Equals(ip))
                        return true;
                    if (IsInSubnet(ipAddress, ip))
                        return true;
                }
            }
            catch (Exception) { }
            return false;
        }


        /* Hash Staging table names and columns if they exceed the database identifier limit. This code is explicitly called in MoveSpace
        * 
        */
        public static void buildStagingTables(MainAdminForm ma)
        {
            StagingTables stagingTablesBuilder = new StagingTables(ma);
            ApplicationBuilder ab = new ApplicationBuilder(ma);
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                st.Name = stagingTablesBuilder.getStagingNameFromSourceFileName(st.SourceFile, st.LiveAccess, ma);
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.Name.Length > ab.getMaxPhysicalColumnNameLength() && ma.builderVersion >= 28)
                    {
                        sc.PhysicalName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(ma), sc.Name + ApplicationBuilder.PHYSICAL_SUFFIX, ab.getMaxPhysicalColumnNameLength(), ma.builderVersion);
                    }
                }
            }

        }


        public static void buildApplication(Performance_Optimizer_Administration.MainAdminForm maf, string mainSchema, DateTime loadDate, int loadNumber, Dictionary<string, int> sourceGroupLoadNumberMap, Space sp, HttpSessionState session)
        {
            // Global.systemLog.Debug("-- build application");
            if (maf.DoNotAllowBirstToRebuildApplicationCheckBox.Checked) // allow enterprise implementations to do builds outside of Birst
            {
                Global.systemLog.Info("Tried to build application for space " + sp.ID + ", but DoNotAllowBirstToRebuildApplication is set to true");
                return;
            }

            bool isInUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);

            maf.setRepositoryDirectory(sp.Directory);

            //setup localetl loadgroup to localetl config map
            // Global.systemLog.Debug("--- get local etl configs");
            maf.localETLLoadGroupToLocalETLConfig = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            // Global.systemLog.Debug("--- set time");
            if ((maf.timeDefinition == null || maf.timeDefinition.isDefault()) &&
                (maf.stagingTableMod.getAllStagingTables().Count > 0 ||
                (maf.Imports != null && maf.Imports.Length > 0)))
            {
                maf.timeDefinition = new TimeDefinition();
                maf.timeDefinition.Day = true;
                maf.timeDefinition.Week = true;
                maf.timeDefinition.Month = true;
                maf.timeDefinition.Quarter = true;
                maf.timeDefinition.Year = true;
                maf.timeDefinition.GenerateTimeDimension = true;
                maf.timeDefinition.Periods = new TimePeriod[7];
                maf.timeDefinition.Periods[0] = new TimePeriod("TTM", TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 12, 0, 0, false);
                maf.timeDefinition.Periods[1] = new TimePeriod("T3M", TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 3, 0, 0, false);
                maf.timeDefinition.Periods[2] = new TimePeriod("MAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_MONTH, true);
                maf.timeDefinition.Periods[3] = new TimePeriod("QAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_QUARTER, true);
                maf.timeDefinition.Periods[4] = new TimePeriod("YAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_YEAR, true);
                maf.timeDefinition.Periods[5] = new TimePeriod("QTD", TimePeriod.AGG_TYPE_TO_DATE, TimePeriod.PERIOD_QUARTER, 1, 0, 0, false);
                maf.timeDefinition.Periods[6] = new TimePeriod("YTD", TimePeriod.AGG_TYPE_TO_DATE, TimePeriod.PERIOD_YEAR, 1, 0, 0, false);
                maf.serverPropertiesModule.snowflakeSize.Text = "3";
            }
            maf.serverPropertiesModule.generateUniqueKeys.Checked = true;
            if (maf.timeDefinition != null && maf.stagingTableMod.getAllStagingTables().Count > 0)
                setupTime(maf, loadDate, loadNumber);
            /*
             * Make sure key columns are only counted and all other columns' aggregations are set according to data types
             */
            // Global.systemLog.Debug("--- set aggs");
            setAggregations(maf);
            /*
             * If advanced, make sure that no dimension tables are being generated 
             * for which there are no grains
             */
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                if (maf.timeDefinition != null && maf.timeDefinition.Name == h.DimensionName)
                    continue;
                List<Level> levelList = new List<Level>();
                h.getChildLevels(levelList);
                foreach (Level l in levelList)
                {
                    bool found = false;
                    foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                    {
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] == h.DimensionName && level[1] == l.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found)
                            break;
                    }
                    if (!found)
                        l.GenerateDimensionTable = false;
                }
            }
            /*
             * Build the application.
             */
            ApplicationBuilder ab = new ApplicationBuilder(maf);
            ab.setCarryKeysDownLevels(!maf.disableImpliedGrainsProcessing);
            /*
             * Build any required inherited measure tables
             */
            if (GENERATE_INHERITED_MEASURE_TABLES)
                ab.setGenerateInheritedTables(true);
            // Set degenerate levels
            // Global.systemLog.Debug("--- set degenerate levels");
            bool useDegenerates = maf.builderVersion >= 6 && (loadNumber == 1 || (sourceGroupLoadNumberMap != null && sourceGroupLoadNumberMap.Count == 0));

            if (useDegenerates)
            {
                HashSet<string> listOfLoadGroups = getLoadGroups(maf);
                foreach (String loadGroup in listOfLoadGroups)
                {
                    bool hasLoadedData = Status.hasLoadedData(sp, loadGroup);
                    if (hasLoadedData)
                    {
                        useDegenerates = false;
                        break;
                    }
                }
            }

            ab.setUseDegenerateTables(useDegenerates);
            ab.setAutomaticModeDependencies(sp.Automatic);
            // Set imports
            // Global.systemLog.Debug("--- set imports");
            if (maf.Imports != null && maf.Imports.Length > 0)
            {
                List<ImportedRepositoryItem> ilist = Util.getImportedTables(maf, session, null);
                if (ilist != null && ilist.Count > 0)
                {
                    Dictionary<StagingTable, SourceFile> importList = new Dictionary<StagingTable, SourceFile>();
                    Dictionary<StagingTable, MainAdminForm> importRepositories = new Dictionary<StagingTable, MainAdminForm>();
                    Dictionary<DimensionTable, MainAdminForm> importDimRepositories = new Dictionary<DimensionTable, MainAdminForm>();
                    Dictionary<MeasureTable, MainAdminForm> importMRepositories = new Dictionary<MeasureTable, MainAdminForm>();
                    foreach (ImportedRepositoryItem it in ilist)
                    {
                        if (it.st != null)
                        {
                            importList.Add(it.st, it.sf);
                            importRepositories.Add(it.st, it.ispace.maf);
                        }
                        if (it.dt != null)
                        {
                            importDimRepositories.Add(it.dt, it.ispace.maf);
                        }
                        if (it.mt != null)
                        {
                            importMRepositories.Add(it.mt, it.ispace.maf);
                        }
                    }
                    if (importList.Count > 0 || importDimRepositories.Count > 0 || importMRepositories.Count > 0)
                        ab.setImportList(importList, importRepositories, importDimRepositories, importMRepositories);
                }
            }
            // Global.systemLog.Debug("--- maf build application");
            maf.buildApplication(mainSchema, ab);
            // Global.systemLog.Debug("--- generate snowflakes");
            if (maf.dependencies != null)
            {
                if (maf.builderVersion >= 15)
                {
                    if (GENERATE_SNOWFLAKE_JOINS_FOR_DEPENDENCIES)
                    {
                        ab.generateSnowflakeJoinsForDependencies();
                    }
                }
                else
                {
                    /*
                     * Hide dimensions that are solely dependencies and create snowflake dimensions
                     */
                    if (GENERATE_SNOWFLAKE_HIERARCHIES)
                    {
                        ab.generateSnowflakeHierarchies();
                    }
                }
            }
            /* 
             * Add filters to each date measure table - Ensures that measure table definitions 
             * that join via dates other than the load date are filtered for the latest load only
             */
            if (loadNumber >= 0)
            {
                // Global.systemLog.Debug("--- set load id filters (1)");
                setLoadIDFilters(maf, loadNumber, sourceGroupLoadNumberMap);
            }
            else if (sp != null)
            {
                // Global.systemLog.Debug("--- set load id filters (2)");
                loadNumber = sp.LoadNumber;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Dictionary<string, int> curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                    setLoadIDFilters(maf, loadNumber, curLoadNumbers);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            /*
             * Consistency checks
             */
            // Global.systemLog.Debug("--- check consistency");
            checkConsistency(maf, session);
            maf.setInUberTransaction(isInUberTransaction);
        }


        public static int getLatestLoadNumber(Space sp, String loadGroup)
        {
            int loadNumber = -1;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Dictionary<string, int> curLoadNumbers = Database.getCurrentLoadNumbers(conn, Util.getMainSchema(), sp.ID);

                if (loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                {
                    loadNumber = curLoadNumbers.ContainsKey(SalesforceLoader.LOAD_GROUP_NAME) ? curLoadNumbers[SalesforceLoader.LOAD_GROUP_NAME] : SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                }
                else if (loadGroup == Util.LOAD_GROUP_NAME)
                {
                    loadNumber = sp.LoadNumber;
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return loadNumber;
        }

        private static void checkConsistency(MainAdminForm maf, HttpSessionState session)
        {
            validateHierarchyLevelColumns(maf);
            validateDiscoveryHierarchies(maf);
            validateSourceFileColumns(maf);
            validateGrainsAndKeys(maf, session);
        }

        public static List<MeasureTable> getNonVizLiveAccessMeasureTables(MainAdminForm maf, HashSet<string> realTimeConnectionsList)
        {
            List<MeasureTable> liveAccessNonVizMeasureTables = new List<MeasureTable>();
            if (realTimeConnectionsList != null && realTimeConnectionsList.Count > 0)
            {
                if (maf.measureTablesList != null && maf.dimensionTablesList.Count > 0)
                {
                    foreach (MeasureTable measureTable in maf.measureTablesList)
                    {
                        if (measureTable.AutoGenerated || (measureTable.InheritTable != null && measureTable.InheritTable.Length > 0))
                        {
                            continue;
                        }
                        string measureTableConnection = measureTable.TableSource.Connection;
                        if (measureTableConnection != null && realTimeConnectionsList.Contains(measureTableConnection))
                        {
                            liveAccessNonVizMeasureTables.Add(measureTable);
                        }
                    }
                }
            }
            return liveAccessNonVizMeasureTables;
        }

        public static List<DimensionTable> getNonVizLiveAccessDimensionTables(MainAdminForm maf, HashSet<string> realTimeConnectionsList)
        {
            List<DimensionTable> liveAccessNonVizDimTables = new List<DimensionTable>();
            if (realTimeConnectionsList != null && realTimeConnectionsList.Count > 0)
            {
                if (maf.dimensionTablesList != null && maf.dimensionTablesList.Count > 0)
                {
                    foreach (DimensionTable dimensionTable in maf.dimensionTablesList)
                    {
                        if (dimensionTable.AutoGenerated)
                        {
                            // dont show autogenerated dim tables on Packages screen
                            continue;
                        }
                        string dimTableConnection = dimensionTable.TableSource.Connection;
                        string dimensionTableLevel = dimensionTable.DimensionName + "." + dimensionTable.Level;
                        if (dimTableConnection != null && realTimeConnectionsList.Contains(dimTableConnection))
                        {
                            liveAccessNonVizDimTables.Add(dimensionTable);
                        }
                    }
                }
            }
            return liveAccessNonVizDimTables;
        }

        private static void validateDiscoveryHierarchies(MainAdminForm maf)
        {
            bool isAllDiscovery = true;
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.Disabled)
                    continue;
                if (!st.DiscoveryTable)
                {
                    isAllDiscovery = false;
                    break;
                }
            }
            HashSet<string> realTimeConnectionsList = getRealTimeConnectionsFromMAF(maf);
            List<DimensionTable> nonVizSourceLiveAccessDimTables = Util.getNonVizLiveAccessDimensionTables(maf, realTimeConnectionsList);
            List<MeasureTable> nonVizSourceLiveAccessMeasureTables = Util.getNonVizLiveAccessMeasureTables(maf, realTimeConnectionsList);
            if (isAllDiscovery && (maf.stagingTableMod.getAllStagingTables().Count == 0 ||
                nonVizSourceLiveAccessDimTables.Count > 0 || nonVizSourceLiveAccessMeasureTables.Count > 0))
            {
                isAllDiscovery = false;
            }

            if (isAllDiscovery)
            {
                List<Hierarchy> dhlist = new List<Hierarchy>();
                foreach (Hierarchy h in maf.hmodule.getHierarchies())
                {
                    if (h.DimensionName == "Time")
                        continue;
                    bool found = false;
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        if (st.Disabled)
                            continue;
                        if (h.DimensionName == ApplicationBuilder.getDisplayName(st))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        dhlist.Add(h);
                }
                foreach (Hierarchy h in dhlist)
                {
                    maf.hmodule.removeHierarchyWithoutRefresh(h);
                }
            }
        }

        public static List<Hierarchy> getImportedHierarchies(MainAdminForm maf, HttpSessionState session)
        {
            // check if its joined to an imported datawarehouse
            List<ImportedRepositoryItem> importedItemsList = Util.getImportedTables(maf, session, null);
            List<Hierarchy> importedHierarchies = new List<Hierarchy>();

            if (importedItemsList != null && importedItemsList.Count > 0)
            {
                foreach (ImportedRepositoryItem it in importedItemsList)
                {
                    if (it.dt == null)
                        continue;
                    Hierarchy h = it.ispace.maf.hmodule.getDimensionHierarchy(it.dt.DimensionName);
                    if (h == null)
                    {
                        continue;
                    }
                    bool found = false;
                    foreach (Hierarchy sh in importedHierarchies)
                    {
                        if (sh.Name == h.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        h.Imported = true;
                        importedHierarchies.Add(h);
                    }
                }
            }
            return importedHierarchies;
        }

        private static void validateGrainsAndKeys(MainAdminForm maf, HttpSessionState session)
        {
            List<Hierarchy> importedHierarchies = getImportedHierarchies(maf, session);
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                /*
                 * Make sure all grain levels and hierarchies exist
                 */
                List<string[]> newlevels = new List<string[]>();
                foreach (string[] stlevel in st.Levels)
                {
                    if (stlevel[0] == "Time")
                    {
                        newlevels.Add(stlevel);
                        continue;
                    }
                    bool valid = true;
                    Hierarchy h = maf.hmodule.getHierarchy(stlevel[0]);
                    if (h == null)
                        valid = false;
                    else
                    {
                        Level l = h.findLevel(stlevel[1]);
                        if (l == null)
                            valid = false;
                    }
                    if (valid)
                        newlevels.Add(stlevel);
                }
                if (newlevels.Count != st.Levels.Length)
                {
                    st.Levels = newlevels.ToArray();
                }
                /*
                 * Make sure all foreign keys are valid
                 */
                if (st.ForeignKeys != null)
                {
                    List<ForeignKey> newfkeys = new List<ForeignKey>();
                    foreach (ForeignKey fk in st.ForeignKeys)
                    {
                        if (fk.Condition != null && fk.Condition.Trim().Length > 0)
                        {
                            // Don't prune complex joins
                            newfkeys.Add(fk);
                            continue;
                        }
                        bool valid = true;
                        StagingTable source = maf.stagingTableMod.findTable(fk.Source);
                        if (source == null)
                        {
                            valid = false;
                            //Not pointing to a staging table - potentially pointing to a warehouse table
                            if ((fk.Source != null) && (fk.Source.Contains(".")))
                            {
                                string[] arr = fk.Source.Split('.');
                                if (arr.Length == 2)
                                {
                                    string dim = arr[0];
                                    string level = arr[1];
                                    List<string> levels = maf.hmodule.getDimensionLevelNames(dim);
                                    if ((levels != null) && (levels.Contains(level)))
                                    {
                                        valid = true;
                                    }
                                    else if (maf.Imports != null && maf.Imports.Length > 0)
                                    {
                                        if (importedHierarchies != null && importedHierarchies.Count > 0)
                                        {
                                            foreach (Hierarchy iHierarchy in importedHierarchies)
                                            {
                                                List<string> iHLevels = maf.hmodule.getDimensionLevelNames(iHierarchy);
                                                if ((iHLevels != null) && (iHLevels.Contains(level)))
                                                {
                                                    valid = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (source.SourceKey == null || source.SourceKey.Length == 0)
                                valid = false;
                            else
                            {
                                bool validkey = true;
                                foreach (string cname in source.SourceKey)
                                {
                                    bool found = false;
                                    foreach (StagingColumn sc in st.Columns)
                                    {
                                        if (sc.Name == cname)
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        validkey = false;
                                        break;
                                    }
                                }
                                if (!validkey)
                                    valid = false;
                            }
                        }
                        if (valid)
                        {
                            newfkeys.Add(fk);
                        }
                    }
                    if (newfkeys.Count != st.ForeignKeys.Length)
                    {
                        st.ForeignKeys = newfkeys.ToArray();
                    }
                }
                /*
                 * Make sure all foreign keys are valid
                 */
                if (st.FactForeignKeys != null)
                {
                    List<ForeignKey> newffkeys = new List<ForeignKey>();
                    foreach (ForeignKey fk in st.FactForeignKeys)
                    {
                        if (fk.Condition != null && fk.Condition.Trim().Length > 0)
                        {
                            // Don't prune complex joins
                            newffkeys.Add(fk);
                            continue;
                        }
                        bool valid = true;
                        StagingTable source = maf.stagingTableMod.findTable(fk.Source);
                        if (source == null)
                            valid = false;
                        else
                        {
                            if (source.SourceKey == null || source.SourceKey.Length == 0)
                                valid = false;
                            else
                            {
                                bool validkey = true;
                                foreach (string cname in source.SourceKey)
                                {
                                    bool found = false;
                                    foreach (StagingColumn sc in st.Columns)
                                    {
                                        if (sc.Name == cname)
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        validkey = false;
                                        break;
                                    }
                                }
                                if (!validkey)
                                    valid = false;
                            }
                        }
                        if (valid)
                        {
                            newffkeys.Add(fk);
                        }
                    }
                    if (newffkeys.Count != st.FactForeignKeys.Length)
                    {
                        st.FactForeignKeys = newffkeys.ToArray();
                    }
                }
            }
        }

        private static void validateHierarchyLevelColumns(MainAdminForm maf)
        {
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    // Make sure column names are mapped
                    if (l.ColumnNames == null)
                        continue;
                    List<string> newlist = new List<string>();
                    for (int i = 0; i < l.ColumnNames.Length; i++)
                    {
                        string s = l.ColumnNames[i];
                        bool ok = false;
                        if (l.HiddenColumns != null && l.HiddenColumns.Length > i && l.HiddenColumns[i])
                            ok = true;
                        else
                        {
                            // Make sure the column is mapped
                            foreach (DimensionTable dt in maf.dimensionTablesList)
                            {
                                if (dt.DimensionName == h.DimensionName)
                                {
                                    DataRowView[] drv = maf.dimensionColumnMappingsView.FindRows(new object[] { s, dt.TableName });
                                    if (drv.Length > 0)
                                    {
                                        ok = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (ok)
                            newlist.Add(s);
                    }
                    // Remove any column in a hierarchy which is not physically mapped (will break drilling)
                    if (l.ColumnNames.Length != newlist.Count)
                        l.ColumnNames = newlist.ToArray();
                }
            }
        }

        private static void validateSourceFileColumns(MainAdminForm maf)
        {
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf == null)
                    continue;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                        continue;
                    bool found = false;
                    foreach (SourceFile.SourceColumn sfc in sf.Columns)
                    {
                        if (sfc.Name == sc.SourceFileColumn)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        // See if there is a column with the proper logical name
                        found = false;
                        foreach (SourceFile.SourceColumn sfc in sf.Columns)
                        {
                            if (sfc.Name == sc.Name)
                            {
                                found = true;
                                sc.SourceFileColumn = sfc.Name;
                                break;
                            }
                        }
                        if (!found)
                        {
                            // Otherwise disable so we don't break the load
                            sc.SourceFileColumn = null;
                        }
                    }
                }
            }
        }

        private static void clearLoadVariables(MainAdminForm maf)
        {
            List<Variable> vlist = new List<Variable>();
            foreach (Variable sv in maf.getVariables())
            {
                if (sv.Name.StartsWith("Load:"))
                {
                    vlist.Add(sv);
                }
            }
            foreach (Variable sv in vlist)
                maf.getVariables().Remove(sv);
        }


        /// <summary>
        /// This is used so that 4.4.x can run concurrently with 4.3.8
        /// Basically, 4.4.x works on schedules which are 1000 year in future and update the schedule accordingly in future also
        /// When looking to pick up the task scheduler will pick up the task on date-1000 years
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime getCustomizedSchedulerTime(DateTime datetime)
        {
            DateTime backToTheFuture = datetime != DateTime.MinValue ? datetime.AddYears(2000) : datetime;
            return backToTheFuture;
        }

        public static DateTime getLowerLimitOfCustomerScheduledTime()
        {
            if (scheduledReportReference == DateTime.MinValue)
            {
                scheduledReportReference = new DateTime(4000, 1, 1);
            }

            return scheduledReportReference;
        }

        private static Variable setLoadVariable(MainAdminForm maf, string schema, string name, string tname, string defval, string connection)
        {
            Variable v = null;
            foreach (Variable sv in maf.getVariables())
            {
                if (sv.Name == "Load:" + name)
                {
                    v = sv;
                    break;
                }
            }
            if (v == null)
            {
                v = new Variable();
                v.Name = "Load:" + name;
                v.Type = Variable.VariableType.Repository;
                v.PhysicalQuery = true;
            }
            v.Query = "SELECT MAX(LOAD_ID) FROM " + schema + "." + tname;
            v.Connection = connection;
            v.DefaultIfInvalid = defval;

            // add or update and save
            maf.updateVariable(v, null);
            return v;
        }

        public static void setLoadIDFilters(MainAdminForm maf, int loadNumber, Dictionary<string, int> sourceGroupLoadNumberMap)
        {
            bool hasSubgroups = false;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.SubGroups != null && st.SubGroups.Length > 0)
                {
                    hasSubgroups = true;
                    break;
                }
            }
            clearLoadVariables(maf);
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                List<TableSource.TableSourceFilter> flist = new List<TableSource.TableSourceFilter>();
                if (mt.TableSource.Filters != null)
                    foreach (TableSource.TableSourceFilter tsf in mt.TableSource.Filters)
                    {
                        if (!tsf.CurrentLoadFilter)
                            flist.Add(tsf);
                    }
                if (mt.AutoGenerated && mt.InheritTable != null && mt.InheritTable.Length > 0 && !mt.Transactional && mt.TimeAlias)
                {
                    int ln = loadNumber;
                    if (sourceGroupLoadNumberMap != null && mt.LoadGroups != null)
                    {
                        foreach (string s in mt.LoadGroups)
                        {
                            if (sourceGroupLoadNumberMap.ContainsKey(s))
                            {
                                ln = sourceGroupLoadNumberMap[s];
                                break;
                            }
                        }
                    }
                    string[] loadFilters = new string[mt.TableSource.Tables.Length];
                    for (int i = 0; i < loadFilters.Length; i++)
                    {
                        loadFilters[i] = ln.ToString();
                    }
                    if (ln > 1)
                    {
                        ApplicationBuilder ab = new ApplicationBuilder(maf);
                        // See if there's a snapshot policy or subgroups
                        foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                        {
                            MeasureGrain mg = new MeasureGrain();
                            mg.measureTableGrains = new MeasureTableGrain[st.Levels.Length];
                            mg.LoadGroups = st.LoadGroups != null && st.LoadGroups.Length == 0 ? null : st.LoadGroups;
                            string connection = null;
                            mg.connection = ab.hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : Database.DEFAULT_CONNECTION;
                            for (int i = 0; i < st.Levels.Length; i++)
                            {
                                mg.measureTableGrains[i] = new MeasureTableGrain();
                                mg.measureTableGrains[i].DimensionName = st.Levels[i][0];
                                mg.measureTableGrains[i].DimensionLevel = st.Levels[i][1];
                            }

                            if (mt.TableSource.Tables[0].PhysicalName == ab.getMeasureTablePhysicalName(ab.getMeasureTableBaseName(mg)))
                            {
                                if (st.Snapshots != null || hasSubgroups)
                                {
                                    string schema = null;
                                    foreach (DatabaseConnection sdc in maf.connection.connectionList)
                                    {
                                        if (sdc.Name == mt.TableSource.Connection)
                                        {
                                            schema = sdc.Schema;
                                        }
                                    }
                                    if (schema == null && mt.TableSource.Schema != null && mt.TableSource.Schema.Length > 0)
                                        schema = mt.TableSource.Schema;
                                    if (schema != null)
                                    {
                                        Variable v = setLoadVariable(maf, schema, ab.getMeasureTableBaseName(mg) + " Fact", mt.TableSource.Tables[0].PhysicalName, loadNumber.ToString(), mg.connection);
                                        if (v != null)
                                            loadFilters[0] = "V{" + v.Name + "}";
                                        if (mt.TableSource.Tables.Length > 1)
                                        {
                                            //If it is a combo fact table, make sure that we have load_id filter for joining tables as well.
                                            for (int i = 1; i < mt.TableSource.Tables.Length; i++)
                                            {
                                                TableSource.TableDefinition table = mt.TableSource.Tables[i];
                                                if (table == null)
                                                {
                                                    continue;
                                                }
                                                string physicalNameForJoiningTable = table.PhysicalName;
                                                string logicalNameForJoiningTable = null;
                                                foreach (MeasureTable mt1 in maf.measureTablesList)
                                                {
                                                    if ((mt1.TableSource != null) && (mt1.TableSource.Tables != null) &&
                                                        (mt1.TableSource.Tables.Length == 1) &&
                                                        (mt1.TableSource.Tables[0] != null) &&
                                                        (mt1.TableSource.Tables[0].PhysicalName == physicalNameForJoiningTable))
                                                    {
                                                        logicalNameForJoiningTable = mt1.TableName;
                                                        break;
                                                    }
                                                }
                                                if (logicalNameForJoiningTable == null || physicalNameForJoiningTable == null)
                                                {
                                                    continue;
                                                }
                                                Variable v1 = setLoadVariable(maf, schema, logicalNameForJoiningTable + " Fact", physicalNameForJoiningTable, loadNumber.ToString(), mg.connection);
                                                if (v1 != null)
                                                    loadFilters[i] = "V{" + v1.Name + "}";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                        tsf.Filter = mt.TableSource.Tables[0].PhysicalName + ".LOAD_ID=" + loadFilters[0];
                        if (loadFilters.Length > 1)
                        {
                            for (int i = 1; i < loadFilters.Length; i++)
                            {
                                tsf.Filter += " AND " + mt.TableSource.Tables[i].PhysicalName + ".LOAD_ID=" + loadFilters[i];
                            }
                        }
                        tsf.CurrentLoadFilter = true;
                        flist.Add(tsf);
                    }
                }
                if (flist.Count == 0)
                    mt.TableSource.Filters = null;
                else
                    mt.TableSource.Filters = flist.ToArray();
            }
        }

        /*
         * Set aggregations for keys, numeric and non-numeric types
         */
        private static void setAggregations(MainAdminForm maf)
        {
            /*
             * Make sure key columns are only counted (same with varchars) and any numeric, float, integers have the appropriate target aggregations
             */
            HashSet<StagingColumn> naturalKeyColumns = new HashSet<StagingColumn>();
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                bool allKeys = true;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (!sc.NaturalKey)
                    {
                        allKeys = false;
                        break;
                    }
                }
                if (!allKeys)
                {
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.NaturalKey && sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0 && sc.DataType != "Float")
                        {
                            // Clear it in all staging tables
                            foreach (StagingTable st2 in maf.stagingTableMod.getAllStagingTables())
                            {
                                foreach (StagingColumn sc2 in st2.Columns)
                                {
                                    if (sc2.Name == sc.Name)
                                    {
                                        sc2.TargetAggregations = new string[] { "COUNT", "COUNT DISTINCT" };
                                        naturalKeyColumns.Add(sc2);
                                    }
                                }
                            }
                        }
                        else if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0 && sc.DataType != null && !naturalKeyColumns.Contains(sc))
                        {
                            if (sc.DataType == "Varchar" || sc.DataType == "DateTime" || sc.DataType == "Date")
                            {
                                sc.TargetAggregations = new string[] { "COUNT", "COUNT DISTINCT", "MIN", "MAX" };
                            }
                            else if (maf.builderVersion >= 9 && (sc.DataType == "Integer" || sc.DataType == "Float" || sc.DataType == "Number"))
                            {
                                sc.TargetAggregations = new string[] { "SUM", "AVG", "COUNT", "COUNT DISTINCT", "MIN", "MAX" };
                            }
                        }
                        else if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") < 0)
                        {
                            sc.TargetAggregations = null;
                        }
                    }
                }
            }
        }

        private static DateTime FIRST_VALID_LOAD_DATE = DateTime.Parse("1/1/2009");

        private static void setupTime(MainAdminForm maf, DateTime loadDate, int loadNumber)
        {
            /* 
             * Add time ids for loaded staging tables
             * Also, add generated time hierarchies
             */
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.LiveAccess)
                    continue;
                // Add time ids for loaded staging tables
                // See if column already exists
                bool exists = false;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.Name == ApplicationBuilder.DayIDName && sc.TargetTypes != null)
                    {
                        if (Array.IndexOf<string>(sc.TargetTypes, maf.timeDefinition.Name) >= 0)
                        {
                            exists = true;
                            break;
                        }
                    }
                }
                List<StagingColumn> sclist = null;
                if (!exists)
                {
                    StagingColumn sc = new StagingColumn();
                    sclist = new List<StagingColumn>(st.Columns);
                    sc.Name = ApplicationBuilder.DayIDName;
                    sc.NaturalKey = true;
                    sc.TargetTypes = new string[] { maf.timeDefinition.Name };
                    sc.DataType = "Integer";
                    sc.Transformations = new Transformation[] {
                            new DateID()};
                    ((DateID)sc.Transformations[0]).DateExpression = "V{LoadDate}";
                    ((DateID)sc.Transformations[0]).DateType = TimePeriod.PERIOD_DAY;
                    sclist.Add(sc);
                    st.Columns = sclist.ToArray();
                    maf.stagingTableMod.updateStagingTable(st, "");
                }
                exists = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == maf.timeDefinition.Name)
                    {
                        level[1] = ApplicationBuilder.DayLevelName;
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    List<string[]> llist = new List<string[]>(st.Levels);
                    llist.Add(new string[] { maf.timeDefinition.Name, ApplicationBuilder.DayLevelName });
                    st.Levels = llist.ToArray();
                    maf.stagingTableMod.updateStagingTable(st, "");
                }
            }
            if (loadNumber <= 0)
                return;
            // Make sure the date variable is in the repository
            Variable dateVar = getDateVariable(maf);
            if (dateVar == null)
            {
                Variable v = new Variable();
                v.Name = "LoadDate";
                v.Type = Variable.VariableType.Repository;
                v.Constant = true;
                dateVar = v;
            }
            Variable dateSFDCVar = getSFDCDateVariable(maf);
            if (dateSFDCVar == null)
            {
                Variable v = new Variable();
                v.Name = "LoadDateSFDC";
                v.Type = Variable.VariableType.Repository;
                v.Constant = true;
                dateSFDCVar = v;
            }
            if (loadNumber >= 0)
            {
                string dbType = getDBTypeForConnection(maf, Database.DEFAULT_CONNECTION);
                if (loadDate == null || loadDate < FIRST_VALID_LOAD_DATE)
                {
                    // Date not valid, if needed (i.e. blank) use system date, else leave as-is
                    if (dateVar.Query == null || dateVar.Query.Length == 0)
                        dateVar.Query = getShortDateString(DateTime.Now, dbType);
                    if (dateSFDCVar.Query == null || dateSFDCVar.Query.Length == 0)
                        dateSFDCVar.Query = getDateTimeInISOFormat(DateTime.Now);
                }
                else
                {
                    dateVar.Query = getShortDateString(loadDate, dbType);
                    dateSFDCVar.Query = getDateTimeInISOFormat(loadDate);
                }
            }
            dateVar.BirstCanModify = false;
            dateSFDCVar.BirstCanModify = false;
            // add or update and save
            maf.updateVariable(dateVar, null);
            maf.updateVariable(dateSFDCVar, null);

            // Make sure the load number is in the repository
            Variable loadNum = getLoadVariable(maf);
            if (loadNum == null)
            {
                Variable v = new Variable();
                v.Name = "LoadNumber";
                v.Type = Variable.VariableType.Repository;
                v.Constant = true;
                loadNum = v;
            }
            loadNum.BirstCanModify = false;
            if (loadNumber >= 0)
                loadNum.Query = loadNumber.ToString();
            // add or update and save
            maf.updateVariable(loadNum, null);
        }


        public static Variable getDateVariable(MainAdminForm maf)
        {
            Variable dateVar = null;
            foreach (Variable v in maf.getVariables())
            {
                if (v.Name == "LoadDate")
                {
                    dateVar = v;
                }
            }
            return (dateVar);
        }

        public static Variable getSFDCDateVariable(MainAdminForm maf)
        {
            Variable dateVar = null;
            foreach (Variable v in maf.getVariables())
            {
                if (v.Name == "LoadDateSFDC")
                {
                    dateVar = v;
                }
            }
            return (dateVar);
        }

        public static Variable getLoadVariable(MainAdminForm maf)
        {
            Variable loadNum = null;
            foreach (Variable v in maf.getVariables())
            {
                if (v.Name == "LoadNumber")
                {
                    loadNum = v;
                }
            }
            return (loadNum);
        }

        public static void cleanApplication(Performance_Optimizer_Administration.MainAdminForm maf)
        {
            maf.cleanApp();
        }

        private static void setPrivileges(Space sp, int accountType)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> spaceGroupList = Database.getSpaceGroups(conn, schema, sp);

                if (spaceGroupList != null && spaceGroupList.Count > 0)
                {
                    foreach (SpaceGroup spg in spaceGroupList)
                    {
                        if (spg.Name == OWNER_GROUP_NAME && spg.InternalGroup)
                        {
                            List<int> ownerAclIds = new List<int>();
                            foreach (string ownerPrivilege in User.OWNER_ACCESS_PRIVILIGES[accountType])
                            {
                                int ownerAclId = ACLDetails.getACLID(ownerPrivilege);
                                Database.addACLToGroup(conn, schema, sp, spg.ID, ownerAclId);
                            }
                        }

                        if (spg.Name == USER_GROUP_NAME && spg.InternalGroup)
                        {
                            List<int> userAclIds = new List<int>();
                            foreach (string userPrivilege in User.USER_ACCESS_PRIVILIGES[accountType])
                            {
                                int userAclId = ACLDetails.getACLID(userPrivilege);
                                Database.addACLToGroup(conn, schema, sp, spg.ID, userAclId);
                            }
                        }
                    }
                }
                else
                {
                    Global.systemLog.Warn("No space groups found in the db. Cannot set privileges for the account type " + accountType + "  and space " + sp.ID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting account privileges" + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setGroupPrivileges(Space sp, string ownerUsername, int accountType)
        {
            setGroupPrivileges(sp, ownerUsername, accountType, true);
        }

        public static void setGroupPrivileges(Space sp, string ownerUsername, int accountType, bool updateOwnerGroupMembershipIfRequired)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, schema, ownerUsername);
                List<SpaceGroup> spaceGroupList = Database.getSpaceGroups(conn, schema, sp);
                SpaceGroup ownerGroup = null;
                if (spaceGroupList != null && spaceGroupList.Count > 0)
                {
                    // Find the owner group
                    foreach (SpaceGroup g in spaceGroupList)
                    {
                        if (g.Name == OWNER_GROUP_NAME)
                        {
                            ownerGroup = g;
                            break;
                        }
                    }
                }

                if (ownerGroup == null)
                {
                    // Create one
                    ownerGroup = new SpaceGroup();
                    ownerGroup.InternalGroup = true;
                    ownerGroup.Name = OWNER_GROUP_NAME;
                    ownerGroup.ID = Database.addGroupToSpace(conn, schema, sp, ownerGroup.Name, ownerGroup.InternalGroup);
                }

                if (ownerUsername != null && updateOwnerGroupMembershipIfRequired)
                {
                    bool hasUser = false;
                    if (ownerGroup.GroupUsers != null && ownerGroup.GroupUsers.Count > 0)
                    {
                        hasUser = UserAndGroupUtils.isUserGroupMember(ownerGroup, u.ID);
                    }

                    if (!hasUser)
                    {
                        Database.addUserToGroup(conn, schema, sp, ownerGroup.ID, u.ID, true);
                    }
                }


                // Find the user group
                SpaceGroup userGroup = null;
                if (spaceGroupList != null && spaceGroupList.Count > 0)
                {
                    foreach (SpaceGroup g in spaceGroupList)
                    {
                        if (g.Name == USER_GROUP_NAME && g.InternalGroup)
                        {
                            userGroup = g;
                            break;
                        }
                    }
                }
                // Create user group if doesn't exist
                if (userGroup == null)
                {
                    userGroup = new SpaceGroup();
                    userGroup.InternalGroup = true;
                    userGroup.Name = USER_GROUP_NAME;
                    userGroup.ID = Database.addGroupToSpace(conn, schema, sp, userGroup.Name, userGroup.InternalGroup);
                }

                setPrivileges(sp, accountType);

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting group privileges" + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }


        /*
         * add the default space parameters (server parameters in the repository) or update them from the SPACES table
         */
        private static void setupSpaceParameters(Space sp, MainAdminForm maf, HttpSessionState session, out bool changed)
        {
            changed = false;

            if (maf.serverPropertiesModule.appPath.Text != sp.Directory)
            {
                maf.serverPropertiesModule.appPath.Text = sp.Directory;
                changed = true;
            }
            string newCachePath = sp.Directory + "\\cache";
            if (maf.serverPropertiesModule.cachePath.Text != newCachePath)
            {
                maf.serverPropertiesModule.cachePath.Text = newCachePath;
                changed = true;
            }
            String temp = sp.MaxQueryRows.ToString();
            if (maf.serverPropertiesModule.maxRecords.Text != temp)
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.maxRecords.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.maxRecords.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }
            temp = sp.MaxQueryTimeout.ToString();
            if (maf.serverPropertiesModule.maxQueryTimeTextBox.Text != temp)
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.maxQueryTimeTextBox.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.maxQueryTimeTextBox.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }

            bool mapNullsToZero = maf.serverPropertiesModule.mapNullsToZeroCheckBox.Checked;
            if (mapNullsToZero != sp.MapNullsToZero) // null means false
            {
                maf.serverPropertiesModule.mapNullsToZeroCheckBox.Checked = sp.MapNullsToZero;
                changed = true;
            }

            if (maf.QueryLanguageVersionText.Text != sp.QueryLanguageVersion.ToString())
            {
                maf.QueryLanguageVersionText.Text = sp.QueryLanguageVersion.ToString();
                changed = true;
            }
            if (session != null && changed)
            {
                session["AdminDirty"] = true;
            }
        }

        public static int getQueryLanguageVersion(MainAdminForm maf)
        {
            int version = 1;
            if (!Int32.TryParse(maf.QueryLanguageVersionText.Text, out version))
            {
                version = 1;
            }
            return version;
        }

        /*
         * add the default user space parameters (server parameters in the repository) or update them from the SPACES table
         */
        private static void setupUserSpaceParameters(Space sp, User u, MainAdminForm maf, HttpSessionState session)
        {
            if (u == null)
                return;

            bool changed = false;
            string temp = u.MaxScriptStatements.ToString();
            if (maf.serverPropertiesModule.maxScriptStatementsBox.Text != null && !maf.serverPropertiesModule.maxScriptStatementsBox.Text.Equals(temp))
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.maxScriptStatementsBox.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.maxScriptStatementsBox.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }
            temp = u.MaxInputRows.ToString();
            if (maf.serverPropertiesModule.maxInputRowsBox.Text != null && !maf.serverPropertiesModule.maxInputRowsBox.Text.Equals(temp))
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.maxInputRowsBox.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.maxInputRowsBox.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }
            temp = u.MaxOutputRows.ToString();
            if (maf.serverPropertiesModule.maxOutputRowsBox.Text != null && !maf.serverPropertiesModule.maxOutputRowsBox.Text.Equals(temp))
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.maxOutputRowsBox.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.maxOutputRowsBox.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }
            temp = u.ScriptQueryTimeout.ToString();
            if (maf.serverPropertiesModule.scriptQueryTimeoutBox.Text != null && !maf.serverPropertiesModule.scriptQueryTimeoutBox.Text.Equals(temp))
            {
                try
                {
                    // only change this value if increasing it, hack for now XXX
                    Int64 new64 = Int64.Parse(temp);
                    Int64 old64 = Int64.Parse(maf.serverPropertiesModule.scriptQueryTimeoutBox.Text);
                    if (new64 > old64)
                    {
                        maf.serverPropertiesModule.scriptQueryTimeoutBox.Text = temp;
                        changed = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.Message, ex);
                }
            }
            if (session != null && changed)
            {
                session["AdminDirty"] = true;
            }
        }

        /*
         * add a default connection if it does not exist or update it (using the information from the SPACES table)
         */
        private static Performance_Optimizer_Administration.DatabaseConnection setupDefaultConnection(Space sp, MainAdminForm maf, HttpSessionState session, out bool changed)
        {
            changed = false;
            Performance_Optimizer_Administration.DatabaseConnection dc = null;

            if (maf == null)
                return null;

            if (maf.connection.connectionList.Count > 0)
                dc = maf.connection.connectionList[0];
            else
            {
                dc = new Performance_Optimizer_Administration.DatabaseConnection();
                maf.connection.connectionList.Add(dc);
                changed = true;
            }
            dc.ConnectionPoolSize = 2;
            dc.MaxConnectionThreads = 1;
            dc.Name = Database.DEFAULT_CONNECTION;
            string newSchema = sp == null ? mainSchema : sp.Schema;
            if (newSchema != dc.Schema)
            {
                dc.Schema = newSchema;
                changed = true;
            }
            if (dc.Type != sp.DatabaseType)
            {
                dc.Type = sp.DatabaseType;
                changed = true;
            }
            if (dc.ConnectString != sp.ConnectString)
            {
                int index1 = sp.ConnectString.IndexOf("//");
                int index2 = index1 > 0 ? sp.ConnectString.IndexOf('/', index1 + 2) : -1;
                if (dc.Type == "MemDB" && index2 > 0)
                    dc.ConnectString = sp.ConnectString.Substring(0, index2);
                else
                    dc.ConnectString = sp.ConnectString;
                changed = true;
            }
            if (dc.DBTimeZone != sp.dbTimeZone)
            {
                dc.DBTimeZone = sp.dbTimeZone;
                changed = true;
            }
            if (dc.Driver != sp.DatabaseDriver)
            {
                dc.Driver = sp.DatabaseDriver;
                changed = true;
            }
            if (dc.UserName != sp.AdminUser)
            {
                dc.UserName = sp.AdminUser;
                changed = true;
            }
            string encPW = Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            if (dc.Password != encPW)
            {
                dc.Password = encPW;
                changed = true;
            }
            /* this really should have been another space type */
            if (sp.QueryConnectString != null && sp.QueryConnectString.Length > 0)
            {
                Performance_Optimizer_Administration.DatabaseConnection qdc = null;
                foreach (DatabaseConnection sdc in maf.connection.connectionList)
                {
                    if (sdc.Name == sp.QueryConnectionName)
                    {
                        qdc = sdc;
                        break;
                    }
                }
                if (qdc == null)
                {
                    qdc = new Performance_Optimizer_Administration.DatabaseConnection();
                    maf.connection.connectionList.Add(qdc);
                    changed = true;
                }
                qdc.ConnectionPoolSize = 2;
                qdc.MaxConnectionThreads = 1;
                if (qdc.Name != sp.QueryConnectionName)
                {
                    qdc.Name = sp.QueryConnectionName;
                    changed = true;
                }
                newSchema = sp == null ? mainSchema : sp.Schema;
                if (newSchema != qdc.Schema)
                {
                    qdc.Schema = newSchema;
                    changed = true;
                }
                if (qdc.ConnectString != sp.QueryConnectString)
                {
                    int index1 = sp.QueryConnectString.IndexOf("//");
                    int index2 = index1 > 0 ? sp.QueryConnectString.IndexOf('/', index1 + 2) : -1;
                    if (qdc.Type == "MemDB" && index2 > 0)
                        qdc.ConnectString = sp.QueryConnectString.Substring(0, index2);
                    else
                        qdc.ConnectString = sp.QueryConnectString;
                    changed = true;
                }
                if (qdc.Type != sp.QueryDatabaseType)
                {
                    qdc.Type = sp.QueryDatabaseType;
                    changed = true;
                }
                if (qdc.Driver != sp.QueryDatabaseDriver)
                {
                    qdc.Driver = sp.QueryDatabaseDriver;
                    changed = true;
                }
                if (qdc.UserName != sp.QueryUser)
                {
                    qdc.UserName = sp.QueryUser;
                    changed = true;
                }
                encPW = Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.QueryPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                if (qdc.Password != encPW)
                {
                    qdc.Password = encPW;
                    changed = true;
                }
                if (qdc.Name != Database.MEM_AGG_CONNECTION)
                {
                    if (maf.defaultConnection != qdc.Name)
                    {
                        maf.defaultConnection = qdc.Name;
                        changed = true;
                    }
                }
            }
            if (session != null && changed)
            {
                session["AdminDirty"] = true;
            }
            return dc;
        }


        public static void updateStagingTable(MainAdminForm maf, StagingTable st, SourceFile sf, string username, bool modelAndPositionTables)
        {
            if (sf != null)
            {
                if (st.SourceKey == null && sf != null && sf.KeyIndices != null && sf.KeyIndices.Length > 0)
                {
                    List<string> keys = new List<string>();
                    // Populate source file indices to match
                    for (int i = 0; i < sf.KeyIndices.Length; i++)
                    {
                        string name = sf.Columns[i].Name;

                        foreach (StagingColumn ssc in st.Columns)
                        {
                            if (ssc.SourceFileColumn == name)
                            {
                                keys.Add(ssc.Name);
                                break;
                            }
                        }
                    }
                    if (keys.Count == sf.KeyIndices.Length)
                    {
                        st.SourceKey = keys.ToArray();
                    }
                }
            }
            if (st.DiscoveryTable && modelAndPositionTables)
            {
                /*
                 * For new sources, connect any joins that should be defaulted based on logical names
                 */
                foreach (StagingTable st2 in maf.stagingTableMod.getStagingTables())
                {
                    if (st2.Name == st.Name)
                        continue;
                    /*
                     * See if there is a foreign key relationship
                     */
                    if (st.SourceKey != null)
                    {
                        bool match = true;
                        foreach (string key in st.SourceKey)
                        {
                            bool found = false;
                            foreach (StagingColumn sc in st2.Columns)
                            {
                                if (sc.Name == key)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                match = false;
                                break;
                            }
                        }
                        if (match)
                        {
                            List<ForeignKey> newKeys = null;
                            if (st2.ForeignKeys != null)
                                newKeys = new List<ForeignKey>(st2.ForeignKeys);
                            else
                                newKeys = new List<ForeignKey>();
                            bool dupe = false;
                            foreach (ForeignKey sfk in newKeys)
                            {
                                if (sfk.Source == st.Name)
                                {
                                    dupe = true;
                                    break;
                                }
                            }
                            if (!dupe)
                            {
                                ForeignKey fk = new ForeignKey();
                                fk.Source = st.Name;
                                fk.Type = ForeignKey.INNER_JOIN;
                                newKeys.Add(fk);
                                st2.ForeignKeys = newKeys.ToArray();
                            }
                        }
                    }
                    if (st2.SourceKey != null)
                    {
                        bool match = true;
                        foreach (string key in st2.SourceKey)
                        {
                            bool found = false;
                            foreach (StagingColumn sc in st.Columns)
                            {
                                if (sc.Name == key)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                match = false;
                                break;
                            }
                        }
                        if (match)
                        {
                            List<ForeignKey> newKeys = null;
                            if (st.ForeignKeys != null)
                                newKeys = new List<ForeignKey>(st.ForeignKeys);
                            else
                                newKeys = new List<ForeignKey>();
                            bool dupe = false;
                            foreach (ForeignKey sfk in newKeys)
                            {
                                if (sfk.Source == st2.Name)
                                {
                                    dupe = true;
                                    break;
                                }
                            }
                            if (!dupe)
                            {
                                ForeignKey fk = new ForeignKey();
                                fk.Source = st2.Name;
                                fk.Type = ForeignKey.INNER_JOIN;
                                newKeys.Add(fk);
                                st.ForeignKeys = newKeys.ToArray();
                            }
                        }
                    }
                }
                positionDiscoveryTables(maf);
            }
            // update HierarchyName & LevelName correctly
            // find the source keys. Then find the columns in the source and get the targetted dimension. 
            // That dimension should be set for st.hierarchyName. For level, go through staging table grain and find the correct level name
            if (st.SourceKey != null && st.SourceKey.Length > 0 && st != null && st.Columns != null && st.Columns.Length > 0)
            {
                string hName = null;
                string lName = null;

                // Get the hierarchy first of the targetted source key.
                // All the source key columns should be targetted to the same dim. If not, skip it.
                bool noMismatch = true;
                foreach (string sourceKey in st.SourceKey)
                {
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.Name == sourceKey)
                        {
                            if (sc.TargetTypes != null && sc.TargetTypes.Length > 0)
                            {
                                foreach (string ttype in sc.TargetTypes)
                                {
                                    if (ttype != "Measure" && ttype != "Time")
                                    {
                                        // see if ttype matches with the other source keys. They should be same
                                        if (hName == null)
                                        {
                                            hName = ttype;
                                        }

                                        noMismatch = hName == ttype;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    if (!noMismatch)
                    {
                        break;
                    }
                }
                if (hName != null && noMismatch && st.Levels != null && st.Levels.Length > 0)
                {
                    // Get the level which is part of the grain for this source
                    foreach (string[] stLevel in st.Levels)
                    {
                        if (stLevel[0] == hName)
                        {
                            lName = stLevel[1];
                            break;
                        }
                    }

                    if (hName != null && lName != null && hName.Trim().Length > 0 && lName.Trim().Length > 0)
                    {
                        st.HierarchyName = hName;
                        st.LevelName = lName;
                    }
                }
            }
            maf.stagingTableMod.updateStagingTable(st, username);
        }

        private static int X_SPACE = 190;
        private static int Y_SPACE = 60;
        private static int SNAP_TO = 10;
        private static int Y_MARGIN = 30;

        private static List<StagingTable> getStagingConnections(MainAdminForm maf, StagingTable st)
        {
            List<StagingTable> result = new List<StagingTable>();
            if (st.ForeignKeys != null)
            {
                foreach (ForeignKey fk in st.ForeignKeys)
                {
                    StagingTable sst = maf.stagingTableMod.findTable(fk.Source);
                    if (sst != null)
                        result.Add(sst);
                }
            }
            foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
            {
                if (!sst.DiscoveryTable || sst.ForeignKeys == null)
                    continue;
                foreach (ForeignKey fk in sst.ForeignKeys)
                {
                    if (fk.Source == st.Name)
                        result.Add(sst);
                }
            }
            return result;
        }

        private static void positionDiscoveryTables(MainAdminForm maf)
        {
            // Start with most connected tables and work outward
            List<StagingTable>[] numConnections = new List<StagingTable>[100];
            int max = 0;
            List<StagingTable> nonPlacedTables = new List<StagingTable>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (!st.DiscoveryTable || !st.Autoplace)
                {
                    nonPlacedTables.Add(st);
                    continue;
                }
                List<StagingTable> connections = getStagingConnections(maf, st);
                int num = connections.Count;
                if (num < numConnections.Length)
                {
                    if (numConnections[num] == null)
                        numConnections[num] = new List<StagingTable>();
                    numConnections[num].Add(st);
                    if (num > max)
                        max = num;
                }
            }
            List<StagingTable> tablesToPlace = new List<StagingTable>();
            for (int num = max; num > 0; num--)
            {
                if (numConnections[num] == null)
                    continue;
                foreach (StagingTable st in numConnections[num])
                    tablesToPlace.Add(st);
            }
            List<StagingTable> tablesPlaced = new List<StagingTable>();
            while (tablesToPlace.Count > 0)
            {
                StagingTable st = tablesToPlace[0];
                tablesToPlace.Remove(st);
                List<StagingTable> connections = getStagingConnections(maf, st);
                List<StagingTable> centerTables = new List<StagingTable>();
                foreach (StagingTable cst in connections)
                {
                    if (tablesPlaced.Contains(cst))
                    {
                        centerTables.Add(cst);
                    }
                }
                if (centerTables.Count == 0)
                    centerTables = tablesPlaced;
                Location center = getCenterLocation(centerTables, st);
                List<Location> llist = new List<Location>();
                if (tablesPlaced.Count == 0)
                    llist.Add(center);
                else if (tablesPlaced.Count == 1)
                {
                    Location l = new Location();
                    l.LoadGroupName = "All";
                    l.x = center.x + X_SPACE;
                    l.y = center.y;
                    llist.Add(l);
                }
                else
                {
                    List<StagingTable> positionedTables = new List<StagingTable>(tablesPlaced);
                    positionedTables.AddRange(nonPlacedTables);
                    llist.Add(findClosestLocation(st, center, positionedTables));
                }
                if (st.Locations != null)
                {
                    foreach (Location l in st.Locations)
                    {
                        if (l.LoadGroupName != "All")
                            llist.Add(l);
                    }
                }
                st.Locations = llist.ToArray();
                tablesPlaced.Add(st);
            }
            /*
             * Make sure all locations are positive
             */
            int minx = Int32.MaxValue;
            int miny = Int32.MaxValue;
            foreach (StagingTable st in tablesPlaced)
            {
                if (st.Locations == null)
                    continue;
                foreach (Location l in st.Locations)
                {
                    if (l.LoadGroupName != "All")
                        continue;
                    if (l.x < minx)
                        minx = l.x;
                    if (l.y < miny)
                        miny = l.y;
                }
            }
            minx -= X_SPACE;
            miny -= Y_MARGIN;
            foreach (StagingTable st in tablesPlaced)
            {
                if (st.Locations == null)
                    continue;
                foreach (Location l in st.Locations)
                {
                    if (l.LoadGroupName != "All")
                        continue;
                    l.x -= minx;
                    l.y -= miny;
                }
            }
            /*
             * Now place all the ones that haven't been placed yet
             */
            int cury = 0;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (!st.DiscoveryTable || !st.Autoplace)
                    continue;
                if (tablesPlaced.Contains(st))
                    continue;
                Location newl = new Location();
                newl.LoadGroupName = "All";
                newl.x = 0;
                newl.y = cury;
                cury += Y_SPACE;
                List<Location> llist = new List<Location>();
                llist.Add(newl);
                if (st.Locations != null)
                {
                    foreach (Location l in st.Locations)
                    {
                        if (l.LoadGroupName != "All")
                            llist.Add(l);
                    }
                }
                st.Locations = llist.ToArray();
            }
        }

        private static bool isConnected(StagingTable st, StagingTable newTable)
        {
            bool connected = false;
            if (st.ForeignKeys != null)
            {
                foreach (ForeignKey fk in st.ForeignKeys)
                {
                    if (fk.Source == newTable.Name)
                    {
                        connected = true;
                        break;
                    }
                }
            }
            if (!connected && newTable.ForeignKeys != null)
            {
                foreach (ForeignKey fk in newTable.ForeignKeys)
                {
                    if (fk.Source == st.Name)
                    {
                        connected = true;
                        break;
                    }
                }
            }
            return connected;
        }

        private static Location getCenterLocation(List<StagingTable> list, StagingTable newTable)
        {
            Location l = new Location();
            l.LoadGroupName = "All";
            if (list.Count == 0)
            {
                l.x = 0;
                l.y = 0;
                return l;
            }
            int sumx = 0;
            int sumy = 0;
            int count = 0;
            int allsumx = 0;
            int allsumy = 0;
            int allcount = 0;
            foreach (StagingTable st in list)
            {
                if (st.Locations == null)
                    continue;
                bool connected = isConnected(st, newTable);
                foreach (Location l2 in st.Locations)
                {
                    if (l2.LoadGroupName != "All")
                        continue;
                    if (connected)
                    {
                        sumx += l2.x;
                        sumy += l2.y;
                        count++;
                    }
                    allsumx += l2.x;
                    allsumy += l2.y;
                    allcount++;
                }
            }
            if (count == 0)
            {
                if (allcount == 0)
                {
                    l.x = 0;
                    l.y = 0;
                    return l;
                }
                l.x = allsumx / allcount;
                l.y = allsumy / allcount;
                return l;
            }
            l.x = sumx / count;
            l.y = sumy / count;
            return l;
        }

        private static Location findClosestLocation(StagingTable table, Location center, List<StagingTable> list)
        {
            Location l = new Location();
            l.LoadGroupName = "All";
            if (list.Count == 0)
            {
                l.x = 0;
                l.y = 0;
                return l;
            }
            double degree = 0;
            double curdist = Double.MinValue;
            Location curLocation = null;
            for (double curdegree = 0; curdegree < 2 * Math.PI; curdegree += Math.PI / 16)
            {
                long x = center.x + (int)(100000d * Math.Cos(curdegree));
                long y = center.y + (int)(100000d * Math.Sin(curdegree));
                long dist = long.MaxValue;
                Location loc = null; ;
                foreach (StagingTable st in list)
                {
                    if (st.Locations == null)
                        continue;
                    foreach (Location sl in st.Locations)
                    {
                        if (sl.LoadGroupName != "All")
                            continue;
                        long hdist = sl.x - x;
                        long vdist = sl.y - y;
                        bool connected = isConnected(table, st);
                        long sdist = (hdist * hdist) + (vdist * vdist);
                        if (!connected)
                            sdist *= 3 / 2;
                        if (sdist < dist)
                        {
                            dist = sdist;
                            loc = sl;
                        }
                    }
                }
                if (dist > curdist)
                {
                    degree = curdegree;
                    curdist = dist;
                    curLocation = loc;
                }
            }
            l.x = curLocation.x + (int)(X_SPACE * Math.Cos(degree));
            l.y = curLocation.y + (int)(Y_SPACE * Math.Sin(degree));
            l.x = (l.x / SNAP_TO) * SNAP_TO;
            l.y = (l.y / SNAP_TO) * SNAP_TO;
            return l;
        }

        public static MainAdminForm getRepositoryDev(Space sp, User u)
        {
            MainAdminForm maf = new MainAdminForm();
            maf.timeDefinition = null;
            maf.setHeadless(true);
            maf.setLogger(Global.systemLog);
            maf.setRepositoryDirectory(sp.Directory);
            string fileName = Path.Combine(sp.Directory, "repository_dev.xml");
            if (File.Exists(fileName))
            {
                maf.loadRepositoryFromFile(fileName);
                maf.setupTree();
            }
            else
            {
                throw new Exception("Repository file does not exist.");
            }
            return maf;
        }

        public static void saveApplication(Performance_Optimizer_Administration.MainAdminForm maf, Space sp, HttpSessionState session, User u)
        {
            if (session != null && u == null)
            {
                u = (User)session["user"];
            }
            maf.appName.Text = sp.Name + "/" + u.Username;
            setupUserSpaceParameters(sp, u, maf, session);
            saveApplicationWithoutOwner(maf, sp, session);
            PackageUtils.synchCreatedPackagesWithRepository(session, maf, sp);
        }

        public static void saveApplicationWithoutOwner(MainAdminForm maf, Space sp, HttpSessionState session)
        {
            saveApplicationWithoutOwner(maf, sp, session, null);
        }

        public static void saveApplicationWithoutOwner(MainAdminForm maf, Space sp, HttpSessionState session, string fileName)
        {
            // update requires publish flag if reset by a background process like birst connect scheduled processing or external scheduler
            maf.refreshParams();
            bool previousUberTransaction = maf.isInUberTransaction();
            try
            {
                maf.setInUberTransaction(true);
                // check consistency
                checkConsistency(maf, session);

                // create/update the default connection
                bool changedDCDev = false;
                Performance_Optimizer_Administration.DatabaseConnection dc = setupDefaultConnection(sp, maf, session, out changedDCDev);

                // see if we need to update the repository with new server parameters (query language, timeout, max rows, etc)
                bool changedSpaceParamsDev = false;
                setupSpaceParameters(sp, maf, session, out changedSpaceParamsDev);

                bool unicode = false;
                foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                {
                    if (sf.Encoding != null && (sf.Encoding.ToLower() != "iso-8859-1" ||
                        sf.Encoding.ToLower() == "ansi" || sf.Encoding.ToLower() == "us-ascii"))
                    {
                        unicode = true;
                        break;
                    }
                }
                dc.UnicodeDatabase = unicode;

                // Setup directories (XXX this is no longer needed, setupSpaceParameters does this)
                maf.serverPropertiesModule.appPath.Text = sp.Directory;
                maf.serverPropertiesModule.cachePath.Text = Path.Combine(sp.Directory, "cache");

                // Add load group
                addLoadGroup(maf, LOAD_GROUP_NAME);

                // Make sure output portion of scripts are set even if source is disabled so iterating over all staging tables
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (st.Script != null)
                        ManageSources.setOutputString(st, maf);
                }

                // Add script group
                addScriptGroup(maf, LOAD_GROUP_NAME);

                Global.systemLog.Info("repository_dev.xml being updated for space " + sp.Name);

                string repProdFileName = Path.Combine(sp.Directory, Util.REPOSITORY_PROD);
                string repDevFileName = Path.Combine(sp.Directory, Util.REPOSITORY_DEV);

                if (fileName != null)
                {
                    maf.saveRepository(fileName); // XXX hack for time repository
                    // if its time repository save and return
                    return;
                }
                else
                    maf.saveRepository(repDevFileName);

                // Copy to production if possible
                if (!maf.RequiresPublish)
                {

                    updatePackageSpacesInfoForSMIWeb(maf, sp);
                    Util.FileCopy(repDevFileName, repProdFileName, true);
                    if (session != null)
                        session["requiresLogout"] = true;

                }
                else
                {
                    Global.systemLog.Info("repository_dev.xml not copied to repository.xml due to 'requirespublish' for space " + sp.Name + ", however the space and database settings have been updated");
                    bool newRepository = false;
                    // load up repository.xml
                    MainAdminForm mafrep = loadRepository(sp, out newRepository, Util.REPOSITORY_PROD);
                    if (!newRepository)
                    {
                        // update the database connection information
                        bool changedDCProd = false;
                        Performance_Optimizer_Administration.DatabaseConnection dc2 = setupDefaultConnection(sp, mafrep, session, out changedDCProd);
                        // update the space directory settings
                        bool changedSpaceParamsProd = false;
                        setupSpaceParameters(sp, mafrep, session, out changedSpaceParamsProd);
                        // save out repository.xml
                        if (changedDCProd || changedSpaceParamsProd)
                        {
                            mafrep.saveRepository(repProdFileName);
                            if (session != null)
                                session["requiresLogout"] = true;
                        }
                    }
                }
                // Make a copy based on the load number
                Util.FileCopy(repDevFileName, Path.Combine(sp.Directory, "repository_dev.xml." + sp.LoadNumber + ".save"), true);
            }
            finally
            {
                maf.setInUberTransaction(previousUberTransaction);
            }
        }

        /// <summary>
        /// Update the RequiresPublish flag and tracks the last modified time this flag was changed
        /// Useful to determine if the flag was somehow changed by any background processing 
        /// like scheduler which needs to be updated in the memory maf object later on in Util.save()
        /// </summary>
        /// <param name="maf"></param>
        /// <param name="value"></param>
        public static void updateRequiresPublishFlag(MainAdminForm maf, bool value)
        {
            maf.RequiresPublish = value;
            maf.updatedParams("RequiresPublish");
        }

        public static void revertApplication(HttpSessionState session, Space sp)
        {
            Global.systemLog.Info("repository_dev.xml being overwritten (reverted) with repository.xml for space " + sp.Name);

            string repProdFileName = Path.Combine(sp.Directory, Util.REPOSITORY_PROD);
            string repDevFileName = Path.Combine(sp.Directory, Util.REPOSITORY_DEV);

            Util.FileCopy(repProdFileName, repDevFileName, true);
            setSpace(session, sp, null);
        }

        /*
         * failsafe - keep a compressed (gzip) copy of the metadata just in case...
         * - called when entering the Birst Admin UI
         * - should also call when scheduled processing is called since that can update the repository
         */
        static readonly DateTime StartOfEpoch = new DateTime(1970, 1, 1, 0, 0, 0);
        static readonly string BackupDir = "backup";
        static readonly string[] backupFileNames = new string[] {Util.REPOSITORY_DEV, "Packages.xml", "DashboardStyleSettings.xml", 
            "SavedExpressions.xml", "sforce.xml"};
        static readonly Dictionary<string, string> backupSubDirectories = new Dictionary<string, string>(){            
            {Util.DCCONFIG_DIR, "*.xml"},
            {"custom-subject-areas", "subjectarea*.xml"},
            {"DashboardTemplates", "*.style"}
        };

        public static void backupMetadata(Space sp, User u)
        {
            if (sp == null || u == null)
            {
                Global.systemLog.Error("backupUpMetadata: user/space is null - this should never happen");
                return;
            }
            try
            {
                TimeSpan t = (DateTime.UtcNow - StartOfEpoch);
                long delta = (long)t.TotalMilliseconds; // milliseconds since the epoch
                string backupdir = Path.Combine(sp.Directory, BackupDir);
                DirectoryInfo di = new DirectoryInfo(backupdir);
                if (!di.Exists)
                    di.Create();

                // cleanup old files first
                Util.cleanupOldFiles(sp);

                foreach (string backupFileName in backupFileNames)
                {
                    backupFile(sp.Directory, di.FullName, backupFileName, u, delta);
                }

                foreach (KeyValuePair<string, string> backupSubDir in backupSubDirectories)
                {
                    string backupSubDirName = backupSubDir.Key;
                    string filePatternToBackup = backupSubDir.Value;
                    backupDirectory(sp.Directory, di.FullName, backupSubDirName, filePatternToBackup, u, delta);
                }
            }
            catch (Exception ex)
            {
                // can not let any exception stop from going into the admin screen
                Global.systemLog.Debug(ex, ex);
            }
        }

        /// <summary>
        /// Updated SMIWeb with new imported packages and spaces properties via trusted service.
        /// </summary>
        /// <param name="maf"></param>
        /// <param name="sp"></param>
        public static void updatePackageSpacesInfoForSMIWeb(MainAdminForm maf, Space sp)
        {
            if (maf == null || maf.RequiresPublish)
                return;
            // basically looks in the maf.imports (which is most updated information when package is removed/added)
            try
            {
                string[] packageSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
                // 1 Minute timeout
                string localprotocol = null;
                if (MOVE_LOCAL_PROTOCOL != null)
                    localprotocol = MOVE_LOCAL_PROTOCOL;
                else
                    localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                ts.Timeout = 1 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                ts.updatePackageSpacesInfo(sp.Directory, packageSpacesInfoArray);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error in updating SMIWeb with updated package spaces information", ex);
            }
        }

        private static void backupDirectory(string currentDirectory, string backupDirectory, string dirName, string filter, User u, long delta)
        {
            try
            {
                DirectoryInfo dirI = new DirectoryInfo(Path.Combine(currentDirectory, dirName));
                if (dirI.Exists)
                {
                    FileInfo[] files = dirI.GetFiles(filter);
                    if (files != null)
                    {
                        DirectoryInfo backup = new DirectoryInfo(Path.Combine(backupDirectory, dirName));
                        if (!backup.Exists)
                            backup.Create();
                        foreach (FileInfo fi in files)
                        {
                            backupFile(dirI.FullName, backup.FullName, fi.Name, u, delta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // can not let any exception stop from going into the admin screen
                Global.systemLog.Debug("Failed to create backup directory - " + dirName + ": " + ex.Message);
            }
        }

        private static void backupFile(string currentDir, string backupDir, string fileName, User u, long delta)
        {
            string oldFileName = Path.Combine(currentDir, fileName);
            string newFileName = Path.Combine(backupDir, fileName) + "." + delta + "." + u.ID + ".gz";
            try
            {
                FileStream sourceFile = null;
                FileStream destFile = null;
                GZipStream compStream = null;
                try
                {
                    FileInfo fi = new FileInfo(oldFileName);
                    if (!fi.Exists || fi.Length == 0)
                        return;
                    sourceFile = File.OpenRead(oldFileName);
                    destFile = File.Create(newFileName);
                    compStream = new GZipStream(destFile, CompressionMode.Compress);
                    sourceFile.CopyTo(compStream);
                }
                finally
                {
                    if (compStream != null)
                        compStream.Dispose();
                    if (sourceFile != null)
                        sourceFile.Dispose();
                    if (destFile != null)
                        destFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Failed to create backup file - " + newFileName + ": " + ex.Message);
            }
        }

        private static void backupFile(string fileName, User u, long delta)
        {
            string newFileName = fileName + "." + delta + "." + u.ID + ".gz";
            try
            {
                FileStream sourceFile = null;
                FileStream destFile = null;
                GZipStream compStream = null;
                try
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (!fi.Exists || fi.Length == 0)
                        return;
                    sourceFile = File.OpenRead(fileName);
                    destFile = File.Create(newFileName);
                    compStream = new GZipStream(destFile, CompressionMode.Compress);
                    sourceFile.CopyTo(compStream);
                }
                finally
                {
                    if (compStream != null)
                        compStream.Dispose();
                    if (sourceFile != null)
                        sourceFile.Dispose();
                    if (destFile != null)
                        destFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Failed to create backup file - " + newFileName + ": " + ex.Message);
            }
        }

        private static void recoverFromBackup(MainAdminForm maf, Space sp, string repositoryFileName)
        {
            // get backup directory location
            string backupdir = Path.Combine(sp.Directory, BackupDir);
            DirectoryInfo di = new DirectoryInfo(backupdir);
            if (!di.Exists)
                throw new Exception("Could not recover the backup repository, no backup directory.");

            // see if repositoryFileName.time.user.gz is there, get the most recent one
            FileInfo[] fInfo = di.GetFiles(repositoryFileName + ".*");
            if (fInfo == null || fInfo.Length == 0)
                throw new Exception("Could not recover the backup repository, no backed up repositories.");

            Array.Sort(fInfo, CompareFilesByDate);
            FileInfo mostRecentI = fInfo[0]; // sort order is set to have the most recent file first
            string properLocation = Path.Combine(sp.Directory, repositoryFileName);
            // need to gzip uncompress
            try
            {
                FileStream sourceFile = null;
                FileStream destFile = null;
                GZipStream compStream = null;
                try
                {
                    destFile = File.Create(properLocation);
                    sourceFile = File.OpenRead(mostRecentI.FullName);
                    compStream = new GZipStream(sourceFile, CompressionMode.Decompress);
                    compStream.CopyTo(destFile);
                }
                finally
                {
                    if (compStream != null)
                        compStream.Dispose();
                    if (sourceFile != null)
                        sourceFile.Dispose();
                    if (destFile != null)
                        destFile.Dispose();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Failed to uncompress backup file + " + mostRecentI.FullName + ": " + ex.Message);
                try
                {
                    File.Delete(properLocation); // cleanup, just in case we left a partial file
                }
                catch (Exception)
                { }
                throw new Exception("Could not uncompress the backup repository.");
            }

            // try to load it
            validateRepositorySize(properLocation);
            try
            {
                maf.loadRepositoryFromFile(properLocation);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Could not load the backup repository - " + ex.Message);
                throw new Exception("Could not load the backup repository.");
            }
            return;
        }

        // sort so that the most recent is first
        private static int CompareFilesByDate(FileInfo f1, FileInfo f2)
        {
            return DateTime.Compare(f2.LastWriteTime, f1.LastWriteTime);
        }

        public static void addLoadGroup(MainAdminForm maf, string name)
        {
            List<LoadGroup> groups = maf.loadGroupMod.groups;
            LoadGroup lg = null;
            foreach (LoadGroup slg in groups)
            {
                if (slg.Name == name)
                {
                    lg = slg;
                    break;
                }
            }
            if (lg == null)
            {
                lg = new LoadGroup();
                lg.Name = name;
                groups.Add(lg);
            }
        }

        public static void addScriptGroup(MainAdminForm maf, string name)
        {
            List<ScriptGroup> scgplist = maf.scriptGroupMod.getScriptGroupList();
            ScriptGroup sg = null;
            foreach (ScriptGroup ssg in scgplist)
            {
                if (ssg.Name == name)
                {
                    sg = ssg;
                    break;
                }
            }
            if (sg == null)
            {
                sg = new ScriptGroup();
                sg.Name = name;
                scgplist.Add(sg);
            }
        }


        /// <summary>
        /// This is basically adding user to the space group in the Acorn Database (Release 4.3 feature).
        /// If user is not found in the acorn database, nothing is added and a warning is logged.
        /// </summary>
        /// <param name="sp">Space</param>
        /// <param name="username">User Name</param>
        /// <param name="groupName">Group Name</param>
        public static void addUserToSpace(MainAdminForm maf, Space sp, string username, string groupName)
        {
            if (maf != null)
            {
                Performance_Optimizer_Administration.User ru = new Performance_Optimizer_Administration.User();
                ru.Username = username;
                bool found = false;
                if (maf.usersAndGroups.usersList != null)
                {
                    foreach (Performance_Optimizer_Administration.User au in maf.usersAndGroups.usersList)
                    {
                        if (au.Username == ru.Username)
                        {
                            found = true;
                            break;
                        }
                    }
                }
                else
                {
                    maf.usersAndGroups.usersList = new List<Performance_Optimizer_Administration.User>();
                }
                if (!found)
                    maf.usersAndGroups.usersList.Add(ru);
            }
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, schema, username);
                if (u == null)
                {
                    Global.systemLog.Warn(username + " user do not exist in Acorn database");
                    return;
                }

                List<SpaceGroup> spaceGroupList = Database.getSpaceGroups(conn, schema, sp);
                if (spaceGroupList != null && spaceGroupList.Count > 0)
                {
                    foreach (SpaceGroup spg in spaceGroupList)
                    {
                        if (spg.Name == groupName)
                        {
                            bool isAlreadyMember = UserAndGroupUtils.isUserGroupMember(spg, u.ID);
                            if (!isAlreadyMember)
                            {
                                Database.addUserToGroup(conn, schema, sp, spg.ID, u.ID, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting group privileges" + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static QueryConnection getConnectionWithVariedRetries(string connectString, string dbType)
        {
            QueryConnection conn = null;
            if (dbType == Database.REDSHIFT)
            {
                conn = ConnectionPool.getConnection(connectString, true, 2);
                if (conn == null)
                {
                    Global.systemLog.Error("Cannot obtain connection to the redshift database");
                    throw new Exceptions.BirstException("Cannot obtain connection to the database");
                }
            }
            else
            {
                conn = ConnectionPool.getConnection(connectString);
            }
            return conn;
        }

        public static bool removeUserFromSpace(Space sp, string username)
        {
            bool removed = false;
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, schema, username);
                if (u != null)
                {
                    List<SpaceGroup> spaceGroupList = Database.getSpaceGroups(conn, schema, sp);
                    if (spaceGroupList != null && spaceGroupList.Count > 0)
                    {
                        foreach (SpaceGroup spg in spaceGroupList)
                        {
                            bool isGroupMember = UserAndGroupUtils.isUserGroupMember(spg, u.ID);
                            if (isGroupMember)
                            {
                                Database.removeUserFromGroup(conn, schema, sp, spg.ID, u.ID, true);
                            }
                        }
                    }
                }
                else
                {
                    Global.systemLog.Warn("Cannot remove " + username + " from Space " + sp.ID + " . User info not found in Database");
                }
                removed = true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error removing user " + username + " from space groups" + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return removed;
        }


        public static MainAdminForm loadRepository(Space sp, out bool newRepository)
        {
            return loadRepository(sp, out newRepository, null);
        }

        public static MainAdminForm loadRepository(Space sp, out bool newRepository, string repositoryFileName)
        {
            MainAdminForm maf = new MainAdminForm();
            maf.setHeadless(true);
            maf.setLogger(Global.systemLog);
            maf.setRepositoryDirectory(sp.Directory);
            if (repositoryFileName == null)
                repositoryFileName = "repository_dev.xml";
            string fileName = Path.Combine(sp.Directory, repositoryFileName);
            newRepository = false;
            if (File.Exists(fileName))
            {
                validateRepositorySize(fileName);
                maf.loadRepositoryFromFile(fileName); // let exceptions be thrown and shown to user (for now)
            }
            else
            {
                Global.systemLog.Warn("Repository " + fileName + " not found, trying a backup");
                recoverFromBackup(maf, sp, repositoryFileName);
            }
            maf.setupTree();
            return maf;
        }

        private static void validateRepositorySize(string fileName)
        {
            FileInfo f = new FileInfo(fileName);
            if (f.Length > REPOSITORY_SIZE_ERROR)
            {
                Global.systemLog.Warn("Repository " + fileName + " is too large, will not load it (" + Util.formatBytes(f.Length) + " >= " + Util.formatBytes(REPOSITORY_SIZE_ERROR) + ")");
                throw new Exception("Repository is too large, Birst will not load it (" + Util.formatBytes(f.Length) + " >= " + Util.formatBytes(REPOSITORY_SIZE_ERROR) + ").");
            }
            else if (f.Length > REPOSITORY_SIZE_WARN)
            {
                Global.systemLog.Info("Repository " + fileName + " is very large (" + Util.formatBytes(f.Length) + " >= " + Util.formatBytes(REPOSITORY_SIZE_WARN) + ")");
            }
        }

        public static void saveDirty(HttpSessionState session)
        {
            saveDirty(session, false);
        }

        public static void saveDirty(HttpSessionState session, bool rebuildNow)
        {
            if (session["AdminDirty"] != null)
            {
                MainAdminForm mafx = (MainAdminForm)session["MAF"];
                if (mafx == null)
                    return;
                Space sp = (Space)session["space"];
                if (sp == null)
                    return;
                session.Remove("AdminDirty");
                if (session["RequiresRebuild"] != null)
                {
                    mafx.RequiresRebuild = !rebuildNow;
                    if (rebuildNow)
                    {
                        buildApplication(mafx, mainSchema, DateTime.MinValue, -1, null, sp, session);
                    }
                    session.Remove("RequiresRebuild");
                }
                if (mafx != null && sp != null)
                    Util.saveApplication(mafx, sp, session, null);
            }
        }

        public static void reBuildIfRequired(HttpSessionState session, MainAdminForm maf, Space sp, bool save)
        {
            if (maf == null)
            {
                maf = Util.getSessionMAF(session);
            }
            if (sp == null)
            {
                sp = Util.getSessionSpace(session);
            }

            if (maf != null && sp != null && maf.RequiresRebuild)
            {
                // flip the flag
                maf.RequiresRebuild = false;
                buildApplication(maf, mainSchema, DateTime.MinValue, -1, null, sp, session);
                if (save)
                    Util.saveApplication(maf, sp, session, null);
            }
        }

        /// <summary>
        /// Creates the directory where all the files used by the wrapper and scheduler are put in
        /// </summary>
        public static void createSLogsDirectoryIfNeeded(Space sp)
        {
            try
            {
                string target = Path.Combine(sp.Directory, "slogs");
                if (!Directory.Exists(target))
                    Directory.CreateDirectory(target);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while creating slogs directory", ex);
            }

        }

        // create an empty initial repository
        public static MainAdminForm createInitialRepository(HttpSessionState session, Space sp, User u)
        {
            MainAdminForm maf = new MainAdminForm();
            maf.setHeadless(true);
            maf.setLogger(Global.systemLog);
            maf.setRepositoryDirectory(sp.Directory);
            maf.setupBlankRepository();
            maf.setupTree();
            setupNewRepository(maf, mainSchema, sp, u);
            saveApplication(maf, sp, session, u);
            return maf;
        }

        public static MainAdminForm setSpace(HttpSessionState session, Space sp, User u)
        {
            return setSpace(session, sp, u, true);
        }

        public static MainAdminForm setSpace(HttpSessionState session, Space sp, User u, bool fillSessionStatus)
        {
            if (u == null && session != null)
                u = (User)session["user"];

            createSLogsDirectoryIfNeeded(sp);
            resetSessionObjectWithSpace(session, sp, u);

            int currentSpaceOpId = SpaceOpsUtils.getSpaceAvailability(sp);
            if (SpaceOpsUtils.isSwapSpaceInProgress(currentSpaceOpId) || SpaceOpsUtils.isNewSpaceBeingCopied(currentSpaceOpId))
            {
                // This prevents any creation of new repository or dcconfig directories if swap space is going on and another user accesses this space.
                // For new copied space, this prevents the partial population of space_group table.
                return null;
            }

            bool newRepository = false;
            MainAdminForm maf = loadRepository(sp, out newRepository);
            if (maf == null)
                return null;
            if (session != null)
            {
                session["spaceloadtime"] = DateTime.Now;
                session["hasspace"] = true;
            }

            // Check whether user-space-group mapping needs to be created in the database. Create the mappings if it does not 
            // exist.

            checkAndCreateUserGroupDbMappings(sp, maf);
            if (session != null)
                session["MAF"] = maf;
            if (newRepository)
            {
                setupNewRepository(maf, mainSchema, sp, u);
                if (session != null)
                {
                    session["status"] = new Status.StatusResult(Status.StatusCode.Ready);
                    session["AdminDirty"] = true;
                }
            }
            else
            {
                if (fillSessionStatus)
                {
                    Status.StatusResult sr = null;
                    bool fetchOldWay = true;
                    if (Util.useExternalSchedulerForProcess(sp, maf))
                    {
                        sr = Status.getStatusUsingExternalScheduler(sp);
                        // this is a hack for now.
                        // To avoid the confusion between different processing engine
                        // we get the status both the old and the new way. Will phase out completly 
                        // once users move to >= 5.1 processing engine.
                        fetchOldWay = sr != null && Status.isRunningCode(sr.code) ? false : true;
                    }

                    if (fetchOldWay)
                    {
                        sr = Status.getLoadStatus(session, sp);
                        if (sr.code == Status.StatusCode.Complete)
                        {
                            QueryConnection conn = null;
                            try
                            {
                                conn = ConnectionPool.getConnection();
                                ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, sp.LoadNumber, Util.LOAD_GROUP_NAME);
                            }
                            finally
                            {
                                ConnectionPool.releaseConnection(conn);
                            }
                        }
                        updateLoadInfo(sp, maf, sp.LoadNumber, Util.LOAD_GROUP_NAME, sr);
                    }

                    if (session != null)
                        session["status"] = sr;
                }
            }

            if (session != null)
            {
                Util.setDesignerAndDashboardViewablePermissions(session);
            }
            // see if we need to update the repository with new DB connection information
            bool changedDC = false;
            setupDefaultConnection(sp, maf, session, out changedDC);
            // see if we need to update the repository with new server parameters (query language, timeout, max rows, etc)
            bool changedSpaceParams = false;

            setupSpaceParameters(sp, maf, session, out changedSpaceParams);
            setupUserSpaceParameters(sp, u, maf, session);
            // see if we need to update the realtime connections
            setupRealtimeconnections(session, sp, maf);
            // see if we need to supply proxydb credential for imported spaces
            setUpAdminConnectInfoForImportedSpace(maf, session);
            // save out the repository if it has changed from the above operations            

            if (session != null)
            {
                saveDirty(session);
            }
            sp.getSettings();

            // upgrade the space with the report styles information
            DirectoryInfo reportStylesI = new DirectoryInfo(Path.Combine(sp.Directory, "catalog", "shared", "styles"));
            if (!reportStylesI.Exists)
            {
                reportStylesI.Create();
                string acornPath = HostingEnvironment.MapPath("~");
                if (acornPath == null)
                {
                    Global.systemLog.Info("acorn path was not available in hosting environment static reference. Trying to get it from System.Configuration");
                    acornPath = System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"];
                }
                if (acornPath != null)
                {
                    DirectoryInfo reportDefaultsI = new DirectoryInfo(Path.Combine(acornPath, "config", "ReportDefaults"));
                    if (reportDefaultsI.Exists)
                    {
                        // copy the contents to the styles directory
                        FileInfo[] fileList = reportDefaultsI.GetFiles();
                        foreach (FileInfo fi in fileList)
                        {
                            Util.FileCopy(fi.FullName, Path.Combine(reportStylesI.FullName, fi.Name));
                        }
                    }
                }
                else
                {
                    Global.systemLog.Warn("report style information is not copied over to space " + sp.ID.ToString() + " since acornPath couldn't be determined.");
                }
            }
            log4net.ThreadContext.Properties["user"] = u.Username;
            using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
            {
                Global.userEventLog.Info("SETSPACE");
            }
            return (maf);
        }

        public static void logEvent(User user, string itemId, string eventName)
        {
            if (user == null)
            {
                Global.systemLog.Debug("No user given for logging Event. Should not happen : itemId : " + itemId + " : eventName : " + eventName);
                return;
            }
            log4net.ThreadContext.Properties["user"] = user.Username;
            using (log4net.ThreadContext.Stacks["itemid"].Push(itemId))
            {
                Global.userEventLog.Info(eventName);
            }
        }


        public static bool useExternalSchedulerForDeletion(Space sp, MainAdminForm maf)
        {
            // singls if server property for manual mode to use processing is enabled
            bool manualDeletionMode = false;
            object obj = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalSchedulerForDeletion"];
            if (obj != null)
            {
                bool.TryParse((string)obj, out manualDeletionMode);
            }

            return manualDeletionMode && useExternalSchedulerForProcess(sp, maf);
        }

        public static bool useExternalSchedulerForManualProcessing(Space sp, MainAdminForm maf)
        {
            // singls if server property for manual mode to use processing is enabled
            bool manualProcessMode = false;
            object obj = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalSchedulerForManualProcess"];
            if (obj != null)
            {
                bool.TryParse((string)obj, out manualProcessMode);
            }

            return manualProcessMode && useExternalSchedulerForProcess(sp, maf);
        }

        public static bool useExternalSchedulerForManualExtract(Space sp)
        {
            // signals if space is pointing to SFDC enabled performance engine via SFDCVersion column
            bool supportedVersion = false;
            // singls if server property for manual mode to use extraction is enabled
            bool manualExtractMode = false;
            // signlas whether this is dev mode or prod mode for extraction
            bool enableMode = isSpaceEnabledForExtractionAndProcessing(sp);
            object obj = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalSchedulerForManualExtract"];
            if (obj != null)
            {
                bool.TryParse((string)obj, out manualExtractMode);
            }

            // additionally check for 
            PerformanceEngineVersion extractVersion = Util.getSFDCEnabledPerformanceEngineVersion(sp);
            if (extractVersion != null && extractVersion.SupportsSFDCExtract)
            {
                supportedVersion = true;
            }

            return manualExtractMode && supportedVersion && enableMode;
        }

        public static bool useExternalSchedulerForProcess(Space sp, MainAdminForm maf)
        {
            bool use = false;
            bool enableMode = isSpaceEnabledForExtractionAndProcessing(sp);
            bool newRepository;
            if (maf == null)
                maf = Util.loadRepository(sp, out newRepository);
            if (maf != null && maf.stagingTableMod != null)
            {
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    if (st.LoadGroups != null)
                    {
                        foreach (string lg in st.LoadGroups)
                        {
                            if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                                return false;
                        }
                    }
                }
            }
            // additionally check for 
            PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
            if (peVersion != null && peVersion.IsProcessingGroupAware)
            {
                use = true;
            }

            return use && enableMode;
        }

        public static bool useExternalSchedulerExtract(Space sp)
        {
            bool use = false;
            bool enableMode = isSpaceEnabledForExtractionAndProcessing(sp);
            // additionally check for 
            if (ConnectorUtils.UseNewConnectorFramework())
            {
                use = true;
            }
            else
            {
                PerformanceEngineVersion peVersion = Util.getSFDCEnabledPerformanceEngineVersion(sp);
                if (peVersion != null && peVersion.SupportsSFDCExtract)
                {
                    use = true;
                }
            }
            return use && enableMode;
        }

        public static bool isSpaceEnabledForExtractionAndProcessing(Space sp)
        {
            return isProdModeForExtractionAndProcessing() && !sp.DisallowExternalScheduler;
        }

        public static bool isProdModeForExtractionAndProcessing()
        {
            bool prodMode = true;
            object obj = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalSchedulerProdMode"];
            if (obj != null)
            {
                bool.TryParse((string)obj, out prodMode);
            }

            return prodMode;
        }


        // Called by Acorn wrapper AcornExecute project
        public static void updateLoadInfo(Space sp, User u, MainAdminForm maf, int loadNumber, string loadGroup)
        {
            bool removeRunningProcessInfo = true;
            Status.StatusResult sr = Status.getLoadStatus(null, sp, loadNumber, loadGroup);
            if (Status.isRunningCode(sr.code) || (sp.LoadNumber == loadNumber && (sr.code == Status.StatusCode.None || sr.code == Status.StatusCode.BuildingDashboards)))
            {
                Global.systemLog.Warn("Marking the load as failed for space : " + sp.ID + " : name " + sp.Name + " : loadNumber " + loadNumber +
                    " : loadGroup : " + loadGroup);
                removeRunningProcessInfo = Status.setLoadFailed(sp, loadNumber, loadGroup, true);
            }
            else if (sr.code == Status.StatusCode.Failed)
            {
                // Make sure at this point there is a loadwarehouse entry 3 for this loadnumber, if not put one
                Global.systemLog.Info("Load failed. Marking all the required entries as false : space : " + sp.ID + " : name " + sp.Name + " : loadNumber " + loadNumber +
                    " : loadGroup : " + loadGroup);
                removeRunningProcessInfo = Status.setLoadFailed(sp, loadNumber, loadGroup, true);
            }
            else if (sr.code == Status.StatusCode.Complete)
            {
                Util.updateRequiresPublishFlag(maf, false);
                Util.saveApplication(maf, sp, null, u);
                updateLoadInfo(sp, maf, loadNumber, loadGroup, sr);
            }
            if (removeRunningProcessInfo)
            {
                Database.removeProcessingServerForSpace(sp.ID, Util.RUNNING_TYPE_LOAD);
            }
        }

        // loadNumber is the new  loadNumber
        public static void updateLoadInfo(Space sp, MainAdminForm maf, int loadNumber, string loadGroup, Status.StatusResult sr)
        {
            if (sr.code == Status.StatusCode.Complete)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    ApplicationLoader.updateLoadSuccess(maf, sp, Util.getMainSchema(), conn, sr, loadNumber - 1, loadGroup);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        public static void setSessionObject(HttpSessionState session, string name, object value)
        {
            if (session != null)
            {
                session[name] = value;
            }
        }

        public static object getSessionObject(HttpSessionState session, string name)
        {
            if (session != null)
            {
                return session[name];
            }
            return null;
        }

        public static void setSessionDeleteLastLoadNumber(HttpSessionState session, int deleteLastLoadNumber)
        {
            if (session != null)
            {
                session["deleteLastLoadNumber"] = deleteLastLoadNumber;
            }
        }

        public static int getSessionDeleteLastLoadNumber(HttpSessionState session)
        {
            if (session != null && session["deleteLastLoadNumber"] != null)
            {
                return (int)session["deleteLastLoadNumber"];
            }

            return -1;
        }

        public static void removeSessionDeleteLastLoadNumber(HttpSessionState session)
        {
            if (session != null && session["deleteLastLoadNumber"] != null)
            {
                session.Remove("deleteLastLoadNumber");
            }
        }

        public static void resetSessionObjectWithSpace(HttpSessionState session, Space sp, User u)
        {
            Util.setLogging(u, sp);
            if (sp != null)
            {
                Global.systemLog.Info("Space being logged in to: " + sp.Name);
            }
            else
            {
                Global.systemLog.Info("No space information available");
            }
            if (session != null)
            {
                // save the space for the old session and then clear out the session information specific to the space
                saveDirty(session);
                session["space"] = sp;
                session.Remove("stagingdatatable");
                session.Remove("autopublish");
                session.Remove("MAF");
                session.Remove("status");
                session.Remove("AdminDirty");
                session.Remove("RequiresRebuild");
                session.Remove("sferrors");
                session.Remove("salesforcesettings");
                session.Remove("sfbinding");
                session.Remove("sfautomatic");
                session.Remove("sfobjects");
                session.Remove("statusFailedTM");
                session.Remove("TempCopySchedules");
                session.Remove("importmap");
                session.Remove("AdhocPermitted");
                session.Remove("DashboardPermitted");
                session.Remove(SESSION_OBJECT_PACKAGELIST_CACHE);
                if (session["sforceloader"] != null)
                {
                    session.Remove("sforceloader");
                }
                if (session["dctime"] != null)
                    session.Remove("dctime");
            }
        }

        public static void setupRealtimeconnections(HttpSessionState session, Space sp, MainAdminForm maf)
        {
            if (maf == null || maf.connection == null || maf.connection.connectionList == null)
            {
                Global.systemLog.Warn("Cannot setup realtime connections " + (sp != null ? " for space " + sp.ID.ToString() : ""));
                return;
            }
            try
            {
                bool changed = false;
                string dcDirPath = sp.Directory + "\\" + Util.DCCONFIG_DIR;
                //Moving dcconfig.xml to dcconfig directory
                BirstConnectUtil.moveFileToDirectory(sp, Util.DCCONFIG_DIR, Util.DCCONFIG_FILE_NAME);
                if (Directory.Exists(dcDirPath))
                {
                    string[] files = Directory.GetFiles(dcDirPath, "*dcconfig.xml");
                    if (files != null)
                    {
                        string[] fnames = new string[files.Length];
                        Dictionary<string, DateTime> dcFileMap = new Dictionary<string, DateTime>();
                        for (int i = 0; i < files.Length; i++)
                        {
                            string dcFile = new FileInfo(files[i]).Name;
                            DateTime ctime = File.GetLastWriteTime(dcDirPath + "\\" + dcFile);
                            dcFileMap.Add(dcFile, ctime);
                        }
                        if (session != null)
                        {
                            session["dctime"] = dcFileMap;
                        }
                        // Find deleted realtime connections
                        List<Connection> rcList = BirstConnectUtil.getRealtimeConnectionList(sp, files);
                        ArrayList toRemoveList = new ArrayList();
                        foreach (Performance_Optimizer_Administration.DatabaseConnection dc in maf.connection.connectionList)
                        {
                            if (dc.Realtime == true)
                            {
                                bool found = false;
                                foreach (Connection rc in rcList)
                                {
                                    if (rc.Name == dc.Name)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    if (!maf.isConnectionInUse(dc.Name))
                                        toRemoveList.Add(dc);
                                }
                            }
                        }
                        //remove realtime connections
                        if (toRemoveList.Count > 0)
                        {
                            changed = maf.removeConnections(toRemoveList);
                        }
                        //add/edit realtime connection
                        foreach (Connection rc in rcList)
                        {
                            string name = rc.Name;
                            if (name != null)
                            {
                                Performance_Optimizer_Administration.DatabaseConnection dc = null;
                                foreach (Performance_Optimizer_Administration.DatabaseConnection sdc in maf.connection.connectionList)
                                {
                                    if (sdc.Name == name)
                                    {
                                        dc = sdc;
                                        break;
                                    }
                                }
                                if (dc == null)
                                {
                                    dc = new Performance_Optimizer_Administration.DatabaseConnection();
                                    maf.connection.connectionList.Add(dc);
                                    changed = true;
                                }
                                if (!dc.Realtime)
                                {
                                    dc.Realtime = true;
                                    changed = true;
                                }
                                if (name != dc.Name)
                                {
                                    dc.Name = name;
                                    changed = true;
                                }
                                if (dc.VisibleName != rc.VisibleName)
                                {
                                    dc.VisibleName = rc.VisibleName;
                                    changed = true;
                                }
                                if (sp.ID.ToString() != dc.ID)
                                {
                                    dc.ID = sp.ID.ToString();
                                    changed = true;
                                }
                                string servername = rc.Server;
                                string port = rc.Port == null ? "" : rc.Port;
                                string dbType = rc.Type == null ? "" : rc.Type;
                                if (rc.Database != dc.Database)
                                {
                                    dc.Database = rc.Database;
                                    changed = true;
                                }
                                dbType = rc.Type;
                                if (rc.Driver != dc.Driver)
                                {
                                    dc.Driver = rc.Driver;
                                    changed = true;
                                }
                                if (rc.UserName != dc.UserName)
                                {
                                    dc.UserName = rc.UserName;
                                    changed = true;
                                }
                                if (rc.Password != dc.Password)
                                {
                                    dc.Password = rc.Password;
                                    changed = true;
                                }
                                if (rc.Schema != dc.Schema)
                                {
                                    dc.Schema = rc.Schema;
                                    changed = true;
                                }
                                if (dc.useDirectConnection != rc.useDirectConnection)
                                {
                                    dc.useDirectConnection = rc.useDirectConnection;
                                    changed = true;
                                }
                                string dtype = ConnectionForm.getConnectionType(dc.Driver, dbType);
                                if (dtype != null && dtype != dc.Type) // don't change if null... XXX ... not sure why we get null, but...
                                {
                                    dc.Type = dtype;
                                    changed = true;
                                }
                                if (dbType != null && dbType.Equals("Infobright") && port == "")
                                    port = "5029";
                                string dccs = dc.ConnectString;
                                string dcdb = dc.Database;
                                if (rc.GenericDatabase)
                                {
                                    dc.ConnectString = rc.ConnectString;
                                }
                                else if (rc.Type == "In-memory Database" && rc.ConnectString == "inprocess")
                                {
                                    dc.ConnectString = "inprocess";
                                }
                                else
                                {
                                    ConnectionForm.setConnectInfo(dc, servername, port, dc.Database);
                                }
                                if (dccs != dc.ConnectString || dcdb != dc.Database)
                                {
                                    changed = true;
                                }

                                // Create a temporary proxy db connection so that we can copy over the properties to server parameters
                                setUpProxyAdminConnectInfo(maf, session);
                            }
                        }
                        foreach (string leConn in Utils.BirstConnectUtil.getLocalETLConenctions(sp, maf).Keys)
                        {
                            foreach (Performance_Optimizer_Administration.DatabaseConnection conn in maf.connection.connectionList)
                            {
                                if (conn.VisibleName == leConn)
                                {
                                    if (!conn.UnicodeDatabase)
                                    {
                                        conn.UnicodeDatabase = true;
                                        changed = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (changed)
                {
                    if (session != null)
                        session["AdminDirty"] = true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting up realtime connections " + (sp != null ? " for space " + sp.ID.ToString() : ""), ex);
            }
        }

        private static void setUpProxyAdminConnectInfo(MainAdminForm maf, HttpSessionState session)
        {
            bool changed = false;
            // Create a temporary proxy db connection so that we can copy over the properties to server parameters
            Performance_Optimizer_Administration.DatabaseConnection proxydc = new Performance_Optimizer_Administration.DatabaseConnection();
            proxydc.Type = PROXY_DB_TYPE;
            proxydc.Driver = PROXY_DB_DRIVER_NAME;
            ConnectionStringSettings cs = ConfigUtils.getConnectionString();
            string[] tokens = Performance_Optimizer_Administration.Util.splitString(cs.ConnectionString, ';');
            string proxydcServerName = null;
            string proxydcPort = "";
            if (tokens != null && tokens.Length > 0)
            {
                foreach (string token in tokens)
                {
                    string[] propTokens = Performance_Optimizer_Administration.Util.splitString(token, '=');
                    if (propTokens != null && propTokens.Length > 1)
                    {
                        if (propTokens[0].ToLower() == "server")
                        {
                            if (propTokens[1].Contains(","))
                            {
                                string[] items = propTokens[1].Split(',');
                                proxydcServerName = items[0];
                                proxydcPort = items[1];
                            }
                            else
                            {
                                proxydcServerName = propTokens[1];
                                proxydcPort = "";
                            }
                        }
                        else if (propTokens[0].ToLower() == "database")
                        {
                            proxydc.Database = propTokens[1];
                        }
                        else if (propTokens[0].ToLower() == "dbq")
                        {
                            proxydc.Database = propTokens[1].IndexOf("/") > 0 ? propTokens[1].Substring(propTokens[1].IndexOf("/") + 1) : "";
                        }
                        else if (propTokens[0].ToLower() == "uid")
                        {
                            proxydc.UserName = propTokens[1];
                        }
                        else if (propTokens[0].ToLower() == "pwd")
                        {
                            proxydc.Password = propTokens[1];
                        }
                    }
                }
            }
            ConnectionForm.setConnectInfo(proxydc, proxydcServerName, proxydcPort, proxydc.Database);
            //Now that the proxy dc connection has been processed, extract the required properties and set them to server parameters
            if (proxydc.ConnectString != maf.serverPropertiesModule.ProxyDBConnectionStringTextBox.Text)
            {
                maf.serverPropertiesModule.ProxyDBConnectionStringTextBox.Text = proxydc.ConnectString;
                changed = true;
            }
            // originally these were not encrypted in the repository, they are now (unfortunately they still are in cleartext in web.config)
            if (proxydc.UserName != maf.serverPropertiesModule.ProxyDBUserNameTextBox.Text)
            {
                maf.serverPropertiesModule.ProxyDBUserNameTextBox.Text = proxydc.UserName;
                changed = true;
            }
            if (proxydc.Password != maf.serverPropertiesModule.ProxyDBPasswordTextBox.Text)
            {
                maf.serverPropertiesModule.ProxyDBPasswordTextBox.Text = proxydc.Password;
                changed = true;
            }

            if (changed)
            {
                if (session != null)
                    session["AdminDirty"] = true;
            }
        }


        /// <summary>
        /// Add the proxy dc connection info if space has any imported packages
        /// Takes care of the live access case where proxy dc connection is used
        /// </summary>
        /// <param name="maf"></param>
        /// <param name="session"></param>
        private static void setUpAdminConnectInfoForImportedSpace(MainAdminForm maf, HttpSessionState session)
        {
            if (maf.Imports != null && maf.Imports.Length > 0)
            {
                setUpProxyAdminConnectInfo(maf, session);
            }
        }

        public static void checkAndCreateUserGroupDbMappings(Space sp)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> spaceGroups = Database.getSpaceGroups(conn, schema, sp);
                if (spaceGroups == null || spaceGroups.Count == 0)
                {
                    // If there are no groups, means space-group-usermapping has not been done (4.3 feature)
                    bool newRepository = false;
                    MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                    Database.mapAndCreateUserGroupMappings(conn, schema, sp, maf);
                    sp.SpaceGroups = Database.getSpaceGroups(conn, schema, sp);
                }
                else
                {
                    sp.SpaceGroups = spaceGroups;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while creating group/user/acl mapping for " + sp.ID);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static bool areSpacesInSameDatabase(Space sp1, Space sp2)
        {
            return sp1.ConnectString.ToLower() == sp2.ConnectString.ToLower();
        }


        public static bool isSpaceProcessing(Space sp)
        {  
            if (Util.isSpaceEnabledForExtractionAndProcessing(sp))
            {
                Status.StatusResult sr = Status.getStatusUsingExternalScheduler(sp);
                if(sr != null && Status.isRunningCode(sr.code))
                {
                    return true;
                }
            }

            return checkForPublishingStatus(sp);
        }


        /// <summary>
        /// Check for both publish.lock file as well the txn_command_history.
        /// Reason being because publish.lock is spit out after loadstaging command and before loadwarehouse operation.
        /// Hence the need to check for both.
        /// </summary>
        /// <param name="sp">Space Object</param>
        /// <returns>true if the space is publishing. False otherwise</returns>
        public static bool checkForPublishingStatus(Space sp)
        {
            // check for publish lock file first
            bool currentProcessingStatus = ApplicationLoader.isSpaceBeingPublished(sp);
            if (!currentProcessingStatus)
            {
                // if publish.lock is not present, make sure to check the load status from database
                Status.StatusResult statusResult = Status.getLoadStatus(null, sp);
                if (statusResult != null)
                {
                    currentProcessingStatus = Status.isRunningCode(statusResult.code);
                }
            }
            return currentProcessingStatus;
        }

        /// <summary>
        /// Lazy Database Migration for Space - Group/User/ACL mapping (4.3 Release Feature).
        /// Essentially, if space do not have any database enteries, they are mapped using "maf" userAndGroups object.
        /// If it has already been mapped in db, no need.
        /// </summary>
        /// <param name="sp">Space</param>
        /// <param name="maf">Repository data structure for Acorn</param>
        public static void checkAndCreateUserGroupDbMappings(Space sp, MainAdminForm maf)
        {
            if (sp == null || maf == null || maf.usersAndGroups == null || maf.usersAndGroups.groupsList == null)
            {
                return;
            }

            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> spaceGroups = Database.getSpaceGroups(conn, schema, sp);
                if (spaceGroups == null || spaceGroups.Count == 0)
                {
                    // If there are no groups, means space-group-usermapping has not been done (4.3 feature)
                    Database.mapAndCreateUserGroupMappings(conn, schema, sp, maf);
                    spaceGroups = Database.getSpaceGroups(conn, schema, sp);
                }
                sp.SpaceGroups = spaceGroups;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while creating group/user/acl mapping for " + sp.ID);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void removeDCAuthenticationInformation(string filename)
        {
            if (File.Exists(filename))
            {
                // Setup realtime connections
                XmlDocument doc = new XmlDocument();
                string config = File.ReadAllText(filename, Encoding.UTF8);
                int index = config.IndexOf("<object ");
                if (index < 0)
                    return;
                doc.XmlResolver = null;
                doc.LoadXml(config.Substring(index));
                XmlNode node = doc.FirstChild;
                if (node.Attributes[0].Value == "com.birst.dataconductor.DataConductorConfig")
                {
                    foreach (XmlNode cnode in node.ChildNodes)
                    {
                        if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "taskList")
                        {
                            foreach (XmlNode cnode2 in cnode.ChildNodes)
                            {
                                if (cnode2.Attributes[0].Name == "method" && cnode2.Attributes[0].Value == "add")
                                {
                                    foreach (XmlNode cnode3 in cnode2.ChildNodes)
                                    {
                                        if (cnode3.Attributes[0].Value == "com.birst.dataconductor.Task")
                                        {
                                            foreach (XmlNode cnode4 in cnode3.ChildNodes)
                                            {
                                                if (cnode4.Attributes[0].Name == "property" && cnode4.Attributes[0].Value == "sourceList")
                                                {
                                                    foreach (XmlNode cnode5 in cnode4.ChildNodes)
                                                    {
                                                        if (cnode5.Attributes[0].Name == "method" && cnode5.Attributes[0].Value == "add")
                                                        {
                                                            foreach (XmlNode cnode6 in cnode5.ChildNodes)
                                                            {
                                                                if (cnode6.Attributes[0].Value == "com.birst.dataconductor.QuerySource")
                                                                {
                                                                    foreach (XmlNode inode in cnode6.ChildNodes)
                                                                    {
                                                                        if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "databaseName")
                                                                        {
                                                                            inode.ChildNodes[0].InnerText = "";
                                                                        }
                                                                        else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "serverName")
                                                                        {
                                                                            inode.ChildNodes[0].InnerText = "";
                                                                        }
                                                                        else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "username")
                                                                        {
                                                                            inode.ChildNodes[0].InnerText = "";
                                                                        }
                                                                        else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "password")
                                                                        {
                                                                            inode.ChildNodes[0].InnerText = "";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                string result = config.Substring(0, index) + doc.InnerXml;
                File.WriteAllText(filename, result);
            }
        }

        // make sure that the URL is a valid one - https://[^/?]+\\.(sales|visual\\.)force\\.com/services/(S|s)(O|o)(A|a)(P|p)/(u|c)/.* 
        // - SFDC security requirement to do this check
        public static bool isValidSFDCUrl(string url)
        {
            Regex sfdcurlregex = new Regex("https://[^/?]+\\.(sales|visual\\.)force\\.com/services/(S|s)(O|o)(A|a)(P|p)/(u|c)/.*");
            return sfdcurlregex.IsMatch(url);
        }

        public static void setupNewRepository(MainAdminForm maf, string mainSchema, Space sp, User u)
        {
            // Setup admin user
            addUserToSpace(maf, sp, u.Username, null);
            setGroupPrivileges(sp, u.Username, u.AccountType);
            // Create subject area
            maf.serverPropertiesModule.genSABox.Checked = true;

            Util.buildApplication(maf, mainSchema, DateTime.Now, sp.LoadNumber + 1, null, sp, null);
        }

        public static void deleteDir(DirectoryInfo dir)
        {
            if (!dir.Exists)
                return;

            DirectoryInfo[] dlist = dir.GetDirectories();
            foreach (DirectoryInfo di in dlist)
                deleteDir(di);

            FileInfo[] fileList = dir.GetFiles();
            foreach (FileInfo fi in fileList)
            {
                fi.Delete();
            }
            dir.Delete();
        }

        public static void copyDir(string source, string target)
        {
            copyDir(source, target, null);
        }

        public static void copyDir(string source, string target, string[] skipExtensions)
        {
            DirectoryInfo sd = new DirectoryInfo(source);
            if (!sd.Exists)
            {
                Global.systemLog.Debug("copyDir: source directory does not exist: " + source);
                return;
            }

            if (!Directory.Exists(target))
                Directory.CreateDirectory(target);

            FileInfo[] fileList = sd.GetFiles();            
            foreach (FileInfo fi in fileList)
            {
                bool skip = false;
                if (skipExtensions != null)
                {
                    foreach (string extension in skipExtensions)
                    {
                        if (fi.Name.ToLower().EndsWith(extension))
                        {
                            skip = true;
                            break;
                        }
                    }
                }
                if (!skip)
                {
                    // retry if there's an issue
                    int count = 0;
                    while (count++ < 10)
                    {
                        try
                        {
                            Util.FileCopy(fi.FullName, Path.Combine(target, fi.Name), true);
                            break;
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Debug("copyDir: " + fi.FullName + ", " + Path.Combine(target, fi.Name) + ": " + ex.Message);
                            Thread.Sleep(1000);
                        }
                    }
                }
            }
            DirectoryInfo[] dlist = sd.GetDirectories();
            foreach (DirectoryInfo di in dlist)
                copyDir(di.FullName, Path.Combine(target, di.Name), skipExtensions);
        }

        /*
         * This method forks perfEngine command to copy catalog directory.
         * This method is being invoked only if Acorn cannot copy catalog directory
         * */
        public static void copyCatalogDir(Space oldsp, string source, string target, string[] skipExtensions, string[] allowedItems, bool replicate)
        {
            string cmdFilename = getCopyCatalogCmd(oldsp, source, target, skipExtensions, allowedItems, replicate);           

            // Create the ProcessInfo object
            string enginecmd = Util.getEngineCommand(oldsp);
            ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
            psi.Arguments = cmdFilename + " " + oldsp.Directory;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            Global.systemLog.Info("copy catalog from " + source + " to " +  target  + " started");            
            Process proc = Process.Start(psi);
            proc.WaitForExit();
            string line = null;
            while ((line = proc.StandardError.ReadLine()) != null)
            {
                Global.systemLog.Warn(line);
            }
            while ((line = proc.StandardOutput.ReadLine()) != null)
            {
                Global.systemLog.Info(line);
            } 
            if (proc.HasExited)
            {
                Global.systemLog.Info("copy catalog from " + source + " to " + target + " Exited with code: " + proc.ExitCode);
                Global.systemLog.Info("copy catalog from " + source + " to " + target + " completed");            
            }
           
            proc.Close();           
        }

        /**
         * This method is used to check whether current directory name or child directories/files name exceed max length.
         * If it throws PathTooLongException then perfEngine command will used to copy catalog directory while copying space from UI.
         * Supported MAX_LENGTH for directory is 248 characters
         * Supported MAX_LENGTH for file is 260 characters 
         */
        public static Boolean isDirectoryLengthSupported(string source)
        {
            Boolean result = true;
            try
            {
                DirectoryInfo sd = new DirectoryInfo(source);
                if (!sd.Exists)
                {                   
                    return true;
                }

                FileInfo[] fileList = sd.GetFiles();
                foreach (FileInfo fi in fileList)
                {
                    string fname = fi.FullName; //throws PathTooLongException, if lenth exceed MAX_LENGTH
                }
                DirectoryInfo[] dlist = sd.GetDirectories();
                foreach (DirectoryInfo di in dlist)
                {
                    result = isDirectoryLengthSupported(di.FullName); //throws PathTooLongException, if lenth exceed MAX_LENGTH
                    if(!result)
                    {
                        break;
                    }
                }
            }
            catch (PathTooLongException)
            {
                return false;
            }
            return result;
        }

        /**
         * It creates copycatalog.cmd file in space directory and returns the absolute file path
         * It is being used only if perf engine command is required to copy catalog
         * */
        public static string getCopyCatalogCmd(Space oldsp, string source, string target, string[] skipExtensions, string[] allowedItems, bool replicate)
        {
            string cmdFilename = oldsp.Directory + "\\" + "copycatalog.cmd";
            StringBuilder commands = new StringBuilder();
            commands.Append("copycatalog " + source + " " + target + " ");
            if (skipExtensions != null)
            {
                int i = 1;
                foreach (string ext in skipExtensions)
                {
                    commands.Append(ext);
                    if (i < skipExtensions.Length)
                    {
                        i++;
                        commands.Append(",");
                    }
                }
            }
            else
            {
                commands.Append("NONE");
            }
            commands.Append(" ");
            if (allowedItems != null)
            {
                int j = 1;
                foreach (string item in allowedItems)
                {
                    commands.Append(item);
                    if (j < allowedItems.Length)
                    {
                        j++;
                        commands.Append(",");
                    }
                }
            }
            else
            {
                commands.Append("NONE");
            }
            commands.Append(" " + replicate);
            File.WriteAllText(cmdFilename, commands.ToString());
            return cmdFilename;
        }

        public static void setBreadCrumbPage(Space sp, User u, MasterPage master)
        {
            setBreadCrumbPage(sp, u, master, false);
        }

        public static void setBreadCrumbPage(Space sp, User u, MasterPage master, bool adhocLabel)
        {
            setHeaderDetails(sp, u, master);
        }


        public static string[] getMatchedLogFileNames(Space sp, int loadId, string loadDate)
        {
            string[] fnames = null;

            // first try to get for load id
            string logfile = String.Format("smiengine.*.{0:0000000000}.log", loadId);
            fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
            // if it doesn't exist then try with the loaddate
            //look for loadDate - if data processed using older processing engine, logfile name will not have load id
            if ((fnames == null || fnames.Length == 0) && loadDate != null)
            {
                string[] sameDateFnames = null;
                string[] prevDateFnames = null;
                DateTime dt;
                string[] formats = { "M/dd/yyyy", "MM/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "yyyy/MM/dd", "MM-dd-yyyy" };
                if (DateTime.TryParseExact(loadDate, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dt))
                {
                    logfile = String.Format("smiengine.*.{0}.log", dt.ToString("yyyy-MM-dd"));
                    sameDateFnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                    logfile = String.Format("smiengine.*.{0}.log", dt.AddDays(-1).ToString("yyyy-MM-dd"));
                    prevDateFnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                }

                List<string> consolidatedDateFnames = new List<string>();
                if (prevDateFnames != null && prevDateFnames.Length > 0)
                {
                    prevDateFnames = Util.sortFiles(prevDateFnames);
                    foreach (string sameDayName in prevDateFnames)
                    {
                        consolidatedDateFnames.Add(sameDayName);
                    }
                }

                if (sameDateFnames != null && sameDateFnames.Length > 0)
                {
                    sameDateFnames = Util.sortFiles(sameDateFnames);
                    foreach (string sameDayName in sameDateFnames)
                    {
                        consolidatedDateFnames.Add(sameDayName);
                    }
                }

                fnames = consolidatedDateFnames.Count > 0 ? consolidatedDateFnames.ToArray() : null;
            }

            return fnames;
        }

        public static void saveSession(HttpSessionState s)
        {
            MainAdminForm maf = (MainAdminForm)s["MAF"];
            Space sp = (Space)s["space"];
            buildApplication(maf, mainSchema, DateTime.MinValue, -1, null, sp, s);
            saveApplication(maf, sp, s, null);
            s.Remove("save");
        }

        public static bool loadSpaceParameters(QueryConnection conn, string schema, string path)
        {
            // Read in parameters for each space type
            string filename = path + "\\" + SPACE_CONFIG_FILENAME;
            if (!File.Exists(filename))
            {
                Global.systemLog.Info("No spaces.config file: " + filename);
                return true;
            }
            Dictionary<int, SpaceConfig> configurations = new Dictionary<int, SpaceConfig>();
            Global.systemLog.Info("Loading spaces.config from '" + filename + "'");
            StreamReader reader = new StreamReader(filename);
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//") || line.Equals(""))
                    continue;
                string[] cols = line.Split(new char[] { '\t' });
                SpaceConfig sc = new SpaceConfig();
                sc.Type = Int32.Parse(cols[0]);
                sc.URL = cols[1];
                sc.LocalURL = cols[2];
                sc.DatabaseConnectString = cols[3];
                sc.DatabaseType = cols[4];
                sc.DatabaseDriver = cols[5];
                sc.DatabaseLoadDir = cols[6];
                sc.AdminUser = Performance_Optimizer_Administration.MainAdminForm.decrypt(cols[7], Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                sc.AdminPwd = Performance_Optimizer_Administration.MainAdminForm.decrypt(cols[8], Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                sc.EngineCommand = cols[9];
                sc.DeleteDataCommand = cols[10];
                if (cols.Length > 11) // compatibility with older Spaces.config
                {
                    sc.QueryDatabaseConnectString = cols[11];
                    sc.QueryDatabaseType = cols[12];
                    sc.QueryDatabaseDriver = cols[13];
                    sc.QueryUser = Performance_Optimizer_Administration.MainAdminForm.decrypt(cols[14], Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                    sc.QueryPwd = Performance_Optimizer_Administration.MainAdminForm.decrypt(cols[15], Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                    sc.QueryConnectionName = cols[16];
                }
                if (configurations.ContainsKey(sc.Type))
                    configurations.Remove(sc.Type);
                configurations.Add(sc.Type, sc);
                Global.systemLog.Info("Loaded Space Type: " + sc.Type);
            }
            reader.Close();
            Database.addNewSpaceConfigs(conn, schema, configurations);
            return true;
        }

        public static bool loadNewUserSpacesParameters(string path)
        {
            // Read in parameters for each space type
            newUserSpaces.Clear();
            string filename = path + "\\" + NEW_USER_SPACE_CONFIG_FILENAME;
            if (!File.Exists(filename))
            {
                Global.systemLog.Error("No NewUserSpaces.config file: " + filename);
                return false;
            }
            Global.systemLog.Info("Loading NewUserSpaces.config from '" + filename + "'");
            StreamReader reader = new StreamReader(filename);
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//") || line.Equals(""))
                    continue;
                try
                {
                    string[] cols = line.Split(new char[] { '\t' });
                    NewUserSpace nus = new NewUserSpace();
                    nus.description = cols[0];
                    nus.ID = new Guid(cols[1]);
                    nus.valid = Boolean.Parse(cols[2]);
                    string[] types = cols[3].Split(new char[] { ',' });
                    nus.accountTypes = new int[types.Length];
                    for (int i = 0; i < types.Length; i++)
                        nus.accountTypes[i] = int.Parse(types[i]);
                    if (cols.Length > 4)
                        nus.type = int.Parse(cols[4]);
                    if (cols.Length > 5)
                    {
                        nus.spaceSetUp = int.Parse(cols[5]);                                                                        
                    }
                    if (cols.Length > 6)
                    {
                        string[] aclsArr = cols[6].Split(new char[] { ',' });
                        nus.acls = new int[aclsArr.Length];
                        for (int i = 0; i < aclsArr.Length; i++)
                        {
                            nus.acls[i] = int.Parse(aclsArr[i]);
                        }
                    }
                    newUserSpaces.Add(nus);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn(ex.Message + " - " + line, ex);
                }
            }
            reader.Close();
            return true;
        }

        public static List<NewUserSpace> getNewUserSpaces()
        {
            return newUserSpaces;
        }

        public static SpaceConfig getSpaceConfiguration(int type)
        {
            SpaceConfig sc = getSpaceConfigurationNoException(type);
            if (sc == null)
                throw new Exception("Invalid/missing space type: " + type);
            return sc;
        }

        public static SpaceConfig getSpaceConfigurationNoException(int type)
        {
            QueryConnection conn = null;
            SpaceConfig sc = null;
            try
            {
                conn = ConnectionPool.getConnection();
                sc = Database.getSpaceConfig(conn, getMainSchema(), type);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return sc;
        }

        public static bool isQuartzSchedulerEnabled()
        {
            bool useQuartzScheduler = false;
            try
            {
                string useQuartzSchedulerString = System.Web.Configuration.WebConfigurationManager.AppSettings["UseQuartzScheduler"];
                if (useQuartzSchedulerString != null && useQuartzSchedulerString.Length > 0)
                {
                    bool.TryParse(useQuartzSchedulerString, out useQuartzScheduler);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in getting the UseQuartzScheduler value ", ex);
            }
            return useQuartzScheduler;
        }

        public static TrustedService.TrustedService getSMIWebTrustedService(string localUrl)
        {
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Url = localprotocol + localUrl + "/SMIWeb/services/TrustedService";
            return ts;
        }

        public static bool showOldSchedulerUI()
        {
            bool show = false;
            try
            {
                string showOldSchedulerString = System.Web.Configuration.WebConfigurationManager.AppSettings["showOldScheduler"];
                if (showOldSchedulerString != null && showOldSchedulerString.Length > 0)
                {
                    bool.TryParse(showOldSchedulerString, out show);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in getting the showOldScheduler value ", ex);
            }
            return show;
        }

        public static bool useBackgroundUploader()
        {
            bool show = false;
            try
            {
                string useBgUploader = System.Web.Configuration.WebConfigurationManager.AppSettings["UseBackgroundUploader"];
                if (useBgUploader != null && useBgUploader.Length > 0)
                {
                    show = bool.Parse(useBgUploader.Trim());
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in getting the UseBackGroundHelper value ", ex);
            }
            return show;
        }

        /// <summary>
        /// Build the regex for parsing the engine command or delete command 
        /// from spaces config. This is to get the version number of the 
        /// performance engine being usd for a particular space type
        /// </summary>
        /// <returns></returns>

        public static Regex getRegexForSpacesConfigCommandVersion()
        {
            if (spacesConfigCommandVersionRegex == null)
            {
                string regexExpression = @"^.*\\(.*)\\bin";
                spacesConfigCommandVersionRegex = new Regex(regexExpression);
            }

            return spacesConfigCommandVersionRegex;
        }

        public static Regex getRegexForTrialUserEmail()
        {
            if (trialUserEmailAddressRegex == null)
            {
                string regexExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                trialUserEmailAddressRegex = new Regex(regexExpression);
            }

            return trialUserEmailAddressRegex;
        }

        public static List<PerformanceEngineVersion> getPerformanceEngineDetails(HttpSessionState session)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                return null;
            }

            bool showOnlyVisibleVersions = !u.OperationsFlag;
            return getPerformanceEngineDetails(showOnlyVisibleVersions);
        }
        /// <summary>
        /// Return the Performance Engine Versions that can be used by the user
        /// to assign the space to.
        /// </summary>
        /// <param name="session">session object of the logged in user</param>
        /// <param name="showOnlyVisibleVersions">If true, returns only the Version with Visible property set to true in the database</param>
        /// <returns></returns>
        public static List<PerformanceEngineVersion> getPerformanceEngineDetails(bool showOnlyVisibleVersions)
        {
            QueryConnection conn = null;
            List<PerformanceEngineVersion> filteredPerformanceEngineVersionsList = new List<PerformanceEngineVersion>();
            try
            {
                conn = ConnectionPool.getConnection();
                List<PerformanceEngineVersion> allPerformanceEngineVersionsList = Database.getPerformanceEngineVersions(conn, Util.getMainSchema());
                // filter the visible releases only based on the boolean flag supplied
                if (showOnlyVisibleVersions)
                {
                    if (allPerformanceEngineVersionsList != null && allPerformanceEngineVersionsList.Count > 0)
                    {
                        foreach (PerformanceEngineVersion pe in allPerformanceEngineVersionsList)
                        {
                            if (pe.Visible)
                            {
                                filteredPerformanceEngineVersionsList.Add(pe);
                            }
                        }
                    }
                }
                else
                {
                    filteredPerformanceEngineVersionsList = allPerformanceEngineVersionsList;
                }
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                catch (Exception)
                {
                }
            }

            return filteredPerformanceEngineVersionsList;
        }

        public static PerformanceEngineVersion getPerformanceEngineVersion(Space sp, string loadGroup)
        {
            if (loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
            {
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                LocalETLConfig leConfig = null;
                if (localETLConfigs != null && localETLConfigs.ContainsKey(loadGroup))
                {
                    leConfig = localETLConfigs[loadGroup];
                }
                else
                {
                    throw new Exception("Local ETL configuration issue, no matching local ETL config found for load group: " + loadGroup);
                }
                return LiveAccessUtil.getLocalProcessingEngineVersion(sp, leConfig.getLocalETLConnection());
            }
            else
            {
                return Util.getPerformanceEngineVersion(sp);
            }
        }

        public static void updateSpaceSFDCVersion(Space sp, int sfdcVersionId, bool forceUpdate)
        {
            QueryConnection conn = null;
            try
            {
                if (sp != null && sp.ID != Guid.Empty)
                {
                    conn = ConnectionPool.getConnection();
                    string mainSchema = Util.getMainSchema();
                    bool update = forceUpdate;
                    if (!forceUpdate)
                    {
                        int currentSFDCVersionID = Database.getSpaceSFDCVersionID(conn, mainSchema, sp.ID);
                        // if there is already a non-null sfdc version id, then we don't update it
                        // unless it a force update
                        update = currentSFDCVersionID > 0 ? false : true;
                    }

                    if (update)
                    {
                        if (Database.updateSpaceSFDCVersion(conn, mainSchema, sp, sfdcVersionId))
                        {
                            sp.SFDCVersionID = sfdcVersionId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while update space availability for space " + sp.ID, ex);
            }
            finally
            {
                try
                {
                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Error while releasing connection", ex2);
                }
            }
        }

        public static int getDefaultSFDCVersion()
        {
            if (sfdcDefaultVersionId <= 0)
            {
                string obj = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultSFDCVersionID"];
                if (obj != null)
                {
                    string sfdcDefaultVersionIdString = (string)obj;
                    int parseValue = 0;
                    if (int.TryParse(sfdcDefaultVersionIdString, out parseValue))
                    {
                        sfdcDefaultVersionId = parseValue;
                    }
                }
            }

            return sfdcDefaultVersionId > 0 ? sfdcDefaultVersionId : -1;
        }

        public static PerformanceEngineVersion getSFDCEnabledPerformanceEngineVersion(Space sp)
        {
            PerformanceEngineVersion extractVersion = null;
            if (sp.SFDCVersionID > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    String mainSchema = Util.getMainSchema();
                    extractVersion = Database.getPerformanceEngineVersionForSpace(conn, mainSchema, sp.SFDCVersionID);
                }
                finally
                {
                    if (conn != null)
                    {
                        try
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Warn(ex2);
                        }
                    }
                }
                if (extractVersion == null)
                {
                    Global.systemLog.Warn("Could not find processing engine " + sp.ProcessVersionID + " for space " + sp.ID + " in the " + Database.PROCESS_VERSIONS_TABLE + " table");
                }
            }

            return extractVersion;

        }

        /// <summary>
        /// only if peVersion is not defined ie it is equal to -1, fill full details
        /// otherwise just fill up ID parameter
        /// </summary>
        /// <param name="processVersionID"></param>
        /// <returns></returns>
        public static PerformanceEngineVersion getPerformanceEngineVersion(Space sp)
        {
            PerformanceEngineVersion peVersion = new PerformanceEngineVersion();

            if (sp.ProcessVersionID <= 0)
            {
                peVersion.ID = Util.SPACE_PROCESS_VERSION_UNDEFINED;
                // If the space does not have performance engine version explictly set, 
                // infer it from from the spaces config file.
                // This is to ensure that even if the Process Version Table does not contain the engine command for the logged in space
                // we should still be able to show the user the correct version that they are currently using            
                string versionName = Util.getVersionNameFromSpacesConfigCommands(sp);

                // For dev machines, versionName might be null because it doesn't follow the standard
                // that prod/qa machines follow. In that case, just display "empty" string to the user

                peVersion.Name = versionName == null ? "" : versionName;
                peVersion.Release = versionName == null ? "" : versionName;
                peVersion.Visible = true;
                peVersion.SupportsRetryLoad = false;
                peVersion.IsProcessingGroupAware = false;
            }
            else
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    peVersion = Database.getPerformanceEngineVersionForSpace(conn, Util.getMainSchema(), sp.ProcessVersionID);
                }
                finally
                {
                    if (conn != null)
                    {
                        try
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Warn(ex2);
                        }
                    }
                }
            }
            if (peVersion == null)
            {
                Global.systemLog.Warn("Could not find processing engine " + sp.ProcessVersionID + " for space " + sp.ID + " in the " + Database.PROCESS_VERSIONS_TABLE + " table");
            }
            return peVersion;
        }


        /// <summary>
        /// Check to see if the version name supplied is already present in the input List        /// 
        /// </summary>
        /// <param name="peVersionList">Input List used for searching</param>
        /// <param name="versionName">Version Name to search</param>
        /// <param name="compareItWithRelease">Check the supplied version name against both the Name and Release parameter of the PerformanceEngineVersion</param>
        /// <returns></returns>
        public static bool isVersionAlreadyPresent(List<PerformanceEngineVersion> peVersionList, string versionName, bool compareItWithRelease)
        {
            bool present = false;
            if (versionName != null && versionName.Length > 0
                && peVersionList != null && peVersionList.Count > 0)
            {
                foreach (PerformanceEngineVersion peVersion in peVersionList)
                {
                    if (peVersion.Name.ToLower().Equals(versionName.ToLower()))
                    {
                        present = true;
                        break;
                    }

                    if (compareItWithRelease && peVersion.Release.ToLower().Equals(versionName.ToLower()))
                    {
                        present = true;
                        break;
                    }
                }
            }
            return present;
        }

        public static string getVersionNameFromSpacesConfigCommands(Space sp)
        {
            string peVersion = null;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            if (sc != null)
            {
                // Parse both engine command and delete command to make sure 
                // that versions returned are same
                String engineCommandVersion = parseAndGetVersionFromPECommand(sc.EngineCommand);
                String deleteCommandVersion = parseAndGetVersionFromPECommand(sc.DeleteDataCommand);
                if (engineCommandVersion != null && deleteCommandVersion != null
                    && engineCommandVersion.Equals(deleteCommandVersion))
                {
                    peVersion = engineCommandVersion;
                }
                else
                {
                    Global.systemLog.Warn(sc.EngineCommand + "  AND  " + sc.DeleteDataCommand + " refers to different versions ");
                }

            }
            return peVersion;
        }

        public static string parseAndGetVersionFromPECommand(string processVersionCommand)
        {
            string peVersionName = null;

            // e.g. the command looks like C:\Birst\4.3.8\bin\load_warehouse.bat
            if (processVersionCommand != null && processVersionCommand.Length > 0)
            {
                Regex regex = Util.getRegexForSpacesConfigCommandVersion();
                Match match = regex.Match(processVersionCommand);
                if (match.Success)
                {
                    peVersionName = match.Groups[1].Value;
                }
                /*
                int indexOfBinPart = processVersionCommand.IndexOf("\\bin\\");
                string commandPartWithVersion = processVersionCommand.Substring(0, indexOfBinPart); // indexOfBinPart also servs as the length of substring
                int startIndexOfVersionPart = commandPartWithVersion.LastIndexOf("\\");
                peVersionName = commandPartWithVersion.Substring(startIndexOfVersionPart);                
                 */
            }
            return peVersionName;
        }


        public static string getExtractionEngineCommand(Space sp, int engineVersion)
        {
            string extractionEngineCommandFileName = engineVersion > 0 ? getEngineCommandFileName(engineVersion) : getEngineCommandFileName(sp.SFDCVersionID);
            if (extractionEngineCommandFileName != null && extractionEngineCommandFileName.Length > 0)
            {
                FileInfo fi = new FileInfo(extractionEngineCommandFileName);
                if (fi.Exists)
                    return extractionEngineCommandFileName;
                else
                    throw new Exception("Extraction Engine command for the space is specified, but the file does not exist: " + extractionEngineCommandFileName);
            }
            else
            {
                throw new Exception("Invalild value of SFDC Extraction Engine specified for space : " + sp.SFDCVersionID);
            }
        }

        public static int getProcessingEngineVersion(Space sp)
        {
            //check processing engine version
            int engineVersionInfo = 0;
            try
            {
                string engineVersionInfoCommand = Util.getVersionInfoCommand(sp);
                // Create the ProcessInfo object
                ProcessStartInfo psi = new ProcessStartInfo(engineVersionInfoCommand);
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.RedirectStandardOutput = true;
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardInput = false;
                psi.RedirectStandardError = true;
                psi.WorkingDirectory = (new FileInfo(engineVersionInfoCommand)).Directory.FullName;

                Process proc = Process.Start(psi);
                string line = null;
                bool parsedValue = false;
                while ((line = proc.StandardError.ReadLine()) != null)
                {
                    Global.systemLog.Warn(line);
                }
                while ((line = proc.StandardOutput.ReadLine()) != null)
                {
                    if (Int32.TryParse(line, out engineVersionInfo))
                    {
                        Global.systemLog.Debug("Successfully parsed processing engine version - " + engineVersionInfo);
                        parsedValue = true;
                    }
                    else
                    {
                        Global.systemLog.Debug("Could not parse value '" + line + "' for processing engine version");
                    }
                }
                if (!proc.HasExited)
                {
                    proc.WaitForExit();
                }
                if (!parsedValue)
                {
                    Global.systemLog.Debug("Could not parse processing engine version, using defaultvalue - " + engineVersionInfo);
                }
                proc.Close();
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message, ex);
                Global.systemLog.Debug("Unable to find version info for processing engine, using defaultvalue - " + engineVersionInfo);
            }

            return engineVersionInfo;
        }

        public static string getEngineCommand(Space sp)
        {
            FileInfo fi = null;
            string engineCommandFileName = getEngineCommandFileName(sp.ProcessVersionID);
            if (engineCommandFileName != null && engineCommandFileName.Length > 0)
            {
                fi = new FileInfo(engineCommandFileName);
                if (fi.Exists)
                    return engineCommandFileName;
                else
                    throw new Exception("Engine command for the space is specified, but the file does not exist: " + engineCommandFileName);
            }
            SpaceConfig sc = getSpaceConfiguration(sp.Type);
            if (sc == null)
                throw new Exception("Could not find the Engine command, bad space type: " + sp.Type);
            fi = new FileInfo(sc.EngineCommand);
            if (fi.Exists)
                return sc.EngineCommand;
            throw new Exception("Engine command for the space type is specified, but the file does not exist: " + sp.Type + "/" + sc.EngineCommand);
        }

        public static string getEngineCommandFileName(Space sp)
        {
            return getEngineCommandFileName(sp.ProcessVersionID);
        }

        public static string getEngineCommandFileName(int processVersionID)
        {
            string engineCommand = null;
            QueryConnection conn = null;
            if (processVersionID > 0)
            {
                try
                {
                    conn = ConnectionPool.getConnection();
                    PerformanceEngineVersion peVersion = Database.getPerformanceEngineVersionForSpace(conn, Util.getMainSchema(), processVersionID);
                    if (peVersion != null)
                    {
                        engineCommand = peVersion.EngineCommand;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in getting process info for process version id " + processVersionID, ex);
                }
                finally
                {
                    try
                    {
                        if (conn != null)
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Error in releasing connection ", ex2);
                    }
                }
            }
            return engineCommand;
        }

        public static string getVersionInfoCommand(Space sp)
        {
            FileInfo fi = null;
            string versionInfoCommandFileName = getVersionInfoCommandFileName(sp.ProcessVersionID);
            if (versionInfoCommandFileName != null && versionInfoCommandFileName.Length > 0)
            {
                fi = new FileInfo(versionInfoCommandFileName);
                if (fi.Exists)
                    return versionInfoCommandFileName;
                else
                    throw new Exception("VersionInfo command for the space is specified, but the file does not exist: " + versionInfoCommandFileName);
            }
            SpaceConfig sc = getSpaceConfiguration(sp.Type);
            if (sc == null)
                throw new Exception("Could not find the Engine command, bad space type: " + sp.Type);
            fi = new FileInfo(sc.EngineCommand);
            fi = new FileInfo(Path.Combine(fi.DirectoryName, "versioninfo.bat"));
            if (fi.Exists)
                return sc.EngineCommand;
            throw new Exception("VersionInfo command for the space type is specified, but the file does not exist: " + sp.Type + "/" + fi.FullName);
        }

        public static string getVersionInfoCommandFileName(Space sp)
        {
            return getVersionInfoCommandFileName(sp.ProcessVersionID);
        }

        public static string getVersionInfoCommandFileName(int processVersionID)
        {
            string versionInfoCommand = null;
            QueryConnection conn = null;
            if (processVersionID > 0)
            {
                try
                {
                    conn = ConnectionPool.getConnection();
                    PerformanceEngineVersion peVersion = Database.getPerformanceEngineVersionForSpace(conn, Util.getMainSchema(), processVersionID);
                    if (peVersion != null)
                    {
                        FileInfo fi = new FileInfo(peVersion.EngineCommand);
                        versionInfoCommand = Path.Combine(fi.DirectoryName, "versioninfo.bat");
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in getting process info for process version id " + processVersionID, ex);
                }
                finally
                {
                    try
                    {
                        if (conn != null)
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Error in releasing connection ", ex2);
                    }
                }
            }
            return versionInfoCommand;
        }

        public static string getDeleteCommandFileName(Space sp)
        {
            return getDeleteCommandFileName(sp.ProcessVersionID);
        }

        public static string getDeleteCommandFileName(int processVersionID)
        {
            string deleteCommand = null;
            if (processVersionID > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    PerformanceEngineVersion peVersion = Database.getPerformanceEngineVersionForSpace(conn, Util.getMainSchema(), processVersionID);
                    if (peVersion != null)
                    {
                        deleteCommand = peVersion.DeleteCommand;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in getting process info for process version id " + processVersionID, ex);
                }
                finally
                {
                    try
                    {
                        if (conn != null)
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Error in releasing connection ", ex2);
                    }
                }
            }
            return deleteCommand;
        }

        public static string getDeleteDataCommand(Space sp)
        {
            FileInfo fi = null;
            string deleteCommandFileName = getDeleteCommandFileName(sp.ProcessVersionID);
            if (deleteCommandFileName != null && deleteCommandFileName.Length > 0)
            {
                fi = new FileInfo(deleteCommandFileName);
                if (fi.Exists)
                    return deleteCommandFileName;
                else
                    throw new Exception("Delete data command for the space is specified, but the file does not exist: " + deleteCommandFileName);
            }
            SpaceConfig sc = getSpaceConfiguration(sp.Type);
            if (sc == null)
                throw new Exception("Could not find the delete data command, bad space type: " + sp.Type);
            fi = new FileInfo(sc.DeleteDataCommand);
            if (fi.Exists)
                return sc.DeleteDataCommand;
            throw new Exception("Delete data command for the space type is specified, but the file does not exist: " + sp.Type + "/" + sc.DeleteDataCommand);
        }

        public static string getSharedToken(string url)
        {
            WebRequest wrq = WebRequest.Create(url);

            WebResponse wresp = null;
            try
            {
                wresp = wrq.GetResponse();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return (null);
            }
            StreamReader sr = new StreamReader(wresp.GetResponseStream(), System.Text.Encoding.ASCII);
            string result = sr.ReadToEnd();
            sr.Close();
            wresp.Close();
            return (result);
        }

        public static string getHexString(byte[] bytearr)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in bytearr)
                sb.Append(String.Format("{0:x02}", b));
            return (sb.ToString());
        }

        private static byte getByteFromHexChar(char c)
        {
            if (c >= '0' && c <= '9')
                return ((byte)(c - '0'));
            return ((byte)(c - 'a' + 10));
        }

        public static byte[] getBytesFromHex(string s)
        {
            s = s.ToLower();
            byte[] arr = new byte[s.Length / 2];
            for (int i = 0; i < s.Length / 2; i++)
            {
                arr[i] = (byte)(getByteFromHexChar(s[i * 2]) * 16 + getByteFromHexChar(s[i * 2 + 1]));
            }
            return (arr);
        }

        public static string getShortDateString(DateTime date, string dbType)
        {
            string date_format = dbType != null && dbType == Database.INFOBRIGHT ? "yyyy-MM-dd" : DEFAULT_DATE_FORMAT;
            return date.ToString(date_format);
        }

        public static string getDateTimeInISOFormat(DateTime date)
        {
            return date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        }

        public static void endSession(HttpSessionState session)
        {
            if (session["AdminDirty"] != null)
            {
                Performance_Optimizer_Administration.MainAdminForm maf = (Performance_Optimizer_Administration.MainAdminForm)session["MAF"];
                if (maf == null)
                    return;
                Space sp = (Space)session["space"];
                // bugfix 7466 - Making sure that application is built if required.
                if (sp != null)
                {
                    if (session["RequiresRebuild"] != null)
                    {
                        buildApplication(maf, mainSchema, DateTime.MinValue, -1, null, sp, session);
                    }
                }

                session.Remove("AdminDirty");
                session.Remove("RequiresRebuild");
                if (sp == null)
                    return;
                saveApplication(maf, sp, session, null);
            }
        }

        public static bool isSpaceAdmin(User u, Space sp, HttpSessionState session)
        {
            SpaceMembership sm = Util.getSpaceMembership(session, u, sp);
            bool isAdmin = sm != null ? sm.Administrator || sm.Owner : false;
            return isAdmin;
        }

        public static string getASPNETSessionID(HttpRequest request)
        {
            return request.Cookies.Get(sessionIdName).Value;
        }

        public static string getSMILoginToken(string username, Space sp)
        {
            return username + ',' + sp.Directory + ',' + sp.ID;
        }

        public static bool getSMILoginSession(HttpSessionState session, Space sp, String token,
            HttpResponse response, HttpRequest request)
        {
            return getSMILoginSession(session, sp, token, response, request, true, Util.getSessionMAF(session));
        }

        // Login and Create SMIWeb session. 
        // fillSMIWebSession = false creates SMIWeb session WITHOUT population of session variables.
        // Needed for faster session creation time for CatalogManagement and Custom Subject Area on Flex Admin

        public static bool getSMILoginSession(HttpSessionState session, Space sp, String token,
            HttpResponse response, HttpRequest request, bool fillSMIWebSession, MainAdminForm maf)
        {
            try
            {
                bool forceSMIWebLogout = false;
                if (session["requiresLogout"] != null)
                {
                    forceSMIWebLogout = true;
                }

                if (!fillSMIWebSession && session["SMIWebLoginNoSessionVars"] == null)
                {
                    session["SMIWebLoginNoSessionVars"] = true;
                    forceSMIWebLogout = true;
                }
                else if (fillSMIWebSession && session["SMIWebLoginNoSessionVars"] != null)
                {
                    // if the earlier session created did not fill SMI Session Variables and the current required one done, 
                    forceSMIWebLogout = true;
                    session.Remove("SMIWebLoginNoSessionVars");
                }
                // check to see if the state is dirty and we need to force logout
                if (forceSMIWebLogout)
                {
                    logoutSMIWebSession(session, sp, request);
                }

                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                CookieContainer cookieContainer = (CookieContainer)session["SMICookie"];
                String wsRequestSessionId = null;

                if (cookieContainer == null)
                {
                    cookieContainer = new CookieContainer();
                }
                else
                {
                    wsRequestSessionId = getStoredSession(session, sp);

                    if (wsRequestSessionId == null)
                    {
                        cookieContainer = new CookieContainer();
                    } // do the paranoid check on the stored JSESSIONID and the one from incoming request                    
                    else if (wsRequestSessionId != null && !paranoidCheck(wsRequestSessionId, request))
                    {
                        // incoming request and stored session do not match. Create a new session 
                        // and log out cleanly from the stored one
                        logoutSMIWebSession(session, sp, request);
                        cookieContainer = new CookieContainer();
                    }
                }

                // get superuserflag if any on user object
                bool isSuperUser = Util.isTypeSuperUser(session);
                SpaceMembership sm = Util.getSpaceMembership(session, null, sp);
                bool isSpaceAdmin = sm != null ? sm.Administrator || sm.Owner : false;
                User u = (User)session["user"];

                string securityFilterSessionVars = getSessionVarsFromSSORequest(session);

                SMIWebLogin smiWebLogin = new SMIWebLogin();

                SMILogin(smiWebLogin, token, cookieContainer, isSuperUser, isSpaceAdmin, securityFilterSessionVars, fillSMIWebSession, u, sp, maf, session, request);

                Uri serviceUri = new Uri(smiWebLogin.Url);
                bool newSession = false;
                foreach (Cookie c in cookieContainer.GetCookies(serviceUri))
                {
                    if (c.Name.Equals("JSESSIONID") && (wsRequestSessionId == null || !c.Value.Equals(wsRequestSessionId)))
                    {
                        HttpCookie httpCookie = new HttpCookie(c.Name);
                        httpCookie.Value = Util.removeCRLF(c.Value);
                        // Catalog Management and Custom SA resides on Admin Flex UI
                        // Commenting the setting of the path of Cookie to "/SMIWeb", otherwise each click on
                        // Catalog Management or Custom SA will not send the session id and hence unneeded
                        // re-logout and re-login to SMIWeb.
                        //httpCookie.Path = c.Path;
                        httpCookie.HttpOnly = c.HttpOnly;
                        httpCookie.Secure = c.Secure;
                        response.Cookies.Add(httpCookie);
                        newSession = true;
                    }
                }

                WebResponse res = smiWebLogin.getServiceReturnResponse();
                if (newSession)
                {
                    addP3PHeader(response, res);
                }

                try
                {
                    res.Close();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Not able to close response object: ", ex);
                }
                session["SMICookie"] = cookieContainer;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error while logging in to SMIWeb ", e);
                return false;
            }
            return true;
        }

        public static List<string> getSMIWebLoginCredentials(Space sp, User u, MainAdminForm maf, HttpSessionState session,
            bool isSuperUser, bool isSpaceAdmin, bool fillSMIWebSession, string securityFilterSessionVars)
        {
            // Other params are passed in the login webservice signature as keyValuePairs
            List<String> loginCredentials = new List<String>();
            loginCredentials.Add("superUser=" + isSuperUser.ToString().ToLower());

            loginCredentials.Add("spaceAdmin=" + isSpaceAdmin.ToString().ToLower());

            // fillSession = false creates a SMIWeb session without populating any session variables                 
            loginCredentials.Add("fillSession=" + fillSMIWebSession.ToString().ToLower());
            if (securityFilterSessionVars != null)
            {
                loginCredentials.Add("sessionVars=" + securityFilterSessionVars);
            }

            List<PackageSpaceProperties> packageSpacesProperties = Util.getImportedPackageSpaces(sp, maf);
            string spacesInfoHeaderString = Util.getSpacesInfoString(packageSpacesProperties);
            if (spacesInfoHeaderString != null && spacesInfoHeaderString.Length > 0)
            {
                loginCredentials.Add(spacesInfoHeaderString);
            }

            loginCredentials.Add("SpaceID=" + sp.ID);
            loginCredentials.Add("DiscoverySpace=" + sp.DiscoveryMode.ToString().ToLower());

            if (session != null && session["SFDC_sessionId"] != null)
            {
                loginCredentials.Add("SFDC_sessionId=" + session["SFDC_sessionId"]);
                loginCredentials.Add("SFDC_serverURL=" + session["SFDC_serverURL"]);
            }

            string pageTitle = "Birst";
            if (u != null)
            {
                loginCredentials.Add("UserID=" + u.ID.ToString());
                loginCredentials.Add("FreeTrial=" + u.isFreeTrialUser.ToString());
                loginCredentials.Add("UserName=" + u.Username);
                loginCredentials.Add("DiscoveryFreeTrial=" + u.isDiscoveryFreeTrial);
                // Get user preferences for locale, using global as default if necessary
                IDictionary<string, string> localePreferences = ManagePreferences.getPreference(null, u.ID, sp.ID, "Locale");
                loginCredentials.Add("Locale=" + localePreferences["Locale"]);
                // Get user-level preference for timezone, no global default
                IDictionary<string, string> tzPreferences = ManagePreferences.getUserPreference(null, u.ID, "TimeZone");
                if (tzPreferences.ContainsKey("TimeZone") && tzPreferences["TimeZone"] != null)
                {
                    loginCredentials.Add("TimeZone=" + tzPreferences["TimeZone"]);
                }
                if (u.actDetails != null && u.actDetails.OverrideParameters != null)
                {
                    if (u.actDetails.OverrideParameters.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                        pageTitle = u.actDetails.OverrideParameters[OverrideParameterNames.PAGE_TITLE];

                    bool enableAsyncReportGeneration = false;
                    if (u.actDetails.OverrideParameters.ContainsKey(OverrideParameterNames.ENABLE_ASYNC_REPORT_GENERATION))
                    {
                        bool.TryParse(u.actDetails.OverrideParameters[OverrideParameterNames.ENABLE_ASYNC_REPORT_GENERATION], out enableAsyncReportGeneration);
                    }
                    loginCredentials.Add("EnableAsyncReportGeneration=" + enableAsyncReportGeneration);
                }
            }

            loginCredentials.Add("PageTitle=" + pageTitle);
            return loginCredentials;
        }


        public static bool SMILogin(SMIWebLogin smiWebLogin, string token, CookieContainer cookieContainer,
            bool isSuperUser, bool isSpaceAdmin, string securityFilterSessionVars, bool fillSMIWebSession, User u, Space sp, MainAdminForm maf, HttpSessionState session, HttpRequest request)
        {
            // build application if needed
            Util.reBuildIfRequired(session, maf, sp, true);
            // Only ssoUser is passed in the header. ssoUser is specifically checked from header
            // on the SMIWeb side for authentication. Once we remove the authentication from SMIWeb
            // side, this should go away.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);

            NameValueCollection requestHeaders = new NameValueCollection();
            requestHeaders.Add("ssoUser", Util.encodeForHeader(Util.removeCRLF(token)));

            // Other params are passed in the login webservice signature as keyValuePairs
            List<String> loginCredentials = getSMIWebLoginCredentials(sp, u, maf, session, isSuperUser, isSpaceAdmin, fillSMIWebSession, securityFilterSessionVars);

            if (session != null)
            {
                loginCredentials.Add("ASP.NET_SessionId=" + session.SessionID);
            }
            if (request != null)
            {
                //acornroot = Server.MapPath("~").Replace('\\', '/');
                loginCredentials.Add("AcornRoot=" + request.MapPath("~").Replace('\\', '/'));
                //schedulerRoot = Acorn.Utils.SchedulerUtils.getSchedulerRootIpAddress(Request);
                string schedulerRoot = Util.getRequestBaseURL(request);
                if (useisapi != null && !bool.Parse(useisapi))
                {
                    schedulerRoot = Acorn.Utils.SchedulerUtils.getSchedulerRootIpAddress(request);
                }
                loginCredentials.Add("SchedulerRoot=" + schedulerRoot);
                //                loginCredentials.Add("SchedulerRoot=" + Acorn.Utils.SchedulerUtils.getSchedulerRootIpAddress(request));
            }
            //bingMaps = Util.hasBingMaps(userID).ToString().ToLower();
            loginCredentials.Add("BingMaps=" + Util.hasBingMaps(u.ID).ToString().ToLower());
            //this.gaAccount = gaTrackingAccount;
            loginCredentials.Add("GaTrackingAccount=" + FlexModule.gaTrackingAccount);
            loginCredentials.Add("VisualizerEnabled=" + Util.hasVisualizerEnabled(u).ToString().ToLower());
            if (request != null && request.Cookies != null)
            {
                foreach (String name in request.Cookies.Keys)
                {
                    if (name.ToLower() == ".aspxformsauth")
                    {
                        HttpCookie cookie = request.Cookies.Get(name);
                        if (cookie != null)
                        {
                            loginCredentials.Add("aspxformsauth=" + cookie.Value);
                            break;
                        }
                    }
                }
            }

            UserAndGroupUtils.updatedUserGroupMappingFile(sp);
            smiWebLogin.setRequestHeaders(requestHeaders);
            String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            smiWebLogin.Url = localprotocol + sc.LocalURL + loginServiceEndPoint;
            // Global.systemLog.Debug("SMIWeb: " + localprotocol + ", " + sc.LocalURL + ", " + smiWebLogin.Url);
            smiWebLogin.CookieContainer = cookieContainer;
            // call the Login webservice and login operation

            String smiWebResponse = null;
            try
            {
                smiWebResponse = smiWebLogin.login(loginCredentials.ToArray());
                if (smiWebResponse != null && smiWebResponse == "true")
                {
                    Global.systemLog.Info("Successful login to SMIWeb for token: " + token);
                }
                else
                {
                    Global.systemLog.Warn("Failed to login to SMIWeb (" + smiWebLogin.Url + ") for token: " + token + ", with response: " + smiWebResponse);
                    return false;
                }
            }
            catch (WebException wex)
            {
                Global.systemLog.Error("Error while logging in to SMIWeb: " + smiWebLogin.Url, wex);
                return false;
            }

            return true;
        }

        private static bool hasVisualizerEnabled(User u)
        {
            if (u != null && u.ManagedAccountId != null && u.ManagedAccountId != Guid.Empty)
            {
                AccountDetails accountDetails = null;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    accountDetails = Database.getAccountDetails(conn, Util.mainSchema, u.ManagedAccountId, u);
                    return accountDetails != null && accountDetails.hasVisualizerEnabled();
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return false;
        }

        public static string encodeForHeader(string p)
        {
            byte[] b = System.Text.Encoding.UTF8.GetBytes(p);
            return Convert.ToBase64String(b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="repositoryType">either repostiory.xml or repostiory_dev.xml</param>
        public static void touchRepository(Space sp, String repositoryTypeFileName)
        {
            try
            {
                if (sp != null && (repositoryTypeFileName == REPOSITORY_DEV || repositoryTypeFileName == REPOSITORY_PROD))
                {
                    string fileName = Path.Combine(sp.Directory, repositoryTypeFileName);
                    FileInfo fileInfo = new FileInfo(fileName);
                    fileInfo.LastWriteTime = fileInfo.LastAccessTime = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while touching the repository file ", ex);
            }
        }

        public static string getSpacesInfoString(List<PackageSpaceProperties> packageSpacePropertiesList)
        {
            if (packageSpacePropertiesList != null && packageSpacePropertiesList.Count > 0)
            {
                StringBuilder packageSpacesInfoBuilder = new StringBuilder();
                foreach (PackageSpaceProperties packageSpaceProperties in packageSpacePropertiesList)
                {
                    if (packageSpacesInfoBuilder.Length > 0)
                    {
                        packageSpacesInfoBuilder.Append(";");
                    }
                    string spaceInfo = getSpacePropertiesStringForWebService(packageSpaceProperties);
                    packageSpacesInfoBuilder.Append(spaceInfo);
                }

                return "PackageSpacesInfo=" + packageSpacesInfoBuilder.ToString();
            }

            return null;
        }

        public static string[] getSpacesInfoArray(List<PackageSpaceProperties> packageSpacePropertiesList)
        {
            if (packageSpacePropertiesList != null && packageSpacePropertiesList.Count > 0)
            {
                List<string> responseList = new List<string>();
                foreach (PackageSpaceProperties packageSpaceProperties in packageSpacePropertiesList)
                {
                    responseList.Add(getSpacePropertiesStringForWebService(packageSpaceProperties));
                }
                return responseList.ToArray();
            }
            return null;
        }

        private static string getSpacePropertiesStringForWebService(PackageSpaceProperties packageSpaceProperties)
        {
            string packageSpaceInfo = null;
            if (packageSpaceProperties != null)
            {
                packageSpaceInfo = "packageID=" + packageSpaceProperties.PackageID.ToString() + "," +
                    "packageName=" + packageSpaceProperties.PackageName + "," +
                    "spaceID=" + packageSpaceProperties.SpaceID.ToString() + "," +
                    "spaceDirectory=" + packageSpaceProperties.SpaceDirectory.ToString() + "," +
                    "spaceName=" + packageSpaceProperties.SpaceName.ToString();
            }
            return packageSpaceInfo;
        }

        public static bool paranoidCheck(String storedSessionID, HttpRequest request)
        {
            return paranoidCheck(storedSessionID, request, null);
        }

        public static bool paranoidCheck(String storedSessionID, HttpRequest request, String cookieName)
        {
            if (storedSessionID == null)
            {
                return false;
            }

            cookieName = cookieName == null ? "JSESSIONID" : cookieName;
            bool result = false;
            HttpCookieCollection httpCookies = request.Cookies;

            HttpCookie requestCookie = httpCookies.Get(cookieName);
            if (requestCookie != null)
            {
                // check that the JSESSIONIDs are same in request and stored one
                if (requestCookie.Value.Equals(storedSessionID))
                {
                    result = true;
                }
            }
            else
            {
                // no cookie in the request. need to create a new login session.
                result = false;
            }
            return result;
        }

        /*
        public static bool paranoidCheck(String storedSessionID, HttpRequest request)
        {
            if (storedSessionID == null)
            {
                return false;
            }

            bool result = false;
            HttpCookieCollection httpCookies = request.Cookies;

            HttpCookie requestSMICookie = httpCookies.Get("JSESSIONID");
            if (requestSMICookie != null)
            {
                // check that the JSESSIONIDs are same in request and stored one
                if (requestSMICookie.Value.Equals(storedSessionID))
                {
                    result = true;
                }
            }
            else
            {
                // no cookie in the request. need to create a new login session.
                result = false;
            }
            return result;
        }
        */

        public static void addP3PHeader(HttpResponse serverResponse, WebResponse clientResponse)
        {
            if (serverResponse == null || clientResponse == null)
            {
                return;
            }
            WebHeaderCollection webheader = clientResponse.Headers;
            String p3pValue = webheader.Get("P3P");
            if (p3pValue != null)
            {
                serverResponse.AppendHeader("P3P", removeCRLF(p3pValue));
            }
        }

        /**
         * make sure that the file is actually inside of the directory
         * - stops people from trying to ../../...... outside of sp.Directory
         */
        public static void validateFileLocation(string directory, string file)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(directory);
                FileInfo fi = new FileInfo(file);
                if (fi.FullName.ToLower().StartsWith(di.FullName.ToLower()))
                    return;
            }
            catch (IOException)
            {
                Global.systemLog.Warn("Attempt to access file outside of allowed space directory (malformed filename): " + directory + ", " + file);
                throw new NotAllowedException("Attempt to access file outside of allowed space directory (malformed filename)");
            }
            Global.systemLog.Warn("Attempt to access file outside of allowed space directory: " + directory + ", " + file);
            throw new NotAllowedException("Attempt to access file outside of allowed space directory");
        }

        public static void validateSession(HttpSessionState session)
        {
            if (session.IsNewSession)
                throw new UserNotLoggedInException("There no valid session; possible reasons: the session has timed out or the server was restarted.");
        }

        public static void validateAdminUser(HttpSessionState session)
        {
            validateAdminUser(session, null, null);
        }

        public static void validateAdminUser(HttpSessionState session, Space sp, User u)
        {
            validateSession(session);

            string userName = null;
            string spaceID = null;
            try
            {
                if (u == null)
                {
                    u = Util.getSessionUser(session);
                }

                if (sp == null)
                {
                    sp = Util.getSessionSpace(session);
                }

                userName = u != null ? u.Username : null;
                spaceID = sp != null ? sp.ID.ToString() : null;

                if (Util.isSpaceAdmin(u, sp, session))
                {
                    return;
                }
            }
            catch (IOException)
            {
                // Global.systemLog.Warn("Unauthorized attempt to access admin service.  userName : " + userName + " : spaceID : " + spaceID, ex);
                throw new NotAuthorizedException("Unauthorized attempt to access the admin service");
            }
            // Global.systemLog.Warn("Unauthorized attempt to access admin service. User does not have space admin access. userName : " + userName + " : spaceID : " + spaceID);
            throw new NotAuthorizedException("Unauthorized attempt to access the admin service");
        }

        public static void validateAdminOrAccountUser(HttpSessionState session, Space sp, User u)
        {
            validateSession(session);
            string userName = null;
            string spaceID = null;
            try
            {
                if (u == null)
                {
                    u = Util.getSessionUser(session);
                }

                if (sp == null)
                {
                    sp = Util.getSessionSpace(session);
                }

                userName = u != null ? u.Username : null;
                spaceID = sp != null ? sp.ID.ToString() : null;

                if (UserManagementUtils.isUserAuthorized(u) || Util.isSpaceAdmin(u, sp, session))
                {
                    return;
                }
            }
            catch (IOException ex)
            {
                Global.systemLog.Warn("Unauthorized attempt to access admin service.  userName : " + userName + " : spaceID : " + spaceID, ex);
                throw new NotAuthorizedException("Unauthorized attempt to access the admin service");
            }
            Global.systemLog.Warn("Unauthorized attempt to access admin service. User does not have space admin access. userName : " + userName + " : spaceID : " + spaceID);
            throw new NotAuthorizedException("Unauthorized attempt to access the admin service");
        }

        private static String replaceGetVariableWithOldSyntax(String str)
        {
            if (str == null || str.Length == 0)
            {
                return str;
            }
            String modifiedString = str;

            bool found = true;
            while (found)
            {
                String getVariableStringBegin = "GETVARIABLE('";
                String getVariableStringEnd = "')";
                int startIndex = modifiedString.ToUpper().IndexOf(getVariableStringBegin);
                if (startIndex == -1)
                {
                    found = false;
                    break;
                }
                else
                {
                    int endIndex = modifiedString.ToUpper().IndexOf(getVariableStringEnd, startIndex);
                    if (endIndex > 0)
                    {
                        String preGetVariable = modifiedString.Substring(0, startIndex);
                        int indexOfVarNameStart = startIndex + getVariableStringBegin.Length;
                        String getVariable = modifiedString.Substring(indexOfVarNameStart, endIndex - indexOfVarNameStart);
                        String postGetVariable = modifiedString.Substring(endIndex + getVariableStringEnd.Length);
                        modifiedString = preGetVariable + "V{" + getVariable + "}" + postGetVariable;
                    }
                    else
                    {
                        found = false;
                        break;
                    }
                }
            }

            return modifiedString;
        }

        private static Dictionary<string, string> getVariableValues(Space sp, User u)
        {
            if (sp == null)
            {
                return null;
            }

            Dictionary<String, String> response = null;
            // get the variables first
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 5 Minute timeout
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            String variables = ts.getVariables(sp.Directory, u.Username, sp.ID.ToString(), null);
            Global.systemLog.Debug("variables for space " + sp.Name + ":" + variables);
            // variables are pipe-delimited
            string[] variableArray = variables.Split('|');
            if (variableArray != null && variableArray.Length > 0)
            {
                response = new Dictionary<string, string>();
                foreach (String str in variableArray)
                {
                    if (str != null && str.Trim().Length > 0)
                    {
                        string[] varNameValuePair = str.Split('=');
                        if (varNameValuePair != null && varNameValuePair.Length == 2)
                        {
                            response.Add(varNameValuePair[0], varNameValuePair[1]);
                        }
                        else
                        {
                            Global.systemLog.Warn("Invalid variable name value pair returned from Trusted Service fo space " + sp.ID + " : variable : " + varNameValuePair);
                        }
                    }
                }
            }

            return response;
        }

        public static String replaceVariables(Space sp, User u, string query, bool removeQuotesIfAny)
        {
            if (query == null || query.Trim().Length == 0)
            {
                return query;
            }

            String str = query;
            try
            {
                str = replaceGetVariableWithOldSyntax(query);
                if (str.ToUpper().Contains("V{"))
                {
                    StringBuilder sb = new StringBuilder(str);
                    // get variable values from SMIWeb
                    Dictionary<string, string> variableValuesMap = getVariableValues(sp, u);
                    if (variableValuesMap != null && variableValuesMap.Count > 0)
                    {
                        foreach (String varName in variableValuesMap.Keys)
                        {
                            // varName is of the form V{name}
                            if (str.IndexOf(varName) > 0)
                            {
                                String value = variableValuesMap[varName];
                                // remove any quotes around if any
                                if (removeQuotesIfAny && value.StartsWith("'") && value.EndsWith("'"))
                                {
                                    value = value.Substring(1, value.Length - 2);
                                }
                                str = str.Replace(varName, value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while replacing variables in the string : " + query, ex);
                str = query;
            }

            return str;
        }

        // clean up CRLF (for HEADER splitting)
        public static string removeCRLF(string str)
        {
            string clean = str.Replace("\r", string.Empty).Replace("\n", string.Empty);
            return clean;
        }

        public static void logoutSMIWebSession(HttpSessionState session, Space sp, HttpRequest request)
        {
            try
            {
                if (sp == null)
                {
                    Global.systemLog.Warn("Space not given for logging out");
                    return;
                }
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                CookieContainer cookieContainer = (CookieContainer)session["SMICookie"];

                String storedSessionID = getStoredSession(session, sp);
                List<String> smiWebSessionIDs = new List<String>();
                if (storedSessionID != null)
                {
                    smiWebSessionIDs.Add(storedSessionID);
                }

                HttpCookie requestSMICookie = request.Cookies.Get("JSESSIONID");
                if (requestSMICookie != null && storedSessionID != null)
                {
                    if (!requestSMICookie.Value.Equals(storedSessionID))
                    {
                        smiWebSessionIDs.Add(requestSMICookie.ToString());
                    }
                }

                String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SMIWebSession smiWebSession = new SMIWebSession();
                smiWebSession.Url = localprotocol + sc.LocalURL + sessionServiceEndPoint;

                // call the SMIWebSession webservice and logout operation
                foreach (String sessionId in smiWebSessionIDs)
                {
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("Cookie", "JSESSIONID=" + Util.removeCRLF(sessionId));
                    smiWebSession.setRequestHeaders(nvc);
                    SMIWebSessionService.logoutResponse logoutResponse = smiWebSession.logout();

                    if (logoutResponse != null && logoutResponse.@return == "true")
                    {
                        Global.systemLog.Info("Successful logout for SMIWeb session: " + sessionId);
                    }
                    else
                    {
                        Global.systemLog.Warn("Problem in logging out for SMIWeb session: " + sessionId);
                    }
                }

                if (session["SMICookie"] != null)
                {
                    session.Remove("SMICookie");
                }
                if (session["requiresLogout"] != null)
                {
                    session.Remove("requiresLogout");
                }
            }
            catch (Exception e)
            {
                //log the exception
                Global.systemLog.Error("Exception thrown while logging out of SMIWeb ", e);
            }
        }

        public static String getStoredSession(HttpSessionState session, Space sp)
        {
            if (session == null)
                return null;
            SpaceConfig sc = null;
            String storedSessionID = null;
            CookieContainer cookieContainer = (CookieContainer)session["SMICookie"];
            if (cookieContainer == null)
            {
                // No Session stored. Return null
                return storedSessionID;
            }

            if (sp != null)
            {
                sc = getSpaceConfiguration(sp.Type);
            }
            else
            {
                sc = getSpaceConfiguration(0);
            }

            String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            String loginServiceURL = localprotocol + sc.LocalURL + loginServiceEndPoint;
            Uri serviceUri = new Uri(loginServiceURL);

            foreach (Cookie cookie in cookieContainer.GetCookies(serviceUri))
            {
                if (cookie.Name.Equals("JSESSIONID"))
                {
                    storedSessionID = cookie.Value;
                }
            }
            return storedSessionID;
        }

        public static void pollSMIWeb(HttpSessionState session)
        {
            String sessionId = getStoredSession(session, null);
            if (sessionId == null)
            {
                // No session to poll
                return;
            }
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                return;
            }
            SpaceConfig sc = getSpaceConfiguration(sp.Type);

            SMIWebSession smiWebSession = new SMIWebSession();
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("Cookie", "JSESSIONID=" + Util.removeCRLF(sessionId));
            smiWebSession.setRequestHeaders(nvc);
            String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            smiWebSession.Url = localprotocol + sc.LocalURL + sessionServiceEndPoint;
            smiWebSession.poll();
        }

        // shorten long space names for display in the banner
        public static string shortSpaceName(string name)
        {
            string temp = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ShortSpaceNameLength"];
            int ilen = 35;
            if (temp != null && temp.Length > 0)
            {
                if (!int.TryParse(temp, out ilen))
                    ilen = 35;
            }
            if (name != null)
            {
                if (name.Length > ilen)
                {
                    return name.Substring(0, ilen) + "...";
                }
                else
                {
                    return name;
                }
            }
            else
            {
                return "";
            }
        }

        public static bool validatePermissions(HttpSessionState session, User u, string module)
        {
            Space sp = Util.getSessionSpace(session);

            bool moduleAccess = false;
            string accessTag = null;
            if (module == null)
            {
                return moduleAccess;
            }

            if (module == "adhoc" || module == "designer")
            {
                accessTag = "Adhoc";
            }
            else if (module == "systemAdmin")
            {
                accessTag = "Manage";
            }

            if (accessTag == null)
            {
                return moduleAccess;
            }

            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {
                foreach (SpaceGroup spg in sp.SpaceGroups)
                {
                    moduleAccess = UserAndGroupUtils.isGroupACLDefined(spg, accessTag);
                    if (moduleAccess)
                    {
                        break;
                    }
                }
            }

            return moduleAccess;
        }

        public static MainAdminForm validateAndGetMAF(HttpSessionState session)
        {
            MainAdminForm response = getSessionMAF(session);
            if (response == null)
            {
                Global.systemLog.Error("Session MAF unavailable");
                throw new BirstException();
            }
            return response;
        }

        public static User validateAndGetUser(HttpSessionState session)
        {
            User response = getSessionUser(session);
            if (response == null)
            {
                Global.systemLog.Error("Session User unavailable");
                throw new UserNotLoggedInException();
            }
            return response;
        }

        public static Space validateAndGetSpace(HttpSessionState session)
        {
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                Global.systemLog.Error("Space Not found in session");
                throw new BirstException();
            }

            return sp;
        }

        public static Status.StatusResult getSessionStatus(HttpSessionState session)
        {
            Status.StatusResult sr = null;
            if (session["status"] != null)
            {
                sr = (Status.StatusResult)session["status"];
            }
            return sr;
        }

        public static Space getSessionSpace(HttpSessionState session)
        {
            if (Util.TEST_FLAG)
            {
                return (Space)Util.mockSession["space"];
            }

            Space sp = (Space)session["space"];
            return sp;
        }

        public static MainAdminForm getSessionMAF(HttpSessionState session)
        {
            if (Util.TEST_FLAG)
            {
                return (MainAdminForm)Util.mockSession["user"];
            }

            MainAdminForm maf = (MainAdminForm)session["MAF"];
            return maf;
        }

        public static User getSessionUser(HttpSessionState session)
        {
            if (Util.TEST_FLAG)
            {
                return (User)Util.mockSession["user"];
            }
            return (User)session["user"];
        }

        /**
         * get the version # of the Acorn assembly
         */
        public static string getInternalAppVersion()
        {
            string temp = Assembly.Load("Acorn").FullName;
            if (temp == null)
                return "";
            int index1 = temp.IndexOf("Version=") + "Version=".Length;
            if (index1 < 0)
                return "";
            int index2 = temp.IndexOf(", Culture=", index1);
            if (index2 < 0)
                return "";
            return temp.Substring(index1, index2 - index1);
        }

        public static string getAppVersion()
        {
            return Global.appVersion;
        }

        public static string getCopyright()
        {
            string copyRight = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["copyRight"];
            if (copyRight == null)
                copyRight = "";
            return copyRight;
        }

        public static bool validateSpace(Space sp, User u, HttpSessionState session)
        {
            int currentSpaceOpId = SpaceOpsUtils.getSpaceAvailability(sp);
            if (SpaceOpsUtils.isSwapSpaceInProgress(currentSpaceOpId) || SpaceOpsUtils.isNewSpaceBeingCopied(currentSpaceOpId))
            {
                Global.systemLog.Error("Space is busy in either swapping or copying operations : space id " + sp.ID);
                return false;
            }

            string fileName = sp.Directory + "\\repository_dev.xml";
            DateTime lastSpaceLoad = (DateTime)session["spaceloadtime"];
            if (File.GetLastWriteTime(fileName) > lastSpaceLoad)
            {
                Util.setSpace(session, sp, null, false);
                List<SpaceGroup> spgList = sp.SpaceGroups;
                bool found = false;
                if (spgList != null && spgList.Count > 0)
                {
                    foreach (SpaceGroup spg in spgList)
                    {
                        found = UserAndGroupUtils.isUserGroupMember(spg, u.ID);
                        if (found)
                        {
                            break;
                        }
                    }
                }
                if (!found)
                    Global.systemLog.Debug("validateSpace did not find " + u.Username + " in " + sp.Name);
                return (found);
            }
            else
                return (true);
        }

        public static User validateAuth(HttpSessionState session, HttpResponse response)
        {
            object o = (object)session["auth"];
            User u = Util.getSessionUser(session);
            if (o == null || u == null)
            {
                response.Redirect(Util.getLoginPageURL(session));
            }
            return u;
        }

        public static Space validateSpace(HttpSessionState session, HttpResponse response, HttpRequest request)
        {
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                response.Redirect(Util.getHomePageURL(session, request));
            }
            return sp;
        }

        public static string getRequestScheme(HttpRequest request)
        {
            Uri uri = getHeaderURL(request);
            return uri.Scheme;
        }

        public static Uri getHeaderURL(HttpRequest request)
        {
            if (urlHeader != null && urlHeader.Length > 0)
            {
                string temp = request.Headers[urlHeader];
                if (temp != null && temp.Length > 0)
                    return new Uri(temp);
            }
            return request.Url;
        }

        public static string getLoginPageURL(HttpSessionState session)
        {
            if (session != null)
            {
                string masterURL = (string)session["birst.masterURL"];
                if (masterURL != null)
                {
                    return masterURL;
                }
            }
            return "~/Login.aspx";
        }

        public static bool isValidHttpUrl(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                Uri result;
                bool valid = Uri.TryCreate(url, UriKind.Absolute, out result);
                if (result != null && (result.Scheme == Uri.UriSchemeHttp || result.Scheme == Uri.UriSchemeHttps))
                {
                    return true;
                }
            }
            return false;
        }

        public static string getInactivityTimeoutScript(HttpSessionState session)
        {
            if (session != null)
            {
                int timeout = -1;
                User u = Util.getSessionUser(session);
                if (u == null)
                    return null;
                System.Collections.Generic.Dictionary<String, String> overrideParameters = Util.getLocalizedOverridenParams(null, u);
                if (overrideParameters != null && overrideParameters.Count > 0 && overrideParameters.ContainsKey(OverrideParameterNames.INACTIVITY_TIMEOUT))
                {
                    string val = overrideParameters[OverrideParameterNames.INACTIVITY_TIMEOUT];
                    if (val != "")
                        Int32.TryParse(val, out timeout);
                }
                else
                {
                    // backwards compatibility with SAML implementation
                    object timeoutSessionValue = session["SAMLTIMEOUT"];
                    if (timeoutSessionValue != null && timeoutSessionValue is int)
                        timeout = (int) timeoutSessionValue;
                }
                if (timeout > 0)
                {
                    int timeoutInSeconds = timeout * 60;
                    // session timeout redirect script
                    String logoutScript = "<script src=\"/SAMLSSO/jquery-2.0.0.min.js\"></script>\n" +
                        "<script type=\"text/javascript\">\n\tvar tTimeOut= " + timeoutInSeconds + "; //secs to timeout\n\tvar lTimrOutURL=\"/Logout.aspx\"; //redirect to URL when timeout happens\n" +
                        "var idleInterval = setInterval(\"timerIncrement()\", 1000);\n\tvar idleTime = 0;\n    //Zero the idle timer on mouse down.\n    $(this).mousedown(function (e) {\n    idleTime = 0;\n" +
                        "    }); //zero out timer on keydown\n    $(this).keypress(function (e) {\n        idleTime = 0;\n    });\n\tfunction timerIncrement() {\n" +
                        "\t\tidleTime++;\n\t\tif (idleTime > tTimeOut) {\n\t\t\tclearInterval(idleInterval);\n\t\t\tdocument.location = lTimrOutURL; \n\t\t}\n\t}\n</script>\n";
                    return logoutScript;
                }
            }
            return null;
        }

        public static string getMasterURL(HttpSessionState session, HttpRequest request)
        {
            if (session != null)
            {
                string masterURL = (string)session["birst.masterURL"];
                if (masterURL != null)
                {
                    return masterURL;
                }
            }
            return Util.getRequestBaseURL(request);
        }

        public static string getLogoutRedirectURL(HttpSessionState session)
        {
            // look in session
            // look in account
            // else
            return getLoginPageURL(session);
        }

        public static void needToChangePassword(HttpResponse response, HttpSessionState session)
        {
            if (session["PasswordChangeRequired"] != null)
            {
                response.Redirect("~/ChangePassword.aspx");
            }
        }

        public static string getHomePageURL(HttpSessionState session, HttpRequest request)
        {
            HttpBrowserCapabilities browser = request.Browser;
            bool redirect = true;

            if (browser.Browser == "IE")
            {
                if (browser.MajorVersion < 9)
                    redirect = false;
            }
            else if (browser.Browser == "Unknown")
            {
                // iPad
                redirect = false;
            }
            if (redirect)
            {
                return "~/html/home.aspx";
            }

            string homePageURL = "~/FlexModule.aspx?birst.module=home";
            return homePageURL;
        }

        public static string getRequestBaseURL(HttpRequest request)
        {
            Uri uri = getHeaderURL(request);
            return uri.Scheme + "://" + uri.Host + ((uri.Port != -1) ? (":" + uri.Port) : "");
        }

        public static string getRequestBaseHost(HttpRequest request)
        {
            Uri uri = getHeaderURL(request);
            return uri.Host + ((uri.Port != -1) ? (":" + uri.Port) : "");
        }

        public static AccountDetails getAccountDetails(User u)
        {
            AccountDetails accountDetails = null;
            if (u != null && u.ManagedAccountId != Guid.Empty)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    accountDetails = Database.getAccountDetails(conn, Util.mainSchema, u.ManagedAccountId, u);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return accountDetails;
        }

        public static string getDomainReplacedUrl(AccountDetails acctDetails, User u, string inputUrl)
        {
            if (u != null && inputUrl != null && acctDetails != null)
            {
                string domainMatch = acctDetails.getDomainMatch();
                string domainReplace = acctDetails.getDomainReplace();
                if (domainMatch != null && domainReplace != null)
                {
                    Global.systemLog.Debug("Account has override properties for domain match : " + domainMatch + " : and replace : " + domainReplace);
                    Uri uri = new Uri(inputUrl);
                    string host = uri.Host;
                    Regex domainRegex = new Regex(domainMatch);
                    Match match = domainRegex.Match(host);
                    if (match.Success)
                    {
                        string existingValue = match.Groups[1].Value;
                        return inputUrl.Replace(existingValue, domainReplace);
                    }
                    else
                    {
                        Global.systemLog.Warn("No match found for domain match regex in the string : " + host);
                    }
                }
            }
            return inputUrl;
        }

        public static string getUserRedirectedSite(HttpRequest request, User u)
        {
            String redirectUrl = null;
            int releaseType = u.ReleaseType;
            if (releaseType > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    ReleaseInfo releaseInfo = Database.getReleaseInfoForUser(conn, Util.getMainSchema(), u);
                    if (releaseInfo != null)
                    {
                        redirectUrl = releaseInfo.RedirectUrl;
                    }

                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in getting release info for release type " + releaseType, ex);
                }
                finally
                {
                    try
                    {
                        if (conn != null)
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Error in releasing connection ", ex2);
                    }
                }
            }

            return redirectUrl;
        }

        public static string nameValuePairToUrlString(NameValueCollection col)
        {
            StringBuilder sb = new StringBuilder();
            foreach (String key in col.Keys)
            {
                if (sb.Length > 0)
                    sb.Append('&');
                sb.Append(key);
                sb.Append('=');
                sb.Append(col[key]);
            }
            return sb.ToString();
        }

        public static void endSMIWebSession(HttpSessionState session, HttpRequest request)
        {
            Space sp = getSessionSpace(session);
            if (sp == null)
            {
                return;
            }

            logoutSMIWebSession(session, sp, request);
        }

        public static bool isSessionAttributeTrue(HttpSessionState session, string name)
        {
            if (session == null)
                return false;
            object value = session[name];
            if (value is bool)
                return (bool)value;
            return false;
        }

        private static void setLimitedHeader(HttpSessionState session, Space sp, User u, MasterPage master)
        {
            if (u == null)
            {
                return;
            }
            setHeaderDetails(sp, u, master);
            PlaceHolder adhocPh = (PlaceHolder)master.FindControl("AdhocHeaderPlaceHolder");
            PlaceHolder dashboardPh = (PlaceHolder)master.FindControl("DashboardHeaderPlaceHolder");
            PlaceHolder homePh = (PlaceHolder)master.FindControl("UpdatedHome");
            PlaceHolder accountPh = (PlaceHolder)master.FindControl("AccountHeaderPlaceHolder");
            adhocPh.Visible = false;
            dashboardPh.Visible = false;
            homePh.Visible = false;
            accountPh.Visible = false;
        }

        public static void setUpdatedHeader(HttpSessionState session, Space sp, User u, MasterPage master)
        {
            if (u == null)
            {
                return;
            }
            setHeaderDetails(sp, u, master);
            if (session["PasswordChangeRequired"] != null)
            {
                setLimitedHeader(session, sp, u, master);
            }
            else
            {
                // show adhoc or dashboard links
                PlaceHolder adhocPh = (PlaceHolder)master.FindControl("AdhocHeaderPlaceHolder");
                PlaceHolder dashboardPh = (PlaceHolder)master.FindControl("DashboardHeaderPlaceHolder");
                if (sp != null && Util.isSessionAttributeTrue(session, "AdhocPermitted"))
                {
                    adhocPh.Visible = true;
                }
                else
                {
                    adhocPh.Visible = false;
                }
                if (sp != null && Util.isSessionAttributeTrue(session, "DashboardPermitted"))
                {
                    dashboardPh.Visible = true;
                }
                else
                {
                    dashboardPh.Visible = false;
                }
            }
        }

        public static void setHeaderDetails(Space sp, User u, MasterPage master)
        {
            if (u == null)
            {
                return;
            }

            PlaceHolder ha = (PlaceHolder)master.FindControl("HeaderBlank");
            PlaceHolder hs = (PlaceHolder)master.FindControl("HeaderWithSpaceLabel");
            PlaceHolder heb = (PlaceHolder)master.FindControl("HeaderExtraBlank");
            if (heb != null)
                heb.Visible = false;
            if (ha != null)
                ha.Visible = false;
            if (hs != null)
                hs.Visible = true;
            Label spaceNameLabel = (Label)master.FindControl("HeaderSpaceName");
            Label userNameLabel = (Label)master.FindControl("HeaderUserName");
            if (spaceNameLabel != null)
            {
                spaceNameLabel.Text = HttpUtility.HtmlEncode(sp != null ? Util.shortSpaceName(sp.Name) : "");
                spaceNameLabel.ToolTip = HttpUtility.HtmlEncode(sp != null ? sp.Name : "");
                if (u.isSuperUser || u.RepositoryAdmin)
                {
                    spaceNameLabel.ToolTip += HttpUtility.HtmlEncode(sp != null ? "\nID: " + sp.ID : "");
                    spaceNameLabel.ToolTip += HttpUtility.HtmlEncode("\nServer: " + Util.getHostName() + "\nVersion: " + Util.getAppVersion());
                }
            }

            if (userNameLabel != null)
                userNameLabel.Text = HttpUtility.HtmlEncode(u.Username);
        }

        public static bool isDemoSpace(HttpSessionState session)
        {
            bool demo = false;
            Space sp = getSessionSpace(session);
            if (sp != null && sp.Demo)
            {
                demo = true;
            }

            return demo;

        }

        private static void setConnectionLimit(Uri uri)
        {
            try
            {
                Global.systemLog.Info("Default Connection Limit:" + ServicePointManager.DefaultConnectionLimit);
                //ServicePointManager.DefaultConnectionLimit = 120;
                ServicePoint sp = ServicePointManager.FindServicePoint(uri);
                //sp.ConnectionLimit = 120;
                Global.systemLog.Info("Connection Limit specific to uri: " + uri + " = " + sp.ConnectionLimit);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex.Message, ex);
            }
        }

        public static string getSessionVarsFromSSORequest(HttpSessionState session)
        {
            string sessionVarsString = (string)session["SessionVars"];
            return sessionVarsString;
        }

        public static string[] sortFiles(string[] fileNames)
        {
            try
            {
                List<FileInfo> fileInfoList = new List<FileInfo>();
                foreach (string fileName in fileNames)
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.Exists)
                    {
                        fi.Refresh();
                        fileInfoList.Add(fi);
                    }
                    else
                    {
                        string msg = "File do not seem to exist " + fileName + " : Returning the original list";
                        throw new Exception(msg);
                    }
                }
                fileInfoList.Sort(new FileInfoComparer());

                List<string> sortedFiles = new List<string>();
                foreach (FileInfo fi in fileInfoList)
                {
                    sortedFiles.Add(fi.FullName);
                }
                return sortedFiles.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error in sorting files : " + fileNames + " : " + "Returning the original list AS-IS", ex);
                return fileNames;
            }

        }

        /**
         * methods to check if various datastore items should be copied
         */
        public static bool includeDatawarehouseTables(HashSet<string> options)
        {
            return options.Contains("datastore-warehouse_tables") || options.Contains("datastore-warehouse") || options.Contains("datastore");
        }

        public static bool includeStagingTables(HashSet<string> options)
        {
            return options.Contains("datastore-staging_tables") || options.Contains("datastore-staging") || options.Contains("datastore");
        }

        public static bool includeDatawarehouseIndices(HashSet<string> options)
        {
            return options.Contains("datastore-warehouse_indices") || options.Contains("datastore-warehouse") || options.Contains("datastore");
        }

        public static bool includeStagingIndices(HashSet<string> options)
        {
            return options.Contains("datastore-staging_indices") || options.Contains("datastore-staging") || options.Contains("datastore");
        }

        public static bool includeDataStore(HashSet<string> options)
        {
            return includeDatawarehouseTables(options) || includeStagingTables(options) || includeDatawarehouseIndices(options) || includeStagingIndices(options);
        }

        public static bool includeAggregates(HashSet<string> options)
        {
            return options.Contains("datastore-aggregates") || options.Contains("datastore");
        }

        public static bool includeTxnTables(HashSet<string> options)
        {
            return options.Contains("datastore-txntables") || options.Contains("datastore");
        }

        // option:item1,item2,...,itemN
        public static string[] containsOptions(string option, HashSet<string> options)
        {
            foreach (string opt in options)
            {
                string optionColon = option + ':';
                if (opt.StartsWith(optionColon)) // need to do this with ':' to make sure we don't match bob instead of bob2
                {
                    string itemStr = opt.Substring(optionColon.Length);
                    return itemStr.Split(new char[] { ',' });
                }
                if (opt.Equals(option)) // no colon, just the basic option
                    return new string[] { };
            }
            return null;
        }

        public static string copyReplicateLabel(bool replicate)
        {
            return replicate ? "replicated" : "copied";
        }

        public static string copyReplicateLabelPresentTense(bool replicate)
        {
            return replicate ? "replicate" : "copy";
        }

        private static void copyReplicateDirectory(string fromDir, string toDir, bool replicate)
        {
            if (replicate)
            {
                DirectoryInfo toDirI = new DirectoryInfo(toDir);
                if (toDirI.Exists)
                    Util.deleteDir(toDirI);
            }
            if (Directory.Exists(fromDir))
            {
                Util.copyDir(fromDir, toDir);
            }
        }

        private static void cleanupRepositoryTrash(DirectoryInfo di, string pattern)
        {
            FileInfo[] fileInfos = di.GetFiles(pattern);
            foreach (FileInfo finfo in fileInfos)
            {
                finfo.Delete();
            }
        }

        /**
         * clean up repository variants that should be cleaned up after a copy of a repository
         */
        public static void cleanupRepositoryTrash(Space sp)
        {
            DirectoryInfo di = new DirectoryInfo(sp.Directory);
            cleanupRepositoryTrash(di, "repository*.xml.abin*");
            cleanupRepositoryTrash(di, "repository*.xml.bin*");
            cleanupRepositoryTrash(di, "repository*.xml.*.save");
        }

        private static void copyReplicateCustomSubjectAreas(Space fromSpace, Space toSpace, string[] items, bool replicate, bool permissions)
        {
            if (items != null)
            {
                string fromSpaceCSA = Path.Combine(fromSpace.Directory, "custom-subject-areas");
                string toSpaceCSA = Path.Combine(toSpace.Directory, "custom-subject-areas");

                if (!Directory.Exists(toSpaceCSA))
                    Directory.CreateDirectory(toSpaceCSA);

                if (items.Length == 0)
                {
                    if (permissions)
                    {
                        // do the whole thing
                        copyReplicateDirectory(fromSpaceCSA, toSpaceCSA, replicate);
                        Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " custom subject areas from " + " to " + toSpace.ID);
                    }
                    else
                    {
                        Global.systemLog.Warn("can only use the custom-subject-area-no-permissions option with an explicit list of custom subject areas");
                    }
                }
                else
                {
                    // do individual custom subject area files
                    foreach (string item in items)
                    {
                        // each item is actually two items (if permissions is false, only copy/replicate the custom subject area, not the permissions)
                        string fullItem = item;
                        if (item == "default")
                            fullItem = "Default Subject Area";
                        string[] realItems = null;
                        if (permissions)
                            realItems = new string[] { "subjectarea-metadata_" + fullItem + ".xml", "subjectarea_" + fullItem + ".xml" };
                        else
                            realItems = new string[] { "subjectarea_" + fullItem + ".xml" };
                        foreach (string realItem in realItems)
                        {
                            string fromSpaceCSAItem = Path.Combine(fromSpaceCSA, realItem);
                            string toSpaceCSAItem = Path.Combine(toSpaceCSA, realItem);
                            Util.validateFileLocation(fromSpaceCSA, fromSpaceCSAItem);
                            Util.validateFileLocation(toSpaceCSA, toSpaceCSAItem);

                            // file copy
                            if (File.Exists(fromSpaceCSAItem))
                            {
                                Util.FileCopy(fromSpaceCSAItem, toSpaceCSAItem, true);
                            }
                        }
                        Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " custom subject area " + item + " to " + toSpace.ID + (permissions ? " (including permissons)" : " (without permissions)"));
                    }
                }
            }
        }

        /**
         * copy the file/directory contents of a space from one space to another
         */
        public static void copyReplicateSpaceFilesAndDirectoriesIntoSpace(QueryConnection conn, string schema, Space fromSpace, Space toSpace, User u, bool replicate, HashSet<string> options)
        {
            string[] items = null;

            Util.backupMetadata(toSpace, u); // safety first!

            if (options.Contains("repository"))
            {
                // copy repository meta data (do not copy repository_dev.xml.#.save or repository*.xml.{abin*,bin*})
                // repository during final saveApplication will update internals as appropriate to the Space settings (appPath, cache, default connection, etc)
                string[] repfiles = { Util.REPOSITORY_DEV, Util.REPOSITORY_PROD };
                foreach (string file in repfiles)
                {
                    string fromSpaceFile = Path.Combine(fromSpace.Directory, file);
                    if (File.Exists(fromSpaceFile))
                    {
                        string toSpaceFile = Path.Combine(toSpace.Directory, file);
                        Util.FileCopy(fromSpaceFile, toSpaceFile, true);
                    }
                }

                // clean up various bin and save files
                cleanupRepositoryTrash(toSpace);
                Global.systemLog.Info("copied repository meta data (repository.xml, repository_dev.xml) to " + toSpace.ID);
                Util.copyReplicatePackagesInfo(conn, schema, fromSpace, toSpace, u);
                Global.systemLog.Info("Copied packages information for " + toSpace.ID);

            }

            // copy the dcconfig files, only works on modern spaces where the dcconfig files are in the dcconfig directory
            // birst-connect:item1,item2
            items = Util.containsOptions("birst-connect", options);
            if (items != null)
            {
                if (items.Length == 0)
                {
                    // do the whole thing
                    string fromSpaceDir = Path.Combine(fromSpace.Directory, Util.DCCONFIG_DIR);
                    string toSpaceDir = Path.Combine(toSpace.Directory, Util.DCCONFIG_DIR);
                    copyReplicateDirectory(fromSpaceDir, toSpaceDir, replicate);
                    Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " birst connect configurations to " + toSpace.ID);
                }
                else
                {
                    // do individual files
                    string fromSpaceDC = Path.Combine(fromSpace.Directory, Util.DCCONFIG_DIR);
                    string toSpaceDC = Path.Combine(toSpace.Directory, Util.DCCONFIG_DIR);
                    if (!Directory.Exists(toSpaceDC))
                        Directory.CreateDirectory(toSpaceDC);
                    foreach (string item in items)
                    {
                        string realItem = null;
                        if (item == "default")
                            realItem = "dcconfig.xml";
                        else
                            realItem = item + "_dcconfig.xml";
                        string fromSpaceDCItem = Path.Combine(fromSpaceDC, realItem);
                        string toSpaceDCItem = Path.Combine(toSpaceDC, realItem);
                        Util.validateFileLocation(fromSpaceDC, fromSpaceDCItem);
                        Util.validateFileLocation(toSpaceDC, toSpaceDCItem);

                        // file copy
                        if (File.Exists(fromSpaceDCItem))
                        {
                            Util.FileCopy(fromSpaceDCItem, toSpaceDCItem, true);
                            Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " birst connect configuration " + item + " to " + toSpace.ID);
                        }
                    }
                }
            }

            // copy the connector config files
            // connectors:item1,item2
            items = Util.containsOptions("connectors", options);
            if (items != null)
            {
                if (items.Length == 0)
                {
                    // do the whole thing
                    string fromSpaceDir = Path.Combine(fromSpace.Directory, Util.CONNECTOR_CONFIG_DIR);
                    string toSpaceDir = Path.Combine(toSpace.Directory, Util.CONNECTOR_CONFIG_DIR);
                    copyReplicateDirectory(fromSpaceDir, toSpaceDir, replicate);
                    Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " connector configurations to " + toSpace.ID);
                }
                else
                {
                    // do individual files
                    string fromSpaceDC = Path.Combine(fromSpace.Directory, Util.CONNECTOR_CONFIG_DIR);
                    string toSpaceDC = Path.Combine(toSpace.Directory, Util.CONNECTOR_CONFIG_DIR);
                    if (!Directory.Exists(toSpaceDC))
                        Directory.CreateDirectory(toSpaceDC);
                    foreach (string item in items)
                    {
                        string connectorName = null;
                        if (item.ToLower() == "salesforce")
                            connectorName = "sfdc";
                        else
                            connectorName = item.ToLower();
                        string connConfig = connectorName + "_config.xml";
                        string fromSpaceDCItem = Path.Combine(fromSpaceDC, connConfig);
                        string toSpaceDCItem = Path.Combine(toSpaceDC, connConfig);
                        Util.validateFileLocation(fromSpaceDC, fromSpaceDCItem);
                        Util.validateFileLocation(toSpaceDC, toSpaceDCItem);

                        // file copy
                        if (File.Exists(fromSpaceDCItem))
                        {
                            Util.FileCopy(fromSpaceDCItem, toSpaceDCItem, true);
                            Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " connector configuration " + connectorName + " to " + toSpace.ID);
                        }
                    }
                }
            }

            // copy various XML files (do not copy UsersAndGroups.xml - it is auto generated and will go away soon)
            // - spacesettings.xml has information about the background color and logo
            string[] files = { "CustomGeoMaps.xml", "DrillMaps.xml", "spacesettings.xml", "SavedExpressions.xml" };
            foreach (string file in files)
            {
                if (options.Contains(file))
                {
                    string fromSpaceFile = Path.Combine(fromSpace.Directory, file);
                    string toSpaceFile = Path.Combine(toSpace.Directory, file);
                    if (File.Exists(fromSpaceFile))
                    {
                        Util.FileCopy(fromSpaceFile, toSpaceFile, true);
                        Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " configuration file " + file + " to " + toSpace.ID);
                    }
                }
            }

            // copy the custom subject areas
            // custom-subject-areas:item1,item2
            items = Util.containsOptions("custom-subject-areas", options);
            copyReplicateCustomSubjectAreas(fromSpace, toSpace, items, replicate, true);
            items = Util.containsOptions("custom-subject-areas-no-permissions", options);
            copyReplicateCustomSubjectAreas(fromSpace, toSpace, items, replicate, false);

            // copy the dashboard styles
            if (options.Contains("dashboardstyles"))
            {
                string[] sfiles = { "DashboardStyleSettings.xml" };
                foreach (string file in sfiles)
                {
                    string fromSpaceFile = Path.Combine(fromSpace.Directory, file);
                    if (File.Exists(fromSpaceFile))
                    {
                        string toSpaceFile = Path.Combine(toSpace.Directory, file);
                        Util.FileCopy(fromSpaceFile, toSpaceFile, true);
                    }
                }
                string[] sdirectories = { "DashboardTemplates", "DashboardBackgroundImage" };
                foreach (string dir in sdirectories)
                {
                    string fromSpaceDir = Path.Combine(fromSpace.Directory, dir);
                    string toSpaceDir = Path.Combine(toSpace.Directory, dir);
                    copyReplicateDirectory(fromSpaceDir, toSpaceDir, replicate);
                }
                Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " dashboard style files (DashboardStyleSettings.xml, DashboardBackgroundImage directory, and DashboardTemplates directory) to " + toSpace.ID);
            }

            // copy the salesforce configuration
            if (options.Contains("salesforce"))
            {
                string[] sfiles = { "sforce.xml" };
                foreach (string file in sfiles)
                {
                    string fromSpaceFile = Path.Combine(fromSpace.Directory, file);
                    if (File.Exists(fromSpaceFile))
                    {
                        string toSpaceFile = Path.Combine(toSpace.Directory, file);
                        Util.FileCopy(fromSpaceFile, toSpaceFile, true);
                    }
                }
                string[] sdirectories = { "mapping" };
                foreach (string dir in sdirectories)
                {
                    string fromSpaceDir = Path.Combine(fromSpace.Directory, dir);
                    string toSpaceDir = Path.Combine(toSpace.Directory, dir);
                    copyReplicateDirectory(fromSpaceDir, toSpaceDir, replicate);
                }
                Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " salesforce configuration files (sforce.xml and mapping directory) to " + toSpace.ID);
            }

            // copy directories (should almost never copy attachments, logs, slots, archive, or output)
            // - temp and backup are not allowed
            string[] directories2 = { "attachments", "data", "logs", "slogs", "archive", "output" };
            foreach (string dir in directories2)
            {
                if (options.Contains(dir))
                {
                    string fromSpaceDir = Path.Combine(fromSpace.Directory, dir);
                    string toSpaceDir = Path.Combine(toSpace.Directory, dir);
                    copyReplicateDirectory(fromSpaceDir, toSpaceDir, replicate);
                    Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " directory " + dir + " to " + toSpace.ID);
                }
            }

            // copy the catalog
            // catalog:shared/reports;shared/bob.AdhocReport
            items = Util.containsOptions("catalog", options);
            if (items != null)
            {
                if (items.Length == 0)
                {
                    items = new string[] { "shared", "private", "StickyNotes", "Kml" }; // don't do the whole thing, just do shared, private, sticky notes, and kml; leave out various cache and trash directories
                }
                string fromSpaceCatalog = Path.Combine(fromSpace.Directory, "catalog");
                string toSpaceCatalog = Path.Combine(toSpace.Directory, "catalog");

                if (Directory.Exists(fromSpaceCatalog))
                {
                    if (Util.isDirectoryLengthSupported(fromSpaceCatalog)) //Execute Acorn command to copy catalog dir
                    {
                        copyReplicateCatalogDir(fromSpaceCatalog, toSpaceCatalog, toSpace, items, replicate);
                    }
                    else
                    {
                        //execute perfEngine command to copy catalog dir
                        if (Util.getProcessingEngineVersion(fromSpace) > 4)
                        {
                            Util.copyCatalogDir(fromSpace, fromSpace.Directory + "\\catalog", toSpace.Directory + "\\catalog\\", null, items, replicate);
                        }
                        else
                        {
                            Global.systemLog.Warn("Copying of catalog directory failed as the specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.\n To support this, please upgrade processing engine version to 5");
                            throw new Exception("Copying of catalog directory failed as the specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.");
                        }
                    }
                }         
            }
            // cleanup the dashboard cache (new and removed dashboards and changed permissions (in the DB and in the catalog))
            toSpace.invalidateDashboardCache();
        }

        public static void copyReplicateCatalogDir(string fromSpaceCatalog, string toSpaceCatalog, Space toSpace, string[] items, bool replicate)
        {
            foreach (string item in items)
            {
                string fromSpaceCatalogItem = Path.Combine(fromSpaceCatalog, item);
                string toSpaceCatalogItem = Path.Combine(toSpaceCatalog, item);
                Util.validateFileLocation(fromSpaceCatalog, fromSpaceCatalogItem);
                Util.validateFileLocation(toSpaceCatalog, toSpaceCatalogItem);

                if (Directory.Exists(fromSpaceCatalogItem))
                {
                    copyReplicateDirectory(fromSpaceCatalogItem, toSpaceCatalogItem, replicate);
                    Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " catalog entry " + item + " to " + toSpace.ID);
                }
                else
                {
                    // file copy
                    if (File.Exists(fromSpaceCatalogItem))
                    {
                        Util.FileCopy(fromSpaceCatalogItem, toSpaceCatalogItem, true);
                        Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " catalog entry " + item + " to " + toSpace.ID);
                    }
                }
            }
        }

        public static void copyReplicatePackagesInfo(QueryConnection conn, string schema, Space fromSpace, Space toSpace, User u)
        {
            copyAndUpdateCreatedPackages(conn, schema, fromSpace, toSpace, u);
            updateImportedPackagesUsages(conn, schema, fromSpace, toSpace, u);
        }

        private static void copyAndUpdateCreatedPackages(QueryConnection conn, string schema, Space fromSpace, Space toSpace, User u)
        {
            if (fromSpace != null && toSpace != null)
            {
                string fromSpacePackagesFilePath = Path.Combine(fromSpace.Directory, "packages.xml");
                if (File.Exists(fromSpacePackagesFilePath))
                {
                    AllPackages fromPackagesList = AllPackages.load(fromSpace.Directory);
                    if (fromPackagesList != null && fromPackagesList.Packages != null && fromPackagesList.Packages.Length > 0)
                    {
                        List<PackageDetail> toSpacePackages = new List<PackageDetail>();
                        foreach (PackageDetail package in fromPackagesList.Packages)
                        {
                            PackageDetail toSpacePackage = new PackageDetail();
                            toSpacePackage.ID = package.ID;
                            toSpacePackage.SpaceID = toSpace.ID;
                            toSpacePackage.Name = package.Name;
                            toSpacePackage.Description = package.Description;
                            toSpacePackage.StagingTables = PackageUtils.getDeepCopy(package.StagingTables);
                            toSpacePackage.DimensionTables = PackageUtils.getDeepCopy(package.DimensionTables);
                            //toSpacePackage.DimensionTableDisplayNames = package.DimensionTableDisplayNames;
                            toSpacePackage.MeasureTables = PackageUtils.getDeepCopy(package.MeasureTables);
                            //toSpacePackage.MeasureTableDisplayNames = package.MeasureTableDisplayNames;
                            toSpacePackage.Variables = PackageUtils.getDeepCopy(package.Variables);
                            toSpacePackage.Aggregates = PackageUtils.getDeepCopy(package.Aggregates);
                            toSpacePackage.CustomSubjectAreas = PackageUtils.getDeepCopy(package.CustomSubjectAreas);
                            toSpacePackage.CreatedBy = u.Username;
                            toSpacePackage.CreatedDate = DateTime.Now;
                            toSpacePackage.ModifiedBy = null;
                            toSpacePackage.ModifiedDate = null;
                            toSpacePackages.Add(toSpacePackage);
                        }

                        AllPackages toSpacePackageList = new AllPackages();
                        toSpacePackageList.Packages = toSpacePackages.ToArray();
                        toSpacePackageList.save(toSpace.Directory);

                        // clear out any toSpace package groups association
                        Database.removeAllPackageGroupsFromSpace(conn, schema, toSpace.ID);

                        List<SpaceGroup> toSpaceGroups = Database.getSpaceGroups(conn, schema, toSpace);
                        foreach (PackageDetail package in fromPackagesList.Packages)
                        {
                            List<SpaceGroup> fromPackageGroups = Database.getPackageGroups(conn, schema, package.ID, fromSpace.ID);
                            List<string> toGroupIds = new List<string>();
                            if (fromPackageGroups != null && fromPackageGroups.Count > 0)
                            {
                                foreach (SpaceGroup fromPackageGroup in fromPackageGroups)
                                {
                                    SpaceGroup toSpaceGroup = UserAndGroupUtils.getGroupInfoByName(toSpaceGroups, fromPackageGroup.Name, false);
                                    // if there is a group available in the tospace with the same name, then add group accessibility to the package
                                    if (toSpaceGroup != null)
                                    {
                                        toGroupIds.Add(toSpaceGroup.ID.ToString());
                                    }
                                }
                            }

                            if (toGroupIds != null && toGroupIds.Count > 0)
                            {
                                // package id of the to space is the same as the from space since packages.xml are copied as is to the toSpace
                                Database.addGroupsToPackage(conn, schema, u, package.ID, toSpace, toGroupIds.ToArray());
                            }
                        }
                    }
                }
            }
        }

        private static void updateImportedPackagesUsages(QueryConnection conn, string schema, Space fromSpace, Space toSpace, User u)
        {
            Database.removeAllPackageUsagesForChildSpace(conn, schema, toSpace.ID);
            List<PackageUsage> packageUsages = Database.getPackageUsageByChildSpace(conn, schema, fromSpace.ID);
            if (packageUsages != null && packageUsages.Count > 0)
            {
                // get all the parent spaces that the from space is registered to/child of
                // register the to space also with the same parents
                foreach (PackageUsage packageUsage in packageUsages)
                {
                    string parentSpaceID = packageUsage.PackageSpaceID.ToString();
                    string packageID = packageUsage.PackageID;
                    if (packageID != null && packageID.Trim().Length > 0
                        && parentSpaceID != null && parentSpaceID != Guid.Empty.ToString())
                    {
                        Database.addUsageToPackage(conn, schema, u, new Guid(parentSpaceID), new Guid(packageID), toSpace.ID);
                    }
                }
            }
        }

        /**
         * copy/replicate the groups and ACLs from one space to another
         */
        public static void copyReplicateSpaceGroupsIntoSpace(QueryConnection conn, string schema, Space fromSpace, Space toSpace, User u, bool replicate, bool updateMappingModifiedDate)
        {
            try
            {
                List<SpaceGroup> spgOldList = Database.getSpaceGroups(conn, schema, fromSpace);
                List<SpaceGroup> spgNewList = Database.getSpaceGroups(conn, schema, toSpace);

                // find all groups that are in 'from' but not in 'to', copy them over
                if (spgOldList != null)
                {
                    foreach (SpaceGroup oldGroup in spgOldList)
                    {
                        if (Util.isOwnerGroup(oldGroup))
                        {
                            // dont modify owner group
                            continue;
                        }
                        bool found = false;
                        if (spgNewList != null)
                        {
                            foreach (SpaceGroup newGroup in spgNewList)
                            {
                                if (newGroup.Name == oldGroup.Name)
                                {
                                    found = true;
                                    // make sure ACLs and Users are copied/replicated for this group
                                    Util.copyReplicateGroupACLs(conn, schema, toSpace, oldGroup, newGroup, replicate, false);
                                    Util.copyReplicateGroupUsers(conn, schema, toSpace, oldGroup, newGroup, replicate, false);
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            // the group does not exist in the 'to' space, create it and add the ACLs and Users
                            Util.addGroupToSpace(conn, schema, toSpace, oldGroup, false);
                        }
                    }
                }
                if (replicate)
                {
                    // see if there are any groups in 'to' that are not in 'from' and remove them from the 'to' space
                    if (spgNewList != null)
                    {
                        foreach (SpaceGroup newGroup in spgNewList)
                        {
                            bool found = false;
                            if (spgOldList != null)
                            {
                                foreach (SpaceGroup oldGroup in spgOldList)
                                {
                                    if (newGroup.Name == oldGroup.Name)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (!found)
                            {
                                // the group does not exist in the 'from' space, remove from the 'to' space
                                Database.removeGroupFromSpace(conn, schema, toSpace, newGroup, true, false);
                            }
                        }
                    }
                }
            }
            finally
            {
                if (updateMappingModifiedDate)
                    Database.updateMappingModifiedDate(conn, schema, toSpace);
            }
            Global.systemLog.Info(Util.copyReplicateLabel(replicate) + " groups and ACLs to " + toSpace.ID);
        }

        /**
         * copy/replicate the space membership
         */
        public static void copyReplicateSpaceMembership(QueryConnection conn, string mainSchema, Space fromSpace, Space toSpace, bool replicate, bool updateMappingModifiedDate)
        {
            // copy space memberships
            List<SpaceMembership> fromSpaceUsers = Database.getUsers(conn, mainSchema, fromSpace, false);
            List<SpaceMembership> toSpaceUsers = Database.getUsers(conn, mainSchema, toSpace, false);

            if (fromSpaceUsers != null)
            {
                foreach (SpaceMembership fromSM in fromSpaceUsers)
                {
                    if (fromSM.User == null)
                        continue;
                    bool found = false;
                    if (toSpaceUsers != null)
                    {
                        foreach (SpaceMembership toSM in toSpaceUsers)
                        {
                            if (toSM.User == null)
                                continue;
                            if (toSM.User.ID == fromSM.User.ID)
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                    {
                        Database.addSpaceMembership(conn, mainSchema, fromSM.User.ID, toSpace.ID, fromSM.Administrator, fromSM.Owner);
                    }
                }
            }

            if (replicate)
            {
                // find all Users missing from fromSpace
                if (toSpaceUsers != null)
                {
                    foreach (SpaceMembership toSM in toSpaceUsers)
                    {
                        if (toSM.User == null)
                            continue;
                        if (fromSpaceUsers != null)
                        {
                            bool found = false;
                            foreach (SpaceMembership fromSM in fromSpaceUsers)
                            {
                                if (fromSM.User == null)
                                    continue;
                                if (toSM.User.ID == fromSM.User.ID)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                Database.removeSpaceMembership(conn, mainSchema, toSM.User.ID, toSpace.ID);
                            }
                        }
                    }
                }
            }
            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, mainSchema, toSpace);
        }

        /**
         * copy/replicate the Users from one group to another
         */
        private static void copyReplicateGroupUsers(QueryConnection conn, string schema, Space sp, SpaceGroup fromSPG, SpaceGroup toSPG, bool replicate, bool updateMappingModifiedDate)
        {
            List<User> fromUsers = fromSPG.GroupUsers; // contains ID, sufficient for this method
            List<User> toUsers = toSPG.GroupUsers;

            // find all Users missing from toSPG
            if (fromUsers != null)
            {
                foreach (User fromUser in fromUsers)
                {
                    if (toUsers != null)
                    {
                        bool found = false;
                        foreach (User toUser in toUsers)
                        {
                            if (fromUser.ID == toUser.ID)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            Database.addUserToGroup(conn, schema, sp, toSPG.ID, fromUser.ID, false);
                        }
                    }
                }
            }
            if (replicate)
            {
                // find all Users missing from fromSPG
                if (toUsers != null)
                {
                    foreach (User toUser in toUsers)
                    {
                        if (fromUsers != null)
                        {
                            bool found = false;
                            foreach (User fromUser in fromUsers)
                            {
                                if (fromUser.ID == toUser.ID)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                Database.removeUserFromGroup(conn, schema, sp, toSPG.ID, toUser.ID, false);
                            }
                        }
                    }
                }
            }
            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
        }

        /**
         * add a group to a space
         */
        private static Guid addGroupToSpace(QueryConnection conn, string schema, Space sp, SpaceGroup spg, bool updateMappingModifiedDate)
        {
            // add the group to the space
            Guid groupID = Database.addGroupToSpace(conn, schema, sp, spg.Name, spg.InternalGroup, false);
            if (groupID != Guid.Empty)
            {
                // add the ACLs to the group
                if (spg.ACLIds != null)
                {
                    foreach (int aclID in spg.ACLIds)
                    {
                        Database.addACLToGroup(conn, schema, sp, groupID, aclID, false);
                    }
                }
                // add the Users to the group
                foreach (User user in spg.GroupUsers)
                {
                    Database.addUserToGroup(conn, schema, sp, spg.ID, user.ID, false);
                }
            }
            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);

            return groupID;
        }

        /**
         * copy/replicate the ACLs from one group to another
         */
        private static void copyReplicateGroupACLs(QueryConnection conn, string schema, Space sp, SpaceGroup fromSPG, SpaceGroup toSPG, bool replicate, bool updateMappingModifiedDate)
        {
            List<int> fromACLs = fromSPG.ACLIds;
            List<int> toACLs = toSPG.ACLIds;

            // find all ACLs missing from toSPG
            if (fromACLs != null)
            {
                foreach (int fromACLID in fromACLs)
                {
                    if (toACLs != null)
                    {
                        if (!toACLs.Contains(fromACLID))
                        {
                            // add fromACLID to toSPG
                            Database.addACLToGroup(conn, schema, sp, toSPG.ID, fromACLID, false);
                        }
                    }
                }
            }
            if (replicate)
            {
                // find all ACLs missing from fromSPG
                if (toACLs != null)
                {
                    foreach (int toACLID in toACLs)
                    {
                        if (fromACLs != null)
                        {
                            if (!fromACLs.Contains(toACLID))
                            {
                                // remove toACLID from toSPG
                                Database.removeACLFromGroup(conn, schema, sp, toSPG.ID, toACLID, false);
                            }
                        }
                    }
                }
            }
            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
        }

        // copy the group/acl/user from one space to another. This makes sure that the account type related priveleges
        // are stayed intact (e.g. User B copying shared space from User A and both B& A have different account types)
        public static void copyGroupUserACLInfo(Space oldSpace, Space newSpace, User owner, bool updateOwnerGroupMembershipIfRequired)
        {
            QueryConnection conn = null;
            try
            {

                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                List<SpaceGroup> spgOldList = Database.getSpaceGroups(conn, schema, oldSpace);
                List<SpaceGroup> spgNewList = Database.getSpaceGroups(conn, schema, newSpace);
                if (spgOldList != null && spgOldList.Count > 0)
                {
                    // clean first all the group info, then reload 
                    foreach (SpaceGroup oldGroup in spgOldList)
                    {
                        SpaceGroup spgNewGroup = null;
                        if (spgNewList != null && spgNewList.Count > 0)
                        {
                            foreach (SpaceGroup newGroup in spgNewList)
                            {
                                if (newGroup.Name == oldGroup.Name && newGroup.InternalGroup == oldGroup.InternalGroup)
                                {
                                    spgNewGroup = newGroup;
                                    break;
                                }
                            }
                        }

                        // If the group does not exist, create one
                        if (spgNewGroup == null)
                        {
                            Guid addedGroupID = Database.addGroupToSpace(conn, schema, newSpace, oldGroup.Name, oldGroup.InternalGroup);
                            spgNewGroup = Database.getGroupInfo(conn, schema, newSpace, oldGroup.Name);
                        }

                        // Add group acls.
                        List<int> sp1GroupAcls = oldGroup.ACLIds;
                        List<int> sp2GroupAcls = spgNewGroup.ACLIds != null ? spgNewGroup.ACLIds : new List<int>();
                        if (sp1GroupAcls != null && sp1GroupAcls.Count > 0)
                        {
                            foreach (int sp1g1Acl in sp1GroupAcls)
                            {
                                if (!sp2GroupAcls.Contains(sp1g1Acl))
                                {
                                    Database.addACLToGroup(conn, mainSchema, newSpace, spgNewGroup.ID, sp1g1Acl);
                                }
                            }
                        }
                    }
                }


                // For repository created before 4.3, db might not have space/group/user/acl info if no lazy migration has
                // occured for the repository.
                // In that case, when we copy, we copy the repository_dev already and try to create group/acl/info from 
                // repository itself
                Util.checkAndCreateUserGroupDbMappings(newSpace);

                // Lastly, reload ACL information for OWNER$ and USER$ since copiedTo and copiedFrom users might be different

                // get the groups again and remove acls first
                spgNewList = Database.getSpaceGroups(conn, schema, newSpace);
                if (spgNewList != null && spgNewList.Count > 0)
                {
                    foreach (SpaceGroup spg in spgNewList)
                    {
                        if (!spg.InternalGroup)
                        {
                            continue;
                        }

                        if (spg.Name == Util.OWNER_GROUP_NAME || spg.Name == Util.USER_GROUP_NAME)
                        {
                            Database.removeAllACLsFromGroup(conn, schema, newSpace, spg.ID);
                        }
                    }
                }

                // reload the priveleges for OWNER$ and USER$ group
                Util.setGroupPrivileges(newSpace, owner.Username, owner.AccountType, updateOwnerGroupMembershipIfRequired);

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while trying to reload GroupPriveleges", ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void updateCopiedRepository(Space sp, User u)
        {
            bool newRep;
            if (File.Exists(sp.Directory + "\\" + Util.REPOSITORY_DEV))
            {
                MainAdminForm maf1 = loadRepository(sp, out newRep);
                updateRepositoryWithOwnerInfo(sp, maf1, u);
                maf1.saveRepository(sp.Directory + "\\" + Util.REPOSITORY_DEV);
            }
            if (File.Exists(sp.Directory + "\\" + Util.REPOSITORY_PROD))
            {
                MainAdminForm maf2 = loadRepository(sp, out newRep, Util.REPOSITORY_PROD);
                updateRepositoryWithOwnerInfo(sp, maf2, u);
                maf2.saveRepository(sp.Directory + "\\" + Util.REPOSITORY_PROD);
            }
        }

        // This will update the repository with the new owner if different and remove the user from the copied 
        // space. Also no need to add space-group/acl/user mapping. It will be added when user logs into space        
        private static void updateRepositoryWithOwnerInfo(Space sp, MainAdminForm maf, User u)
        {
            if (maf == null || sp == null || u == null)
            {
                return;
            }

            maf.appName.Text = sp.Name + "/" + u.Username;

            foreach (Performance_Optimizer_Administration.Group g in maf.usersAndGroups.groupsList)
            {
                if (g.Name == Util.OWNER_GROUP_NAME & g.InternalGroup == true)
                {
                    if (Array.IndexOf(g.Usernames, u.Username) < 0)
                    {
                        // replacing the owner name with the current user
                        string[] usernames = new string[] { u.Username };
                        g.Usernames = usernames;
                    }
                }
                else
                {
                    // removing the current user (owner) name from all other groups
                    if (g.Usernames != null && Array.IndexOf(g.Usernames, u.Username) >= 0)
                    {
                        List<string> updatedGroupUserNames = new List<string>();
                        foreach (string name in g.Usernames)
                        {
                            if (name.Equals(u.Username))
                            {
                                continue;
                            }
                            updatedGroupUserNames.Add(name);
                        }
                        g.Usernames = updatedGroupUserNames.ToArray();
                    }
                }
            }
        }

        public static void markSessionDirty(HttpSessionState session)
        {
            if (session != null)
            {
                session["AdminDirty"] = true;
            }
        }

        public static bool repositoryAvailable(Space sp)
        {
            bool available = false;
            if (File.Exists(sp.Directory + "\\" + Util.REPOSITORY_DEV))
            {
                available = true;
            }
            return available;
        }

        public static bool isTypeSuperUser(HttpSessionState session)
        {
            bool isSuperUser = false;
            User u = (User)session["user"];
            if (u != null)
            {
                isSuperUser = u.RepositoryAdmin;
            }

            return isSuperUser;
        }

        public static SpaceMembership getSpaceMembership(HttpSessionState session)
        {
            return getSpaceMembership(session, null, null);
        }
        // get space memberships for the logged in user
        public static SpaceMembership getSpaceMembership(HttpSessionState session, User u, Space sp)
        {
            if (u == null)
            {
                u = Util.getSessionUser(session);
            }

            if (sp == null)
            {
                sp = Util.getSessionSpace(session);
            }
            if (u != null && sp != null)
            {
                List<SpaceMembership> mlist = null;

                if (session != null)
                {
                    mlist = (List<SpaceMembership>)session["mlist"];
                }
                if (mlist == null)
                {
                    mlist = getSpaceMembershipListFromDB(u);
                    if (session != null)
                    {
                        session["mlist"] = mlist;
                    }
                }

                if (mlist != null)
                    return getSpaceMembershipFromList(sp, mlist);
            }

            return null;
        }

        public static List<SpaceMembership> getSpaceMembershipListFromDB(User u)
        {
            // get it from database
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                return Database.getSpaces(conn, mainSchema, u, true);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error getting membership list for user " + u.Username, e);
                return null;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static SpaceMembership getSpaceMembershipFromDB(User u, Space sp)
        {
            return getSpaceMembershipFromList(sp, getSpaceMembershipListFromDB(u));
        }

        private static SpaceMembership getSpaceMembershipFromList(Space sp, List<SpaceMembership> mlist)
        {
            if (mlist != null)
            {
                for (int i = 0; i < mlist.Count; i++)
                {
                    if (mlist[i].Space.ID == sp.ID)
                    {
                        return mlist[i];
                    }
                }
            }

            return null;
        }
        /*
        public static DateTime getProcessInitiatedTime(Space sp, MainAdminForm maf)
        {
            Variable processTimeVariable = getVariable(maf, Util.LOAD_INITIATED_TIME, false);
            DateTime dateTime = DateTime.MinValue;
            if (processTimeVariable != null)
            {
                DateTime.TryParse(processTimeVariable.Query, out dateTime);
            }

            return dateTime;
        }
        */
        public static void setProcessInitiatedTime(Space sp, MainAdminForm maf)
        {
            setProcessInitiatedTime(sp, maf, DateTime.Now);
        }

        // create a repository variable for storing time
        // at which process load is initiated

        public static void setProcessInitiatedTime(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            if (dateTime == DateTime.MinValue)
            {
                dateTime = DateTime.Now;
            }

            maf.setRepositoryDirectory(sp.Directory);

            Variable processTimeVariable = getVariable(maf, Util.LOAD_INITIATED_TIME, false);
            if (processTimeVariable == null)
            {
                processTimeVariable = new Variable();
                processTimeVariable.Name = Util.LOAD_INITIATED_TIME;
                processTimeVariable.Type = Variable.VariableType.Repository;
                processTimeVariable.Constant = true;
            }

            processTimeVariable.Query = TimeZoneUtils.getProcessingTimeZoneAdjustedDateTime(sp, maf, dateTime).ToString("g");

            // add or update and save
            maf.updateVariable(processTimeVariable, null);
        }

        public static void setProcessStartTime(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            if (dateTime == DateTime.MinValue)
            {
                dateTime = DateTime.Now;
            }

            maf.setRepositoryDirectory(sp.Directory);

            Variable processTimeVariable = getVariable(maf, Util.LOAD_START_TIME, false);
            if (processTimeVariable == null)
            {
                processTimeVariable = new Variable();
                processTimeVariable.Name = Util.LOAD_START_TIME;
                processTimeVariable.Type = Variable.VariableType.Repository;
                processTimeVariable.Constant = true;
            }

            processTimeVariable.Query = Util.getDateTimeInISOFormat(TimeZoneUtils.getProcessingTimeZoneAdjustedDateTime(sp, maf, dateTime));

            // add or update and save
            maf.updateVariable(processTimeVariable, null);
        }

        public static void setProcessCompletionTimeVariable(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            maf.setRepositoryDirectory(sp.Directory);

            Variable variable = getVariable(maf, Util.LOAD_COMPLETION_TIME, false);
            if (variable == null)
            {
                variable = new Variable();
                variable.Name = Util.LOAD_COMPLETION_TIME;
                variable.Type = Variable.VariableType.Repository;
                variable.Constant = true;
            }

            variable.Query = getDateTimeInISOFormat(dateTime);

            maf.updateVariable(variable, null);
        }

        public static void setSFDCExtractInitiatedTimeVariable(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            maf.setRepositoryDirectory(sp.Directory);

            Variable variable = getVariable(maf, Util.SFDC_EXTRACT_START_TIME, false);
            if (variable == null)
            {
                variable = new Variable();
                variable.Name = Util.SFDC_EXTRACT_START_TIME;
                variable.Type = Variable.VariableType.Repository;
                variable.Constant = true;
            }

            variable.Query = getDateTimeInISOFormat(dateTime);

            maf.updateVariable(variable, null);
        }

        public static void setSFDCExtractCompletionTimeVariable(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            maf.setRepositoryDirectory(sp.Directory);

            Variable variable = getVariable(maf, Util.SFDC_EXTRACT_COMPLETION_TIME, false);
            if (variable == null)
            {
                variable = new Variable();
                variable.Name = Util.SFDC_EXTRACT_COMPLETION_TIME;
                variable.Type = Variable.VariableType.Repository;
                variable.Constant = true;
            }

            variable.Query = getDateTimeInISOFormat(dateTime);

            maf.updateVariable(variable, null);
        }

        public static void setConnectorTimeVariable(Space sp, MainAdminForm maf, string variableName, DateTime dateTime)
        {
            maf.setRepositoryDirectory(sp.Directory);

            Variable variable = getVariable(maf, variableName, false);
            if (variable == null)
            {
                variable = new Variable();
                variable.Name = variableName;
                variable.Type = Variable.VariableType.Repository;
                variable.Constant = true;
            }

            variable.Query = getDateTimeInISOFormat(dateTime);

            maf.updateVariable(variable, null);
        }

        // get variable from the repository
        // isSessionVariable is passed true or false if the variable is of type Session or Repository respectively
        public static Variable getVariable(MainAdminForm maf, string variableName, bool isSessionVariable)
        {
            Variable foundVariable = null;

            if (maf != null)
            {
                foreach (Variable variable in maf.getVariables())
                {
                    if (variable.Name.Equals(variableName) && variable.Session == isSessionVariable)
                    {
                        foundVariable = variable;
                        break;
                    }
                }
            }

            return foundVariable;
        }

        public static void markSMIWebSessionToLogout(HttpSessionState session)
        {
            if (session != null)
            {
                session["requiresLogout"] = true;
            }
        }

        public static void removeSessionObject(HttpSessionState session, string objectName)
        {
            if (session != null && session[objectName] != null)
            {
                session.Remove(objectName);
            }

        }

        public static bool isFeatureFlagEnabled(string featureId, bool defaultValue)
        {            
            string featureFlagString = (string)System.Web.Configuration.WebConfigurationManager.AppSettings[featureId];
            if (featureFlagString == null)
                return defaultValue;
            bool enabled = false;
            return bool.TryParse(featureFlagString, out enabled) ? enabled : defaultValue;
        }

        public static bool isOwner(HttpSessionState session, User u)
        {
            Space sp = getSessionSpace(session);
            return isOwner(sp, u);
        }

        public static bool isOwner(Space sp, User u)        
        {
            bool isOwner = false;            
            // first check user_space_inxn table
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                SpaceMembership record = Database.getSpaceMembershipRecord(conn, schema, u, sp.ID);
                if (record != null && record.User != null)
                {
                    isOwner = record.Owner;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception getting ownership details " + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            if (isOwner)
            {
                return true;
            }

            // Only go to OWNER$ group as secondary            
            List<SpaceGroup> spaceGroupList = sp.SpaceGroups;
            if (spaceGroupList != null && spaceGroupList.Count > 0)
            {
                foreach (SpaceGroup spg in spaceGroupList)
                {
                    if (spg.Name == Util.OWNER_GROUP_NAME && spg.InternalGroup)
                    {
                        isOwner = UserAndGroupUtils.isUserGroupMember(spg, u.ID);
                        break;
                    }
                }
            }

            return isOwner;
        }

        /// <summary>
        /// Get the regular expression object for the time format used in smi-engine logs.
        /// </summary>
        /// <returns>regex object</returns>

        public static Regex getRegexForSmiEngineLogTime()
        {
            if (smiEngineTimeFormatRegex == null)
            {
                // If the smi engine logger changes the format, this regex pattern WILL
                // need to be updated.                
                string timeFormatToLookFor = @"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}";
                smiEngineTimeFormatRegex = new Regex(timeFormatToLookFor);
            }

            return smiEngineTimeFormatRegex;
        }

        /// <summary>
        /// Used to find matches for the SMIEngine timestamps in log line. 
        /// Useful for cases to determine which log line is recent especially 
        /// when everything else is identical.
        /// 
        /// E.g. The load could be processed, deleted and re processed again in which case the same 
        /// load number can appear multiple times in logs on the same day and it will be difficult to determine
        /// which is recent
        /// </summary>
        /// <param name="logLine">string to look for match</param>
        /// <param name="lengthToLookFor">maximum length to look for matches</param>
        /// <returns>Match for time stamp in log line</returns>
        public static Match getMatchForTimeInSmiEngineLog(string logLine, int lengthToLookFor)
        {
            if (lengthToLookFor == -1)
            {
                lengthToLookFor = logLine.Length;
            }

            Match match = Util.getRegexForSmiEngineLogTime().Match(logLine, 0, lengthToLookFor);
            return match;
        }

        /* ------------------------------------------------------------------------------
         * -- ALERT EMAIL methods                                                      --
         * ------------------------------------------------------------------------------ */

        public static void sendErrorDetailEmail(string shortErrorMessage,
                                                string duringProcess,
                                                string errorDetails,
                                                Exception exception, Space sp,
                                                string toAddressEmail) // send in addition to other alert group)
        {
            MailMessage mm = new MailMessage();
            string from = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["OperationsAlertFrom"];
            from = from.Replace("${HOSTNAME}", System.Environment.MachineName);
            string fromName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["OperationsAlertFromName"];
            fromName = fromName.Replace("${HOSTNAME}", System.Environment.MachineName);
            mm.From = new MailAddress(from, fromName);
            string toEmailAddress = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["OperationsAlertEmail"];
            mm.To.Add(new MailAddress(toEmailAddress));
            if (toAddressEmail != null && toAddressEmail.Trim().Length > 0)
            {
                mm.To.Add(new MailAddress(toAddressEmail));
            }
            mm.IsBodyHtml = true;
            StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/OpsAlertEmail.htm"));
            mm.Body = reader.ReadToEnd();
            reader.Close();
            mm.Subject = shortErrorMessage;
            mm.Body = mm.Body.Replace("${PROCESS}", duringProcess);
            string messageBody = "<b>Description</b>";
            if (sp != null)
            {
                messageBody += "<p><blockquote><pre>Space ID: " + sp.ID + ", Space Name: " + sp.Name + "</pre></blockquote>";
            }
            messageBody += "<p><blockquote>" + errorDetails + "</blockquote>";
            if (exception != null)
            {
                messageBody += "<h4>Exception</h4>";
                messageBody += "<blockquote>";
                messageBody += exception.GetBaseException().GetType().Name + ": " + exception.Message;
                messageBody += "<p>";
                messageBody += "<pre>" + exception.StackTrace + "</pre>";
                messageBody += "</blockquote>";
                Exception innerException = exception.InnerException;
                if (innerException != null)
                {
                    messageBody += "<h4>Caused by</h4>";
                    messageBody += "<blockquote>";
                    messageBody += innerException.GetBaseException().GetType().Name + ": " + innerException.Message;
                    messageBody += "<p>";
                    messageBody += "<pre>" + innerException.StackTrace + "</pre>";
                    messageBody += "</blockquote>";
                }
            }
            mm.Body = mm.Body.Replace("${MESSAGE}", messageBody);
            SmtpClient sc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                sc.EnableSsl = true;
            sc.Send(mm);
        }

        public static string getContentDir()
        {
            return getAppSettingProperty("ContentDirectory");
        }

        public static string getTimeCommand()
        {
            return getAppSettingProperty("CreateTimeCommand");
        }

        public static string getAppSettingProperty(string key)
        {
            Object obj = System.Web.Configuration.WebConfigurationManager.AppSettings[key];
            if (obj != null)
                return (string)obj;
            return null;
        }

        public static string getMainSchema()
        {
            string schema = mainSchema;
            if (Util.TEST_FLAG)
            {
                schema = Acorn.tests.TestUtil.schema;
            }
            return schema;
        }

        public static string getHostName()
        {
            return System.Net.Dns.GetHostName();
        }

        public static void setWarningThresholdForConcurrentLogins()
        {
            try
            {
                THRESHOLD_WARN_CONCURRENT_LOGINS = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["ThresholdWarnMaxConcurrentLogins"]);
            }
            catch (Exception)
            {
                Global.systemLog.Warn("Error while retreiving threshold limit for MaxConcurrentLogins Warnings, settings to 5");
                THRESHOLD_WARN_CONCURRENT_LOGINS = 5;
            }
        }

        // XXX hack until we figure out where to capture and store proxy information
        public static bool getProxyServerInfo(string path, Guid spaceID, ref String username, ref String password)
        {
            string filename = path + "\\proxyinfo.config";
            if (!File.Exists(filename))
            {
                Global.systemLog.Info("no proxyinfo.config file: " + filename);
                return false;
            }
            StreamReader reader = new StreamReader(filename);
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//"))
                    continue;
                string[] cols = line.Split(new char[] { '\t' }); // spaceID    user   password (cleartext)
                if (cols[0].ToLower().Equals(spaceID.ToString().ToLower()))
                {
                    reader.Close();
                    username = cols[1];
                    password = cols[2];
                    return true;
                }
            }
            return false;
        }

        public static void logVersionInformation()
        {
            Global.systemLog.Info(Assembly.Load("Acorn").FullName);
            Global.systemLog.Info(Assembly.Load("Birst Administration").FullName);
            Global.systemLog.Info(Assembly.Load("LogicalExpression").FullName);
            Global.systemLog.Info(".Net Framework: " + Assembly.Load("Acorn").ImageRuntimeVersion);
        }

        public static RARepository getRepository(MainAdminForm maf)
        {
            // Get a list of realtime connections
            List<string> rtlist = new List<string>();
            foreach (DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Realtime)
                    rtlist.Add(dc.Name);
            }
            if (maf.EnableLiveAccessToDefaultConnection)
                rtlist.Add(Database.DEFAULT_CONNECTION);
            RARepository rap = new RARepository();
            rap.DimensionalStructure = maf.DimensionalStructure;
            rap.Connections = new RARepository.RAConnection[rtlist.Count];
            int count = 0;
            for (int i = 0; i < maf.connection.connectionList.Count; i++)
            {
                if (maf.connection.connectionList[i].Realtime || (i == 0 && maf.EnableLiveAccessToDefaultConnection))
                {
                    DatabaseConnection dconn = maf.connection.connectionList[i];
                    rap.Connections[count] = new RARepository.RAConnection(dconn);
                    count++;
                }
            }
            List<Hierarchy> hlist = maf.hmodule.getHierarchies();
            rap.Hierarchies = new Hierarchy[hlist.Count];
            for (int i = 0; i < hlist.Count; i++)
            {
                // Lock levels that are used by autogenerated metadata
                List<Level> clist = new List<Level>();
                hlist[i].getChildLevels(clist);
                foreach (Level l in clist)
                {
                    l.Locked = false;
                    foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                    {
                        foreach (string[] stl in st.Levels)
                        {
                            if (stl[0] == hlist[i].DimensionName && stl[1] == l.Name)
                            {
                                l.Locked = true;
                                break;
                            }
                        }
                        if (l.Locked)
                            break;
                    }
                }
                rap.Hierarchies[i] = hlist[i];
            }
            Dictionary<string, string> tset = new Dictionary<string, string>();
            rap.Dimensions = new RARepository.RADimension[maf.dimensionsList.Count];
            for (int i = 0; i < maf.dimensionsList.Count; i++)
            {
                rap.Dimensions[i] = new RARepository.RADimension();
                rap.Dimensions[i].Name = maf.dimensionsList[i].Name;

                DataRow[] results = maf.dimensionColumns.Select("DimensionName='" + rap.Dimensions[i].Name + "'");
                rap.Dimensions[i].Columns = new RARepository.RADimensionColumn[results.Length];
                for (int j = 0; j < results.Length; j++)
                {
                    rap.Dimensions[i].Columns[j] = new RARepository.RADimensionColumn();
                    rap.Dimensions[i].Columns[j].ColumnName = (string)results[j]["ColumnName"];
                    rap.Dimensions[i].Columns[j].DataType = (string)results[j]["DataType"];
                    rap.Dimensions[i].Columns[j].Format = (string)results[j]["Format"];
                    string s = (string)results[j]["ColWidth"];
                    rap.Dimensions[i].Columns[j].Width = s.Length == 0 ? 0 : int.Parse(s);
                    rap.Dimensions[i].Columns[j].AutoGenerated = (bool)results[j]["AutoGenerated"];
                    rap.Dimensions[i].Columns[j].ManuallyEdited = (bool)results[j]["ManuallyEdited"];
                }
                List<RARepository.RADimensionTable> definitions = new List<RARepository.RADimensionTable>();
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (rtlist.IndexOf(dt.TableSource.Connection) < 0)
                    {
                        if (dt.DimensionName == rap.Dimensions[i].Name)
                        {
                            rap.Dimensions[i].UsedByNonLiveAccessSource = true;
                        }
                        continue;
                    }
                    if (dt.DimensionName != rap.Dimensions[i].Name)
                        continue;
                    RARepository.RADimensionTable radt = new RARepository.RADimensionTable();
                    if (!tset.ContainsKey(dt.TableName))
                        tset.Add(dt.TableName, null);
                    radt.Name = dt.TableName;
                    radt.Level = dt.Level;
                    radt.Type = dt.Type;
                    radt.PhysicalName = dt.PhysicalName;
                    radt.Source = new TableSource();
                    radt.Source.Connection = dt.TableSource.Connection;
                    radt.Source.Schema = dt.TableSource.Schema;
                    radt.Source.Filters = dt.TableSource.Filters;
                    radt.Source.Tables = new TableSource.TableDefinition[dt.TableSource.Tables.Length];
                    radt.Cacheable = dt.Cacheable;
                    radt.TTL = dt.TTL;
                    for (int j = 0; j < dt.TableSource.Tables.Length; j++)
                    {
                        radt.Source.Tables[j] = new TableSource.TableDefinition();
                        radt.Source.Tables[j].PhysicalName = dt.TableSource.Tables[j].PhysicalName;
                        radt.Source.Tables[j].JoinClause = dt.TableSource.Tables[j].JoinClause;
                        radt.Source.Tables[j].JoinType = dt.TableSource.Tables[j].JoinType;
                    }
                    radt.Query = dt.OpaqueView;
                    radt.Cacheable = dt.Cacheable;
                    radt.TTL = dt.TTL;
                    radt.AutoGenerated = dt.AutoGenerated;
                    DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(dt.TableName);
                    radt.Mappings = new RARepository.RADimensionColumnMapping[drv.Length];
                    for (int j = 0; j < drv.Length; j++)
                    {
                        radt.Mappings[j] = new RARepository.RADimensionColumnMapping();
                        radt.Mappings[j].ColumnName = (string)drv[j]["ColumnName"];
                        radt.Mappings[j].Formula = (string)drv[j]["PhysicalName"];
                        radt.Mappings[j].AutoGenerated = (bool)drv[j]["AutoGenerated"];
                        radt.Mappings[j].ManuallyEdited = (bool)drv[j]["ManuallyEdited"];
                    }
                    definitions.Add(radt);
                }
                rap.Dimensions[i].Definitions = definitions.ToArray();
            }
            // Measures
            rap.Measures = new RARepository.RAMeasureColumn[maf.measureColumnsTable.Rows.Count];
            for (int i = 0; i < maf.measureColumnsTable.Rows.Count; i++)
            {
                rap.Measures[i] = new RARepository.RAMeasureColumn();
                rap.Measures[i].ColumnName = (string)maf.measureColumnsTable.Rows[i]["ColumnName"];
                rap.Measures[i].AggRule = (string)maf.measureColumnsTable.Rows[i]["AggregationRule"];
                rap.Measures[i].DataType = (string)maf.measureColumnsTable.Rows[i]["DataType"];
                rap.Measures[i].Format = (string)maf.measureColumnsTable.Rows[i]["Format"];
                rap.Measures[i].Width = (int)maf.measureColumnsTable.Rows[i]["Width"];
                rap.Measures[i].AutoGenerated = (bool)maf.measureColumnsTable.Rows[i]["AutoGenerated"];
                rap.Measures[i].ManuallyEdited = (bool)maf.measureColumnsTable.Rows[i]["ManuallyEdited"];
            }
            // Measure Table Definitions
            List<Acorn.RARepository.RAMeasureTable> mtlist = new List<Acorn.RARepository.RAMeasureTable>();
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (rtlist.IndexOf(mt.TableSource.Connection) < 0)
                    continue;
                RARepository.RAMeasureTable rmt = new RARepository.RAMeasureTable();
                if (!tset.ContainsKey(mt.TableName))
                    tset.Add(mt.TableName, null);
                rmt.Name = mt.TableName;
                rmt.Query = mt.OpaqueView;
                rmt.Source = new TableSource();
                rmt.Source.Connection = mt.TableSource.Connection;
                rmt.Source.Schema = mt.TableSource.Schema;
                rmt.Source.Filters = mt.TableSource.Filters;
                rmt.Source.Tables = new TableSource.TableDefinition[mt.TableSource.Tables.Length];
                rmt.Cacheable = mt.Cacheable;
                rmt.TTL = mt.TTL;
                for (int j = 0; j < mt.TableSource.Tables.Length; j++)
                {
                    rmt.Source.Tables[j] = new TableSource.TableDefinition();
                    rmt.Source.Tables[j].PhysicalName = mt.TableSource.Tables[j].PhysicalName;
                    rmt.Source.Tables[j].JoinClause = mt.TableSource.Tables[j].JoinClause;
                    rmt.Source.Tables[j].JoinType = mt.TableSource.Tables[j].JoinType;
                }
                rmt.Type = mt.Type;
                rmt.PhysicalName = mt.PhysicalName;
                rmt.Cacheable = mt.Cacheable;
                rmt.TTL = mt.TTL;
                rmt.Cardinality = mt.Cardinality;
                rmt.AutoGenerated = mt.AutoGenerated;
                DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(mt.TableName);
                rmt.Mappings = new RARepository.RAMeasureColumnMapping[drv.Length];
                for (int j = 0; j < drv.Length; j++)
                {
                    rmt.Mappings[j] = new RARepository.RAMeasureColumnMapping();
                    rmt.Mappings[j].ColumnName = (string)drv[j]["ColumnName"];
                    rmt.Mappings[j].Formula = (string)drv[j]["PhysicalName"];
                    rmt.Mappings[j].AutoGenerated = (bool)drv[j]["AutoGenerated"];
                    rmt.Mappings[j].ManuallyEdited = (bool)drv[j]["ManuallyEdited"];
                }
                mtlist.Add(rmt);
            }
            rap.MeasureTables = mtlist.ToArray();
            // Joins
            List<RARepository.RAJoin> jlist = new List<RARepository.RAJoin>();
            foreach (DataRow dr in maf.Joins.Rows)
            {
                if (tset.ContainsKey((string)dr["Table1"]) && tset.ContainsKey((string)dr["Table2"]))
                {
                    RARepository.RAJoin raj = new RARepository.RAJoin();
                    raj.Table1 = (string)dr["Table1"];
                    raj.Table2 = (string)dr["Table2"];
                    raj.JoinCondition = (string)dr["JoinCondition"];
                    raj.Redundant = (bool)dr["Redundant"];
                    raj.JoinType = (string)dr["JoinType"];
                    raj.Invalid = (bool)dr["Invalid"];
                    raj.AutoGenerated = (bool)dr["AutoGenerated"];
                    raj.Federated = (bool)dr["Federated"];
                    raj.FederatedJoinKey = (string)dr["FederatedJoinKeys"];
                    jlist.Add(raj);
                }
            }
            rap.Joins = jlist.ToArray();
            rap.queryLanguageVersion = getQueryLanguageVersion(maf);
            return rap;
        }

        public static void setRepository(RARepository rap, MainAdminForm maf, Space sp, System.Web.SessionState.HttpSessionState session, User u)
        {
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);
            try
            {
                // List of table names
                Dictionary<string, string> tableList = new Dictionary<string, string>();
                // Get a list of realtime connections
                List<string> rtlist = new List<string>();
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                    if (dc.Realtime)
                        rtlist.Add(dc.Name);
                maf.DimensionalStructure = rap.DimensionalStructure;
                // Update hierarchies
                foreach (Hierarchy h in rap.Hierarchies)
                {
                    Hierarchy curh = null;
                    bool hasGuid = true;
                    if (h.Guid == null || Guid.Empty == h.Guid)
                    {
                        hasGuid = false;
                    }
                    foreach (Hierarchy sh in maf.hmodule.getHierarchies())
                    {
                        if (hasGuid && sh.Guid != null && sh.Guid != Guid.Empty) //match GUID first as names may differ if changing dimension name from Manage Local Sources UI
                        {
                            if (sh.Guid.Equals(h.Guid))
                            {
                                curh = sh;
                                break;
                            }
                        }
                        else if (sh.Name == h.Name) //Match name only if GUID not available or GUID not matching. i.e. pre 5.2 repository may not have GUID
                        {
                            curh = sh;
                            break;
                        }
                    }
                    if (curh != null)
                    {
                        if (curh.Locked || curh.AutoGenerated)
                            continue;
                        curh.DimensionName = h.DimensionName;
                        curh.DimensionKeyLevel = h.DimensionKeyLevel;
                        curh.OlapDimensionName = h.OlapDimensionName;
                        curh.SharedChildren = h.SharedChildren;
                        curh.Name = h.Name;
                        List<Level> llist = new List<Level>();
                        h.getChildLevels(llist);
                        foreach (Level l in llist)
                        {
                            Level curl = curh.findLevel(l.Name);
                            if (curl != null)
                            {
                                l.Cardinality = curl.Cardinality;
                                l.Degenerate = curl.Degenerate;
                                l.GenerateDimensionTable = curl.GenerateDimensionTable;
                                l.GenerateInheritedCurrentDimTable = curl.GenerateInheritedCurrentDimTable;
                                l.Locked = curl.Locked;
                                l.SCDType = curl.SCDType;
                            }
                        }
                        curh.Children = h.Children;
                        maf.hmodule.updateHierarchy(curh);
                    }
                    else
                    {
                        maf.hmodule.addHierarchy(h);
                    }
                }
                // Delete removed hierarchies
                List<Hierarchy> hToRemove = new List<Hierarchy>();
                foreach (Hierarchy h in maf.hmodule.getHierarchies())
                {
                    bool found = false;
                    bool hasGuid = true;
                    if (h.Guid == null || Guid.Empty == h.Guid)
                    {
                        hasGuid = false;
                    }
                    foreach (Hierarchy sh in rap.Hierarchies)
                    {
                        if (hasGuid && sh.Guid != null && sh.Guid != Guid.Empty) //match GUID first as names may differ if changing dimension name from Manage Local Sources UI
                        {
                            if (sh.Guid.Equals(h.Guid))
                            {
                                found = true;
                                break;
                            }
                        }
                        else if (h.Name == sh.Name) //Match name only if GUID not available or GUID not matching. i.e. pre 5.2 repository may not have GUID
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found && !(maf.timeDefinition != null && maf.timeDefinition.Name == h.DimensionName) && !h.Locked)
                    {
                        hToRemove.Add(h);
                    }
                }
                foreach (Hierarchy h in hToRemove)
                    maf.hmodule.removeHierarchyWithoutRefresh(h);
                List<DimensionTable> toRemove = new List<DimensionTable>();
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (rtlist.IndexOf(dt.TableSource.Connection) >= 0)
                        toRemove.Add(dt);
                }
                maf.dimensionColumns.BeginLoadData();
                maf.dimensionColumnMappings.BeginLoadData();
                foreach (RARepository.RADimension dim in rap.Dimensions)
                {
                    bool found = false;
                    foreach (Dimension d in maf.dimensionsList)
                    {
                        if (d.Name == dim.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        maf.addDimension(dim.Name);
                    DataRow[] curRows = maf.dimensionColumns.Select("[DimensionName]='" + dim.Name + "'");
                    // Remove any deleted rows
                    if (!dim.Locked)
                        foreach (DataRow dr in curRows)
                        {
                            found = false;
                            foreach (RARepository.RADimensionColumn dc in dim.Columns)
                            {
                                if ((string)dr["ColumnName"] == dc.ColumnName)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                                maf.dimensionColumns.Rows.Remove(dr);
                        }
                    // Add/update rows
                    foreach (RARepository.RADimensionColumn dc in dim.Columns)
                    {
                        DataRow dr = maf.addDimensionColumnDataRowIfNecessary(dim.Name, dc.ColumnName, dc.DataType, dc.Format, dc.Width, null, false);
                        dr["AutoGenerated"] = dc.AutoGenerated;
                        dr["ManuallyEdited"] = dc.ManuallyEdited;
                    }
                    foreach (RARepository.RADimensionTable def in dim.Definitions)
                    {
                        DimensionTable dt = null;
                        foreach (DimensionTable sdt in maf.dimensionTablesList)
                        {
                            if (sdt.TableName == def.Name)
                            {
                                dt = sdt;
                                toRemove.Remove(dt);
                                break;
                            }
                        }
                        if (dt == null)
                        {
                            Hierarchy h = maf.hmodule.getDimensionHierarchy(dim.Name);
                            if (h == null)
                                continue;
                            Level l = h.findLevel(def.Level);
                            if (l == null)
                                continue;
                            dt = new DimensionTable();
                            dt.Type = DimensionTable.NORMAL;
                            dt.TableSource = new TableSource();
                            maf.addDimensionTable(dt, l, false, -1);
                        }
                        dt.TableName = def.Name;
                        dt.DimensionName = dim.Name;
                        dt.Level = def.Level;
                        dt.AutoGenerated = def.AutoGenerated;
                        if (!tableList.ContainsKey(def.Name))
                            tableList.Add(def.Name, null);
                        DataRowView[] curMappings = maf.dimensionColumnMappingsTablesView.FindRows(dt.TableName);
                        // Remove any deleted rows
                        foreach (DataRowView drv in curMappings)
                        {
                            found = false;
                            foreach (RARepository.RADimensionColumnMapping map in def.Mappings)
                            {
                                if ((string)drv.Row["ColumnName"] == map.ColumnName)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                                maf.dimensionColumnMappings.Rows.Remove(drv.Row);
                        }
                        // Add/update rows
                        foreach (RARepository.RADimensionColumnMapping map in def.Mappings)
                        {
                            DataRowView[] drv = maf.dimensionColumnMappingsView.FindRows(new object[] { map.ColumnName, def.Name });
                            DataRow dr = drv.Length == 0 ? null : drv[0].Row;
                            if (dr == null)
                            {
                                dr = maf.dimensionColumnMappings.NewRow();
                                maf.dimensionColumnMappings.Rows.Add(dr);
                            }
                            dr["TableName"] = def.Name;
                            dr["ColumnName"] = map.ColumnName;
                            dr["PhysicalName"] = map.Formula;
                            dr["Qualify"] = false;
                            dr["AutoGenerated"] = map.AutoGenerated;
                            dr["ManuallyEdited"] = map.ManuallyEdited;
                        }
                        // Setup table definitions
                        dt.TableSource.Connection = def.Source.Connection;
                        dt.TableSource.Schema = def.Source.Schema;
                        dt.TableSource.Filters = def.Source.Filters;
                        dt.TableSource.Tables = new TableSource.TableDefinition[def.Source.Tables.Length];
                        dt.Cacheable = def.Cacheable;
                        dt.TTL = def.TTL;
                        for (int i = 0; i < def.Source.Tables.Length; i++)
                        {
                            dt.TableSource.Tables[i] = new TableSource.TableDefinition();
                            dt.TableSource.Tables[i].PhysicalName = def.Source.Tables[i].PhysicalName;
                            dt.TableSource.Tables[i].JoinClause = def.Source.Tables[i].JoinClause;
                            dt.TableSource.Tables[i].JoinType = def.Source.Tables[i].JoinType;
                        }
                        //now wanted to save table type and query (OpaQueue view)
                        dt.OpaqueView = def.Query;
                        dt.Type = def.Type;
                        if (def.Type == DimensionTable.OPAQUE_VIEW)
                            dt.PhysicalName = def.PhysicalName;
                    }
                }
                foreach (DimensionTable dt in toRemove)
                    maf.removeDimensionTable(dt.TableName, true);
                // Remove any unneeded dimensions
                List<Dimension> dToRemove = new List<Dimension>();
                foreach (Dimension d in maf.dimensionsList)
                {
                    bool found = false;
                    foreach (RARepository.RADimension dim in rap.Dimensions)
                    {
                        if (dim.Name == d.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found && !(maf.timeDefinition != null && maf.timeDefinition.Name == d.Name))
                    {
                        // Make sure there are no tables using it
                        foreach (DimensionTable dt in maf.dimensionTablesList)
                        {
                            if (dt.DimensionName == d.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            dToRemove.Add(d);
                    }
                }
                foreach (Dimension d in dToRemove)
                    maf.dimensionsList.Remove(d);
                maf.dimensionColumns.EndLoadData();
                maf.dimensionColumnMappings.EndLoadData();
                maf.setMeasureColumnCache();
                maf.measureTables.BeginLoadData();
                maf.measureColumnsTable.BeginLoadData();
                maf.measureColumnMappings.BeginLoadData();
                // Add any necessary logical measure columns
                foreach (RARepository.RAMeasureColumn mc in rap.Measures)
                {
                    DataRow dr = maf.addMeasureColumnDataRowIfNecessary(mc.ColumnName, mc.DataType, mc.Format, mc.Width, mc.AggRule, false, false, false);
                    dr["AutoGenerated"] = mc.AutoGenerated;
                    dr["ManuallyEdited"] = mc.ManuallyEdited;
                }
                List<MeasureTable> mtoRemove = new List<MeasureTable>();
                foreach (MeasureTable mt in maf.measureTablesList)
                {
                    if (rtlist.IndexOf(mt.TableSource.Connection) >= 0)
                        mtoRemove.Add(mt);
                }
                foreach (RARepository.RAMeasureTable rmt in rap.MeasureTables)
                {
                    MeasureTable mt = null;
                    foreach (MeasureTable smt in maf.measureTablesList)
                    {
                        if (smt.TableName == rmt.Name)
                        {
                            mt = smt;
                            mtoRemove.Remove(mt);
                            break;
                        }
                    }
                    if (mt == null)
                    {
                        mt = new MeasureTable();
                        maf.addNewMeasureTable(mt);
                        mt.TableName = rmt.Name;
                        mt.Type = MeasureTable.NORMAL;
                        mt.TableSource = new TableSource();
                    }
                    if (!tableList.ContainsKey(mt.TableName))
                        tableList.Add(mt.TableName, null);
                    DataRowView[] curMappings = maf.measureColumnMappingsTablesView.FindRows(mt.TableName);
                    // Remove any deleted rows
                    foreach (DataRowView drv in curMappings)
                    {
                        bool found = false;
                        foreach (RARepository.RAMeasureColumnMapping map in rmt.Mappings)
                        {
                            if ((string)drv.Row["ColumnName"] == map.ColumnName)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            maf.measureColumnMappings.Rows.Remove(drv.Row);
                    }
                    // Add/update rows
                    foreach (RARepository.RAMeasureColumnMapping map in rmt.Mappings)
                    {
                        DataRowView[] drv = maf.measureColumnMappingsNamesView.FindRows(new object[] { map.ColumnName });
                        DataRow dr = null;
                        foreach (DataRowView drvx in drv)
                            if ((string)drvx.Row["TableName"] == rmt.Name)
                            {
                                dr = drvx.Row;
                                break;
                            }
                        if (dr == null)
                        {
                            dr = maf.measureColumnMappings.NewRow();
                            maf.measureColumnMappings.Rows.Add(dr);
                        }
                        dr["TableName"] = rmt.Name;
                        dr["ColumnName"] = map.ColumnName;
                        dr["PhysicalName"] = map.Formula;
                        dr["Qualify"] = false;
                        dr["AutoGenerated"] = map.AutoGenerated;
                        dr["ManuallyEdited"] = map.ManuallyEdited;
                    }
                    // Setup table definitions
                    mt.TableSource.Connection = rmt.Source.Connection;
                    mt.TableSource.Schema = rmt.Source.Schema;
                    mt.TableSource.Filters = rmt.Source.Filters;
                    mt.TableSource.Tables = new TableSource.TableDefinition[rmt.Source.Tables.Length];
                    mt.Cacheable = rmt.Cacheable;
                    mt.Cardinality = rmt.Cardinality;
                    mt.TTL = rmt.TTL;
                    mt.AutoGenerated = rmt.AutoGenerated;
                    for (int i = 0; i < rmt.Source.Tables.Length; i++)
                    {
                        mt.TableSource.Tables[i] = new TableSource.TableDefinition();
                        mt.TableSource.Tables[i].PhysicalName = rmt.Source.Tables[i].PhysicalName;
                        mt.TableSource.Tables[i].JoinClause = rmt.Source.Tables[i].JoinClause;
                        mt.TableSource.Tables[i].JoinType = rmt.Source.Tables[i].JoinType;
                    }
                    //now wanted to save table type and query (OpaQueue view)
                    mt.OpaqueView = rmt.Query;
                    mt.Type = rmt.Type;
                    if (rmt.Type == MeasureTable.OPAQUE_VIEW)
                        mt.PhysicalName = rmt.PhysicalName;
                }
                //remove measure columns
                List<string> mctoRemove = new List<string>();
                HashSet<string> mcset = new HashSet<string>();
                foreach (RARepository.RAMeasureColumn mc in rap.Measures)
                {
                    mcset.Add(mc.ColumnName);
                }
                for (int i = 0; i < maf.measureColumnsTable.Rows.Count; i++)
                {
                    string columnName = (string)maf.measureColumnsTable.Rows[i]["ColumnName"];
                    bool found = mcset.Contains(columnName);
                    if (!found)
                        mctoRemove.Add(columnName);
                }
                if (mctoRemove.Count > 0)
                    maf.removeMeasureColumnDataRows(mctoRemove);
                foreach (MeasureTable mt in mtoRemove)
                    maf.removeMeasureTableDefinition(mt.TableName, true);
                maf.measureTables.EndLoadData();
                maf.measureColumnsTable.EndLoadData();
                maf.measureColumnMappings.EndLoadData();
                // Update joins
                List<DataRow> jlist = new List<DataRow>();
                foreach (DataRow dr in maf.Joins.Rows)
                {
                    if (tableList.ContainsKey((string)dr["Table1"]) && tableList.ContainsKey((string)dr["Table2"]))
                        jlist.Add(dr);
                }
                maf.Joins.BeginLoadData();
                foreach (DataRow dr in jlist)
                    maf.Joins.Rows.Remove(dr);
                foreach (RARepository.RAJoin raj in rap.Joins)
                {
                    DataRow dr = maf.Joins.NewRow();
                    dr["Table1"] = raj.Table1;
                    dr["Table2"] = raj.Table2;
                    dr["JoinCondition"] = raj.JoinCondition;
                    dr["JoinType"] = raj.JoinType;
                    dr["Redundant"] = raj.Redundant;
                    dr["Invalid"] = raj.Invalid;
                    dr["AutoGenerated"] = raj.AutoGenerated;
                    dr["Federated"] = raj.Federated;
                    dr["FederatedJoinKeys"] = raj.FederatedJoinKey;
                    maf.Joins.Rows.Add(dr);
                }
                maf.Joins.EndLoadData();
                Util.saveApplication(maf, sp, session, u);
            }
            finally
            {
                maf.setInUberTransaction(previousUberTransaction);
            }
        }

        public static List<string> getCustomUploadSourcesForSpace(MainAdminForm maf)
        {
            if (maf == null)
                return null;
            List<string> customUploadFiles = new List<string>();
            List<SourceFile> files = maf.sourceFileMod.getSourceFiles();
            if (files == null)
                return null;
            foreach (SourceFile sf in files)
            {
                if (sf.CustomUpload)
                {
                    if (sf.FileName.IndexOf('.') >= 0)
                        customUploadFiles.Add(sf.FileName.Substring(0, sf.FileName.LastIndexOf('.')));
                    else
                        customUploadFiles.Add(sf.FileName);
                }
            }
            return customUploadFiles;
        }

        public static void createPublishKillLockFile(Space sp)
        {
            string killPublishLockFileName = null;
            try
            {
                killPublishLockFileName = getPublishKillLockFileName(sp);
                if (!File.Exists(killPublishLockFileName))
                {
                    File.Create(killPublishLockFileName);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while creating " + killPublishLockFileName, ex);
            }
        }

        public static string getPublishKillLockFileName(Space sp)
        {
            return sp.Directory + "\\" + Util.PUBLISH_KILL_LOCK_FILE;
        }

        public static void createIfNotExistsSFDCKillLockFile(Space sp)
        {
            if (sp == null)
            {
                return;
            }

            FileStream fs = null;
            string killLockFileName = getSFDCKillLockFileName(sp);
            try
            {
                if (!Util.isSFDCKillLockFilePresent(sp))
                {
                    fs = File.Create(killLockFileName);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in creating " + killLockFileName, ex);
            }
            finally
            {
                if (fs != null)
                {
                    try
                    {
                        fs.Close();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Exception in closing filestream for " + killLockFileName, ex2);
                    }
                }
            }
        }

        public static void createIfNotExistsSFDCExtractStatusFilePresent(Space sp)
        {
            if (sp == null)
            {
                return;
            }

            FileStream fs = null;
            string extractFileName = getSFDCExtractStatusFileName(sp);
            try
            {
                if (!isSFDCExtractStatusFilePresent(sp))
                {
                    fs = File.Create(extractFileName);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in creating " + extractFileName, ex);
            }
            finally
            {
                if (fs != null)
                {
                    try
                    {
                        fs.Close();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Exception in closing filestream for " + extractFileName, ex2);
                    }
                }
            }
        }

        public static void deleteSFDCExtractStatusFileIfItExists(Space sp, bool force, int hoursAfterWhichToDelete)
        {
            // Default duration older than 1 day. Delete the file
            hoursAfterWhichToDelete = hoursAfterWhichToDelete <= 0 ? 24 : hoursAfterWhichToDelete;
            double durationAfterWhichToDelete = hoursAfterWhichToDelete * 60 * 60; ;

            string extractFileName = getSFDCExtractStatusFileName(sp);
            if (extractFileName != null)
            {
                try
                {
                    FileInfo fi = new FileInfo(extractFileName);
                    if (fi != null && fi.Exists)
                    {
                        if (force || DateTime.Now.Subtract(fi.CreationTime).TotalSeconds > durationAfterWhichToDelete)
                            fi.Delete();
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in deleting " + extractFileName, ex);
                }
            }
        }

        public static string getSFDCExtractStatusFileName(Space sp)
        {
            string extractFileName = null;
            if (sp != null)
            {
                extractFileName = sp.Directory + "\\" + Util.SFDC_EXTRACT_LOCK_FILE;
            }

            return extractFileName;
        }

        public static string getSFDCKillLockFileName(Space sp)
        {
            string killLockFileName = null;
            if (sp != null)
            {
                killLockFileName = sp.Directory + "\\" + Util.SFDC_KILL_LOCK_FILE;
            }

            return killLockFileName;
        }

        public static bool isSFDCExtractStatusFilePresent(Space sp)
        {
            bool exists = false;

            string extractFileName = getSFDCExtractStatusFileName(sp);
            FileInfo fi = new FileInfo(extractFileName);
            if (fi.Exists)
            {
                exists = true;
            }

            return exists;
        }

        public static bool isSFDCKillLockFilePresent(Space sp)
        {
            bool exists = false;

            string lockFileName = getSFDCKillLockFileName(sp);
            FileInfo fi = new FileInfo(lockFileName);
            if (fi.Exists)
            {
                exists = true;
            }

            return exists;
        }

        public static string getLoadLockFileName(Space sp)
        {
            string extractFileName = null;
            if (sp != null)
            {
                extractFileName = sp.Directory + "\\" + Util.LOAD_LOCK_FILE;
            }

            return extractFileName;
        }

        public static bool isLoadLockFilePresent(Space sp)
        {
            bool exists = false;
            try
            {
                string lockFileName = getLoadLockFileName(sp);
                FileInfo fi = new FileInfo(lockFileName);
                if (fi.Exists)
                {
                    exists = true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while checking the existing of load.lock file : space : " + (sp != null ? sp.ID.ToString() : ""), ex);
            }
            return exists;
        }

        public static void deleteLoadLockFile(Space sp)
        {
            try
            {
                if (isLoadLockFilePresent(sp))
                {
                    File.Delete(getLoadLockFileName(sp));
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while deleting load lock file : space : " + (sp != null ? sp.ID.ToString() : " "), ex);
            }
        }

        public static bool isSFDCExtractRunning(Space sp)
        {
            bool extractRunning = false;
            try
            {
                // Check the extract file first, 
                extractRunning = Util.isSFDCExtractStatusFilePresent(sp);

                // if not there then check the load schedule table
                if (!extractRunning)
                {

                    List<ScheduledItem> sItemList = Database.getTaskScheduleLoadStatus(sp.ID, SalesforceLoader.SOURCE_GROUP_NAME);
                    if (sItemList != null && sItemList.Count > 0)
                    {
                        foreach (ScheduledItem sItem in sItemList)
                        {
                            if (sItem.WorkTime != DateTime.MinValue)
                            {
                                // if the job has already been picked up and is running for less than 12 hours (a randomn upper limit)
                                TimeSpan tspan = DateTime.Now - sItem.WorkTime;
                                if (tspan.Minutes < 12 * 60)
                                {
                                    extractRunning = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting ScheduleItemList for space " + sp.ID, ex);
            }

            return extractRunning;
        }

        public static void clearSFDCExtractLockFiles(Dictionary<Guid, string> spacesDirectoryMap)
        {
            if (spacesDirectoryMap == null || spacesDirectoryMap.Count == 0)
            {
                return;
            }

            try
            {
                foreach (KeyValuePair<Guid, string> spaceInfo in spacesDirectoryMap)
                {
                    Guid spaceID = spaceInfo.Key;
                    string spaceDir = spaceInfo.Value;
                    if (spaceID == Guid.Empty || spaceDir == null)
                    {
                        Global.systemLog.Warn("Skipping the removal of SFDC extract file for " + spaceID + " with directory " + spaceDir);
                        continue;
                    }
                    deleteSFDCExtractFile(spaceDir);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("clearSFDCExtractLockFiles: Exception while deleting SFDC kill lock files", ex);
            }
        }

        public static void deleteSFDCExtractFile(string spaceDir)
        {
            try
            {
                if (Directory.Exists(spaceDir))
                {
                    string extractLockFilePath = spaceDir + "\\" + Util.SFDC_EXTRACT_LOCK_FILE;
                    FileInfo fi = new FileInfo(extractLockFilePath);
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in deleting extract lock file for " + spaceDir, ex);
            }
        }

        public static void clearSFDCKillLockFiles(Dictionary<Guid, string> spacesDirectoryMap)
        {
            if (spacesDirectoryMap == null || spacesDirectoryMap.Count == 0)
            {
                return;
            }

            try
            {
                foreach (KeyValuePair<Guid, string> spaceInfo in spacesDirectoryMap)
                {
                    Guid spaceID = spaceInfo.Key;
                    string spaceDir = spaceInfo.Value;
                    if (spaceID == Guid.Empty || spaceDir == null)
                    {
                        Global.systemLog.Warn("Skipping the removal of SFDC extract file for " + spaceID + " with directory " + spaceDir);
                        continue;
                    }

                    deleteSFDCKillLockFile(spaceDir);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("clearSFDCKillLockFiles: Exception while deleting SFDC kill lock files", ex);
            }
        }

        public static void deleteSFDCKillLockFile(Space sp)
        {
            if (sp != null && sp.Directory != null)
            {
                deleteSFDCKillLockFile(sp.Directory);
            }
        }

        public static void deleteSFDCKillLockFile(string spaceDirectory)
        {
            try
            {
                if (Directory.Exists(spaceDirectory))
                {
                    string killLockFilePath = spaceDirectory + "\\" + Util.SFDC_KILL_LOCK_FILE;
                    FileInfo fi = new FileInfo(killLockFilePath);
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in deleting SFDC kill lock file for " + spaceDirectory, ex);
            }
        }

        /*
         * assumes that size is already in MB
         */
        public static string formatBytes(long megaBytes)
        {
            return formatBytes(megaBytes, null);
        }

        /*
         * assumes that size is already in MB
         */
        public static string formatBytes(long megaBytes, CultureInfo locale)
        {
            string[] Suffix = { /* "B", "KB",*/ "MB", "GB", "TB" };
            int i;
            double dblSByte = megaBytes;
            for (i = 0; (Int64)(megaBytes / 1024) > 0; i++, megaBytes /= 1024)
                dblSByte = megaBytes / 1024.0;
            return dblSByte.ToString("#,##0.0", locale) + Suffix[i];
        }

        public static string formatCount(long count)
        {
            return formatCount(count, null);
        }

        public static string formatCount(long count, CultureInfo locale)
        {
            string[] Suffix = { "", "K", "M", "G", "T" };
            int i;
            double dblSCount = 0;
            if (count < 1000)
            {
                return count.ToString("0", locale);
            }
            for (i = 0; (int)(count / 1000) > 0; i++, count /= 1000)
                dblSCount = count / 1000.0;
            return dblSCount.ToString("#,##0.0", locale) + Suffix[i];
        }

        /// <summary>
        /// Checks if the space name is duplicate. If it is, returns the duplicate space
        /// </summary>
        /// <param name="u"></param>
        /// <param name="spaceName"></param>
        /// <returns></returns>
        public static Space checkForDuplicateSpace(User u, String spaceName)
        {
            Space duplicateSpace = null;
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceMembership> mlist = Database.getSpaces(conn, schema, u, true);
                // Prevent duplicates
                foreach (SpaceMembership sm in mlist)
                {
                    if (sm.Space.Name.ToLower() == spaceName.ToLower())
                    {
                        duplicateSpace = sm.Space;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while running isDuplicate function ", ex);
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    try
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Problem in releasing connection in isDuplicate function ", ex2);
                    }
                }
            }
            return duplicateSpace;
        }


        /*
         * Iterate over all staging tables to get a list of all load groups that need to be loaded
         */
        public static HashSet<string> getLoadGroups(MainAdminForm maf)
        {
            HashSet<string> loadGroups = new HashSet<string>();
            List<StagingTable> stagingTables = maf.stagingTableMod.getStagingTables();
            if (stagingTables != null && stagingTables.Count > 0)
            {
                foreach (StagingTable st in stagingTables)
                {
                    //Ignore the disable staging tables that are not going to be loaded
                    if (st.Disabled)
                    {
                        continue;
                    }
                    foreach (string lg in st.LoadGroups)
                    {
                        loadGroups.Add(lg);
                    }
                }
            }
            return loadGroups;
        }

        public static string getDBTypeForConnection(MainAdminForm maf, string connectionName)
        {
            string dbType = null;
            if (maf.connection != null && maf.connection.connectionList != null)
            {
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    if (dc.Name == connectionName)
                    {
                        dbType = dc.Type;
                        break;
                    }
                }
            }
            return dbType;
        }

        public static Space getSpaceByDirectory(string spaceDirectory)
        {
            Space sp = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                sp = Database.getSpace(conn, Util.getMainSchema(), Guid.Empty, spaceDirectory);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return sp;
        }

        public static Space getSpaceById(Guid spaceID)
        {
            Space sp = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                sp = Database.getSpace(conn, Util.getMainSchema(), spaceID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return sp;
        }

        public static User getUserById(Guid userID)
        {
            User user = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                user = Database.getUserById(conn, Util.getMainSchema(), userID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return user;
        }

        /*
         * validate that the user has a non-expired 'EDITION' product
         */
        public static bool doesUserHaveValidEdition(QueryConnection conn, string mainSchema, User u)
        {
            List<Product> plist = Database.getProducts(conn, mainSchema, u.ID);
            foreach (Product p in plist)
            {
                if (p.Type == Product.TYPE_EDITION)
                {
                    return true;
                }
            }
            return false;
        }

        public static Space getSpaceById(QueryConnection conn, string mainSchema, User u, Guid id)
        {
            Space sp = null;
            List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, true);
            if (smlist == null || smlist.Count == 0)
            {
                return null;
            }
            foreach (SpaceMembership sm in smlist)
            {
                if (sm.Space.ID == id)
                {
                    sp = sm.Space;
                    break;
                }
            }
            return sp;
        }

        public static Space getSpaceByName(QueryConnection conn, string mainSchema, User u, string name)
        {
            Space sp = null;
            List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, true);
            if (smlist == null || smlist.Count == 0)
            {
                return null;
            }
            foreach (SpaceMembership sm in smlist)
            {
                if (sm.Space.Name == name)
                {
                    sp = sm.Space;
                    break;
                }
            }
            return sp;
        }

        public static User getSpaceOwner(Space sp)
        {
            QueryConnection conn = null;
            User u = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceMembership> smlist = Database.getUsers(conn, Util.getMainSchema(), sp, true);
                foreach (SpaceMembership sm in smlist)
                {
                    if (sm.User == null)
                        continue;
                    if (sm.Owner)
                    {
                        u = sm.User;
                        break;
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return u;
        }


        /*
         * set logging information
         */
        public static void setLoggingNDC(HttpSessionState session)
        {
            User u = null;
            Space sp = null;
            if (session != null)
            {
                u = (User)session["user"];
                sp = (Space)session["space"];
            }
            setLogging(u, sp);
        }

        public static void setLogging(User u, Space sp)
        {
            setLogging(u, sp, null);
        }

        public static void setLogging(User u, Space sp, string extractionUID)
        {
            log4net.NDC.Clear();
            if (u != null)
            {
                string val = u.Username;
                if (sp != null)
                {
                    val = val + "," + sp.ID;
                }
                if (extractionUID != null)
                {
                    val = val + "," + extractionUID;
                }
                log4net.NDC.Push(val);
            }
        }

        public static bool matchesImportedSource(HttpSessionState session, MainAdminForm maf, string fileName)
        {
            if (fileName != null && maf != null && maf.Imports != null && maf.Imports.Length > 0)
            {
                string stagingTableName = maf.stagingTableMod.getStagingNameFromSourceFileName(fileName, false, maf);
                if (stagingTableName.Length > 255)
                    stagingTableName = stagingTableName.Substring(0, 255);
                List<ImportedRepositoryItem> itList = getImportedTables(maf, session, stagingTableName);
                // its possible that imported source could be live access whose name will be name without ST_..check for that
                if (itList == null || itList.Count == 0 || itList[0].st == null)
                {
                    string potentialImportedLiveAccessSource = maf.stagingTableMod.getStagingNameFromSourceFileName(fileName, true, maf);
                    itList = getImportedTables(maf, session, potentialImportedLiveAccessSource);
                }

                if (itList != null && itList.Count > 0 && itList[0].st != null)
                {
                    return true;
                }
            }
            return false;
        }

        public static string encryptXSRFToken(string text)
        {
            if (text == null) return null;

            AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
            provider.Mode = CipherMode.CBC;
            provider.Padding = PaddingMode.PKCS7;
            provider.BlockSize = 128;
            provider.KeySize = 128;
            byte[] pwdBytes = System.Text.Encoding.UTF8.GetBytes(Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            byte[] keyBytes = new byte[16];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length) len = keyBytes.Length;
            System.Array.Copy(pwdBytes, keyBytes, len);

            provider.Key = keyBytes;
            provider.IV = keyBytes;
            ICryptoTransform transform = provider.CreateEncryptor();

            byte[] plainText = Encoding.UTF8.GetBytes(text);
            byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);

            string newVal = Util.getHexString(cipherBytes);
            return newVal;
        }

        public static string encryptToken(string toEncrypt)
        {
            byte[] buff = Encoding.ASCII.GetBytes(toEncrypt);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            byte[] keybuff = new byte[aes.Key.Length];
            byte[] secretkey = Encoding.ASCII.GetBytes(Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            for (int i = 0; i < secretkey.Length && i < keybuff.Length; i++)
                keybuff[i] = secretkey[i];
            aes.Key = keybuff;
            keybuff = new byte[aes.IV.Length];
            for (int i = 0; i < secretkey.Length && i < keybuff.Length; i++)
                keybuff[i] = secretkey[i];
            aes.IV = keybuff;
            ICryptoTransform ict = aes.CreateEncryptor();
            byte[] tokenBytes = ict.TransformFinalBlock(buff, 0, buff.Length);
            return Util.getHexString(tokenBytes);
        }

        public static Guid decryptToken(string token)
        {
            byte[] tokenBytes = Util.getBytesFromHex(token);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            byte[] keybuff = new byte[aes.Key.Length];
            byte[] secretkey = Encoding.ASCII.GetBytes(Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            for (int i = 0; i < secretkey.Length && i < keybuff.Length; i++)
                keybuff[i] = secretkey[i];
            aes.Key = keybuff;
            keybuff = new byte[aes.IV.Length];
            for (int i = 0; i < secretkey.Length && i < keybuff.Length; i++)
                keybuff[i] = secretkey[i];
            aes.IV = keybuff;
            ICryptoTransform ict = aes.CreateDecryptor();
            byte[] buff;
            try
            {
                buff = ict.TransformFinalBlock(tokenBytes, 0, tokenBytes.Length);
            }
            catch (Exception)
            {
                Global.systemLog.Warn("Invalid BirstSSOToken - decrypt failed - " + token);
                return Guid.Empty;
            }

            Guid tokenID = Guid.Empty;
            try
            {
                tokenID = new Guid(Encoding.ASCII.GetString(buff));
            }
            catch (Exception)
            {
                Global.systemLog.Warn("Invalid BirstSSOToken - GUID construction failed - " + token);
                return Guid.Empty;
            }
            return tokenID;
        }

        public static bool isScriptedSource(StagingTable st)
        {
            bool scriptedSource = false;
            if (st != null && st.Script != null && st.Script.Script != null && st.Script.Script.Length > 0)
            {
                scriptedSource = true;
            }

            return scriptedSource;
        }

        /**
         * implement security settings via headers
         */

        // dump the request headers (allows us to validate that Host, X_FORWARDED_PROTO, X-Birst-URL are being set
        public static void dumpHeaders(HttpRequest request)
        {
            var headers = String.Empty;
            foreach (var key in request.Headers.AllKeys)
                Global.systemLog.Debug(key + "=" + request.Headers[key]);
        }

        // add headers to the response for a frameable entity
        public static void setUpHeaders(HttpResponse response)
        {
            setUpHeaders(response, true);
        }

        public static void setUpHeaders(HttpResponse response, bool frameable)
        {
            // stops us from being framed in other sites (okay for adhoc/dashboard, not okay for forms that capture credentials or other sensitive information)
            if (!frameable)
                response.AppendHeader("X-Frame-Options", "DENY");

            // do not cache pages
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetNoServerCaching();
            response.Cache.SetNoStore();
            response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            response.Cache.SetExpires(DateTime.Now);
        }

        /* This method effectively used only by the HTML5 dashboard page 
         */
        public static void injectCSRFToken(HttpResponse Response, HttpSessionState Session, string filename, string logoutScript)
        {
            string locale = "en_US";
            string bkColor = "#FFFFFF";
            string fgColor = "#2A303A";
            User u = Util.validateAuth(Session, Response);
            Space sp = (Space)Session["space"];
            if (sp != null && sp.Settings != null)
            {
                if (sp.Settings.HeaderBackgroundColor >= 0)
                {
                    bkColor = "#" + Util.getHexStringFromLongColor(sp.Settings.HeaderBackgroundColor);
                }
                if (sp.Settings.HeaderForegroundColor >= 0)
                {
                    fgColor = "#" + Util.getHexStringFromLongColor(sp.Settings.HeaderForegroundColor);
                }
            }
            else if (u.HeaderBackground >= 0) // XXX remove user headerbackground stuff
            {
                bkColor = "#" + Util.getHexStringFromLongColor(u.HeaderBackground);
            }

            Guid userID = ((User)Session["user"]).ID;
            IDictionary<string, string> preferences = ManagePreferences.getPreference(null, userID, "Locale");
            if (preferences["Locale"] != null)
                locale = preferences["Locale"].Replace('-', '_');

            string token = Util.encryptXSRFToken(Session.SessionID);
            StreamReader reader = null;
            StreamWriter writer = new StreamWriter(Response.OutputStream);
            try
            {
                reader = new StreamReader(filename);
                string file = reader.ReadToEnd();
                file = file.Replace("##BIRST_REPLACE_WITH_SPACE_BACKGROUND_COLOR##", bkColor);
                file = file.Replace("##BIRST_REPLACE_WITH_SPACE_FOREGROUND_COLOR##", fgColor);
                file = file.Replace("##Birst-Locale-Value##", locale);
                file = file.Replace("<!--##Logout-Script##-->", logoutScript);
                writer.Write(file);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            writer.Close();
        }

        /* currently used only by html5 home page*/
        public static void injectIntoPlaceholders(HttpResponse Response, HttpSessionState Session, string filename, string logoutScript)
        {
            string locale = "en_US";
            string supportLinkValue = "";
            string termsOfServiceLinkValue = "";
            string privacyPolicyLinkValue = "";
            string contactUsLinkValue = "";
            string helpLinkValue = "";
            string copyRightText = "";
            string birstTitle = "";
            string birstFaviconIcon = "/FaviconIconUrl.aspx"; 
            string logoutLinkValue = "";
            string token = Util.encryptXSRFToken(Session.SessionID);
            User user = null;
            if (user == null)
            {
                Util.getSessionUser(Session);
            }

            user = (User)Session["user"];
            Guid userID = ((User)Session["user"]).ID;
            IDictionary<string, string> preferences = ManagePreferences.getPreference(null, userID, "Locale");
            if (preferences["Locale"] != null)
                locale = preferences["Locale"].Replace('-', '_');

            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();

            overrideParameters = Util.getLocalizedOverridenParams(locale, user);
         
            if (user.isFreeTrialUser)
                helpLinkValue = "Help/BXE/index.htm";

            if (overrideParameters != null && overrideParameters.Count > 0)
            {
                if (overrideParameters.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                {
                    supportLinkValue = overrideParameters[OverrideParameterNames.SUPPORT_URL];
                }
                else
                {
                    supportLinkValue = "/Support.aspx";
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.HELP_URL))
                {
                    helpLinkValue = overrideParameters[OverrideParameterNames.HELP_URL];
                }
                else
                {
                    helpLinkValue = "/Help/Full/index.htm";
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                {
                    termsOfServiceLinkValue = overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL];

                }
                else
                {
                    termsOfServiceLinkValue = termsOfServiceURL;
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                {
                    privacyPolicyLinkValue = overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL];
                }
                else
                {
                    privacyPolicyLinkValue = privacyPolicyURL;
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                {
                    contactUsLinkValue = overrideParameters[OverrideParameterNames.CONTACT_US_URL];
                }
                else
                {
                    contactUsLinkValue = contactUsURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                {
                    copyRight = overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT];
                    copyRightText = HttpUtility.HtmlEncode(copyRight);
                }

                else
                {
                    copyRightText = HttpUtility.HtmlEncode(copyRight);
                }


                if (overrideParameters.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                {
                    birstTitle = overrideParameters[OverrideParameterNames.PAGE_TITLE];
                }

                else
                {
                    birstTitle = "Birst";
                }  

                if (overrideParameters.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                {
                    logoutLinkValue = overrideParameters[OverrideParameterNames.LOGOUT_URL];
                }
                else
                {
                    logoutLinkValue = "/Logout.aspx";
                }

            }
            else
            {
                string copyRight = Util.getCopyright();

                if (termsOfServiceURL != null)
                    termsOfServiceLinkValue = termsOfServiceURL;

                if (privacyPolicyURL != null)
                    privacyPolicyLinkValue = privacyPolicyURL;

                if (contactUsURL != null)
                    contactUsLinkValue = contactUsURL;

                copyRightText = HttpUtility.HtmlEncode(copyRight);

                supportLinkValue = "/Support.aspx";

                helpLinkValue = "/Help/Full/index.htm";

                birstTitle = "Birst";

           //     birstFaviconIcon = "/Images/favicon.ico";

                logoutLinkValue = "/Logout.aspx";

            }

            StreamReader reader = null;
            StreamWriter writer = new StreamWriter(Response.OutputStream);
            try
            {
                reader = new StreamReader(filename);
                string file = reader.ReadToEnd();
                file = file.Replace("##Birst-Locale-Value##", locale);
                file = file.Replace("##Birst-Support-Value##", supportLinkValue);
                file = file.Replace("##Birst-Help-Value##", helpLinkValue);
                file = file.Replace("##Birst-TermsOfService-Value##", termsOfServiceLinkValue);
                file = file.Replace("##Birst-PrivacyPolicy-Value##", privacyPolicyLinkValue);
                file = file.Replace("##Birst-ContactUs-Value##", contactUsLinkValue);
                file = file.Replace("##Birst-Title-Value##", birstTitle);
                file = file.Replace("##Birst-Fevicon-Icon##", birstFaviconIcon);
                file = file.Replace("##Birst-CopyRight-Value##", copyRightText);
                file = file.Replace("<!--##Logout-Script##-->", logoutScript);
                file = file.Replace("##Birst-Logout-Value##", logoutLinkValue);
                writer.Write(file);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            writer.Close();
        }

        public static string userLocale(User u)
        {
            string locale = "en_US";
            if (u != null)
            {
                Guid userID = u.ID;
                IDictionary<string, string> preferences = ManagePreferences.getPreference(null, userID, "Locale");
                if (preferences["Locale"] != null)
                    locale = preferences["Locale"].Replace('-', '_');
            }
            return locale;
        }

        public static string userLocale(HttpSessionState Session)
        {
            User u = (User)Session["user"];
            return userLocale(u);
        }

        public static Connection[] getConnections(Space sp, MainAdminForm maf)
        {
            //Always add default connection and it should be first connection
            List<Connection> connList = new List<Connection>();
            if (maf == null || maf.connection == null)
            {
                Global.systemLog.Debug("Unable to retrieve connection details" + (sp != null ? " for space " + sp.ID.ToString() : ""));
                Connection defaultConn = new Connection(Database.DEFAULT_CONNECTION, Database.DEFAULT_CONNECTION, false);
                connList.Add(defaultConn);
            }
            else
            {
                try
                {
                    Connection dconn = null;
                    DatabaseConnection defaultConn = maf.connection.findConnection(Database.DEFAULT_CONNECTION);
                    if (defaultConn == null)
                    {
                        dconn = new Connection(Database.DEFAULT_CONNECTION, Database.DEFAULT_CONNECTION, false);
                    }
                    else
                    {
                        dconn = new Connection(defaultConn);
                    }
                    connList.Add(dconn);
                    //get local etl connections
                    Dictionary<string, Connection> localETLConnections = BirstConnectUtil.getLocalETLConenctions(sp, maf);
                    // add connections
                    foreach (DatabaseConnection dc in maf.connection.connectionList)
                    {
                        string name = dc.Name;
                        if (name == Database.DEFAULT_CONNECTION) //Default Connection is already added
                            continue;
                        string visiblename = dc.VisibleName != null ? dc.VisibleName : name;
                        Connection conn = null;
                        if (localETLConnections != null && localETLConnections.ContainsKey(visiblename))
                        {
                            conn = localETLConnections[visiblename];
                        }
                        else
                        {
                            conn = new Connection(dc);
                        }
                        connList.Add(conn);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Problem in retrieving connection details" + (sp != null ? " for space " + sp.ID.ToString() : "") + ".", ex);
                }
            }
            return connList.ToArray();
        }

        public static string getServerIP()
        {
            string ipStr = null;
            foreach (System.Net.IPAddress ip in System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()))
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipStr = ip.ToString();
                    break;
                }
            }
            return ipStr;
        }

        public static string getServerPort()
        {
            return (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalAdminSitePort"];
        }

        public static string[][] getEscapes(string s)
        {
            if (s == null || s.Length == 0)
                return null;
            string[] pairs = Performance_Optimizer_Administration.Util.splitString(s, ',');
            List<string[]> results = new List<string[]>();
            foreach (string pair in pairs)
            {
                string[] parts = Performance_Optimizer_Administration.Util.splitString(pair, '=');
                if (parts.Length == 2)
                {
                    parts[0] = parts[0].Trim(new char[] { '"' });
                    parts[1] = parts[1].Trim(new char[] { '"' });
                    results.Add(parts);
                }
            }
            if (results.Count == 0)
                return null;
            return results.ToArray();
        }

        public static List<string> getTrustedInterServerIPs()
        {
            if (trustedInterServerIPs == null)
                return null;
            string[] validIPs = trustedInterServerIPs.Split(',');
            return new List<string>(validIPs);
        }

        public static List<string> getTrustedTrialRequestIPs()
        {
            if (trustedTrialRequestServerIPs == null)
                return null;
            string[] validIPs = trustedTrialRequestServerIPs.Split(',');
            return new List<string>(validIPs);
        }

        public static string getFullConnectString(string ConnectString, string username, string password)
        {
            string connectStr = "";
            string temp = ConnectString.ToLower();
            password = Performance_Optimizer_Administration.MainAdminForm.decrypt(password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            if (temp.StartsWith("jdbc:sqlserver://"))
            {
                connectStr = "Server=" + ConnectString.Substring(17).Replace("databaseName", "Database") + ";Uid=" + username + ";Pwd=" + password + ";Driver={SQL Native Client}";
                connectStr = connectStr.Replace(':', ',');
            }
            else if (temp.StartsWith("jdbc:mysql://"))
            {
                string server = ConnectString.Substring(13).Replace("/", "");
                int portindex = server.IndexOf(':');
                string port = "";
                if (portindex > 0)
                {
                    port = "port=" + server.Substring(portindex + 1) + ";";
                    server = server.Substring(0, portindex);
                }
                connectStr = "Server=" + server + ";Uid=" + username + ";Password=" + password + ";Driver={MySQL ODBC 5.1 Driver};" + port;
            }
            return (connectStr);
        }

        public static string createQueryString(HttpServerUtility server, System.Collections.Specialized.NameValueCollection Params)
        {
            string otherArguments = "";
            if (Params != null && Params.Count > 0)
            {
                bool firstParam = true;
                foreach (String key in Params.AllKeys)
                {
                    if (firstParam)
                    {
                        otherArguments = key + "=" + server.UrlEncode(Params[key]);
                        firstParam = false;
                    }
                    else
                    {
                        otherArguments = otherArguments + "&" + key + "=" + server.UrlEncode(Params[key]);
                    }
                }
            }
            return otherArguments;
        }

        public static string getBirstParam(HttpRequest request, string newName, string oldName)
        {
            string val = request.Params[newName];
            if (val == null || val.Length == 0)
                val = request.Params[oldName];
            return val;
        }

        // eventually this will go away, next pass will be to log anything that gets converted
        public static void fixupArguments(System.Collections.Specialized.NameValueCollection arguments)
        {
            // not needed past this point
            arguments.Remove("BirstSSOToken");
            arguments.Remove("ReturnUrl");
            arguments.Remove("birst.spaceId");
            arguments.Remove("birst.masterURL");

            // convert old style arguments to new ones
            fixupArgument("module", "birst.module", arguments);
            fixupArgument("embedded", "birst.embedded", arguments);
            fixupArgument("name", "birst.name", arguments);  // dashboard_page (obsolete, should be using birst.dashboard and birst.page)
            fixupArgument("dashboard", "birst.dashboard", arguments);
            fixupArgument("page", "birst.page", arguments);
            fixupArgument("hideDashboardNavigation", "birst.hideDashboardNavigation", arguments);
            fixupArgument("hideDashboardPrompts", "birst.hideDashboardPrompts", arguments);
            fixupArgument("viewMode", "birst.viewMode", arguments);
            fixupArgument("wmode", "birst.wmode", arguments);
        }

        private static void fixupArgument(string oldName, string newName, System.Collections.Specialized.NameValueCollection arguments)
        {
            string oldValue = arguments[oldName];
            if (oldValue != null && oldValue.Length > 0)
            {
                arguments.Remove(oldName);
                arguments[newName] = oldValue;
            }
        }

        /**
         * generate a JNLP file
         */
        private static string key = "AZ" + "RB" + "PZ" + "WN" + "Z4" + "WH" + "BH" + "C2" + "W9" + "2I";
        private static string REALM = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AuthenticationRealm"];
        private static string birstConnectURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BirstConnectURL"];
        private static string defaultConfigFile = "dcconfig.xml";

        public static string getSpaceJNLPFile(User u, Space sp, string path, string codebase, string configfile)
        {
            StreamReader reader = new StreamReader(path + "launch.jnlp");
            string jnlp = reader.ReadToEnd();
            reader.Close();
            if (birstConnectURL != null && birstConnectURL.Length > 0) // just in case we want to direct this elsewhere
                codebase = birstConnectURL;
            jnlp = jnlp.Replace("{CODEBASE}", codebase);
            jnlp = jnlp.Replace("{UNAME}", u.Username);
            jnlp = jnlp.Replace("{PWD}", MainAdminForm.encrypt(u.Password, key));
            jnlp = jnlp.Replace("{SPACEID}", sp.ID.ToString());
            jnlp = jnlp.Replace("{SPACENAME}", sp.Name);
            jnlp = jnlp.Replace("{REALM}", REALM);
            jnlp = jnlp.Replace("{DCCONFIG_FILE_NAME}", configfile != null ? configfile : defaultConfigFile);
            String proxyUser = null;
            String proxyPassword = null;
            if (!Util.getProxyServerInfo(path, sp.ID, ref proxyUser, ref proxyPassword))
            {
                proxyUser = "";
                proxyPassword = "";
            }
            jnlp = jnlp.Replace("{PROXY_UNAME}", proxyUser);
            jnlp = jnlp.Replace("{PROXY_PWD}", MainAdminForm.encrypt(proxyPassword, key));
            // See if the user has realtime
            List<Product> plist = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                plist = Database.getProducts(conn, mainSchema, u.ID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            bool hasRealtime = false;
            bool hasSAPConnector = false;
            bool hasLocalETL = false;
            foreach (Product p in plist)
            {
                if (p.Type == Product.TYPE_REALTIME)
                {
                    hasRealtime = true;
                }
                else if (p.Type == Product.TYPE_SAP_CONNECTOR)
                {
                    hasSAPConnector = true;
                }
                else if (p.Type == Product.TYPE_LOCAL_ETL_LOADER)
                {
                    hasLocalETL = true;
                }
            }
            if (hasRealtime)
            {
                jnlp = jnlp.Replace("{REALTIME}", "\r\n        <property name=\"jnlp.realtime\" value=\"True\"/>");
            }
            else
            {
                jnlp = jnlp.Replace("{REALTIME}", "");
            }
            if (hasSAPConnector)
            {
                jnlp = jnlp.Replace("{SAPCONNECT}", "\r\n        <property name=\"jnlp.sapconnect\" value=\"True\"/>");
            }
            else
            {
                jnlp = jnlp.Replace("{SAPCONNECT}", "");
            }
            if (hasLocalETL)
            {
                jnlp = jnlp.Replace("{LOCALETL}", "\r\n        <property name=\"jnlp.localetl\" value=\"True\"/>");
                reader = new StreamReader(path + "LocalEtlResources.txt");
                string localEtlResources = reader.ReadToEnd();
                reader.Close();
                jnlp = jnlp.Replace("{LOCALETLSOURCES}", localEtlResources);
            }
            else
            {
                jnlp = jnlp.Replace("{LOCALETL}", "");
                jnlp = jnlp.Replace("{LOCALETLSOURCES}", "");
            }

            if (u.isDiscoveryFreeTrial)
            {
                jnlp = jnlp.Replace("{DISCOVERYTRIAL}", "\r\n        <property name=\"jnlp.dtrial\" value=\"True\"/>");
            }
            else
            {
                jnlp = jnlp.Replace("{DISCOVERYTRIAL}", "");
            }
            return jnlp;
        }

        public static int getNumThresholdFailedRecords(HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                throw new BirstException("Unable to get Num Threshold Failed Records");
            }
            int numThresholdFailedRecords = 0;
            Int32.TryParse(maf.NumThresholdFailedRecordsBox.Text, out numThresholdFailedRecords);
            return numThresholdFailedRecords;
        }

        public static void setNumThresholdFailedRecords(int numThresholdFailedRecords, HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                throw new BirstException("Cannot set Num Threshold Failed Records");
            }

            int numFailedAllow = 0;
            Int32.TryParse(maf.NumThresholdFailedRecordsBox.Text, out numFailedAllow);
            if (numThresholdFailedRecords != numFailedAllow)
            {
                maf.NumThresholdFailedRecordsBox.Text = numThresholdFailedRecords.ToString();
                Util.markSessionDirty(session);
            }
            Util.saveDirty(session);
        }

        public static bool hasBingMaps(Guid userID)
        {
            // get a list of products (right now only bing maps matters XXX)
            List<Product> plist = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                plist = Database.getProducts(conn, mainSchema, userID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            foreach (Product p in plist)
            {
                if (p.Type == Product.TYPE_BING_MAPS)
                {
                    return true;
                }
            }
            return false;
        }

        public static int getMinYear(HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                throw new BirstException("Unable to get MinYear");
            }
            int minYear = -1;
            Int32.TryParse(maf.minYearText.Text, out minYear);
            return minYear;
        }

        public static int getMaxYear(HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                throw new BirstException("Unable to get MaxYear");
            }
            int maxYear = -1;
            Int32.TryParse(maf.maxYearText.Text, out maxYear);
            return maxYear;
        }

        public static void setMinMaxYear(int minYearNew, int maxYearNew, HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                throw new BirstException("Cannot set Min/Max Year");
            }

            // cleanup
            if (minYearNew <= 0)
                minYearNew = -1;
            if (maxYearNew <= 0)
                maxYearNew = -1;

            int minYear = -1;
            Int32.TryParse(maf.minYearText.Text, out minYear);
            if (minYear != minYearNew)
            {
                maf.minYearText.Text = minYearNew.ToString();
                Util.markSessionDirty(session);
            }
            int maxYear = -1;
            Int32.TryParse(maf.maxYearText.Text, out maxYear);
            if (maxYear != maxYearNew)
            {
                maf.maxYearText.Text = maxYearNew.ToString();
                Util.markSessionDirty(session);
            }
            Util.saveDirty(session);
        }

        public static void overrideProxyDBTypeSettings()
        {
            ConnectionStringSettings cs = ConfigUtils.getConnectionString();
            if (cs != null && cs.ConnectionString.Contains(("Driver={" + QueryConnection.oracledrivername + "}")))
            {
                PROXY_DB_TYPE = Database.ORACLE;
                PROXY_DB_DRIVER_NAME = Database.ORACLE_JDBC_DRIVER_NAME;
            }
        }

        public static void formatPage(Space sp, User u, Literal logo, Literal style, out string fgcolor)
        {
            fgcolor = "#464446";
            if (u.actDetails != null && u.actDetails.OverrideParameters != null && u.actDetails.OverrideParameters.ContainsKey(OverrideParameterNames.LOGO_PATH))
                logo.Text = u.actDetails.OverrideParameters[OverrideParameterNames.LOGO_PATH];
            else if (sp != null && sp.Settings != null && sp.Settings.LogoImage != null)
                logo.Text = "Logo.aspx";
            else if (u.LogoImageID != Guid.Empty)
                logo.Text = "Logo.aspx";
            if (sp != null && sp.Settings != null)
            {
                string bgstyle = null;
                if (sp.Settings.HeaderBackgroundColor >= 0)
                {
                    bgstyle = "background-color: #" + Util.getHexStringFromLongColor(sp.Settings.HeaderBackgroundColor) + "; ";
                }
                if (bgstyle != null)
                    style.Text = "<style type=\"text/css\">#headerstyle{" + bgstyle + "} #idHeaderContainer .x-panel-body{" + bgstyle + "} </style>";
                if (sp.Settings.HeaderForegroundColor >= 0)
                {
                    fgcolor = "#" + Util.getHexStringFromLongColor(sp.Settings.HeaderForegroundColor);
                }

            }
            else if (u.HeaderBackground >= 0) // XXX remove user headerbackground stuff
            {
                style.Text = "<style type=\"text/css\">#headerstyle{background-color: #" + Util.getHexStringFromLongColor(u.HeaderBackground) + ";} #idHeaderContainer .x-panel-body{background-color: #" + Util.getHexStringFromLongColor(u.HeaderBackground) + ";} </style>";
            }
        }

        public static string getHexStringFromLongColor(long color)
        {
            byte[] colors = new byte[3];
            colors[0] = (byte)(color >> 16);
            colors[1] = (byte)((color >> 8) & 255);
            colors[2] = (byte)(color & 255);
            return Util.getHexString(colors);
        }

        public static bool isEmbedded(HttpRequest request)
        {
            string embedParam = request.Params["embedded"];
            if (embedParam != null && embedParam == "true")
            {
                return true;
            }
            return false;
        }

        public static bool isNoHeader(HttpRequest request)
        {
            string noheaderParam = request.Params["noheader"];
            if (noheaderParam != null && noheaderParam == "true")
            {
                return true;
            }
            return false;
        }

        public static string getExtraParams(HttpServerUtility server, HttpRequest request, out string wmode)
        {
            string extraParams = "";
            wmode = null;
            System.Collections.Specialized.NameValueCollection requestParams = request.QueryString;
            if (requestParams != null && requestParams.Count > 0)
            {
                bool first = true;
                foreach (String key in requestParams.AllKeys)
                {
                    // should filter out 'bad' parameters (not entirely possible since we don't know the parameter names for the dashboard)
                    if (key == "birst.module" || key == "BirstSSOToken" || key == "birst.embedded")
                        continue;
                    string paramValue = requestParams[key];
                    if (paramValue != null)
                    {
                        if (key == "birst.wmode")
                        {
                            wmode = server.UrlEncode(paramValue);
                            continue;
                        }
                        paramValue = server.UrlEncode(paramValue);
                    }
                    if (first)
                    {
                        extraParams = key + "=" + paramValue;
                        first = false;
                    }
                    else
                    {
                        extraParams = extraParams + "," + key + "=" + paramValue;
                    }
                }
            }
            return extraParams;
        }

        public static string getEscapeStringForDB(QueryConnection conn, string prefix)
        {
            if (prefix == null || prefix.Length == 0 || prefix.IndexOf('\\') <= 0)
                return "";
            else if (conn.isDBTypeOracle() || conn.isDBTypeSQLServer()) //Need to provide escape char for ORACLE and MSSQL i.e. SELECT <col_list> from <table> where <col> like 'DW\_%' ESCAPE '\'
                return "ESCAPE \'" + "\\" + "\'";
            else
                return "";
        }

        public static List<PackageDetail> getImportList(MainAdminForm maf, HttpSessionState session, out Dictionary<Guid, ImportedSpace> importMap)
        {
            List<PackageDetail> importList = new List<PackageDetail>();
            importMap = session == null ? null : (Dictionary<Guid, ImportedSpace>)session["importmap"];
            if (maf.Imports != null)
            {
                /*
                 * Make sure that we cache packages so that we don't have to incur the load penalty each time
                 */
                if (importMap == null)
                {
                    importMap = new Dictionary<Guid, ImportedSpace>();
                    if (session != null)
                        session["importmap"] = importMap;
                }
                foreach (PackageImport pi in maf.Imports)
                {
                    Space psp = null;
                    if (!importMap.ContainsKey(pi.SpaceID))
                    {
                        psp = Util.getSpaceById(pi.SpaceID);
                        bool newrep = false;
                        MainAdminForm pmaf = null;
                        try
                        {
                            pmaf = Util.loadRepository(psp, out newrep);
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Warn("Exception in loading imported space repository: " + ex.Message);
                            continue;
                        }
                        ImportedSpace ispace = new ImportedSpace();
                        ispace.maf = pmaf;
                        ispace.sp = psp;
                        ispace.pi = pi;
                        importMap[pi.SpaceID] = ispace;
                    }
                    else
                    {
                        ImportedSpace ispace = new ImportedSpace();
                        ispace = importMap[pi.SpaceID];
                        psp = ispace.sp;
                    }
                    AllPackages pl = AllPackages.load(psp.Directory);
                    if (pl != null)
                    {
                        PackageDetail p = pl.getPackage(pi.PackageID);
                        if (p != null)
                        {
                            importList.Add(p);
                            // Make sure import entries are created for each item
                            List<string> itemNames = new List<string>();
                            if (p.StagingTables != null && p.StagingTables.Length > 0)
                            {
                                itemNames.AddRange(p.getStagingTableNames());
                            }
                            if (p.MeasureTables != null && p.MeasureTables.Length > 0)
                            {
                                itemNames.AddRange(p.getMeasureTableNames());
                            }
                            if (p.DimensionTables != null && p.DimensionTables.Length > 0)
                                itemNames.AddRange(p.getDimensionTableNames());
                            pi.setItems(itemNames);
                        }
                    }
                }
            }
            return importList;
        }


        /// <summary>
        /// This returns true in the scenario where if the swapping is being done on parent space and some users are already logged 
        /// into child spaces, the import map that is session cached needs to be refreshed--failing which application will throw false errors.
        /// </summary>
        /// <param name="maf"></param>
        /// <param name="session"></param>
        /// <param name="importList"></param>
        /// <param name="importMap"></param>
        /// <returns></returns>
        private static bool refreshImportMapAndList(MainAdminForm maf, HttpSessionState session,
            List<PackageDetail> importList, Dictionary<Guid, ImportedSpace> importMap)
        {
            if (session != null && importMap != null && importList != null && importList.Count > 0)
            {
                foreach (PackageDetail p in importList)
                {
                    if (!importMap.ContainsKey(p.SpaceID))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static List<ImportedRepositoryItem> getImportedTables(MainAdminForm maf, HttpSessionState session, string stagingTableName)
        {
            /*
             * Process imported staging tables
             */
            List<ImportedRepositoryItem> slist = new List<ImportedRepositoryItem>();
            Dictionary<Guid, ImportedSpace> importMap = null;
            List<PackageDetail> importList = Util.getImportList(maf, session, out importMap);
            if (refreshImportMapAndList(maf, session, importList, importMap))
            {
                // reload the importMap and importList
                if (session != null)
                    session.Remove("importMap");
                importList = Util.getImportList(maf, session, out importMap);
            }
            foreach (PackageDetail p in importList)
            {
                if (p.StagingTables != null)
                {
                    foreach (PStagingTable pStagingTable in p.StagingTables)
                    {
                        string stname = pStagingTable.Name;
                        if (stagingTableName != null && stagingTableName != stname)
                            continue;
                        ImportedSpace ispace = importMap[p.SpaceID];
                        StagingTable st = ispace.maf.stagingTableMod.findTable(stname);
                        if (st == null)
                        {
                            continue;
                        }
                        SourceFile sf = ispace.maf.sourceFileMod.getSourceFile(st.SourceFile);
                        ImportedRepositoryItem it = new ImportedRepositoryItem();
                        it.ispace = ispace;
                        it.st = st;
                        it.sf = sf;
                        it.hide = pStagingTable.Hide;
                        // if the staging table is given and it matches, then return that item only
                        if (stagingTableName != null && stagingTableName.Trim().Length > 0 && stagingTableName == stname)
                        {
                            slist.Clear();
                            slist.Add(it);
                            return slist;
                        }
                        slist.Add(it);
                    }
                }
                if (p.DimensionTables != null)
                {
                    for (int i = 0; i < p.DimensionTables.Length; i++)
                    {
                        string dtname = p.DimensionTables[i].Name;
                        ImportedSpace ispace = importMap[p.SpaceID];
                        int index = ispace.maf.findDimensionTableIndex(dtname);
                        if (index < 0)
                            continue;
                        DimensionTable dt = ispace.maf.dimensionTablesList[index];
                        ImportedRepositoryItem it = new ImportedRepositoryItem();
                        it.ispace = ispace;
                        it.dt = dt;
                        it.displayName = p.DimensionTables[i].DisplayName;
                        it.hide = p.DimensionTables[i].Hide;
                        slist.Add(it);
                    }
                }
                if (p.MeasureTables != null)
                {
                    for (int i = 0; i < p.MeasureTables.Length; i++)
                    {
                        string mtname = p.MeasureTables[i].Name;
                        ImportedSpace ispace = importMap[p.SpaceID];
                        int index = ispace.maf.findMeasureTableIndex(mtname);
                        if (index < 0)
                            continue;
                        MeasureTable mt = ispace.maf.measureTablesList[index];
                        ImportedRepositoryItem it = new ImportedRepositoryItem();
                        it.ispace = ispace;
                        it.mt = mt;
                        it.displayName = p.MeasureTables[i].DisplayName;
                        it.hide = p.MeasureTables[i].Hide;
                        slist.Add(it);
                    }
                }

                if (p.Variables != null && p.Variables.Length > 0)
                {
                    foreach (PVariable pVariable in p.Variables)
                    {
                        string variableName = pVariable.Name;
                        ImportedSpace ispace = importMap[p.SpaceID];
                        MainAdminForm importedMAF = ispace.maf;
                        List<Variable> variables = importedMAF.getVariables();
                        if (variables != null && variables.Count > 0)
                        {
                            foreach (Variable sv in variables)
                            {
                                if (variableName == sv.Name)
                                {
                                    ImportedRepositoryItem it = new ImportedRepositoryItem();
                                    it.ispace = ispace;
                                    it.v = sv;
                                    it.hide = pVariable.Hide;
                                    slist.Add(it);
                                    break;
                                }
                            }
                        }
                    }
                }

                if (p.Aggregates != null && p.Aggregates.Length > 0)
                {
                    foreach (PAggregate pAggregate in p.Aggregates)
                    {
                        string aggregateName = pAggregate.Name;
                        ImportedSpace ispace = importMap[p.SpaceID];
                        MainAdminForm importedMAF = ispace.maf;
                        if (importedMAF.aggModule != null)
                        {
                            Aggregate agg = importedMAF.aggModule.findAggregate(aggregateName);
                            if (agg != null)
                            {
                                ImportedRepositoryItem it = new ImportedRepositoryItem();
                                it.ispace = ispace;
                                it.aggregate = agg;
                                it.hide = pAggregate.Hide;
                                slist.Add(it);
                            }
                        }
                    }
                }
            }
            return slist;
        }

        public static HashSet<string> getRealTimeConnectionsFromMAF(MainAdminForm maf)
        {
            HashSet<string> realTimeConnectionsList = new HashSet<string>();
            List<DimensionTable> liveAccessNonVizDimTables = new List<DimensionTable>();
            List<MeasureTable> liveAccessNonVizMeasureTables = new List<MeasureTable>();
            // get real time dimension tables (set using Manage Local Sources)
            foreach (DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Realtime)
                    realTimeConnectionsList.Add(dc.Name);
            }
            if (maf.EnableLiveAccessToDefaultConnection)
                realTimeConnectionsList.Add(Database.DEFAULT_CONNECTION);

            return realTimeConnectionsList;
        }


        private static string getGrainString(string[][] grains)
        {
            string grainstr = "";
            foreach (string[] grain in grains)
            {
                if (grainstr.Length > 0)
                    grainstr += ",";
                grainstr += grain[0] + "." + grain[1];
            }
            return grainstr;
        }

        internal static bool isInternalCreatedVariable(Variable variable)
        {
            if (variable != null && !variable.BirstCanModify)
                return true;

            return false;
        }

        internal static string getStagingTableDisplayName(StagingTable st)
        {
            return st != null && st.Name != null ? st.Name.Substring(3) : null;
        }

        public static string[] getImportedPackageSpacesInfoArray(Space sp, MainAdminForm maf)
        {
            if (maf != null)
            {
                List<PackageSpaceProperties> spacesList = getImportedPackageSpaces(sp, maf);
                if (spacesList != null && spacesList.Count > 0)
                {
                    return getSpacesInfoArray(spacesList);
                }
            }
            return null;
        }

        public static List<PackageSpaceProperties> getImportedPackageSpaces(Space sp, MainAdminForm maf)
        {
            List<PackageSpaceProperties> response = new List<PackageSpaceProperties>();
            if (maf != null && maf.Imports != null && maf.Imports.Length > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    List<string> availablePackageIds = new List<string>();
                    List<PackageUsage> packageUsageList = Database.getPackageUsageByChildSpace(conn, mainSchema, sp.ID);
                    if (packageUsageList != null && packageUsageList.Count > 0)
                    {
                        foreach (PackageUsage packageUsage in packageUsageList)
                        {
                            if (packageUsage.Available)
                            {
                                string allowedPackageID = packageUsage.PackageID;
                                if (!availablePackageIds.Contains(allowedPackageID))
                                {
                                    availablePackageIds.Add(allowedPackageID);
                                }
                            }
                        }
                    }

                    Dictionary<string, Space> spaceIDToSpaceMap = new Dictionary<string, Space>();
                    foreach (PackageImport packageImport in maf.Imports)
                    {
                        if (packageImport.PackageID == null || packageImport.PackageID == Guid.Empty)
                        {
                            continue;
                        }

                        if (availablePackageIds.Contains(packageImport.PackageID.ToString()))
                        {
                            Guid parentSpaceID = packageImport.SpaceID;
                            Space parentSpace = null;
                            if (!spaceIDToSpaceMap.ContainsKey(parentSpaceID.ToString()))
                            {
                                parentSpace = Database.getSpace(conn, Util.getMainSchema(), packageImport.SpaceID);
                                spaceIDToSpaceMap.Add(parentSpaceID.ToString(), parentSpace);
                            }

                            parentSpace = spaceIDToSpaceMap[parentSpaceID.ToString()];
                            if (parentSpace != null)
                            {
                                PackageSpaceProperties packageSpaceProperties = new PackageSpaceProperties();
                                packageSpaceProperties.PackageID = packageImport.PackageID;
                                packageSpaceProperties.PackageName = packageImport.PackageName;
                                packageSpaceProperties.SpaceDirectory = parentSpace.Directory;
                                packageSpaceProperties.SpaceID = parentSpace.ID;
                                packageSpaceProperties.SpaceName = parentSpace.Name;
                                response.Add(packageSpaceProperties);
                            }
                        }
                    }
                }
                finally
                {
                    try
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    catch (Exception) { }
                }

            }
            return response;
        }

        /// <summary>
        /// only needed in case of a specific use case where native space does not have any sources of its own
        /// In this case, need to make sure that time dimension gets created
        /// </summary>
        /// <param name="session"></param>
        /// <param name="sp"></param>
        /// <param name="maf"></param>
        internal static void setRebuildFlagOnPackageImportIfNoSourcesPresent(HttpSessionState session, Space sp, MainAdminForm maf)
        {
            if (session != null && sp != null &&
                maf != null && maf.Imports != null && maf.Imports.Length > 0)
            {
                List<StagingTable> allStagingTables = maf.stagingTableMod.getAllStagingTables();
                if (allStagingTables == null || allStagingTables.Count == 0)
                {
                    session["AdminDirty"] = true;
                    session["RequiresRebuild"] = true;
                }
            }
        }

        /// <summary>
        /// Set dashboard session permissions flag to true
        /// If the designer permission session flag is not set, then update it. Otherwise no need to touch it
        /// </summary>
        /// <param name="session"></param>
        /// <param name="user"></param>
        internal static void setDesignerAndDashboardViewablePermissions(HttpSessionState session)
        {
            if (session != null)
            {
                User user = Util.getSessionUser(session);
                // if it is already set, no need to touch it
                if (user != null && !Util.isSessionAttributeTrue(session, "AdhocPermitted"))
                {
                    // passing admin flag as false. No need to get admin module access
                    DefaultUserPage.setSpace(session, false, user, new SpaceDetails());
                }
            }
        }

        /// <summary>
        /// Reset dashboard session permissions
        /// </summary>
        /// <param name="session"></param>
        /// <param name="user"></param>
        internal static void resetDesignerAndDashboardViewablePermissions(HttpSessionState session)
        {
            if (session != null)
            {
                session.Remove("AdhocPermitted");
                session.Remove("DashboardPermitted");
            }
        }

        internal static bool isOwnerGroup(SpaceGroup toAddUserGroup)
        {
            if (toAddUserGroup != null && toAddUserGroup.InternalGroup && toAddUserGroup.Name == Util.OWNER_GROUP_NAME)
            {
                return true;
            }
            return false;
        }

        public static void validateSpaceId(HttpRequest Request, HttpResponse Response, HttpSessionState Session)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string spaceId = Request["birst.spaceId"];
            if (spaceId == null || spaceId.Length == 0)
                return;
            if (sp.ID.ToString() != spaceId)
            {
                Response.Write("You already have a logged in session associated with this browser on a different space.  Please logout in that browser session and the refresh this page.");
                Response.End();
            }
        }

        public static string getSerializedString(string[] arrayofstring)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < arrayofstring.Length; i++)
            {
                if (i > 0)
                    sb.Append("|1");
                sb.Append(arrayofstring[i]);
            }
            return sb.ToString();
        }

        public static string getSerializedString(bool[] arrayofbool)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < arrayofbool.Length; i++)
            {
                if (i > 0)
                    sb.Append("|1");
                sb.Append(arrayofbool[i]);
            }
            return sb.ToString();
        }

        public static string getSerializedString(string[][] arrayofarrayofstring)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string[] arrayofstring in arrayofarrayofstring)
            {
                if (sb.Length > 0)
                    sb.Append("|2");
                sb.Append(getSerializedString(arrayofstring));
            }
            return sb.ToString();
        }

        /**
         * Birst version of File.Copy
         * 
         * why? because we keep getting 'System.IO.IOException: Not enough storage is available to process this command.' from System.IO.File.Copy
         * 
         * from the internet:
         * - http://stackoverflow.com/questions/2201459/cant-copy-big-db-files-using-copy-robocopy-or-even-eseutil
         * - https://groups.google.com/forum/?fromgroups=#!topic/hfm-net/yvdrNTpkOMQ
         * 
         * also, it's safe, copies to a temp file, and then does move/delete (same as used for repository save in MainAdminForm.cs)
         */

        static ulong repositoryCounter = 0; // add some uniqueness to the temp file names, for safety

        public static void FileCopy(string from, string to)
        {
            Util.FileCopy(from, to, false);
        }

        public static void FileCopy(string from, string to, bool overwrite)
        {
            ulong value = ++repositoryCounter; // should be atomic
            string tname = to + ".new" + value;

            byte[] buffer = new byte[8 * 1024];

            if (!overwrite && File.Exists(to))
            {
                throw new IOException("File exists and overwrite flag was false");
            }
            if (overwrite)
                File.Delete(tname);

            Stream input = null;
            Stream output = null;
            try
            {
                input = File.OpenRead(from);
                output = File.Create(tname);
                int len;
                while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, len);
                }
            }
            finally
            {
                if (output != null)
                    output.Close();
                if (input != null)
                    input.Close();
            }

            // save the old version, move the new version to the proper location, delete the old version
            FileInfo fi = new FileInfo(to);
            string tname2 = to + ".old" + value;
            if (fi.Exists)
            {
                File.Delete(tname2);   // delete old (paranoid, should never be there)
                File.Move(to, tname2); // move current to old
            }
            File.Move(tname, to);  // move new to current
            File.Delete(tname2);                // delete old
            return;
        }

        private static int MaxSaveFiles = 5;        // only keep the last 5 save versions of the repository (for delete last)
        private static int MaxAge = 3 * 30;         // only keep the last 3 months worth of backup files
        private static int MaxLoadLogs = 14;        // only keep the last 14 load logs
        private static int MaxExtractFiles = 14;    // only keep the last 14 extract files (of each type)

        /**
         * clean up all old and/or unused files
         */
        public static void cleanupOldFiles(Space sp)
        {
            // sanity check since this could be bad if true
            if (sp.Directory == null || sp.Directory.Length == 0)
            {
                Global.systemLog.Warn("called cleanupOldFiles with a null space directory");
                return;
            }
            try
            {
                // clean up old save files
                string[] files = { "repository.xml", "repository_dev.xml" };
                int startLoadCount = 0;
                if (sp.LoadNumber >= SalesforceLoader.BASE_SFORCE_LOAD_NUMBER)
                {
                    startLoadCount = SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                }
                for (int i = startLoadCount; i < sp.LoadNumber - MaxSaveFiles; i++)
                {
                    foreach (string file in files)
                    {
                        string fileName = Path.Combine(sp.Directory, file + "." + i + ".save");
                        FileInfo fi = new FileInfo(fileName);
                        if (fi.Exists)
                        {
                            try
                            {
                                Global.systemLog.Debug("Cleaning up (only save a limited number): " + fi.Name);
                                fi.Delete();
                            }
                            catch (IOException)
                            { }
                        }
                    }
                }
                // clean up save files out of sync with load number
                for (int i = sp.LoadNumber + 1; i < sp.LoadNumber + 50; i++)
                {
                    foreach (string file in files)
                    {
                        string fileName = Path.Combine(sp.Directory, file + "." + i + ".save");
                        FileInfo fi = new FileInfo(fileName);
                        if (fi.Exists)
                        {
                            try
                            {
                                Global.systemLog.Debug("Cleaning up (only save a limited number): " + fi.Name);
                                fi.Delete();
                            }
                            catch (IOException)
                            { }
                        }
                    }
                }

                string backupDir = Path.Combine(sp.Directory, BackupDir);

                // delete LogicalExpressions*.xml (.gz), Variables*.xml (.gz) files, they are no longer used
                string[] patterns = { "LogicalExpressions*", "Variables*" };
                foreach (string pattern in patterns)
                {
                    string[] unusedFiles = Directory.GetFiles(sp.Directory, pattern, SearchOption.TopDirectoryOnly);
                    foreach (string file in unusedFiles)
                    {
                        try
                        {
                            FileInfo fi = new FileInfo(file);
                            Global.systemLog.Debug("Cleaning up (no longer used): " + fi.Name);
                            fi.Delete();
                        }
                        catch (IOException)
                        { }
                    }

                    if (Directory.Exists(backupDir))
                    {
                        string[] unusedBackupFiles = Directory.GetFiles(backupDir, pattern, SearchOption.TopDirectoryOnly);
                        foreach (string file in unusedBackupFiles)
                        {
                            try
                            {
                                FileInfo fi = new FileInfo(file);
                                Global.systemLog.Debug("Cleaning up (no longer used): " + fi.Name);
                                fi.Delete();
                            }
                            catch (IOException)
                            { }
                        }
                    }
                }

                pruneBackupDirectories(sp);
                // delete any backup files at the top level (stored them there for a short period of time)
                string[] oldBackupFiles = Directory.GetFiles(sp.Directory, "*.gz", SearchOption.TopDirectoryOnly);
                foreach (string file in oldBackupFiles)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        Global.systemLog.Debug("Cleaning up (top level backup): " + fi.Name);
                        fi.Delete();
                    }
                    catch (IOException)
                    { }
                }

                // delete old jasper files (we recompile all of the time)
                string[] oldJasperPatterns = { "*.jasper", "*.smijasper", "*.jasper440" };
                string catalogDir = Path.Combine(sp.Directory, "catalog");
                if (Directory.Exists(catalogDir))
                {
                    foreach (string oldJasperPattern in oldJasperPatterns)
                    {
                        string[] oldJasperFiles = Directory.GetFiles(catalogDir, oldJasperPattern, SearchOption.AllDirectories);
                        foreach (string file in oldJasperFiles)
                        {
                            try
                            {
                                FileInfo fi = new FileInfo(file);
                                Global.systemLog.Debug("Cleaning up (no longer used): " + fi.Name);
                                fi.Delete();
                            }
                            catch (IOException)
                            { }
                        }
                    }
                }

                // delete old load logs
                Util.deleteAllButN(sp, "logs", "*.log", MaxLoadLogs);

                string[] extractionPatterns = { "message.*.txt", "success-extract-*.txt", "failed-extract-*.txt", "killed-extract-*.txt", "procesLog.*.log" };
                foreach (string extractionPattern in extractionPatterns)
                {
                    Util.deleteAllButN(sp, "slogs", extractionPattern, MaxExtractFiles);
                }

                // delete the old cache files
                try
                {
                    DirectoryInfo dirI = new DirectoryInfo(Path.Combine(sp.Directory, "cache"));
                    Util.deleteDir(dirI);
                }
                catch (IOException)
                { }
            }
            catch (Exception ex)
            {
                // this can fail, so log the errors and and continue on
                Global.systemLog.Debug(ex);
            }
        }

        private static void pruneBackupDirectories(Space sp)
        {
            if (sp == null || sp.Directory == null)
                return;

            string backupDir = Path.Combine(sp.Directory, BackupDir);
            if (Directory.Exists(backupDir))
            {
                foreach (string bkUpFile in backupFileNames)
                {
                    keepNSetsOfBackUpsPerDay(backupDir, bkUpFile + "*.gz", MaxAge, 1);
                }

                foreach (KeyValuePair<string, string> subDirKvPair in backupSubDirectories)
                {
                    string backupSubDirName = Path.Combine(backupDir, subDirKvPair.Key);
                    string backupFilePattern = subDirKvPair.Value + "*.gz";
                    keepNSetsOfBackUpsPerDay(backupSubDirName, backupFilePattern, MaxAge, 1);
                }
            }
        }

        /// <summary>
        /// Goes through the directory and processes the backup files per the following rules
        /// Keep all the copies of the current day
        /// Keep the latest copy of all the previous days up to the "ageToDelete" parameter
        /// Delete all the copies after ageToDelete time stamp
        /// Time stamp that is checked is again CreationTime of the file
        /// </summary>
        /// <param name="dir"></param>        
        /// <param name="deleteAllAge"></param>
        /// <param name="backupsPerDay"></param>
        private static void keepNSetsOfBackUpsPerDay(string dir, string searchPattern, int deleteAllAge, int backupsPerDay)
        {
            if (!Directory.Exists(dir))
                return;
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            // directory could contain files of various names 
            // e.g. custom subject area contains 3 types of various files. This datastructure contains
            // the dictionary of each file, the value of each file is itself a dictionary of date to list of all backup files 
            Dictionary<string, Dictionary<string, List<FileInfo>>> filesPerDayList = new Dictionary<string, Dictionary<string, List<FileInfo>>>();
            List<FileInfo> toDeleteFiles = new List<FileInfo>();

            IEnumerable<FileInfo> fileInfoEnumerable = dirInfo.EnumerateFiles(searchPattern, SearchOption.TopDirectoryOnly);
            if (fileInfoEnumerable != null)
            {
                foreach (FileInfo fi in fileInfoEnumerable)
                {
                    if (isAnOlderFile(fi, deleteAllAge))
                    {
                        toDeleteFiles.Add(fi);
                    }
                    else
                    {
                        string dateString = fi.CreationTimeUtc.ToString("d", CultureInfo.InvariantCulture);
                        int index = fi.Name.IndexOf(".");
                        string fileNamePrefix = fi.Name.Substring(0, index);

                        if (!filesPerDayList.ContainsKey(fileNamePrefix))
                        {
                            filesPerDayList.Add(fileNamePrefix, new Dictionary<string, List<FileInfo>>());
                        }

                        if (!filesPerDayList[fileNamePrefix].ContainsKey(dateString))
                        {
                            filesPerDayList[fileNamePrefix].Add(dateString, new List<FileInfo>());
                        }

                        filesPerDayList[fileNamePrefix][dateString].Add(fi);
                    }
                }


                // sort the files less than "Age"
                if (filesPerDayList.Count > 0)
                {
                    string todayString = DateTime.UtcNow.ToString("d", CultureInfo.InvariantCulture);
                    foreach (KeyValuePair<string, Dictionary<string, List<FileInfo>>> kv in filesPerDayList)
                    {
                        Dictionary<string, FileInfo[]> sortedFiles = new Dictionary<string, FileInfo[]>();

                        foreach (KeyValuePair<string, List<FileInfo>> kvPair in kv.Value)
                        {

                            string fileDateString = kvPair.Key;
                            // include all files from today
                            if (fileDateString == todayString)
                                continue;

                            List<FileInfo> filesList = kvPair.Value;
                            if (filesList != null && filesList.Count > 0)
                            {
                                if (filesList.Count == 1)
                                    continue;
                                FileInfo[] filesToSort = kvPair.Value.ToArray();
                                IComparer comparer = new FileCreationTimeUTCComparer();
                                Array.Sort(filesToSort, comparer);
                                Array.Reverse(filesToSort);

                                sortedFiles.Add(fileDateString, filesToSort);
                            }
                        }


                        if (sortedFiles.Count > 0)
                        {
                            foreach (KeyValuePair<string, FileInfo[]> sortedFileKvPair in sortedFiles)
                            {
                                FileInfo[] fileInfos = sortedFileKvPair.Value;
                                if (fileInfos != null && fileInfos.Length > 1)
                                {
                                    // delete all expcet backupsPerDay number of files
                                    for (int i = backupsPerDay; i < fileInfos.Length; i++)
                                    {
                                        FileInfo oFi = null;
                                        try
                                        {
                                            oFi = fileInfos[i];
                                            Global.systemLog.Debug("Cleaning up (deleting older backup files): " + oFi.Name);
                                            oFi.Delete();
                                        }
                                        catch (IOException)
                                        {
                                            // continue to the next file
                                            Global.systemLog.Warn("Problem deleting an older file " + oFi.Name + " . Continuing to the next one");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // after the lists process them
                // delete the older ones
                if (toDeleteFiles != null && toDeleteFiles.Count > 0)
                {
                    foreach (FileInfo dFi in toDeleteFiles)
                    {
                        try
                        {
                            Global.systemLog.Debug("Cleaning up (backup too old): " + dFi.Name);
                            dFi.Delete();
                        }
                        catch (IOException)
                        {
                            // continue to the next file
                            Global.systemLog.Warn("Problem deleting the file " + dFi.Name + " . Continuing to the next one");
                        }
                    }
                }
            }

        }

        private static bool isAnOlderFile(FileInfo fileInfo, int age)
        {
            TimeSpan delta = DateTime.UtcNow - fileInfo.CreationTimeUtc;
            return delta.Days > age;
        }

        private static void deleteAllButN(Space sp, string dir, string pattern, int max)
        {
            // delete old load logs
            string logDir = Path.Combine(sp.Directory, dir);
            if (!File.Exists(logDir))
                return;
            string[] logFiles = Directory.GetFiles(logDir, pattern, SearchOption.TopDirectoryOnly);
            if (logFiles.Length > max)  // don't bother if we don't have more than the max to save
            {
                IComparer comp = new FileComparer();
                Array.Sort(logFiles, comp);
                for (int i = 0; i < logFiles.Length - MaxLoadLogs; i++)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(logFiles[i]);
                        Global.systemLog.Debug("Cleaning up (old file): " + fi.Name);
                        fi.Delete();
                    }
                    catch (IOException)
                    { }
                }
            }
        }

        // validate a string value
        public static void validateString(string label, string value, string regex, int length, bool allowNull)
        {
            if (!allowNull && value == null)
                throw new BirstException(label + " can not be empty");
            if (value == null)
                return;
            if (value.Length > length)
                throw new BirstException(label + " exceeds the maximum length");
            if (regex != null)
            {
                Regex invalidCharacters = new Regex(regex);
                if (invalidCharacters.IsMatch(value))
                    throw new BirstException(label + " contains invalid characters");
            }
        }

        //validate password against Postgre ODBC driver version
        public static void validateAndGetODBCDBPwd(string dbPwd)
        {
            string[] ver = getPostGresDriverVersionSubset();
            if ((int.Parse(ver[0]) == 9 && int.Parse(ver[1]) > 1 && (dbPwd.Contains("{") || dbPwd.Contains("}"))) ||
                (int.Parse(ver[0]) >= 10 && (dbPwd.Contains("{") || dbPwd.Contains("}")))) 
            {
                throw new BirstException("Current PostGreDriverVersion doesn't support special characters '{' or '}' in password");
            }
            else if ((int.Parse(ver[0]) == 9 && int.Parse(ver[1]) <= 1 && (dbPwd.Contains("{") || dbPwd.Contains("}") || dbPwd.Contains("%") || dbPwd.Contains(";"))) ||
                (int.Parse(ver[0]) < 9 && (dbPwd.Contains("{") || dbPwd.Contains("}") || dbPwd.Contains("%") || dbPwd.Contains(";"))))  //for versions older than 09.02.xxx
            {
                throw new BirstException("Current PostGreDriverVersion doesn't support special characters '{','}','%' or ';' in password");
            }      
        }

        /*        
         * Older than (09.02.xxx) Postgres ODBC driver doesn't support special characters like ({,},%,;) in password.
         * while starting from driver version(09.02.xxx) supports (%,;) by passing password surrounded with curly brackets. But still it doesn't 
         * support ({,})
         * 
         * This method returns subset of driver (Ex, if driver version 09.01.0100 then it returns 01)
         * */          
        public static string[] getPostGresDriverVersionSubset()
        {
            String driverVersion;
            if (postgreSQLDriverVersion != null && postgreSQLDriverVersion.Contains(".") && postgreSQLDriverVersion.Split('.').Length > 1)
            {
                driverVersion = postgreSQLDriverVersion;
            }
            else
            {
                driverVersion = defaultPostgresSQLDriverVersion;
            }
            string[] ver = driverVersion.Split('.');

            return ver;
        }

        // validate a schema name
        public static void validateSchemaName(string connectstring, string databasetype, string schemaName)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                //schema must exist at target db
                if (!Database.isDBSchemaExist(connectstring, databasetype, schemaName))
                {
                    throw new SchemaException(BirstException.ERROR_DB_SCHEMA_NOT_FOUND, "Schema does not exist");
                }
                //verify schema is in use from admin db schema table
                if (Database.isDBSchemaInUse(conn, mainSchema, schemaName))
                {
                    throw new SchemaException(BirstException.ERROR_DB_SCHEMA_IN_USE, "Schema is in use by other space");
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static ServerConfig getServerConfig()
        {
            ServerConfig serverConfig = new ServerConfig();
            serverConfig.AllowOverrideSpaceSchema = allowOverrideSchema != null ? bool.Parse(allowOverrideSchema) : false;
            serverConfig.IsApplianceMode = isApplianceMode != null ? bool.Parse(isApplianceMode) : false;
            return serverConfig;
        }

        internal static string parseClusterRegion(string connectString)
        {
            Match match = Regex.Match(connectString, @"us-(\w+)-");
            if (match.Success)
            {
                return match.Groups[1].Value;
            }
            return null;
        }

        internal static string getS3BucketName(SpaceConfig sc)
        {
            if (sc != null && sc.regionId > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    AWSRegion aRegion = Database.getRedshiftRegionDetails(conn, mainSchema, sc.regionId);
                    if (aRegion != null)
                    {
                        return aRegion.TimeBucket;
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return s3BucketName;
        }

        internal static string getTimeDimensionFolder(SpaceConfig sc)
        {           
            return timeDimensionFolder;
        }


        internal static string getAWSAccessKey()
        {
            return awsAccessKeyId;
        }

        internal static string getAWSSecretKey()
        {
            return awsSecretKey;
        }

        internal static string getCreateTimeCommand(Space sp)
        {
            if (Util.isSpaceRedshift(sp))
            {
                return redShiftTimeCommand;
            }
            if (Util.isSpaceHANA(sp))
            {
                return sapHanaCreateTimeCommand;
            }
            return ProcessLoad.createtimecmd;
        }

        internal static string getUploadS3BucketName(string connectString)
        {
            string regionName = parseClusterRegion(connectString);
            if (regionName != null && regionName.Length > 0)
            {
                return uploadS3BucketName.Replace("V{region}", regionName);
            }
            return uploadS3BucketName;
        }

        internal static string getUploadAWSAccessKey()
        {
            return uploadAwsAccessKeyId;
        }

        internal static string getUploadAWSSecretKey()
        {
            return uploadAwsSecretKey;
        }

        internal static bool isSpaceRedshift(Space sp)
        {
            return sp.DatabaseType == Database.REDSHIFT;
        }

        internal static bool isSpaceHANA(Space sp)
        {
            return sp.DatabaseType == Database.HANA;
        }

        internal static string getPostgresDriverName()
        {
            return postgresDriverName;
        }

        internal static bool isBirstExpress(string userName)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                User user = Database.getUser(conn, mainSchema, userName);
                return user != null && user.isFreeTrialUser;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static bool checkFileExistsWithException(string fileName)
        {
            if (File.Exists(fileName))
            {
                return true;
            }
            else
            {
                StreamReader reader = null;
                try
                {
                    //Try to open stream reader. File not found exception is expected. Do not consume any other error. i.e. UnauthorizedAccessException
                    reader = new StreamReader(fileName);
                }
                catch (FileNotFoundException)
                {
                    return false;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return false;
        }

        public static Dictionary<String, String> getLocalizedOverridenParams(string Locale, User u)
        {
            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();
            QueryConnection conn = null;
            try
            {
                Guid id;
                if (u == null)
                {
                    Global.systemLog.Warn("Session User unavailable");
                    id = Guid.Empty;
                }
                else
                {
                    id = u.AdminAccountId;
                }
                try
                {
                    conn = ConnectionPool.getConnection();
                    overrideParameters = Database.getOverrideParametersWithLocale(conn, WebServiceSessionHelper.mainSchema, id, Locale);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting overriden parameters " + ex);
            }
            finally
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }
            return overrideParameters;
        }

        public static void checkIfUserInactiveAndDisable(QueryConnection conn, User u, DateTime lastLoginDate)
        {
            // check if we need to disable the user since he/she has not logged in for a while
            Dictionary<string, string> overrides = Util.getLocalizedOverridenParams(null, u);
            if (overrides != null && overrides.Count > 0 && overrides.ContainsKey(OverrideParameterNames.USER_DISABLE_INACTIVITY_TIMEOUT))
            {
                int days = -1;
                string temp2 = overrides[OverrideParameterNames.USER_DISABLE_INACTIVITY_TIMEOUT];
                if (temp2 != "")
                    Int32.TryParse(temp2, out days);
                TimeSpan ts = DateTime.Now - lastLoginDate;
                if (days > 0 && ts.Days > days)
                {
                    Database.enableUser(conn, WebServiceSessionHelper.mainSchema, u, false);
                    u.Disabled = true;
                    Global.systemLog.Info("Automatically disabling user (" + u.Username + ") due to inactivity (> " + days + " days), last login: " + lastLoginDate + " (" + ts.Days + " days ago)");
                }
            }
        }

        public static Regex supportedNameExpression = new Regex("[\\w\\s:\\$#%\\-&!^@,;><\\/" + generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]*");

		public static string generateRangeForUnicodeCharacters(int var1, int var2)
		{
   			return Convert.ToChar(var1) + "-" + Convert.ToChar(var2);
		}


        public static bool isNameValid(string name)
        {
            Match match = supportedNameExpression.Match(name);
            return match.Length == name.Length;
        }

        /* @length required length for randome password for oracle
         * Using RNGCryptoServiceProvider to generate random passwords
         * http://stackoverflow.com/a/19068116.
         */
        public static string GetRandomPasswordForOracle(int length)
        {
            const string alphaCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" ;                
            const string numericAndSpecialChars = "0123456789_"; //Ignoring @ and # in passowrds as oracle does not recommend it
            return GetRandomString(1, alphaCharacters) + GetRandomString(length, alphaCharacters+ numericAndSpecialChars); //First char has to be an alphabet for Oracle
        }

        public static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

		
    }

    public class FileComparer : IComparer
    {
        int IComparer.Compare(object x, object y)
        {
            FileInfo fix = new FileInfo(x.ToString());
            FileInfo fiy = new FileInfo(y.ToString());
            return DateTime.Compare(fix.LastWriteTime, fiy.LastWriteTime);
        }
    }


    public class FileCreationTimeUTCComparer : IComparer
    {
        int IComparer.Compare(object x, object y)
        {
            FileInfo fix = new FileInfo(x.ToString());
            FileInfo fiy = new FileInfo(y.ToString());
            return DateTime.Compare(fix.CreationTimeUtc, fiy.CreationTimeUtc);
        }
    }

}
