﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    
    public class UserSpaces
    {
        public ErrorOutput Error;
        public string UserName;
        public string FirstName;
        public string SizeOfSpaceOwned;
        public string LastLoginDate;
        public bool EnableCopy;
        public bool ShowFreeTrialPage;
        public bool canCreateNewSpace;
        public bool showTemplate;
        public SpaceSummary[] SpacesList;
        public int Selected;                
        public InviteResponse[] Invites;
        public bool EnableUserManagement;
        public MessageResponse[] Messages;
        public int UserMode;
        public ServerConfig serverConfig;
        public bool ExternalDBEnabled;
        public bool ShowDiscoveryFreeTrialPage;
        public bool OptOutVisualizerBanner;
    }

    public class SpaceSummary
    {
        public string SpaceName;
        public string SpaceID;
        public string SpacePath;
        public string OwnerUsername;
        public bool Owner;
        public int AvailabilityCode;
        public bool TrialVisualizer;
    }
    
    public class SpaceDetails
    {
        public ErrorOutput Error;  
        public string SpaceName;
        public string SpaceID;
        public string UserEmail;        
        public bool Owner;
        public string OwnerUsername;
        public bool Administrator;
        public bool Adhoc;
        public bool Dashboards;
        public bool QuickDashboard;        
        public bool EnableAdhoc;
        public bool EnableDashboards;
        public bool EnableQuickDashBoards;
        public bool EnableVisualizer;
        public AdminModulesAccess AdminAccess;
        public string LastUsed;
        public string LastUpload;
        public string SpaceSize;
        public string SpaceComments;
        public bool SuperUser;
        public int AvailabilityCode;
        public int SpaceMode;
        public bool IndependentMode;
        public bool FreeTrialUser;
        public string UserID;
        public string AppVersion;
        public string HostName;
        public bool DenyAddSpace;
    }

    public class SpaceStatistics
    {
        public string SpaceSize;
        public int UserCount;
    }
    
    public class AdminModulesAccess
    {
        public bool ManageCatalog;
        public bool Publish;
        public bool UseTemplate;
        public bool ServicesAdmin;
        public bool CopySpace;
        public bool CustomFormula;
        public bool ShareSpace;
        public bool QuartzSchedulerEnabled;
        public bool showOldScheduler;
        public bool BirstConnect;
        public bool LocalConnect;
    }

    public class Invite
    {
        public string SpaceID;
        public string Email;
        public DateTime InviteDate;
        public string SpaceName;
        public string InviteUserEmail;
    }

    public class InviteResponse
    {
        public string SpaceID;
        public string Email;
        public string InviteDate;
        public string SpaceName;
        public string InviteUserEmail;
    }

    public class MessageResponse
    {
        public string Date;
        public string Text;
    }

    public class SpaceSummaryList : GenericResponse
    {
        public SpaceSummary[] SummaryList;
    }
}
