﻿using System;
using System.Web.SessionState;
using System.Data;
using System.Collections.Generic;
using Performance_Optimizer_Administration;
using Acorn.Utils;

namespace Acorn
{
    public class PublishLog
    {
        public static DataTable getPublishSummaryData(HttpSessionState session, Space sp, int loadnum, string loadGroup)
        {
            return getPublishSummaryData(session, sp, loadnum, loadGroup, null, true);
        }
        
        public static DataTable getPublishSummaryData(HttpSessionState session, Space sp, int loadnum, string loadGroup, string processingGroup, bool ignoreProcessingGroup)
        {
            if (session != null && sp == null)
                sp = (Space)session["space"];
            MainAdminForm maf = null;
            if (session != null)
                maf = (MainAdminForm)session["MAF"];
            Acorn.Status.StatusResult sr = Status.getLoadStatus(session, sp, loadnum, loadGroup);
            DataTable dt = new DataTable();
            dt.Columns.Add("DataSource");
            dt.Columns.Add("Processed", typeof(long));
            dt.Columns.Add("Errors", typeof(long));
            dt.Columns.Add("Warnings", typeof(long));
            dt.Columns.Add("Duration", typeof(long));
           
           
            if (sr.substeps != null)
            {
                if (Status.EMPTY_PROCESSING_GROUP.Equals(processingGroup))
                    processingGroup = null;
                HashSet<string> liveAccessLoadGroups = new HashSet<string>();
                if (loadGroup == Util.LOAD_GROUP_NAME)
                {
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    foreach (string lg in leConfigs.Keys)
                    {
                        liveAccessLoadGroups.Add(lg);
                    }
                }
                string prefix = loadGroup + ": ";
                foreach (Status.StatusStep ss in sr.substeps)
                {
                    if (!ignoreProcessingGroup && (ss.processingGroup != processingGroup))
                        continue;
                    if (ss.step == Status.StatusCode.LoadStaging && ss.result == Acorn.Status.COMPLETE)
                    {
                        if (ss.substep.StartsWith(prefix) || BirstConnectUtil.startsWithLiveAccessLoadGroup(ss.substep + ":", liveAccessLoadGroups))
                        {
                            DataRow dr = dt.NewRow();
                            string physicalName= ss.substep.Substring(ss.substep.IndexOf(":") + 2);
                            String tname = physicalName.Substring(3);
                            if (maf != null)
                            {
                                StagingTable st = ViewProcessed.getStagingTableFromName(maf, tname);
                                if (st != null && st.LogicalName != null)
                                    tname = st.LogicalName;
                            }
                            dr[0] = tname;
                            dr[1] = ss.numRows < 0 ? 0 : ss.numRows;
                            dr[2] = ss.numErrors < 0 ? 0 : ss.numErrors;
                            dr[3] = ss.numWarnings < 0 ? 0 : ss.numWarnings;
                            dr[4] = ss.duration < 0 ? 0 : ss.duration;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            return (dt);
        }    
    }
}
