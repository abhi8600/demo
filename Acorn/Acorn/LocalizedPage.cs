﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class LocalizedPage : System.Web.UI.Page
    {

        protected override void InitializeCulture()
        {
//            Global.systemLog.Debug("Entering 'InitializeCulture()'.");
            // Already stored for this session?
            string locale = (string)Session["Locale"];
            if (locale == null)
            {
                // Get user or default (global) preference for language
                User u = (User)Session["user"];
                if (u == null)
                {
                    return;
                }
                IDictionary<string, string> localePreference = ManagePreferences.getPreference(null, u.ID, "Locale");
                locale = localePreference["Locale"];
                if (locale == null)
                {
                    Global.systemLog.Warn("'ManagePreferences.getPreference()' did not find a **default** 'locale' preference. Using default of 'en-US'.");
                    locale = "en-US";
                }
                Session["Locale"] = locale;
            }
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(locale);
            // Global.systemLog.Debug("'CurrentCulture' is: " + Thread.CurrentThread.CurrentCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(locale);
            // Global.systemLog.Debug("'CurrentUICulture' is: " + Thread.CurrentThread.CurrentUICulture);
            base.InitializeCulture();
        }
    }
}
