﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class BirstConnectConfig
    {
        public string VisibleName;
        public string FileName;
        public BirstConnectConfig() { }
        public BirstConnectConfig(string filename, string visiblename)
        {
            this.VisibleName = visiblename;
            this.FileName = filename;
        }
    }
}
