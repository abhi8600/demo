﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.Odbc;
using System.Text;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class Skin : System.Web.UI.Page
    {
        private string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            User u = Util.validateAuth(Session, Response);
            if (!u.CanSkin)
            {
                Response.Redirect("Account.aspx");
                return;
            }
            Util.setUpdatedHeader(Session, null, u, Master);
        }

        protected void UploadClick(object sender, EventArgs e)
        {
            User u = (User)Session["user"];
            if (u == null)
                return;
            if (LogoUpload.HasFile)
            {
                using (BinaryReader reader = new BinaryReader(LogoUpload.PostedFile.InputStream))
                {
                    byte[] image = reader.ReadBytes(LogoUpload.PostedFile.ContentLength);
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        if (u.LogoImageID != Guid.Empty)
                            Database.deleteImage(conn, mainSchema, u.LogoImageID);
                        u.LogoImageID = Guid.NewGuid();
                        string type = "jpeg";
                        if (LogoUpload.FileName.ToLower().EndsWith("png"))
                            type = "png";
                        else if (LogoUpload.FileName.ToLower().EndsWith("gif"))
                            type = "gif";
                        Database.saveImage(conn, mainSchema, u.LogoImageID, image, type);
                        Database.updateUser(conn, mainSchema, u);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    Session.Remove("image");
                }
            }
            Response.Redirect("~/Skin.aspx");
        }

        protected void ResetClick(object sender, EventArgs e)
        {
            Session.Remove("image");
            User u = (User)Session["user"];
            if (u == null)
                return;
            bool updateUser = false;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                if (u.LogoImageID != Guid.Empty)
                {
                    Database.deleteImage(conn, mainSchema, u.LogoImageID);
                    u.LogoImageID = Guid.Empty;
                    updateUser = true;
                }
                if (u.HeaderBackground > 0)
                {
                    updateUser = true;
                    u.HeaderBackground = -1;
                }
                if (updateUser)
                    Database.updateUser(conn, mainSchema, u);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Response.Redirect("~/Skin.aspx");
        }

        protected void CancelChanges_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account.aspx");
        }

        protected void SubmitChanges_Click(object sender, EventArgs e)
        {
            string color = ColorBox.Text;
            if (color.Length == 6)
            {
                User u = (User)Session["user"];
                byte[] colors = Util.getBytesFromHex(color);
                u.HeaderBackground = ((long)colors[0] << 16) + ((long)colors[1] << 8) + colors[2];
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.updateUser(conn, mainSchema, u);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            Response.Redirect("~/Account.aspx");
        }
    }
}
