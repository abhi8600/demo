﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Specialized;
using Acorn.Utils;

namespace Acorn
{
    /**
     * support for single signon integration with the salesforce support portal
     */
    public partial class Support : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string orgId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SalesForceSupportOrgId"];
        private static string portalId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SalesForceSupportPortalId"];
        private static string sfdcUrl = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SalesForceSupportPortalURL"];
        private static string sfdcPublicUrl = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SalesForceSupportPortalPublicURL"];
        private static bool supportEnabled = false;

        protected class HTMLInput
        {
            public string name;
            public string value;
            public string id;

            public HTMLInput(string _name, string _value, string _id)
            {
                name = _name;
                value = _value;
                id = _id;
            }
        }

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
            if (orgId == null || orgId.Trim().Length == 0)
            {
                Global.systemLog.Warn("Support Portal 'SalesForceSupportOrgId' not set, support portal access disabled");
                return;
            }
            if (portalId == null || portalId.Trim().Length == 0)
            {
                Global.systemLog.Warn("Support Portal 'SalesForceSupportPortalId' not set, support portal access disabled");
                return;
            }
            if (portalId == null || portalId.Trim().Length == 0)
            {
                Global.systemLog.Warn("Support Portal 'SalesForceSupportPortalURL' not set, support portal access disabled");
                return;
            }
            if (sfdcUrl == null || sfdcUrl.Trim().Length == 0)
            {
                Global.systemLog.Warn("Support Portal 'SalesForceSupportPortalURL' not set, support portal access disabled");
                return;
            }
            supportEnabled = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!supportEnabled)
            {
                Global.systemLog.Warn("redirecting to home page due to support not being enabled (Support.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            User u = Util.validateAuth(Session, Response);
            if (u.isFreeTrialUser)
            {
                Response.Redirect(sfdcPublicUrl);
            }

            // validate where the user should go - if they are a sfdc user, then post the form, otherwise, just go to the public area
            bool supportPortalUser = SalesforceLoader.isSupportPortalUser(u);
            if (!supportPortalUser)
                Response.Redirect(sfdcPublicUrl);

            bool lockedout = false;
            DBConnection.QueryConnection conn = null;
            Global.systemLog.Debug("Attempting to SSO to SFDC Support for " + u.Username + " from " + Request.UserHostAddress);
            try
            {
                conn = ConnectionPool.getConnection();
                string username = u.Username;
                string token = TokenGenerator.getSFDCDelegatedAuthenticationSecurityToken(conn, mainSchema, u, Request.UserHostAddress, ref lockedout);
                token = Util.encryptToken(token);
                List<HTMLInput> inputs = new List<HTMLInput>();
                inputs.Add(new HTMLInput("startURL", "", null));
                inputs.Add(new HTMLInput("loginURL", "", null));
                inputs.Add(new HTMLInput("useSecure", "true", null));
                inputs.Add(new HTMLInput("orgId", orgId, null));
                inputs.Add(new HTMLInput("portalId", portalId, null));
                inputs.Add(new HTMLInput("loginType", "3", null));
                inputs.Add(new HTMLInput("action", "loginAction", null));
                inputs.Add(new HTMLInput("un", username, "username")); // assumes that the SFDC USER username is the same as the Birst username
                inputs.Add(new HTMLInput("pw", token, "password"));

                RedirectAndPOST(this, sfdcUrl, inputs);
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message, ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void RedirectAndPOST(Page page, string destinationUrl, List<HTMLInput> inputs)
        {
            string strForm = PreparePOSTForm(destinationUrl, inputs);
            page.Controls.Add(new LiteralControl(strForm));
        }

        private static String PreparePOSTForm(string url, List<HTMLInput> inputs)
        {
            //Set a name for the form
            string formID = "login";

            //Build the form using the specified data to be posted.
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");
            foreach (HTMLInput input in inputs)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + input.name + "\"");
                if (input.value != null)
                    strForm.Append(" value=\"" + input.value + "\"");
                if (input.id != null)
                    strForm.Append(" id=\"" + input.id + "\"");
                strForm.Append("/>");
            }
            strForm.Append("</form>");

            // Build the JavaScript which will do the Posting operation.
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language='javascript'>");
            strScript.Append("var v" + formID + " = document." + formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");
            // Return the form and the script concatenated. The order is important, Form then JavaScript
            return strForm.ToString() + strScript.ToString();
        }
    }
}