﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Text;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using Acorn.Utils;

// XXX THIS PAGE SHOULD BE REPLACED!
// XXX IT IS PART OF THE OLD HTML VERSION
// XXX USED BY ADDSALESFORCE.ASPX TO SEE SFORCE LOADS
// XXX SHOULD BE MOVED TO THE EXISTING FLEX VERSION

namespace Acorn
{
    public partial class NewPublishLog : System.Web.UI.Page
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (NewPublishLog.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            } 
            
            string loadNum = Request.Params["load"];
            string loadGroup = Request.Params["group"];
            string loadDate = Request.Params["date"];
            string file = Request.Params["file"];
            string back = Request.Params["back"];
            string queryid = Request.Params["queryid"];
            if (loadNum == null && queryid == null)
            {
                if (back != null)
                    Response.Redirect("~/" + HttpUtility.HtmlEncode(back));
                else
                    Response.Redirect("~/Load.aspx");
            }

            if (back == null)
            {
                // bugfix 7451
                // Only SalesForce screen directs to this page. 
                // All the rest goes to 4.0 flex UI. 
                // Eventually this will be obsolete when this page moves to flex UI.
                back = "AddSalesforce.aspx";
            }
            if (back != null)
                BackLink.PostBackUrl = "~/" + HttpUtility.HtmlEncode(back);
            Util.setBreadCrumbPage(sp,u,Master, false);
            LoadNumber.Text = HttpUtility.HtmlEncode(loadNum);
            if (file != null)
            {
                ShowLists.Visible = false;
                FileDetail.Visible = true;
                QueryDetailPanel.Visible = false;
                loadFileGrid(sp, file, loadDate, Request.Params["warn"] != null, int.Parse(loadNum));
            }
            else if (queryid != null && Session["querymap"] != null)
            {
                ShowLists.Visible = false;
                FileDetail.Visible = false;
                QueryDetailPanel.Visible = true;
                try
                {
                    long id = long.Parse(queryid);
                    string q = ((Dictionary<long, string>)Session["querymap"])[id];
                    if (q != null)
                    {
                        string dim = Request.Params["dimension"];
                        string level = Request.Params["level"];
                        string source = Request.Params["source"];
                        string grain = Request.Params["grain"];
                        Query.Text = HttpUtility.HtmlEncode(processQuery(sp, q, source, dim, level, grain));
                    }
                }
                catch (Exception)
                {
                }
                QueryBackButton.PostBackUrl = "~/NewPublishLog.aspx?load=" + HttpUtility.HtmlEncode(loadNum) + "&group=" + HttpUtility.HtmlEncode(loadGroup) + "&date=" + HttpUtility.HtmlEncode(loadDate);
            }
            else
            {
                ShowLists.Visible = true;
                FileDetail.Visible = false;
                QueryDetailPanel.Visible = false;
                int ln = int.Parse(loadNum);
                setLoadGrid(ln, loadGroup == null ? Util.LOAD_GROUP_NAME : loadGroup, loadDate);
                loadSummaryGrid(sp, loadDate, ln, loadGroup == null ? Util.LOAD_GROUP_NAME : loadGroup);
            }
        }

        private string processQuery(Space sp, string q, string datasource, string dimension, string level, string grain)
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            StagingTable sourcest = null;
            DimensionTable targetdt = null;
            MeasureTable targetmt = null;
            if (datasource != null)
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    if (st.Name.Substring(3) == datasource)
                    {
                        sourcest = st;
                        break;
                    }
                }
            if (dimension != null && level != null)
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (dt.AutoGenerated && (dt.InheritTable == null || dt.InheritTable.Length == 0) &&
                        dt.DimensionName == dimension && dt.Level == level)
                    {
                        targetdt = dt;
                        break;
                    }
                }
            if (grain != null)
                foreach (MeasureTable mt in maf.measureTablesList)
                {
                    if (mt.AutoGenerated && (mt.InheritTable == null || mt.InheritTable.Length == 0) &&
                        grainString(maf, mt) == grain)
                    {
                        targetmt = mt;
                        break;
                    }
                }
            q = q.Replace(sp.Schema + ".", "");
            // Create list of from expressions
            Regex r = new Regex(@"FROM \w+ \w\s");
            MatchCollection mc = r.Matches(q);
            Dictionary<string, List<string>> aliasList = new Dictionary<string, List<string>>();
            foreach (Match m in mc)
            {
                int index1 = m.Value.IndexOf(' ');
                int index2 = m.Value.IndexOf(' ', index1 + 1);
                string tname = m.Value.Substring(index1 + 1, index2 - index1 - 1);
                string alias = m.Value.Substring(index2 + 1, m.Value.Length - index2 - 2);
                List<string> list = null;
                if (aliasList.ContainsKey(tname))
                    list = aliasList[tname];
                else
                {
                    list = new List<string>();
                    aliasList.Add(tname, list);
                }
                list.Add(alias);
            }
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.Length > 0) || dt.TableSource.Tables.Length != 1)
                    continue;
                string tname = dt.TableSource.Tables[0].PhysicalName;
                q = q.Replace(tname + "_CKSUM$", "[" + dt.DimensionName + "." + dt.Level + " checksum]");
            }
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                q = q.Replace(st.Name + "_CKSUM$", "[" + st.Name.Substring(3) + " checksum]");
            }
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.Length > 0) || dt.TableSource.Tables.Length != 1)
                    continue;
                string tname = dt.TableSource.Tables[0].PhysicalName;
                List<string> pnames = new List<string>();
                pnames.Add(tname);
                if (aliasList.ContainsKey(tname))
                    pnames.AddRange(aliasList[tname]);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                        foreach (DataRowView dr in drv)
                        {
                            if (!((bool)dr["Qualify"]))
                                continue;
                            if (q.IndexOf(pname + '.' + dr["PhysicalName"]) >= 0)
                            {
                                if (pname == tname)
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + dt.DimensionName + "." + dt.Level + "." + dr["ColumnName"] + "]");
                                else
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + pname + "." + dr["ColumnName"] + "]");
                            }
                        }
                    }
                }
                q = q.Replace(tname, "[" + dt.DimensionName + "." + dt.Level + "]");
            }
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (!mt.AutoGenerated || (mt.InheritTable != null && mt.InheritTable.Length > 0) || mt.TableSource.Tables.Length != 1)
                    continue;
                string tname = mt.TableSource.Tables[0].PhysicalName;
                List<string> pnames = new List<string>();
                pnames.Add(tname);
                if (aliasList.ContainsKey(tname))
                    pnames.AddRange(aliasList[tname]);
                string grainstr = grainString(maf, mt);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { mt.TableName });
                        foreach (DataRowView dr in drv)
                        {
                            if (!((bool)dr["Qualify"]))
                                continue;
                            if (q.IndexOf(pname + '.' + dr["PhysicalName"]) >= 0)
                            {
                                if (pname == tname)
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + grainstr + "." + dr["ColumnName"] + "]");
                                else
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + pname + "." + dr["ColumnName"] + "]");
                            }
                        }
                    }
                }
                q = q.Replace(tname, "[" + grainstr + "]");
            }
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                List<string> pnames = new List<string>();
                pnames.Add(st.Name);
                if (aliasList.ContainsKey(st.Name))
                    pnames.AddRange(aliasList[st.Name]);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                                continue;
                            string cname = pname + '.' + Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name) + '$';
                            if (q.IndexOf(cname) >= 0)
                            {
                                if (pname == st.Name)
                                    q = q.Replace(cname, "[" + pname.Substring(3) + "." + sc.Name + "]");
                                else
                                    q = q.Replace(cname, "[" + pname + "." + sc.Name + "]");
                            }
                        }
                    }
                }
                q = q.Replace(st.Name, "[" + st.Name.Substring(3) + "]");
            }
            if (targetdt != null)
            {
                DataRowView[] ddrv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { targetdt.TableName });
                foreach (DataRowView dr in ddrv)
                {
                    if (!((bool)dr["Qualify"]))
                        continue;
                    if (q.IndexOf((string)dr["PhysicalName"]) >= 0)
                        q = q.Replace((string)dr["PhysicalName"], "[" + dr["ColumnName"] + "]");
                }
            }
            q = q.Replace("dbo.", "");
            return q;
        }

        public int loadN;
        public string loadD;

 
        private void setLoadGrid(int loadnum, string loadGroup, string loadDate)
        {
            Space sp = (Space)Session["space"];
            DataTable dt = PublishLog.getPublishSummaryData(Session, sp, loadnum, loadGroup);
            loadN = loadnum;
            loadD = loadDate;
            LoadDetail.DataSource = dt;
            LoadDetail.DataBind();
        }

        string[] markers = new string[] { "[INSERTDT:", "[UPDATEDT:", "[INSERTF:", "[UPDATEF:" };

        private class Update
        {
            public long ID;
            public int Type;
            public string TableName;
            public string StagingTableName;
            public string Columns;
            public long Rows;
        }

        private void loadSummaryGrid(Space sp, string loadDate, int loadNum, string loadGroup)
        {
            DateTime date = DateTime.Parse(loadDate);
            string logfile = String.Format("smiengine.*.{0:yyyy-MM-dd}.log", date);
            if (!Directory.Exists(sp.Directory + "\\logs"))
                return;
            
            string[] names = Util.getMatchedLogFileNames(sp, loadNum, loadDate);
            if (names == null || names.Length == 0)
                return;
            List<Update> ulist = new List<Update>();
            Dictionary<long, string> querymap = new Dictionary<long, string>();
            DateTime currentLogLineTimeStamp = DateTime.MinValue;
            foreach (string fname in names)
            {
                bool foundStartMarker = false;
                StreamReader reader = new StreamReader(File.Open(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                string line = null;
                //string startMarker = "Starting: ResetETLRun " + loadNum;
                try
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        // Have we ever found startMarker in the current file
                        bool markerMatchedForDifferentLoad = false;
                        int startMarkerIndexForCurrentLoad = PublishLogDetailsService.getStartMarkerIndex(line, loadNum, out markerMatchedForDifferentLoad);
                        foundStartMarker = startMarkerIndexForCurrentLoad >= 0 ? true : foundStartMarker;
                        // if there are multiple load numbers in the same file, make sure that we dont parse the lines from other load number process
                        if (markerMatchedForDifferentLoad)
                        {
                            foundStartMarker = false;
                        }

                        if (startMarkerIndexForCurrentLoad > 0)
                        {
                            // if there is another start marker for this load number, make sure this is the most recent one. It might happen
                            // that even after iteration through sorted files by date, most recent load number (for previous iterations) 
                            // might appear earlier than later since sorted files are based on last modified time 
                            bool isUpdated;
                            DateTime dt = PublishLogDetailsService.getUpdatedLoadLineTimeStamp(line, loadNum, currentLogLineTimeStamp, out isUpdated);
                            if (isUpdated)
                            {
                                currentLogLineTimeStamp = dt;
                                ulist.Clear();
                            }
                        }

                        /*
                        if (line.IndexOf(startMarker) >= 0)
                            ulist.Clear();
                         */

                        if (foundStartMarker)
                        {
                            int qindex = line.IndexOf("[QUERY:");
                            if (qindex > 0)
                            {
                                int index1 = line.IndexOf(":", qindex + 7) + 1;
                                int index2 = line.IndexOf("]", index1) + 1;
                                int count = 0;
                                while (index2 == 0)
                                {
                                    line += "<br/>" + reader.ReadLine();
                                    index2 = line.IndexOf("]", index1) + 1;
                                    if (count++ > 15)
                                        break;
                                }
                                long id = 0;
                                if (index2 > 0)
                                {
                                    try
                                    {
                                        id = long.Parse(line.Substring(qindex + 7, index1 - qindex - 8));
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    string q = line.Substring(index1, index2 - index1 - 1);
                                    if (!querymap.ContainsKey(id))
                                        querymap.Add(id, q);
                                    else
                                        querymap[id] = querymap[id] + "\r\n" + q;
                                }
                            }
                            else
                                for (int i = 0; i < markers.Length; i++)
                                {
                                    string m = markers[i];
                                    int index = line.IndexOf(m);
                                    if (index > 0)
                                    {
                                        int index2 = line.LastIndexOf("]");
                                        if (index2 > 0)
                                        {
                                            Update u = new Update();
                                            u.Type = i;
                                            string[] parts = line.Substring(index + 1, index2 - index - 1).Split(new char[] { ':' });
                                            if (parts.Length < 6)
                                                continue;
                                            int partloadnum = -1;
                                            if (parts.Length == 6)
                                            {
                                                u.TableName = parts[2];
                                                u.StagingTableName = parts[3];
                                                long.TryParse(parts[4], out u.Rows);
                                                u.Columns = parts[5];
                                            }
                                            else
                                            {
                                                u.ID = long.Parse(parts[1]);
                                                if (parts[2] != null)
                                                    int.TryParse(parts[2], out partloadnum);
                                                u.TableName = parts[3];
                                                u.StagingTableName = parts[4];
                                                long.TryParse(parts[5], out u.Rows);
                                                u.Columns = parts[6];
                                            }
                                            u.Columns = u.Columns.Trim(new char[] { '[', ']' });
                                            if (partloadnum < 0 || partloadnum == loadNum)
                                                ulist.Add(u);
                                        }
                                        break;
                                    }
                                }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            Session["querymap"] = querymap;
            DataTable dimTable = new DataTable();
            dimTable.Columns.Add("Dimension");
            dimTable.Columns.Add("Level");
            dimTable.Columns.Add("Type");
            dimTable.Columns.Add("Datasource");
            dimTable.Columns.Add("Rows", typeof(long));
            dimTable.Columns.Add("Columns");
            dimTable.Columns.Add("ID", typeof(long));
            dimTable.Columns.Add("load");
            dimTable.Columns.Add("group");
            dimTable.Columns.Add("date");
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            foreach (Update u in ulist)
            {
                if (u.Rows == 0)
                    continue;
                if (u.Type == 0 || u.Type == 1)
                {
                    int index = maf.findDimensionTableIndex(u.TableName);
                    if (index < 0)
                        continue;
                    DimensionTable dt = maf.dimensionTablesList[index];
                    DataRow dr = dimTable.NewRow();
                    dr[0] = HttpUtility.HtmlEncode(maf.hmodule.getDimensionHierarchy(dt.DimensionName).Name);
                    dr[1] = HttpUtility.HtmlEncode(dt.Level);
                    dr[2] = u.Type == 0 ? "Add Records" : "Update Records";
                    dr[3] = HttpUtility.HtmlEncode(u.StagingTableName.Substring(3));
                    dr[4] = u.Rows;
                    dr[5] = HttpUtility.HtmlEncode(u.Columns);
                    dr[6] = u.ID;
                    dr[7] = loadNum;
                    dr[8] = HttpUtility.HtmlEncode(loadGroup);
                    dr[9] = HttpUtility.HtmlEncode(loadDate);
                    dimTable.Rows.Add(dr);
                }
            }
            DimensionLoadGrid.DataSource = dimTable;
            DimensionLoadGrid.DataBind();
            DataTable mTable = new DataTable();
            mTable.Columns.Add("Grain");
            mTable.Columns.Add("Type");
            mTable.Columns.Add("Datasource");
            mTable.Columns.Add("Rows", typeof(long));
            mTable.Columns.Add("Columns");
            mTable.Columns.Add("ID", typeof(long));
            mTable.Columns.Add("load");
            mTable.Columns.Add("group");
            mTable.Columns.Add("date");
            foreach (Update u in ulist)
            {
                if (u.Rows == 0)
                    continue;
                MeasureTable mt = null;
                foreach (MeasureTable smt in maf.measureTablesList)
                {
                    if (smt.TableName == u.TableName)
                    {
                        mt = smt;
                        break;
                    }
                }
                if (mt == null)
                    continue;
                if (u.Type == 2 || u.Type == 3)
                {
                    DataRow dr = mTable.NewRow();
                    dr[0] = HttpUtility.HtmlEncode(grainString(maf, mt));
                    dr[1] = u.Type == 2 ? "Add Records" : "Update Records";
                    dr[2] = HttpUtility.HtmlEncode(u.StagingTableName.Substring(3));
                    dr[3] = u.Rows;
                    dr[4] = HttpUtility.HtmlEncode(u.Columns);
                    dr[5] = u.ID;
                    dr[6] = loadNum;
                    dr[7] = HttpUtility.HtmlEncode(loadGroup);
                    dr[8] = HttpUtility.HtmlEncode(loadDate);
                    mTable.Rows.Add(dr);
                }
            }
            MeasureLoadGrid.DataSource = mTable;
            MeasureLoadGrid.DataBind();
        }

        private string grainString(MainAdminForm maf, MeasureTable mt)
        {
            List<MeasureTableGrain> mtglist = maf.getGrainInfo(mt);
            StringBuilder sb = new StringBuilder();
            foreach (MeasureTableGrain mtg in mtglist)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(mtg.DimensionName + "." + mtg.DimensionLevel);
            }
            return sb.ToString();
        }

        private static string SEARCH = " ERROR - [DATAERROR:";
        private static string WARN_SEARCH = " WARN  - [DATAWARNING:";

        private void loadFileGrid(Space sp, string filename, string loadDate, bool warnings, int loadID)
        {            
            DataSourceName.Text = HttpUtility.HtmlEncode(filename);
            WarnLabel.Visible = warnings;
            ErrorLabel.Visible = !warnings;
            DateTime date = DateTime.Parse(loadDate);
            string logfile = String.Format("smiengine.*.{0:0000000000}.log", loadID);
            string[] fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
            if (fnames.Length == 0)
                return;
            DataTable dt = new DataTable();
            dt.Columns.Add("Error");
            List<string> issues = new List<string>();
            DateTime currentLogLineTimeStamp = DateTime.MinValue;
            foreach (string fname in fnames)
            {
                bool foundStartMarker = false;
                StreamReader reader = new StreamReader(File.Open(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                string line = null;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                StagingTable st = null;
                foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
                {
                    if (sst.Name == "ST_" + filename)
                    {
                        st = sst;
                        break;
                    }
                }
                if (st == null)
                    return;
                SourceFile sf = maf.getSourceFile(st);
                string find = (warnings ? WARN_SEARCH : SEARCH) + sf.FileName + "]";
                try
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        // Have we ever found startMarker in the current file
                        bool markerMatchedForDifferentLoad = false;
                        int startMarkerIndexForCurrentLoad = PublishLogDetailsService.getStartMarkerIndex(line, loadID, out markerMatchedForDifferentLoad);
                        foundStartMarker = startMarkerIndexForCurrentLoad >= 0 ? true : foundStartMarker;
                        // if there are multiple load numbers in the same file, make sure that we dont parse the warnings from other load numbers
                        if (markerMatchedForDifferentLoad)
                        {
                            foundStartMarker = false;
                        }

                        if (startMarkerIndexForCurrentLoad >= 0)
                        {
                            // if there is another start marker for this load number, make sure this is the most recent one. It might happen
                            // that even after iteration through sorted files by date, most recent load number (for previous iterations) 
                            // might appear earlier than later since sorted files are based on last modified time 
                            bool isUpdated;
                            DateTime dateTime = PublishLogDetailsService.getUpdatedLoadLineTimeStamp(line, loadID, currentLogLineTimeStamp, out isUpdated);
                            if (isUpdated)
                            {
                                currentLogLineTimeStamp = dateTime;
                                issues.Clear();
                            }
                        }


                        if (foundStartMarker && line.Contains(find))
                        {
                            int index = line.IndexOf(find) + find.Length;
                            string error = line.Substring(index + 1);
                            DataRow dr = dt.NewRow();
                            dr[0] = HttpUtility.HtmlEncode(error);
                            dt.Rows.Add(dr);
                            if (dt.Rows.Count > 100)
                                break;
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            ErrorGrid.DataSource = dt;
            ErrorGrid.DataBind();
        }
    }
}
