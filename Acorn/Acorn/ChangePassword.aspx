﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs"
    Inherits="Acorn.ChangePassword" MasterPageFile="~/WebAdmin.Master" %>

<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <div style="padding-right: 10px">
            <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;">
                <tr>
                    <td>
                        <table border="0" cellpadding="0">
                            <tr>
                                <td colspan="2" style="font-weight: bold">
                                    <asp:Label ID="ChangePasswordLabel" runat="server" Text="Change Your Password (we recommend that you do not share this password with any other services)" Visible="false" />
                                    <asp:Label ID="RequiredChangePasswordLabel" runat="server" Text="You must change your password (we recommend that you do not share this password with any other services)" Visible="false" />
                                </td>
                            </tr>
                            <tr style="height: 20px; vertical-align: top">
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Password:</asp:Label>
                                </td>
                                <td style="padding-left: 3px">
                                    <font color="blue">*</font>&nbsp;<asp:TextBox ID="CurrentPassword" runat="server"
                                        TextMode="Password" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="CustomValidator1" ControlToValidate="CurrentPassword" runat="server"
                                        OnServerValidate="OldPasswordValidator_Validate" EnableClientScript="False" ValidationGroup="ChangePasswordGroup"></asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePasswordGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                                </td>
                                <td style="padding-left: 3px">
                                    <font color="blue">*</font>&nbsp;<asp:TextBox ID="NewPassword" runat="server" TextMode="Password"
                                        autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="PasswordValidator" ControlToValidate="NewPassword" runat="server"
                                        OnServerValidate="PasswordValidator_Validate" EnableClientScript="False" ValidationGroup="ChangePasswordGroup"></asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                        ErrorMessage="New Password is required." ToolTip="New Password is required."
                                        ValidationGroup="ChangePasswordGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                                </td>
                                <td style="padding-left: 3px">
                                    <font color="blue">*</font>&nbsp;<asp:TextBox ID="ConfirmNewPassword" runat="server"
                                        TextMode="Password" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                        ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                        ValidationGroup="ChangePasswordGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                        ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                        ValidationGroup="ChangePasswordGroup"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr style="height: 35px; vertical-align: top">
                                <td colspan="2">
                                    <span style="font-size: 9pt"><font style="color: blue">*</font>&nbsp;Required information</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:ImageButton ID="ChangePasswordButton" runat="server" OnClick="ChangePasswordButton_Click" CausesValidation="true"
                                        ImageUrl="~/Images/Change_Password.png" ToolTip="Change Password" ValidationGroup="ChangePasswordGroup" />
                                </td>
                            </tr>
                            <asp:Panel ID="MessagesPanel" runat="server" Visible="false">
                            <tr style="height: 20px; vertical-align: top">
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="SuccessLabel" runat="server"  ForeColor="Green" Text="Password change succeeded" Visible="false"/>
                                    <asp:Label ID="FailLabel" runat="server" ForeColor="Red" Text="Password change failed" Visible="false" />
                                </td>
                            </tr>
                            </asp:Panel>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="height: 50px">
        &nbsp;</div>
</asp:Content>
