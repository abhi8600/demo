﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPublishLog.aspx.cs" Inherits="Acorn.NewPublishLog"
    MasterPageFile="~/WebAdmin.Master" Title="Birst" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <asp:Panel ID="ShowLists" runat="server">
            <div class="pageheader">
                Processing History Detail
            </div>
            <div style="font-size: 9pt">
                <asp:LinkButton ID="BackLink" runat="server" PostBackUrl="~/Load.aspx">Back</asp:LinkButton>
            </div>
            <div style="height: 10px">
                &nbsp;</div>
            <div style="font-size: 10pt">
                Processing Load Number:&nbsp;<asp:Label ID="LoadNumber" runat="server"></asp:Label>
            </div>
            <div style="height: 5px">
                &nbsp;</div>
            <asp:GridView ID="LoadDetail" runat="server" AutoGenerateColumns="False" CellPadding="0"
                ForeColor="#333333" GridLines="None" Width="600px">
                <RowStyle BackColor="White" ForeColor="#333333" CssClass="listviewcell" />
                <Columns>
                    <asp:BoundField DataField="DataSource" HeaderText="Data Source" HeaderStyle-Width="40%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" Width="35%" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Processed" HeaderText="Rows Processed" HeaderStyle-Width="30%"
                        DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <HeaderStyle HorizontalAlign="Right" Width="25%" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Errors">
                        <ItemTemplate>
                            <asp:HyperLink ID="ErrorLink" runat="server" NavigateUrl='<%# ((long)Eval("Errors")) > 0 ? Eval("DataSource", "NewPublishLog.aspx?file={0}")+"&load="+this.loadN+"&date="+this.loadD:"" %>'
                                Text='<%# Eval("Errors", "{0:#,##0}") %>'></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" Width="20%" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Warnings">
                        <ItemTemplate>
                            <asp:HyperLink ID="WarningLink" runat="server" NavigateUrl='<%# ((long)Eval("Warnings")) > 0 ? Eval("DataSource", "NewPublishLog.aspx?warn=true&file={0}")+"&load="+this.loadN+"&date="+this.loadD:"" %>'
                                Text='<%# Eval("Warnings", "{0:#,##0}") %>'></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" Width="20%" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <div style="height: 15px">
                &nbsp;</div>
            <div class="pageheader">
                Hierarchy Load Detail
            </div>
            <div style="height: 5px">
                &nbsp;</div>
            <asp:GridView ID="DimensionLoadGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                ForeColor="#333333" GridLines="None" Width="900px">
                <RowStyle BackColor="#E7E6E3" ForeColor="#333333" CssClass="listviewcell" Font-Size="9pt" />
                <Columns>
                    <asp:BoundField DataField="Datasource" HeaderText="Data Source" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false"></asp:BoundField>
                    <asp:HyperLinkField DataTextField="Type" HeaderText="Type" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="False" DataTextFormatString="{0}"
                        DataNavigateUrlFormatString="NewPublishLog.aspx?queryid={0}&load={1}&group={2}&date={3}&source={4}&dimension={5}&level={6}"
                        DataNavigateUrlFields="ID,load,group,date,Datasource,Dimension,Level" />
                    <asp:TemplateField HeaderText="Target Level">
                        <ItemTemplate>
                            <%# Eval("Dimension")+"."+Eval("Level") %>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Rows" HeaderText="# Rows" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0}&nbsp;" HeaderStyle-Width="75px">
                    </asp:BoundField>
                    <asp:BoundField DataField="Columns" HeaderText="Source Columns Used" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                </Columns>
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <div style="height: 15px">
                &nbsp;</div>
            <div class="pageheader">
                Measure Grain Detail
            </div>
            <div style="height: 5px">
                &nbsp;</div>
            <asp:GridView ID="MeasureLoadGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                ForeColor="#333333" GridLines="None" Width="900px">
                <RowStyle BackColor="#E7E6E3" ForeColor="#333333" CssClass="listviewcell" Font-Size="9pt" />
                <Columns>
                    <asp:BoundField DataField="Datasource" HeaderText="Data Source" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false"></asp:BoundField>
                    <asp:HyperLinkField DataTextField="Type" HeaderText="Type" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="False" DataTextFormatString="{0}"
                        DataNavigateUrlFormatString="NewPublishLog.aspx?queryid={0}&load={1}&group={2}&date={3}&source={4}&grain={5}"
                        DataNavigateUrlFields="ID,load,group,date,Datasource,Grain" />
                    <asp:BoundField DataField="Grain" HeaderText="Grain" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px"></asp:BoundField>
                    <asp:BoundField DataField="Rows" HeaderText="# Rows" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" DataFormatString="{0:#,##0}&nbsp;">
                    </asp:BoundField>
                    <asp:BoundField DataField="Columns" HeaderText="Source Columns Used" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false"></asp:BoundField>
                </Columns>
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="FileDetail" runat="server" Visible="false">
            <div class="pageheader">
                <asp:Label ID="ErrorLabel" runat="server">Data Source Errors</asp:Label>
                <asp:Label ID="WarnLabel" runat="server">Data Source Warnings</asp:Label>
            </div>
            <div style="height: 5px">
                &nbsp;</div>
            <div style="font-size: 10pt">
                Data Source:&nbsp;<asp:Label ID="DataSourceName" runat="server"></asp:Label>
            </div>
            <div style="height: 5px">
                &nbsp;</div>
            <asp:GridView ID="ErrorGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                ForeColor="#333333" GridLines="None" Width="900px">
                <RowStyle BackColor="White" ForeColor="#333333" CssClass="listviewcell" />
                <Columns>
                    <asp:BoundField DataField="Error" HeaderText="Issue" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                </Columns>
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <div style="height: 5px">
                &nbsp;</div>
            <div style="padding-left: 10px">
                <asp:ImageButton ID="DoneButton" runat="server" PostBackUrl="~/NewPublishLog.aspx" ImageUrl="~/Images/Done.png">
                </asp:ImageButton>
            </div>
        </asp:Panel>
        <asp:Panel ID="QueryDetailPanel" runat="server">
            <div class="pageheader">
                Process Step Detail
            </div>
            <div style="font-size: 9pt">
                <asp:LinkButton ID="QueryBackButton" runat="server" PostBackUrl="~/NewPublishLog.aspx">Back</asp:LinkButton>
            </div>
            <div style="height: 10px">
                &nbsp;</div>
            <pre style="font-size: 8pt">
<asp:Label ID="Query" runat="server"></asp:Label></pre>
        </asp:Panel>
    </div>
</asp:Content>
