﻿using System;
using System.Collections;
using System.Collections.Generic;
using Performance_Optimizer_Administration;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Acorn.Utils;

namespace Acorn
{
    public partial class DetailLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            int loadid = 0;
            string temp = Request["loadid"]; // only passed in 4.4.2 and beyond
            if (temp != null && temp.Length > 0)
            {
                if (!Int32.TryParse(temp, out loadid))
                {
                    loadid = 0;
                }
            }
            string loadGroup = null;
            temp = Request["loadgroup"]; // only passed in 4.4.2 and beyond for Birst Local
            if (temp != null && temp.Trim().Length > 0)
            {
                loadGroup = temp;                
            }
            string loadDate = null;
            temp = Request["loadDate"]; // only passed in 5.0.1 and beyond for backward compatibility
            if (temp != null && temp.Trim().Length > 0)
            {
                loadDate = temp;
            }

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);

            processLogFiles(u, sp, loadid, loadGroup, loadDate);
        }

        // find a load log file in the space logs directory
        private void processLogFiles(User u, Space sp, int loadid, string loadGroup, string loadDate)
        {
            string[] fnames = null;
            if (!Directory.Exists(sp.Directory + "\\logs"))
            {
                if (loadGroup != null && loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                {
                    Directory.CreateDirectory(sp.Directory + "\\logs");
                }
                else
                {
                    noResult("Logs directory for the space does not exist.");
                    return;
                }
            }
            if (loadid > 0)
            {
                // specific load id
                string logfile = String.Format("smiengine.*.{0:0000000000}.log", loadid);
                if (loadGroup != null && loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                {
                    fnames = LiveAccessUtil.getLogFileNameForLiveAccessConnection(sp, loadid, loadGroup);                                        
                }
                else
                {
                    fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                }
                if ((fnames == null || fnames.Length == 0) && loadDate != null && 
                    (loadGroup == null || !loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX)))
                {
                    //look for loadDate - if data processed using older processing engine, logfile name will not have load id
                    DateTime dt;
                    string[] formats = { "M/dd/yyyy", "MM/dd/yyyy", "M/d/yyyy", "MM/d/yyyy" };
                    if (DateTime.TryParseExact(loadDate, formats,System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dt))
                    {
                        logfile = String.Format("smiengine.*.{0}.log", dt.ToString("yyyy-MM-dd"));
                        fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                    }
                }
            }

            if (fnames == null || fnames.Length == 0) // backwards compatibility
            {
                if (loadGroup != null && loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                {
                    fnames = LiveAccessUtil.getLogFileNameForLiveAccessConnection(sp, 0, loadGroup);
                }
                else
                {
                    // most recent file (backwards compatibilty - pre 4.4.2)
                    string logfile = String.Format("smiengine.*.log", loadid);
                    var files = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly).OrderByDescending(d => new FileInfo(d).LastWriteTime);
                    foreach (string s in files)
                    {
                        fnames = new string[] { s };
                        break;
                    }
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    List<string> fnamesList = new List<string>();
                    if (fnames != null)
                        fnamesList.AddRange(fnames);
                    foreach (LocalETLConfig leConfig in leConfigs.Values)
                    {
                        //assuming most recent file (for current load) will have loadid = sp.LoadNumber + 1
                        string[] localFnames = LiveAccessUtil.getLogFileNameForLiveAccessConnection(sp, sp.LoadNumber + 1, leConfig.getLocalETLLoadGroup());
                        if (localFnames != null)
                            fnamesList.AddRange(localFnames);
                    }
                    if (fnamesList.Count > 1)
                    {
                        //find the logfile with highest loadid
                        int maxLoadId = 0;
                        string lastestFname = null;
                        foreach (string fname in fnamesList)
                        {
                            string tfname = fname.Substring(0, fname.Length - ".log".Length);
                            string loadIdStr = tfname.Substring(tfname.LastIndexOf(".") + 1);
                            int parseID;
                            if (int.TryParse(loadIdStr, out parseID) && parseID > maxLoadId)
                            {
                                maxLoadId = parseID;
                                lastestFname = fname;
                            }
                        }
                        fnames = new string[] { lastestFname };
                    }
                    else if (fnamesList.Count == 1)
                    {
                        fnames = fnamesList.ToArray();
                    }
                }
            }
            if (fnames == null || fnames.Length == 0)
            {
                noResult("No matching load log files available.");
                return;
            }
            processLogFile(u, sp, fnames[0], loadid);
        }

        private void noResult(string message)
        {
            Response.Clear();
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(Response.OutputStream);
                writer.Write(message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            Response.Flush();
            Response.End();
        }

        private void outputLine(String line, bool stripThreadInfo)
        {
            if (stripThreadInfo)
            {
                // strip first [....]
                int first = line.IndexOf('[');
                int second = line.IndexOf(']', first);
                if (first > 0 && second > first)
                {
                    line = line.Remove(first, second - first + 2); // time [thread name] INFO  - message
                }
            }
            line = line + System.Environment.NewLine;
            byte[] array3 = Encoding.UTF8.GetBytes(line);
            Response.OutputStream.Write(array3, 0, array3.Length);
        }
        
        private static string INFOSTR =  "] INFO  - ";
        private static string WARNSTR =  "] WARN  - ";
        private static string ERRORSTR = "] ERROR - ";

        private void processLogFile(User u, Space sp, string filename, int loadid)
        {
            string name = "birstloadlog.txt";
            if (loadid > 0)
            {
                name = String.Format("birstloadlog.{0:0000000000}.txt", loadid);
            }
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(name));
            Response.ContentType = "text/plain";

            bool full = false; // u.RepositoryAdmin || u.OperationsFlag; // do not sanitize the log file

            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                FileStream fs = null;
                StreamWriter writer = null;
                StreamReader reader = null;
                try
                {
                    fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    reader = new StreamReader(fs);

                    string line = null;
                    string lowerid = sp.ID.ToString().ToLower();
                    string lowerschema = sp.Schema.ToString().ToLower();
                    int count = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!Response.IsClientConnected) // make sure we are still connected
                            return;
                        if (count > 0 && (count % 100) == 0)
                            Response.Flush();
                        if (full)
                        {
                            outputLine(line, false);
                            count++;
                            continue;
                        }
                        int index = line.IndexOf(INFOSTR);
                        if (index < 0)
                        {
                            index = line.IndexOf(WARNSTR);
                            if (index < 0)
                            {
                                index = line.IndexOf(ERRORSTR);
                                if (index < 0)
                                    continue;
                            }
                        }
                        int tindex = line.IndexOf("[");
                        if (tindex < 0 || tindex >= index)
                            continue;
                        string pool = line.Substring(tindex + 1, index - tindex - 1);
                        if (pool == "main")
                            continue;
                        int start = index + INFOSTR.Length;
                        bool startquery = false;
                        if (line.IndexOf("[QUERY:", start) >= 0)
                            startquery = true;
                        line = line.Replace("[ACORN: ", "[");

                        // replace ID's with Names
                        line = line.Replace(sp.ID.ToString(), "[" + sp.Name + "]");
                        line = line.Replace(lowerid, "[" + sp.Name + "]");
                        line = line.Replace(sp.Schema, "[" + sp.Name + "]");
                        line = line.Replace(lowerschema, "[" + sp.Name + "]");

                        // output the line
                        outputLine(line, true);

                        // deal with the query continuation lines, last one ends with ']'
                        int cnt = 0;
                        while (startquery && !line.EndsWith("]"))
                        {
                            line = reader.ReadLine();
                            if (line != null)
                            {
                                // replace ID's with Names
                                line = line.Replace(sp.Schema, "[" + sp.Name + "]");
                                line = line.Replace(lowerschema, "[" + sp.Name + "]");
                                outputLine(line, false);
                                count++;
                            }
                            else break;
                            cnt++;
                            if (cnt > 100) // failsafe
                                break;
                        }
                    }
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                    if (fs != null)
                        fs.Close();
                    if (writer != null)
                        writer.Close();
                }
            }
            Response.Flush();
            Response.End();
        }
    }
}
