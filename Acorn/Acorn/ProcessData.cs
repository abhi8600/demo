﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Net.Mail;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Text;
using System.Collections.Generic;
using Acorn.Utils;

namespace Acorn
{
    public class ProcessData
    {
        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        public static string createtimecmd = System.Web.Configuration.WebConfigurationManager.AppSettings["CreateTimeCommand"];
        public static string contentDir = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];

        public static void Process(MainAdminForm maf, Space sp, User u, string email, string path, DateTime loadDate,
                                   string loadGroupName, int loadNumber, bool createAutoDashboards, string[] subgroups)
        {
            Process(maf, sp, u, email, path, loadDate, loadGroupName, loadNumber, createAutoDashboards, subgroups, false, true);
        }

        public static void Process(MainAdminForm maf, Space sp, User u, string email, string path, DateTime loadDate,
                                   string loadGroupName, int loadNumber, bool createAutoDashboards, string[] subgroups,
                                   bool backgroundRequest, bool sendEmail)
        {
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);
            try
            {
                maf.setupTree();
                string enginecmd = Util.getEngineCommand(sp);

                // Make sure we can publish
                bool canpublish = SpaceAdminService.checkSpacePublishability(maf, sp);
                Acorn.ApplicationLoader.LoadAuthorization la = ApplicationLoader.LoadAuthorization.OK;
                bool isBeingPublished = false;
                bool isSpaceBusyInOtherOp = false;
                HashSet<string> liveAccessLoadGroups = new HashSet<string>();
                if (canpublish)
                {
                    try
                    {
                        ApplicationLoader al =
                            new ApplicationLoader(maf, sp, loadDate, mainSchema, enginecmd, createtimecmd,
                                contentDir, null, u, loadGroupName, loadNumber + 1);
                        if (ApplicationLoader.isSpaceBeingPublished(sp))
                            isBeingPublished = true;
                        if (isBeingPublished)
                        {
                            Global.systemLog.Warn("Processing for space id: " + sp.ID + " : name : " + sp.Name
                                + " in progress. Will not be able to service request to process the same space before earlier load finishes");
                        }


                        int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp);
                        if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                        {
                            isSpaceBusyInOtherOp = true;
                            Global.systemLog.Warn("Cannot process : space id: " + sp.ID + " : name : " + sp.Name
                                + " in progress. " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));
                        }

                        la = al.getLoadAuthorization(loadGroupName);
                        if (!isSpaceBusyInOtherOp && !isBeingPublished && la == Acorn.ApplicationLoader.LoadAuthorization.OK)
                        {
                            Global.systemLog.Info("Load Authorized for space : " + sp.ID);
                            if (subgroups != null && subgroups.Length > 0)
                                al.SubGroups = subgroups;
                            al.load(createAutoDashboards, null, backgroundRequest);
                            if (al.liveAccessLoadStatusMap != null)
                            {
                                // need to poll status of localetl loads to get the consolidated status of load
                                Status.StatusResult sr = null;
                                do
                                {
                                    sr = null;
                                    Dictionary<string, bool> liveAccessLoadStatusMap = new Dictionary<string, bool>();
                                    foreach (string lg in al.liveAccessLoadStatusMap.Keys)
                                    {
                                        if (al.liveAccessLoadStatusMap[lg])
                                        {
                                            liveAccessLoadStatusMap.Add(lg, al.liveAccessLoadStatusMap[lg]);
                                        }
                                    }
                                    foreach (string lg in liveAccessLoadStatusMap.Keys)
                                    {
                                        liveAccessLoadGroups.Add(lg);
                                        sr = Status.getLoadStatus(null, sp, loadNumber + 1, loadGroupName, false, new Status.StatusResult(Status.StatusCode.LoadingAnotherLoadGroup));
                                        if (sr != null && Status.isRunningCode(sr.code))
                                            break;
                                        else
                                        {
                                            Global.systemLog.Debug("ProcessData.Process - status = " + sr == null ? "null" : sr.code.ToString());
                                            al.liveAccessLoadStatusMap[lg] = false;
                                        }
                                        System.Threading.Thread.Sleep(5000);
                                    }
                                    System.Threading.Thread.Sleep(5000);
                                } while (sr != null && sr.code == Status.StatusCode.LoadingAnotherLoadGroup);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Warn("Exception while scheduled processing for space id : " + sp.ID + " : name : " + sp.Name + " : loadGroup : " + loadGroupName);
                        Util.sendErrorDetailEmail("[Problem during scheduled processing for space "
                            + (sp != null ? sp.ID.ToString() : null) +
                            " and user " + (u != null ? u.Username : null) + " ]",
                            "Scheduled Processing",
                            "Exception while loading the space for iteration " + (loadNumber + 1) + " : loadGroup : " + loadGroupName + " : " + ex.Message,
                            ex, sp, null);
                        return;
                    }
                }

                if (!sendEmail)
                {
                    return;
                }
                Global.systemLog.Debug("Now sending e-mail for space = " + sp.ID.ToString());
                try
                {
                    // Send acknowledgement                    
                    string body = getProcessEmailBody(path);
                    MailMessage mm = getMailMessage(sp, email, body);
                    if (!isSpaceBusyInOtherOp && !isBeingPublished && canpublish && la == ApplicationLoader.LoadAuthorization.OK)
                    {
                        fixUpBodyOnSuccessfulProcessing(mm, sp, loadNumber + 1, loadGroupName, liveAccessLoadGroups);
                    }
                    else
                    {
                        bool dataLimitExceeded = la == ApplicationLoader.LoadAuthorization.DataLimitExceeded || la == ApplicationLoader.LoadAuthorization.DataPublishLimitExceeded;
                        bool rowLimitExceeded = la == ApplicationLoader.LoadAuthorization.RowPublishLimitExceeded || la == ApplicationLoader.LoadAuthorization.RowPublishLimitExceeded;
                        mm = fixUpMessageOnFailure(mm, canpublish, isBeingPublished, isSpaceBusyInOtherOp, dataLimitExceeded, rowLimitExceeded, true, sp);
                    }                    
                    sendSecureEmailIfNeeded(mm);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Could not send email", ex);
                }
            }
            finally
            {
                maf.setInUberTransaction(previousUberTransaction);
            }
        }

        public static void sendSecureEmailIfNeeded(MailMessage mm)
        {
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            sendEmailMessage(mm, useSSL);
        }

        public static string getProcessEmailBody(string path)
        {
            StreamReader reader = new StreamReader(path + "ProcessEmail.htm");
            string body = reader.ReadToEnd();
            reader.Close();
            return body;
        }

        public static MailMessage getMailMessage(Space sp, string toEmail, string body)
        {
            MailMessage mm = new MailMessage();
            mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
            mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
            mm.To.Add(new MailAddress(toEmail));
            mm.IsBodyHtml = true;
            mm.Body = body;
            mm.Body = mm.Body.Replace("{SPACE}", sp.Name);
            return mm;
        }

        private static string getTruncatedSpaceName(Space sp)
        {
            string spaceName = sp.Name;
            if (sp.Name.Length > 128)
                spaceName = sp.Name.Substring(0, 128);
            return spaceName;
        }

        public static MailMessage fixUpBodyOnSuccessfulExtraction(MailMessage mm, Space sp, string connectorName, string[] extractGroups)
        {
            mm.Subject = "Birst Notification - Successful Extraction [" + getTruncatedSpaceName(sp) + "]";
            mm.Body = mm.Body.Replace("{ERROR}", "");
            DataTable consolidatedDT = new DataTable();
            consolidatedDT.Columns.Add("DataSource");
            consolidatedDT.Columns.Add("Warnings", typeof(long));

            if (sp == null || connectorName == null)
                return null;
            string configFileDirectory = ConnectorUtils.getConnectorsDirectoryName(sp);
            if (!Directory.Exists(configFileDirectory))
                return null;
            string configFileName = Path.Combine(configFileDirectory, connectorName + "_config.xml");
            if (!File.Exists(configFileName))
                return null;

            XmlDocument xDocRoot = new XmlDocument();
            xDocRoot.XmlResolver = null;
            xDocRoot.Load(configFileName);
            HashSet<String> names = ConnectorUtils.getObjectNames(xDocRoot, extractGroups);
            foreach (string name in names)
            {
                DataRow newdr = consolidatedDT.NewRow();
                newdr["DataSource"] = name;
                newdr["Warnings"] = 0L;
                consolidatedDT.Rows.Add(newdr);
            }

            if (consolidatedDT.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table style=\"font-family: Courier; font-size: 12pt\">");
                sb.Append("<tr><td>Data Source</td><td>Warnings</td></tr>");
                foreach (DataRow dr in consolidatedDT.Rows)
                {
                    string warnings = String.Format("{0:#,###}", (long)dr["Warnings"]);
                    sb.Append("<tr><td>" + dr["DataSource"] + "</td><td align=\"right\">" + warnings + "</td></tr>");
                }
                sb.Append("</table>");
                mm.Body = mm.Body.Replace("{RESULTS}", sb.ToString());
            }
            else
            {
                mm.Body = mm.Body.Replace("{RESULTS}", "<p>No data rows extracted.</p>");
            }

            return mm;
        }

        public static MailMessage fixUpBodyOnSuccessfulProcessing(MailMessage mm, Space sp, int loadNumber, string loadGroupName,
           System.Collections.Generic.HashSet<string> liveAccessLoadGroups)
        {
            return fixUpBodyOnSuccessfulProcessing(mm, sp, loadNumber, loadGroupName, liveAccessLoadGroups, null);
        }

        public static MailMessage fixUpBodyOnSuccessfulProcessing(MailMessage mm, Space sp, int loadNumber, string loadGroupName,
            System.Collections.Generic.HashSet<string> liveAccessLoadGroups, string parentSource)
        {
            if (parentSource == "Connector")
            {
                mm.Subject = "Birst Notification - Successful Extraction and Processing [" + getTruncatedSpaceName(sp) + "]";
            }
            else
            {
                mm.Subject = "Birst Notification - Successful Processing [" + getTruncatedSpaceName(sp) + "]";
            }
            mm.Body = mm.Body.Replace("{ERROR}", "");
            DataTable consolidatedDT = new DataTable();
            consolidatedDT.Columns.Add("DataSource");
            consolidatedDT.Columns.Add("Processed", typeof(long));
            consolidatedDT.Columns.Add("Warnings", typeof(long));
            DataTable dt = PublishLog.getPublishSummaryData(null, sp, loadNumber, loadGroupName);
            foreach (DataRow dr in dt.Rows)
            {
                DataRow newdr = consolidatedDT.NewRow();
                newdr["DataSource"] = dr["DataSource"];
                newdr["Processed"] = dr["Processed"];
                newdr["Warnings"] = dr["Warnings"];
                consolidatedDT.Rows.Add(newdr);
            }
            foreach (string lg in liveAccessLoadGroups)
            {
                dt = PublishLog.getPublishSummaryData(null, sp, loadNumber + 1, lg);
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow newdr = consolidatedDT.NewRow();
                    newdr["DataSource"] = dr["DataSource"];
                    newdr["Processed"] = dr["Processed"];
                    newdr["Warnings"] = dr["Warnings"];
                    consolidatedDT.Rows.Add(newdr);
                }
            }

            if (consolidatedDT.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table style=\"font-family: Courier; font-size: 12pt\">");
                sb.Append("<tr><td>Data Source</td><td>Rows Processed</td><td>Warnings</td></tr>");
                foreach (DataRow dr in consolidatedDT.Rows)
                {
                    string processed = String.Format("{0:#,###}", (long)dr["Processed"]);
                    string warnings = String.Format("{0:#,###}", (long)dr["Warnings"]);
                    sb.Append("<tr><td>" + dr["DataSource"] + "</td><td align=\"right\">" + processed + "</td><td align=\"right\">" + warnings + "</td></tr>");
                }
                sb.Append("</table>");
                mm.Body = mm.Body.Replace("{RESULTS}", sb.ToString());
            }
            else
            {
                mm.Body = mm.Body.Replace("{RESULTS}", "<p>No data rows processed</p>");
            }

            return mm;
        }

        public static MailMessage fixUpMessageOnFailure(MailMessage mm, bool canpublish, bool isBeingPublished, bool isSpaceBusyInOtherOp, 
            bool dataLimitExceeded, bool rowLimitExceeded, bool generalError, Space sp)
        {
            mm.Subject = "Birst Notification - Unable to Process Data [" + getTruncatedSpaceName(sp) + "]";
            if (!canpublish)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space has not been configured for processing. Please visit the manage data screen to correct.</p>");
            else if (isBeingPublished)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is being processed currently. Please wait till the previous load is processed.</p>");
            else if (isSpaceBusyInOtherOp)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is currently not available for processing. Space is involved in either copying, swap or deletion operation.</p>");
            else if (dataLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Data limit exceeded - please upgrade your account to process more data.</p>");
            else if (rowLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Row limit exceeded - please upgrade your account to increase your limit.</p>");
            else if (generalError)
                mm.Body = mm.Body.Replace("{ERROR}", "<p></p>");
            mm.Body = mm.Body.Replace("{RESULTS}", "");

            return mm;
        }

        public static MailMessage fixUpMessageOnExtractionFailure(MailMessage mm, bool canpublish, bool isBeingPublished, bool isSpaceBusyInOtherOp,
            bool dataLimitExceeded, bool rowLimitExceeded, bool generalError, Space sp)
        {
            mm.Subject = "Birst Notification - Extraction Failed [" + getTruncatedSpaceName(sp) + "]";
            if (!canpublish)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space has not been configured for extraction. Please visit the manage data screen to correct.</p>");
            else if (isBeingPublished)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is being processed currently. Please wait till the previous load is processed.</p>");
            else if (isSpaceBusyInOtherOp)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is currently not available for extraction. Space is involved in either copying, swap or deletion operation.</p>");
            else if (dataLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Data limit exceeded - please upgrade your account to extract more data.</p>");
            else if (rowLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Row limit exceeded - please upgrade your account to increase your limit.</p>");
            else if (generalError)
                mm.Body = mm.Body.Replace("{ERROR}", "<p></p>");
            mm.Body = mm.Body.Replace("{RESULTS}", "");

            return mm;
        }

        public static MailMessage fixUpMessageOnProcessExtractionFailure(MailMessage mm, bool canpublish, bool isBeingPublished, bool isSpaceBusyInOtherOp,
            bool dataLimitExceeded, bool rowLimitExceeded, bool generalError, Space sp)
        {
            mm.Subject = "Birst Notification - Extraction Failed [" + getTruncatedSpaceName(sp) + "]";
            if (!canpublish)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space has not been configured for extraction. Please visit the manage data screen to correct.</p>");
            else if (isBeingPublished)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is being processed currently. Please wait till the previous load is processed.</p>");
            else if (isSpaceBusyInOtherOp)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is currently not available for extraction. Space is involved in either copying, swap or deletion operation.</p>");
            else if (dataLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Data limit exceeded - please upgrade your account to extract more data.</p>");
            else if (rowLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Row limit exceeded - please upgrade your account to increase your limit.</p>");
            else if (generalError)
                mm.Body = mm.Body.Replace("{ERROR}", "<p></p>");
            mm.Body = mm.Body.Replace("{RESULTS}", "");

            return mm;
        }

        public static MailMessage fixUpMessageOnProcessFailure(MailMessage mm, bool canpublish, bool isBeingPublished, bool isSpaceBusyInOtherOp,
            bool dataLimitExceeded, bool rowLimitExceeded, bool generalError, Space sp)
        {
            mm.Subject = "Birst Notification - Data Processing Failure [" + getTruncatedSpaceName(sp) + "]";
            if (!canpublish)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space has not been configured for processing. Please visit the manage data screen to correct.</p>");
            else if (isBeingPublished)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is being processed currently. Please wait till the previous load is processed.</p>");
            else if (isSpaceBusyInOtherOp)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Space is currently not available for processing. Space is involved in either copying, swap or deletion operation.</p>");
            else if (dataLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Data limit exceeded - please upgrade your account to process more data.</p>");
            else if (rowLimitExceeded)
                mm.Body = mm.Body.Replace("{ERROR}", "<p>Row limit exceeded - please upgrade your account to increase your limit.</p>");
            else if (generalError)
                mm.Body = mm.Body.Replace("{ERROR}", "<p></p>");
            mm.Body = mm.Body.Replace("{RESULTS}", "");

            return mm;
        }

        public static void sendEmailMessage(MailMessage mm, bool useSSL)
        {
            SmtpClient smtpc = new SmtpClient();
                if (useSSL)
                    smtpc.EnableSsl = true;
                smtpc.Send(mm);
        }
    }
}
