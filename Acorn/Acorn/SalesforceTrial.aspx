﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesforceTrial.aspx.cs"
    Inherits="Acorn.SalesforceTrial" Title="Birst" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Birst is the leading on-demand business intelligence solution" />
    <link rel="stylesheet" type="text/css" href="common/css/styles.css" />
    <!--[if gt IE 6]>
<link rel="stylesheet" type="text/css" href="common/css/ie7only.css" />
<![endif]-->
<script language=javascript>
<!--
if (top.frames.length != 0)
  top.location=self.document.location;
// -->
</script>
    <title>Birst - On-Demand Business Intelligence that's easy and affordable</title>
</head>
<style type="text/css">
#loginWrapBigger			{ width: 500px; margin-left: 30px; }
#loginWrapBigger .loginBigger		{ float: left; padding-bottom: 2px; }
#loginWrapBigger .loginFillBigger	{ width: 500px; float: left; background: url(/common/css/img/loginBoxBigger.jpg) no-repeat; }
#loginWrapBigger .loginFillBigger table	{ margin: 10px 0 10px 5px; }
#loginWrapBigger .loginFillBigger td	{ padding: 5px 10px; }
#loginWrapBigger .loginFillBigger input	{ width: 265px; height: 20px; }
#loginWrapBigger .loginFillBigger .btn	{ width: auto; height: auto; border: 0; }
</style>
<body>
    <noscript><p class="nonjsAlert">NOTE: Birst requires Javascript and cookies. You can enable both via your browser's preference settings.</p></noscript>
    <form id="Form1" runat="server">
    <div id="container">
        <div class="top">
        </div>
        <div class="wrapper">
            <div class="margins">
                <div id="header">
                    <div class="logo">
                        <a href="http://www.birst.com/index.shtml">
                            <img src="images/logo-birst.png" width="149" height="35" border="0" alt="Birst" /></a></div>
                </div>
                <div class="cleaner">
                </div>
            </div>
            <div id="contentWrap">
                <div id="leftCol">
                </div>
                <div id="centerCol">
                    <div class="headline">
                        <h1>
                            Salesforce.com Pipeline Stress Test</h1>
                        <h2>
                            Welcome to the Birst Pipeline Stress Test Free Trial for Salesforce.com</h2>
                        <h3>
                            </h3>
                    </div>
                    <div class="content">
                        <div id="loginWrapBigger">
                            <div class="loginBigger">
                            <div class="loginFillBigger">
        <div style="height: 20px">
            &nbsp;</div>
        <div style="font-weight:bold;padding-left:10px">
            Prerequisites
        </div>
        <div style="height: 10px">
            &nbsp;</div>
        <div style="padding-left: 10px;padding-right: 10px">
            The free Pipeline Stress Test leverages the default opportunity object fields. If you have made major configuration changes to your Salesforce.com instance (for example, you are not using the default opportunity amount field or have recently changed opportunity stages), please <a href="mailto:sales@birst.com">contact</a> Birst to obtain an account where such modifications can be accommodated.
        </div>
        <div style="height: 10px">
            &nbsp;</div>
        <div style="padding-left: 10px;padding-right: 10px">
            Birst works by accessing Salesforce.com's web API. If you already have a security token, simply enter your Salesforce.com username and password plus security token combination below and click on 'Next' to create a Birst login.
            Alternatively, you can grant Birst permission to access your data via the API. In that case, you will need administration privileges on your account and need to follow the steps below:             
        </div>
        <div style="height: 10px">
            &nbsp;</div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 1 - Log into your Salesforce.com account and navigate to Setup
        </div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 2 - Expand 'Security Controls' under 'Administration Setup' and open the 'Network Access' link
        </div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 3 - Click on the New button to add a new allowed IP address
        </div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 4 - Enter 174.143.72.147 in the 'Start IP Address' field and 174.143.72.158 in the 'End IP Address' field
        </div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 5 - Click on the Save button
        </div>
        <div style="padding-left:20px;padding-right: 10px">
            Step 6 - Finally, enter your Salesforce.com username and password below and click 'Next' to create a Birst login
        </div>
        <div style="height: 30px">
            &nbsp;</div>
        <div style="font-weight:bold;padding-left:10px">
            Please enter your Salesforce.com credentials
        </div>
        <div style="height: 10px">
            &nbsp;</div>
        <asp:Label ID="LoginError" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        <table>
            <tr>
                <td>
                    Username:
                </td>
                <td>
                    <asp:TextBox ID="UsernameBox" MaxLength="64" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:TextBox ID="PasswordBox" MaxLength="64" runat="server" Width="200px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div style="height: 20px">
            &nbsp;</div>
        <asp:ImageButton ID="CredentialsNextButton" runat="server" 
            ImageUrl="~/Images/Next.png" onclick="CredentialsNextButton_Click" Width="63px" />

                                </div>
                            </div>
                            <div class="cleaner">
                                <br />
                            </div>
                            <table border="0" align="center" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="center" valign="top" width="126">

                                        <script src="https://seal.verisign.com/getseal?host_name=app.birst.com&size=M&use_flash=NO&use_transparent=NO&lang=en"></script>

                                        <br />
                                        <a href="http://www.verisign.com/ssl-certificate/" target="_blank" style="color: #000000;
                                            text-decoration: none; font: bold 7px verdana,sans-serif; letter-spacing: .5px;
                                            text-align: center; margin: 0px; padding: 0px;">ABOUT SSL CERTIFICATES</a>
                                    </td>
                                    <td align="center" valign="top" width="122">
                                        <a href="https://www.securitymetrics.com/site_certificate.adp?s=app.birst.com&i=190634"
                                            target="_blank">
                                            <img src="https://www.securitymetrics.com/images/sm_tested3.gif" alt="SecurityMetrics Certified"
                                                border="0"></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="cleaner">
                        </div>
                    </div>                     
                </div>
                <div id="rightCol">
                </div>                
            </div>
                        <div class="cleaner">
                        </div>            
            <div class="margins">
        <div style="background-image: url(Images/FooterBackground.png); height: 57px;">
            <table width="100%" cellpadding="0" cellspacing="0" style="padding-top: 10px">
            <tr>
            <td colspan="7">&nbsp;</td>
            </tr>
                <tr>
                    <td style="color: #555555; text-align: right">
                        <asp:Literal ID="Copyright" runat="server" 
                            meta:resourcekey="CopyrightResource1"></asp:Literal>
                        &nbsp;
                        <asp:Literal ID="Version" runat="server" meta:resourcekey="VersionResource1"></asp:Literal>
                        &nbsp;
                    </td>
                    <td style="color: #BFBFBF">
                        |
                    </td>
                    <td style="width: 125px">
                        <asp:HyperLink ID='TermsOfServiceLink' runat='server' NavigateUrl="http://www.birst.com/terms-of-service" Target="_blank" ForeColor="#006FAA" Font-Overline="false">
                            Terms of Service
                        </asp:HyperLink>
                    </td>
                    <td style="color: #BFBFBF">
                        |
                    </td>
                    <td style="width: 100px">
                        <asp:HyperLink ID='PrivacyPolicyLink' runat='server' NavigateUrl="http://www.birst.com/privacy-policy" Target="_blank" ForeColor="#006FAA" Font-Overline="false">
                            Privacy Policy
                        </asp:HyperLink>
                    </td>
                    <td style="color: #BFBFBF">
                        |
                    </td>
                    <td style="width: 100px">
                        <asp:HyperLink ID='ContactLink' runat='server' NavigateUrl="http://www.birst.com/company/contact" Target="_blank" ForeColor="#006FAA" Font-Overline="false">
                            Contact Us
                        </asp:HyperLink>
                    </td>
                </tr>
            </table>
        </div>                
            </div>
        </div>
    </div>
    </form>
</body>
</html>
