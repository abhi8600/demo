using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using Acorn.Exceptions;
using Acorn.Background;
using Performance_Optimizer_Administration;
using System.Data;
using System.Net;

namespace Acorn.Utils
{
    public class ConnectorUtils
    {
        public static readonly string CONNECTOR_EXTRACT_FILE_SUFFIX = "_extract.lock";
        public static readonly string CONNECTOR_SFDC = "sfdc";
        public static readonly string CONNECTOR_NETSUITE = "netsuite";
        public static readonly string CONNECTOR_OLD_SFDC = "sforce.xml";
        private static readonly string BIRST_PARAM_SFDC_CLIENT = "SFDCClientID";
        private static string useNewConnectorFramework = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseNewConnectorFramework"];
        public const string SFDC_CONNECTOR = "sfdc";
        private const string NETSUITE_CONNECTOR = "netsuite";
        private static int getConnectorVariablesTimeOutSeconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesTimeOutSeconds"] == null ? 0 : int.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesTimeOutSeconds"]);
        private static int getConnectorVariablesMaxRetryCount = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesMaxRetryCount"] == null ? 0 : int.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesMaxRetryCount"]);
        private static int getConnectorVariablesRetryIntervalSeconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesRetryIntervalSeconds"] == null ? 0 : int.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GetConnectorVariablesRetryIntervalSeconds"]);

        public static bool UseNewConnectorFramework()
        {
            return useNewConnectorFramework != null && bool.Parse(useNewConnectorFramework);
        }

        private static string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
        
        private static TrustedService.TrustedService getTrustedServiceInstance(Space sp)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            return ts;
        }

        public static Dictionary<string, string> getConnectorSettingsFiles(Space sp)
        {
            Dictionary<string, string> connectorToSettings = new Dictionary<string,string>();
            string connDir = getConnectorsDirectoryName(sp);
            if (Directory.Exists(connDir))
            {
                string[] connectorConfigs = Directory.GetFiles(connDir, "*_config.xml");
                if (connectorConfigs != null)
                {
                    foreach (string configFile in connectorConfigs)
                    {
                        FileInfo fi = new FileInfo(configFile);
                        string fileName = fi.Name;
                        string connName = fileName.Substring(0, (fileName.Length - "_config.xml".Length));
                        connectorToSettings.Add(connName, fi.Name);
                    }
                }
            }
            return connectorToSettings;
        }

        public static Dictionary<string, string> connectorNameToSettingsFileNames = new Dictionary<string, string>(){
            {"sfdc", "sfdc_config.xml"},
            {"netsuite", "netsuite_config.xml"},
            {"marketo", "marketo_config.xml"}
        };

        public static HashSet<string> retrieveSupportedScheduleTypes(HttpSessionState session, User user, Space sp)
        {
            HashSet<string> scheduleTypes = new HashSet<string>();
            foreach (Connector conn in getConfiguredConnectors(session, user, sp))
            {
                scheduleTypes.Add(conn.ConnectorName + " Connector");
            }
            return scheduleTypes;
        }

        public static List<Connector> getConfiguredConnectors(HttpSessionState session, User user, Space sp)
        {
            List<Connector> connectorsList = new List<Connector>();
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorsList(user.Username, sp.ID.ToString());
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            XElement[] connectorElements = WebServiceResponseXml.getElements(response.getRoot(), "Connector");
            Dictionary<string, List<string>> connectorAvailableAPIVersions = new Dictionary<string, List<string>>();
            if (connectorElements != null)
            {
                for (int i = 0; i < connectorElements.Length; i++)
                {
                    Connector conn = new Connector();
                    conn.ConnectorName = WebServiceResponseXml.getFirstElementValue(connectorElements[i], "ConnectorName");
                    bool foundConnector = false;
                    foreach (Connector connector in connectorsList)
                    {
                        if (connector.ConnectorName.Equals(conn.ConnectorName))
                        {
                            foundConnector = true;
                            break;
                        }
                    }
                    if (foundConnector)
                    {
                        continue;
                    }
                    conn.ConnectorAPIVersion = WebServiceResponseXml.getFirstElementValue(connectorElements[i], "ConnectorVersion");
                    bool isDefault = false;
                    bool.TryParse(WebServiceResponseXml.getFirstElementValue(connectorElements[i], "IsDefaultAPIVersion"), out isDefault);
                    conn.IsDefaultAPIVersion = isDefault;
                    conn.ConnectionString = WebServiceResponseXml.getFirstElementValue(connectorElements[i], "ConnectorConnectionString");
                    string filename = getConnectorConfigFileName(getConnectorNameFromConnectorConnectionString(conn.ConnectionString), sp);
                    if (File.Exists(filename))
                    {
                        connectorsList.Add(conn);
                    }
                }
            }
            return connectorsList;
        }

        public static XElement getAllExtractAndProcessingGroups(HttpSessionState session, User user, Space sp)
        {
            XElement[] groupsXML = new XElement[2];
            if (UseNewConnectorFramework())
            {
                List<Connector> configuredConnectors = getConfiguredConnectors(session, user, sp);
                groupsXML[0] = new XElement("ExtractGroupsXml");
                foreach (Connector conn in configuredConnectors)
                {
                    Dictionary<string, List<string>> egToObjectsMap = new Dictionary<string, List<string>>();
                    egToObjectsMap.Add("All", new List<string>());
                    XElement connExtractGroupsEl = new XElement("ConnectorExtractGroups");
                    XElement connNameEl = new XElement("ConnectorName");
                    connNameEl.Add(conn.ConnectorName);
                    connExtractGroupsEl.Add(connNameEl);
                    string filename = getConnectorConfigFileName(getConnectorNameFromConnectorConnectionString(conn.ConnectionString), sp);
                    if (File.Exists(filename))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.XmlResolver = null;
                        doc.Load(filename);
                        foreach (XmlNode configNode in doc.ChildNodes)
                        {
                            if (configNode.Name == "ConnectorConfig")
                            {
                                foreach (XmlNode exObjsNode in configNode.ChildNodes)
                                {
                                    if (exObjsNode.Name == "ExtractionObjects")
                                    {
                                        foreach (XmlNode exObjNode in exObjsNode.ChildNodes)
                                        {
                                            if (exObjNode.Name == "ExtractionObject")
                                            {
                                                string objName = null;
                                                string objExtractGroups = null;
                                                foreach (XmlNode exObjNameNode in exObjNode.ChildNodes)
                                                {
                                                    if (exObjNameNode.Name == "Name")
                                                    {
                                                        objName = exObjNameNode.InnerText;
                                                        break;
                                                    }
                                                }
                                                egToObjectsMap["All"].Add(objName);
                                                foreach (XmlNode exObjNameNode in exObjNode.ChildNodes)
                                                {
                                                    if (exObjNameNode.Name == "ExtractionGroupNames")
                                                    {
                                                        objExtractGroups = exObjNameNode.InnerText;
                                                        if (objExtractGroups != null && objExtractGroups.Trim().Length == 0)
                                                            objExtractGroups = null;
                                                        break;
                                                    }
                                                }
                                                if (objExtractGroups != null)
                                                {
                                                    string[] extGroups = objExtractGroups.Split(',');
                                                    if (extGroups != null)
                                                    {
                                                        foreach (string eg in extGroups)
                                                        {
                                                            if (!egToObjectsMap.ContainsKey(eg))
                                                                egToObjectsMap.Add(eg, new List<string>());
                                                            egToObjectsMap[eg].Add(objName);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    List<string> egNames = new List<string>(egToObjectsMap.Keys);
                    egNames.Remove("All");
                    egNames.Sort();
                    egNames.Insert(0, "All");
                    XElement egroupsElement = new XElement("ExtractGroups");
                    foreach (string egName in egNames)
                    {
                        XElement egroupElement = new XElement("ExtractGroup");
                        XElement egNameElement = new XElement("Name");
                        egNameElement.Add(egName);
                        egroupElement.Add(egNameElement);
                        XElement egObjectsElement = new XElement("Objects");
                        foreach (string obj in egToObjectsMap[egName])
                        {
                            XElement egObjectElement = new XElement("Object");
                            egObjectElement.Add(obj);
                            egObjectsElement.Add(egObjectElement);
                        }
                        egroupElement.Add(egObjectsElement);
                        egroupsElement.Add(egroupElement);
                    }
                    connExtractGroupsEl.Add(egroupsElement);
                    groupsXML[0].Add(connExtractGroupsEl);
                }

                groupsXML[1] = new XElement("ProcessingGroupsXml");
                Dictionary<string, List<string>> pgToSourcesMap = new Dictionary<string, List<string>>();
                pgToSourcesMap.Add("All", new List<string>());
                MainAdminForm maf = Util.getSessionMAF(session);
                HashSet<string> scriptedSources = new HashSet<string>();
                if (maf != null)
                {
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        if (st.Disabled || st.DiscoveryTable || st.LiveAccess)
                            continue;
                        string sourceName = (st.Name.StartsWith("ST_") ? st.Name.Substring("ST_".Length) : st.Name);
                        if (st.Script != null && st.Script.Script != null && st.Script.InputQuery != null && st.Script.Output != null)
                            scriptedSources.Add(sourceName);
                        pgToSourcesMap["All"].Add(sourceName);
                        if (st.SubGroups != null)
                        {
                            foreach (string sg in st.SubGroups)
                            {
                                if (!pgToSourcesMap.ContainsKey(sg))
                                    pgToSourcesMap.Add(sg, new List<string>());
                                pgToSourcesMap[sg].Add(sourceName);
                            }
                        }
                    }
                }
                List<string> pgNames = new List<string>(pgToSourcesMap.Keys);
                pgNames.Remove("All");
                pgNames.Sort();
                pgNames.Insert(0, "All");
                foreach (string pgName in pgNames)
                {
                    XElement pgElement = new XElement("ProcessingGroup");
                    XElement pgNameElement = new XElement("Name");
                    pgNameElement.Add(pgName);
                    pgElement.Add(pgNameElement);
                    XElement pgSourcesElement = new XElement("Sources");
                    foreach (string sourceName in pgToSourcesMap[pgName])
                    {
                        XElement pgSourceElement = new XElement("Source");
                        XElement sourceNameElement = new XElement("Name");
                        sourceNameElement.Add(sourceName);
                        pgSourceElement.Add(sourceNameElement);
                        XElement sourceScriptedElement = new XElement("IsScripted");
                        sourceScriptedElement.Add(scriptedSources.Contains(sourceName).ToString());
                        pgSourceElement.Add(sourceScriptedElement);
                        pgSourcesElement.Add(pgSourceElement);
                    }
                    pgElement.Add(pgSourcesElement);
                    groupsXML[1].Add(pgElement);
                }
            }

            XElement responseXml =
                    new XElement("BirstWebServiceResult",
                        new XElement("Result",
                            groupsXML));

            return responseXml;
        }
      
        public static XElement getListofConnectors(HttpSessionState session, User user, Space sp)
        {
            if (UseNewConnectorFramework())
            {
                TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
                XmlNode[] obj = (XmlNode[])ts.getConnectorsList(user.Username, sp.ID.ToString());
                WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
                XElement[] connectorsList = WebServiceResponseXml.getElements(response.getRoot(), "Connector");

                createConnectorsDirectory(sp);

                string defaultSelectedConnectorName = null;
                string defaultSelectedAPIVersion = null;
                Dictionary<string, List<string>> connectorAvailableAPIVersions = new Dictionary<string, List<string>>();
                Dictionary<string, string> connectorDefaultAPIVersions = new Dictionary<string, string>();
                Dictionary<string, string> connectorSelectedAPIVersion = new Dictionary<string, string>();
                XElement connectorList = new XElement("ConnectorsList");
                connectorList.Add(new XElement("UseNewFramework", "true"));
                List<Connector> connectors = new List<Connector>();
                for (int i = 0; i < connectorsList.Length; i++)
                {
                    string connName = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "ConnectorName");
                    string connAPIVersion = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "ConnectorVersion");
                    string isDefaultAPIVersion = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "IsDefaultAPIVersion");
                    string connConnectionString = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "ConnectorConnectionString");

                    bool isDefault = false;
                    bool.TryParse(isDefaultAPIVersion, out isDefault);
                    if (user.isFreeTrialUser)
                    {
                        if (!(connName == "Salesforce" && isDefault))
                        {
                            //only show default Salesforce connector to Free Trial Users
                            continue;
                        }
                    }
                    if (isDefault)
                    {
                        connectorDefaultAPIVersions.Add(connName, connAPIVersion);
                    }
                    if (!connectorAvailableAPIVersions.ContainsKey(connName))
                        connectorAvailableAPIVersions.Add(connName, new List<string>());
                    connectorAvailableAPIVersions[connName].Add(connAPIVersion);

                    if (!connectorSelectedAPIVersion.ContainsKey(connName))
                    {
                        // see if settings file is there
                        string filename = getConnectorConfigFileName(getConnectorNameFromConnectorConnectionString(connConnectionString), sp);
                        if (File.Exists(filename))
                        {
                            if (defaultSelectedConnectorName == null)
                                defaultSelectedConnectorName = connName;
                            XmlDocument doc = new XmlDocument();
                            doc.XmlResolver = null;
                            doc.Load(filename);
                            foreach (XmlNode cnode in doc.ChildNodes)
                            {
                                if (cnode.Name == "ConnectorConfig")
                                {
                                    foreach (XmlNode chNode in cnode.ChildNodes)
                                    {
                                        if (chNode.Name == "ConnectorAPIVersion")
                                        {
                                            string connectionString = chNode.InnerText;
                                            if (connectionString != null && connectionString.Trim().Length > 0)
                                            {
                                                string connectorVersion = connectionString.Trim().Substring(connectionString.LastIndexOf(":") + 1);
                                                connectorSelectedAPIVersion.Add(connName, connectorVersion);
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }                            
                        }                        
                    }
                    if (isDefault && !connectorSelectedAPIVersion.ContainsKey(connName))
                    {
                        connectorSelectedAPIVersion.Add(connName, connAPIVersion);
                    }

                    Connector connector = new Connector();
                    connector.ConnectorName = connName;
                    connector.ConnectorAPIVersion = connAPIVersion;
                    connector.IsDefaultAPIVersion = isDefault;
                    connector.ConnectionString = connConnectionString;
                    connectors.Add(connector);                                        
                }
                //check if any of the selected api version is deprecated, if so, use default api version
                Dictionary<string, string> resetSelectedAPIVersions = new Dictionary<string, string>();
                foreach (string connectorType in connectorSelectedAPIVersion.Keys)
                {
                    if (!connectorAvailableAPIVersions[connectorType].Contains(connectorSelectedAPIVersion[connectorType]))
                    {
                        resetSelectedAPIVersions.Add(connectorType, connectorSelectedAPIVersion[connectorType]);                        
                    }
                }
                foreach (string connectorType in resetSelectedAPIVersions.Keys)
                {
                    connectorSelectedAPIVersion[connectorType] = connectorDefaultAPIVersions[connectorType];
                }

                // If a particular connector is being extracted, use that connector
                foreach (string conn in connectorAvailableAPIVersions.Keys)
                {
                    if (isConnectorSpecificExtractFilePresent(sp, conn))
                    {
                        defaultSelectedConnectorName = conn;
                        break;
                    }
                }

                if (defaultSelectedConnectorName == null)
                {
                    defaultSelectedConnectorName = "Salesforce";                    
                }
                if (defaultSelectedAPIVersion == null)
                {
                    if (connectorSelectedAPIVersion.ContainsKey(defaultSelectedConnectorName))
                        defaultSelectedAPIVersion = connectorSelectedAPIVersion[defaultSelectedConnectorName];
                    else
                        defaultSelectedAPIVersion = connectorDefaultAPIVersions[defaultSelectedConnectorName];
                }

                HashSet<string> reportedNotAvailableConnector = new HashSet<string>();
                foreach (Connector conn in connectors)
                {
                    bool isSelectedAPIVersion = false;
                    if (conn.ConnectorAPIVersion == connectorSelectedAPIVersion[conn.ConnectorName])
                    {
                        isSelectedAPIVersion = true;
                    }
                    connectorList.Add(new XElement("ConnectorName", conn.ConnectorName));
                    connectorList.Add(new XElement("ConnectorAPIVersion", conn.ConnectorAPIVersion));
                    connectorList.Add(new XElement("IsDefaultAPIVersion", conn.IsDefaultAPIVersion.ToString()));
                    connectorList.Add(new XElement("IsSelectedAPIVersion", isSelectedAPIVersion.ToString()));
                    connectorList.Add(new XElement("ConnectionString", conn.ConnectionString));
                    if (resetSelectedAPIVersions.ContainsKey(conn.ConnectorName) && !reportedNotAvailableConnector.Contains(conn.ConnectorName))
                    {
                        reportedNotAvailableConnector.Add(conn.ConnectorName);
                        connectorList.Add(new XElement("SelectedConnectorNotAvailable", conn.ConnectorName));
                        connectorList.Add(new XElement("SelectedVersionNotAvailable", resetSelectedAPIVersions[conn.ConnectorName]));                        
                    }
                }

                connectorList.Add(new XElement("SelectedConnector", defaultSelectedConnectorName));
                connectorList.Add(new XElement("SelectedAPIVersion", defaultSelectedAPIVersion));
                XElement responseXml =
                    new XElement("BirstWebServiceResult",
                        new XElement("Result",
                            connectorList));

                return responseXml;
            }
            else
            {
                List<string> connectorsList = new List<string>();
                connectorsList.Add(CONNECTOR_SFDC);
                connectorsList.Add(CONNECTOR_NETSUITE);
                // add more connectors
                connectorsList.Sort();

                createConnectorsDirectory(sp);
                string defaultSelected =null;
                XElement connectorList = new XElement("ConnectorsList");
                foreach (string connName in connectorsList)
                {
                    if (defaultSelected == null)
                    {
                        // see if settings file is there
                        string filename = Path.Combine(getConnectorsDirectoryName(sp),connectorNameToSettingsFileNames[connName]);
                        if (File.Exists(filename))
                        {
                            defaultSelected = connName;
                        }
                    }
                    connectorList.Add(new XElement("name", connName));
                }
                if(defaultSelected == null)
                {
                    defaultSelected = CONNECTOR_SFDC;
                }
            
                // add the logic. If a particular connector is being extracted, use that connector
                connectorList.Add(new XElement("Selected", defaultSelected));
                XElement responseXml =
                    new XElement("BirstWebServiceResult",
                        new XElement("Result",
                            connectorList));

                return responseXml;
            }
        }

        public static XElement getCloudConnectorConfigXML(HttpSessionState session, Space sp)
        {
            TrustedService.TrustedService ts =  getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getCloudConnectorConfigXML(Util.getSessionUser(session).Username, sp.ID.ToString(), sp.Directory);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            XElement cloudConnectorsElement = WebServiceResponseXml.getFirstElement(response.getRoot(), "CloudConnectors");
            XElement responseXml =
                    new XElement("BirstWebServiceResult",
                        new XElement("Result",
                            cloudConnectorsElement));
            return responseXml;
        }

        public static string findConnectorAPIVersionInConfig(string connectorName, Space sp)
        {
            string connectionString = null;
            string filename = getConnectorConfigFileName(connectorName, sp);
            if (File.Exists(filename))
            {
                XmlDocument doc = new XmlDocument();
                doc.XmlResolver = null;
                doc.Load(filename);
                foreach (XmlNode cnode in doc.ChildNodes)
                {
                    if (cnode.Name == "ConnectorConfig")
                    {
                        foreach (XmlNode chNode in cnode.ChildNodes)
                        {
                            if (chNode.Name == "ConnectorAPIVersion")
                            {
                                connectionString = chNode.InnerText;                                
                            }
                        }
                        break;
                    }
                }
            }
            return connectionString;
        }

        public class Connector
        {
            public string ConnectorName { get; set; }
            public string ConnectorAPIVersion { get; set; }
            public bool IsDefaultAPIVersion { get; set;}
            public bool IsSelectedAPIVersion { get; set; }
            public string ConnectionString { get; set; }
        }

        private static string getConnectorConfigFileName(string connName, Space sp)
        {
            string connectorConfigFileName = connName.ToLower();
            if (connectorConfigFileName == "salesforce")
            {
                connectorConfigFileName = CONNECTOR_SFDC;
            }
            connectorConfigFileName = connectorConfigFileName + "_config.xml";
            return Path.Combine(getConnectorsDirectoryName(sp), connectorConfigFileName);            
        }

        public static XElement getConnectorCredentials(HttpSessionState session, User user, Space sp, string connectorName)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode [])ts.getConnectorCredentials(user.Username, sp.ID.ToString(), sp.Directory, connectorName);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getSelectedObjects(HttpSessionState session, User user, Space sp, string connectorName)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getSelectedConnectorObjects(user.Username, sp.ID.ToString(), sp.Directory, connectorName);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getAllObjects(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getAllConnectorObjects(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getAllObjectsForCloudConnection(HttpSessionState session, User user, Space sp, string connectionID, string xmlData)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getAllObjectsForCloudConnection(user.Username, sp.ID.ToString(), sp.Directory, connectionID, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }
        public static XElement saveConnectorCredentials(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.saveConnectorCredentials(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement testConnection(HttpSessionState session, User user, Space sp, string connectorConnectionString, string credentialsXML)
        {
            credentialsXML = replaceConfigVariables(credentialsXML, connectorConnectionString);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.testConnection(user.Username, sp.ID.ToString(), sp.Directory, connectorConnectionString, credentialsXML);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement saveConnection(HttpSessionState session, User user, Space sp, string connectorConnectionString, string connectionDetailsXML)
        {
            connectionDetailsXML = replaceConfigVariables(connectionDetailsXML, connectorConnectionString);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.saveConnection(user.Username, sp.ID.ToString(), sp.Directory, connectorConnectionString, connectionDetailsXML);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getObjectQuery(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorObjectQuery(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getObject(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorObject(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getConnectorDynamicParameters(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorDynamicParameters(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement validateObjectQuery(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.validateConnectorObjectQuery(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement validateObject(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.validateConnectorObject(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getObjectDetails(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorObjectDetails(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getSavedObjectDetails(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getSavedObjectDetails(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement saveQueryObject(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.saveConnectorQueryObject(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement saveObject(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.saveConnectorObject(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement getMetaData(HttpSessionState session, User user, Space sp, string connectorName)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorMetaData(user.Username, sp.ID.ToString(), sp.Directory, connectorName);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }
        
        public static string getConnectorVariables(HttpSessionState session, String path, User user, Space sp, String connectionString, String extractGroups)
        {
            return getConnectorVariables(session, path, user, sp, connectionString, extractGroups, 0);
        }
        
        private static string getConnectorVariables(HttpSessionState session, String path, User user, Space sp, String connectionString, String extractGroups, int retryCnt)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            if (getConnectorVariablesTimeOutSeconds > 0)
            {
                ts.Timeout = getConnectorVariablesTimeOutSeconds * 1000;
            }
            string obj = "";
            try
            {
                obj = ts.getConnectorVariables(path, user.Username, sp.ID.ToString(), connectionString, extractGroups);
            }
            catch (WebException ex)
            {
                if (getConnectorVariablesMaxRetryCount == 0 || retryCnt >= getConnectorVariablesMaxRetryCount || ex.Status != WebExceptionStatus.Timeout)
                {
                    throw new WebException(ex.Message);
                }
                else
                {
                    retryCnt += 1;
                    System.Threading.Thread.Sleep(getConnectorVariablesRetryIntervalSeconds * retryCnt * 1000);
                    Global.systemLog.Warn("Exception while getConnectorVariables \"" + ex.Message + "\" for space " + sp.ID.ToString());
                    Global.systemLog.Info("Retrying getConnectorVariables operation. Current retry #" + retryCnt + " and max retry count #" + getConnectorVariablesMaxRetryCount);
                    return getConnectorVariables(session, path, user, sp, connectionString, extractGroups, retryCnt);
                }
            }
            
            return obj;
        }

        public static XElement saveSelectedObjects(HttpSessionState session, User user, Space sp, string connectorName, string xmlData)
        {
            xmlData = replaceConfigVariables(xmlData, connectorName);
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.saveSelectedObjects(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement updateSelectedConnectorObjects(HttpSessionState session, User user, Space sp, string connectionName, string connectorType, string xmlData)
        {
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.updateSelectedObjects(user.Username, sp.ID.ToString(), sp.Directory, connectionName, connectorType, xmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        public static XElement startExtract(HttpSessionState session, User user, Space sp, string connectorName, string xmlData, string[] extractGroups)
        {   
            if (Util.isLoadLockFilePresent(sp))
            {
                Global.systemLog.Error("Cannot start extract : Load Lock file is present for space : " + (sp != null ? sp.ID.ToString() : ""));
                throw new ConnectorException(BirstException.ERROR_CONNECTOR_EXTRACT_LOAD_IN_PROGRESS);
            }

            if (Util.checkForPublishingStatus(sp))
            {
                Global.systemLog.Error("Cannot extract : Space loading in progress : " + (sp != null ? sp.ID.ToString() : ""));
                throw new ConnectorException(BirstException.ERROR_CONNECTOR_EXTRACT_LOAD_IN_PROGRESS);
            }

            if (Util.isSFDCExtractRunning(sp))
            {
                Global.systemLog.Error("Cannot extract : Another extraction in progress : " + (sp != null ? sp.ID.ToString() : ""));
                throw new ConnectorException(BirstException.ERROR_CONNECTOR_EXTRACT_ANOTHER_IN_PROGRESS);
            }

            xmlData = replaceConfigVariables(xmlData, connectorName);
            if (!Util.useExternalSchedulerForManualExtract(sp))
            {
                startExtractWithoutScheduler(session, user, sp, null, connectorName, xmlData, extractGroups);
                return null;
            }

            if (UseNewConnectorFramework())
            {
                XmlDocument doc = new XmlDocument();
                doc.XmlResolver = null;
                doc.LoadXml(xmlData);
                foreach (XmlNode node in doc.FirstChild.ChildNodes)
                {
                    if (node.Name.Equals("ConnectorType"))
                    {
                        node.InnerText = getConnectorNameFromConnectorConnectionString(connectorName) + "_connector";
                        break;
                    }
                }
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                StringWriter sw = new StringWriter(sb);
                doc.Save(sw);
                sw.Flush();
                xmlData = sb.ToString();
            }
            return SchedulerUtils.runNowScheduleExtract(sp.ID, user.ID, xmlData, extractGroups);
        }

        public static string getConnectorNameFromConnectorConnectionString(string connectionString)
        {
            string connName = connectionString.Substring("connector:birst://".Length);
            if (connName.IndexOf(":") >= 0)
                connName = connName.Substring(0, connName.IndexOf(":"));
            return connName;
        }

        public static void startExtractWithoutScheduler(HttpSessionState session, User user, Space sp, MainAdminForm maf, string connectorName, string xmlData, string[] extractGroups)
        {
            string connectorNameFromConnectionString = connectorName;
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorNameFromConnectionString = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            createExtractInfoFile(sp, connectorNameFromConnectionString);
            XElement extractGroupsXML = new XElement("ExtractGroups");
            if (extractGroups != null)
            {
                foreach (string eg in extractGroups)
                {
                    extractGroupsXML.Add(new XElement("ExtractGroup", eg));
                }
            }
            // for dev machines
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.startExtract(user.Username, sp.ID.ToString(), sp.Directory, connectorName, xmlData, extractGroupsXML.ToString());
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                XElement resultElem = wsResponseXml.getResult();
                XElement extractRootElement = WebServiceResponseXml.getFirstElement(resultElem, "Extract");
                string extractId = WebServiceResponseXml.getFirstElementValue(extractRootElement, "ExtractId");
                Util.createIfNotExistsSFDCExtractStatusFilePresent(sp);
                ConnectorUtils.createIfNotExistsConnectorSpecifcExtractFile(sp, connectorNameFromConnectionString);
                createExtractInfoFile(sp, connectorNameFromConnectionString);
                ConnectorStatus connectorStatus = new ConnectorStatus(session, extractId, sp, user, connectorNameFromConnectionString, (session != null ? Util.getSessionMAF(session) : maf), xmlData, extractGroups);
                BackgroundProcess.startModule(new BackgroundTask(connectorStatus, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));                
            }
            else
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling startExtractWithoutScheduler - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                throw new BirstException(errorMessage);
            }
        }

        private static void createExtractInfoFile(Space sp, string connectorName)
        {
            try
            {
                string extractInfoFileDirectory = Path.Combine(sp.Directory, "connectors");
                if (!Directory.Exists(extractInfoFileDirectory))
                {
                    Directory.CreateDirectory(extractInfoFileDirectory);
                }
                
                List<string> outputContents = new List<string>();
                outputContents.Add(connectorName + "=" + Guid.NewGuid().ToString());
                string extractInfoFileName = Path.Combine(extractInfoFileDirectory, "extract_info.txt");
                FileInfo fileInfo = new FileInfo(extractInfoFileName);
                if (fileInfo.Exists)
                {
                    string[] allLines = File.ReadAllLines(extractInfoFileName);
                    if (allLines != null && allLines.Length > 0)
                    {
                        foreach (string line in allLines)
                        {
                            if (!line.StartsWith(connectorName))
                            {
                                outputContents.Add(line);
                            }
                        }
                    }
                }
                File.WriteAllLines(extractInfoFileName, outputContents.ToArray());
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error in creating extract info file", ex);
                throw ex;
            }
        }

        public static string getExtractInfoId(Space sp, string connectorName)
        {
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorName = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            bool retry = false;
            int count = 0;
            do
            {
                retry = false;
                try
                {   
                    string extractInfoFileDirectory = Path.Combine(sp.Directory, "connectors");
                    if (Directory.Exists(extractInfoFileDirectory))
                    {
                        string extractInfoFileName = Path.Combine(extractInfoFileDirectory, "extract_info.txt");
                        FileInfo fileInfo = new FileInfo(extractInfoFileName);
                        if (fileInfo.Exists)
                        {
                            string[] allLines = File.ReadAllLines(extractInfoFileName);
                            if (allLines != null && allLines.Length > 0)
                            {
                                foreach (string line in allLines)
                                {
                                    if (line.StartsWith(connectorName))
                                    {
                                        string[] props = line.Split('=');
                                        if (props.Length == 2)
                                        {
                                            return props[1];
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Error in reading extract info file");
                    if (ex is IOException)
                    {
                        count++;
                        retry = count < 5;
                    }
                    else
                    {
                        Global.systemLog.Error("Error in reading extract info file", ex);
                        throw ex;
                    }
                }
            } while (retry);
            return null;
        }

        public static void cancelExtract(HttpSessionState session, User user, Space sp, string connectorName)
        {
            Util.createIfNotExistsSFDCKillLockFile(sp);
        }

        public static XElement getExtractionStatus(HttpSessionState session, User user, Space sp, string connectorName)
        {
            int status = Status.COMPLETE;
            string source = "Extract";
            XElement objectsElem = new XElement("ObjectsExtractionStatus");
            XElement failedMessagesElement = new XElement("FailedMessages");
            string connectorNameFromConnectionString = connectorName;
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorNameFromConnectionString = getConnectorNameFromConnectorConnectionString(connectorName);
            }            
            bool extractRunning = isConnectorSpecificExtractFilePresent(sp, connectorNameFromConnectionString);
            if(extractRunning)
            {
                status = Status.RUNNING;
                
                if (UseNewConnectorFramework())
                {
                    objectsElem.Add(new XElement("ExtractGroup", getExtractGroupName(sp, connectorNameFromConnectionString)));
                }

                // get the objects status that are in progress
                List<ObjectExtractionStatus> objectsStatuses = getObjectsStatus(sp, connectorNameFromConnectionString);
                if (objectsStatuses != null && objectsStatuses.Count > 0)
                {
                    foreach (ObjectExtractionStatus objectStatus in objectsStatuses)
                    {
                        if (objectStatus.Status == Status.RUNNING)
                        {
                            XElement objectElement = new XElement("ObjectExtractionStatus",
                                new XElement("ObjectName", objectStatus.ObjectName),
                                new XElement("Count", objectStatus.Count));
                            objectsElem.Add(objectElement);
                        }
                    }
                }
            }
            else
            {
                status = getExtractStatusViaFileName(sp, connectorNameFromConnectionString);
                if (Util.isSpaceEnabledForExtractionAndProcessing(sp))
                {
                    Status.StatusResult statusResult = Status.getStatusUsingExternalScheduler(sp, -1, null, false);
                    if (statusResult.type != Status.StatusResult.TYPE_EXTRACT && Status.isRunningCode(statusResult.code))
                    {
                        source = "Process";
                        status = Status.RUNNING;
                    }
                }
                if (status != Status.RUNNING)
                {
                    //get it from db just to make sure that no processing is running
                    Status.StatusResult sr = Status.getLoadStatus(session, sp);
                    if(Status.isRunningCode(sr.code))
                    {
                        source = "Process";
                        status = Status.RUNNING;
                    }
                }
            }

            if (status == Status.FAILED && source == "Extract")
            {
                // get the objects that failed
                List<ObjectExtractionStatus> objectsStatuses = getObjectsStatus(sp, connectorName);
                if (objectsStatuses != null && objectsStatuses.Count > 0)
                {
                    foreach (ObjectExtractionStatus objectStatus in objectsStatuses)
                    {
                        if (objectStatus.Status == Status.FAILED)
                        {
                            string msg = objectStatus.ObjectName + " Extract Failed";
                            failedMessagesElement.Add(new XElement("Message", msg));
                        }
                    }
                }

                string fileName = getPostExtractOutputFileName(sp, Status.FAILED, getExtractInfoId(sp, connectorName), connectorName);
                if (File.Exists(fileName))
                {
                    string[] failedMsgs = File.ReadAllLines(fileName);
                    if (failedMsgs != null && failedMsgs.Length > 0)
                    {
                        foreach (string failedMsg in failedMsgs)
                        {
                            failedMessagesElement.Add(new XElement("Message", failedMsg));
                        }
                    }
                }
            }

            // if extracting. Get the objects progress
            XElement responseXml =
                new XElement("BirstWebServiceResult", 
                    new XElement("Result", 
                        new XElement("ExtractionStatus",
                                new XElement("Source", source),
                                new XElement("Status", status),
                                new XElement("RunningConnector", connectorNameFromConnectionString),
                                objectsElem,
                                failedMessagesElement)));

            return responseXml;
        }

        public static HashSet<String> getObjectNames(XmlDocument xDocRoot, string[] extractGroups)
        {
            List<ObjectExtractionStatus> response = new List<ObjectExtractionStatus>();
            // just need the keys for the object name
            Dictionary<string, string> objectToMappingDict = getObjectAndMappingsFromConfigFile(xDocRoot, extractGroups);
            HashSet<string> objectNames = new HashSet<string>();
            if (objectToMappingDict != null && objectToMappingDict.Count > 0)
            {
                string sourceFileNamePrefix = getSourceNameFilePrefix(xDocRoot);
                foreach (KeyValuePair<string, string> kvPair in objectToMappingDict)
                {
                    string objectName = kvPair.Key;
                    if (sourceFileNamePrefix != null && sourceFileNamePrefix.Trim().Length > 0)
                    {
                        objectName = sourceFileNamePrefix + "_" + objectName;
                    }
                    objectNames.Add(objectName);
                }
            }
            return objectNames;
        }

        private static string getExtractGroupName(Space sp, string connectorName)
        {
            if (sp == null || connectorName == null)
                return null;
            string configFileDirectory = getConnectorsDirectoryName(sp);
            if (!Directory.Exists(configFileDirectory))
                return null;
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorName = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            string[] extractGroupFiles = Directory.GetFiles(configFileDirectory, connectorName + "-ExtractGroup-*.txt");
            if (extractGroupFiles != null && extractGroupFiles.Length > 0)
            {
                string filename = new FileInfo(extractGroupFiles[0]).Name;
                string extractGroup = filename.Substring((connectorName + "-ExtractGroup-").Length);
                extractGroup = extractGroup.Remove(extractGroup.Length - ".txt".Length);
                return extractGroup;
            }
            return null;
        }

        private static List<ObjectExtractionStatus> getObjectsStatus(Space sp, string connectorName)
        {
            if (sp == null || connectorName == null)
                return null;
            string configFileDirectory = getConnectorsDirectoryName(sp);
            if (!Directory.Exists(configFileDirectory))
                return null;
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorName = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            string configFileName = Path.Combine(configFileDirectory, connectorName + "_config.xml");
            if (!File.Exists(configFileName))
                return null;

            XmlDocument xDocRoot = new XmlDocument();
            xDocRoot.XmlResolver = null;
            xDocRoot.Load(configFileName);

            List<ObjectExtractionStatus> response = new List<ObjectExtractionStatus>();
            // just need the keys for the object name
            Dictionary<string, string> objectToMappingDict = getObjectAndMappingsFromConfigFile(xDocRoot, null);
            HashSet<string> objectNames = getObjectNames(xDocRoot, null);
            
            if (objectNames != null && objectNames.Count > 0)
            {
                foreach(string objectName in objectNames)
                {
                    try
                    {
                        string statusFileName = Path.Combine(configFileDirectory, objectName + "-status.txt");
                        if (File.Exists(statusFileName))
                        {
                            string[] lines = File.ReadAllLines(statusFileName);
                            // should be only 1 line
                            if (lines != null && lines.Length > 0)
                            {
                                ObjectExtractionStatus objectStatus = new ObjectExtractionStatus();
                                string[] columns = lines[0].Split(new char[] { '\t' });
                                if (columns != null && columns.Length > 0)
                                {
                                    int status = -1;
                                    string statusString = columns[0];
                                    if (statusString == "Running")
                                    {
                                        status = Status.RUNNING;
                                    }
                                    else if (statusString == "Complete")
                                    {
                                        status = Status.COMPLETE;
                                    }
                                    else if (statusString == "Failed")
                                    {
                                        status = Status.FAILED;
                                    }
                                    objectStatus.ObjectName = objectName;
                                    objectStatus.Status = status;
                                    int count = 0;
                                    if (columns.Length > 1)
                                    {
                                        int.TryParse(columns[1], out count);
                                    }
                                    objectStatus.Count = count;
                                    response.Add(objectStatus);
                                }
                            }
                        }
                    }catch(Exception ex)
                    {
                        Global.systemLog.Warn("Error while reading object status file " + objectName, ex);
                    }
                }
            }
            return response;

        }

        

        public static string replaceConfigVariables(string xmlData, string connectorName)
        {
            string connName = connectorName;
            if (UseNewConnectorFramework())
            {
                connName = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            Dictionary<string, string> reservedValuesMap = new Dictionary<string, string>();
            string extractionEngine = "{connectorExtractionEngine}";
            string reservedExtractionEngineValue = Util.getDefaultSFDCVersion().ToString();
            if (reservedExtractionEngineValue != null)
            {
                reservedValuesMap.Add(extractionEngine, reservedExtractionEngineValue);
            }

            if (!UseNewConnectorFramework())
            {
                string reservedSFDCSiteName = "{Sandbox}";
                if (xmlData.IndexOf(reservedSFDCSiteName) >= 0)
                {
                    string reservedSFDCSiteValue = null;
                    switch (connName)
                    {
                        case SFDC_CONNECTOR:
                            {
                                reservedSFDCSiteValue = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
                                break;
                            }
                        case NETSUITE_CONNECTOR:
                            {
                                reservedSFDCSiteValue = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["NETSUITE.Sandbox.URL"];
                                break;
                            }
                    }
                    if (reservedSFDCSiteValue != null)
                    {
                        reservedValuesMap.Add(reservedSFDCSiteName, reservedSFDCSiteValue);
                    }
                }

                string reservedBirstParamsName = "{birstParams}";
                string reservedSFDCClientValue = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
                if (reservedSFDCClientValue != null)
                {
                    // at this point only one value. If need to add more, it should be comma separated
                    string reservedBirstParamsValue = BIRST_PARAM_SFDC_CLIENT + "=" + reservedSFDCClientValue;
                    reservedValuesMap.Add(reservedBirstParamsName, reservedBirstParamsValue);
                }
            }

            foreach (KeyValuePair<string, string> kvPair in reservedValuesMap)
            {
                string reservedName = kvPair.Key;
                string reservedValue = kvPair.Value;
                if (xmlData != null && xmlData.IndexOf(reservedName) >= 0)
                {
                    xmlData = xmlData.Replace(reservedName, reservedValue);
                }
            }
            return xmlData;
        }

        public class ConnectorStatus : BackgroundModule
        {
            HttpSessionState session;
            bool processAfter;
            string uid;
            Space sp;
            User u;
            MainAdminForm maf;
            string connectorName;
            string[] extractGroups;

            public ConnectorStatus(HttpSessionState session, string uid, Space sp, User u, string connectorName, MainAdminForm maf, string inputXmlData, string[] extractGroups)
            {
                this.session = session;
                this.uid = uid;
                this.sp = sp;
                this.u = u;
                this.connectorName = connectorName;
                this.maf = maf;
                this.extractGroups = extractGroups;
                parseAndSetProcessAfterFlag(inputXmlData);
            }

            private void parseAndSetProcessAfterFlag(string xmlData)
            {
                if (xmlData != null)
                {
                    WebServiceResponseXml inputXmlRequest = WebServiceResponseXml.parseFromString(xmlData);
                    string processAfterString = WebServiceResponseXml.getFirstElementValue(inputXmlRequest.getRoot(), "ProcessAfter");
                    processAfter = bool.Parse(processAfterString);
                }
                else
                {
                    processAfter = false;
                }
            }

            private bool isExtractSuccessFilePresent()
            {
                return isExtractStatusFilePresent("success-extract-");
            }

            private bool isExtractFailedFilePresent()
            {
                return isExtractStatusFilePresent("failed-extract-");
            }

            private bool isExtractKilledFilePresent()
            {
                return isExtractStatusFilePresent("killed-extract-");
            }

            private bool isExtractStatusFilePresent(string fileNamePart)
            {
                string sLogsDirectory = Path.Combine(sp.Directory, "slogs");
                if (!Directory.Exists(sLogsDirectory))
                    return false;
                string extractSuccessFileName = Path.Combine(sLogsDirectory, fileNamePart + uid + ".txt");
                if (File.Exists(extractSuccessFileName))
                    return true;

                return false;
            }
            public void run()
            {
                DateTime startdate = DateTime.Now.Date;
                try
                {
                    Util.setLogging(u, sp, uid);
                    Util.deleteSFDCKillLockFile(sp);
                    Util.createIfNotExistsSFDCExtractStatusFilePresent(sp);
                    ConnectorUtils.createIfNotExistsConnectorSpecifcExtractFile(sp, connectorName);
                    string extractFileName = Util.getSFDCExtractStatusFileName(sp);
                    string extractInfoId = getExtractInfoId(sp, connectorName);
                    clearEndStatusFile(sp, connectorName, extractInfoId);
                    while (Util.isSFDCExtractStatusFilePresent(sp))  // if it gets deleted by some other operation
                    {
                        System.Threading.Thread.Sleep(5000);
                        if (isExtractSuccessFilePresent())
                        {
                            Global.systemLog.Info("Detected success extraction file for space : " + sp.ID); 
                            break;
                        }
                        if (isExtractKilledFilePresent())
                        {
                            Global.systemLog.Info("Detected killed extraction file for space : " + sp.ID);
                            createEndStatusFile(sp, connectorName, Status.CANCELLED, extractInfoId);
                            return;
                        }
                        if(isExtractFailedFilePresent())
                        {
                            Global.systemLog.Info("Detected failed extraction file for space : " + sp.ID);
                            createEndStatusFile(sp, connectorName, Status.FAILED, extractInfoId);
                            return;
                        }
                    }
                    if (isExtractSuccessFilePresent())
                    {
                        DataTable errorsTable = new DataTable();
                        errorsTable.Columns.Add("Source");
                        errorsTable.Columns.Add("Severity");
                        errorsTable.Columns.Add("Error");
                        try
                        {
                            ConnectorUtils.postConnectorExtractionUploader(sp, maf, u, connectorName, errorsTable, extractGroups);
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("Error while uploading the extracted files : " + sp.ID + " - " + ex.ToString(), ex);
                            string infoId = getExtractInfoId(sp, connectorName);
                            string fileName = getPostExtractOutputFileName(sp, Status.FAILED, infoId, connectorName);
                            File.WriteAllText(fileName, "Error while uploading the extracted files");
                            createEndStatusFile(sp, connectorName, Status.FAILED, extractInfoId);
                            return;
                        }
                        if (!SalesforceLoader.ignoreUploaderErrors(sp, errorsTable))
                        {
                            Global.systemLog.Warn("Error while uploading the extracted files : " + sp.ID);
                            SalesforceLoader.logErrors(errorsTable, sp);
                            writeFailedMessages(sp, connectorName, errorsTable);
                            createEndStatusFile(sp, connectorName, Status.FAILED, extractInfoId);
                            return;
                        }

                        setConnectorExtractTimes(sp, maf, connectorName, true);
                        Util.saveApplication(maf, sp, null, u);
                        createEndStatusFile(sp, connectorName, Status.COMPLETE, extractInfoId);
                        Util.deleteSFDCExtractFile(sp.Directory);
                        deleteConnectorSpecificExtractFile(sp, connectorName);
                        if (processAfter)
                        {
                            if (session != null)
                            {
                                Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Started); 
                                session["status"] = sr;
                            }
                            Global.systemLog.Error("Starting to process after uploading the extracted files : " + sp.ID);
                            string loadGroup = DefaultUserPage.isIndependentMode(sp) ? SalesforceLoader.LOAD_GROUP_NAME : Util.LOAD_GROUP_NAME;
                            int loadNumber = ProcessLoad.getLoadNumber(sp, loadGroup);
                            ProcessData.Process(maf, sp, u, null, null, DateTime.Now, loadGroup, loadNumber, false, null, false, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while extracting " + connectorName + " : spaceID " + sp.ID, ex);                    
                }
                finally
                {
                    try
                    {
                        Util.setLogging(u, sp);
                        Util.deleteSFDCExtractFile(sp.Directory);
                        deleteConnectorSpecificExtractFile(sp, connectorName);
                        writePostExtractLogs(sp, connectorName, uid, startdate);
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Error in deleting extracting file " + connectorName + " : spaceID " + sp.ID, ex2);
                    }
                }
            }

            
            public void finish() { }
            public void kill() { }
            public Status.StatusResult getStatus() { return null; }
            public void addObserver(BackgroundModuleObserver observer, object o){}
        }

        private static void writePostExtractLogs(Space sp, string connectorName, string uid, DateTime startdate)
        {
            try
            {
                string webLogDir = ConnectorLog.getWebLogDir();
                if (webLogDir != null)
                {
                    FileInfo[] files = ConnectorLog.getLogFiles(webLogDir, "birst.*", startdate);
                    ConnectorLog.writePostExtractLogs(sp, files, connectorName, uid);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in writing post extract log file " + connectorName + " : spaceID " + sp.ID, ex);
            }
        }
                    
        private static void clearEndStatusFile(Space sp, string connectorName, string extractInfoId)
        {
            createConnectorsDirectory(sp);
            string[] names = Directory.GetFiles(getConnectorsDirectoryName(sp));
            if (names != null && names.Length > 0)
            {
                foreach (string name in names)
                {
                    if (name.Contains(connectorName+"Status") && !name.Contains(extractInfoId))
                    {
                        File.Delete(name);
                    }
                }
            }
        }

        public static void createEndStatusFile(Space sp, string connectorName, int outputCode, string extractInfoId)
        {
            createConnectorsDirectory(sp);
            FileStream fs = null;
            string fileName = null;
            try
            {
                fileName = getPostExtractOutputFileName(sp, outputCode, extractInfoId, connectorName);
                if (!File.Exists(fileName))
                {
                    fs = File.Create(fileName);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while creating " + fileName, ex);
            }
            finally
            {
                if (fs != null)
                {
                    try
                    {
                        fs.Close();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Error while closing stream for " + fileName, ex2);
                    }
                }
            }
        }

        public static void writeFailedMessages(Space sp, string connectorName, DataTable dt)
        {
            try
            {
                if (sp == null || dt == null)
                    return;
                if (dt.Rows != null && dt.Rows.Count > 0)
                {
                    List<string> lines = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        string errorType = null;
                        if (dr["Severity"] != null)
                        {
                            errorType = (string)dr["Severity"];
                        }

                        if (errorType == null)
                        {
                            continue;
                        }

                        if (errorType == ApplicationUploader.ERROR_STR)
                        {
                            string line = dr["Source"] + " " + dr["Error"];
                            lines.Add(line);
                        }
                    }

                    string uid = getExtractInfoId(sp, connectorName);
                    string fileName = getPostExtractOutputFileName(sp, Status.FAILED, uid, connectorName);
                    File.WriteAllLines(fileName, lines.ToArray());
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Unable to write failed messages", ex);
            }
        }

        public static void setConnectorExtractTimes(Space sp, MainAdminForm maf, string connectorName, bool transientUbserTransactionFlag)
        {
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(transientUbserTransactionFlag);
            string configFileDirectory = Path.Combine(sp.Directory, "connectors");
            if (!Directory.Exists(configFileDirectory))
                return;
            string configFileName = Path.Combine(configFileDirectory, connectorName + "_config.xml");
            if (!File.Exists(configFileName))
                return;
            DateTime startExtractionTime = DateTime.MinValue;
            DateTime endExtractionTime = DateTime.MinValue;
            XmlDocument xDocRoot = new XmlDocument();
            xDocRoot.XmlResolver = null;
            xDocRoot.Load(configFileName);
            foreach (XmlLinkedNode xDoc in xDocRoot.ChildNodes)
            {
                if (xDoc.Name == "ConnectorConfig")
                {
                    foreach (XmlNode xnode in xDoc.ChildNodes)
                    {
                        if(xnode.Name == "StartExtractionTime")
                        {
                            DateTime.TryParse(xnode.InnerText, out startExtractionTime);
                        }

                        if(xnode.Name == "EndExtractionTime")
                        {
                            DateTime.TryParse(xnode.InnerText, out endExtractionTime);
                        }
                    }
                }
            }

            if (startExtractionTime != null && endExtractionTime != null)
            {
                string startTimeVariableName = null;
                string endTimeVariableName = null;
                if(connectorName == CONNECTOR_SFDC)
                {
                    startTimeVariableName = Util.SFDC_EXTRACT_START_TIME;
                    endTimeVariableName = Util.SFDC_EXTRACT_COMPLETION_TIME;
                }
                else if(connectorName == CONNECTOR_NETSUITE)
                {
                    startTimeVariableName = Util.NETSUITE_EXTRACT_START_TIME;
                    endTimeVariableName = Util.NETSUITE_EXTRACT_COMPLETION_TIME;
                }

                if (startTimeVariableName != null && endTimeVariableName != null)
                {
                    Util.setConnectorTimeVariable(sp, maf, startTimeVariableName, startExtractionTime);
                    Util.setConnectorTimeVariable(sp, maf, endTimeVariableName, endExtractionTime);
                }
            }
            maf.setInUberTransaction(previousUberTransaction);
        }

        public static string getPostExtractOutputFileName(Space sp, int outputcode, string uid, string connectorName)
        {
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorName = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            string outputFilePrefix = "success";
            if (outputcode == Status.FAILED)
            {
                outputFilePrefix = "failed";
            }
            if (outputcode == Status.CANCELLED)
            {
                outputFilePrefix = "killed";
            }
            createConnectorsDirectory(sp);
            string outputFileName = connectorName + "Status-" + outputFilePrefix + "-" + uid + ".txt";
            return Path.Combine(getConnectorsDirectoryName(sp), outputFileName);
        }

        public static int getExtractStatusViaFileName(Space sp, string connectorName)
        {
            string uid = getExtractInfoId(sp, connectorName);
            string successName = getPostExtractOutputFileName(sp, Status.COMPLETE, uid, connectorName);
            string failedName = getPostExtractOutputFileName(sp, Status.FAILED, uid, connectorName);
            string killedName = getPostExtractOutputFileName(sp, Status.CANCELLED, uid, connectorName);

            if (File.Exists(killedName))
            {
                return Status.CANCELLED;
            }
            if(File.Exists(failedName))
            {
                return Status.FAILED;
            }
            return Status.COMPLETE;
        }

        public static void createConnectorsDirectory(Space sp)
        {
            string dir = getConnectorsDirectoryName(sp);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public static string getConnectorsDirectoryName(Space sp)
        {
            return Path.Combine(sp.Directory, "connectors");
        }
        public static void postConnectorExtractionUploader(Space sp, MainAdminForm maf, User user, string connectorName, DataTable errorsTable, string[] extractGroups)
        {
            Dictionary<string, string> objectsToMapping = getObjectAndMappingsFromConfigFile(sp, connectorName, extractGroups);
            if (objectsToMapping != null && objectsToMapping.Count > 0)
            {
                string sourceFileNamePrefix = getSourceNameFilePrefix(connectorName, sp);
                bool isIndependentMode = DefaultUserPage.isIndependentMode(sp);
                foreach (KeyValuePair<string, string> kvPair in objectsToMapping)
                {
                    string objectName = kvPair.Key;
                    if (sourceFileNamePrefix != null && sourceFileNamePrefix.Trim().Length > 0)
                    {
                        objectName = sourceFileNamePrefix + "_" + objectName;
                    }
                    // if an empty stub is created by extraction, then bypass the upload
                    if (isEmptyStubFile(sp, objectName))
                    {
                        Global.systemLog.Debug("Object SourceFile " + objectName + ".txt is empty, not uploading.");
                        continue;
                    }
                    string mapping = kvPair.Value;
                    List<SalesforceLoader.SFDCField> sfdcFields = (mapping == null ? null : SalesforceLoader.getMappedFields(mapping));
                    SalesforceLoader.mapAndUploadFile(isIndependentMode, maf, sp, user, objectName, errorsTable, sfdcFields);
                    //upload dependent sourcefiles for NetSuite
                    if (connectorName == CONNECTOR_NETSUITE)
                    {
                        string dataDir = Path.Combine(sp.Directory, "data");
                        string[] dependentSourceFiles = Directory.GetFiles(dataDir, objectName + "*.txt", SearchOption.TopDirectoryOnly);
                        if (dependentSourceFiles != null)
                        {
                            foreach (string sfName in dependentSourceFiles)
                            {
                                FileInfo sfFileInfo = new FileInfo(sfName);
                                string sfObjName = sfFileInfo.Name.Substring(0, sfFileInfo.Name.Length - 4);
                                if (sfObjName.Equals(objectName))
                                    continue;
                                if (isEmptyStubFile(sp, sfObjName))
                                {
                                    Global.systemLog.Debug("Dependent Object SourceFile " + sfFileInfo.Name + " is empty, not uploading.");
                                    continue;
                                }
                                SalesforceLoader.mapAndUploadFile(isIndependentMode, maf, sp, user, sfObjName, errorsTable, sfdcFields);
                            }
                        }
                    }
                }
            }
        }

        private static bool isEmptyStubFile(Space sp, string objectName)
        {
            bool isEmpty = false;
            if (sp != null && objectName != null && objectName.Length > 0)
            {
                string dataDir = Path.Combine(sp.Directory, "data");
                string fileName = Path.Combine(dataDir, objectName + ".txt");
                FileInfo fileInfo = new FileInfo(fileName);
                if (fileInfo.Exists && fileInfo.Length == 0)
                {
                    return true;
                }
            }
            return isEmpty;
        }

        private static string getSourceNameFilePrefix(string connectorName, Space sp)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            string configFileDirectory = getConnectorsDirectoryName(sp);
            if (!Directory.Exists(configFileDirectory))
                return null;
            string configFileName = Path.Combine(configFileDirectory, connectorName + "_config.xml");
            if (!File.Exists(configFileName))
                return null;

            XmlDocument xDocRoot = new XmlDocument();
            xDocRoot.XmlResolver = null;
            xDocRoot.Load(configFileName);
            return getSourceNameFilePrefix(xDocRoot);
        }

        private static string getSourceNameFilePrefix(XmlDocument xDocRoot)
        {
            foreach (XmlLinkedNode xDoc in xDocRoot.ChildNodes)
            {
                if (xDoc.Name == "ConnectorConfig")
                {
                    foreach (XmlNode xnode in xDoc.ChildNodes)
                    {
                        if (xnode.Name == "ConnectionProperties")
                        {
                            if (xnode.ChildNodes != null && xnode.ChildNodes.Count > 0)
                            {
                                XmlNode foundNode = null;
                                foreach (XmlNode connProperty in xnode.ChildNodes)
                                {
                                    if (connProperty.ChildNodes != null && connProperty.ChildNodes.Count > 0)
                                    {
                                        foreach (XmlNode connectorPropNode in connProperty.ChildNodes)
                                        {
                                            string paramName = connectorPropNode.Name;
                                            string propValue = connectorPropNode.InnerText;
                                            if (paramName == "Name" && propValue == "SourceFileNamePrefix")
                                            {
                                                foundNode = connProperty;
                                                break;
                                            }
                                        }
                                        if (foundNode != null)
                                            break;
                                    }
                                }
                                if (foundNode != null)
                                {
                                    foreach (XmlNode propParam in foundNode.ChildNodes)
                                    {
                                        if (propParam.Name == "Value")
                                        {
                                            return propParam.InnerText != null && propParam.InnerText.Length > 0 ? propParam.InnerText : null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        public static bool isConnectorSpecificExtractFilePresent(Space sp, string connectorName)
        {
            string connectorNameFromConnectionString = connectorName;
            if (UseNewConnectorFramework() && connectorName.IndexOf(":") >= 0 && connectorName.StartsWith("connector:birst://"))
            {
                connectorNameFromConnectionString = getConnectorNameFromConnectorConnectionString(connectorName);
            }
            string extractFileName = getConnectorSpecificExtractFileName(sp, connectorNameFromConnectionString);
            if (extractFileName != null)
            {
                FileInfo fi = new FileInfo(extractFileName);
                return fi.Exists;
            }
            return false;
        }

        public static string getConnectorSpecificExtractFileName(Space sp, string connectorName)
        {
            string extractFileName = null;
            if (sp != null)
            {
                string extractFile = connectorName + CONNECTOR_EXTRACT_FILE_SUFFIX;
                if (connectorName.Equals("sfdc", StringComparison.InvariantCultureIgnoreCase) || connectorName.Equals("salesforce", StringComparison.InvariantCultureIgnoreCase))
                    extractFile = Util.SFDC_EXTRACT_LOCK_FILE;                
                extractFileName = Path.Combine(ConnectorUtils.getConnectorsDirectoryName(sp), extractFile);                
            }
            return extractFileName;
        }

        public static void deleteAllConnectorSpecificExtractFiles(HttpSessionState session,Space sp, User u)
        {
            if (UseNewConnectorFramework())
            {
                foreach (Connector conn in getConfiguredConnectors(session, u, sp))
                {
                    deleteConnectorSpecificExtractFile(sp, conn.ConnectorName);
                }
            }
            else
            {
                deleteConnectorSpecificExtractFile(sp, CONNECTOR_SFDC);
                deleteConnectorSpecificExtractFile(sp, CONNECTOR_NETSUITE);
            }
        }

        public static void deleteConnectorSpecificExtractFile(Space sp, string connectorName)
        {
            if (sp == null || connectorName == null)
                return;
            try
            {
                string extractFileName = getConnectorSpecificExtractFileName(sp, connectorName);
                if(extractFileName != null)
                {
                    FileInfo fi = new FileInfo(extractFileName);
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in deleting extract lock file for " + sp.Directory + " : " + connectorName, ex);
            }
        }

        public static void createIfNotExistsConnectorSpecifcExtractFile(Space sp, string connectorName)
        {
            if (sp == null)
                return;

            string extractFileName = getConnectorSpecificExtractFileName(sp, connectorName);
            if (extractFileName != null)
            {
                FileStream fs = null;
                try
                {
                    if (!isConnectorSpecificExtractFilePresent(sp, connectorName))
                    {
                        fs = File.Create(extractFileName);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in creating " + extractFileName, ex);
                }
                finally
                {
                    if (fs != null)
                    {
                        try
                        {
                            fs.Close();
                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Error("Exception in closing filestream for " + extractFileName, ex2);
                        }
                    }
                }
            }
        }

        public static Dictionary<string, string> getObjectAndMappingsFromConfigFile(Space sp, string connectorName, string[] extractGroups)
        {   
            string configFileDirectory = getConnectorsDirectoryName(sp);
            if (!Directory.Exists(configFileDirectory))
                return null;
            string configFileName = Path.Combine(configFileDirectory, connectorName + "_config.xml");
            if (!File.Exists(configFileName))
                return null;

            XmlDocument xDocRoot = new XmlDocument();
            xDocRoot.XmlResolver = null;
            xDocRoot.Load(configFileName);
            return getObjectAndMappingsFromConfigFile(xDocRoot, extractGroups);
        }

        public static Dictionary<string, string> getObjectAndMappingsFromConfigFile(XmlDocument xDocRoot, string[] extractGroups)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            foreach (XmlLinkedNode xDoc in xDocRoot.ChildNodes)
            {
                if (xDoc.Name == "ConnectorConfig")
                {
                    foreach (XmlNode xnode in xDoc.ChildNodes)
                    {
                        if (xnode.Name == "ExtractionObjects")
                        {
                            if (xnode.ChildNodes != null && xnode.ChildNodes.Count > 0)
                            {
                                foreach (XmlNode exObject in xnode.ChildNodes)
                                {
                                    if (exObject.ChildNodes != null && exObject.ChildNodes.Count > 0)
                                    {
                                        string objectName = null;
                                        string mapping = null;
                                        string objectExtractionGroups = null;
                                        foreach (XmlNode objectParam in exObject.ChildNodes)
                                        {
                                            string itemName = objectParam.Name;
                                            string itemValue = objectParam.InnerText;
                                            if (itemName == "Name")
                                            {
                                                objectName = itemValue;
                                            }
                                            else if (itemName == "Mapping")
                                            {
                                                mapping = itemValue;
                                            }
                                            else if (itemName == "ExtractionGroupNames")
                                            {
                                                objectExtractionGroups = itemValue;
                                            }
                                        }
                                        if (objectName != null)
                                        {
                                            bool objBelongsToSelectedExtractGroups = false;
                                            if (extractGroups != null && extractGroups.Length > 0)
                                            {
                                                if (objectExtractionGroups != null)
                                                {
                                                    string[] objExtractGroups = objectExtractionGroups.Split(',');
                                                    if (objExtractGroups != null)
                                                    {
                                                        foreach (string oeg in objExtractGroups)
                                                        {
                                                            if (Array.IndexOf(extractGroups, oeg) >= 0)
                                                            {
                                                                objBelongsToSelectedExtractGroups = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    objBelongsToSelectedExtractGroups = true;
                                                }
                                            }
                                            else
                                            {
                                                objBelongsToSelectedExtractGroups = true;
                                            }
                                            if (objBelongsToSelectedExtractGroups)
                                            {
                                                response.Add(objectName, mapping == null ? "" : mapping);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }
    }
}
