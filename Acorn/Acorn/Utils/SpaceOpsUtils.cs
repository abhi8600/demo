﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.IO;
using Acorn.DBConnection;

namespace Acorn.Utils
{
    public class SpaceOpsUtils
    {
        public static Dictionary<int, string> SpaceOpsDictionary = new Dictionary<int, string>();
        public const int OP_AVAILABLE = 0;
        public const int OP_COPY_TO_NEW = 1;
        public const int OP_COPY_TO_EXISTING = 2;
        public const int OP_COPY_FROM = 3;
        public const int OP_SWAP = 4;
        public const int OP_DELETE_DATA = 5;
        public const int OP_DELETE_SPACE = 6;

        public const string SWAP_TMP_FILE = "swapTemp.lock";

        public static string getSpaceOpName(int id)
        {
            string name = null;
            Dictionary<int, string> spaceOps = getGlobalSpaceOperations();
            if (spaceOps != null && spaceOps.Count > 0)
            {
                name = spaceOps[id];
            }
            return name;
        }

        public static int getSpaceOpID(string name)
        {
            int spaceOpsID = -1;
            Dictionary<int, string> spaceOps = getGlobalSpaceOperations();
            if (spaceOps != null && spaceOps.Count > 0)
            {
                foreach (KeyValuePair<int, string> kvPair in spaceOps)
                {
                    int key = kvPair.Key;
                    string value = kvPair.Value;
                    if (name == value)
                    {
                        spaceOpsID = key;
                        break;
                    }
                }
            }
            return spaceOpsID;
        }

        public static Dictionary<int, string> getGlobalSpaceOperations()
        {
            if (SpaceOpsDictionary == null || (SpaceOpsDictionary != null && SpaceOpsDictionary.Count == 0))
            {
                QueryConnection conn = null;
                try
                {
                    // get it from database and populate it
                    conn = ConnectionPool.getConnection();
                    string mainSchema = Util.getMainSchema();
                    SpaceOpsDictionary = Database.getSpaceOperations(conn, mainSchema);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while populating Space Operations", ex);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }

            return SpaceOpsDictionary;
        }


        public static int getSpaceAvailability(Space sp)
        {
            return getSpaceAvailability(sp.ID);
        }

        public static int getSpaceAvailability(Guid spaceID)
        {
            QueryConnection conn = null;
            int status = -1;
            try
            {
                if (spaceID != Guid.Empty)
                {
                    conn = ConnectionPool.getConnection();
                    string mainSchema = Util.getMainSchema();
                    status = Database.getSpaceAvailabilityStatus(conn, mainSchema, spaceID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while update space availability for space " + spaceID, ex);
            }
            finally
            {
                try
                {
                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Error while releasing connection", ex2);
                }
            }

            return status;
        }

        public static void markSpaceAvailable(Space sp, User user, int endedOperation)
        {   
            updateSpaceAvailability(sp, OP_AVAILABLE, user);
            if (endedOperation != OP_AVAILABLE)
            {
                logEndEvent(user, sp.ID.ToString(), endedOperation);
            }
        }

        public static void updateSpaceAvailability(Space sp, int opID, User user)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string mainSchema = Util.getMainSchema();
                Database.updateSpaceAvailability(conn, mainSchema, sp, opID);
                if (opID != OP_AVAILABLE)
                {
                    string userName = user != null ? user.Username : "";
                    logStartEvent(user, sp.ID.ToString(), opID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while update space availability for space " + sp.ID, ex);
            }
            finally
            {
                try
                {
                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Error while releasing connection", ex2);
                }
            }
        }

        public static void logStartEvent(User user, string itemId, int opId){
            string spaceOpName = SpaceOpsUtils.getSpaceOpName(opId);
            if (!string.IsNullOrWhiteSpace(spaceOpName))
            {
                Util.logEvent(user, itemId, SpaceOpsUtils.getSpaceOpName(opId) + ":Start");
            }
        }

        public static void logEndEvent(User user, string itemId, int opId)
        {
            string spaceOpName = SpaceOpsUtils.getSpaceOpName(opId);
            if (!string.IsNullOrWhiteSpace(spaceOpName))
            {
                Util.logEvent(user, itemId, SpaceOpsUtils.getSpaceOpName(opId) + ":End");
            }
        }

        public static bool isSpaceAvailable(Guid spaceID)
        {
            return isSpaceAvailable(getSpaceAvailability(spaceID));
        }

        public static bool isSpaceAvailable(Space sp)
        {
            return isSpaceAvailable(getSpaceAvailability(sp));
        }

        public static bool isSpaceAvailable(int spaceOpID)
        {
            return spaceOpID > 0 ? false : true;
        }

        public static string getAvailabilityMessage(string spaceName, int opID)
        {
            string msg = "";
            string name = spaceName != null && spaceName.Length > 0 ? "'" + spaceName + "'" : "space";
            switch (opID)
            {
                case (OP_COPY_FROM):
                    msg = "Data is being copied from " + name + " .";
                    break;
                case (OP_COPY_TO_EXISTING):
                    msg = "Data is being copied to " + name + " .";
                    break;
                case (OP_COPY_TO_NEW):
                    msg = "Data is being copied to " + name + " .";
                    break;
                case (OP_SWAP):
                    msg = name + " is being swapped";
                    break;
                case (OP_DELETE_DATA):
                    msg = "Data is being deleted from " + name + " .";
                    break;
                case (OP_DELETE_SPACE):
                    msg = name + " is being deleted.";
                    break;
                default:
                    msg = name + " is unavailable.";
                    break;
            }
            return msg;
        }

        public static bool isSpaceBeingCopiedFrom(int spaceOpID)
        {
            return spaceOpID == OP_COPY_FROM ? true : false;
        }

        public static bool isNewSpaceBeingCopied(int spaceOpID)
        {
            return spaceOpID == OP_COPY_TO_NEW ? true : false;
        }

        public static bool isSwapSpaceInProgress(int spaceOpID)
        {
            return spaceOpID == OP_SWAP ? true : false;
        }
    }

    public class SpaceOperationStatus
    {
        Space sp;
        User user;        
        
        public SpaceOperationStatus(Space sp, User user)
        {
            this.sp = sp;
            this.user = user;            
        }

        public void logBeginStatus(int opId)
        {
            SpaceOpsUtils.updateSpaceAvailability(sp, opId, user);
        }

        public void logEndStatus(int opId)
        {
            SpaceOpsUtils.markSpaceAvailable(sp, user, opId);
        }
    }
}
