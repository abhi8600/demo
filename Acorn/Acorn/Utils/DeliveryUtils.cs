﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;

namespace Acorn.Utils
{
    public class DeliveryUtils
    {

        public static void scheduleEmail(Space sp, string from, string to, string subject, string body)
        {
            Guid id = Guid.NewGuid();
            EmailOptions emailOptions = new EmailOptions();
            emailOptions.ID = id;
            emailOptions.From = from;
            emailOptions.To = to;
            emailOptions.Subject = subject;
            emailOptions.Body = body;
            string emailTemplateFileName = sp.Directory + Path.DirectorySeparatorChar + "Email." + id + ".xml";
            saveToFile(emailOptions, emailTemplateFileName, emailOptions.GetType());
        }

        public static void saveToFile(Object obj, string fileName, Type type)
        {    
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter(fileName);
                XmlSerializer serializer = new XmlSerializer(type);
                serializer.Serialize(writer, obj);
            }
            finally
            {
                try
                {
                    if (writer != null)
                        writer.Close();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception in releasing resources during serializing: " + fileName, ex);
                    throw ex;
                }
            }
        }
    }

    [Serializable]
    public class EmailOptions
    {
        public Guid ID;
        public string From;
        public string To;
        public string Subject;
        public string Body;
    }

}
