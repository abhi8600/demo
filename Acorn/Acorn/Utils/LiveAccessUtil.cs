﻿using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.IO;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Data.Odbc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Threading;
using System.Net.Security;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Text.RegularExpressions;
using Acorn.tests;
using System.Net.Sockets;
using Acorn.WebDav;
using Acorn.DBConnection;

namespace Acorn.Utils
{
    public class LiveAccessUtil
    {
        public static string LIVE_ACCESS_LOAD_GROUP_PREFIX = "LIVEACCESS_";
        public static readonly int LIVEACCESS_LAST_LOAD_LOG_FILE = 0;
        public static readonly int LIVEACCESS_TRACE_LOG_FILE = 1;
        public static readonly int LIVEACCESS_LOAD_LOG_FILE_FOR_LOADID = 2;
        public static readonly int LIVEACCESS_ACTIVITY_LOG = 3;

        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public static bool sendCommandToLiveAccess(Space sp, string connection, String command)
        {
            Global.systemLog.Debug("Executing live access command " + command + " for space " + sp.ID);
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation " + command);
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 3 * 60;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder(command + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                s = s.Substring(4);
                if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                {
                    Global.systemLog.Error(s);
                    return false;
                }
                else if (s.StartsWith("OK"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return false;
        }

        public static bool updateTxnCommandHistoryFromLiveAccessConnection(Space sp, string connection, int loadNumber)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation updateTxnCommandHistoryFromLiveAccessConnection:loadNumeber=" + loadNumber);
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("gettxncommandhistorysnapshot\r\n" + loadNumber + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    return s.ToLower().Equals("true");
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return false;
        }

        public static int executeUpdateLiveAccessQuery(Space sp, string connection, string query)
        {
            int rows = -1;
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation executeLiveAccessQuery:query=" + query);
                return rows;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return rows;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return rows;
                if (!client.Connected)
                    return rows;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("executeupdatequery" + "\r\n" + query + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();

                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                {
                    return rows;
                }
                Int32.TryParse(s, out rows);

            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return rows;
        }

        private static bool initializeSMIWebSession(HttpSessionState session, bool fillSessionVars)
        {
            User u = Util.getSessionUser(session);
            Space sp = Util.getSessionSpace(session);
            string token = Util.getSMILoginToken(u.Username, sp);
            try
            {
                MainAdminForm maf = Util.getSessionMAF(session);
                return Util.getSMILoginSession(session, sp, token, HttpContext.Current.Response, HttpContext.Current.Request, fillSessionVars, maf);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to create session for SMIWeb");
                Global.systemLog.Error(ex);                
            }
            return false;
        }

        public static object[][] executeLiveAccessQuery(HttpSessionState session, User u, Space sp, string connection, string query, bool headers)
        {
            if (u == null && session != null)
            {
                u = Util.getSessionUser(session);
            }
            string command = "resultsonlyquery" + (headers ? "withheaders" : "");
            string commandArgs = query;
            if (isDirectConnection(sp, connection))
            {
                if (!initializeSMIWebSession(session, true))
                {
                    Global.systemLog.Error("Error initializing SMIWeb Session");
                    return null;
                }
                return importLiveAccessMetaDataUsingDirectConnection(u, sp, connection, command, commandArgs);
            }
            else
            {
                QueryConnection conn = null;
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation executeLiveAccessQuery:query=" + query);
                    return null;
                }
                try
                {
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                        return null;
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    client.LingerState.Enabled = false;
                    client.NoDelay = true;
                    if (client == null)
                        return null;
                    if (!client.Connected)
                        return null;
                    int timeoutSec = 30;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder(command + "\r\n" + commandArgs + "\r\n");
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();

                    byte b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        return null;
                    }
                    List<object[]> results = new List<object[]>();
                    int pos = 0;
                    while (pos < s.Length)
                    {
                        int end = s.IndexOf('\r', pos);
                        if (end < 0)
                            end = s.Length - 1;
                        string line = s.Substring(pos, end - pos);
                        if (line.Length > 0)
                        {
                            string[] cols = line.Split('|');
                            results.Add(cols);
                        }
                        pos = end + 2;
                    }
                    return results.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
                return null;
            }
        }

        public static bool tableExistsOnLiveAccess(Space sp, string connection, string schemaName, string tableName)
        {

            bool tableExists = false;
            if (schemaName != null && schemaName.Length > 0)
            {
                QueryConnection conn = null;
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation tableExistsOnLiveAccess:schemaName=" + schemaName + "\ttableName=" + tableName);
                    return false;
                }
                try
                {
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                        return false;
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    client.LingerState.Enabled = false;
                    client.NoDelay = true;
                    if (client == null)
                        return false;
                    if (!client.Connected)
                        return false;
                    int timeoutSec = 30;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder("tableexists\r\n" + schemaName + "\t" + tableName + "\r\n");
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();

                    byte b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    if (s.ToLower().StartsWith("true"))
                    {
                        tableExists = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
            }
            return tableExists;
        }

        public static TrustedService.ExecuteResult executeScriptOnLiveAccessConnection(Space sp, string connection, int loadNumber,
                    string inputQuery, string output, string script, int numRows)
        {
            TrustedService.ExecuteResult er = new Acorn.TrustedService.ExecuteResult();
            er.errorCodeSpecified = true;
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation executeliveaccessscript");
                er.errorCode = -2;
                er.errorMessage = "Failed to execute script on live access connection: " + connection;
                return er;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return null;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null || !client.Connected)
                {
                    Global.systemLog.Debug("Failed to obtain client connection for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation executeliveaccessscript");
                    er.errorCode = -2;
                    er.errorMessage = "Failed to execute script on live access connection: " + connection;
                    return er;
                }
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw1 = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw1.WriteLine(key);
                StringBuilder argsXML = new StringBuilder();
                StringWriter sw = new StringWriter(argsXML);
                XmlTextWriter writer = new XmlTextWriter(sw);
                writer.WriteStartElement("ExecuteScript");
                writer.WriteStartElement("LoadNumber");
                writer.WriteString(loadNumber.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement("InputQuery");
                writer.WriteString(inputQuery);
                writer.WriteEndElement();
                writer.WriteStartElement("Output");
                writer.WriteString(output);
                writer.WriteEndElement();
                writer.WriteStartElement("Script");
                writer.WriteString(script);
                writer.WriteEndElement();
                writer.WriteStartElement("MaxNumRows");
                writer.WriteString(numRows.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
                writer.Close();
                sw.Flush();
                sw.Close();
                StringBuilder sb = new StringBuilder("executeliveaccessscript\r\n" + argsXML.ToString() + "\r\n");
                sw1.WriteLine(sb.ToString().Length);
                sw1.WriteLine(sb.ToString());
                sw1.Flush();

                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw1.Close();
                ns.Close();
                client.Close();
                if (s.StartsWith("OK"))
                {
                    StringReader sr = new StringReader(s);
                    sr.ReadLine();
                    er.numInputRows = long.Parse(sr.ReadLine());
                    er.numOutputRows = long.Parse(sr.ReadLine());
                    er.errorCode = 0;
                    er.numInputRowsSpecified = true;
                    er.numOutputRowsSpecified = true;
                    er.success = true;
                    er.successSpecified = true;
                    return er;
                }
                else
                {
                    er.errorCode = -2;
                    er.errorMessage = s;
                    return er;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception occured during script execution ", ex);
                er.errorCode = -2;
                er.errorMessage = ex.Message;
                return er;
            }
        }

        public static bool getSourceFileFromLiveAccessConnection(Space sp, string connection, string filename)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getsourcefile:filename=" + filename);
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("getsourcefile\r\n" + filename + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();

                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    return s.ToLower().Equals("true");
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return false;
        }

        public static string[] getLogFileNameForLiveAccessConnection(Space sp, int loadid, string loadGroup)
        {
            string[] fnames = null;
            Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            if (leConfigs.ContainsKey(loadGroup))
            {
                string connName = leConfigs[loadGroup].getLocalETLConnection();
                if (loadid > 0)
                {
                    string filename = String.Format("smiengine." + connName + ".{0:0000000000}.log", loadid);
                    if (LiveAccessUtil.getLogFileFromLiveAccessConnection(sp, connName, 0, LiveAccessUtil.LIVEACCESS_LOAD_LOG_FILE_FOR_LOADID, loadid, filename) &&
                            File.Exists(sp.Directory + "\\logs\\temp_" + connName + "\\" + filename))
                    {
                        fnames = Directory.GetFiles(sp.Directory + "\\logs\\temp_" + connName + "\\", filename, SearchOption.TopDirectoryOnly);
                    }
                }
                else //get lastest logfile
                {
                    string filename = "smiengine." + connName + ".log";
                    if (LiveAccessUtil.getLogFileFromLiveAccessConnection(sp, connName, 0, LiveAccessUtil.LIVEACCESS_LAST_LOAD_LOG_FILE, 0, filename) &&
                            File.Exists(sp.Directory + "\\logs\\temp_" + connName + "\\" + filename))
                    {
                        fnames = Directory.GetFiles(sp.Directory + "\\logs\\temp_" + connName + "\\", filename, SearchOption.TopDirectoryOnly);
                    }
                }
            }
            return fnames;
        }

        public static bool getLogFileFromLiveAccessConnection(Space sp, string connection, long bytesToRead, int logType, int loadId, string filename)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getLastLoadLog");
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw1 = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw1.WriteLine(key);
                string liveAccessCommand = null;
                if (logType == LIVEACCESS_LAST_LOAD_LOG_FILE)
                    liveAccessCommand = "getlastloadlog";
                else if (logType == LIVEACCESS_TRACE_LOG_FILE)
                    liveAccessCommand = "gettracelog";
                else if (logType == LIVEACCESS_LOAD_LOG_FILE_FOR_LOADID)
                    liveAccessCommand = "getlogforloadid";
                else if (logType == LIVEACCESS_ACTIVITY_LOG)
                    liveAccessCommand = "getactivitylog";
                StringBuilder sb = new StringBuilder(liveAccessCommand + "\r\n" + bytesToRead +
                    (logType == LIVEACCESS_LOAD_LOG_FILE_FOR_LOADID ? "\t" + loadId : "") + "\r\n");
                sw1.WriteLine(sb.ToString().Length);
                sw1.WriteLine(sb.ToString());
                sw1.Flush();

                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadLine();
                if (!s.StartsWith("Error: ") && !s.Equals("EMPTY"))
                {
                    if (!Directory.Exists(sp.Directory + "\\logs"))
                    {
                        Directory.CreateDirectory(sp.Directory + "\\logs");
                    }
                    else
                    {
                        if (Directory.Exists(sp.Directory + "\\logs\\temp_" + connection))
                            Directory.Delete(sp.Directory + "\\logs\\temp_" + connection, true);
                        if (File.Exists(sp.Directory + "\\logs\\temp_" + connection + "\\" + filename))
                            File.Delete(sp.Directory + "\\logs\\temp_" + connection + "\\" + filename);
                    }
                    Directory.CreateDirectory(sp.Directory + "\\logs\\temp_" + connection);
                    StreamWriter sw = File.CreateText(sp.Directory + "\\logs\\temp_" + connection + "\\" + filename);
                    do
                    {
                        sw.WriteLine(s);
                    }
                    while ((s = reader.ReadLine()) != null);
                    sw.Flush();
                    sw.Close();
                }
                sw1.Close();
                ns.Close();
                client.Close();
                if (s != null && (s.StartsWith("Error: ") || s.Equals("EMPTY")))
                {
                    Global.systemLog.Error("Failed to get log file from local conneciton '" + connection + "' : " + s);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return false;
        }

        public static bool fileExistsOnLiveAccessConnection(Space sp, string connection, string filename)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation fileexists:filename=" + filename);
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("fileexists\r\n" + filename + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();

                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    return s.ToLower().Equals("true");
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return false;
        }

        public static object[][] getTableSchema(HttpSessionState session, User u, MainAdminForm maf, Space sp, string connection, string[] tables, string schema)
        {
            if (u == null && session != null)
            {
                u = Util.getSessionUser(session);
            }
            string command = "gettableschema";
            string commandArgs = schema == null ? "" : "schema=" + schema + "\t";
            StringBuilder commandArgsBuilder = new StringBuilder();
            foreach (string t in tables)
                commandArgsBuilder.Append(t + "\t");
            commandArgs += commandArgsBuilder.ToString();
            if (isDirectConnection(sp, connection))
            {
                return importLiveAccessMetaDataUsingDirectConnection(u, sp, connection, command, commandArgs);
            }
            else
            {
                QueryConnection conn = null;
                if (maf.EnableLiveAccessToDefaultConnection && connection == maf.connection.connectionList[0].Name)
                {
                    List<object[]> results = new List<object[]>();
                    QueryConnection qc = ConnectionPool.getConnection(sp.getFullConnectString());
                    try
                    {
                        foreach (string tableName in tables)
                        {
                            DataTable dt = qc.GetSchema("Columns", new string[] { null, sp.Schema, tableName, null }); // for security reasons, can only see the space's schema
                            foreach (DataRow dr in dt.Rows)
                            {
                                results.Add(new object[] { dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], dr[6] });
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(qc);
                    }
                    return results.ToArray();
                }
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (string t in tables)
                        sb.Append(t + "\t");
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getTableSchema:tables=" + sb.ToString());
                    return null;
                }
                try
                {
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                        return null;
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    if (client == null)
                        return null;
                    int timeoutSec = 30;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder(command + "\r\n" + (schema == null ? "" : "schema=" + schema + "\t"));
                    foreach (string t in tables)
                        sb.Append(t + "\t");
                    sb.Append("\r\n");
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();
                    byte b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    List<object[]> results = new List<object[]>();
                    int pos = 0;
                    while (pos < s.Length)
                    {
                        int end = s.IndexOf('\r', pos);
                        if (end < 0)
                            end = s.Length - 1;
                        string line = s.Substring(pos, end - pos);
                        if (line.Length > 0)
                        {
                            string[] cols = line.Split('\t');
                            results.Add(cols);
                        }
                        pos = end + 2;
                    }
                    return results.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
                return null;
            }
        }

        public static HashSet<string> getLiveAccessLoadGroups(MainAdminForm maf)
        {
            HashSet<string> laLoadGroups = new HashSet<string>();
            HashSet<string> loadGroups = Util.getLoadGroups(maf);

            if (loadGroups != null && loadGroups.Count > 0)
            {
                foreach (string lg in loadGroups)
                {
                    if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                    {
                        laLoadGroups.Add(lg);
                    }
                }
            }
            return laLoadGroups;
        }

        private static string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

        private static TrustedService.TrustedService getTrustedServiceInstance(Space sp)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            return ts;
        }

        public static object[][] importLiveAccessMetaDataUsingDirectConnection(User u, Space sp, string connection, string command, string commandArgs)
        {
            //route request to SMIWeb instead of LiveAccess connection
            try
            {
                TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
                XmlNode[] obj = (XmlNode[])ts.importLiveAccessMetaDataUsingDirectConnection(u.Username, sp.ID.ToString(), sp.Directory, connection, command, commandArgs);
                WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
                if (response != null)
                {
                    string success = WebServiceResponseXml.getFirstElementValue(response.getRoot(), "Success");
                    bool isSuccessful = Boolean.TrueString.ToLower() == success;
                    if (isSuccessful)
                    {
                        XElement[] resultElements = WebServiceResponseXml.getElements(response.getRoot(), "Result");
                        List<object[]> results = new List<object[]>();
                        foreach (XElement rowElement in WebServiceResponseXml.getElements(resultElements[0], "Row"))
                        {
                            string row = WebServiceResponseXml.getElementValue(rowElement);
                            string[] cols = row.Split('\t');
                            results.Add(cols);
                        }
                        return results.ToArray();
                    }
                    else
                    {
                        string reason = WebServiceResponseXml.getFirstElementValue(response.getRoot(), "Reason");
                        throw new Exception(reason);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            return null;
        }

        public static object[][] getCubes(HttpSessionState session, User u, Space sp, string connection)
        {
            if (u == null && session != null)
            {
                u = Util.getSessionUser(session);
            }
            string command = "getcubes";
            if (isDirectConnection(sp, connection))
            {
                return importLiveAccessMetaDataUsingDirectConnection(u, sp, connection, command, null);
            }
            else
            {
                QueryConnection conn = null;
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getCubes");
                    return null;
                }
                try
                {
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                        return null;
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    client.LingerState.Enabled = false;
                    client.NoDelay = true;
                    if (client == null)
                        return null;
                    if (!client.Connected)
                        return null;
                    int timeoutSec = 3 * 60;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder(command + "\r\n");
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();
                    StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    s = s.Substring(4);
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        return null;
                    }
                    List<object[]> results = new List<object[]>();
                    int pos = 0;
                    while (pos < s.Length)
                    {
                        int end = s.IndexOf('\r', pos);
                        if (end < 0)
                            end = s.Length - 1;
                        string line = s.Substring(pos, end - pos);
                        if (line.Length > 0)
                        {
                            string[] cols = line.Split('\t');
                            results.Add(cols);
                        }
                        pos = end + 2;
                    }
                    return results.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
                return null;
            }            
        }

        public static object[][] getCube(HttpSessionState session, User u, Space sp, string connection, string name, int dimStructureImportType)
        {
            if (u == null && session != null)
            {
                u = Util.getSessionUser(session);
            }
            string command = "getcube";
            string commandArgs = (dimStructureImportType == MainAdminForm.RELATIONAL_DIMENSIONAL_STRUCTURE ? name : name + "\t" + dimStructureImportType);
            if (isDirectConnection(sp, connection))
            {
                return importLiveAccessMetaDataUsingDirectConnection(u, sp, connection, command, commandArgs);
            }
            else
            {
                QueryConnection conn = null;
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getCube:cubename=" + name);
                    return null;
                }
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return null;
                try
                {
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    client.LingerState.Enabled = false;
                    client.NoDelay = true;
                    if (client == null)
                        return null;
                    if (!client.Connected)
                        return null;
                    int timeoutSec = 3 * 60;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder(command + "\r\n");
                    if (dimStructureImportType == MainAdminForm.RELATIONAL_DIMENSIONAL_STRUCTURE)
                    {
                        sb.Append(name + "\r\n");
                    }
                    else
                    {
                        sb.Append(name + "\t" + dimStructureImportType + "\r\n");
                    }                    
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();
                    StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    s = s.Substring(4);
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        return null;
                    }
                    List<object[]> results = new List<object[]>();
                    int pos = 0;
                    while (pos < s.Length)
                    {
                        int end = s.IndexOf('\r', pos);
                        if (end < 0)
                            end = s.Length - 1;
                        string line = s.Substring(pos, end - pos);
                        if (line.Length > 0)
                        {
                            string[] cols = line.Split('\t');
                            results.Add(cols);
                        }
                        pos = end + 2;
                    }
                    return results.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
                return null;
            }
        }

        public static bool isDirectConnection(Space sp, string connection)
        {
            string dcDirPath = Path.Combine(sp.Directory, Util.DCCONFIG_DIR);
            string dcFile = Path.Combine(sp.Directory, "dcconfig.xml");
            string[] dcconfigFiles = null;
            if (Directory.Exists(dcDirPath))
                dcconfigFiles = Directory.GetFiles(dcDirPath, "*dcconfig.xml");
            else if (File.Exists(dcFile))
                dcconfigFiles = new string[] { dcFile };
            foreach (Connection rc in BirstConnectUtil.getRealtimeConnectionList(sp, dcconfigFiles))
            {
                if (rc.VisibleName == connection)
                {
                    return rc.useDirectConnection;
                }
            }
            return false;
        }

        public static bool isRealTimeConnectionActive(ProxyRegistration pr, Space sp, string connection)
        {
            try
            {
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PingRealTimeConnectionTimeOutSeconds"];
                int timeoutSec = 10;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("ping\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                StreamReader reader = new StreamReader(ns,Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    s = s.Substring(4);
                    return s.Equals("True");
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error(e.Message);
                return false;
            }
        }

        public static object[][] getSchemaTables(HttpSessionState session, User u, MainAdminForm maf, Space sp, string connection, string schema)
        {
            string command = "gettables";
            string commandArgs = schema;
            if (isDirectConnection(sp, connection))
            {
                if (u == null && session != null)
                {
                    u = Util.getSessionUser(session);
                }
                return importLiveAccessMetaDataUsingDirectConnection(u, sp, connection, command, commandArgs);
            }
            else
            {
                QueryConnection conn = null;
                if (maf.EnableLiveAccessToDefaultConnection && connection == maf.connection.connectionList[0].Name)
                {
                    List<object[]> results = new List<object[]>();
                    QueryConnection qc = ConnectionPool.getConnection(sp.getFullConnectString());
                    try
                    {
                        DataTable dt = qc.GetSchema("Tables", new string[] {null, sp.Schema, null}); // for security reasons, can only see the space schema
                        foreach (DataRow dr in dt.Rows)
                        {
                            results.Add(new object[] { dr[0], dr[1], dr[2], dr[3] });
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(qc);
                    }
                    return results.ToArray();
                }
                ProxyRegistration pr = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getSchemaTables");
                    return null;
                }
                try
                {
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                        return null;
                    TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                    client.LingerState.Enabled = false;
                    client.NoDelay = true;
                    if (client == null)
                        return null;
                    if (!client.Connected)
                        return null;
                    int timeoutSec = 30;
                    if (seconds != null)
                    {
                        timeoutSec = int.Parse(seconds);
                    }
                    client.ReceiveTimeout = timeoutSec * 1000;
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                    string key = ConnectionRelay.getKey(sp, connection);
                    sw.WriteLine(key);
                    StringBuilder sb = new StringBuilder(command + "\r\n" + (schema == null ? "" : schema + "\r\n"));
                    sw.WriteLine(sb.ToString().Length);
                    sw.WriteLine(sb.ToString());
                    sw.Flush();
                    byte b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    b = (byte)ns.ReadByte();
                    StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                    string s = reader.ReadToEnd();
                    sw.Close();
                    ns.Close();
                    client.Close();
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        return null;
                    }
                    List<object[]> results = new List<object[]>();
                    int pos = 0;
                    while (pos < s.Length)
                    {
                        int end = s.IndexOf('\r', pos);
                        if (end < 0)
                            end = s.Length - 1;
                        string line = s.Substring(pos, end - pos);
                        if (line.Length > 0)
                        {
                            string[] cols = line.Split('\t');
                            results.Add(cols);
                        }
                        pos = end + 2;
                    }
                    return results.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
                return null;
            }
        }

        public static StringBuilder getLiveAccessDeleteAllCommands(Space sp, string applicationPath)
        {
            StringBuilder commands = new StringBuilder();
            string liveAccessDirectory = applicationPath + "\\" + sp.ID;
            commands.Append("repository \"" + liveAccessDirectory + "\\repository_dev.xml\"\n");
            commands.Append("deletealldata" + "\n");
            return commands;
        }

        public static StringBuilder getLiveAccessDeleteSpaceCommands(Space sp, string applicationPath)
        {
            StringBuilder commands = new StringBuilder();
            string liveAccessDirectory = applicationPath + "\\" + sp.ID;
            commands.Append("repository \"" + liveAccessDirectory + "\\repository_dev.xml\"\n");
            commands.Append("deletespace" + "\n");
            return commands;
        }

        public static StringBuilder getLiveAccessDeleteLastCommands(Space sp, string applicationPath, int loadNumber)
        {
            StringBuilder commands = new StringBuilder();
            string liveAccessDirectory = applicationPath + "\\" + sp.ID;
            commands.Append("repository \"" + liveAccessDirectory + "\\repository_dev.xml\"\n");
            commands.Append("resetetlrun " + loadNumber + "\n");
            commands.Append("deletedata " + loadNumber + "\n");
            return commands;
        }

        public static bool copySpaceDataToNewSpace(Space sp, string connection, string newSpaceSchemaName, string newSpaceID)
        {
            bool started = false;
            try
            {
                started = initiateCopySpaceDataToNewSpace(sp, connection, newSpaceSchemaName, newSpaceID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return false;
            }
            if (started)
            {
                //poll copy operation with retries
                int numRetries = 0;
                int maxRetryCount = 5;
                int waitForRetry = 60 * 1000;
                bool isComplete = false;
                double maxTimeoutMillis = ApplicationLoader.MAX_TIMEOUT * 60 * 1000;
                DateTime startTime = DateTime.Now;
                while (!isComplete)
                {
                    try
                    {
                        if ((DateTime.Now - startTime).TotalMilliseconds >= maxTimeoutMillis)
                        {
                            Global.systemLog.Error("Giving up pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection + " due to timeout");
                            return false;
                        }
                        string status = pollCopyspacedatatonewspace(sp, connection, newSpaceSchemaName, newSpaceID);
                        if (status.StartsWith("Error: ") || status.Equals("EMPTY"))
                        {
                            Global.systemLog.Error(status);
                            throw new Exception(status);
                        }
                        if (status.Equals("true", StringComparison.InvariantCultureIgnoreCase))
                            isComplete = true;
                        else if (status.Equals("running", StringComparison.InvariantCultureIgnoreCase))
                        {
                            isComplete = false;
                            System.Threading.Thread.Sleep(waitForRetry);
                        }
                        else if (status.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Global.systemLog.Error("Failed to perform copyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection);
                            return false;
                        }
                        else
                        {
                            Global.systemLog.Error("Invalid status received for pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection + " - " + status);
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (++numRetries > maxRetryCount)
                        {
                            Global.systemLog.Error("Failed to get status of pollCopyspacedatatonewspace after max retries " + maxRetryCount + " - giving up now", ex);
                            return false;
                        }
                        else
                        {
                            Global.systemLog.Warn("Failed to get status of pollCopyspacedatatonewspace - retrying");
                            System.Threading.Thread.Sleep(waitForRetry);
                        }
                    }
                }
                return isComplete;//copy space completed
            }
            else
            {
                Global.systemLog.Error("Failed to initiatecopyspacedatatonewspace for :" + sp.ID.ToString() + "/" + connection);
                return false;
            }
        }

        public static string pollCopyspacedatatonewspace(Space sp, string connection, string newSpaceSchemaName, string newSpaceID)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation copySpaceDataToNewSpace");
                throw new Exception("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation copySpaceDataToNewSpace");
            }
            if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                throw new Exception("LiveAccess connection not alive for pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection);
            TcpClient client = new TcpClient(pr.ServerName, pr.Port);
            client.LingerState.Enabled = false;
            client.NoDelay = true;
            if (client == null)
                throw new Exception("Invalid LiveAccess client for pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection);
            if (!client.Connected)
                throw new Exception("LiveAccess client not connected for pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection);
            int timeoutSec = 30;
            if (seconds != null)
            {
                timeoutSec = int.Parse(seconds);
            }
            client.ReceiveTimeout = timeoutSec * 1000;
            NetworkStream ns = client.GetStream();
            StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
            string key = ConnectionRelay.getKey(sp, connection);
            sw.WriteLine(key);
            StringBuilder sb = new StringBuilder("pollcopyspacedatatonewspace\r\n" + newSpaceSchemaName + "\t" + newSpaceID + "\r\n");
            sw.WriteLine(sb.ToString().Length);
            sw.WriteLine(sb.ToString());
            sw.Flush();
            byte b = (byte)ns.ReadByte();
            b = (byte)ns.ReadByte();
            b = (byte)ns.ReadByte();
            b = (byte)ns.ReadByte();
            StreamReader reader = new StreamReader(ns, Encoding.UTF8);
            string s = reader.ReadToEnd();
            sw.Close();
            ns.Close();
            client.Close();
            if (s == null)
            {
                throw new Exception("Invalid status received for pollcopyspacedatatonewspace operation for :" + sp.ID.ToString() + "/" + connection);
            }
            else
            {
                return s;                
            }            
        }

        public static bool initiateCopySpaceDataToNewSpace(Space sp, string connection, string newSpaceSchemaName, string newSpaceID)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation copySpaceDataToNewSpace");
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("initiatecopyspacedatatonewspace\r\n" + newSpaceSchemaName + "\t" + newSpaceID + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        Global.systemLog.Error(s);
                    }
                    return s.Equals("true", StringComparison.InvariantCultureIgnoreCase);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return false;
            }            
        }

        public static bool swapSpaceDirectories(Space sp, string connection, string otherSpaceID)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation swapSpaceDirectories");
                return false;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return false;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);                
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("swapspacedirectories\r\n" + otherSpaceID + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        Global.systemLog.Error(s);
                    }
                    return s.Equals("true", StringComparison.InvariantCultureIgnoreCase);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return false;
            }
        }

        public static bool isActiveConnection(Space sp, string connection)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getLastLoadLog");
                return false;
            }
            if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                return false;
            return true;
        }

        public static bool migratespacetolocal(Space sp, string connection, List<string> tablesToPopulate)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                throw new Exception("Unable to migratespacetolocal for space " + sp.ID.ToString() + ": cannot find proxy registration");
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    throw new Exception("Unable to migratespacetolocal for space " + sp.ID.ToString() + ": live access connection not active - " + connection);
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return false;
                if (!client.Connected)
                    return false;
                int timeoutSec = 30;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder();
                bool first = true;
                foreach (string table in tablesToPopulate)
                {
                    if (first)
                        first = false;
                    else
                        sb.Append("\t");
                    sb.Append(table);
                }
                StringBuilder sb1 = new StringBuilder("migratespacetolocal\r\n" + sb.ToString() + "\r\n");
                sw.WriteLine(sb1.ToString().Length);
                sw.WriteLine(sb1.ToString());
                sw.Flush();
                byte b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                b = (byte)ns.ReadByte();
                StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                if (s == null)
                {
                    return false;
                }
                else
                {
                    if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                    {
                        Global.systemLog.Error(s);
                    }
                    return s.Equals("true", StringComparison.InvariantCultureIgnoreCase);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return false;
            }
        }

        public static string markLoadAsFailed(Space sp, string connection, String command)
        {
            Global.systemLog.Debug("Executing live access command " + command + " for space " + sp.ID);
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation " + command);
                return null;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return null;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return null;
                if (!client.Connected)
                    return null;
                int timeoutSec = 3 * 60;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder(command + "\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                s = s.Substring(4);
                if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                {
                    Global.systemLog.Error(s);
                }
                return s;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return null;
            }
        }

        public static PerformanceEngineVersion getLocalProcessingEngineVersion(Space sp, string connection)
        {
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + connection);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            string seconds = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RealTimeConnectionTimeOutSeconds"];
            if (pr == null)
            {
                Global.systemLog.Debug("Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + connection + " for LiveAccess operation getLocalProcessingEngineVersion");
                return null;
            }
            try
            {
                if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, connection))
                    return null;
                TcpClient client = new TcpClient(pr.ServerName, pr.Port);
                client.LingerState.Enabled = false;
                client.NoDelay = true;
                if (client == null)
                    return null;
                if (!client.Connected)
                    return null;
                int timeoutSec = 3 * 60;
                if (seconds != null)
                {
                    timeoutSec = int.Parse(seconds);
                }
                client.ReceiveTimeout = timeoutSec * 1000;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns, Encoding.UTF8);
                string key = ConnectionRelay.getKey(sp, connection);
                sw.WriteLine(key);
                StringBuilder sb = new StringBuilder("getlocalprocessingengineversion\r\n");
                sw.WriteLine(sb.ToString().Length);
                sw.WriteLine(sb.ToString());
                sw.Flush();
                StreamReader reader = new StreamReader(ns, Encoding.UTF8);
                string s = reader.ReadToEnd();
                sw.Close();
                ns.Close();
                client.Close();
                s = s.Substring(4);
                if (s.StartsWith("Error: ") || s.Equals("EMPTY"))
                {
                    Global.systemLog.Error(s);
                    return null;
                }
                else if (s.Equals("Undefined"))
                {
                    Global.systemLog.Error("Unable to find local processing engine version");
                    return null;
                }
                Global.systemLog.Debug("Local Processing Engine version for space " + sp.ID.ToString() + " for connection " + connection + " = " + s);
                //check if the version is 5.1.0 or later
                try
                {
                    string[] versions = s.Split(new char[] { '.' });
                    if (versions != null && versions.Length == 3)
                    {
                        int major1 = int.Parse(versions[0]);
                        int major2 = int.Parse(versions[1]);
                        int minor = int.Parse(versions[2]);
                        if (major1 > 5 || (major1 == 5 && major2 > 0))
                        {
                            PerformanceEngineVersion peVersion = new PerformanceEngineVersion();
                            peVersion.SupportsRetryLoad = true;
                            peVersion.IsProcessingGroupAware = true;
                            return peVersion;
                        }
                    }                    
                }
                catch (Exception)
                {
                    Global.systemLog.Error("Unable to parse local processing engine version - " + s);
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return null;
            }
        }
    }
}
