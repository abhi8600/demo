﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.Web.SessionState;
using System.Collections.Specialized;
using System.Xml.Linq;
using System.Xml;
using Acorn.Exceptions;
using Performance_Optimizer_Administration;

namespace Acorn.Utils
{
    public class SchedulerUtils
    {
        private static String schedulerAccessServiceEndPoint = "/Scheduler/services/SchedulerAccess.SchedulerAccessHttpSoap11Endpoint";
        private static String schedulerTrustedAdminServiceEndPoint = "/Scheduler/services/SchedulerTrustedAdmin.SchedulerTrustedAdminHttpSoap11Endpoint";

        public static String STORED_SCHEDULED_SESSION_OBJ = "SchedulerCookie";
        // note this is  customized name of the default JSESSIONID by changing the configuration of the scheduler tomcat instance
        public static String SCHEDULER_COOKIE_NAME = "BSJSESSIONID";

        private static String SCHEDULER_INTERNAL_ADDRESS;
        // used on dev boxes when isapi enable is false
        private static String SCHEDULER_ROOT_IP;
        private static String TYPE_SFDC_EXTRACTION = "SFDCExtraction";
        private static String TYPE_REPORT = "Report";

        public static String getSchedulerInternalIpAddress()
        {
            if (SCHEDULER_INTERNAL_ADDRESS == null || SCHEDULER_INTERNAL_ADDRESS.Length == 0)
            {
                SCHEDULER_INTERNAL_ADDRESS = System.Web.Configuration.WebConfigurationManager.AppSettings["InternalSchedulerIPAddress"];
            }
            return SCHEDULER_INTERNAL_ADDRESS;
        }

        public static String getSchedulerRootIpAddress(HttpRequest request)
        {
            if (SCHEDULER_ROOT_IP == null || SCHEDULER_ROOT_IP.Length == 0)
            {
                String schedulerPort = System.Web.Configuration.WebConfigurationManager.AppSettings["SchedulerWebAppPort"];
                Uri uri = request.Url;
                SCHEDULER_ROOT_IP = uri.Scheme + "://" + uri.Host + ":" + schedulerPort;
            }
            return SCHEDULER_ROOT_IP;
        }

        public static bool getSchedulerLogin(HttpSessionState session, HttpResponse response, HttpRequest request)
        {
            try
            {
                // logoutSchedulerSession(session, sp, request);                                
                CookieContainer cookieContainer = (CookieContainer)session[SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ];
                String wsRequestSessionId = null;

                if (cookieContainer == null)
                {
                    cookieContainer = new CookieContainer();
                }
                else
                {
                    wsRequestSessionId = getSchedulerStoredSession(session);

                    if (wsRequestSessionId == null)
                    {
                        cookieContainer = new CookieContainer();
                    } // do the paranoid check on the stored BSJSESSIONID and the one from incoming request                    
                    else if (wsRequestSessionId != null && !Acorn.Util.paranoidCheck(wsRequestSessionId, request, SchedulerUtils.SCHEDULER_COOKIE_NAME))
                    {
                        // incoming request and stored session do not match. Create a new session 
                        // and log out cleanly from the stored one
                        logoutSchedulerSession(session, request);
                        cookieContainer = new CookieContainer();
                    }
                }

                Space sp = Util.getSessionSpace(session);
                User u = Util.getSessionUser(session);
                List<String> loginCredentials = new List<String>();
                loginCredentials.Add("userID=" + u.ID);
                loginCredentials.Add("userName=" + u.Username);
                loginCredentials.Add("operationsUserMode=" + u.OperationsFlag.ToString());
                if (sp != null)
                {
                    loginCredentials.Add("spaceID=" + sp.ID);
                    Dictionary<string, string> props = Util.getReportScheduleEmailProps(sp);
                    if (props != null && props.Count > 0)
                    {
                        if (props.ContainsKey(Util.KEY_REPORT_EMAIL_FROM))
                        {
                            loginCredentials.Add("scheduleFrom=" + props[Util.KEY_REPORT_EMAIL_FROM]);
                        }
                        if (props.ContainsKey(Util.KEY_REPORT_EMAIL_SUBJECT))
                        {
                            loginCredentials.Add("scheduleSubject=" + props[Util.KEY_REPORT_EMAIL_SUBJECT]);
                        }
                    }
                }                
                String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SchedulerLogin schedulerLogin = new SchedulerLogin();
                schedulerLogin.Url = localprotocol + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerAccessServiceEndPoint;
                Uri serviceUri = new Uri(schedulerLogin.Url);
                schedulerLogin.CookieContainer = cookieContainer;
                // call the Login webservice and login operation

                String schedulerResponse = null;
                try
                {
                    schedulerResponse = schedulerLogin.login(loginCredentials.ToArray());
                    if (schedulerResponse != null && schedulerResponse == "true")
                    {
                        Global.systemLog.Info("Successful login to Scheduler for user: " + u.ID + " , " + u.Username);
                    }
                    else
                    {
                        Global.systemLog.Warn("Failed to login to Scheduler (" + schedulerLogin.Url + ") for user: " + u.Username + ", with response: " + schedulerResponse);
                        return false;
                    }
                }
                catch (WebException wex)
                {
                    Global.systemLog.Error("Error while logging in to Scheduler: " + schedulerLogin.Url, wex);
                    return false;
                }

                bool newSession = false;
                foreach (Cookie c in cookieContainer.GetCookies(serviceUri))
                {
                    if (c.Name.Equals(SchedulerUtils.SCHEDULER_COOKIE_NAME) && (wsRequestSessionId == null || !c.Value.Equals(wsRequestSessionId)))
                    {
                        HttpCookie httpCookie = new HttpCookie(c.Name);
                        httpCookie.Value = Util.removeCRLF(c.Value);
                        // Scheduler Module and Administration resides on Admin Flex UI
                        // Commenting the setting of the path of Cookie to "/Scheduler", otherwise each click on
                        // Scheduler Module/Admin will not send the session id and hence unneeded
                        // re-logout and re-login to Scheduler.
                        //httpCookie.Path = c.Path;
                        httpCookie.HttpOnly = c.HttpOnly;
                        httpCookie.Secure = c.Secure;
                        response.Cookies.Add(httpCookie);
                        newSession = true;
                    }
                }

                WebResponse res = schedulerLogin.getServiceReturnResponse();
                if (newSession)
                {
                    Acorn.Util.addP3PHeader(response, res);
                }

                try
                {
                    res.Close();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Not able to close response object: ", ex);
                }
                session[SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ] = cookieContainer;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error while logging in to Scheduler ", e);
                return false;
            }
            return true;
        }

        public static string getSchedulerStoredSession(HttpSessionState session)
        {

            String storedSessionID = null;
            CookieContainer cookieContainer = (CookieContainer)session[SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ];
            if (cookieContainer == null)
            {
                // No Session stored. Return null
                return storedSessionID;
            }

            String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            String schedulerAccessServiceURL = localprotocol + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerAccessServiceEndPoint;
            Uri serviceUri = new Uri(schedulerAccessServiceURL);

            foreach (Cookie cookie in cookieContainer.GetCookies(serviceUri))
            {
                if (cookie.Name.Equals(SchedulerUtils.SCHEDULER_COOKIE_NAME))
                {
                    storedSessionID = cookie.Value;
                }
            }
            return storedSessionID;
        }

        public static void endSchedulerSession(HttpSessionState session, HttpRequest request)
        {
            logoutSchedulerSession(session, request);
        }

        public static void logoutSchedulerSession(HttpSessionState session, HttpRequest request)
        {
            try
            {
                CookieContainer cookieContainer = (CookieContainer)session[SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ];

                String storedSessionID = getSchedulerStoredSession(session);
                List<String> schedulerSessionIDs = new List<String>();
                if (storedSessionID != null)
                {
                    schedulerSessionIDs.Add(storedSessionID);
                }

                HttpCookie requestSchedulerCookie = request.Cookies.Get(SchedulerUtils.SCHEDULER_COOKIE_NAME);
                if (requestSchedulerCookie != null && storedSessionID != null)
                {
                    if (!requestSchedulerCookie.Value.Equals(storedSessionID))
                    {
                        schedulerSessionIDs.Add(requestSchedulerCookie.ToString());
                    }
                }

                SchedulerLogin schedulerAccess = new SchedulerLogin();
                schedulerAccess.Url = Util.getLocalProtocol() + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerAccessServiceEndPoint;

                // call the scheduler webservice and logout operation for session ids
                foreach (String sessionId in schedulerSessionIDs)
                {
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("Cookie", SchedulerUtils.SCHEDULER_COOKIE_NAME + "=" + sessionId);
                    schedulerAccess.setRequestHeaders(nvc);
                    SchedulerAccess.logoutResponse logoutResponse = schedulerAccess.logout();

                    if (logoutResponse != null && logoutResponse.@return == "true")
                    {
                        Global.systemLog.Info("Successful logout for Scheduler session: " + sessionId);
                    }
                    else
                    {
                        Global.systemLog.Warn("Problem in logging out for Scheduler session: " + sessionId);
                    }
                }

                if (session[SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ] != null)
                {
                    session.Remove(SchedulerUtils.STORED_SCHEDULED_SESSION_OBJ);
                }
            }
            catch (Exception e)
            {
                //log the exception
                Global.systemLog.Error("Exception thrown while logging out of Scheduler ", e);
            }
        }

        public static void loginIntoSchedulerIfAllowed(HttpSessionState session, HttpResponse response, HttpRequest request)
        {
            User user = Util.getSessionUser(session);
            Space sp = Util.getSessionSpace(session);
            if(user == null)
            {
                Global.systemLog.Warn("Scheduler Login : No user found in session");
                return;
            }

            if(sp == null)
            {
                Global.systemLog.Warn("Scheduler Login : No space found in session");
                return;
            }

            bool loginRequired = Util.isOwner(session, user);
            if (!loginRequired)
            {
                // make sure groups are updated from database
                UserAndGroupUtils.populateSpaceGroups(sp, true);                
                loginRequired = UserAndGroupUtils.isACLDefinedInAnyGroupForUser(user, sp.SpaceGroups, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED );
                // find dynamic groups that might be self scheduled allowed
                loginRequired = loginRequired || anyDynamicGroupsACLDefined(session, user, sp, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED);
            }

            if (loginRequired)
            {
                SchedulerUtils.getSchedulerLogin(session, response, request);
            }
        }

        private static bool anyDynamicGroupsACLDefined(HttpSessionState session, User u, Space sp, string aclTag)
        {   
            if (session != null && u != null && sp != null && aclTag != null)
            {
                // first get list of all the groups that are ACL defined for self schedule
                // then see if any user dynamic group name is part of the self schedule groups
                List<SpaceGroup> selfScheduleGroups = UserAndGroupUtils.getGroupsForGivenACL(sp.SpaceGroups, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED, false);                
                HashSet<string> selfScheduleGroupNames = null;
                if(selfScheduleGroups != null && selfScheduleGroups.Count > 0)
                {
                    selfScheduleGroupNames = new HashSet<string>();
                    foreach(SpaceGroup spg in selfScheduleGroups)
                    {
                        selfScheduleGroupNames.Add(spg.Name);
                    }
                }

                // if there are no groups with the acl in the first place, why bother going through user group membership
                if (selfScheduleGroupNames != null && selfScheduleGroupNames.Count > 0)
                {   
                    List<string> ssoPassedGroupList = DefaultUserPage.getSSOPassedGroupNames(session);
                    if (ssoPassedGroupList != null && ssoPassedGroupList.Count > 0)
                    {   
                        foreach (String dynGroupName in ssoPassedGroupList)
                        {
                            if(selfScheduleGroupNames.Contains(dynGroupName))
                            {
                                return true;
                            }
                        }
                    }
                    else if (sp.UseDynamicGroups)
                    {
                        // Is there a dynamic groups variable defined? Get dynamic query result from SMIWeb and look at the groups
                        Variable dynamicGroupsVar = GroupManagementService.getDynamicGroupsQueryVariable(session);
                        if (dynamicGroupsVar != null)
                        {
                            try
                            {
                                TrustedService.ExecuteResult result = DefaultUserPage.getDynamicGroupsFromSMIWeb(sp, u);
                                if (result != null && result.groups != null && result.groups.Length > 0)
                                {
                                    foreach (String groupName in result.groups)
                                    {
                                        if(selfScheduleGroupNames.Contains(groupName))
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Global.systemLog.Error("Error while getting dynamic group details : ", ex);
                            }
                        }
                    }
                }
            }
            return false;
        }
        
        public static void pollScheduler(HttpSessionState session)
        {   
            String sessionId = getSchedulerStoredSession(session);
            if (sessionId == null)
            {
                // No session to poll
                return;
            }
            SchedulerLogin schedulerAccess = new SchedulerLogin();
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("Cookie", SchedulerUtils.SCHEDULER_COOKIE_NAME + "=" + sessionId);
            schedulerAccess.setRequestHeaders(nvc);
            String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];            
            schedulerAccess.Url = localprotocol + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerAccessServiceEndPoint;
            schedulerAccess.poll();
        }

        public static void markProcessJobInSchedulerKilled(Space sp)
        {
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();

            scTrustedAdmin.killJob(sp.ID.ToString(), "Processing", null);
        }

        public static Status.SpaceStatus getCurrentStatus(Space sp, int loadId, string requestLoadGroup)
        {
            return getCurrentStatus(sp, loadId, requestLoadGroup, "Processing");
        }

        public static void runNowScheduledReport(Space sp, User user, string reportScheduleName)
        {
            string reportScheduleId = getReportScheduleId(sp, user, reportScheduleName);
            if (reportScheduleId != null && reportScheduleId.Trim().Length > 0)
            {
                SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
                scTrustedAdmin.runNowTrusedService(sp.ID.ToString(), TYPE_REPORT, reportScheduleId);
            }
            else
            {
                throw new BirstException("Cannot find the schedule name");
            }
            
        }

        public static string getReportScheduleId(Space sp, User user, string reportScheduleName)
        {
            if (sp == null || user == null || (reportScheduleName == null || reportScheduleName.Trim().Length == 0))
            {
                return null;
            }
            string id = null;
            List<ReportJob> scheduledReportJobs = getAllScheduledReports(sp, user);
            if (scheduledReportJobs != null && scheduledReportJobs.Count > 0)
            {
                foreach (ReportJob reportJob in scheduledReportJobs)
                {
                    if (reportJob.name != null && 
                        reportJob.name.ToLower() == reportScheduleName.ToLower() 
                        && reportJob.schedulerVersion != null)
                    {
                        if (reportJob.schedulerVersion.primaryScheduler)
                        {
                            id = reportJob.id;
                        }
                        else
                        {
                            string msg = "Report is scheduled on a different release";
                            string reportJobScheduler = reportJob.schedulerVersion.schedulerDisplayName;
                            if(reportJobScheduler != null && reportJobScheduler.Trim().Length > 0)
                            {
                                msg = msg + " (" + reportJobScheduler + ")";
                            }
                            throw new BirstException(msg);
                        }
                    }
                }
            }
            return id;
        }

        public static List<ReportJob> getAllScheduledReports(Space sp, User user)
        {
            List<ReportJob> response = new List<ReportJob>();
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XmlNode[] obj = (XmlNode[])scTrustedAdmin.getAllJobsTrustedService(sp.ID.ToString(), TYPE_REPORT, "false", user.ID.ToString(), "false");
            // obj is OMElement, need to parse 

            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                XElement resultElem = wsResponseXml.getResult();
                XElement scheduleJobListElem = WebServiceResponseXml.getFirstElement(resultElem, Constants.NODE_SCHEDULED_JOB_LIST);
                string primarySchedulerName = WebServiceResponseXml.getFirstElementValue(scheduleJobListElem, Constants.NODE_PRIMARY_SCHEDULER_NAME);
                XElement[] scheduledReports = WebServiceResponseXml.getElements(scheduleJobListElem, Constants.NODE_SCHEDULED_JOB);
                if (scheduledReports != null && scheduledReports.Length > 0)
                {
                    foreach (XElement scheduledJob in scheduledReports)
                    {
                        string id = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_ID);
                        if (id != null && id.Trim().Length > 0)
                        {
                            ReportJob sReport = new ReportJob();
                            sReport.id = id;
                            string reportJobName = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_NAME);
                            sReport.name = reportJobName;
                            
                            string schedulerName = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_NAME);
                            string schedulerDisplayName = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_DISPLAY_NAME);
                            string enableMigrationString = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_MIGRATION);
                            bool enableMigration = false;
                            if (enableMigrationString != null && enableMigrationString.Trim().Length > 0)
                            {
                                bool.TryParse(enableMigrationString, out enableMigration);
                            }
                            string isPrimarySchedulerString = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_IS_PRIMARY_SCHEDULER);
                            bool isPrimaryScheduler = false;
                            if (isPrimarySchedulerString != null && isPrimarySchedulerString.Trim().Length > 0)
                            {
                                bool.TryParse(isPrimarySchedulerString, out isPrimaryScheduler);
                            }

                            SchedulerVersion schedulerVersion = new SchedulerVersion();
                            schedulerVersion.schedulerName = schedulerName;
                            schedulerVersion.schedulerDisplayName = schedulerDisplayName;
                            schedulerVersion.enableMigration = enableMigration;
                            schedulerVersion.primaryScheduler = isPrimaryScheduler;
                            schedulerVersion.primarySchedulerName = primarySchedulerName;
                            sReport.schedulerVersion = schedulerVersion;
                            response.Add(sReport);
                        }
                    }
                }
                else
                {
                    Global.systemLog.Info("No scheduled reports returned from scheduler");
                }
            }
            else
            {
                Global.systemLog.Error("Error in getting retrieving report jobs from scheduler : errorCode : " + wsResponseXml.getErrorCode() + " : errorMessage : " + wsResponseXml.getErrorMessage());
                throw new Exception("Error in retrieving scheduled reports");
            }

            return response;
        }


        public static Status.SpaceStatus getCurrentStatus(Space sp, int loadId, string requestLoadGroup, string statusType)
        {
            Status.SpaceStatus result = new Status.SpaceStatus();
            result.available = false;
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            scTrustedAdmin.Url = Util.getLocalProtocol() + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerTrustedAdminServiceEndPoint;
            XmlNode[] obj = (XmlNode [])scTrustedAdmin.getJobStatusTrustedService(sp.ID.ToString(), statusType, null);
            // obj is OMElement, need to parse 
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                XElement resultElem = wsResponseXml.getResult();
                XElement spaceStatusElem = WebServiceResponseXml.getFirstElement(resultElem, Constants.NODE_SPACE_STATUS);
                string availableStr = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_SPACE_STATUS_AVAILABILITY);
                if (availableStr != null)
                {
                    bool available = bool.Parse(availableStr);
                    result.available = available;
                    result.stepName = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_SPACE_STEP_NAME);
                    if (result.stepName != null)
                    {
                        // if stepname is null means no load data available for this space
                        string stepStatus = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_SPACE_STEP_STATUS);
                        if (stepStatus != null)
                        {
                            result.stepStatus = int.Parse(stepStatus);
                        }
                        else
                        {
                            throw new Exception("Required value of stepStatus missing for space : " + sp.ID.ToString());
                        }
                        result.loadGroup = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_SPACE_LOAD_GROUP);
                        string loadIdString = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_SPACE_LOAD_ID);
                        if (loadIdString != null && loadIdString.Trim().Length > 0)
                        {
                            result.loadId = int.Parse(loadIdString);
                        }

                       result.createdDate = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_CREATED_DATE);
                       result.modifiedDate = WebServiceResponseXml.getFirstElementValue(spaceStatusElem, Constants.NODE_MODIFIED_DATE);
                    }
                }
                else
                {
                    throw new Exception("No availability node returned from scheduler" );
                }
            }
            else
            {
                throw new Exception("Error in getting status from scheduler : errorCode : " + wsResponseXml.getErrorCode() + " : errorMessage : " + wsResponseXml.getErrorMessage());
            }

            return result;
        }

        public static void schedulePublishJob(Space sp, User user, 
            bool createOverviewDashboards, int loadId, string loadGroup, 
            DateTime loadDate, string[] subGroups, bool retryFailedLoad, bool sendEmail)
        {
            try
            {   
                // The format of the load date string does not affect if its infobright or sql server
                // This string is re-evaluated into DateTime and reformatted depending on the db type
                // Here we just need to pass string value in the web service call
                String loadDateString = Util.getShortDateString(loadDate, null);
                SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
                XElement jobOptionsXml =
                        new XElement("ProcessInfo",
                                new XElement("LoadId", loadId),
                                new XElement("LoadGroup", loadGroup),
                                new XElement("createOverviewDashboards", createOverviewDashboards ? "true" : "false"),
                                new XElement("RetryFailedLoad", retryFailedLoad ? "true" : "false"),
                                new XElement(Constants.NODE_SEND_EMAIL, sendEmail ? "true" : "false"),
                                new XElement("LoadDate", loadDate)
                       );
                if (subGroups != null && subGroups.Length > 0)
                {
                    string subGroupElemString = "";
                    bool first = true;
                    foreach (String subGroup in subGroups)
                    {
                        if (!first)
                        {
                            subGroupElemString = subGroupElemString + ";";
                        }

                        subGroupElemString = subGroupElemString + subGroup;
                        if (first)
                        {
                            first = false;
                        }
                    }
                    XElement subGroupsElement = new XElement("SubGroups", subGroupElemString);
                    jobOptionsXml.Add(subGroupsElement);
                }
                scTrustedAdmin.addJobTrustedService(sp.ID.ToString(), user.ID.ToString(), "Processing", jobOptionsXml.ToString());
            }
            catch (WebException ex)
            {
                Global.systemLog.Error("Error while scheduling a publishing job", ex);
                cleanUpStatusInDb(sp, user, loadId, loadGroup);
                if (sendEmail)
                {
                    SalesforceLoader.sendFailureEmail(user.Email, "Birst Notification - Unable to Process Data", "Problem during processing for space " + sp.Name + ": " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Cleans up status in txn_command_history table. If there is a "hanging" entry
        /// add failed entries
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="user"></param>
        /// <param name="loadId"></param>
        /// <param name="loadGroup"></param>
        private static void cleanUpStatusInDb(Space sp, User user, int loadId, string loadGroup)
        {
            Status.StatusResult sr = Status.getLoadStatus(null, sp, loadId, loadGroup, true, null);
            if (sr != null && Status.isRunningCode(sr.code))
            {
                Status.setLoadFailed(sp, loadId, loadGroup, true, true);
            }
        }

        public static SchedulerTrustedAdminService getSchedulerTrustedAdminService()
        {
            SchedulerTrustedAdminService scTrustedAdmin = new SchedulerTrustedAdminService();
            scTrustedAdmin.Url = Util.getLocalProtocol() + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerTrustedAdminServiceEndPoint;
            return scTrustedAdmin;
        }

        public static SchedulerTrustedService.SchedulerTrustedAdmin getSchedulerTrustedService()
        {
            SchedulerTrustedService.SchedulerTrustedAdmin scTrustedAdmin = new SchedulerTrustedService.SchedulerTrustedAdmin();
            scTrustedAdmin.Url = Util.getLocalProtocol() + SchedulerUtils.getSchedulerInternalIpAddress() + schedulerTrustedAdminServiceEndPoint;
            return scTrustedAdmin;
        }

        public static void scheduleDeleteJob(Guid spaceID, Guid userID, bool deleteAll, bool restoreRepository, int loadId, string loadGroup)
        {
            SchedulerTrustedService.SchedulerTrustedAdmin scTrustedAdmin = getSchedulerTrustedService();
            string deleteType = deleteAll || loadId == 1 ? "DeleteAll" : "DeleteLast";
            XElement jobOptionsXml =
                    new XElement("DeleteInfo",
                            new XElement("LoadId", loadId),
                            new XElement("LoadGroup", loadGroup),
                            new XElement("Restore", restoreRepository ? "true" : "false"),
                            new XElement("DeleteType", deleteType)
                   );
            scTrustedAdmin.addJobTrustedService(spaceID.ToString(), userID.ToString(), "DeleteLoad", jobOptionsXml.ToString());
        }

        public static void runNowScheduleExtract(Guid spaceID, Guid userID, bool processAfter)
        {
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XElement jobOptionsXml = getStartExtractOptionsXml(processAfter);
            scTrustedAdmin.addJobTrustedService(spaceID.ToString(), userID.ToString(), TYPE_SFDC_EXTRACTION, jobOptionsXml.ToString());
        }

        public static XElement getStartExtractOptionsXml(bool processAfter)
        {
            XElement jobOptionsXml =
                   new XElement(Constants.NODE_SFDC_INFO,
                           new XElement("ExtractionEngine", Util.getDefaultSFDCVersion().ToString()),
                           new XElement(Constants.NODE_SFDC_PROCESS_AFTER, processAfter),
                           new XElement(Constants.NODE_SFDC_RUN_NOW_MODE, "true"),
                           new XElement("ConnectorType", "sfdc"),
                           new XElement(Constants.NODE_SEND_EMAIL, "false")
                  );
            return jobOptionsXml;
        }

        public static XElement runNowScheduleExtract(Guid spaceID, Guid userID, string xmlData, string[] extractGroups)
        {
            XElement extractGroupsXML = new XElement("ExtractGroups");
            if (extractGroups != null)
            {
                string egroups = "";
                bool first = true;
                foreach (string eg in extractGroups)
                {
                    if (first)
                        first = false;
                    else
                        egroups += ",";
                    egroups += eg;
                }
                extractGroupsXML.Value = egroups;
            }
            XElement xmlEl = XElement.Parse(xmlData);
            xmlEl.Add(extractGroupsXML);
            string extractGroupAppendedXmlData = xmlEl.ToString();
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XmlNode[] obj = (XmlNode[])scTrustedAdmin.addJobTrustedService(spaceID.ToString(), userID.ToString(), TYPE_SFDC_EXTRACTION, extractGroupAppendedXmlData);
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            return response.getRoot();
        }

        /// <summary>
        /// Assumption : all the requests coming are for scheduled job
        /// For manual extract, call runNowScheduleExtract instead
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="user"></param>
        /// <param name="sfdcJob"></param>
        /// <param name="scheduleListElem"></param>
        public static void addSFDCScheduleJob(Space sp, User user, SalesforceSettings.ScheduleSFDCJob sfdcJob, XElement scheduleListElem)
        {
            bool processAfter = sfdcJob != null ? sfdcJob.processAfterExtraction : false;
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XElement jobOptionsXml =
                    new XElement(Constants.NODE_SFDC_INFO,
                        new XElement(Constants.NODE_SFDC_RUN_NOW_MODE, "false"),
                        new XElement("ConnectorType", "sfdc"),
                        new XElement(Constants.NODE_SFDC_PROCESS_AFTER, processAfter),
                        new XElement(Constants.NODE_SEND_EMAIL, "true"),
                        scheduleListElem
                   );
            scTrustedAdmin.addJobTrustedService(sp.ID.ToString(), user.ID.ToString(), TYPE_SFDC_EXTRACTION, jobOptionsXml.ToString());
        }

        public static void addOrUpdateSFDCSchedules(Space sp, User user, SalesforceSettings.ScheduleSFDCJob sfdcJob, XElement scheduleListElem)
        {
            if (sfdcJob != null && sfdcJob.id != null && sfdcJob.id.Length > 0)
            {   
                updateSFDCSchedules(sp, user, sfdcJob, scheduleListElem);
            }
            else
            {
                addSFDCScheduleJob(sp, user, sfdcJob, scheduleListElem);
            }
        }

        // copy from old release schedule to a new one
        public static void copySFDCSchedules(Space sp, User user, SalesforceSettings.ScheduleSFDCJob sfdcJob)
        {
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            if (sfdcJob != null && sfdcJob.id != null && sfdcJob.id.Length > 0
                && sfdcJob.schedulerVersion != null && sfdcJob.schedulerVersion.schedulerDisplayName != null
                && sfdcJob.schedulerVersion.schedulerDisplayName.Length > 0)
            {
                XElement copyOptionsXml =
                    new XElement(Constants.NODE_COPY,
                        new XElement(Constants.NODE_COPY_JOB_ID, sfdcJob.id),
                        new XElement(Constants.NODE_COPY_FROM_SCHEDULE, sfdcJob.schedulerVersion.schedulerDisplayName),
                        new XElement(Constants.NODE_COPY_TYPE, TYPE_SFDC_EXTRACTION),
                        new XElement(Constants.NODE_COPY_DELETE_FROM, "true")
                   );
                scTrustedAdmin.copyJob(sp.ID.ToString(), user.ID.ToString(), TYPE_SFDC_EXTRACTION, copyOptionsXml.ToString(), null);
            }            
        }
        
        public static void updateSFDCSchedules(Space sp, User user, SalesforceSettings.ScheduleSFDCJob sfdcJob, XElement scheduleListElem)
        {
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XElement jobOptionsXml =
                new XElement(Constants.NODE_SFDC_INFO,
                    new XElement(Constants.NODE_SCHEDULE_ID, sfdcJob.id),
                    new XElement("ConnectorType", "sfdc"),
                    new XElement(Constants.NODE_SFDC_PROCESS_AFTER, sfdcJob.processAfterExtraction),
                    new XElement(Constants.NODE_SFDC_RUN_NOW_MODE, "false"),
                    new XElement(Constants.NODE_SEND_EMAIL, "true"),
                    scheduleListElem
                    );
            
            scTrustedAdmin.updateJobTrustedService(sp.ID.ToString(), user.ID.ToString(), TYPE_SFDC_EXTRACTION, sfdcJob.id, jobOptionsXml.ToString());
        }

        public static void removeSFDCJob(Space sp, User user, SalesforceSettings.ScheduleSFDCJob sfdcJob)
        {
            if (sfdcJob != null && sfdcJob.id != null && sfdcJob.id.Length > 0)
            {
                SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
                scTrustedAdmin.removeJobTrustedService(sp.ID.ToString(), TYPE_SFDC_EXTRACTION, sfdcJob.id.ToString());
            }
        }

        public static SalesforceSettings.ScheduleSFDCJob getSFDCJob(Space sp, User user)
        {
            SalesforceSettings.ScheduleSFDCJob result = null;
            SchedulerTrustedAdminService scTrustedAdmin = getSchedulerTrustedAdminService();
            XmlNode[] obj = (XmlNode[])scTrustedAdmin.getAllJobsTrustedService(sp.ID.ToString(), TYPE_SFDC_EXTRACTION, "false", user.ID.ToString(), "false");
            // obj is OMElement, need to parse 
            
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                XElement resultElem = wsResponseXml.getResult();
                XElement scheduleJobListElem = WebServiceResponseXml.getFirstElement(resultElem, Constants.NODE_SCHEDULED_JOB_LIST);
                string primarySchedulerName = WebServiceResponseXml.getFirstElementValue(scheduleJobListElem, Constants.NODE_PRIMARY_SCHEDULER_NAME);
                XElement scheduledJob = WebServiceResponseXml.getFirstElement(scheduleJobListElem, Constants.NODE_SCHEDULED_JOB);
                if (scheduledJob != null)
                {   
                    string id = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_ID);
                    if (id != null && id.Trim().Length > 0)
                    {
                        result = new SalesforceSettings.ScheduleSFDCJob();
                        result.id = id;
                        string processAfterStr = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SFDC_PROCESS_AFTER);
                        if (processAfterStr != null)
                        {
                            result.processAfterExtraction = bool.Parse(processAfterStr);
                        }

                        // get the scheduler version details of the sfdc job
                        string schedulerName = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_NAME);                        
                        string schedulerDisplayName = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_DISPLAY_NAME);
                        string enableMigrationString = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_SCHEDULER_MIGRATION);
                        bool enableMigration = false;
                        if (enableMigrationString != null && enableMigrationString.Trim().Length > 0)
                        {
                            bool.TryParse(enableMigrationString, out enableMigration);
                        }
                        string isPrimarySchedulerString = WebServiceResponseXml.getFirstElementValue(scheduledJob, Constants.NODE_IS_PRIMARY_SCHEDULER);
                        bool isPrimaryScheduler = false;
                        if (isPrimarySchedulerString != null && isPrimarySchedulerString.Trim().Length > 0)
                        {
                            bool.TryParse(isPrimarySchedulerString, out isPrimaryScheduler);
                        }
                        
                        SchedulerVersion schedulerVersion = new SchedulerVersion();
                        schedulerVersion.schedulerName = schedulerName;
                        schedulerVersion.schedulerDisplayName = schedulerDisplayName;
                        schedulerVersion.enableMigration = enableMigration;
                        schedulerVersion.primaryScheduler = isPrimaryScheduler;
                        schedulerVersion.primarySchedulerName = primarySchedulerName;
                        result.schedulerVersion = schedulerVersion;

                        // get the schedules
                        XElement scheduleList = WebServiceResponseXml.getFirstElement(scheduledJob, Constants.NODE_SCHEDULE_LIST);
                        List<SalesforceSettings.ScheduleInfoExternalScheduler> scheduleInfoList = new List<SalesforceSettings.ScheduleInfoExternalScheduler>(); ;
                        if (scheduleList != null)
                        {
                            XElement[] scheduleElements = WebServiceResponseXml.getElements(scheduleList, Constants.NODE_SCHEDULE);
                            if (scheduleElements != null)
                            {
                                foreach (XElement scheduleElement in scheduleElements)
                                {
                                    SalesforceSettings.ScheduleInfoExternalScheduler scheduleInfo = new SalesforceSettings.ScheduleInfoExternalScheduler();
                                    string scheduleInfoId = WebServiceResponseXml.getFirstElementValue(scheduleElement, Constants.NODE_SCHEDULE_ID);
                                    string enableStr = WebServiceResponseXml.getFirstElementValue(scheduleElement, Constants.NODE_ENABLE);

                                    scheduleInfo.id = scheduleInfoId;
                                    if (enableStr != null)
                                    {
                                        scheduleInfo.enable = bool.Parse(enableStr);
                                    }
                                    string cronExpression = WebServiceResponseXml.getFirstElementValue(scheduleElement, Constants.NODE_CRON);
                                    if (cronExpression != null)
                                    {
                                        ScheduleCronExpression sCronEx = ScheduleCronExpression.parseScheduleCronExpression(cronExpression);
                                        scheduleInfo.Hour = sCronEx.hour;
                                        scheduleInfo.Minute = sCronEx.minutes;
                                        scheduleInfo.AmPm = sCronEx.amPm;
                                        if (sCronEx.interval == ScheduleCronExpression.INTERVAL_WEEKLY)
                                        {
                                            scheduleInfo.type = SalesforceSettings.UpdateSchedule.Weekly;
                                            scheduleInfo.Day = ScheduleCronExpression.getNameDayOfWeek(sCronEx.daysOfWeek[0]);
                                        }
                                        else if (sCronEx.interval == ScheduleCronExpression.INTERVAL_MONTHLY)
                                        {
                                            scheduleInfo.type = SalesforceSettings.UpdateSchedule.Monthly;
                                        }
                                        else if (sCronEx.interval == ScheduleCronExpression.INTERVAL_DAILY)
                                        {
                                            scheduleInfo.type = SalesforceSettings.UpdateSchedule.Daily;
                                        }
                                        else
                                        {
                                            // some interval we can't understand
                                            continue;
                                        }

                                        scheduleInfoList.Add(scheduleInfo);
                                    }
                                }
                            }

                            result.schedulerDetailsNewScheduler = scheduleInfoList.ToArray();
                        }
                    }
                }
                else
                {
                    Global.systemLog.Info("No sfdc job node returned from scheduler");
                }
            }
            else
            {
                Global.systemLog.Error("Error in getting status from scheduler : errorCode : " + wsResponseXml.getErrorCode() + " : errorMessage : " + wsResponseXml.getErrorMessage());
            }   

            return result;
        }
    }

    public class ReportJob
    {
        public string id;
        public string name;
        public SchedulerVersion schedulerVersion;
    }

    public class SchedulerVersion
    {
        public string schedulerName;
        public string schedulerDisplayName;
        public bool primaryScheduler;
        public bool enableMigration;
        public string primarySchedulerName;

        public SchedulerVersion()
        {
        }
    }
}
