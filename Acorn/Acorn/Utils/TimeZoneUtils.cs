﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using Acorn.Exceptions;

namespace Acorn.Utils
{
    /// <summary>
    /// Time zone utility methods.
    /// </summary>
    public class TimeZoneUtils
    {
        private static List<TimeZoneInfo> systemTimeZones;
        private static List<string> timezoneDisplayNames;
        private static List<string> timeZoneIDs;

        public static List<TimeZoneInfo> getSystemTimeZones()
        {
            if (systemTimeZones == null)
            {
                systemTimeZones = Performance_Optimizer_Administration.TimeZoneUtil.getSystemTimeZones();
            }
            return new List<TimeZoneInfo>(systemTimeZones);
        }

        /// <summary>
        /// Retrieves all the time zone IDs sorted into GMT (actually UTC) order.
        /// </summary>
        /// <returns>all time zone IDs sorted into GMT order</returns>
        public static List<string> getTimeZoneIDs()
        {
            if (timeZoneIDs == null)
            {
                List<TimeZoneInfo> timezones = getSystemTimeZones();
//                timezones.Sort(new TimeZoneGmtComparer());
                timeZoneIDs = new List<string>();
                foreach (TimeZoneInfo timeZoneInfo in timezones)
                {
                    timeZoneIDs.Add(timeZoneInfo.Id);
                }

            }
            return timeZoneIDs;
        }

        public static List<string> getTimeZoneDisplayNames()
        {
            if (timezoneDisplayNames == null)
            {
                if (systemTimeZones == null || systemTimeZones.Count == 0)
                {
                    systemTimeZones = getSystemTimeZones();
                }
                timezoneDisplayNames = new List<string>();
                timezoneDisplayNames.Add("Default");
                foreach (TimeZoneInfo tzi in systemTimeZones)
                {
                    timezoneDisplayNames.Add(tzi.DisplayName);
                }
            }
            return timezoneDisplayNames;
        }

        public static string getSpaceTimeZone(HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            Space sp = Util.getSessionSpace(session);
            if (sp == null || maf == null)
            {
                throw new BirstException("Unable to get space time zone");
            }

            return getSpaceTimeZone(sp, maf);
        }

        public static string getSpaceTimeZone(Space sp, MainAdminForm maf)
        {
            string spaceTZ = Performance_Optimizer_Administration.Util.GLOBAL_TIMEZONE_DEFAULT;

            if (maf.serverPropertiesModule == null)
            {
                throw new BirstException("Unable to get space time zone");
            }

            if (maf.serverPropertiesModule != null && maf.serverPropertiesModule.ProcessingTimeZoneComboBox != null)
            {
                if (maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedIndex == -1)
                {
                    // selectedindex is -1 when space is newly created. Return default
                    return spaceTZ;

                }
                else if (maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedIndex == 0)
                {
                    // the first element into the data provider of the ComboBox is a string
                    spaceTZ = (string)maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedItem;
                }
                else
                {
                    // when selectedIndex is greater than 0
                    TimeZoneInfo tzi = (TimeZoneInfo)maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedItem;
                    spaceTZ = tzi.DisplayName;
                }
            }

            return spaceTZ;
        }

        public static void setSpaceTimeZone(string spaceLevelTimeZone, HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            Space sp = Util.getSessionSpace(session);
            if (sp == null || maf == null)
            {
                throw new BirstException("Cannot set Processing Time Zone");
            }

            if (spaceLevelTimeZone.Equals(Performance_Optimizer_Administration.Util.GLOBAL_TIMEZONE_DEFAULT) || spaceLevelTimeZone.Equals("Default"))
            {
                maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedIndex = 0;
                Util.markSessionDirty(session);
            }
            else
            {
                int index = maf.serverPropertiesModule.ProcessingTimeZoneComboBox.FindStringExact(spaceLevelTimeZone);
                if (index >= 0)
                {
                    maf.serverPropertiesModule.ProcessingTimeZoneComboBox.SelectedIndex = index;
                    Util.markSessionDirty(session);
                }
                else
                {
                    Global.systemLog.Error("Cannot find selected space level time zone in the list :" + spaceLevelTimeZone);
                    throw new BirstException("Cannot configure selected Processing Time Zone");
                }
            }

            Util.saveDirty(session);
        }

        public static void setDataSourceTimeZone(SourceFile sf, string dataSourceTimeZone)
        {
            if (sf == null || dataSourceTimeZone == null)
            {
                Global.systemLog.Warn("Cannot set time zone at Data Source Level");
                return;
            }

            if (dataSourceTimeZone.Equals(Performance_Optimizer_Administration.Util.SOURCE_TIMEZONE_DEFAULT) || dataSourceTimeZone.Equals("Default"))
            {
                string dataSourceDefault = Performance_Optimizer_Administration.Util.SOURCE_TIMEZONE_DEFAULT;
                sf.InputTimeZone = dataSourceDefault;
            }
            else
            {
                int index = TimeZoneUtils.getTimeZoneIndexByDisplayName(dataSourceTimeZone);
                if (index < 0)
                {
                    Global.systemLog.Warn("Cannot find selected datasource time zone in the list :" + dataSourceTimeZone);
                }
                else
                {
                    sf.InputTimeZone = systemTimeZones[index].Id;
                }
            }
        }

        public static int getTimeZoneIndexByDisplayName(string selectedDisplayName)
        {
            int index = -1;
            if (systemTimeZones == null || systemTimeZones.Count == 0)
            {
                systemTimeZones = getSystemTimeZones();
            }

            if (systemTimeZones != null)
            {
                for (int i = 0; i < systemTimeZones.Count; i++)
                {
                    string tziDisplayName = systemTimeZones[i].DisplayName;
                    if (tziDisplayName.Equals(selectedDisplayName))
                    {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        public static int getTimeZoneIndexById(string selectedId)
        {
            if (systemTimeZones == null || systemTimeZones.Count == 0)
            {
                systemTimeZones = getSystemTimeZones();
            }
            int index = -1;
            for (int i = 0; i < systemTimeZones.Count; i++)
            {
                if (systemTimeZones[i].Id.Equals(selectedId))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public static String getTimeZoneDisplayName(string id)
        {
            string displayName = null;
            if (id.Equals(Performance_Optimizer_Administration.Util.GLOBAL_TIMEZONE_DEFAULT)
                || id.Equals(Performance_Optimizer_Administration.Util.DISPLAY_TIMEZONE_DEFAULT)
                || id.Equals(Performance_Optimizer_Administration.Util.SOURCE_TIMEZONE_DEFAULT))
            {
                return id;
            }

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(id);
            if (tzi != null)
            {
                displayName = tzi.DisplayName;
            }
            else
            {
                Global.systemLog.Warn("Unable to find id for TimeZone id = " + id);
            }
            return displayName;
        }

        // Based on the processing time zone setting at the space, 
        // updated datetime is returned
        public static DateTime getProcessingTimeZoneAdjustedDateTime(Space sp, MainAdminForm maf, DateTime dateTime)
        {
            string processingTimeZoneDisplayName = getSpaceTimeZone(sp, maf);
            DateTime convertedDateTime = dateTime;
            if (processingTimeZoneDisplayName != Performance_Optimizer_Administration.Util.GLOBAL_TIMEZONE_DEFAULT)
            {
                foreach (TimeZoneInfo tzi in getSystemTimeZones())
                {
                    if (tzi.DisplayName.Equals(processingTimeZoneDisplayName))
                    {
                        DateTime utcDateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime);
                        convertedDateTime = TimeZoneInfo.ConvertTime(utcDateTime, tzi);
                    }
                }
            }

            return convertedDateTime;
        }

        public static string[] getTimeZoneValueLabelPairs()
        {
            List<String> timeZoneValueLabelPairs = new List<string>();
            foreach (TimeZoneInfo tzi in getSystemTimeZones())
            {
                int index = TimeZoneUtils.getTimeZoneIndexByDisplayName(tzi.DisplayName);
                if (index < 0)
                {
                    Global.systemLog.Warn("Cannot find selected datasource time zone in the list :" + tzi.DisplayName);
                }
                else
                {
                    timeZoneValueLabelPairs.Add(tzi.DisplayName + "|" + systemTimeZones[index].Id);
                }
            }
            return timeZoneValueLabelPairs.ToArray();
        }
    }
}
