﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Configuration;
using System.Text;
using Amazon.S3;
using Amazon.Runtime;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.Auth.AccessControlPolicy;
using Amazon.S3.Model;

namespace Acorn.Utils
{
    public class AWSUtils
    {
        private static int TOKEN_LIFETIME = 60 * 60 * 36;		// 36 hours (in seconds), time for the temporary credentials for upload/copy to be valid 

        public static AmazonS3Client getS3Client(string awsAccessKeyId, string awsSecretKey)
        {
            return getS3Client(new BasicAWSCredentials(awsAccessKeyId, awsSecretKey));
        }

        public static AmazonS3Client getS3Client(AWSCredentials s3credentials)
        {
            return new AmazonS3Client(s3credentials);
        }

        public static bool s3ObjectExists(AWSCredentials credentials, SourceFile sf)
        {
            AmazonS3Client s3client = getS3Client(credentials);
            GetObjectMetadataRequest req = new GetObjectMetadataRequest().WithBucketName(sf.S3Bucket).WithKey(sf.S3Folder + '/' + sf.FileName);
            return (s3client.GetObjectMetadata(req) != null);
        }

        // return the AWS credentials arguments for the Birst command line processing operations (Redshift only)
        public static string getAWSCredentials(string dbType, Space sp)
        {
            if (dbType == null || dbType != Database.REDSHIFT || sp.awsAccessKeyId == null || sp.awsSecretKey == null)
                return null;

            // Get AWS temporary session credentials
            AWSCredentials s3credentials = Acorn.Utils.AWSUtils.getTemporaryCredentials(sp.awsAccessKeyId, sp.awsSecretKey, sp.DatabaseLoadDir);

            string creds =
               " \"awsaccesskeyid=" + s3credentials.GetCredentials().AccessKey + "\""
             + " \"awssecretkey=" + s3credentials.GetCredentials().ClearSecretKey + "\""
             + ((s3credentials is SessionAWSCredentials) ? (" \"awstoken=" + s3credentials.GetCredentials().Token + "\"") : "");
            return creds;
        }

        // return the AWS credentials arguments for the Birst command line processing operations (Redshift only)
        public static string getAWSCredentials(string awsAccessKeyId, string awsSecretKey)
        {
            if (awsAccessKeyId == null || awsSecretKey == null)
                return null;

            // Get AWS temporary session credentials
            AWSCredentials s3credentials = Acorn.Utils.AWSUtils.getTemporaryCredentials(awsAccessKeyId, awsSecretKey, null);

            string creds =
               " \"awsaccesskeyid=" + s3credentials.GetCredentials().AccessKey + "\""
             + " \"awssecretkey=" + s3credentials.GetCredentials().ClearSecretKey + "\""
             + ((s3credentials is SessionAWSCredentials) ? (" \"awstoken=" + s3credentials.GetCredentials().Token + "\"") : "");
            return creds;
        }

        // 
        // create temporary credentials for the load operations
        // - s3Location is s3://bucketName/spaceId
        //
        public static AWSCredentials getTemporaryCredentials(string awsAccessKeyId, string awsSecretKey, string s3Location)
        {
            AWSCredentials s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);

            // try to create a federated token, if it fails, fall back on permanent credentials XXX only here for DSI right now
            try
            {
                AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(s3credentials);

                GetFederationTokenRequest req = new GetFederationTokenRequest();
                req.Name = "BirstRedshift";
                req.DurationSeconds = TOKEN_LIFETIME;

                string arnBucketResource = "arn:aws:s3:::*";
                string arnObjectResource = "arn:aws:s3:::*/*";

                if (s3Location != null)
                {
                    // clean up the bucket name (s3://bucketName\stuff or s3://bucketName/stuff)
                    string arnResource = s3Location;
                    if (arnResource.StartsWith("s3://"))
                        arnResource = arnResource.Substring("s3://".Length);
                    // clean up \\
                    arnResource = arnResource.Replace('\\', '/');
                    int pos = arnResource.IndexOf('/');
                    arnBucketResource = "arn:aws:s3:::" + arnResource.Substring(0, pos);
                    arnObjectResource = "arn:aws:s3:::" + arnResource + "/*";
                    Global.systemLog.Debug("bucket resource: " + arnBucketResource);
                    Global.systemLog.Debug("object resource: " + arnObjectResource);
                }

                Policy policy = new Policy();
                Statement stmt = new Statement(Statement.StatementEffect.Allow);
                stmt.Actions.Add(new ActionIdentifier("s3:ListBucket"));
                stmt.Actions.Add(new ActionIdentifier("s3:ListBucketVersions"));
                stmt.Resources.Add(new Amazon.Auth.AccessControlPolicy.Resource(arnBucketResource));
                policy.Statements.Add(stmt);

                stmt = new Statement(Statement.StatementEffect.Allow);
                stmt.Actions.Add(new ActionIdentifier("s3:GetObject"));
                stmt.Actions.Add(new ActionIdentifier("s3:GetObjectVersion"));
                stmt.Actions.Add(new ActionIdentifier("s3:PutObject"));
                stmt.Actions.Add(new ActionIdentifier("s3:DeleteObject"));
                stmt.Actions.Add(new ActionIdentifier("s3:DeleteObjectVersion"));
                stmt.Actions.Add(new ActionIdentifier("s3:ListObject"));
                stmt.Actions.Add(new ActionIdentifier("s3:ListObjectVersion"));
                stmt.Resources.Add(new Amazon.Auth.AccessControlPolicy.Resource(arnObjectResource));
                policy.Statements.Add(stmt);

                req.Policy = policy.ToJson();

                GetFederationTokenResponse resp = stsClient.GetFederationToken(req);
                GetFederationTokenResult res = resp.GetFederationTokenResult;
                Credentials sessionCredentials = res.Credentials;

                SessionAWSCredentials basicSessionCredentials = new SessionAWSCredentials(
                        sessionCredentials.AccessKeyId,
                        sessionCredentials.SecretAccessKey,
                       sessionCredentials.SessionToken);

                return basicSessionCredentials;
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Temporary credentials creation failed, using permanent credentials " + ex.Message);
            }
            return s3credentials;
        }
    }
}
