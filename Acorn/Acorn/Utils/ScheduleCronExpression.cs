﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Utils
{
    public class ScheduleCronExpression
    {   
		public string interval;
		public int[] daysOfWeek;
		public string dayOfMonth;		
		public int hour;
		public int minutes;		
		public string amPm;
	
        private string expression;
		private string[] cronParts;

        public const int INDEX_CRON_SECONDS = 0;
		public const int INDEX_CRON_MINUTES = 1;
		public const int INDEX_CRON_HOURS = 2;
		public const int INDEX_CRON_DAYOFMONTH = 3;
		public const int INDEX_CRON_MONTH = 4;
		public const int INDEX_CRON_DAYOFWEEK = 5;
		public const int INDEX_CRON_YEAR = 6;		
		public const string VALUE_LAST_DAY_MONTH = "L";

        public const string INTERVAL_DAILY = "Daily";
		public const string INTERVAL_WEEKLY = "Weekly";
		public const string INTERVAL_MONTHLY = "Monthly";
	
		public const string DAY_SUNDAY = "Sunday";
		public const string DAY_MONDAY = "Monday";
		public const string DAY_TUESDAY = "Tuesday";
		public const string DAY_WEDNESDAY = "Wednesday";
		public const string DAY_THURSDAY = "Thursday";
		public const string DAY_FRIDAY = "Friday";
		public const string DAY_SATURDAY = "Saturday";

		public const string TIME_AM = "AM";
		public const string TIME_PM = "PM";
		
		public const string TIMEZONE_EST = "EST";
		public const string TIMEZONE_PST = "PST";
		public const string TIMEZONE_CST = "CST";
		public const string TIMEZONE_MST = "MST";
		
		public static  string MONTH_LAST_DAY = "Last Day";

        
        public ScheduleCronExpression()
        {
        }

        public string create()
        {
            cronParts = new string[7];
            setCronSeconds();
            setCronMinute();
            setCronHour();
            setCronDayOfMonth();
            setCronMonth();
            setCronDaysOfWeek();
            joinCronParts();
            return expression;
        }

        private void joinCronParts()
		{
			expression = cronParts[INDEX_CRON_SECONDS] + " " +
						 cronParts[INDEX_CRON_MINUTES] + " " +
						 cronParts[INDEX_CRON_HOURS] + " " +
						 cronParts[INDEX_CRON_DAYOFMONTH] + " " +
						 cronParts[INDEX_CRON_MONTH] + " " +
						 cronParts[INDEX_CRON_DAYOFWEEK] + " " +
						 "*";				 
		}
		
		public void parse()
		{
			if(expression != null)
			{
				cronParts = expression.Split(' ');				
				parseInterval();
				parseDaysOfWeek();
				parseDayOfMonth();
				parseHour();
				parseMinute();				
			}	
		}
		
		private void parseInterval()
		{
			 if(cronParts[INDEX_CRON_DAYOFWEEK] == "*" 
			 		&& cronParts[INDEX_CRON_MONTH] == "*"
			 		&& cronParts[INDEX_CRON_DAYOFMONTH] == "?")
			 		{
			 			interval = ScheduleCronExpression.INTERVAL_DAILY;	
			 			return;		 			
			 		}
			 						 			
			 
			 if(cronParts[INDEX_CRON_DAYOFWEEK] != "*" 
			 			&& cronParts[INDEX_CRON_MONTH] == "*"
			 			&& cronParts[INDEX_CRON_DAYOFMONTH] == "?")
			 			{
			 				interval = ScheduleCronExpression.INTERVAL_WEEKLY;
			 				return;
			 			}
			 			
			 if(cronParts[INDEX_CRON_DAYOFWEEK] == "?" 
			 			&& cronParts[INDEX_CRON_MONTH] == "*"
			 			&& (cronParts[INDEX_CRON_DAYOFMONTH] != "?" || cronParts[INDEX_CRON_DAYOFMONTH] != "*"))
			 			{
			 				interval = ScheduleCronExpression.INTERVAL_MONTHLY;
			 				return;
			 			}
		}
		
		private void parseDaysOfWeek()
		{
            if (interval == null || interval == INTERVAL_WEEKLY)
            {
                string daysString = cronParts[INDEX_CRON_DAYOFWEEK];
                string[] strArray = daysString.Split(',');
                List<int> daysOfWeekList = new List<int>();
                bool success = true;
                foreach (string str in strArray)
                {
                    int result = -1;
                    int.TryParse(str, out result);
                    if (result >= 0)
                    {
                        daysOfWeekList.Add(result);
                    }
                    else
                    {
                        // if the result is special chars e.g. *,? etc
                        // interval is something else
                        success = false;
                        break;
                    }
                }

                if (success)
                {
                    daysOfWeek = daysOfWeekList.ToArray();
                }
            }
		}
		
		private void parseDayOfMonth()
		{
			dayOfMonth = cronParts[INDEX_CRON_DAYOFMONTH];
		}
		
		private void parseHour()
		{
			hour = int.Parse(cronParts[INDEX_CRON_HOURS]);
			if(hour >= 12)
			{				
				this.amPm = ScheduleCronExpression.TIME_PM;
				hour = hour == 12 ? hour : hour - 12;
			}
			else
			{
				this.amPm = ScheduleCronExpression.TIME_AM;
				hour = hour == 0 ? 12 : hour; 
			}
		}
		
		private void parseMinute()
		{
			minutes = int.Parse(cronParts[INDEX_CRON_MINUTES]);
		}
		
		
		private void setCronSeconds()
		{
			cronParts[INDEX_CRON_SECONDS] = "0";
		}	
		
		private void setCronMinute()
		{
			cronParts[INDEX_CRON_MINUTES] = minutes.ToString();
		}
		
		private void setCronHour()
		{
			int cronHour = hour;
			if(amPm == ScheduleCronExpression.TIME_AM)
			{
				cronHour = cronHour == 12 ? 0 : cronHour;
			}
			else
			{
				cronHour = cronHour == 12 ? 12 : cronHour + 12; 
			}
			
			cronParts[INDEX_CRON_HOURS] = cronHour.ToString();
		}
		
		private void setCronDayOfMonth()
		{
			if(interval == ScheduleCronExpression.INTERVAL_DAILY || interval == ScheduleCronExpression.INTERVAL_WEEKLY)			
			{
				cronParts[INDEX_CRON_DAYOFMONTH] = "?";
				return;
			}
			
			if(interval == ScheduleCronExpression.INTERVAL_MONTHLY)				
			{				
				if(dayOfMonth == ScheduleCronExpression.MONTH_LAST_DAY)
				{
					cronParts[INDEX_CRON_DAYOFMONTH] = VALUE_LAST_DAY_MONTH;
				}
				else
				{
					cronParts[INDEX_CRON_DAYOFMONTH] = dayOfMonth;
				}
			}
			 
		}
		
		private void setCronMonth()
		{
			// Right now we allow user a simplified monthly option
			cronParts[INDEX_CRON_MONTH] = "*";
		}
		
		private void setCronDaysOfWeek()
		{
			if(interval == ScheduleCronExpression.INTERVAL_DAILY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = "*";
			}
			
			if(interval == ScheduleCronExpression.INTERVAL_WEEKLY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = getDaysOfWeek();
			}
			
			if(interval == ScheduleCronExpression.INTERVAL_MONTHLY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = "?";
			}
		}
		
		private string getDaysOfWeek()
		{
			string response = null;
			if(daysOfWeek != null && daysOfWeek.Length > 0)
			{
				response = daysOfWeek[0].ToString();
                for (int i = 1; i < daysOfWeek.Length; i++ )
                {
                    response = response + "," + daysOfWeek[i];
                }
			}
			
			return response;
		}

        public static ScheduleCronExpression parseScheduleCronExpression(string _expression)
		{
			ScheduleCronExpression scheduleCronExpression = new ScheduleCronExpression();
			scheduleCronExpression.expression = _expression;
			scheduleCronExpression.parse();
			return scheduleCronExpression;
		}

        public static int getIndexIntDayOfWeek(string dayString)
        {
            if(dayString == DAY_SUNDAY) { return 1;}
			if(dayString == DAY_MONDAY) { return 2;}
			if(dayString == DAY_TUESDAY) { return 3;}
			if(dayString == DAY_WEDNESDAY) { return 4;}
			if(dayString == DAY_THURSDAY) { return 5;}
			if(dayString == DAY_FRIDAY) { return 6;}
			if(dayString == DAY_SATURDAY) { return 7;}
            return -1;
        }

        public static string getNameDayOfWeek(int index)
        {
            if (index == 1) { return DAY_SUNDAY; }
            if (index == 2) { return DAY_MONDAY; }
            if (index == 3) { return DAY_TUESDAY; }
            if (index == 4) { return DAY_WEDNESDAY; }
            if (index == 5) { return DAY_THURSDAY; }
            if (index == 6) { return DAY_FRIDAY; }
            if (index == 7) { return DAY_SATURDAY; }
            return null;
        }
    }
}