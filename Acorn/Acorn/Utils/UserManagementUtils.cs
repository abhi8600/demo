﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Acorn.Utils
{
    public class UserManagementUtils
    {
        public static int LIMIT_USER_RESULT = 10000;
        /// <summary>
        /// Return true if user is authorized for user management activities
        /// Right now, it checks for AdminAccountID and ManageAccountID flag in users table
        /// If both are not null and are same
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public static bool isUserAuthorized(User u)
        {
            if (u.isFreeTrialUser)
            {
                return false;
            }
            else if (u.OperationsFlag)
            {
                return true;
            }
            else if (!u.AdminAccountId.Equals(Guid.Empty) && !u.ManagedAccountId.Equals(Guid.Empty) && u.AdminAccountId.ToString() == u.ManagedAccountId.ToString())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the filename in the temporary folder uniquely determined by Guid (like userId).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string getTempFileName(Guid userId, string filename)
        {
            string path = System.IO.Path.GetTempPath();
            string fullTmpFileName = Path.Combine(path, userId.ToString() + "-" + filename);
            Util.validateFileLocation(path, fullTmpFileName);
            return fullTmpFileName;            
        }
    }
}
