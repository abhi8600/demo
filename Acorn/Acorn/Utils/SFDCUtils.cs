﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.SessionState;
using Acorn.sforce;
using System.Web.Services.Protocols;
using Acorn.Exceptions;
using System.Net;
using Performance_Optimizer_Administration;
using Acorn.Background;
using System.Data;

namespace Acorn.Utils
{
    public class SFDCUtils
    {

        public const string SFORCE_XML = "sforce.xml";
        public const string SESSION_SFORCE_LOADER = "sforceloader";
        public const string SESSION_SFORCE_BINDING = "sfbinding";

        public static SFDCCredentials getSFDCCredentials(HttpSessionState session, Space sp)
        {
            SFDCCredentials credentials = new SFDCCredentials();
            if (sp != null && sp.Directory != null && sp.Directory.Trim().Length > 0)
            {
                string sforceXmlFileName = Path.Combine(sp.Directory, SFORCE_XML);
                if (File.Exists(sforceXmlFileName))
                {   
                    // settings file do not exist, return null
                    SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                    credentials.Username = settings.Username;
                    credentials.Password = settings.Password;
                    credentials.Sitetype = settings.sitetype.ToString();
                    credentials.savedLogin = settings.saveAuthentication;
                    // automatically logged in if not logged in
                    bool isAlreadyLoggedIn = false;
                    if (session != null)
                    {
                        if (session[SESSION_SFORCE_BINDING] != null)
                        {
                            isAlreadyLoggedIn = true;
                        }
                        else if (settings.Username != null && settings.Username.Length > 0 
                            && settings.Password != null && settings.Password.Length > 0)
                        {
                            try
                            {
                                SforceService binding = loginAndGetNewBinding(settings.Username, settings.Password, settings.sitetype.ToString());
                                if (binding != null)
                                {
                                    session[SESSION_SFORCE_BINDING] = binding;
                                    isAlreadyLoggedIn = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                Global.systemLog.Error("Error while trying to automatically login to SFDC ", ex);
                            }
                        }
                    }
                    credentials.isAlreadyLoggedIn = isAlreadyLoggedIn; 
                }
            }
            return credentials;
        }

        public static List<SFDCSelectedObject> getSFDCSelectedObjects(Space sp)
        {
            List<SFDCSelectedObject> sfdcObjectsList = new List<SFDCSelectedObject>();
            if (sp != null && sp.Directory != null && sp.Directory.Trim().Length > 0)
            {
                SalesforceSettings settings = getSFDCSettings(sp);
                if (settings != null)
                {
                    SalesforceSettings.SalesforceObjectConnection[] socArray = settings.objects;
                    if (socArray != null && socArray.Length > 0)
                    {
                        foreach (SalesforceSettings.SalesforceObjectConnection soc in socArray)
                        {
                            SFDCSelectedObject sfdcObject = new SFDCSelectedObject();
                            sfdcObject.Name = soc.name;
                            if (soc.lastUpdatedDate != DateTime.MinValue)
                            {
                                sfdcObject.LastUpdatedDate = soc.lastUpdatedDate;
                            }

                            if (soc.lastSystemModStamp != DateTime.MinValue)
                            {
                                sfdcObject.LastSystemModStamp = soc.lastSystemModStamp;
                            }
                            sfdcObject.ColumnNames = soc.columnNames;
                            sfdcObjectsList.Add(sfdcObject);
                        }
                    }
                }
            }

            return sfdcObjectsList;
        }

        public static SalesforceSettings getSFDCSettings(Space sp)
        {
            SalesforceSettings settings = null;
            string sforceXmlFileName = Path.Combine(sp.Directory, SFORCE_XML);
            if (File.Exists(sforceXmlFileName))
            {
                settings  = SalesforceSettings.getSalesforceSettings(sforceXmlFileName);
            }
            return settings;
        }

        public static List<SFDCObject> retrieveAllSFDCObjects(HttpSessionState session, Space sp, bool markOrAddSelected)
        {
            List<SFDCObject> response = new List<SFDCObject>();
            if (sp != null && sp.Directory != null && sp.Directory.Trim().Length > 0)
            {
                SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                SforceService service = getBinding(session, sp, settings);

                if (service == null)
                {
                    Global.systemLog.Warn("Unable to create binding to retrieve SFDC objects");
                    return null;
                }

                sforce.DescribeGlobalResult dgr = service.describeGlobal();
                if (dgr == null)
                {
                    Global.systemLog.Debug("service.describeGlobal() returned null");
                }
                else
                {
                    Global.systemLog.Debug("Encoding: " + dgr.encoding);
                }

                if (dgr != null)
                {
                    DescribeGlobalSObjectResult[] dgos = dgr.sobjects;
                    if (dgos == null)
                    {
                        Global.systemLog.Debug("dgr.sobjects returned null");
                    }
                    foreach (DescribeGlobalSObjectResult dgsor in dgos)
                    {
                        // not including objects which are not queryable
                        if (!dgsor.queryable)
                        {
                            continue;
                        }
                        SFDCObject sfdcObject = new SFDCObject();
                        sfdcObject.Name = dgsor.name;
                        sfdcObject.Label = dgsor.label;
                        response.Add(sfdcObject);
                    }
                }

                List<SFDCSelectedObject> selectedObjects = getSFDCSelectedObjects(sp);
                if (selectedObjects != null && selectedObjects.Count > 0)
                {
                    List<SFDCObject> toAddObjects = new List<SFDCObject>();
                    foreach (SFDCSelectedObject existingObject in selectedObjects)
                    {
                        bool found = false;
                        foreach (SFDCObject sfdcObject in response)
                        {
                            if (sfdcObject.Name == existingObject.Name && (existingObject.Query == null || existingObject.Query.Trim().Length == 0))
                            {
                                sfdcObject.Selected = true;
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            // the object does not exist anymore on sforce but was added before
                            SFDCObject toAddObject = new SFDCObject();
                            toAddObject.Name = existingObject.Name;
                            toAddObject.Label = existingObject.Name;
                            toAddObject.Selected = true;
                            toAddObjects.Add(toAddObject);
                        }
                    }

                    if (toAddObjects != null && toAddObjects.Count > 0)
                    {
                        response.AddRange(toAddObjects);
                    }
                }

                // To add query objects

            }
            return response;
        }

        public static SforceService getBinding(HttpSessionState session, Space sp, SalesforceSettings settings)
        {   
            // see if any cached version is available
            SforceService service = session != null ? (SforceService)session[SESSION_SFORCE_BINDING] : null;
            if (service != null)
            {
                return service;
            }
            if (sp == null || settings == null)
            {
                return null;
            }

            string userName = settings.Username;
            string password = settings.Password;
            service = createNewBinding(settings.Username, settings.Password, settings.sitetype);
            session[SESSION_SFORCE_BINDING] = service;
            return service;
        }

        public static SforceService createNewBinding(string userName, string password, SalesforceSettings.SiteType siteType)
        {
            SforceService binding = new SforceService();
            if (siteType == SalesforceSettings.SiteType.Sandbox)
                binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Error("Bad SFDC binding URL prior to authentication: " + binding.Url);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            binding.Timeout = 30 * 60 * 1000;
            String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
            if (clientID != null && clientID.Length > 0)
            {
                CallOptions co = new CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            // Invoke the login call and save results in LoginResult
            LoginResult lr = null;
            try
            {
                lr = binding.login(userName, password);
            }
            catch (SoapException ex)
            {
                Global.systemLog.Error("Error while logging for SFDC ", ex);
                throw new BirstException(ex.Message);
                //throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            catch (WebException ex)
            {
                Global.systemLog.Error("Error while logging for SFDC ", ex);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }

            if (lr.passwordExpired)
            {
                Global.systemLog.Error("Password expired for SFDC - username : " + userName);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            /*
             * Can't login using zip until after authentication - errors not caught correctly if zip is on
             */
            binding = new SforceServiceGzip(true, true);
            if (siteType == SalesforceSettings.SiteType.Sandbox)
                binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Error("Bad SFDC binding URL prior to gzip authentication: " + binding.Url);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            binding.Timeout = 30 * 60 * 1000;
            if (clientID != null && clientID.Length > 0)
            {
                CallOptions co = new CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            try
            {
                lr = binding.login(userName, password);
            }
            catch (SoapException ex)
            {
                Global.systemLog.Error("Error while logging for SFDC ", ex);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            catch (WebException ex)
            {
                Global.systemLog.Error("Error while logging for SFDC ", ex);
                throw new SFDCException(BirstException.ERROR_SFDC_CREDENTIALS_LOGIN);
            }
            // Reset the SOAP endpoint to the returned server URL
            binding.Url = lr.serverUrl;
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Warn("Bad SFDC binding URL after authentication: " + binding.Url);
                return null;
            }
            // Create a new session header object
            // Add the session ID returned from the login
            binding.SessionHeaderValue = new SessionHeader();
            binding.SessionHeaderValue.sessionId = lr.sessionId;
            GetUserInfoResult userInfo = lr.userInfo;
            return binding;
        }


        internal static void startExtract(HttpSessionState session, Space sp, User user, MainAdminForm maf)
        {
            if (Util.isLoadLockFilePresent(sp))
            {
                Global.systemLog.Error("Cannot start extract : Load Lock file is present for space : " + (sp != null ? sp.ID.ToString() : ""));
                throw new SFDCException(BirstException.ERROR_SFDC_EXTRACT_LOAD_IN_PROGRESS);
            }

            if(Util.checkForPublishingStatus(sp))
            {
                Global.systemLog.Error("Cannot extract : Space loading in progress : " + (sp != null ? sp.ID.ToString() : ""));
                throw new SFDCException(BirstException.ERROR_SFDC_EXTRACT_LOAD_IN_PROGRESS);
            }

            if (Util.isSFDCExtractRunning(sp))
            {
                Global.systemLog.Error("Cannot extract : Another extraction in progress : " + (sp != null ? sp.ID.ToString() : ""));
                throw new SFDCException(BirstException.ERROR_SFDC_EXTRACT_ANOTHER_IN_PROGRESS);
            }
            SalesforceSettings settings = getSFDCSettings(sp);
            if (settings != null && settings.Objects.Length > 0)
            {
                List<string> objects = new List<string>();
                for (int i = 0; i < settings.Objects.Length; i++)
                {
                    objects.Add(settings.Objects[i].name);
                }
                SforceService service = getBinding(session, sp, settings);
                SalesforceLoader sl = new SalesforceLoader(objects, maf, sp, user, service, settings, false, null, false);
                DataTable errors = new DataTable();
                errors.Columns.Add("Source");
                errors.Columns.Add("Severity");
                errors.Columns.Add("Error");
                errors.Columns.Add("Object");
                sl.Errors = errors;
                BackgroundProcess.startModule(new BackgroundTask(sl, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
                Util.setSessionObject(session, SESSION_SFORCE_LOADER , sl);
                
            }
        }

        internal static List<ObjectExtractionStatus> getObjectsExtractionStatus(HttpSessionState session, Space sp)
        {
            // for now we will only have one object (manual extraction). Later will have multiple objects owing to parallel extraction
            List<ObjectExtractionStatus> response = new List<ObjectExtractionStatus>();
            SalesforceLoader sl = retrieveAndSetSalesforceLoaderSessionObject(session, sp);

            if (sl != null && sl.CurrentObject != null)
            {
                ObjectExtractionStatus objectStatus = new ObjectExtractionStatus();
                objectStatus.ObjectName = sl.CurrentObject;
                objectStatus.Count = sl.NumRecords;
                objectStatus.Status = Status.RUNNING;
                response.Add(objectStatus);
            }

            return response;
        }

        // retrieve the salesforce loader object from session
        // if not present, look into the global dictionary, if found set the session and return object
        private static SalesforceLoader retrieveAndSetSalesforceLoaderSessionObject(HttpSessionState session, Space sp)
        {
            SalesforceLoader sl = null;
            object sessionSalesforceLoader = Util.getSessionObject(session, SESSION_SFORCE_LOADER);
            if (sessionSalesforceLoader != null)
            {
                sl = (SalesforceLoader)sessionSalesforceLoader;
            }

            if (sl == null)
            {
                sl = SalesforceLoader.getLoader(sp.ID);
                Util.setSessionObject(session, SESSION_SFORCE_LOADER, sl);
            }
            return sl;
        }

        internal static ExtractionStatus getExtractionStatus(HttpSessionState session, Space sp)
        {
            ExtractionStatus response = new ExtractionStatus();
            if (Util.isSFDCExtractRunning(sp))
            {
                response.Status = Status.RUNNING;
                List<ObjectExtractionStatus> extractionStatusOfObjects = SFDCUtils.getObjectsExtractionStatus(session, sp);
                if (extractionStatusOfObjects != null && extractionStatusOfObjects.Count > 0)
                {
                    response.ObjectsExtractionStatus = extractionStatusOfObjects.ToArray();
                }
            }
            else
            {
                // TO DO refine the status to give more details about each object
                int status = Status.COMPLETE;
                List<string> errorMessages = null;
                // see if we can get errors based on the salesforceloader                
                SalesforceLoader sl = retrieveAndSetSalesforceLoaderSessionObject(session, sp);
                // If it is just warning during upload of files, we ignore it
                if (sl != null && !SalesforceLoader.ignoreUploaderErrors(sp, sl.Errors))
                {
                    status = Status.FAILED;
                    errorMessages = new List<string>();
                    foreach (DataRow dr in sl.Errors.Rows)
                    {
                        string errorType = null;
                        string errorMsg = null;
                        string errorObject = null;
                        if (dr["Severity"] != null)
                        {
                            errorType = (string)dr["Severity"];
                            errorMsg = (string)dr["Error"];
                            errorObject = (string)dr["Object"];
                        }
                        if (errorType != null && errorType == ApplicationUploader.ERROR_STR && errorObject != null && errorObject.Length > 0)
                        {
                            errorMessages.Add(errorObject);
                        }
                    }
                }
                else if (sl != null && sl.isTerminated())
                {
                    status = Status.CANCELLED;
                }

                response.Status = status;
                response.failedMessages = errorMessages != null && errorMessages.Count > 0? errorMessages.ToArray() : null;
            }

            return response;
        }

        private static SforceService loginAndGetNewBinding(string userName, string password, string siteType)
        {
            SalesforceSettings.SiteType loginSiteType = SalesforceSettings.SiteType.Production;
            if (siteType != null && siteType.ToLower() == "sandbox")
            {
                loginSiteType = SalesforceSettings.SiteType.Sandbox;
            }

            return createNewBinding(userName, password, loginSiteType);
        }

        internal static void saveCredentials(HttpSessionState session, Space sp, string userName, string password, string siteType, bool saveLogin)
        {   
            SforceService binding = loginAndGetNewBinding(userName, password, siteType);
            if (binding != null)
            {
                session[SESSION_SFORCE_BINDING] = binding;
                SalesforceSettings settings = getSFDCSettings(sp);
                if (settings == null)
                {
                    // create a new one
                    settings = new SalesforceSettings();
                }
                settings.saveAuthentication = saveLogin;
                settings.Username = userName;
                settings.Password = password;
                settings.saveSettings(sp.Directory);
            }
        }

        internal static void saveSelectedObjects(HttpSessionState session, Space sp, string[] objectsArray)
        {
            // all of the objects need to be saved
            SalesforceSettings settings = getSFDCSettings(sp);
            if (settings == null)
            {
                settings = new SalesforceSettings();
            }

            // Make sure no duplicates
            HashSet<string> objects = new HashSet<string>();
            if (objectsArray != null && objectsArray.Length > 0)
            {
                foreach (string objName in objectsArray)
                {
                    objects.Add(objName);
                }
            }
            List<SalesforceSettings.SalesforceObjectConnection> updatedList = new List<SalesforceSettings.SalesforceObjectConnection>();
            SalesforceSettings.SalesforceObjectConnection[] existingObjects = settings.objects;
            Dictionary<string, SalesforceSettings.SalesforceObjectConnection> existingStandardObjectsMap = new Dictionary<string,SalesforceSettings.SalesforceObjectConnection>();
            Dictionary<string, SalesforceSettings.SalesforceObjectConnection> existingQueryObjectsMap = new Dictionary<string, SalesforceSettings.SalesforceObjectConnection>();
            if (existingObjects != null && existingObjects.Length > 0)
            {
                foreach (SalesforceSettings.SalesforceObjectConnection existingObject in existingObjects)
                {
                    string key = existingObject.name.ToLower();
                    // all the query objects are also saved
                    if (existingObject.query != null && existingObject.query.Length > 0 && !existingQueryObjectsMap.ContainsKey(key))
                    {
                        updatedList.Add(existingObject);
                        existingQueryObjectsMap.Add(key, existingObject);
                        continue;
                    }
                    
                    if (!existingStandardObjectsMap.ContainsKey(key))
                    {
                        existingStandardObjectsMap.Add(key, existingObject);
                    }
                }
            }
            
            // if the object already exist, no need to overrwrite it since we will loose information on last updated time
            if (objects != null && objects.Count > 0)
            {
                foreach (string selectedObject in objects)
                {
                    string key = selectedObject.ToLower();
                    if (existingStandardObjectsMap.ContainsKey(key))
                    {
                        updatedList.Add(existingStandardObjectsMap[key]);
                    }
                    else if(!existingQueryObjectsMap.ContainsKey(key))
                    {
                        // never overrwrite an already present query with the standard object
                        SalesforceSettings.SalesforceObjectConnection newObject = new SalesforceSettings.SalesforceObjectConnection();
                        newObject.name = selectedObject;
                        updatedList.Add(newObject);
                    }
                }
            }

            
            settings.objects = updatedList.ToArray();
            settings.saveSettings(sp.Directory);
        }
    }
}