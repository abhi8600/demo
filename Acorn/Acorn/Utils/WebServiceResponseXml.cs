﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Linq;
using System.Xml.Linq;

namespace Acorn.Utils
{
    public class WebServiceResponseXml
    {
        public static readonly string ERROR_OTHER = "-2";
        // private static string SCHEDULER_ADMIN_NAMESPACE = "http://admin.webServices.scheduler.birst.com";
        private XDocument xDocument;

        public static WebServiceResponseXml getWebServiceResponseXml(XmlNode xmlNode)
        {
            return new WebServiceResponseXml(XDocument.Load(new XmlNodeReader(xmlNode)));
        }

        public static WebServiceResponseXml getWebServiceResponseXml(string str)
        {
            return new WebServiceResponseXml(XDocument.Load(str));
        }

        public static WebServiceResponseXml parseFromString(string str)
        {
            return new WebServiceResponseXml(XDocument.Parse(str));
        }

        public WebServiceResponseXml(XDocument _xDocument)
        {
            this.xDocument = _xDocument;
        }

        public bool isSuccesssful()
        {
            bool success = false;
            string errorCode = getErrorCode();
            if (errorCode != null && int.Parse(errorCode) == 0)
            {
                success = true;
            }

            return success;
        }

        public XElement getRoot()
        {
            return xDocument != null ? xDocument.Root : null;
        }

        public string getErrorCode()
        {
            return xDocument != null ? getFirstElementValue(xDocument.Root, "ErrorCode") : null;
        }

        public string getErrorMessage()
        {
            return xDocument != null ? getFirstElementValue(xDocument.Root, "ErrorMessage") : null;
            
        }

        public XElement getResult()
        {
            return xDocument != null ? getFirstElement(xDocument.Root, "Result") : null;
        }


        public static string getFirstElementValue(XElement parentElement, string childElementName)
        {
            XElement element = getFirstElement(parentElement, childElementName);
            return getElementValue(element);
        }

        public static string getElementValue(XElement element)
        {
            string response = null;
            if (element != null)
            {
                String value = element.Value;
                response = value != null && value.Trim().Length > 0 ? value.Trim() : null;
            }
            return response;
        }


        /// <summary>
        /// useful when there is only one known element know.
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="childElementName"></param>
        /// <returns></returns>
        
        public static XElement getFirstElement(XElement parentElement, string childElementName)
        {
            XElement[] elements = getElements(parentElement, childElementName);
            return elements != null ? elements[0] : null;
        }

        public static XElement[] getElements(XElement parentElment, string childElementName)
        {
            List<XElement> responseList = new List<XElement>();
            if (parentElment != null)
            {
                IEnumerable<XElement> elements = parentElment.Descendants(childElementName);
                foreach (XElement elem in elements)
                {
                    responseList.Add(elem);
                }
            }

            return responseList.Count == 0 ? null : responseList.ToArray();
        }
    }
}