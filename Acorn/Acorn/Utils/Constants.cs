﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Utils
{
    public class Constants
    {
        // Scheduler response node names
        public const string NODE_SCHEDULED_JOB = "ScheduledJob";
        public const string NODE_SPACE_STATUS = "SpaceStatus";
        public const string NODE_SPACE_STATUS_AVAILABILITY = "Available";
        public const string NODE_SPACE_STEP_NAME = "Step";
        public const string NODE_SPACE_STEP_STATUS = "Status";
        public const string NODE_SPACE_LOAD_ID = "LoadId";
        public const string NODE_SPACE_LOAD_GROUP = "LoadGroup";
        public const string NODE_CREATED_DATE = "CreatedDate";
        public const string NODE_MODIFIED_DATE = "ModifiedDate";

        public const string NODE_SFDC_INFO = "ConnectorInfo";
        public const string NODE_SFDC_PROCESS_AFTER = "ProcessAfter";
        public const string NODE_SFDC_RUN_NOW_MODE = "RunNowMode";
        public const string NODE_SEND_EMAIL = "SendEmail";

        public const string NODE_COPY = "CopyJobInfo";
        public const string NODE_COPY_JOB_ID = "JobId"; 
        public const string NODE_COPY_FROM_SCHEDULE = "FromScheduler";
        public const string NODE_COPY_TYPE = "Type";
        public const string NODE_COPY_DELETE_FROM = "DeleteFrom";

        // All Jobs response 
        public const string NODE_SCHEDULED_JOB_LIST = "ScheduledJobList";
        public const string NODE_PRIMARY_SCHEDULER_NAME = "PrimarySchedulerName";
        public const string NODE_SCHEDULER_NAME = "SchedulerName";
        public const string NODE_SCHEDULER_DISPLAY_NAME = "SchedulerDisplayName";
        public const string NODE_SCHEDULER_MIGRATION = "SchedulerMigration";
        public const string NODE_IS_PRIMARY_SCHEDULER = "PrimaryScheduler";
        public const string NODE_ID = "Id";        
        public const string NODE_NAME = "Name";
        public const string NODE_SPACE_ID = "SpaceID";
        public const string NODE_TYPE = "Type";
        public const string NODE_USER_ID = "UserID";
        public const string NODE_CREATED_USER = "CreatedUser";
        public const string NODE_RUN = "Run";
        public const string NODE_SCHEDULE_LIST = "ScheduleList";
        public const string NODE_SCHEDULE = "Schedule";
        public const string NODE_SCHEDULE_ID = "ScheduleId";
        public const string NODE_JOB_ID = "JobId";
        public const string NODE_CRON = "Cron";
        public const string NODE_TIMEZONE = "TimeZone";
        public const string NODE_ENABLE = "Enable";
        public const string NODE_START_TIME = "StartTime";

        // Processing values
        public const int SPACE_OPERATION_AVAILABLE = 0;
        public const int SPACE_PROCESS_PROGRESS = 1;
        public const int SPACE_PROCESS_FINISHED = 2;
        public const int SPACE_PROCESS_FAILED = 3;
    }
}