﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.DBConnection;
using Atp.Saml2;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Collections;
using System.Text.RegularExpressions;
using Acorn.SAMLSSO;
using System.Collections.Specialized;
using System.Text;

namespace Acorn.Utils
{
    public class SamlUtils
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static Regex subDomainRegex = new Regex(@"^(.+)\.\w+\.\w+$");
        public const string KEY_RELAY_SAML_STATE = "birst.samlstate";
        public const string PREFIX_BIRST_ATTRIBUTE = "birst.";

        public static bool isSAMLRequest(HttpRequest request)
        {
            try
            {
                SubDomainInfo subDomainInfo = getSubDomainInfo(request);
                return subDomainInfo != null && subDomainInfo.Active;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while evaluating request to be a SAML one", ex);
                return false;
            }
        }

        public static SubDomainInfo getSubDomainInfo(HttpRequest request)
        {
            if (request != null)
            {
                string subDomain = getSubDomain(request.Url);
                if (!string.IsNullOrEmpty(subDomain))
                {
                    return getSubDomainInfoFromDB(subDomain);
                }
            }
            return null;
        }
        public static SubDomainInfo getSubDomainInfoFromDB(string subDomain)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                return Database.getSubDomainInfo(conn, Util.mainSchema, subDomain);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

        }

        public static IdpMetaData getIdpMetaDataByIssuerID(string idpIssuerID)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<IdpMetaData> infoList = Database.getSamlInfoByIssuerID(conn, Util.mainSchema, idpIssuerID);
                foreach (IdpMetaData cur in infoList)
                {
                    if (cur.active)
                    {
                        return cur;
                    }
                }
                return null;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static IdpMetaData getIdpMetaDataByName(string idpName)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<IdpMetaData> infoList = Database.getSamlInfoByIdpName(conn, Util.mainSchema, idpName);
                foreach (IdpMetaData cur in infoList)
                {
                    if (cur.active)
                    {
                        return cur;
                    }
                }
                return null;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }


        public static string getSubDomain(Uri uri)
        {
            if (uri != null)
            {
                string host = uri.Host;
                if (host != null && host.Split('.').Length > 2)
                {
                    Match match = subDomainRegex.Match(host);
                    string value = match.Groups[1].Value;
                    Global.systemLog.Debug("SAML: getting subdomain " + value);
                    if (value.StartsWith("www"))
                    {
                        value = value.Substring(4);
                    }
                    return value;
                }
            }
            return null;
        }

        public static User getSamlUser(string subjectNameIdentifier, string issuerNameIdentifier)
        {
            QueryConnection conn = null;
            string fedId = null;
            try
            {
                fedId = subjectNameIdentifier + "|" + issuerNameIdentifier;
                Global.systemLog.Debug("SAML: url request for " + fedId);
                conn = ConnectionPool.getConnection();
                User u = null;
                // get the OpenID/Birst User association
                Guid userId = Database.getUserIdForOpenId(conn, mainSchema, fedId);
                if (userId == Guid.Empty)
                {
                    Global.systemLog.Info("The Federation ID (" + fedId + ") is not associated with a Birst user in open id table. Using username instead");
                    u = Database.getUser(conn, mainSchema, subjectNameIdentifier);
                }
                // get the actual Birst User
                if (u == null)
                {
                    u = Database.getUserById(conn, mainSchema, userId);
                }
                if (u == null)
                {
                    Global.systemLog.Info("The Birst user associated with the SAML Federation ID is not in the database or has expired products - " + fedId);
                    return null;
                }
                if (u.Disabled)
                {
                    Global.systemLog.Info("The Birst user associated with the SAML Federation ID has been disabled - " + fedId);
                    return null;
                }
                return u;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("An error occured during authentication of SAML Federation ID: " + fedId + " Exception: " + e.StackTrace);
                return null;
            }
        }

        public static Boolean provisionSamlUser(String fedId, String idpName, String birstUsername)
        {
            QueryConnection conn = null;
            User u = null;

            try
            {
                conn = ConnectionPool.getConnection();
                u = Database.getUser(conn, mainSchema, birstUsername);
                Database.createOpenIdUsersTable(conn, mainSchema);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("The Birst user cant be used - " + birstUsername + "\n" + e.StackTrace);
                return false;
            }

            try
            {
                if (!Database.addOpenIDForUserId(conn, mainSchema, u.ID, fedId + '|' + idpName))
                {
                    Global.systemLog.Info("The Birst user can not be mapped to the passed info - username:" + birstUsername + "External ID: " + fedId + "idp name: " + idpName);
                    return false;
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("The Birst user cant be mapped to the passed info - username:" + birstUsername + "External ID: " + fedId + "idp name: " + idpName + " -- \n" + e.StackTrace);
                return false;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return false;
        }

        public static void removeSamlUser(String fedId, String idpName, String birstUsername)
        {
            QueryConnection conn = null;
            User u = null;

            try
            {
                conn = ConnectionPool.getConnection();
                u = Database.getUser(conn, mainSchema, birstUsername);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("The Birst user cant be used - " + birstUsername + "\n" + e.StackTrace);
                return;
            }
            Database.removeOpenIDForUserId(conn, mainSchema, u.ID, fedId + '|' + idpName);
        }

        public static User getSamlUserFromAssertion(Atp.Saml2.Assertion assertion, Atp.Saml2.Issuer issuer)
        {            
            string fedId = assertion.Subject.NameId.NameIdentifier;
            Global.systemLog.Debug("SAML: user fedid from samlResponse " + fedId);
            return getSamlUser(fedId, issuer.NameIdentifier);
        }

        public static string getIdpInitiatedRelayState(User user)
        {
            if (user == null)
                return "";
            return "username=" + user.Username + "&BirstSpaceId=" + Guid.Empty + "&module=";
        }

        public static string getSPInitiatedRelayState(User user, string birstSamlState)
        {
            if (user == null)
                return "";
            return "username=" + user.Username + "&BirstSpaceId=" + Guid.Empty + "&module=" + getBirstModuleFromSamlState(birstSamlState);
        }

        public static string getBirstModuleFromSamlState(string birstSamlState)
        {
            string moduleName = "";
            if (!string.IsNullOrEmpty(birstSamlState))
            {
                int index = birstSamlState.IndexOf(KEY_RELAY_SAML_STATE);
                if (index >= 0)
                {
                    string stateName = birstSamlState.Substring(index + KEY_RELAY_SAML_STATE.Length + 1);
                    if (stateName.IndexOf("/html/home.aspx") >= 0 ||
                        stateName.IndexOf("FlexModule.aspx?birst.module=home") >= 0)
                    {
                        moduleName = "home";
                    }

                    if (stateName.IndexOf("FlexModule.aspx?birst.module=dashboard") >= 0 ||
                        stateName.IndexOf("/html/dasboards") >= 0)
                    {
                        moduleName = "dashboard";
                    }

                    if (stateName.IndexOf("FlexModule.aspx?birst.module=designer") >= 0)
                    {
                        moduleName = "designer";
                    }

                    if (stateName.IndexOf("FlexModule.aspx?birst.module=admin") >= 0)
                    {
                        moduleName = "admin";
                    }
                }
            }
            return moduleName;
        }

        internal static bool isValidSamlAccount(User user, IdpMetaData info)
        {   
            if (user != null && info != null && user.ManagedAccountId != null && user.ManagedAccountId != Guid.Empty)
            {
                bool match = false;
                List<Guid> samlAccounts = getSamlAccounts(info);
                if (samlAccounts != null && samlAccounts.Count > 0)
                {
                    foreach (Guid accountId in samlAccounts)
                    {
                        if (accountId == user.ManagedAccountId)
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        Global.systemLog.Warn("User account (" + user.Username + ") does not match with the registered SAML provider - (" 
                            + info.ID + " : " + info.idpIssuerID + " : " + info.idpName + ")");
                    }
                }
                else
                {
                    Global.systemLog.Warn("No accounts configured for the registered SAML provider - ("
                        + info.ID + " : " + info.idpIssuerID + " : " + info.idpName + ")");
                }
                
                return match;
            }
            return false;
        }

        private static List<Guid> getSamlAccounts(IdpMetaData info)
        {
            if (info != null && info.ID != Guid.Empty)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    return Database.getSamlAccountMappings(conn, mainSchema, info.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return null;
        }



        internal static bool isEncryptionEnabled(Response samlResponse)
        {
            if (samlResponse != null)
            {
                IList<EncryptedAssertion> encryptedAssertions = samlResponse.GetEncryptedAssertions();
                return encryptedAssertions != null && encryptedAssertions.Count > 0;
            }
            return false;
        }

        internal static bool validateEncryption(HttpServerUtility Server, Response samlResponse, out Assertion assertion)
        {
            string birstCert = Server.MapPath("~/config/SamlPrivate.pfx");
            IList<EncryptedAssertion> encryptedAssertions = samlResponse.GetEncryptedAssertions();
            assertion = null;
            if (encryptedAssertions != null)
            {
                EncryptedAssertion encryptedAssertion = encryptedAssertions[0];
                
                X509Certificate2 decryptKey = new X509Certificate2(birstCert, getSamlPrivateExportPassword());
                assertion = encryptedAssertion.Decrypt(decryptKey.PrivateKey, null);
            }
            
            return true;
        }

        private static String getSamlPrivateExportPassword()
        {
            object obj = System.Web.Configuration.WebConfigurationManager.AppSettings["SamlPrivateExportPassword"];
            if (obj != null)
            {
                return (string)obj;
            }
            return "";
        }

        internal static IDictionary<string, string> formatBirstAttributes(List<Atp.Saml2.Attribute> attributes)
        {   
            if (attributes != null)
            {
                IDictionary<string, string> attributesDict = new Dictionary<string, string>();
                foreach (Atp.Saml2.Attribute attribute in attributes)
                {
                    if (attribute.Name.Trim().StartsWith(PREFIX_BIRST_ATTRIBUTE))
                    {
                        attributesDict.Add(attribute.Name, attribute.Values[0].Data.ToString());
                    }
                }
                return attributesDict;
            }
            return null;
        }

        internal static void removeDictonaryItems(IDictionary<string, string> attributesDict, HashSet<string> toStripKeys)
        {
            if (attributesDict != null && toStripKeys != null)
            {
                foreach (string toStripKey in toStripKeys)
                {
                    attributesDict.Remove(toStripKey);
                }
            }
        }

        internal static System.Collections.Specialized.NameValueCollection getStrippedParams(IDictionary<string, string> attributesDict)
        {
            if (attributesDict != null)
            {
                NameValueCollection nvColl = new NameValueCollection();
                foreach (KeyValuePair<string, string> kvPair in attributesDict)
                {
                    string key = kvPair.Key;
                    if (key.StartsWith(PREFIX_BIRST_ATTRIBUTE))
                    {
                        key = key.Substring(PREFIX_BIRST_ATTRIBUTE.Length);
                    }
                    nvColl.Add(key, kvPair.Value);
                }
                return nvColl;
            }
            return null;
        }

        private static AttributeStatement getSamlBirstAttributeStatement(Assertion assertion)
        {
            if (assertion != null)
            {
                IList<AttributeStatement> attStatements = assertion.AttributeStatements;
                if (attStatements != null)
                {
                    foreach (AttributeStatement attStatement in attStatements)
                    {
                        if (attStatement.Attributes.Count > 0)
                        {
                            foreach (Atp.Saml2.Attribute attribute in attStatement.Attributes)
                            {
                                if (attribute.Name.Trim().StartsWith(SamlUtils.PREFIX_BIRST_ATTRIBUTE))
                                {
                                    return attStatement;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        public static List<Atp.Saml2.Attribute> getSamlBirstAttributes(Assertion assertion)
        {
            AttributeStatement birstAttStatement = getSamlBirstAttributeStatement(assertion);
            if (birstAttStatement != null)
            {
                if (birstAttStatement.Attributes.Count > 0)
                {
                    List<Atp.Saml2.Attribute> birstAttributes = new List<Atp.Saml2.Attribute>();
                    foreach (Atp.Saml2.Attribute att in birstAttStatement.Attributes)
                    {
                        if (att.Name.StartsWith(SamlUtils.PREFIX_BIRST_ATTRIBUTE))
                        {
                            birstAttributes.Add(att);
                        }
                    }
                    return birstAttributes;
                }
            }
            return null;
        }

        public static NameValueCollection fixUpPromptParams(string dashboardPrompts, string dashboardPromptsSeparator)
        {
            if (dashboardPrompts != null)
            {
                string separator = dashboardPromptsSeparator != null && dashboardPromptsSeparator.Trim().Length > 0 ? dashboardPromptsSeparator.Trim() : ";";
                string[] prompts = dashboardPrompts.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
                if (prompts != null)
                {
                    NameValueCollection nvc = new NameValueCollection();
                    foreach (string prompt in prompts)
                    {
                        string[] array = prompt.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                        if (array.Length == 2)
                        {
                            nvc.Add(array[0], array[1]);
                        }
                    }
                    return nvc.Count > 1 ? nvc : null;
                }
            }
            return null;
        }
    }
}