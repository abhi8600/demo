﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Acorn.Utils
{
    public class ConfigUtils
    {

        public const int WEB_MODE = 1;
        public const int CONSOLE_MODE = 2;
        public const int MOVE_MODE = 3;
        public static string MOVE_CONNECT_STRING;

        // If the mode is not set (-1), defaults to Web mode
        public static int RUNNING_MODE = -1;

        // Configuration Keys
        public const String KEY_ADMIN_DB_CONNECTION = "AdminDbConnectionKey";
        
        // Dictionary of all the Configuration parameters
        // Right now only used in console mode.  We can potentially use it also for web app configuration
        private static Dictionary<String, String> configMap = new Dictionary<string, string>();

        public static ConnectionStringSettings getConnectionString()
        {
            ConnectionStringSettings cs = null;
            if (isWebAppMode())
            {
                cs = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["DatabaseConnection"];
                if (cs == null)//which happens when accessing web.config from AcornTestConsole, find ConnectionString from AppSettings
                    cs = new ConnectionStringSettings("DatabaseConnection", System.Web.Configuration.WebConfigurationManager.AppSettings["AdminConnectionString"]);
            }
            else if (isConsoleMode())
            {
                // Used by AcornExecute -- Acorn wrapper
                cs = new ConnectionStringSettings("DatabaseConnection", System.Web.Configuration.WebConfigurationManager.AppSettings[KEY_ADMIN_DB_CONNECTION]);
                //cs = new ConnectionStringSettings("DatabaseConnection", getConfigValue(KEY_ADMIN_DB_CONNECTION));
            }
            else if (isMoveMode())
            {
                // Used by AcornExecute -- Acorn wrapper
                cs = new ConnectionStringSettings("DatabaseConnection", MOVE_CONNECT_STRING);
            }
            else if (Util.TEST_FLAG)
            {
                cs = new ConnectionStringSettings("DatabaseConnection", Acorn.tests.TestUtil.TEST_CONNECT_STRING);
            }
            
            return cs;
        }

        public static string getConfigValue(string key)
        {
            return configMap[key];
        }

        public static void addConfigValue(string key, string value)
        {
            configMap.Add(key, value);
        }

        public static bool isWebAppMode()
        {
            return getRunningMode() == WEB_MODE ? true : false;
        }

        public static bool isConsoleMode()
        {
            return getRunningMode() == CONSOLE_MODE ? true : false;
        }

        public static bool isMoveMode()
        {
            return getRunningMode() == MOVE_MODE ? true : false;
        }

        public static int getRunningMode()
        {
            // By default its in webapp mode
           return RUNNING_MODE == -1 ? WEB_MODE : RUNNING_MODE;
        }

        public static void setRunningMode(int mode)
        {
            switch(mode)
            {
                case WEB_MODE:
                    RUNNING_MODE = WEB_MODE;
                    break;
                case CONSOLE_MODE:
                    RUNNING_MODE = CONSOLE_MODE;
                    break;
                default:
                    break;
            }
        }
    }
}
