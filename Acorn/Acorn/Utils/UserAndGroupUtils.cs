﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Acorn.DBConnection;
using Performance_Optimizer_Administration;
using System.Text;

namespace Acorn.Utils
{
    public class UserAndGroupUtils
    {
        public static string USER_AND_GROUP_FILENAME = "UsersAndGroups.xml";
        private const int USER_GROUP_FILE_VERSION = 1;

        public static void populateSpaceGroups(Space sp, bool forceUpdate)
        {
            if (sp.SpaceGroups == null || forceUpdate)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    sp.SpaceGroups = Database.getSpaceGroups(conn, Util.getMainSchema(), sp);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while populating spaceGroups", ex);
                    throw ex;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        public static SpaceGroup getGroupInfoById(Space sp, Guid groupId)
        {
            return getGroupInfoByName(sp, groupId, false);
        }

        public static SpaceGroup getGroupInfoByName(Space sp, Guid groupId, bool internalAllowed)
        {
            SpaceGroup spg = null;
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {
                // Never expose internal groups. They will be eventually phased out
                // Possibly supplied to SMIWeb through a webservice                
                foreach (SpaceGroup spaceGroup in sp.SpaceGroups)
                {
                    if (!internalAllowed && spaceGroup.InternalGroup)
                    {
                        continue;
                    }

                    if (spaceGroup.ID.ToString() == groupId.ToString())
                    {
                        spg = spaceGroup;
                        break;
                    }
                }
            }
            return spg;
        }


        public static SpaceGroup getGroupInfoByName(Space sp, string groupName)
        {
            return getGroupInfoByName(sp, groupName, false);
        }

        public static SpaceGroup getGroupInfoByName(Space sp, string groupName, bool internalAllowed)
        {
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            return getGroupInfoByName(sp.SpaceGroups, groupName, internalAllowed);
        }

        public static SpaceGroup getGroupInfoByName(List<SpaceGroup> spaceGroups, string groupName, bool internalAllowed)
        {
            SpaceGroup spg = null;
            
            if (spaceGroups != null && spaceGroups.Count > 0)
            {
                // Never expose internal groups. They will be eventually phased out
                // Possibly supplied to SMIWeb through a webservice                
                foreach (SpaceGroup spaceGroup in spaceGroups)
                {
                    if (!internalAllowed && spaceGroup.InternalGroup)
                    {
                        continue;
                    }

                    if (spaceGroup.Name == groupName)
                    {
                        spg = spaceGroup;
                        break;
                    }
                }
            }
            return spg;
        }

        public static bool isGroupNameDuplicate(Space sp, SpaceGroup group, string newName)
        {
            bool found = false;
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            List<SpaceGroup> spgList = sp.SpaceGroups;
            if (spgList != null && spgList.Count > 0)
            {
                foreach (SpaceGroup spg in spgList)
                {
                    if (spg.InternalGroup)
                        continue;

                    if (group != null && spg.ID.ToString() == group.ID.ToString())
                    {
                        // ofcourse if its the same group, its gonna be a match. duh
                        continue;
                    }
                    if (spg.Name == newName )
                    {
                        found = true;
                        break;
                    }
                }
            }
            return found;
        }

        public static bool isUserGroupMember(SpaceGroup group, string userName)
        {            
            User memberUser = getUserFromGroup(group, userName);
            bool isMember = memberUser != null && memberUser.ID != Guid.Empty && memberUser.Username != null;
            return isMember;
        }

        public static bool isUserGroupMember(SpaceGroup group, Guid userId)
        {
            User memberUser = getUserFromGroup(group, userId);
            bool isMember = memberUser != null && memberUser.ID != Guid.Empty && memberUser.Username != null;
            return isMember;            
        }

        public static User getUserFromGroup(SpaceGroup group, string userName)
        {
            User memberUser = null;
            if (group != null && userName != null)
            {
                List<User> groupUserList = group.GroupUsers;
                if (groupUserList != null && groupUserList.Count > 0)
                {
                    foreach (User u in groupUserList)
                    {
                        if (u.Username.Equals(userName))
                        {
                            memberUser = u;
                            break;
                        }
                    }
                }
            }

            return memberUser;
        }

        public static User getUserFromGroup(SpaceGroup group, Guid userId)
        {
            User memberUser = null;
            if (group != null && userId != Guid.Empty)
            {
                List<User> groupUserList = group.GroupUsers;
                if (groupUserList != null && groupUserList.Count > 0)
                {
                    foreach (User u in groupUserList)
                    {
                        if (u.ID.ToString().Equals(userId.ToString()))
                        {
                            memberUser = u;
                            break;
                        }
                    }
                }
            }

            return memberUser;
        }

        public static List<SpaceGroup> getUserGroups(User u, List<SpaceGroup> allSpaceGroups)
        {
            List<SpaceGroup> userSpaceGroupList = new List<SpaceGroup>();

            if (allSpaceGroups != null && allSpaceGroups.Count > 0 && u != null)
            {
                foreach (SpaceGroup spg in allSpaceGroups)
                {
                    if(UserAndGroupUtils.isUserGroupMember(spg, u.ID))
                    {
                        userSpaceGroupList.Add(spg);
                    }
                }
            }

            return userSpaceGroupList;
        }

        public static bool isGroupACLDefined(SpaceGroup group, string aclTag)
        {
            if (group == null)
            {
                Global.systemLog.Warn("ACL info cannot be retreived for null group");
                return false;
            }

            if (aclTag == null)
            {
                Global.systemLog.Warn("ACL info cannot be retreived for group " + group.ID + " and acl " + aclTag);
                return false;
            }

            bool found = UserAndGroupUtils.isGroupACLDefined(group.ACLIds, aclTag);
            return found;
        }

        public static bool isGroupACLDefined(List<int> aclIds, string ACLTag)
        {
            bool present = false;
            if (aclIds != null && aclIds.Count > 0 && ACLTag != null)
            {
                foreach (int aclId in aclIds)
                {
                    string tag = ACLDetails.getACLTag(aclId);
                    if (tag == ACLTag)
                    {
                        present = true;
                        break;
                    }
                }
            }
            return present;
        }

        /// <summary>
        /// Get groups which has particular ACL defined
        /// </summary>
        /// <param name="spaceGroups"></param>
        /// <param name="aclToLookFor"></param>
        /// <returns></returns>
        public static List<SpaceGroup> getGroupsForGivenACL(List<SpaceGroup> spaceGroups, string aclToLookFor, bool internalGroupAllowed)
        {
            List<SpaceGroup> response = new List<SpaceGroup>();
            if (spaceGroups != null && spaceGroups.Count > 0 && aclToLookFor != null)
            {
                foreach (SpaceGroup g in spaceGroups)
                {
                    if (!internalGroupAllowed && g.InternalGroup)
                        continue;
                    if (UserAndGroupUtils.isGroupACLDefined(g, aclToLookFor))
                    {
                        response.Add(g);
                    }
                }
            }
            return response;
        }

        public static bool isVisualizerPermittedByACL(User user, List<SpaceGroup> spaceGroups)
        {
            return isACLDefinedInAnyGroupForUser(user, spaceGroups, ACLDetails.ACL_VIZUALIZER_ONLY) || isACLDefinedInAnyGroupForUser(user, spaceGroups, ACLDetails.ACL_VISUALIZER_REGULAR);
        }

        public static bool isACLDefinedInAnyGroupForUser(User user, List<SpaceGroup> spaceGroups, string aclTagToLookFor)
        {
            bool allowed = false;
            if (spaceGroups != null && spaceGroups.Count > 0)
            {
                foreach (SpaceGroup g in spaceGroups)
                {
                    if(UserAndGroupUtils.isUserGroupMember(g, user.ID))                    
                    {
                        if (g.ACLIds != null && g.ACLIds.Count > 0)
                        {
                            foreach (int aclId in g.ACLIds)
                            {
                                string groupACLTag = ACLDetails.getACLTag(aclId);
                                if (groupACLTag == aclTagToLookFor)
                                {
                                    allowed = true;
                                    break;
                                }
                            }
                            if (allowed)
                                break;
                        }
                    }
                }
            }

            return allowed;
        }

        public static void addOrRemoveACLForGroup(Space sp, SpaceGroup group, string aclTag, bool access)
        {
            bool foundACL = UserAndGroupUtils.isGroupACLDefined(group, aclTag);
            QueryConnection conn = null; 
            string mainSchema = Util.getMainSchema();
            int aclId = ACLDetails.getACLID(aclTag);

            try
            {
                conn = ConnectionPool.getConnection();
                // if found and to be removed
                if (foundACL && !access)
                {
                    Database.removeACLFromGroup(conn, mainSchema, sp, group.ID, aclId);
                }

                // if not found and to be added
                if (!foundACL && access)
                {
                    Database.addACLToGroup(conn, mainSchema, sp, group.ID, aclId);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        // Convert Acorn data structure to the old datastructure.
        // So that we can use the XMLSerializer to write the same output without doing any 
        // clever hacking

        public static bool saveUserAndGroupToFile(Space sp)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            string writeTime = null;
            try
            {
                conn = ConnectionPool.getConnection();
                DateTime st = Database.getMappingModifiedDate(conn, schema, sp);
                if (st == null || st == DateTime.MinValue)
                {
                    Database.updateMappingModifiedDate(conn, schema, sp);
                    st = Database.getMappingModifiedDate(conn, schema, sp);
                }
                writeTime = st.ToString();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while retrieving MappingModifiedDate for space " + sp.ID, ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return saveUserAndGroupToFile(sp, writeTime);
        }

        public static bool saveUserAndGroupToFile(Space sp, string writeTime)
        {
            bool success = false;
            if (sp == null )
            {
                return false;
            }
                        
            UserAndGroupUtils.populateSpaceGroups(sp, true);

            UserAndGroupTransient usersAndGroups = buildUserAndGroupTransient(sp, writeTime);
            populateOwnersFromSpaceMembership(sp, usersAndGroups);
            /*
            UserAndGroupTransient usersAndGroups = new UserAndGroupTransient();            

            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {
                
                List<Performance_Optimizer_Administration.ACLItem> mafAccessList = new List<Performance_Optimizer_Administration.ACLItem>();
                List<Performance_Optimizer_Administration.Group> mafGroupList = new List<Performance_Optimizer_Administration.Group>();
                List<Performance_Optimizer_Administration.User> mafUserList = new List<Performance_Optimizer_Administration.User>();
                List<string> globalUserNameList = new List<string>();

                List<SpaceGroup> spaceGroupList = sp.SpaceGroups;
                foreach (SpaceGroup spg in spaceGroupList)
                {
                    Performance_Optimizer_Administration.Group mafGroup = new Performance_Optimizer_Administration.Group();
                    mafGroup.Name = spg.Name;
                    mafGroup.InternalGroup = spg.InternalGroup;                   

                    List<string> userList = new List<string>();
                    List<User> groupUserList = spg.GroupUsers;
                    if (groupUserList != null && groupUserList.Count > 0)
                    {
                        foreach (User groupUser in groupUserList)
                        {
                            string uName = groupUser.Username;
                            userList.Add(uName);
                            if (!globalUserNameList.Contains(uName))
                            {                                
                                Performance_Optimizer_Administration.User mafUser = new Performance_Optimizer_Administration.User();
                                mafUser.Username = uName;
                                mafUserList.Add(mafUser);
                                globalUserNameList.Add(uName);                                
                            }
                        }
                    }

                    mafGroup.Usernames = userList.ToArray();
                    mafGroupList.Add(mafGroup);

                    List<int> aclIds = spg.ACLIds;                    
                    if (aclIds != null && aclIds.Count > 0)
                    {                        
                        foreach (int aclId in aclIds)
                        {                            
                            Performance_Optimizer_Administration.ACLItem aclItem = new Performance_Optimizer_Administration.ACLItem();
                            aclItem.Group = spg.Name;
                            // In the new data structure, the presence of aclId means it is valid
                            aclItem.Access = true;
                            aclItem.Tag = ACLDetails.getACLTag(aclId);
                            mafAccessList.Add(aclItem);
                        }
                    }
                }

                                
                usersAndGroups.Groups = mafGroupList.ToArray();
                usersAndGroups.ACL = mafAccessList.ToArray();
                usersAndGroups.Users = mafUserList.ToArray();                
            }

            usersAndGroups.LastEditTime = writeTime;
            */
            string fileName = sp.Directory + "\\" + UserAndGroupUtils.USER_AND_GROUP_FILENAME;
            writeUserGroupMappingFromDbToXMLFile(usersAndGroups, fileName);

            success = true;
            return success;
        }

        private static void populateOwnersFromSpaceMembership(Space sp, UserAndGroupTransient usersAndGroups)
        {
            if (sp != null && usersAndGroups != null)
            {
                Dictionary<string, User> spaceOwners = new Dictionary<string, User>();
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    string schema = Util.getMainSchema();
                    List<SpaceMembership> smList = Database.getUsers(conn, schema, sp, false);
                    if (smList != null && smList.Count > 0)
                    {
                        foreach (SpaceMembership sm in smList)
                        {
                            if (sm.User == null)
                                continue;
                            if (sm.Owner && !spaceOwners.ContainsKey(sm.User.Username)) // used map in case of duplicate entries
                            {
                                spaceOwners.Add(sm.User.Username, sm.User);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception getting ownership details. Putting in blank owner" + ex);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }

                User owner = null;
                if (spaceOwners.Count == 0)
                {
                    Global.systemLog.Error("SPACE OWNERSHIP PROBLEM: No owner found for the space in space membership table for space " + sp.ID);
                }
                else if (spaceOwners.Count > 1)
                {
                    StringBuilder ownersName = new StringBuilder();
                    foreach (string spaceOwner in spaceOwners.Keys)
                    {
                        if (ownersName.Length > 0)
                        {
                            ownersName.Append(",");
                        }
                        ownersName.Append(spaceOwner);
                    }
                    Global.systemLog.Error("SPACE OWNERSHIP PROBLEM: Creating blank OWNER$ groups in usersAndGroups.xml since multiple owners found for the space in space membership table for space " + sp.ID + " : " + ownersName);
                }
                else
                {
                    // only one owner is placed at this point
                    foreach (User user in spaceOwners.Values)
                    {
                        owner = user;
                    }
                }

                if (usersAndGroups.Groups != null && usersAndGroups.Groups.Length > 0)
                {
                    Group ownerGroup = null;
                    foreach (Group group in usersAndGroups.Groups)
                    {
                        if (group.Name == Util.OWNER_GROUP_NAME && group.InternalGroup)
                        {
                            ownerGroup = group;
                            break;
                        }
                    }

                    if (ownerGroup != null)
                    {
                        ownerGroup.Usernames = owner != null ? new string[] { owner.Username } : new string[0];
                    }
                }
            }
        }

        public static UserAndGroupTransient buildUserAndGroupTransient(Space sp, string writeTime)
        {
            UserAndGroupTransient usersAndGroups = new UserAndGroupTransient();

            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {

                List<Performance_Optimizer_Administration.ACLItem> mafAccessList = new List<Performance_Optimizer_Administration.ACLItem>();
                List<Performance_Optimizer_Administration.Group> mafGroupList = new List<Performance_Optimizer_Administration.Group>();
                List<Performance_Optimizer_Administration.User> mafUserList = new List<Performance_Optimizer_Administration.User>();
                List<string> globalUserNameList = new List<string>();

                List<SpaceGroup> spaceGroupList = sp.SpaceGroups;
                foreach (SpaceGroup spg in spaceGroupList)
                {
                    Performance_Optimizer_Administration.Group mafGroup = new Performance_Optimizer_Administration.Group();
                    mafGroup.Name = spg.Name;
                    mafGroup.InternalGroup = spg.InternalGroup;
                    mafGroup.Id = spg.ID.ToString();

                    List<string> userList = new List<string>();
                    List<User> groupUserList = spg.GroupUsers;
                    if (groupUserList != null && groupUserList.Count > 0)
                    {
                        foreach (User groupUser in groupUserList)
                        {
                            string uName = groupUser.Username;
                            userList.Add(uName);
                            if (!globalUserNameList.Contains(uName))
                            {
                                Performance_Optimizer_Administration.User mafUser = new Performance_Optimizer_Administration.User();
                                mafUser.Username = uName;
                                mafUserList.Add(mafUser);
                                globalUserNameList.Add(uName);
                            }
                        }
                    }

                    mafGroup.Usernames = userList.ToArray();
                    mafGroupList.Add(mafGroup);

                    List<int> aclIds = spg.ACLIds;
                    if (aclIds != null && aclIds.Count > 0)
                    {
                        foreach (int aclId in aclIds)
                        {
                            Performance_Optimizer_Administration.ACLItem aclItem = new Performance_Optimizer_Administration.ACLItem();
                            aclItem.Group = spg.Name;
                            // In the new data structure, the presence of aclId means it is valid
                            aclItem.Access = true;
                            aclItem.Tag = ACLDetails.getACLTag(aclId);
                            mafAccessList.Add(aclItem);
                        }
                    }
                }


                usersAndGroups.Groups = mafGroupList.ToArray();
                usersAndGroups.ACL = mafAccessList.ToArray();
                usersAndGroups.Users = mafUserList.ToArray();
            }

            usersAndGroups.LastEditTime = writeTime;
            usersAndGroups.SpaceID = sp.ID.ToString();
            return usersAndGroups;
        }

        public static void writeUserGroupMappingFromDbToXMLFile(UserAndGroupTransient userAndGroups, string fileName)
        {
            if (fileName == null || fileName.Length == 0)
            {
                Global.systemLog.Warn("No fileName supplied for writing userAndGroup information");
                return;
            }

            string tmpFileName = fileName + ".tmp";
            TextWriter writer = null;
            XmlSerializer serializer = null;
            try
            {
                if (File.Exists(tmpFileName))
                {
                    File.Delete(tmpFileName);
                }
                writer = new StreamWriter(tmpFileName);
                serializer = new XmlSerializer(typeof(UserAndGroupTransient));
                userAndGroups.Version = USER_GROUP_FILE_VERSION;
                serializer.Serialize(writer, userAndGroups);
                writer.Close();                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to write " + tmpFileName, ex);
                try
                {                    
                    if (writer != null)
                    {
                        writer.Close();
                    }                    
                }
                catch (Exception)
                {
                    Global.systemLog.Warn("Unable to close writer.");
                }

                throw ex;
            }
            
            try
            {
                Util.FileCopy(tmpFileName, fileName, true);                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while trying to copy " + tmpFileName + " to " + fileName);
                try
                {
                    if (tmpFileName != null && File.Exists(tmpFileName))
                    {
                        File.Delete(tmpFileName);
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Unable to delete tmp File " + tmpFileName + " ", ex2);
                }
                throw ex;
            }
        }

        public static string getLastModifiedTimeFromMappingFile(UserAndGroupTransient uAg, out int version)
        {
            string lastEditTime = null;
            version = -1;             
            if (uAg != null && uAg.LastEditTime != null)
            {
                lastEditTime = uAg.LastEditTime;
                version = uAg.Version;
            }

            return lastEditTime;
        }

        public static UserAndGroupTransient readFromUserGroupMappingFile(Space sp)
        {
           return readFromUserGroupMappingFile(sp, null);
        }
        /// <summary>
        /// Create and return the data structure after reading from mapping file in the space directory
        /// </summary>
        ///         
        public static UserAndGroupTransient readFromUserGroupMappingFile(Space sp, string userAndGroupMappingFileName)
        {
            UserAndGroupTransient uAg = null;
            if (userAndGroupMappingFileName == null)
            {
                userAndGroupMappingFileName = sp.Directory + "\\" + UserAndGroupUtils.USER_AND_GROUP_FILENAME;
            }
            FileInfo fileInfo = new FileInfo(userAndGroupMappingFileName);
            if (!fileInfo.Exists)
            {
                Global.systemLog.Error("Unable to find the file " + userAndGroupMappingFileName);
                return uAg;
            }

            TextReader reader = null; 
            XmlReader xreader = null; 
            XmlSerializer serializer = null;  

            try
            {
                reader = new StreamReader(new FileStream(userAndGroupMappingFileName, FileMode.Open, FileAccess.Read));
                xreader = new XmlTextReader(reader);
                serializer = new XmlSerializer(typeof(UserAndGroupTransient));
                uAg = (UserAndGroupTransient)serializer.Deserialize(xreader);
                xreader.Close();
                reader.Close();                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while reading/deserializing file : " + userAndGroupMappingFileName);
                try{
                    if(xreader != null)
                    {
                        xreader.Close();
                    }
                    else if(reader != null)
                    {
                        reader.Close();
                    }
                }catch(Exception)
                {
                    // Nothing to do. Let System close resources
                }               
                                
                throw ex;
            }

            return uAg;
        }

        /// <summary>
        /// Write user group mapping file if the spaces table in the database has a 
        /// greater timespace than the LastEditTime string in existing mapping file. 
        /// MappingModifiedDate column in spaces table is updated after every db transaction involving
        /// group/user/acl 
        /// </summary>
        /// <param name="sp">Space</param>
        public static void updatedUserGroupMappingFile(Space sp)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            DateTime dbMappingModifiedDate = DateTime.MinValue;
            DateTime fileDt = DateTime.MinValue;
            try
            {
                conn = ConnectionPool.getConnection();
                // Get the mapping modified time stamp from db at the space level
                dbMappingModifiedDate = Database.getMappingModifiedDate(conn, schema, sp);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(" Error in getting MappingModifiedDate from db", ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            // Get the modified time stamp from the existing mapping file 
            int lastSavedVersion = -1;
            UserAndGroupTransient uAg = UserAndGroupUtils.readFromUserGroupMappingFile(sp);
            string lastEditTime = UserAndGroupUtils.getLastModifiedTimeFromMappingFile(uAg, out lastSavedVersion);            
            if (lastEditTime != null)
            {
                try
                {
                    fileDt = DateTime.Parse(lastEditTime);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Debug("Error while parsing date time (" + lastEditTime + ") from mapping file for space " + sp.ID, ex);
                }
            }

            // Write the file only if db time stamp is later than the file edit timestamp
            TimeSpan ts = dbMappingModifiedDate - fileDt;
            bool same = (ts.Hours == 0 && ts.Minutes == 0 && ts.Seconds == 0);
            bool modify = !same || lastSavedVersion <= 0;
            modify = modify || !spaceIDMatched(sp, uAg);
            if (modify)
            {
                UserAndGroupUtils.saveUserAndGroupToFile(sp, dbMappingModifiedDate.ToString());
            }            
        }

        private static bool spaceIDMatched(Space sp, UserAndGroupTransient uAg)
        {
            if (sp != null && uAg != null && uAg.SpaceID != null && uAg.SpaceID.Trim().Length > 0)
            {
                return (sp.ID.ToString() == uAg.SpaceID);
            }
            return false;
        }

        internal static SpaceGroup getSpaceGroup(List<SpaceGroup> spaceGroups, string groupName, bool isGroupInternal)
        {
            if (spaceGroups != null && spaceGroups.Count > 0)
            {
                foreach (SpaceGroup spg in spaceGroups)
                {
                    if (spg.InternalGroup == isGroupInternal && spg.Name == groupName)
                    {
                        return spg;
                    }
                }
            }
            return null;
        }
    }

    [XmlRoot(ElementName="UsersAndGroups", Namespace = "http://www.successmetricsinc.com", IsNullable = false)]
    public class UserAndGroupTransient : Performance_Optimizer_Administration.BaseObject
    {
        public string SpaceID;
        public string LastEditTime;
        public int Version;
        public Performance_Optimizer_Administration.Group[] Groups;
        public Performance_Optimizer_Administration.User[] Users;
        public Performance_Optimizer_Administration.ACLItem[] ACL;        

        public UserAndGroupTransient()
        {
        }
    }

}
