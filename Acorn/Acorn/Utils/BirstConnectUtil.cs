﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Configuration;
using System.Text;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace Acorn.Utils
{
    public class BirstConnectUtil
    {
        public static int hasAnyRealtimeConnectionInUse(Space sp, MainAdminForm maf, string filename)
        {
            //response code
            // 0 = file does not have any realtime connection in use
            // 1 = file has one/more realtime connection in use
            // -1 = exception @ server
            int responseCode = 1;
            try
            {
                if (sp != null && maf != null)
                {
                    string dcfile = sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + filename;
                    string[] dcfiles = new string[] { dcfile };
                    List<Connection> rcList = getRealtimeConnectionList(sp, dcfiles);
                    if (rcList.Count == 0)
                    {
                        responseCode = 0;
                    }
                    else
                    {
                        bool connInUse = false;
                        foreach (Connection rc in rcList)
                        {
                            if (maf.isConnectionExist(rc.Name) && maf.isConnectionInUse(rc.Name))
                            {
                                connInUse = true;
                                break;
                            }
                        }
                        responseCode = connInUse ? 1 : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while checking realtime connections status in  " + filename + " for space " + sp.Directory, ex);
                responseCode = -1;
            }
            return responseCode;
        }

        public static void deleteBirstConnectConfig(Space sp, string filename)
        {
            try
            {
                if (sp == null)
                    return;
                string dcdir = Path.Combine(sp.Directory, Util.DCCONFIG_DIR);
                string dcfile = Path.Combine(dcdir, filename);
                Util.validateFileLocation(dcdir, dcfile);
                if (File.Exists(dcfile))
                {
                    File.Delete(dcfile);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while deleting file " + filename + " for " + sp.Directory, ex);
            }
        }

        public static void addBirstConnectConfig(Space sp, string filename)
        {
            try
            {
                if (sp == null)
                    return;
                string dcDir = Path.Combine(sp.Directory, Util.DCCONFIG_DIR);
                if (Directory.Exists(dcDir))
                {
                    string file = Path.Combine(dcDir, filename);
                    Util.validateFileLocation(dcDir, file);
                    if (!File.Exists(file))
                    {
                        FileStream fs = File.Create(file);
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while creating file " + filename + " for " + sp.Directory, ex);
            }
        }

        public static void renameBirstConnectConfig(Space sp, string oldName, string newName)
        {
            try
            {
                if (sp == null)
                    return;
                string dcDir = Path.Combine(sp.Directory, Util.DCCONFIG_DIR);
                string sourcefile = Path.Combine(dcDir, oldName);
                string targetfile = Path.Combine(dcDir, newName);
                Util.validateFileLocation(dcDir, sourcefile);
                Util.validateFileLocation(dcDir, targetfile);
                if (File.Exists(sourcefile) && !File.Exists(targetfile))
                {
                    File.Move(sourcefile, targetfile);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while renaming file from " + oldName + " to " + newName + " for " + sp.Directory, ex);
            }
        }
       
        public static BirstConnectConfig[] getBirstConnectConfigs(Space sp)
        {
            List<BirstConnectConfig> configList = new List<BirstConnectConfig>();
            //always add default dcconfig
            BirstConnectConfig config = new BirstConnectConfig(Util.DCCONFIG_FILE_NAME, "Default");
            configList.Add(config);
            try
            {
                if (sp != null)
                {
                    string configDirPath = sp.Directory + "\\" + Util.DCCONFIG_DIR;
                    if (Directory.Exists(configDirPath))
                    {
                        string[] files = Directory.GetFiles(configDirPath, "*_dcconfig.xml");
                        if (files != null)
                        {
                            string filename = null;
                            int index = -1;
                            for (int i = 0; i < files.Length; i++)
                            {
                                filename = new FileInfo(files[i]).Name;
                                index = filename.IndexOf("_" + Util.DCCONFIG_FILE_NAME);
                                if (index > 0)
                                {
                                    string visiblename = filename.Substring(0, index);
                                    config = new BirstConnectConfig(filename, visiblename);
                                    configList.Add(config);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting dcconfig files for " + sp.Directory, ex);
            }
            return configList.ToArray(); ;
        }

        public static Dictionary<string, LocalETLConfig> getLocalETLConfigsForSpace(Space sp)
        {
            Dictionary<string, LocalETLConfig> localETLConfigs = new Dictionary<string, LocalETLConfig>();
            string dcDirPath = Path.Combine(sp.Directory, Util.DCCONFIG_DIR);
            string dcFile = Path.Combine(sp.Directory, "dcconfig.xml");
            string[] dcconfigFiles = null;
            if (Directory.Exists(dcDirPath))
                dcconfigFiles = Directory.GetFiles(dcDirPath, "*dcconfig.xml");
            else if (File.Exists(dcFile))
                dcconfigFiles = new string[] { dcFile };

            if (dcconfigFiles != null)
            {
                foreach (string dcfile in dcconfigFiles)
                {
                    if (File.Exists(dcfile))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.XmlResolver = null;
                        string config = File.ReadAllText(dcfile, Encoding.UTF8);
                        int index = config.IndexOf("<object ");
                        if (index >= 0)
                        {
                            doc.LoadXml(config.Substring(index));
                            XmlNode node = doc.FirstChild;
                            if (node.Attributes[0].Value == "com.birst.dataconductor.DataConductorConfig")
                            {
                                XmlNode configNode = null;
                                foreach (XmlNode cnode in node.ChildNodes)
                                {
                                    if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "localETLConfig")
                                    {
                                        configNode = cnode;
                                        break;
                                    }
                                }
                                if (configNode != null) // local etl config is found
                                {
                                    string leLoadGroup = null, leConn = null, appDir = null;
                                    bool useLEC = false;
                                    foreach (XmlNode cnode in configNode.ChildNodes)
                                    {
                                        if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "localETLLoadGroup")
                                        {
                                            leLoadGroup = cnode.ChildNodes[0].InnerText;
                                        }
                                        else if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "localETLConnection")
                                        {
                                            leConn = cnode.ChildNodes[0].InnerText;
                                        }
                                        else if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "applicationDirectory")
                                        {
                                            appDir = cnode.ChildNodes[0].InnerText;
                                        }
                                        else if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "useLocalETLConnection")
                                        {
                                            useLEC = Boolean.Parse(cnode.ChildNodes[0].InnerText);
                                        }
                                    }
                                    if (leLoadGroup != null && leLoadGroup.Length > 0 && useLEC && leConn != null && appDir != null)
                                    {
                                        LocalETLConfig leConfig = new LocalETLConfig(leLoadGroup, useLEC, appDir, leConn);
                                        localETLConfigs.Add(leLoadGroup, leConfig);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return localETLConfigs;
        }

        public static void moveFileToDirectory(Space sp, string targetDirName, string fileName)
        {
            try
            {
                if (sp == null)
                    return;
                string targetDir = Path.Combine(sp.Directory, targetDirName);
                Util.validateFileLocation(sp.Directory, targetDir);
                if (!Directory.Exists(targetDir))
                    Directory.CreateDirectory(targetDir);
                FileInfo fis = new FileInfo(Path.Combine(sp.Directory, fileName));
                FileInfo ftarget = new FileInfo(Path.Combine(targetDir, fileName));
                if (fis.Exists && !ftarget.Exists)
                {
                    fis.MoveTo(Path.Combine(targetDir, fileName));
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while moving file " + fileName + " to " + targetDirName + " directory for " + sp.Directory, ex);
            }
        }

        public static void updateSchemaNameForLocalLiveAccessConnections(Space sp, string oldSchemaName, string newSchemaName)
        {
            try
            {
                if (sp != null)
                {
                    string[] files = Directory.GetFiles(Path.Combine(sp.Directory, Util.DCCONFIG_DIR), "*dcconfig.xml");
                    foreach (string filename in files)
                    {
                        if (File.Exists(filename))
                        {
                            string config = File.ReadAllText(filename, Encoding.UTF8);
                            config = config.Replace(oldSchemaName, newSchemaName);
                            File.WriteAllText(filename, config);
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while updating schemaname for local liveaccess connections for space " + sp.ID.ToString(), ex);
            }
        }

        //method parses dcconfigs and returns realtime connections
        public static List<Connection> getRealtimeConnectionList(Space sp, string[] dcconfigFiles)
        {
            List<Connection> rcList = new List<Connection>();
            try
            {
                if (sp != null)
                {
                    foreach (string dcfile in dcconfigFiles)
                    {
                        if (File.Exists(dcfile))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.XmlResolver = null;
                            string config = File.ReadAllText(dcfile, Encoding.UTF8);
                            int index = config.IndexOf("<object ");
                            if (index >= 0)
                            {
                                doc.LoadXml(config.Substring(index));
                                XmlNode node = doc.FirstChild;
                                if (node.Attributes[0].Value == "com.birst.dataconductor.DataConductorConfig")
                                {
                                    XmlNode connectNode = null;
                                    foreach (XmlNode cnode in node.ChildNodes)
                                    {
                                        if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "realtimeConnections")
                                        {
                                            connectNode = cnode;
                                            break;
                                        }
                                    }
                                    if (connectNode != null)
                                    {
                                        foreach (XmlNode cnode in connectNode.ChildNodes)
                                        {
                                            string visiblename = null;
                                            XmlNode onode = null;
                                            foreach (XmlNode inode in cnode.ChildNodes)
                                            {
                                                if (inode.Name == "string")
                                                    visiblename = inode.InnerText;
                                                else if (inode.Name == "object")
                                                    onode = inode;
                                            }
                                            Connection rc = new Connection(null, null, true);
                                            rcList.Add(rc);
                                            bool isGeneric = false;
                                            bool useDirectConnection = false;
                                            foreach (XmlNode inode in onode.ChildNodes)
                                            {
                                                if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "databaseName")
                                                    rc.Database = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "databaseType")
                                                    rc.Type = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "driverName")
                                                    rc.Driver = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "serverName")
                                                    rc.Server = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "port")
                                                    rc.Port = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "username")
                                                    rc.UserName = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "password")
                                                    rc.Password = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "name")
                                                    rc.Name = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "schema")
                                                    rc.Schema = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "genericDriver")
                                                {
                                                    Boolean.TryParse(inode.ChildNodes[0].InnerText, out isGeneric);
                                                    rc.GenericDatabase = isGeneric;
                                                }
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "connectString")
                                                    rc.ConnectString = inode.ChildNodes[0].InnerText;
                                                else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "useDirectConnection")
                                                {
                                                    Boolean.TryParse(inode.ChildNodes[0].InnerText, out useDirectConnection);
                                                    rc.useDirectConnection = useDirectConnection;
                                                } 
                                            }
                                            // Local in-memory database
                                            if (rc.Type == "In-memory Database" && rc.ConnectString == null)
                                                rc.ConnectString = "inprocess";
                                            //set rc.visibleName only if we get name property from dcconfig.xml
                                            if (rc.Name == null)
                                                rc.Name = visiblename;
                                            else
                                                rc.VisibleName = visiblename;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while reterieving realtime connections for " + sp.Directory, ex);
            }
            return rcList;
        }

        public static bool hasLocalETLConnections(Space sp)
        {
            Dictionary<string, LocalETLConfig> localETLConnectionsMap = getLocalETLConfigsForSpace(sp);
            if (localETLConnectionsMap != null && localETLConnectionsMap.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static Dictionary<string, Connection> getLocalETLConenctions(Space sp, MainAdminForm maf)
        {
            Dictionary<string, Connection> localConnections = new Dictionary<string, Connection>();
            if (sp == null || maf == null || maf.connection == null)
            {
                Global.systemLog.Debug("Unable to retrieve Local ETL connections" + (sp != null ? " for space " + sp.ID.ToString() : ""));
                return localConnections;
            }
            try
            {
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                foreach (KeyValuePair<string, LocalETLConfig> localETLConfig in localETLConfigs)
                {
                    LocalETLConfig config = localETLConfig.Value;
                    string connName = config.getLocalETLConnection();
                    DatabaseConnection dc = maf.connection.findConnection(connName);
                    if (dc == null)
                    {
                        Global.systemLog.Debug("Local ETL connection with name \"" + connName + "\" not found for space " + sp.ID.ToString()); 
                        continue;
                    }
                    Connection rc = new Connection(dc);
                    rc.LoadGroup = config.getLocalETLLoadGroup();
                    rc.LocalETL = true;
                    localConnections.Add(rc.VisibleName, rc);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting Local ETL connections for space " + sp.ID.ToString(), ex);
            }
            return localConnections;
        }

        public static LocalETLConfig getLocalETLConfig(Space sp, string connectionName)
        {
            if (sp == null || connectionName == null || connectionName.Trim().Length == 0)
                return null;
            try
            {
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                foreach (KeyValuePair<string, LocalETLConfig> localETLConfig in localETLConfigs)
                {
                    LocalETLConfig config = localETLConfig.Value;
                    string connName = config.getLocalETLConnection();
                    if (connName == connectionName)
                        return config;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting Local ETL config for space " + sp.ID + " using connection " + connectionName, ex);
            }
            return null;
        }

        public static StringBuilder getLiveAccessTimeCommands(Space sp, int loadNumber, string applicationPath)
        {
            StringBuilder commands = new StringBuilder();
            string liveAccessDirectory = applicationPath + "\\" + sp.ID;
            commands.Append("repository \"" + liveAccessDirectory + "\\repository_dev.xml\"\n");
            commands.Append("generatetimeschema " + loadNumber + "\n");
            return commands;
        }

        public static StringBuilder getLiveAccessOverrideProperties(Space sp, MainAdminForm maf, string applicationPath, string liveAccessConnectionName, bool useDefaultSchema)
        {
            StringBuilder overrideProperties = new StringBuilder();

            string liveAccessDirectory = applicationPath.Replace("\\", "\\\\") + "\\\\" + sp.ID;

            overrideProperties.Append("ApplicationPath=" + liveAccessDirectory + "\n");
            overrideProperties.Append("CachePath=" + liveAccessDirectory + "\\\\cache\n");
            overrideProperties.Append("UseCacheForRepositoryInitialization=false\n");
            overrideProperties.Append("UseMailForNotification=false\n");
            overrideProperties.Append("DoNotEndJvmOnCompletion=true\n");

            DatabaseConnection liveAccessConnection = null;
            List<DatabaseConnection> dcList = maf.connection.connectionList;
            if (dcList != null && dcList.Count > 0)
            {
                foreach (DatabaseConnection conn in dcList)
                {
                    if (conn.Name == liveAccessConnectionName)
                    {
                        liveAccessConnection = conn;
                        break;
                    }
                }
            }
            if (liveAccessConnection != null)
            {
                overrideProperties.Append("DatabaseConnection.Default+Connection.Disabled=true\n");
                overrideProperties.Append("DatabaseConnection." + liveAccessConnectionName.Replace(" ", "+") + ".Realtime=false\n");
                overrideProperties.Append("DefaultConnection=" + liveAccessConnectionName + "\n");

                if (useDefaultSchema)
                {
                    overrideProperties.Append("DatabaseConnection." + liveAccessConnectionName.Replace(" ", "+") + ".Schema=dbo\n");
                }
                else
                {
                    overrideProperties.Append("DatabaseConnection." + liveAccessConnectionName.Replace(" ", "+") + ".Schema=" + sp.Schema + "\n");
                }
            }

            return overrideProperties;
        }

        public static bool equalsLocalConnections(Space sp1, MainAdminForm maf1, Space sp2, MainAdminForm maf2)
        {
            Dictionary<string, Connection> connList1 = getLocalETLConenctions(sp1, maf1);
            Dictionary<string, Connection> connList2 = getLocalETLConenctions(sp2, maf2);
            if (connList1.Count != connList2.Count)
                return false;
            foreach (string key in connList1.Keys)
            {
                if (!connList2.ContainsKey(key))
                    return false;
                Connection conn1 = connList1[key];
                Connection conn2 = connList2[key];
                if (!conn1.EqualsSpecial(conn2, true))
                    return false;
            }
            return true;
        }

        public static bool setUpGenericJDBCRealTimeConnectionForSpace(User u, Space sp, string configFileName, string connectionName,
                    bool useDirectConnection, string sqlType, string driverName, string connectionString, string filter, string userName, string password, int timeout)
        {
            bool newRepository;
            MainAdminForm maf = Util.loadRepository(sp, out newRepository);
            foreach (Performance_Optimizer_Administration.DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Name == connectionName)
                {
                    throw new Exception("A connection with name " + connectionName + " already exists in space, cannot setup realtime connection with same name");
                }
            }
            if (configFileName == null)
                configFileName = Util.DCCONFIG_FILE_NAME;

            string filename = Path.Combine(sp.Directory, "dcconfig", configFileName);
            if (!Util.checkFileExistsWithException(filename))
            {
                setupEmptyDCConfig(filename);
            }
            Connection rc = new Connection();
            rc.Realtime = true;
            rc.GenericDatabase = true;
            rc.Name = connectionName;
            rc.VisibleName = connectionName;
            rc.useDirectConnection = useDirectConnection;
            rc.Driver = driverName;
            rc.ConnectString = connectionString;
            rc.Type = sqlType;
            rc.UserName = userName;
            rc.Password = password;
            rc.Timeout = timeout.ToString();
            return addRealTimeConnection(u, sp, filename, rc, filter);            
        }

        public static bool setUpRealTimeConnectionForSpace(User u, Space sp, string configFileName, string connectionName,
                    string databaseType, bool useDirectConnection, string host, int port, string databaseName, string userName, string password, int timeout)
        {
            bool newRepository;
            MainAdminForm maf = Util.loadRepository(sp, out newRepository);
            foreach (Performance_Optimizer_Administration.DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Name == connectionName)
                {
                    throw new Exception("A connection with name " + connectionName + " already exists in space, cannot setup realtime connection with same name");
                }
            }
            if (configFileName == null)
                configFileName = Util.DCCONFIG_FILE_NAME;

            string filename = Path.Combine(sp.Directory, "dcconfig", configFileName);
            if (!Util.checkFileExistsWithException(filename))
            {
                setupEmptyDCConfig(filename);
            }
            Connection rc = new Connection();
            rc.Realtime = true;
            rc.Name = connectionName;
            rc.VisibleName = connectionName;
            rc.useDirectConnection = useDirectConnection;
            rc.Type = databaseType;
            rc.Server = host;
            rc.Port = port.ToString();
            rc.Database = databaseName;
            rc.UserName = userName;
            rc.Password = password;
            rc.Timeout = timeout.ToString();
            return addRealTimeConnection(u, sp, filename, rc, null);            
        }

        private static bool addRealTimeConnection(User u, Space sp, string configFileName, Connection rc, string filter)
        {
            if (!rc.GenericDatabase)
            {
                Performance_Optimizer_Administration.ConnectionForm.setConnectionDriver(rc);                
            }
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            string config = File.ReadAllText(configFileName, Encoding.UTF8);
            int index = config.IndexOf("<object ");
            if (index >= 0)
            {
                doc.LoadXml(config.Substring(index));
                XmlNode node = doc.FirstChild;
                if (node.Attributes[0].Value == "com.birst.dataconductor.DataConductorConfig")
                {
                    XmlNode connectionsNode = null;
                    foreach (XmlNode cnode in node.ChildNodes)
                    {
                        if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "realtimeConnections")
                        {
                            connectionsNode = cnode;
                            break;
                        }
                    }
                    if (connectionsNode != null)
                    {
                        XmlDocument temp = new XmlDocument();
                        temp.XmlResolver = null;
                        temp.LoadXml(getRealTimeConnectionXML(rc, filter));
                        XmlNode new_node = connectionsNode.OwnerDocument.ImportNode(temp.DocumentElement, true);
                        connectionsNode.AppendChild(new_node);
                        string tfname = "tempdconfig" + DateTime.Now.ToString("MMddyyyyHHmmssfff") + ".xml";
                        string tempFilePath = Path.Combine(sp.Directory, "dcconfig", tfname);
                        doc.Save(tempFilePath);
                        config = config.Substring(0, index) + File.ReadAllText(tempFilePath, Encoding.UTF8);
                        File.WriteAllText(configFileName, config);
                        File.Delete(tempFilePath);
                        MainAdminForm maf = Util.setSpace(null, sp, u);
                        Util.saveApplication(maf, sp, null, u);
                        return true;
                    }
                }
            }
            return false;
        }

        private static void setupEmptyDCConfig(string filename)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").AppendLine();
            xml.Append("<java version=\"1.7.0_17\" class=\"java.beans.XMLDecoder\">").AppendLine();
            xml.Append("<object class=\"com.birst.dataconductor.DataConductorConfig\">").AppendLine();
                xml.Append("  <void property=\"realtimeConnections\" />").AppendLine();
            xml.Append("</object>");
            StreamWriter sw = new StreamWriter(filename, false, Encoding.UTF8);
            sw.Write(xml.ToString());
            sw.Flush();
            sw.Close();
        }

        public static bool importCubeMetaDataIntoSpace(User u, Space sp, string connectionName, string databaseType, string cubeName, int importType, bool cacheable)
        {
            MainAdminForm maf = Acorn.Util.setSpace(null, sp, u);
            RARepository rap = Acorn.Util.getRepository(maf);
            object[][] res = LiveAccessUtil.getCube(null, u, sp, connectionName, cubeName, importType);
            if (res == null)
                throw new Exception("Unable to import cube:" + cubeName);
            if (res != null && res.Length > 0)
            {
                Dictionary<string, RARepository.RAMeasureTable> mtables = new Dictionary<string, RARepository.RAMeasureTable>();
                Dictionary<string, RARepository.RADimensionTable> dtables = new Dictionary<string, RARepository.RADimensionTable>();
                Dictionary<string, List<string>> lmap = new Dictionary<string, List<string>>();
                List<string> llist = null;
                string cube = null;
                string originaldname = null;
                string originalhname = null;
                string originalcname = null;
                string dtype = null;
                string cname = null;
                string logicalcname = null;
                string dname = null;
                string logicaldname = null;
                string hname = null;
                string logicalhname = null;
                string lUniqueName = null;

                Dictionary<string, List<string>> dimList = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> dimHierarchies = new Dictionary<string, List<string>>();
                Dictionary<string, Dictionary<string, List<string>>> hLevels = new Dictionary<string, Dictionary<string, List<string>>>();
                for (int i = 0; i < res.Length; i++)
                {
                    cube = (string)res[i][0];
                    originaldname = (string)res[i][1];
                    if (originaldname == "Measures")
                        continue;
                    if (res.Length <= 6)
                    {
                        originalhname = (string)res[i][2];
                        originalcname = (string)res[i][3];
                        dtype = (string)res[i][4];
                    }
                    else
                    {
                        logicaldname = Util.replaceUnSupportedColChars((string)res[i][2], sp.QueryLanguageVersion);
                        originalhname = (string)res[i][3];
                        logicalhname = Util.replaceUnSupportedColChars((string)res[i][4], sp.QueryLanguageVersion);
                        originalcname = (string)res[i][5];
                        logicalcname = Util.replaceUnSupportedColChars((string)res[i][6], sp.QueryLanguageVersion);
                        lUniqueName = (string)res[i][7];
                        dtype = (string)res[i][8];
                    }
                    cname = Util.replaceUnSupportedColChars(originalcname, sp.QueryLanguageVersion);
                    dname = Util.replaceUnSupportedColChars(originaldname, sp.QueryLanguageVersion);
                    hname = Util.replaceUnSupportedColChars(originalhname, sp.QueryLanguageVersion);
                    if (res.Length <= 6)
                    {
                        logicaldname = dname;
                        logicalhname = hname;
                        logicalcname = cname;
                    }
                    if (originalcname.Equals("(All)"))
                        continue;
                    if (originalcname.Equals("Levels(0)") || originalcname == "LEVEL00")
                        continue;
                    if (importType == Performance_Optimizer_Administration.MainAdminForm.RELATIONAL_DIMENSIONAL_STRUCTURE)
                    {
                        string lkey = dname + (hname == null || hname.ToLower() == "null" ? "" : "." + hname);
                        llist = null;
                        if (lmap.ContainsKey(lkey))
                            llist = lmap[lkey];
                        if (llist == null)
                        {
                            llist = new List<string>();
                            lmap[lkey] = llist;
                        }
                        llist.Add(logicalcname);
                    }
                    else if (importType == Performance_Optimizer_Administration.MainAdminForm.OLAP_DIMENSIONAL_STRUCTURE)
                    {
                        if (!dimList.ContainsKey(dname))
                        {
                            dimHierarchies.Add(dname, new List<string>());
                            dimList.Add(dname, dimHierarchies[dname]);
                        }
                        if (hname == null || hname.ToLower() == "null")
                        {
                            hname = dname;
                        }
                        if (!dimHierarchies[dname].Contains(hname))
                        {
                            if (!hLevels.ContainsKey(dname))
                            {
                                hLevels[dname] = new Dictionary<string, List<string>>();
                            }
                            if (!hLevels[dname].ContainsKey(hname))
                            {
                                hLevels[dname][hname] = new List<string>();
                            }
                            dimHierarchies[dname].Add(hname);
                        }
                        hLevels[dname][hname].Add(logicalcname);
                    }
                }
                for (int i = 0; i < res.Length; i++)
                {
                    cube = (string)res[i][0];
                    originaldname = (string)res[i][1];
                    string aggrule = null;
                    string colwidth = null;

                    if (res.Length <= 6)//cube being imported using old dataconductor - do not have logicalnames
                    {
                        originalhname = (string)res[i][2];
                        originalcname = (string)res[i][3];
                        dtype = (string)res[i][4];
                        if (originaldname == "Measures")
                            aggrule = (string)res[i][5];
                        else
                            colwidth = (string)res[i][5];
                    }
                    else
                    {
                        logicaldname = Util.replaceUnSupportedColChars((string)res[i][2], sp.QueryLanguageVersion);
                        if (originaldname == "Measures")
                        {
                            originalcname = (string)res[i][3];
                            logicalcname = Util.replaceUnSupportedColChars((string)res[i][4], sp.QueryLanguageVersion);
                            dtype = (string)res[i][5];
                            aggrule = (string)res[i][6];
                        }
                        else
                        {
                            originalhname = (string)res[i][3];
                            logicalhname = Util.replaceUnSupportedColChars((string)res[i][4], sp.QueryLanguageVersion);
                            originalcname = (string)res[i][5];
                            logicalcname = Util.replaceUnSupportedColChars((string)res[i][6], sp.QueryLanguageVersion);
                            lUniqueName = (string)res[i][7];
                            dtype = (string)res[i][8];
                            colwidth = (string)res[i][9];
                        }
                    }
                    cname = Util.replaceUnSupportedColChars(originalcname, sp.QueryLanguageVersion);
                    dname = Util.replaceUnSupportedColChars(originaldname, sp.QueryLanguageVersion);
                    hname = Util.replaceUnSupportedColChars(originalhname, sp.QueryLanguageVersion);
                    if (res.Length <= 6)
                    {
                        logicaldname = dname;
                        logicalhname = hname;
                        logicalcname = cname;
                    }
                    string lkey = dname + (hname == null || hname.ToLower() == "null" ? "" : "." + hname);
                    if (originalcname.Equals("(All)"))
                        continue;
                    if (originalcname.Equals("Levels(0)") || originalcname == "LEVEL00")
                        continue;
                    string dimension = dname;
                    if (dimension.Equals("Measures"))
                    {
                        string tname = "Cube: " + cube + ", Measures";
                        RARepository.RAMeasureTable mt = null;
                        if (mtables.ContainsKey(tname))
                            mt = mtables[tname];
                        if (mt == null)
                        {
                            if (rap.MeasureTables != null)
                            {
                                foreach (RARepository.RAMeasureTable smt in rap.MeasureTables)
                                {
                                    if (smt.Name.Equals(tname))
                                    {
                                        mt = smt;
                                        mtables[tname] = mt;
                                        break;
                                    }
                                }
                            }
                        }
                        // Create a measure table if necessary
                        if (mt == null)
                        {
                            mt = new RARepository.RAMeasureTable();
                            mt.Name = tname;
                            mtables[tname] = mt;
                            mt.Type = 0;
                            mt.TTL = -1;
                            mt.Cardinality = 0;
                            mt.Cacheable = cacheable;
                            mt.Source = new TableSource();
                            mt.Source.Connection = connectionName;
                            TableSource.TableDefinition td = new TableSource.TableDefinition();
                            mt.Source.Tables = new TableSource.TableDefinition[] { td };
                            td.JoinType = 0;
                            td.JoinClause = "";
                            td.PhysicalName = cube;
                            if (rap.MeasureTables != null)
                            {
                                List<RARepository.RAMeasureTable> lst = rap.MeasureTables.ToList<RARepository.RAMeasureTable>();
                                lst.Add(mt);
                                rap.MeasureTables = lst.ToArray<RARepository.RAMeasureTable>();
                            }
                            else
                            {
                                rap.MeasureTables = new RARepository.RAMeasureTable[] { mt };
                            }
                        }
                        string agg = "SUM";
                        if (aggrule.ToUpper().Equals("AVG"))
                            agg = "AVG";
                        else if (aggrule.ToUpper().Equals("MIN"))
                            agg = "MIN";
                        else if (aggrule.ToUpper().Equals("MAX"))
                            agg = "MAX";
                        else if (aggrule.ToUpper().Equals("COUNT"))
                            agg = "COUNT";
                        else if (aggrule.ToUpper().Equals("COUNT DISTINCT"))
                            agg = "COUNT DISTINCT";
                        addMeasureIfNecessary(rap, logicalcname, agg, dtype, 0);
                        bool found = false;
                        RARepository.RAMeasureColumnMapping mc = null;
                        if (mt.Mappings != null)
                        {
                            foreach (RARepository.RAMeasureColumnMapping mcmp in mt.Mappings)
                            {
                                if (mcmp.Formula.Equals(originalcname))
                                {
                                    mc = mcmp;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            mc = new RARepository.RAMeasureColumnMapping();
                            if (mt.Mappings != null)
                            {
                                List<RARepository.RAMeasureColumnMapping> lst = mt.Mappings.ToList<RARepository.RAMeasureColumnMapping>();
                                lst.Add(mc);
                                mt.Mappings = lst.ToArray<RARepository.RAMeasureColumnMapping>();
                            }
                            else
                            {
                                mt.Mappings = new RARepository.RAMeasureColumnMapping[] { mc };
                            }
                        }
                        if (!mc.ManuallyEdited)
                        {
                            mc.ColumnName = logicalcname;
                            mc.Formula = "[" + originalcname + "]";
                        }
                    }
                    else
                    {
                        if (importType == Performance_Optimizer_Administration.MainAdminForm.RELATIONAL_DIMENSIONAL_STRUCTURE)
                        {
                            if (lmap.ContainsKey(lkey))
                                llist = lmap[lkey];
                            if (llist.Count > 1)
                            {
                                // Create a new dimension for every hierarchy with more than 1 level
                                dimension = logicaldname + (logicalhname == null || logicalhname.ToLower() == "null" ? "" : "-" + logicalhname);
                            }
                            else if (llist.Count == 1)
                            {
                                dimension = logicaldname;
                            }
                        }
                        else if (importType == Performance_Optimizer_Administration.MainAdminForm.OLAP_DIMENSIONAL_STRUCTURE)
                        {
                            if (hname == null || hname.ToLower() == "null")
                            {
                                hname = dname;
                            }
                            dimension = dname + "-" + hname;
                            llist = hLevels[dname][hname];
                        }
                        string dtname = "Cube: " + cube + ", Dimension: " + dimension;
                        RARepository.RADimensionTable dt = null;
                        if (dtables.ContainsKey(dtname))
                            dt = dtables[dtname];
                        RARepository.RADimension d = null;
                        if (rap.Dimensions != null)
                        {
                            foreach (RARepository.RADimension dim in rap.Dimensions)
                            {
                                if (!dim.Name.Equals(dimension))
                                    continue;
                                d = dim;
                                break;
                            }
                        }
                        // Create dimension if necessary
                        if (d == null)
                        {
                            d = new RARepository.RADimension();
                            d.Name = dimension;
                            if (rap.Dimensions != null)
                            {
                                List<RARepository.RADimension> lst = rap.Dimensions.ToList<RARepository.RADimension>();
                                lst.Add(d);
                                rap.Dimensions = lst.ToArray<RARepository.RADimension>();
                            }
                            else
                            {
                                rap.Dimensions = new RARepository.RADimension[] { d };
                            }
                        }

                        Hierarchy curh = null;
                        if (rap.Hierarchies != null)
                        {
                            foreach (Hierarchy h in rap.Hierarchies)
                            {
                                if (h.DimensionName.Equals(d.Name))
                                {
                                    curh = h;
                                    break;
                                }
                            }
                        }
                        Level l = null;
                        if (curh == null)
                        {
                            curh = new Hierarchy();
                            curh.DimensionName = d.Name;
                            curh.Name = d.Name;
                            if (importType == Performance_Optimizer_Administration.MainAdminForm.OLAP_DIMENSIONAL_STRUCTURE)
                            {
                                curh.OlapDimensionName = dname;
                            }
                            curh.DimensionKeyLevel = llist[llist.Count - 1];
                            Level curLevel = null;
                            foreach (string lname in llist)
                            {
                                l = new Level();
                                l.ColumnNames = new string[] { lname };
                                l.HiddenColumns = new bool[] { false };
                                l.Name = lname;
                                l.Locked = false;
                                LevelKey lk = new LevelKey();
                                l.Keys = new LevelKey[] { lk };
                                l.Children = new Level[] { };
                                lk.ColumnNames = new string[] { lname };
                                if (curLevel == null)
                                {
                                    if (curh.Children == null)
                                        curh.Children = new Level[] { l };
                                    else
                                    {
                                        List<Level> lst = curh.Children.ToList<Level>();
                                        lst.Add(l);
                                        curh.Children = lst.ToArray<Level>();
                                    }
                                }
                                else
                                {
                                    if (curLevel.Children == null)
                                        curLevel.Children = new Level[] { l };
                                    else
                                    {
                                        List<Level> lst = curLevel.Children.ToList<Level>();
                                        lst.Add(l);
                                        curLevel.Children = lst.ToArray<Level>();
                                    }
                                }
                                curLevel = l;
                            }
                            if (rap.Hierarchies != null)
                            {
                                List<Hierarchy> lst = rap.Hierarchies.ToList<Hierarchy>();
                                lst.Add(curh);
                                rap.Hierarchies = lst.ToArray<Hierarchy>();
                            }
                            else
                            {
                                rap.Hierarchies = new Hierarchy[] { curh };
                            }
                        }

                        if (dt == null && d.Definitions != null)
                        {
                            foreach (RARepository.RADimensionTable sdt in d.Definitions)
                            {
                                if (sdt.Name.Equals(dtname))
                                {
                                    dt = sdt;
                                    dtables[dtname] = dt;
                                    break;
                                }
                            }
                        }
                        // Create a dimension table if necessary
                        if (dt == null)
                        {
                            dt = new RARepository.RADimensionTable();
                            dt.Name = dtname;
                            dt.Cacheable = cacheable;
                            dtables[dtname] = dt;
                            if (d.Definitions != null)
                            {
                                List<RARepository.RADimensionTable> lst = d.Definitions.ToList<RARepository.RADimensionTable>();
                                lst.Add(dt);
                                d.Definitions = lst.ToArray<RARepository.RADimensionTable>();
                            }
                            else
                            {
                                d.Definitions = new RARepository.RADimensionTable[] { dt };
                            }
                            dt.Type = 0;
                            dt.TTL = -1;
                            dt.Level = (l == null ? llist[llist.Count - 1] : l.Name);
                            dt.Source = new TableSource();
                            dt.Source.Connection = connectionName;
                            TableSource.TableDefinition td = new TableSource.TableDefinition();
                            dt.Source.Tables = new TableSource.TableDefinition[] { td };
                            td.JoinType = 0;
                            td.JoinClause = "";
                            td.PhysicalName = cube;

                        }

                        bool found = false;
                        RARepository.RADimensionColumn dc = null;
                        if (d.Columns != null)
                        {
                            foreach (RARepository.RADimensionColumn cdc in d.Columns)
                            {
                                if (cdc.ColumnName.Equals(logicalcname))
                                {
                                    dc = cdc;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            dc = new RARepository.RADimensionColumn();
                            if (d.Columns != null)
                            {
                                List<RARepository.RADimensionColumn> lst = d.Columns.ToList<RARepository.RADimensionColumn>();
                                lst.Add(dc);
                                d.Columns = lst.ToArray<RARepository.RADimensionColumn>();
                            }
                            else
                            {
                                d.Columns = new RARepository.RADimensionColumn[] { dc };
                            }
                        }
                        if (!dc.ManuallyEdited)
                        {
                            dc.ColumnName = logicalcname;
                            dc.DataType = dtype;
                            dc.Width = Int32.Parse(colwidth);
                        }
                        if (l != null && (l.ColumnNames == null || l.ColumnNames.Length == 0))
                        {
                            if (l.ColumnNames != null)
                            {
                                List<string> lst = l.ColumnNames.ToList<string>();
                                lst.Add(logicalcname);
                                l.ColumnNames = lst.ToArray<string>();
                            }
                            else
                            {
                                l.ColumnNames = new string[] { logicalcname };
                            }
                            if (l.HiddenColumns != null)
                            {
                                List<bool> lst = l.HiddenColumns.ToList<bool>();
                                lst.Add(false);
                                l.HiddenColumns = lst.ToArray<bool>();
                            }
                            else
                            {
                                l.HiddenColumns = new bool[] { false };
                            }
                            if (l.Keys[0].ColumnNames != null)
                            {
                                List<string> lst = l.Keys[0].ColumnNames.ToList<string>();
                                lst.Add(logicalcname);
                                l.Keys[0].ColumnNames = lst.ToArray<string>();
                            }
                            else
                            {
                                l.Keys[0].ColumnNames = new string[] { cname };
                            }

                        }

                        found = false;
                        string formula = null;
                        Regex levelMatch = new Regex("Levels\\(.\\)");
                        if (databaseType.Equals("Hyperion Essbase (XMLA)"))
                        {
                            if (levelMatch.IsMatch(originalcname))
                            {
                                formula = "[" + originaldname + "].[" + originalhname + "]." + originalcname;
                            }
                            else
                                formula = "[" + originaldname + "]." + originalcname;
                        }
                        else if (databaseType.Equals("SAP BW (XMLA)"))
                        {
                            formula = lUniqueName;
                        }
                        else
                        {
                            formula = "[" + originaldname + "].[" + originalhname + "].[" + originalcname + "]";
                        }
                        RARepository.RADimensionColumnMapping dcm = null;
                        if (dt.Mappings != null)
                        {
                            foreach (RARepository.RADimensionColumnMapping dcmp in dt.Mappings)
                            {
                                if (dcmp.Formula.Equals(formula))
                                {
                                    dcm = dcmp;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            dcm = new RARepository.RADimensionColumnMapping();
                            if (dt.Mappings != null)
                            {
                                List<RARepository.RADimensionColumnMapping> lst = dt.Mappings.ToList<RARepository.RADimensionColumnMapping>();
                                lst.Add(dcm);
                                dt.Mappings = lst.ToArray<RARepository.RADimensionColumnMapping>();
                            }
                            else
                            {
                                dt.Mappings = new RARepository.RADimensionColumnMapping[] { dcm };
                            }
                        }
                        if (!dcm.ManuallyEdited)
                        {
                            dcm.ColumnName = logicalcname;
                            dcm.Formula = formula;
                        }
                    }
                }

                // Add redundant joins if both measure and dimension tables imported
                foreach (RARepository.RAMeasureTable jmt in mtables.Values)
                {
                    foreach (RARepository.RADimensionTable jdt in dtables.Values)
                    {
                        if (jmt.Source.Tables[0].PhysicalName != null && jdt.Source.Tables[0].PhysicalName != null && jmt.Source.Tables[0].PhysicalName.Equals(jdt.Source.Tables[0].PhysicalName))
                        {
                            bool found = false;
                            if (rap.Joins != null)
                            {
                                foreach (RARepository.RAJoin j in rap.Joins)
                                {
                                    if ((j.Table1.Equals(jdt.Name) && j.Table2.Equals(jmt.Name)) || (j.Table2.Equals(jdt.Name) && j.Table1.Equals(jmt.Name)))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (!found)
                            {
                                RARepository.RAJoin nj = new RARepository.RAJoin();
                                nj.Table1 = jdt.Name;
                                nj.Table2 = jmt.Name;
                                nj.JoinCondition = "";
                                nj.JoinType = "Inner";
                                nj.Redundant = true;
                                if (rap.Joins != null)
                                {
                                    List<RARepository.RAJoin> lst = rap.Joins.ToList<RARepository.RAJoin>();
                                    lst.Add(nj);
                                    rap.Joins = lst.ToArray<RARepository.RAJoin>();
                                }
                                else
                                {
                                    rap.Joins = new RARepository.RAJoin[] { nj };
                                }
                            }
                        }
                    }
                }
                if (importType == Performance_Optimizer_Administration.MainAdminForm.OLAP_DIMENSIONAL_STRUCTURE)
                {
                    rap.DimensionalStructure = importType;
                }
                Acorn.Util.setRepository(rap, maf, sp, null, u);
            }
            return true;            
        }

        private static void addMeasureIfNecessary(RARepository rap, string name, string aggRule, string dtype, int width)
        {
            RARepository.RAMeasureColumn curm = null;
            if (rap.Measures != null)
            {
                foreach (RARepository.RAMeasureColumn m in rap.Measures)
                {
                    if (m.ColumnName.Equals(name))
                    {
                        curm = m;
                        break;
                    }
                }
            }
            if (curm == null)
            {
                curm = new RARepository.RAMeasureColumn();
                curm.ColumnName = name;
                if (rap.Measures != null)
                {
                    List<RARepository.RAMeasureColumn> lst = rap.Measures.ToList<RARepository.RAMeasureColumn>();
                    lst.Add(curm);
                    rap.Measures = lst.ToArray<RARepository.RAMeasureColumn>();
                }
                else
                {
                    rap.Measures = new RARepository.RAMeasureColumn[] { curm };
                }
            }
            curm.AggRule = aggRule;
            curm.DataType = getType(dtype);
            curm.Width = width;
        }

        private static string getType(string dtype)
        {
            if (dtype.ToLower().Equals("varchar") || dtype.ToLower().Equals("nvarchar") || dtype.ToLower().Equals("nchar") || dtype.ToLower().Equals("char") || dtype.ToLower().Equals("ntext") || dtype.ToLower().Equals("varchar2"))
                return "Varchar";
            else if (dtype.ToLower().Equals("int") || dtype.ToLower().Equals("integer") || dtype.ToLower().Equals("int identity") || dtype.ToLower().Equals("smallint"))
                return "Integer";
            else if (dtype.ToLower().Equals("float"))
                return "Float";
            else if (dtype.ToLower().Equals("datetime") || dtype.ToLower().IndexOf("timestamp") != -1)
                return "DateTime";
            else if (dtype.ToLower().Equals("date"))
                return "Date";
            else if (dtype.ToLower().Equals("decimal") || dtype.ToLower().Equals("money") || dtype.ToLower().Equals("real") || dtype.ToLower().Equals("number") || dtype.ToLower().Equals("double") || dtype.ToLower().Equals("currency"))
                return "Number";
            return "Varchar";
        }
        private static string getRealTimeConnectionXML(Connection rc, string filter)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<void method=\"put\">");
                xml.Append("<string>").Append(rc.VisibleName).Append("</string>");
                xml.Append("<object class=\"com.birst.dataconductor.RealtimeConnection\">");
                    xml.Append("<void property=\"name\">");
                        xml.Append("<string>").Append(rc.Name).Append("</string>");
                    xml.Append("</void>");
                    xml.Append("<void property=\"filter\">");
                        if (filter == null || filter.Length == 0)
                            xml.Append("<string />");
                        else
                            xml.Append("<string>").Append(filter).Append("</string>");
                    xml.Append("</void>");
                    xml.Append("<void property=\"driverName\">");
                    xml.Append("<string>").Append(rc.Driver).Append("</string>");
                    xml.Append("</void>");
                    if (rc.GenericDatabase)
                    {
                        xml.Append("<void property=\"genericDriver\">");
                            xml.Append("<boolean>true</boolean>");
                        xml.Append("</void>");
                        xml.Append("<void property=\"connectString\">");
                            xml.Append("<string>").Append(rc.ConnectString).Append("</string>");
                        xml.Append("</void>");
                    }
                    else
                    {
                        xml.Append("<void property=\"databaseName\">");
                            xml.Append("<string>").Append(rc.Database).Append("</string>");
                        xml.Append("</void>");
                        xml.Append("<void property=\"serverName\">");
                            xml.Append("<string>").Append(rc.Server).Append("</string>");
                        xml.Append("</void>");
                        xml.Append("<void property=\"port\">");
                            xml.Append("<string>").Append(rc.Port).Append("</string>");
                        xml.Append("</void>");                
                    }
                    xml.Append("<void property=\"username\">");
                        xml.Append("<string>").Append(rc.UserName).Append("</string>");
                    xml.Append("</void>");
                    xml.Append("<void property=\"password\">");
                        xml.Append("<string>").Append(MainAdminForm.encrypt(rc.Password, MainAdminForm.SecretPassword)).Append("</string>");
                    xml.Append("</void>");
                    xml.Append("<void property=\"databaseType\">");
                        xml.Append("<string>").Append(rc.Type).Append("</string>");
                    xml.Append("</void>");
                    xml.Append("<void property=\"timeout\">");
                        xml.Append("<int>").Append(rc.Timeout).Append("</int>");
                    xml.Append("</void>");
                    if (rc.useDirectConnection)
                    {
                        xml.Append("<void property=\"useDirectConnection\">");
                            xml.Append("<boolean>true</boolean>");
                        xml.Append("</void>");
                    }
                xml.Append("</object>");
            xml.Append("</void>");
            return xml.ToString();
        }

        public static bool updateConfigForConnection(Space sp, string dcconfigFile, Connection rc)
        {
            try
            {
                if (sp != null)
                {
                    if (File.Exists(dcconfigFile))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.XmlResolver = null;
                        string config = File.ReadAllText(dcconfigFile, Encoding.UTF8);
                        int index = config.IndexOf("<object ");
                        if (index >= 0)
                        {
                            doc.LoadXml(config.Substring(index));
                            XmlNode node = doc.FirstChild;
                            if (node.Attributes[0].Value == "com.birst.dataconductor.DataConductorConfig")
                            {
                                XmlNode connectNode = null;
                                foreach (XmlNode cnode in node.ChildNodes)
                                {
                                    if (cnode.Attributes[0].Name == "property" && cnode.Attributes[0].Value == "realtimeConnections")
                                    {
                                        connectNode = cnode;
                                        break;
                                    }
                                }
                                if (connectNode != null)
                                {
                                    foreach (XmlNode cnode in connectNode.ChildNodes)
                                    {
                                        string visiblename = null;
                                        XmlNode onode = null;
                                        foreach (XmlNode inode in cnode.ChildNodes)
                                        {
                                            if (inode.Name == "string")
                                                visiblename = inode.InnerText;
                                            else if (inode.Name == "object")
                                                onode = inode;
                                        }
                                        if (visiblename == null || !visiblename.Equals(rc.VisibleName))
                                            continue;
                                        foreach (XmlNode inode in onode.ChildNodes)
                                        {
                                            if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "databaseName")
                                                inode.ChildNodes[0].InnerText = rc.Database;
                                            else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "serverName")
                                                inode.ChildNodes[0].InnerText = rc.Server;
                                            else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "port")
                                                inode.ChildNodes[0].InnerText = rc.Port;
                                            else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "username")
                                                inode.ChildNodes[0].InnerText = rc.UserName;
                                            else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "password")
                                                inode.ChildNodes[0].InnerText = MainAdminForm.encrypt(rc.Password, MainAdminForm.SecretPassword);
                                            else if (inode.Attributes[0].Name == "property" && inode.Attributes[0].Value == "timeout")
                                                inode.ChildNodes[0].InnerText = rc.Timeout;
                                        }
                                        string tfname = "tempdconfig" + DateTime.Now.ToString("MMddyyyyHHmmssfff") + ".xml";
                                        string tempFilePath = Path.Combine(sp.Directory, "dcconfig", tfname);
                                        doc.Save(tempFilePath);
                                        config = config.Substring(0, index) + File.ReadAllText(tempFilePath, Encoding.UTF8);
                                        File.WriteAllText(dcconfigFile, config);
                                        File.Delete(tempFilePath);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while updating realtime connection \"" + rc.Name + " \" for " + sp.Directory, ex);
            }
            return false;
        }

        public static bool startsWithLiveAccessLoadGroup(string str, HashSet<string> liveAccessLoadGroups)
        {
            if (str == null || liveAccessLoadGroups == null)
                return false;
            foreach (string lg in liveAccessLoadGroups)
            {
                if (str.StartsWith(lg))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
