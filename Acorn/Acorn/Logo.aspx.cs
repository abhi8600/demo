﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Acorn.DBConnection;
using System.IO;
using System.Collections.Generic;
using System.Drawing;

namespace Acorn
{
    public partial class Logo : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static byte[] birstLogo = null;
        Dictionary<string, string> overrideParametersLogo = new Dictionary<string, string>();
        private byte[] getBirstLogo()
        {           
            string logoImage = "";

            if (birstLogo == null)
            {
                FileStream fs = null;
                try
                {
                    overrideParametersLogo = getLocalizedOverridenParamsLogo();
                    if (overrideParametersLogo != null && overrideParametersLogo.Count > 0 && overrideParametersLogo.ContainsKey(OverrideParameterNames.LOGO_PATH))
                    {
                        if ( overrideParametersLogo[OverrideParameterNames.LOGO_PATH] != "")
                        {

                            logoImage = overrideParametersLogo[OverrideParameterNames.LOGO_PATH];
                            birstLogo = getImageFromUrl(logoImage);
                        }
                    }
                    else
                    {
                        logoImage = "/Images/Logo.png";
                        fs = new FileStream(Server.MapPath(logoImage), FileMode.Open, FileAccess.Read);
                        BinaryReader br = new BinaryReader(fs);
                        birstLogo = br.ReadBytes((int)fs.Length);
                    } 
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
            return birstLogo;
        }
        public byte[] getImageFromUrl(string url)
        {
            System.Net.HttpWebRequest request = null;
            System.Net.HttpWebResponse response = null;
            byte[] b = null;

            request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            response = (System.Net.HttpWebResponse)request.GetResponse();
            try
            {
                if (request.HaveResponse)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Stream receiveStream = response.GetResponseStream();
                        using (BinaryReader br = new BinaryReader(receiveStream))
                        {
                            b = br.ReadBytes(500000);
                            br.Close();
                        }


                    }
                }

                return b;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        public Dictionary<string, string> getLocalizedOverridenParamsLogo()
        {
            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();

            try
            {
                QueryConnection conn = null;                
                User u = Util.getSessionUser(Session);
                Guid id;
                if (u == null)
                {
                    Global.systemLog.Warn("Session User unavailable");
                    id = Guid.Empty;
                }
                else
                {
                    id = u.AdminAccountId;
                }
                try
                {
                    conn = ConnectionPool.getConnection();
                    overrideParameters = Database.getOverrideParametersLogo(conn, WebServiceSessionHelper.mainSchema, id);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting logo " + ex);
            }
            return overrideParameters;
        }
        public byte[] getBirstLogoBytes(byte[] birstlogo)
        {
            if (birstLogo == null)
            {
                FileStream fs = null;
                try
                {
                    fs = new FileStream(Server.MapPath("/Images/Logo.png"), FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    birstLogo = br.ReadBytes((int)fs.Length);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
            return birstLogo;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp != null && sp.Settings != null && sp.Settings.LogoImage != null)
            {
                  Response.Clear();
                  Response.ContentType = "image/" + Util.removeCRLF(sp.Settings.LogoImageType);
                  Response.BinaryWrite(sp.Settings.LogoImage);
                  return;
            }

            User u = (User)Session["user"];
            if (u == null || u.LogoImageID == Guid.Empty)
            {
                Response.Clear();
                Response.ContentType = "image/png";
                Response.BinaryWrite(getBirstLogo());
                return;
            }
            /// check for 
            StoredImage image = (StoredImage)Session["image"];
            if (image == null)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    image = Database.getImage(conn, mainSchema, u.LogoImageID);
                    Session["image"] = image;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            if (image == null)
                return;
            Response.Clear();
            Response.ContentType = "image/" + Util.removeCRLF(image.type);
            Response.BinaryWrite(image.data);
        }
    }
}
