﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections;

namespace Acorn
{
    public class ScanResults
    {
        public object[] values;
        public int[] nullCounts;
        public List<BitArray> nullList;
        public Dictionary<string, int> nullPatterns;
        public long totalNull = 0;
        public int rows = 0;
        public int[] keys;
        public bool wellFormed = true;
        public double nullCutoff;
        public double[] probs;
        public List<int> defectiveRows = new List<int>();

        public bool isLikelyPattern(BitArray ba)
        {
            double prob = rows;
            for (int i = 0; i < ba.Length; i++)
            {
                // Count columns that tend to be non-null but are null
                if (ba[i] && probs[i] < 0.5)
                {
                    prob *= probs[i];
                }
            }
            if (prob < 0.05)
            {
                return false;
            }
            return true;
        }

        public bool isLikelyPatternWithFilter(BitArray ba)
        {
            double prob = rows;
            int numNulls = 0;
            for (int i = 0; i < ba.Length; i++)
            {
                // Count columns that tend to be non-null but are null
                if (ba[i] && probs[i] < 0.5)
                {
                    prob *= probs[i];
                    numNulls++;
                }
            }
            if (prob < 0.05)
            {
                // Only find rows with a lot of nulls
                double nullPct = ((double)numNulls) / ((double)ba.Length);
                if (nullPct > nullCutoff)
                {
                    string s = getByteString(ba);
                    if (nullPatterns[s] < 5)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static string getByteString(BitArray ba)
        {
            char[] result = new char[ba.Length / 8 + 1];
            for (int i = 0; i < ba.Length; i++)
            {
                if (ba[i])
                    result[i / 8] |= (char)(1 << (i % 8));
            }
            return (new string(result));
        }
    }
}
