﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class UploaderResponse
    {
       public ErrorOutput Error;
       public UploaderError[] UploadErrors;   
       public ExcelWarningError[] ExcelErrors;
    }

    public class UploaderError
    {
       public string Source;
       public string Severity;
       public string Issue;
    }

    public class ExcelWarningError
    {
        public string FileName;
        public string[] Rows;
        public bool[] Defective;
    }
}
