﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Acorn
{
    public class ScanBuffer
    {
        private static int NUM_LINES_TO_SCAN = 2000;
        private static long MAX_SCAN_LINES = 100000;

        private Dictionary<SourceFile, List<string[]>> scanCache = new Dictionary<SourceFile, List<string[]>>(new IdentityComparer<SourceFile>());

        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        public List<string[]> getBuffer(SourceFile sf, string directory)
        {
            if (scanCache.ContainsKey(sf))
                return (scanCache[sf]);
            if (sf == null)
                return (null);
            // Top of range of number of lines to skip each time (for random sampling)
            long maxScanLines = Math.Min(sf.NumRows, MAX_SCAN_LINES);
            int skip = (int)(2.0 * ((double)maxScanLines / (double)NUM_LINES_TO_SCAN - 1));
            string fname = Path.Combine(directory, sf.FileName);
            Util.validateFileLocation(directory, fname);
            List<string[]> buff = new List<string[]>();
            StreamReader reader = null;
            if (!File.Exists(fname))
                return (buff);
            Encoding encoding = null;
            if (sf.Encoding == "UTF-16" || sf.Encoding == "Unicode")
                encoding = Encoding.Unicode;
            else if (sf.Encoding == "UTF-16LE")
                encoding = Encoding.Unicode;
            else if (sf.Encoding == "UTF-16BE")
                encoding = Encoding.BigEndianUnicode;
            try
            {
                if (encoding == null)
                    reader = new StreamReader(fname);
                else
                    reader = new StreamReader(fname, encoding);
            }
            catch (Exception)
            {
                return (buff);
            }
            string line = null;
            if (sf.HasHeaders)
                Performance_Optimizer_Administration.SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1: 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
            try
            {
                bool end = false;
                int count = 0;
                while ((line = Performance_Optimizer_Administration.SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1: 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline)) != null && buff.Count < NUM_LINES_TO_SCAN)
                {
                    string[] cols = Performance_Optimizer_Administration.Util.splitString(line, sf.Separator[0]);
                    if (sf.Separator != null && sf.Separator.Length > 0)
                        for (int i = 0; i < cols.Length; i++)
                        {
                            if (cols[i].StartsWith(sf.Quote) && cols[i].EndsWith(sf.Quote) && cols[i].Length >= sf.Quote.Length * 2)
                            {
                                cols[i] = cols[i].Substring(sf.Quote.Length, cols[i].Length - (sf.Quote.Length * 2));
                            }
                        }
                    if (cols.Length != sf.Columns.Length)
                        continue;
                    buff.Add(cols);
                    if (skip > 0)
                    {
                        byte[] rnd = new byte[4];
                        rngCsp.GetBytes(rnd);
                        int skipLines = Math.Abs(BitConverter.ToInt32(rnd, 0)) % skip;
                        for (int i = 0; i < skipLines; i++)
                        {
                            line = Performance_Optimizer_Administration.SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1: 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
                            if (line == null)
                            {
                                end = true;
                                break;
                            }
                        }
                    }
                    if (count > MAX_SCAN_LINES)
                        break;
                    count++;
                    if (end)
                        break;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                reader.Close();
            }
            scanCache.Add(sf, buff);
            return (buff);
        }
    }
}
