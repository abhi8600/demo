﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;

namespace Acorn.DBConnection
{
    public class QueryDataAdapter
    {
        private OdbcDataAdapter oadapt;
        QueryConnection conn;
        string query;

        public QueryDataAdapter(string query, QueryConnection conn)
        {
            if (conn.OdbcConnection != null)
                oadapt = new OdbcDataAdapter(query, conn.OdbcConnection);
            else
            {
                this.conn = conn;
                this.query = query;
            }
        }

        public int Fill(DataTable dt)
        {
            if (oadapt != null)
                return oadapt.Fill(dt);
            QueryCommand qc = conn.CreateCommand();
            qc.CommandText = query;
            QueryReader reader = qc.ExecuteReader();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                dt.Columns.Add(reader.GetName(i));
            }
            int count = 0;
            while (reader.Read())
            {
                DataRow dr = dt.NewRow();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    dr[i] = reader.GetValue(i);
                }
                dt.Rows.Add(dr);
                count++;
            }
            return count;
        }
    }
}