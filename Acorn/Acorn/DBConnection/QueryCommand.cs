﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Net.Sockets;
using System.IO;
using System.Data.Common;
using System.Text;

namespace Acorn.DBConnection
{
    public class QueryCommand
    {
        private OdbcCommand ocmd;
        private string commandText;
        private QueryParameterCollection parameters;
        private string server;
        private int memdbport;
        private QueryConnection.DBType dbType;

        public QueryCommand(OdbcCommand ocmd, QueryConnection.DBType dbType)
        {
            this.ocmd = ocmd;
            this.dbType = dbType;
        }

        public QueryCommand(string query, QueryConnection conn)
        {
            QueryCommand cmd = conn.CreateCommand();
            ocmd = cmd.ocmd;
            cmd.CommandText = query;
            dbType = conn.getDBType();
        }

        public QueryCommand(string server, int memdbport)
        {
            this.server = server;
            this.memdbport = memdbport;
        }

        public string CommandText
        {
            get
            {
                return commandText;
            }
            set
            {
                commandText = value;
                if (ocmd != null)
                    ocmd.CommandText = value;
            }
        }

        public OdbcTransaction Transaction
        {
            get
            {
                if (ocmd != null)
                    return ocmd.Transaction;
                return null;
            }
            set
            {
                if (ocmd != null)
                    ocmd.Transaction = value;
            }
        }

        public int CommandTimeout
        {
            get
            {
                if (ocmd != null)
                    return ocmd.CommandTimeout;
                return 0;
            }
            set
            {
                if (ocmd != null)
                    ocmd.CommandTimeout = value;
            }
        }

        public QueryParameterCollection Parameters
        {
            get
            {
                if (ocmd != null)
                    return new QueryParameterCollection(ocmd.Parameters);
                else if (server != null)
                {
                    if (parameters == null)
                        parameters = new QueryParameterCollection();
                    return parameters;
                }
                return null;
            }
        }

        public void Dispose()
        {
            if (ocmd != null)
                ocmd.Dispose();
        }

        public int ExecuteNonQuery()
        {
            if (ocmd != null)
                return ocmd.ExecuteNonQuery();
            else if (server != null)
            {
                TcpClient client = null;
                StreamWriter writer = null;
                StreamReader reader = null;
                try
                {
                    client = new TcpClient(server, memdbport);
                    if (parameters != null && parameters.Count > 0)
                        commandText = replaceParameters(commandText);
                    NetworkStream ns = client.GetStream();

                    writer = new StreamWriter(ns);
                    writer.WriteLine(commandText);
                    writer.Flush();

                    reader = new StreamReader(ns);
                    string result = reader.ReadLine();
                    if (result != null && result.Length > 0 && !result.StartsWith("OK"))
                        throw new Exception(result);
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                    if (writer != null)
                        writer.Close();
                    if (client != null)
                        client.Close();
                }
            }
            return 0;
        }

        public QueryReader ExecuteReader()
        {
            if (ocmd != null)
                return new QueryReader(ocmd.ExecuteReader(), dbType);
            else if (server != null)
            {
                TcpClient client = new TcpClient(server, memdbport);
                if (parameters != null && parameters.Count > 0)
                    commandText = replaceParameters(commandText);
                StreamWriter writer = new StreamWriter(client.GetStream());
                writer.WriteLine(commandText);
                writer.Flush();
                QueryReader qr = new QueryReader(client);
                return qr;
            }
            return null;
        }

        public QueryReader ExecuteReader(CommandBehavior behavior)
        {
            if (ocmd != null)
                return new QueryReader(ocmd.ExecuteReader(behavior), dbType);
            else if (server != null)
            {
                TcpClient client = new TcpClient(server, memdbport);
                if (parameters != null && parameters.Count > 0)
                    commandText = replaceParameters(commandText);
                StreamWriter writer = new StreamWriter(client.GetStream());
                writer.WriteLine(commandText);
                writer.Flush();
                QueryReader qr = new QueryReader(client);
                return qr;
            }
            return null;
        }

        public Object ExecuteScalar()
        {
            if (ocmd != null)
                return ocmd.ExecuteScalar();
            else if (server != null)
            {
                TcpClient client = new TcpClient(server, memdbport);
                if (parameters != null && parameters.Count > 0)
                    commandText = replaceParameters(commandText);
                StreamWriter writer = new StreamWriter(client.GetStream());
                writer.WriteLine(commandText);
                writer.Flush();
                QueryReader qr = new QueryReader(client);
                if (qr.Read())
                    return qr.GetValue(0);
            }
            return null;
        }

        public void Cancel()
        {
            if (ocmd != null)
                ocmd.Cancel();
        }

        private string replaceParameters(string command)
        {
            string[] pieces = command.Split('?');
            string result = "";
            for (int i = 0; i < pieces.Length; i++)
            {
                result += pieces[i];
                if (i < parameters.Count)
                {
                    if (((QueryParameter)parameters[i]).OdbcType == OdbcType.Int || ((QueryParameter)parameters[i]).OdbcType == OdbcType.BigInt ||
                        ((QueryParameter)parameters[i]).OdbcType == OdbcType.Double || ((QueryParameter)parameters[i]).OdbcType == OdbcType.Real)
                    {
                        result += parameters[i].Value.ToString();
                    }
                    else
                    {
                        result += '\'' + parameters[i].Value.ToString() + '\'';
                    }
                }
            }
            return result;
        }
    }
}