﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Data.Odbc;

namespace Acorn.DBConnection
{
    public class QueryParameterCollection: DbParameterCollection
    {
        List<object> parameters = new List<object>();
        OdbcParameterCollection oparameters;

        public QueryParameterCollection()
        {
        }

        public QueryParameterCollection(OdbcParameterCollection oparameters)
        {
            this.oparameters = oparameters;
        }

        public override int Add(object value)
        {
            if (oparameters != null)
                return oparameters.Add(value);
            parameters.Add(value);
            return parameters.Count - 1;
        }

        public DbParameter Add(string name, OdbcType type)
        {
            if (oparameters != null)
            {
                return oparameters.Add(name, type);
            }
            parameters.Add(new QueryParameter(name, type));
            return (DbParameter) parameters[Count - 1];
        }

        public DbParameter Add(string name, OdbcType type, int width)
        {
            if (oparameters != null)
            {
                return oparameters.Add(name, type, width);
            }
            parameters.Add(new QueryParameter(name, type, width));
            return (DbParameter)parameters[Count - 1];
        }

        public DbParameter AddWithValue(string name, object value)
        {
            if (oparameters != null)
            {
                return oparameters.AddWithValue(name, value);
            }
            QueryParameter qp = new QueryParameter(name, OdbcType.VarChar);
            qp.Value = value;
            parameters.Add(qp);
            return (DbParameter)qp;
        }

        public override void AddRange(Array values)
        {
            if (oparameters != null)
            {
                oparameters.AddRange(values);
                return;
            }
            foreach (Object o in values)
                Add(o);
        }

        public override void Clear()
        {
            if (oparameters != null)
                oparameters.Clear();
            else
                parameters.Clear();
        }

        public override bool Contains(string value)
        {
            if (oparameters != null)
                return oparameters.Contains(value);
            else
                return parameters.Contains(value);
        }

        public override bool Contains(object value)
        {
            if (oparameters != null)
                return oparameters.Contains(value);
            else
                return parameters.Contains(value);
        }

        public override void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public override int Count
        {
            get { if (oparameters != null) return oparameters.Count; else return parameters.Count; }
        }

        public override System.Collections.IEnumerator GetEnumerator()
        {
            if (oparameters != null)
                return oparameters.GetEnumerator();
            return parameters.GetEnumerator();
        }

        protected override DbParameter GetParameter(string parameterName)
        {
            if (oparameters != null)
                return oparameters[parameterName];
            for (int i = 0; i < parameters.Count; i++)
                if (((QueryParameter)parameters[i]).ParameterName == parameterName)
                    return ((DbParameter)parameters[i]);
            return null;
        }

        protected override DbParameter GetParameter(int index)
        {
            if (oparameters != null)
                return oparameters[index];
            return (DbParameter) parameters[index];
        }

        public override int IndexOf(string parameterName)
        {
            if (oparameters != null)
                return oparameters.IndexOf(parameterName);
            for (int i = 0; i < parameters.Count; i++)
                if (((QueryParameter)parameters[i]).ParameterName == parameterName)
                    return i;
            return -1;
        }

        public override int IndexOf(object value)
        {
            if (oparameters != null)
                return oparameters.IndexOf(value);
            for (int i = 0; i < parameters.Count; i++)
                if (((QueryParameter)parameters[i]).Value == value)
                    return i;
            return -1;
        }

        public override void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public override bool IsFixedSize
        {
            get { throw new NotImplementedException(); }
        }

        public override bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public override bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public override void Remove(object value)
        {
            if (oparameters != null)
                oparameters.Remove(value);
            else
                parameters.Remove(value);
        }

        public override void RemoveAt(string parameterName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        protected override void SetParameter(string parameterName, DbParameter value)
        {
            throw new NotImplementedException();
        }

        protected override void SetParameter(int index, DbParameter value)
        {
            throw new NotImplementedException();
        }

        public override object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }
    }
}