﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data.Common;

namespace Acorn.DBConnection
{
    public class QueryParameter : DbParameter
    {
        private OdbcParameter oparam;
        private string name;
        private OdbcType type;
        private int width;
        private object value;

        public QueryParameter(OdbcParameter oparam)
        {
            this.oparam = oparam;
        }

        public QueryParameter(string name, OdbcType type)
        {
            this.name = name;
            this.type = type;
        }

        public QueryParameter(string name, OdbcType type, int width)
        {
            this.name = name;
            this.type = type;
            this.width = width;
        }

        public OdbcType OdbcType
        {
            get
            {
                if (oparam != null)
                    return oparam.OdbcType;
                return type;
            }
        }

        public override object Value
        {
            get
            {
                if (oparam != null)
                    return oparam.Value;
                return value;
            }
            set
            {
                if (oparam != null)
                    oparam.Value = value;
                else
                    this.value = value;
            }
        }


        public override System.Data.DbType DbType
        {
            get
            {
                if (oparam != null)
                    return oparam.DbType;
                throw new NotImplementedException();
            }
            set
            {
                if (oparam != null)
                    oparam.DbType = value;
                else
                    throw new NotImplementedException();
            }
        }

        public override System.Data.ParameterDirection Direction
        {
            get
            {
                if (oparam != null)
                    return oparam.Direction;
                throw new NotImplementedException();
            }
            set
            {
                if (oparam != null)
                    oparam.Direction = value;
                else
                    throw new NotImplementedException();
            }
        }

        public override bool IsNullable
        {
            get
            {
                if (oparam != null)
                    return oparam.IsNullable;
                throw new NotImplementedException();
            }
            set
            {
                if (oparam != null)
                    oparam.IsNullable = value;
                else
                    throw new NotImplementedException();
            }
        }

        public override string ParameterName
        {
            get
            {
                if (oparam != null)
                    return oparam.ParameterName;
                return name;
            }
            set
            {
                if (oparam != null)
                    oparam.ParameterName = value;
                else
                    name = value;
            }
        }

        public override void ResetDbType()
        {
            if (oparam != null)
                oparam.ResetDbType();
        }

        public override int Size
        {
            get
            {
                if (oparam != null)
                    return oparam.Size;
                return width;
            }
            set
            {
                if (oparam != null)
                    oparam.Size = value;
                else
                    width = value;
            }
        }

        public override string SourceColumn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool SourceColumnNullMapping
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override System.Data.DataRowVersion SourceVersion
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}