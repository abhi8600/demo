﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Net.Sockets;
using System.IO;
using System.Data;
using System.Text;

namespace Acorn.DBConnection
{
    public class QueryReader
    {
        private OdbcDataReader oreader;
        private TcpClient client;
        private string[] colNames;
        private int[] dataTypes;
        private List<string[]> lines;
        private int lineno = -1;
        private QueryConnection.DBType dbType;

        public QueryReader(OdbcDataReader oreader, QueryConnection.DBType dbType)
        {
            this.oreader = oreader;
            this.dbType = dbType;
        }

        public QueryReader(TcpClient client)
        {
            this.client = client;
            client.ReceiveTimeout = 10000;
            StreamReader reader = new StreamReader(client.GetStream());
            try
            {
                string result = reader.ReadLine();
                if (result.StartsWith("SQLException:"))
                {
                    throw new DataException(result.Substring("SQLException:".Length));
                }
                string names = reader.ReadLine();
                colNames = names.Split(new char[] { '|' });
                string types = reader.ReadLine();
                int count = 0;
                dataTypes = new int[colNames.Length];
                foreach (string s in types.Split(new char[] { '|' }))
                {
                    int res;
                    if (Int32.TryParse(s, out res))
                    {
                        dataTypes[count++] = res;
                    }
                }
                string line = reader.ReadLine();
                int numlines = int.Parse(line);
                lines = new List<string[]>();
                for (int i = 0; i < numlines; i++)
                {
                    lines.Add(readLine(reader).Split(new string[] { "|0" }, StringSplitOptions.None));
                }
            }
            catch (IOException ex)
            {
                throw new DataException("IOException reading from database: " + ex.Message);
            }
            reader.Close();
            client.Close();
        }

        private string readLine(StreamReader reader)
        {
            StringBuilder sb = new StringBuilder();
            char lastc = ' ';
            while (true)
            {
                char c = (char)reader.Read();
                if (c == '1' && lastc == '|')
                    break;
                sb.Append(c);
                lastc = c;
            }
            return sb.ToString(0, sb.Length - 1);
        }

        public virtual void Close()
        {
            if (oreader != null)
                oreader.Close();
        }

        public virtual DataTable GetSchemaTable()
        {
            if (oreader != null)
                return oreader.GetSchemaTable();
            return null;
        }

        public virtual bool GetBoolean(int colnum)
        {
            if (oreader != null)
            {
                if (dbType == QueryConnection.DBType.Oracle)
                    return (GetInt32(colnum) == 1);
                return oreader.GetBoolean(colnum);
            }
            bool result;
            bool.TryParse((string)lines[lineno][colnum], out result);
            return result;
        }

        public virtual DateTime GetDateTime(int colnum)
        {
            if (oreader != null)
                return oreader.GetDateTime(colnum);  // XXX this odbc.net driver can't handle DATETIME2???
            return DateTime.Now;
        }

        public virtual string GetString(int colnum)
        {
            if (oreader != null)
                return oreader.GetString(colnum);
            return ((string)lines[lineno][colnum]);
        }

        public virtual Guid GetGuid(int colnum)
        {
            if (oreader != null)
            {
                if (dbType == QueryConnection.DBType.Oracle)
                {
                    return new Guid(oreader.GetString(colnum));
                }
                return oreader.GetGuid(colnum);
            }
            return Guid.Empty;
        }

        public virtual bool IsDBNull(int colnum)
        {
            if (oreader != null)
                return oreader.IsDBNull(colnum);
            return ((string)lines[lineno][colnum]) == "";
        }

        public virtual int GetInt16(int colnum)
        {
            if (oreader != null)
            {
                if (dbType == QueryConnection.DBType.Oracle)
                {
                    if (oreader.GetValue(colnum) is double)
                        return Int16.Parse(oreader.GetDouble(colnum).ToString());
                    else
                        return Int16.Parse(oreader.GetDecimal(colnum).ToString());
                }
                return oreader.GetInt16(colnum);
            }
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return (int)Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual int GetInt32(int colnum)
        {
            if (oreader != null)
            {
                try
                {
                    if (dbType == QueryConnection.DBType.Oracle)
                    {
                        try
                        {
                            if (oreader.GetValue(colnum) is double)
                                return Int32.Parse(oreader.GetDouble(colnum).ToString());
                            else if (oreader.GetValue(colnum) is decimal)
                                return Int32.Parse(oreader.GetDecimal(colnum).ToString());
                            else oreader.GetInt32(colnum);
                        }
                        catch (InvalidCastException)
                        {
                            return Int32.Parse(oreader.GetValue(colnum).ToString());
                        }
                    }
                    return oreader.GetInt32(colnum);
                }
                catch (InvalidCastException)
                {
                    return Int32.Parse(oreader.GetDecimal(colnum).ToString());
                }
            }
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return (int) Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual long GetInt64(int colnum)
        {
            if (oreader != null)
            {
                try
                {
                    if (dbType == QueryConnection.DBType.Oracle)
                    {
                        if (oreader.GetValue(colnum) is double)
                            return Int64.Parse(oreader.GetDouble(colnum).ToString());
                        else
                            return Int64.Parse(oreader.GetDecimal(colnum).ToString());
                    }
                    return oreader.GetInt64(colnum);
                }
                catch (InvalidCastException)
                {
                    return Int64.Parse(oreader.GetDecimal(colnum).ToString());
                }
            }
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return (long)Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual float GetFloat(int colnum)
        {
            if (oreader != null)
                return oreader.GetFloat(colnum);
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return float.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual Object GetValue(int colnum)
        {
            if (oreader != null)
                return oreader.GetValue(colnum);
            return ((string)lines[lineno][colnum]);
        }

        public virtual decimal GetDecimal(int colnum)
        {
            if (oreader != null)
                return oreader.GetDecimal(colnum);
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return Decimal.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual double GetDouble(int colnum)
        {
            if (oreader != null)
                return oreader.GetDouble(colnum);
            try
            {
                string s = lines[lineno][colnum].ToString();
                if (s.Length == 0)
                    return 0;
                return Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public virtual string GetName(int colnum)
        {
            if (oreader != null)
                return oreader.GetName(colnum);
            return colNames[colnum];            
        }

        public virtual bool Read()
        {
            if (oreader != null)
                return oreader.Read();
            return ++lineno < lines.Count;
        }

        public virtual bool HasRows
        {
            get
            {
                if (oreader != null)
                    return oreader.HasRows;
                else if (client != null)
                    return lines != null && lines.Count > 0;
                return false;
            }
        }

        public virtual bool IsClosed
        {
            get
            {
                if (oreader != null)
                    return oreader.IsClosed;
                return false;
            }
        }

        public virtual int FieldCount
        {
            get
            {
                if (oreader != null)
                    return oreader.FieldCount;
                else if (client != null && colNames != null)
                    return  colNames.Length;
                return 0;
            }
        }

        public virtual string GetDataTypeName(int colnum)
        {
            if (oreader != null)
                return oreader.GetDataTypeName(colnum);
            return null;
        }
    }
}