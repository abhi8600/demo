﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Net.Sockets;
using System.Configuration;

namespace Acorn.DBConnection
{
    public class QueryConnection
    {
        private OdbcConnection oconn;
        private string connectString;
        private int port = 30000;
        private string server;
        public static string oracledrivername = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["OracleDriverName"];
        public enum DBType { Default, MySQL, MonetDB, Oracle, MemDB, ParAccel, Hana };

        public QueryConnection()
        {
            oconn = new OdbcConnection();
        }

        public QueryConnection(string connectString)
        {
            if (connectString.StartsWith("jdbc:memdb://"))
            {
                server = connectString.Substring("jdbc:memdb://".Length);
                int portindex = server.IndexOf(':');
                if (portindex > 0)
                {
                    try
                    {
                        string newserver = server.Substring(0, portindex);
                        string portstr = server.Substring(portindex + 1);
                        int index = portstr.IndexOf('/');
                        if (index > 0)
                            port = Int32.Parse(portstr.Substring(index + 1));
                        server = newserver;
                    }
                    catch (Exception)
                    {

                    }
                }
                this.connectString = connectString;
                return;
            }
            oconn = new OdbcConnection(connectString);
        }

        public ConnectionState State
        {
            get
            {
                if (oconn != null)
                    return oconn.State;
                return ConnectionState.Open;
            }
        }

        public string ConnectionString
        {
            get
            {
                if (oconn != null)
                    return oconn.ConnectionString;
                return connectString;
            }
            set
            {
                if (oconn != null)
                    oconn.ConnectionString = value;
            }
        }

        public int ConnectionTimeout
        {
            get
            {
                if (oconn != null)
                    return oconn.ConnectionTimeout;
                return 0;
            }
            set
            {
                if (oconn != null)
                    oconn.ConnectionTimeout = value;
            }
        }

        public string Database
        {
            get
            {
                if (oconn != null)
                    return oconn.Database;
                return null;
            }
        }

        public string Driver
        {
            get
            {
                if (oconn != null)
                    return oconn.Driver;
                return null;
            }
        }

        public string DataSource
        {
            get
            {
                if (oconn != null)
                    return oconn.DataSource;
                return null;
            }
        }

        public DataTable GetSchema()
        {
            if (oconn != null)
                return oconn.GetSchema();
            return null;
        }

        public DataTable GetSchema(string name)
        {
            if (oconn != null)
                return oconn.GetSchema(name);
            return null;
        }

        public DataTable GetSchema(string collectionName, string[] restrictionValues)
        {
            if (oconn != null)
                return oconn.GetSchema(collectionName, restrictionValues);
            return null;
        }

        public void Close()
        {
            if (oconn != null)
                oconn.Close();

        }

        public void Open()
        {
            if (oconn != null)
                oconn.Open();
        }

        public void Dispose()
        {
            if (oconn != null)
                oconn.Dispose();
        }

        public QueryCommand CreateCommand()
        {
            if (oconn != null)
            {
                return new QueryCommand(oconn.CreateCommand(), getDBType());
            }
            else if (server != null)
            {
                return new QueryCommand(server, port);
            }
            return null;
        }

        public bool isDBTypeOracle()
        {
            if (oconn != null && oconn.ConnectionString != null && oconn.ConnectionString.Contains("Driver={"+ oracledrivername + "}"))
                return true;
            return false;
        }

        public bool isDBTypeMySQL()
        {
            if (oconn != null && oconn.ConnectionString != null && (oconn.ConnectionString.StartsWith("jdbc:mysql") || oconn.ConnectionString.Contains("Driver={MySQL ODBC")))
                return true;
            return false;
        }

        public bool isDBTypeMemDB()
        {
            if (ConnectionString != null && ConnectionString.StartsWith("jdbc:memdb"))
                return true;
            return false;
        }

        public bool isDBTypeSAPHana()
        {
            if (oconn != null && oconn.ConnectionString != null && (oconn.ConnectionString.StartsWith("jdbc:sap") || oconn.ConnectionString.Contains("Driver={HDBODBC")))
                return true;
            return false;
        }

		public bool isDBTypeParAccel()
        {
            string postgresDriverName = Util.getPostgresDriverName();
            if (postgresDriverName == null || postgresDriverName.Trim().Length == 0)
            {
                postgresDriverName = "PostgreSQL Unicode";
            }
            if (oconn != null && oconn.ConnectionString != null && (oconn.ConnectionString.Contains("Driver={ParAccel}") || oconn.ConnectionString.Contains("Driver={" + postgresDriverName + "}")))                return true;
            return false;
        }

        public bool isDBTypeSQLServer()
        {
            if (oconn != null && oconn.ConnectionString != null && (oconn.ConnectionString.Contains("Driver={SQL Native Client}") ||
                oconn.ConnectionString.Contains("SQL Server Native Client 10.0") || oconn.ConnectionString.Contains("SQL Server")))
                return true;
            return false;
        }

        public DBType getDBType()
        {
            if (isDBTypeOracle())
                return DBType.Oracle;
            else if (isDBTypeParAccel())
                return DBType.ParAccel;
            else if (isDBTypeMySQL())
                return DBType.MySQL;
            else if (isDBTypeMemDB())
                return DBType.MemDB;
            else if (isDBTypeSAPHana())
                return DBType.Hana;
            return DBType.Default;
        }
        
        public OdbcTransaction BeginTransaction()
        {
            if (oconn != null)
                return oconn.BeginTransaction();
            return null;
        }

        public OdbcConnection OdbcConnection
        {
            get
            {
                return oconn;
            }
        }

        //reserved name in all dbs (i.e. MSSQL, Oracle)
        public string getReservedName(string name) // return reserved name for dbtype
        {
            if (isDBTypeOracle())
                return getOracleReservedName(name);
            return getMSSQLReservedName(name);
        }

        //reserved name in MSSQL only and not in other db (i.e. Oracle)
        public string getMSSQLReservedName(string name) //if MSSQL then return reserved name else return name
        {
            if (isDBTypeSQLServer()) 
                return "[" + name + "]";
            return name;
        }

        //reserved name in Oracle only and not in other db (i.e. MSSQL)
        public string getOracleReservedName(string name) //if oracle then return reserved name else return name
        {
            if (isDBTypeOracle()) 
                return name + "$";
            return name;
        }

        public bool supportsIdentity()
        {
            return isDBTypeSQLServer();
        }

        public string getDateTimeType()
        {
            if (isDBTypeOracle())
                return "TIMESTAMP";
            else if (isDBTypeSAPHana())
                return "SECONDDATE";
            return "DATETIME";
        }

        public string getUniqueIdentiferType()
        {
            if (isDBTypeOracle())
                return "Varchar2(36)";
            return "UniqueIdentifier";
        }

        public OdbcType getODBCUniqueIdentiferType()
        {
            if (isDBTypeOracle())
                return OdbcType.VarChar;
            return OdbcType.UniqueIdentifier;
        }

        public object getUniqueIdentifer(Guid data)
        {
            if (getODBCUniqueIdentiferType() == OdbcType.VarChar)
                return data.ToString();
            return data;
        }

        public string getBooleanType(string column)
        {
            if (isDBTypeOracle())
                return "NUMBER(1) CHECK (" + column + " IN ( 0, 1 ))";
            return "bit";
        }

        public OdbcType getODBCBooleanType()
        {
            if (isDBTypeOracle())
                return OdbcType.Int;
            return OdbcType.Bit;
        }

        public object getBooleanValue(bool data)
        {
            if (getODBCBooleanType() == OdbcType.Int)
               return data ? 1 : 0;
            return data;
        }

        public string getBigIntType()
        {
            if (isDBTypeOracle())
                return "NUMBER(19)";
            return "BIGINT";
        }

        public OdbcType getODBCBigIntType()
        {
            if (isDBTypeOracle())
                return OdbcType.Numeric;
            return OdbcType.BigInt;
        }

        public string getVarcharType(int width, bool useMax)
        {
            if (useMax)
            {
                if (isDBTypeOracle())
                    return "VARCHAR2(4000)";
                return "VARCHAR(MAX)";
            }
            if (isDBTypeOracle())
            {
                if (width > 4000)
                    width = 4000;
                return "VARCHAR2(" + width + ")";
            }
            if (isDBTypeSAPHana())
            {
                if (width > 5000)
                    width = 5000;
            }
            return "VARCHAR(" + width + ")";
        }

        public string getNvarcharType(int width, bool useMax)
        {
            if (useMax)
            {
                if (isDBTypeOracle())
                    return "NVARCHAR2(2000)";
                return "NVARCHAR(MAX)";
            }
            if (isDBTypeOracle())
            {
                if (width > 2000)
                    width = 2000;
                return "NVARCHAR2(" + width + ")";
            }
            if (isDBTypeSAPHana())
            {
                if (width > 5000)
                    width = 5000;
            }
            return "NVARCHAR(" + width + ")";
        }

        public bool supportsRowNum()
        {
            return isDBTypeOracle();
        }

        public bool supportsTop()
        {
            return isDBTypeSQLServer();
        }

        public bool supportsLimit()
        {
            return (isDBTypeMySQL() || isDBTypeParAccel() || isDBTypeSAPHana());
        }

        public string getCurrentTimestamp()
        {
            if (isDBTypeOracle() || isDBTypeSAPHana())
                return "current_timestamp";
            else if (isDBTypeMySQL())
                return "current_timestamp()";
            return "getdate()";
        }

        public bool supportsIndexes()
        {
            if (isDBTypeMySQL() || isDBTypeParAccel() || isDBTypeMemDB() || isDBTypeSAPHana())
                return false;
            return true;
        }
    }
}