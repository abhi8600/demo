﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppExchangeSSO.aspx.cs" Inherits="Acorn.AppExchangeSSO" %>

<html>
<head><title></title></head>
<body>

<% if (Request.HttpMethod == "GET")
    { %>

    <script src="canvas_all.js" type="text/javascript"></script>

    <script type="text/javascript">

        function loginHandler(e) {
            var uri;
            if (!Sfdc.canvas.oauth.loggedin()) {
                uri = Sfdc.canvas.oauth.loginUrl();
                Sfdc.canvas.oauth.login(
                    { uri: uri,
                        params: {
                            response_type: "token",
                            client_id: '<%=System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCBirstAppClientId"]%>',
                            redirect_uri: encodeURIComponent("<%=Acorn.Util.getMasterURL(null, Request)%>/AppExchangeSSOCallback.aspx")
                        }
                    });
            }
            return false;
        }

        // Bootstrap the page once the DOM is ready.
        Sfdc.canvas(function () {
            // On Ready...
            var login = Sfdc.canvas.byId("login"),
                loggedIn = Sfdc.canvas.oauth.loggedin(),
                token = Sfdc.canvas.oauth.token();

            if (!loggedIn && document.location.href.indexOf("access_denied") == -1) { // if not logged in and not clicked oauth Deny button
                loginHandler();
            }
            else if (loggedIn) {
                var client = Sfdc.canvas.oauth.client();
                Sfdc.canvas.client.ctx(callback, client);
            }
        });

        function callback(msg) {
            if (msg.status == 200) {
                document.location = "<%=Acorn.Util.getMasterURL(null, Request)%>/AppExchangeSSO.aspx"
                    + "?email=" + msg.payload.user.email
                    + "&userName=" + msg.payload.user.userName
                    + "&spaceId=" + msg.payload.environment.parameters.birst.spaceId
                    + "&useSFDCEmailForUsername=" + msg.payload.environment.parameters.birst.useSFDCEmailForUsername
                    + "&module=" + msg.payload.environment.parameters.birst.module;
            }
            else {
                alert("Error logging in: " + msg.status);
            }
        }

    </script>


<%  } %>


</body>
</html>