﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.SAMLSSO
{
    public class IdpMetaData
    {
        public Guid ID;
        public string idpName;
        public string idpIssuerID;
        public string cert;
        public int timeout;
        public string logoutPage;
        public string errorPage;
        public bool active;
        public string idpURL;
        public bool signIdpRequest;
        public string idpBinding;
        public bool spInitiated;
        public Guid accountID;
    }

    public class SubDomainInfo
    {
        public Guid ID;
        public string SubDomain;
        public string SamlIdpName;
        public bool Active;
    }
}