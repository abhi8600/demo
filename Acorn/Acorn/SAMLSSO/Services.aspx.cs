using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Xml;
using System.Web;
using System.Web.Security;
using Atp.Saml;
using Atp.Saml2;
using Atp.Saml2.Binding;
using System.Web.UI;
using System.Xml.Linq;
using System;
using Acorn.DBConnection;
using System.Collections.Generic;
using Acorn.Utils;
using System.Collections.Specialized;

namespace Acorn.SAMLSSO
{
    public partial class Services : System.Web.UI.Page
    {
        /// <summary>
        /// Receives the SAML response from the identity provider.
        /// </summary>
        /// <param name="samlResponse">The SAML Response object.</param>
        /// <param name="relayState">The relay state object.</param>
        /// 
        /// The query string variable that indicates the IdentityProvider to ServiceProvider binding.
        /// </summary>
        public const string BindingVarName = "binding";
        private X509Certificate2 x509Certificate = null;
        private IdpMetaData info = null;
        private bool ignoreRedirectError = false;
        /// <summary>
        /// The query string parameter that contains error description for the login failure. 
        /// </summary>
        public const string ErrorVarName = "error";
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        protected void sendAuthRequest()
        {

            AuthnRequest authnRequest = BuildAuthenticationRequest(null, true);

            // Create and cache the relay state so we remember which SP resource the user wishes 
            // to access after SSO and lots of other stuff

            string certificate = WebConfigurationManager.AppSettings[this.Request.Url.Host + "Certificate"];
            string spResourceUrl = GetAbsoluteUrl(this, FormsAuthentication.GetRedirectUrl("", false));
            string relayState = System.Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new System.TimeSpan(1, 0, 0));
            string idpUrl = authnRequest.Destination;
            byte[] cert = new byte[certificate.Length * sizeof(char)];
            System.Buffer.BlockCopy(certificate.ToCharArray(), 0, cert, 0, cert.Length);
            x509Certificate = new X509Certificate2(cert);

            // Send the authentication request to the identity provider over the selected binding.
            switch (authnRequest.ProtocolBinding)
            {
                case SamlBindingUri.HttpRedirect:
                    authnRequest.Redirect(Response, idpUrl, relayState, x509Certificate.PrivateKey);
                    break;

                case SamlBindingUri.HttpPost:
                    authnRequest.SendHttpPost(Response, idpUrl, relayState);
                    // Don't send this form.
                    Response.End();
                    break;

                default:
                    throw new Exceptions.BirstException("Unacceptable binding type");
            }

        }

        /// <summary>
        /// In .NET Redirect can throw ThreadAbort exception. The flag is used to 
        /// bypass a wrong redirection error
        /// </summary>
        /// <param name="ignore"></param>
        private void setIgnoreRedirectException(bool ignore)
        {
            ignoreRedirectError = ignore;
        }


        private AuthnRequest BuildAuthenticationRequest(string spURL, bool signAuthn)
        {
            string birstCert = Server.MapPath("~/Birst2Cert.pfx");
            string SSOIdProviderUrl = "";
            //Loading the requsters config
            string requesterDomain = this.Request.Url.Host;
            Global.systemLog.Info("Request domain " + requesterDomain);
            if (!string.IsNullOrWhiteSpace(spURL))
            {
                SSOIdProviderUrl = spURL;
            }
            else
            {
                SSOIdProviderUrl = WebConfigurationManager.AppSettings[requesterDomain + "IdProviderUrl"];
            }

            if (SSOIdProviderUrl.Equals(""))
                return null;

            X509Certificate2 x509Certificate = new X509Certificate2();
            x509Certificate.Import(birstCert, "", X509KeyStorageFlags.MachineKeySet);
            // Construct the assertion Consumer Service Url.
            string assertionConsumerServiceUrl = getAssertionConsumerURL();

            // Create the authentication request.
            AuthnRequest authnRequest = new AuthnRequest();
            authnRequest.Destination = SSOIdProviderUrl;
            authnRequest.Issuer = new Issuer("Birst");
            authnRequest.ForceAuthn = false;
            authnRequest.NameIdPolicy = new NameIdPolicy(null, null, true);
            authnRequest.ProtocolBinding = "binding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
            authnRequest.AssertionConsumerServiceUrl = assertionConsumerServiceUrl;

            // Sign the authentication request.
            if (signAuthn)
            {
                Global.systemLog.Info("Signing Authnrequest");
                authnRequest.Sign(x509Certificate);
            }

            return authnRequest;

        }

        private string getAssertionConsumerURL()
        {
            string baseURL = Util.getRequestBaseURL(Request);
            return baseURL + "/SAMLSSO/Services.aspx";
        }

        public static string GetAbsoluteUrl(Page page, string relativeUrl)
        {
            return new System.Uri(page.Request.Url, page.ResolveUrl(relativeUrl)).ToString();
        }

        private void ExtractResponse(out Atp.Saml2.Response samlResponse, out string relayState, out bool errorRedirect, out bool accountMappingCheck)
        {
            // Only using HTTP post as binding type. all other types will result in exeptions
            errorRedirect = false;
            accountMappingCheck = true;
            samlResponse = null;
            relayState = null;
            X509Certificate2 x509Certificate = null;
            try
            {
                //Global.systemLog.Info("--SAML-- Request params: " + Request.Params.Get(0));
                //Global.systemLog.Info("--SAML-- Request params: " + Request.Params.Get(1));

                string samlResponseStr = Request.Params["SAMLResponse"];
                if (samlResponseStr == null || samlResponseStr.Trim().Length == 0)
                {
                    Global.systemLog.Info("No SAMLResponse found");
                    return;
                }

                samlResponse = Atp.Saml2.Response.Create(Request);
                relayState = samlResponse.RelayState;

                string idpIssuerID = samlResponse.Issuer.NameIdentifier;
                info = SamlUtils.getIdpMetaDataByIssuerID(idpIssuerID);
                string certificate = null;
                if (info == null)
                {
                    // see if this is an older version where it was storied in Web.config
                    Global.systemLog.Info("Saml Certificate not found in db. Looking in Web.config file");
                    certificate = WebConfigurationManager.AppSettings[idpIssuerID + "Certificate"];
                    if (string.IsNullOrWhiteSpace(certificate))
                    {
                        Global.systemLog.Error("--SAML-- an active info for this Idp does not exist in the system.");
                        errorRedirect = true;
                        SamlErrorRedirect(samlResponse);
                        return;
                    }
                    accountMappingCheck = false; 
                }
                else
                {
                    certificate = info.cert;
                }
                Global.systemLog.Info("--SAML-- idp Host value: " + idpIssuerID);
                try
                {
                    byte[] cert = new byte[certificate.Length * sizeof(char)];
                    System.Buffer.BlockCopy(certificate.ToCharArray(), 0, cert, 0, cert.Length);
                    x509Certificate = new X509Certificate2(cert);
                }
                catch (NullReferenceException e)
                {
                    Global.systemLog.Warn("--SAML-- Idp does not exist in the system." + e.ToString());
                    errorRedirect = true;
                    SamlErrorRedirect(samlResponse);
                    return;
                }

                if (SamlUtils.isEncryptionEnabled(samlResponse) && !samlResponse.IsSigned())
                {
                    // verify the signed assertion
                    Assertion assertion = null;
                    if (SamlUtils.validateEncryption(Server, samlResponse, out assertion) && assertion != null)
                    {
                        bool validAssertion = assertion.IsSigned() && assertion.Validate(x509Certificate);
                        if (!validAssertion)
                        {
                            Global.systemLog.Error("Encrypted assertion : Able to decrypt but Unable to verify the signature on the decrypted assertion");
                            errorRedirect = true;
                            SamlErrorRedirect(samlResponse);
                            return;
                        }
                    }
                    else
                    {
                        Global.systemLog.Error("Unable to validate encrypted assertion. Not able to decrypt.");
                        errorRedirect = true;
                        SamlErrorRedirect(samlResponse);
                        return;
                    }
                }
                else if (!samlResponse.Validate(x509Certificate))
                {
                    Global.systemLog.Error("The SAML response signature failed to verify. Requester NameIdentifier : " + idpIssuerID + "SAML Response: " + samlResponse.GetXml());
                    errorRedirect = true;
                    SamlErrorRedirect(samlResponse);
                    return;
                }

                Global.systemLog.Info(idpIssuerID + " Successfully initiated a SAML call.");
            }
            catch (Atp.Saml.SamlException e)
            {
                Global.systemLog.Error("Bad Saml SSO Request." + " Exception: " + e.ToString());
                errorRedirect = true;
                SamlErrorRedirect(samlResponse);
                return;
            }
            catch (NullReferenceException e)
            {
                Global.systemLog.Warn("Empty SAML response" + e.ToString());
                return;
            }

        }

        /// <summary>
        /// Processes a successful SAML response.
        /// </summary>
        private void SamlSuccessRedirect(Atp.Saml2.Response samlResponse, string relayState, 
            List<Atp.Saml2.Attribute> attributes, bool overrideRelayStateWithAttributes)
        {   
            char[] delimiterChars = { '=', '&', ';' };
            string userName, embedded="", module="";
            Guid spaceId;
            string[] tempParams = relayState.Split(delimiterChars);

            System.Collections.Specialized.NameValueCollection parameters = HttpUtility.ParseQueryString(relayState);
            userName = parameters.Get("username");
            embedded = parameters.Get("embedded");
            module = parameters.Get("module");
            spaceId = Guid.Parse(parameters.Get("BirstSpaceId"));
     
            string sessionVarParams = parameters.Get("sessionvars");
          
            if (sessionVarParams != null && sessionVarParams.Length > 0)
            {
                // not sure what these replacements are for. Not removing them for backward compatility purposes
                sessionVarParams = sessionVarParams.Replace(",", "\',\'");
                sessionVarParams = sessionVarParams.Replace("=", "=\'");
                sessionVarParams = sessionVarParams.Replace(";","\';") + "\'";
            }

            IDictionary<string, string> attributesDict = SamlUtils.formatBirstAttributes(attributes);
            if (overrideRelayStateWithAttributes)
            {
                embedded = getBirstAttributeValue(attributesDict, "birst.embedded", embedded);
                module = getBirstAttributeValue(attributesDict, "birst.module", module);                
                spaceId = new Guid(getBirstAttributeValue(attributesDict, "birst.spaceId", spaceId.ToString()));
                sessionVarParams = getBirstAttributeValue(attributesDict, "birst.sessionVars", sessionVarParams);
            }

            QueryConnection conn = ConnectionPool.getConnection();
            bool lockedout = false;

            // getting the actual Birst User
            User u = Database.getUser(conn, mainSchema, userName);

            if (u == null)
            {
                Global.systemLog.Error("The Birst user associated with the SAML SSO is not in the database or has expired products - " + userName);
                SamlErrorRedirect(samlResponse);
                return;
            }
            if (u.Disabled)
            {
                Global.systemLog.Error("The Birst user associated with the SAML SSO has been disabled - " + userName);
                SamlErrorRedirect(samlResponse);
                return;
            }
            Util.setLogging(u, null);
            // determine the site redirection
            string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
            string redirectURL = null;
            // no site means go to the original URL (backwards compatibility)
            if (userRedirectedSite == null || userRedirectedSite.Length == 0)
            {
                redirectURL = Util.getRequestBaseURL(Request);
            }
            else
            {
                redirectURL = userRedirectedSite;
            }
            // get the Birst security token and validate the user (products, IP restrictions)

            string token = getSAMLSecurityToken(conn, u, spaceId, sessionVarParams, ref lockedout, info);
            if (token == null || token.Length == 0)
            {
                if (lockedout)
                {
                    Global.systemLog.Error("Login has been locked after too many unsuccessful attempts");
                    SamlErrorRedirect(samlResponse);
                }
                else
                {
                    Global.systemLog.Error("The SpaceID " + spaceId + " is not associated with the user  " + userName);
                    SamlErrorRedirect(samlResponse);
                }
                return;

            }

            NameValueCollection nameValCollection = Request.QueryString;            
            if (overrideRelayStateWithAttributes)
            {
                nameValCollection = new NameValueCollection();
                string dashboardPrompts = getBirstAttributeValue(attributesDict, "birst.dashboardParams", null);
                string dashboardPromptPathSeparator = getBirstAttributeValue(attributesDict, "birst.dashParamsSeparator", null);
                NameValueCollection promptNvc = SamlUtils.fixUpPromptParams(dashboardPrompts, dashboardPromptPathSeparator);
                if (promptNvc != null && promptNvc.Count > 0)
                {
                    nameValCollection.Add(promptNvc);
                }
                HashSet<string> toStripKeys = new HashSet<string>() { "birst.spaceId", "birst.sessionVars", "birst.module", "birst.embedded",
                    "birst.dashboardParams", "birst.dashParamsSeparator" };
                SamlUtils.removeDictonaryItems(attributesDict, toStripKeys);
                NameValueCollection nvc = SamlUtils.getStrippedParams(attributesDict);
                if(nvc != null && nvc.Count > 0)
                {
                    nameValCollection.Add(nvc);
                }
            }
            // once authenticated, redirect to the SSO servlet
            token = Util.encryptToken(token);
            string redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, nameValCollection) + "&birst.module=" + module + "&birst.embedded=" + embedded;
            Global.systemLog.Debug("Redirecting to: " + redirectUrl + " Response Status Code is: " + Response.StatusCode);            
            setIgnoreRedirectException(true);
            Response.Redirect(redirectUrl, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        private string getBirstAttributeValue(IDictionary<string,string> dictionary, string key, string defaultValue)
        {
            if (dictionary != null && dictionary.ContainsKey(key))
            {
                return dictionary[key] != null ? dictionary[key] : defaultValue;
            }
            return defaultValue;
        }

        /// <summary>
        /// Processes an error SAML response. 
        /// </summary>
        private void SamlErrorRedirect(Atp.Saml2.Response samlResponse)
        {
            try
            {                
                if (ignoreRedirectError)
                    return;
                
                if (info != null && info.errorPage != null && Util.isValidHttpUrl(info.errorPage))
                {                    
                    Response.Redirect(info.errorPage, false);
                }
                else
                {                    
                    Response.Redirect("~/SAMLSSO/Error.html", false);
                }
                
                Context.ApplicationInstance.CompleteRequest();
            }
            catch (System.Exception)
            {
                Global.systemLog.Info("Redirected to the Error Page");
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                Atp.Saml2.Response samlResponse;
                string relayState;
                bool errorRedirect = false;
                // Get the SAML response
                bool accountMappingCheck = true;
                ExtractResponse(out samlResponse, out relayState, out errorRedirect, out accountMappingCheck);
                if (errorRedirect)
                    return;
                
                if (samlResponse == null)
                {
                    Global.systemLog.Info("The SAML response was null, creating auth.");
                    if (createAuth())
                    {
                        Context.ApplicationInstance.CompleteRequest();
                        return;
                    }
                    else
                    {
                        SamlErrorRedirect(samlResponse);
                    }
                    return;
                }

                // It indicates a success or an error?
                if (samlResponse.IsSuccess())
                {
                    Assertion assertion = null;
                    Global.systemLog.Info("Relay state before parse assertion" + relayState);
                    bool isRelayStateForDeepLink = isRelayStateForDeepLinking(relayState);
                    // need to detect some customer (Moto) -- which passes relaystate with space informaiton in original relay state
                    bool overrideRelayStateWithAttributeInfo = false; 
                    if (relayState == null || relayState.Equals("") || isRelayStateForDeepLink)
                    {
                        overrideRelayStateWithAttributeInfo = true;
                        try
                        {
                            // if encrypted..check for validity
                            if (SamlUtils.isEncryptionEnabled(samlResponse))
                            {
                                if (!SamlUtils.validateEncryption(Server, samlResponse, out assertion))
                                {
                                    Global.systemLog.Error("Encryption validatation failed for saml assertion");
                                    SamlErrorRedirect(samlResponse);
                                    return;
                                }
                            }
                            else
                            {
                                assertion = samlResponse.GetAssertions()[0];                                                        
                            }

                            User user = SamlUtils.getSamlUserFromAssertion(assertion, samlResponse.Issuer);
                            if (user == null)
                            {
                                Global.systemLog.Error("Unable to get user from saml assertion ");
                                SamlErrorRedirect(samlResponse);
                                return;
                            }

                            if (accountMappingCheck && !SamlUtils.isValidSamlAccount(user, info))
                            {
                                Global.systemLog.Error("Account Validation : User sent via saml assertion (" +  user.Username + ") does not belong to the same account as registered SAML - ("
                                    + info.ID + " : " + info.idpIssuerID + " : " + info.idpName + ")");
                                SamlErrorRedirect(samlResponse);
                                return;
                            }                            

                            if (isRelayStateForDeepLink)
                            {
                                Global.systemLog.Info("Relay state found for deep-linking : " + relayState);
                                relayState = SamlUtils.getSPInitiatedRelayState(user, relayState);
                            }
                            else
                            {
                                relayState = SamlUtils.getIdpInitiatedRelayState(user);
                            }

                            Global.systemLog.Info("Relay state after parse assertion" + relayState);
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("Problem parsing assertion " + ex.Message, ex);
                            SamlErrorRedirect(samlResponse);
                            return;
                        }
                    }

                    // Process the success response.
                    Global.systemLog.Info("The SAML response was good, redirecting to SSOToken processing.");
                    List<Atp.Saml2.Attribute> birstSamlAttributes = SamlUtils.getSamlBirstAttributes(assertion);
                    SamlSuccessRedirect(samlResponse, relayState, birstSamlAttributes, overrideRelayStateWithAttributeInfo);
                    return;
                }
                else
                {
                    // Process the error response.
                    Global.systemLog.Error("Error while extracting the response, redirecting to error.");
                    SamlErrorRedirect(samlResponse);
                    return;
                }
            }
            catch (System.Exception exception)
            {
                Global.systemLog.Info("Error onLoad for SAMLSSO Services " + exception);
                SamlErrorRedirect(null);
                return;
            }
        }





        protected bool createAuth()
        {
            // Create the authentication request.

            string relayState = getRelayStateForDeepLinking(Request);
            relayState = relayState == null ? "" : relayState;
            Global.systemLog.Info("Relay state in the request : " + relayState);
            SubDomainInfo subDomainInfo = SamlUtils.getSubDomainInfo(Request);            
            if (subDomainInfo != null && subDomainInfo.Active)
            {
                Global.systemLog.Info("Registered SubDomain IDP Name " + subDomainInfo.SamlIdpName);
                if (info == null)
                {
                    info = SamlUtils.getIdpMetaDataByName(subDomainInfo.SamlIdpName);
                }

                string idpUrl = info != null ? info.idpURL : null;
                if (string.IsNullOrWhiteSpace(idpUrl))
                {
                    return false;
                }

                if (!info.spInitiated)
                {
                    Global.systemLog.Info("Redirecting to idp url (effectively an idp-initiated request)");
                    setIgnoreRedirectException(true);
                    // It is effectively redirecting to idp and simulating an idp-initated request
                    Response.Redirect(idpUrl, false);
                    return true;
                }

                AuthnRequest authnRequest = BuildAuthenticationRequest(idpUrl, info.signIdpRequest);
                Global.systemLog.Info("idpurl " + idpUrl);

                if (info.idpBinding == SamlBindingUri.HttpRedirect)
                {
                    setIgnoreRedirectException(true);
                    Global.systemLog.Info("Sending SP-initiated via HTTP-Redirect");
                    authnRequest.Redirect(Response, idpUrl, relayState, null);
                }
                else
                {
                    Global.systemLog.Info("SP-initiated via HTTP-POST");
                    setIgnoreRedirectException(true);
                    authnRequest.SendHttpPost(Response, idpUrl, relayState);
                }
                return true;
            }
            else
            {
                Global.systemLog.Error("No idp information found for subdomain from url " + Request.Url);
                return false;
            }
        }

        private static string getRelayStateForDeepLinking(HttpRequest request)
        {
            string state = request.QueryString[SamlUtils.KEY_RELAY_SAML_STATE];
            if (!string.IsNullOrWhiteSpace(state))
            {
                return SamlUtils.KEY_RELAY_SAML_STATE + "=" + state;
            }
            return null;
        }

        private static bool isRelayStateForDeepLinking(string relayState)
        {
            return !string.IsNullOrWhiteSpace(relayState) && relayState.IndexOf(SamlUtils.KEY_RELAY_SAML_STATE) >= 0;
        }

        private static bool isLockedOut(User u)
        {
            MembershipUser mu = Membership.GetUser(u.Username, false);
            if (mu != null && mu.IsLockedOut)
            {
                Global.systemLog.Warn("Security token request failed - user locked out");
                return true;
            }
            return false;
        }

        public static string getSAMLSecurityToken(QueryConnection conn, User u, Guid spaceId, string sessionVarParams, ref bool lockedout, IdpMetaData idpMetaData)
        {
            Global.systemLog.Info("Creating Security Token. UserName: " + u.ToString() + " Space ID: " + spaceId.ToString() + " Session vars: " + sessionVarParams);
            if ((lockedout = isLockedOut(u)) == true)
                return null;
            Space sp = null;
            if (spaceId != Guid.Empty)
            {
                sp = Util.getSpaceById(conn, mainSchema, u, spaceId);
                if (sp == null)
                {
                    Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceId + " or the space does not exist");
                    return null;
                }
            }

            if (!Global.initialized)
                return "";

            // create a token and store the parameters in the database
            SSOToken ssoToken = new SSOToken();
            ssoToken.tokenID = Guid.NewGuid();
            ssoToken.userID = u.ID;
            ssoToken.spaceID = sp != null ? sp.ID : Guid.Empty;
            ssoToken.sessionVariables = sessionVarParams;
            ssoToken.repAdmin = u.RepositoryAdmin;
            ssoToken.type = "SAML SSO";
            ssoToken.timeout = idpMetaData != null ? idpMetaData.timeout : -1;
            ssoToken.logoutPage = idpMetaData != null ? idpMetaData.logoutPage : null;
            Database.createSSOToken(conn, mainSchema, ssoToken);
            return ssoToken.tokenID.ToString();

        }

    }
}

