﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class BirstException: Exception
    {
        public BirstException()
        { 
        }

        public BirstException(string message)
            : base(message)
        {
        }

        public BirstException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
