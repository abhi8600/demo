﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using System.IO;
using Acorn.DBConnection;

namespace Acorn
{
    public class Upgrade
    {
        int currentVersion;
        int newVersion;
        string mainSchema;
        QueryConnection mainConn;

        public Upgrade(int currentVersion, int newVersion, string mainSchema, QueryConnection mainConn)
        {
            this.currentVersion = currentVersion;
            this.newVersion = newVersion;
            this.mainSchema = mainSchema;
            this.mainConn = mainConn;
        }

        public void run()
        {
            Global.systemLog.Info("Beginning upgrade process- current version: " + currentVersion + ", new version: " + newVersion);
            if (currentVersion < 1)
                upgradeTo1();
            if (currentVersion < 2)
                upgradeTo2();
            if (currentVersion < 3)
                upgradeTo3(mainConn, mainSchema);
            if (currentVersion < 4)
                upgradeTo4();
            if (currentVersion < 5)
                upgradeTo5(mainConn, mainSchema);
            if (currentVersion < 6)
                upgradeTo6(mainConn, mainSchema);
            if (currentVersion < 7)
                upgradeTo7(mainConn, mainSchema);
            if (currentVersion < 8)
                upgradeTo8(mainConn, mainSchema);
            if (currentVersion < 9)
                upgradeTo9(mainConn, mainSchema);
            if (currentVersion < 10)
                upgradeTo10(mainConn, mainSchema);
            if (currentVersion < 11)
                upgradeTo11(mainConn, mainSchema);
            if (currentVersion < 12)
                upgradeTo12(mainConn, mainSchema);
            if (currentVersion < 13)
                upgradeTo13(mainConn, mainSchema);
            if (currentVersion < 14)
                upgradeTo14(mainConn, mainSchema);
            if (currentVersion < 15)
                upgradeTo15(mainConn, mainSchema);
            if (currentVersion < 16)
                upgradeTo16(mainConn, mainSchema);
            if (currentVersion < 17)
                upgradeTo17(mainConn, mainSchema);
            if (currentVersion < 18)
                upgradeTo18(mainConn, mainSchema);
            if (currentVersion < 19)
                upgradeTo19(mainConn, mainSchema);
            if (currentVersion < 20)
                upgradeTo20(mainConn, mainSchema);
            if (currentVersion < 21)
                upgradeTo21(mainConn, mainSchema);
            if (currentVersion < 22)
                upgradeTo22(mainConn, mainSchema);
            if (currentVersion < 23)
                upgradeTo23(mainConn, mainSchema);
            if (currentVersion < 24)
                upgradeTo24(mainConn, mainSchema);
            if (currentVersion < 25)
                upgradeTo25(mainConn, mainSchema);
            if (currentVersion < 26)
                upgradeTo26(mainConn, mainSchema);
            if (currentVersion < 27)
                upgradeTo27(mainConn, mainSchema);
            if (currentVersion < 28)
                upgradeTo28(mainConn, mainSchema);
            if (currentVersion < 29)
                upgradeTo29(mainConn, mainSchema);
            if (currentVersion < 30)
                upgradeTo30(mainConn, mainSchema);
            if (currentVersion < 31)
                upgradeTo31(mainConn, mainSchema);
            if (currentVersion < 32)
                upgradeTo32(mainConn, mainSchema);
            if (currentVersion < 33)
                upgradeTo33(mainConn, mainSchema);
            if (currentVersion < 34)
                upgradeTo34(mainConn, mainSchema);
            if (currentVersion < 35)
                upgradeTo35(mainConn, mainSchema);
            if (currentVersion < 36)
                upgradeTo36(mainConn, mainSchema);
            if (currentVersion < 37)
                upgradeTo37(mainConn, mainSchema);
            if (currentVersion < 38)
                upgradeTo38(mainConn, mainSchema);
            if (currentVersion < 39)
                upgradeTo39(mainConn, mainSchema);
            if (currentVersion < 40)
                upgradeTo40(mainConn, mainSchema);
            if (currentVersion < 41)
                upgradeTo41(mainConn, mainSchema);
            if (currentVersion < 42)
                upgradeTo42(mainConn, mainSchema);
            if (currentVersion < 43)
                upgradeTo43(mainConn, mainSchema);
            if (currentVersion < 44)
                upgradeTo44(mainConn, mainSchema);
            if (currentVersion < 45)
                upgradeTo45(mainConn, mainSchema);
            if (currentVersion < 46)
                upgradeTo46(mainConn, mainSchema);
            if (currentVersion < 47)
                upgradeTo47(mainConn, mainSchema);
            if (currentVersion < 48)
                upgradeTo48(mainConn, mainSchema);
            if (currentVersion < 49)
                upgradeTo49(mainConn, mainSchema);
            if (currentVersion < 50)
                upgradeTo50(mainConn, mainSchema);
            if (currentVersion < 51)
                upgradeTo51(mainConn, mainSchema);
            if (currentVersion < 52)
                upgradeTo52(mainConn, mainSchema);
            if (currentVersion < 53)
                upgradeTo53(mainConn, mainSchema);
            if (currentVersion < 54)
                upgradeTo54(mainConn, mainSchema);
            if (currentVersion < 55)
                upgradeTo55(mainConn, mainSchema);
            if (currentVersion < 56)
                upgradeTo56(mainConn, mainSchema);
            if (currentVersion < 57)
                upgradeTo57(mainConn, mainSchema);
            if (currentVersion < 58)
                upgradeTo58(mainConn, mainSchema);
            if (currentVersion < 59)
                upgradeTo59(mainConn, mainSchema);
            if (currentVersion < 60)
                upgradeTo60(mainConn, mainSchema);
            if (currentVersion < 61)
                upgradeTo61(mainConn, mainSchema);
            if (currentVersion < 62)
                upgradeTo62(mainConn, mainSchema);
            if (currentVersion < 63)
                upgradeTo63(mainConn, mainSchema);
            if (currentVersion < 64)
                upgradeTo64(mainConn, mainSchema);
            if (currentVersion < 65)
                upgradeTo65(mainConn, mainSchema);
            if (currentVersion < 66)
                upgradeTo66(mainConn, mainSchema);
            if (currentVersion < 67)
                upgradeTo67(mainConn, mainSchema);
            if (currentVersion < 68)
                upgradeTo68(mainConn, mainSchema);
            if (currentVersion < 69)
                upgradeTo69(mainConn, mainSchema);
            if (currentVersion < 70)
                upgradeTo70(mainConn, mainSchema);
            if (currentVersion < 71)
                upgradeTo71(mainConn, mainSchema);
            if (currentVersion < 72)
                upgradeTo72(mainConn, mainSchema);
            if (currentVersion < 73)
                upgradeTo73(mainConn, mainSchema);
            if (currentVersion < 74)
                upgradeTo74(mainConn, mainSchema);
            if (currentVersion < 75)
                upgradeTo75(mainConn, mainSchema);
            if (currentVersion < 76)
                upgradeTo76(mainConn, mainSchema);
            if (currentVersion < 77)
                upgradeTo77(mainConn, mainSchema);
            if (currentVersion < 78)
                upgradeTo78(mainConn, mainSchema);
            if (currentVersion < 79)
                upgradeTo79(mainConn, mainSchema);
            if (currentVersion < 80)
                upgradeTo80(mainConn, mainSchema);
            if (currentVersion < 81)
                upgradeTo81(mainConn, mainSchema);
            if (currentVersion < 82)
                upgradeTo82(mainConn, mainSchema);
            if (currentVersion < 83)
                upgradeTo83(mainConn, mainSchema);
            if (currentVersion < 84)
                upgradeTo84(mainConn, mainSchema);
            if (currentVersion < 85)
                upgradeTo85(mainConn, mainSchema);
            if (currentVersion < 86)
                upgradeTo86(mainConn, mainSchema);
            if (currentVersion < 87)
                upgradeTo87(mainConn, mainSchema);
            if (currentVersion < 88)
                upgradeTo88(mainConn, mainSchema);
            if (currentVersion < 89)
                upgradeTo89(mainConn, mainSchema);
            if (currentVersion < 90)
                upgradeTo90(mainConn, mainSchema);
            if (currentVersion < 91)
                upgradeTo91(mainConn, mainSchema);
             if (currentVersion < 92)
                upgradeTo92(mainConn, mainSchema);
             if (currentVersion < 93)
                 upgradeTo93(mainConn, mainSchema);
            if (currentVersion < 94)
                 upgradeTo94(mainConn, mainSchema);
             if (currentVersion < 95)
                 upgradeTo95(mainConn, mainSchema);
             if (currentVersion < 96)
                 upgradeTo96(mainConn, mainSchema);
             if (currentVersion < 97)
                 upgradeTo97(mainConn, mainSchema);
             if (currentVersion < 98)
                 upgradeTo98(mainConn, mainSchema);
             if (currentVersion < 99)
                 upgradeTo99(mainConn, mainSchema);
             if (currentVersion < 100)
                 upgradeTo100(mainConn, mainSchema);
             if (currentVersion < 101)
                 upgradeTo101(mainConn, mainSchema);
             if (currentVersion < 102)
                 upgradeTo102(mainConn, mainSchema);
             if (currentVersion < 103)
                 upgradeTo103(mainConn, mainSchema);
             Global.systemLog.Info("Successfully upgraded");
            Database.updateCurrentVersion(mainConn, mainSchema, newVersion);
        }


        public void upgradeSpace(Space sp)
        {
            Global.systemLog.Info("Beginning upgrade process of space: " + sp.ID + " - current version: " + sp.AppVersion + ", new version: " + newVersion);
            bool upgraded = false;
            if (sp.AppVersion < 50)
                upgraded = upgradeSpaceTo50(mainConn, mainSchema, sp);
            if (upgraded)
            {
                sp.AppVersion = Global.CURRENT_VERSION;                
                Database.updateNonDBSpaceParams(mainConn, mainSchema, sp);                
            }            
            Global.systemLog.Info("Successfully upgraded: "+sp.ID);
        }

        private void upgradeTo1()
        {
            List<Space> slist = Database.getSpaces(mainConn, mainSchema);
            // Upgrade spaces to include error counts in the transaction table
            Dictionary<string, QueryConnection> connectionCache = new Dictionary<string, QueryConnection>();
            foreach (Space sp in slist)
            {
                try
                {
                    string connString = sp.getFullConnectString();
                    QueryConnection conn = null;
                    if (!connectionCache.ContainsKey(connString))
                    {
                        conn = ConnectionPool.getConnection(connString);
                        connectionCache.Add(connString, conn);
                    }
                    else
                        conn = connectionCache[connString];
                    List<string> colNames = new List<string>();
                    List<string> colTypes = new List<string>();
                    List<int> actions = new List<int>();
                    colNames.Add("NUMERRORS");
                    colTypes.Add(mainConn.getBigIntType());
                    actions.Add(Database.ALTER_ADD);
                    Database.alterTable(conn, sp.Schema, "TXN_COMMAND_HISTORY", colNames, colTypes, actions);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
            }
        }

        private void upgradeTo2()
        {
            List<Space> slist = Database.getSpaces(mainConn, mainSchema);
            // Upgrade spaces to include error counts in the transaction table
            Dictionary<string, QueryConnection> connectionCache = new Dictionary<string, QueryConnection>();
            foreach (Space sp in slist)
            {
                try
                {
                    DirectoryInfo di = new DirectoryInfo(sp.Directory);
                    if (di.Exists)
                    {
                        FileInfo[] files = di.GetFiles("*.jasper", SearchOption.AllDirectories);
                        foreach (FileInfo fi in files)
                        {
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception)
                            {
                            }
                        }
                        files = di.GetFiles("*.smijasper", SearchOption.AllDirectories);
                        foreach (FileInfo fi in files)
                        {
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    di = new DirectoryInfo(sp.Directory + "\\catalog\\shared\\Auto-Generated");
                    if (di.Exists)
                    {
                        FileInfo[] files = di.GetFiles("*.*", SearchOption.AllDirectories);
                        foreach (FileInfo fi in files)
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception)
                            {
                            }
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
            }
        }

        private void upgradeTo3(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Demo");
            colTypes.Add(conn.getBooleanType("Demo"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, "SPACES", colNames, colTypes, actions);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, "SPACES") + " SET Demo=0";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo4()
        {
            List<Space> slist = Database.getSpaces(mainConn, mainSchema);
            // Upgrade spaces to include error counts in the transaction table
            Dictionary<string, QueryConnection> connectionCache = new Dictionary<string, QueryConnection>();
            foreach (Space sp in slist)
            {
                try
                {
                    string connString = sp.getFullConnectString();
                    QueryConnection conn = null;
                    if (!connectionCache.ContainsKey(connString))
                    {
                        conn = ConnectionPool.getConnection(connString);
                        connectionCache.Add(connString, conn);
                    }
                    else
                        conn = connectionCache[connString];
                    List<string> colNames = new List<string>();
                    List<string> colTypes = new List<string>();
                    List<int> actions = new List<int>();
                    colNames.Add("NUMWARNINGS");
                    colTypes.Add(conn.getBigIntType());
                    actions.Add(Database.ALTER_ADD);
                    Database.alterTable(conn, sp.Schema, "TXN_COMMAND_HISTORY", colNames, colTypes, actions);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                }
            }
        }

        private void upgradeTo5(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ExternalBilling");
            colTypes.Add(conn.getBooleanType("ExternalBilling"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " SET ExternalBilling=0";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo6(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("TableName");
            colTypes.Add(conn.getVarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PUBLISH_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo7(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("RepositoryAdmin");
            colTypes.Add(conn.getBooleanType("RepositoryAdmin"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " SET RepositoryAdmin=0";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo8(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ConnectString");
            colTypes.Add(conn.getVarcharType(120, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo9(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.PRODUCTS_TABLE);
        }

        private void upgradeTo10(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Department");
            colTypes.Add(conn.getVarcharType(50, false));
            colNames.Add("Industry");
            colTypes.Add(conn.getVarcharType(50, false));
            colNames.Add("OrgSize");
            colTypes.Add(conn.getVarcharType(15, false));
            colNames.Add("LikelyUse");
            colTypes.Add(conn.getVarcharType(20, false));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo11(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DefaultSpace");
            colTypes.Add(conn.getVarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("DefaultDashboards");
            colTypes.Add(conn.getBooleanType("DefaultDashboards"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " SET DefaultDashboards=0";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo12(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("LoadGroup");
            colTypes.Add(conn.getVarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PUBLISH_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo13(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SSO");
            colTypes.Add(conn.getBooleanType("SSO"));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("SSOPWD");
            colTypes.Add(conn.getVarcharType(60, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo14(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("LOGO_IMAGE_ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            colNames.Add("HeaderBackground");
            colTypes.Add(conn.getBigIntType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo15(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Private");
            colTypes.Add(conn.getBooleanType("Private"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.TEMPLATES_TABLE_NAME, colNames, colTypes, actions);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.TEMPLATES_TABLE_NAME) + " SET Private=0";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo16(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("PartnerCode");
            colTypes.Add(conn.getVarcharType(60, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo17(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SSOPWD");
            colTypes.Add(conn.getVarcharType(128, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo18(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.PRODUCTS_TABLE);
        }

        private void upgradeTo19(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.SFORCE_SCHEDULE_TABLE);
        }

        private void upgradeTo20(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("CreateNewUserRights");
            colTypes.Add(conn.getBooleanType("CreateNewUserRights"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo21(QueryConnection conn, string mainSchema)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " SET Username=LOWER(Username)";
            cmd.ExecuteNonQuery();
        }

        private void upgradeTo22(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("WorkTime");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SCHEDULED_LOAD_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo23(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.PRODUCTS_TABLE);
        }

        private void upgradeTo24(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("WorkingServer");
            colTypes.Add(conn.getVarcharType(40, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Parameters");
            colTypes.Add(conn.getVarcharType(-1, true));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SCHEDULED_LOAD_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo25(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("OBJ_ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SCHEDULED_LOAD_TABLE, colNames, colTypes, actions);
            Database.createIndex(conn, mainSchema, Database.SCHEDULED_LOAD_TABLE, "IDX1", new string[] { "SPACE_ID" });
        }

        private void upgradeTo26(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.SCHEDULED_REPORTS_TABLE);
        }

        private void upgradeTo27(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(conn, mainSchema, Database.PRODUCTS_TABLE);
        }

        private void upgradeTo28(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Type");
            colTypes.Add(conn.getVarcharType(20, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.SCHEDULED_REPORTS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo29(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("QueryConnectString");
            colTypes.Add(conn.getVarcharType(120, false));
            colNames.Add("QueryDatabaseType");
            colTypes.Add(conn.getVarcharType(60, false));
            colNames.Add("QueryDatabaseDriver");
            colTypes.Add(conn.getVarcharType(60, false));
            colNames.Add("QueryUser");
            colTypes.Add(conn.getVarcharType(60, false));
            colNames.Add("QueryPWD");
            colTypes.Add(conn.getVarcharType(60,false));
            colNames.Add("QueryConnectionName");
            colTypes.Add(conn.getVarcharType(60,false));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo30(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("StartPublishDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PUBLISH_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo31(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Status");
            colTypes.Add(conn.getVarcharType(100, false));
            actions.Add(Database.ALTER_ADD);
            if (!Database.alterTable(conn, mainSchema, Database.SCHEDULED_LOAD_TABLE, colNames, colTypes, actions))
            {
                Global.systemLog.Info("Please ignore any error regarding alter table - columns already exist for table. " +
                    "This will potentially happen because we will need to backdate a version from 31 to 30 during 4.3 push " + Database.SCHEDULED_LOAD_TABLE);
            }
        }

        private void upgradeTo32(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("MaxQueryRows");
            colTypes.Add("int");
            colNames.Add("MaxQueryTimeout");
            colTypes.Add("int");
            colNames.Add("QueryLanguageVersion");
            colTypes.Add("int");
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);

            colNames = new List<string>();
            colTypes = new List<string>();
            actions = new List<int>();
            colNames.Add("MappingModifiedDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo33(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ADMIN_ACCOUNT_ID");
            colNames.Add("MANAGED_ACCOUNT_ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo34(QueryConnection conn, string mainSchema)
        {
            if (!Database.tableExists(conn, mainSchema, Database.ACCOUNTS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ACCOUNT_ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "ExpirationDate", conn.getDateTimeType() });
                columns.Add(new string[] { "MaxConcurrentLogins", "Int" });
                columns.Add(new string[] { "MaxUsers", "Int" });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ModifiedDate", conn.getDateTimeType() });
                Database.createTable(conn, mainSchema, Database.ACCOUNTS_TABLE, columns, false);
            }
            else
            {
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("ExpirationDate");
                colTypes.Add(conn.getDateTimeType());
                actions.Add(Database.ALTER_ADD);
                Database.alterTable(conn, mainSchema, Database.ACCOUNTS_TABLE, colNames, colTypes, actions);
            }
        }

        private void upgradeTo35(QueryConnection conn, string mainSchema)
        {
            if (!Database.tableExists(conn, mainSchema, Database.ACCOUNTS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ACCOUNT_ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "ExpirationDate", conn.getDateTimeType() });
                columns.Add(new string[] { "MaxConcurrentLogins", "Int" });
                columns.Add(new string[] { "MaxUsers", "Int" });
                columns.Add(new string[] { "IsEnterprise", conn.getBooleanType("IsEnterprise") });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ModifiedDate", conn.getDateTimeType() });
                Database.createTable(conn, mainSchema, Database.ACCOUNTS_TABLE, columns, false);
            }
            else
            {
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("IsEnterprise");
                colTypes.Add(conn.getBooleanType("IsEnterprise"));
                actions.Add(Database.ALTER_ADD);
                Database.alterTable(conn, mainSchema, Database.ACCOUNTS_TABLE, colNames, colTypes, actions);
            }
        }

        private void upgradeTo36(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Operations");
            colTypes.Add(conn.getBooleanType("Operations"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            // set the default value
            try
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " SET Operations=0";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting the default value of Operations flag in Users table", ex);
            }
        }

        private void upgradeTo37(QueryConnection conn, string mainSchema)
        {
            if (!Database.tableExists(conn, mainSchema, Database.CURRENT_LOGINS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "Remote_IP", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Login_Time", conn.getDateTimeType() });
                columns.Add(new string[] { "Logout_Time", conn.getDateTimeType() });
                columns.Add(new string[] { "Session_ID", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Host_Server", conn.getVarcharType(255, false) });
                Database.createTable(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, columns, false);
                Database.createIndex(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, "IDX1", new string[] { "Session_ID" });
                Database.createIndex(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, "IDX2", new string[] { "Logout_Time" });
            }
            else
            {
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("Logout_Time");
                colTypes.Add(conn.getDateTimeType());
                actions.Add(Database.ALTER_ADD);
                Database.alterTable(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, colNames, colTypes, actions);
                Database.createIndex(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, "IDX1", new string[] { "Session_ID" });
                Database.createIndex(conn, mainSchema, Database.CURRENT_LOGINS_TABLE, "IDX2", new string[] { "Logout_Time" });
            }
        }

        private void upgradeTo38(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Type");
            colTypes.Add(conn.getVarcharType(128, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.SCHEDULED_REPORTS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo39(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("UsageTracking");
            colTypes.Add(conn.getBooleanType("UsageTracking"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }
        private void upgradeTo40(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DefaultSpaceId");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }
        private void upgradeTo41(QueryConnection conn, string mainSchema)
        {
            if (!Database.tableExists(conn, mainSchema, Database.SERVER_STATUS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "HOSTADDR", conn.getVarcharType(64, false) + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "STATUSFLAG", "INT NOT NULL" });
                columns.Add(new string[] { "MESSAGE", conn.getVarcharType(512, false) });
                Database.createTable(conn, mainSchema, Database.SERVER_STATUS_TABLE, columns, false);
            }
        }
        private void upgradeTo42(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Site");
            colTypes.Add(conn.getVarcharType(256, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }
        private void upgradeTo43(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SUPERUSER");
            colTypes.Add(conn.getBooleanType("SUPERUSER"));
            colNames.Add("REPADMIN");
            colTypes.Add(conn.getBooleanType("REPADMIN"));
            colNames.Add("TYPE");
            colTypes.Add(conn.getVarcharType(32, false));
            colNames.Add("HOSTADDR");
            colTypes.Add(conn.getVarcharType(64, false));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SSO_TOKENS_TABLE, colNames, colTypes, actions);
        }
        private void upgradeTo44(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("EngineCommand");
            colTypes.Add(conn.getVarcharType(1024, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }
        private void upgradeTo45(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DeleteDataCommand");
            colTypes.Add(conn.getVarcharType(1024, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo46(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ReleaseType");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo47(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ProcessVersion");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }
        private void upgradeTo48(QueryConnection conn, string mainSchema)
        {
            if (!Database.tableExists(conn, mainSchema, Database.PASSWORD_RESET_REQUEST_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TOKEN_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "USER_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "END_OF_LIFE", conn.getDateTimeType() });
                columns.Add(new string[] { "USED", conn.getBooleanType("USED") });
                Database.createTable(conn, mainSchema, Database.PASSWORD_RESET_REQUEST_TABLE, columns, false);
            }
        }

        private void upgradeTo49(QueryConnection conn, string mainSchema)
        {
            Database.createMessagesTable(conn, mainSchema);
        }

        private void upgradeTo50(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("AppVersion");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);

        }

        private bool upgradeSpaceTo50(QueryConnection conn, string mainSchema, Space sp)
        {
            return Database.addDurationColumnToTxnCommandHistory(sp);
        }

        private void upgradeTo51(QueryConnection conn, string mainSchema)
        {
            Database.createRunningProcessTable(conn, mainSchema);
        }

        private void upgradeTo52(QueryConnection conn, string mainSchema)
        {
            Database.createSpacesConfigTable(conn, mainSchema);
        }

        private void upgradeTo53(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Available");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo54(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("UseDynamicGroups");
            colTypes.Add(conn.getBooleanType("UseDynamicGroups"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo55(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Deleted");
            colTypes.Add(conn.getBooleanType("Deleted"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo56(QueryConnection conn, string mainSchema)
        {
            // set the default value (previously upgrad 55 set all spaces to 0, but that didn't work for spaces created after upgrade with an old release, so having non null mean deleted)
            try
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.SPACE_TABLE_NAME) + " SET Deleted=NULL WHERE Deleted=0";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while setting the default value of Operations flag in Users table", ex);
            }
        }

        private void upgradeTo57(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SupportsRetryLoad");
            colTypes.Add(conn.getBooleanType("SupportsRetryLoad"));
            colNames.Add("IsProcessingGroupAware");
            colTypes.Add(conn.getBooleanType("IsProcessingGroupAware"));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PROCESS_VERSIONS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo58(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("AllowRetryFailedLoad");
            colTypes.Add(conn.getBooleanType("AllowRetryFailedLoad"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo59(QueryConnection conn, string mainSchema)
        {
            Database.createOpenIdUsersTable(conn, mainSchema);
        }

        private void upgradeTo60(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SFDCVersion");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo61(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SupportSFDCExtract");
            colTypes.Add("bit");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PROCESS_VERSIONS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo62(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DeletedDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo63(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DisallowExternalScheduler");
            colTypes.Add(conn.getBooleanType("DisallowExternalScheduler"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo64(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ExpirationDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PROXY_USERS, colNames, colTypes, actions);
        }
        private void upgradeTo65(QueryConnection conn, string mainSchema)
        {
            // added extra column to a table, might fail, this is okay
            if (Database.tableExists(conn, mainSchema, Database.PASSWORD_HISTORY_TABLE))
            {
                // does it already exist
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("CHANGEDATE");
                colTypes.Add(conn.getDateTimeType());
                actions.Add(Database.ALTER_ADD);
                Global.systemLog.Warn("This ALTER TABLE might fail, this is okay");
                Database.alterTable(conn, mainSchema, Database.PASSWORD_HISTORY_TABLE, colNames, colTypes, actions);
            }
        }

        private void upgradeTo66(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SFDCNumThreads");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo67(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Discovery");
            colTypes.Add(conn.getBooleanType("Discovery"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo68(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Disabled");
            colTypes.Add(conn.getBooleanType("Disabled"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo69(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("MapNullsToZero");
            colTypes.Add(conn.getBooleanType("MapNullsToZero"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, "SPACES", colNames, colTypes, actions);
        }

        private void upgradeTo70(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SCHEDULERWEBLOG");
            colTypes.Add(conn.getVarcharType(1024, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SERVER_INFO_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo71(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("BCRegExp");
            colTypes.Add(conn.getVarcharType(2048, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Enabled");
            colTypes.Add(conn.getBooleanType("Enabled"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.RELEASE_INFO_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo72(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Disabled");
            colTypes.Add(conn.getBooleanType("Disabled"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.ACCOUNTS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo73(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("PARENT_SPACE_ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Available");
            colTypes.Add(conn.getBooleanType("Available"));
            actions.Add(Database.ALTER_ADD);
            if (Database.alterTable(conn, mainSchema, Database.PACKAGE_USAGE_INXN, colNames, colTypes, actions))
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + Database.getTableName(mainSchema, Database.PACKAGE_USAGE_INXN) + " SET Available=1";
                cmd.ExecuteNonQuery();
            }
        }

        private void upgradeTo74(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ConnectString");
            colTypes.Add(conn.getVarcharType(1024, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo75(QueryConnection conn, string mainSchema)
        {
            Database.createGlobalPropertiesTable(conn, mainSchema);
        }

        private void upgradeTo76(QueryConnection conn, string mainSchema)
        {
            bool containsIDColumn = false;
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID FROM " + mainSchema + "." + Database.GLOBAL_PROPERTIES_TABLE;
                reader = cmd.ExecuteReader();
                containsIDColumn = true;
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                { }
            }
            if (containsIDColumn)//don't add again
                return;
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.GLOBAL_PROPERTIES_TABLE, colNames, colTypes, actions);
        }

        public void upgradeTo77(QueryConnection conn, String mainchema)
        {
           
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Source");
            colTypes.Add(conn.getVarcharType(100, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("PID");
            colTypes.Add(conn.getVarcharType(100, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Info");
            colTypes.Add(conn.getVarcharType(256, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("CreatedDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.RUNNING_PROCESSES_TABLE, colNames, colTypes, actions);            
        }

        private void upgradeTo78(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ACCOUNT_ID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.ALLOWED_IPS, colNames, colTypes, actions);
            Database.createIndex(conn, mainSchema, Database.ALLOWED_IPS, "IDX2", new string[] { "ACCOUNT_ID" });
        }

        private void upgradeTo79(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("HTMLInterface");
            colTypes.Add(conn.getBooleanType("HTMLInterface"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo80(QueryConnection conn, string mainSchema)
        {
            // add the AWS information to SPACES and SPACES_CONFIG
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("AWSAccessKeyId");
            colNames.Add("AWSSecretKey");
            colTypes.Add(conn.getVarcharType(256, false));
            colTypes.Add(conn.getVarcharType(256, false));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
            Database.alterTable(conn, mainSchema, Database.SPACES_CONFIG_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo81(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SpaceType");
            colTypes.Add("int");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo82(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();            
            List<int> actions = new List<int>();
            colNames.Add("ReportFrom");
            colNames.Add("ReportSubject");
            colTypes.Add(conn.getVarcharType(128, false));
            colTypes.Add(conn.getVarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo83(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("CONNECTORWEBLOG");
            colTypes.Add(conn.getVarcharType(1024, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SERVER_INFO_TABLE, colNames, colTypes, actions);
        }
        
        private void upgradeTo84(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("HASHALGORITHM");
            colTypes.Add(conn.getVarcharType(255, false));
            actions.Add(Database.ALTER_ADD);;
            colNames.Add("MAXFAILEDLOGINATTEMPTS");
            colTypes.Add("int");
            actions.Add(Database.ALTER_ADD);
            colNames.Add("FAILEDLOGINWINDOW");
            colTypes.Add("int");
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.PASSWORD_POLICY_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo85(QueryConnection conn, string mainSchema)
        {
            // new password hashing requires more space
            try
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "ALTER TABLE " + Database.getTableName(mainSchema, Database.USER_TABLE_NAME) + " ALTER COLUMN PASSWORD VARCHAR(2048)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while widening the PASSWORD field in the USERS table", ex);
            }
        }

        private void upgradeTo86(QueryConnection conn, string mainSchema)
        {
            // unicode space names require nvarchar
            try
            {
                string maxnVarcharType = conn.getNvarcharType(-1, true);
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "ALTER TABLE " + Database.getTableName(mainSchema, Database.SPACE_TABLE_NAME) + " ALTER COLUMN NAME NVARCHAR(255)";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE " + Database.getTableName(mainSchema, Database.SPACE_TABLE_NAME) + " ALTER COLUMN COMMENTS " + maxnVarcharType;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while converting the NAME field in the SPACES table to nvarchar", ex);
            }
        }

        private void upgradeTo87(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ExternalDBEnabled");
            colTypes.Add(conn.getBooleanType("ExternalDBEnabled"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo88(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Name");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Description");
            colTypes.Add(conn.getNvarcharType(-1, true));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACES_CONFIG_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo89(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("GroupName");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.SPACE_GROUP_TABLE, colNames, colTypes, actions);

            colNames = new List<string>();
            colTypes = new List<string>();
            actions = new List<int>();
            colNames.Add("Username");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo90(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DefaultSpace");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);          
        }


        private void upgradeTo91(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DBTimeZone");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACES_CONFIG_TABLE, colNames, colTypes, actions);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo92(QueryConnection mainConn, string mainSchema)
        {
            Database.dropTable(mainConn, mainSchema, Database.LOCALE_TABLE);

        }

        private void upgradeTo93(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Name");
            colTypes.Add(conn.getNvarcharType(255, false));
            actions.Add(Database.ALTER_ADD);
            colNames.Add("Comments");
            colTypes.Add(conn.getNvarcharType(-1, true));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACES_CONFIG_TABLE, colNames, colTypes, actions);
        }
        private void upgradeTo94(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();

            colNames.Add("TIMEOUT");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);

            colNames.Add("LOGOUTPAGE");
            colTypes.Add(conn.getVarcharType(-1, true));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SSO_TOKENS_TABLE, colNames, colTypes, actions); 
        }

        private void upgradeTo95(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("SessionVariables");
            colTypes.Add(conn.getNvarcharType(-1, true));
            actions.Add(Database.ALTER_COLUMN);
            Database.alterTable(conn, mainSchema, Database.SSO_TOKENS_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo96(QueryConnection conn, string mainSchema)
        {
            Database.dropTable(mainConn, mainSchema, Database.LOCALE_TABLE);
        }

        private void upgradeTo97(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("Comments");
            colTypes.Add(conn.getVarcharType(-1, true));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.GLOBAL_PROPERTIES_TABLE, colNames, colTypes, actions);
        }
        private void upgradeTo98(QueryConnection mainConn, string mainSchema)
        {
            Database.dropTable(mainConn, mainSchema, Database.LOCALE_TABLE);
        }
        
        private void upgradeTo99(QueryConnection mainConn, string mainSchema)
        {
            Database.dropTable(mainConn, mainSchema, Database.LOCALE_TABLE);
        }

        private void upgradeTo100(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("DenyAddSpace");
            colTypes.Add(conn.getBooleanType("DenyAddSpace"));
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.USER_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo101(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("CreatedDate");
            colTypes.Add(conn.getDateTimeType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SPACE_TABLE_NAME, colNames, colTypes, actions);
        }

        private void upgradeTo102(QueryConnection conn, string mainSchema)
        {
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("RegionID");
            colTypes.Add("INT");
            actions.Add(Database.ALTER_ADD);            
            Database.alterTable(conn, mainSchema, Database.SPACES_CONFIG_TABLE, colNames, colTypes, actions);
        }

        private void upgradeTo103(QueryConnection conn, string mainSchema)
        {   
            List<string> colNames = new List<string>();
            List<string> colTypes = new List<string>();
            List<int> actions = new List<int>();
            colNames.Add("ACCOUNTID");
            colTypes.Add(conn.getUniqueIdentiferType());
            actions.Add(Database.ALTER_ADD);
            Database.alterTable(conn, mainSchema, Database.SAML_IDP_TABLE, colNames, colTypes, actions);
        }
    }
}