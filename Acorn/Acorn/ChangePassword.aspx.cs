﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class ChangePassword : LocalizedPage
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string CurrentPasswordHashVersion = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["CurrentPasswordHashVersion"];

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            /* change on first use fails with this since we redirect XXX
            if (Request.RequestType != "POST")
            {
                Response.Redirect(Util.getHomePageURL(Session));
                return;
            }
             */
            Util.setUpHeaders(Response, false);
            User u = Util.validateAuth(Session, Response);
            if (Session["ssoAuthentication"] != null) // SSO should not be allowed to see the accounts page
            {
                Response.Redirect(Util.getHomePageURL(Session, Request));
                return;
            }
            if (Session["PasswordChangeRequired"] != null)
            {
                ChangePasswordLabel.Visible = false;
                RequiredChangePasswordLabel.Visible = true;
            }
            else
            {
                ChangePasswordLabel.Visible = true;
                RequiredChangePasswordLabel.Visible = false;
            }
            Util.setUpdatedHeader(Session, null, u, Master);
        }

        protected void ChangePasswordButton_Click(object sender, ImageClickEventArgs args)
        {
            if (Request.RequestType != "POST")
            {
                Response.Redirect(Util.getHomePageURL(Session, Request));
                return;
            }
            if (Page.IsValid)
            {
                User user = (User)Session["user"];
                if (user == null)
                    return;

                // change password and show changed password label
                MembershipUser mu = Membership.GetUser(user.Username, false);
                string tempPassword = mu.ResetPassword();
                mu.ChangePassword(tempPassword, NewPassword.Text);
                PasswordPolicy policy = null;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    policy = Database.getPasswordPolicy(conn, WebServiceSessionHelper.mainSchema, user.ManagedAccountId);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                user.Password = Password.HashPassword(NewPassword.Text, user.Username, (policy == null ? null : policy.hashAlgorithm)); // update the session version so changing the password again in this session will work
                MessagesPanel.Visible = true;
                SuccessLabel.Visible = true;
                FailLabel.Visible = false;
                Session.Remove("PasswordChangeRequired");
            }
            else
            {
                // show failed label
                MessagesPanel.Visible = true;
                FailLabel.Visible = true;
                SuccessLabel.Visible = false;
            }
        }

        protected void PasswordValidator_Validate(object source, ServerValidateEventArgs args)
        {
            User user = (User)Session["user"];

            args.IsValid = false;
            if (user == null)
                return;

            CustomValidator validator = (CustomValidator)source;

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                PasswordPolicy policy = Database.getPasswordPolicy(conn, mainSchema, user.ManagedAccountId);
                if (policy != null)
                {
                    validator.ErrorMessage = policy.description + "\nCommon passwords, repeated characters, and sequences (lexigraphic, numeric, and keyboard) are not allowed";
                    validator.ToolTip = policy.description + "\nCommon passwords, repeated characters, and sequences (lexigraphic, numeric, and keyboard) are not allowed";
                    List<string> history = Database.getPasswordHistory(conn, mainSchema, user.ID);
                    args.IsValid = policy.isValid(args.Value, user.Username, history);
                }
                else
                    args.IsValid = true; // no password rules
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void OldPasswordValidator_Validate(object source, ServerValidateEventArgs args)
        {
            User user = (User)Session["user"];

            args.IsValid = false;
            if (user == null)
                return;

            CustomValidator validator = (CustomValidator)source;
            args.IsValid = Password.VerifyPassword(args.Value, user.Password, user.Username);
            if (!args.IsValid)
                validator.ErrorMessage = "Incorrect password";
        }
    }
}
