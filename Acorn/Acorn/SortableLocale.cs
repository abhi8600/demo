﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class SortableLocale : CultureInfo, IComparable
    {
        public SortableLocale(int culture)
            : base(culture)
        {
        }
        public SortableLocale(string name)
            : base(name)
        {
        }
        public SortableLocale(int culture, Boolean useUserOverride)
            : base(culture, useUserOverride)
        {
        }
        public SortableLocale(string name, Boolean useUserOverride)
            : base(name, useUserOverride)
        {
        }
        public int CompareTo(object obj)
        {
            CultureInfo x = (CultureInfo)this;
            CultureInfo y = (CultureInfo)obj;
            return x.DisplayName.CompareTo(y.DisplayName);
        }
    }
}
