﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Collections.Generic;
using Acorn.DBConnection;

namespace Acorn
{
    public class SnapshotPolicyManager
    {
        public static string TABLENAME = "TXN_SNAPSHOT_POLICY";
        private Space sp;
        private MainAdminForm maf;
        private QueryConnection conn;
        private string mainschema;

        public SnapshotPolicyManager(QueryConnection conn, string mainschema, Space sp, MainAdminForm maf)
        {
            this.conn = conn;
            this.mainschema = mainschema;
            this.sp = sp;
            this.maf = maf;
        }

        public void setupPolicyTable(int loadNumber)
        {
            bool hasPolicy = false;
            List<StagingTable> stSnapshots = new List<StagingTable>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.Snapshots != null)
                {
                    hasPolicy = true;
                    stSnapshots.Add(st);
                }
            }
            if (!hasPolicy)
                return;
            Global.systemLog.Info("Setting up the Snapshot Policy for load " + loadNumber);
            List<Publish> plist = Database.getPublishDetail(conn, mainschema, sp.ID);
            QueryConnection sconn = null;
            try
            {
                bool isIB = Database.isDBTypeIB(sp.ConnectString);
                sconn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "Policy", "VARCHAR(255)" });
                columns.Add(new string[] { "LOADID", "INTEGER" });
                if (isIB)
                    Database.createTableOnInfobright(sconn, sp.Schema, TABLENAME, columns, true);
                else
                    Database.createTable(sconn, sp.Schema, TABLENAME, columns, true);
                foreach (StagingTable st in stSnapshots)
                {
                    Publish curPublish = new Publish();
                    curPublish.StartPublishDate = DateTime.Now;
                    curPublish.LoadNumber = loadNumber;
                    curPublish.TableName = st.Name;
                    plist.Add(curPublish);
                }
                foreach (StagingTable st in stSnapshots)
                {
                    foreach (Publish p in plist)
                    {
                        if (p.TableName != st.Name)
                            continue;
                        if (matchesPolicy(p, st.Snapshots))
                        {
                            // Take the first load for each day only (so you don't retain multiple snapshots)
                            bool earliestInDay = true;
                            foreach (Publish p2 in plist)
                            {
                                if (p2 == p)
                                    continue;
                                if (p2.TableName != st.Name)
                                    continue;
                                if (p.StartPublishDate.Year == p2.StartPublishDate.Year && p.StartPublishDate.Month == p2.StartPublishDate.Month && p.StartPublishDate.Day == p2.StartPublishDate.Day &&
                                    p.StartPublishDate > p2.StartPublishDate)
                                {
                                    earliestInDay = false;
                                    break;
                                }
                            }
                            if (earliestInDay)
                            {
                                Database.addPolicyLoadID(sconn, sp, TABLENAME, st.Name, p.LoadNumber);
                            }
                        }
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(sconn);
            }
        }

        private bool matchesPolicy(Publish p, SnapshotPolicy spol)
        {
            // Current Day used for daily. If its daily, see if the publish day matches with today
            //dont only rely on type... type is set to loweset of daily, weekly , monthly
            if (spol.CurrentDay && DateTime.Now.Subtract(p.StartPublishDate).Days == 0)
                return true;
            if (spol.DaysOfWeek > 0) //weekly is set?
            {
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_SUNDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Sunday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_MONDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Monday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_TUESDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Tuesday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_WEDNESDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Wednesday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_THURSDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Thursday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_FRIDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Friday)
                    return true;
                if (((spol.DaysOfWeek & SnapshotPolicy.DAY_OF_WEEK_SATURDAY) > 0) && p.StartPublishDate.DayOfWeek == DayOfWeek.Saturday)
                    return true;
            }
            // Possibly monthly
            if (spol.DayOfMonth > 0 || spol.firstDayOfMonth || spol.lastDayOfMonth) //monthly is set?
            {
                if (p.StartPublishDate.Day == spol.DayOfMonth)
                {
                    return true;
                }

                if (spol.firstDayOfMonth && p.StartPublishDate.Day == 1)
                {
                    return true;
                }

                if (spol.lastDayOfMonth)
                {
                    // Calculate the last day of the month and see if the supplied publish date matches
                    DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime lastDay = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    if (p.StartPublishDate.Day == lastDay.Day)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
