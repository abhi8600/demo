//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "10.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserSettingsLabels {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserSettingsLabels() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.UserSettingsLabels", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        internal static string AddLabel {
            get {
                return ResourceManager.GetString("AddLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Password.
        /// </summary>
        internal static string ChangePasswordLabel {
            get {
                return ResourceManager.GetString("ChangePasswordLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Default Space:.
        /// </summary>
        internal static string DefaultSpaceLabel {
            get {
                return ResourceManager.GetString("DefaultSpaceLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Default to Dashboards:.
        /// </summary>
        internal static string DefaultToDashboardsLabel {
            get {
                return ResourceManager.GetString("DefaultToDashboardsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string DeleteLabel {
            get {
                return ResourceManager.GetString("DeleteLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Settings.
        /// </summary>
        internal static string EditSettingsLabel {
            get {
                return ResourceManager.GetString("EditSettingsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email:.
        /// </summary>
        internal static string EmailLabel {
            get {
                return ResourceManager.GetString("EmailLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email is required..
        /// </summary>
        internal static string EmailRequiredLabel {
            get {
                return ResourceManager.GetString("EmailRequiredLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Features.
        /// </summary>
        internal static string FeaturesLabel {
            get {
                return ResourceManager.GetString("FeaturesLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid Email address.
        /// </summary>
        internal static string InvalidEmailLabel {
            get {
                return ResourceManager.GetString("InvalidEmailLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid OpenID URL.
        /// </summary>
        internal static string InvalidOpenIDLabel {
            get {
                return ResourceManager.GetString("InvalidOpenIDLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Language:.
        /// </summary>
        internal static string LanguageLabel {
            get {
                return ResourceManager.GetString("LanguageLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modify Look and Feel.
        /// </summary>
        internal static string ModifyLookAndFeelLabel {
            get {
                return ResourceManager.GetString("ModifyLookAndFeelLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OpenID already in use.
        /// </summary>
        internal static string OpenIDAlreadyUseLabel {
            get {
                return ResourceManager.GetString("OpenIDAlreadyUseLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OpenID Associations (see.
        /// </summary>
        internal static string OpenIDLabel {
            get {
                return ResourceManager.GetString("OpenIDLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter a valid OpenID URL.
        /// </summary>
        internal static string OpenIDUrlValidLabel {
            get {
                return ResourceManager.GetString("OpenIDUrlValidLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter a valid OpenID URL.
        /// </summary>
        internal static string OpenIDValidUrlLabel {
            get {
                return ResourceManager.GetString("OpenIDValidUrlLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        internal static string OtherLabel {
            get {
                return ResourceManager.GetString("OtherLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Products:.
        /// </summary>
        internal static string ProductsLabel {
            get {
                return ResourceManager.GetString("ProductsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Changes.
        /// </summary>
        internal static string SaveChangesLabel {
            get {
                return ResourceManager.GetString("SaveChangesLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        internal static string SettingsLabel {
            get {
                return ResourceManager.GetString("SettingsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Name (login).
        /// </summary>
        internal static string UserNameLabel {
            get {
                return ResourceManager.GetString("UserNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Settings.
        /// </summary>
        internal static string UserSettingsLabel {
            get {
                return ResourceManager.GetString("UserSettingsLabel", resourceCulture);
            }
        }
    }
}
