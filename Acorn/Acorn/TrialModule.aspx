﻿<%@ Page Title="Birst" Language="C#" AutoEventWireup="True" CodeBehind="TrialModule.aspx.cs"
    Inherits="Acorn.TrialModule" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="./web-files/dtjava.js"></script>
    <script>
    function javafxEmbed() {
        dtjava.embed(
            {
                url : 'TrialMode.jnlp',
                placeholder : 'javafx-app-placeholder',
                width : 1024,
                height : 600,
                jnlp_content : 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxqbmxwIHNwZWM9IjEuMCIgeG1sbnM6amZ4PSJodHRwOi8vamF2YWZ4LmNvbSIgaHJlZj0iVHJpYWxNb2RlLmpubHAiPg0KICA8aW5mb3JtYXRpb24+DQogICAgPHRpdGxlPlRyaWFsTW9kZTwvdGl0bGU+DQogICAgPHZlbmRvcj5icGV0ZXJzPC92ZW5kb3I+DQogICAgPGRlc2NyaXB0aW9uPlNhbXBsZSBKYXZhRlggMi4wIGFwcGxpY2F0aW9uLjwvZGVzY3JpcHRpb24+DQogICAgPG9mZmxpbmUtYWxsb3dlZC8+DQogIDwvaW5mb3JtYXRpb24+DQogIDxzZWN1cml0eT4NCiAgICA8YWxsLXBlcm1pc3Npb25zLz4NCiAgPC9zZWN1cml0eT4NCiAgPHJlc291cmNlcyBvcz0iV2luZG93cyI+DQogICAgPGpmeDpqYXZhZngtcnVudGltZSB2ZXJzaW9uPSIyLjArIiBocmVmPSJodHRwOi8vamF2YWRsLnN1bi5jb20vd2ViYXBwcy9kb3dubG9hZC9HZXRGaWxlL2phdmFmeC1sYXRlc3Qvd2luZG93cy1pNTg2L2phdmFmeDIuam5scCIvPg0KICA8L3Jlc291cmNlcz4NCiAgPHJlc291cmNlcz4NCiAgICA8ajJzZSB2ZXJzaW9uPSIxLjciLz4NCiAgICA8amFyIGhyZWY9IkRhdGFDb25kdWN0b3IuamFyIiBkb3dubG9hZD0iZWFnZXIiIC8+DQogIDwvcmVzb3VyY2VzPg0KICA8YXBwbGV0LWRlc2MgIHdpZHRoPSIxMDI0IiBoZWlnaHQ9IjYwMCIgbWFpbi1jbGFzcz0iY29tLmphdmFmeC5tYWluLk5vSmF2YUZYRmFsbGJhY2siICBuYW1lPSJUcmlhbE1vZGUiIC8+DQogIDxqZng6amF2YWZ4LWRlc2MgIHdpZHRoPSIxMDI0IiBoZWlnaHQ9IjYwMCIgbWFpbi1jbGFzcz0iY29tLmJpcnN0LnRyaWFsbW9kZS5UcmlhbE1vZGUiICBuYW1lPSJUcmlhbE1vZGUiIC8+DQogIDx1cGRhdGUgY2hlY2s9ImJhY2tncm91bmQiLz4NCjwvam5scD4NCg=='
            },
            {
                javafx : '2.0+'
            },
            {}
        );
    }

    function movedown() {
    var obj = document.getElementById('overlay');
    obj.style.top = 600;
    }
    function moveup() {
    var obj = document.getElementById('overlay');
    obj.style.top = 100;
    }
    function settab(tabname) {
    var ifrm = document.getElementById('embedframe');
    var doc = ifrm.contentDocument? ifrm.contentDocument: ifrm.contentWindow.document;
    var obj = doc.getElementById('Navigation');
    obj.settab(tabname);
    }

    dtjava.addOnloadCallback(javafxEmbed);

    </script>
        <style type="text/css">
        html, body { 
            height: 100%;
            width:100%;
            margin: 0px; 
            overflow:hidden;
            zoom: 1;
        }
        
        #mainForm {
            height:100%;
        }
        
        #headerTable {
            height: 44px; 
            width: 100%; 
            padding: 0 5px 0 5px;
            font-size: 10pt; 
            font-weight: normal; 
            text-align: right;
        }
        
        #headerTable A 
        {
            color: <%= this.fgcolor %>;            
            text-decoration: none;
        }
        
        .headerTd {
            width: 20%; 
            color: <%= this.fgcolor %>;
            font-size: 10pt; 
            text-align: left; 
            padding: 0 5px 0 5px;
        }
        
        .linkTd {
            color: <%= this.fgcolor %>; 
            font-size: 10pt;
            padding: 0 5px 0 5px; 
        }

        .centerTd 
        {
            color: <%= this.fgcolor %>;
            font-size: 10pt;
            text-align: center;
            padding: 0 5px 0 5px; 
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id='javafx-app-placeholder' style='background-color: #000000'>
    </div>
    <div id='overlay' style='top: 100px; position: absolute; width: 100%; height:900px'>
        <iframe id="embedframe" src="http://localhost:57747/FlexModule.aspx?birst.module=admin&debug=true&embedded=true&noheader=true#"
            style="width: 100%; height: 800px"></iframe>
    </div>
    </form>
</body>
</html>
