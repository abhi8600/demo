﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using Acorn.Background;
using Acorn.DBConnection;
using Acorn.Utils;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.Text;

namespace Acorn
{
    public class ProcessHandler
    {

        public const string INFO_LOAD_ID_KEY = "LN";
        public const string INFO_LOAD_GROUP_KEY = "LG";
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string defaultErrorMessage = "Unable to stop processing";
        public static string killRunningProcess(string spaceID, string username, bool findServer, bool processOnThisServer) //returning error message if any
        {
            Global.systemLog.Debug("Beginning stop running process for space: " + spaceID + " by user: " + username);
            try
            {
                string response = stopLocalLoad(spaceID, username);
                if (response != null)
                {
                    Global.systemLog.Debug("Exiting from killRunningProcess for space: " + spaceID + ". Response from birst connect: " + response);
                    if (response.Equals("OK") || response.Equals("EMPTY"))
                        return null;
                    return response;
                }

                Space sp = Util.getSpaceById(new Guid(spaceID));
                string serverURL = Database.getProcessingServerURL(mainSchema, new Guid(spaceID));
                if (serverURL == null)
                {

                    Global.systemLog.Info("Not able to find processing server information from database for space. Cleaning up status entries for space : " + spaceID);
                    checkAndcleanUpStatusEntries(sp);
                    cleanUpAfterStopProcessing(sp);
                    return null;
                }

                if (findServer)
                {
                    processOnThisServer = serverURL == (Util.getServerIP() + ":" + Util.getServerPort());
                }

                if (!processOnThisServer)
                {
                    string ipStr = Util.getServerIP();
                    Acorn.InterServerService.InterServerWebService iss = new Acorn.InterServerService.InterServerWebService();
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    iss.Timeout = 5 * 60 * 1000;
                    Global.systemLog.Debug("Kill running process request for space " + spaceID + " redirected from " + ipStr + " server to " + (localprotocol + serverURL));
                    iss.Url = localprotocol + serverURL + "/InterServerWebService.asmx";
                    return iss.KillRunningProcess(spaceID, username);
                }
                else
                {
                    bool isKilled = killProcess(sp);
                    Global.systemLog.Info("Exiting from killRunningProcess for space: " + spaceID + " with status:" + isKilled);
                    if (isKilled)
                        return null;
                    else
                        return defaultErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while killRunningProcess for space: " + spaceID, ex);
                return defaultErrorMessage;
            }
        }

        private static void updateSchedulerStatus(Space sp)
        {
            try
            {
                SchedulerUtils.markProcessJobInSchedulerKilled(sp);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while marking the process entry in scheduler as killed", ex);
            }
        }

        private static List<int> getChildrenPids(string pidString)
        {
            if (pidString != null && pidString.Length > 0)
            {
                string[] pidList = pidString.Split(';'); // parent;child1;child2                                
                if (pidList != null && pidList.Length > 1)
                {
                    List<int> response = new List<int>();
                    for (int i = 1; i < pidList.Length; i++)
                    {
                        int childPidNumber = -1;
                        if (int.TryParse(pidList[i], out childPidNumber))
                        {
                            response.Add(childPidNumber);
                        }
                        else
                        {
                            Global.systemLog.Warn("Unable to parse child process pid " + pidList[i]);
                        }
                    }
                    return response;
                }
            }
            return null;
        }

        public static bool killProcess(Space sp)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<RunningProcessInfo> runningProcesses = Database.getRunningProcessInfo(conn, mainSchema, Util.getServerIP(), sp.ID);
                if (runningProcesses != null && runningProcesses.Count > 0)
                {
                    foreach (RunningProcessInfo runningProcessInfo in runningProcesses)
                    {
                        if (runningProcessInfo.pids != null && runningProcessInfo.pids.Trim().Length > 0)
                        {
                            // comma separated values
                            string[] pids = runningProcessInfo.pids.Split(',');
                            foreach (string pid in pids)
                            {
                                string[] pidList = pid.Split(';'); // parent;child1;child2                                
                                List<int> childrenPids = getChildrenPids(pid);
                                if (pidList != null && pidList.Length > 0)
                                {
                                    int parentPidNumber = -1;
                                    if (int.TryParse(pidList[0], out parentPidNumber))
                                    {
                                        if (isValidLoadParentProcessPid(sp, parentPidNumber))
                                        {
                                            KillProcessAndChildren(parentPidNumber);
                                            if (isValidLoadParentProcessPid(sp, parentPidNumber))
                                            {
                                                Global.systemLog.Error("Unable to kill parent load process " + parentPidNumber);
                                                return false;
                                            }
                                            else if (childrenPids != null && childrenPids.Count > 0)
                                            {
                                                // check if all the children have exited
                                                foreach (int childPidNumber in childrenPids)
                                                {
                                                    if (isValidLoadChildProcessPid(sp, childPidNumber))
                                                    {
                                                        Global.systemLog.Error("Unable to kill child load process " + childPidNumber);
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Global.systemLog.Info("Parent process not running :  " + parentPidNumber + " .Going over children pids : space : " + sp.ID);
                                            // if parent pid is not found, go over children and kill them individually

                                            if (childrenPids != null && childrenPids.Count > 0)
                                            {
                                                foreach (int childPidNumber in childrenPids)
                                                {
                                                    if (isValidLoadChildProcessPid(sp, childPidNumber))
                                                    {
                                                        KillProcessAndChildren(childPidNumber);
                                                        if (isValidLoadChildProcessPid(sp, childPidNumber))
                                                        {
                                                            Global.systemLog.Error("Unable to kill child load process " + childPidNumber);
                                                            return false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Global.systemLog.Info("Child process not running :  " + childPidNumber + " : space : " + sp.ID);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Global.systemLog.Warn("Unable to parse parent pid number " + pidList);
                                    }
                                }
                            }
                        }
                        cleanUpStatusEntries(sp, runningProcessInfo);
                    }
                }
                else
                {
                    Global.systemLog.Warn("No running process found in db for the server. Clearing out lock files and running info db entry");
                }
                cleanUpAfterStopProcessing(sp);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in finding pids from db for space " + sp.ID, ex);
                return false;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return true;
        }

        private static void cleanUpAfterStopProcessing(Space sp)
        {
            cleanUpLockFiles(sp);
            cleanUpRunningInfo(sp.ID, Util.RUNNING_TYPE_LOAD);
            clearFromGlobalMap(sp);
            updateSchedulerStatus(sp);
        }


        private static void cleanUpLockFiles(Space sp)
        {
            try
            {
                if (File.Exists(sp.Directory + "\\publish.lock"))
                    File.Delete(sp.Directory + "\\publish.lock");
                Util.deleteLoadLockFile(sp);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during MonitorLoad.kill", ex);
            }
        }

        private static void clearFromGlobalMap(Space sp)
        {
            ApplicationLoader.clearBackgroundModule(sp.ID.ToString());
        }

        private static void cleanUpRunningInfo(Guid spaceID, string type)
        {
            Database.removeProcessingServerForSpace(spaceID, type);
        }

        private static void cleanUpStatusEntries(Space sp, RunningProcessInfo runningProcessInfo)
        {
            if (runningProcessInfo != null && runningProcessInfo.info != null && runningProcessInfo.info.Trim().Length > 0)
            {
                string info = runningProcessInfo.info;
                string[] details = info.Split(',');
                int loadNumber = -1;
                string loadGroup = null;
                foreach (string detail in details)
                {
                    if (detail.StartsWith(INFO_LOAD_ID_KEY))
                    {
                        int.TryParse(detail.Substring(INFO_LOAD_ID_KEY.Length + 1), out loadNumber);
                    }

                    if (detail.StartsWith(INFO_LOAD_GROUP_KEY))
                    {
                        loadGroup = detail.Substring(INFO_LOAD_GROUP_KEY.Length + 1);
                    }
                }

                if (loadNumber > 0 && loadGroup != null)
                {
                    Status.setLoadFailed(sp, loadNumber, loadGroup, true);
                }
            }
        }

        private static void checkAndcleanUpStatusEntries(Space sp)
        {
            // acorn load group
            string[] loadGroups = new string[] { Util.LOAD_GROUP_NAME, SalesforceLoader.LOAD_GROUP_NAME };
            foreach (string loadGroup in loadGroups)
            {
                int loadNumber = ProcessLoad.getLoadNumber(sp, loadGroup) + 1;
                Status.StatusResult sr = Status.getLoadStatus(null, sp, loadNumber, loadGroup);
                if (sr != null && Status.isRunningCode(sr.code) && sr.code != Status.StatusCode.LoadingAnotherLoadGroup)
                {
                    Global.systemLog.Warn("Marking the load as failed for space : " + sp.ID + " : name " + sp.Name + " : loadNumber " + loadNumber +
                        " : loadGroup : " + loadGroup);
                    Status.setLoadFailed(sp, loadNumber, loadGroup, true);
                }
            }
        }

        /// <summary>
        /// returns the parent process along with children process ids delimited by semi-colon
        /// A;c1;c2 - first entry is parent pid and rest are children process pids
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="waitForChildToKickoff">Retry if no children has been found</param>
        /// <returns></returns>
        public static string getPidsString(string pid, bool waitForChildToKickOff)
        {
            StringBuilder sb = new StringBuilder(pid);
            int retryCount = 3;
            do
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
                ManagementObjectCollection moc = searcher.Get();
                if (moc != null && moc.Count > 0)
                {
                    retryCount = 0;
                    foreach (ManagementObject mo in moc)
                    {
                        sb.Append(";");
                        sb.Append(Convert.ToInt32(mo["ProcessID"]));
                    }
                }
                else
                {
                    // sleep and set the counter
                    retryCount--;
                    System.Threading.Thread.Sleep(2000);
                }
            } while (retryCount > 0);

            return sb.ToString();
        }

        private static bool isValidLoadParentProcessPid(Space sp, int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                string commandLine = (string)mo["CommandLine"];
                if (commandLine != null && commandLine.IndexOf(sp.Directory) >= 0 &&
                    commandLine.IndexOf("load_warehouse.bat") >= 0 && commandLine.IndexOf("load.cmd") >= 0)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool isValidLoadChildProcessPid(Space sp, int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                string commandLine = (string)mo["CommandLine"];
                if (commandLine != null && commandLine.IndexOf(sp.Directory) >= 0 &&
                    commandLine.IndexOf("Main") >= 0 && commandLine.IndexOf("load.cmd") >= 0)
                {
                    return true;
                }
            }
            return false;
        }


        public static string stopLocalLoad(string spaceID, string username)
        {
            QueryConnection conn = ConnectionPool.getConnection();
            User u = Database.getUser(conn, mainSchema, username);
            if (u == null)
                return null;
            Space sp = Util.getSpaceById(conn, mainSchema, u, new Guid(spaceID));
            Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            if (localETLConfigs == null || localETLConfigs.Count == 0)
                return null;
            int iteration = sp.LoadNumber + 1;
            string response = "";
            foreach (string lg in localETLConfigs.Keys)
            {
                LocalETLConfig leconfig = localETLConfigs[lg];
                Status.StatusResult sr = Status.getLoadStatus(null, sp, iteration, Util.LOAD_GROUP_NAME);
                if (sr != null && sr.code == Status.StatusCode.LoadingAnotherLoadGroup)
                {
                    response = LiveAccessUtil.markLoadAsFailed(sp, leconfig.getLocalETLConnection(), "markloadasfailed\r\n" + iteration);
                    Global.systemLog.Debug("MarkLoadAsFailed: Connection - " + leconfig.getLocalETLConnection() + ", response - " + response);
                    if (response == null)
                    {
                        response = "Please restart birst connect and try again to stop running data processing";
                        break;
                    }
                    else if (!response.Equals("OK"))
                    {
                        break;
                    }
                }
            }
            return response;
        }

        public static void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                PropertyDataCollection collection = mo.Properties;
                foreach (PropertyData prop in collection)
                {
                    Global.systemLog.Info(prop.Name + "=" + prop.Value);
                }
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                if (!proc.HasExited)
                {
                    proc.Kill();
                    proc.WaitForExit();
                    proc.Close();
                }
            }
            catch (ArgumentException)
            { /* process already exited */ }
        }
    }

    public class RunningProcessInfo
    {
        public Guid spaceID;
        public string server;
        public string port;
        public string type;
        public string source;
        public string pids;
        public string info;
    }
}
