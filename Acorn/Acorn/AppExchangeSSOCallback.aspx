﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<html>
<head><title></title></head>
<body>

<script type="text/javascript">

    if (document.location.href.indexOf("access_token") > -1) {
        // User has clicked Allow button
        try {
            window.opener.Sfdc.canvas.oauth.childWindowUnloadNotification(self.location.hash);
        } catch (ignore) { }
    }
    else if (document.location.href.indexOf("access_denied") > -1) {
        // User has clicked Deny button
        window.opener.location.href = "<%=HttpContext.Current.Request.Url.Scheme%>://<%=HttpContext.Current.Request.Url.Authority%>/AppExchangeSSO.aspx?access_denied=true";
    }

    self.close();

</script>

</body>
</html>