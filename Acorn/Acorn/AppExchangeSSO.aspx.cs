﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Principal;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Collections.Specialized;
using Acorn.sforce;
using Acorn.Utils;
using Acorn.DBConnection;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Net;

namespace Acorn
{
    public partial class AppExchangeSSO : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string secretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCBirstAppSecretKey"]; // secret key for our app

        // return a collection based upon the contents of the JSON dictionary, starting at 'root' (e.g., context.environment.parameters.birst)
        private NameValueCollection getCollectionFromParameters(Dictionary<string, object> dict, string root)
        {
            NameValueCollection newCol = new NameValueCollection();
            if (dict != null)
            {
                string[] keys = root.Split(new char[] { '.' });
                foreach (string key in keys)
                {
                    if (!dict.ContainsKey(key))
                        return newCol;  // exit early, did not find the key

                    object val = dict[key];
                    if (val is Dictionary<string, object>)
                    {
                        dict = (Dictionary<string, object>)val;
                    }
                    else
                    {
                        return newCol; // exit early, did not find the full path (no root parameters)
                    }
                }

                // dict should now point to the right place, just iterate over dict
                foreach (string key in dict.Keys)
                {
                    object val = dict[key];
                    if (val != null && !(val is Dictionary<string, object>))
                    {
                        newCol.Add("birst." + key, val.ToString());
                        Global.systemLog.Debug("AppExchangeSSO: birst." + key + " = " + val.ToString());
                    }
                }
            }
            return newCol;
        }

        // dive through the JSON dictionary structure to find an item with the key 'attr' (e.g., context.user.username)
        private object getValue(Dictionary<string, object> dict, string attr)
        {
            if (dict != null)
            {
                string[] keys = attr.Split(new char[] { '.' });
                object val = null;
                foreach (string key in keys)
                {
                    if (dict.ContainsKey(key))
                    {
                        val = dict[key];
                        if (val is Dictionary<string, object>)
                        {
                            dict = (Dictionary<string, object>)val; // dive down to the next level
                        }
                        else
                        {
                            return val; // hit a leaf (should really only be if this is the last key)
                        }
                    }
                    else
                    {
                        return null; // key not found at the correct level
                    }
                }
            }
            return null; // dictionary is null, should never happen
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.validateSpaceId(Request, Response, Session);
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            QueryConnection conn = null;

            try
            {
                string username = null;
                string spaceName = null;
                string id = null;
                string sessionVarsParam = null;
                NameValueCollection newCol = null; // query string or context parameters

                // from salesforce.com Custom Tab or Visual Force apex:iframe
                string sessionId = Request.QueryString["sessionid"];
                string serverURL = Request.QueryString["serverurl"];

                // from force.com canvas signed request
                string signedrequest = Request.Form["signed_request"];

                // from canvas oauth (javascript) flow
                string uname = Request.QueryString["userName"];
                string access_denied = Request.QueryString["access_denied"];

                // If no auth flow is in progress and we no url params passed in the return blank page
                if (uname == null && sessionId == null && signedrequest == null && access_denied == null)
                {
                    return;
                }
                
               if (uname != null)
                {
                    // If user has clicked oauth Approve button
                    username = uname;
                    id = Request.QueryString["spaceId"];

                    if (Request.QueryString["useSFDCEmailForUsername"] == "false")
                        username = uname;
                    else
                        username = Request.QueryString["email"];

                    newCol = new NameValueCollection(Request.QueryString);
                }
                else if (access_denied != null) 
                {
                    // If user has clicked oauth Deny button
                    Response.Write("Access to Birst has not been approved.");
                    return;
                }
                else if (sessionId == null && signedrequest != null) // will always be POST, whereas custom tab will always be GET...
                {
                    //
                    // support Force.com Canvas Connected Applications
                    //
                    // how it gets inserted into the HTML page
                    //
                    // <div class="canvas-iframe" id="canvas-iframe">
                    //   <form name="canvas-hidden-form" style="display:none" action="https://sfo0-qa-04.birst.com:9443/AppExchangeSSO.aspx" target="canvas-frame-06Pi00000008OWv" method="post">
                    //     <input type="text" name="signed_request" id="signed_request">
                    //     <input type="submit" post="post">
                    //   </form>
                    //   <iframe id="if-06Pi00000008OWv" name="canvas-frame-06Pi00000008OWv" style="border: 0px; width: 800px; height: 900px;">
                    //   </iframe>
                    // </div>
                    // 
                    // https://www.salesforce.com/us/developer/docs/platform_connectpre/canvas_framework.pdf
                    // https://na1.salesforce.com/help/doc/en/salesforce_spring13_release_notes.pdf
                    //
                    // birst specific parameters are passed via 'parameters.birst.*'
                    //
                    // <apex:page docType="html-5.0" showHeader="false" cache="false">
                    //   <apex:canvasApp applicationName="BirstDemo"
                    //                   namespacePrefix="birstdemo" height="800px" width="1280px"
                    //                   parameters="{birst: {spaceId:'bb60320c-5e30-425f-ba25-2ec5479be3e4',useSFDCEmailForUsername:'true',module:'dashboard',
                    //                                        renderType:'HTML',sessionVars:'fred=1,bob=2'}}"/>
                    // </apex:page>
                    //
                    // processing the signed request (per canvas framework documentation)
                    //
                    string[] split = signedrequest.Split(new char[] { '.' });
                    if (split.Length != 2)
                    {
                        Global.systemLog.Warn("AppExchangeSSO: not a valid signed request - missing '.'");
                        Response.Write("Not a valid Force.com Connected App request");
                        return;
                    }
                    string encodedSig = split[0];
                    string encodedEnvelope = split[1];
                    // Global.systemLog.Debug("AppExchangeSSO: encodedSig: " + encodedSig);
                    // Global.systemLog.Debug("AppExchangeSSO: encodedEnvelope: " + encodedEnvelope);

                    byte[] secretBytes = System.Text.Encoding.UTF8.GetBytes(secretKey);

                    // get the envelope before validation since it contains the algorithm to use
                    string jsonEnvelope = Encoding.UTF8.GetString(Convert.FromBase64String(encodedEnvelope));
                    // Global.systemLog.Debug("AppExchangeSSO: " + jsonEnvelope);

                    // parse the JSON into a more friendly format
                    Dictionary<string, object> dict = (Dictionary<string, object>)new JavaScriptSerializer().DeserializeObject(jsonEnvelope);

                    string algorithm = (string)getValue(dict, "algorithm");

                    if (algorithm != null && algorithm != "HMACSHA256")
                    {
                        // only support HMACSHA256
                        Global.systemLog.Warn("AppExchangeSSO: unexpected algorithm - " + algorithm);
                        Response.Write("Not a valid Force.com Connected App request (bad algorithm specification)");
                        return;
                    }

                    // validate the signed request
                    HMACSHA256 hash = new HMACSHA256(secretBytes);
                    byte[] resultBytes = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(encodedEnvelope));
                    string result = Convert.ToBase64String(resultBytes);
                    // Global.systemLog.Debug("AppExchangeSSO: result: " + result);
                    if (result != encodedSig)
                    {
                        Global.systemLog.Warn("AppExchangeSSO: not a valid signed request - validation failed");
                        Response.Write("Not a valid Force.com Connected App request (validation failed)");
                        return;
                    }

                    // get the birst specific parameters
                    newCol = getCollectionFromParameters(dict, "context.environment.parameters.birst");

                    // get the proper birst username (either sfdc user or email)
                    bool useSFDCEmailForBirstUsername = false;
                    string temp = newCol["birst.useSFDCEmailForUsername"];
                    if (temp != null && temp.Length > 0)
                        bool.TryParse(temp, out useSFDCEmailForBirstUsername);

                    if (useSFDCEmailForBirstUsername)
                        username = (string)getValue(dict, "context.user.email");
                    else
                        username = (string)getValue(dict, "context.user.userName");

                    if (username == null)
                    {
                        Response.Write("Could not determine the username, please contact support");
                        Global.systemLog.Warn("AppExchangeSSO: missing username");
                        return;
                    }

                    id = newCol["birst.spaceId"];
                    if (id == null)
                    {
                        Response.Write("Missing Birst space identifier, please contact support.");
                        Global.systemLog.Warn("AppExchangeSSO: missing Birst space identifier");
                        return;
                    }
                    sessionVarsParam = newCol["birst.sessionVars"];
                }
                else
                {
                    //
                    // support Force.com APEX IFRAME pages and Custom Web Tabs
                    //

                    // make sure there are values for sessionId and serverURL

                    if (sessionId == null || sessionId == "")
                    {
                        Global.systemLog.Warn("AppExchangeSSO: sessionid parameter is missing or blank");
                        Response.Write("The sessionid parameter is missing or blank. The redirect URL is probably incorrect.");
                        return;
                    }
                    if (serverURL == null || serverURL == "")
                    {
                        Global.systemLog.Warn("AppExchangeSSO: serverurl parameter is missing or blank");
                        Response.Write("The serverurl parameter is missing or blank. The redirect URL is probably incorrect.");
                        return;
                    }
                    // validate that the serverURL is a SFDC URL
                    Uri uri = new Uri(serverURL);
                    if (!Regex.IsMatch(uri.AbsoluteUri, Util.ForceComURL) || !(uri.Query == ""))
                    {
                        // protect against spoofing web request
                        Global.systemLog.Warn("AppExchangeSSO: Not a valid Force.com API Server URL: " + serverURL);
                        Response.Write("The serverurl is not a valid Force.com API Server URL: " + HttpUtility.HtmlEncode(serverURL) + ", please contact support.");
                        return;
                    }

                    // binding to the web service
                    SforceService binding = new SforceService();
                    binding.SessionHeaderValue = new SessionHeader();
                    binding.SessionHeaderValue.sessionId = sessionId;
                    serverURL = serverURL.Replace("/c/", "/u/"); // change from enterprise.wsdl endpoint to partner.wsdl endpoint
                    binding.Url = serverURL;
                    String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
                    if (clientID != null && clientID.Length > 0)
                    {
                        CallOptions co = new CallOptions();
                        co.client = clientID;
                        binding.CallOptionsValue = co;
                    }

                    // get the user information
                    GetUserInfoResult userInfoResult = binding.getUserInfo();
                    Global.systemLog.Debug("SessionID: " + sessionId + ", serverURL: " + serverURL);

                    // allow username or email from SFDC to be used as username in Birst
                    string temp = Util.getBirstParam(Request, "birst.useSFDCEmailForBirstUsername", "useSFDCEmailForBirstUsername");
                    bool useSFDCEmailForBirstUsername = false;
                    if (temp != null && temp.Length > 0)
                        bool.TryParse(temp, out useSFDCEmailForBirstUsername);

                    if (useSFDCEmailForBirstUsername)
                        username = userInfoResult.userEmail;
                    else
                    {
                        username = userInfoResult.userName;
                        if (userInfoResult.userEmail != userInfoResult.userName)
                            Global.systemLog.Debug("Issue detected: birst.useSFDCEmailForBirstUsername is false and the username is not the same as the email address (" + userInfoResult.userName + '/' + userInfoResult.userEmail + ')');
                    }

                    spaceName = Util.getBirstParam(Request, "birst.spacename", "BirstSpaceName");
                    id = Util.getBirstParam(Request, "birst.spaceId", "BirstSpaceId");
                    sessionVarsParam = Util.getBirstParam(Request, "birst.sessionVars", "sessionVars");

                    newCol = new NameValueCollection(Request.QueryString);
                }

                Global.systemLog.Debug("AppExchangeSSO: username is " + username);
                Global.systemLog.Debug("AppExchangeSSO: space id is " + id);

                if (spaceName != null)
                    Global.systemLog.Info("AppExchangeSSO: Using birst.spacename or BirstSpaceName in the request, should be using birst.spaceId");

                User u = null;
                conn = ConnectionPool.getConnection();
                u = Database.getUser(conn, mainSchema, username);
                if (u == null)
                {
                    Global.systemLog.Warn("AppExchangeSSO: user (" + username + ") does not exist in the Birst system");
                    Response.Write("The user (" + HttpUtility.HtmlEncode(username) + ") does not exist in the reporting system, please contact support.");
                    return;
                }
                Util.setLogging(u, null);
                // no site means go to the original URL (backwards compatibility)
                string redirectURL = null;
                string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
                if (userRedirectedSite == null || userRedirectedSite.Length == 0)
                {
                    redirectURL = Util.getRequestBaseURL(Request);
                }
                else
                {
                    redirectURL = userRedirectedSite;
                }

                Guid spaceId = Guid.Empty;
                if (id != null && id.Length > 0)
                {
                    try
                    {
                        spaceId = new Guid(id);
                    }
                    catch (Exception)
                    {
                        Response.Write("Corrupt Birst space identifier, please contact support.");
                        Global.systemLog.Warn("AppExchangeSSO: Bad space id - " + id);
                        return;
                    }
                }
                if (spaceId == Guid.Empty)
                {
                    if (spaceName == null || spaceName.Length == 0)
                    {
                        if (u.DefaultSpaceId != Guid.Empty)
                            spaceId = u.DefaultSpaceId;
                        else
                            spaceName = u.DefaultSpace;
                    }
                    if (spaceId == Guid.Empty)
                    {
                        if (spaceName == null || spaceName.Length == 0)
                        {
                            Response.Write("Invalid or missing space identifier, please contact support.");
                            Global.systemLog.Warn("AppExchangeSSO: No space id or space name");
                            return;
                        }
                        Space sp = Util.getSpaceByName(conn, mainSchema, u, spaceName);
                        if (sp == null)
                        {
                            Global.systemLog.Warn("AppExchangeSSO: Security token request failed for user: " + username + " - does not have access to space " + spaceName + " or the space does not exist");
                            Response.Write("The user (" + HttpUtility.HtmlEncode(username) + ") does not have access to the requested space or the space does not exist");
                            return;
                        }
                        spaceId = sp.ID;
                    }
                }

                int spaceOpID = SpaceOpsUtils.getSpaceAvailability(spaceId);
                if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID) && !SpaceOpsUtils.isSpaceBeingCopiedFrom(spaceOpID))
                {
                    Response.Write("Space is currently unavailable due to copy, delete or swap operation. Please try again later.");
                    Global.systemLog.Warn("AppExchangeSSO: The space is currently busy in operations. space id : " + spaceId + " : " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));
                    return;
                }

                bool lockedout = false;
                string token = TokenGenerator.getAppExchangeSecurityToken(conn, mainSchema, u, Request.UserHostAddress, spaceId, sessionVarsParam, ref lockedout);
                if (lockedout)
                {
                    Response.Write("User has been locked after too many unsuccessful Login attempts, please contact support to unlock.");
                    return;
                }
                if (token == null || token.Length == 0)
                {
                    Response.Write("Invalid security token, please contact support.");
                    Global.systemLog.Info("AppExchangeSSO: null security token for " + u.Username);
                    return;
                }
                token = Util.encryptToken(token);

                // AppExchangeSSO should always be embedded
                if (newCol["birst.embedded"] == null)
                    newCol.Add("birst.embedded", "true");
                string redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, newCol);
                Global.systemLog.Debug("AppExchangeSSO: " + redirectUrl);
                Response.Redirect(redirectUrl);
            }
            catch (System.Threading.ThreadAbortException tae)
            {
                Global.systemLog.Debug(tae.Message); // XXX no reason to debug this
            }
            catch (System.Web.Services.Protocols.SoapException exsoap)
            {
                Global.systemLog.Error("AppExchangeSSO: " + exsoap.Message);
                Response.StatusCode = 500;
            }
            catch (System.UriFormatException uriEx)
            {
                Global.systemLog.Error("AppExchangeSSO: the Server URL is invalid: " + uriEx.Message);
                Response.StatusCode = 500;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("AppExchangeSSO: Error: " + ex.Message, ex);
                Response.StatusCode = 500;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
