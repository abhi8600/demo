﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;
using log4net;
using log4net.Config;
using System.Data.Odbc;
using Acorn.WebDav;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Acorn.DBConnection;

namespace Acorn
{
    public class Global : System.Web.HttpApplication
    {
        public static readonly ILog systemLog = LogManager.GetLogger("SystemLog");
        public static readonly ILog userEventLog = LogManager.GetLogger("UserEvents");
        public static string AlertMessage = "";
        public static string Path;
        public static int CURRENT_VERSION = 103;
        public static bool connectionIsViaHTTPS = false;
        public static bool SFDCFailExtractOnObjectFailure = false;
        public static int SFDCObjectRetriesOnFailure = 4;
        public static string siteID = "0";
        public static string appVersion = "";
        public static bool initialized = false;
        public static bool showOldScheduler = true;
        public static bool DebugDataMode = false;
        public static bool UseBackGroundUploader = false;

        protected void Application_Start(object sender, EventArgs e)
        {
            QueryConnection conn = null;

            try
            {
                Path = Server.MapPath("~");

                FileInfo configFile = new FileInfo(Path + "config/log4net.config");
                if (configFile.Exists)
                    log4net.Config.XmlConfigurator.Configure(configFile);
                else
                    log4net.Config.XmlConfigurator.Configure();
                log4net.NDC.Clear();
                log4net.NDC.Push("Startup");

                string tmp = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteID();
                if (tmp != null && tmp.Length > 0)
                    siteID = tmp;

                Global.systemLog.Warn("************************************************************");
                Global.systemLog.Warn("************************************************************");
                Global.systemLog.Info("Initializing the Server: " + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName() + " / " + siteID);
                appVersion = Util.getInternalAppVersion();
                Util.logVersionInformation();
                Performance_Optimizer_Administration.AllPackages.setLogger(systemLog);
                Space.NEW_SPACE_TYPE = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
                Space.NEW_SPACE_PROCESSVERSION_ID = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["ProcessingEngineForNewSpace"]);
                string defaultSFDCVersionIDValue = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultSFDCVersionID"];
                setDebugDataMode();
                if (defaultSFDCVersionIDValue != null && defaultSFDCVersionIDValue.Length > 0)
                {
                    try
                    {
                        int sfdcDefaultVersionId = -1;
                        if (int.TryParse(defaultSFDCVersionIDValue, out sfdcDefaultVersionId))
                        {
                            Space.NEW_SPACE_SFDCVERSION_ID = sfdcDefaultVersionId;
                        }
                    }
                    catch (Exception ex3)
                    {
                        Global.systemLog.Warn("Exception while trying to parse DefaultSFDCVersionID", ex3);
                    }
                }

                string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

                string sfdcObjectRetryValue = System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCObjectRetries"];
                if (sfdcObjectRetryValue != null && sfdcObjectRetryValue.Length > 0)
                {
                    try
                    {
                        int retries = 0;
                        if (int.TryParse(sfdcObjectRetryValue, out retries))
                        {
                            SFDCObjectRetriesOnFailure = retries;
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Exception while trying to parse SFDCObjectRetries", ex2);
                    }
                }

                string sfdcHaltExtract = System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCStopExtractOnObjectFailure"];
                if (sfdcHaltExtract != null && sfdcHaltExtract.Length > 0)
                {
                    try
                    {
                        bool halt = false;
                        if (bool.TryParse(sfdcHaltExtract, out halt))
                        {
                            SFDCFailExtractOnObjectFailure = halt;
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Exception while trying to parse SFDCStopExtractOnObjectFailure", ex2);
                    }
                }
                showOldScheduler = Util.showOldSchedulerUI();
                UseBackGroundUploader = Util.useBackgroundUploader();
                conn = ConnectionPool.getConnection();
                Database.createLogTable(conn, mainSchema);
                int version = Database.getCurrentVersion(conn, mainSchema);
                if (version > 0 && version < CURRENT_VERSION)
                {
                    Global.systemLog.Info("Upgrading from version " + version + " to version " + CURRENT_VERSION);
                    Upgrade ug = new Upgrade(version, CURRENT_VERSION, mainSchema, conn);
                    ug.run();
                }
                Database.createTables(conn, mainSchema);

                //Remove all running process data information for the server
                //Database.removeProcessingServer(mainSchema, Util.getServerIP(), Util.getServerPort());
                // Remove all the current Login information for the server
                Database.removeCurrentLoginsForServer(conn, mainSchema, Util.getHostName());
                Util.setWarningThresholdForConcurrentLogins();

                // remove old SSO tokens
                Database.reapSSOTokens(conn, mainSchema);
                
                // remove old Upload tokens
                Database.reapUploadTokens(conn, mainSchema);

                // remove old password reset tokens
                Database.reapPasswordResetTokens(conn, mainSchema);

                // load various config files
                if (!Database.loadPreferencesTable(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load the preferences table");
                }
                if (!Util.loadSpaceParameters(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load space parameters");
                }
                if (!Database.loadProductsTable(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load the products table");
                }
                if (!Database.loadACLTable(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load the ACL table");
                }
                if (!Util.loadNewUserSpacesParameters(Path))
                {
                    throw new Exception("FAILED Initialized");
                }
                if (!Database.loadSpaceOperationsTable(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load the Space Operations table");
                }
                if (!Database.loadLocaleTable(conn, mainSchema, Path))
                {
                    throw new Exception("FAILED to load the Locale table");
                }

                PasswordPolicy.loadBadPasswords(Path);

                // clear out stuck schedules before starting the scheduler
                string workingServer = QueryServer.getHostIPAddress().ToString() + ":" + siteID + "/%";
                Database.reapTasksInBadState(conn, mainSchema, workingServer);

                Background.BackgroundProcess.StartBackgroundProcess();

                // Start task scheduler
                configureTaskScheduler();

                // Start realtime server
                ConnectionRelay.startServer(true);
                ServicePointManager.ServerCertificateValidationCallback = CertificateValidationCallback; // return true for all certificates
                Util.overrideProxyDBTypeSettings();
            }
            catch (Exception ex)
            {
                Global.systemLog.Fatal("************************************************************");
                Global.systemLog.Fatal("FAILED initialization");
                Global.systemLog.Fatal("************************************************************"); 
                Global.systemLog.Fatal(ex.Message, ex);
                Global.systemLog.Fatal("************************************************************");
                // HostingEnvironment.InitiateShutdown(); // causes a shutdown/startup/shutdown sequence
                return;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Warn("************************************************************");
            log4net.NDC.Clear();
            initialized = true;
        }

        private void setDebugDataMode()
        {
            string debugDataModeValue = System.Web.Configuration.WebConfigurationManager.AppSettings["DebugDataMode"];
            if (debugDataModeValue != null && debugDataModeValue.Length > 0)
            {
                try
                {
                    bool mode = false;
                    if (bool.TryParse(debugDataModeValue, out mode))
                    {
                        Global.DebugDataMode = mode;
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Exception while trying to parse DebugDataMode", ex2);
                }
            }
        }

        private void configureTaskScheduler()
        {
            TaskScheduler.path = Path;
            bool useQuartzScheduler = Util.isQuartzSchedulerEnabled();
            // If the quartz scheduler is not enabled, use inbuilt Task Scheduler for SFDC and scheduled report tasks
            // If the quartz scheduler is enabled, use the inbuilt Task Scheduler ONLY for tasks defined in the Web.Config

            
            if (isScheduledTaskEnabled("RunSFDCTask"))
            {
                TaskScheduler.addTask(SalesforceLoader.SOURCE_GROUP_NAME, SalesforceLoader.processSFDC);
                Util.taskScheduledForSForce = true;
            }

            if (isScheduledTaskEnabled("RunScheduledReportsTask"))
            {
                TaskScheduler.addTask(ScheduledReport.MODULE_NAME, ScheduledReport.execute);
                Util.taskScheduledForReport = true;
            }

            TaskScheduler.runScheduler();
        }

        private bool isScheduledTaskEnabled(String taskName)
        {
            bool run = false;
            String enableString = (string)System.Web.Configuration.WebConfigurationManager.AppSettings[taskName];
            if (enableString != null && enableString.Length > 0)
            {
                if (!Boolean.TryParse(enableString, out run))
                {
                    Global.systemLog.Warn(taskName + " set to an invalid value in web.config, Task Scheduler not enabled");
                    return run;
                }
            }
            else
            {
                Global.systemLog.Info(taskName + " not set in web.config");
                return run;
            }

            Global.systemLog.Info(taskName + " set to " + run);
            return run;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Server.GetLastError() == null)
                return;
            Exception excpt = Server.GetLastError().GetBaseException();

            if (excpt.Message == "File does not exist.")
            {
                try
                {
                    string url = HttpContext.Current.Request.Url.ToString();
                    string urlReferrer = "none";
                    if (HttpContext.Current.Request.UrlReferrer != null)
                        urlReferrer = HttpContext.Current.Request.UrlReferrer.ToString();
                    Global.systemLog.Error(excpt.Message + ": Referrer: " + urlReferrer + ", File: " + url + " ", excpt);     // Log
                }
                catch (Exception)
                {
                    Global.systemLog.Error(excpt.Message, excpt);
                }
                return;
            }
            Global.systemLog.Error(excpt);
            try
            {
                Session["Error"] = excpt.Message;
            }
            catch (Exception)
            {
            }
            Server.ClearError();
            try
            {
                if (Response != null)
                    Response.Redirect("~/Errors.aspx");
            }
            catch (Exception)
            {
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            log4net.NDC.Clear();
            Global.systemLog.Warn("************************************************************");
            log4net.NDC.Push("Shutdown");
            Global.systemLog.Warn("Application Shutdown starting");
            dumpReason();
            ConnectionRelay.stopServer();
            TaskScheduler.stopScheduler();
            Global.systemLog.Warn("Application Shutdown complete");
            Global.systemLog.Warn("************************************************************");
            Global.systemLog.Warn("************************************************************");
            log4net.NDC.Clear();
        }

        private void dumpReason()
        {
            HttpRuntime runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime",
                                                                                   BindingFlags.NonPublic
                                                                                   | BindingFlags.Static
                                                                                   | BindingFlags.GetField,
                                                                                   null,
                                                                                   null,
                                                                                   null);
            if (runtime == null)
                return;
            string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage",
                                                                             BindingFlags.NonPublic
                                                                             | BindingFlags.Instance
                                                                             | BindingFlags.GetField,
                                                                             null,
                                                                             runtime,
                                                                             null);
            string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack",
                                                                           BindingFlags.NonPublic
                                                                           | BindingFlags.Instance
                                                                           | BindingFlags.GetField,
                                                                           null,
                                                                           runtime,
                                                                           null);
             Global.systemLog.Warn(String.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}",
                                         shutDownMessage,
                                         shutDownStack));
        }

        protected void Application_Disposed(object sender, EventArgs e)
        {
        }

        void Application_EndRequest(object sender, EventArgs e)
        {
            try
            {
                if (!connectionIsViaHTTPS)
                {
                    if (Request.IsSecureConnection)
                    {
                        connectionIsViaHTTPS = true;
                        Global.systemLog.Info("enableSecureCookies being enabled since requests to the server are using https (request)");
                    }
                    else
                    {
                        string temp = Request.Params["birst.masterURL"]; // where we originally came from, before potential redirect
                        if (temp != null)
                        {
                            Uri uri = new Uri(temp);
                            if (uri.Scheme == "https")
                            {
                                connectionIsViaHTTPS = true;
                                Global.systemLog.Info("enableSecureCookies being enabled since requests to the server are using https (lb request)");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex.Message);
            }
            if (connectionIsViaHTTPS && (Response.Cookies.Count > 0))
            {
                foreach (string s in Response.Cookies.AllKeys)
                {
                    if (s == FormsAuthentication.FormsCookieName || s.ToLower() == "asp.net_sessionid") // XXX why not all cookies?
                    {
                        Response.Cookies[s].Secure = true;
                    }
                }
            }
            // most web service failures return 500, but most http response handlers can't handle 500, so lie and call it 200
            if (Response.StatusCode == 500 && Request.Url != null && Request.Url.AbsolutePath != null && Request.Url.AbsoluteUri.Contains(".asmx"))
            {
                Response.StatusCode = 200;
            }

            // do not cache web services responses (the .aspx pages deal with this via a method in Util to set up headers, should look at combining this
            if (Request.Url != null && Request.Url.AbsolutePath != null && Request.Url.AbsolutePath.Contains(".asmx"))
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoServerCaching();
                Response.Cache.SetNoStore();
                Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                Response.Cache.SetExpires(DateTime.Now);
            }
        }

        void Session_OnEnd(object sender, EventArgs e)
        {
            Util.endSession(Session);
            try
            {
                if (Session["User"] != null)
                {
                    User u = (User)Session["User"];
                    log4net.NDC.Clear();
                    log4net.NDC.Push(u.Username);
                    QueryConnection conn = null;
                    string schema = Util.getMainSchema();
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Database.removeCurrentLogin(conn, schema, u.ID, Session.SessionID);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    Global.systemLog.Info("Session timed out, sessionID " + Session.SessionID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Exception during logging out the user: " + ex.Message, ex);
            }
        }

        public static bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }      
    }
}