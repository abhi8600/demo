﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class Publish
    {
        public Guid SpaceID;
        public string LoadGroup;
        public string TableName;
        public int LoadNumber;
        public DateTime PublishDate; // end publish date
        public DateTime LoadDate;
        public int NumRows;
        public long DataSize;
        public DateTime StartPublishDate;
    }
}
