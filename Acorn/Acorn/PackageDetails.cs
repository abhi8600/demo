﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.Utils;

namespace Acorn
{
    public class PackageDetailsResponse : GenericResponse
    {
        public PackageDetails PackageDetails;
    }

    public class PackageObjects
    {
        public BaseSourceObject[] StagingTables;
        public BaseSourceObject[] DimensionTables;
        public BaseSourceObject[] MeasureTables;
        public BaseSourceObject[] Variables;
        public BaseSourceObject[] Aggregates;
        public BaseSourceObject[] CustomSubjectAreas;
    }

    public class PackageDetails
    {
        public string ID;
        public string Name;
        public string Description;
        public string CreatedBy;
        public string ModifiedBy;
        public DateTime? CreatedDate;
        public DateTime? ModifiedDate;
        public PackageObjects PackageObjects;
        public PackageGroup[] PackageGroups;
        public PackageUsage[] PackageUsages;
        public int CreatedVersion; // for tracking pre-5.3 vs 5.3 & beyond compatibility issue
    }

    public class PackageUsage
    {
        public string PackageID;
        public string ChildSpaceID;
        public string SpaceName;
        public string SpaceOwner;
        public bool Available;
        public string PackageSpaceID;
    }

    public class PackageGroup
    {
        public string GroupID;
        public string GroupName;
        public bool GroupAllowed;
    }


    public class BaseSourceObjectList : GenericResponse
    {
        public BaseSourceObject[] StagingTables;
        public BaseSourceObject[] DimensionTables;
        public BaseSourceObject[] MeasureTables;
        public BaseSourceObject[] Variables;
        public BaseSourceObject[] Aggregates;
        public BaseSourceObject[] CustomSubjectAreas;
    }

    public class BaseSourceObject
    {
        public BaseSourceObject() { }
        public BaseSourceObject(string name, string displayName, int type, bool hide)
        {
            this.Name = name;
            this.DisplayName = displayName;
            this.Type = type;
            this.Hide = hide;
            setAllowHide();
        }
        // currently name and display name are almost same for all the objects
        // in future will be helpful to allow customer to have customized display names
        public string Name;
        public string DisplayName;
        public int Type;
        public bool Hide;
        public bool AllowHide;

        private void setAllowHide()
        {
            // for now only csa is not allowed to be hidden
            this.AllowHide = Type != PackageUtils.PACKAGE_OBJECT_TYPE_CUSTOM_SUBJECT_AREA;
        }
    }

    public class PackageObjectDependency : GenericResponse
    {
        public string PackageID;
        public string PackageName;
        public BaseSourceObject SourceObject;
        public BaseSourceObject[] DependentObjects;
    }

    public class PackageGroupsList : GenericResponse
    {
        public PackageGroup[] PackageGroups;
    }

    public class Collision: GenericResponse
    {
        public PackageCollision[] PackageCollisions;
    }

    public class PackageCollision
    {
        public PackageCollision() { }
        public PackageCollision(string _spaceID, string _spaceName, string _packageID, string _packageName, string _objectName, int _collisionType)
        {
            this.SpaceID = _spaceID;
            this.SpaceName = _spaceName;
            this.PackageID = _packageID;
            this.PackageName = _packageName;
            this.ObjectName = _objectName;
            this.CollisionType = _collisionType;
        }

        public string SpaceID;
        public string SpaceName;
        public string PackageID;
        public string PackageName;
        public string ObjectName;
        public int CollisionType;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(PackageCollision).Equals(obj.GetType())) return false;
            PackageCollision other = (PackageCollision)obj;
            if (!Performance_Optimizer_Administration.Repository.EqualsWithNull(SpaceID, other.SpaceID)) return false;
            if (!Performance_Optimizer_Administration.Repository.EqualsWithNull(SpaceName, other.SpaceName)) return false;
            if (!Performance_Optimizer_Administration.Repository.EqualsWithNull(PackageID, other.PackageID)) return false;
            if (!Performance_Optimizer_Administration.Repository.EqualsWithNull(PackageName, other.PackageName)) return false;
            if (!Performance_Optimizer_Administration.Repository.EqualsWithNull(ObjectName, other.ObjectName)) return false;
            if (CollisionType != other.CollisionType) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

}