﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using Acorn.sforce;
using System.Web.Services.Protocols;
using Acorn.WebDav;
using Acorn.DBConnection;
using Acorn.Utils;
using System.Configuration;

namespace Acorn
{
    public class TaskScheduler
    {
        public static string path;
        public delegate TaskResults ExecuteTask(MainAdminForm maf, User u, QueryConnection conn, string schema, Space sp, DateTime curTime, Guid id, string parameters);
        private static int MINUTES_TO_WAIT = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["TaskSchedulePollingPeriod"]);
        private static int MINUTES_SLEEP_TASK_THREAD = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["TaskScheduleWorkerThreadSleepPeriod"]);
        private static Dictionary<string, ExecuteTask> modules = new Dictionary<string, ExecuteTask>();
        private static string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static Thread t;
        private static AutoResetEvent evt;

        public static void runScheduler()
        {
            bool run = false;
            String temp = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RunTaskScheduler"];
            if (temp != null)
            {
                if (!Boolean.TryParse(temp, out run))
                {
                    Global.systemLog.Warn("RunTaskScheduler set to an invalid value in web.config, Task Scheduler not enabled");
                   return;
                }
            }
            else
            {
                Global.systemLog.Info("RunTaskScheduler not set in web.config, Task Scheduler not enabled");
                return;
            }
            if (!run)
            {
                Global.systemLog.Info("RunTaskScheduler not set in web.config, Task Scheduler not enabled");
                return;
            }
            else
            {
                ConnectionStringSettings cs = ConfigUtils.getConnectionString();
                if (cs != null && cs.ConnectionString.Contains(("Driver={" + QueryConnection.oracledrivername + "}")))
                {
                    Global.systemLog.Warn("Oracle Admin DB does not support internal task scheduler. Disabling Acorn TaskScheduler.");
                    return;
                }
            }
            evt = new AutoResetEvent(true);
            Loop lp = new Loop();
            t = new Thread(new ThreadStart(lp.run));
            t.IsBackground = true;
            t.Start();
            Global.systemLog.Info("Started task scheduler");
        }

        public static void stopScheduler()
        {
            try
            {
                if (t != null)
                {
                    t.Abort();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to abort task scheduler thread", ex);
            }
        }

        // Used to wake up scheduler forcibly if it is sleeping. Used for run now option
        public static void wakeUpScheduler()
        {
            try
            {
                evt.Set();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to wake scheduler ", ex);
            }
        }

        public static bool addTask(string module, ExecuteTask t)
        {
            modules[module] = t;
            return true;
        }

        private class Loop
        {
            public void run()
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Scheduler");
                int numThreads = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NumScheduleThreads"]);
                // Wake periodically and see if any spaces need processing                                
                do
                {
                    Global.systemLog.Info("Running task scheduler");
                    DateTime curTime = DateTime.Now;
                    Thread[] threads = new Thread[numThreads];
                    for (int i = 0; i < threads.Length; i++)
                    {
                        TaskQueue tq = new TaskQueue(Global.siteID, i);
                        threads[i] = new Thread(new ThreadStart(tq.getFromQueue));
                        threads[i].IsBackground = true;
                        threads[i].Start();
                    }
                    for (int i = 0; i < threads.Length; i++)
                    {
                        threads[i].Join();
                    }
                    evt.WaitOne(1000 * 60 * MINUTES_TO_WAIT);
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Database.clearDeadTasks(conn);
                    }
                    catch (Exception e)
                    {
                        Global.systemLog.Error(e);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    //Thread.Sleep(1000 * 60 * MINUTES_TO_WAIT);
                } while (true);
            }
        }

        private class TaskQueue
        {
            int threadNum;
            string serverID;
            public TaskQueue(string serverID, int threadNum)
            {
                this.serverID = serverID;
                this.threadNum = threadNum;
            }

            private string getWorkingHost()
            {
                return QueryServer.getHostIPAddress().ToString() + ":" + serverID + "/" + threadNum;
            }

            public void getFromQueue()
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Scheduler");
                do
                {
                    QueryConnection conn = null;
                    ScheduledItem si = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        si = Database.getNextTask(conn, schema, getWorkingHost());
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    if (si == null)
                    {
                        // sleep for few minutes and continue to check
                        Global.systemLog.Debug("No scheduled task found. Sleeping before retrying again : " + MINUTES_SLEEP_TASK_THREAD + " (minutes)");
                        Thread.Sleep(1000 * 60 * MINUTES_SLEEP_TASK_THREAD);
                        continue;
                    }
                    if (si.Module != null && modules.ContainsKey(si.Module))
                    {
                        ExecuteTask et = modules[si.Module];
                        if (et != null)
                        {
                            TimeSpan delta = Util.getCustomizedSchedulerTime(DateTime.Now) - si.NextDate;
                            Global.systemLog.Info("Executing scheduled task for module: " + si.Module + ", id: " 
                                + si.ID + ", space: " + si.SpaceID + ", parameters: " + si.Parameters + ", delay (minutes): " + (int) delta.TotalMinutes);
                            try
                            {
                               	conn = ConnectionPool.getConnection();
                                processScheduledData(conn, schema, si, DateTime.Now, et);
                            }
                            catch (Exception e)
                            {
                                // prevent continous picking up of failed job. Update the scheduled time if needed.
                                Global.systemLog.Error(e);
                                DateTime nextDateTime = DateTime.MinValue;
                                Space sp = Database.getSpace(conn, schema, si.SpaceID);
                                if (sp != null)
                                {
                                    if (si.Module == SalesforceLoader.SOURCE_GROUP_NAME)
                                    {
                                        Util.deleteSFDCExtractFile(sp.Directory);
                                        Util.deleteSFDCKillLockFile(sp.Directory);
                                        // Check for thread abort exception during restart
                                        if (!(e is ThreadAbortException))
                                        {
                                            SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                                            if (settings != null)
                                            {
                                                SalesforceSettings.ScheduleInfo scheduleInfo = settings.getScheduleInfo(si.ID);
                                                nextDateTime = SalesforceLoader.getNextDate(scheduleInfo, scheduleInfo.nextScheduleTime);
                                            }
                                        }
                                    }
                                    if (si.Module == ScheduledReport.MODULE_NAME)
                                    {
                                        ScheduledReport sr = Database.getScheduledReport(conn, schema, si.ID);
                                        if (sr != null )
                                        {
                                            if (sr.Interval == ScheduledReport.INTERVAL_ONCE)
                                            {
                                                // if report was schedule to run instantly, then error out here and delete
                                                // the record
                                                Global.systemLog.Error("Instant report could not be run for space " + sp.ID + " and report id " + sr.ID);
                                                Database.deleteScheduledReport(conn, schema, new Guid(sr.ID));
                                                continue;
                                            }
                                            else
                                            {
                                                // Check for thread abort exception during restart
                                                if (!(e is ThreadAbortException))
                                                {
                                                    nextDateTime = sr.calcNextTime();
                                                }
                                            }
                                        }
                                    }
                                    Database.clearWorkingTasks(conn, schema, si.SpaceID, si.ID, si.Module, nextDateTime);
                                }                                
                            }
                            finally
                            {
                                ConnectionPool.releaseConnection(conn);
                            }
                        }
                    }
                } while (true);
            }
        }

        public static void processScheduledData(QueryConnection conn, string schema, ScheduledItem si, DateTime curTime, ExecuteTask runtask)
        {
            Space sp = Database.getSpace(conn, schema, si.SpaceID);
            if (sp == null)
            {
                // If not found, remove
                Global.systemLog.Warn("Cannot find the space " + si.SpaceID + ", removing the task");
                Database.removeTaskSchedule(conn, schema, si.SpaceID, Guid.Empty, si.Module);
                return;
            }            
            bool newRep = false;
            MainAdminForm maf = Util.loadRepository(sp, out newRep);
            List<SpaceMembership> smlist = Database.getUsers(conn, schema, sp, true);
            User u = null;
            foreach (SpaceMembership sm in smlist)
            {
                if (sm.User == null)
                    continue;
                if (sm.Owner)
                {
                    u = sm.User;
                    break;
                }
            }
            if (u == null)
            {
                Global.systemLog.Warn("Could not find the owner of the space " + sp.ID + "/" + sp.Name + ", removing the task");
                Database.removeTaskSchedule(conn, schema, si.SpaceID, Guid.Empty, si.Module);
                return;
            }

            try
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Scheduler: " + u.Username + "," + sp.ID);

                // Make sure that group/user/acl mapping is present in the database (4.3 release)
                Util.checkAndCreateUserGroupDbMappings(sp, maf);

                TaskResults results = runtask(maf, u, conn, schema, sp, curTime, si.ID, si.Parameters);
                if (results != null)
                {
                    Global.systemLog.Info("Running task with Process: " + results.Process);
                }
                else
                {
                    Global.systemLog.Info("Running task");
                }
                if (results != null && results.Process)
                {
                    ProcessData.Process(maf, sp, u, u.Email, path, DateTime.Now, results.LoadGroupName, results.LoadNumber, false, null);
                }
                Global.systemLog.Info("Completed task");
            }
            finally
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Scheduler");
            }
        }
    }
}
