﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Skin.aspx.cs" Inherits="Acorn.Skin"
    MasterPageFile="~/WebAdmin.Master" %>

<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <div class="pageheader">
            Modify Look and Feel
        </div>
        <div style="padding-right: 10px">
            <div class="wizard">
                <table>
                    <tr valign="top">
                        <td style="width: 175px; padding-left: 10px">
                            Logo Image:
                        </td>
                        <td style="font-weight: normal">
                            <div>
                                <asp:FileUpload ID="LogoUpload" runat="server" />&nbsp;<asp:Button ID="UploadButton"
                                    runat="server" Text="Upload" OnClick="UploadClick" />
                            </div>
                            <div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="width: 175px; padding-left: 10px">
                            Header Background Color:
                        </td>
                        <td style="font-weight: normal">
                            <asp:TextBox ID="ColorBox" runat="server"></asp:TextBox>&nbsp;<span style='font-size: 8pt'>Hex
                                value, e.g.: F7F6F3</span><asp:RegularExpressionValidator ID="ColorValidator" runat="server"
                                    ErrorMessage="&nbsp;Error: must be a hexadecimal color value" ControlToValidate="ColorBox"
                                    ValidationExpression="[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr height="40px" valign="middle">
                        <td align="center">
                            <asp:ImageButton ID="CancelChanges" runat="server" ToolTip="Cancel" OnClick="CancelChanges_Click"
                                ImageUrl="~/Images/Cancel.png" CausesValidation="false" />
                            <asp:ImageButton ID="SubmitSChanges" runat="server" ToolTip="Submit" OnClick="SubmitChanges_Click"
                                ImageUrl="~/Images/Done.png" />
                        </td>
                    </tr>
                </table>
                <asp:LinkButton ID="ResetLookAndFeel" runat="server" OnClick="ResetClick">Reset to Default</asp:LinkButton></div>
        </div>
    </div>
</asp:Content>
