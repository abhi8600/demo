﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.IO;
using Acorn.Utils;
using System.Web.Hosting;
using Acorn.DBConnection;
using Acorn.Exceptions;
using System.Text.RegularExpressions;
using System.Text;
using System.Diagnostics;

namespace Acorn
{
    public class NewSpaceService
    {
        private static string contentDirectory = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string spaceType = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"];
        private static string freeTrialSpaceType = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["FreeTrialNewSpaceType"];
        private static string processType = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ProcessingEngineForNewSpace"];
        private static string sfdcType = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultSFDCVersionID"];


        public static GenericResponse createSpace(HttpSessionState session, string spaceName, string schemaName, string spaceComments, int spaceMode, bool useTemplate, int queryLanguageVersion, string tid, User u, int createSpaceType)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            try
            {
                createSpaceBase(session, spaceName, schemaName, spaceComments, spaceMode, useTemplate, queryLanguageVersion, tid, u, createSpaceType);
            }
            catch (BirstException ex)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex, ex); // log unexpected exceptions
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = ex.Message;
            }
            return response;
        }

        // change to return spaceID and throw exceptions, write a wrapper that catches exception and returns response
        public static Guid createSpaceBase(HttpSessionState session, string spaceName, string schemaName, string spaceComments, int spaceMode, bool useTemplate, int queryLanguageVersion, string tid, User u, int createSpaceType)
        {
            Util.validateString("space name", spaceName, "[\\/<>:\\|\\*\\?\"]", 255, false); // See BPD-18581 and BPD-18751
            Util.validateString("space comments", spaceComments, null, 400, true);

            if (u == null)
            {
                throw new BirstException("Unable to continue. Please login again.");
            }

            if (useTemplate && tid == null)
            {
                throw new BirstException("Please choose a template");
            }

            QueryConnection conn = null;

            try
            {
                conn = ConnectionPool.getConnection();
                if (Util.checkForDuplicateSpace(u, spaceName) != null)
                {
                    throw new BirstException("Space Name already used");
                }

                // get parameters
                // defaults
                int processId = Space.NEW_SPACE_PROCESSVERSION_ID;
                int sfdcId = Space.NEW_SPACE_SFDCVERSION_ID;

                // web.config defaults
                if (processType != null)
                    Int32.TryParse(processType, out processId);
                if (sfdcType != null)
                    Int32.TryParse(sfdcType, out sfdcId);
               
                int spaceTypeId = getSpaceType(u);
                bool useUserOverrides = true;
                // if space id is explictly given use that
                if (createSpaceType > 0)
                {
                    spaceTypeId = createSpaceType;
                    useUserOverrides = false;
                }

                SpaceConfig sc = Util.getSpaceConfiguration(spaceTypeId);

                Space sp = new Space(u, spaceTypeId, processId, sfdcId, contentDirectory, schemaName);
                sp.Name = spaceName.Trim();
                sp.Comments = spaceComments.Trim();
                if (spaceMode == Space.SPACE_MODE_AUTOMATIC)
                {
                    sp.Automatic = true;
                    sp.DiscoveryMode = false;
                }
                else if (spaceMode == Space.SPACE_MODE_ADVANCED)
                {
                    sp.Automatic = false;
                    sp.DiscoveryMode = false;
                }
                else if (spaceMode == Space.SPACE_MODE_DISCOVERY)
                {
                    sp.Automatic = false;
                    sp.DiscoveryMode = true;
                }

                sp.fillSpaceAttributes(sc, u, useUserOverrides);
                sp.QueryLanguageVersion = queryLanguageVersion;

                if (schemaName != null && schemaName.Length > 0)
                {
                    Util.validateSchemaName(sp.getFullConnectString(), sp.DatabaseType, sp.Schema);
                }

                Database.createSpace(conn, mainSchema, u, sp);

                if (Database.isDBTypeParAccel(sp.ConnectString))
                {
                    string connectStringSuffix = Database.getPADBConnectionStringSuffix(conn, mainSchema, sp);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && !sp.ConnectString.EndsWith(connectStringSuffix))
                    {
                        sp.ConnectString += connectStringSuffix;
                    }
                }
                log4net.ThreadContext.Properties["user"] = u.Username;
                using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
                {
                    Global.userEventLog.Info("NEWSPACE");
                }

                bool clearStats = false;
                if (useTemplate)
                {
                    List<Template> tlist = Database.getTemplates(conn, mainSchema, null, new Guid(tid), u.ID);
                    if (tlist.Count == 0)
                    {
                        Util.setSpace(session, sp, u, false);
                        return sp.ID;
                    }
                    Template t = tlist[0];
                    sp.SourceTemplateName = t.Name;
                    sp.Automatic = t.Automatic;
                    string contentdir = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];
                    // XXX should really use Path.Combine for these path building operations
                    string templatedir = contentdir + "\\templates";
                    templatedir += "\\" + t.ID;
                    // Copy attachments
                    DirectoryInfo di = new DirectoryInfo(templatedir);
                    if (!Directory.Exists(sp.Directory + "\\attachments"))
                        Directory.CreateDirectory(sp.Directory + "\\attachments");
                    // Copy repository
                    Util.FileCopy(templatedir + "\\repository_dev.xml", sp.Directory + "\\repository_dev.xml");
                    // Copy Birst connect settings
                    string dcTargetDir = sp.Directory + "\\" + Util.DCCONFIG_DIR;
                    if (!Directory.Exists(dcTargetDir))
                        Directory.CreateDirectory(dcTargetDir);
                    if (Directory.Exists(templatedir + "\\" + Util.DCCONFIG_DIR))
                    {
                        DirectoryInfo source = new DirectoryInfo(templatedir + "\\" + Util.DCCONFIG_DIR);
                        FileInfo[] sfInfo = source.GetFiles();
                        if (sfInfo != null)
                        {
                            foreach (FileInfo fi in sfInfo)
                            {
                                Util.FileCopy(source.FullName + "\\" + fi.Name, dcTargetDir + "\\" + fi.Name);
                            }
                        }
                    }
                    else if (File.Exists(templatedir + "\\" + Util.DCCONFIG_FILE_NAME))
                    {
                        Util.FileCopy(templatedir + "\\" + Util.DCCONFIG_FILE_NAME, dcTargetDir + "\\" + Util.DCCONFIG_FILE_NAME);
                        //moving templateDir/dcconfig.xml to templateDir/dcconfig folder
                        String templateTargetDir = templatedir + "\\" + Util.DCCONFIG_DIR;
                        if (!Directory.Exists(templateTargetDir))
                            Directory.CreateDirectory(templateTargetDir);
                        File.Move(templatedir + "\\" + Util.DCCONFIG_FILE_NAME, templateTargetDir + "\\" + Util.DCCONFIG_FILE_NAME);
                    }
                    // Copy SFDC
                    if (File.Exists(templatedir + "\\sforce.xml"))
                        Util.FileCopy(templatedir + "\\sforce.xml", sp.Directory + "\\sforce.xml");
                    // Copy catalog
                    Util.copyDir(templatedir + "\\catalog\\", sp.Directory + "\\catalog");
                    // Copy attachments
                    Util.copyDir(templatedir + "\\attachments", sp.Directory + "\\attachments");
                    // Copy subject areas
                    if (!Directory.Exists(sp.Directory + "\\custom-subject-areas"))
                    {
                        Directory.CreateDirectory(sp.Directory + "\\custom-subject-areas");
                    }
                    if (Directory.Exists(templatedir + "\\custom-subject-areas"))
                    {
                        Util.copyDir(templatedir + "\\custom-subject-areas", sp.Directory + "\\custom-subject-areas");
                    }
                    // Clear out the stats from template
                    clearStats = true;
                    // Update number of uses
                    t.NumUses++;
                    Database.updateTemplate(conn, mainSchema, t);
                    // Make sure for spaces using template, OWNER$, USER$ get created
                    Util.setGroupPrivileges(sp, u.Username, u.AccountType);
                }
                else
                {
                    // create the catalog directory structure
                    DirectoryInfo catalogDirI = new DirectoryInfo(Path.Combine(sp.Directory, "catalog"));
                    if (!catalogDirI.Exists)
                        catalogDirI.Create();
                    DirectoryInfo privateDirI = new DirectoryInfo(Path.Combine(catalogDirI.FullName, "private"));
                    if (!privateDirI.Exists)
                        privateDirI.Create();
                    DirectoryInfo sharedDirI = new DirectoryInfo(Path.Combine(catalogDirI.FullName, "shared"));
                    if (!sharedDirI.Exists)
                        sharedDirI.Create();

                    DirectoryInfo reportStylesI = new DirectoryInfo(Path.Combine(sp.Directory, "catalog", "shared", "styles"));
                    if (!reportStylesI.Exists)
                    {
                        reportStylesI.Create();
                        string acornPath = HostingEnvironment.MapPath("~");
                        if (acornPath == null)
                        {
                            Global.systemLog.Info("acorn path was not available in hosting environment static reference. Trying to get it from System.Configuration");
                            acornPath = System.Configuration.ConfigurationManager.AppSettings["ConfigLocation"];
                        }
                        if (acornPath != null)
                        {
                            DirectoryInfo reportDefaultsI = new DirectoryInfo(Path.Combine(acornPath, "config", "ReportDefaults"));
                            if (reportDefaultsI.Exists)
                            {
                                // copy the contents to the styles directory
                                FileInfo[] fileList = reportDefaultsI.GetFiles();
                                foreach (FileInfo fi in fileList)
                                {
                                    Util.FileCopy(fi.FullName, Path.Combine(reportStylesI.FullName, fi.Name));
                                }
                            }
                        }
                    }
                    // create a dummy repository
                    Util.createInitialRepository(session, sp, u);
                }
                // Set the space
                Util.setSpace(session, sp, u, false);
                if (clearStats)
                {
                    MainAdminForm maf = null;
                    if (session != null)
                    {
                        maf = (MainAdminForm)session["MAF"];
                        maf.RequiresPublish = true;
                        foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                        {
                            sf.FileLength = 0;
                            sf.NumRows = 0;
                            sf.LastUploadDate = DateTime.Now;
                        }
                        // Clear any existing users
                        if (maf.usersAndGroups.usersList.Count > 1)
                        {
                            maf.usersAndGroups.usersList.RemoveRange(1, maf.usersAndGroups.usersList.Count - 1);
                        }
                    }
                    // Make sure user is added to the repository
                    Util.addUserToSpace(maf, sp, u.Username, Util.OWNER_GROUP_NAME);
                    Util.saveApplication(maf, sp, session, u);
                }
                List<SpaceMembership> mlist = Database.getSpaces(conn, mainSchema, u, true);
                if (session != null)
                    session["mlist"] = mlist;
                // update the UsersAndGroups.xml file            
                UserAndGroupUtils.saveUserAndGroupToFile(sp);
                return sp.ID;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static Boolean copyTablesLoadUnload(Space newsp, Space oldsp, MainAdminForm maf, List<String> tablesList)
        {
            string databaseloaddir = newsp.DatabaseLoadDir + "\\datastore";
            string databaseloaddirpath = newsp.DatabaseLoadDir + "\\datastore";
            string dbType = Util.getDBTypeForConnection(maf, Database.DEFAULT_CONNECTION);
            string awsCredentials = null;
            if (dbType == Database.INFOBRIGHT)
            {
                databaseloaddir = System.Web.Configuration.WebConfigurationManager.AppSettings["DatabaseLoadDir"] + "\\" + newsp.ID + "\\" + "datastore";
                databaseloaddirpath = System.Web.Configuration.WebConfigurationManager.AppSettings["DatabaseLoadDirAbsolute"] + "\\" + newsp.ID + "\\" + "datastore";
            }
            else if (dbType == Database.REDSHIFT)
            {                
                awsCredentials = Acorn.Utils.AWSUtils.getAWSCredentials(dbType, newsp);
                databaseloaddirpath = newsp.Directory + "\\datastore";
            }
            else if (dbType == Database.PARACCEL)
            {
                databaseloaddirpath = newsp.Directory + "\\datastore";
            }
            
            if (!Directory.Exists(databaseloaddirpath))
            {
                Directory.CreateDirectory(databaseloaddirpath);
            }

            string unloadCmdFileName = newsp.Directory + "\\" + "unloadtables.cmd";
            StringBuilder commands = new StringBuilder();
            commands.Append("repository " + oldsp.Directory + "\\repository.xml\n");

            foreach (String tableName in tablesList)
            {
                commands.Append("unload " + oldsp.Schema + " " + tableName + " " + databaseloaddir + " true"
                    + " targetschema="+newsp.Schema
                    + " databaseloaddirpath=" + databaseloaddirpath
                    +  (awsCredentials!=null?(" " + awsCredentials):"")
                    + "\n");
            }
            File.WriteAllText(unloadCmdFileName, commands.ToString());

            string loadCmdFileName = newsp.Directory + "\\" + "loadtables.cmd";
            commands = new StringBuilder();
            commands.Append("repository " + newsp.Directory + "\\repository.xml\n");

            foreach (String tableName in tablesList)
            {
                commands.Append("load " + newsp.Schema + " " + tableName + " " + databaseloaddir + "\\" + tableName + ".txt"
                    + " databaseloaddirpath=" + databaseloaddirpath
                    + " sourceschema=" + oldsp.Schema
                    + (awsCredentials != null ? (" " + awsCredentials) : "")
                    + "\n");
            }
            File.WriteAllText(loadCmdFileName, commands.ToString());

            int engineVersionInfo = 0;

            try
            {
                string engineVersionInfoCommand = Util.getVersionInfoCommand(newsp);
                // Create the ProcessInfo object
                ProcessStartInfo psi = new ProcessStartInfo(engineVersionInfoCommand);
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.RedirectStandardOutput = true;
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardInput = false;
                psi.RedirectStandardError = true;
                psi.WorkingDirectory = (new FileInfo(engineVersionInfoCommand)).Directory.FullName;

                Process proc = Process.Start(psi);
                string line = null;
                bool parsedValue = false;
                while ((line = proc.StandardError.ReadLine()) != null)
                {
                    Global.systemLog.Warn(line);
                }
                while ((line = proc.StandardOutput.ReadLine()) != null)
                {
                    if (Int32.TryParse(line, out engineVersionInfo))
                    {
                        Global.systemLog.Debug("Successfully parsed processing engine version - " + engineVersionInfo);
                        parsedValue = true;
                    }
                    else
                    {
                        Global.systemLog.Debug("Could not parse value '" + line + "' for processing engine version");
                    }
                }
                if (!proc.HasExited)
                {
                    proc.WaitForExit();
                }
                if (!parsedValue)
                {
                    Global.systemLog.Debug("Could not parse processing engine version, using defaultvalue - " + engineVersionInfo);
                }
                proc.Close();
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message, ex);
                Global.systemLog.Debug("Unable to find version info for processing engine, using defaultvalue - " + engineVersionInfo);
            }
            if (engineVersionInfo >= 4)
            {
                try
                {
                    if (!File.Exists(newsp.Directory + "\\publish.lock"))
                    {
                        FileStream fs = File.Create(newsp.Directory + "\\publish.lock");
                        fs.Close();
                        string enginecmd = Util.getEngineCommand(newsp);

                        //Unload tables
                        ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                        psi.Arguments = newsp.Directory + "\\unloadtables.cmd " + newsp.Directory;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        Process proc = Process.Start(psi);
                        proc.WaitForExit();
                        string line = null;
                        while ((line = proc.StandardError.ReadLine()) != null)
                        {
                            Global.systemLog.Warn(line);
                        }
                        while ((line = proc.StandardOutput.ReadLine()) != null)
                        {
                            Global.systemLog.Info(line);
                        }
                        if (proc.HasExited)
                        {
                            Global.systemLog.Info("UnloadTables (" + newsp.ID + ") Exited with code: " + proc.ExitCode);
                        }
                        else
                        {
                            Global.systemLog.Info("UnloadTables (" + newsp.ID + ") - Running");
                            proc.WaitForExit();
                        }
                        proc.Close();

                        //Load the tables
                        psi = new ProcessStartInfo(enginecmd);
                        psi.Arguments = newsp.Directory + "\\loadtables.cmd " + newsp.Directory;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        proc = Process.Start(psi);
                        proc.WaitForExit();
                        line = null;
                        while ((line = proc.StandardError.ReadLine()) != null)
                        {
                            Global.systemLog.Warn(line);
                        }
                        while ((line = proc.StandardOutput.ReadLine()) != null)
                        {
                            Global.systemLog.Info(line);
                        }
                        if (proc.HasExited)
                        {
                            Global.systemLog.Info("LoadTables (" + newsp.ID + ") Exited with code: " + proc.ExitCode);
                        }
                        else
                        {
                            Global.systemLog.Info("LoadTables (" + newsp.ID + ") - Running");
                            proc.WaitForExit();
                        }
                        proc.Close();
                        return true;
                    }
                    else
                    {
                        Global.systemLog.Info("Exception while executing copytable command for space: " + newsp.ID.ToString() + " Error: Cannot execute while processing/updatestats/copytable is running");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Debug(ex.Message, ex);
                    Global.systemLog.Debug("Unable to find version info for processing engine, using defaultvalue - " + engineVersionInfo);
                    return false;
                }
                finally
                {
                    try
                    {
                        File.Delete(newsp.Directory + "\\publish.lock");
                        Directory.Delete(databaseloaddirpath, true);
                    }
                    catch (IOException ex)
                    {
                        Global.systemLog.Error("Unable to delete publish lock on space " + newsp.ID.ToString(), ex);
                    }
                }
            }
            else
            {
                Global.systemLog.Debug("Cannot copy tables for Paraccel with Process Version less than 4. Current Version - " + engineVersionInfo);
                return false;
            }
        }

        public static Boolean copyTablesUsingCmdFile(Space sp, List<String> tablesList, String sourceSchema, String targetSchema,MainAdminForm maf)
        {         
            string dbType = Util.getDBTypeForConnection(maf, Database.DEFAULT_CONNECTION);
            bool appendDatabasePath = false;
            if (dbType == Database.PARACCEL || dbType == Database.REDSHIFT)
                appendDatabasePath = true;
            else if (dbType == Database.INFOBRIGHT && maf.builderVersion >= 16 && maf.UseNewETLSchemeForIB)
            {
                appendDatabasePath = true;
            }

            string cmdFilename = sp.Directory + "\\" + "copytables.cmd";
            StringBuilder commands = new StringBuilder();
            commands.Append("repository " + sp.Directory + "\\repository.xml\n");

            string awsCredentials = null;
            if (dbType != null && dbType == Database.REDSHIFT)
            {
                awsCredentials = Acorn.Utils.AWSUtils.getAWSCredentials(dbType, sp);
            }
            commands.Append("generateschema 1 notime \"execonchange=copy " + sp.Directory + "\\..\\publish.lock " + sp.Directory + "\""
                + (appendDatabasePath ? " \"databasepath=" + sp.DatabaseLoadDir + "\\data\"" : "")
                 + awsCredentials
                 + "\n");
            commands.Append("resetetlrun 1" + "\n"); 
            
            foreach(String tableName in tablesList)
            {
                if (tableName != null && tableName.ToUpper().Equals("TXN_SCRIPT_LOG"))
                {
                    commands.Append("createscriptlog" + " " + "TXN_SCRIPT_LOG" + "\n");
                }
                commands.Append("copytable " + sourceSchema + " " + targetSchema + " " + tableName + " " + sp.DatabaseLoadDir + "\\data\\ "
                    + awsCredentials 
                    + "\n");
            }
            File.WriteAllText(cmdFilename, commands.ToString());
            
            int engineVersionInfo = 0;

            try
            {
                string engineVersionInfoCommand = Util.getVersionInfoCommand(sp);
                // Create the ProcessInfo object
                ProcessStartInfo psi = new ProcessStartInfo(engineVersionInfoCommand);
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.RedirectStandardOutput = true;
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardInput = false;
                psi.RedirectStandardError = true;
                psi.WorkingDirectory = (new FileInfo(engineVersionInfoCommand)).Directory.FullName;

                Process proc = Process.Start(psi);
                string line = null;
                bool parsedValue = false;
                while ((line = proc.StandardError.ReadLine()) != null)
                {
                    Global.systemLog.Warn(line);
                }
                while ((line = proc.StandardOutput.ReadLine()) != null)
                {
                    if (Int32.TryParse(line, out engineVersionInfo))
                    {
                        Global.systemLog.Debug("Successfully parsed processing engine version - " + engineVersionInfo);
                        parsedValue = true;
                    }
                    else
                    {
                        Global.systemLog.Debug("Could not parse value '" + line + "' for processing engine version");
                    }
                }
                if (!proc.HasExited)
                {
                    proc.WaitForExit();
                }
                if (!parsedValue)
                {
                    Global.systemLog.Debug("Could not parse processing engine version, using defaultvalue - " + engineVersionInfo);
                }
                proc.Close();
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message, ex);
                Global.systemLog.Debug("Unable to find version info for processing engine, using defaultvalue - " + engineVersionInfo);
            }
            if (engineVersionInfo >= 4)
            {
                try
                {

                    if (!File.Exists(sp.Directory + "\\publish.lock"))
                    {
                        FileStream fs = File.Create(sp.Directory + "\\publish.lock");
                        fs.Close();
                        string enginecmd = Util.getEngineCommand(sp);
                        ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                        psi.Arguments = sp.Directory + "\\copytables.cmd " + sp.Directory ;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        Process proc = Process.Start(psi);
                        proc.WaitForExit();
                        string line = null;
                        while ((line = proc.StandardError.ReadLine()) != null)
                        {
                            Global.systemLog.Warn(line);
                        }
                        while ((line = proc.StandardOutput.ReadLine()) != null)
                        {
                            Global.systemLog.Info(line);
                        }
                        if (proc.HasExited)
                        {
                            Global.systemLog.Info("CopyTable (" + sp.ID + ") Exited with code: " + proc.ExitCode);
                        }
                        else
                        {
                            Global.systemLog.Info("CopyTable (" + sp.ID + ") - Running");
                            proc.WaitForExit();
                        }
                        proc.Close();
                        return true;
                    }
                    else
                    {
                        Global.systemLog.Info("Exception while executing copytable command for space: " + sp.ID.ToString() + " Error: Cannot execute while processing/updatestats/copytable is running");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Debug(ex.Message, ex);
                    Global.systemLog.Debug("Unable to find version info for processing engine, using defaultvalue - " + engineVersionInfo);
                    return false;
                }
                finally
                {
                    try
                    {
                        File.Delete(sp.Directory + "\\publish.lock");
                    }
                    catch (IOException ex)
                    {
                        Global.systemLog.Error("Unable to delete publish lock on space " + sp.ID.ToString(), ex);
                    }
                }
            }
            else
            {
                Global.systemLog.Debug("Cannot copy tables for Paraccel with Process Version less than 4. Current Version - " + engineVersionInfo);
                return false;
            }
        }

        public static int getSpaceType(User u)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                
                // get parameters
                // defaults
                int spaceTypeId = Space.NEW_SPACE_TYPE;
                int freeTrialSpaceTypeId = Space.NEW_SPACE_TYPE;
                
                // web.config defaults
                if (spaceType != null)
                    Int32.TryParse(spaceType, out spaceTypeId);
                if (u.isFreeTrialUser && freeTrialSpaceType != null)
                {
                    if (Int32.TryParse(freeTrialSpaceType, out freeTrialSpaceTypeId))
                        spaceTypeId = freeTrialSpaceTypeId;
                }

                // allow account space type to override default
                AccountDetails details = Database.getAccountDetails(conn, mainSchema, u.ManagedAccountId, u);
                if (details != null)
                {
                    int acctSpaceType = details.getDefaultSpaceType();
                    if (acctSpaceType != -1)
                        spaceTypeId = acctSpaceType;
                }
                // allow use space type to override default, much better than putting DB connection information in the user record... sigh... we need to stop populating it
                if (u.SpaceType != -1)
                    spaceTypeId = u.SpaceType;

                return spaceTypeId;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
        //overloading for MoveSpace to pass in mainSchema
        public static int getSpaceType(User u,string mainSchema)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();

                // get parameters
                // defaults
                int spaceTypeId = Space.NEW_SPACE_TYPE;
                int freeTrialSpaceTypeId = Space.NEW_SPACE_TYPE;

                // web.config defaults
                if (spaceType != null)
                    Int32.TryParse(spaceType, out spaceTypeId);
                if (u.isFreeTrialUser && freeTrialSpaceType != null)
                {
                    if (Int32.TryParse(freeTrialSpaceType, out freeTrialSpaceTypeId))
                        spaceTypeId = freeTrialSpaceTypeId;
                }

                // allow account space type to override default
                AccountDetails details = Database.getAccountDetails(conn, mainSchema, u.ManagedAccountId, u);
                if (details != null)
                {
                    int acctSpaceType = details.getDefaultSpaceType();
                    if (acctSpaceType != -1)
                        spaceTypeId = acctSpaceType;
                }
                // allow use space type to override default, much better than putting DB connection information in the user record... sigh... we need to stop populating it
                if (u.SpaceType != -1)
                    spaceTypeId = u.SpaceType;

                return spaceTypeId;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
