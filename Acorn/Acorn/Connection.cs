﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class Connection : DatabaseConnection
    {
        public string LoadGroup;
        public bool LocalETL;
        public string Server;
        public string Port;
        public string Timeout;
        public bool GenericDatabase;
        public Connection() { }
        public Connection(string visiblename, string name, bool realtime)
        {
            this.VisibleName = visiblename;
            this.Name = name;
            this.Realtime = realtime;
        }
        public Connection(DatabaseConnection dc)
        {
            this.VisibleName = dc.VisibleName == null ? dc.Name : dc.VisibleName;
            this.Name = dc.Name;
            this.ConnectionPoolSize = dc.ConnectionPoolSize;
            this.ConnectString = dc.ConnectString;
            this.Database = dc.Database;
            this.Driver = dc.Driver;
            this.ID = dc.ID;
            this.Password = dc.Password;
            this.Realtime = dc.Realtime;
            this.Schema = dc.Schema;
            this.Type = dc.Type;
            this.UnicodeDatabase = dc.UnicodeDatabase;
            this.UserName = dc.UserName;
        }
    }
}
