﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using System.Net.Mail;
using System.IO;
using Acorn.Background;
using Acorn.Utils;
using Acorn.Exceptions;
using Performance_Optimizer_Administration;
using Acorn.DBConnection;
using System.Security.Cryptography;
using Amazon.Runtime;

namespace Acorn
{
    public class SpaceAdminService
    {
        public static string SPACE_AUTOMATIC_NAME = "Automatic";
        public static string SPACE_ADVANCED_NAME = "Advanced";
        public static string SPACE_NEW_GROUP_NAME = "Group";
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string awsAccessKeyId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsAccessKeyId"];
        private static string awsSecretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsSecretKey"];

        public static GenericResponse createCopy(HttpSessionState session, string spaceName, string schemaName, string copyComments)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            // Prevent duplicates
            User u = (User)session["User"];
            if (u == null)
            {
                clearIsCopySessionValue(session);
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again.";
                return response;
            }

            if (session["iscopy"] == null)
                session["iscopy"] = true;
            else
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Already copying a space. Please wait until process is finished.";
                return response;
            }

            Space oldsp = (Space)session["space"];
            if (oldsp == null)
            {
                clearIsCopySessionValue(session);
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;                
                return response;
            }

            if (schemaName != null && schemaName.Length > 0)
            {
                try
                {
                    Util.validateSchemaName(oldsp.getFullConnectString(), oldsp.DatabaseType, schemaName);
                }
                catch (SchemaException schemaEx)
                {
                    clearIsCopySessionValue(session);
                    error.ErrorType = schemaEx.getErrorType();
                    error.ErrorMessage = schemaEx.getErrorMessage();
                    return response;
                }
            }

            Dictionary<Guid, Database.SpaceUsage> sulist = null;
            QueryConnection conn = null;
            spaceName = spaceName.Trim();
            if (Util.checkForDuplicateSpace(u, spaceName) != null)
            {
                clearIsCopySessionValue(session);
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Name already exists. Please choose a new name.";                
                return response;
            }
            try
            {
                conn = ConnectionPool.getConnection();                                
                sulist = Database.getSpaceUsage(conn, mainSchema, u);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            // is the copied from space busy in other operations
            int oldSpOpID = SpaceOpsUtils.getSpaceAvailability(oldsp);
            if (!SpaceOpsUtils.isSpaceAvailable(oldSpOpID))
            {
                string oldSpaceUnavailableMsg = SpaceOpsUtils.getAvailabilityMessage(oldsp.Name, oldSpOpID);
                Global.systemLog.Error(oldSpaceUnavailableMsg);
                throw new SpaceUnavailableException(oldSpOpID, oldSpaceUnavailableMsg);
            }

            if (Util.checkForPublishingStatus(oldsp))
            {
                clearIsCopySessionValue(session);
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to copy space. Source space is being published";
                return response;
            }

            CopySpace cs = new CopySpace(oldsp, session, spaceName, schemaName, copyComments);
            BackgroundProcess.startModule(new BackgroundTask(cs, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));

            clearIsCopySessionValue(session);
            return response;
        }

        private static void clearIsCopySessionValue(HttpSessionState session)
        {
            if (session != null)
            {
                session.Remove("iscopy");
            }
        }

        private class CopySpace : BackgroundModule
        {
            Space sp;
            HttpSessionState session;
            string name;
            string comments;
            string schemaName;
            private Dictionary<BackgroundModuleObserver, object> observers;

            public CopySpace(Space sp, HttpSessionState session, string name, string schemaName, string comments)
            {
                this.sp = sp;
                this.session = session;
                this.name = name;
                this.schemaName = schemaName;
                this.comments = comments;
                observers = new Dictionary<BackgroundModuleObserver, object>();
            }

            public void run()
            {
                if (session != null)
                {
                    User u = (User)session["user"];
                    if (u != null)
                        log4net.ThreadContext.Properties["user"] = u.Username;
                }
                Database.copySpaceToNewSpace(sp, session, name, schemaName, comments);
                using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
                {
                    Global.userEventLog.Info("COPYSPACE");
                }
                session["setupspaces"] = true;
            }

            public void kill()
            {
                /*
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.killed(kvp.Value);
                }
                 */
            }

            public void finish()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            #region BackgroundModule Members


            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }

            public Status.StatusResult getStatus()
            {
                // I don't think this can fail.
				return new Status.StatusResult(Status.StatusCode.Complete);
            }

            #endregion
        }

        public static GenericResponse convertSpace(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)session["space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }

            sp.Automatic = false;
            sp.DiscoveryMode = false;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        public static GenericResponse generateSSOCredentials(HttpSessionState session, HttpServerUtility server)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = Util.getSessionSpace(session);
            User u = (User)session["user"];
            if (sp == null || u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please try again.";
                return response;
            }

            setNewSSOCredentials(sp);
            response.other = new string[] { sp.SSOPassword };
            sp.SSO = true;
            QueryConnection conn = null; 
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        private static void setNewSSOCredentials(Space sp)
        {
            sp.SSOPassword = genRandomCredential();
        }

        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        private static string genRandomCredential()
        {
            char[] buff = new char[32];
            for (int i = 0; i < buff.Length; i++)
            {
                byte[] rnd = new byte[4];
                rngCsp.GetBytes(rnd);
                int r = Math.Abs(BitConverter.ToInt32(rnd, 0)) % 62;
                if (r < 26)
                    buff[i] = (char)('a' + r);
                else if (r < 26 + 26)
                    buff[i] = (char)('A' + (r - 26));
                else
                    buff[i] = (char)('0' + (r - 26 - 26));
            }
            return new string(buff);
        }

        public static GenericResponse resetSpaceSettings(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorMessage = "Unable to get space details";
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                return response;
            }
            sp.clearSettings();
            return response;
        }

        public static GenericResponse modifySpaceProperties(HttpSessionState session, HttpServerUtility server,
            string spaceName, string spaceComments, bool testMode, bool enableSSO,
            string bgcolor, string fgcolor, string spaceProcessingTimeZone, int maxQueryRows, int maxQueryTimeout, int queryLanguageVersion,
            bool enableUsage, int peVersionID, bool useDynamicGroups, bool allowRetryFailedLoad, int numThresholdFailedRecords,
            int minYear, int maxYear, bool mapNullsToZero, string scheduleFrom, string scheduleSubject,string databaseCollation,
            string rServerURL, int rServerPort, string rServerUsername, string rServerPassword)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorMessage = "Unable to get space details";
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                return response;
            }

            if (string.IsNullOrWhiteSpace(spaceName))
            {
                throw new BirstException("Space name cannot be blank");
            }

            User u = Util.getSessionUser(session);
            Space duplicateSpace = Util.checkForDuplicateSpace(u, spaceName);
            if (duplicateSpace != null && duplicateSpace.ID != sp.ID )
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Space Name already used";
                return response;
            }

            sp.Name = spaceName.Trim();
            sp.TestMode = testMode;
            if (enableSSO && sp.SSOPassword == null)
            {
                setNewSSOCredentials(sp);
            }
            sp.SSO = enableSSO;
            sp.Comments = spaceComments.Trim();
            // Upload request for logo can go to a different server than the current and saves it in settings file
            // Make sure to refresh the uploaded logo image if needed
            // TO DO make sure all the actions on this page are only savable on "Save button". Right now its partial.
            sp.refreshLogoImage();
            setBGColorProperties(sp, bgcolor);
            setFGColorProperties(sp, fgcolor);         

            // can only update these if a repository admin
            if (u.RepositoryAdmin)
            {
                sp.MaxQueryRows = maxQueryRows;
                sp.MaxQueryTimeout = maxQueryTimeout;
                if (sp.QueryLanguageVersion != queryLanguageVersion)
                {
                    response.other = new string[] { "UpdateSources" };
                }                
            }

            MainAdminForm maf = (MainAdminForm)session["MAF"];
            string repDevFileName = Path.Combine(sp.Directory, Util.REPOSITORY_DEV);
            if (maf != null)
            {
                if (databaseCollation != null && databaseCollation.Length>0)
                {
                    maf.serverPropertiesModule.Collation = databaseCollation;
                }
                else
                {
                    maf.serverPropertiesModule.Collation = null;
                }
                if (rServerURL != null && rServerURL.Trim().Length > 0)
                {
                    maf.RServer = new RServer();
                    maf.RServer.URL = rServerURL.Trim();
                    maf.RServer.Port = rServerPort;
                    maf.RServer.Username = rServerUsername;
                    maf.RServer.Password = rServerPassword;
                }
                else
                {
                    maf.RServer = null;
                }
                maf.saveRepository(repDevFileName);
            }          

            sp.QueryLanguageVersion = queryLanguageVersion;
            sp.MapNullsToZero = mapNullsToZero;
            sp.UsageTracking = enableUsage;
            sp.ProcessVersionID = peVersionID;
            sp.UseDynamicGroups = useDynamicGroups;
            sp.AllowRetryFailedLoad = allowRetryFailedLoad;
            sp.ScheduleFrom = scheduleFrom;
            sp.ScheduleSubject = scheduleSubject;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, mainSchema, sp);            
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            TimeZoneUtils.setSpaceTimeZone(spaceProcessingTimeZone, session);
            Util.setNumThresholdFailedRecords(numThresholdFailedRecords, session);
            Util.setMinMaxYear(minYear, maxYear, session);
            return response;
        }
        public static void setBGColorProperties(Space sp,string bgcolor)
        {
            if (bgcolor.Length == 6)
            {
                if (sp.Settings == null)
                    sp.Settings = new SpaceSettings();
                byte[] colors = Util.getBytesFromHex(bgcolor);
                sp.Settings.HeaderBackgroundColor = ((long)colors[0] << 16) + ((long)colors[1] << 8) + colors[2];
                sp.saveSettings();
            }           
        }

        public static void setFGColorProperties(Space sp, string fgcolor) 
        {
            if (fgcolor.Length == 6)
            {
                if (sp.Settings == null)
                    sp.Settings = new SpaceSettings();
                byte[] colors = Util.getBytesFromHex(fgcolor);
                sp.Settings.HeaderForegroundColor = ((long)colors[0] << 16) + ((long)colors[1] << 8) + colors[2];
                sp.saveSettings();
            }           
        }

        public static SpaceProperties getSpaceProperties(HttpSessionState session)
        {
            SpaceProperties response = new SpaceProperties();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User u = (User)session["User"];
            Space sp = Util.getSessionSpace(session);
            if (u == null || sp == null)
            {
                error.ErrorMessage = "Unable to get space details.";
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                return response;
            }
            response.SpaceID = sp.ID.ToString();
            response.SpaceName = sp.Name;
            response.SpaceComments = sp.Comments;
            if (sp.Automatic)
            {
                response.SpaceType = Space.SPACE_MODE_AUTOMATIC;
            }
            else if (sp.DiscoveryMode)
            {
                response.SpaceType = Space.SPACE_MODE_DISCOVERY;
            }
            else
            {
                response.SpaceType = Space.SPACE_MODE_ADVANCED;
            }

            response.TestMode = u.CanUseTestMode;
            response.checkTestMode = sp.TestMode;
            response.EnableSSO = true;
            response.checkSSO = sp.SSO;
            response.SSOPassword = sp.SSOPassword;
            response.LookAndFeel = true;
            response.EnableConvert = !u.isDiscoveryModeOnly() && (sp.Automatic || sp.DiscoveryMode);
            response.LoadNumber = sp.LoadNumber;
            response.UploadData = u.CanUploadData;
            response.MaxQueryRows = sp.MaxQueryRows;
            response.MaxQueryTimeout = sp.MaxQueryTimeout;
            response.QueryLanguageVersion = sp.QueryLanguageVersion;
            response.MapNullsToZero = sp.MapNullsToZero;
            response.RepositoryAdmin = u.RepositoryAdmin;
            response.EnableUsage = sp.UsageTracking;
            response.ProcessVersion = Util.getPerformanceEngineVersion(sp); // sp.ProcessVersionID <= 0 ? Util.SPACE_PROCESS_VERSION_UNDEFINED : sp.ProcessVersionID;
            response.EnableAllPEVersions = u.OperationsFlag;
            response.UseDynamicGroups = sp.UseDynamicGroups;
            response.Published = Status.hasLoadedData(sp, null);
            // Always retrieve it from db

            Dictionary<string, string> props = Util.getReportScheduleEmailProps(sp);
            if (props != null && props.Count > 0)
            {
                if (props.ContainsKey(Util.KEY_REPORT_EMAIL_FROM))
                {
                    response.ScheduleFrom = props[Util.KEY_REPORT_EMAIL_FROM];
                }
                if (props.ContainsKey(Util.KEY_REPORT_EMAIL_SUBJECT))
                {
                    response.ScheduleSubject = props[Util.KEY_REPORT_EMAIL_SUBJECT];
                }
            }

            bool newRepository = false;
            MainAdminForm maf = Util.loadRepository(sp, out newRepository);
            bool peSupportsRetryFailedLoad = (response.ProcessVersion != null ? response.ProcessVersion.SupportsRetryLoad : false);
            if (!peSupportsRetryFailedLoad)
            {                
                Dictionary<string, Connection> localETLConnections = BirstConnectUtil.getLocalETLConenctions(sp, maf);
                foreach (string leconn in localETLConnections.Keys)
                {
                    PerformanceEngineVersion lePerfEngineVersion = LiveAccessUtil.getLocalProcessingEngineVersion(sp, leconn);
                    if (lePerfEngineVersion != null && lePerfEngineVersion.SupportsRetryLoad)
                    {
                        peSupportsRetryFailedLoad = lePerfEngineVersion.SupportsRetryLoad;
                        break;
                    }
                }                
            }            
            response.SupportsRetryFailedLoad = peSupportsRetryFailedLoad;
            response.AllowRetryFailedLoad = peSupportsRetryFailedLoad && sp.AllowRetryFailedLoad;
            response.DBCollationName = maf.serverPropertiesModule.Collation;

            // Get list of spaces for swapping
            QueryConnection conn = null;
            List<SpaceMembership> mlist = null;
            List<string> dbCollations = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getSpaces(conn, mainSchema, u, true);
                dbCollations = Database.getDatabaseCollations(conn, mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            List<string[]> adminSpaces = new List<string[]>();
            for (int i = 0; i < mlist.Count; i++)
            {
                if (mlist[i].Space.ID != sp.ID && mlist[i].Administrator)
                {
                    string[] spid = new string[] { mlist[i].Space.Name, mlist[i].Space.ID.ToString() };
                    adminSpaces.Add(spid);
                }
            }            

            if (adminSpaces.Count > 0)
                response.AdminSpaces = adminSpaces.ToArray();
            if (dbCollations != null && dbCollations.Count > 0)
            {
                response.DBCollations = dbCollations.ToArray();
                response.DefaultCollationName = Database.getDefaultCollationName(conn,sp);
            }
            // add time zone set at the space/repository level
            string spaceTZ = TimeZoneUtils.getSpaceTimeZone(session);
            if (spaceTZ == null)
            {
                Global.systemLog.Error("Unable to get the space time zone");
            }
            response.SpaceTimeZone = spaceTZ;

            // fetch the status only if the current load number is 0 because
            // space might be still in processing mode. If the load number is non zero,
            // we do not care since ProcessingTime Zone will be locked regardless.            
            if (sp.LoadNumber > 0)
            {
                response.LockProcessingTimeZone = true;
                response.LockDBCollations = true;
            }
            else if (sp.LoadNumber == 0)
            {
                Status.StatusResult status = (Status.StatusResult)session["status"];
                if (status != null && Status.isRunningCode(status.code))
                {
                    response.LockProcessingTimeZone = true;
                    response.LockDBCollations = true;
                }
                else
                {
                    response.LockProcessingTimeZone = false;
                }

            }
            if (dbCollations.Count == 0)
                response.LockDBCollations = true;
            response.NumThresholdFailedRecords = Util.getNumThresholdFailedRecords(session);
            response.MinYear = Util.getMinYear(session);
            response.MaxYear = Util.getMaxYear(session);

            response.bgcolor = "FFFFFF";
            response.fgcolor = "2A303A";

            if (sp.Settings != null)
            {
                if (sp.Settings.HeaderBackgroundColor >= 0)
                    response.bgcolor = Util.getHexStringFromLongColor(sp.Settings.HeaderBackgroundColor);
                if (sp.Settings.HeaderForegroundColor >= 0)
                    response.fgcolor = Util.getHexStringFromLongColor(sp.Settings.HeaderForegroundColor);
            }
            if (maf.RServer != null && maf.RServer.URL != null && maf.RServer.URL.Length > 0)
            {
                response.RServerURL = maf.RServer.URL;
                response.RServerPort = maf.RServer.Port;
                response.RServerUsername = maf.RServer.Username;
                response.RServerPassword = maf.RServer.Password;
            }
            return response;
        }

        public static void swapSpaces(Space curSpace, Space otherSpace, MainAdminForm maf, User u, bool alignCreatedPackages)
        {
            Global.systemLog.Info("SwapSpaces: " + curSpace.ID + " and " + otherSpace.ID);

            Dictionary<string, Connection> leConn = BirstConnectUtil.getLocalETLConenctions(curSpace, maf);
            if (leConn.Keys.Count > 0)
            {
                foreach (string key in leConn.Keys)
                {
                    if (!LiveAccessUtil.isActiveConnection(curSpace, leConn[key].Name))
                    {
                        Global.systemLog.Error("Birst Local Live Access Connection " + key + " is not alive - swap operation not attempted");
                        throw new Exception("warn: Birst Local Live Access Connection " + key + " is not alive - swap operation not attempted");
                    }
                }
            }

            SpaceOperationStatus curSpaceOperation = null;
            SpaceOperationStatus otherSpaceOperation = null;
            try
            {
                int curSpaceOp = SpaceOpsUtils.getSpaceAvailability(curSpace);
                bool curSpaceAvailable = SpaceOpsUtils.isSpaceAvailable(curSpaceOp);
                if (!curSpaceAvailable)
                {
                    string curSpaceUnavailableMsg = SpaceOpsUtils.getAvailabilityMessage(curSpace.Name, curSpaceOp);
                    Global.systemLog.Error(curSpaceUnavailableMsg);
                    throw new SpaceUnavailableException(curSpaceOp, curSpaceUnavailableMsg);
                }

                int otherSpaceOp = SpaceOpsUtils.getSpaceAvailability(otherSpace);
                bool otherSpaceAvailable = SpaceOpsUtils.isSpaceAvailable(otherSpaceOp);
                if (!otherSpaceAvailable)
                {
                    string otherSpaceUnavailableMsg = SpaceOpsUtils.getAvailabilityMessage(otherSpace.Name, otherSpaceOp);
                    Global.systemLog.Error(otherSpaceUnavailableMsg);
                    throw new SpaceUnavailableException(otherSpaceOp, otherSpaceUnavailableMsg);
                }

                if (Util.isSpaceProcessing(curSpace))
                {
                    Global.systemLog.Error("Unable to swap. Space is already processing : " + curSpace.ID);
                    throw new SpaceUnavailableException(SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE, curSpace.Name + " is currently processing");
                }

                if (Util.isSpaceProcessing(otherSpace))
                {
                    Global.systemLog.Error("Unable to swap. Space is already processing : " + otherSpace.ID);
                    throw new SpaceUnavailableException(SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE, otherSpace.Name + " is currently processing");
                }

                curSpaceOperation = new SpaceOperationStatus(curSpace, u);
                curSpaceOperation.logBeginStatus(SpaceOpsUtils.OP_SWAP);                
                otherSpaceOperation = new SpaceOperationStatus(otherSpace, u);
                otherSpaceOperation.logBeginStatus(SpaceOpsUtils.OP_SWAP);

                string originalProdRepository = curSpace.Directory;
                string originalDevRepository = otherSpace.Directory;

                // Swap space schemas
                string curSchema = curSpace.Schema;
                curSpace.Schema = otherSpace.Schema;
                otherSpace.Schema = curSchema;

                // Swap space directories
                string curDirectory = curSpace.Directory;
                curSpace.Directory = otherSpace.Directory;
                otherSpace.Directory = curDirectory;

                // Swap DBLoadDirs
                string curDatabaseLoadDir = curSpace.DatabaseLoadDir;
                curSpace.DatabaseLoadDir = otherSpace.DatabaseLoadDir;
                otherSpace.DatabaseLoadDir = curDatabaseLoadDir;

                // Swap load ids
                int curLoad = curSpace.LoadNumber;
                curSpace.LoadNumber = otherSpace.LoadNumber;
                otherSpace.LoadNumber = curLoad;

                // Swap database connection information
                // - Type, ConnectString, DatabaseDriver, DatabaseType, AdminUser, AdminPwd
                int curType = curSpace.Type;
                String curConnectString = curSpace.ConnectString;
                String curDatabaseDriver = curSpace.DatabaseDriver;
                String curDatabaseType = curSpace.DatabaseType;
                String curAdminUser = curSpace.AdminUser;
                String curAdminPwd = curSpace.AdminPwd;

                // swap space local application directories (XXX is this correct for the new swapspace?)
                if (leConn.Keys.Count > 0)
                {
                    foreach (string key in leConn.Keys)
                    {
                        LiveAccessUtil.swapSpaceDirectories(curSpace, leConn[key].Name, otherSpace.ID.ToString());
                    }
                }

                curSpace.Type = otherSpace.Type;
                curSpace.ConnectString = otherSpace.ConnectString;
                curSpace.DatabaseDriver = otherSpace.DatabaseDriver;
                curSpace.DatabaseType = otherSpace.DatabaseType;
                curSpace.AdminUser = otherSpace.AdminUser;
                curSpace.AdminPwd = otherSpace.AdminPwd;

                otherSpace.Type = curType;
                otherSpace.ConnectString = curConnectString;
                otherSpace.DatabaseDriver = curDatabaseDriver;
                otherSpace.DatabaseType = curDatabaseType;
                otherSpace.AdminUser = curAdminUser;
                otherSpace.AdminPwd = curAdminPwd;

                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();

                    if (alignCreatedPackages)
                    {
                        // make sure created packages are ID-aligned if the swap is done using swapspaceforpackages command
                        alignPackageIDsForSwapAndManageGroups(conn, mainSchema, curSpace.ID, originalProdRepository, otherSpace.ID, originalDevRepository);
                    }
                    // update the space records (swapped schema name, directory, database connection information, and load number)
                    Database.updateSpace(conn, mainSchema, curSpace);
                    Database.updateSpace(conn, mainSchema, otherSpace);
                    

                    // force updating of user/groups xml file (XXX until we get rid of it)
                    Database.updateMappingModifiedDate(conn, mainSchema, curSpace);
                    UserAndGroupUtils.saveUserAndGroupToFile(curSpace);
                    Database.updateMappingModifiedDate(conn, mainSchema, otherSpace);
                    UserAndGroupUtils.saveUserAndGroupToFile(otherSpace);

                    // update publish history
                    Guid tempGuid = new Guid();
                    Database.setPublishesID(conn, mainSchema, curSpace.ID, tempGuid);       // move current space publish records to a temp id
                    Database.setPublishesID(conn, mainSchema, otherSpace.ID, curSpace.ID);  // move other space publish records to current space
                    Database.setPublishesID(conn, mainSchema, tempGuid, otherSpace.ID);     // move temp id to other space id

                    
                    if(!alignCreatedPackages)
                    {
                        // swap created packages - swap done using convention swapspacecontents or UI operation
                        // We move the packages.xml between swapped directories
                        // This ensures that package group association does not break. Package access is limited to groups defined by package access tab.
                        // Since groups/users are not swapped, packages.xml are moved back to the spaces directory to ensure that association is still valid.
                        // Also the child spaces refer to the packages via spID-pkID context which still points to the same packages.xml
                        swapCreatedPackages(curSpace, otherSpace);

                    }
                    // swap child space ids in the package usages for synching child space ids for imported packages usage info
                    Database.swapPackageUsages(conn, mainSchema, curSpace.ID, otherSpace.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }

                // need to invalidate data and dashboard caches
                curSpace.invalidateDataCache();
                otherSpace.invalidateDataCache();
                curSpace.invalidateDashboardCache();
                otherSpace.invalidateDashboardCache();
            }
            finally
            {                   
                if (curSpaceOperation != null)
                {
                    curSpaceOperation.logEndStatus(SpaceOpsUtils.OP_SWAP);
                }
                if (otherSpaceOperation != null)
                {
                    otherSpaceOperation.logEndStatus(SpaceOpsUtils.OP_SWAP);
                }
            }
        }

        private static void alignPackageIDsForSwapAndManageGroups(QueryConnection adminConnection, string mainSchema, 
            Guid prodSpaceID, string prodSpaceDirectory,
            Guid devSpaceID, string devSpaceDirectory)
        {
            string prodPackagesFilePath = Path.Combine(prodSpaceDirectory, "packages.xml");
            AllPackages prodPackages = null;
            AllPackages devPackages = null;
            if (File.Exists(prodPackagesFilePath))
            {
                prodPackages = AllPackages.load(prodSpaceDirectory);
            }

            string devPackagesFilePath = Path.Combine(devSpaceDirectory, "packages.xml");
            if (File.Exists(devPackagesFilePath))
            {
                devPackages = AllPackages.load(devSpaceDirectory);
            }

            Dictionary<Guid, Guid> devToProdAlignPackagsIds = new Dictionary<Guid, Guid>();

            List<Guid> unmatchedProdPackageIds = prodPackages != null ? getPackageIds(prodPackages.Packages) : null;
            List<Guid> unmatchedDevPackageIds = devPackages != null ? getPackageIds(devPackages.Packages) : null;

            if (prodPackages != null && prodPackages.Packages != null && prodPackages.Packages.Length > 0
                && devPackages != null && devPackages.Packages != null && devPackages.Packages.Length > 0)
            {
                PackageDetail[] prodPackagesList = prodPackages.Packages;
                PackageDetail[] devPackagesList = devPackages.Packages;
                foreach (PackageDetail prodPkg in prodPackagesList)
                {
                    foreach (PackageDetail devPkg in devPackagesList)
                    {
                        if (devPkg.Name.Trim() == prodPkg.Name.Trim())
                        {
                            if (!devToProdAlignPackagsIds.ContainsKey(devPkg.ID))
                            {
                                unmatchedProdPackageIds.Remove(prodPkg.ID);
                                unmatchedDevPackageIds.Remove(devPkg.ID);
                                // if the space is a copy then package ids will be same anyways
                                if (devPkg.ID != prodPkg.ID)
                                {
                                    devToProdAlignPackagsIds.Add(devPkg.ID, prodPkg.ID);
                                    devPkg.ID = prodPkg.ID;
                                }
                                break;
                            }
                        }
                    }
                }
            }

            // swap Space Ids in packages.xml
            if (prodPackages != null)
            {
                setPackagesSpaceID(prodPackages.Packages, devSpaceID);
            }
            if (devPackages != null)
            {
                setPackagesSpaceID(devPackages.Packages, prodSpaceID);
            }

            Dictionary<Guid, List<Guid>> spaceToUnmatchedPackages = new Dictionary<Guid, List<Guid>>();
            if (unmatchedProdPackageIds != null && unmatchedProdPackageIds.Count > 0)
            {
                spaceToUnmatchedPackages.Add(prodSpaceID, unmatchedProdPackageIds);
            }

            if (unmatchedDevPackageIds != null && unmatchedDevPackageIds.Count > 0)
            {
                spaceToUnmatchedPackages.Add(devSpaceID, unmatchedDevPackageIds);
            }

            // Although we change the spaceIDs we still save packages.xml in the original directories. 
            // Space Ids will align properly Once the pointers to the space directory are switched in succeeding steps
            string packagesBeforeSwapFileName = "packages.b4swap.xml";
            string packagesInSwapFileName = "packages.inswap.xml";
            string prodPackagesBackupFilePath = Path.Combine(prodSpaceDirectory, packagesBeforeSwapFileName);                        
            string prodpackagesInSwapFilePath = Path.Combine(prodSpaceDirectory, packagesInSwapFileName);
            string devPackagesBackupFilePath = Path.Combine(devSpaceDirectory, packagesBeforeSwapFileName);
            string devpackagesInSwapFilePath = Path.Combine(devSpaceDirectory, packagesInSwapFileName);
            try
            {
                if (prodPackages != null && prodPackages.Packages != null && prodPackages.Packages.Length > 0)
                {
                    if (File.Exists(prodPackagesBackupFilePath))
                    {
                        File.Delete(prodPackagesBackupFilePath);
                    }
                    // Take a backup copy of original prod packages
                    Util.FileCopy(prodPackagesFilePath, prodPackagesBackupFilePath);
                    if (File.Exists(prodpackagesInSwapFilePath))
                    {
                        File.Delete(prodpackagesInSwapFilePath);
                    }
                    // save the new packages.xml to .inswap file name
                    prodPackages.save(prodSpaceDirectory, packagesInSwapFileName);
                    // move the .inswap file name to actual packages.xml
                    File.Delete(prodPackagesFilePath);
                    File.Move(prodpackagesInSwapFilePath, prodPackagesFilePath);
                }

                if (devPackages != null && devPackages.Packages != null && devPackages.Packages.Length > 0)
                {
                    if (File.Exists(devPackagesBackupFilePath))
                    {
                        File.Delete(devPackagesBackupFilePath);
                    }
                    // Take a backup copy of original prod packages
                    Util.FileCopy(devPackagesFilePath, devPackagesBackupFilePath);
                    if (File.Exists(devpackagesInSwapFilePath))
                    {
                        File.Delete(devpackagesInSwapFilePath);
                    }
                    // save the new packages.xml to .inswap file name
                    devPackages.save(devSpaceDirectory, packagesInSwapFileName);
                    // move the .inswap file name to actual packages.xml
                    File.Delete(devPackagesFilePath);
                    File.Move(devpackagesInSwapFilePath, devPackagesFilePath);
                }
                Database.updatePackageGroupsInfoOnSwap(adminConnection, mainSchema, prodSpaceID, devSpaceID, spaceToUnmatchedPackages, devToProdAlignPackagsIds);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to swap new packages file. Try to recover from back up files", ex);
                try
                {
                    // try to recover from backup files
                    if (File.Exists(prodPackagesBackupFilePath))
                    {
                        File.Move(prodPackagesBackupFilePath, prodPackagesFilePath);
                    }

                    if (File.Exists(devPackagesBackupFilePath))
                    {
                        File.Move(devPackagesBackupFilePath, devPackagesFilePath);
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Exception while copying the backup packages file", ex2);
                }
                throw ex;
            }
            finally
            {
                // clean up tmp files created
                if(File.Exists(prodPackagesBackupFilePath))
                {
                    File.Delete(prodPackagesBackupFilePath);
                }

                if (File.Exists(prodpackagesInSwapFilePath))
                {
                    File.Delete(prodpackagesInSwapFilePath);
                }

                if(File.Exists(devPackagesBackupFilePath))
                {
                    File.Delete(devPackagesBackupFilePath);
                }

                if (File.Exists(devpackagesInSwapFilePath))
                {
                    File.Delete(devpackagesInSwapFilePath);
                }
            }
        }

        private static void setPackagesSpaceID(PackageDetail[] packages, Guid spaceID)
        {
            if (packages != null && packages.Length > 0)
            {
                // change Space Ids in prod packages to refer to dev space id
                foreach (PackageDetail pkg in packages)
                {
                    pkg.SpaceID = spaceID;
                }
            }
        }

        private static List<Guid> getPackageIds(PackageDetail[] packages)
        {
            List<Guid> response = null;
            if (packages != null && packages.Length > 0)
            {
                response = new List<Guid>();
                foreach (PackageDetail pkg in packages)
                {
                    response.Add(pkg.ID);
                }
            }
            return response;
        }

        private static void swapCreatedPackages(Space sp1, Space sp2)
        {
            // spaces directories already point to the swapped spaces
            // Need to move back packages.xml 
            string spaceDirectory1 = sp1.Directory;
            string spaceDirectory2 = sp2.Directory;

            if (Directory.Exists(spaceDirectory1) && Directory.Exists(spaceDirectory2))
            {
               // first copy it to tmp file and then move to space directory 
                // take a backup in case recovery is needed
                string packages1FilePath = Path.Combine(spaceDirectory1, "packages.xml");
                string packages1BackUpFilePath = Path.Combine(spaceDirectory1, "packages.xml.bak");
                string packages1TmpFilePath = Path.Combine(spaceDirectory1, "packages.xml.tmp");

                string packages2FilePath = Path.Combine(spaceDirectory2, "packages.xml");
                string packages2BackUpFilePath = Path.Combine(spaceDirectory2, "packages.xml.bak");
                string packages2TmpFilePath = Path.Combine(spaceDirectory2, "packages.xml.tmp");
                try
                {
                    if (File.Exists(packages1BackUpFilePath))
                    {
                        File.Delete(packages1BackUpFilePath);
                    }
                    if (File.Exists(packages2BackUpFilePath))
                    {
                        File.Delete(packages2BackUpFilePath);
                    }

                    if (File.Exists(packages1FilePath))
                    {
                        Util.FileCopy(packages1FilePath, packages1BackUpFilePath, true);
                        File.Move(packages1FilePath, packages1TmpFilePath);
                    }

                    if (File.Exists(packages2FilePath))
                    {
                        Util.FileCopy(packages2FilePath, packages2BackUpFilePath, true);
                        File.Move(packages2FilePath, packages2TmpFilePath);
                    }

                    if (File.Exists(packages1TmpFilePath))
                    {
                        File.Move(packages1TmpFilePath, packages2FilePath);
                    }

                    if (File.Exists(packages2TmpFilePath))
                    {
                        File.Move(packages2TmpFilePath, packages1FilePath);
                    }

                    if (File.Exists(packages1BackUpFilePath))
                    {
                        File.Delete(packages1BackUpFilePath);
                    }
                    if (File.Exists(packages2BackUpFilePath))
                    {
                        File.Delete(packages2BackUpFilePath);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to swap packages.xml file. Try to recover from back up files", ex);
                    try
                    {
                        // try to recover from backup files
                        if (File.Exists(packages1BackUpFilePath))
                        {
                            File.Move(packages1BackUpFilePath, packages1FilePath);
                        }

                        if (File.Exists(packages2BackUpFilePath))
                        {
                            File.Move(packages2BackUpFilePath, packages2FilePath);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Unable to copy the back up packages file", ex2);
                    }
                }
            }
        }


        public static GenericResponse swapWithSpace(HttpSessionState session, string id)
        {
            GenericResponse response = new GenericResponse();
            Space curSpace = (Space)session["space"];
            QueryConnection conn = null;
            Space otherSpace = null;
            try
            {
                conn = ConnectionPool.getConnection();
                otherSpace = Database.getSpace(conn, mainSchema, new Guid(id));
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            User u = (User)session["user"];
            bool newRep;
            MainAdminForm otherMaf = Util.loadRepository(otherSpace, out newRep);
            if (curSpace == null || otherSpace == null || maf == null || otherMaf == null || u == null)
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorMessage = "Unable to swap space content: one of the spaces not found";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                return response;
            }
           
            if (Util.getQueryLanguageVersion(maf) != Util.getQueryLanguageVersion(otherMaf))
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorMessage = "Unable to swap space content. You are attempting to swap two spaces that are incompatible. " + 
                    "Make sure that both spaces use the same version of the logical query language.";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                return response;
            }
            //Now check if only one of the spaces contains any local configuration
            if (!BirstConnectUtil.equalsLocalConnections(curSpace, maf, otherSpace, otherMaf))
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorMessage = "Unable to swap space content. You are attempting to swap two spaces that are incompatible. " +
                    "Make sure both spaces have same birst local configuration(s).";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                return response;
            }

            if (PackageUtils.checkCyclicPackageReference(null, curSpace, maf, otherSpace, otherMaf))
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorMessage = "Unable to swap space content. You are attempting to swap two spaces that are incompatible. " +
                    "Found cyclic reference, atleast one package in your spaces is imported from the space you are swapping with. Please correct this before proceeding with the swap.";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                return response;
            }

            if (!PackageUtils.checkIfCreatedPackagesAlign(session, curSpace, maf, otherSpace, otherMaf))
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorMessage = "Unable to swap space content. You are attempting to swap two spaces that are incompatible. " +
                    "At least one Package in your spaces does not align with the metadata of the space you are swapping with. Please correct this before proceeding with the swap.";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                return response;
            }


            try
            {
                swapSpaces(curSpace, otherSpace, maf, u, false);
            }
            catch (SpaceUnavailableException spEx)
            {
                response.Error = new ErrorOutput();
                response.Error.ErrorType = spEx.getErrorType();
                response.Error.ErrorMessage = "Unable to swap the space contents. " + spEx.getErrorMessage();
                return response;
            }
            catch (Exception ex)
            {
                // swapping the space failed
                response.Error = new ErrorOutput();
                // XXX should really create our own exceptions
                if (ex.Message.Contains("warn:"))
                    response.Error.ErrorMessage = "Unable to swap the space contents.  One of the spaces is currently in use. Please try swap space again. Please contact Birst Customer Support if retrying does not resolve the issue.";
                else
                    response.Error.ErrorMessage = "Unable to swap the space contents.  Please contact Birst Customer Support.";
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                return response;
            }
            Util.setSpace(session, curSpace, u);
            return response;
        }


        public static bool checkSpacePublishability(MainAdminForm maf, Space sp)
        {
            bool publish = true;
            int count = 0;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.LiveAccess)
                    continue;
                count++;
                Publishability pub = SpaceAdminService.checkPublishability(maf, sp, st);
                if (!pub.publishable)
                {
                    Global.systemLog.Warn("Publishability Status : " + pub.message + " : Staging Table : " + st.Name);
                    publish = false;
                    break;
                }
            }
            return (publish && count > 0);
        }

        public static Publishability checkPublishability(MainAdminForm maf, Space sp, StagingTable st)
        {
            Publishability pub = new Publishability();
            if (st.Disabled)
                return (pub);
            /*if (st.Levels == null || st.Levels.Length <= 1)
            {
                if (st.Levels.Length == 0 || (maf.timeDefinition != null && st.Levels[0][0] == maf.timeDefinition.Name))
                {
                    pub.publishable = false;
                    pub.message = "No grain has been defined";
                    return (pub);
                }
            }*/
            int countCols = 0;
            int countKeys = 0;
            pub.publishable = true;
            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.NaturalKey)
                    countKeys++;
                if (sc.TargetTypes != null && sc.TargetTypes.Length > 0)
                    countCols++;
            }
            /*
            if (countKeys == 0)
            {
                pub.publishable = false;
                pub.message = "No keys have been defined";
                return (pub);
            }
             * */

            // For scripted source columns might not have been targeted
            // DayID column gets set during build operation. Hence a valid bypass here.
            if (countCols == 0 && !Util.isScriptedSource(st))
            {
                pub.publishable = false;
                pub.message = "No columns have been defined";
                return (pub);
            }
            SourceFile sf = getSourceFile(maf, st);
            bool exists = false;
            if (sf != null)
            {
                if (sf.S3Bucket != null)
                    exists = AWSUtils.s3ObjectExists(new BasicAWSCredentials(awsAccessKeyId, awsSecretKey), sf);
                else
                    exists = File.Exists(Path.Combine(sp.Directory, "data", sf.FileName));
            }
            if ((sf == null || !exists) && !st.LiveAccess)
            {
                if (st.Script == null || st.Script.InputQuery == null)
                {
                    if (sf == null || sf.SearchPattern == null || sf.SearchPattern.Length == 0 || Directory.GetFiles(Path.Combine(sp.Directory, "data"), sf.SearchPattern).Length == 0)
                    {
                        pub.message = "Source not present";
                        if (st.WebOptional)
                            pub.warning = true;
                        else
                            pub.publishable = false;
                    }
                }
            }
            return (pub);
        }


        private static SourceFile getSourceFile(MainAdminForm maf, StagingTable st)
        {
            SourceFile sf = null;
            foreach (SourceFile ssf in maf.sourceFileMod.getSourceFiles())
            {
                if (st.SourceFile.ToLower() == ssf.FileName.ToLower())
                {
                    sf = ssf;
                    break;
                }
            }
            return (sf);
        }
    }
}