﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class ImportedRepositoryItem
    {
        public ImportedSpace ispace;
        public StagingTable st;
        public SourceFile sf;
        public DimensionTable dt;
        public MeasureTable mt;
        public Variable v;
        public Aggregate aggregate;
        public string displayName;
        public bool hide;
    }
}