﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class AWSRegion
    {
        public int ID;
        public string Name;
        public string TimeBucket;
        public string UploadBucket;
        public bool Active;
    }

    public class AWSRegions : GenericResponse
    {
        public AWSRegion[] SupportedRegions;
    }
}