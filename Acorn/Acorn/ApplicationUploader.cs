﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Text;
using System.Data.Odbc;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using Acorn.Utils;
using Acorn.DBConnection;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.Serialization;

namespace Acorn
{
    public class ApplicationUploader
    {
        public static int MAX_LINES_TO_SCAN = 100000; // set to -1 for all
        private static int MAX_NUM_BASE_MEASURE_TABLES = 25;
        // Name of column based on consolidation source
        private static string CONSOLIDATION_GROUP_COLUMN = "Group";
        public static string ERROR_STR = "Error";
        public static string WARNING_STR = "Warning";
        public static string INFO_STR = "Information";

        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        MainAdminForm maf;
        Space sp;
        string fname;
        bool firstRowHasHeaders;
        User u;
        HttpSessionState session;
        private int beginSkip;
        private int endSkip;
        private bool filterExcelRows;
        private bool preventChanges;
        private string[][] escapes;
        private string encoding;
        private string separator;
        private bool ignoreInternalQuotes;
        private bool forcenumcolumns;
        private string sourceGroups;
        private string loadGroups;
        private string quotechar;
        private bool isSpacePublished;
        private bool ignoreCarriageReturn;
        private bool autoModel;
        public bool isBrowserUpload;
        // added during modification of uploader to be used with flex webservice.
        private int successfullCounts;
        private Dictionary<string, ScanSummaryResult> excelWarningDetails;
        public bool checkDupStagingCols = false;
        private string rserverpath;

        public ApplicationUploader(MainAdminForm maf, Space sp, string fname, bool firstRowHasHeaders, bool preventChanges, bool ignoreInternalQuotes, string[][] escapes,
            bool forcenumcolumns, string quotechar, User u, HttpSessionState session, int beginSkip, int endSkip, bool filterExcelRows, string encoding, string separator, bool autoModel)
        {
            this.maf = maf;
            this.sp = sp;
            this.fname = fname;
            this.firstRowHasHeaders = firstRowHasHeaders;
            this.u = u;
            this.session = session;
            this.beginSkip = beginSkip;
            this.endSkip = endSkip;
            this.filterExcelRows = filterExcelRows;
            this.preventChanges = preventChanges;
            this.escapes = escapes;
            this.encoding = encoding;
            this.separator = separator;
            this.ignoreInternalQuotes = ignoreInternalQuotes;
            this.forcenumcolumns = forcenumcolumns;
            this.quotechar = quotechar;
            this.autoModel = autoModel;
            this.isSpacePublished = Status.hasLoadedData(this.sp, null);
        }

        public string RServerPath
        {
            get
            {
                return rserverpath;
            }
            set
            {
                this.rserverpath = value;
            }
        }

        public string SourceGroups
        {
            get
            {
                return sourceGroups;
            }
            set
            {
                this.sourceGroups = value;
            }
        }

        public string LoadGroups
        {
            get
            {
                return loadGroups;
            }
            set
            {
                this.loadGroups = value;
            }
        }

        public int getSuccessfullCounts()
        {
            return successfullCounts;
        }

        public bool isIgnoreCarriageReturn()
        {
            return ignoreCarriageReturn;
        }

        public void setIgnoreCarriageReturn(bool ignoreCarriageReturn)
        {
            this.ignoreCarriageReturn = ignoreCarriageReturn;
        }

        public Space getSpace()
        {
            return sp;
        }

        public Dictionary<string, ScanSummaryResult> getExcelWarningDetails()
        {
            return excelWarningDetails;
        }
       
        public class UploadFilesInfo
        {
            public Dictionary<string, bool> firstRowHeaders;
            public DataTable errorTable;
            public Dictionary<string, ScanSummaryResult> filenames;
            public Dictionary<string, string> fileNameToBaseFileNameMap; //txt file name to base file name map i.e. Categories.txt to Northwind.xls
            public List<string> files;
        }

        private List<string> expandZipfile(String name, DataTable errorTable, List<string> existingFiles)
        {
            List<string> files = new List<string>();
            FileStream zfs = null;
            ZipInputStream zs = null;
            string path = Path.Combine(sp.Directory, "data");
            string filename = Path.Combine(path, name);
            Util.validateFileLocation(path, filename);
            Global.systemLog.Debug("expandZipfile " + filename);
            try
            {
                // retrieve the meta data information
                Dictionary<string, ZipEntry> zipEntriesByName = new Dictionary<string, ZipEntry>();
                ZipFile zf = null;
                try
                {
                    zf = new ZipFile(filename);
                    foreach (ZipEntry zen in zf)
                    {
                        zipEntriesByName.Add(zen.Name, zen);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Failed to process the zip file catalog: " + ex.Message);
                }
                finally
                {
                    if (zf != null)
                        zf.Close();
                }

                FileInfo fi = new FileInfo(filename);
                zfs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                zs = new ZipInputStream(zfs);
                ZipEntry ze = null;
                while ((ze = zs.GetNextEntry()) != null)
                {
                    // Don't include directories - load all into the 'data' directory
                    if (ze.IsDirectory)
                        continue;
                    if (ze.Name.StartsWith("__MACOSX/")) // ignore MAC resource fork files in Zips
                        continue;
                    string zfname = ze.Name;
                    long zesize = ze.Size;
                    if (zesize == -1) // need to get the size from the cached meta data (streaming zipfiles have the size at the end)
                    {
                        if (zipEntriesByName.ContainsKey(ze.Name))
                        {
                            zesize = zipEntriesByName[ze.Name].Size;
                        }
                    }
                    zfname = zfname.Replace('\\', '_').Replace('/', '_'); // get rid of path separators
                    // Don't expand duplicates
                    if (!files.Contains(zfname) && !existingFiles.Contains(zfname))
                    {
                        FileStream fs = null;
                        string zfilename = Path.Combine(path, zfname);
                        Util.validateFileLocation(path, zfilename);
                        try
                        {
                            Global.systemLog.Debug("Unpacking file: " + zfilename + " (" + zesize + " bytes)");
                            fs = new FileStream(zfilename, FileMode.Create, FileAccess.Write);
                            int size = 2048;
                            long total = 0;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = zs.Read(data, 0, data.Length);
                                total += size;
                                // sanity check, make sure that the file is not bigger than the zip entry size (sadly, files that we create put the sizes at the end..., so we see -1)
                                if (zesize != -1 && total > zesize)
                                {
                                    Global.systemLog.Warn("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                                    throw new Exception("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                                }
                                if (size > 0)
                                {
                                    fs.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            files.Add(zfname);
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Warn("Unable to unpack file: " + zfilename + ", " + ex.ToString());
                            Global.systemLog.Warn("Unable to unzip file: " + filename + ", " + ex.ToString());
                            DataRow dr = errorTable.NewRow();
                            dr[0] = zfname;
                            dr[1] = "Unable to unzip file";
                            errorTable.Rows.Add(dr);
                        }
                        finally
                        {
                        	if (fs != null)
                        		fs.Close();
                        }
                    }
                }
            }
            catch (Exception ex2)
            {
                Global.systemLog.Warn("Unable to unzip file: " + filename + ", " + ex2.ToString());
                DataRow dr = errorTable.NewRow();
                dr[0] = name;
                dr[1] = "Unable to unzip file";
                errorTable.Rows.Add(dr);
            }
            finally
            {
                if (zs != null)
                    zs.Close();
                if (zfs != null)
                    zfs.Close();
            }
            try
            {
                File.Delete(filename);
            }
            catch (IOException ex)
            {
                Global.systemLog.Warn("Unable to delete file: " + filename + ", " + ex.ToString());
            }
            return (files);
        }

        private List<string> processZipFile(String name, DataTable errorTable)
        {
            List<string> zipFiles = new List<string>();
            zipFiles.Add(name);
            List<string> newZipFiles = new List<string>();
            List<string> files = new List<string>();
            int count = 0;
            while (zipFiles.Count > 0 && count < 3)
            {
                List<string> unzippedFiles = new List<string>();
                foreach (string s in zipFiles)
                    unzippedFiles.AddRange(expandZipfile(s, errorTable, files));
                foreach (string s in unzippedFiles)
                {
                    if (s.ToLower().EndsWith(".zip"))
                        newZipFiles.Add(s);
                    else
                        files.Add(s);
                }
                zipFiles = newZipFiles;
                newZipFiles = new List<string>();
                count++;
            }
            return files;
        }

        private bool hasExtension(string[] extensions, string name)
        {
            foreach (string extension in extensions)
            {
                if (name.ToLower().EndsWith(extension))
                    return true;
            }
            return false;
        }

        private bool isValidFileName(string name)
        {
            if (name.IndexOf(".") > 0)//considering file has valid extension
            {
                name = name.Substring(0, name.LastIndexOf("."));
            }
            if (Regex.IsMatch(name, Util.BAD_FILE_NAME_CHARS))
                return false;
            return true;
        }

        public static DataTable getNewErrorTable()
        {
            DataTable errorTable = new DataTable();
            errorTable.Columns.Add("Source");
            errorTable.Columns.Add("Severity");
            errorTable.Columns.Add("Error");
            errorTable.Columns.Add("Object");
            return errorTable;
        }

        public DataTable uploadFile(bool consolidateLikeSources, bool unmapColumnsNotPresent)
        {
            return uploadFile(consolidateLikeSources, false, unmapColumnsNotPresent);
        }

        public DataTable uploadFile(bool consolidateLikeSources, bool uploaderWebserviceCaller, bool unmapColumnsNotPresent)
        {
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);
            DataTable errorTable = null;
            try
            {
                if (session != null)
                    session.Remove("wellformed");
                List<string> files = new List<string>();
                errorTable = getNewErrorTable();

                if (fname.ToLower().EndsWith(".zip"))
                {
                    files.AddRange(processZipFile(fname, errorTable));
                }
                else
                    files.Add(fname);
                Dictionary<string, ScanSummaryResult> filenames = new Dictionary<string, ScanSummaryResult>();
                Dictionary<string, string> fileNameToBaseFileNameMap = new Dictionary<string, string>();
                Dictionary<string, bool> firstRowHeaders = new Dictionary<string, bool>();
                HashSet<string> processedFiles = new HashSet<string>();

                string bad = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BadFileExtensions"];
                string good = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["GoodFileExtensions"];
                string[] badFileExtensions = null;
                string[] goodFileExtensions = null;
                if (bad != null && bad.Length > 0)
                {
                    badFileExtensions = bad.Split(new char[] { ',' });
                }
                if (good != null && good.Length > 0)
                {
                    goodFileExtensions = good.Split(new char[] { ',' });
                    // only one of these should be set, if both, use good since that is more limiting
                    badFileExtensions = null;
                }

                foreach (string name in files)
                {
                    Global.systemLog.Info("Attempting to upload file: " + name);
                    string path = Path.Combine(sp.Directory, "data");
                    string fullname = Path.Combine(path, name);
                    Util.validateFileLocation(path, fullname);
                    // find files that we can process or those we won't process
                    if (goodFileExtensions != null)
                    {
                        if (!hasExtension(goodFileExtensions, name))
                        {
                            Global.systemLog.Info("Skipping uploaded file (not a good extension): " + name);
                            File.Delete(fullname);
                            DataRow dr = errorTable.NewRow();
                            dr[0] = name;
                            dr[1] = WARNING_STR;
                            dr[2] = "File extension is not allowed (allowed extensions are: " + good + ")";
                            errorTable.Rows.Add(dr);
                            continue;
                        }
                    }
                    if (badFileExtensions != null)
                    {
                        if (hasExtension(badFileExtensions, name))
                        {
                            Global.systemLog.Warn("Skipping uploaded file (a bad extension): " + name);
                            File.Delete(fullname);
                            DataRow dr = errorTable.NewRow();
                            dr[0] = name;
                            dr[1] = WARNING_STR;
                            dr[2] = "File extension is not allowed";
                            errorTable.Rows.Add(dr);
                            continue;
                        }
                    }
                    if (!isValidFileName(name))
                    {
                        Global.systemLog.Warn("Skipping uploaded file (a bad file name): " + name);
                        File.Delete(fullname);
                        DataRow dr = errorTable.NewRow();
                        dr[0] = name;
                        dr[1] = ERROR_STR;
                        dr[2] = "File name is not allowed (unsupported character(s): \"" + Util.BAD_FILE_NAME_CHARS_STR + "\")";
                        errorTable.Rows.Add(dr);
                        continue;
                    }

                    if (name.ToLower().EndsWith("xls") || name.ToLower().EndsWith("xlsx"))
                    {
                        string connectStr = null;
                        if (name.EndsWith("xls"))
                            connectStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullname + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                        else
                            connectStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullname + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                        Dictionary<string, ScanSummaryResult> slist = getScanResults(Path.Combine(sp.Directory, "data"), name, connectStr, null, true, errorTable, processedFiles);
                        if (slist != null)
                        {
                            foreach (string s in slist.Keys)
                            {
                                if (!filenames.ContainsKey(s))
                                {
                                    filenames.Add(s, slist[s]);
                                    firstRowHeaders.Add(s, firstRowHasHeaders);
                                }
                                if (!fileNameToBaseFileNameMap.ContainsKey(s))
                                {
                                    fileNameToBaseFileNameMap.Add(s, name);
                                }
                            }
                        }
                        else
                            File.Delete(fullname);

                    }
                    else if (name.ToLower().EndsWith("mdb") || name.ToLower().EndsWith("accdb"))
                    {
                        List<string[]> restrictions = new List<string[]>();
                        string connectStr = null;
                        if (name.ToLower().EndsWith("mdb"))
                            connectStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullname + ";User Id=admin;Password=";
                        else
                            connectStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullname + ";Persist Security Info=False";
                        restrictions.Add(new string[] { "TABLE_TYPE", "TABLE" });
                        Dictionary<string, ScanSummaryResult> slist = getScanResults(Path.Combine(sp.Directory, "data"), name, connectStr, restrictions, true, errorTable, processedFiles);
                        if (slist != null)
                        {
                            foreach (string s in slist.Keys)
                            {
                                if (!filenames.ContainsKey(s))
                                {
                                    filenames.Add(s, slist[s]);
                                    firstRowHeaders.Add(s, true);
                                }
                                if (!fileNameToBaseFileNameMap.ContainsKey(s))
                                {
                                    fileNameToBaseFileNameMap.Add(s, name);
                                }
                            }
                        }
                        else
                            try
                            {
                                File.Delete(fullname);
                            }
                            catch (Exception)
                            {
                                Global.systemLog.Warn("Unable to clean up the temp file in the data folder:" + sp.Directory + "\\data\\" + name);
                            }
                    }
                    else if (name.ToLower().EndsWith("xml"))
                    {
                        string textName = processXMLFile(Path.Combine(sp.Directory, "data"), name, errorTable);
                        if (textName != null)
                        {
                            if (!filenames.ContainsKey(textName))
                            {
                                ScanSummaryResult sr = new ScanSummaryResult();
                                sr.keys = new int[0];
                                filenames.Add(textName, sr);
                                firstRowHeaders.Add(textName, firstRowHasHeaders);
                            }
                            if (!fileNameToBaseFileNameMap.ContainsKey(textName))
                            {
                                fileNameToBaseFileNameMap.Add(textName, name);
                            }
                        }
                        else
                            try
                            {
                                File.Delete(fullname);
                            }
                            catch (Exception)
                            {
                                Global.systemLog.Warn("Unable to clean up the temp file in the data folder:" + sp.Directory + "\\data\\" + name);
                            }
                    }
                    else if (name.ToLower().EndsWith("xsl"))
                    {
                        // save away the XSL file, compile to validate, don't add to any lists
                        validateXSLFile(Path.Combine(sp.Directory, "data"), name, errorTable);
                    }
                    else
                    {
                        if (!filenames.ContainsKey(name))
                        {
                            ScanSummaryResult sr = new ScanSummaryResult();
                            sr.keys = new int[0];
                            filenames.Add(name, sr);
                            firstRowHeaders.Add(name, firstRowHasHeaders);
                        }
                        if (!fileNameToBaseFileNameMap.ContainsKey(name))
                        {
                            fileNameToBaseFileNameMap.Add(name, name);
                        }
                    }
                }

                // see if any of the file names matched with the imported source names
                // remove it from the files to be processed and fill the error table
                if (filenames != null && filenames.Count > 0)
                {
                    List<string> toRemoveFileList = new List<string>();
                    foreach (String toProcessFileName in filenames.Keys)
                    {
                        // filename ends with .txt
                        if (Util.matchesImportedSource(session, maf, toProcessFileName))
                        {   
                            Global.systemLog.Warn("Skipping uploaded file (name collision with an imported source): " + toProcessFileName);
                            toRemoveFileList.Add(toProcessFileName);
                            DataRow dr = errorTable.NewRow();
                            dr[0] = toProcessFileName;
                            dr[1] = ERROR_STR;
                            dr[2] = "An imported source with the same name exists";
                            errorTable.Rows.Add(dr);                            
                            continue;
                        }
                    }
                    
                    if (toRemoveFileList != null && toRemoveFileList.Count > 0)
                    {
                        foreach (string toRemoveFileName in toRemoveFileList)
                        {
                            filenames.Remove(toRemoveFileName);
                            // deletes the file from the directory also
                            try
                            {
                                string dir = Path.Combine(sp.Directory, "data");
                                string fullName = Path.Combine(dir, toRemoveFileName);
                                Util.validateFileLocation(dir, fullName);
                                if (File.Exists(fullName))
                                {
                                    File.Delete(fullName);
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }

                        Global.systemLog.Error("Stopping the upload process due to imported source name collision");
                        return errorTable;
                    }
                }

                UploadFilesInfo ufi = new UploadFilesInfo();
                ufi.firstRowHeaders = firstRowHeaders;
                ufi.errorTable = errorTable;
                ufi.filenames = filenames;
                ufi.fileNameToBaseFileNameMap = fileNameToBaseFileNameMap;
                ufi.files = files;
                bool wellFormed = true;
                foreach (ScanSummaryResult sr in filenames.Values)
                    if (!sr.wellFormed)
                    {
                        wellFormed = false;
                        break;
                    }
                if (session != null && !wellFormed)
                {
                    session["processfileinfo"] = ufi;
                    // incase of webservice call, store filenames in object variable to be retrieved later
                    if (uploaderWebserviceCaller)
                    {
                        excelWarningDetails = filenames;
                    }
                    log4net.ThreadContext.Properties["user"] = u.Username;
                    using (log4net.ThreadContext.Stacks["itemid"].Push(fname))
                    {
                        Global.userEventLog.Info("EXCELWARNING");
                    }
                }
                else
                {
                    processFiles(ufi, consolidateLikeSources, uploaderWebserviceCaller, unmapColumnsNotPresent, autoModel);
                }
            }
            finally
            {
                maf.setInUberTransaction(previousUberTransaction);
            }
            return errorTable;
        }

        /**
         * validate an XSL file
         * - note that C# only supports XSLT 1.0, not 2.0
         */
        private void validateXSLFile(string dir, string name, DataTable errorTable)
        {
            Global.systemLog.Debug("validating '" + name + "'");

            string fname = Path.Combine(dir, name);
            Util.validateFileLocation(dir, fname);

            try
            {
                XslCompiledTransform myXslTrans = new XslCompiledTransform();
                myXslTrans.Load(fname, null, null);   // null for the XmlResolver so that we don't try to pull in any external references
            }
            catch (XsltCompileException ex)
            {
                DataRow dr = errorTable.NewRow();
                dr[0] = name;
                dr[1] = ERROR_STR;
                dr[2] = ex.Message;
                errorTable.Rows.Add(dr);
            }
            return;
        }

        /**
         * process an XML file
         * - transform using an XSL file with the same basename
         * - creates a txt file with the same basename
         * - note that C# only supports XSLT 1.0, not 2.0
         * 
         * - dir is the space directory
         * - name is the name of the xml file
         * - errorTable is used for sending back error messages
         * 
         * - returns the name of the newly created text file or null if there was an error
         */
        private string processXMLFile(string dir, string name, DataTable errorTable)
        {
            Global.systemLog.Debug("processXMLFile with '" + name + "'");

            string fname = Path.Combine(dir, name);
            Util.validateFileLocation(dir, fname);

            string cleanName = name.Substring(0, name.Length - 4); // strip .xml
            string xslName = cleanName + ".xsl";
            string xslFileName = Path.Combine(dir, xslName);
            string textName = cleanName + ".txt";
            string textFileName = Path.Combine(dir, textName);

            // verify that the XSL file exists
            if (!File.Exists(xslFileName))
            {
                Global.systemLog.Info("No XSL file");
                DataRow edr = errorTable.NewRow();
                edr[0] = name;
                edr[1] = ERROR_STR;
                edr[2] = "No XSL file";
                errorTable.Rows.Add(edr);
                return null;
            }

            // do the XSL transformation
            XmlTextWriter writer = null;
            try
            {
                XPathDocument myXPathDoc = new XPathDocument(fname);
                XslCompiledTransform myXslTrans = new XslCompiledTransform(true);
                myXslTrans.Load(xslFileName, null, null);   // null for the XmlResolver so that we don't try to pull in any external references
                writer = new XmlTextWriter(textFileName, Encoding.UTF8);
                myXslTrans.Transform(myXPathDoc, null, writer, null); // for null the XmlResolver so that we don't try to pull in any external references
            }
            catch (Exception ex)
            {
                DataRow dr = errorTable.NewRow();
                dr[0] = name;
                dr[1] = ERROR_STR;
                dr[2] = ex.Message;
                errorTable.Rows.Add(dr);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
            return textName;
        }

        private Dictionary<string, ScanSummaryResult> getScanResults(string dir, string name, string oleDBConnectString, List<string[]> restrictions, bool pruneBlank, DataTable errorTable, HashSet<string> processedFiles)
        {
            //return processOleDBFile(dir, name, oleDBConnectString, restrictions, pruneBlank, errorTable, processedFiles);
            OldedbWrapper oWrapper = new OldedbWrapper(filterExcelRows, sp.Directory, dir, name, oleDBConnectString, restrictions, pruneBlank, errorTable, processedFiles, beginSkip, endSkip);
            oWrapper.sp = sp;
            return oWrapper.process();
        }

        public void processFiles(UploadFilesInfo ufi, bool consolidateLikeSources, bool unmapColumnsNotPresent, bool autoModel)
        {
            processFiles(ufi, consolidateLikeSources, false, unmapColumnsNotPresent, autoModel);
        }

        public void processFiles(UploadFilesInfo ufi, bool consolidateLikeSources, bool uploaderWebserviceCaller, bool unmapColumnsNotPresent, bool autoModel)
        {
            int successCount = ufi.filenames.Count;
            foreach (string fk in ufi.filenames.Keys)
            {
                Global.systemLog.Debug("processing file " + fk);
                string sep = separator;
                int bs = beginSkip;
                int es = endSkip;
                if ((sep == null || sep.Length == 0) && ufi.fileNameToBaseFileNameMap != null)
                {
                    if (ufi.fileNameToBaseFileNameMap.ContainsKey(fk))
                    {
                        string fname = ufi.fileNameToBaseFileNameMap[fk];
                        if (fname.ToLower().EndsWith(".xls") || fname.ToLower().EndsWith(".xlsx") || fname.ToLower().EndsWith(".mdb"))
                        {
                            Global.systemLog.Debug("Force in '|' as separator for file " + fname + "(" + fk + ")" + " in space " + sp.ID.ToString());
                            sep = "|";
                            beginSkip = 0;
                            endSkip = 0;
                        }
                    }
                }
                string result = addFile(maf, sp, fk, ufi.firstRowHeaders[fk], ufi.filenames[fk].keys, ufi.errorTable, uploaderWebserviceCaller, unmapColumnsNotPresent, sep, autoModel);
                // reset begin and end skip to original values - might have changed if excel file.
                beginSkip = bs;
                endSkip = es;
                if (result != null)
                {
                    try
                    {
                        string dir = Path.Combine(sp.Directory, "data");
                        string filename = Path.Combine(dir, fk);
                        Util.validateFileLocation(dir, filename);
                        File.Delete(filename);
                    }
                    catch (Exception)
                    {
                    }
                    DataRow dr = ufi.errorTable.NewRow();
                    dr[0] = fk;
                    dr[1] = ERROR_STR;
                    dr[2] = result;
                    ufi.errorTable.Rows.Add(dr);
                    successCount--;
                }
            }
            if (successCount > 0)
            {
                // settings the counts on the object instance to retreive it separately
                successfullCounts = successCount;
                Util.updateRequiresPublishFlag(maf, true);
                if (consolidateLikeSources)
                    consolidate(session, mainSchema, sp);
                if (mainSchema != null)
                {
                    Util.buildApplication(maf, mainSchema, DateTime.MinValue, -1, null, sp, session);
                    Util.saveApplication(maf, sp, null, u);
                }
                if (ufi.errorTable.Rows.Count == 0)
                {
                    log4net.ThreadContext.Properties["user"] = u.Username;
                    using (log4net.ThreadContext.Stacks["itemid"].Push(fname))
                    {
                        Global.userEventLog.Info("UPLOAD");
                    }
                }
            }
        }

        private bool hasBeenLoaded(DataTable dt, StagingTable st)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if ((string)dr[2] == "LoadStaging" && (!dr.IsNull(3) && (string)dr[3] == "ACORN: " + st.Name) && (int)dr[4] == Status.COMPLETE)
                    return (true);
            }
            return (false);
        }

        private bool consolidate(HttpSessionState session, string mainSchema, Space sp)
        {
            /*
             * See if two sources are the same
             */
            List<StagingTable> tables = maf.stagingTableMod.getStagingTables();
            List<int> deleted = new List<int>();
            bool save = false;
            // Get publish history
            DataTable dt = null;
            Publish lastPublish = null;
            if (session != null)
            {
                dt = Status.getPublishHistory(session, sp, -1, Util.LOAD_GROUP_NAME, true, false, -1);
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    List<Publish> publishes = Database.getPublishes(conn, mainSchema, sp.ID);
                    foreach (Publish spb in publishes)
                        if (lastPublish == null || spb.PublishDate > lastPublish.PublishDate)
                            lastPublish = spb;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            for (int index = 0; index < tables.Count; index++)
            {
                if (deleted.Contains(index))
                    continue;
                StagingTable st = tables[index];
                if (st.Consolidation)
                    continue;
                // Don't consolidate script sources
                if (st.Script != null && st.Script.InputQuery != null && st.Script.InputQuery.Length > 0)
                    continue;
                List<StagingTable> curStList = maf.stagingTableMod.getStagingTables();
                bool found = false;
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if(sf == null){
                    continue;
                }
                string path = Path.Combine(sp.Directory, "data");
                string file = Path.Combine(path, sf.FileName);
                Util.validateFileLocation(path, file);
                if (!File.Exists(file))
                    continue;
                foreach (StagingTable st2 in curStList)
                {
                    if (Object.ReferenceEquals(st, st2))
                        continue;
                    if (!st2.Consolidation)
                        continue;
                    if (st2.Script != null && st2.Script.InputQuery != null && st2.Script.InputQuery.Length > 0)
                        continue;
                    SourceFile sf2 = maf.sourceFileMod.getSourceFile(st2.SourceFile);
                    string file2 = Path.Combine(path, sf2.FileName);
                    Util.validateFileLocation(path, file2);
                    if (!File.Exists(file2))
                        continue;
                    if (!sameDefinition(st, st2, sf, sf2))
                        continue;
                    save = true;
                    sf2.Columns[0].NumDistinctValues++;
                    sf2.NumRows += sf.NumRows;
                    maf.sourceFileMod.removeSourceFile(sf.FileName);
                    maf.stagingTableMod.removeStagingTable(st.Name);
                    if (lastPublish != null && sf2.LastUploadDate < lastPublish.PublishDate)
                    {
                        /* 
                         * Truncate consolidation file if it hasn't been uploaded since last processing. 
                         * If there are headers copy the first row, otherwise start empty.
                        */
                        if (sf2.HasHeaders)
                            truncateFile(sp, sf2);
                        else
                            File.Delete(file2);
                    }
                    addConsolidatedSource(sf, sf2, sf.FileName, false);
                    sf2.FileLength = (new FileInfo(file2)).Length;
                    sf2.LastUploadDate = sf.LastUploadDate;
                    for (int i = 0; i < sf.Columns.Length; i++)
                    {
                        string curname = sf2.Columns[i + 1].Name;
                        string s = getConsolidatedName(sf.Columns[i].Name, sf2.Columns[i + 1].Name, sf2, i + 1);
                        sf2.Columns[i + 1].Name = s;
                        if (curname != s)
                        {
                            st2.Columns[i + 1].Name = s;
                            st2.Columns[i + 1].SourceFileColumn = s;
                        }
                        sf2.Columns[i + 1].Width = Math.Max(sf.Columns[i].Width, sf2.Columns[i + 1].Width);
                        st2.Columns[i + 1].Width = Math.Max(st.Columns[i].Width, st2.Columns[i + 1].Width);
                    }

                    // Remove hierarchies
                    HashSet<string> hnames = new HashSet<string>();
                    getLevels(st.Levels, hnames);
                    foreach (string s in hnames)
                    {
                        Hierarchy h = maf.hmodule.getHierarchy(s);
                        if (h != null)
                            maf.hmodule.removeHierarchy(h);
                    }
                    found = true;
                    break;
                }
                if (!found)
                    foreach (StagingTable st2 in curStList)
                    {
                        if (Object.ReferenceEquals(st, st2))
                            continue;
                        if (st2.Consolidation)
                            continue;
                        if (st2.Script != null && st2.Script.InputQuery != null && st2.Script.InputQuery.Length > 0)
                            continue;
                        // Setup new source file
                        SourceFile sf2 = maf.sourceFileMod.getSourceFile(st2.SourceFile);
                        string file2 = Path.Combine(path, sf2.FileName);
                        Util.validateFileLocation(path, file2);
                        if (!File.Exists(file2))
                            continue;
                        // See if same as another unconsolidated staging table
                        if (!sameDefinition(st, st2, sf, sf2))
                            continue;
                        save = true;
                        if (sf.Encoding != sf2.Encoding)
                            continue;
                        SourceFile newSf = new SourceFile();
                        newSf.FileName = getSourceFileName(getConsolidatedName(sp.Name));
                        if (dt != null)
                        {
                            if (hasBeenLoaded(dt, st))
                            {
                                newSf.FileName = sf.FileName;
                                if (sf.HasHeaders)
                                    truncateFile(sp, sf);
                                else
                                    File.Delete(file);
                            }
                            else if (hasBeenLoaded(dt, st2))
                            {
                                newSf.FileName = sf2.FileName;
                                if (sf2.HasHeaders)
                                    truncateFile(sp, sf2);
                                else
                                    File.Delete(file2);
                            }
                        }
                        newSf.HasHeaders = sf.HasHeaders;
                        newSf.IgnoredLastRows = 0;
                        newSf.IgnoredRows = 0;
                        newSf.LastUploadDate = newSf.FileName != sf.FileName ? sf.LastUploadDate : sf2.LastUploadDate;
                        newSf.NumRows = sf.NumRows + sf2.NumRows + (sf2.HasHeaders ? -1 : 0);
                        newSf.OnlyQuoteAfterSeparator = sf.OnlyQuoteAfterSeparator;
                        newSf.Quote = sf.Quote;
                        newSf.SearchPattern = sf.SearchPattern;
                        newSf.Separator = sf.Separator;
                        newSf.Escapes = sf.Escapes;
                        newSf.FileLength = sf.FileLength + sf2.FileLength;
                        newSf.ForceNumColumns = sf.ForceNumColumns;
                        newSf.FormatType = sf.FormatType;
                        newSf.HasHeaders = sf.HasHeaders;
                        newSf.Encoding = sf.Encoding;
                        newSf.DoNotTreatCarriageReturnAsNewline = sf.DoNotTreatCarriageReturnAsNewline;
                        newSf.Columns = new SourceFile.SourceColumn[sf.Columns.Length + 1];
                        newSf.Columns[0] = new SourceFile.SourceColumn();
                        newSf.Columns[0].Name = CONSOLIDATION_GROUP_COLUMN;
                        newSf.Columns[0].DataType = "Varchar";
                        newSf.Columns[0].Width = Math.Max(80, Math.Max(st.SourceFile.Length, st2.SourceFile.Length));
                        newSf.Columns[0].NumDistinctValues = 2;
                        for (int i = 0; i < sf.Columns.Length; i++)
                        {
                            newSf.Columns[i + 1] = new SourceFile.SourceColumn();
                            if (sf.Columns[i].DataType == "Float" || sf2.Columns[i].DataType == "Float")
                                newSf.Columns[i + 1].DataType = "Float";
                            else
                                newSf.Columns[i + 1].DataType = sf.Columns[i].DataType;
                            newSf.Columns[i + 1].Width = Math.Max(sf.Columns[i].Width, sf2.Columns[i].Width);
                            newSf.Columns[i + 1].NumDistinctValues = sf.Columns[i].NumDistinctValues;
                            newSf.Columns[i + 1].Multiplier = sf.Columns[i].Multiplier;
                            newSf.Columns[i + 1].Format = sf.Columns[i].Format;
                            newSf.Columns[i + 1].Encrypted = sf.Columns[i].Encrypted;
                            newSf.Columns[i + 1].Name = getConsolidatedName(sf.Columns[i].Name, sf2.Columns[i].Name, newSf, i + 1);
                        }
                        maf.sourceFileMod.removeSourceFile(sf.FileName);
                        maf.sourceFileMod.removeSourceFile(sf2.FileName);
                        maf.sourceFileMod.addSourceFile(newSf);
                        maf.stagingTableMod.removeStagingTable(st.Name);
                        maf.stagingTableMod.removeStagingTable(st2.Name);
                        // Remove hierarchies
                        HashSet<string> hnames = new HashSet<string>();
                        getLevels(st.Levels, hnames);
                        getLevels(st2.Levels, hnames);
                        foreach (string s in hnames)
                        {
                            Hierarchy h = maf.hmodule.getHierarchy(s);
                            if (h != null)
                            {
                                maf.hmodule.removeHierarchy(h);
                                // Remove dimension as well
                                Dimension dim = null;
                                foreach (Dimension sdim in maf.dimensionsList)
                                {
                                    if (sdim.Name == h.DimensionName)
                                    {
                                        dim = sdim;
                                        break;
                                    }
                                }
                                if (dim != null)
                                    maf.dimensionsList.Remove(dim);
                            }
                        }
                        bool first = true;
                        // Consolidate files
                        if (newSf.FileName != sf.FileName)
                        {
                            addConsolidatedSource(sf, newSf, sf.FileName, first);
                            first = false;
                        }
                        if (newSf.FileName != sf2.FileName)
                        {
                            addConsolidatedSource(sf2, newSf, sf2.FileName, first);
                        }
                        StagingTable newSt = null;
                        // If keys are different, rescan
                        bool diffKeys = (sf.KeyIndices != null && sf2.KeyIndices != null && sf.KeyIndices.Length != sf2.KeyIndices.Length) ||
                            (sf.KeyIndices != null && sf2.KeyIndices == null) || (sf.KeyIndices == null && sf2.KeyIndices != null);
                        if (!diffKeys && sf.KeyIndices != null && sf2.KeyIndices != null)
                            for (int i = 0; i < sf.KeyIndices.Length; i++)
                            {
                                if (sf.KeyIndices[i] != sf2.KeyIndices[i])
                                {
                                    diffKeys = true;
                                    break;
                                }
                            }
                        newSf.KeyIndices = sf.KeyIndices;
                        if (diffKeys)
                        {
                            // Find possible keys
                            List<int[]> results = maf.sourceFileMod.scanKeys(file, newSf.Separator[0], newSf.IgnoredRows, newSf.IgnoredLastRows, newSf.Separator, newSf.OnlyQuoteAfterSeparator, SourceFiles.getEncoding(newSf), newSf.IgnoreBackslash, newSf.ForceNumColumns, newSf.Columns.Length, newSf.Quote, newSf.HasHeaders, newSf.DoNotTreatCarriageReturnAsNewline);
                            List<int[]> results2 = maf.sourceFileMod.scanKeys(file2, newSf.Separator[0], newSf.IgnoredRows, newSf.IgnoredLastRows, newSf.Separator, newSf.OnlyQuoteAfterSeparator, SourceFiles.getEncoding(newSf), newSf.IgnoreBackslash, newSf.ForceNumColumns, newSf.Columns.Length, newSf.Quote, newSf.HasHeaders, newSf.DoNotTreatCarriageReturnAsNewline);
                            bool foundKey = false;
                            List<int[]> common = new List<int[]>();
                            // Get keys present in both files
                            foreach (int[] key in results)
                            {
                                foreach (int[] key2 in results2)
                                {
                                    if (sameKey(key, key2))
                                    {
                                        common.Add(key);
                                        break;
                                    }
                                }
                            }
                            // Preferentially pick one that is already being used
                            foreach (int[] key in common)
                            {
                                if (sameKey(sf.KeyIndices, key))
                                {
                                    foundKey = true;
                                    newSf.KeyIndices = key;
                                    break;
                                }
                                if (sameKey(sf2.KeyIndices, key))
                                {
                                    foundKey = true;
                                    newSf.KeyIndices = key;
                                    break;
                                }
                            }
                            // If can't take the first
                            if (!foundKey && results.Count > 0)
                                newSf.KeyIndices = results[0];
                        }
                        if (newSf.KeyIndices != null)
                        {
                            int[] copyOfNewSfKeyIndices = (int[])newSf.KeyIndices.Clone();
                            newSf.KeyIndices = new int[copyOfNewSfKeyIndices.Length + 1];
                            newSf.KeyIndices[0] = 0;
                            for (int i = 0; i < copyOfNewSfKeyIndices.Length; i++)
                                newSf.KeyIndices[i + 1] = copyOfNewSfKeyIndices[i] + 1;
                        }
                        else
                            newSf.KeyIndices = new int[] { 0 };
                        newSt = maf.stagingTableMod.addStagingTable(newSf, false, false);
                        // Setup levels, etc.
                        ApplicationUploader.setupStagingTable(maf, newSt, null, false, loadGroups, sourceGroups);
                        ApplicationUploader.setupNewStagingTable(maf, newSt, newSf, sourceGroups, null, null, false);
                        newSt.LoadGroups = new string[] { Util.LOAD_GROUP_NAME };
                        newSt.SourceFile = newSf.FileName;
                        newSt.SourceType = StagingTable.SOURCE_FILE;
                        newSt.Consolidation = true;
                        newSt.Disabled = false;
                        // Don't look at st2 now
                        int dindex = 0;
                        for (; dindex < tables.Count; dindex++)
                        {
                            if (Object.ReferenceEquals(st2, tables[dindex]))
                            {
                                deleted.Add(dindex);
                                break;
                            }
                        }
                        break;
                    }
            }
            return (save);
        }

        public static void truncateFile(Space sp, SourceFile sf)
        {
            string path = Path.Combine(sp.Directory, "data");
            string file1 = Path.Combine(path, sf.FileName);
            string file2 = Path.Combine(path, sf.FileName + ".tmp");
            Util.validateFileLocation(path, file1);
            Util.validateFileLocation(path, file2);

            StreamReader reader = null;
            StreamWriter writer = null;
            try
            {
                reader = new StreamReader(file1);
                writer = new StreamWriter(file2);

                for (int i = 0; i < sf.IgnoredRows + (sf.HasHeaders ? 1 : 0); i++)
                {
                    string line = SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1: 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
                    writer.WriteLine(line);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                }
                catch (Exception)
                { }
                try
                {
                    if (writer != null)
                        writer.Close();
                }
                catch (Exception)
                { }
            }
            File.Delete(file1);
            File.Move(file2, file1);
        }

        private bool sameKey(int[] a, int[] b)
        {
            if (a == null || b == null)
                return false;
            if (a.Length != b.Length)
                return (false);
            for (int i = 0; i < a.Length; i++)
                if (a[i] != b[i])
                    return (false);
            return (true);
        }

        private void getLevels(string[][] levels, HashSet<string> hnames)
        {
            foreach (string[] level in levels)
                if (maf.timeDefinition == null || level[0] != maf.timeDefinition.Name)
                {
                    bool found = false;
                    // Make sure isn't used anywhere else
                    foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
                    {
                        foreach (string[] sl in sst.Levels)
                        {
                            if (sl[0] == level[0])
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                        hnames.Add(level[0]);
                }
        }

        private string getConsolidatedName(string namea, string nameb, SourceFile targetSf, int curIndex)
        {
            string s = getCommonName(namea, nameb);
            if (s != null && s.Length > 0)
            {
                s = s.Replace("  ", " ").Trim();
                int count = 0;
                bool hasName = false;
                string name = null;
                do
                {
                    name = s + (count > 0 ? " " + count.ToString() : "");
                    hasName = false;
                    for (int j = 0; j < curIndex; j++)
                    {
                        if (targetSf.Columns[j] != null && targetSf.Columns[j].Name == name)
                        {
                            hasName = true;
                            break;
                        }
                    }
                    count++;
                } while (hasName);
                return name;
            }
            else
                return nameb;
        }

        private void addConsolidatedSource(SourceFile originalFile, SourceFile consolidatedFile, string name, bool first)
        {
            int lineCount = 0;
            int count = 0;
            StreamReader reader = new StreamReader(sp.Directory + "\\data\\" + originalFile.FileName);
            StreamWriter writer = new StreamWriter(sp.Directory + "\\data\\" + consolidatedFile.FileName, true);
            string line = null;
            if (!first && originalFile.HasHeaders)
                originalFile.IgnoredRows++;
            int index = name.LastIndexOf('.');
            if (index >= 0)
                name = name.Substring(0, index);
            while ((line = SourceFiles.readLine(reader, originalFile.Separator, originalFile.IgnoreBackslash, originalFile.Quote, originalFile.ForceNumColumns ? originalFile.Columns.Length - 1 : 0, originalFile.OnlyQuoteAfterSeparator, originalFile.DoNotTreatCarriageReturnAsNewline)) != null)
            {
                bool skip = false;
                if (originalFile.IgnoredRows > 0 && count < originalFile.IgnoredRows)
                    skip = true;
                else if (originalFile.IgnoredLastRows > 0 && (lineCount - count - 1) < originalFile.IgnoredLastRows)
                    skip = true;
                count++;
                if (skip)
                    continue;
                if (line.Length > 0)
                {
                    if (first && lineCount == 0 && originalFile.HasHeaders)
                        writer.WriteLine(CONSOLIDATION_GROUP_COLUMN + consolidatedFile.Separator + line);
                    else
                        writer.WriteLine(name + consolidatedFile.Separator + line);
                    lineCount++;
                }
            }
            reader.Close();
            writer.Close();
        }

        private string getSourceFileName(string name)
        {
            return (name + ".txt");
        }

        /*
         * Figure out what common string exists with the smallest deletion from either
         */
        private static string getCommonName(string a, string b)
        {
            if (a.Length == b.Length && a == b)
                return (a);
            StringBuilder result = null;
            for (int dlength = 0; dlength < Math.Min(a.Length, b.Length) - 1; dlength++)
            {
                int lengtha = dlength + (a.Length > b.Length ? a.Length - b.Length : 0);
                int lengthb = dlength + (b.Length > a.Length ? b.Length - a.Length : 0);
                int commonlength = Math.Min(b.Length - lengthb, a.Length - lengtha);
                for (int posa = 0; posa < a.Length - lengtha; posa++)
                {
                    for (int posb = 0; posb < b.Length - lengthb; posb++)
                    {
                        // Compare
                        bool eq = true;
                        for (int j = 0; j < commonlength; j++)
                        {
                            int x1 = j <= posa ? j : j + lengtha;
                            int x2 = j <= posb ? j : j + lengthb;
                            if (a[x1] != b[x2])
                            {
                                eq = false;
                                break;
                            }
                        }
                        if (eq)
                        {
                            result = new StringBuilder();
                            for (int j = 0; j < commonlength; j++)
                            {
                                int x1 = (j <= posa) ? j : j + lengtha;
                                result.Append(a[x1]);
                            }
                            // go to nearest word boundary
                            if (lengtha > 0 && lengthb > 0)
                            {
                                int spacestart = posa;
                                int spaceend = posa;
                                for (int i = posa; i >= 0; i--)
                                {
                                    if (result[i] == ' ')
                                    {
                                        spacestart = i;
                                        break;
                                    }
                                }
                                for (int i = posa; i < result.Length; i++)
                                {
                                    if (result[i] == ' ')
                                    {
                                        spaceend = i;
                                        break;
                                    }
                                }
                                if (spacestart != spaceend)
                                    result = result.Remove(spacestart, spaceend - spacestart + 1);
                            }
                            break;
                        }
                    }
                    if (result != null)
                        break;
                }
                if (result != null)
                    break;
            }
            return (result == null ? null : result.ToString());
        }

        private string getConsolidatedName(string name)
        {
            int i = 0;
            string result = null;
            bool found = false;
            do
            {
                result = name + (i > 0 ? i.ToString() : "");
                found = false;
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    if (st.Name == maf.stagingTableMod.getStagingNameFromSourceFileName(getSourceFileName(result), false, maf))
                    {
                        found = true;
                        i++;
                        break;
                    }
            } while (found);
            return (result);
        }

        private int countCols(StagingTable st)
        {
            int result = 0;
            for (int i = 0; i < st.Columns.Length; i++)
            {
                StagingColumn sc = st.Columns[i];
                if (sc.DataType != null && !sc.DataType.StartsWith("Date ID:") &&
                    !(i == st.Columns.Length - 1 && sc.TargetTypes != null &&
                    sc.TargetTypes.Length == 1 && sc.Name == "Day ID" && maf.timeDefinition != null &&
                    Array.IndexOf<string>(sc.TargetTypes, maf.timeDefinition.Name) == 0))
                    result++;
            }
            return (result);
        }

        bool typeMatch(string a, string b)
        {
            if (a == "Float" && (b == "Float" || b == "Integer"))
                return true;
            if (b == "Float" && a == "Integer")
                return true;
            return (a == b);
        }

        private bool sameDefinition(StagingTable a, StagingTable b, SourceFile sfa, SourceFile sfb)
        {
            int offset = b.Consolidation ? 1 : 0;
            int bcols = countCols(b);
            if (countCols(a) + offset != bcols)
                return (false);
            bool namesMatch = true;
            for (int i = 0; i < bcols - offset; i++)
            {
                if (!typeMatch(a.Columns[i].DataType, b.Columns[i + offset].DataType) ||
                    (a.Columns[i].DataType == "Varchar" &&
                    a.Columns[i].Name != b.Columns[i + offset].Name &&
                    !ApplicationLoader.similarWidth(a.Columns[i].Width, b.Columns[i + offset].Width)))
                    return (false);
                if (a.Columns[i].Name != b.Columns[i + offset].Name)
                    namesMatch = false;

            }
            return (namesMatch || a.Columns.Length > 5);
        }

        private static char[] currencyprefixes = new char[] { '$', '£', '€', '¥' };

        private string addFile(MainAdminForm maf, Space sp, string addName, bool firstRowHeaders, int[] keys, DataTable errorTable, bool unmapColumnsNotPresent, bool autoModel)
        {
            return addFile(maf, sp, addName, firstRowHasHeaders, keys, errorTable, false, unmapColumnsNotPresent, separator, autoModel);
        }

        private string addFile(MainAdminForm maf, Space sp, string addName, bool firstRowHeaders, int[] keys, DataTable errorTable, bool uploadWebServiceCaller, 
            bool unmapColumnsNotPresent, string sep, bool autoModel)
        {
            object o = null;
            bool exists = maf.sourceFileMod.sourceFileExists(addName) != null;
            List<SourceFile.SourceColumn> oldColList = new List<SourceFile.SourceColumn>();
            if (exists)
            {
                SourceFile oldsf = maf.sourceFileMod.getSourceFile(addName);
                SourceFile.FileFormatType fmtType = oldsf.FormatType;
                if (fmtType == SourceFile.FileFormatType.FixedWidth) // can only be set in old admin tool, assume we know what we are doing...
                {
                    return null;
                }
                foreach (SourceFile.SourceColumn sfc in oldsf.Columns)
                    oldColList.Add(sfc);
            }
            int ignoredRows = beginSkip;
            int ignoredLastRows = endSkip;
            List<string> errors;
            List<string> warnings;
            List<string> changes;
            string path = Path.Combine(sp.Directory, "data");
            string file = Path.Combine(path, addName);
            Util.validateFileLocation(path, file);
            o = maf.sourceFileMod.importFile(Path.Combine(sp.Directory, "data"), addName, encoding, MAX_LINES_TO_SCAN, true, keys.Length == 0, 
                firstRowHeaders, ignoredRows, ignoredLastRows, out errors, out warnings, out changes, preventChanges, ignoreInternalQuotes,
                escapes, forcenumcolumns, quotechar, isSpacePublished, sp.QueryLanguageVersion, ignoreCarriageReturn, sep, isBrowserUpload, maf.builderVersion);
            if (/* (aspage != null || uploadWebServiceCaller) && */ (errors.Count > 0 || warnings.Count > 0 || changes.Count > 0))
            {
                foreach (string s in errors)
                {
                    DataRow dr = errorTable.NewRow();
                    dr[0] = addName;
                    dr[1] = ERROR_STR;
                    dr[2] = s;
                    errorTable.Rows.Add(dr);
                }
                foreach (string s in warnings)
                {
                    DataRow dr = errorTable.NewRow();
                    dr[0] = addName;
                    dr[1] = WARNING_STR;
                    dr[2] = s;
                    errorTable.Rows.Add(dr);
                }
                foreach (string s in changes)
                {
                    DataRow dr = errorTable.NewRow();
                    dr[0] = addName;
                    dr[1] = INFO_STR;
                    dr[2] = s;
                    errorTable.Rows.Add(dr);
                }
            }
            if (typeof(string).Equals(o.GetType()))
            {
                maf.sourceFileMod.removeSourceFile(addName);
                return ("Unable to load file " + addName + ": " + o.ToString());
            }
            SourceFile sf = (SourceFile)o;
            if (sf.UploadStatus == SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED)
            {
                File.Delete(file);
                return ("Unable to import definition for file " + addName + ". Format is locked.");
            }
            else if (sf.UploadStatus == SourceFile.UPLOAD_ERROR_SOURCE_COLUMN_LOCKED)
            {
                File.Delete(file);
                return ("Unable to import definition for file " + addName + ". Format of one of the columns is locked.");
            }
            else if (sf.UploadStatus == SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED)
            {
                File.Delete(file);
                return ("Unable to import definition for file " + addName + ". Would change existing schema.");
            }

            if (sf.Columns.Length <= 1)
            {
                File.Delete(file);
                return ("Unable to import definition for file " + addName + ". Files must be delimited, contain more than one column and contain data.");
            }
            if (keys.Length > 0)
                sf.KeyIndices = keys;
            int index = sf.FileName.LastIndexOf('\\');
            if (index >= 0)
                sf.FileName = sf.FileName.Substring(index + 1);
            
            // If need to allow for null binding stagingcolumns for the removed sourcefilecolumns
            if (!unmapColumnsNotPresent && sf.SchemaLock && sf.AllowNullBindingRemovedColumns)
                unmapColumnsNotPresent = true;

            StagingTable st = maf.stagingTableMod.addStagingTable(sf, unmapColumnsNotPresent, false, checkDupStagingCols);
            if (rserverpath != null)
                st.RServerPath = rserverpath;
            if (u.isFreeTrialUser)
                st.TruncateOnLoad = true;
            if (sp.DiscoveryMode)
            {
                st.DiscoveryTable = true;
                st.Autoplace = true;
            }
            if (st.Name.Length > 255)
                st.Name = st.Name.Substring(0, 255);
            List<StagingTable> curList = new List<StagingTable>(maf.stagingTableMod.getAllStagingTables());
            if (curList.Contains(st, new IdentityComparer<StagingTable>()))
            {
                SourceFile cursf = null;
                foreach (SourceFile ssf in maf.sourceFileMod.getSourceFiles())
                {
                    if (ssf.FileName.ToLower() == st.SourceFile.ToLower())
                    {
                        cursf = ssf;
                        break;
                    }
                }
                if (cursf.Guid != sf.Guid)
                {
                    maf.sourceFileMod.removeSourceFile(addName);
                    string msg = ("Unable to add the file (" + sf.FileName + ") due to conflict with another data source - an existing source data file ("
                        + cursf.FileName + ") is already associated with the staging table (" + st.Name + ")");
                    Global.systemLog.Warn(msg);
                    return msg;
                }
            }
            setupStagingTable(maf, st, oldColList, exists, loadGroups, sourceGroups);
            bool isAdvancedSpace = !sp.Automatic && !sp.DiscoveryMode;
            if (isAdvancedSpace && !exists)
                st.Disabled = true;
            if (sp.Automatic && !exists)
            {
                /*
                 * If this is automatic mode, then setup a dimension for each source file. Infer the natural key if possible.
                 */
                setupNewStagingTable(maf, st, sf, sourceGroups, null, null, false);
            }
            if (sp.Automatic && exists)
            {
                index = 0;
                foreach (StagingColumn sc in st.Columns)
                {
                    bool found = false;
                    foreach (SourceFile.SourceColumn sfc in oldColList)
                    {
                        if (sc.SourceFileColumn != null && sc.SourceFileColumn == sfc.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        continue;
                    /* 
                     * If this is a new column (found during import), then it will not have a target type set
                     */
                    if (sc.TargetTypes == null || sc.TargetTypes.Length == 0)
                    {
                        if (st.NewColumnTarget == null || st.NewColumnTarget == "" || st.NewColumnTarget == StagingTable.TARGET_COMMON)
                        {
                            Dictionary<string, int> counts = new Dictionary<string, int>();
                            // Target the majority of what is targeted by others, if possible
                            int max = 0;
                            foreach (StagingColumn ssc in st.Columns)
                            {
                                if (ssc.TargetTypes == null)
                                    continue;
                                foreach (string s in ssc.TargetTypes)
                                {
                                    if (s == "Measure" || (maf.timeDefinition != null && s == maf.timeDefinition.Name))
                                        continue;
                                    if (counts.ContainsKey(s))
                                        counts[s] = counts[s] + 1;
                                    else
                                        counts[s] = 1;
                                    if (counts[s] > max)
                                        max = counts[s];
                                }
                            }
                            string hname = null;
                            if (counts.Count > 0)
                            {
                                foreach (string s in counts.Keys)
                                {
                                    if (counts[s] == max)
                                        hname = s;
                                }
                            }
                            if (hname != null)
                            {
                                List<string> ttypes = sc.TargetTypes != null ? new List<string>(sc.TargetTypes) : new List<string>();
                                ttypes.Add(hname);
                                sc.TargetTypes = ttypes.ToArray();
                            }
                            setKey(st, sf, sc, index);
                        }
                        else if (st.NewColumnTarget != null && st.NewColumnTarget != StagingTable.NO_TARGET)
                        {
                            bool foundh = false;
                            foreach (Hierarchy h in maf.hmodule.getHierarchies())
                            {
                                if (h.DimensionName == st.NewColumnTarget)
                                {
                                    foundh = true;
                                    break;
                                }
                            }
                            if (foundh)
                            {
                                List<string> ttypes = sc.TargetTypes != null ? new List<string>(sc.TargetTypes) : new List<string>();
                                ttypes.Add(st.NewColumnTarget);
                                sc.TargetTypes = ttypes.ToArray();
                            }
                        }
                    }
                    index++;
                }
            }
            maf.stagingTableMod.setRepositoryDirectory(sp.Directory);
            Util.updateStagingTable(maf, st, sf, u.Username, !exists && autoModel);
            return (null);
        }

        public static void setupStagingTable(MainAdminForm maf, StagingTable st, List<SourceFile.SourceColumn> oldColList, bool exists, string loadGroups, string sourceGroups)
        {
            int numBaseMeasureTables = ApplicationBuilder.numBaseMeasureTables(maf);
            bool onlyContainsLocalLoadGroup = false;
            if (st.LoadGroups != null && st.LoadGroups.Length == 1 && st.LoadGroups[0].StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
            {
                onlyContainsLocalLoadGroup = true;                
            }
            // Set load group UNLESS the staging table has only local loadgroup OR is marked for local
            if (!onlyContainsLocalLoadGroup && !st.UseBirstLocalForSource)
                st.LoadGroups = loadGroups != null ? loadGroups.Split(new char[] { ',' }) : new string[] { Util.LOAD_GROUP_NAME };
            if (sourceGroups != null)
                st.SourceGroups = sourceGroups;
            string targetDimensionForDiscoverySources = null; 
            if (st.DiscoveryTable)
            {
                targetDimensionForDiscoverySources = ApplicationBuilder.getDisplayName(st);
            }
            bool discoveryTableFirstAnalyzeByDate = false;
            foreach (StagingColumn sc in st.Columns)
            {
                bool found = false;
                if (oldColList != null)
                    foreach (SourceFile.SourceColumn sfc in oldColList)
                    {
                        if (sc.SourceFileColumn != null && sc.SourceFileColumn == sfc.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                if (found)
                    continue;
                // For discovery source, set target types to a default dimension
                if (st.DiscoveryTable && targetDimensionForDiscoverySources != null && sc.SourceFileColumn != null && sc.SourceFileColumn.Length > 0)
                {
                    List<string> ttypes = sc.TargetTypes != null ? new List<string>(sc.TargetTypes) : new List<string>();
                        if (sc.DataType != "Float")
                            ttypes.Add(targetDimensionForDiscoverySources);
                        sc.TargetTypes = ttypes.ToArray();
                }
                // Set all numbers to metrics
                if (sc.DataType == "Number" || sc.DataType == "Integer" || sc.DataType == "Float")
                {
                    // Don't create metrics for time keys
                    if (maf.timeDefinition != null && sc.NaturalKey && Array.IndexOf<string>(sc.TargetTypes, maf.timeDefinition.Name) >= 0)
                    {
                        // Make sure it's a dateID transformation
                        found = false;
                        if (sc.Transformations != null)
                            foreach (Transformation t in sc.Transformations)
                                if (typeof(DateID).Equals(t.GetType()))
                                {
                                    found = true;
                                    break;
                                }
                        if (found)
                            continue;
                    }
                    if (!exists || sc.TargetTypes == null)
                    {
                        List<string> ttlist = new List<string>();
                        if (sc.TargetTypes != null)
                            foreach (string s in sc.TargetTypes)
                                ttlist.Add(s);
                        ttlist.Insert(0, "Measure");
                        sc.TargetTypes = ttlist.ToArray();
                        if (sc.NaturalKey)
                            sc.TargetAggregations = new string[] { "SUM", "AVG", "COUNT", "COUNT DISTINCT", "MIN", "MAX" };
                        else
                            sc.TargetAggregations = new string[] { "SUM", "AVG", "COUNT DISTINCT", "MIN", "MAX" };
                    }
                }
                else if (sc.DataType == "DateTime" || sc.DataType == "Date")
                {
                    //Find out if this date staging column is manually derived i.e. it has no source file column and is based on a transformation
                    bool isManuallyDerivedColumn = false;
                    if ((sc.SourceFileColumn == null || sc.SourceFileColumn == "") && sc.Transformations != null && sc.Transformations.Length > 0)
                    {
                        isManuallyDerivedColumn = true;
                    }
                    if (!isManuallyDerivedColumn && (numBaseMeasureTables < MAX_NUM_BASE_MEASURE_TABLES))
                    {
                        if (!st.DiscoveryTable)
                        {
                            sc.GenerateTimeDimension = true;
                        }
                        else if (st.DiscoveryTable && !discoveryTableFirstAnalyzeByDate)
                        {
                            sc.GenerateTimeDimension = true;
                            discoveryTableFirstAnalyzeByDate = true;
                        }
                    }
                }
            }
        }

        public static bool matchKey(string[] sourcekey1, string[] sourcekey2)
        {
            if (sourcekey1 == null || sourcekey2 == null || sourcekey1.Length != sourcekey2.Length)
                return false;
            int count = 0;
            for (int i = 0; i < sourcekey1.Length; i++)
            {
                bool found = false;
                for (int j = 0; j < sourcekey2.Length; j++)
                {
                    if (sourcekey1[i] == sourcekey2[j])
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    count++;
            }
            return count == sourcekey1.Length;
        }

        /*
         * If this is automatic mode, then setup a dimension for each source file. Infer the natural key if possible.
         */
        public static Hierarchy setupNewStagingTable(MainAdminForm maf, StagingTable st, SourceFile sf, string sourceGroups, string hierarchyName, string levelName, bool onlyNew)
        {
            string hname = hierarchyName != null ? hierarchyName : getHName(maf, st);
            Hierarchy h = null;
            foreach (Hierarchy sh in maf.hmodule.getHierarchies())
            {
                if (sh.Name == hname)
                {
                    h = sh;
                    break;
                }

                // if the user has modified this hierarchy, use it, don't try to fold stuff into it.
                if (sh.ModelWizard == false)
                {
                    List<Level> children = new List<Level>(sh.Children);
                    while (children.Count > 0)
                    {
                        Level child = children[0];
                        children.RemoveAt(0);
                        if (child.Name == hname)
                        {
                            h = sh;
                            break;
                        }
                        if (child.Children != null && child.Children.Length > 0)
                        {
                            children.AddRange(child.Children);
                        }
                    }
                }
                if (h != null)
                    break;
            }
            if (h != null && onlyNew)
                return h;
            if (h != null && (maf.timeDefinition == null || maf.timeDefinition.Name != h.Name))
            {
                // Remove existing hierarchy
                ManageSources.deleteHierarchy(maf, h, null);
            }
            // See if a table with a foreign key relationship with the same key already exists, if so use it
            if (st.ForeignKeys != null && st.SourceKey != null)
            {
                foreach (StagingTable fkst in maf.stagingTableMod.getAllStagingTables())
                {
                    if (fkst == st)
                        continue;
                    if (ForeignKey.findKey(st.ForeignKeys, fkst.Name) == null && ForeignKey.findKey(fkst.ForeignKeys, st.Name) == null)
                        continue;
                    if (matchKey(fkst.SourceKey, st.SourceKey))
                    {
                        h = maf.hmodule.getHierarchy(fkst.HierarchyName);
                        if (h != null)
                        {
                            if (!st.DiscoveryTable)
                            {
                                // Set the level of the source
                                Level l = h.Children[0];
                                st.Levels = new string[][] { new string[] { h.DimensionName, l.Name } };
                            }
                            break;
                        }
                    }
                }
            }
            string sequenceNumberColName = null;
            // Don't allow overwriting of our default time dimension
            if ((maf.timeDefinition == null || maf.timeDefinition.Name != hname) && h == null)
            {
                h = new Hierarchy();
                // If a hierarchy name is supplied, then using the model wizard - mark as such
                if (hierarchyName != null)
                    h.ModelWizard = true;
                // Remove the ST from name
                h.Name = hname;
                h.DimensionName = h.Name;
                h.Children = new Level[0];
                if (sourceGroups != null)
                    h.SourceGroups = sourceGroups;
                ApplicationUploader.addNewHierarchy(maf, h);
                Level l = null;
                if (sf.KeyIndices != null)
                {
                    l = h.Children[0];
                    l.Name = levelName != null ? levelName : h.Name;
                    // Only put keys at a level if they are single-keys
                    l.ColumnNames = sf.KeyIndices.Length == 1 ? new string[sf.KeyIndices.Length] : new string[0];
                    l.HiddenColumns = new bool[sf.KeyIndices.Length];
                    l.Keys = new LevelKey[1];
                    l.Keys[0] = new LevelKey();
                    l.Keys[0].ColumnNames = new string[sf.KeyIndices.Length];
                    for (int i = 0; i < sf.KeyIndices.Length; i++)
                    {
                        if (sf.KeyIndices.Length == 1)
                            l.ColumnNames[i] = st.Columns[sf.KeyIndices[i]].Name;
                        l.Keys[0].ColumnNames[i] = st.Columns[sf.KeyIndices[i]].Name;
                    }
                    h.DimensionKeyLevel = l.Name;
                    if (!st.DiscoveryTable)
                    {
                        st.Levels = new string[][] { new string[] { h.DimensionName, l.Name } };
                    }
                    l.Children = new Level[0];
                    l.Cardinality = sf.NumRows;
                    l.GenerateDimensionTable = true;
                    l.SharedChildren = new string[0];
                }
                /*
                 * If there is no key column (i.e. no column that is distinct for each row), then all columns are key
                 */
                if (sf.KeyIndices == null)
                {
                    l = h.Children[0];
                    l.Name = h.Name;
                    h.DimensionKeyLevel = l.Name;
                    if (!st.DiscoveryTable)
                    {
                        st.Levels = new string[][] { new string[] { h.DimensionName, l.Name } };
                    }
                    l.Children = new Level[0];
                    l.Cardinality = sf.NumRows;
                    l.GenerateDimensionTable = true;
                    l.Keys = new LevelKey[1];
                    l.Keys[0] = new LevelKey();
                    /*
                     * Setup a sequence number - an integer which is a natural key and a level
                     * key which has no source column
                     */
                    StagingColumn sc = new StagingColumn();
                    List<StagingColumn> sclist = new List<StagingColumn>(st.Columns);
                    sclist.Add(sc);
                    st.Columns = sclist.ToArray();
                    sc.NaturalKey = true;
                    sc.DataType = "Integer";
                    if (st.Name.StartsWith("ST_") && st.Name.Length > 3)
                        sc.Name = st.Name.Substring(3) + " Sequence #";
                    else
                        sc.Name = st.Name + " Sequence #";
                    sequenceNumberColName = sc.Name;
                    sc.TargetTypes = new string[] { "Measure" };
                    sc.TargetAggregations = new string[] { "COUNT" };
                    l.ColumnNames = new string[] { sc.Name };
                    l.HiddenColumns = new bool[] { false };
                    l.Keys[0].ColumnNames = new string[] { sc.Name };
                    l.SharedChildren = new string[0];
                }
                maf.hmodule.updateHierarchy(h);
            }
            if (h != null)
            {
                int index = 0;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.SourceFileColumn != null || sc.Name == sequenceNumberColName)
                    {
                        if (sc.DataType != "Float" && !sc.DataType.StartsWith("Date ID: "))
                        {
                            /*
                             * Retarget column (if hierarchies have changed in last update model this is needed)
                             * Don't retarget if the existing targeting is to a hierarchy not created by the model
                             * wizard
                             */ 
                            List<string> ttypes = new List<string>();
                            // Only retain targeting to measure (if there are other dimensions targeted, drop)
                            bool allowRetarget = true;
                            if (sc.TargetTypes != null)
                                foreach (string s in sc.TargetTypes)
                                {
                                    if (s == "Measure")
                                        ttypes.Add(s);
                                    else
                                    {
                                        Hierarchy oldh = maf.hmodule.getHierarchy(s);
                                        if (oldh != null && !oldh.ModelWizard)
                                        {
                                            allowRetarget = false;
                                            break;
                                        }
                                    }
                                }
                            if (allowRetarget)
                            {
                                ttypes.Add(h.Name);
                                sc.TargetTypes = ttypes.ToArray();
                            }
                        }
                        setKey(st, sf, sc, index);
                    }
                    index++;
                }
            }
            return h;
        }

        private static void setKey(StagingTable st, SourceFile sf, StagingColumn sc, int index)
        {
            if (!sc.NaturalKey && sf.KeyIndices != null && Array.IndexOf<int>(sf.KeyIndices, index) >= 0)
            {
                sc.NaturalKey = true;
                // Make sure the key is not turned into a metric other than count
                if (sf.KeyIndices.Length == 1 && st.Columns[sf.KeyIndices[0]].Name == sc.Name)
                {
                    if (Array.IndexOf<string>(sc.TargetTypes, "Measure") < 0)
                    {
                        List<string> ttlist = new List<string>();
                        if (sc.TargetTypes != null)
                            foreach (string s in sc.TargetTypes)
                                ttlist.Add(s);
                        ttlist.Add("Measure");
                        sc.TargetTypes = ttlist.ToArray();
                    }
                    if (sc.DataType == "Float")
                        sc.TargetAggregations = new string[] { "SUM", "AVG", "COUNT", "MIN", "MAX" };
                    else
                        sc.TargetAggregations = new string[] { "COUNT" };
                }
            }
        }

        public static string getHName(MainAdminForm maf, StagingTable st)
        {
            string hname = st.Name.Substring(3);
            if (maf.builderVersion >= Performance_Optimizer_Administration.Repository.I18NLogicalNames && st.LogicalName != null && st.LogicalName != st.Name)
            {
                hname = st.LogicalName;
            }
            if (maf.timeDefinition != null && hname.Equals(maf.timeDefinition.Name, StringComparison.OrdinalIgnoreCase))
            {
                hname += " (File)";
            }
            return (hname);
        }

        public static void addNewHierarchy(MainAdminForm maf, Hierarchy h)
        {
            maf.hmodule.addHierarchy(h);
            // Add dimension key level automatically
            Level l = new Level();
            /*
             * Since this is the dimension key level, make sure it is generated automatically
             */
            l.GenerateDimensionTable = true;
            // Set maximum cardinality for level (simplifying assumption)
            l.Cardinality = Int32.MaxValue - 1;
            l.Name = "Lowest Level";
            l.ColumnNames = new string[0];
            l.SharedChildren = new string[0];
            l.Keys = new LevelKey[0];
            l.Children = new Level[0];
            h.Children = new Level[1] { l };
            h.DimensionKeyLevel = l.Name;
            // Add dimension to application
            maf.addDimension(h.Name);
        }
    }
}
