﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Performance_Optimizer_Administration;
using System.Collections.Generic;

namespace Acorn
{
    public class GoogleAnalyticsSettings
    {
        public string username;
        public string password;
        public DateTime startDate;
        public DateTime endDate;
        public string profileTableId;
        public GoogleAnalyticsProfile profile;

        [XmlIgnore]
        public string Username
        {
            get
            {
                if (username == null || password.Length == 0)
                    return null;
                return Performance_Optimizer_Administration.MainAdminForm.decrypt(username, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
            set
            {
                username = Performance_Optimizer_Administration.MainAdminForm.encrypt(value, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
        }

        [XmlIgnore]
        public string Password
        {
            get
            {
                if (password == null || password.Length == 0)
                    return null;
                return Performance_Optimizer_Administration.MainAdminForm.decrypt(password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
            set
            {
                password = Performance_Optimizer_Administration.MainAdminForm.encrypt(value, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
        }

        public static GoogleAnalyticsSettings getSettings(string dir)
        {
            if (!File.Exists(dir + "\\googleanalytics.xml"))
            {
                return new GoogleAnalyticsSettings();
            }
            TextReader reader = new StreamReader(dir + "\\googleanalytics.xml");
            XmlReader xreader = new XmlTextReader(reader);
            XmlSerializer serializer = new XmlSerializer(typeof(GoogleAnalyticsSettings));
            try
            {
                GoogleAnalyticsSettings settings = (GoogleAnalyticsSettings)serializer.Deserialize(xreader);
                return settings;
            }
            catch (System.InvalidOperationException ex)
            {
                Global.systemLog.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public void saveSettings(string dir)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(dir + "\\googleanalytics.xml");
                XmlSerializer serializer = new XmlSerializer(typeof(GoogleAnalyticsSettings));
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch (Exception e)
            {
                Global.systemLog.Error(e);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public void deleteSettings(string dir)
        {
            try
            {
                if (File.Exists(dir + "\\googleanalytics.xml"))
                    File.Delete(dir + "\\googleanalytics.xml");
            }
            catch (Exception e)
            {
                Global.systemLog.Error(e);
            }
        }

        public void integrateIntoRepository(MainAdminForm maf, string directory)
        {
            MainAdminForm gaf = new MainAdminForm();
            string fileName = Path.Combine(directory, "GA.xml");
            gaf.loadRepositoryFromFile(fileName);
            gaf.setupTree();
            DatabaseConnection gdc = null;
            foreach (DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Name == "GoogleAnalytics")
                {
                    gdc = dc;
                    break;
                }
            }
            if (gdc == null)
            {
                gdc = new DatabaseConnection();
                gdc.Name = "GoogleAnalytics";
                gdc.ID = profileTableId;
                maf.connection.connectionList.Add(gdc);
            }
            gdc.Driver = "GoogleAnalytics";
            gdc.Type = "Google Analytics";
            gdc.UserName = Username;
            gdc.Password = password;
            gdc.ConnectString = "&start-date=" + startDate.ToString("yyyy-MM-dd") + "&end-date=" + endDate.ToString("yyyy-MM-dd");
            foreach (Dimension d in gaf.dimensionsList)
            {
                bool found = false;
                foreach (Dimension sd in maf.dimensionsList)
                {
                    if (sd.Name == d.Name)
                    {
                        found = true;
                    }
                }
                if (!found)
                    maf.dimensionsList.Add(d);
            }
            foreach (Hierarchy h in gaf.hmodule.getHierarchies())
            {
                if (h.AutoGenerated)
                    continue;
                bool found = false;
                foreach (Hierarchy sh in maf.hmodule.getHierarchies())
                {
                    if (h.Name == sh.Name)
                    {
                        found = true;
                    }
                }
                if (!found)
                    maf.hmodule.addHierarchy(h);
            }
            foreach (MeasureTable mt in gaf.measureTablesList)
            {
                bool found = false;
                foreach (MeasureTable smt in maf.measureTablesList)
                {
                    if (smt.TableName == mt.TableName)
                    {
                        found = true;
                    }
                }
                if (!found)
                    maf.measureTablesList.Add(mt);
            }
            foreach (DimensionTable dt in gaf.dimensionTablesList)
            {
                bool found = false;
                foreach (DimensionTable sdt in maf.dimensionTablesList)
                {
                    if (sdt.TableName == dt.TableName)
                    {
                        found = true;
                    }
                }
                if (!found)
                    maf.dimensionTablesList.Add(dt);
            }
            foreach (DataRow dr in gaf.measureColumnsTable.Rows)
            {
                DataRowView[] ldrv = maf.measureColumnNamesView.FindRows((string)dr["ColumnName"]);
                if (ldrv.Length == 0)
                {
                    maf.measureColumnsTable.Rows.Add(dr.ItemArray);
                }
            }
            foreach (DataRow dr in gaf.measureColumnMappings.Rows)
            {
                DataRowView[] ldrv = maf.measureColumnMappingsNamesView.FindRows((string)dr["ColumnName"]);
                if (ldrv.Length == 0)
                {
                    maf.measureColumnMappings.Rows.Add(dr.ItemArray);
                }
            }
            foreach (DataRow dr in gaf.dimensionColumns.Rows)
            {
                DataRowView[] ldrv = maf.dimensionColumnNamesView.FindRows(new object[] { (string)dr["DimensionName"], (string)dr["ColumnName"] });
                if (ldrv.Length == 0)
                {
                    maf.dimensionColumns.Rows.Add(dr.ItemArray);
                }
            }
            foreach (DataRow dr in gaf.dimensionColumnMappings.Rows)
            {
                DataRowView[] ldrv = maf.dimensionColumnMappingsView.FindRows(new object[] { (string)dr["ColumnName"], (string)dr["TableName"] });
                if (ldrv.Length == 0)
                {
                    maf.dimensionColumnMappings.Rows.Add(dr.ItemArray);
                }
            }
            foreach (DataRow dr in gaf.Joins.Rows)
            {
                DataRowView[] ldrv = maf.joinsView.FindRows(new object[] { (string)dr["table1"], (string)dr["table2"] });
                if (ldrv.Length == 0)
                {
                    maf.Joins.Rows.Add(dr.ItemArray);
                }
            }
        }

        public void removeFromRepository(MainAdminForm maf)
        {
            List<MeasureTable> mtRemove = new List<MeasureTable>();
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (mt.TableSource.Connection == "GoogleAnalytics")
                {
                    mtRemove.Add(mt);
                }
            }
            foreach (MeasureTable mt in mtRemove)
                maf.removeMeasureTableDefinition(mt.TableName, true);
            List<DimensionTable> dtRemove = new List<DimensionTable>();
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (dt.TableSource.Connection == "GoogleAnalytics")
                {
                    dtRemove.Add(dt);
                }
            }
            foreach (DimensionTable dt in dtRemove)
                maf.removeDimensionTable(dt.TableName, true);
            int index = 0;
            for(; index < maf.connection.connectionList.Count; index++)
                if (maf.connection.connectionList[index].Name == "GoogleAnalytics")
                {
                    break;
                }
            if (index < maf.connection.connectionList.Count)
                maf.connection.connectionList.RemoveAt(index);
        }
    }
}
