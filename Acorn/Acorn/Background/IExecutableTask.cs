﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using Performance_Optimizer_Administration;
using Acorn.DBConnection;

namespace Acorn.Background
{
    public interface IExecutableTask
    {
        TaskResults ExecuteTask(MainAdminForm maf,
                                Acorn.User u,
                                QueryConnection conn,
                                string schema,
                                Acorn.Space sp,
                                DateTime curTime,
                                Guid id,
                                string parameters);
    }
}
