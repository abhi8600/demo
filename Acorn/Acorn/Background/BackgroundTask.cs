﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;

namespace Acorn.Background
{
    public class BackgroundTask
    {
        private BackgroundModule module;
        private bool timedOut;
        private int timeout; // timeout is in millseconds
        private AutoResetEvent are = new AutoResetEvent(false);

        public BackgroundTask(BackgroundModule module, int timeout)
        {
            this.module = module;
            this.timeout = timeout;
        }

        public void waitUntilFinished()
        {
            are.WaitOne();
        }

        public void finished()
        {
            are.Set();
        }

        public bool TimedOut
        {
            get
            {
                return timedOut;
            }
            set
            {
                timedOut = value;
            }
        }

        public int Timeout
        {
            get
            {
                return timeout;
            }
            set
            {
                timeout = value;
            }
        }

        public BackgroundModule Module
        {
            get
            {
                return module;
            }
            set
            {
                module = value;
            }
        }
    }
}
