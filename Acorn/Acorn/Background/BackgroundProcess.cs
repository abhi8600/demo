﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Acorn.Background
{
    public class BackgroundProcess
    {
        private static BlockingQueue queue = new BlockingQueue();

        public static void StartBackgroundProcess()
        {
            Global.systemLog.Info("Starting background process");
            Thread t = new Thread(new ThreadStart(monitor));
            t.IsBackground = true;
            t.Start();
            t = new Thread(new ThreadStart(pingLocal));
            t.IsBackground = true;
            t.Start();
        }

        private static void monitor()
        {
            while (true)
            {
                BackgroundTask b = (BackgroundTask)queue.Dequeue();
                StartClass sc = new StartClass(b);
                Thread t = new Thread(new ThreadStart(sc.run));
                t.IsBackground = true;
                t.Start();
            }
        }

        /*
         * Self ping to keep the application alive and loaded. Otherwise, if there's no activity IIS can recycle the application and potentially 
         * end this process
         */
        private static void pingLocal()
        {
            int port = 80;
            string temp = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalAdminSitePort"];
            if (temp == null || temp.Length == 0 || !Int32.TryParse(temp, out port))
            {
                Global.systemLog.Warn("LocalAdminSitePort not set or not an integer, self ping disabled");
                return;
            }
            while (true)
            {
                // Ping every 10 minutes
                Thread.Sleep(1000 * 60 * 10);
                try
                {
                    TcpClient client = new TcpClient("localhost", port);
                    NetworkStream ns = client.GetStream();
                    ns.ReadTimeout = 10 * 1000;
                    Stream os = ns;
                    StreamWriter writer = new StreamWriter(os);
                    writer.Write("GET /Login.aspx HTTP/1.1\r\n");
                    writer.Write("Accept: image/gif, image/jpeg, image/pjpeg, application/x-ms-application, application/vnd.ms-xpsdocument, application/xaml+xml, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, */*\r\n");
                    writer.Write("Accept-Language: en-us\r\n");
                    writer.Write("User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.5.21022; .NET CLR 3.5.30729; OfficeLiveConnector.1.3; OfficeLivePatch.0.0; .NET CLR 3.0.30618)\r\n");
                    writer.Write("Host: localhost:" + port + "\r\n");
                    writer.Write("\r\n");
                    writer.Flush();
                    StreamReader reader = new StreamReader(os);
                    string s = reader.ReadLine();
                    if (s == null || !s.Contains("HTTP/1.1 200 OK"))
                    {
                        Global.systemLog.Warn("Unexpected local ping results: " + s + " (expected 'HTTP/1.1 200 OK')");
                    }
                    client.Close();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Unable to self-ping to: " + ex.Message);
                }
            }
        }

        static bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private class StartClass
        {
            BackgroundTask bt;
            public StartClass(BackgroundTask bt)
            {
                this.bt = bt;
            }
            public void run()
            {
                try
                {
                    Thread t = new Thread(new ThreadStart(bt.Module.run));
                    t.IsBackground = true;
                    t.Start();
                    if (bt.Timeout > 0)
                    {
                        bt.TimedOut = !t.Join(bt.Timeout);
                        if (bt.TimedOut)
                        {
                            Global.systemLog.Warn("Timing out Background Task after " + bt.Timeout + " milliseconds");
                            bt.Module.kill();
                            t.Abort();
                        }
                    }
                    else
                        t.Join();
                    bt.Module.finish();
                    bt.finished();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while running the background task " + ex.Message, ex);
                }
            }
        }

        public static void startModule(BackgroundTask bt)
        {
            queue.Enqueue(bt);
        }
    }
}
