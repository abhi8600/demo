﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Acorn.Background
{
    public interface BackgroundModule
    {
        void run();
        void kill();
        void finish();
        void addObserver(BackgroundModuleObserver observer, object o);

		Status.StatusResult getStatus();
    }

    public interface BackgroundModuleObserver
    {
        void killed(object o);
        void finished(object o);
    }
}
