﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class SpaceMembership
    {
        public Space Space;
        public User User;
        public bool Administrator;
        public bool Adhoc;
        public bool Dashboards;
        public DateTime StartDate;
        public String OwnerUsername;
        public bool Owner;

        public static int compareByName(SpaceMembership sm1, SpaceMembership sm2)
        {
            return (sm1.Space.Name.CompareTo(sm2.Space.Name));
        }
    }
}
