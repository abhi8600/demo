﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using Acorn.TrustedService;
using System.Collections.Generic;
using Acorn.Utils;
using System.Net.Mail;
using Acorn.DBConnection;

namespace Acorn
{
    public class ScheduledReport
    {
        public static string MODULE_NAME = "SCHEDULEDREPORT";

        public static int INTERVAL_DAY = 0;
        public static int INTERVAL_WEEK = 1;
        public static int INTERVAL_MONTH = 2;
        // Used for Run Now option
        public static int INTERVAL_ONCE = 3;
        public static int DAY_OF_WEEK_SUNDAY = 0;
        public static int DAY_OF_WEEK_MONDAY = 1;
        public static int DAY_OF_WEEK_TUESDAY = 2;
        public static int DAY_OF_WEEK_WEDNESDAY = 4;
        public static int DAY_OF_WEEK_THURSDAY = 8;
        public static int DAY_OF_WEEK_FRIDAY = 16;
        public static int DAY_OF_WEEK_SATURDAY = 32;

        public string ID;
        public string SpaceID;
        public string User;
        public string ReportPath;
        public int Interval;
        public int DayOfWeek;
        public int DayOfMonth;
        public int Hour; // 24 hour schedule
        public int Minute;
        public string Type;
        public string Subject;
        public string EmailBody;
        public string TriggerReportPath;
        public string[] ToList;
        public string ToReportPath;

        public static TaskResults execute(MainAdminForm maf, User u, QueryConnection conn, string schema, Space sp, DateTime curTime, Guid id, string parameters)
        {
            Global.systemLog.Info("Executing scheduled report: " + id + ", parameters: " + parameters);
            TaskResults tr = new TaskResults();
            // Make sure the user hasn't exceeded their quota
            int reportsDelivered = Database.getDeliveriesLast24Hours(conn, schema, sp.ID);
            ScheduledReport sr = Database.getScheduledReport(conn, schema, id);
            if (reportsDelivered >= u.MaxDeliveredReportsPerDay)
            {
                Global.systemLog.Info("Report quota exceeded");
                tr.Success = false;
                tr.Process = false;
                // Update for next recurrence
                if (sr.Interval == ScheduledReport.INTERVAL_ONCE)
                {
                    Database.deleteScheduledReport(conn, schema, new Guid(sr.ID));
                    return tr;
                }
                Database.updateTaskSchedule(conn, schema, sp.ID, new Guid(sr.ID), ScheduledReport.MODULE_NAME, sr.calcNextTime(), null);
                return tr;
            }
            tr.Success = true;
            tr.Process = false;
            User ru = null;
            if (sr != null)
                ru = Database.getUser(conn, schema, null, new Guid(sr.User), null);
            if (sr != null && ru != null)
            {
                // Default to global if user-level pref does not exist
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, ru.ID, "Locale");
                // Only query for the user-level preference
                IDictionary<string, string> timezonePref = ManagePreferences.getUserPreference(null, ru.ID, "TimeZone");
                string timezone = null;
                if (timezonePref.ContainsKey("TimeZone"))
                {
                    timezone = timezonePref["TimeZone"];
                }

                // Make sure that user and group information is updated.
                UserAndGroupUtils.updatedUserGroupMappingFile(sp);

                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
                // 5 Minute timeout
                ts.Timeout = 5 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                string from = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"];
                int index = sr.ReportPath.LastIndexOf('\\');
                string name = index >= 0 ? sr.ReportPath.Substring(index + 1) : sr.ReportPath;
                string toReportPath = sr.ToReportPath == null ? null : sr.ToReportPath + ".jasper";
                string triggerReportPath = sr.TriggerReportPath == null ? null : sr.TriggerReportPath + ".jasper";
                bool error = false;
                ExecuteResult er = null;
                try
                {
                    string[] tolist = sr.ToList;
                    if (tolist == null)
                    {
                        tolist = new string[] { "" };
                    }
                    SmtpClient smtp = new SmtpClient();
                    er = ts.exportReport(smtp.Host, sp.Directory, sp.ID.ToString(), sr.Type, name, ru.Username, from, sr.Subject, sr.EmailBody,
                        sr.ReportPath + ".JASPER440", tolist, toReportPath, triggerReportPath, u.MaxDeliveredReportsPerDay - reportsDelivered, true, localePref["Locale"], timezone);
                }
                catch (System.Exception e)
                {
                    Global.systemLog.Warn("May not be able to deliver report for space: " + name);
                    string reason = e.GetBaseException().Message;
                    Global.systemLog.Warn("Reason for report failure: " + reason);
                    Global.systemLog.Warn("Exception: " + e + "\n" + e.StackTrace);
                    // Send to user
                    /* Can't send to them because the SMIWeb.TrustedService may have sent an email even though the comm failed on this end
                    Util.sendErrorSummaryEmail(sr.ToList[0], 
                                               "[Problem generating scheduled report] " + sr.Subject,
                                               "scheduled report based on: " + sr.ReportPath.Replace("V{CatalogDir}", ""), 
                                               reason);
                    */
                    // Send email alert to operations if it's anything but a timeout
                    if (e is WebException && !((WebException)e).Status.Equals(WebExceptionStatus.Timeout)) 
                    {
                        error = true;
                        Util.sendErrorDetailEmail("[Problem generating scheduled report] " + sr.Subject,
                                                  "scheduled report based on: " + sr.ReportPath.Replace("V{CatalogDir}", ""),
                                                  "ScheduledReport.execute() raised an exception: " + reason,
                                                  e, sp, null);
                    }
                }

                // Delete the load schedule entry if the report is set to run only once
                if (sr.Interval == ScheduledReport.INTERVAL_ONCE)
                {
                    Database.deleteScheduledReport(conn, schema, new Guid(sr.ID));
                    return tr;
                }
                // Update for next recurrence
                DateTime nextTime;
                if (error)
                {
                    nextTime = sr.calcRetryTime();
                }
                else
                {
                    nextTime = sr.calcNextTime();
                }
                Database.updateTaskSchedule(conn, schema, sp.ID, new Guid(sr.ID), ScheduledReport.MODULE_NAME, nextTime, null);
                Global.systemLog.Info("Updating schedule for scheduled report " + sr.ID + " to time " + nextTime.ToString());
                
                // Log the report delivery total (numer of reports delivered)
                Database.logDelivery(conn, schema, sp.ID, DateTime.Now, sr.ReportPath, er == null ? 0 : (int)er.numInputRows);
            }
            return tr;
        }

        public DateTime calcNextTime()
        {
            DateTime dt = DateTime.Now;
            dt = dt.AddHours(Hour - dt.Hour);
            dt = dt.AddMinutes(Minute - dt.Minute);
            if (Interval == 0)
            {
                while (dt < DateTime.Now)
                    dt = dt.AddDays(1);
            }
            else if (Interval == 1)
            {
                while (!equalDayOfWeek(dt, DayOfWeek) || dt < DateTime.Now)
                    dt = dt.AddDays(1);
            }
            else if (Interval == 2)
            {
                while (dt < DateTime.Now || dt.Day != DayOfMonth)
                {
                    dt = dt.AddDays(1);
                    // If not enough days in the month, take the last day
                    if (dt.Month - DateTime.Now.Month > 1)
                    {
                        dt = dt.AddDays(-1);
                        break;
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// Calculate the retry time based on the report frequency:
        /// If the frequency is 'daily', repeat again in 3 hours
        /// If the frequency is 'weekly' or 'monthly', repeat again in 1 day
        /// </summary>
        /// <returns>an instance of DateTime with the date and time to retry the report</returns>
        public DateTime calcRetryTime()
        {
            string daily = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["DailyScheduledReportRetrySeconds"];
            string weekly = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["WeeklyScheduledReportRetrySeconds"];
            string monthly = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MonthlyScheduledReportRetrySeconds"];
            if (daily == null || weekly == null || monthly == null)
            {
                Global.systemLog.Warn("Scheduled reports' retry parameters are not set in Web.config.");
                Global.systemLog.Warn("Effected scheduled reports will have retry intervals set to 1 day.");
            }
            DateTime dt = DateTime.Now;
            DateTime newDateTime = DateTime.Now.AddDays(1);
            if (Interval == 0 && daily != null)
            {
                newDateTime = dt.AddSeconds(int.Parse(daily));
            }
            else if (Interval == 1 && weekly != null)
            {
                newDateTime = dt.AddSeconds(int.Parse(weekly));
            }
            else if (Interval == 2 && monthly != null)
            {
                newDateTime = dt.AddSeconds(int.Parse(monthly));
            }
            return newDateTime;
        }

        private bool equalDayOfWeek(DateTime dt, int dow)
        {
            return ((dt.DayOfWeek == System.DayOfWeek.Sunday && dow == 0) ||
                (dt.DayOfWeek == System.DayOfWeek.Monday && dow == 1) ||
                (dt.DayOfWeek == System.DayOfWeek.Tuesday && dow == 2) ||
                (dt.DayOfWeek == System.DayOfWeek.Wednesday && dow == 4) ||
                (dt.DayOfWeek == System.DayOfWeek.Thursday && dow == 8) ||
                (dt.DayOfWeek == System.DayOfWeek.Friday && dow == 16) ||
                (dt.DayOfWeek == System.DayOfWeek.Saturday && dow == 32));
        }

    }
}
