﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Diagnostics;
using System.Web.SessionState;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Threading;
using System.Collections.Generic;
using Acorn.Background;
using System.IO;
using System.Xml;
using System.Management;
using Acorn.DBConnection;
using System.Text;

namespace Acorn
{
    /*
     * Monitor the process spun off to do the load. If it terminates, wait. If it does not
     * change the session status to be something other than running, then the process must
     * have terminated unexpectedly - in which case, set the load as having failed.
     */
    public class MonitorLoad : BackgroundModule
    {
        public ProcessStartInfo[] psiArray;
        private Process[] processes;
        private Thread[] processMonitors;
        public HttpSessionState session;
        public Space sp;
        private int LoadNumber;
        public MainAdminForm maf;
        public ScanBuffer scanBuff;
        public User u;
        public Status.StatusResult sr = null;
        bool createOverviewDashboard;
        BuildDashboards bd;
        string loadGroup;
        string mainSchema;
        private bool killed = false;
        private Dictionary<BackgroundModuleObserver, object> observers;
        private bool needToUpdateStatusOnKill = false;

        public MonitorLoad(bool createOverviewDashboard, BuildDashboards bd, int loadNumber, string loadGroup, string mainSchema)
        {
            this.createOverviewDashboard = createOverviewDashboard;
            this.bd = bd;
            this.LoadNumber = loadNumber;
            this.loadGroup = loadGroup;
            this.mainSchema = mainSchema;
            observers = new Dictionary<BackgroundModuleObserver, object>();
        }

        public void addObserver(BackgroundModuleObserver bmo, object o) 
        {
            observers.Add(bmo, o);
        }

        public void run()
        {
            try
            {
                if (u != null)
                {
                    log4net.ThreadContext.Properties["user"] = u.Username;
                    Util.setLogging(u, sp);
                }

                if (createOverviewDashboard)
                {
                    Status.setBuildDashboards(sp, Status.RUNNING);
                    // Build automatic dashboards
                    if (bd == null)
                        bd = new BuildDashboards(maf, sp.Directory + "\\data");
                    bd.build(sp.Directory, scanBuff);
                    // Save the application
                    Util.saveApplication(maf, sp, null, u);
                    Status.setBuildDashboards(sp, Status.COMPLETE);
                }
                if (killed)
                    return;
                string loadInfo = ProcessHandler.INFO_LOAD_ID_KEY + "=" + LoadNumber + "," + ProcessHandler.INFO_LOAD_GROUP_KEY + "=" + loadGroup;
                if (psiArray.Length == 1)
                {
                    // Start the process
                    ProcessStartInfo psi = psiArray[0];
                    Global.systemLog.Debug("Starting: " + psi.FileName + " " + psi.Arguments);
                    DateTime startTime = DateTime.Now;
                    Process proc = Process.Start(psi);
                    processes = new Process[]{proc};
                    Database.addProcessingServer(mainSchema, sp.ID, Util.RUNNING_TYPE_LOAD, Util.getServerIP(), Util.getServerPort(), "Manual", ProcessHandler.getPidsString(proc.Id.ToString(), true), loadInfo);
                    string line = null;
                    while ((line = proc.StandardError.ReadLine()) != null)
                    {
                        Global.systemLog.Warn(line);
                    }
                    while ((line = proc.StandardOutput.ReadLine()) != null)
                    {
                        Global.systemLog.Info(line);
                    }
                    if (proc.HasExited)
                    {
                        Global.systemLog.Info("Publishing space (" + sp.ID + ") - Exited with code: " + proc.ExitCode);
                        if (File.Exists(sp.Directory + "\\publish.lock"))
                            File.Delete(sp.Directory + "\\publish.lock");
                        Util.deleteLoadLockFile(sp);
                    }
                    else
                    {
                        Global.systemLog.Info("Publishing space (" + sp.ID + ") - Running");
                        proc.WaitForExit();
                        Global.systemLog.Info("Load process finished: " + sp.ID);
                    }


                    if (proc.ExitCode == 0)
                    {
                        Util.updateRequiresPublishFlag(maf, false);
                        Util.saveApplication(maf, sp, null, u);
                    }
                    proc.Close();
                }
                else
                {
                    // Start the processes
                    processes = new Process[psiArray.Length];
                    processMonitors = new Thread[psiArray.Length];
                    StringBuilder pids = new StringBuilder();
                    for (int i = 0; i < psiArray.Length; i++)
                    {
                        Global.systemLog.Debug("Starting: " + psiArray[i].FileName + " " + psiArray[i].Arguments);
                        processes[i] = Process.Start(psiArray[i]);
                        if (i > 0)
                            pids.Append(",");
                        pids.Append(ProcessHandler.getPidsString(processes[i].Id.ToString(), true));
                        ProcessMonitor pm = new ProcessMonitor();
                        pm.proc = processes[i];
                        pm.processThreadID = (i < processes.Length - 1 ? "LoaderProcess-" + i : "PollProcess-" + i);
                        Thread t = new Thread(pm.monitorProcess);
                        processMonitors[i] = t;
                        t.Start();
                    }                    
                    Database.addProcessingServer(mainSchema, sp.ID, Util.RUNNING_TYPE_LOAD, Util.getServerIP(), Util.getServerPort(), "Manual", pids.ToString(), loadInfo);
                    // Monitor the processes
                    for (int i = 0; i < processMonitors.Length; i++)
                    {
                        processMonitors[i].Join();
                    }

                    int exitCode = 0;
                    for (int i = 0; i < processes.Length; i++)
                    {
                        Global.systemLog.Info("[" + (i < processes.Length - 1 ? "LoaderProcess-" + i : "PollProcess-" + i) + "] Publishing space (" + sp.ID.ToString() + ") - Exited with code: " + exitCode);
                        if (processes[i].ExitCode != 0)
                        {
                            exitCode = processes[i].ExitCode;                        
                        }
                    }
                
                    if (File.Exists(sp.Directory + "\\publish.lock"))
                        File.Delete(sp.Directory + "\\publish.lock");
                    Util.deleteLoadLockFile(sp);
                    if (exitCode == 0)
                    {
                        Util.updateRequiresPublishFlag(maf, false);
                        Util.saveApplication(maf, sp, null, u);
                    }
                    for (int i = 0; i < processes.Length; i++)
                    {
                        processes[i].Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
        }

        private class ProcessMonitor
        {
            public Process proc;
            public string processThreadID;
            public void monitorProcess()
            {
                string line = null;
                while ((line = proc.StandardError.ReadLine()) != null)
                {
                    Global.systemLog.Warn("[" + processThreadID + "]" + line);
                }
                while ((line = proc.StandardOutput.ReadLine()) != null)
                {
                    Global.systemLog.Info("[" + processThreadID + "]" + line);
                }
                if (!proc.HasExited)
                {
                    proc.WaitForExit();                        
                }                
            }
        }

        public void kill()
        {
            try
            {   
                if (needToUpdateStatusOnKill)
                {
                    killed = updateStatusOnKill();
                }
                else
                {
                    ProcessHandler.killProcess(sp);
                }
                Global.systemLog.Debug("Finished with killing load process for space: " + sp.ID.ToString());
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }

            try
            {
                if (File.Exists(sp.Directory + "\\publish.lock"))
                    File.Delete(sp.Directory + "\\publish.lock");
                Util.deleteLoadLockFile(sp);
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.killed(kvp.Value);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during MonitorLoad.kill", ex);
            }
        }

        public bool updateStatusOnKill()
        {
            bool updateSuccessful = true;
            try
            {
                sr = Status.getLoadStatus(session, sp, LoadNumber, loadGroup);
                if (Status.isRunningCode(sr.code) || (sp.LoadNumber == LoadNumber && (sr.code == Status.StatusCode.None || sr.code == Status.StatusCode.BuildingDashboards)))
                {
                    Status.setLoadFailed(sp, LoadNumber, loadGroup, true);
                    Database.removeProcessingServerForSpace(sp.ID, Util.RUNNING_TYPE_LOAD);
                }
            }
            catch (Exception ex)
            {
                updateSuccessful = false;
                Global.systemLog.Error("Exception during MonitorLoad.updateStatusOnKill", ex);
            }
            return updateSuccessful;
        }

        public void finish()
        {
            bool removeRunningProcessInfo = true;
            try
            {
                Global.systemLog.Info("Finishing load for space: " + (sp != null ? sp.ID.ToString() : null));
                sr = Status.getLoadStatus(session, sp, LoadNumber, loadGroup);
                Global.systemLog.Info("Status of finished load for space: " + sp.ID + " - " + sr.code);

                if (Status.isRunningCode(sr.code) || (sp.LoadNumber == LoadNumber && (sr.code == Status.StatusCode.None || sr.code == Status.StatusCode.BuildingDashboards)))
                {
                    removeRunningProcessInfo = Status.setLoadFailed(sp, LoadNumber, loadGroup, true);
                    Global.systemLog.Error("Load timed out for space (note that this could be because load_warehouse.bat failed to run): " + (sp != null ? sp.ID.ToString() : null));
                    sr = new Status.StatusResult(Status.StatusCode.Failed);
                    if (session != null)
                        session["status"] = sr;
                }
                else
                {
                    // Once it is done, do the housekeeping (redundant code, need to seperate out of the setRunning
                    QueryConnection adminConnection = null;
                    try
                    {
                        adminConnection = ConnectionPool.getConnection();
                        if (sr.code == Status.StatusCode.Complete)
                        {
                            ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, adminConnection, sr, LoadNumber - 1, loadGroup);
                            Util.setDesignerAndDashboardViewablePermissions(session);
                        }
                        else if (sr.code == Status.StatusCode.Failed)
                        {
                            Global.systemLog.Error("Load failed, space: " + sp != null ? sp.ID.ToString() : null);
                            // Make sure at this point there is a loadwarehouse entry 3 for this loadnumber, if not put one
                            removeRunningProcessInfo = Status.setLoadFailed(sp, LoadNumber, loadGroup, true);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(adminConnection);
                    }
                }
                if (File.Exists(sp.Directory + "\\publish.lock"))
                    File.Delete(sp.Directory + "\\publish.lock");
                Util.deleteLoadLockFile(sp);
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }
            catch (Exception ex)
            {
                removeRunningProcessInfo = false;
                Global.systemLog.Error("Exception during MonitorLoad.finish()", ex);
            }
            finally
            {
                if (removeRunningProcessInfo)
                {
                    try
                    {
                        ApplicationLoader.clearBackgroundModule(sp.ID.ToString());
                        Database.removeProcessingServerForSpace(sp.ID, Util.RUNNING_TYPE_LOAD);
                    }
                    catch(Exception ex)
                    {
                        Global.systemLog.Warn("Exception while deleting processing server: ", ex);
                    }
                }
                needToUpdateStatusOnKill = !removeRunningProcessInfo;
            }
        }

        private void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                if (!proc.HasExited)
                {
                    proc.Kill();
                    proc.WaitForExit();
                    proc.Close();
                }
            }
            catch (ArgumentException)
            { /* process already exited */ }
        }

        public bool isProcessKilled()
        {
            return killed;
        }   

        #region BackgroundModule Members


        public Status.StatusResult getStatus()
        {
			return sr;
        }

        #endregion
    }
}
