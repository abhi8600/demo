﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Acorn
{
    public class Template
    {
        public Guid ID;
        public Guid Creator_ID;
        public string Name;
        public string Category;
        public string Comments;
        public int NumUses;
        public double AvgRating;
        public DateTime CreateDate;
        public bool Automatic;
        public bool Private;
        public List<string> Attachments;

        public Template()
        {
        }

        public Template(Guid Creator_ID, string Name, string Category)
        {
            this.Creator_ID = Creator_ID;
            ID = Guid.NewGuid();
            this.Name = Name;
            this.Category = Category;
            this.CreateDate = DateTime.Now;
        }
    }
}
