﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net.Mail;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Security.Cryptography;
using Acorn.Utils;
using System.Diagnostics;
using System.Threading;
using Acorn.Exceptions;
using Acorn.DBConnection;
using System.Web.Services.Protocols;
using System.Xml;

namespace Acorn
{
    public class Command
    {
        private static string validUsernameMask = "[a-zA-Z0-9_\\.\u0080-\uFFCF\\-@]";
        private static string validUnknownMask = "[a-zA-Z0-9_\\.\u0080-\uFFCF\\-@\\$#%\\*&:;\\/<>,=\\+|]";
        public static string newUserProductIDs = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["CommandNewUserProductIDs"];
        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        public static string auditTableName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AuditTableName"];
        public static int NO_ERROR = 0;
        public static int GENERAL_ERROR = 1;
        List<string> log;
        Space sp;
        User u;
        HttpRequest request;
        HttpServerUtility server;
        System.Web.SessionState.HttpSessionState session;
        MainAdminForm maf;
        // flag used in case the logged in session 
        // holding the space object needs to be refreshed
        bool refreshLoggedInSpace = false;
        public int exitStatus { get; set; }

        public Command(Space sp, User u, List<string> log, HttpRequest request, HttpServerUtility server, System.Web.SessionState.HttpSessionState session, MainAdminForm maf)
        {
            this.sp = sp;
            this.u = u;
            this.log = log;
            this.server = server;
            this.request = request;
            this.session = session;
            this.maf = maf;
        }

        public static bool isValidUsername(string uname)
        {
            RegexStringValidator validator = new RegexStringValidator(validUsernameMask);
            return validate(uname, validator);
        }

        private static bool validate(string s, RegexStringValidator validator)
        {
            try
            {
                validator.Validate(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool isValidUnknown(string mask)
        {
            RegexStringValidator validator = new RegexStringValidator(validUnknownMask);
            return validate(mask, validator);
        }

        public bool isRefreshSpaceRequired()
        {
            return refreshLoggedInSpace;
        }

        public void setRefreshLoggedInSpace(bool flag)
        {
            this.refreshLoggedInSpace = flag;
        }

        public static bool isValidEmailAddress(User u, string uname)
        {
            List<string> masklist = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                masklist = Database.getAllowedUsernameMasks(conn, mainSchema, u.ID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            foreach (string mask in masklist)
            {
                try
                {
                    RegexStringValidator validator = new RegexStringValidator(mask);
                    try
                    {
                        validator.Validate(uname);
                        return true;
                    }
                    catch (Exception e)
                    {
                        Global.systemLog.Warn(e.InnerException.Message);
                    }
                }
                catch (Exception)
                {
                    Global.systemLog.Warn("Invalid create email mask in the database - " + mask);
                }
            }
            return false;
        }

        public static List<string> gettokens(string line)
        {
            List<string> tlist = new List<string>();
            StringBuilder curToken = new StringBuilder();
            bool quote = false;
            for (int i = 0; i < line.Length; i++)
            {
                if (!quote && (line[i] == ' ' || line[i] == '\t'))
                {
                    if (curToken.Length >= 0)
                    {
                        tlist.Add(curToken.ToString());
                        curToken = new StringBuilder();
                    }
                }
                else if (line[i] == '\"')
                {
                    quote = !quote;
                }
                else
                    curToken.Append(line[i]);
            }
            if (curToken.Length >= 0)
                tlist.Add(curToken.ToString());
            return tlist;
        }

        private void swapSpaces(string callerCommand, string space1Name, string space2Name, bool alignCreatedPackages)
        {
            if (space1Name == space2Name)
            {
                log.Add("The two space names must be different");
                Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " (the spaces names are the same)");
                this.exitStatus = GENERAL_ERROR;
                return;
            }
            Space sp1 = null;
            Space sp2 = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, false);
                foreach (SpaceMembership sm in smlist)
                {
                    if (sm.Space.Name == space1Name)
                        sp1 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                    else if (sm.Space.Name == space2Name)
                        sp2 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                }
                if (sp1 == null || sp2 == null)
                {
                    log.Add("Unable to find spaces to swap");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " (one or both spaces do not exist)");
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }

                if (!Database.isSpaceAdmin(conn, mainSchema, u, sp1))
                {
                    log.Add("User is not an adminstrator of the space");
                    Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }

                if (!Database.isSpaceAdmin(conn, mainSchema, u, sp2))
                {
                    log.Add("User is not an adminstrator of the space");
                    Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            bool newRep;
            MainAdminForm maf1 = Util.loadRepository(sp1, out newRep);
            MainAdminForm maf2 = Util.loadRepository(sp2, out newRep);
            if (maf1 == null || maf2 == null)
            {
                log.Add("Unable to find spaces to swap");
                Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " (one or both repositories do not exist)");
                this.exitStatus = GENERAL_ERROR;
                return;
            }

            try
            {
                if (!BirstConnectUtil.equalsLocalConnections(sp1, maf1, sp2, maf2))
                {
                    log.Add("Unable to swap space content. You are attempting to swap two spaces that are incompatible. Make sure both spaces have same birst local configuration(s).");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " (incompatible spaces for swap operation)");
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }

                if (PackageUtils.checkCyclicPackageReference(null, sp1, maf1, sp2, maf2))
                {
                    log.Add("Unable to swap space content. You are attempting to swap two spaces that are incompatible. Found cyclic reference, atleast one package in your spaces is imported from the space you are swapping with. Please correct this before proceeding with the swap.");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " Found cyclic reference, atleast one package in your spaces is imported from the space you are swapping with.");
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }

                if (!alignCreatedPackages && !PackageUtils.checkIfCreatedPackagesAlign(null, sp1, maf1, sp2, maf2))
                {
                    log.Add("Unable to swap space content. You are attempting to swap two spaces that are incompatible. At least one Package in your spaces does not align with the metadata of the space you are swapping with. Please correct this before proceeding with the swap.");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " failed to swap spaces " + space1Name + " and " + space2Name + " At least one Package in your spaces does not align with the metadata of the space you are swapping with.");
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }

                SpaceAdminService.swapSpaces(sp1, sp2, maf1, u, alignCreatedPackages);
                // After successful swap, point the current logged in session space to the updated one
                if (sp.ID.ToString() == sp1.ID.ToString() || sp.ID.ToString() == sp2.ID.ToString())
                {
                    setRefreshLoggedInSpace(true);
                }
            }
            catch (SpaceUnavailableException spEx)
            {

                log.Add("Spaces not swapped. " + spEx.getErrorMessage());
                Global.systemLog.Error(callerCommand + ": User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + " (one of the spaces is currently in use) " + spEx.getErrorMessage());
                this.exitStatus = GENERAL_ERROR;
                return;
            }
            catch (Exception ex)
            {
                // XXX should really create our own exceptions
                if (ex.Message.Contains("warn:"))
                {
                    log.Add("Spaces not swapped: " + sp1.Name + ", " + sp2.Name + " (one of the spaces is currently in use). Please try swap space again. Please contact Birst Customer Support if retrying does not resolve the issue.");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + " (one of the spaces is currently in use)");
                }
                else
                {
                    log.Add("Spaces not swapped: " + sp1.Name + ", " + sp2.Name + ". Please contact Birst Customer Support.");
                    Global.systemLog.Info(callerCommand + ": User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". Please contact Birst Customer Support.");
                }
                Global.systemLog.Error(ex.Message, ex);
                this.exitStatus = GENERAL_ERROR;
                return;
            }
            log.Add("Spaces swapped: " + sp1.Name + ", " + sp2.Name);
            Global.systemLog.Info(callerCommand + ": User " + u.Username + " just swapped spaces " + sp1.Name + " and " + sp2.Name);
            return;
        }

        public void processCommand(string line)
        {
            try
            {
                List<string> tokens = gettokens(line);
                if (tokens.Count == 0)
                    return;
                string cname = tokens[0].ToLower();
                log.Add("Executing command: " + line);
                // Global.systemLog.Debug("Executing command: " + line); // security issue when using createuser, setuserpassword, or setspacessopassword
                if (cname == "help")
                {
                    log.Add("Command reference: square brackets [] signify arguments and values (they are not delimiters, do not include [] in the command); items enclosed in braces {} are optional (do not include the braces when specifying the optional arguments), arguments with spaces should be enclosed with double quotes (\"test space\")");
                    log.Add("");
                    log.Add("help - Listing of commands");

                    log.Add("\n-- space operations --");
                    if (u.OperationsFlag)
                    {
                        log.Add("setspaceowner [spaceID] [userName] - set the owner of the space (operations only)");
                    }
                    log.Add("getspaceid [spaceName] - get the id for a space name.");
                    log.Add("listusersinspace [spacename] - List users in the space.\n\tExample: listusersinspace \"Acme Labs Space\"");
                    log.Add("addusertospace [username] [spacename] {admin=[true/false]} - Adds a user to a space.\n\tExample: addusertospace pinky@acmelabs.com \"Acme Labs Space\"");
                    log.Add("dropuserfromspace [username] [spacename] - Drops a user from a space.\n\tExample: dropuserfromspace snowball@microsponge.com \"Acme Labs Space\"");
                    log.Add("clearcache {[spaceId]/[spacename]} - Clears cache for a space (defaults to current space)\n\tExample: clearcache \"Acme Labs Space\"");
                    log.Add("setspacessopassword [spaceId] [password] - Set the space SSO password");
                    log.Add("swapspacecontents [space1name] [space2name] - Swaps the content of space 1 with that of space 2.\n\tExample: swapspacecontents TestSpace \"Production Space\"");
                    log.Add("swapspaceforpackages {prod=space1name} {dev=space2name} - Swaps the content of space 1 with that of space 2.\n\tExample: swapspaceforpackages prod=\"Production Space\" dev=\"Test Space\"");

                    // new copyspace, rather than a list of individual ones
                    log.Add("copyspace [space1id] [space2id] [copy/replicate] [options] - Copies the contents of space1 into space2, copy or replicate, and options is a comma separated list of items to copy. See the documentation for details of the list of options.");

                    // various forms of copyspace, deprecated
                    log.Add("copyspacecontents [space1name] [space2name] - Copies the content of space 1 into/over space 2. Deprecated, should use copyspace.");
                    log.Add("copydashboardtemplates [space1id] [space2id] - Copies the dashboard templates of space 1 into/over space 2. Deprecated, should use copyspace.");
                    log.Add("copycustomsubjectarea [space1id] [customsubjectareaname] [space2id] - Copies the custom subject area from space 1 into/over space 2. Deprecated, should use copyspace.");
                    log.Add("copygroupsandusers [spacename1] [spacename2] Copies the groups and users of space 1 into space 2. Deprecated, should use copyspace.");
                    log.Add("copycatalogdirectory [space1name] [space2name] [directoryname] - Copies the content of a catalog directory. Deprecated, should use copyspace. Note that [directoryname] must be relative (i.e., not starting with a forward or backward slash).\n\tExample: copycatalogdirectory dev prod shared/myreports");

                    log.Add("\n-- group and acl operations --");
                    log.Add("listgroups [spacename] - Lists groups for a space");
                    log.Add("creategroup [spacename] [groupname] - Creates a group.\n\tExample: creategroup \"Acme Labs Space\" \"World Domination Group\"");
                    log.Add("deletegroup [spacename] [groupname] - Deletes a group.\n\tExample: deletegroup \"Acme Labs Space\" \"World Domination Group\"");
                    log.Add("listgroupacls [spacename] [groupname] - Lists ACLs for a group");
                    log.Add("addacltogroup [spacename] [groupname] [acltag] - Add the acl to the group");
                    log.Add("removeaclfromgroup [spacename] [groupname] [acltag] - Remove the acl from the group");
                    log.Add("listusersingroup [spacename] [groupname] - List users in the group");
                    log.Add("addusertogroup [username] [spacename] [groupname] - Adds a user to a group");
                    log.Add("dropuserfromgroup [username] [spacename] [groupname] - Drops a user from a group");

                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n-- user operations --");
                        log.Add("listmanagedusers - Lists all users managed by the person running the command");
                        log.Add("createuser [username] {password=[xxxx]} {email=[xxxx]} - Creates a new user.\n\tExample: createuser pinky email=pinky@acmelabs.com password=zort");
                        if (u.OperationsFlag)
                        {
                            log.Add("\n-- product operations (operations only) --");
                            log.Add("listproducts {[username]} - list all products, or those for a user");
                            log.Add("addproduct [username] [productID] {[quantity]} - Add a product to a user.\n\tExample: addproduct pinky@acmelabs.com 2");
                            log.Add("removeproduct [username] [productID] - Remove a product from a user");

                            log.Add("\n-- mask operations (operations only) --");
                            log.Add("listmasks [username] - List the create masks for a user");
                            log.Add("addmask [username] [mask] - Add a create mask (regular expression) to a user.\n\tExample: addmask pinky@acmelabs.com (\\w)+@acmelabs(\\.)com");
                        }
                        log.Add("listreleases - Lists the available releases");
                        log.Add("getuserrelease [username] - Gets the release for a user");
                        log.Add("setuserrelease [username] [release] - Sets the release for a user");
                        if (u.OperationsFlag)
                        	log.Add("setuserrendertype [username] [html/flash] - Sets the render type (html or flash) for a user");
                        log.Add("setdenycreatespace [username] [true|false] - Prevents [allows] a user to create a new space");
                        log.Add("setuserpassword [username] [password] - Sets the password for a user.\n\tExample: setuserpassword pink@acmelabs.com h4h4h4N4rf!");
                        log.Add("setuserdefaultspace [username] [spaceID] [dashboards] - Sets the default space for a user and sets if they default to dashboards (true/false).\n\tExample: setuserdefaultspace pinky@acmelabs.com 5bf1dff7-1f47-4dc6-a16b-d8529b76e37b true");
                        log.Add("enableuser [username] [enable] - Enable/disable a user.\n\tExample: enableuser pinky@acmelabs.com false (disables pinky@acmelabs.com)");
                        log.Add("deleteuser [username] - Delete a user.\n\tExample: deleteuser pinky@acmelabs.com");
                        log.Add("unlockuser [username] - Unlock a user.\n\tExample: unlockuser pinky@acmelabs.com");
                        // commands to manage a users locale information
                        log.Add("setlanguageforuser [username] [language code] - Sets a User's language.\n\tExample: setlanguageforuser joe@birst.com en-US");
                        log.Add("getlanguageforuser [username] - Returns the language and code assigned to a user.\n\tExample: getlanguageforuser joe@birst.com");
                        log.Add("listlanguages - Lists the supported languages and their codes.");

                    }
                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n-- proxy user operations --");
                        log.Add("listproxyusers [username] - Lists all users that the user can proxy for");
                        if (u.OperationsFlag)
                            log.Add("addproxyuser [username] [proxyusername][expiration] [repAdminFlag] - Adds a user as a proxy for another (username can proxy for proxyusername). The setting has an expiration associated with it (yyyy-MM-dd).  [repAdminFlag] is true or false (operations only).\n\tExample: addproxyuser brain@acmelabs.com orson.welles@rko.com true 2013-04-04");
                        log.Add("addproxyuser [username] [proxyusername] [expiration] - Adds a user as a proxy for another (username can proxy for proxyusername). The setting has an expiration associated with it (yyyy-MM-dd).\n\tExample: addproxyuser brain@acmelabs.com orson.welles@rko.com 2013-12-31");
                        log.Add("removeproxyuser [username] [proxyusername] - Drops a user as a proxy for another (username can no longer proxy for proxyusername)");
                    }

                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n-- OpenID operations --");
                        log.Add("listopenids [username] - Lists all openids for the user");
                        log.Add("addopenid [username] [openid] - Adds an OpenID for the user");
                        log.Add("removeopenid [username] [openid] - Removes an OpenID for the user");
                    }

                    if (u.OperationsFlag)
                    {
                        log.Add("\n-- account operations --");
                        log.Add("enableaccount [accountID] [enable] - Enable/disable an account.\n\tExample: enableaccount 40D4767B-F97F-4BF9-98FB-0E46E1DCDFAA false (disables 40D4767B-F97F-4BF9-98FB-0E46E1DCDFAA)");
                    }

                    if (u.OperationsFlag)
                    {
                        log.Add("\n-- Redshift region operations --");
                        log.Add("addredshiftregion [Name] [TimeBucket] [UploadBucket] - Add a new region support for redshift.\n\tExample: addredshiftregion \"US EAST\" s3://Birst-Time-Dimension-Files/time s3://Birst_Upload ");
                        log.Add("listallredshiftregions - list all redshift region entries in database");
                    }

                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n-- IP address restriction operations --");
                        log.Add("listallowedips [username] - Lists all IPv4 addressess and netblocks that the user can login from (includes user and account IP addrs).\n\tExample: listallowedips brain@acmelabs.com");
                        log.Add("addallowedip [username] [ip/cidr] - Add an IPv4 address or a netblock in CIDR format.\n\tExample: addallowedip pinky@acmelabs.com 123.234.232.0/24");
                        log.Add("removeallowedip [username] [ip/cidr] - Remove an IPv4 address or a netblock in CIDR format");
                        if (u.OperationsFlag)
                        {
                            log.Add("listipaddrsforaccount [accountId] - Lists all IPv4 addressess and netblocks that the user can login from (operations only)");
                            log.Add("addipaddrforaccount [ip/cidr] [accountId] - Add an IPv4 address or a netblock in CIDR format (operations only)");
                            log.Add("removeipaddrforaccount [ip/cidr] [accountId] - Remove an IPv4 address or a netblock in CIDR format (operations only)");
                        }
                        log.Add("listipaddrsforaccount - Lists all IPv4 addressess and netblocks that the user can login from.");
                        log.Add("addipaddrforaccount [ip/cidr] - Add an IPv4 address or a netblock in CIDR format.\n\tExample: addipaddrforaccount 123.234.232.0/24");
                        log.Add("removeipaddrforaccount [ip/cidr] - Remove an IPv4 address or a netblock in CIDR format.\n\tExample: removeipaddrforaccount 123.234.232.0/24");
                    }

                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n-- password policy operations --");
                        if (u.OperationsFlag)
                        {
                            log.Add("getpasswordpolicy [accountID] - Returns the password policy for the account");
                            log.Add("setpasswordpolicy [options] [description] [regularexpression] [accountID] - Sets the password policy for the account. Options is a comma separated list of key=value pairs.\n"
                                + "\tkeys are: minlength, maxlength, containsmixedcase, containsspecial, containsnumeric, historylength, expirationdays, changeonfirstuse, maxfailedloginattempts, failedloginwindow (in miniutes), hashalgorithm\n"
                                + "\tif specified, hashalgorithm allowed values are: bcrypt:costFactor [costFactor should be between 10 and 17, it is a power of 2], pbkdf2:prf:iterations [prf is one of 'hmac-sha1', 'hmac-sha256', 'hmac-sha384', 'hmac-sha512'; iterations should be between 1000 and 100000; see NIST SP 800-132] - note that the bigger the costFactor/iterations, the longer it will take to login.\n"
                                + "\tExample: setpasswordpolicy \"minlength=4,containsnumeric=true\" \"Passwords must be 4 characters in length and contain at least one number\" \"\" xxx-xxx-xxx-xxx"
                                + "\tExample: setpasswordpolicy \"\" \"Passwords contain at least 1 lower case character\" \"[a-z]+\" xxx-xxx-xxx-xxx");
                            log.Add("testpassword [password] [username] [accountID] - Test a password against the account password policy.\n\tExample: testpassword s3kr1T pinky@acmelabs.com xxx-xxx-xxx-xxx");
                        }
                        else
                        {
                            log.Add("getpasswordpolicy - Returns the password policy for the account");
                            log.Add("setpasswordpolicy [options] [description] [regularexpression] - Sets the password policy for the account. Options is a comma separated list of key=value pairs.\n"
                                + "\tkeys are: minlength, maxlength, containsmixedcase, containsspecial, containsnumeric, historylength, expirationdays, changeonfirstuse, maxfailedloginattempts, failedloginwindow (in minutes), hashalgorithm\n"
                                + "\tif specified, hashalgorithm allowed values are: bcrypt:costFactor [costFactor should be between 10 and 17, it is a power of 2], pbkdf2:prf:iterations [prf is one of 'hmac-sha1', 'hmac-sha256', 'hmac-sha384', 'hmac-sha512'; iterations should be between 1000 and 100000; see NIST SP 800-132] - note that the bigger the costFactor/iterations, the longer it will take to login.\n"
                                + "\tExample: setpasswordpolicy \"minlength=4,containsnumeric=true\" \"Passwords must be 4 characters in length and contain at least one number\" \"\""
                                + "\tExample: setpasswordpolicy \"\" \"Passwords contain at least 1 lower case character\" \"[a-z]+\"");
                            log.Add("testpassword [password] [username] - Test a password against the account password policy.\n\tExample: testpassword s3kr1T pinky@acmelabs.com");
                        }
                    }

                    if (UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("\n--authentication type operations --");
                        if (u.OperationsFlag)
                        {
                            log.Add("getauthentication [accountID] - Returns the authentication settings (if any) for the account");
                            log.Add("setauthentication [type] {[host]} [secret] [accountID] - Sets the authentication settings for the account.  Type is RADIUS or BIRST\n"
                                + "\tExample: setauthentication RADIUS myradius.server.com secret  xxx-xxx-xxx-xxx\n"
                                + "\tExample: setauthentication BIRST xxx-xxx-xxx");
                        }
                        else
                        {
                            log.Add("getauthentication {[accountID]} - Returns the authentication settings (if any) for the account");
                            log.Add("setauthentication [type] {[host]} {[secret]} - Sets the authentication settings for the account.  Type is RADIUS or BIRST\n"
                                + "\tExample: setauthentication RADIUS myradius.server.com secret\n"
                                + "\tExample: setauthentication BIRST");
                        }
                    }

                    log.Add("\n-- usage tracking operations --");
                    log.Add("getusage [usagespace] [fromdate] - Get the usage information for your spaces from 'fromdate' (yyyy-MM-dd) and put it into 'usagespace' for processing");
                    log.Add("listspacesforusage - List the spaces that will be used in the 'getusage' command");

                    log.Add("\n-- data sampling operations (advanced) --");
                    log.Add("createsample [Dimension] [Level] [Source] [percent] - Create a sample set for a given dimension level of a given percent using a source");
                    log.Add("derivesample [SourceDimension] [SourceLevel] [TargetDimension] [TargetLevel] [Source] - Create a sample set for a given dimension level using another dimension\n\tlevel sample and a staging table as cross reference");
                    log.Add("applysample [Dimension] [Level] [Source] - Apply sampling for a given level to a source");
                    log.Add("removesample [Dimension] [Level] [Source] - Remove sampling for a given level from a source");
                    log.Add("showsamples - Show all samples present in a path");
                    log.Add("deletesamples - Delete all created sample files in a path");

                    log.Add("\n-- misc operations (advanced) --");
                    log.Add("persistaggregate [name] - persist the named aggregate (the definition must already exist)");
                    log.Add("dropaggregate [name] - drop the named aggregate (the definition must already exist)");
                    log.Add("updatestats - update query optimization statistics on tables and/or index views of the space (sql server only)");
                    log.Add("setvariable [variablename] [value] - Sets the value of a variable");
                    log.Add("generatebulkqueryresults [query=<logicalquery> | reportpath=<pathofreportincatalog>] [filename=<outputfilename>] {[numrows=<MaxNumRows>]} {[uselogical=<true|false>]} - Generates bulkresults for logical query or report. Note that <pathofreportCatalog> must begin with a '/'.\n\tExample: generatebulkqueryresults reportpath=/shared/report filename=akexport.csv");
                    log.Add("checkconnections - Get the status (Active/Inactive) for all connections of a space");
                    log.Add("validatemetadata - run rule(s) against repository to validate metadata");
                    if (u.OperationsFlag)
                        log.Add("copyfile [fromSpaceID] [fromFile] [toSpaceID] [toFileOrDir] [overwrite]. Note that [toFileOrDir] must be relative (i.e., not starting with a forward or backward slash)");
                    return;
                }
                else if (cname == "migratespacetolocal")
                {
                    //permit use of command only if user is operations user and local etl connection is alive for space
                    if (!u.OperationsFlag && (session == null || session["proxyIsOperation"] == null || (!(bool)session["proxyIsOperation"])))
                    {
                        log.Add("Must be the operations user to perform migratespacetolocal.");
                        Global.systemLog.Error("Must be the operations user to perform migratespacetolocal.");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (tokens.Count != 2)
                    {
                        log.Add("Invalid migratespacetolocal command: " + line);
                        Global.systemLog.Info("Invalid migratespacetolocal command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!sp.ConnectString.ToLower().StartsWith("jdbc:sqlserver://"))
                    {
                        log.Add("migratespacetolocal is only supported for sql server database.");
                        Global.systemLog.Error("migratespacetolocal is only supported for sql server database.");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    bool newRep;
                    MainAdminForm maf = Util.loadRepository(sp, out newRep);
                    Dictionary<string, Connection> leConnnections = BirstConnectUtil.getLocalETLConenctions(sp, maf);
                    if (!leConnnections.Keys.Contains(tokens[1]))
                    {
                        log.Add("Invalid local etl connection name supplied for migratespacetolocal command: " + tokens[1]);
                        Global.systemLog.Error("Invalid local etl connection name supplied for migratespacetolocal command: " + tokens[1]);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Connection leConn = leConnnections[tokens[1]];
                    ProxyRegistration pr = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        pr = Database.getProxyRegistration(conn, mainSchema, sp.ID.ToString() + "/" + leConn.Name);
                        if (pr == null)
                        {
                            log.Add("Cannot migrate space to local - Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + leConn.Name);
                            Global.systemLog.Error("Cannot migrate space to local - Failed to obtain ProxyRegistration for :" + sp.ID.ToString() + "/" + leConn.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!LiveAccessUtil.isRealTimeConnectionActive(pr, sp, leConn.Name))
                        {
                            log.Add("Cannot migrate space to local - local etl live access connection '" + tokens[1] + "' is not alive");
                            Global.systemLog.Error("Cannot migrate space to local - local etl live access connection '" + tokens[1] + "' is not alive");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        Database.migrateSpaceToLocal(sp, maf, conn, leConn, u);
                    }
                    catch (Exception ex)
                    {
                        log.Add("Cannot migrate space to local - " + ex.Message);
                        Global.systemLog.Error("Cannot migrate space to local - " + ex.Message);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                // BPD-19336 List all supported languages for a user
                else if (cname == "listlanguages")
                {
                    List<Language> languages = null;
                    try
                    {
                        languages = CommandHelper.listLanguages(u);

                        log.Add("listLanguages [username] - Lists all languages for a user");

                        foreach (Language language in languages)
                        {
                            log.Add(language.code + ": " + language.description);
                        }
                        return;
                    }
                    catch (Exception e)
                    {
                        log.Add("Error getting list of languages for user: u.Username" + e.Message);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                }

                // BPD-19340 Set a User's language   
                else if (cname == "setlanguageforuser")
                {
                    // setlanguageforuser language
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setlanguageforuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string user = tokens[1];
                    string localeId = tokens[2]; // like "en-US"
                    try
                    {
                        CommandHelper.setLanguageForUser(u, user, localeId);
                    }
                    catch (Exception e)
                    {
                        log.Add("Error setting languages for user: user" + e.Message);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    log.Add("Set language: " + localeId + " for user: " + user);
                }

                // BPD-19339 Get language for User 
                else if (cname == "getlanguageforuser")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid getlanguageforuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string user = tokens[1];
                    List<Language> languages = null;
                    string localeId = "";
                    string language = "";
                    try
                    {
                        localeId = CommandHelper.getLanguageForUser(u, user);
                        languages = CommandHelper.listLanguages(u);

                        foreach (Language lang in languages)
                        {
                            if (localeId.Equals(lang.code))
                            {
                                language = lang.description;
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        log.Add("Error setting language for user: user" + e.Message);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    log.Add("getLanguageForUser got language: " + localeId + ":" + language + " for user: " + user);
                    return;
                }
                else if (cname == "clearcache")
                {
                    string spaceId = sp.ID.ToString();
                    if (tokens.Count == 2)
                    {
                        spaceId = tokens[1];
                        Guid result;
                        if (!Guid.TryParse(spaceId, out result))
                        {
                            // must be a space name
                            QueryConnection conn = null;
                            try
                            {
                                conn = ConnectionPool.getConnection();
                                Space newsp = Database.getSpace(conn, tokens[1], u, true, mainSchema);
                                if (newsp == null)
                                {
                                    log.Add("Space not found: " + tokens[1]);
                                    Global.systemLog.Warn("clearcache: User " + u.Username + " tried to clearcache on a non-existant space " + tokens[1]);
                                    this.exitStatus = GENERAL_ERROR;
                                    return;
                                }
                                spaceId = newsp.ID.ToString();
                            }
                            finally
                            {
                                ConnectionPool.releaseConnection(conn);
                            }
                        }
                    }
                    if (!CommandHelper.clearCache(u, spaceId))
                        log.Add("Failed to clear cache: " + spaceId);
                    else
                        log.Add("Cleared cache: " + spaceId);
                    return;
                }
                else if (cname == "listspacesforusage")
                {
                    QueryConnection conn = null;

                    if (u.AdminAccountId == null || u.AdminAccountId == Guid.Empty)
                    {
                        log.Add("Must be the administrator of an account to use usage tracking. Contact Birst Customer Support.");
                        Global.systemLog.Info("Must be the administrator of an account to use usage tracking. Contact Birst Customer Support.");
                        return;
                    }
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        List<Space> spaceIds = Database.getSpacesToAudit(conn, mainSchema, u.AdminAccountId);
                        foreach (Space space in spaceIds)
                        {
                            log.Add(space.ID + ": " + space.Name);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    Global.systemLog.Info("listspacesforusage: " + u.Username);
                    return;
                }
                else if (cname == "getusage")
                {
                    // getusage [usagespace] [fromdate]
                    // getusage audit 2010-01-01
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid getusage command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string auditSpaceName = tokens[1];
                    string fromDate = tokens[2];
                    QueryConnection conn = null;
                    QueryReader reader = null;
                    StreamWriter writer = null;

                    if (u.AdminAccountId == null || u.AdminAccountId == Guid.Empty)
                    {
                        log.Add("Must be the administrator of an account to use usage tracking. Contact Birst Customer Support.");
                        Global.systemLog.Info("Must be the administrator of an account to use usage tracking. Contact Birst Customer Support.");
                        return;
                    }
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        // get the auditspace
                        Space auditSpace = Database.getSpace(conn, auditSpaceName, u, true, mainSchema);
                        // query the audit db, joining with USERS (for userID) and SPACES (for spacename), filtering on spaceIds and from date
                        DateTime dt = DateTime.Parse(fromDate);
                        reader = Database.getAuditRecords(conn, mainSchema, auditTableName, u.AdminAccountId, dt);
                        if (reader == null)
                        {
                            log.Add("No spaces marked for usage tracking");
                            Global.systemLog.Info("No spaces marked for usage tracking");
                            return;
                        }
                        DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        // open file for writing
                        string fname = auditSpace.Directory + "\\data\\usage.txt";
                        writer = new StreamWriter(fname, false, Encoding.UTF8);
                        int count = 0;
                        writer.Write("timestamp|epochTime|userID|userName|spaceID|spaceName|mode|name|dashboard|sequenceID|sessionID|elapsedTime");
                        writer.WriteLine();
                        while (reader.Read())
                        {
                            // time, userid, username, spaceid, spacename, mode, report, dashboard, sequence, elapsed time
                            int i = 0;
                            DateTime dtA = reader.GetDateTime(i++);
                            writer.Write(dtA.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            writer.Write('|');
                            long delta = (long)((dtA - epoch).TotalMilliseconds);
                            writer.Write(delta);
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string userId = reader.GetGuid(i).ToString();
                                writer.Write(userId);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string userName = reader.GetString(i);
                                writer.Write(userName);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string spaceId = reader.GetString(i);
                                writer.Write(spaceId);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string spaceName = reader.GetString(i);
                                writer.Write(spaceName);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string mode = reader.GetString(i);
                                writer.Write(mode);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string report = reader.GetString(i);
                                writer.Write(report);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string dashboard = reader.GetString(i);
                                writer.Write(dashboard);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string sequence = reader.GetString(i);
                                writer.Write(sequence);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                string sessionid = reader.GetString(i);
                                writer.Write(sessionid);
                            }
                            i++;
                            writer.Write('|');
                            if (!reader.IsDBNull(i))
                            {
                                int elapsedTime = reader.GetInt32(i);
                                writer.Write(elapsedTime);
                            }
                            writer.WriteLine();
                            count++;
                        }
                        log.Add("Created usage tracking data in space " + auditSpaceName + " with " + count + " records");
                        Global.systemLog.Info("Created usage information in space " + auditSpaceName + " with " + count + " records");
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                        if (reader != null)
                            reader.Close();
                        if (writer != null)
                            writer.Close();
                    }
                    return;
                }
                else if (cname == "listcreatedusers" || cname == "listmanagedusers")
                {
                    // listcreatedusers
                    if (tokens.Count < 1)
                    {
                        log.Add("Invalid listmanagedusers command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    List<string> ids = CommandHelper.listManagedUsers(u);
                    if (ids != null)
                    {
                        foreach (string name in ids)
                        {
                            log.Add(name);
                        }
                    }
                    Global.systemLog.Info("listmanagedusers: User " + u.Username + " listed managed users");
                    return;
                }
                else if (cname == "listproxyusers")
                {
                    // listproxyusers username
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listproxyusers command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    List<string> ids = CommandHelper.listProxyUsers(u, userName);
                    if (ids != null)
                    {
                        foreach (string id in ids)
                        {
                            log.Add(id);
                        }
                    }
                    Global.systemLog.Info("listproxyusers: User " + u.Username + " listed proxy users for - " + userName);
                    return;
                }
                else if (cname == "listopenids")
                {
                    // listopenids username
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listopenids command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    List<string> ids = CommandHelper.listOpenIDs(u, userName);
                    if (ids != null)
                    {
                        foreach (string name in ids)
                        {
                            log.Add(name);
                        }
                    }
                    else
                    {
                        log.Add("Invalid copyfile command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                 
                    CommandHelper.copyFile(u, tokens[1], tokens[2], tokens[3], tokens[4], bool.Parse(tokens[5]));
                    log.Add("copyfile: User " + u.Username + " copied file: "
                        + tokens[1] + ", " + tokens[2] + ", " + tokens[3] + ", " + tokens[4] + ", " + tokens[5]);
                    Global.systemLog.Info("copyfile: User " + u.Username + " copied file: "
                        + tokens[1] + ", " + tokens[2] + ", " + tokens[3] + ", " + tokens[4] + ", " + tokens[5]);
                    return;
                }
                else if (cname == "swapspaceforpackages")
                {
                    // swapspaceforpackages {prod=space1name} {dev=space2name}
                    int tokenLength = tokens.Count;
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid swapspaceforpackages command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string[] tokenContainingSpaceNames = new string[] { tokens[1], tokens[2] };
                    string prodSpaceName = null;
                    string devSpaceName = null;
                    foreach(string tok in tokenContainingSpaceNames)
                    {
                        string[] tokArray = tok.Split('=');
                        if (tokArray.Length != 2)
                        {
                            break;
                        }
                        if (tokArray[0] == "prod" || tokArray[0] == "dev")
                        {
                            string spaceName = tokArray[1].Trim(new char[] { '"' });
                            if (spaceName != null && spaceName.Trim().Length > 0)
                            {
                                if (tokArray[0] == "prod")
                                {
                                    prodSpaceName = spaceName.Trim();
                                }
                                else
                                {
                                    devSpaceName = spaceName.Trim();
                                }
                            }
                        }
                    }

                    if (prodSpaceName == null || devSpaceName == null)
                    {
                        log.Add("Invalid swapspaceforpackages command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (prodSpaceName == devSpaceName)
                    {
                        log.Add("The two space names must be different");
                        Global.systemLog.Info("swapspaceforpackages: User " + u.Username + " failed to swap spaces " + prodSpaceName + " and " + devSpaceName + " (the spaces names are the same)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    swapSpaces("swapspaceforpackages", prodSpaceName, devSpaceName, true);
                }
                else if (cname == "swapspacecontents")
                {
                    int tokensLength = tokens.Count;
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid swapspacecontents command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    if (tokens[1] == tokens[2])
                    {
                        log.Add("The two space names must be different");
                        Global.systemLog.Info("swapspacecontents: User " + u.Username + " failed to swap spaces " + tokens[1] + " and " + tokens[2] + " (the spaces names are the same)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    swapSpaces("swapspacecontents", tokens[1], tokens[2], false);
                    return;                    
                }
                else if (cname == "copyspacecontents" && tokens.Count == 3)
                {
                    if (tokens[1] == tokens[2])
                    {
                        log.Add("The two space names must be different");
                        Global.systemLog.Info("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " (the spaces names are the same)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Space sp1 = null;
                    Space sp2 = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, false);
                        foreach (SpaceMembership sm in smlist)
                        {
                            if (sm.Space.Name == tokens[1])
                                sp1 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                            else if (sm.Space.Name == tokens[2])
                                sp2 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                        }
                        if (sp1 == null || sp2 == null)
                        {
                            log.Add("Unable to find spaces to copy");
                            Global.systemLog.Info("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " (one or both spaces do not exist)");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp1))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp2))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // is the copied from busy in other operations except concurrent copy from. That is ok
                        int sp1OpID = SpaceOpsUtils.getSpaceAvailability(sp1);
                        if (!SpaceOpsUtils.isSpaceAvailable(sp1OpID) && sp1OpID != SpaceOpsUtils.OP_COPY_FROM)
                        {
                            log.Add("Unable to copy space. " + SpaceOpsUtils.getAvailabilityMessage(sp1.Name, sp1OpID));
                            Global.systemLog.Error("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " " + SpaceOpsUtils.getAvailabilityMessage(sp1.Name, sp1OpID));
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // is the copied to busy in other operations.
                        int sp2OpID = SpaceOpsUtils.getSpaceAvailability(sp2);
                        if (!SpaceOpsUtils.isSpaceAvailable(sp2OpID))
                        {
                            log.Add("Unable to copy space contents. " + SpaceOpsUtils.getAvailabilityMessage(sp2.Name, sp2OpID));
                            Global.systemLog.Error("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " " + SpaceOpsUtils.getAvailabilityMessage(sp1.Name, sp2OpID));
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // check for the processing status for sp1
                        if (Util.checkForPublishingStatus(sp1))
                        {
                            log.Add("Unable to copy space contents. " + sp1.Name + " is being published.");
                            Global.systemLog.Error("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + ". " + sp1.Name + " is being published");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // check for the processing status for sp1
                        if (Util.checkForPublishingStatus(sp2))
                        {
                            log.Add("Unable to copy space contents. " + sp2.Name + " is being published.");
                            Global.systemLog.Error("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + ". " + sp2.Name + " is being published");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Util.areSpacesInSameDatabase(sp1, sp2))
                        {
                            
                            log.Add("Unable to copy space contents. You are attempting to copy contents of two spaces that are incompatible (different connection strings). ");
                            Global.systemLog.Error("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " (different connection strings");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    bool newRep;
                    MainAdminForm maf1 = Util.loadRepository(sp1, out newRep);
                    MainAdminForm maf2 = Util.loadRepository(sp2, out newRep);
                    if (maf1 == null || maf2 == null)
                    {
                        log.Add("Unable to find spaces to copy");
                        Global.systemLog.Info("copyspacecontents: User " + u.Username + " failed to copy space contents for " + tokens[1] + " and " + tokens[2] + " (one or both repositories do not exist)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!BirstConnectUtil.equalsLocalConnections(sp1, maf1, sp2, maf2))
                    {
                        log.Add("Unable to copy space content. You are attempting to copy contents of two spaces that are incompatible. Make sure both spaces have same birst local configuration(s).");
                        Global.systemLog.Info("copyspacecontents: User " + u.Username + " failed to copy space contents " + tokens[1] + " and " + tokens[2] + " (incompatible spaces for copy contents operation)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Database.copySpaceToExistingSpace(sp1, u, sp2,maf1);
                    // Rebuild application
                    // Get the current publish loadnumbers
                    Dictionary<string, int> curLoadNumbers = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        foreach (StagingTable st in maf1.stagingTableMod.getStagingTables())
                        {
                            // If there are loadgroups other than ACORN, get them
                            if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, "ACORN") >= 0 ? 1 : 0))
                            {
                                curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                                break;
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    // Copy entire repository, but preserve users and groups
                    List<Performance_Optimizer_Administration.User> ulist = maf2.usersAndGroups.usersList;
                    List<Performance_Optimizer_Administration.Group> glist = maf2.usersAndGroups.groupsList;
                    maf2 = maf1;
                    maf2.usersAndGroups.usersList = ulist;
                    maf2.usersAndGroups.groupsList = glist;
                    Util.buildApplication(maf2, mainSchema, DateTime.Now, sp2.LoadNumber, curLoadNumbers, sp2, session);
                    Util.saveApplication(maf2, sp2, null, u);
                    log.Add("Space contents copied: " + tokens[1] + ", " + tokens[2]);
                    Global.systemLog.Info("copyspacecontents: User " + u.Username + " just copied space contents for " + tokens[1] + " and " + tokens[2]);
                    return;
                }
                else if (cname == "copycatalogdirectory" && tokens.Count == 4)
                {
                    if (tokens[1] == tokens[2])
                    {
                        log.Add("The two space names must be different");
                        Global.systemLog.Info("copycatalogdirectory: User " + u.Username + " failed to copy the catalog directory for spaces " + tokens[1] + " and " + tokens[2] + " (the spaces names are the same)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Space sp1 = null;
                    Space sp2 = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, false);
                        foreach (SpaceMembership sm in smlist)
                        {
                            if (sm.Space.Name == tokens[1])
                                sp1 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                            else if (sm.Space.Name == tokens[2])
                                sp2 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                        }
                        if (sp1 == null || sp2 == null)
                        {
                            log.Add("Unable to find spaces to copy");
                            Global.systemLog.Info("copycatalogdirectory: User " + u.Username + " failed to copy the catalog directory for spaces " + tokens[1] + " and " + tokens[2] + " (one or both spaces do not exist)");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp1))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp2))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    if (tokens[3].StartsWith("/") || tokens[3].StartsWith("\\"))
                    {
                        log.Add("[directoryname] can not start with a / or \\, it must be a relative path");
                        Global.systemLog.Warn("User " + u.Username + " tried to use a non-relative directory path: " + tokens[3] + ", " + sp.Name);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string sp1CatalogDir = Path.Combine(sp1.Directory, "catalog");
                    string sp2CatalogDir = Path.Combine(sp2.Directory, "catalog");

                    string name1 = Path.Combine(sp1CatalogDir, tokens[3]);
                    string name2 = Path.Combine(sp2CatalogDir, tokens[3]);

                    Util.validateFileLocation(sp1CatalogDir, name1);
                    Util.validateFileLocation(sp2CatalogDir, name2);

                    if (!Directory.Exists(name1))
                    {
                        log.Add("Directory not found: " + tokens[3]);
                        Global.systemLog.Info("copycatalogdirectory: User " + u.Username + " failed to copy the catalog directory for spaces " + tokens[1] + " and " + tokens[2] + " (the source directory (" + tokens[3] + ") does not exist)");
                        Global.systemLog.Info("copycatalogdirectory: Full Dirs: " + name1 + ", " + name2);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Util.copyDir(name1, name2);
                    sp2.invalidateDashboardCache();
                    log.Add("Catalog directory copied: " + tokens[3] + ", " + tokens[1] + ", " + tokens[2]);
                    Global.systemLog.Info("copycatalogdirectory: User " + u.Username + " copied the catalog directory " + tokens[3] + " for the spaces " + tokens[1] + " and " + tokens[2]);
                    return;
                }
                else if (cname == "createuser")
                {
                    if (!UserManagementUtils.isUserAuthorized(u))
                    {
                        log.Add("User is not an account administrator and does not have permission to execute createUser command");
                        return;
                    }
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid createuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    if (!isValidUsername(uname))
                    {
                        log.Add("Invalid username: username contains invalid characters");
                        Global.systemLog.Warn("createuser: User " + u.Username + " tried to create a username with invalid characters '" + uname + "'");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    QueryConnection conn = null;
                    PasswordPolicy policy = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        policy = Database.getPasswordPolicy(conn, mainSchema, u.ManagedAccountId);
                        if (policy == null)
                        {
                            Global.systemLog.Error("Missing default password policy, please add one to the Birst Admin database");
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    MembershipUser mu = Membership.GetUser(uname);
                    if (mu == null)
                    {
                        string pwd = System.Web.Security.Membership.GeneratePassword(10, 2);
                        MembershipCreateStatus mstatus;
                        string fname = null;
                        string lname = null;
                        string email = uname; // email defaults to user name

                        for (int i = 2; i < tokens.Count; i++)
                        {
                            string t = tokens[i].ToLower();
                            if (t.StartsWith("password="))
                            {
                                pwd = tokens[i].Substring(9);
                                if (policy != null && !policy.isValid(pwd, uname, null))
                                {
                                    log.Add("Failed to create user (" + uname + ") - password does not conform to password policy for the account - " + policy.description);
                                    Global.systemLog.Warn("createuser: User " + u.Username + " failed to create user (" + uname + ") - password does not conform to password policy");
                                    this.exitStatus = GENERAL_ERROR;
                                    return;
                                }
                            }
                            else if (t.StartsWith("firstname="))
                            {
                                fname = tokens[i].Substring(10);
                            }
                            else if (t.StartsWith("lastname="))
                            {
                                lname = tokens[i].Substring(9);
                            }
                            else if (t.StartsWith("email="))
                            {
                                email = tokens[i].Substring(6);
                            }
                        }
                        if (email == null || email.Length == 0)
                            email = uname;
                        bool valid = isValidEmailAddress(u, email);
                        if (!valid)
                        {
                            log.Add("Invalid username: no rights to create a user with the given email address - " + email + ".  Please contact Birst Customer Support in order to define a list of formats for your account.");
                            Global.systemLog.Warn("createuser: User " + u.Username + " has no rights to create a user with the given email address - " + email);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        mu = Membership.CreateUser(uname, pwd, email, null, null, true, out mstatus);
                        if (mstatus != MembershipCreateStatus.Success)
                        {
                            log.Add("Failed to create user (" + uname + ") - " + mstatus.ToString());
                            Global.systemLog.Warn("createuser: User " + u.Username + " failed to create user (" + uname + ") - " + mstatus.ToString());
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        string[] productIDs = newUserProductIDs.Split(new char[] { ',' });
                        try
                        {
                            conn = ConnectionPool.getConnection();
                            if (u.isDiscoveryModeOnly())
                            {
                                // get the visualization product from db and use that for the child user
                                List<Product> products = Database.getProducts(conn, mainSchema, User.DISCOVERY_TYPE);
                                // There should be only one of them. If there are duplicate or more than one just take the first one
                                if (products != null && products.Count > 0)
                                {
                                    productIDs = new string[] { products[0].ProductID.ToString() };
                                }
                                else
                                {
                                    // should not happen but in case product was accidently removed we stop that
                                    productIDs = new string[0];
                                }
                            }

                            Database.setupNewUser(conn, mainSchema, uname, fname, lname, email, null, null, null, null, null, null, null, true, 0, "", "", "", "", new List<string>(productIDs), DateTime.MinValue);
                            Database.logCreatedUser(conn, mainSchema, (Guid)mu.ProviderUserKey, u.ID, pwd);
                        }
                        finally
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        log.Add("User added: " + uname);
                        Global.systemLog.Info("createuser: User " + u.Username + " create user " + uname);
                        return;
                    }
                    else
                    {
                        log.Add("User (" + uname + ") already exists, not creating");
                        Global.systemLog.Warn("createuser: User " + u.Username + " tried to create a user that already exists - " + uname);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                }
                else if (cname == "addusertospace")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid addusertospace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string spaceId = tokens[2];
                    bool admin = false;
                    for (int i = 3; i < tokens.Count; i++)
                    {
                        string t = tokens[i].ToLower();
                        if (t.StartsWith("admin="))
                        {
                            bool.TryParse(t.Substring(6), out admin);
                        }
                        else if (t.StartsWith("administrator=")) // handle full name
                        {
                            bool.TryParse(t.Substring(14), out admin);
                        }
                    }
                    CommandHelper.addUserToSpace(u, uname, spaceId, admin);

                    log.Add("User added to space: " + uname + ", " + spaceId + ", with permissions:" + (admin ? " admin" : ""));
                    Global.systemLog.Info("addusertospace: User " + u.Username + " added user " + uname + " to space " + spaceId + ", with permissions:" + (admin ? " admin" : ""));
                    return;
                }
                else if (cname == "setspaceowner")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setspaceowner command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spaceID = tokens[1];
                    string uname = tokens[2];
                    CommandHelper.setSpaceOwner(u, spaceID, uname);
                    log.Add("Set space owner " + uname + ", " + spaceID);
                    Global.systemLog.Info("setspaceowner: User " + u.Username + " set space ownwer to " + uname + " in space " + spaceID);
                    return;
                }
                else if (cname == "getspaceid")
                {
                    // getspaceid spacename
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid getspaceid command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("getspaceid: User " + u.Username + " tried to getspaceid in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        else
                        {
                            log.Add("Space ID for " + spname + " is " + sp.ID);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    return;
                }
                else if (cname == "listusersinspace")
                {
                    // listusersinspace spacename
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listusersinspace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("listusersinspace: User " + u.Username + " tried to listusersinspace in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        List<SpaceMembership> mlist = Database.getUsers(conn, mainSchema, sp, false);
                        if (mlist != null)
                        {
                            foreach (SpaceMembership sm in mlist)
                            {
                                User user = sm.User;
                                if (user != null)
                                {
                                    log.Add(user.Username);
                                }
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    Global.systemLog.Info("listusersinspace: User " + u.Username + " listed users for space " + spname);
                    return;
                }
                else if (cname == "creategroup")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid creategroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gname = tokens[2];
                    QueryConnection conn = null;
                    Space sp = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("creategroup: User " + u.Username + " tried to create a group in a non-existant space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        SpaceGroup spg = Database.getGroupInfo(conn, mainSchema, sp, gname);
                        if (spg != null)
                        {
                            log.Add("Group already exists: " + gname);
                            Global.systemLog.Warn("creategroup: User " + u.Username + " tried to create a group (" + gname + ") that already exists in space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        Database.addGroupToSpace(conn, mainSchema, sp, gname, false);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    log.Add("Group added to space: " + gname + ", " + spname);
                    Global.systemLog.Info("creategroup: User " + u.Username + " added group " + gname + " in space " + spname);
                    return;
                }
                else if (cname == "deletegroup")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid deletegroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gname = tokens[2];
                    QueryConnection conn = null;
                    Space sp = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("deletegroup: User " + u.Username + " tried to create a group in a non-existant space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        SpaceGroup spg = Database.getGroupInfo(conn, mainSchema, sp, gname);
                        if (spg == null)
                        {
                            log.Add("Group does not exist: " + gname);
                            Global.systemLog.Warn("deletegroup: User " + u.Username + " tried to delete a group (" + gname + ") that does not exist in space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.removeGroupFromSpace(conn, mainSchema, sp, spg))
                        {
                            log.Add("Failed to delete group: " + gname);
                            Global.systemLog.Warn("deletegroup: User " + u.Username + " tried to delete a group (" + gname + ") in space " + spname + " but it failed");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    log.Add("Group delete from space: " + gname + ", " + spname);
                    Global.systemLog.Info("deletegroup: User " + u.Username + " delete group " + gname + " from space " + spname);
                    return;
                }
                else if (cname == "listgroups")
                {
                    // listgroups spacename
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listgroups command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    List<SpaceGroup> groups = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("listgroups: User " + u.Username + " tried to listgroups in non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        groups = Database.getSpaceGroups(conn, mainSchema, sp);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    foreach (SpaceGroup group in groups)
                    {
                        log.Add(group.Name);
                    }
                    Global.systemLog.Info("listgroups: User " + u.Username + " listed groups for space " + spname);
                    return;
                }
                else if (cname == "listusersingroup")
                {
                    // listusersingroup spacename groupname
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid listusersingroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gpname = tokens[2];
                    SpaceGroup group = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("listusersingroup: User " + u.Username + " tried to listusersingroup in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<SpaceGroup> groups = Database.getSpaceGroups(conn, mainSchema, sp);
                        foreach (SpaceGroup gp in groups)
                        {
                            if (gp.Name == gpname)
                            {
                                group = gp;
                                break;
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    if (group != null)
                    {
                        List<User> users = group.GroupUsers;
                        if (users != null)
                        {
                            foreach (User user in users)
                            {
                                log.Add(user.Username);
                            }
                        }
                    }
                    else
                    {
                        log.Add("Group does not exist in the space");
                        Global.systemLog.Info("listusersingroup: User " + u.Username + " tried to list group users for space " + spname + ", and non existent group " + gpname);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Global.systemLog.Info("listusersingroup: User " + u.Username + " listed group users for space " + spname + ", and group " + gpname);
                    return;
                }
                else if (cname == "listgroupacls")
                {
                    // listgroups spacename groupname
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid listgroupacls command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gpname = tokens[2];
                    SpaceGroup group = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("listgroupacls: User " + u.Username + " tried to listgroupacls in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<SpaceGroup> groups = Database.getSpaceGroups(conn, mainSchema, sp);
                        foreach (SpaceGroup gp in groups)
                        {
                            if (gp.Name == gpname)
                            {
                                group = gp;
                                break;
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    if (group == null)
                    {
                        log.Add("Group does not exist in the space");
                        Global.systemLog.Info("listgroupacls: User " + u.Username + " tried to list group acls for space " + spname + ", and non existent group " + gpname);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (group != null)
                    {
                        List<int> ACLIds = group.ACLIds;
                        if (ACLIds != null)
                        {
                            foreach (int aclId in ACLIds)
                            {
                                log.Add(ACLDetails.getACLTag(aclId));
                            }
                        }
                    }
                    Global.systemLog.Info("listgroupacls: User " + u.Username + " listed group acls for space " + spname + ", and group " + gpname);
                    return;
                }
                else if (cname == "addacltogroup")
                {
                    // addacltogroup spacename groupname acltag
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid addacltogroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gpname = tokens[2];
                    string acltag = tokens[3];
                    SpaceGroup group = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("addacltogroup: User " + u.Username + " tried to add acl to a group in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<SpaceGroup> groups = Database.getSpaceGroups(conn, mainSchema, sp);
                        foreach (SpaceGroup gp in groups)
                        {
                            if (gp.Name == gpname)
                            {
                                group = gp;
                                break;
                            }
                        }

                        if (group != null)
                        {
                            int aclId = ACLDetails.getACLID(acltag);
                            if (aclId == -1)
                            {
                                Global.systemLog.Warn("addacltogroup:  User " + u.Username + " tried to add a bad acl " + acltag + " to space " + spname + ", and group " + gpname);
                                log.Add("Bad ACL");
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                            List<int> ACLIds = group.ACLIds;
                            if (ACLIds == null || !ACLIds.Contains(aclId))
                            {
                                Database.addACLToGroup(conn, mainSchema, sp, group.ID, aclId);
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    log.Add("ACL " + acltag + " added to group " + gpname + " in space " + spname);
                    Global.systemLog.Info("addacltogroup: User " + u.Username + " added acl " + acltag + " to space " + spname + ", and group " + gpname);
                    return;
                }
                else if (cname == "removeaclfromgroup")
                {
                    // removeaclfromgroup spacename groupname acltag
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid removeaclfromgroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spname = tokens[1];
                    string gpname = tokens[2];
                    string acltag = tokens[3];
                    SpaceGroup group = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Space sp = Database.getSpace(conn, spname, u, true, mainSchema);
                        if (sp == null)
                        {
                            log.Add("Space not found: " + spname);
                            Global.systemLog.Warn("removeaclfromgroup: User " + u.Username + " tried to remove acl from a group in a non-existent space " + spname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<SpaceGroup> groups = Database.getSpaceGroups(conn, mainSchema, sp);
                        foreach (SpaceGroup gp in groups)
                        {
                            if (gp.Name == gpname)
                            {
                                group = gp;
                                break;
                            }
                        }

                        if (group != null)
                        {
                            int aclId = ACLDetails.getACLID(acltag);
                            if (aclId == -1)
                            {
                                Global.systemLog.Warn("removeaclfromgroup:  User " + u.Username + " tried to remove a bad acl " + acltag + " from space " + spname + ", and group " + gpname);
                                log.Add("Bad ACL");
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                            List<int> ACLIds = group.ACLIds;
                            if (ACLIds != null && ACLIds.Contains(aclId))
                            {
                                Database.removeACLFromGroup(conn, mainSchema, sp, group.ID, aclId);
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    log.Add("ACL " + acltag + " removed from group " + gpname + " in space " + spname);
                    Global.systemLog.Info("removeaclfromgroup: User " + u.Username + " removed acl " + acltag + " from space " + spname + ", and group " + gpname);
                    return;
                }
                else if (cname == "addusertogroup")
                {
                    // addusertogroup username spacename groupname
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid addusertogroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string spname = tokens[2];
                    string gname = tokens[3];
                    QueryConnection conn = null;
                    Space newsp = null;
                    User newuser = null;
                    List<SpaceMembership> mlist = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        newsp = Database.getSpace(conn, spname, u, true, mainSchema);
                        newuser = Database.getUser(conn, mainSchema, uname);
                        if (newsp != null)
                            mlist = Database.getUsers(conn, mainSchema, newsp, false);

                        if (newsp == null)
                        {
                            log.Add("Space not found: " + tokens[2]);
                            Global.systemLog.Warn("addusertogroup: User " + u.Username + " tried to add user " + uname + " to a group in a non-existant space " + tokens[2]);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (newuser == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("addusertogroup: User " + u.Username + " tried to add a non-existant user " + uname + " to the group " + newsp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, newsp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + newsp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    // determine if the user is a member of the space, necessary for adding to the group
                    bool inspace = false;
                    foreach (SpaceMembership sm in mlist)
                    {
                        if (sm.User == null)
                            continue;
                        if (sm.User.ID != null && newuser.ID == sm.User.ID)
                        {
                            inspace = true;
                            break;
                        }
                    }

                    if (!inspace)
                    {
                        log.Add("User not added to group - user not a member of the space");
                        Global.systemLog.Info("addusertogroup: User " + u.Username + " could not user " + newuser.Username + " to group " + gname + " in space " + newsp.Name + ": user not a member of the space");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    // Check if space-group-user mapping is done in the db. If not, populate required tables
                    Util.checkAndCreateUserGroupDbMappings(newsp);

                    try
                    {
                        conn = ConnectionPool.getConnection();
                        SpaceGroup toAddUserGroup = Database.getGroupInfo(conn, mainSchema, newsp, gname);
                        if (toAddUserGroup == null)
                        {
                            log.Add("User not added to group - group not found");
                            Global.systemLog.Info("addusertogroup: User " + u.Username + " could not add user " + newuser.Username + " to group " + gname + " in space " + newsp.Name + ": group not found");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (Util.isOwnerGroup(toAddUserGroup))
                        {
                            log.Add("User not added to group - modification to internal owner group not allowed");
                            Global.systemLog.Info("addusertogroup: User " + u.Username + " could not add user " + newuser.Username + " to group " + gname + " in space " + newsp.Name + ": OWNER$ group membership not modifable");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        Database.addUserToGroup(conn, mainSchema, newsp, toAddUserGroup.ID, newuser.ID, true);

                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    log.Add("User added to group: " + newuser.Username + ", " + gname + ", " + newsp.Name);
                    Global.systemLog.Info("addusertogroup: User " + u.Username + " added user " + newuser.Username + " to group " + gname + " in space " + newsp.Name);
                    return;

                }
                else if (cname == "dropuserfromspace")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid dropuserfromspace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string spaceId = tokens[2];
                    CommandHelper.removeUserFromSpace(u, uname, spaceId);
                    log.Add("User dropped from space");
                    Global.systemLog.Info("dropuserfromspace: User " + u.Username + " removed user " + uname + " from space " + spaceId);
                    return;
                }
                else if (cname == "dropuserfromgroup")
                {
                    // dropuserfromgroup username spacename groupname
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid dropuserfromgroup command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string spname = tokens[2];
                    string gname = tokens[3];
                    Space newsp = null;
                    User newuser = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        newsp = Database.getSpace(conn, spname, u, true, mainSchema);
                        newuser = Database.getUser(conn, mainSchema, uname);
                        if (newsp == null)
                        {
                            log.Add("Space not found: " + tokens[2]);
                            Global.systemLog.Warn("dropuserfromgroup: User " + u.Username + " tried to drop user " + uname + " from a group in a non-existant space " + tokens[2]);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (newuser == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("dropuserfromgroup: User " + u.Username + " tried to drop a non-existant user " + uname + " from the group " + newsp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (!Database.isSpaceAdmin(conn, mainSchema, u, newsp))
                        {
                            log.Add("User is not an adminstrator of the space");
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + newsp.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // Check if space-group-user mapping is done in the db. If not, populate required tables
                        Util.checkAndCreateUserGroupDbMappings(newsp);

                        SpaceGroup spg = Database.getGroupInfo(conn, mainSchema, newsp, gname);
                        if (spg == null)
                        {
                            log.Add("Group not found  - cannot drop from group: " + newuser.Username + ", " + gname);
                            Global.systemLog.Info("dropuserfromgroup: User " + u.Username + " could not drop " + newuser.Username + " from group " + gname + " in space " + newsp.Name + ": group not found");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        if (UserAndGroupUtils.isUserGroupMember(spg, newuser.ID))
                        {
                            Database.removeUserFromGroup(conn, mainSchema, newsp, spg.ID, newuser.ID, true);
                        }
                        else
                        {
                            log.Add("User not member of group  - nothing to drop: " + newuser.Username + ", " + gname);
                            Global.systemLog.Info("dropuserfromgroup: User " + u.Username + " could not drop " + newuser.Username + " from group " + gname + " in space " + newsp.Name + ": user not member of group");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    log.Add("Dropped user from group: " + newuser.Username + ", " + gname);
                    Global.systemLog.Info("dropuserfromgroup: User " + u.Username + " dropped user " + newuser.Username + " from group " + gname + " in space " + newsp.Name);
                    return;

                }
                else if (cname == "addmask")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid addmask command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    User newuser = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        newuser = Database.getUser(conn, mainSchema, uname);
                        if (newuser == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("addmask: User " + u.Username + " tried to add a mask to a non-existant user " + uname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        Database.addAllowedUsernameMasks(conn, mainSchema, newuser.ID, tokens[2]);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    log.Add("Mask added to user");
                    Global.systemLog.Info("addmask: User " + u.Username + " added mask " + tokens[2] + " to " + newuser.Username);
                    return;
                }
                else if (cname == "listmasks")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listmasks command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    User newuser = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        newuser = Database.getUser(conn, mainSchema, uname);
                        if (newuser == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("addmask: User " + u.Username + " tried to add a mask to a non-existant user " + uname);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<string> masks = Database.getAllowedUsernameMasks(conn, mainSchema, newuser.ID);
                        foreach (string mask in masks)
                        {
                            log.Add(mask);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    return;
                }
                else if (cname == "copyspace")
                {
                    // copyspace fromspaceid tospaceid replicate options
                    if (tokens.Count < 5)
                    {
                        log.Add("Invalid copyspace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string fromSpaceID = tokens[1];
                    string toSpaceID = tokens[2];
                    string mode = tokens[3];
                    string options = tokens[4];

                    log.Add("started " + Util.copyReplicateLabel(mode == "replicate") + " space from " + fromSpaceID + " to " + toSpaceID + ": " + options);
                    CommandHelper.copySpace(u, fromSpaceID, toSpaceID, mode, options);

                    return;
                }
                else if (cname == "copydashboardtemplates")
                {
                    // copydashboardtemplates fromspaceid tospaceid
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid copydashboardtemplates command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string fromSpace = tokens[1];
                    string toSpace = tokens[2];
                    CommandHelper.copyDashboardTemplates(u, fromSpace, toSpace);

                    log.Add("copydashboardtemplates from " + fromSpace + " to " + toSpace);
                    Global.systemLog.Info("copydashboardtemplates from " + fromSpace + " to " + toSpace);
                    return;
                }
                else if (cname == "copycustomsubjectarea")
                {
                    // copycustomsubjectarea fromspaceid customSubjectAreaName tospaceid
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid copycustomsubjectarea command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string fromSpace = tokens[1];
                    string csaName = tokens[2];
                    string toSpace = tokens[3];
                    CommandHelper.copyCustomSubjectArea(u, fromSpace, csaName, toSpace);

                    log.Add("copycustomsubjectarea " + csaName + " from " + fromSpace + " to " + toSpace);
                    Global.systemLog.Info("copycustomsubjectarea " + csaName + " from " + fromSpace + " to " + toSpace);
                    return;
                }
                else if (cname == "getpasswordpolicy")
                {
                    // getpasswordpolicy {accountid}
                    string accountID = u.ManagedAccountId.ToString();
                    if (tokens.Count == 2)
                        accountID = tokens[1];

                    PasswordPolicy policy = CommandHelper.getPasswordPolicy(u, accountID);

                    Global.systemLog.Info("getpasswordpolicy called for " + accountID);
                    if (policy == null)
                    {
                        log.Add("No password policy");
                        return;
                    }

                    log.Add("Password Policy is: "
                    + "description: " + policy.description + ", regEx: " + policy.regularExpression
                    + ", minLen: " + policy.minLength + ", maxLen: " + policy.maxLength + ", mixedCase: " + policy.containsMixedCase
                    + ", special: " + policy.containsSpecial + ", numeric: " + policy.containsNumeric + ", nousername: " + policy.doesnotContainUsername
                    + ", changeonfirstuse: " + policy.changeOnFirstUse
                    + ", history: " + policy.historyLength + ", expiration: " + policy.expirationDays
                    + ", hashalgorithm: " + policy.hashAlgorithm
                    + ", maxfailedloginattempts: " + policy.maxFailedLoginAttempts + ", failedloginwindow: " + policy.failedLoginWindow);
                    return;
                }
                else if (cname == "testpassword")
                {
                    // testpassword pw username accountID
                    string accountID = u.ManagedAccountId.ToString();
                    if (tokens.Count == 4)
                        accountID = tokens[3];

                    PasswordPolicy policy = CommandHelper.getPasswordPolicy(u, accountID);
                    QueryConnection conn = null;
                    List<string> history = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        User user = Database.getUser(conn, mainSchema, tokens[2]);
                        if (user != null)
                            history = Database.getPasswordHistory(conn, mainSchema, user.ID);
                        if (policy.isValid(tokens[1], tokens[2], history))
                            log.Add("Password succeeds");
                        else
                            log.Add("Password fails");
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                }
                else if (cname == "setpasswordpolicy")
                {
                    // setpasswordpolicy options desc regex {accountID}
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid setpasswordpolicy command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string accountID = u.ManagedAccountId.ToString();
                    if (tokens.Count == 5)
                        accountID = tokens[4];
                    else if (u.OperationsFlag)
                        accountID = Guid.Empty.ToString();

                    CommandHelper.setPasswordPolicy(u, accountID, tokens[1], tokens[2], tokens[3]);

                    log.Add("Set password policy for " + accountID);
                    Global.systemLog.Info("setpasswordpolicy called for " + accountID);
                    return;
                }
                else if (cname == "getauthentication")
                {
                    // getradius {accountid}
                    string accountID = u.ManagedAccountId.ToString();
                    if (tokens.Count == 2)
                    {
                        if (u.OperationsFlag)
                        {
                            accountID = tokens[1];
                        }
                        else
                        {
                            log.Add("Invalid getauthentication command (not operations): " + line);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                    }

                    string description = CommandHelper.getAuthDescription(u, accountID);

                    Global.systemLog.Info("getradius called for " + accountID);
                    if (description == null)
                    {
                        log.Add("Birst Authentication (default)");
                        return;
                    }

                    log.Add("Authentication settings: " + description);
                    return;
                }
                else if (cname == "setauthentication")
                {
                    // setradis type {host} {secret} {accountID}
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid setauthentication command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string type = null;
                    string host = null;
                    string secret = null;

                    // if token count 2, then setradius BIRST
                    // if token count 3, then setradius BIRST accountId
                    // if token count 4, then setradius RADIUS host secret
                    // if token count 5, then setraidus RADIUS host secret accountId
                    string accountID = u.ManagedAccountId.ToString();
                    type = tokens[1];

                    switch (tokens.Count)
                    {
                        case 2:
                            break;
                        case 3:
                            if (u.OperationsFlag)
                            {
                                accountID = tokens[2];
                            }
                            else
                            {
                                log.Add("Invalid setauthentication command (not operations): " + line);
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                            break;
                        case 4:
                            host = tokens[2];
                            secret = tokens[3];
                            break;
                        case 5:
                            host = tokens[2];
                            secret = tokens[3];
                            if (u.OperationsFlag)
                            {
                                accountID = tokens[4];
                            }
                            else
                            {
                                log.Add("Invalid setauthentication command (not operations): " + line);
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                            break;
                        default:
                            log.Add("Invalid setauthentication command: " + line);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                    }

                    CommandHelper.setAuthentication(u, accountID, type, host, secret);

                    log.Add("Set Authentication to " + type + " for " + accountID);
                    Global.systemLog.Info("setauthentication called for " + accountID);
                    return;
                }
                else if (cname == "addopenid")
                {
                    // addopenid username openid
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid addopenid command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string openID = tokens[2];
                    CommandHelper.addOpenID(u, userName, openID);

                    log.Add("OpenID added: " + userName + ", " + openID);
                    Global.systemLog.Info("addopenid: openID added: " + userName + ", " + openID);
                    return;
                }
                else if (cname == "removeopenid")
                {
                    // removeopenid username openid
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid removeopenid command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string openID = tokens[2];
                    CommandHelper.removeOpenID(u, userName, openID);

                    log.Add("OpenID removed: " + userName + ", " + openID);
                    Global.systemLog.Info("removeopenid: openID added: " + userName + ", " + openID);
                    return;
                }
                else if (cname == "setspacessopassword")
                {
                    // setspacessopassword spaceid password
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setspacessopassword command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string spaceId = tokens[1];
                    string password = tokens[2];
                    CommandHelper.setSpaceSSOPassword(u, spaceId, password);

                    log.Add("Set Space SSOPassword for " + spaceId);
                    Global.systemLog.Info("setspacessopassword: " + spaceId);
                    return;
                }
                else if (cname == "setuserrendertype")
                {
                    // setuserrendertype username html/flash
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setuserrendertype command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string type = tokens[2];
                    CommandHelper.setUserRenderType(u, uname, type);
                    log.Add("Set Render Type for User " + uname + ", " + type);
                    Global.systemLog.Info("setuserrendertype: " + uname + ", " + type);
                    return;
                }
                else if (cname == "setdenycreatespace")
                {
                    // setdenycreatespace username [true|false]
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setdenycreatespace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string allow = tokens[2];
                    CommandHelper.setDenyAddSpace(u, uname, bool.Parse(allow));
                    log.Add("Set deny create space for User " + uname + ", " + allow);
                    Global.systemLog.Info("setdenycreatespace: " + uname + ", " + allow);
                    return;
                }
                else if (cname == "getuserrelease")
                {
                    // getuserrelease username
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid getuserrelease command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string release = CommandHelper.getUserRelease(u, uname);
                    log.Add("Get Release for User " + uname + ", " + (release != null ? release : "(not set)"));
                    Global.systemLog.Info("getuserrelease: " + uname + ", " + (release != null ? release : "(not set)"));
                    return;
                }
                else if (cname == "setuserrelease")
                {
                    // setuserrelease username release
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setuserrelease command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string release = tokens[2];
                    CommandHelper.setUserRelease(u, uname, release);
                    log.Add("Set Release for User " + uname + ", " + release);
                    Global.systemLog.Info("setuserrelease: " + uname + ", " + release);
                    return;
                }
                else if (cname == "listreleases")
                {
                    // listreleases
                    List<string> infoList = CommandHelper.listReleases(u);
                    if (infoList != null)
                    {
                        foreach (string name in infoList)
                        {
                            log.Add(name);
                        }
                    }
                    Global.systemLog.Info("listreleases: User " + u.Username);
                    return;
                }
                else if (cname == "setuserdefaultspace")
                {
                    // setuserdefaultspace username spaceID dashboards
                    if (tokens.Count != 4)
                    {
                        log.Add("Invalid setuserdefaultspace command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string spaceID = tokens[2];
                    bool dashboards = bool.Parse(tokens[3]);
                    CommandHelper.setUserDefaultSpace(u, uname, spaceID, dashboards);
                    log.Add("Set Default Space for " + uname + " to " + spaceID + (dashboards ? " (default to dashboards)" : ""));
                    Global.systemLog.Info("setuserdefaultspace: " + uname + ", " + spaceID + ", " + dashboards);
                    return;
                }
                else if (cname == "setuserpassword")
                {
                    // setuserpassword username password
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid setuserpassword command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string password = tokens[2];
                    CommandHelper.setUserPassword(u, uname, password);
                    log.Add("Set password for User " + uname);
                    Global.systemLog.Info("setuserpassword: " + uname);
                    return;
                }
                else if (cname == "enableuser")
                {
                    // enableuser username enable (true/false)
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid enableuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string enable = tokens[2];
                    CommandHelper.enableUser(u, uname, bool.Parse(enable));
                    log.Add("Enable User " + uname + ", " + enable);
                    Global.systemLog.Info("enableuser: " + uname + ", " + enable);
                    return;
                }
                else if (cname == "deleteuser")
                {
                    // enableuser username
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid deleteuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    CommandHelper.deleteUser(u, uname);
                    log.Add("Delete User " + uname);
                    Global.systemLog.Info("deleteuser: " + uname);
                    return;
                }
                else if (cname == "enableaccount")
                {
                    // enableaccount accountID enable (true/false)
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid enableaccount command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string accountID = tokens[1];
                    string enable = tokens[2];
                    CommandHelper.enableAccount(u, accountID, bool.Parse(enable));
                    log.Add("Enable Account " + accountID + ", " + enable);
                    Global.systemLog.Info("enableaccount: " + accountID + ", " + enable);
                    return;
                }
                else if (cname == "listallredshiftregions")
                {
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    List<AWSRegion> aRegions = UserAdministration.getAvailableRegionsForRedshift();
                    if (aRegions != null)
                    {
                        foreach (AWSRegion aRegion in aRegions)
                        {
                            if (!aRegion.Active)
                                continue;
                            log.Add(aRegion.Name);
                        }
                    }
                    else
                    {
                        log.Add("No redshift region entries found in database");
                    }
                    Global.systemLog.Info("listallredshiftregions : " + u.Username);
                    return;
                }
                else if (cname == "addredshiftregion")
                {
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    // addRedshiftRegion name timebucket uploadbucket
                    if (tokens.Count != 4)
                    {
                        log.Add("Invalid addredshiftregion command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string name = tokens[1];
                    string timeBucket = tokens[2];
                    string uploadBucket = tokens[3];
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        AWSRegion aRegion = Database.getRedshiftRegionDetails(conn, mainSchema, name);
                        if (aRegion != null)
                        {
                            log.Add("Region Name already used : " + line);
                            return;
                        }

                        int maxRegionId = Database.getMaxRedshiftRegionID(conn, mainSchema);
                        maxRegionId = maxRegionId > 0 ? maxRegionId + 1 : 1;
                        Database.addRedshiftAWSRegion(conn, mainSchema, maxRegionId, name, timeBucket, uploadBucket, true);
                        log.Add("Successfully executed command : " + line);
                    }
                    catch (Exception ex)
                    {
                        log.Add("Unable to create region entry");
                        Global.systemLog.Error("Unable to create redshift region entry ", ex);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    return;

                }
                else if (cname == "unlockuser")
                {
                    // unlockuser user
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid unlockuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    CommandHelper.unlockUser(u, uname);
                    log.Add("Unlock user " + uname);
                    Global.systemLog.Info("unlockuser: " + uname);
                    return;
                }
                else if (cname == "addproxyuser")
                {
                    // addproxyuser username proxyuser [repAdmin]
                    if ((u.OperationsFlag && tokens.Count < 5) || (!u.OperationsFlag && tokens.Count < 4))
                    {
                        log.Add("Invalid addproxyuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string proxyUserName = tokens[2];
                    DateTime expiration = DateTime.MaxValue;
                    DateTime.TryParse(tokens[3], out expiration);
                    bool repAdmin = false;
                    if (u.OperationsFlag)
                    {
                        Boolean.TryParse(tokens[4], out repAdmin);
                    }
                    CommandHelper.addProxyUser(u, userName, proxyUserName, expiration, repAdmin);
                    if (u.OperationsFlag)
                    {
                        log.Add("Proxy added: " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd") + ", " + repAdmin);
                        Global.systemLog.Info("addproxyuser: added " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd") + ", " + repAdmin);
                    }
                    else
                    {
                        log.Add("Proxy added: " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd"));
                        Global.systemLog.Info("addproxyuser: added " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd"));
                    }
                    return;
                }
                else if (cname == "removeproxyuser")
                {
                    // removeproxyuser username proxyuser
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid removeproxyuser command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string proxyUserName = tokens[2];
                    CommandHelper.removeProxyUser(u, userName, proxyUserName);
                    log.Add("Proxy removed: " + userName + ", " + proxyUserName);
                    Global.systemLog.Info("removeproxyuser: removed " + userName + ", " + proxyUserName);
                    return;
                }

                    // IP filtering commands
                else if (cname == "listallowedips")
                {
                    // listallowedips username
                    if (tokens.Count < 2)
                    {
                        log.Add("Invalid listallowedips command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    List<string> ids = CommandHelper.listAllowedIPs(u, userName);
                    if (ids != null)
                    {
                        foreach (string name in ids)
                        {
                            log.Add(name);
                        }
                    }
                    Global.systemLog.Info("listallowedips: User " + u.Username + " listed allowed IPs for - " + userName);
                    return;
                }
                else if (cname == "listipaddrsforaccount")
                {
                    // listipaddrsforaccount {accountId} [accountId can only be specified if you are in operations]
                    if (tokens.Count > 2)
                    {
                        log.Add("Invalid listipaddrsforaccount command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string accountId = null;
                    if (tokens.Count == 2)
                        accountId = tokens[1];
                    List<string> ids = CommandHelper.listAllowedIPAddrsForAccount(u, accountId);
                    if (ids != null)
                    {
                        foreach (string name in ids)
                        {
                            log.Add(name);
                        }
                    }
                    Global.systemLog.Info("listipaddrsforaccount: User " + u.Username + " listed allowed IPs" + (accountId != null ? (" for" + accountId) : ""));
                    return;
                }

                else if (cname == "addallowedip")
                {
                    // addallowedip username ip/cidr
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid addallowedip command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string ipaddress = tokens[2];
                    CommandHelper.addAllowedIP(u, userName, ipaddress);
                    log.Add("IP added: " + userName + ", " + ipaddress);
                    Global.systemLog.Info("addallowedip: IP address added: " + userName + ", " + ipaddress);
                    return;
                }
                else if (cname == "addipaddrforaccount")
                {
                    // addipaddrforaccount ip/cidr {accountId} [accountId can only be specified if you are in operations]
                    if (tokens.Count > 3 || tokens.Count < 2)
                    {
                        log.Add("Invalid addipaddrforaccount command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string ipaddress = tokens[1];
                    if (!CommandHelper.isValidIPAddress(ipaddress))
                    {
                        log.Add("Ip address is incorrectly formatted for addipaddrforaccount command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    string accountId = null;
                    if (tokens.Count == 3)
                        accountId = tokens[2];
                    CommandHelper.addAllowedIPAddrForAccount(u, accountId, ipaddress);
                    log.Add("IP added: " + ipaddress + (accountId != null ? accountId : ""));
                    Global.systemLog.Info("addipaddrforaccount: IP address added: " + ipaddress + (accountId != null ? accountId : ""));
                    return;
                }
                else if (cname == "removeallowedip")
                {
                    // removeallowedip username ip/cidr
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid removeallowedip command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string userName = tokens[1];
                    string ipaddress = tokens[2];
                    CommandHelper.removeAllowedIP(u, userName, ipaddress);
                    log.Add("IP address removed: " + userName + ", " + ipaddress);
                    Global.systemLog.Info("removeallowedip: IP address removed: " + userName + ", " + ipaddress);
                    return;
                }
                else if (cname == "removeipaddrforaccount")
                {
                    // removeipaddrforaccount ip/cidr {accountId} [accountId can only be specified if you are in operations]
                    if (tokens.Count > 3 || tokens.Count < 2)
                    {
                        log.Add("Invalid removeipaddrforaccount command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string ipaddress = tokens[1];
                    string accountId = null;
                    if (tokens.Count == 3)
                        accountId = tokens[2];
                    CommandHelper.removeAllowedIPAddrForAccount(u, accountId, ipaddress);
                    log.Add("IP address removed: " + ipaddress + (accountId != null ? accountId : ""));
                    Global.systemLog.Info("removeipaddrforaccount: IP address removed: " + ipaddress + (accountId != null ? accountId : ""));
                    return;
                }
                else if (cname == "removeproduct")
                {
                    // removeproduct username product
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid removeproduct command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string productID = tokens[2];
                    QueryConnection conn = null;
                    User user = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        user = Database.getUser(conn, mainSchema, uname);
                        if (user == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("removeproduct: User " + u.Username + " tried to remove a product for a non-existent user: " + uname + ", " + productID);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<Product> plist = new List<Product>();
                        Product p = new Product();
                        p.ProductID = Int32.Parse(productID);
                        plist.Add(p);
                        Database.disableProducts(conn, mainSchema, user.ID, plist);
                        log.Add("Product removed: " + uname + ", " + productID);
                        Global.systemLog.Info("removeproduct: Product removed: " + uname + ", " + productID);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    return;
                }
                else if (cname == "addproduct")
                {
                    // add username product
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid addproduct command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string uname = tokens[1];
                    string productID = tokens[2];
                    int quantity = 1;
                    if (tokens.Count == 4)
                    {
                        Int32.TryParse(tokens[3], out quantity);
                    }
                    QueryConnection conn = null;
                    User user = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        user = Database.getUser(conn, mainSchema, uname);
                        if (user == null)
                        {
                            log.Add("User not found: " + uname);
                            Global.systemLog.Warn("addproduct: User " + u.Username + " tried to add a product to a non-existent user: " + uname + ", " + productID);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }
                        List<Product> plist = new List<Product>();
                        Product p = new Product();
                        p.ProductID = Int32.Parse(productID);
                        p.Quantity = quantity;
                        p.Active = true;
                        plist.Add(p);
                        Database.setProducts(conn, mainSchema, user.ID, new List<Product>(), plist);
                        log.Add("Product added: " + uname + ", " + productID);
                        Global.systemLog.Info("addproduct: Product added: " + uname + ", " + productID);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    return;
                }
                else if (cname == "listproducts")
                {
                    // listproducts {username}
                    if (tokens.Count > 2)
                    {
                        log.Add("Invalid listproducts command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (!u.OperationsFlag)
                    {
                        log.Add("Only available to operations staff");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    List<Product> products;
                    QueryConnection conn = null;
                    string username = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        if (tokens.Count == 2)
                        {
                            username = tokens[1];
                            User user = Database.getUser(conn, mainSchema, username);
                            if (user == null)
                            {
                                log.Add("Invalid user: " + username);
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                            products = Database.getProducts(conn, mainSchema, user.ID);
                        }
                        else
                        {
                            products = Database.getProducts(conn, mainSchema);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    foreach (Product product in products)
                    {
                        if (tokens.Count == 2)
                            log.Add(product.ProductID + ", " + product.Name + ", " + product.EndDate + ", " + product.Quantity);
                        else
                            log.Add(product.ProductID + ", " + product.Name);
                    }
                    Global.systemLog.Info("listproducts: User " + username);
                    return;
                }
                else if (cname == "exportreport")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No report path specified to export report: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    AdminService ads = new AdminService();
                    string path = tokens[1].Replace('/', '\\');
                    TrustedService.ExecuteResult er = ads.exportData(sp, u, path, true, maf, null);
                    if (!er.success)
                    {
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Global.systemLog.Info("exportreport: User " + u.Username + " exported the report " + path + " to space " + sp.Name);
                    log.Add("User " + u.Username + " exported the report " + path + " to space " + sp.Name);
                    return;
                }
                else if (cname == "generatebulkqueryresults")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid command arguments for generatebulkqueryresults command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    AdminService ads = new AdminService();
                    string query = null, reportPath = null;
                    if (tokens[1].ToLower().StartsWith("query="))
                    {
                        query = tokens[1].Substring("query=".Length);
                        if (query.StartsWith("\"") && query.EndsWith("\""))
                            query = query.Trim(new char[] { '"' });
                    }
                    else if (tokens[1].ToLower().StartsWith("reportpath="))
                    {
                        reportPath = tokens[1].Substring("reportpath=".Length);
                        reportPath = reportPath.Replace('/', '\\');
                        if (reportPath.StartsWith("\"") && reportPath.EndsWith("\""))
                            reportPath = reportPath.Trim(new char[] { '"' });
                    }
                    else
                    {
                        log.Add("Invalid command arguments to generatebulkqueryresults command(query or reportpath must be specified): " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    string filename = null;
                    if (tokens[2].ToLower().StartsWith("filename="))
                    {
                        filename = tokens[2].Substring("filename=".Length);
                        if (filename.StartsWith("\"") && filename.EndsWith("\""))
                            filename = filename.Trim(new char[] { '"' });
                    }
                    else
                    {
                        log.Add("Invalid command arguments to generatebulkqueryresults command(filename must be specified): " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    int numRows = 250000;
                    bool useLogicalQuery = false;
                    if (tokens.Count > 3)
                    {
                        for (int i = 3; i < tokens.Count; i++)
                        {
                            if (tokens[i].ToLower().StartsWith("numrows="))
                            {
                                string rows = tokens[i].Substring("numrows=".Length);
                                if (rows.StartsWith("\"") && rows.EndsWith("\""))
                                    rows = rows.Trim(new char[] { '"' });
                                if (!int.TryParse(rows, out numRows))
                                {
                                    log.Add("Invalid command arguments to generatebulkqueryresults command(invalid value for numrows specified): " + line);
                                    this.exitStatus = GENERAL_ERROR;
                                    return;
                                }
                            }
                            else if (tokens[i].ToLower().StartsWith("uselogical="))
                            {
                                if (!Boolean.TryParse(tokens[i].Substring("uselogical=".Length), out useLogicalQuery))
                                {
                                    log.Add("Invalid command arguments to generatebulkqueryresults command(invalid value for uselogical specified): " + line);
                                    this.exitStatus = GENERAL_ERROR;
                                    return;
                                }
                            }
                            else
                            {
                                log.Add("Invalid command arguments to generatebulkqueryresults command : " + tokens[i]);
                                this.exitStatus = GENERAL_ERROR;
                                return;
                            }
                        }

                    }
                    if (numRows > 1000000)
                    {
                        log.Add("numrows = " + numRows + " : cannot exceed 1000000");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    if (numRows > 250000 && useLogicalQuery)
                    {
                        log.Add("Cannot use logicalquery when numrows exceeds 250000");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    TrustedService.ExecuteResult er = ads.generateBulkQueryResults(sp, u, query, reportPath, filename, numRows, useLogicalQuery, maf);
                    if (!er.success)
                    {
                        if (er.errorMessage != null)
                            log.Add(er.errorMessage);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Global.systemLog.Info("generatebulkqueryresults: User " + u.Username + " generated bulkqueryresults for " + (query != null ? "query " + query : "reportPath" + reportPath) + " to space " + sp.Name);
                    log.Add("User " + u.Username + " generated bulkqueryresults for " + (query != null ? "query " + query : "reportPath" + reportPath) + " to space " + sp.Name);
                    return;
                }
                else if (cname == "getmodeldataset")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No dataset name specified: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("getmodeldataset '" + tokens[1].Replace("'", "") + "'\n");
                    File.WriteAllText(sp.Directory + "\\model.cmd", commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = sp.Directory + "\\model.cmd " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    Global.systemLog.Info("getmodeldataset for space (" + sp.ID + "/" + tokens[1] + ")");
                    log.Add("getmodeldataset for space (" + tokens[1] + ") started");
                    Process proc = Process.Start(psi);
                    if (proc.HasExited)
                    {
                        Global.systemLog.Info("getmodeldataset for space (" + sp.ID + "/" + tokens[1] + ") - Exited with code: " + proc.ExitCode);
                        log.Add("getmodeldataset for space (" + tokens[1] + ") completed");
                    }
                    else
                    {
                        Global.systemLog.Info("getmodeldataset for space (" + sp.ID + "/" + tokens[1] + ") - Running");
                        proc.WaitForExit();
                        log.Add(proc.StandardOutput.ReadToEnd());
                        log.Add("getmodeldataset for space (" + tokens[1] + ") completed");
                        Global.systemLog.Info(" getmodeldataset finished: " + sp.ID);
                    }
                    proc.Close();
                    return;
                }
                else if (cname == "summarizedataset")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No dataset name specified: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("summarizedataset '" + tokens[1].Replace("'", "") + "'\n");
                    File.WriteAllText(sp.Directory + "\\model.cmd", commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = sp.Directory + "\\model.cmd " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    Global.systemLog.Info("summarizedataset for space (" + sp.ID + "/" + tokens[1] + ")");
                    log.Add("summarizedataset for space (" + tokens[1] + ") started");
                    Process proc = Process.Start(psi);
                    if (proc.HasExited)
                    {
                        Global.systemLog.Info("summarizedataset for space (" + sp.ID + "/" + tokens[1] + ") - Exited with code: " + proc.ExitCode);
                        log.Add("summarizedataset for space (" + tokens[1] + ") completed");
                    }
                    else
                    {
                        Global.systemLog.Info("summarizedataset for space (" + sp.ID + "/" + tokens[1] + ") - Running");
                        proc.WaitForExit();
                        log.Add(proc.StandardOutput.ReadToEnd());
                        log.Add("summarizedataset for space (" + tokens[1] + ") completed");
                        Global.systemLog.Info(" summarizedataset finished: " + sp.ID);
                    }
                    proc.Close();
                    return;
                }
                else if (cname == "generatemodel")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No dataset name specified: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("generateoutcomemodels '" + tokens[1].Replace("'", "") + "'\n");
                    File.WriteAllText(sp.Directory + "\\model.cmd", commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = sp.Directory + "\\model.cmd " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    Global.systemLog.Info("generateoutcomemodels for space (" + sp.ID + "/" + tokens[1] + ")");
                    log.Add("generatemodel for space (" + tokens[1] + ") started");
                    Process proc = Process.Start(psi);
                    if (proc.HasExited)
                    {
                        Global.systemLog.Info("generateoutcomemodels for space (" + sp.ID + "/" + tokens[1] + ") - Exited with code: " + proc.ExitCode);
                        log.Add("generatemodel for space (" + tokens[1] + ") completed");
                    }
                    else
                    {
                        Global.systemLog.Info("generateoutcomemodels for space (" + sp.ID + "/" + tokens[1] + ") - Running");
                        proc.WaitForExit();
                        log.Add(proc.StandardOutput.ReadToEnd());
                        log.Add("generatemodel for space (" + tokens[1] + ") completed");
                        Global.systemLog.Info(" generateoutcomemodels finished: " + sp.ID);
                    }
                    proc.Close();
                    return;
                }
                else if (cname == "scoremodel")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No dataset name specified: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("analyzeoutcome '" + tokens[1].Replace("'", "") + "' writecsv usedatasetsegments\n");
                    File.WriteAllText(sp.Directory + "\\model.cmd", commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = sp.Directory + "\\model.cmd " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    Global.systemLog.Info("analyzeoutcome for space (" + sp.ID + "/" + tokens[1] + ")");
                    log.Add("scoremodel for space (" + tokens[1] + ") started");
                    Process proc = Process.Start(psi);
                    if (proc.HasExited)
                    {
                        Global.systemLog.Info("analyzeoutcome for space (" + sp.ID + "/" + tokens[1] + ") - Exited with code: " + proc.ExitCode);
                        log.Add("scoremodel for space (" + tokens[1] + ") completed");
                    }
                    else
                    {
                        Global.systemLog.Info("analyzeoutcome for space (" + sp.ID + "/" + tokens[1] + ") - Running");
                        proc.WaitForExit();
                        log.Add(proc.StandardOutput.ReadToEnd());
                        log.Add("scoremodel for space (" + tokens[1] + ") completed");
                        Global.systemLog.Info(" analyzeoutcome finished: " + sp.ID);
                    }
                    proc.Close();
                    bool newRep = false;
                    MainAdminForm maf = Util.loadRepository(sp, out newRep);
                    string modelDir = getModelDir(sp.Directory, maf);
                    FileInfo fi = new FileInfo(modelDir + "\\" + tokens[1] + "All - scoring.txt");
                    if (fi.Exists)
                    {
                        string targetName = tokens[1] + " - Scores.txt";
                        if (File.Exists(sp.Directory + "\\data\\" + targetName))
                            File.Delete(sp.Directory + "\\data\\" + targetName);
                        fi.MoveTo(sp.Directory + "\\data\\" + targetName);
                        ApplicationUploader ul = new ApplicationUploader(maf, sp, targetName, true, false, false, null, false, null,
                            u, null, 0, 0, false, null, null, false);

                        // upload and retreive any error during extension processing
                        ul.uploadFile(false, false, false);
                    }
                    return;
                }
                else if (cname == "persistaggregate")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No aggregate name specified: " + line);
                        return;
                    }
                    string aggName = tokens[1].Replace("'", "").Replace(" ", "_");
                    if (maf != null && maf.aggModule != null)
                    {
                        Aggregate agg = maf.aggModule.findAggregate(aggName);
                        if (agg != null)
                        {
                            LocalETLConfig leConfig = BirstConnectUtil.getLocalETLConfig(sp, agg.Connection);
                            if (leConfig != null)
                            {
                                string loadgroup = "";
                                if (agg.LoadGroups != null)
                                {
                                    for (int i = 0; i < agg.LoadGroups.Length; i++)
                                        loadgroup = loadgroup + agg.LoadGroups[i];
                                }
                                string filename = "persistaggregate." + aggName + "." + loadgroup + ".properties";
                                StringBuilder localAggCmmands = new StringBuilder();
                                localAggCmmands.Append("repository \"" + leConfig.getApplicationDirectory() + "\\" + sp.ID + "\\repository_dev.xml\"\n");
                                localAggCmmands.Append("persistaggregate '" + tokens[1].Replace("'", "") + "'\n");
                                File.WriteAllText(sp.Directory + "\\" + filename, localAggCmmands.ToString());
                                StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                                File.WriteAllText(sp.Directory + "\\customer." + leConfig.getLocalETLLoadGroup() + ".properties", overrideProperties.ToString());
                                LiveAccessUtil.sendCommandToLiveAccess(sp, agg.Connection, "runcommandfileonliveaccess\r\n" + filename + "\r\n");
                                log.Add("persistaggregate" + " for space (" + sp.ID + ") completed");
                                PUT_Handler.sendCommandEmail(u, sp, server.MapPath("~/"), log);
                                return;
                            }
                        }
                    }
                    string cmdFilename = sp.Directory + "\\" + "persistaggregate." + aggName + ".cmd";
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("persistaggregate '" + tokens[1].Replace("'", "") + "'\n");
                    File.WriteAllText(cmdFilename, commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = cmdFilename + " " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    processCommandFileInBackground(psi, "persistaggregate" + aggName, u, sp, log, false);
                    return;
                }
                else if (cname == "dropaggregate")
                {
                    if (tokens.Count < 2)
                    {
                        log.Add("No aggregate name specified: " + line);
                        return;
                    }
                    string aggName = tokens[1].Replace("'", "").Replace(" ", "_");
                    if (maf != null && maf.aggModule != null)
                    {
                        Aggregate agg = maf.aggModule.findAggregate(aggName);
                        if (agg != null)
                        {
                            LocalETLConfig leConfig = BirstConnectUtil.getLocalETLConfig(sp, agg.Connection);
                            if (leConfig != null)
                            {
                                string loadgroup = "";
                                if (agg.LoadGroups != null)
                                {
                                    for (int i = 0; i < agg.LoadGroups.Length; i++)
                                        loadgroup = loadgroup + agg.LoadGroups[i];
                                }
                                string filename = "dropaggregate." + aggName + "." + loadgroup + ".properties";
                                StringBuilder localAggCmmands = new StringBuilder();
                                localAggCmmands.Append("repository \"" + leConfig.getApplicationDirectory() + "\\" + sp.ID + "\\repository_dev.xml\"\n");
                                localAggCmmands.Append("dropaggregate '" + tokens[1].Replace("'", "") + "' persist\n");
                                File.WriteAllText(sp.Directory + "\\" + filename, localAggCmmands.ToString());
                                StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                                File.WriteAllText(sp.Directory + "\\customer." + leConfig.getLocalETLLoadGroup() + ".properties", overrideProperties.ToString());
                                LiveAccessUtil.sendCommandToLiveAccess(sp, agg.Connection, "runcommandfileonliveaccess\r\n" + filename + "\r\n");
                                log.Add("dropaggregate" + " for space (" + sp.ID + ") completed");
                                PUT_Handler.sendCommandEmail(u, sp, server.MapPath("~/"), log);
                                return;
                            }
                        }
                    }
                    string cmdFilename = sp.Directory + "\\" + "dropaggregate." + aggName + ".cmd";
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("dropaggregate '" + tokens[1].Replace("'", "") + "' persist\n");
                    File.WriteAllText(cmdFilename, commands.ToString());
                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = cmdFilename + " " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    Global.systemLog.Info("dropaggregate for space (" + sp.ID + "/" + tokens[1] + ")");
                    log.Add("dropaggregate for space (" + tokens[1] + ") started");
                    processCommandFileInBackground(psi, "dropaggregate" + aggName, u, sp, log, false);
                    return;
                }
                else if (cname == "updatestats")
                {
                    string cmdFilename = sp.Directory + "\\" + "updatestats.cmd";
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository.xml\n");
                    commands.Append("updatestats\n");
                    File.WriteAllText(cmdFilename, commands.ToString());

                    // Create the ProcessInfo object
                    string enginecmd = Util.getEngineCommand(sp);
                    ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                    psi.Arguments = cmdFilename + " " + sp.Directory;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.RedirectStandardOutput = true;
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardInput = false;
                    psi.RedirectStandardError = false;
                    psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                    if (!File.Exists(sp.Directory + "\\publish.lock"))
                    {
                        FileStream fs = File.Create(sp.Directory + "\\publish.lock");
                        fs.Close();
                        processCommandFileInBackground(psi, "updatestats", u, sp, log, true);
                    }
                    else
                    {
                        Global.systemLog.Info("Exception while executing updatestats command for space: " + sp.ID.ToString() + " Error: Cannot execute while processing/updatestats is running");
                        log.Add("Error: Cannot execute updatestats while processing/updatestats is running");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    return;
                }
                else if (cname == "setvariable")
                {
                    if (tokens.Count != 3)
                    {
                        log.Add("Variable name and value must be defined: " + line);
                        return;
                    }
                    bool newRep = false;
                    MainAdminForm maf = Util.loadRepository(sp, out newRep);
                    Variable foundv = null;
                    if (maf.variableMod != null && maf.getVariables() != null)
                        foreach (Variable sv in maf.getVariables())
                        {
                            if (tokens[1] == sv.Name)
                            {
                                foundv = sv;
                                break;
                            }
                        }
                    if (foundv != null)
                    {
                        foundv.Query = tokens[2];
                        Util.saveApplication(maf, sp, null, u);
                    }
                    else
                    {
                        log.Add("Variable [" + tokens[1] + "] not found");
                    }
                    return;
                }
                else if (cname == "copygroupsandusers")
                {
                    if (tokens.Count < 3)
                    {
                        log.Add("Invalid copygroupsandusers command: " + line);
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }

                    if (tokens[1] == tokens[2])
                    {
                        log.Add("The two space names must be different");
                        Global.systemLog.Info("copygroupsandusers: User " + u.Username + " failed to copy groups and users from " + tokens[1] + " to " + tokens[2] + " (the spaces names are the same)");
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Space sp1 = null;
                    Space sp2 = null;
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        List<SpaceMembership> smlist = Database.getSpaces(conn, mainSchema, u, false);
                        foreach (SpaceMembership sm in smlist)
                        {
                            if (sm.Space.Name == tokens[1])
                                sp1 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                            else if (sm.Space.Name == tokens[2])
                                sp2 = Database.getSpace(conn, mainSchema, sm.Space.ID);
                        }
                        if (sp1 == null)
                        {
                            log.Add("Unable to find space to copygroupsandusers " + tokens[1]);
                            Global.systemLog.Info("copygroupsandusers: User " + u.Username + " failed to copy groups and users from " + tokens[1] + " to " + tokens[2] + " . (" + tokens[1] + " do not exist)");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (sp2 == null)
                        {
                            log.Add("Unable to find space to copygroupsandusers " + tokens[2]);
                            Global.systemLog.Info("copygroupsandusers: User " + u.Username + " failed to copy groups and users from " + tokens[1] + " to " + tokens[2] + " . (" + tokens[2] + " do not exist)");
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp1))
                        {
                            log.Add("User is not an adminstrator of the space " + tokens[1]);
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp1.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        if (!Database.isSpaceAdmin(conn, mainSchema, u, sp2))
                        {
                            log.Add("User is not an adminstrator of the space " + tokens[2]);
                            Global.systemLog.Warn("User " + u.Username + " tried to perform an operation on a space that he/she does not administer: " + sp2.Name);
                            this.exitStatus = GENERAL_ERROR;
                            return;
                        }

                        // copy space memberships
                        List<SpaceMembership> space1Users = Database.getUsers(conn, mainSchema, sp1, false);
                        List<SpaceMembership> space2Users = Database.getUsers(conn, mainSchema, sp2, false);
                        User space2Owner = null;
                        if (space2Users != null && space2Users.Count > 0)
                        {
                            foreach (SpaceMembership sp2sm in space2Users)
                            {
                                if (sp2sm.Owner)
                                {
                                    space2Owner = sp2sm.User;
                                    break;
                                }
                            }
                        }

                        List<SpaceMembership> spaceMembershipsToAdd = null;
                        if (space1Users != null && space1Users.Count > 0)
                        {
                            spaceMembershipsToAdd = new List<SpaceMembership>();
                            foreach (SpaceMembership sm1 in space1Users)
                            {
                                if (sm1.User == null)
                                    continue;
                                bool found = false;
                                if (space2Users != null && space2Users.Count > 0)
                                {
                                    foreach (SpaceMembership sm2 in space2Users)
                                    {
                                        if (sm2.User == null)
                                            continue;
                                        if (sm2.User.ID == sm1.User.ID)
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (!found)
                                {
                                    spaceMembershipsToAdd.Add(sm1);
                                }

                            }
                        }

                        if (spaceMembershipsToAdd != null && spaceMembershipsToAdd.Count > 0)
                        {
                            foreach (SpaceMembership sm in spaceMembershipsToAdd)
                            {
                                // If its the owner, make sure add it as an admin
                                bool adminFlag = sm.Administrator;
                                if (sm.Owner)
                                {
                                    adminFlag = true;
                                }

                                Database.addSpaceMembership(conn, mainSchema, sm.User.ID, sp2.ID, adminFlag, false);
                            }
                        }

                        List<SpaceGroup> sp1GroupList = Database.getSpaceGroups(conn, mainSchema, sp1);
                        // spacegroups are not populated during Database.getSpace(..)
                        sp1.SpaceGroups = sp1GroupList;
                        if (sp1GroupList != null && sp1GroupList.Count > 0)
                        {
                            List<SpaceGroup> sp2GroupList = Database.getSpaceGroups(conn, mainSchema, sp2);
                            // spacegroups are not populated during Database.getSpace(..)
                            sp2.SpaceGroups = sp2GroupList;

                            foreach (SpaceGroup sp1Group in sp1GroupList)
                            {
                                if (sp1Group.Name == "Administrators" && sp1Group.InternalGroup)
                                {
                                    continue;
                                }
                                if (sp1Group.Name == Util.OWNER_GROUP_NAME && sp1Group.InternalGroup)
                                {
                                    continue;
                                }

                                SpaceGroup sp2Group = null;
                                // see if the group already exists in sp2. If it doesn't exist, just add
                                if (sp2GroupList != null)
                                {
                                    sp2Group = UserAndGroupUtils.getGroupInfoByName(sp2, sp1Group.Name, true);
                                }

                                if (sp2Group == null)
                                {
                                    Guid addedGroupId = Database.addGroupToSpace(conn, mainSchema, sp2, sp1Group.Name, sp1Group.InternalGroup);
                                    sp2Group = Database.getGroupInfo(conn, mainSchema, sp2, sp1Group.Name);
                                }

                                // Add group acls.
                                List<int> sp1GroupAcls = sp1Group.ACLIds;
                                List<int> sp2GroupAcls = sp2Group.ACLIds != null ? sp2Group.ACLIds : new List<int>();
                                if (sp1GroupAcls != null && sp1GroupAcls.Count > 0)
                                {
                                    foreach (int sp1g1Acl in sp1GroupAcls)
                                    {
                                        if (!sp2GroupAcls.Contains(sp1g1Acl))
                                        {
                                            Database.addACLToGroup(conn, mainSchema, sp2, sp2Group.ID, sp1g1Acl);
                                        }
                                    }

                                    if (sp2Group.Name == Util.USER_GROUP_NAME && sp2Group.InternalGroup)
                                    {
                                        // update the group acls
                                        sp2GroupAcls = Database.getGroupACLs(conn, mainSchema, sp2Group.ID);
                                        // for USER$ groups, delete the acls which were not in the "from copied" space
                                        foreach (int sp2g2Acl in sp2GroupAcls)
                                        {
                                            if (!sp1GroupAcls.Contains(sp2g2Acl))
                                            {
                                                Database.removeACLFromGroup(conn, mainSchema, sp2, sp2Group.ID, sp2g2Acl);
                                            }
                                        }
                                    }
                                }

                                // Add group users if it does not exist
                                if (sp1Group.GroupUsers != null && sp1Group.GroupUsers.Count > 0)
                                {
                                    foreach (User spg1User in sp1Group.GroupUsers)
                                    {
                                        // If the user is the owner of space 2, no need to add him/her to any group. Owner should have access to everything
                                        if (spg1User.ID == space2Owner.ID)
                                        {
                                            continue;
                                        }
                                        if (!UserAndGroupUtils.isUserGroupMember(sp2Group, spg1User.ID))
                                        {
                                            Database.addUserToGroup(conn, mainSchema, sp2, sp2Group.ID, spg1User.ID, true);
                                        }
                                    }
                                }
                            }

                            // If the currently logged in space is the "TO" space, just refresh the space groups so that
                            // if user goes to Group management in the same session, it sees the updated info
                            if (sp != null && sp2 != null && sp.ID == sp2.ID)
                            {
                                UserAndGroupUtils.populateSpaceGroups(sp, true);
                            }
                        }
                        log.Add("Group And Users copied from : " + tokens[1] + " to " + tokens[2]);
                        Global.systemLog.Info("copygroupandusers: User " + u.Username + " just copied group and users from " + tokens[1] + " to " + tokens[2]);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                else if (cname == "createsample")
                {
                    if (tokens.Count < 5)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("createsample " + sp.Directory + "\\data");
                    commands.Append(" '" + tokens[1].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[2].Replace("'", "") + "'");
                    String stagingTableName = tokens[3].Replace("'", "");
                    stagingTableName = getStagingTableFromDisplayName(stagingTableName).Name;
                    commands.Append(" '" + stagingTableName + "'");
                    commands.Append(" '" + tokens[4].Replace("'", "") + "'\n");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "createsample", line);
                    return;
                }
                else if (cname == "derivesample")
                {
                    if (tokens.Count < 6)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("derivesample " + sp.Directory + "\\data");
                    commands.Append(" '" + tokens[1].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[2].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[3].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[4].Replace("'", "") + "'\n");
                    String stagingTableName = tokens[5].Replace("'", "");
                    stagingTableName = getStagingTableFromDisplayName(stagingTableName).Name;
                    commands.Append(" '" + stagingTableName + "'\n");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "derivesample", line);
                    return;
                }
                else if (cname == "applysample")
                {
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("applysample " + sp.Directory + "\\data");
                    commands.Append(" '" + tokens[1].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[2].Replace("'", "") + "'");
                    String stagingTableName = tokens[3].Replace("'", "");
                    stagingTableName = getStagingTableFromDisplayName(stagingTableName).Name;
                    commands.Append(" '" + stagingTableName + "'");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "applysample", line);
                    return;
                }
                else if (cname == "removesample")
                {
                    if (tokens.Count < 4)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("removesample " + sp.Directory + "\\data");
                    commands.Append(" '" + tokens[1].Replace("'", "") + "'");
                    commands.Append(" '" + tokens[2].Replace("'", "") + "'");
                    String stagingTableName = tokens[3].Replace("'", "");
                    stagingTableName = getStagingTableFromDisplayName(stagingTableName).Name;
                    commands.Append(" '" + stagingTableName + "'");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "removesample", line);
                    return;
                }
                else if (cname == "deletesamples")
                {
                    if (tokens.Count < 1)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("deletesamples " + sp.Directory + "\\data");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "deletesamples", line);

                    //Set last modified date for all staging tables to current so that next time data is processed, all staging tables would be reloaded.
                    List<StagingTable> stList = maf.stagingTableMod.getStagingTables();
                    if ((stList != null) && (stList.Count > 0))
                    {
                        foreach (StagingTable st in stList)
                        {
                            st.LastModifiedDate = DateTime.UtcNow;
                        }
                    }
                    Acorn.Util.saveApplication(maf, sp, session, null);

                    return;
                }
                else if (cname == "showsamples")
                {
                    if (tokens.Count < 1)
                    {
                        log.Add("Invalid syntax: " + line);
                        return;
                    }
                    StringBuilder commands = new StringBuilder();
                    commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
                    commands.Append("showsamples " + sp.Directory + "\\data");
                    File.WriteAllText(sp.Directory + "\\exec.cmd", commands.ToString());
                    execprocess(sp, "showsamples", line);
                    return;
                }
                else if (cname == "validatemetadata")
                {
                    Acorn.AdminService adm = new Acorn.AdminService();
                    Object obj = null;
                    if (tokens.Count == 2 && tokens[1].ToLower().Equals("all"))
                    {
                        obj = adm.validateRepositoryMetadata(tokens[1], null);
                    }
                    else if (tokens.Count == 3 && (tokens[1].ToLower().Equals("rule") || tokens[1].ToLower().Equals("rulegroup")))
                    {
                        obj = adm.validateRepositoryMetadata(tokens[1], tokens[2]);
                    }
                    else
                    {
                        log.Add("Bad command syntax: " + line);
                        log.Add("usage: validatemetadata all");
                        log.Add("usage: validatemetadata rule/rulegroup ruleName/groupName");
                        return;
                    }

                    if (obj != null)
                    {
                        parseValidationOutput((XmlNode[])obj);
                    }
                    else
                    {
                        log.Add("Failed to execute command");
                    }
                    return;
                }
                else if (cname == "checkconnections")
                {
                    // Status: valid - able to open connection , invalid - not able to open connection
                    bool newRep = false;
                    MainAdminForm maf = Util.loadRepository(sp, out newRep);
                    if (maf == null)
                    {
                        log.Add("Not able to load repository while executing \"checkconnections\" command for space " + sp.ID.ToString());
                        this.exitStatus = GENERAL_ERROR;
                        return;
                    }
                    Connection[] connections = Util.getConnections(sp, maf);
                    foreach (Connection con in connections)
                    {
                        if (con.Realtime)
                        {
                            bool isValidConnection = LiveAccessUtil.sendCommandToLiveAccess(sp, con.Name, "checkconnection\r\ntimeout=120");
                            if (isValidConnection)
                            {
                                log.Add(con.VisibleName + ", " + "Active");
                            }
                            else
                            {
                                log.Add(con.VisibleName + ", " + "Inactive");
                            }
                        }
                        else
                        {
                            QueryConnection tConn = null;
                            try
                            {
                                string connectString = Util.getFullConnectString(con.ConnectString, con.UserName, con.Password);
                                tConn = new QueryConnection(connectString);
                                tConn.ConnectionTimeout = 120;
                                tConn.Open();
                                log.Add(con.VisibleName + ", " + "Active");
                            }
                            catch (Exception ex)
                            {
                                Global.systemLog.Info("Exception while executing checkconnections command for space: " + sp.ID.ToString() + ", Connection: " + con.VisibleName + " Error: " + ex.Message);
                                log.Add(con.VisibleName + ", " + "Inactive");
                            }
                            finally
                            {
                                if (tConn != null)
                                {
                                    try
                                    {
                                        if (tConn.State != ConnectionState.Closed && tConn.State != ConnectionState.Broken)
                                        {
                                            tConn.Close();
                                        }
                                        tConn.Dispose();
                                    }
                                    catch (Exception ex)
                                    {
                                        Global.systemLog.Info("Error while closing connection for checkconnection command for space: " + sp.ID.ToString() + ", Error: " + ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Global.systemLog.Info("Bad command syntax: " + line);
                    log.Add("Bad command syntax: " + line);
                    this.exitStatus = GENERAL_ERROR;
                    return;
                }
            }
            catch (SoapException sex) // CommandHelper exceptions
            {
                Global.systemLog.Error(sex.Message);
                log.Add(sex.Message);
                this.exitStatus = GENERAL_ERROR;
                return;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in Command Processor for line: " + line, ex);
                log.Add("Internal Error on: " + line);
                this.exitStatus = GENERAL_ERROR;
                return;
            }
        }

        /**
         * parse output of validatemetadata command
         * */
        private void parseValidationOutput(XmlNode[] xmlNode)
        {
            if (xmlNode != null && xmlNode.Length > 0 && xmlNode[0].HasChildNodes && xmlNode[0].ChildNodes.Count > 0)
            {
                /*
                 * xmlNode[0] = Root
                 * root.ChildNodes[0] = Result
                 * root.ChildNodes[0].ChildNodes = list of RuleValidationResult                 * 
                 * */
                XmlNode root = xmlNode[0].FirstChild;                              
                XmlNodeList list = root.ChildNodes[0].ChildNodes;
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].HasChildNodes && list[i].ChildNodes.Count >= 3)
                        {
                            log.Add("\n");
                            log.Add("Rule: " + list[i].ChildNodes[0].InnerText);  //RuleName
                            log.Add("ValidationMessage: " + list[i].ChildNodes[3].InnerText); //ValidationMessage
                            if (list[i].ChildNodes.Count >= 5)
                            {
                                for (int j = 0; j < list[i].ChildNodes[4].ChildNodes.Count; j++) {
                                    log.Add("  " + list[i].ChildNodes[4].ChildNodes[j].InnerText);
                                }
                            }
                        }
                    }
                }
                else
                {
                    log.Add("Invalid RuleName/RuleGroupName");
                }
                        
                /**
                 * Display reason if, in case, command execution gets failed
                 * */
                if (root.HasChildNodes && root.ChildNodes.Count > 0)
                {
                    XmlNodeList child = root.ChildNodes;
                    for (int i = 0; i < child.Count; i++)
                    {
                        if (child[i].Name.Equals("Reason"))
                        {
                            log.Add("\n");
                            log.Add("Failed to execute command");
                            log.Add("Reason: " + child[i].InnerText);
                        }
                    }                        
                }
            }
        }

        private StagingTable getStagingTableFromDisplayName(string displayName)
        {
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                string dname = ApplicationBuilder.getDisplayName(st);
                if (dname == displayName)
                {
                    return st;
                }
            }
            return null;
        }

        private void execprocess(Space sp, string cmd, string line)
        {
            // Create the ProcessInfo object
            string enginecmd = Util.getEngineCommand(sp);
            ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
            psi.Arguments = sp.Directory + "\\exec.cmd " + sp.Directory;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = false;
            psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            Global.systemLog.Info(cmd + " for space (" + sp.ID + "/" + line + ")");
            DateTime start = DateTime.Now;
            Process proc = Process.Start(psi);
            if (proc.HasExited)
            {
                Global.systemLog.Info(cmd + " for space (" + sp.ID + "/" + line + ") - Exited with code: " + proc.ExitCode);
                log.Add(cmd + " for space (" + line + ") completed");
            }
            else
            {
                Global.systemLog.Info(cmd + " for space (" + sp.ID + "/" + line + ") - Running");
                proc.WaitForExit();
                log.Add(proc.StandardOutput.ReadToEnd().Replace("\r\n", "\r"));
                DateTime end = DateTime.Now;
                TimeSpan diff = end.Subtract(start);
                double secdiff = diff.Minutes * 60 + diff.Seconds + ((double)diff.Milliseconds) / 1000;
                log.Add(cmd + " completed - " + secdiff + " seconds");
                Global.systemLog.Info(cmd + "  finished: " + sp.ID);
            }
            proc.Close();
        }

        private void processCommandFileInBackground(ProcessStartInfo psi, string name, User u, Space sp, List<string> log,bool delPublishLock)
        {
            StartInBackground sib = new StartInBackground(psi, name, u, sp, log, server.MapPath("~/"), delPublishLock);
            Thread t = new Thread(new ThreadStart(sib.run));
            t.IsBackground = true;
            t.Start();
        }

        private class StartInBackground
        {
            private ProcessStartInfo psi;
            private string name;
            private User u;
            private Space sp;
            private List<string> log;
            private string path;
            private bool delPublishLock;

            public StartInBackground(ProcessStartInfo psi, string name, User u, Space sp, List<string> log, string path, bool delPublishLock)
            {
                this.psi = psi;
                this.name = name;
                this.u = u;
                this.sp = sp;
                this.log = log;
                this.path = path;
                this.delPublishLock = delPublishLock;
            }

            public void run()
            {
                try
                {
                Process proc = Process.Start(psi);
                if (proc.HasExited)
                {
                        Global.systemLog.Info(name + " for space (" + sp.ID + ") - Exited with code: " + proc.ExitCode);
                        log.Add(name + " for space (" + sp.ID + ") completed");
                }
                else
                {
                        Global.systemLog.Info(name + " for space (" + sp.ID + ") - Running");
                    proc.WaitForExit();
                    log.Add(proc.StandardOutput.ReadToEnd());
                        log.Add(name + " for space (" + sp.ID + ") completed");
                        Global.systemLog.Info(name + " process finished: " + sp.ID);
                }
                proc.Close();                
                PUT_Handler.sendCommandEmail(u, sp, path, log);
                }
                finally
                {
                    if (delPublishLock)
                    {
                        try
                        {
                            File.Delete(sp.Directory + "\\publish.lock");
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Info("Unable to delete publish.lock for space - " + ex.Message);
                        }
                    }
                }
            }
        }

        public string getModelDir(string directory, MainAdminForm maf)
        {
            return directory + "\\" + Performance_Optimizer_Administration.Util.generatePhysicalName(maf.appName.Text);
        }

        public static void sendMailAfterPasswordReset(User user, HttpServerUtility server, HttpRequest request, string password)
        {
            // send the email (should really encapsulate this)
            MailMessage mm = new MailMessage();
            mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
            mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
            mm.To.Add(new MailAddress(user.Email));
            mm.Subject = "Birst Password Reset";
            mm.IsBodyHtml = true;
            StreamReader reader = new StreamReader(server.MapPath("~/") + "ResetPassword2.htm");
            mm.Body = reader.ReadToEnd();
            reader.Close();
            mm.Body = mm.Body.Replace("${LINK}", Util.getRequestBaseURL(request) + "/Login.aspx");
            mm.Body = mm.Body.Replace("${PASSWORD}", password);
            SmtpClient smtpc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                smtpc.EnableSsl = true;
            smtpc.Send(mm);
        }
    }
}
