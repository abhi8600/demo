﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class PublishLogResult
    {
        public PublishLogSummary[] Summary;
        public DimensionTableResult[] DimensionResult;
        public MeasureTableResult[] MeasureResult;
        public ErrorOutput Error;
    }

    public class PublishLogSummary
    {
        public string DataSource;
        public string Processed;
        public string Errors;
        public string Warnings;
        public long Duration;
    }

    public class DimensionTableResult
    { 
        public string Dimension;
        public string Level;
        public string Type;
        public string DataSource;
        public string Rows;
        public string Columns;
        public string ID;
        public int Load;
        public string Group;
        public string Date;
        public long Duration;
    }

    public class MeasureTableResult
    {        
        public string Grain;
        public string Type;
        public string DataSource;
        public string Rows;
        public string Columns;
        public string ID;
        public int Load;
        public string Group;
        public string Date;
        public long Duration;
    }
}
