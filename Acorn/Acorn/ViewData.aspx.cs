﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Text;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class ViewData : System.Web.UI.Page
    {
        private const int NUM_RECORDS_TO_SHOW = 1000;

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Debug("redirecting to home page due to not being the space admin (admin)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            if (IsPostBack)
                return;
            bindTables();
        }

        private void bindTables()
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            DataTable gt = new DataTable();
            gt.Columns.Add("Grain");
            Dictionary<string, string[][]> grains = CustomCalculations.getGrains(Session);
            foreach (string s in grains.Keys)
            {
                DataRow dr = gt.NewRow();
                dr[0] = s;
                gt.Rows.Add(dr);
            }
            MetricTablesView.DataSource = gt;
            MetricTablesView.DataBind();
            DataTable ht = new DataTable();
            ht.Columns.Add("Level");
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                if (maf.timeDefinition != null && h.DimensionName == maf.timeDefinition.Name)
                    continue;
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    if (!l.GenerateDimensionTable)
                        continue;
                    string name = h.DimensionName + "." + l.Name;
                    DataRow dr = ht.NewRow();
                    dr[0] = name;
                    ht.Rows.Add(dr);
                }
            }
            HierarchyTablesView.DataSource = ht;
            HierarchyTablesView.DataBind();
            DataTable st = new DataTable();
            st.Columns.Add("DataSource");
            foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
            {
                DataRow dr = st.NewRow();
                dr[0] = sst.Name.Substring(3);
                st.Rows.Add(dr);
            }
            StagingView.DataSource = st;
            StagingView.DataBind();
            ShowTables.Visible = true;
            ShowData.Visible = false;
        }

        protected void MeasureCommand(object sender, GridViewCommandEventArgs e)
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            Dictionary<string, string[][]> grains = CustomCalculations.getGrains(Session);
            string[][] grain = null;
            string grainkey = null;
            int count = 0;
            int index = Convert.ToInt32(e.CommandArgument);
            foreach (string s in grains.Keys)
            {
                if (count == index)
                {
                    grain = grains[s];
                    grainkey = s;
                    break;
                }
                count++;
            }
            MeasureTable foundmt = null;
            MeasureGrain mg = new MeasureGrain();
            mg.measureTableGrains = new MeasureTableGrain[grain.Length];
            for (int i = 0; i < grain.Length; i++)
            {
                mg.measureTableGrains[i] = new MeasureTableGrain();
                mg.measureTableGrains[i].DimensionName = grain[i][0];
                mg.measureTableGrains[i].DimensionLevel = grain[i][1];
            }
            ApplicationBuilder ab = new ApplicationBuilder(maf);
            string mtname = ab.getLogicalMeasureTableName(null, mg);
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (mt.TableName == mtname)
                {
                    foundmt = mt;
                    break;
                }
            }
            LoadIDPlaceHolder.Visible = false;
            if (foundmt != null)
            {
                if (e.CommandName == "MeasureTableClick")
                {
                    Session["datatable"] = foundmt;
                    bindOnPhysicalTable(foundmt, null, SortDirection.Ascending);
                }
                else if (e.CommandName == "MeasureTableProfile")
                {
                    Session["datatable"] = foundmt;
                    LoadIDPlaceHolder.Visible = true;
                    LoadList.Items.Clear();
                    LoadList.Items.Add("ALL");
                    Space sp = (Space)Session["space"];
                    for (int i = 1; i <= sp.LoadNumber; i++)
                    {
                        LoadList.Items.Add(i.ToString());
                    }
                    LoadList.SelectedIndex = 0;
                    bindOnProfile(foundmt, maf, 0);
                }
            }
            NameLabel.Text = grainkey;
        }

        protected void LoadListChanged(object sender, EventArgs e)
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            bindOnProfile(Session["datatable"], maf, LoadList.SelectedIndex);
        }

        protected void LevelCommand(object sender, GridViewCommandEventArgs e)
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            LoadIDPlaceHolder.Visible = false;
            int index = Convert.ToInt32(e.CommandArgument);
            Hierarchy sh = null;
            Level sl = null;
            int count = 0;
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                if (maf.timeDefinition != null && h.DimensionName == maf.timeDefinition.Name)
                    continue;
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    if (!l.GenerateDimensionTable)
                        continue;
                    if (count++ == index)
                    {
                        sh = h;
                        sl = l;
                        break;
                    }
                }
                if (sh != null)
                    break;
            }
            if (sh == null)
                return;
            DimensionTable founddt = null;
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (dt.DimensionName == sh.DimensionName && dt.Level == sl.Name && (dt.InheritTable == null || dt.InheritTable.Length == 0))
                {
                    founddt = dt;
                    break;
                }
            }
            if (founddt != null)
            {
                if (e.CommandName == "LevelClick")
                {
                    Session["datatable"] = founddt;
                    bindOnPhysicalTable(founddt, null, SortDirection.Ascending);
                }
                else if (e.CommandName == "LevelProfile")
                {
                    bindOnProfile(founddt, maf, 0);
                }
                NameLabel.Text = founddt.DimensionName + "." + founddt.Level;
            }
        }

        protected void StagingCommand(object sender, GridViewCommandEventArgs e)
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            LoadIDPlaceHolder.Visible = false;
            int index = Convert.ToInt32(e.CommandArgument);
            StagingTable st = maf.stagingTableMod.getStagingTables()[index];
            if (e.CommandName == "DataSourceClick")
            {
                Session["datatable"] = st;
                bindOnPhysicalTable(st, null, SortDirection.Ascending);
            }
            else if (e.CommandName == "DataSourceProfile")
            {
                bindOnProfile(st, maf, 0);
            }
            NameLabel.Text = st.Name.Substring(3);
        }

        private string getLogicalName(Object o, string physicalName)
        {
            if (typeof(DimensionTable).Equals(o.GetType()))
            {
                foreach (DimensionColumn dc in ((DimensionTable)o).DimensionColumns)
                {
                    if (dc.Qualify && dc.PhysicalName == physicalName)
                        return dc.ColumnName;
                }
            }
            else if (typeof(MeasureTable).Equals(o.GetType()))
            {
                foreach (MeasureColumn mc in ((MeasureTable)o).MeasureColumns)
                {
                    if (mc.Qualify && mc.PhysicalName == physicalName)
                        return mc.ColumnName;
                }
            }
            else if (typeof(StagingTable).Equals(o.GetType()))
            {
                foreach (StagingColumn sc in ((StagingTable)o).Columns)
                {
                    if (Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name)+'$' == physicalName)
                        return sc.Name;
                }
            }
            return physicalName;
        }

        private string getPhysicalName(Object o, string logicalName)
        {
            if (typeof(DimensionTable).Equals(o.GetType()))
            {
                foreach (DimensionColumn dc in ((DimensionTable)o).DimensionColumns)
                {
                    if (dc.Qualify && dc.ColumnName == logicalName)
                        return dc.PhysicalName;
                }
            }
            else if (typeof(MeasureTable).Equals(o.GetType()))
            {
                foreach (MeasureColumn mc in ((MeasureTable)o).MeasureColumns)
                {
                    if (mc.Qualify && mc.ColumnName == logicalName)
                        return mc.PhysicalName;
                }
            }
            else if (typeof(StagingTable).Equals(o.GetType()))
            {
                foreach (StagingColumn sc in ((StagingTable)o).Columns)
                {
                    if (sc.Name == logicalName)
                        return Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name) + "$";
                }
            }
            return logicalName;
        }

        protected void bindOnPhysicalTable(object o, string sortExpression, SortDirection sd)
        {
            Space sp = (Space)Session["space"];
            string pname = null;
            string tname = null;
            bool dimension = typeof(DimensionTable).Equals(o.GetType());
            bool measure = typeof(MeasureTable).Equals(o.GetType());
            bool staging = typeof(StagingTable).Equals(o.GetType());
            if (dimension)
            {
                pname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                tname = ((DimensionTable)o).TableName;
            }
            else if (measure)
            {
                pname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                tname = ((MeasureTable)o).TableName;
            }
            else if (staging)
            {
                pname = ((StagingTable)o).Name;
                tname = ((StagingTable)o).Name.Substring(3);
            }
            string sortclause = "";
            if (sortExpression != null)
                sortclause = " ORDER BY " + getPhysicalName(o, sortExpression) + (sd == SortDirection.Ascending ? " ASC" : " DESC");
            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                QueryDataAdapter adapt = new QueryDataAdapter("SELECT TOP " + NUM_RECORDS_TO_SHOW + " * FROM " + sp.Schema + "." + pname + sortclause, conn);
                DataTable dt = new DataTable(tname);
                adapt.Fill(dt);
                List<int> hidden = new List<int>();
                DataView.Columns.Clear();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dimension && dt.Columns[i].ColumnName.EndsWith("_CKSUM$") && dt.Columns[i].ColumnName.StartsWith("ST_"))
                        continue;
                    else if (staging && dt.Columns[i].ColumnName.EndsWith("_CKSUM$"))
                        continue;
                    else
                        dt.Columns[i].ColumnName = getLogicalName(o, dt.Columns[i].ColumnName);
                    BoundField bf = new BoundField();
                    bf.DataField = dt.Columns[i].ColumnName;
                    bf.HeaderText = bf.DataField;
                    bf.SortExpression = bf.DataField;
                    DataView.Columns.Add(bf);
                }
                DataView.DataSource = dt;
                DataView.DataBind();
                foreach (int i in hidden)
                    DataView.Columns[i].Visible = false;
                ShowTables.Visible = false;
                ShowData.Visible = true;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void DataSort(object sender, GridViewSortEventArgs e)
        {
            object o = Session["datatable"];
            if (o == null)
                return;
            string sortexp = (string)Session["sortexpression"];
            object sdo = Session["sortdirection"];
            SortDirection sd = sdo == null ? SortDirection.Ascending : (SortDirection)sdo;
            if (sortexp == e.SortExpression)
                sd = (sd == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            bindOnPhysicalTable(o, e.SortExpression, sd);
            Session["sortexpression"] = e.SortExpression;
            Session["sortdirection"] = sd;
        }

        protected void bindOnProfile(object o, MainAdminForm maf, int loadNumber)
        {
            Space sp = (Space)Session["space"];
            string pname = null;
            string tname = null;
            bool dimension = typeof(DimensionTable).Equals(o.GetType());
            bool measure = typeof(MeasureTable).Equals(o.GetType());
            bool staging = typeof(StagingTable).Equals(o.GetType());
            StringBuilder columns = new StringBuilder();
            DataTable resultdt = new DataTable();
            resultdt.Columns.Add("Column");
            resultdt.Columns.Add("Count", typeof(int));
            resultdt.Columns.Add("Count Distinct", typeof(int));
            resultdt.Columns.Add("Sum", typeof(double));
            resultdt.Columns.Add("Average", typeof(double));
            resultdt.Columns.Add("Min", typeof(object));
            resultdt.Columns.Add("Max", typeof(object));
            resultdt.Columns.Add("Null Count", typeof(int));
            List<string> colNames = new List<string>();
            List<string> colDisplayNames = new List<string>();
            if (dimension)
            {
                pname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                tname = ((DimensionTable)o).TableName;
                DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { tname });
                foreach (DataRowView dr in drv)
                {
                    if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                    {
                        if (columns.Length > 0)
                            columns.Append(',');
                        string cname = Performance_Optimizer_Administration.Util.generatePhysicalName((string)dr["ColumnName"]);
                        string cpname = (string)dr["PhysicalName"];
                        columns.Append("COUNT(" + cpname + ")");
                        columns.Append(",COUNT(DISTINCT " + cpname + ")");
                        DataRowView[] drv2 = maf.dimensionColumnNamesView.FindRows(new object[] { ((DimensionTable)o).DimensionName, (string)dr["ColumnName"] });
                        string dtype = null;
                        if (drv2.Length > 0)
                        {
                            dtype = (string)drv2[0]["DataType"];
                        }
                        if (dtype == "Integer" || dtype == "Number" || dtype == "Float")
                        {
                            columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                            columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                        }
                        else
                        {
                            columns.Append(",NULL,NULL");
                        }
                        columns.Append(",MIN(" + cpname + ")");
                        columns.Append(",MAX(" + cpname + ")");
                        columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                        colNames.Add(cname);
                        colDisplayNames.Add((string)dr["ColumnName"]);
                    }
                }
            }
            else if (measure)
            {
                pname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                tname = ((MeasureTable)o).TableName;
                DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { tname });
                foreach (DataRowView dr in drv)
                {
                    if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                    {
                        if (columns.Length > 0)
                            columns.Append(',');
                        string cname = Performance_Optimizer_Administration.Util.generatePhysicalName((string)dr["ColumnName"]);
                        string cpname = (string)dr["PhysicalName"];
                        columns.Append("COUNT(" + cpname + ")");
                        columns.Append(",COUNT(DISTINCT " + cpname + ")");
                        DataRowView[] drv2 = maf.measureColumnNamesView.FindRows(new object[] { (string)dr["ColumnName"] });
                        string dtype = null;
                        if (drv2.Length > 0)
                        {
                            dtype = (string)drv2[0]["DataType"];
                        }
                        if (dtype == "Integer" || dtype == "Number" || dtype == "Float")
                        {
                            columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                            columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                        }
                        else
                        {
                            columns.Append(",NULL,NULL");
                        }
                        columns.Append(",MIN(" + cpname + ")");
                        columns.Append(",MAX(" + cpname + ")");
                        columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                        colNames.Add(cname);
                        colDisplayNames.Add((string)dr["ColumnName"]);
                    }
                }
            }
            else if (staging)
            {
                pname = ((StagingTable)o).Name;
                tname = ((StagingTable)o).Name.Substring(3);
                StagingTable st = (StagingTable)o;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.SourceFileColumn == null)
                        continue;
                    if (columns.Length > 0)
                        columns.Append(',');
                    string cname = sc.Name;
                    string cpname = Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name) + '$';
                    columns.Append("COUNT(" + cpname + ")");
                    columns.Append(",COUNT(DISTINCT " + cpname + ")");
                    string dtype = sc.DataType;
                    if (dtype == "Integer" || dtype == "Float" || dtype == "Number")
                    {
                        columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                        columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                    }
                    else
                    {
                        columns.Append(",NULL,NULL");
                    }
                    columns.Append(",MIN(" + cpname + ")");
                    columns.Append(",MAX(" + cpname + ")");
                    columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                    colNames.Add(cname);
                    colDisplayNames.Add(cname);
                }
            }

            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                QueryCommand cmd = new QueryCommand("SELECT " + columns + " FROM " + sp.Schema + "." + pname +
                    (loadNumber > 0 ? " WHERE LOAD_ID=" + loadNumber : ""), conn);
                QueryReader reader = cmd.ExecuteReader();
                int NUM_AGGS = 7;
                if (reader.Read())
                {
                    for (int i = 0; i < colNames.Count; i++)
                    {
                        DataRow dr = resultdt.NewRow();
                        dr[0] = colDisplayNames[i];
                        dr[1] = reader.GetInt32(i * NUM_AGGS);
                        dr[2] = reader.GetInt32((i * NUM_AGGS) + 1);
                        if (!reader.IsDBNull((i * NUM_AGGS) + 2))
                            dr[3] = reader.GetDouble((i * NUM_AGGS) + 2);
                        if (!reader.IsDBNull((i * NUM_AGGS) + 3))
                            dr[4] = reader.GetDouble((i * NUM_AGGS) + 3);
                        if (!reader.IsDBNull((i * NUM_AGGS) + 4))
                            dr[5] = reader.GetValue((i * NUM_AGGS) + 4);
                        if (!reader.IsDBNull((i * NUM_AGGS) + 5))
                            dr[6] = reader.GetValue((i * NUM_AGGS) + 5);
                        if (!reader.IsDBNull((i * NUM_AGGS) + 6))
                            dr[7] = reader.GetInt32((i * NUM_AGGS) + 6);
                        resultdt.Rows.Add(dr);
                    }
                }
                reader.Close();
                DataView.Columns.Clear();
                DataView.CellPadding = 2;
                for (int i = 0; i < resultdt.Columns.Count; i++)
                {
                    BoundField bf = new BoundField();
                    bf.DataField = resultdt.Columns[i].ColumnName;
                    bf.HeaderText = bf.DataField;
                    if (i > 0)
                    {
                        bf.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    }
                    DataView.Columns.Add(bf);
                }
                DataView.DataSource = resultdt;
                DataView.DataBind();
                ShowTables.Visible = false;
                ShowData.Visible = true;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
