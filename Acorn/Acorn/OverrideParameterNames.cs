﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class OverrideParameterNames
    {
        public static string HELP_URL = "HELP_URL";
        public static string CONTEXT_HELP_URL = "CONTEXT_HELP_URL";
        public static string COPYRIGHT_TEXT = "COPYRIGHT_TEXT";
        public static string LOGO_PATH = "LOGO_PATH";
        public static string ERROR_URL = "ERROR_URL";
        public static string TERMS_OF_SERVICE_URL = "TERMS_OF_SERVICE_URL";
        public static string PRIVACY_POLICY_URL = "PRIVACY_POLICY_URL";
        public static string CONTACT_US_URL = "CONTACT_US_URL";
        public static string PAGE_TITLE = "PAGE_TITLE";
        public static string SUPPORT_URL = "SUPPORT_URL";
        public static string CUSTOM_UPLOAD_HEADER_URL = "CUSTOM_UPLOAD_HEADER_URL";
        public static string FAVICON_ICON = "FAVICON_ICON";
        public static string LOGOUT_URL = "LOGOUT_URL";
        public static string INACTIVITY_TIMEOUT = "INACTIVITY_TIMEOUT"; // timeout is in minutes
        public static string ENABLE_ASYNC_REPORT_GENERATION = "ENABLE_ASYNC_REPORT_GENERATION";
        public static string USER_DISABLE_INACTIVITY_TIMEOUT = "USER_DISABLE_INACTIVITY_TIMEOUT"; // timeout is in days, after that the user is disabled (enabling must reset last logout)
    }
}
