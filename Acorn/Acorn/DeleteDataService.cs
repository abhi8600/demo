﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.Background;
using Acorn.Utils;
using Acorn.Exceptions;
using Acorn.DBConnection;
using System.Text;

namespace Acorn
{
    public class DeleteDataService
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public static DeleteDataOutput canDeleteLast(HttpSessionState session)
        {
            DeleteDataOutput response = new DeleteDataOutput();
            ErrorOutput error = new ErrorOutput();
            response.errorOutput = error;

            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {                
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";               
                return response;
            }
            setDeleteLastOption(session, response);
            return response;
        }
        
        public static DeleteDataOutput deleteSpace(HttpSessionState session, HttpRequest request)
        {
            DeleteDataOutput response = new DeleteDataOutput();
            ErrorOutput error = new ErrorOutput();
            response.errorOutput = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            session["deletespace"] = true;
            session.Remove("deletealldata");
            session.Remove("deletelast");
            session.Remove("deleteoverview");            
            logout(session, request);
            delete(session, response);            
            return response;
        }

        public static DeleteDataOutput deleteAll(HttpSessionState session, HttpRequest request)
        {

            DeleteDataOutput response = new DeleteDataOutput();
            ErrorOutput error = new ErrorOutput();
            response.errorOutput = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            session["deletealldata"] = true;
            session.Remove("deletespace");
            session.Remove("deletelast");
            session.Remove("deleteoverview");            
            logout(session, request);
            delete(session, response);
            setDeleteLastOption(session, response);
            Util.resetDesignerAndDashboardViewablePermissions(session);
            return response;
        }

        public static DeleteDataOutput deleteLast(HttpSessionState session, HttpRequest request, bool restore)
        {
            DeleteDataOutput response = new DeleteDataOutput();
            ErrorOutput error = new ErrorOutput();
            response.errorOutput = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            session["deletelast"] = true;
            session.Remove("deletealldata");
            session.Remove("deletespace");
            session.Remove("deleteoverview");
            logout(session, request);
            delete(session, restore, response);
            setDeleteLastOption(session, response);
            return response;
        }


        public static DeleteDataOutput deleteOverview(HttpSessionState session)
        {

            DeleteDataOutput response = new DeleteDataOutput();
            ErrorOutput error = new ErrorOutput();
            response.errorOutput = error;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            session["deleteoverview"] = true;
            session.Remove("deletealldata");
            session.Remove("deletespace");
            session.Remove("deletelast");
            delete(session, response);
            setDeleteLastOption(session, response);
            return response;
        }

        private static void logout(HttpSessionState session, HttpRequest request)
        {
            Space sp = (Space)session["space"];            
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            Util.logoutSMIWebSession(session, sp, request);
        }

        private static void setDeleteLastOption(HttpSessionState session, DeleteDataOutput output)
        {
            Space sp = Util.getSessionSpace(session);
            if (sp.LoadNumber == 0)
            {
                output.deleteLast = false;
            }
            else
            {
                output.deleteLast = true;
            }

        }

        private static void delete(HttpSessionState session, DeleteDataOutput output)
        {
            delete(session, false, output);
        }


        private static void delete(HttpSessionState session, bool restore, DeleteDataOutput output)
        {
            ErrorOutput error = output.errorOutput;
            if (error == null)
            {
                error = new ErrorOutput();
                output.errorOutput = error;
            }

            Space sp = Util.getSessionSpace(session);
            // is the space busy in other operations.
            int spOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            if (!SpaceOpsUtils.isSpaceAvailable(spOpID))
            {   
                Global.systemLog.Error("delete operation: " + SpaceOpsUtils.getAvailabilityMessage(sp.Name, spOpID));
                output.spaceInUse = true;
                return;
            }

            // check for the processing status for sp1
            bool spacePublishing = false;
            try
            {
                spacePublishing = Util.checkForPublishingStatus(sp);
            }
            catch (System.Xml.XmlException sx)
            {
                // if there is an xml loading exception and operation is delete space, ignore exception and continue to 
                // delete space. This allows any space with corrupted xml file like dcconfig to be deleted
                if (session["deletespace"] == null)
                {
                    throw sx;
                }
            }

            if (spacePublishing)
            {
                Global.systemLog.Error("delete operation: " + sp.Name + " is being published");
                output.spaceInUse = true;
                return;
            }

            if (session["deletespace"] != null)
            {
                session.Remove("deletespace");
                //LogoutPanel.Visible = false;
                Status.StatusResult status = (Status.StatusResult)session["status"];
                bool result = true;
                if (status == null || !Status.isRunningCode(status.code))
                {
                    result = deleteSpace(sp, (User)session["user"], session);
                }
                
                if (!result || (status != null && Status.isRunningCode(status.code)))
                {
                    output.spaceInUse = true;
                    Global.systemLog.Warn("Could not delete the space (" + sp.Name + ") due to the system thinking that there is a load in progress");
                    return;
                }
                else
                {
                    session.Remove("space");
                    //DeletePanel.CssClass = "invisible";
                    output.isDeleted = true;
                    return;
                }
            }
            else if (session["deletealldata"] != null || session["deletelast"] != null)
            {
                Status.StatusResult status = (Status.StatusResult)session["status"];
                bool allDelete = false;
                if (session["deletealldata"] != null)
                {
                    allDelete = true;
                }
                session.Remove("deletealldata");
                session.Remove("deletelast");             
                // Get whether anything is still running
                if (status != null && Status.isRunningCode(status.code))
                {
                    output.spaceInUse = true;
                    Global.systemLog.Warn("Could not delete the space (" + sp.Name + ") due to the system thinking that there is a load in progress");
                    return;                    
                }
                DeleteDataService.deleteData(sp, allDelete, restore, session, null, output);
                
                output.isDeleted = true;
                return;
            }
            else if (session["deleteoverview"] != null)
            {
                MainAdminForm maf = (MainAdminForm)session["MAF"];
                if (maf.quickDashboards != null)
                {
                    maf.quickDashboards = null;
                    Util.saveApplication(maf, sp, session, null);
                    if (Directory.Exists(sp.Directory + "\\catalog\\shared\\Auto-generated"))
                    {
                        DirectoryInfo di = new DirectoryInfo(sp.Directory + "\\catalog\\shared\\Auto-generated");
                        foreach (FileInfo fi in di.GetFiles())
                            fi.Delete();
                        di.Delete();
                    }
                }
                session.Remove("deleteoverview");
                output.isDeleted = true;
                return;                
            }
        }

        public static bool deleteSpace(Space sp, User user, HttpSessionState session)
        {
            bool result = true;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                result = Database.deleteSpace(conn, mainSchema, sp, session);
                signalSMIWebToClearMongoDBCache(sp);
                Database.setShareQuotaUsage(conn, mainSchema, user);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return result;
        }

        private static void signalSMIWebToClearMongoDBCache(Space sp)
        {
            if (sp == null)
            {
                Global.systemLog.Warn("Space cannot be null. Skipping sending signal to SMIWeb to clear Mongo DB Cache");
                return;
            }

            Acorn.TrustedService.TrustedService ts = null;  
            try
            {
                ts = new Acorn.TrustedService.TrustedService();    
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                ts = new Acorn.TrustedService.TrustedService();
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                ts.clearMongoDBAndFileCaches(sp.ID.ToString());
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while sending Clear Cache Signal to SMIWeb for " + sp.ID, ex);
            }
        }

        public static void deleteData(Space sp, bool deleteAll, bool restoreRepository, System.Web.SessionState.HttpSessionState Session, User u, DeleteDataOutput response)
        {
            string cmd = Util.getDeleteDataCommand(sp);
            List<Guid> statusTableCache = null;
            MainAdminForm maf = null;
            if (Session != null)
            {
                statusTableCache = (List<Guid>)Session["statustablecache"];
                maf = (MainAdminForm)Session["MAF"];
            }
            else
            {
                maf = Util.setSpace(null, sp, u);
            }

            if (statusTableCache != null)
            {
                statusTableCache.Remove(sp.ID);
            }

            if (u == null && Session != null)
            {
                u = Util.getSessionUser(Session);
            }

            int toDeleteLoadNumber = ProcessLoad.getLoadNumber(sp, Util.LOAD_GROUP_NAME);
            DeleteDataThread ddt = new DeleteDataThread(sp, maf, deleteAll, cmd, Session, mainSchema, restoreRepository, toDeleteLoadNumber, u);
            // bugfix 8767, Remove the earlier entry for the load number and put in the start of DeleteData                    
            bool updatedTxnStatus = Status.updateTxnStatusWithNextOperation(sp, toDeleteLoadNumber, Status.OP_DELETE_DATA, false);
            Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            if (localETLConfigs != null && localETLConfigs.Count > 0)
            {
                foreach (string lg in localETLConfigs.Keys)
                {
                    LocalETLConfig leConfig = localETLConfigs[lg];
                    Status.updateTxnStatusWithNextOperationOnLiveAccessConnection(sp, toDeleteLoadNumber, Status.OP_DELETE_DATA, false, leConfig);
                }
            }
            if (!updatedTxnStatus)
            {
                ErrorOutput error = new ErrorOutput();
                response.errorOutput = error;
                error.ErrorMessage = "Error while starting delete operation. Please try again";
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                
                Global.systemLog.Error("DeleteData failed during update of Status table");
                return;
            }

            if (Util.useExternalSchedulerForDeletion(sp, maf))
            {
                SchedulerUtils.scheduleDeleteJob(sp.ID, u.ID, deleteAll, restoreRepository, toDeleteLoadNumber, Util.LOAD_GROUP_NAME);
            }
            else
            {
                Util.setSessionDeleteLastLoadNumber(Session, toDeleteLoadNumber);
                BackgroundProcess.startModule(new BackgroundTask(ddt, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
            }
            
            if (Session != null)
                Session["status"] = new Status.StatusResult(Status.StatusCode.DeleteData);
            if (deleteAll || toDeleteLoadNumber == 1)
            {
                sp.LoadNumber = 0;
            }
        }

        
        public static string getDeleteCmdArguments(Space sp, int loadNumber)
        {
            string args = sp.Directory + "\\repository_dev.xml" + " " + loadNumber + " " + sp.Directory;
            PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
            if (peVersion != null && peVersion.IsProcessingGroupAware)
            {
                string checkInFileName = sp.Directory + "\\checkIn-delete.txt";
                return  args + " " + checkInFileName;
            }

            return args;
        }

        public static string getDeleteCmdArguments(Space sp, int loadNumber, string partitionConnectionName)
        {
            return sp.Directory + "\\delete_" + partitionConnectionName + ".cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber) + " \"." + partitionConnectionName + "\"";
        }

        public static void performDeleteAllOperation(Space sp, User user, MainAdminForm maf)
        {
            SpaceOperationStatus spaceOperation = null;
            try
            {                
                spaceOperation = new SpaceOperationStatus(sp, user);
                spaceOperation.logBeginStatus(SpaceOpsUtils.OP_DELETE_DATA);
                DeleteDataService.deletePublishedDataAndLogs(sp);
                cleanPublishTableAndConsolidatedData(null, sp, user, maf, -1);
            }
            finally
            {                
                if (spaceOperation != null)
                {
                    spaceOperation.logEndStatus(SpaceOpsUtils.OP_DELETE_DATA);
                }
            }

        }

        public static void deletePublishedDataAndLogs(Space sp)
        {
            // clean publish data 
            QueryConnection spconn = null;
            try
            {
                spconn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                Database.deleteSpacePublishedData(spconn, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(spconn);
            }
            Global.systemLog.Info("Deleted all published data for space: " + sp.ID);

            try
            {
                DeleteDataService.deleteLogs(sp);
                Global.systemLog.Info("Deleted logs for space: " + sp.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Info("Deleting log files failed: " + ex.Message);
            }
            if (sp.QueryConnectionName != null && sp.QueryConnectionName == "IB Connection")
            {
                spconn = null;
                try
                {
                    spconn = ConnectionPool.getConnection(sp.getQueryFullConnectString());
                    Database.deleteSpacePublishedData(spconn, sp);
                }
                finally
                {
                    ConnectionPool.releaseConnection(spconn);
                }
                Global.systemLog.Info("Deleted all published query data for space: " + sp.ID);
            }
        }


        public static void restoreSavedRepository(HttpSessionState session, Space sp, User user, int loadNumber)
        {
            if (loadNumber > 0)
            {
                Util.FileCopy(sp.Directory + "\\repository_dev.xml." + (loadNumber) + ".save", sp.Directory + "\\repository_dev.xml", true);
                if (session != null)
                {
                    Util.setSpace(session, sp, null, false);
                    // setSpace will clear out all the session variable
                    session["status"] = new Status.StatusResult(Status.StatusCode.DeleteData);
                }
                else
                    Util.setSpace(null, sp, user, false);
                Global.systemLog.Info("Repository copied and space reset for space: " + sp.ID);
            }
        }

        public static void deleteBirstLocal(Space sp, MainAdminForm maf, int loadNumber)
        {
            Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            if (localETLConfigs != null && localETLConfigs.Count > 0)
            {
                foreach (string lg in localETLConfigs.Keys)
                {
                    LocalETLConfig leConfig = localETLConfigs[lg];
                    StringBuilder commands = LiveAccessUtil.getLiveAccessDeleteLastCommands(sp, leConfig.getApplicationDirectory(), loadNumber);
                    File.WriteAllText(sp.Directory + "\\delete." + lg + ".properties", commands.ToString());
                    StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                    File.WriteAllText(sp.Directory + "\\customer." + lg + ".properties", overrideProperties.ToString());
                    Global.systemLog.Debug("Live access delete last load started for load number " + loadNumber + ", load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                    bool compeletedDeleteLastLoadOnLiveAccess = LiveAccessUtil.sendCommandToLiveAccess(sp, leConfig.getLocalETLConnection(), "liveaccessdeletelastload\r\n" + loadNumber);
                    if (!compeletedDeleteLastLoadOnLiveAccess)
                    {
                        Global.systemLog.Error("Live access delete last load for load number " + loadNumber + " could not be compeleted for load group: " + lg);
                        continue;
                    }
                    Global.systemLog.Debug("Live access delete last load completed for load number " + loadNumber + ", load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                }
            }
        }

        public static void preDeleteLastOperation(HttpSessionState session, Space sp, User user, MainAdminForm maf, bool restoreRepository, int loadNumber)
        {
            if (restoreRepository)
            {
                restoreSavedRepository(session, sp, user, loadNumber - 1);
            }
        }

        public static void postDeleteLastOperation(Space sp, User user, MainAdminForm maf, int loadNumber)
        {
            try
            {   
                cleanPublishTableAndConsolidatedData(null, sp, user, maf, loadNumber);
            }
            finally
            {
                SpaceOpsUtils.markSpaceAvailable(sp, user, SpaceOpsUtils.OP_DELETE_DATA);
            }
        }

        public static void cleanPublishTableAndConsolidatedData(HttpSessionState session, Space sp, User user, MainAdminForm maf, int loadNumber)
        {
            QueryConnection conn1 = null;
            try
            {
                conn1 = ConnectionPool.getConnection();
                Database.removePublishes(conn1, mainSchema, sp.ID, loadNumber);
                Global.systemLog.Info("Removed publish history for space: " + sp.ID);
                Dictionary<string, int> curLoads = Database.getCurrentLoadNumbers(conn1, mainSchema, sp.ID);
                if (curLoads != null && curLoads.Count > 0)
                {
                    foreach (string loadGroup in curLoads.Keys)
                    {
                        if (loadGroup == Util.LOAD_GROUP_NAME || loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                        {
                            sp.LoadNumber = curLoads[loadGroup];
                            break;
                        }
                    }
                }
                else
                    sp.LoadNumber = 0;
                Util.setLoadIDFilters(maf, sp.LoadNumber, curLoads);
                Database.updateNonDBSpaceParams(conn1, mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn1);
            }
            Global.systemLog.Info("Updated space information from delete for space: " + sp.ID);
            // Clear the cache
            Util.saveApplication(maf, sp, session, user);
            // "publish" repository
            Util.FileCopy(sp.Directory + "\\repository_dev.xml", sp.Directory + "\\repository.xml", true); 

            /*
             * Delete any consolidated data
             */
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.Consolidation)
                {
                    SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (sf != null)
                    {
                        ApplicationUploader.truncateFile(sp, sf);
                    }
                }
            }
            
            Global.systemLog.Info("Finished delete for space: " + sp.ID);
            if (session != null)
                session["status"] = new Status.StatusResult(Status.StatusCode.Ready);
        }

        public static void deleteLogs(Space sp)
        {
            DirectoryInfo di = new DirectoryInfo(sp.Directory + "\\logs");
            foreach (FileInfo fi in di.GetFiles())
                fi.Delete();
        }


    }
}
