﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Acorn
{
    [Serializable]
    public class ScanSummaryResult
    {
        public string cleanName;

        public bool wellFormed = true;
        public int[] keys;
        public List<int> defectiveRows;

        private ScanResults sr;

        public ScanSummaryResult()
        {
        }

        public void setScanResult(ScanResults sr)
        {
            this.sr = sr;
        }

        public void setWellFormed(bool wellFormed)
        {
            this.wellFormed = wellFormed;
        }

        public void setKeys(int[] keys)
        {
            this.keys = keys;
        }

        internal bool isLikelyPattern(int p)
        {
            return defectiveRows == null || !defectiveRows.Contains(p);
            /*
            System.Collections.BitArray ba = sr.nullList[p];
            return sr.isLikelyPattern(ba);
             * */
        }
    }

    [Serializable]
    public class ScanSummaryList
    {
        public ScanSummaryResult[] scanSummaryList;
        // only used for retrieving and passing information
        // to the background process for oledb uploader/scanner
        public string[][] errors;
        public HashSet<string> processedFiles;        

        public static ScanSummaryList deserializeScanResults(string scanResultsFileName)
        {
            StreamReader reader = null;
            XmlReader xreader = null;
            ScanSummaryList response = null;
            try
            {             
                XmlSerializer serializer = new XmlSerializer(typeof(ScanSummaryList));
                reader = new StreamReader(scanResultsFileName);
                xreader = new XmlTextReader(reader);
                response = (ScanSummaryList)serializer.Deserialize(xreader);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while deserializing scan results " + scanResultsFileName, ex);
            }
            finally
            {
                try
                {
                    if (xreader != null)
                    {
                        xreader.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                        reader.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Error while closing the reader for scan results" + scanResultsFileName, ex);
                }
            }
            return response;

        }

        public static string getScanResultsFileName(string spaceDirectory, string fName)
        {
            return Path.Combine(new string[] { spaceDirectory, "data", fName + getScanResultsFileSuffix() });
        }

        public static string getScanResultsFileSuffix()
        {
            return "-scanResults.xml";
        }

        public static void serializeScanResults(ScanSummaryList sr, string scanResultsFileName)
        {
            StreamWriter writer = null;
            try
            {                
                writer = new StreamWriter(scanResultsFileName);
                XmlSerializer serializer = new XmlSerializer(typeof(ScanSummaryList));
                serializer.Serialize(writer, sr);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while serializing scan results " + ex.Message, ex);
            }
            finally
            {
                if (writer != null)
                {
                    try
                    {
                        writer.Close();
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Warn("Error in closing the writer for scan result for " + scanResultsFileName, ex);
                    }
                }
            }
        }
    }
}