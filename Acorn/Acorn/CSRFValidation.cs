﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace Acorn
{
    public class CSRFValidation : SoapExtension
    {
        SoapValidationContext context;

        public override object GetInitializer(Type serviceType)
        {
            return new SoapValidationContext();
        }

        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return new SoapValidationContext();
        }

        public override void Initialize(object initializer)
        {
            context = (SoapValidationContext)initializer;
        }

        // validate that the two uris came from the same origin (referer)
        private bool compareUris(Uri request, Uri reforg)
        {
            /* due to SSL termination in the load balancer, we can't compare the scheme
            if (request.Scheme != reforg.Scheme)
                return false;
             */
            if (request.Host != reforg.Host)
                return false;
            /*
            if (request.Port != reforg.Port)
                return false;
             */
            return true;
        }

        public override void ProcessMessage(SoapMessage message)
        {
            if (message.Stage == SoapMessageStage.BeforeDeserialize)
            {
                // check for cross site request forgery
                // - validate content type
                // - validate POST
                // - validate Referer and Origin headers (if they exist)
                // - validate hidden Anti-CSRF header

                if (message.ContentType == null || !message.ContentType.StartsWith("text/xml"))
                {
                    Global.systemLog.Warn("CSRF attempt: web service request ContentType header is not text/xml: " + message.ContentType);
                    throw new System.Net.WebException("CSRF attempt");
                }

                // no Http Context/Request, possibly a call to/from TrustedService
                HttpContext ctx = HttpContext.Current;
                if (ctx == null || ctx.Request == null)
                {
                    return;
                }
                
                Uri requestUri = ctx.Request.Url;

                // verify that this is an incoming request, not the result of an outgoing call
                if (message is SoapClientMessage)
                {
                    // Global.systemLog.Debug("CSRF module: ignoring SoapClientMessage - " + requestUri.ToString());
                    return;
                }

                if (ctx.Request.HttpMethod != "POST")
                {
                    Global.systemLog.Warn("CSRF attempt: web wervice request not via POST: " + ctx.Request.HttpMethod);
                    throw new System.Net.WebException("CSRF attempt");
                }


                string referer = ctx.Request.Headers["Referer"];
                if (referer != null)
                {
                    Uri refererUri = new Uri(referer);
                    if (!compareUris(requestUri, refererUri))
                    {
                        Global.systemLog.Warn("CSRF attempt: web service bad referer header: " + referer + " / " + requestUri.ToString());
                        throw new System.Net.WebException("CSRF attempt");
                    }     
                }

                string origin = ctx.Request.Headers["Origin"];
                if (origin != null)
                {
                    Uri originUri = new Uri(origin);
                    if (!compareUris(requestUri, originUri))
                    {
                        Global.systemLog.Warn("CSRF attempt: web service bad origin header: " + origin + " / " + requestUri.ToString());
                        throw new System.Net.WebException("CSRF attempt");
                    } 
                }

                HttpCookieCollection cookies = ctx.Request.Cookies;
                HttpCookie sessionCookie = cookies["ASP.NET_SessionId"];

                if (sessionCookie != null && sessionCookie.Value != null)
                {
                    string value = Util.encryptXSRFToken(sessionCookie.Value);
                    string token = ctx.Request.Headers["X-XSRF-TOKEN"]; // AngularJS
                    if (token == null)
                    {
                        Global.systemLog.Warn("CSRF attempt: web service Anti-CSRF header token is missing");
                        throw new System.Net.WebException("CSRF attempt");
                    }
                    else if (value != token)
                    {
                        Global.systemLog.Warn("CSRF attempt: web service Anti-CSRF header token is not correct - " + sessionCookie + " / " + token);
                        throw new System.Net.WebException("CSRF attempt");
                    }
                }
            }
        }
    }
}