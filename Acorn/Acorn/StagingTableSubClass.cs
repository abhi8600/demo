﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
     [Serializable]
    public class StagingTableSubClass  
    {
         public StagingTableSubClass() { 
         
         }
        public string Name;
        public string OriginalName;
        public bool Disabled;
        public ScriptDefinition Script;
        public DateTime? LastModifiedDate;
        public string[] SubGroups;
        public SourceColumnSubClass[] Columns;
    }
}