using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.IO;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Threading;

namespace Acorn
{
    public partial class Load : System.Web.UI.Page
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (Load.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            Loader.Master = Master;
            Loader.LoadNumber = sp.LoadNumber;
            Loader.LoadGroup = Util.LOAD_GROUP_NAME;
        }
    }
}
