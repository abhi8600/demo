﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.IO;

namespace Acorn
{
    public partial class CustomUpload : System.Web.UI.Page
    {
        public string customUploadHeaderURL = "CustomUploadHeader.htm";
        
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (CustomUpload.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            if (u.isFreeTrialUser)
            {
                Global.systemLog.Warn("redirecting to home page due to being a free trial user (CustomUpload.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (u.actDetails != null && u.actDetails.OverrideParameters != null)
            {
                if (u.actDetails.OverrideParameters.ContainsKey(OverrideParameterNames.CUSTOM_UPLOAD_HEADER_URL))
                {
                    customUploadHeaderURL = u.actDetails.OverrideParameters[OverrideParameterNames.CUSTOM_UPLOAD_HEADER_URL];
                }
            }

            // check if user has access to this page and if not show proper message and hide controls
            if (u.UserACLs == null || !u.UserACLs.Contains(ACLDetails.getACLID(ACLDetails.ACL_CUSTOM_UPLOAD_ALLOWED)))
            {
                UnableToAccessMessage.Text = "You do not have access to this functionality.";
                hideControls();
                return;
            }

            if (!IsPostBack)
            {
                if (ApplicationLoader.isSpaceBeingPublished(sp))
                {
                    // Inform user that data is being processed and disable dropdown and upload control
                    UnableToAccessMessage.Text = "Data is currently being processed, please try after some time.";
                    hideControls();
                    return;
                }

                // Fill the combobox with custom upload sources
                custoUploadSourcesList.Items.Clear();
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                List<string> customUploadSources = Util.getCustomUploadSourcesForSpace(maf);
                if (customUploadSources != null && customUploadSources.Count > 0)
                {
                    foreach (string filename in customUploadSources)
                    {
                        custoUploadSourcesList.Items.Add(filename);
                    }
                    custoUploadSourcesList.SelectedIndex = 0;
                    custoUploadSourcesList_SelectedIndexChanged(null, null);
                }
                else
                {
                    UnableToAccessMessage.Text = "There are no sources defined for custom upload for this space.";
                    hideControls();
                    return;
                }
            }
        }

        private void hideControls()
        {
            Uploader.Visible = false;
            custoUploadSourcesLabel.Visible = false;
            custoUploadSourcesList.Visible = false;
            requiredFormatText.Visible = false;
            UnableToAccessPanel.Visible = true;
        }

        protected void custoUploadSourcesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (custoUploadSourcesList.SelectedValue != null && !custoUploadSourcesList.SelectedValue.Equals(""))
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                foreach (SourceFile sourceFile in maf.sourceFileMod.getSourceFiles())
                {
                    if (sourceFile.FileName.Substring(0, sourceFile.FileName.LastIndexOf('.')).Equals(custoUploadSourcesList.SelectedValue))
                    {
                        if (sourceFile.Columns != null)
                        {
                            DataTable dt = new DataTable();
                            dt.Columns.Add("Name");
                            dt.Columns.Add("DataType");
                            foreach (SourceFile.SourceColumn sfc in sourceFile.Columns)
                            {
                                dt.Rows.Add(new string[] { sfc.Name, sfc.DataType });                                
                            }
                            SourceColumns.DataSource = dt;
                            SourceColumns.DataBind();
                        }
                    }
                }
            }
        }

        private void showGeneralErrors()
        {
            InnerErrorPanel.Visible = true;
            ErrorPanelExtender.Show();
            CurrentStage.Text = "";
            PhaseLabel.Text = "";
            SuccessPanel.Visible = false;
            updateTimer.Enabled = false;
            InnerLoadPanel.Visible = false;
            LoadPanel.Visible = false;            
        }

        protected void Error_OK_Click(object sender, EventArgs e)
        {
            InnerErrorPanel.Visible = false;
            ErrorPanelExtender.Hide();
            Uploader.Enabled = true;            
        }

        protected void Uploader_FileUploaded(object sender, EventArgs e)
        {
            if (Uploader.HasFile)
            {
                Space sp = (Space)Session["space"];
                string TempFileLocation = Path.Combine(sp.Directory, "data", "temp");
                if (!Directory.Exists(TempFileLocation))
                    Directory.CreateDirectory(TempFileLocation);
                string ClientFileName = Path.GetFileName(Uploader.FileName);
                string TempFileName = Path.Combine(TempFileLocation, ClientFileName);
                try
                {
                    Util.validateFileLocation(TempFileLocation, TempFileName);
                    Uploader.SaveAs(TempFileName);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex, ex);
                    Uploader.Enabled = false;
                    ErrorHeader.Text = "Unable to upload file";
                    ErrorMessage.Text = "Could not save the temp file.";
                    showGeneralErrors();
                    using (log4net.ThreadContext.Stacks["itemid"].Push(ClientFileName))
                    {
                        Global.userEventLog.Info("UPLOADFAILED");
                    }
                    return;

                }
                CurrentStage.Text = "";
                SuccessPanel.Visible = false;
                hideUploadErrors();

                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                SourceFile sf = null;
                foreach (SourceFile sourceFile in maf.sourceFileMod.getSourceFiles())
                {
                    if (sourceFile.FileName.Substring(0, sourceFile.FileName.LastIndexOf('.')).Equals(custoUploadSourcesList.SelectedValue))
                    {
                        sf = sourceFile;
                        break;
                    }
                }
                if (!ClientFileName.Substring(0, ClientFileName.LastIndexOf('.')).Equals(custoUploadSourcesList.SelectedValue))
                {
                    File.Delete(TempFileName);
                    Uploader.Enabled = false;
                    ErrorHeader.Text = "Unable to upload file";
                    ErrorMessage.Text = "The name of file you are attempting to upload must match with the name selected for upload.";
                    showGeneralErrors();
                    using (log4net.ThreadContext.Stacks["itemid"].Push(ClientFileName))
                    {
                        Global.userEventLog.Info("UPLOADFAILED");
                    }
                    return;
                }
                string path = Path.Combine(sp.Directory, "data");
                string fullClientName = Path.Combine(path, ClientFileName);
                try
                {
                    Util.validateFileLocation(path, fullClientName);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex, ex);
                    Uploader.Enabled = false;
                    ErrorHeader.Text = "Unable to upload file";
                    ErrorMessage.Text = "Could not save the file.";
                    showGeneralErrors();
                    using (log4net.ThreadContext.Stacks["itemid"].Push(ClientFileName))
                    {
                        Global.userEventLog.Info("UPLOADFAILED");
                    }
                    return;
                }
                if (fullClientName != TempFileName)
                {
                    // If re-uploading the same file and renamed, then delete original
                    File.Delete(fullClientName);
                    File.Move(TempFileName, fullClientName);
                }
                ApplicationUploader ul = new ApplicationUploader(maf, sp, ClientFileName, true, sf.LockFormat, false, sf.Escapes, false, null,
                    (User)Session["user"], Session, 0, 0, false, sf.Encoding, sf.Separator, false);
                DataTable et = ul.uploadFile(false, false);
                if (et.Rows.Count > 0)
                {
                    showUploadErrors(et);
                    return;
                }
                else
                {
                    // Data uploaded successfully
                    // Now Process Data for corresponding Processing Group
                    if (ul.getSuccessfullCounts() > 0)
                    {
                        CurrentStage.Text = "Data successfully uploaded and currently processing data...";
                        SuccessPanel.Visible = true;
                    }
                    ProcessUploadedData();                                        
                }                
            }            
        }

        protected void SessionTick(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                // Do nothing - prevent optimizer from pruning the code (just want to be sure)
            }
            return;
        }

        private void showUploadErrors(DataTable errorTable)
        {
            UploadIssuesPanel.Visible = true;
            IssueView.DataSource = errorTable;
            IssueView.DataBind();            
        }

        private void hideUploadErrors()
        {
            UploadIssuesPanel.Visible = false;
            IssueView.DataSource = null;
            IssueView.DataBind();
        }

        private void ProcessUploadedData()
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            SourceFile sf = null;
            foreach (SourceFile sourceFile in maf.sourceFileMod.getSourceFiles())
            {
                if (sourceFile.FileName.Substring(0, sourceFile.FileName.LastIndexOf('.')).Equals(custoUploadSourcesList.SelectedValue))
                {
                    sf = sourceFile;
                    break;
                }
            }
            List<string> subGroups = new List<string>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.SourceFile.Equals(sf.FileName))
                {
                    if (st.SubGroups == null)
                    {
                        ErrorHeader.Text = "Unable to process data";
                        ErrorMessage.Text = "No processing group defined for source, cannot process data.";
                        showGeneralErrors();
                        Global.systemLog.Error("No processing group defined for source, cannot process data.");
                        return;
                    }
                    subGroups.AddRange(st.SubGroups);
                }
            }
            GenericResponse resp = ProcessLoad.startPublishing(Session, Request, DateTime.Now.ToString(), Util.LOAD_GROUP_NAME, subGroups.ToArray(), false, true, false);
            // show errors if any
            if (resp == null)
            {
                ErrorHeader.Text = "Unable to process data";
                ErrorMessage.Text = "Error Processing Data.";
                showGeneralErrors();
                Global.systemLog.Warn("Error Processing Data.");
                return;
            }
            else if (resp.Error != null && resp.Error.ErrorType != 0)
            {
                ErrorHeader.Text = "Unable to process data";
                ErrorMessage.Text = HttpUtility.HtmlEncode("Error Processing Data" + (resp.Error.ErrorMessage == null ? "" : ": " + resp.Error.ErrorMessage));
                showGeneralErrors();
                Global.systemLog.Warn("Error Processing Data: " + resp.Error.ErrorType + " " + resp.Error.ErrorMessage + " " + resp.Error.ErrorMessageSource);
                return;                
            }
            // Processing started, now update processing status
            InnerLoadPanel.Visible = true;
            PhaseLabel.Text = "";
            LoadPanel.Visible = true;
            updateTimer.Enabled = true;            
        }

        protected void updateStatus(object sender, EventArgs e)
        {
            ProcessOptions pResp = ProcessLoad.getProgressStatus(Session);
            string progressStatus = "";
            string percentComplete = "";
            if (pResp == null)
            {
                ErrorHeader.Text = "Unable to update progress";
                ErrorMessage.Text = "Error updating progress for data processing.";
                showGeneralErrors();
                Global.systemLog.Warn("Error updating progress");                
            }
            else if (pResp.ErrorOutput != null && pResp.ErrorOutput.ErrorType != 0)
            {
                ErrorHeader.Text = "Unable to update progress";
                ErrorMessage.Text = HttpUtility.HtmlEncode("Error updating progress for data processing" + (pResp.ErrorOutput.ErrorMessage == null ? "" : ": " + pResp.ErrorOutput.ErrorMessage));
                showGeneralErrors();
                Global.systemLog.Warn("Error updating progress: " + pResp.ErrorOutput.ErrorType + " " + pResp.ErrorOutput.ErrorMessage + " " + pResp.ErrorOutput.ErrorMessageSource);
            }
            else if (pResp.CurrentState != null && pResp.CurrentState.Equals("Processing"))
            {
                // update progress information
                progressStatus = pResp.progressText;
                percentComplete = "Percent Completed " + pResp.progressPercentage + "%";
            }
            else if (pResp.CurrentState != null && (pResp.CurrentState.Equals("Available") || pResp.CurrentState.Equals("Done")))
            {
                updateTimer.Enabled = false;
                progressStatus = "Data Processed Successfully";
                CurrentStage.Text = "Data successfully uploaded and processed.";
                SuccessPanel.Visible = true;
                LoadPanel.Visible = false;                
            }
            else
            {
                ErrorHeader.Text = "Unable to update progress";
                ErrorMessage.Text = "Error updating progress for data processing.";
                showGeneralErrors();
                Global.systemLog.Error("Error updating progress");                
            }
            PhaseLabel.Text = HttpUtility.HtmlEncode(progressStatus);
            PercentComplete.Text = HttpUtility.HtmlEncode(percentComplete);
        }
    }
}
