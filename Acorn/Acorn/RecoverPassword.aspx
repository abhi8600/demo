﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs"
    Inherits="Acorn.RecoverPassword" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="WebAdminMasterHeadTag" runat="server">
    <title>Birst</title>
    <link href="global.css" type="text/css" rel="Stylesheet" />
    <link rel="shortcut icon" href="/FaviconIconUrl.aspx" />
    <link rel="icon" href="/FaviconIconUrl.aspx"/>
    <asp:Literal ID="HeaderColorStyle" runat="server"><style type="text/css">
                                                          #headerstyle
                                                          {
                                                              background-color: #E7E1D8;
                                                          }
                                                      </style></asp:Literal>
    <style type="text/css">
        #logostyle
        {
        	width: 20%;   
            vertical-align: middle;
            background-repeat: no-repeat;
            padding: 5px 10px;         	
            background-image: url(<asp:Literal ID="LogoLiteral" runat="server" >/Logo.aspx</asp:Literal>);
        }
       
    </style>
<script language="javascript">
<!--
if (top.frames.length != 0)
  top.location=self.document.location;
// -->
</script>
</head>
<body>
    <form id="Form1" runat="server">
        <div id="headerstyle" runat="server" style="height:44px;">
          <table id="headerTable">
            <tr>
                <td id="logostyle" style="height:44px; width:130px">
                </td>
            </tr>
          </table>
        </div>
    <div>
  <asp:MultiView ID="MultiView" runat="server" ActiveViewIndex="0">
    <asp:View ID="RequestUsernameView" runat="server">
        <asp:Label Width="80%" ID="FailLabel" runat="server" Visible="false" style="color:Red;padding:5px"></asp:Label>
        <asp:PasswordRecovery ID="RecoverPasswordControl" runat="server" OnSendingMail="RecoverPasswordControl_SendingMail"
            Width="100%" GeneralFailureText="The username that you provided is not registered. Please check the username and try again."
            MembershipProvider="UserProvider" OnSendMailError="RecoverPasswordControl_SendMailError"
            OnVerifyingUser="RecoverPasswordControl_VerifyingUser">
            <MailDefinition Subject="Birst Password Reset" BodyFileName="~/PasswordResetRequest.htm" IsBodyHtml="True">
            </MailDefinition>
            <UserNameTemplate>
                <div class="wizard">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 100%">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2">
                                            Enter your username to start the process of resetting your password.
                                        </td>
                                    </tr>
                                    <tr style="height: 20px">
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="UserName" MaxLength="255" runat="server" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="RecoverPasswordControl">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
<recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    Theme="red"
    PublicKey="RecaptchaPublicKey in web.config"
    PrivateKey="RecaptchaPrivateKey in web.config"
    />                                    
                                    </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color: Red;">
                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:ImageButton ID="SubmitButton" runat="server" CommandName="Submit" ValidationGroup="RecoverPasswordControl"
                                                ImageUrl="~/Images/Submit.png" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </UserNameTemplate>
            <SuccessTemplate>
                <div class="pageheader">
                    Reset Password
                </div>
                <div class="wizard">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 100%">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            Please check for an email that will explain how to reset your password.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </SuccessTemplate>
        </asp:PasswordRecovery>
        </asp:View>
                    <asp:View ID="ChangePasswordView" runat="server">
                                 <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="2" style="font-weight: bold">
                                                        Change Your Password (we recommend that you do not share this password with any other services)
                                                    </td>
                                                </tr>
                                                <tr style="height: 20px; vertical-align: top">
                                                    <td colspan="2">
                                                        &nbsp;
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                                                    </td>
                                                    <td style="padding-left: 3px">
                                                        <font color="blue">*</font>&nbsp;<asp:TextBox ID="NewPassword" runat="server" MaxLength="128" TextMode="Password" autocomplete="off"></asp:TextBox>
                                                        <asp:CustomValidator ID="PasswordValidator" ControlToValidate="NewPassword" runat="server"
                                                            OnServerValidate="PasswordValidator_Validate" EnableClientScript="False" ValidationGroup="ChangePassword"></asp:CustomValidator>
                                                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                            ErrorMessage="New Password is required." ToolTip="New Password is required."
                                                            ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                                                    </td>
                                                    <td style="padding-left: 3px">
                                                        <font color="blue">*</font>&nbsp;<asp:TextBox ID="ConfirmNewPassword" runat="server"
                                                            TextMode="Password" MaxLength="128" autocomplete="off"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                            ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                            ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                            ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                            ValidationGroup="ChangePassword"></asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2" style="color: Red;">
                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr style="height: 35px; vertical-align: top">
                                                    <td colspan="2">
                                                        <span style="font-size: 9pt"><font style="color: blue">*</font>&nbsp;Required information</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ChangePassword" runat="server" OnClick="ChangePWDButton_Click"
                                                            ImageUrl="~/Images/Change_Password.png" ToolTip="Change Password" ValidationGroup="ChangePassword" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                    </asp:View>
                    <asp:View ID="View1" runat="server">
                        <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="ChangedPassword" Text="Successfully changed the password" runat="server"></asp:Label>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:HyperLink ID="LoginLink" runat="server" Text="Return to the Login Page" />
                            </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                            <td style="font-weight: bold">                    
                                <asp:Label ID="FailedChange" Text="Failed to change the password (invalid or expired token)" runat="server"></asp:Label>
                            </td>
                            </tr>
                        </table>                                
                    </asp:View>
        </asp:MultiView>
    </div>
    </form>

    <div id="footerstyle" style="background-image: url(Images/FooterBackground.png); height: 40px; margin-left:auto; margin-right:auto" class="footerstyle">
            <table width="75%" cellpadding="0" cellspacing="20" style="padding-top: 0px; padding-bottom:20px">
                <tr>
                    <td style="color: #555555; text-align: center">
                        <asp:Literal ID="Copyright" runat="server" 
                            meta:resourcekey="CopyrightResource1"></asp:Literal>
                        &nbsp;
                        <asp:Literal ID="Version" runat="server" meta:resourcekey="VersionResource1"></asp:Literal>
                        &nbsp;
                    </td>
                    <td style="color: #BFBFBF">
                        |
                    </td>
                    <asp:PlaceHolder ID="TermsOfServicePlaceHolder" runat="server">
                        <td style="text-align:center ">
                            <asp:HyperLink ID="TermsOfServiceLink" runat='server' NavigateUrl="http://www.birst.com/terms-of-service" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Terms of Service" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="PrivacyPolicyPlaceHolder" runat="server">
                        <td style="text-align: center">
                            <asp:HyperLink ID="PrivacyPolicyLink" runat='server' NavigateUrl="http://www.birst.com/privacy-policy" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Privacy Policy" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="ContactUsPlaceHolder" runat="server">
                        <td style="text-align: center">
                            <asp:HyperLink ID="ContactUsLink" runat='server' NavigateUrl="http://www.birst.com/company/contact" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Contact Us" />
                        </td>
                    </asp:PlaceHolder>
                </tr>
            </table>
    </div>
</body>
</html>
