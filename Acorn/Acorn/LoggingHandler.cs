﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class LoggingHandler : IHttpModule
    {
        public void Init(HttpApplication app)
        {
            app.PreRequestHandlerExecute += new EventHandler(OnPreRequestHandlerExecute);
            app.PostRequestHandlerExecute += new EventHandler(OnPostRequestHandlerExecute);
        }

        public void Dispose() { }

        public void OnPreRequestHandlerExecute(Object s, EventArgs e)
        {
            HttpApplication app = s as HttpApplication;
            if (app != null && app.Context != null && app.Context.Session != null)
                Util.setLoggingNDC(app.Context.Session);
        }

        public void OnPostRequestHandlerExecute(Object s, EventArgs e)
        {
            log4net.NDC.Clear();
        }	
    }
}
