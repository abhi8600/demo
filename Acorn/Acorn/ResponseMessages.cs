﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class ResponseMessages
    {
        // Error Types
        // Load Authorization Types
        public static int SUCCESS = 0;
        public static int ERROR_TYPE_ROW_LIMIT = -1;
        public static int ERROR_TYPE_DATA_LIMIT = -2;
        public static int ERROR_TYPE_ROW_PUBLISH_LIMIT = -3;
        public static int ERROR_TYPE_DATA_PUBLISH_LIMIT = -4;

        // OTHER TYPES
        public static int ERROR_TYPE_SCRIPT = -5;
        public static int ERROR_TYPE_PUBLISH = -6;
        public static int ERROR_TYPE_GENERAL = -7;
        // Used for redirection on the client side
        public static int ERROR_TYPE_OTHER = -8;

        // Error Messages
        // Not able to publish failure
        public static string ERROR_MSG_PUBLISH = "Unable to process data. Return to Manage Sources to setup for processing";

        // Load Authorization Failures
        public static string ERROR_MSG_ROW_LIMIT = " Unable to process data. The number of rows added would exceed the limit set for your account. To process more data, please upgrade your account.";
        public static string ERROR_MSG_DATA_LIMIT = "Unable to process data. The amount of data to be processed would exceed the limit set for your account. To process more data, please upgrade your account.";
        public static string ERROR_MSG_ROW_PUBLISH_LIMIT = "Unable to process data. The number of rows added would exceed the amount that you can process at any one time.To process more data, please contact Birst to upgrade your account.";
        public static string ERROR_MSG_DATA_PUBLISH_LIMIT = "Unable to process data. The amount of data to be processed would exceed the amount that you can process at any one time. To process more data, please contact Birst to upgrade your account.";

        public static string ERROR_MSG_GENERAL = "Failed to process data, please verify your data format matches that defined. Please contact Birst technical support for more help if needed.";

        // Script Failure
        public static string ERROR_MSG_SCRIPT = "Failed to process data due to script failure.";

        // Session Error
        public static string ERROR_MSG_SESSION_VARIABLE = "Possible Session Timeout. Please login again";
        public static string ERROR_MSG_PROCESS_NOT_STARTED = "Failed to start processing of data.";
        // Running Status
        public static string STATUS_PROCESSING = "Processing";
        public static string STATUS_DONE = "Done";
        public static string STATUS_ERROR = "Error";
        public static string STATUS_AVAIABLE = "Available";

        public static void setErrorMessages(ErrorOutput errorOutput, int errorType)
        {
            setErrorMessages(errorOutput, errorType, null, null);
        }

        public static void setErrorMessages(ErrorOutput errorOutput, int errorType, string errorMessageSource,
            string errorMessage)
        {            
            if (errorMessage != null)
            {
                errorOutput.ErrorType = errorType;
                if (errorType == ERROR_TYPE_SCRIPT)
                {
                    errorMessage = ERROR_MSG_SCRIPT + errorMessage;
                }
                errorOutput.ErrorMessage = errorMessage;
                errorOutput.ErrorMessageSource = errorMessageSource;
                return;
            }

            if (errorType == ERROR_TYPE_GENERAL)
            {
                errorOutput.ErrorMessage = ERROR_MSG_GENERAL;
            }

            if (errorType == ERROR_TYPE_DATA_LIMIT)
            {
                errorOutput.ErrorMessage = ERROR_MSG_DATA_LIMIT;
            }

            if (errorType == ERROR_TYPE_DATA_PUBLISH_LIMIT)
            {
                errorOutput.ErrorMessage = ERROR_MSG_DATA_PUBLISH_LIMIT;
            }

            if (errorType == ERROR_TYPE_ROW_LIMIT)
            {
                errorOutput.ErrorMessage = ERROR_MSG_ROW_LIMIT;
            }

            if (errorType == ERROR_TYPE_ROW_PUBLISH_LIMIT)
            {
                errorOutput.ErrorMessage = ERROR_MSG_ROW_PUBLISH_LIMIT;
            }

            if (errorType == ERROR_TYPE_PUBLISH)
            {
                errorOutput.ErrorMessage = ERROR_MSG_PUBLISH;
            }

            errorOutput.ErrorMessageSource = errorOutput.ErrorMessage;
        }
    }
}
