﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.Utils;

namespace Acorn
{
    public partial class PollSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Util.setUpHeaders(Response);
                Util.setLoggingNDC(Session);
                Util.pollSMIWeb(Session);
                SchedulerUtils.pollScheduler(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex, ex);
            }
        }
    }
}
