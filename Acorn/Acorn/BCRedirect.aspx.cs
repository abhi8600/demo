﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.DBConnection;
using System.Text.RegularExpressions;

namespace Acorn
{
    public partial class BCRedirect : System.Web.UI.Page
    {
        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        //
        // always point at login.bws.birst.com, redirects to the appropriate place
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response, false);

            String birstConnectVersion = Request.QueryString["birstconnect.version"];
            if (birstConnectVersion == null || birstConnectVersion.Length < 1)
            {
                Global.systemLog.Info("No version specified for Birst Connect redirect");
                Response.StatusCode = 404;
                return;
            }
            // lookup birstConnectVersion in the URL table in the database
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<ReleaseInfo> list = Database.getReleasesInfo(conn, mainSchema);
                if (list != null)
                {
                    foreach (ReleaseInfo info in list)
                    {
                        // name in DB is a regexp:  5\.1.\*\.*, 5\.1\.1\.1
                        if (info.BCRegExp == null)
                            continue;
                        Regex pattern = new Regex(info.BCRegExp);
                        if (pattern.IsMatch(birstConnectVersion))
                        {
                            String url = info.RedirectUrl;
                            Response.Output.WriteLine(url);
                            return;
                        }
                    }
                }
                Global.systemLog.Info("No matching version for Birst Connect redirect: " + birstConnectVersion);
            }
            catch (Exception ex)
            {
                Global.systemLog.Info("Exception when determining Birst Connect redirect: " + birstConnectVersion + ", " + ex.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Response.StatusCode = 404;
        }
    }
}