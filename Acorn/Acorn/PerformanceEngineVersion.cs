﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class PerformanceEngineVersion    
    {
        public int ID;
        public string Name;
        public string Release;
        public string EngineCommand;
        public string DeleteCommand;
        public bool Visible;
        public bool SupportsRetryLoad;
        public bool IsProcessingGroupAware;
        public bool SupportsSFDCExtract;
    }

    public class PerformaceEngineDetails : GenericResponse
    {
        public PerformanceEngineVersion[] PerformanceEngineVersions;
    }
}
