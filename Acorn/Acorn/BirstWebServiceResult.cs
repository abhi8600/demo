﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Acorn
{
    public class BirstWebServiceResult
    {
        public const string NODE_NAME = "BirstWebServiceResult";
        public const string ERROR_CODE = "ErrorCode";
        public const string ERROR_MSG = "ErrorMessage";
        public const string RESULT = "Result";

        private int errorCode;
        private string errorMessage;

        private XmlNode result;

        public BirstWebServiceResult(int code)
        {
            errorCode = code;
            errorMessage = null;
        }

        public BirstWebServiceResult(int code, string message)
        {
            errorCode = code;
            errorMessage = message;
        }

        public void setResult(XmlNode res)
        {
            result = res;
        }

        public XmlNode toXmlNode()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement el = doc.CreateElement(NODE_NAME);
            XmlElement code = doc.CreateElement(ERROR_CODE);
            code.AppendChild(doc.CreateTextNode(errorCode.ToString()));
            el.AppendChild(code);

            XmlElement message = doc.CreateElement(ERROR_MSG);
            if (errorMessage != null)
            {
                message.AppendChild(doc.CreateTextNode(errorMessage));
            }
            el.AppendChild(message);

            XmlElement res = doc.CreateElement(RESULT);
            if (result != null)
            {
                XmlNode importNode = res.OwnerDocument.ImportNode(result, true);
                res.AppendChild(importNode);
            }
            el.AppendChild(res);

            return el;
        }
    }
}
