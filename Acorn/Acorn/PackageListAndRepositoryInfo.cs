﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class PackageListAndRepositoryInfo
    {
        // Used as a cache object to prevent reloading of repo file during listing of packages available
        public Guid spaceID;
        public DateTime packageFileModifiedDate;
        public DateTime repositoryFileModifiedDate;
        public AllPackages packageList;
        public MainAdminForm maf;

        public PackageDetail getPackage(Guid packageID)
        {  
            if (packageList != null && packageList.Packages != null && packageList.Packages.Length > 0)
            {
                return packageList.getPackage(packageID);
            }

            return null;
        }

        public PackageDetail getPackage(string packageName)
        {
            if (packageList != null && packageList.Packages != null && packageList.Packages.Length > 0)
            {
                return packageList.getPackage(packageName);
            }

            return null;
        }
    }
}