﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{

    public class SpaceGroup 
    {
        public Guid ID;
        public string Name;
        public bool InternalGroup;
        public List<User> GroupUsers;
        public List<int> ACLIds;

        public Object Clone()
        {
            SpaceGroup spaceGroup = new SpaceGroup();
            spaceGroup.ID = Guid.NewGuid();
            spaceGroup.Name = Name;
            spaceGroup.InternalGroup = InternalGroup;
            spaceGroup.GroupUsers = GroupUsers != null ? new List<User>(GroupUsers) : null;
            spaceGroup.ACLIds = ACLIds != null ? new List<int>(ACLIds) : null;
            return spaceGroup;
        }
    }

}
