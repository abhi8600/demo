﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class DynamicGroupsResult
    {
        public string[] Hierarchy;        
        public string[] sourceColumns;
        public string selectedGroupSource;
        public string selectedUserColumn;        
        public string selectedGroupColumn;        
        public ErrorOutput Error;
    }

    public class GroupDetails
    {
        public string[] MAFGroups;
        public string GroupName;
        public bool AdhocAccess;
        public bool DashboardAccess;
        public bool EnableDownloadAccess;
        public bool EnableSelfScheduleAccess;
        public bool ModifySavedExpressionAccess;
        public bool EditReportCatalogAccess;
        public bool VisualizerAccess;
        public MemberDetails[] Members;
        public ErrorOutput Error;
    }

    public class MemberDetails
    {
        public string Username;
        public bool Member;
        public bool Admin;
    }

    public class ShareSpaceResult
    {
        // MemberDetails data structure is used to create Access List for space
        // username = email
        public Invite[] InvitedMembers;
        public MemberDetails[] SpaceAccessMembers;
        public ErrorOutput Error;

    }

    
}
