﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    [Serializable]
    public class SourceLocation
    {
        public string LoadGroupName;
        public int x;
        public int y;

        public string toSerializedString()
        {
            return LoadGroupName + "|2" + x + "|2" + y;
        }

        public static SourceLocation[] getFromLocations(Location[] locations)
        {
            if (locations == null)
                return null;
            SourceLocation[] result = new SourceLocation[locations.Length];
            for (int i = 0; i < locations.Length; i++)
            {
                SourceLocation sl = new SourceLocation();
                sl.LoadGroupName = locations[i].LoadGroupName;
                sl.x = locations[i].x;
                sl.y = locations[i].y;
                result[i] = sl;
            }
            return result;
        }

        public static Location[] getFromSourceLocations(SourceLocation[] locations)
        {
            if (locations == null)
                return null;
            Location[] result = new Location[locations.Length];
            for (int i = 0; i < locations.Length; i++)
            {
                Location sl = new Location();
                sl.LoadGroupName = locations[i].LoadGroupName;
                sl.x = locations[i].x;
                sl.y = locations[i].y;
                result[i] = sl;
            }
            return result;
        }
    }
}