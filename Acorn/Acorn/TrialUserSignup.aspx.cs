using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Data.Odbc;
using System.Collections.Generic;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.sforce;
using System.Web.Services.Protocols;
using System.Net;
using Acorn.Background;
using Acorn.DBConnection;
using Acorn.Utils;

namespace Acorn
{
    public partial class TrialUserSignup : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string freeTrialProductIDs = System.Web.Configuration.WebConfigurationManager.AppSettings["FreeTrialProductIDs"];
        private static int freeTrialDays = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["FreeTrialDays"]);
        private static int freeTrialReleaseType = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["FreeTrialReleaseType"]);
        private static string useRecaptcha = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseRecaptchaForFreeTrial"];
        public static string sampleSpaceComment = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SampleSpaceComment"];

        private bool passwordValid = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response, false);

            Recaptcha.RecaptchaControl control = (Recaptcha.RecaptchaControl)CreateUserWizardStep.ContentTemplateContainer.FindControl("recaptcha");

            if (control != null && useRecaptcha != null && useRecaptcha.ToLower().Equals("true"))
            {
                    control.OverrideSecureMode = Global.connectionIsViaHTTPS;
            }
            else
            {
                control.Enabled = false;
                control.Visible = false;
            }

            if (IsPostBack)
            {
                TrackingPanel.Visible = true;
            }
            else
            {   
                TrackingPanel.Visible = false;
                if (Array.IndexOf<string>(Request.Params.AllKeys, "sfdctrial") >= 0)
                {
                    Session["sfdctrial"] = true;
                }
                else
                {
                    Session.Remove("sfdctrial");
                }
                ((Label)CompleteWizardStep.ContentTemplateContainer.FindControl("FromEmailLabel")).Text = System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"];
            }
        }
        
        protected void CreateUserWizard_SendingMail(object sender, MailMessageEventArgs e)
        {
            MembershipUser mu = Membership.GetUser(CreateUserWizard.Email);
            UserCreation.sendSignUpVerificationEmail(e.Message, -1, Util.getRequestBaseURL(Request), (Guid)mu.ProviderUserKey);
            Global.systemLog.Info("Verification email sent to user " + CreateUserWizard.Email);
            
            if (Session["sfdctrial"] != null)
            {
                Space sp = (Space)Session["space"];
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    User u = Database.getUser(conn, mainSchema, mu.UserName);
                    bool newrep = false;
                    SforceService service = (SforceService)Session["sftrialbinding"];
                    LoadSFDCSpace ls = new LoadSFDCSpace(u, sp, Util.loadRepository(sp, out newrep), service, new MailMessage(e.Message.From.ToString(), e.Message.To.ToString(), e.Message.Subject, e.Message.Body));
                    BackgroundProcess.startModule(new BackgroundTask(ls, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
                    e.Cancel = true;
                    return;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            e.Cancel = true; 
        }

        protected void CreateUserWizard_CreatedUser(object sender, EventArgs e)
        {
            UserCreation userCreation = new UserCreation(CreateUserWizard.Email, null, Guid.Empty, freeTrialProductIDs, freeTrialDays, freeTrialReleaseType, sampleSpaceComment, Util.getRequestBaseURL(Request), Session);
            userCreation.configureNewUser();            
        }

        protected void CreateUserWizard_SendMailError(object sender, SendMailErrorEventArgs e)
        {
            Global.systemLog.Warn("Create User Wizard email failed: " + e.Exception.Message.ToString());
        }

        protected void CreateUserWizard_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            Panel ep = (Panel)CreateUserWizardStep.ContentTemplateContainer.FindControl("ErrorPanel");
            ep.Visible = true;
        }

        protected void PasswordValidator_Validate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            if (CreateUserWizard.Email == null)
                return;

            CustomValidator validator = (CustomValidator) CreateUserWizardStep.ContentTemplateContainer.FindControl("PasswordValidator");

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                PasswordPolicy policy = Database.getPasswordPolicy(conn, mainSchema, Guid.Empty);
                if (policy != null)
                {
                    validator.ErrorMessage = policy.description;
                    validator.ToolTip = policy.description;
                    args.IsValid = policy.isValid(args.Value, CreateUserWizard.Email, null);
                }
                else
                    args.IsValid = true; // no password policy
                passwordValid = args.IsValid;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void CreateUserWizard_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            FailLabel.Visible = false;
            Page.Validate();
            if (!Page.IsValid)
            {
                if (passwordValid)
                {
                    if (useRecaptcha != null && useRecaptcha.ToLower().Equals("true"))
                    {
                        FailLabel.Text = "Failed to enter the correct CAPTCHA words, please try again.";
                        FailLabel.Visible = true;
                    }
                    else
                    {
                        return;
                    }
                }
                ((CheckBox)CreateUserWizardStep.ContentTemplateContainer.FindControl("ServiceBox")).Checked = false;
                e.Cancel = true;
                return;
            }
        }

        private class LoadSFDCSpace : BackgroundModule
        {
            SalesforceLoader sl = null;
            MailMessage message = null;
            private Dictionary<BackgroundModuleObserver, object> observers;

            public LoadSFDCSpace(User u, Space sp, MainAdminForm maf, SforceService service, MailMessage message)
            {
                this.message = message;
                List<string> objects = new List<string>();
                SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                for (int i = 0; i < settings.Objects.Length; i++)
                    objects.Add(settings.Objects[i].name);
                sl = new SalesforceLoader(objects, maf, sp, u, service, settings, true, null, true);
                observers = new Dictionary<BackgroundModuleObserver, object>();
            }

            public void run()
            {
                sl.run();
                SmtpClient sc = new SmtpClient();
                bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
                if (useSSL)
                {
                    sc.EnableSsl = true;
                }
                message.IsBodyHtml = true;
                sc.Send(message);
            }

            public void kill()
            {
                /* again does nothing?
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.killed(kvp.Value);
                }
                 */
            }

            public void finish()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            #region BackgroundModule Members


            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }

            #endregion

            #region BackgroundModule Members


            public Status.StatusResult getStatus()
            {
                return sl.getStatus();
            }

            #endregion
        }
    }
}
