using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Threading;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.Utils;
using System.Xml;
using System.Xml.Serialization;
using Acorn.DBConnection;

namespace Acorn
{
    public class Status
    {
        public enum StatusCode
        {
            None, Ready, GenerateSchema, LoadStaging, LoadWarehouse, Complete, Failed, BuildingDashboards, DeleteData, LoadingAnotherLoadGroup, Unpacking, 
            Started, SFDCExtract, PostProcess, Executing, Assembling, TimeCreation
        };

        public const int NONE = 0;
        public const int RUNNING = 1;
        public const int COMPLETE = 2;
        public const int FAILED = 3;
        public const int SCRIPTFAILURE = 4;
        public const int CANCELLED = 5;
        public const int STARTED = 6;
        public const string RUNNING_STR = "Running";
        public const string COMPLETE_STR = "Complete";
        public const string FAILED_STR = "Failed";
        public const string ACORN = "ACORN";
        public const string ACORN_PREFIX = ACORN + ": ";
        public const string OP_DELETE_DATA = "DeleteData";
        public const string OP_GEN_SCHEMA = "GenerateSchema";
        public static string EMPTY_PROCESSING_GROUP = "NONE";

        private static string mainSchema = System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public class StatusStep
        {
            [XmlIgnore]
            public StatusCode step;
            public string stepCode
            {
                get
                {
                    if (step == StatusCode.None)
                        return "None";
                    if (step == StatusCode.BuildingDashboards)
                        return "Building Dashboards";
                    if (step == StatusCode.Complete)
                        return "Complete";
                    if (step == StatusCode.DeleteData)
                        return "Delete Data";
                    if (step == StatusCode.Failed)
                        return "Failed";
                    if (step == StatusCode.GenerateSchema)
                        return "Generate Schema";
                    if (step == StatusCode.LoadingAnotherLoadGroup)
                        return "Loading Another Load Group";
                    if (step == StatusCode.LoadStaging)
                        return "Load Staging";
                    if (step == StatusCode.LoadWarehouse)
                        return "Load Warehouse";
                    if (step == StatusCode.Ready)
                        return "Ready";
                    if (step == StatusCode.Unpacking)
                        return "Unpacking";
                    if (step == StatusCode.Started)
                        return "Started";
                    if (step == StatusCode.SFDCExtract)
                        return "Extract Running";
                    if (step == StatusCode.PostProcess)
                        return "Post Processing";
                    if (step == StatusCode.Assembling)
                        return "Assembling";

                    return null;
                }

                set
                {
                }
            }
            public string substep;
            public int result;
            public long numRows;
            public long numErrors;
            public long numWarnings;
            public long duration;
            public string message;
            public string processingGroup;
        }

        public class SpaceStatus
        {
            public bool available;
            public string loadGroup;
            public string stepName;
            public int stepStatus;
            public int loadId;
            public string message;
            public string createdDate;
            public string modifiedDate;
        }


        public class StatusResult
        {
            public const int TYPE_EXTRACT = 2;
            [XmlIgnore]
            public StatusCode code;
            public string processingGroup;
            public int type;
            public string statusCode
            {
                get
                {
                    if (code == StatusCode.None)
                        return "None";
                    if (code == StatusCode.BuildingDashboards)
                        return "Building Dashboards";
                    if (code == StatusCode.Complete)
                        return "Complete";
                    if (code == StatusCode.DeleteData)
                        return "Delete Data";
                    if (code == StatusCode.Failed)
                        return "Failed";
                    if (code == StatusCode.GenerateSchema)
                        return "Generate Schema";
                    if (code == StatusCode.LoadingAnotherLoadGroup)
                        return "Loading Another Load Group";
                    if (code == StatusCode.LoadStaging)
                        return "Load Staging";
                    if (code == StatusCode.LoadWarehouse)
                        return "Load Warehouse";
                    if (code == StatusCode.Ready)
                        return "Ready";
                    if (code == StatusCode.Unpacking)
                        return "Unpacking";
                    if (code == StatusCode.Started)
                        return "Started";
                    if (code == StatusCode.SFDCExtract)
                        return "Extract Running";
                    if (code == StatusCode.PostProcess)
                        return "Post Processing";
                    if (code == StatusCode.Assembling)
                        return "Assembling";

                    return null;
                }

                set
                {
                }
            }

            public int loadid;
            public string message;
            [XmlIgnore]
            public DateTime dateTime;

            [XmlIgnore]
            public List<StatusStep> substeps;

            // do not use
            public StatusResult()
            {
            }

            public StatusResult(StatusCode code)
            {
                this.code = code;
            }

            public XmlNode toXml()
            {
                BirstWebServiceResult res = new BirstWebServiceResult(code == Status.StatusCode.Failed ? -1 : 0, Status.getPhaseLabel(this, false));
                XmlDocument doc = new XmlDocument();
                res.setResult(doc.CreateTextNode(message));
                return res.toXmlNode();
            }
        }

        public static string tableName = "TXN_COMMAND_HISTORY";

        public static bool isRunningCode(StatusCode code)
        {
            return (code == StatusCode.BuildingDashboards || code == StatusCode.GenerateSchema || code == StatusCode.LoadStaging || code == StatusCode.LoadWarehouse || code == StatusCode.DeleteData || code == StatusCode.LoadingAnotherLoadGroup || code == StatusCode.Unpacking
                || code == StatusCode.Started || code == StatusCode.SFDCExtract || code == StatusCode.PostProcess || code == StatusCode.Assembling);
        }

        public static StatusResult getLoadStatus(System.Web.SessionState.HttpSessionState session, Space sp)
        {
            return (getLoadStatus(session, sp, sp.LoadNumber + 1, Util.LOAD_GROUP_NAME));
        }


        public static StatusResult getLoadStatus(System.Web.SessionState.HttpSessionState session, Space sp, int loadNumber, string loadGroup)
        {
            return getLoadStatus(session, sp, loadNumber, loadGroup, false, null);
        }

        public static bool checkIfTxnCommandHistoryTableExists(HttpSessionState session, QueryConnection conn, Space sp)
        {
            List<Guid> statusTableCache = session == null ? null : (List<Guid>)session["statustablecache"];
            bool tableExists = false;
            bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
            if (session != null)
            {
                if (statusTableCache == null)
                {
                    statusTableCache = new List<Guid>();
                    session["statustablecache"] = statusTableCache;
                }
                else
                    tableExists = statusTableCache.Contains(sp.ID);
            }
            if (!tableExists)
            {
                int trycount = 0;
                do
                {
                    try
                    {
                        if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                        {
                            break;
                        }
                        tableExists = true;
                        if (statusTableCache != null)
                            statusTableCache.Add(sp.ID);
                        break;
                    }
                    catch (OdbcException ex)
                    {
                        Global.systemLog.Warn("Error during tableExist operaton : Retry Count : " + (trycount + 1) + " : " + ex.Message);
                        ConnectionPool.releaseConnection(conn);
                        Thread.Sleep(5000);
                        conn = ConnectionPool.getConnection(sp.getFullConnectString());
                    }
                    catch (InvalidOperationException ex)
                    {
                        if (ex.InnerException != null && ex.InnerException is OdbcException)
                        {
                            ConnectionPool.releaseConnection(conn);
                            Global.systemLog.Warn("InvalidOperation Error during tableExist operaton : Retry Count : " + (trycount + 1) + " : " + ex.InnerException.Message);
                            Thread.Sleep(5000);
                            conn = ConnectionPool.getConnection(sp.getFullConnectString());
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                } while (!tableExists && trycount++ < 5);
            }

            return tableExists;
        }
                
        public static StatusResult getLoadStatus(System.Web.SessionState.HttpSessionState session, Space sp, int loadNumber, string loadGroup, 
            bool bypassOtherLoadGroupCheck, StatusResult initialResultCode)
        {
            StatusResult sr = initialResultCode != null ? initialResultCode : null;

            if (initialResultCode == null)
            {   
                sr = session == null ? null : (Status.StatusResult)session["status"];
                if (sr == null)
                    sr = new StatusResult(StatusCode.None);
            }
            QueryConnection conn = null;
            try
            {

                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                QueryReader result = null;
                QueryCommand cmd = null;
                bool tableExists = checkIfTxnCommandHistoryTableExists(session, conn, sp);
                
                if (!bypassOtherLoadGroupCheck)
                {   
                    StatusResult otherLoadgroupStatus = checkAndRetreiveStatusForOtherGroups(conn, session, sp, loadGroup, loadNumber, tableExists, false, sr);
                    // null means continue to the cloud load groups logic
                    if (otherLoadgroupStatus != null && Status.isRunningCode(otherLoadgroupStatus.code))
                    {
                        return otherLoadgroupStatus;
                    }
                    //If processed first BirstLocal load successfully, TXN_COMMAND_HISTORY will be created as part of checkAndRetreiveStatusForOtherGroups
                    if (!tableExists)
                        tableExists = checkIfTxnCommandHistoryTableExists(session, conn, sp); 
                }
                
                try
                {
                    if (!tableExists)
                    {
                        return sr;
                    }

                    result = executeStatusQuery(conn, sp, loadNumber);
                }
                catch (OdbcException ex)
                {
                    if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                    {
                        Global.systemLog.Error("Odbc exception checking load status ", ex);
                        return (sr);
                    }
                    ConnectionPool.releaseConnection(conn);

                    if (!isDurationColumnNeededInTxnCommandHistory(ex) && !isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        Global.systemLog.Error("Odbc exception checking load status, retrying: ", ex);
                    }

                    bool sleepAndRetry = true;
                    // check the exception if it contains Duration column
                    if (isDurationColumnNeededInTxnCommandHistory(ex))
                    {
                        try
                        {
                            Database.addDurationColumnToTxnCommandHistory(sp);
                            Database.addProcessingGroupToTxnCommandHistory(sp);
                            conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                            result = executeStatusQuery(conn, sp, loadNumber);
                            sleepAndRetry = false;
                        }
                        catch (OdbcException ex2)
                        {
                            Global.systemLog.Error("Odbc exception while adding duration column ", ex2);
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    else if (isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        try
                        {
                            Database.addProcessingGroupToTxnCommandHistory(sp);
                            conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                            result = executeStatusQuery(conn, sp, loadNumber);
                            sleepAndRetry = false;
                        }
                        catch (OdbcException ex2)
                        {
                            Global.systemLog.Error("Odbc exception while adding ProcessingGroup column ", ex2);
                            ConnectionPool.releaseConnection(conn);
                        }
                    }

                    if (sleepAndRetry)
                    {
                        Thread.Sleep(4000);
                        conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                        result = executeStatusQuery(conn, sp, loadNumber);
                    }
                }

                StatusCode resultcode = sr.code;
                List<StatusStep> substeps = new List<StatusStep>();
                string processingGroup = null;
                bool hasResults = false;
                PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
                bool isPeProcessingGroupAware = peVersion.IsProcessingGroupAware;
                bool detectedEndMarker = false;
                while (result.Read())
                {
                    hasResults = true;
                    DateTime dt = result.IsDBNull(0) ? DateTime.Now : result.GetDateTime(0);
                    string step = result.GetString(1);
                    string substep = result.IsDBNull(2) ? null : result.GetString(2);
                    processingGroup = result.IsDBNull(3) ? null : result.GetString(3);
                    int code = result.GetInt32(4);
                    long numRows = result.IsDBNull(5) ? -1 : result.GetInt64(5);
                    long numErrors = result.IsDBNull(6) ? -1 : result.GetInt64(6);
                    long numWarnings = result.IsDBNull(7) ? -1 : result.GetInt64(7);
                    long duration = result.IsDBNull(8) ? -1 : result.GetInt64(8);
                    string message = result.IsDBNull(9) ? null : result.GetString(9);
                    if (code == FAILED && step != "ExecuteScriptGroup")
                    {
                        resultcode = StatusCode.Failed;
                        if (message != null && message.Length > 0)
                            sr.message = message;
                        break;
                    }
                    if (substep == null || substep.IndexOf(':') < 0)
                    {
                        if (step == "BuildDashboards" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.Started))
                            resultcode = StatusCode.BuildingDashboards;
                        else if (step == "LoadMarker" && code == RUNNING && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.Started))
                            resultcode = StatusCode.GenerateSchema;
                        else if (step == "GenerateSchema" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.BuildingDashboards || resultcode == StatusCode.Started))
                            resultcode = StatusCode.GenerateSchema;
                        else if (step == "LoadStaging" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.GenerateSchema || resultcode == StatusCode.BuildingDashboards || resultcode == StatusCode.Started))
                            resultcode = StatusCode.LoadStaging;
                        else if (step == "LoadWarehouse")
                        {
                            if (code == COMPLETE)
                            {
                                // check for the presence of last step to really make sure this is done                                
                                if (getStatusOfLoadMarkerStep(sp, loadNumber, session) == RUNNING)
                                {
                                    resultcode = StatusCode.LoadWarehouse;
                                }
                                else
                                {
                                    resultcode = StatusCode.Complete;
                                    detectedEndMarker = true;
                                    
                                }
                                /*
                                // check for script group status 
                                // if script is still running, don't mark the load as complete
                                if (getScriptGroupStatus(sp, loadNumber, session) == RUNNING)
                                {
                                    resultcode = StatusCode.LoadWarehouse;
                                }
                                else
                                {
                                    resultcode = StatusCode.Complete;
                                }
                                 */
                                //break;
                            }
                            else
                                resultcode = StatusCode.LoadWarehouse;
                        }
                        else if (step == "DeleteData" && substep == null)
                        {
                            if (code == COMPLETE)
                            {
                                resultcode = StatusCode.Complete;
                                break;
                            }
                            else
                                resultcode = StatusCode.DeleteData;
                        }
                    }
                    else
                    {
                        StatusStep ss = new StatusStep();
                        if (step == "BuildDashboards")
                            ss.step = StatusCode.BuildingDashboards;
                        else if (step == "GenerateSchema")
                            ss.step = StatusCode.GenerateSchema;
                        else if (step == "LoadStaging")
                            ss.step = StatusCode.LoadStaging;
                        else if (step == "LoadWarehouse")
                            ss.step = StatusCode.LoadWarehouse;
                        else if (step == "DeleteData")
                            ss.step = StatusCode.DeleteData;
                        ss.substep = substep;
                        ss.result = code;
                        ss.numRows = numRows;
                        ss.numErrors = numErrors;
                        ss.numWarnings = numWarnings;
                        ss.duration = duration;
                        ss.message = message;
                        ss.processingGroup = processingGroup;
                        substeps.Add(ss);
                    }
                }
                result.Close();
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                if (!hasResults && (resultcode == StatusCode.LoadStaging || resultcode == StatusCode.LoadWarehouse || resultcode == StatusCode.GenerateSchema || resultcode == StatusCode.Started || resultcode == StatusCode.BuildingDashboards))
                {
                    bool updateResultCode = shouldUpdateResultCode(sp, conn, loadNumber, resultcode);
                    if (updateResultCode)
                    {
                        resultcode = StatusCode.Complete;
                    }
                }

                // if detected end marker the status should be complete unless status is failed
                if (detectedEndMarker && resultcode != StatusCode.Failed)
                {
                    resultcode = StatusCode.Complete;
                }
                sr.code = resultcode;
                sr.processingGroup = processingGroup;
                sr.substeps = substeps;
                return (sr);
            }
            catch (OdbcException ex)
            {
                sr.code = StatusCode.Failed;
                Global.systemLog.Error("Load failed due to ODBC exception. Space: " + sp.ID.ToString() + ", ", ex);
                return (sr);
            }
            catch (DataException ex)
            {
                sr.code = StatusCode.Failed;
                Global.systemLog.Error("Load failed due to Data exception. Space: " + sp.ID.ToString() + ", ", ex);
                return (sr);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static bool shouldUpdateResultCode(Space sp, QueryConnection conn, int loadNumber, StatusCode resultcode)
        {
            bool updateResultCode = true;
            if ((resultcode == StatusCode.Started || resultcode == StatusCode.BuildingDashboards ) && (loadNumber == 1 || loadNumber == SalesforceLoader.BASE_SFORCE_LOAD_NUMBER))
            {
                // a hack to address the use case for the very first load where txn_command_history table is created but before there is any entry populated
                
                for(int count=0; count < 5; count++)
                {
                    Thread.Sleep(2000);
                    if (Status.loadStepsExists(conn, sp, loadNumber))
                    {
                        updateResultCode = false;
                        break;
                    }
                }
            }
            return updateResultCode;
        }

        /*
        public static StatusResult getLoadStatus(System.Web.SessionState.HttpSessionState session, Space sp, int loadNumber, string loadGroup)
        {
           StatusResult sr = session == null ? null : (Status.StatusResult)session["status"];
                if (sr == null)
                    sr = new StatusResult(StatusCode.None);
            
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection(sp.getFullConnectString());
                QueryReader result = null;
                QueryCommand cmd = null;
                
                List<Guid> statusTableCache = session == null ? null : (List<Guid>)session["statustablecache"];
                bool tableExists = false;
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                if (session != null)
                {
                    if (statusTableCache == null)
                    {
                        statusTableCache = new List<Guid>();
                        session["statustablecache"] = statusTableCache;
                    }
                    else
                        tableExists = statusTableCache.Contains(sp.ID);
                }
                if (!tableExists)
                {
                    int trycount = 0;
                    do
                    {
                        try
                        {
                            if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                            {
                                break;
                            }
                            tableExists = true;
                            if (statusTableCache != null)
                                statusTableCache.Add(sp.ID);
                            break;
                        }
                        catch (OdbcException ex)
                        {
                            Global.systemLog.Warn("Error during tableExist operaton : Retry Count : " + (trycount + 1) + " : " + ex.Message);
                            ConnectionPool.releaseConnection(conn);
                            Thread.Sleep(5000);
                            conn = ConnectionPool.getConnection(sp.getFullConnectString());
                        }
                        catch (InvalidOperationException ex)
                        {
                            if (ex.InnerException != null && ex.InnerException is OdbcException)
                            {
                                ConnectionPool.releaseConnection(conn);
                                Global.systemLog.Warn("InvalidOperation Error during tableExist operaton : Retry Count : " + (trycount + 1) + " : " + ex.InnerException.Message);
                                Thread.Sleep(5000);
                                conn = ConnectionPool.getConnection(sp.getFullConnectString());
                            }
                            else
                            {
                                throw ex;
                            }
                        }
                    } while (!tableExists && trycount++ < 5);
                }               

                
                MainAdminForm maf = null;
                if (session != null)
                {
                    maf = (MainAdminForm)session["MAF"];
                }
                else
                {
                    bool newRepository;
                    maf = Util.loadRepository(sp, out newRepository);
                }
                if (maf != null)
                {
                    List<string> queryAttemptForLoadGroups = new List<string>();
                    foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                    {
                        // If there are other loadgroups running
                        if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, loadGroup) >= 0 ? 1 : 0))
                        {
                            // Get a list of the max status code for the most recent iteration of each load group
                            string steps = "";
                            string liveAccessLoadGroup = null;
                            bool nonLiveAccessLoadGroupExists = false;
                            foreach (string s in st.LoadGroups)
                            {
                                if (s != loadGroup)
                                {
                                    if (s.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                                    {
                                        liveAccessLoadGroup = s;
                                    }
                                    else
                                    {
                                        nonLiveAccessLoadGroupExists = true;
                                    }
                                    if (steps.Length > 0)
                                        steps += ',';
                                    steps += "'" + s + "'";
                                }
                            }
                            if (nonLiveAccessLoadGroupExists && tableExists)
                            {
                                try
                                {
                                    cmd = conn.CreateCommand();
                                    cmd.CommandText = getStatusSQL(sp.Schema, tableName, steps, sp.ConnectString.Contains("jdbc:mysql"), sp.ConnectString.Contains("jdbc:memdb"));
                                    result = cmd.ExecuteReader();
                                }
                                catch (OdbcException ex)
                                {
                                    Global.systemLog.Error("Odbc exception checking load status of other load groups: ", ex);
                                    break;
                                }
                                while (result.Read())
                                {
                                    string gname = result.GetString(0);
                                    string step = result.GetString(1);
                                    int status = result.GetInt32(2);
                                    if (status == Status.RUNNING)
                                    {
                                        result.Close();
                                        cmd.Dispose();
                                        sr.code = StatusCode.LoadingAnotherLoadGroup;
                                        return sr;
                                    }
                                }
                                result.Close();
                                cmd.Dispose();
                                break;
                            }
                            if ((liveAccessLoadGroup != null)
                                        && (!queryAttemptForLoadGroups.Contains(liveAccessLoadGroup)))
                            {
                                queryAttemptForLoadGroups.Add(liveAccessLoadGroup);
                                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                                LocalETLConfig leConfig = null;
                                if (localETLConfigs != null && localETLConfigs.ContainsKey(liveAccessLoadGroup))
                                {
                                    leConfig = localETLConfigs[liveAccessLoadGroup];
                                }
                                if (leConfig == null)
                                {
                                    Global.systemLog.Warn("Could not find local ETL configuration for load group: " + liveAccessLoadGroup + " - processing status will not be reported accurately.");
                                    continue;
                                }
                                string liveAccessConnectionName = leConfig.getLocalETLConnection();
                                DatabaseConnection liveAccessDc = null;
                                foreach (DatabaseConnection dc in maf.connection.connectionList)
                                {
                                    if (dc.Name != null && dc.Name == liveAccessConnectionName)
                                    {
                                        liveAccessDc = dc;
                                        break;
                                    }
                                }
                                if (liveAccessDc == null)
                                {
                                    Global.systemLog.Warn("Could not find live access connection with name: " + liveAccessConnectionName + " - processing status will not be reported accurately.");
                                    continue;
                                }
                                string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                                bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, tableName);
                                if (!liveAccessTableExists)
                                {
                                    break;
                                }
                                string statusSQL = getStatusSQLForLiveAccessConnection(liveAccessConnSchemaName, tableName, liveAccessLoadGroup, liveAccessDc.ConnectString.Contains("jdbc:mysql"));
                                //Perform a live access query on the connection and see if it is running.
                                Object[][] results = LiveAccessUtil.executeLiveAccessQuery(sp, liveAccessConnectionName, statusSQL, false);
                                int status = Status.RUNNING;
                                bool loadComplete = false;
                                bool deleteloadComplete = false;
                                if (results != null && results.Length > 0)
                                {
                                    foreach (Object[] row in results)
                                    {
                                        if (row.Length == 3)
                                        {
                                            string step = (string)row[1];
                                            string statusStr = (string)row[2];
                                            status = int.Parse(statusStr.Trim());
                                            if (status == Status.FAILED || status == Status.SCRIPTFAILURE)
                                            {
                                                break;
                                            }
                                            else if (status == Status.COMPLETE)
                                            {
                                                if (step == "LoadMarker")
                                                {
                                                    loadComplete = true;
                                                }
                                                else if (step == "DeleteData")
                                                {
                                                    deleteloadComplete = true;
                                                }
                                                else
                                                {
                                                    status = Status.RUNNING;
                                                }
                                            }
                                            else if (step == "DeleteData")
                                            {
                                                sr.code = StatusCode.DeleteData;
                                                return sr;
                                            }
                                        }
                                    }
                                    if ((loadComplete || deleteloadComplete) && status != Status.FAILED && status != Status.SCRIPTFAILURE)
                                    {
                                        status = Status.COMPLETE;
                                    }
                                    if (status == Status.RUNNING)
                                    {
                                        sr.code = StatusCode.LoadingAnotherLoadGroup;
                                        return sr;
                                    }
                                    else
                                    {
                                        // synch up txn_command_history
                                        // first check to see if TxnCommandHistory exists on BirstData, if so match the Max(TM)
                                        // for loadNumber and loadgroup with the Max(TM) of localETL's TxnCommandHistory
                                        // and issue update of txncommandhistory only if match fails.
                                        
                                        DateTime serverMaxTM = DateTime.MinValue, localMaxTM = DateTime.MinValue;
                                        bool serverMaxTMfound = false, localMaxTMfound = false;
                                        if (tableExists)
                                        {
                                            cmd = conn.CreateCommand();
                                            cmd.CommandText = "SELECT MAX(TM) FROM " + sp.Schema + "." + tableName
                                                + " WHERE ITERATION=" + loadNumber + " AND SUBSTEP LIKE ('" + liveAccessLoadGroup + "%')";
                                            result = cmd.ExecuteReader();
                                            if (result.Read())
                                            {
                                                if (!result.IsDBNull(0))
                                                {
                                                    serverMaxTM = result.GetDateTime(0);
                                                    serverMaxTMfound = true;
                                                }
                                            }
                                        }
                                        if (serverMaxTMfound)
                                        {
                                            // make a call tableExists on live access before issuing query
                                            //Perform a live access query on the connection and find max TM for loadnumber.
                                            string query = "SELECT MAX(TM) FROM " + sp.Schema + "." + tableName
                                                + " WHERE ITERATION=" + loadNumber;
                                            Object[][] resultsArr = LiveAccessUtil.executeLiveAccessQuery(sp, liveAccessConnectionName, query, false);
                                            if (resultsArr != null && resultsArr.Length > 0 && resultsArr[0].Length > 0)
                                            {
                                                string tm = (string)resultsArr[0][0];
                                                localMaxTMfound = DateTime.TryParse(tm.Trim(), out localMaxTM);
                                            }
                                        }
                                        bool needTxnSnapShotUpdate = true;
                                        if (serverMaxTMfound && localMaxTMfound)
                                        {
                                            if (serverMaxTM.Equals(localMaxTM))
                                                needTxnSnapShotUpdate = false;
                                        }
                                        if (needTxnSnapShotUpdate)
                                        {
                                            bool updated = LiveAccessUtil.updateTxnCommandHistoryFromLiveAccessConnection(sp, liveAccessConnectionName, loadNumber);
                                            if (!updated)
                                            {
                                                Global.systemLog.Error("Error updating TXN_COMMAND_HISTORY from live access connection " + liveAccessConnectionName + " for load number " + loadNumber);
                                            }
                                        }
                                        else
                                        {
                                            Global.systemLog.Debug("Not updating TXN_COMMAND_HISTORY on server as it is in synch with live access connection " + liveAccessConnectionName + " for load number " + loadNumber);
                                        }
                                        if (!nonLiveAccessLoadGroupExists)
                                        {
                                            switch (status)
                                            {
                                                case Status.COMPLETE:
                                                    sr.code = StatusCode.Complete;
                                                    break;
                                                case Status.FAILED:
                                                case Status.SCRIPTFAILURE:
                                                    sr.code = StatusCode.Failed;
                                                    break;
                                            }
                                            if (sr.code == Status.StatusCode.Complete)
                                            {
                                                if (loadComplete)
                                                {
                                                    string schema = System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                                                    ApplicationLoader.updateLoadSuccess(maf, sp, schema, ConnectionPool.getConnection(), sr, ProcessLoad.getLoadNumber(sp, liveAccessLoadGroup), liveAccessLoadGroup);
                                                }
                                                else if (deleteloadComplete && needTxnSnapShotUpdate) // log data deleted succrssfully only if txn_command_history needs to be updated i.e. needTxnSnapShotUpdate is true
                                                {
                                                    Global.systemLog.Debug("Data deleted successfully on live access connection " + liveAccessConnectionName);
                                                }
                                            }
                                            else
                                            {
                                                Global.systemLog.Error("Load failed, space: " + sp.ID.ToString());
                                                // Make sure at this point there is a loadwarehouse entry 3 for this loadnumber, if not put one
                                                Status.setLoadFailedForLiveAccessConnection(sp, ProcessLoad.getLoadNumber(sp, liveAccessLoadGroup) + 1, leConfig, true);
                                            }
                                            return sr;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                

                try
                {
                    if (!tableExists)
                    {
                        return sr;
                    }

                    result = executeStatusQuery(conn, sp, loadNumber);
                }
                catch (OdbcException ex)
                {
                    if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                    {
                        Global.systemLog.Error("Odbc exception checking load status ", ex);
                        return (sr);
                    }
                    ConnectionPool.releaseConnection(conn);

                    if (!isDurationColumnNeededInTxnCommandHistory(ex) && !isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        Global.systemLog.Error("Odbc exception checking load status, retrying: ", ex);
                    }

                    bool sleepAndRetry = true;
                    // check the exception if it contains Duration column
                    if (isDurationColumnNeededInTxnCommandHistory(ex))
                    {
                        try
                        {
                            Database.addDurationColumnToTxnCommandHistory(sp);
                            Database.addProcessingGroupToTxnCommandHistory(sp);
                            conn = ConnectionPool.getConnection(sp.getFullConnectString());
                            result = executeStatusQuery(conn, sp, loadNumber);
                            sleepAndRetry = false;
                        }
                        catch (OdbcException ex2)
                        {
                            Global.systemLog.Error("Odbc exception while adding duration column ", ex2);
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                    else if (isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        try
                        {
                            Database.addProcessingGroupToTxnCommandHistory(sp);
                            conn = ConnectionPool.getConnection(sp.getFullConnectString());
                            result = executeStatusQuery(conn, sp, loadNumber);
                            sleepAndRetry = false;
                        }
                        catch (OdbcException ex2)
                        {
                            Global.systemLog.Error("Odbc exception while adding ProcessingGroup column ", ex2);
                            ConnectionPool.releaseConnection(conn);
                        }
                    }

                    if (sleepAndRetry)
                    {
                        Thread.Sleep(4000);
                        conn = ConnectionPool.getConnection(sp.getFullConnectString());
                        result = executeStatusQuery(conn, sp, loadNumber);
                    }
                }

                StatusCode resultcode = sr.code;
                List<StatusStep> substeps = new List<StatusStep>();
                string processingGroup = null;
                bool hasResults = false;
                PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
                bool isPeProcessingGroupAware = peVersion.IsProcessingGroupAware;
                while (result.Read())
                {
                    hasResults = true;
                    DateTime dt = result.IsDBNull(0) ? DateTime.Now : result.GetDateTime(0);
                    string step = result.GetString(1);
                    string substep = result.IsDBNull(2) ? null : result.GetString(2);
                    processingGroup = result.IsDBNull(3) ? null : result.GetString(3);
                    int code = result.GetInt32(4);
                    long numRows = result.IsDBNull(5) ? -1 : result.GetInt64(5);
                    long numErrors = result.IsDBNull(6) ? -1 : result.GetInt64(6);
                    long numWarnings = result.IsDBNull(7) ? -1 : result.GetInt64(7);
                    long duration = result.IsDBNull(8) ? -1 : result.GetInt64(8);
                    string message = result.IsDBNull(9) ? null : result.GetString(9);
                    if (substep == null || substep.IndexOf(':') < 0)
                    {
                        if (step == "BuildDashboards" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.Started))
                            resultcode = StatusCode.BuildingDashboards;
                        else if (step == "LoadMarker" && code == RUNNING && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.Started))
                            resultcode = StatusCode.GenerateSchema;
                        else if (step == "GenerateSchema" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.BuildingDashboards || resultcode == StatusCode.Started))
                            resultcode = StatusCode.GenerateSchema;
                        else if (step == "LoadStaging" && (resultcode == StatusCode.None || resultcode == StatusCode.Ready || resultcode == StatusCode.GenerateSchema || resultcode == StatusCode.BuildingDashboards || resultcode == StatusCode.Started))
                            resultcode = StatusCode.LoadStaging;
                        else if (step == "LoadWarehouse")
                        {
                            if (code == COMPLETE)
                            {
                                // check for the presence of last step to really make sure this is done                                
                                if (getStatusOfLoadMarkerStep(sp, loadNumber, session) == RUNNING)
                                {
                                    resultcode = StatusCode.LoadWarehouse;
                                }
                                else
                                {
                                    resultcode = StatusCode.Complete;
                                }
                                
                                // check for script group status 
                                // if script is still running, don't mark the load as complete
                                //if (getScriptGroupStatus(sp, loadNumber, session) == RUNNING)
                               // {
                                 //   resultcode = StatusCode.LoadWarehouse;
                                //}
                                //else
                                //{
                                  //  resultcode = StatusCode.Complete;
                                //}
                                 
                                //break;
                            }
                            else if (code == FAILED)
                            {
                                resultcode = StatusCode.Failed;
                                if (message != null && message.Length > 0)
                                    sr.message = message;
                                break;
                            }
                            else
                                resultcode = StatusCode.LoadWarehouse;
                        }
                        else if (step == "DeleteData" && substep == null)
                        {
                            if (code == COMPLETE)
                            {
                                resultcode = StatusCode.Complete;
                                break;
                            }
                            else
                                resultcode = StatusCode.DeleteData;
                        }
                    }
                    else
                    {
                        StatusStep ss = new StatusStep();
                        if (step == "BuildDashboards")
                            ss.step = StatusCode.BuildingDashboards;
                        else if (step == "GenerateSchema")
                            ss.step = StatusCode.GenerateSchema;
                        else if (step == "LoadStaging")
                            ss.step = StatusCode.LoadStaging;
                        else if (step == "LoadWarehouse")
                            ss.step = StatusCode.LoadWarehouse;
                        else if (step == "DeleteData")
                            ss.step = StatusCode.DeleteData;
                        ss.substep = substep;
                        ss.result = code;
                        ss.numRows = numRows;
                        ss.numErrors = numErrors;
                        ss.numWarnings = numWarnings;
                        ss.duration = duration;
                        ss.message = message;
                        ss.processingGroup = processingGroup;
                        substeps.Add(ss);
                    }
                }
                result.Close();
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                if (!hasResults && (resultcode == StatusCode.LoadStaging || resultcode == StatusCode.LoadWarehouse))
                {
                    resultcode = StatusCode.Complete;
                }
                sr.code = resultcode;
                sr.processingGroup = processingGroup;
                sr.substeps = substeps;
                return (sr);
            }
            catch (OdbcException ex)
            {
                sr.code = StatusCode.Failed;
                Global.systemLog.Error("Load failed due to ODBC exception. Space: " + sp.ID.ToString() + ", ", ex);
                return (sr);
            }
            catch (DataException ex)
            {
                sr.code = StatusCode.Failed;
                Global.systemLog.Error("Load failed due to Data exception. Space: " + sp.ID.ToString() + ", ", ex);
                return (sr);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
         * */

        public static bool isDurationColumnNeededInTxnCommandHistory(OdbcException ex)
        {
            if (ex != null && ex.Message != null && ex.Message.Contains("DURATION"))
            {
                return true;
            }

            return false;
        }

        public static StatusResult retreiveAndUpdateBirstLocalGroupStatus(StatusResult initialResultCode, Space sp, int loadNumber)
        {
            StatusResult sr = initialResultCode;
            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                bool birstCloudTxnHistoryTableExists = checkIfTxnCommandHistoryTableExists(null, conn, sp);
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                sr = checkAndRetreiveStatusForOtherGroups(conn, null, sp, Util.LOAD_GROUP_NAME, loadNumber, birstCloudTxnHistoryTableExists, true, sr);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return sr;
            
        }

        public static StatusResult checkAndRetreiveStatusForOtherGroups(QueryConnection conn, HttpSessionState session, Space sp, 
            string loadGroup, int loadNumber, bool tableExists, bool checkOnlyLiveAccess, StatusResult initialResultCode )
        {
            StatusResult sr = initialResultCode;
            MainAdminForm maf = null;
            if (session != null)
            {
                maf = (MainAdminForm)session["MAF"];
            }
            else
            {
                bool newRepository;
                maf = Util.loadRepository(sp, out newRepository);
            }
            if (maf != null)
            {

                QueryCommand cmd = null;
                QueryReader result = null;
                List<string> queryAttemptForLoadGroups = new List<string>();
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    // If there are other loadgroups running
                    if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, loadGroup) >= 0 ? 1 : 0))
                    {
                        // Get a list of the max status code for the most recent iteration of each load group
                        string steps = "";
                        string liveAccessLoadGroup = null;
                        bool nonLiveAccessLoadGroupExists = false;
                        foreach (string s in st.LoadGroups)
                        {
                            if (s != loadGroup)
                            {
                                if (s.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                                {
                                    liveAccessLoadGroup = s;
                                }
                                else
                                {
                                    nonLiveAccessLoadGroupExists = true;
                                }
                                if (steps.Length > 0)
                                    steps += ',';
                                steps += "'" + s + "'";
                            }
                        }
                        if (nonLiveAccessLoadGroupExists && tableExists && !checkOnlyLiveAccess)
                        {
                            try
                            {
                                cmd = conn.CreateCommand();
                                cmd.CommandText = getStatusSQL(sp.Schema, tableName, steps, sp.ConnectString.Contains("jdbc:mysql"), sp.ConnectString.Contains("jdbc:memdb"), sp.ConnectString.StartsWith("jdbc:oracle"));
                                result = cmd.ExecuteReader();
                            }
                            catch (OdbcException ex)
                            {
                                Global.systemLog.Error("Odbc exception checking load status of other load groups: ", ex);
                                break;
                            }
                            while (result.Read())
                            {
                                string gname = result.GetString(0);
                                string step = result.GetString(1);
                                int status = result.GetInt32(2);
                                if (status == Status.RUNNING)
                                {
                                    result.Close();
                                    cmd.Dispose();
                                    sr.code = StatusCode.LoadingAnotherLoadGroup;
                                    return sr;
                                }
                            }
                            result.Close();
                            cmd.Dispose();
                            break;
                        }
                        if ((liveAccessLoadGroup != null)
                                    && (!queryAttemptForLoadGroups.Contains(liveAccessLoadGroup)))
                        {
                            queryAttemptForLoadGroups.Add(liveAccessLoadGroup);
                            Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                            LocalETLConfig leConfig = null;
                            if (localETLConfigs != null && localETLConfigs.ContainsKey(liveAccessLoadGroup))
                            {
                                leConfig = localETLConfigs[liveAccessLoadGroup];
                            }
                            if (leConfig == null)
                            {
                                Global.systemLog.Warn("Could not find local ETL configuration for load group: " + liveAccessLoadGroup + " - processing status will not be reported accurately.");
                                continue;
                            }
                            string liveAccessConnectionName = leConfig.getLocalETLConnection();
                            DatabaseConnection liveAccessDc = null;
                            foreach (DatabaseConnection dc in maf.connection.connectionList)
                            {
                                if (dc.Name != null && dc.Name == liveAccessConnectionName)
                                {
                                    liveAccessDc = dc;
                                    break;
                                }
                            }
                            if (liveAccessDc == null)
                            {
                                Global.systemLog.Warn("Could not find live access connection with name: " + liveAccessConnectionName + " - processing status will not be reported accurately.");
                                continue;
                            }
                            string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                            bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, tableName);
                            if (!liveAccessTableExists)
                            {
                                break;
                            }
                            string statusSQL = getStatusSQLForLiveAccessConnection(liveAccessConnSchemaName, tableName, liveAccessLoadGroup, liveAccessDc.ConnectString.Contains("jdbc:mysql"));
                            //Perform a live access query on the connection and see if it is running.
                            Object[][] results = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, liveAccessConnectionName, statusSQL, false);
                            int status = Status.RUNNING;
                            bool loadComplete = false;
                            bool deleteloadComplete = false;
                            if (results != null && results.Length > 0)
                            {
                                foreach (Object[] row in results)
                                {
                                    if (row.Length == 3)
                                    {
                                        string step = (string)row[1];
                                        string statusStr = (string)row[2];
                                        status = (int)(double.Parse(statusStr.Trim()));
                                        if (status == Status.FAILED || status == Status.SCRIPTFAILURE)
                                        {
                                            break;
                                        }
                                        else if (status == Status.COMPLETE)
                                        {
                                            if (step == "LoadMarker")
                                            {
                                                loadComplete = true;
                                            }
                                            else if (step == "DeleteData")
                                            {
                                                deleteloadComplete = true;
                                            }
                                            else
                                            {
                                                status = Status.RUNNING;
                                            }
                                        }
                                        else if (step == "DeleteData")
                                        {
                                            sr.code = StatusCode.DeleteData;
                                            return sr;
                                        }
                                    }
                                }
                                if ((loadComplete || deleteloadComplete) && status != Status.FAILED && status != Status.SCRIPTFAILURE)
                                {
                                    status = Status.COMPLETE;
                                }
                                if (status == Status.RUNNING)
                                {
                                    sr.code = StatusCode.LoadingAnotherLoadGroup;
                                    return sr;
                                }
                                else
                                {
                                    /* synch up txn_command_history
                                     * first check to see if TxnCommandHistory exists on BirstData, if so match the Max(TM)
                                     * for loadNumber and loadgroup with the Max(TM) of localETL's TxnCommandHistory
                                     * and issue update of txncommandhistory only if match fails.
                                     */
                                    DateTime serverMaxTM = DateTime.MinValue, localMaxTM = DateTime.MinValue;
                                    bool serverMaxTMfound = false, localMaxTMfound = false;
                                    if (tableExists)
                                    {
                                        cmd = conn.CreateCommand();
                                        cmd.CommandText = "SELECT MAX(TM) FROM " + sp.Schema + "." + tableName
                                            + " WHERE ITERATION=" + loadNumber + " AND SUBSTEP LIKE ('" + liveAccessLoadGroup + "%')";
                                        result = cmd.ExecuteReader();
                                        if (result.Read())
                                        {
                                            if (!result.IsDBNull(0))
                                            {
                                                serverMaxTM = result.GetDateTime(0);
                                                serverMaxTMfound = true;
                                            }
                                        }
                                    }
                                    if (serverMaxTMfound)
                                    {
                                        // make a call tableExists on live access before issuing query
                                        //Perform a live access query on the connection and find max TM for loadnumber.
                                        string query = "SELECT MAX(TM) FROM " + sp.Schema + "." + tableName
                                            + " WHERE ITERATION=" + loadNumber;
                                        Object[][] resultsArr = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, liveAccessConnectionName, query, false);
                                        if (resultsArr != null && resultsArr.Length > 0 && resultsArr[0].Length > 0)
                                        {
                                            string tm = (string)resultsArr[0][0];
                                            localMaxTMfound = DateTime.TryParse(tm.Trim(), out localMaxTM);
                                        }
                                    }
                                    bool needTxnSnapShotUpdate = true;
                                    if (serverMaxTMfound && localMaxTMfound)
                                    {
                                        if (serverMaxTM.Equals(localMaxTM))
                                            needTxnSnapShotUpdate = false;
                                    }
                                    if (needTxnSnapShotUpdate)
                                    {
                                        bool updated = LiveAccessUtil.updateTxnCommandHistoryFromLiveAccessConnection(sp, liveAccessConnectionName, loadNumber);
                                        if (!updated)
                                        {
                                            Global.systemLog.Error("Error updating TXN_COMMAND_HISTORY from live access connection " + liveAccessConnectionName + " for load number " + loadNumber);
                                        }
                                    }
                                    else
                                    {
                                        Global.systemLog.Debug("Not updating TXN_COMMAND_HISTORY on server as it is in synch with live access connection " + liveAccessConnectionName + " for load number " + loadNumber);
                                    }
                                    if (!nonLiveAccessLoadGroupExists)
                                    {
                                        switch (status)
                                        {
                                            case Status.COMPLETE:
                                                sr.code = StatusCode.Complete;
                                                break;
                                            case Status.FAILED:
                                            case Status.SCRIPTFAILURE:
                                                sr.code = StatusCode.Failed;
                                                break;
                                        }
                                        if (sr.code == Status.StatusCode.Complete)
                                        {
                                            if (loadComplete)
                                            {
                                                QueryConnection conn2 = null;
                                                try
                                                {
                                                    conn2 = ConnectionPool.getConnection();
                                                    ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn2, sr, ProcessLoad.getLoadNumber(sp, liveAccessLoadGroup), liveAccessLoadGroup);
                                                }
                                                finally
                                                {
                                                    ConnectionPool.releaseConnection(conn2);
                                                }
                                            }
                                            else if (deleteloadComplete && needTxnSnapShotUpdate) // log data deleted succrssfully only if txn_command_history needs to be updated i.e. needTxnSnapShotUpdate is true
                                            {
                                                Global.systemLog.Debug("Data deleted successfully on live access connection " + liveAccessConnectionName);
                                            }
                                        }
                                        else
                                        {
                                            Global.systemLog.Error("Load failed, space: " + sp.ID.ToString());
                                            // Make sure at this point there is a loadwarehouse entry 3 for this loadnumber, if not put one
                                            Status.setLoadFailedForLiveAccessConnection(sp, ProcessLoad.getLoadNumber(sp, liveAccessLoadGroup) + 1, leConfig, true, session);
                                        }
                                        return sr;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }


        public static bool isProcessingGroupColumnNeededInTxnCommandHistory(OdbcException ex)
        {
            if (ex != null && ex.Message != null && ex.Message.Contains("PROCESSINGGROUP"))
            {
                return true;
            }

            return false;
        }

        public static QueryReader executeStatusQuery(QueryConnection conn, Space sp, int loadNumber)
        {
            string query = "SELECT TM,STEP,SUBSTEP,PROCESSINGGROUP,STATUS,NUMROWS,NUMERRORS,NUMWARNINGS,DURATION,MESSAGE FROM " + sp.Schema + "." + tableName
                + " WHERE ITERATION=? ORDER BY TM";
            QueryReader qr = null;

            // If database is Paraccel, call web service to retrieve status info. Otherwise, call standard ODBC query.
            if (conn.isDBTypeParAccel())
            {
                qr = executeWebServiceStatusQuery(sp, conn, query.Replace("?", loadNumber.ToString()));
            }
            else
            {
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                cmd.CommandText = query;
                cmd.Parameters.Add("@ITERATION", OdbcType.VarChar).Value = loadNumber;
                qr = cmd.ExecuteReader();
            }

            return qr;
        }

        public static QueryReader executeWebServiceStatusQuery(Space sp, QueryConnection conn, string query)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            ts.Timeout = 5 * 60000;
            Acorn.TrustedService.QueryResult tqr = ts.getJdbcQueryResults(sp.DatabaseDriver, sp.ConnectString, sp.AdminUser, sp.AdminPwd, query); 
            return new WebServiceQueryReader(tqr, conn.getDBType());
        }

        public static string getStatusSQL(string schema, string tableName, string steps, bool isMySQL, bool isMemDB, bool isOracle)
        {
            string sql;

            if (isMySQL)
            {
                sql = "SELECT A.SUBSTEP,A.STEP,MAX(STATUS) STATUS FROM " + schema + "." + tableName + " A "
                    + "INNER JOIN (SELECT SUBSTEP,MAX(CAST(ITERATION AS SIGNED)) ITERATION FROM " + schema + "." + tableName + " WHERE LOCATE(': ',SUBSTEP)=0 AND SUBSTEP IN (" + steps + ") GROUP BY SUBSTEP) B "
                    + "ON A.SUBSTEP=B.SUBSTEP AND A.ITERATION=B.ITERATION GROUP BY A.SUBSTEP,A.STEP";
            }
            else if (isMemDB)
            {
                sql = "SELECT A.SUBSTEP,A.STEP,MAX(A.STATUS) 'STATUS' FROM " + schema + "." + tableName + " A "
                    + "INNER JOIN MAXONE " + schema + "." + tableName + " B "
                    + "ON A.SUBSTEP=B.SUBSTEP AND A.ITERATION=B.ITERATION WHERE POSITION(': ',B.SUBSTEP)=0 AND B.SUBSTEP IN (" + steps + ") GROUP BY A.SUBSTEP,A.STEP ";
            }
            else if (isOracle)
            {
                sql = "SELECT A.SUBSTEP,A.STEP,MAX(STATUS) STATUS FROM " + schema + "." + tableName + " A "
                    + "INNER JOIN (SELECT SUBSTEP,MAX(CAST(ITERATION AS INT)) ITERATION FROM " + schema + "." + tableName + " WHERE INSTR(SUBSTEP, ': ')=0 AND SUBSTEP IN (" + steps + ") GROUP BY SUBSTEP) B "
                    + "ON A.SUBSTEP=B.SUBSTEP AND A.ITERATION=B.ITERATION GROUP BY A.SUBSTEP,A.STEP";
            }
            else
            {
                sql = "SELECT A.SUBSTEP,A.STEP,MAX(STATUS) STATUS FROM " + schema + "." + tableName + " A "
                    + "INNER JOIN (SELECT SUBSTEP,MAX(CAST(ITERATION AS INT)) ITERATION FROM " + schema + "." + tableName + " WHERE CHARINDEX(': ',SUBSTEP)=0 AND SUBSTEP IN (" + steps + ") GROUP BY SUBSTEP) B "
                    + "ON A.SUBSTEP=B.SUBSTEP AND A.ITERATION=B.ITERATION GROUP BY A.SUBSTEP,A.STEP";
            }
            return sql;
        }

        public static string getStatusSQLForLiveAccessConnection(string schema, string tableName, string loadgroup, bool isMySQL)
        {
            string sql;

            if (isMySQL)
            {
                sql = "SELECT SUBSTEP,STEP, MAX(STATUS) STATUS FROM " + schema + "." + tableName
                    + " WHERE ITERATION = (SELECT MAX(ITERATION) FROM " + schema + "." + tableName + ")"
                    + " AND (SUBSTEP = '" + loadgroup + "' OR SUBSTEP IS NULL)"
                    + " GROUP BY SUBSTEP,STEP";
            }
            else
            {
                sql = "SELECT SUBSTEP,STEP, MAX(STATUS) STATUS FROM " + schema + "." + tableName
                    + " WHERE ITERATION IN (SELECT MAX(ITERATION) FROM " + schema + "." + tableName + ")"
                    + " AND (SUBSTEP = '" + loadgroup + "' OR SUBSTEP IS NULL)"
                    + " GROUP BY SUBSTEP,STEP";
            }
            return sql;
        }

        public static int getPercentComplete(StatusCode code, bool inclDashboards)
        {
            switch (code)
            {
                case StatusCode.BuildingDashboards:
                    return (0);
                case StatusCode.GenerateSchema:
                    return (inclDashboards ? 25 : 0);
                case StatusCode.LoadStaging:
                    return (inclDashboards ? 50 : 33);
                case StatusCode.LoadWarehouse:
                    return (inclDashboards ? 75 : 66);
                case StatusCode.Complete:
                    return (100);
                case StatusCode.Failed:
                    return (0);
                case StatusCode.LoadingAnotherLoadGroup:
                    return (50);
                case StatusCode.Started:
                    return (inclDashboards ? 25 : 0);
                case StatusCode.PostProcess:
                    return (90);
            }
            return (0);
        }

        public static string getPhaseLabel(StatusResult sr, bool inclDashboards)
        {
            StatusCode code = sr.code;
            string pg = sr.processingGroup;
            switch (code)
            {
                case StatusCode.BuildingDashboards:
                    return ("Analyzing data and building QuickDashboards<sup><font style=\"font-size:6pt\">TM</font></sup>");
                case StatusCode.GenerateSchema:
                    return ("Initializing analytical data store");
                case StatusCode.LoadStaging:
                    return ("Normalizing/cleansing uploaded data" + (pg == null || pg == "null" ? "" : (" for processing group: " + pg)));
                case StatusCode.LoadWarehouse:
                    return ("Processing new uploaded data" + (pg == null || pg == "null" ? "" : (" for processing group: " + pg)));
                case StatusCode.Complete:
                    return ("Complete");
                case StatusCode.Failed:
                    return ("Failed");
                case StatusCode.DeleteData:
                    return ("Deleting data");
                case StatusCode.LoadingAnotherLoadGroup:
                    return ("Processing third party data");
                case StatusCode.Started:
                    return ("Processing Started");
                case StatusCode.SFDCExtract:
                    return ("Extraction in progress");
                case StatusCode.PostProcess:
                    return ("Post Processing");
            }
            return ("");
        }

        public static bool getStopProcessing(StatusCode code)
        {
            switch (code)
            {
                case StatusCode.GenerateSchema:
                case StatusCode.LoadStaging:
                case StatusCode.LoadWarehouse:
                case StatusCode.LoadingAnotherLoadGroup:
                    return true;
                default: return false;
            }
        }

        public static bool checkForLoadStartedFailureEntry(HttpSessionState session, Space sp, int loadNumber)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (checkIfTxnCommandHistoryTableExists(session, conn, sp))
                {
                    cmd = conn.CreateCommand();
                    cmd.CommandText = getQueryToCheckProcessStartFailed(sp, loadNumber);
                    Object processNotStarted = cmd.ExecuteScalar();
                    try
                    {
                        int rows = (processNotStarted is int ? (int)processNotStarted : Int32.Parse(processNotStarted.ToString()));
                        return rows > 0;
                    }
                    catch (FormatException)
                    { }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Error while checking the load started failure entry", ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Problem in closing resources", ex);
                }
            }
            return false;
        }

        public static void setLoadFailed(Space sp, int loadNumber, string loadGroup)
        {
            setLoadFailed(sp, loadNumber, loadGroup, false);
        }

        public static bool setLoadFailed(Space sp, int loadNumber, string loadGroup, bool checkIfFailedStatusExists)
        {
            return setLoadFailed(sp, loadNumber, loadGroup, checkIfFailedStatusExists, false);
        }

        public static bool setLoadFailed(Space sp, int loadNumber, string loadGroup, bool checkIfFailedStatusExists, bool markStartFailure)
        {
            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    return true;
                }
                QueryCommand cmd = conn.CreateCommand();

                // Check for loadMarker finished command, if its not there insert one
                if (getStatusOfLoadMarkerStep(sp, loadNumber, null) == RUNNING)
                {
                    cmd.CommandText = getQueryToMarkLoadMarkerFinished(sp, loadGroup, loadNumber, sp.ConnectString);
                    cmd.ExecuteNonQuery();
                }

                if (checkIfFailedStatusExists)
                {
                    cmd.CommandText = getQueryToCheckLoadStagingFinished(sp, loadGroup, loadNumber); // finish ...as in success or fail
                    Object loadStagingFinished = cmd.ExecuteScalar();
                    int numLoadStagingFinishedRows = 0;
                    try
                    {
                        numLoadStagingFinishedRows = (loadStagingFinished is int ? (int)loadStagingFinished : Int32.Parse(loadStagingFinished.ToString()));
                    }
                    catch (FormatException)
                    {
                    }
                    if (numLoadStagingFinishedRows == 0)
                    {
                        cmd.CommandText = getQueryToMarkLoadStagingFailed(sp, loadGroup, loadNumber, sp.ConnectString);
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = getQueryToCheckGenerateSchemaFinished(sp, loadNumber); // finish ...as in success or fail
                    Object generateSchemaFinished = cmd.ExecuteScalar();
                    int numGenerateSchemaFinishedRows = 0;
                    try
                    {
                        numGenerateSchemaFinishedRows = (generateSchemaFinished is int ? (int)generateSchemaFinished : Int32.Parse(generateSchemaFinished.ToString()));
                    }
                    catch (FormatException)
                    {
                    }
                    if (numGenerateSchemaFinishedRows == 0)
                    {
                        cmd.CommandText = getQueryToCheckGenerateSchemaStarted(sp, loadNumber);
                        Object generateSchemaRunningEntries = cmd.ExecuteScalar();
                        int numGenerateSchemaRunningRows = 0;
                        try
                        {
                            numGenerateSchemaRunningRows = (generateSchemaRunningEntries is int ? (int)generateSchemaRunningEntries : Int32.Parse(generateSchemaRunningEntries.ToString()));
                        }
                        catch (FormatException)
                        {
                        }

                        if (numGenerateSchemaRunningRows > 0)
                        {
                            // Only insert generate schema entry if there was a running entry
                            cmd.CommandText = getQueryToMarkGenerateSchemaFailed(sp, loadNumber, sp.ConnectString);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    
                    cmd.CommandText = getQueryToCheckLoadFailed(sp, loadGroup, loadNumber);
                    Object o = cmd.ExecuteScalar();
                    int numRowsFound = 0;
                    try
                    {
                        numRowsFound = (o is int ? (int)o : Int32.Parse(o.ToString()));
                    }
                    catch (FormatException)
                    {
                    }
                    if (numRowsFound > 0)
                    {
                        // no need to insert a duplicate entry
                        cmd.Dispose();
                        return true;
                    }
                }
                // get the data for the step/substep/iteration/status
                cmd.CommandText = getQueryToMarkLoadFailed(sp, loadGroup, loadNumber, sp.ConnectString);
                cmd.ExecuteNonQuery();

                if (markStartFailure)
                {
                    cmd.CommandText = getQueryToMarkLoadStartFailure(sp, loadGroup, loadNumber, sp.ConnectString);
                    cmd.ExecuteNonQuery();
                }
                cmd.Dispose();
                return true;
            }
            catch (OdbcException ex)
            {
                Global.systemLog.Warn("Error while setting the load fail status: ", ex);
                return false;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }


        public static void setLoadFailedForLiveAccessConnection(Space sp, int loadNumber, LocalETLConfig leConfig, bool checkIfFailedStatusExists, HttpSessionState session)
        {
            if (leConfig != null)
            {
                string liveAccessConnectionName = leConfig.getLocalETLConnection();
                DatabaseConnection liveAccessDc = null;
                bool newRepository;
                MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    if (dc.Name != null && dc.Name == liveAccessConnectionName)
                    {
                        liveAccessDc = dc;
                        break;
                    }
                }
                if (liveAccessDc == null)
                {
                    Global.systemLog.Warn("Could not find live access connection with name: " + liveAccessConnectionName + " - cannot update status for Failed Load.");
                    return;
                }
                string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, tableName);
                if (!liveAccessTableExists)
                {
                    return;
                }

                string query = null;
                if (checkIfFailedStatusExists)
                {
                    query = getQueryToCheckLoadFailed(sp, leConfig.getLocalETLLoadGroup(), loadNumber);
                    object[][] result = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, leConfig.getLocalETLConnection(), query, false);
                    if (result != null && result.Length > 0)
                    {
                        int numRowsFound = 0;
                        int.TryParse((string)result[0][0], out numRowsFound);
                        if (numRowsFound > 0)
                        {
                            // no need to insert a duplicate entry
                            return;
                        }
                    }
                }
                // get the data for the step/substep/iteration/status
                query = getQueryToMarkLoadFailed(sp, leConfig.getLocalETLLoadGroup(), loadNumber, liveAccessDc.ConnectString);
                int noRowsInserted = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, leConfig.getLocalETLConnection(), query);
                if (noRowsInserted != 1)
                {
                    Global.systemLog.Error("Could not insert record for load failure on live access connection : " + liveAccessConnectionName + " - cannot update status for Failed Load.");
                }
            }
        }

        private static bool loadStepsExists(QueryConnection conn, Space sp, int loadNumber)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getQueryToCheckLoadExists(sp, loadNumber);
                Object loadExists = cmd.ExecuteScalar();
                int numLoadExistsRows = 0;
                try
                {
                    numLoadExistsRows = (loadExists is int ? (int)loadExists : Int32.Parse(loadExists.ToString()));
                }
                catch (FormatException)
                {
                }
                return numLoadExistsRows > 0;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in query to find load exists", ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception ex) {
                    Global.systemLog.Warn("Error in disposing command ", ex);
                }
            }
            return false;
        }

        private static string getQueryToCheckLoadExists(Space sp, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName 
                        + " WHERE ITERATION=" + loadNumber);
        }

        private static string getQueryToCheckLoadFailed(Space sp, string loadgroup, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName
                        + " WHERE COMMAND_TYPE = 'ETL' AND STEP='LoadWarehouse' AND SUBSTEP='" + loadgroup + "'" +
                        " AND ITERATION=" + loadNumber + " AND STATUS=" + FAILED);
        }

        private static string getQueryToCheckLoadStagingFinished(Space sp, string loadgroup, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName
                        + " WHERE COMMAND_TYPE = 'ETL' AND STEP='LoadStaging' AND SUBSTEP='" + loadgroup + "'" +
                        " AND ITERATION=" + loadNumber + " AND (STATUS=" + FAILED + " OR STATUS=" + COMPLETE + ")" );
        }

        private static string getQueryToCheckGenerateSchemaStarted(Space sp, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName
                        + " WHERE COMMAND_TYPE = 'ETL' AND STEP='GenerateSchema' AND SUBSTEP is NULL" +
                        " AND ITERATION=" + loadNumber + " AND (STATUS=" + RUNNING + ")");
        }

        private static string getQueryToCheckGenerateSchemaFinished(Space sp, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName
                        + " WHERE COMMAND_TYPE = 'ETL' AND STEP='GenerateSchema' AND SUBSTEP is NULL" +
                        " AND ITERATION=" + loadNumber + " AND (STATUS=" + FAILED + " OR STATUS=" + COMPLETE + ")");
        }

        private static string getQueryToCheckProcessStartFailed(Space sp, int loadNumber)
        {
            return ("SELECT COUNT(*) FROM " + sp.Schema + "." + tableName
                        + " WHERE COMMAND_TYPE = 'ETL' AND STEP='ProcessStarted' " +
                        " AND ITERATION=" + loadNumber + " AND (STATUS=" + FAILED + ")");
        }

        private static string getCurrentTimeStamp(string connectString)
        {
            string timestr = "getdate()";
            if (connectString.StartsWith("jdbc:mysql"))
                timestr = "current_timestamp()";
            else if (connectString.StartsWith("jdbc:sap") || connectString.StartsWith("jdbc:oracle"))
                timestr = "current_timestamp";
            else if (connectString.StartsWith("jdbc:memdb"))
                timestr = "'" + DateTime.Now.ToString() + "'";
            return timestr;
        }

        private static string getQueryToMarkLoadFailed(Space sp, string loadgroup, int loadNumber, string connectString)
        {
            string timestr = getCurrentTimeStamp(connectString);
            return ("INSERT INTO " + sp.Schema + "." + tableName
                + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" + timestr + ",'ETL','LoadWarehouse'," + "'" + loadgroup + "'" + "," + loadNumber + "," + FAILED + ")");
        }


        private static string getQueryToMarkLoadStagingFailed(Space sp, string loadgroup, int loadNumber, string connectString)
        {
            string timestr = getCurrentTimeStamp(connectString);
            return ("INSERT INTO " + sp.Schema + "." + tableName
                + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" + timestr + ",'ETL','LoadStaging'," + "'" + loadgroup + "'" + "," + loadNumber + "," + FAILED + ")");
        }

        private static string getQueryToMarkLoadMarkerFinished(Space sp, string loadgroup, int loadNumber, string connectString)
        {
            string timestr = getCurrentTimeStamp(connectString);
            return ("INSERT INTO " + sp.Schema + "." + tableName
                + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" + timestr + ",'ETL','LoadMarker'," + "'" + loadgroup + "'" + "," + loadNumber + "," + COMPLETE + ")");
        }

        private static string getQueryToMarkGenerateSchemaFailed(Space sp, int loadNumber, string connectString)
        {
            string timestr = getCurrentTimeStamp(connectString);
            return ("INSERT INTO " + sp.Schema + "." + tableName
                + " (TM,COMMAND_TYPE,STEP,ITERATION,STATUS) VALUES (" + timestr + ",'ETL','GenerateSchema'," + loadNumber + "," + FAILED + ")");
        }

        private static string getQueryToMarkLoadStartFailure(Space sp, string loadgroup, int loadNumber, string connectString)
        {
            string timestr = getCurrentTimeStamp(connectString);
            return ("INSERT INTO " + sp.Schema + "." + tableName
                + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" + timestr + ",'ETL','ProcessStarted'," + "'" + loadgroup + "'" + "," + loadNumber + "," + FAILED + ")");
        }

        public static bool hasLoadedData(Space sp, string loadGroup)
        {
            QueryConnection conn = null;
            try
            {
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);               
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    return false;
                }
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                string type = null;
                if (sp.DatabaseType == "Infobright")
                    type = "UNSIGNED";
                else if (sp.DatabaseType == "MemDB")
                    type = "INTEGER";
                else
                    type = "INT";
                string query = "SELECT MAX(CAST(ITERATION AS " + type + ")) FROM " + sp.Schema + "." + tableName +
                    " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse'";
                cmd.CommandText = query;
                if (loadGroup != null)
                    cmd.CommandText += " AND SUBSTEP='" + loadGroup + "'";
                object result = (sp.DatabaseType.Equals(Database.PARACCEL)) ? executeWebServiceStatusQuery(sp, conn, query) : cmd.ExecuteScalar();
                cmd.Dispose();
                if (result == null || result == System.DBNull.Value)
                {
                    Global.systemLog.Debug("Data not loaded for space " + sp.ID.ToString() + (loadGroup != null ? " for loadgroup " + loadGroup : ""));
                    return false;
                }
                return true;
            }
            catch (OdbcException ex)
            {
                Global.systemLog.Error("Error while determining loaded data for space " + sp.ID.ToString() + (loadGroup != null ? " for loadgroup " + loadGroup : ""), ex);
                return false;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static int getMaxSuccessfulLoadID(Space sp, string loadGroup)
        {
            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    ConnectionPool.releaseConnection(conn);
                    return 0;
                }
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                if (sp.ConnectString.Contains("jdbc:sqlserver:")) // SQL Server
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS INT)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:mysql:")) // MySQL and Infobright
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS SIGNED)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:oracle:"))   // Oracle
                {
                    cmd.CommandText = "SELECT MAX(TO_NUMBER(ITERATION)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:paraccel:")) // ParAccel
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS INT)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:postgresql:")) // Redshift
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS INT)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:sap:")) // Hana
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS INT)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else if (sp.ConnectString.Contains("jdbc:memdb:")) // MemDB (XXX should have accepted INT rather than change base code)
                {
                    cmd.CommandText = "SELECT MAX(CAST(ITERATION AS INTEGER)) FROM " + sp.Schema + "." + tableName +
                        " WHERE STATUS=" + Status.COMPLETE + " AND STEP='LoadWarehouse' AND SUBSTEP='" + loadGroup +
                        "'";
                }
                else
                {
                    throw new Exception("No valid SQL for getting the MAX ITERATION for this database type, please notify development so that they can add support: " + sp.ConnectString);
                }

                object result = cmd.ExecuteScalar();
                cmd.Dispose();
                if (result == null || result == System.DBNull.Value)
                    return 0;
                if (typeof(int).Equals(result.GetType()))
                    return (int)result;
                try
                {
                    return (int)Double.Parse(result.ToString());
                }
                catch (Exception e)
                {
                    Global.systemLog.Warn("Could not get the max load id, returning 0 (parse exception on the result '" + result.ToString() + "') - " + e.Message);
                    return 0;
                }
            }
            catch (OdbcException e2)
            {
                Global.systemLog.Warn("Could not get the max load id, returning 0 (query exception) - " + e2.Message);
                return 0;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setBuildDashboards(Space sp, int status)
        {
            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                    return;
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                cmd.CommandText = "INSERT INTO " + sp.Schema + "." + tableName
                    + " (TM,COMMAND_TYPE,STEP,ITERATION,STATUS) VALUES ('" + DateTime.Now + "','DASHBOARDS','QuickDashboards'," + (sp.LoadNumber + 1) + "," + status + ")";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (OdbcException)
            {
                return;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

        }

        public static void setBuildDashboardsOnLiveAccessConnection(Space sp, int status, LocalETLConfig leConfig)
        {
            if (leConfig != null)
            {
                string liveAccessConnectionName = leConfig.getLocalETLConnection();
                DatabaseConnection liveAccessDc = null;
                bool newRepository;
                MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    if (dc.Name != null && dc.Name == liveAccessConnectionName)
                    {
                        liveAccessDc = dc;
                        break;
                    }
                }
                if (liveAccessDc == null)
                {
                    Global.systemLog.Warn("Could not find live access connection with name: " + liveAccessConnectionName + " - cannot update status for BuildDashboards.");
                    return;
                }
                string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, tableName);
                if (!liveAccessTableExists)
                {
                    return;
                }
                string query = "INSERT INTO " + sp.Schema + "." + tableName
                    + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" +
                    getCurrentTimeStamp(liveAccessDc.ConnectString) +
                    ",'DASHBOARDS','QuickDashboards','" + leConfig.getLocalETLLoadGroup() + "'," + (sp.LoadNumber + 1) + "," + status + ")";
                int result = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, liveAccessConnectionName, query);
                if (result != 1)
                {
                    Global.systemLog.Warn("Could not update status for BuildDashboards for live access connection:" + liveAccessConnectionName);
                }
            }
        }

        public static DateTime getPublishStartTime(Space sp, int loadNumber, string loadGroup)
        {
            DateTime startTime = DateTime.MinValue;
            DataTable dt = getPublishStartHistory(null, sp, loadNumber, loadGroup, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    int iteration = (int)dr["LoadNumber"];
                    if (iteration == loadNumber && dr["DateTime"] != null)
                    {
                        startTime = (DateTime)dr["DateTime"];
                        break;
                    }
                }
            }

            return startTime;
        }

        public static DataTable getPublishStartHistory(HttpSessionState session, Space sp, int loadNumber, string loadGroup)
        {
            return getPublishStartHistory(session, sp, loadNumber, loadGroup, true);
        }

        public static DataTable getPublishStartHistory(HttpSessionState session, Space sp, int loadNumber, string loadGroup, bool includePreviousLoadNumbers)
        {

            QueryConnection conn = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                DataTable dt = new DataTable();
                dt.Columns.Add("DateTime", typeof(DateTime));
                dt.Columns.Add("LoadNumber", typeof(int));
                dt.Columns.Add("Step");
                dt.Columns.Add("Substep");
                dt.Columns.Add("LoadGroup");

                try
                {
                    bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                    bool isMemDB = sp.ConnectString.Contains("jdbc:memdb");
                    if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                    {
                        return (dt);
                    }
                    QueryCommand cmd = conn.CreateCommand();
                    // get the data for the step/substep/iteration/status
                    cmd.CommandText = "SELECT MIN(TM),ITERATION,STEP,SUBSTEP FROM " + sp.Schema + "." + tableName;
                    string castIteration = null;
                    if (isMysql)
                    {
                        castIteration = "CAST(ITERATION AS SIGNED)";
                    }
                    else if (isMemDB)
                    {
                        castIteration = "CAST(ITERATION AS INTEGER)";
                    }
                    else
                    {
                        castIteration = "CAST(ITERATION AS INT)";
                    }
                    string where = includePreviousLoadNumbers ? castIteration + "<=?" : castIteration + "=?";
                    where += " AND ";
                    where += "(SUBSTEP IS NULL OR SUBSTEP='" + loadGroup + "')";
                    where += " AND ";
                    where += "STATUS =" + RUNNING + " AND (STEP='GenerateSchema' OR STEP='LoadStaging' OR STEP='LoadMarker')";
                    // make sure that for SFORCE iteration is floored by the SFORCE_LOAD_NUMBER
                    if (DefaultUserPage.isIndependentMode(sp))
                    {
                        if (loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                        {
                            where += " AND " + castIteration + ">=" + SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                        }
                        else
                        {
                            where += " AND " + castIteration + "<" + SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                        }
                    }
                    string groupBy = " GROUP BY  SUBSTEP, ITERATION, STEP";
                    string orderBy = " ORDER BY ITERATION DESC";
                    cmd.CommandText += " WHERE " + where + groupBy + orderBy;
                    cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;

                    Global.systemLog.Debug(cmd.CommandText);
                    QueryReader result = cmd.ExecuteReader();
                    Dictionary<int, DataRow> loadNumberToDataRowMapping = new Dictionary<int, DataRow>();

                    while (result.Read())
                    {
                        DateTime date = result.GetDateTime(0);
                        int loadNum = result.GetInt32(1);
                        string step = result.GetString(2);
                        string substep = result.IsDBNull(3) ? null : result.GetString(3);

                        DataRow exitingRowForLoadNumber = null;
                        if (loadNumberToDataRowMapping.ContainsKey(loadNum))
                        {
                            exitingRowForLoadNumber = loadNumberToDataRowMapping[loadNum];
                        }
                        if (exitingRowForLoadNumber != null)
                        {
                            string existingRowStep = (string)exitingRowForLoadNumber[2];
                            // if start marker is already populated, no overwrite required
                            if (existingRowStep == "LoadMarker")
                            {
                                continue;
                            }

                            // if current step is of higher priority and there is a previous record for that
                            // iteration, overwrite it. 
                            // The order of steps for start time retrival is : 
                            // 1. LoadMarker 2. GenerateSchema 3. LoadStaging
                            if (step == "LoadMarker" || step == "GenerateSchema")
                            {
                                dt.Rows.Remove(exitingRowForLoadNumber);
                            }
                            else
                            {
                                continue;
                            }
                        }

                        DataRow dr = dt.NewRow();
                        dr[0] = date;
                        dr[1] = loadNum;
                        dr[2] = step;
                        dr[3] = substep;
                        dr[4] = loadGroup;
                        dt.Rows.Add(dr);
                        if (exitingRowForLoadNumber == null)
                        {
                            loadNumberToDataRowMapping.Add(loadNum, dr);
                        }
                        else if (exitingRowForLoadNumber != null)
                        {
                            loadNumberToDataRowMapping[loadNum] = dr;
                        }
                    }
                    result.Close();
                    cmd.Dispose();
                    return (dt);
                }
                catch (OdbcException ex)
                {
                    Global.systemLog.Error(ex);
                    return (dt);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static DataTable getPublishedRows(Space sp, int loadNumber)
        {

            QueryConnection conn = null;
            DataTable dt = new DataTable();
            dt.Columns.Add("TableName");
            dt.Columns.Add("Rows", typeof(long));

            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    ConnectionPool.releaseConnection(conn);
                    return (dt);
                }
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                cmd.CommandText = "SELECT SUBSTEP, NUMROWS FROM " + sp.Schema + "." + tableName;
                string where = "ITERATION=?";
                where += " AND ";
                where += "(SUBSTEP IS NOT NULL)";
                where += " AND ";
                where += "STATUS =" + COMPLETE + " AND (STEP='LoadStaging')";
                cmd.CommandText += " WHERE " + where;
                cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;

                Global.systemLog.Debug(cmd.CommandText);
                QueryReader result = cmd.ExecuteReader();
                while (result.Read())
                {
                    string tname = result.IsDBNull(0) ? null : result.GetString(0);
                    long numrows = 0;
                    if (!result.IsDBNull(1))
                        numrows = result.GetInt64(1);

                    DataRow dr = dt.NewRow();
                    dr[0] = tname;
                    dr[1] = numrows;
                    dt.Rows.Add(dr);
                }
                result.Close();
                cmd.Dispose();
                return (dt);
            }
            catch (OdbcException ex)
            {
                Global.systemLog.Error(ex);
                return (dt);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static DateTime getPublishEndTime(Space sp, int loadNumber, string loadGroup)
        {
            DateTime endTime = DateTime.MinValue;
            QueryConnection conn = null;
            string loadWarehouseStep = "LoadWarehouse";
            string loadMarkerStep = "LoadMarker";
            Dictionary<String, DateTime> stepToDateMapping = new Dictionary<string, DateTime>();
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                if (Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {

                    QueryCommand cmd = conn.CreateCommand();
                    // get the data for the step/substep/iteration/status
                    cmd.CommandText = "SELECT TM, STEP,SUBSTEP,STATUS FROM " + sp.Schema + "." + tableName;

                    string where = " ITERATION=? ";
                    where += " AND " + "(SUBSTEP IS NULL OR SUBSTEP='" + loadGroup + "')";
                    where += " AND " + "STATUS<>" + RUNNING + " AND (STEP='" + loadWarehouseStep + "' OR STEP = '" + loadMarkerStep + "')";
                    string orderBy = " ORDER BY TM DESC ";
                    cmd.CommandText += " WHERE " + where + orderBy;
                    cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;
                    Global.systemLog.Debug(cmd.CommandText);
                    QueryReader result = cmd.ExecuteReader();

                    while (result.Read())
                    {
                        if (result.IsDBNull(0) || !(result.GetValue(0) is DateTime))
                            continue;
                        DateTime date = result.GetDateTime(0);
                        string step = result.GetString(1);
                        string substep = result.IsDBNull(2) ? null : result.GetString(2);
                        int code = result.GetInt32(3);
                        if (!stepToDateMapping.ContainsKey(step))
                        {
                            stepToDateMapping.Add(step, date);
                        }
                        else
                        {
                            // get the datetime stored for the step, replace it with the later one
                            DateTime existingValue = (DateTime)stepToDateMapping[step];
                            if (date.CompareTo(existingValue) > 0)
                            {
                                stepToDateMapping[step] = date;
                            }
                        }
                    }
                    result.Close();
                    cmd.Dispose();

                    if (stepToDateMapping != null && stepToDateMapping.Count > 0)
                    {
                        if (stepToDateMapping.ContainsKey(loadMarkerStep))
                        {
                            endTime = stepToDateMapping[loadMarkerStep];
                        }
                        else if (stepToDateMapping.ContainsKey(loadWarehouseStep))
                        {
                            endTime = stepToDateMapping[loadWarehouseStep];
                        }
                    }
                }
            }
            catch (OdbcException ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return endTime;

        }

        public static DataTable getPublishHistory(HttpSessionState session, Space sp, int loadNumber, string loadGroup, bool includeSubsteps, bool resultsOnly, int maxRows)
        {
            return getPublishHistory(session, sp, loadNumber, loadGroup, includeSubsteps, resultsOnly, maxRows, true, false, true, false);
        }

        public static DataTable getPublishHistory(HttpSessionState session, Space sp, int loadNumber, string loadGroup, bool includeSubsteps, bool resultsOnly, int maxRows, bool includePreviousLoadNumbers, bool includeDuration)
        {
            return getPublishHistory(session, sp, loadNumber, loadGroup, includeSubsteps, resultsOnly, maxRows, includePreviousLoadNumbers, false, true, false);
        }

        public static DataTable getPublishHistory(HttpSessionState session, Space sp, int loadNumber, string loadGroup, bool includeSubsteps, bool resultsOnly, int maxRows, bool includePreviousLoadNumbers, bool completedOnly, bool getProcessingGroup, bool includeDuration)
        {
            QueryConnection conn = null;
            DataTable dt = new DataTable();
            dt.Columns.Add("DateTime", typeof(DateTime));
            dt.Columns.Add("LoadNumber", typeof(int));
            dt.Columns.Add("Step");
            dt.Columns.Add("Substep");
            dt.Columns.Add("ResultCode", typeof(int));
            dt.Columns.Add("Result");
            dt.Columns.Add("PublishDate", typeof(DateTime));
            dt.Columns.Add("LoadGroup");
            dt.Columns.Add("NumRows", typeof(long));
            dt.Columns.Add("NumErrors", typeof(long));
            dt.Columns.Add("NumWarnings", typeof(long));
            dt.Columns.Add("Duration", typeof(long));
            dt.Columns.Add("ProcessingGroups");
            try
            {
                bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                bool isMemDB = sp.ConnectString.Contains("jdbc:memdb");
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    return (dt);
                }
                QueryCommand cmd = conn.CreateCommand();
                // get the data for the step/substep/iteration/status
                string where = "";
                string castIteration = null;
                if (isMysql)
                {
                    cmd.CommandText = "SELECT MAX(TM) TM,ITERATION,STEP,SUBSTEP,MAX(STATUS) STATUS,NUMROWS,NUMERRORS,NUMWARNINGS" + (includeDuration ? ",DURATION" : "") + " FROM " + sp.Schema + "." + tableName;
                    castIteration = "CAST(ITERATION AS SIGNED)";
                }
                else if (isMemDB)
                {
                    cmd.CommandText = "SELECT MAX(TM) 'TM',ITERATION,STEP,SUBSTEP,MAX(STATUS) 'STATUS',NUMROWS,NUMERRORS,NUMWARNINGS" + (includeDuration ? ",DURATION" : "") + " FROM " + sp.Schema + "." + tableName;
                    castIteration = "CAST(ITERATION AS INTEGER)";
                }
                else
                {
                    cmd.CommandText = "SELECT MAX(TM) TM,ITERATION,STEP,SUBSTEP,MAX(STATUS) STATUS,NUMROWS,NUMERRORS,NUMWARNINGS" + (includeDuration ? ",DURATION" : "") + " FROM " + sp.Schema + "." + tableName;
                    castIteration = "CAST(ITERATION AS INT)";
                }
                if (loadNumber >= 0)
                {
                    if (DefaultUserPage.isIndependentMode(sp))
                    {
                        where = includePreviousLoadNumbers ? castIteration + "<=?" : castIteration + "=?";
                        if (loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                        {
                            where += " AND " + castIteration + " >=" + SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                        }
                        else
                        {
                            where += " AND " + castIteration + " <" + SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                        }
                    }
                }
                if (!includeSubsteps)
                {
                    if (where.Length > 0)
                        where += " AND ";
                    where += "(SUBSTEP IS NULL OR SUBSTEP='" + loadGroup + "')";
                }
                if (resultsOnly)
                {
                    string resultStep = "LoadWarehouse";
                    if (where.Length > 0)
                        where += " AND ";
                    where += "STATUS<>" + RUNNING + " AND STEP='" + resultStep + "'";
                }
                else if (completedOnly)
                {
                    if (where.Length > 0)
                        where += " AND ";
                    where += "STATUS=" + COMPLETE;
                }
                string groupby = " GROUP BY ITERATION,STEP,SUBSTEP,NUMROWS,NUMERRORS,NUMWARNINGS" + (includeDuration ? ",DURATION" : "");
                cmd.CommandText += (where.Length > 0 ? " WHERE " + where : "") + groupby + " ORDER BY TM DESC";
                if (loadNumber >= 0)
                    cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;
                Global.systemLog.Debug(cmd.CommandText);
                QueryReader result = null;
                string queryExecuted = cmd.CommandText;
                try
                {

                    result = cmd.ExecuteReader();
                }
                catch (OdbcException ex)
                {
                    if (isDurationColumnNeededInTxnCommandHistory(ex))
                    {
                        Database.addDurationColumnToTxnCommandHistory(sp);
                        Database.addProcessingGroupToTxnCommandHistory(sp);
                        // retry again
                        cmd.Dispose();
                        cmd = conn.CreateCommand();
                        cmd.CommandText = queryExecuted;
                        if (loadNumber >= 0)
                            cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;
                        result = cmd.ExecuteReader();
                    }
                    else if (isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        Database.addProcessingGroupToTxnCommandHistory(sp);
                        // retry again
                        cmd.Dispose();
                        cmd = conn.CreateCommand();
                        cmd.CommandText = queryExecuted;
                        if (loadNumber >= 0)
                            cmd.Parameters.Add("@ITERATION", OdbcType.Int).Value = loadNumber;
                        result = cmd.ExecuteReader();
                    }
                    else
                    {
                        Global.systemLog.Error("Error while getting publish history ", ex);
                    }
                }
                int count = 0;
                bool hasResult = false;
                while (result != null && result.Read() && (count++ < maxRows || maxRows < 0))
                {
                    DateTime date;
                    if (result.IsDBNull(0) || !(result.GetValue(0) is DateTime))
                        date = DateTime.Now;
                    else
                        date = result.GetDateTime(0);
                    int loadnum = result.GetInt32(1);
                    string step = result.GetString(2);
                    string substep = result.IsDBNull(3) ? null : result.GetString(3);
                    int code = result.GetInt32(4);
                    long numrows = result.IsDBNull(5) ? 0 : result.GetInt64(5);
                    long numerrors = result.IsDBNull(6) ? 0 : result.GetInt64(6);
                    long numwarnings = result.IsDBNull(7) ? 0 : result.GetInt64(7);
                    long duration = includeDuration ?  (result.IsDBNull(8) ? 0 : result.GetInt64(8)) : 0;
                    DataRow dr = dt.NewRow();
                    dr[0] = date;
                    dr[1] = loadnum;
                    dr[2] = step;
                    dr[3] = substep;
                    dr[4] = code;
                    if (code == RUNNING)
                        dr[5] = RUNNING_STR;
                    else if (code == COMPLETE)
                        dr[5] = COMPLETE_STR;
                    else
                        dr[5] = FAILED_STR;
                    dr[7] = loadGroup;
                    dr[8] = numrows;
                    dr[9] = numerrors;
                    dr[10] = numwarnings;
                    dr[11] = duration;
                    dr[12] = null;
                    dt.Rows.Add(dr);
                    hasResult = true;
                }
                if (getProcessingGroup && hasResult)
                {
                    Dictionary<int, string> loadNumberToPGroupMapping = getProcessingGroups(sp);
                    if (loadNumberToPGroupMapping != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i][1] != System.DBNull.Value)
                            {
                                int iteration = (int)dt.Rows[i][1];
                                dt.Rows[i][12] = loadNumberToPGroupMapping.ContainsKey(iteration) ? loadNumberToPGroupMapping[iteration] : null;
                            }
                        }
                    }
                }
                result.Close();
                cmd.Dispose();
                    
                return (dt);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                return (dt);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        /// <summary>
        /// This is basically useful for the use cases where txn_command_history already has the enteries for the iteration number.
        /// E.g. DeleteLast/ReProcess OR Failed/Reprocess.
        /// During delete data, entries from the old iteration are deleted and new set of enteries with
        /// step DeleteData are inserted by Performance Engine using the same iteration number. There are cases/bugs
        /// which suggests that Acorn polled the status from txn_command_history before PE had a chance to update txn_command_history.
        /// In this particular case, we get the status from the old completed/failed iteration (since polling is done at the iteration level
        /// and not at the step level), a false alarm is sent to the end user (completed/failed) where infact it hasn't even started.
        /// So before spawning deleteData thread, we update txn_command_history from the Acorn side by just deleting the old enteries
        /// and inserting a new record DeleteData with running status to mark the start. The similar concept works the other way around too
        /// when you have Deleted and reprocessed.
        /// </summary>
        /// <param name="sp">Space</param>
        /// <param name="loadNumber">LoadNumber</param>
        /// <returns></returns>
        public static bool updateTxnStatusWithNextOperation(Space sp, int loadNumber, string insertOperation, bool retryFailedLoad)
        {
            if (sp == null)
            {
                Global.systemLog.Error("Session Space not available. Cannot update txnStatus with DeleteData");
                return false;
            }

            if (loadNumber < 0)
            {
                Global.systemLog.Error("Session Space not available. Cannot update txnStatus with DeleteData");
                return false;
            }

            bool updated = false;
            QueryConnection conn = null;
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            string statusTable = sp.Schema + "." + tableName;
            bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
            bool isMemDB = sp.ConnectString.Contains("jdbc:memdb");
            bool isRedshift = sp.DatabaseType == "Redshift";    // Redshift seems to have trouble with the transactions being used here, issues an error about SAVEPOINT
            // Error in updating S_N9bb28b01_403b_4c7e_b96b_e16ed71843ba.TXN_COMMAND_HISTORY to make ready for next operation
            // System.Data.Odbc.OdbcException (0x80131937): ERROR [HY000] ERROR: SQL command "SAVEPOINT _EXEC_SVP_0F57D290" not supported.
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                {
                    Global.systemLog.Warn("Unable to update " + tableName + " - Table does not exist for space id " + sp.ID.ToString());
                    // If there is no table, then no need to continue;
                    return true;
                }

                cmd = conn.CreateCommand();
                if (!isRedshift)
                {
                    transaction = conn.BeginTransaction();
                    cmd.Transaction = transaction;
                }
                if (!retryFailedLoad)
                {
                    // Delete records from old iteration number. 
                    cmd.CommandText = "DELETE FROM " + statusTable + " WHERE ITERATION = ?";
                    cmd.Parameters.Add("ITERATION", OdbcType.VarChar).Value = loadNumber;
                    Global.systemLog.Info("Deleting previous for spaceId = " + sp.ID + " and iteration = " + loadNumber);
                    int noRowsDeleted = cmd.ExecuteNonQuery();
                    Global.systemLog.Info("Deletion done from " + sp.Schema + "." + tableName + ". Rows deleted = " + noRowsDeleted);

                    if (insertOperation != null)
                    {
                        // Insert another record for start of DeleteData/GenerateSchema
                        cmd.Parameters.Clear();
                        cmd.CommandText = "INSERT INTO " + statusTable +
                            "(TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (?,?,?,?,?,?)";
                        DateTime dtnow = DateTime.Now;
                        dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                        cmd.Parameters.Add("TM", OdbcType.DateTime).Value = dtnow;
                        cmd.Parameters.Add("COMMAND_TYPE", OdbcType.VarChar).Value = "ETL";
                        cmd.Parameters.Add("STEP", OdbcType.VarChar).Value = insertOperation;
                        cmd.Parameters.Add("SUBSTEP", OdbcType.VarChar).Value = System.DBNull.Value;
                        cmd.Parameters.Add("ITERATION", OdbcType.VarChar).Value = loadNumber;
                        cmd.Parameters.Add("STATUS", OdbcType.Int).Value = RUNNING;
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    //Only delete failed entries and LoadMarker failed/complete record
                    cmd.CommandText = "DELETE FROM " + statusTable
                    + " WHERE ITERATION = ?"
                    + " AND (STATUS = " + Status.FAILED + " OR STATUS = " + Status.SCRIPTFAILURE
                    + " OR (STATUS <> " + Status.RUNNING + " AND STEP = 'LoadMarker'))";
                    cmd.Parameters.Add("ITERATION", OdbcType.VarChar).Value = loadNumber;
                    Global.systemLog.Info("Deleting previous for spaceId = " + sp.ID + " and iteration = " + loadNumber);
                    int noRowsDeleted = cmd.ExecuteNonQuery();
                    Global.systemLog.Info("Deletion done from " + sp.Schema + "." + tableName + ". Rows deleted = " + noRowsDeleted);
                }
                updated = true;
                if (!isMemDB && !isRedshift)
                    transaction.Commit();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in updating " + sp.Schema + "." + tableName + " to make ready for next operation ", ex);
                // rollback transaction
                if (!isMemDB && !isRedshift)
                    transaction.Rollback();
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }

                    if (!isRedshift && transaction != null)
                    {
                        transaction.Dispose();
                    }

                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Problem in closing resources", ex);
                }
            }

            return updated;
        }

        public static bool updateTxnStatusWithNextOperationOnLiveAccessConnection(Space sp, int loadNumber, string insertOperation, bool retryFailedLoad, LocalETLConfig leConfig)
        {
            if (sp == null)
            {
                Global.systemLog.Error("Session Space not available. Cannot update txnStatus with DeleteData");
                return false;
            }

            if (loadNumber < 0)
            {
                Global.systemLog.Error("Session Space not available. Cannot update txnStatus with DeleteData");
                return false;
            }

            bool updated = false;
            if (leConfig != null)
            {
                string liveAccessConnectionName = leConfig.getLocalETLConnection();
                DatabaseConnection liveAccessDc = null;
                bool newRepository;
                MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    if (dc.Name != null && dc.Name == liveAccessConnectionName)
                    {
                        liveAccessDc = dc;
                        break;
                    }
                }
                if (liveAccessDc == null)
                {
                    Global.systemLog.Warn("Unable to update " + tableName + " - Table does not exist on " + liveAccessConnectionName + " connection for space id " + sp.ID.ToString());
                    // If there is no table, then no need to continue;
                    return true;
                }
                string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, tableName);
                if (!liveAccessTableExists)
                {
                    Global.systemLog.Warn("Unable to update " + tableName + " - Table does not exist on " + liveAccessConnectionName + " connection for space id " + sp.ID.ToString());
                    // If there is no table, then no need to continue;
                    return true;
                }
                if (!retryFailedLoad)
                {
                    Global.systemLog.Info("Deleting previous for spaceId = " + sp.ID + " and iteration = " + loadNumber + " on liveaccessconnection:" + liveAccessConnectionName);
                    int noRowsDeleted = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, liveAccessConnectionName, "DELETE FROM " + liveAccessConnSchemaName + "." + tableName + " WHERE ITERATION = " + loadNumber);
                    if (noRowsDeleted == -1)
                    {
                        Global.systemLog.Warn("Could not delete status entries for loadnumber " + loadNumber + " on live access connection:" + liveAccessConnectionName);
                        return updated;
                    }
                    Global.systemLog.Info("Deletion done from " + sp.Schema + "." + tableName + ". Rows deleted = " + noRowsDeleted + " on liveaccessconnection:" + liveAccessConnectionName);
                    int noRowsInserted = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, liveAccessConnectionName, "INSERT INTO " + sp.Schema + "." + tableName +
                        "(TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" +
                        getCurrentTimeStamp(liveAccessDc.ConnectString) +
                        ",'ETL','" + insertOperation + "','" + leConfig.getLocalETLLoadGroup() + "'," + loadNumber + "," + RUNNING + ")");
                    if (noRowsInserted != 1)
                    {
                        Global.systemLog.Warn("Could not insert status entries for starting load for loadnumber " + loadNumber + " on live access connection:" + liveAccessConnectionName);
                        return updated;
                    }
                }
                else
                {
                    //Only delete failed entries and LoadMarker failed/complete record
                    Global.systemLog.Info("Deleting previous for spaceId = " + sp.ID + " and iteration = " + loadNumber + " on liveaccessconnection:" + liveAccessConnectionName);
                    string deleteSQL = "DELETE FROM " + liveAccessConnSchemaName + "." + tableName
                    + " WHERE ITERATION = loadNumber"
                    + " AND (STATUS = " + Status.FAILED + " OR STATUS = " + Status.SCRIPTFAILURE
                    + " OR (STATUS <> " + Status.RUNNING + " AND STEP = 'LoadMarker'))";
                    int noRowsDeleted = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, liveAccessConnectionName, deleteSQL);
                    if (noRowsDeleted == -1)
                    {
                        Global.systemLog.Warn("Could not delete status entries for loadnumber " + loadNumber + " on live access connection:" + liveAccessConnectionName);
                        return updated;
                    }
                    Global.systemLog.Info("Deletion done from " + sp.Schema + "." + tableName + ". Rows deleted = " + noRowsDeleted + " on liveaccessconnection:" + liveAccessConnectionName);
                }
                updated = true;
            }
            return updated;
        }

        public static int getStatusOfLoadMarkerStep(Space sp, int loadNumber, HttpSessionState session)
        {
            String spaceName = sp.Name;
            QueryConnection conn = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            // Assume to start with no status
            // For the load prior to 4.4, there will not be any entries for LoadMarker. 
            // In that case, the returned marker status will be None
            int statusLoadMarkerStep = NONE;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT TM,STEP,SUBSTEP,STATUS FROM " + sp.Schema + "." + tableName
                    + " WHERE STEP = 'LoadMarker' AND ITERATION=? ORDER BY TM DESC";
                cmd.Parameters.Add("@ITERATION", OdbcType.VarChar).Value = loadNumber;
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    statusLoadMarkerStep = result.GetInt32(3);
                    if (statusLoadMarkerStep == COMPLETE || statusLoadMarkerStep == FAILED)
                    {
                        break;
                    }
                }
                result.Close();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while determing the script group status for space " + spaceName, ex);
            }
            finally
            {
                if (conn != null)
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }

            return statusLoadMarkerStep;
        }

        public static int getScriptGroupStatus(Space sp, int loadNumber, HttpSessionState session)
        {

            int statusScriptCode = COMPLETE;
            String spaceName = null;
            if (checkForScriptGroup(session, sp))
            {
                spaceName = sp.Name;
                QueryConnection conn = null;
                QueryCommand cmd = null;
                QueryReader result = null;
                // Assume to start with its running
                statusScriptCode = RUNNING;
                try
                {
                    conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    cmd = conn.CreateCommand();
                    cmd.CommandText = "SELECT TM,STEP,SUBSTEP,STATUS FROM " + sp.Schema + "." + tableName
                        + " WHERE STEP = 'ExecuteScriptGroup' AND ITERATION=? ORDER BY TM";
                    cmd.Parameters.Add("@ITERATION", OdbcType.VarChar).Value = loadNumber;
                    result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        string substep = result.IsDBNull(2) ? null : result.GetString(2);
                        if (substep != null && substep.Length > 0 && substep.Contains(":"))
                        {
                            // Each script when executed inserts an entry of loadGroup:NewScript.
                            // We want to bypass all those entries and go to the overall execution result
                            continue;
                        }
                        int code = result.GetInt32(3);
                        statusScriptCode = code;
                        if (code == COMPLETE || code == FAILED)
                        {
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while determing the script group status for space " + spaceName, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        try
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Warn("Unable to release connection ", ex2);
                        }
                    }
                }
            }
            return statusScriptCode;
        }

        public static bool isProcessRunningOnExternalScheduler(Space sp)
        {
            bool runningOnExternalScheduler = false;
            try
            {
                Status.StatusResult statusResult = Status.getStatusUsingExternalScheduler(sp);
                if (statusResult != null)
                {
                    runningOnExternalScheduler = Status.isRunningCode(statusResult.code);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex, ex);
            }

            return runningOnExternalScheduler;
        }


        /// <summary>
        /// Check if the failed message stored in session status is the updated one or not.
        /// Since manual process does not go through scheduler, a failed scheduled load could succeed as a 
        /// successful manual one but UI will still show the wrong failed message. Compare the timestamp 
        /// to disregard the failed message
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="externalSchedulerStatusResult"></param>
        public static bool isFailedStatusObsolete(Space sp, string loadGroup, Status.StatusResult externalSchedulerStatusResult)
        {
            if (externalSchedulerStatusResult != null && externalSchedulerStatusResult.code == Status.StatusCode.Failed)
            {
                DateTime lastOperationTimeFromScheduler = externalSchedulerStatusResult.dateTime;
                DateTime lastOperationTimeFromDb = Status.getLatestOperationTimeFromTxnCommandHistory(sp, loadGroup);
                if (lastOperationTimeFromDb != DateTime.MinValue && lastOperationTimeFromScheduler != DateTime.MinValue
                    && lastOperationTimeFromScheduler.CompareTo(lastOperationTimeFromDb) < 0)
                {
                    return true;
                }

                if (loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                {
                    // get the last modified time stamp of sforce.xml and compare it
                    // This takes care of failed scheduled extraction/process followed by a successful manual extract only
                    FileInfo fi = new FileInfo(Path.Combine(sp.Directory, "sforce.xml"));
                    if (fi.Exists)
                    {
                        SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(Path.Combine(sp.Directory, "sforce.xml"));
                        if (settings != null)
                        {
                            DateTime updatedExtractionTime = settings.startExtractionTime;
                            if (updatedExtractionTime != DateTime.MinValue && lastOperationTimeFromScheduler != DateTime.MinValue
                                && lastOperationTimeFromScheduler.CompareTo(updatedExtractionTime) < 0)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public static DateTime getLatestOperationTimeFromTxnCommandHistory(Space sp, string loadGroup)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            DateTime response = DateTime.MinValue;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT MAX(TM) FROM " + sp.Schema + "." + tableName + " WHERE SUBSTEP=?";
                cmd.Parameters.Add("SUBSTEP", OdbcType.VarChar).Value = loadGroup;
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    response = (DateTime)result;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting latest operation time from txn_command_history for space " + sp.ID, ex);
            }
            finally
            {   
                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception) { }
                }
                ConnectionPool.releaseConnection(conn);
            }

            return response;
        }

        public static Status.StatusResult getStatusUsingExternalScheduler(Space sp)
        {
            return getStatusUsingExternalScheduler(sp, -1, null, false);
        }
        
        public static Status.StatusResult getStatusUsingExternalScheduler(Space sp, int loadNumber, string loadGroup, bool fetchProcessingDetailsFromTxn)
        {
            try
            {
                StatusResult sr = new StatusResult(StatusCode.None);
                // First get the status from scheduler
                SpaceStatus spaceStatus = SchedulerUtils.getCurrentStatus(sp, loadNumber, loadGroup);
                // spaceStatus tells the most recent step executed for the space
                // if the space is available, check the last recent status for any failure. If it is failed, we need to show the failed message
                string lastStepName = spaceStatus.stepName;
                int lastStepStatus = spaceStatus.stepStatus;
                int lastLoadId = spaceStatus.loadId;
                string lastLoadGroup = spaceStatus.loadGroup;
                string decodedSpaceStatusMsg = decodeSpaceStatusMessage(spaceStatus.message);
                string createdDateString = spaceStatus.createdDate;
                string modifiedDateString = spaceStatus.modifiedDate;
                string chosenDateString = modifiedDateString != null && modifiedDateString.Length > 0 ? modifiedDateString : createdDateString;
                if (chosenDateString != null && chosenDateString.Trim().Length > 0)
                {
                    DateTime dTime = DateTime.MinValue;
                    if (DateTime.TryParse(chosenDateString.Trim(), out dTime))
                    {
                        sr.dateTime = dTime;
                    }
                }

                if (spaceStatus.available)
                {
                    if (lastStepName == "PostProcess" && lastLoadGroup == loadGroup)
                    {
                        sr = new StatusResult(StatusCode.Ready);
                        // For the last load get the status, if it is complete or failed
                        StatusResult updatedStatusResult = getLoadStatus(null, sp, lastLoadId, lastLoadGroup, true, sr);
                        if (updatedStatusResult != null && (updatedStatusResult.code == StatusCode.Failed) || (updatedStatusResult.code == StatusCode.Complete))
                        {
                            // We already know that the this load id is finished, the call to getLoadStatus is to get any detailed failed 
                            // entries if at all
                            sr.code = updatedStatusResult.code;
                        }
                    }
                    else if (lastStepName == "BirstLocal")
                    {
                        StatusResult updatedResult = Status.retreiveAndUpdateBirstLocalGroupStatus(new StatusResult(StatusCode.None), sp, lastLoadId);
                        if (updatedResult != null)
                        {
                            sr.code = updatedResult.code;
                        }
                    }
                    else if (lastStepStatus == FAILED)
                    {
                        // if the last step is failed for send email, do not mark the whole process as failed.
                            // Instead spit out a log entry for now and return a ready status
                            if (lastStepName == "SendEmail")
                            {
                                Global.systemLog.Error("Last step send email failed for space " + sp.ID);
                                sr.code = StatusCode.Ready;
                            }
                            else if (lastLoadGroup == loadGroup
                                    || lastLoadGroup == null && loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                            {

                                sr.code = StatusCode.Failed;
                                if (lastStepName == "ScheduledExtract" || lastStepName == "SFDCEngineCmd" || lastStepName == "SFDCExtract" || lastStepName == "SFDCPostExtract")
                                {
                                    decodedSpaceStatusMsg = "Error during Last Salesforce Extraction";
                                    sr.type = StatusResult.TYPE_EXTRACT;
                                }
                                sr.message = decodedSpaceStatusMsg;
                            }
                    }
                    else
                    {
                        sr.code = StatusCode.Ready;
                    }
                }
                else
                {   
                    // if it is not available, look for the current running step
                    // if it is delete data, simply return DeleteData status
                    if (lastStepName == "DeleteData" || lastStepName == "ScheduledDelete" || lastStepName == "DeleteLastPre"
                        || lastStepName == "DeleteLast" || lastStepName == "DeleteLastPost" || lastStepName == "DeleteAll")
                    {
                        sr.code = StatusCode.DeleteData;
                    }
                    else if (lastStepName == "Process" || lastStepName == "AppLoader" || lastStepName == "ScheduledProcess" ||
                        lastStepName.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) || lastStepName == "PostProcess")
                    {
                        // if it is one of the processing steps, check if load group match. E.g. User on sforce UI but user is on Acorn UI
                        if (lastLoadGroup != loadGroup)
                        {
                            sr.code = StatusCode.LoadingAnotherLoadGroup;
                        }
                        else
                        {
                            if (lastStepName.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                            {
                               // Just show LoadingAnother group
                                sr.code = StatusCode.LoadingAnotherLoadGroup;
                            }
                            else if (lastStepName == "Process")
                            {
                                // get the substeps
                                if (fetchProcessingDetailsFromTxn)
                                {
                                    sr = getLoadStatus(null, sp, lastLoadId, lastLoadGroup, true, new StatusResult(StatusCode.Started));
                                    if (sr.code == StatusCode.Complete || sr.code == StatusCode.Failed)
                                    {
                                        // the status is never to be shown complete here. Complete needs to come from Scheduler since
                                        // txn_command_history might return LoadWarehouse but post processing needs to be done by the scheduler
                                        sr.code = StatusCode.LoadWarehouse;
                                    }
                                }
                                else
                                {
                                    sr.code = StatusCode.LoadStaging;
                                }
                            }
                            else if (lastStepName == "AppLoader" || lastStepName == "ScheduledProcess")
                            {
                                // signals the very start of the processing
                                sr.code = StatusCode.Started;
                            }
                            else if (lastStepName == "PostProcess")
                            {
                                sr.code = StatusCode.PostProcess;
                            }
                        }
                    }
                    else if (isExtractStep(lastStepName))
                    {
                        sr.code = StatusCode.SFDCExtract;
                        sr.message = decodedSpaceStatusMsg;
                    }
                }

                return sr;

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving status using scheduler for space " + sp != null ? sp.ID.ToString() : null, ex);
                throw new Exceptions.SchedulerException("Exception while retreiving status for space " + sp != null ? sp.ID.ToString() : null);
            }
        }

        public static bool isExtractStep(string lastStepName)
        {
            return (lastStepName != null
                && (lastStepName == "SFDCExtract" || lastStepName == "ScheduledExtract" 
                || lastStepName == "SFDCPostExtract" || lastStepName == "SFDCEngineCmd"));
        }

        public static string decodeSpaceStatusMessage(string spaceStatusMessage)
        {
            // To be implemented
            // message contains various failure messages, e.g. for sfdc extract the object failed or the reason for sfdc failure
            // for appLoader could be loadauthorization or data limit messages

            return null;
        }

        public static bool checkForScriptGroup(HttpSessionState session, Space sp)
        {
            bool checkStatusForScriptGroup = false;
            QueryConnection conn = null;
            try
            {
                if (session != null)
                {
                    MainAdminForm maf = Util.getSessionMAF(session);
                    if (maf != null && maf.scriptGroupMod != null)
                    {
                        List<ScriptGroup> scgplist = maf.scriptGroupMod.getScriptGroupList();
                        if (scgplist != null && scgplist.Count > 0)
                        {
                            foreach (ScriptGroup scriptGroup in scgplist)
                            {
                                if (scriptGroup.Scripts != null && scriptGroup.Scripts.Length > 0)
                                {
                                    checkStatusForScriptGroup = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (checkStatusForScriptGroup)
                {
                    // make sure the txn_command_history table exists
                    conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    bool isMysql = sp.ConnectString.Contains("jdbc:mysql");
                    if (!Database.tableExistsUsingSelect(conn, sp.Schema, tableName))
                    {
                        checkStatusForScriptGroup = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in checking script group " + sp.Name, ex);
            }
            finally
            {
                if (conn != null)
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return checkStatusForScriptGroup;
        }

        public static Dictionary<int, string> getProcessingGroups(Space sp)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            string spacename = null;
            Dictionary<int, string> loadNumberToPGroupMapping = null;
            string query = "SELECT DISTINCT ITERATION, PROCESSINGGROUP FROM " + sp.Schema + "." + tableName
                    + " ORDER BY ITERATION, PROCESSINGGROUP";
            try
            {
                spacename = sp.Name;
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                cmd = conn.CreateCommand();
                cmd.CommandText = query;
                try
                {
                    result = cmd.ExecuteReader();
                }
                catch (OdbcException ex)
                {
                    if (isProcessingGroupColumnNeededInTxnCommandHistory(ex))
                    {
                        Database.addProcessingGroupToTxnCommandHistory(sp);
                        // retry again
                        cmd.Dispose();
                        cmd = conn.CreateCommand();
                        cmd.CommandText = query;
                        result = cmd.ExecuteReader();
                    }
                    else
                    {
                        Global.systemLog.Error("Exception while getting processing groups for space " + spacename, ex);
                    }
                }
                if (result != null)
                {
                    loadNumberToPGroupMapping = new Dictionary<int, string>();
                    while (result.Read())
                    {
                        int loadnumber;
                        if (result.IsDBNull(0))
                            continue;
                        int.TryParse(result.GetString(0), out loadnumber);
                        string pg = result.IsDBNull(1) ? EMPTY_PROCESSING_GROUP : result.GetString(1);
                        if (loadNumberToPGroupMapping.ContainsKey(loadnumber))
                        {
                            string pGroups = loadNumberToPGroupMapping[loadnumber] + "," + pg;
                            loadNumberToPGroupMapping[loadnumber] = pGroups;
                        }
                        else
                        {
                            loadNumberToPGroupMapping.Add(loadnumber, pg);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception while getting processing groups for space " + spacename, e);
            }
            finally
            {
                if (conn != null)
                {
                    try
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to release connection ", ex2);
                    }
                }
            }
            return loadNumberToPGroupMapping;
        }

        public static Status.StatusStep checkTimeDimensionStatus(SpaceConfig sc)
        {
            string jdbcurl = sc.DatabaseConnectString;
            string dbuname = sc.AdminUser;
            string dbpwd = sc.AdminPwd;
            string dbType = sc.DatabaseType;
            string connectString = Space.getFullConnectString(jdbcurl, dbuname, dbpwd, dbType);
            Status.StatusStep endStep = null;
            QueryConnection timeConn = null;
            try
            {
                timeConn = Util.getConnectionWithVariedRetries(connectString, dbType);
                string statusTableSchema = mainSchema;
                bool timeStatusTable = Database.tableExists(timeConn, mainSchema, Status.tableName);
                if (!timeStatusTable)
                {
                    timeStatusTable = Database.tableExists(timeConn, "public", Status.tableName);
                    if (timeStatusTable)
                    {
                        statusTableSchema = "public";
                    }
                }

                if (timeStatusTable)
                {
                    TrustedService.TrustedService ts = Util.getSMIWebTrustedService(sc.LocalURL);
                    ts.Timeout = 5 * 60000;
                    string query = getQueryToRetrieveTimeDimStatus(statusTableSchema);
                    Acorn.TrustedService.QueryResult tqr = ts.getJdbcQueryResults(sc.DatabaseDriver, jdbcurl, dbuname, dbpwd, query);
                    QueryReader result = new WebServiceQueryReader(tqr, timeConn.getDBType());
                    List<Status.StatusStep> steps = new List<Status.StatusStep>();
                    bool hasRows = false;
                    while (result.Read())
                    {
                        hasRows = true;
                        DateTime dt = result.IsDBNull(0) ? DateTime.Now : result.GetDateTime(0);
                        string step = result.GetString(1);
                        string substep = result.IsDBNull(2) ? null : result.GetString(2);
                        int code = result.GetInt32(3);
                        long numRows = result.IsDBNull(4) ? -1 : result.GetInt64(4);
                        long numErrors = result.IsDBNull(5) ? -1 : result.GetInt64(5);
                        long numWarnings = result.IsDBNull(6) ? -1 : result.GetInt64(6);
                        long duration = result.IsDBNull(7) ? -1 : result.GetInt64(7);
                        string message = result.IsDBNull(8) ? null : result.GetString(8);
                        if (step == "CreateTimeSchema")
                        {
                            Status.StatusStep sstep = new Status.StatusStep();
                            sstep.step = Status.StatusCode.TimeCreation;
                            sstep.substep = substep;
                            sstep.result = code;
                            sstep.message = message;
                            sstep.duration = duration;
                            sstep.numErrors = numErrors;
                            sstep.numWarnings = numWarnings;
                            sstep.numRows = numRows;
                            steps.Add(sstep);
                        }
                    }

                    if (steps.Count > 0)
                    {
                        int resultCode = -1;
                        foreach (Status.StatusStep sstep in steps)
                        {
                            if (sstep.result > resultCode)
                            {
                                resultCode = sstep.result;
                                endStep = sstep;
                            }
                        }
                    }

                    if (!hasRows)
                    {
                        // no rows but status table exist, it is most likely the pre-status-population use case  
                        endStep = new Status.StatusStep();
                        endStep.result = Status.COMPLETE;
                    }
                }
                else
                {
                    endStep = new Status.StatusStep();
                    endStep.result = Status.NONE;                    
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(timeConn);
            }
            return endStep;
        }

        private static string getQueryToRetrieveTimeDimStatus(string statusSchema)
        {
            // for redsfhit statusSchedule could be public or dbo.// dbo when time dimensions are recreated
            string query = "SELECT TM,STEP,SUBSTEP,STATUS,NUMROWS,NUMERRORS,NUMWARNINGS,DURATION,MESSAGE FROM "
                + statusSchema + "." + Status.tableName
                + " WHERE ITERATION=1 ORDER BY TM";
            return query;
        }
    }
}