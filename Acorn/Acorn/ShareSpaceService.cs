﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Net.Mail;
using System.IO;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class ShareSpaceService
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public static ShareSpaceResult getSpaceShareDetails(HttpSessionState session)
        {
            ShareSpaceResult response = new ShareSpaceResult();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User u = (User)session["user"]; 
            Space sp = (Space)Util.getSessionSpace(session);

            if(u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Please login again";
                return response;
            }

            if(sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get Space Details";
                return response;

            }
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<Invitation> ilist = Database.getInvitations(conn, mainSchema, sp);

                List<Invite> invites = new List<Invite>();
                foreach (Invitation i in ilist)
                {
                    Invite invite = new Invite();
                    invite.Email = i.Email;
                    invite.InviteDate = i.InviteDate;
                    invite.SpaceName = i.SpaceName;

                    invites.Add(invite);
                }

                List<SpaceMembership> mlist = Database.getUsers(conn, mainSchema, sp, false);
                // Forming Access List for this space
                List<MemberDetails> spaceAccessList = new List<MemberDetails>();


                foreach (SpaceMembership sm in mlist)
                {
                    if (sm.User == null)
                        continue;
                    if (sm.User.ID != u.ID)
                    {
                        // if the user is an owner, no need to show it in the share space list
                        // for Admin users. This could result in potential accidental deletion
                        // of owner from the space membership. Everything will be lost then.             
                        if (sm.Owner)
                        {
                            continue;
                        }
                        MemberDetails accessUser = new MemberDetails();
                        accessUser.Username = sm.User.Username;
                        accessUser.Admin = sm.Administrator;
                        spaceAccessList.Add(accessUser);

                    }
                }

                response.InvitedMembers = invites.ToArray();
                response.SpaceAccessMembers = spaceAccessList.ToArray();
                return response;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static GenericResponse sendInvite(HttpSessionState session, HttpRequest request, HttpServerUtility server, string emailId, string emailText)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Please logout and try again.";
                return response;
            }

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Internal Error: Unable to get Space Details. Please logout and try again.";
                return response;

            }

            if (emailId == null || !emailId.Contains('@'))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Invalid email address format.";
                return response;
            }
            
            if (emailId == u.Email)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Cannot invite to yourself.";
                return response;
            }

            MainAdminForm maf = (MainAdminForm)Util.getSessionMAF(session);
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Internal Error. Could not find MAF. Please logout and try again.";
                return response;
            }
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<Invitation> curList = Database.getInvitations(conn, mainSchema, sp);
                foreach (Invitation inv in curList)
                {
                    if (inv.Email == emailId)
                    {
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        error.ErrorMessage = "Invitation to that email is already outstanding.";
                        return response;
                    }
                }

                Database.addInvitation(conn, mainSchema, sp, u, emailId);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            MailMessage mm = new MailMessage();
            mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
            mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
            mm.To.Add(new MailAddress(emailId));
            mm.IsBodyHtml = true;
            mm.BodyEncoding = System.Text.Encoding.UTF8;
            mm.Headers.Add("Content-Type", "content=text/html; charset=\"UTF-8\"");
            StreamReader reader = new StreamReader(server.MapPath("~/") + "InviteEmail.htm");
            mm.Body = reader.ReadToEnd();
            reader.Close();
            mm.Subject = "You're Invited to Birst";
            mm.Body = mm.Body.Replace("${NAME}", u.Username);
            mm.Body = mm.Body.Replace("${EMAIL}", emailId);
            mm.Body = mm.Body.Replace("${MESSAGE}", emailText);
            mm.Body = mm.Body.Replace("${LINK}", Util.getMasterURL(session, request));
            SmtpClient sc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                sc.EnableSsl = true;
            sc.Send(mm);
           
            using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
            {
                Global.userEventLog.Info("INVITE");
            }
            // Add user to repository
            
            Util.addUserToSpace(maf, sp, emailId, Util.USER_GROUP_NAME);

            return response;
        }

        public static GenericResponse deleteInvite(HttpSessionState session, string emailId)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get Space Details";
                return response;

            }

            MainAdminForm maf = (MainAdminForm)Util.getSessionMAF(session);
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to proceed.";
                return response;
            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.removeInvitation(conn, mainSchema, sp.ID, emailId);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            removeFromRepository(maf, sp, session, emailId);
            // update the UsersAndGroups.xml file            
            UserAndGroupUtils.saveUserAndGroupToFile(sp);
            return response;
        }

        public static GenericResponse removeAccess(HttpSessionState session, string userName)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User loggedInUser = (User)session["User"];
            if (loggedInUser == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Please login again";
                return response;
            }

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get Space Details";
                return response;

            }

            MainAdminForm maf = (MainAdminForm)Util.getSessionMAF(session);
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to proceed.";
                return response;
            }
            
            
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, mainSchema, userName);
                Database.removeSpaceMembership(conn, mainSchema, u.ID, sp.ID);
                removeFromRepository(maf, sp, session, userName);
                u = (User)session["User"];
                u.NumShares = u.NumShares > 0 ? u.NumShares - 1 : 0;
                Database.updateUser(conn, mainSchema, u);
                // update the UsersAndGroups.xml file            
                UserAndGroupUtils.saveUserAndGroupToFile(sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        public static GenericResponse updateAdminAccess(HttpSessionState session, string[][] accessMembers)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User loggedInUser = (User)session["User"];
            if (loggedInUser == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Please login again";
                return response;
            }

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get Space Details";
                return response;

            }


            MainAdminForm maf = (MainAdminForm)Util.getSessionMAF(session);
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to proceed.";
                return response;
            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();

                foreach (string[] accessUser in accessMembers)
                {
                    string username = accessUser[0];
                    bool adminAccess = bool.Parse(accessUser[1]);

                    User u = Database.getUser(conn, mainSchema, username);
                    Database.removeSpaceMembership(conn, mainSchema, u.ID, sp.ID);
                    Database.addSpaceMembership(conn, mainSchema, u.ID, sp.ID, adminAccess, false);

                    // The logic here is that if a user is made admin on Acorn side, he is added to the $USER group so that 
                    // he get all the ACLs for USER$. However, if he is not an admin, then remove it from the 
                    // USER$ group (make sure he is part of any other group). 
                    // USER$ information will be used on SMIWeb side

                    // Retreive the fresh group/user/acl membership from database.
                    List<SpaceGroup> spaceGroupList = Database.getSpaceGroups(conn, mainSchema, sp);
                    List<SpaceGroup> userSpaceGroups = UserAndGroupUtils.getUserGroups(u, spaceGroupList);
                    SpaceGroup userGroupInteral = UserAndGroupUtils.getGroupInfoByName(sp, Util.USER_GROUP_NAME, true);

                    if (userGroupInteral == null || (userGroupInteral != null && !userGroupInteral.InternalGroup))
                    {
                        Global.systemLog.Error("Error while retreiving " + Util.USER_GROUP_NAME + " for updating admin access");
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        error.ErrorMessage = "Error while updating sharing space details";
                        return response;
                    }

                    bool isMemberOfUserGroupInternal = UserAndGroupUtils.isUserGroupMember(userGroupInteral, u.ID);
                    if (adminAccess)
                    {
                        if (!isMemberOfUserGroupInternal)
                        {
                            Database.addUserToGroup(conn, mainSchema, sp, userGroupInteral.ID, u.ID, true);
                        }
                    }                    
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }           
            
            return response;
        }

        
        private static void removeFromRepository(MainAdminForm maf, Space sp, HttpSessionState session, string userName)
        {
           
            Util.removeUserFromSpace(sp, userName);
        }

    }
}
