﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.IO;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class GroupManagementService
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private static string NEW_GROUP_NAME = "Group";

        public static DynamicGroupsResult getDynamicGroups(HttpSessionState session, string selectedGroup)
        {
            DynamicGroupsResult response = new DynamicGroupsResult();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }

            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;               
                return response;
            }

            List<string> hierarchyList = new List<string>();
            List<string> sourceColumns = new List<string>();                        
            
            hierarchyList.Add("Not Specified");
            sourceColumns.Add("Not Specified");
            

            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                hierarchyList.Add(h.DimensionName);
            }

            response.selectedGroupSource = hierarchyList[0];            
            response.Hierarchy = hierarchyList.ToArray();
            response.sourceColumns = sourceColumns.ToArray();

            if (selectedGroup == null || selectedGroup.Length == 0 )
            {
                // init population.
                if (maf.DynamicGroups != null)
                {
                    response.selectedGroupSource = maf.DynamicGroups[0][0];
                    setDHierarchy(maf, response, response.selectedGroupSource);
                }
            }
            else
            {
                response.selectedGroupSource = selectedGroup;
                setDHierarchy(maf, response, selectedGroup);

            }
            return response;
        }

        public static GenericResponse getMAFGroups(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }

            /*
            
            */
            response.other = getGroupsList(sp).ToArray();

            return response;

        }

        private static List<string> getGroupsList(Space sp)
        {

            List<string> mafGroups = new List<string>();
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            if (sp.SpaceGroups != null)
            {
                foreach (SpaceGroup spg in sp.SpaceGroups)
                {
                    if (spg.InternalGroup && spg.Name == "Administrators")
                        continue;
                    if (spg.Name == Util.OWNER_GROUP_NAME || spg.Name == Util.USER_GROUP_NAME)
                        continue;
                    mafGroups.Add(spg.Name);
                }
            }             

            return mafGroups;
        }

        public static GroupDetails getGroupDetails(HttpSessionState session, string groupName)
        {
            GroupDetails response = new GroupDetails();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
           
            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Please Login again.";
                return response;
            }
            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            /*
            
            */
            List<string> mafGroups = getGroupsList(sp);
            response.MAFGroups = mafGroups.ToArray();
            if (mafGroups == null || mafGroups.Count == 0)
            {
                return response;
            }
            if (groupName == null || groupName.Length == 0)
            {
                groupName = mafGroups[0];
            }

            response.GroupName = groupName;
                        
            // no storing smlist in session, since a user could navigate between share space and manage group
            // and change space membership which update database and not session variable.
            List<SpaceMembership> smlist = (List<SpaceMembership>)session["smlist"];            
            QueryConnection conn = null;
            string mainSchema = Util.getMainSchema();
            try
            {
                if (smlist == null)
                {
                    conn = ConnectionPool.getConnection();
                    smlist = Database.getUserSpaceMemberships(conn, mainSchema, u, sp.ID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving userspace memberships for space " + sp.ID, ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            SpaceGroup currentGroup = UserAndGroupUtils.getGroupInfoByName(sp, groupName);
            if (currentGroup == null)
            {
                Global.systemLog.Warn("Unable to retrieve group information for " + groupName);
                return response;
            }
                        
            response.AdhocAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_ADHOC);
            response.DashboardAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_EDIT_DASHBOARD);
            response.EnableDownloadAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_ENABLE_DOWNLOAD);
            response.EnableSelfScheduleAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED);
            response.ModifySavedExpressionAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_MODIFY_SAVED_EXPRESSIONS_ALLOWED);
            response.EditReportCatalogAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_EDIT_REPORT_CATALOG);
            response.VisualizerAccess = UserAndGroupUtils.isGroupACLDefined(currentGroup, ACLDetails.ACL_VISUALIZER_REGULAR);

            List<MemberDetails> groupMembersList = new List<MemberDetails>();            
            foreach (SpaceMembership sm in smlist)
            {
                // if the logged in user is owner, do not show him as part of potential members list
                // if the logged in user is admin but not an owner, show him as part of potential members list
                if (sm.User == null)
                    continue;
                if (sm.User.ID == u.ID && sm.Owner)
                    continue;
                MemberDetails dr = new MemberDetails();
                dr.Username = sm.User.Username;
                dr.Admin = false;
                dr.Member = false;

                List<SpaceGroup> spGroups = sp.SpaceGroups;
                if (spGroups != null && spGroups.Count > 0)
                {
                    foreach (SpaceGroup spg in spGroups)
                    {
                        if (spg.InternalGroup && spg.Name == "Administrators")
                            continue;
                        if (spg.Name == Util.OWNER_GROUP_NAME || spg.Name == Util.USER_GROUP_NAME)
                            continue;
                        if (!spg.InternalGroup && spg.Name == groupName && spg.GroupUsers != null)
                        {
                            if(UserAndGroupUtils.isUserGroupMember(spg, sm.User.Username))                            
                            {
                                dr.Member = true;
                                break;
                            }
                            
                        }
                    }
                }
                
                groupMembersList.Add(dr);                
            }

            response.Members = groupMembersList.ToArray();
            return response;
        }

        public static GenericResponse assignDynamicGroups(HttpSessionState session, string groupHierarchyName, string userColumnName, 
            string groupColumnName)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;               
                return response;
            }
			maf.setRepositoryDirectory(sp.Directory);

            Variable v = null;           
            foreach (Variable sv in maf.getVariables())
            {
                // Backward compatability - update the reserved name GROUPS to new Birst$Groups
                if (sv.Name == "GROUPS" && sv.BirstCanModify == false)
                {
                    sv.Name = "Birst$Groups";
                }

                if (sv.Name == "Birst$Groups")
                {                    
                    v = sv;                                        
                    break;
                }
            }

            if (groupHierarchyName == null || groupHierarchyName.Length == 0 || groupHierarchyName == "Not Specified")
            {
                // Remove the GROUPS variable
                if (v != null)
                {
                    maf.getVariables().Remove(v);
                    maf.DynamicGroups = null;
                    session["AdminDirty"] = true;
                    Util.saveDirty(session);
                }
                return response;
            }

            if (maf.DynamicGroups == null)
            {
                maf.DynamicGroups = new string[2][];
            }
            maf.DynamicGroups[0] = new string[] {  groupHierarchyName, userColumnName };
            maf.DynamicGroups[1] = new string[] { groupHierarchyName, groupColumnName };
            // Add the GROUPS variable
            if (v == null)
            {
                v = new Variable();
                // Changing the old reserved word GROUPS to Birst$GROUPS
                v.Name = "Birst$Groups";
                v.MultipleColumns = false;
                v.MultiValue = true;
                v.Connection = Database.DEFAULT_CONNECTION;
                v.Type = Variable.VariableType.Session;
                v.Session = true;
                v.LogicalQuery = true;
                // add or update and save
                maf.updateVariable(v, "Admin");
            }

            string query = null;
            if (Util.getQueryLanguageVersion(maf) > 0)
            {
                // new query language
                query = "SELECT [" + maf.DynamicGroups[1][0] + "." + maf.DynamicGroups[1][1] + "] FROM [ALL] WHERE [" + maf.DynamicGroups[0][0] + "." + maf.DynamicGroups[0][1] + "]=GetVariable('USER')";
            }
            else
            {
                // classic query language
                query = "DC{" + maf.DynamicGroups[1][0] + "." + maf.DynamicGroups[1][1] + "},FDC{" + maf.DynamicGroups[0][0] + "." + maf.DynamicGroups[0][1] + "=V{USER}}";
            }
            if (query != v.Query)
            {
                v.Query = query;
                maf.updateVariable(v, "Admin");
                session["AdminDirty"] = true;
                Util.saveDirty(session);
            }
            completeGroupManagement(session);
            return response;
        }
        

        public static GenericResponse addNewGroup(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            List<SpaceGroup> spgList = sp.SpaceGroups;

            //Performance_Optimizer_Administration.Group newg = new Performance_Optimizer_Administration.Group();
            int count = 0;
            string newgName = NEW_GROUP_NAME + " " + (count++);

            if (spgList != null && spgList.Count > 0)
            {
                // find a unique name                
                bool found = false;
                do
                {
                    found = UserAndGroupUtils.isGroupNameDuplicate(sp, null, newgName);                    
                    if (found)
                        newgName = NEW_GROUP_NAME + " " + (count++);
                } while (found);

            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Guid newGroupID = Database.addGroupToSpace(conn, Util.getMainSchema(), sp, newgName, false);
                if (newGroupID != null && newGroupID != Guid.Empty)
                {
                    UserAndGroupUtils.populateSpaceGroups(sp, true);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding group " + newgName + " to space");
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }            
            response.other = new string[]{newgName};
            completeGroupManagement(session);
            return response;
        }

        public static GenericResponse deleteGroup(HttpSessionState session, string name)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            
            SpaceGroup currentGroup = UserAndGroupUtils.getGroupInfoByName(sp, name);

            if (currentGroup == null)
            {
                Global.systemLog.Error("Unable to find group " + name + " for deletion " + " for space " + sp.ID);
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Error while deleting group";
                return response;
            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                Database.removeGroupFromSpace(conn, schema, sp, currentGroup);
                UserAndGroupUtils.populateSpaceGroups(sp, true);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while deleting group " + currentGroup + " to space");
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            completeGroupManagement(session);
            return response;

        }       

        public static GenericResponse saveGroup(HttpSessionState session, string name, string newName, 
            string[] usernames, bool[] isMember, bool adhocAccess, bool dashboardAccess, bool enableDownloadAccess,
            bool enableSelfScheduleAccess, bool enableModifySavedExpressions, bool enableEditReportCatalog, bool enableVisualizerAccess)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }

            
            if (usernames.Length != isMember.Length)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to save changes. Please try again.";
                Global.systemLog.Error("Expecting equal number of usernames and isMember flags");
                return response;
            }

            SpaceGroup currentGroup = UserAndGroupUtils.getGroupInfoByName(sp, name);
            if (currentGroup == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to save changes. Please try again.";
                Global.systemLog.Error("Unable to find group information by name for " + name);
                return response;
            }

            QueryConnection conn = null;
            string mainSchema = Util.getMainSchema();

            try
            {
                conn = ConnectionPool.getConnection();
                if (name != newName)
                {
                    // changing the name of the group. See if there is any group present with that name
                    bool dupe = UserAndGroupUtils.isGroupNameDuplicate(sp, currentGroup, newName);
                    if (dupe)
                    {
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        error.ErrorMessage = "Unable to save changes. Group Name already used";
                        return response;
                    }

                    currentGroup.Name = newName;
                    // update group info
                    Database.updateSpaceGroupInfo(conn, mainSchema, sp, currentGroup);
                }


                List<string> memberUserNameList = new List<string>(usernames);
                List<bool> isMemberList = new List<bool>(isMember);

                List<string> ulist = new List<string>();
                for (int i = 0; i < memberUserNameList.Count; i++)
                {
                    if (isMemberList[i])
                    {
                        ulist.Add(memberUserNameList[i]);
                    }
                }

                Database.updateUsersForSpaceGroup(conn, mainSchema, sp, currentGroup, ulist);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_ADHOC, adhocAccess);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_DASHBOARD, dashboardAccess);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_EDIT_DASHBOARD, dashboardAccess);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_ENABLE_DOWNLOAD, enableDownloadAccess);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED, enableSelfScheduleAccess);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_MODIFY_SAVED_EXPRESSIONS_ALLOWED, enableModifySavedExpressions);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_EDIT_REPORT_CATALOG, enableEditReportCatalog);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, currentGroup, ACLDetails.ACL_VISUALIZER_REGULAR, enableVisualizerAccess);
            

            // Removed adhoc from normal users access if not already done
            SpaceGroup internalUserGroup = UserAndGroupUtils.getGroupInfoByName(sp, Util.USER_GROUP_NAME, true);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_ADHOC, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_DASHBOARD, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_EDIT_DASHBOARD, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_ENABLE_DOWNLOAD, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_SELF_SCHEDULE_ALLOWED, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_MODIFY_SAVED_EXPRESSIONS_ALLOWED, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_EDIT_REPORT_CATALOG, false);
            UserAndGroupUtils.addOrRemoveACLForGroup(sp, internalUserGroup, ACLDetails.ACL_VISUALIZER_REGULAR, false);

            
            // update the data structure from the database
            UserAndGroupUtils.populateSpaceGroups(sp, true);
            
            completeGroupManagement(session);
            return response;
        }

        public static GenericResponse completeGroupManagement(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)Util.getSessionSpace(session);
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details.";
                return response;
            }
            /*
            
            
             */
            session.Remove("smlist");
            
            UserAndGroupUtils.saveUserAndGroupToFile(sp);
            return response;

        }          

        
        private static void setDHierarchy(MainAdminForm maf, DynamicGroupsResult response, string selectedGroup)
        {
            if (selectedGroup == null || selectedGroup == "Not Specified")
            {                         
                return;
            }

            List<string> sourceColumnsList = new List<string>();            

            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                if (h.DimensionName == selectedGroup)
                {
                    List<string> clist = maf.getDimensionColumnList(h.DimensionName);
                    sourceColumnsList.Add("Not Specified");
                    foreach (string s in clist)
                    {
                        sourceColumnsList.Add(s);
                    }

                    if (maf.DynamicGroups != null && maf.DynamicGroups[0] != null && maf.DynamicGroups[0][0] != null &&
                        maf.DynamicGroups[0][0] == selectedGroup && maf.DynamicGroups[0][0] == maf.DynamicGroups[1][0])
                    {
                        response.selectedUserColumn = maf.DynamicGroups[0][1];
                        response.selectedGroupColumn = maf.DynamicGroups[1][1];
                    }
                    break;
                }
            }

            response.sourceColumns = sourceColumnsList.ToArray();
        }

        public static Variable getDynamicGroupsQueryVariable(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                var spaceName = "";
                Space sp = (Space)Util.getSessionSpace(session);
                if (sp == null)
                {
                    Global.systemLog.Error("Unable to get space details.");
                }
                else
                {
                    spaceName = sp.Name;
                }
                Global.systemLog.Error("Could not find session MAF object for space: " + spaceName);
                return null;
            }

            Variable v = null;
            foreach (Variable sv in maf.getVariables())
            {
                // Backward compatability - update the reserved name GROUPS to new Birst$Groups
                if (sv.Name == "GROUPS" && sv.BirstCanModify == false)
                {
                    sv.Name = "Birst$Groups";
                }

                if (sv.Name == "Birst$Groups")
                {
                    v = sv;
                    break;
                }
            }
            return v;
        }
        
        
        public static bool isACLIncluded(SpaceGroup group, string ACLTag)
        {            
            QueryConnection conn = null;
            List<int> aclIds = null;
            try
            {
                conn = ConnectionPool.getConnection();
                aclIds = Database.getGroupACLs(conn, mainSchema, group.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving ACLS for group " + group.ID, ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return UserAndGroupUtils.isGroupACLDefined(group, ACLTag);
        }
    }
}
