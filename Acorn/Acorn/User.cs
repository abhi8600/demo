using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

namespace Acorn
{
    public class User
    {
        public static int FREE_TYPE = 0;
        public static int BASIC_TYPE = 1;
        public static int PREMIUM_TYPE = 2;
        public static int GROUP_TYPE = 3;
        public static int FREE_TRIAL_BASIC = 4;
        public static int FREE_TRIAL_ADVANCED = 5;
        public static int DISCOVERY_TYPE = 6;
        public static int DISCOVERY_FREE_TRIAL = 7;
        public static int NETSUITE_DISCOVERY_FREE_TRIAL = 8;
        public static string FREE_NAME = "Free";
        public static string BASIC_NAME = "Basic";
        public static string PREMIUM_NAME = "Professional";
        public static string GROUP_NAME = "Group";
        public static string FREE_TRIAL_BASIC_NAME = "Free Trial";
        public static string FREE_TRIAL_ADVANCED_NAME = "Free Trial";
        public static string DISCOVERY_NAME = "Discovery";
        public static string DISCOVERY_FREE_TRIAL_NAME = "Discovery Free Trial";
        public static string NETSUITE_DISCOVERY_FREE_TRIAL_NAME = "Netsuite Free Trial";
        public static string[][] OWNER_ACCESS_PRIVILIGES =
            new string[][] { 
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed"},
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed"}, 
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed"}, 
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" },
            new string[] {"Dashboard", "NewDashboard", "EditDashboard", "Adhoc", "EnableDownload", "AdvancedFilter", "MaintainCatalog", "PremiumAdHoc", "DashboardDelete", "SelfScheduleAllowed" }
            };
        public static string[][] USER_ACCESS_PRIVILIGES =
            new string[][] { 
            new string[] {"Dashboard" },
            new string[] {"Dashboard" },
            new string[] {"Dashboard"},
            new string[] {"Dashboard"},
            new string[] {"Dashboard", "EnableDownload"},
            new string[] {"Dashboard", "EnableDownload" },
            new string[] {"Dashboard"},
            new string[] {"Dashboard" },
            new string[] {"Dashboard" }
            };
        public static int NO_CONTACT = 0;
        public static int ALLOW_CONTACT = 1;

        public const int USER_MODE_DISCOVERY_DISABLED = 1;
		public const int USER_MODE_DISCOVERY_ONLY = 2;
		public const int USER_MODE_DISCOVERY_HYBRID = 3;

        public Guid ID;
        public string Username;
        public string Password;
        public string Email;
        public string LastName;
        public string FirstName;
        public string Title;
        public string Company;
        public string Phone;
        public string City;
        public string State;
        public string Zip;
        public string Country;
        public int AccountType;
        public int ShareLimit;
        public int NumShares;
        public bool StepByStep;
        public int ContactStatus;
        public long MaxScriptStatements;
        public int MaxInputRows;
        public int MaxOutputRows;
        public int ScriptQueryTimeout;
        public int MaxDeliveredReportsPerDay;
        public string DatabaseConnectString;
        public string DatabaseType;
        public string DatabaseDriver;
        public string DatabaseLoadDir;
        public string AdminUser;
        public string AdminPwd;
        public bool RepositoryAdmin;
        public string DefaultSpace;
        public Guid DefaultSpaceId;
        public bool DefaultDashboards;
        public Guid LogoImageID;
        public long HeaderBackground = -1;
        public string PartnerCode;
        public bool CreateNewUserRights;
        public string QueryDatabaseConnectString;
        public string QueryDatabaseType;
        public string QueryDatabaseDriver;
        public string QueryUser;
        public string QueryPwd;
        public string QueryConnectionName;
        public bool isSuperUser;
        public Guid AdminAccountId;     // account id that this user manages, should be empty for most users
        public Guid ManagedAccountId;   // account id that this user belongs to
        public List<int> UserACLs;
        public AccountDetails actDetails;
        public bool OperationsFlag;
        public int ReleaseType;
        public bool Disabled;
        public bool HTMLInterface;
        public int SpaceType;
        public bool ExternalDBEnabled; // used for redshift
        public bool DenyAddSpace;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Email: " + Email);
            return sb.ToString();
        }

        public DataTable getDataSet()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AccountType");
            dt.Columns.Add("Username");
            dt.Columns.Add("Email");
            dt.Columns.Add("DatabaseConnectString");
            dt.Columns.Add("DatabaseType");
            dt.Columns.Add("DatabaseDriver");
            dt.Columns.Add("DatabaseLoadDir");
            dt.Columns.Add("AdminUser");
            dt.Columns.Add("AdminPwd");
            dt.Columns.Add("RepositoryAdmin", typeof(bool));
            dt.Columns.Add("DefaultSpace");
            dt.Columns.Add("DefaultSpaceId");
            dt.Columns.Add("DefaultDashboards", typeof(bool));
            dt.Columns.Add("CreateNewUserRights", typeof(bool));
            dt.Columns.Add("QueryDatabaseConnectString");
            dt.Columns.Add("QueryDatabaseType");
            dt.Columns.Add("QueryDatabaseDriver");
            dt.Columns.Add("QueryUser");
            dt.Columns.Add("QueryPwd");
            dt.Columns.Add("QueryConnectionName");
            DataRow dr = dt.NewRow();
            if (AccountType == FREE_TYPE)
                dr["AccountType"] = FREE_NAME;
            else if (AccountType == BASIC_TYPE)
                dr["AccountType"] = BASIC_NAME;
            else if (AccountType == PREMIUM_TYPE)
                dr["AccountType"] = PREMIUM_NAME;
            else if (AccountType == GROUP_TYPE)
                dr["AccountType"] = GROUP_NAME;
            else if (AccountType == FREE_TRIAL_BASIC)
                dr["AccountType"] = FREE_TRIAL_BASIC_NAME;
            else if (AccountType == FREE_TRIAL_ADVANCED)
                dr["AccountType"] = FREE_TRIAL_ADVANCED_NAME;
            else if (AccountType == DISCOVERY_TYPE)
                dr["AccountType"] = DISCOVERY_NAME;
            else if (AccountType == DISCOVERY_FREE_TRIAL)
                dr["AccountType"] = DISCOVERY_FREE_TRIAL_NAME;
            else if (AccountType == NETSUITE_DISCOVERY_FREE_TRIAL)
                dr["AccountType"] = NETSUITE_DISCOVERY_FREE_TRIAL_NAME;           
            dr["Username"] = Username;
            dr["Email"] = Email;
            dr["DatabaseConnectString"] = DatabaseConnectString;
            dr["DatabaseType"] = DatabaseType;
            dr["DatabaseDriver"] = DatabaseDriver;
            dr["DatabaseLoadDir"] = DatabaseLoadDir;
            dr["AdminUser"] = AdminUser;
            dr["AdminPwd"] = AdminPwd;
            dr["RepositoryAdmin"] = RepositoryAdmin;
            dr["DefaultSpace"] = DefaultSpace + '/' + DefaultSpaceId;
            dr["DefaultDashboards"] = DefaultDashboards;
            dr["CreateNewUserRights"] = CreateNewUserRights;
            dr["QueryDatabaseConnectString"] = QueryDatabaseConnectString;
            dr["QueryDatabaseType"] = QueryDatabaseType;
            dr["QueryDatabaseDriver"] = QueryDatabaseDriver;
            dr["QueryUser"] = QueryUser;
            dr["QueryPwd"] = QueryPwd;
            dr["QueryConnectionName"] = QueryConnectionName;
            dt.Rows.Add(dr);
            return (dt);
        }

        public bool isDiscoveryModeOnly()
        {
            return getUserMode() == USER_MODE_DISCOVERY_ONLY;
        }

        public int getUserMode()
        {
            int userMode = USER_MODE_DISCOVERY_DISABLED;
            if(AccountType == BASIC_TYPE || AccountType == PREMIUM_TYPE || AccountType == GROUP_TYPE)
            {
                userMode = USER_MODE_DISCOVERY_HYBRID;
            }
            else if (AccountType == DISCOVERY_TYPE || AccountType == DISCOVERY_FREE_TRIAL || AccountType == NETSUITE_DISCOVERY_FREE_TRIAL)
            {
                userMode = USER_MODE_DISCOVERY_ONLY;
            }

            return userMode;
        }

        public bool CanSkin
        {
            get
            {
                return !isFreeTrialUser;
            }
        }

        public bool CanManageCatalog
        {
            get
            {
                return true;
            }
        }

        public bool CanUseTestMode
        {
            get
            {
                return !isFreeTrialUser;
            }
        }

        public bool CanUploadData
        {
            get
            {
                return true;
            }
        }

        public bool isFreeTrialUser
        {
            get
            {
                //return true; // AccountType == FREE_TRIAL_BASIC || AccountType == FREE_TRIAL_ADVANCED;
                return AccountType == FREE_TRIAL_BASIC || AccountType == FREE_TRIAL_ADVANCED;
            }
        }

        public bool isDiscoveryFreeTrial
        {
            get
            {
                if (AccountType == NETSUITE_DISCOVERY_FREE_TRIAL)
                    return AccountType == NETSUITE_DISCOVERY_FREE_TRIAL;
                else
                    return AccountType == DISCOVERY_FREE_TRIAL;
            }
        }
    }
}
