﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;

using Acorn.TrustedService;
using Performance_Optimizer_Administration;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class DefaultUserPage
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string useisapi = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["isapiEnabled"];
        private static string exportServlet = "/SMIWeb/ExportServlet.jsp";
 
        public static string INVITATION_QUOTA_ERROR = "Sorry but we are unable to make you a member of the shared space. The sender has already exceeded the share limit.";
        public static string INVITATION_UNAVAILABLE = "Do to popular demand, we are temporarily unable to allow you to accept this invitation. We are adding capacity, so please check back later.";
        public static char SEMI_COLON = ';';
        public static char COMMA = ',';
        public static char EQUALS = '=';
        public static string SINGLE_QUOTE = "'";
        
        public static UserSpaces getAllSpaces(HttpSessionState session, HttpRequest request)
        {
            UserSpaces response = new UserSpaces();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";       
                return response;
            }

            IDictionary<string, string> userPref = ManagePreferences.getPreference(null, u.ID, "Locale");
            CultureInfo locale = null;
            if (userPref != null && userPref["Locale"] != null)
                locale = CultureInfo.GetCultureInfoByIetfLanguageTag(userPref["Locale"]);

            response.UserName = u.Username;
            if (session["LoggedInDate"] != null)
            {
                DateTime loggedInDate = (DateTime)session["LoggedInDate"];
                if (loggedInDate != null)
                {
                    if (locale != null)
                        response.LastLoginDate = loggedInDate.ToString("u", locale);
                    else
                        response.LastLoginDate = loggedInDate.ToString("u");
                }
            }

            loadMessages(u, response, locale);
            loadInvitations(u, response, locale);

            bool canCreateNewSpace = canDoOperation(session, u, ACLDetails.ACL_CREATE_NEW_SPACE, true) && !u.DenyAddSpace;
            setupSpaces(session, request, response, locale);
            response.EnableCopy = canCreateNewSpace; // if you can create, you can copy
            response.canCreateNewSpace = canCreateNewSpace;
            response.showTemplate = canDoOperation(session, u, ACLDetails.ACL_BROWSE_TEMPLATE, false);
            response.UserMode = u.getUserMode();
            response.EnableUserManagement = UserManagementUtils.isUserAuthorized(u);
            response.ShowDiscoveryFreeTrialPage = u.isDiscoveryFreeTrial;
            response.ShowFreeTrialPage = u.isFreeTrialUser;
            response.serverConfig = Util.getServerConfig();
            response.ExternalDBEnabled = u.ExternalDBEnabled;
            return response;
        }

        public static SpaceDetails getLoggedInSpaceDetails(HttpSessionState session, HttpRequest request, HttpResponse httpResponse)
        {
            SpaceDetails response = new SpaceDetails();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            User u = (User)session["User"];            
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";  
                return response;
            }
            Space sp = Util.getSessionSpace(session);
            
            if (sp != null)
            {
               response = getSpaceDetails(session, request, sp.ID.ToString(), sp.Name);                
            }
            return response;
        }

        public static SpaceDetails getSpaceDetails(HttpSessionState session, HttpRequest request, string spaceID, string spaceName)
        {
            return getSpaceDetails(session, request, spaceID, spaceName, false);
        }

        public static SpaceDetails getSpaceDetails(HttpSessionState session, HttpRequest request, string spaceID, string spaceName, bool forceRefresh)
        {
            SpaceDetails response = new SpaceDetails();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            
            User u = (User)session["User"];
            Space sp = Util.getSessionSpace(session);
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";                
                return response;
            }

            List<SpaceMembership> mlist = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getSpaces(conn, mainSchema, u, true);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            SpaceMembership sm = null;
            for (int i = 0; i < mlist.Count; i++)
            {
                if (mlist[i].Space.ID.ToString() == spaceID)
                {                    
                    sm = mlist[i];
                    break;
                }
            }

            if (sm == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to retrieve selected space details";
                return response;
            }

            if ( sp == null || (sp != null && sp.ID.ToString() != spaceID) || forceRefresh)
            {
                // Make sure we are logged out of SMIWeb or old space    
                if (sp != null)
                {
                    Util.logoutSMIWebSession(session, sp, request);
                    SchedulerUtils.endSchedulerSession(session, request);
                }
                sp = sm.Space;
                if(preventFullSessionSetup(session, sp, u, response))
                {
                    return response;
                }

                Util.setSpace(session, sp, u, false);
                if (session != null && session["MAF"] == null)
                {
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    error.ErrorMessage = "Failed to access the space, the meta data repository may be too big.  Please contact Birst Customer Support.";
                    return response;
                }
            }

            response.SpaceID = sm.Space.ID.ToString();
            response.SpaceName = sm.Space.Name;

            if(preventFullSessionSetup(session, sp, u, response))
            {
                return response;
            }

            if (sm.Space.Comments != null)
            {
                response.SpaceComments = sm.Space.Comments;
            }
            response.OwnerUsername = sm.OwnerUsername;
            response.Owner = sm.Owner;

            response.UserID = u.ID.ToString();
            
            IDictionary<string, string> userPref = ManagePreferences.getPreference(null, u.ID, sp.ID, "Locale");
            CultureInfo locale = null;
            if (userPref != null && userPref["Locale"] != null)
                locale = CultureInfo.GetCultureInfoByIetfLanguageTag(userPref["Locale"]);
            response.SpaceSize = getSpaceSize(sm.Space, locale);
            Object lastLoadDate = getLastLoadDate(sm.Space);
            if (lastLoadDate != null)
            {
                if (locale != null)
                    response.LastUpload = ((DateTime)lastLoadDate).ToString("d", locale);
                else
                    response.LastUpload = ((DateTime)lastLoadDate).ToString("d");
            }
            setSpace(session, sm.Administrator, u, response);
            response.SuperUser = !u.isFreeTrialUser && Util.isTypeSuperUser(session);
            if (sp.Automatic)
            {
                response.SpaceMode = Space.SPACE_MODE_AUTOMATIC;
            }
            else if (sp.DiscoveryMode)
            {
                response.SpaceMode = Space.SPACE_MODE_DISCOVERY;
            }
            else
            {
                response.SpaceMode = Space.SPACE_MODE_ADVANCED;
            }

            response.IndependentMode = isIndependentMode(sp);

            response.FreeTrialUser = u.isFreeTrialUser;
            response.DenyAddSpace = u.DenyAddSpace;

            response.HostName = Util.getHostName();
            response.AppVersion = Util.getAppVersion();
            return response;
        }

        public static bool isIndependentMode(Space sp)
        {
            // is sforce present
            string sforceXmlPath = Path.Combine(sp.Directory, "sforce.xml");
            if (File.Exists(sforceXmlPath))
            {
                SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sforceXmlPath);
                if (settings.automatic)
                {
                    return true;
                }
            }
            /*
            // go through repository and see if there is any source with SFORCE load group/source group
            if (maf != null && maf.stagingTableMod != null)
            {
                List<StagingTable> stList = maf.stagingTableMod.getAllStagingTables();
                foreach (StagingTable st in stList)
                {
                    string[] loadGroups = st.LoadGroups;
                    if((loadGroups != null && Array.IndexOf<string>(loadGroups, SalesforceLoader.LOAD_GROUP_NAME) >=0))
                    {
                        return true;
                    }

                    string sourceGroups = st.SourceGroups;
                    if (sourceGroups != null && sourceGroups.Trim().Length > 0)
                    {
                        string[] sg = sourceGroups.Split(new char[] { ',' });
                        if (Array.IndexOf<string>(sg, SalesforceLoader.SOURCE_GROUP_NAME) >= 0)
                        {
                            return true;
                        }
                    }
                }
            }
            */
            return false;

        }

        private static bool preventFullSessionSetup(HttpSessionState session, Space sp, User u, SpaceDetails response)
        {
            int spaceOpId = SpaceOpsUtils.getSpaceAvailability(sp);
            if (SpaceOpsUtils.isNewSpaceBeingCopied(spaceOpId) || SpaceOpsUtils.isSwapSpaceInProgress(spaceOpId))
            {
                Util.resetSessionObjectWithSpace(session, sp, u);
                response.AvailabilityCode = spaceOpId;
                response.SpaceID = sp.ID.ToString();
                response.SpaceName = sp.Name;
                return true;
            }
            return false;
        }

        public static GenericResponse removeMember(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";                 
                return response;
            }

            Space sp = null;
            sp = (Space)session["space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve selected space details";
                return response;
            }
            else
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.removeSpaceMembership(conn, mainSchema, u.ID, sp.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                session.Remove("space");                
            }
            return response;
        }

        public static GenericResponse sendInvitationResponse(HttpSessionState session, bool accept, string invitedSpaceID, string inviteUserEmail)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)session["space"];
            User u = (User)session["User"];
            
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";                
                return response;
            }
            
            //int rem = Int32.Parse((string)e.CommandArgument);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<Invitation> ilist = Database.getInvitations(conn, mainSchema, u.Email);
                // Make sure invitation hasn't been revoked

                bool found = false;
                for (int i = 0; i < ilist.Count; i++)
                {
                    if (ilist[i].SpaceID.ToString() == invitedSpaceID)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    error.ErrorMessage = "Invitation not active.";
                    return response;
                }

                User inviteUser = null;
                if (accept)
                {
                    inviteUser = Database.getUserByEmail(conn, mainSchema, inviteUserEmail);
                    if (inviteUser.NumShares >= inviteUser.ShareLimit)
                    {
                        error.ErrorMessage = INVITATION_QUOTA_ERROR;
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        using (log4net.ThreadContext.Stacks["itemid"].Push(invitedSpaceID))
                        {
                            Global.userEventLog.Info("ACCEPTFAILQUOTA");
                        }
                        return response;
                    }
                    bool allowAccept = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["AllowAcceptInvitations"]);
                    if (!allowAccept)
                    {
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        error.ErrorMessage = INVITATION_UNAVAILABLE;
                        using (log4net.ThreadContext.Stacks["itemid"].Push(invitedSpaceID))
                        {
                            Global.userEventLog.Info("ACCEPTFAILALLOW");
                        }
                        return response;
                    }
                }

                Database.respondToInvitation(conn, mainSchema, u, new Guid(invitedSpaceID), accept);
                Space invitedSpace = Database.getSpace(conn, mainSchema, new Guid(invitedSpaceID));
                if (accept)
                {
                    Database.setShareQuotaUsage(conn, mainSchema, inviteUser);
                    Database.updateUser(conn, mainSchema, inviteUser);
                    if (invitedSpace != null)
                    {
                        Util.addUserToSpace(null, invitedSpace, u.Username, Util.USER_GROUP_NAME);
                    }
                }
                else
                {
                    if (invitedSpace != null)
                    {
                        Util.removeUserFromSpace(invitedSpace, u.Username);
                    }
                }

                using (log4net.ThreadContext.Stacks["itemid"].Push(invitedSpaceID))
                {
                    Global.userEventLog.Info(accept ? "ACCEPT" : "DECLINE");
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        private static void loadInvitations(User u, UserSpaces response, CultureInfo locale)
        {
            QueryConnection conn = null;
            List<Invitation> ilist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                ilist = Database.getInvitations(conn, mainSchema, u.Email);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            List<InviteResponse> invitesList = new List<InviteResponse>();
            foreach (Invitation invitation in ilist)
            {
                InviteResponse invite = new InviteResponse();
                invite.SpaceID = invitation.SpaceID.ToString();
                invite.SpaceName = invitation.SpaceName;
                if (locale != null)
                    invite.InviteDate = invitation.InviteDate.ToString("d", locale);
                else
                    invite.InviteDate = invitation.InviteDate.ToString("d");
                invite.InviteUserEmail = invitation.InviteUserEmail;
                invite.Email = invitation.Email;
                invitesList.Add(invite);
            }
            response.Invites = invitesList.ToArray();
        }

        private static void loadMessages(User u, UserSpaces response, CultureInfo locale)
        {
            QueryConnection conn = null;
            List<Message> mlist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getMessages(conn, mainSchema, u.ManagedAccountId);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            List<MessageResponse> messagesList = new List<MessageResponse>();
            foreach (Message msg in mlist)
            {
                MessageResponse msgR = new MessageResponse();
                if (locale != null)
                    msgR.Date = msg.Date.ToString("G", locale);
                else
                    msgR.Date = msg.Date.ToString("G");
                msgR.Text = msg.Text;
                messagesList.Add(msgR);
            }
            response.Messages = messagesList.ToArray();
        }

       public static void setSpace(HttpSessionState session, bool admin, User u, SpaceDetails response)
       {
           MainAdminForm maf = (MainAdminForm)session["MAF"];
           if (maf == null)
               return;
           
           Space sp = (Space)session["space"];
           if (sp == null)
               return;

           Dictionary<string, int> curLoadNumbers = null;
           QueryConnection conn = null;
           try
           {
               conn = ConnectionPool.getConnection();
               curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
               // Get the updated space groups from db
               sp.SpaceGroups = Database.getSpaceGroups(conn, mainSchema, sp);
           }
           finally
           {
               ConnectionPool.releaseConnection(conn);
           }

           // Check for imported tables
           List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, session, null);

           bool adhoc = isAdhocPermitted(session, u);
           response.Adhoc = adhoc;
           response.QuickDashboard = adhoc;
           // the current system shows it for everybody
           response.Dashboards = true;
           
           bool hasRealtime = false;
           //get local etl connections
           Dictionary<string, Connection> localETLConnections = BirstConnectUtil.getLocalETLConenctions(sp, maf);
           List<DatabaseConnection> dclist = new List<DatabaseConnection>(maf.connection.connectionList);
           if (maf.Imports != null)
           {
               Dictionary<MainAdminForm, object> seenmaf = new Dictionary<MainAdminForm, object>();
               foreach (ImportedRepositoryItem it in itlist)
               {
                   if (seenmaf.ContainsKey(it.ispace.maf))
                       continue;
                   dclist.AddRange(it.ispace.maf.connection.connectionList);
                   hasRealtime = hasRealtime || it.ispace.maf.EnableLiveAccessToDefaultConnection;
                   seenmaf.Add(it.ispace.maf, null);
               }
           }
           foreach (DatabaseConnection dc in dclist)
           {
               if (dc.Realtime || dc.Name == "GoogleAnalytics")
               {
                   if (dc.Realtime && dc.VisibleName != null && localETLConnections != null && localETLConnections.ContainsKey(dc.VisibleName))
                       continue;
                   hasRealtime = true;
                   break;
               }
           }
           hasRealtime = hasRealtime || maf.EnableLiveAccessToDefaultConnection;

           bool hasData = curLoadNumbers.Count > 0 || hasRealtime;

           if (!hasData && itlist.Count > 0)
           {
               bool hasTables = false;
               foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
               {
                   if (!st.Disabled)
                   {
                       hasTables = true;
                       break;
                   }
               }
               // If no data, but importing from a space that does have data, still enable designer, etc.
               // But only if there are no tables in this space
               if (!hasTables)
               {
                   try
                   {
                       conn = ConnectionPool.getConnection();
                       Dictionary<string, int> imploadnumbers = null;
                       foreach (ImportedRepositoryItem it in itlist)
                       {
                           imploadnumbers = Database.getCurrentLoadNumbers(conn, mainSchema, it.ispace.sp.ID);
                           if (imploadnumbers.Count > 0)
                               break;
                       }
                       if (imploadnumbers != null && imploadnumbers.Count > 0)
                           hasData = true;
                   }
                   finally
                   {
                       ConnectionPool.releaseConnection(conn);
                   }
               }
           }

           if (hasData && File.Exists(sp.Directory + "\\repository.xml"))
           {
               session["DashboardPermitted"] = true;
               session["AdhocPermitted"] = adhoc;
               if (adhoc)
               {
                   response.EnableAdhoc = true;                    
                   response.EnableQuickDashBoards = true;
               }
               else
               {
                   response.EnableAdhoc = false;
                   response.EnableQuickDashBoards = false;                    
               }
               response.EnableDashboards = true;
               if (Util.isFeatureFlagEnabled(Util.FEATURE_FLAG_VIZ_GA, true))
               {
                   response.EnableVisualizer = Util.isOwner(sp, u) || UserAndGroupUtils.isVisualizerPermittedByACL(u, sp.SpaceGroups);
               }
               else
               {
                   response.EnableVisualizer = UserAndGroupUtils.isVisualizerPermittedByACL(u, sp.SpaceGroups);
               }
           }
           else
           {
               session["DashboardPermitted"] = false;
               session["AdhocPermitted"] = false;
               response.EnableAdhoc = false;
               response.EnableDashboards = false;
               response.EnableQuickDashBoards = false;
               response.EnableVisualizer = false;
           }
           response.Administrator = admin;                      
           if (admin)
           {
               AdminModulesAccess ama = getAdminModulesAccess(session, sp, maf, u, curLoadNumbers);
               ama.CopySpace = canDoOperation(session, u, ACLDetails.ACL_CREATE_NEW_SPACE, false); // if you can create, you can copy
               response.AdminAccess = ama;
           }

           overrideFlagsForDiscoveryTrialVisualizerOnlySpace(u, sp, response);
        }

        /// <summary>
        /// Only show enable visualizer button and nothing else for 
        /// discovery free trial users with visualizer ACL enabled
        /// </summary>
        /// <param name="u"></param>
        /// <param name="sp"></param>
        /// <param name="response"></param>
       private static void overrideFlagsForDiscoveryTrialVisualizerOnlySpace(User u, Space sp, SpaceDetails response)
       {
           if (u.isDiscoveryFreeTrial && UserAndGroupUtils.isACLDefinedInAnyGroupForUser(u, sp.SpaceGroups, ACLDetails.ACL_VIZUALIZER_ONLY))
           {
               response.EnableAdhoc = response.Adhoc = false;
               response.EnableDashboards = response.Dashboards = false;
               response.EnableQuickDashBoards = response.QuickDashboard = false;
               response.Administrator = false;
               response.EnableVisualizer = true;
               response.AdminAccess = null;
           }
       }

       private static AdminModulesAccess getAdminModulesAccess(HttpSessionState session, Space sp, MainAdminForm maf, User u, Dictionary<string, int> curLoadNumbers)
       {
           AdminModulesAccess ama = new AdminModulesAccess();
           
           bool publish = SpaceAdminService.checkSpacePublishability(maf, sp);
           
           ama.Publish = publish;
           ama.UseTemplate = !sp.Locked;

           bool hasRealtime = false;
           foreach (DatabaseConnection dc in maf.connection.connectionList)
           {
               if (dc.Realtime)
               {
                   hasRealtime = true;
                   break;
               }
           }
           
           ama.ManageCatalog = u.CanManageCatalog;
           ama.ShareSpace = true;
           ama.CustomFormula = true;
           ama.ShareSpace = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["AllowAcceptInvitations"]);

           // check the ACL for Cache Clearance and Query Admin Module
           bool servicesAdmin = false;           
           ama.ServicesAdmin = servicesAdmin;
           ama.QuartzSchedulerEnabled = Util.isQuartzSchedulerEnabled();
           ama.showOldScheduler = Global.showOldScheduler;
           // Show dataconductor if applicable
           bool hasWebDAV = false;
           List<Product> plist = (List<Product>)session["products"];
           foreach (Product p in plist)
           {
               if (p.Type == Product.TYPE_WEBDAV)
               {
                   hasWebDAV = true;
                   break;
               }
           }
           ama.BirstConnect = hasWebDAV;
           ama.LocalConnect = hasRealtime || maf.EnableLiveAccessToDefaultConnection;
           return ama;
       }     

        private static void setupSpaces(HttpSessionState session, HttpRequest request, UserSpaces response, CultureInfo locale)
        {
            List<SpaceSummary> spacesList = new List<SpaceSummary>();

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Space sp = (Space)session["space"];
                User u = (User)session["User"];
                List<SpaceMembership> mlist = Database.getSpaces(conn, mainSchema, u, true);
                session["mlist"] = mlist;
                if (mlist.Count == 0)
                {
                    // nothing to do for now
                }
                else
                {
                    if (sp != null)
                    {
                        // Make sure a space hasn't been deleted
                        bool found = false;
                        for (int i = 0; i < mlist.Count; i++)
                        {
                            if (mlist[i].Space.ID == sp.ID)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            sp = null;
                        else
                        {
                            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp);
                            if (SpaceOpsUtils.isNewSpaceBeingCopied(spaceOpID) || SpaceOpsUtils.isSwapSpaceInProgress(spaceOpID))
                            {
                                Global.systemLog.Warn("Login to space not allowed. " + SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID));
                                Util.resetSessionObjectWithSpace(session, sp, u);
                            }
                            else
                            {
                                // Update if data conductor has changed
                                if (File.Exists(sp.Directory + "\\" + Util.DCCONFIG_FILE_NAME))
                                {
                                    // move dcconfig.xml to dcconfig directory
                                    BirstConnectUtil.moveFileToDirectory(sp, sp.Directory + "\\" + Util.DCCONFIG_DIR, Util.DCCONFIG_FILE_NAME);
                                }
                                if (Directory.Exists(sp.Directory + "\\" + Util.DCCONFIG_DIR))
                                {
                                    DirectoryInfo source = new DirectoryInfo(sp.Directory + "\\" + Util.DCCONFIG_DIR);
                                    FileInfo[] sfInfo = source.GetFiles();
                                    if (sfInfo != null)
                                    {
                                        Dictionary<string, DateTime> dcFileMap = session["dctime"] == null ? null : (Dictionary<string, DateTime>)session["dctime"];
                                        //call setSpace if directory has file and session does not have dc i.e. if session does not have any dc and directory has files means first dc file is added
                                        if (dcFileMap == null)
                                        {
                                            if (sfInfo.Length > 0)
                                                Util.setSpace(session, sp, null, false);
                                        }
                                        else
                                        {
                                            //call setSpace if dictory file count and session dc count does not match i.e. dcconfig added or removed
                                            //i.e. typical scenario session has dc.Count =1 and user deleted config from admin UI and coming to home screen.. so sfInfo.count = 0 and session's dc.Count = 1
                                            if (sfInfo.Length != dcFileMap.Count)
                                                Util.setSpace(session, sp, null, false);
                                            else
                                            {
                                                //call setSpace if config file key not found or config file modified
                                                foreach (FileInfo fi in sfInfo)
                                                {
                                                    DateTime ctime = File.GetLastWriteTime(fi.FullName);
                                                    if (!dcFileMap.ContainsKey(fi.Name) || ctime != dcFileMap[fi.Name])
                                                    {
                                                        Util.setSpace(session, sp, null, false);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }     
                    }
                    if (sp == null)
                    {
                        sp = mlist[0].Space;

                        Util.setSpace(session, sp, null, false);
                        response.Selected = 0;
                        // Make sure we are logged out of the SMIWeb                   
                        if (request != null)
                        {
                            Util.logoutSMIWebSession(session, sp, request);
                            SchedulerUtils.endSchedulerSession(session, request);
                        }
                    }

                    bool ssoAuthentication = false;
                    if (session["ssoAuthentication"] != null)
                        ssoAuthentication = true;
                    long size = 0;
                    bool anyTrialVisualizerSpace = false;
                    for (int i = 0; i < mlist.Count; i++)
                    {
                        if (!ssoAuthentication || (ssoAuthentication && mlist[i].Space.ID == sp.ID))
                        {
                            SpaceSummary spSummary = new SpaceSummary();
                            spSummary.SpaceName = mlist[i].Space.Name;
                            spSummary.SpaceID = mlist[i].Space.ID.ToString();
                            spSummary.OwnerUsername = mlist[i].OwnerUsername;
                            spSummary.Owner = mlist[i].Owner;
                            spSummary.SpacePath = mlist[i].Space.Directory;
                            if (isTrialVisualizerOnlySpace(u, mlist[i].Space))
                            {
                                spSummary.TrialVisualizer = true;
                                anyTrialVisualizerSpace = true;
                            }
                            if (mlist[i].Space.ID == sp.ID)
                            {
                                response.Selected = (ssoAuthentication ? 0 : i);
                            }
                            spacesList.Add(spSummary);
                            if (mlist[i].Owner)
                                size += getSpaceSize(mlist[i].Space); // returned in MB
                        }
                    }
                    response.SpacesList = spacesList.ToArray();                    
                    response.OptOutVisualizerBanner = anyTrialVisualizerSpace ? ManagePreferences.isUserPreferenceEnabled(u, ManagePreferences.PREFERNCE_OPTOUT_VISUALIZER_BANNER, false) : true;

                    if (size > 0)
                    {
                        response.SizeOfSpaceOwned = Util.formatBytes(size, locale);
                        if (u.isFreeTrialUser && size > 1024)
                        {
                            response.SizeOfSpaceOwned += ", you have exceeded your data limit, please delete data";
                            Global.systemLog.Warn("Data limit exceeded (" + Util.formatBytes(size) + ")");
                        }
                    }
                    else
                    {
                        response.SizeOfSpaceOwned = null;
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static bool isTrialVisualizerOnlySpace(User u, Space space)
        {
            return u.isDiscoveryFreeTrial && isVisualizerOnlyACLEnabled(u, space);
        }

        private static bool isVisualizerOnlyACLEnabled(User u, Space sp)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> spaceGroups = Database.getSpaceGroups(conn, mainSchema, sp);
                return UserAndGroupUtils.isACLDefinedInAnyGroupForUser(u, spaceGroups, ACLDetails.ACL_VIZUALIZER_ONLY);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static string getMembershipName(SpaceMembership sm, User u)
        {
            if (u.Username == sm.OwnerUsername)
                return (sm.Space.Name);
            return (sm.Space.Name + " (" + sm.OwnerUsername + ")");
        }

        /**
         * return the size of a space in MB
         */
        private static long getSpaceSize(Space sp)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                return Database.getSpaceSize(conn, mainSchema, sp);
            }
            catch (Exception e)
            {
                Global.systemLog.Debug(e.Message);
                return 0;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static string getSpaceSize(Space sp, CultureInfo locale)
        {
            return Util.formatBytes(getSpaceSize(sp), locale);
        }

        public static Object getLastLoadDate(Space sp)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Object result = Database.getLoadDate(conn, mainSchema, sp.ID);
                return result;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void setSMIWebSessionForCatalog(HttpSessionState session, HttpRequest request, 
            HttpResponse response, SpaceDetails details, Space sp, User u)
        {
            if (details.AdminAccess == null)
            {
                return;
            }

            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
            {
                return;
            }

            if (details.AdminAccess.ManageCatalog == true)
            {
                // create SMIWeb Session for Manage Catalog if catalog access is available
                Dictionary<string, int> curLoadNumbers = null;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                bool validSpace = false;
                bool hasRealtime = false;
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    if (dc.Realtime || dc.Name == "GoogleAnalytics")
                    {
                        hasRealtime = true;
                        break;
                    }
                }
                validSpace = (sp != null) && ((curLoadNumbers.Count > 0 || hasRealtime) && File.Exists(sp.Directory + "\\repository.xml"));
                if (validSpace && u.CanManageCatalog)
                {
                    String token = Util.getSMILoginToken(u.Username, sp);
                    if (!Util.getSMILoginSession(session, sp, token, response, request))
                    {
                        Global.systemLog.Error("Not able to login SMIWeb for catalog management");
                    }
                }
            }
        }

        // XXX need acct level ACLs
        public static bool isHomePermitted(HttpSessionState session, User u)
        {
            return canDoOperation(session, u, ACLDetails.ACL_HOME, true);
        }

        public static bool isSettingsLinkVisible(HttpSessionState session, User u)
        {
            return canDoOperation(session, u, ACLDetails.ACL_ACCOUNT, false);
        }

        public static bool canDoOperation(HttpSessionState session, User u, string item, bool isAllowedInFreeTrial)
        {
            if (u == null)
                return false;
            if (!isAllowedInFreeTrial && u.isFreeTrialUser)
                return false;
            if (session != null && session["ssoAuthentication"] != null) // SSO users should not have access to this function
                return false;
            // if account admin, you do have access
            if (u.AdminAccountId != Guid.Empty && u.AdminAccountId == u.ManagedAccountId)
                return true;
            // if repository admin, you do have access
            if (u.RepositoryAdmin)
                return true;

            // XXX HACK FOR NOW XXX
            return true;
            /*
            if (u.UserACLs != null)
            {
                foreach (int aclId in u.UserACLs)
                {
                    string ACLTag = ACLDetails.getACLTag(aclId);
                    if (ACLTag == item)
                    {
                        return true;
                    }
                }
            }
            return false;
            */
        }

        public static bool isAdhocPermitted(HttpSessionState session, User u)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            Space sp = Util.getSessionSpace(session);
            if (maf == null || sp == null)
            {
                return false;
            }

            List<SpaceGroup> groupsList = new List<SpaceGroup>();
            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {
                foreach (SpaceGroup spg in sp.SpaceGroups)
                {
                    groupsList.Add((SpaceGroup)spg.Clone());
                }
            }

            // If the user is an owner, return true
            if (Util.isOwner(session, u))
            {
                return true;
            }

            // If not the owner, check the explicit defined user-group mapping
            if (isAdhocPermitted(groupsList, u))
            {
                return true;
            }

            // See if the space has been configured to use Dynamic Groups 
            // for Designer Access. There is a separate property on Modify Space Properties screen.

            // See if the dynamic group variable is passed in SSO. If it passed 
            // no need to look for dynamic group variable in Repository since  
            // values passed overwrites repository query output

            bool fetchRepDynamicGroupVariable = true;
            List<string> ssoPassedGroupList = getSSOPassedGroupNames(session);
            if (ssoPassedGroupList != null)
            {
                fetchRepDynamicGroupVariable = false;
                foreach (String dynGroupName in ssoPassedGroupList)
                {
                    addGroupToGroupsList(groupsList, dynGroupName, u);
                }

                // See if any group has adhoc access;
                return isAdhocPermitted(groupsList, u);
            }

            
            if (fetchRepDynamicGroupVariable && sp.UseDynamicGroups)
            {
                bool dynamicGroupsFound = false;
                // Is there a dynamic groups variable defined? Add them to the 'groupsList'
                Variable dynamicGroupsVar = GroupManagementService.getDynamicGroupsQueryVariable(session);
                if (dynamicGroupsVar != null)
                {
                    try
                    {
                        // Make sure that user and group information is updated.
                        UserAndGroupUtils.updatedUserGroupMappingFile(sp);

                        // Get the dynamic groups                        
                        ExecuteResult result = getDynamicGroupsFromSMIWeb(sp, u);                        
                        if (result != null && result.groups != null)
                        {
                            dynamicGroupsFound = true;
                            // If there are dynamic groups, add them to the repository's list
                            foreach (String groupName in result.groups)
                            {
                                addGroupToGroupsList(groupsList, groupName, u);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error("Error while getting dynamic group details : ", ex);
                    }
                }
                if (dynamicGroupsFound)
                {
                    return isAdhocPermitted(groupsList, u);
                }
            }
            return false;
        }

        public static ExecuteResult getDynamicGroupsFromSMIWeb(Space sp, User u)
        {
            // Get the dynamic groups
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            ts.Timeout = 300 * 1000;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            Global.systemLog.Info("Starting Dynamic Group Fetching for space " + sp.ID.ToString());
            ExecuteResult result = ts.getDynamicGroups(sp.Directory, u.Username, sp.ID.ToString());
            Global.systemLog.Info("Completed Dynamic Group Fetching for space " + sp.ID.ToString());
            return result;
        }

        private static bool isAdhocPermitted(List<SpaceGroup> groupsList, User u)
        {
            // See if any group has adhoc access;
            bool adhoc = false;
            if (groupsList != null && groupsList.Count > 0)
            {
                foreach (SpaceGroup g in groupsList)
                {
                    if (UserAndGroupUtils.isUserGroupMember(g, u.ID))
                    {
                        if (g.ACLIds != null && g.ACLIds.Count > 0)
                        {
                            foreach (int aclId in g.ACLIds)
                            {
                                string groupACLTag = ACLDetails.getACLTag(aclId);
                                if (groupACLTag == ACLDetails.ACL_ADHOC)
                                {
                                    adhoc = true;
                                    break;
                                }
                            }
                            if (adhoc)
                                break;
                        }
                    }
                }
            }
            return adhoc;
        }

        /// <summary>
        /// Check to see if the specified 'groupName' is already in the list of groups from
        /// the repository.  If the 'group' is already in the 'groupsList', check that the 
        /// current user is a member of the 'Usernames' for that group.  If not, add the user.
        /// If the 'group' is not in the 'groupsList' then ignore it.
        /// </summary>
        /// <param name="groupsList">The list of Groups from the 'repository.xml' file.</param>
        /// <param name="groupName">The group to check for.</param>
        /// <param name="email">The email to add to the group if it's not already present.</param>
        private static void addGroupToGroupsList(List<SpaceGroup> groupsList, string groupName, User u)
        {           
            foreach (SpaceGroup group in groupsList)
            {
                // Is the group already in 'groupsList'?
                if (group.Name.Equals(groupName))
                {         
                    // Check that the current user is a member of that group
                    // And add the user if it's not already a member

                    bool hasUser = UserAndGroupUtils.isUserGroupMember(group, u.ID);                  
                    if (!hasUser)
                    {
                        group.GroupUsers.Add(u);
                    }
                }
            }           
        }

        public static List<string> getSSOPassedGroupNames(HttpSessionState session)
        {
            List<string> ssoPassedGroupList = null;

            string ssoPassedSessionVars = Util.getSessionVarsFromSSORequest(session);
            if (ssoPassedSessionVars != null)
            {
                string[] keyValuePairs = ssoPassedSessionVars.Split(SEMI_COLON);
                foreach (string kvPair in keyValuePairs)
                {
                    string[] kv = kvPair.Split(EQUALS);
                    if (kv != null && kv.Length == 2 && kv[0] == Util.DYNAMIC_GROUP_VARIABLE_NAME)
                    {
                        ssoPassedGroupList = new List<string>();
                        string[] groupNames = kv[1].Split(COMMA);
                        foreach (string name in groupNames)
                        {
                            // group names need to be properly quoted
                            string quoted = SINGLE_QUOTE;
                            ssoPassedGroupList.Add(name.Trim(quoted.ToCharArray()));
                        }
                        break;
                    }
                }
            }
            return ssoPassedGroupList;
        }

        /**
         * methods for the redirection points: dashboard, catalog, export, adhoc/designer
         */

        private static void redirect(string redirectURL, HttpSessionState session, HttpResponse response, HttpServerUtility server, System.Collections.Specialized.NameValueCollection Params)
        {
            if (Params != null)
            {
                string otherArguments = Util.createQueryString(server, Params);
                if (otherArguments.Length > 0)
                {
                    redirectURL = redirectURL + "?" + otherArguments;
                }
            }
            Global.systemLog.Debug("redirect URL: " + redirectURL);
            response.Redirect(redirectURL);
        }

        public static void redirectToDashboard(HttpSessionState session, HttpResponse response, HttpServerUtility server, System.Collections.Specialized.NameValueCollection Params, HttpRequest request, Space sp)
        {
            if (Params == null)
                Params = new System.Collections.Specialized.NameValueCollection();
            Util.saveDirty(session);

            if (request != null && request.UrlReferrer != null)
            {
                Uri uri = new Uri(request.UrlReferrer.ToString());
                if ((new Regex("html")).IsMatch(uri.Query))
                {
                    Global.systemLog.Debug("redirecting to html5 dashboard");
                    redirect("~/html/dashboards.aspx", session, response, server, Params);
                }
            }

            Params.Set("birst.module", "dashboard");
            string renderType = Params["birst.renderType"];
            bool useHtml = "HTML".Equals(renderType, StringComparison.OrdinalIgnoreCase);
            User u = (User)session["user"];
            if (u != null && u.HTMLInterface)
                useHtml = true;
            if (useHtml)
                redirect("~/html/dashboards.aspx", session, response, server, Params);
            else
                redirect("~/FlexModule.aspx", session, response, server, Params);
        }

        public static void redirectToAdhoc(HttpSessionState session, HttpResponse response, HttpServerUtility server, System.Collections.Specialized.NameValueCollection Params)
        {
            if (Params == null)
                Params = new System.Collections.Specialized.NameValueCollection();
            Util.saveDirty(session);
            Params.Set("birst.module", "designer");
            redirect("~/FlexModule.aspx", session, response, server, Params);
        }

        public static void redirectToCatalog(HttpSessionState session, HttpResponse response, HttpServerUtility server, System.Collections.Specialized.NameValueCollection Params)
        {
            if (Params == null)
                Params = new System.Collections.Specialized.NameValueCollection();
            Params.Set("birst.module", "catalogmanagement");
            redirect("~/FlexModule.aspx", session, response, server, Params);
        }

        public static void redirectToExport(HttpSessionState session, HttpResponse response, HttpServerUtility server, HttpRequest request, System.Collections.Specialized.NameValueCollection Params, Space sp, User u)
        {
            string loginToken = Util.getSMILoginToken(u.Username, sp);
            if (Util.getSMILoginSession(session, sp, loginToken, response, request))
            {
                string baseUrl = "";
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                if (useisapi != null && !bool.Parse(useisapi))
                {
                    baseUrl = Util.getRequestScheme(request) + "://" + sc.URL;
                }
                string url = baseUrl + exportServlet;
                string other = Util.createQueryString(server, Params);
                if (other != null && other.Length > 0)
                    url += "?" + other;
                if (session["sessionVars"] != null && session["sessionVars"].ToString().Length > 0)
                {
                    url += "&sessionVars=" + session["sessionVars"].ToString();
                }
                url += "&birst.anticsrftoken=" + Util.encryptXSRFToken(session.SessionID);
                Global.systemLog.Debug("redirect URL: " + url);
                response.Redirect(url);
            }
            else
            {
                Global.systemLog.Warn("SSO export request failed SMIWeb login");
                response.Status = "401 Unauthorized";
                return;
            }
        }
    }
}
