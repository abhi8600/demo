﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class LastLoadHistory
    {
        public DateTime TM;
        public Guid SPACE_ID;
        public string LOADGROUP;
        public int LOADNUMBER;
        public DateTime LOADDATE;
        public string PROCESSINGGROUPS;
    }
}