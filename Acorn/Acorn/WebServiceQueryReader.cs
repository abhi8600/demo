﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Net.Sockets;
using System.IO;
using System.Data;

namespace Acorn.DBConnection
{
    public class WebServiceQueryReader : QueryReader
    {
        private Acorn.TrustedService.QueryResult queryResult;
        private TcpClient client;
        private string[] colNames;
        private int?[] dataTypes;
        private List<string[]> lines;
        private int lineno = -1;
        private QueryConnection.DBType dbType;

        public WebServiceQueryReader(Acorn.TrustedService.QueryResult queryResult, QueryConnection.DBType dbType)
            : base(null, dbType) 
        {
            this.client = null;
            this.lines = new List<string[]>();
            this.queryResult = queryResult;
            this.dbType = dbType;
            this.dataTypes = queryResult.dataTypes;
            this.colNames = queryResult.columnNames;
            parseRows(queryResult);
        }

        // Overridden methods.
        public override void Close()
        {
        }

        public override DataTable GetSchemaTable()
        {
            return null;
        }

        public override bool GetBoolean(int column)
        {
            if (dbType == QueryConnection.DBType.Oracle)
                return (GetInt32(column) == 1);
            return false;
        }

        public override DateTime GetDateTime(int column)
        {
            return DateTime.Now;
        }

        public override string GetString(int column)
        {
            return ((string)lines[lineno][column]);
        }

        public override Guid GetGuid(int column)
        {
            return Guid.Empty;
        }

        public override bool IsDBNull(int column)
        {
            return ((string)lines[lineno][column]) == "";
        }

        public override int GetInt16(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return (int)Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override int GetInt32(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return (int)Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override long GetInt64(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return (long)Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override float GetFloat(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return float.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override Object GetValue(int column)
        {
            return ((string)lines[lineno][column]);
        }

        public override decimal GetDecimal(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return Decimal.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override double GetDouble(int column)
        {
            try
            {
                string s = lines[lineno][column].ToString();
                if (s.Length == 0)
                    return 0;
                return Double.Parse(s);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        public override string GetName(int column)
        {
            return colNames[column];
        }

        public override bool Read()
        {
            return ++lineno < lines.Count;
        }

        public override bool HasRows
        {
            get
            {
                if (client != null)
                    return lines != null && lines.Count > 0;
                return false;
            }
        }

        public override bool IsClosed
        {
            get
            {
                return false;
            }
        }

        public override int FieldCount
        {
            get
            {
                if (client != null && colNames != null)
                    return colNames.Length;
                return 0;
            }
        }

        public override string GetDataTypeName(int colnum)
        {
            return null;
        }

        // Private helper methods.
        private void parseRows(Acorn.TrustedService.QueryResult queryResult)
        {
            StringReader reader = new StringReader(queryResult.rows);
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                this.lines.Add(line.Split(new char[] { '|' }));
            }
        }
    }
}
