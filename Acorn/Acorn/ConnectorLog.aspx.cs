﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using log4net;
using log4net.Appender;
using System.Text.RegularExpressions;

namespace Acorn
{
    public partial class ConnectorLog : System.Web.UI.Page
    {
        private static string connectorLogDirName = "connectorlogs";
        private static string [] rejectLogLevel= {"DEBUG", "TRACE"};
        private static Regex[] rejectPatterns = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            string connectorName = Request["connectorname"];
            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);

            connectorName = Acorn.Utils.ConnectorUtils.getConnectorNameFromConnectorConnectionString(connectorName);

            getConnectorLogFile(u, sp, connectorName);
        }

        private void getConnectorLogFile(User u, Space sp, string connectorName)
        {
            string logdir = sp.Directory + Path.DirectorySeparatorChar + connectorLogDirName;
            if (!Directory.Exists(logdir))
            {
                Directory.CreateDirectory(logdir);
            }

            string filename = connectorName.ToLower() + ".log";

            string[] fnames = Directory.GetFiles(logdir, filename, SearchOption.TopDirectoryOnly);

            if (fnames == null || fnames.Length == 0)
            {
                noResult("There is no extraction log file for the space. The extraction log file will be available after the next extraction.");
                return;
            }

            //return log file
            string file = fnames[0];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(filename));
            Response.ContentType = "text/plain";

            FileInfo fi = new FileInfo(file);
            if (fi.Exists)
            {
                FileStream fs = null;
                StreamReader reader = null;
                try
                {
                    fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    reader = new StreamReader(fs);

                    string line = null;
                    string lowerid = sp.ID.ToString().ToLower();
                    string lowerschema = sp.Schema.ToString().ToLower();
                    int count = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!Response.IsClientConnected) // make sure we are still connected
                            return;
                        if (count > 0 && (count % 100) == 0)
                            Response.Flush();

                        // replace ID's with Names
                        line = line.Replace(sp.ID.ToString(), sp.Name);
                        line = line.Replace(lowerid, sp.Name);
                        line = line.Replace(sp.Schema, sp.Name);
                        line = line.Replace(lowerschema, sp.Name);

                        // output the line
                        outputLine(line, true);
                        count++;
                    }
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                    if (fs != null)
                        fs.Close();
                }
            }
            Response.Flush();
            Response.End();
        }

        private void noResult(string message)
        {
            Response.Clear();
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(Response.OutputStream);
                writer.Write(message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            Response.Flush();
            Response.End();
        }

        private void outputLine(String line, bool stripThreadInfo)
        {
            if (stripThreadInfo)
            {
                // strip first [....]
                int first = line.IndexOf('[');
                if (first != -1)
                {
                    int second = line.IndexOf(']', first);
                    if (first > 0 && second > first)
                    {
                        line = line.Remove(first, second - first + 2); // time [thread name] INFO  - message
                    }
                }
            }
            line = line + System.Environment.NewLine;
            byte[] array3 = Encoding.UTF8.GetBytes(line);
            Response.OutputStream.Write(array3, 0, array3.Length);
        }

        public static Regex[] getRejectLogLevelPattern()
	    {
            if (rejectPatterns == null)
            {
                rejectPatterns = new Regex[rejectLogLevel.Length];
                int patternIdx = 0;
                foreach (string rejectLogType in rejectLogLevel)
                {
                    Regex p = new Regex("(\\[)(.*?)(\\])[\\s](" + rejectLogType + ")[\\s]-"); //i.e. [<some_text>] DEBUG -
                    rejectPatterns[patternIdx] = p;
                    patternIdx++;
                }
            }
		
		    return rejectPatterns;
	    }

        public static void writePostExtractLogs(Space sp, FileInfo[] sourceFiles, string connectorName, string uid)
        {
            FileStream fs = null;
            StreamReader reader = null;
            StreamWriter writer = null;

            try
            {
                if (sourceFiles == null || sourceFiles.Length == 0 || uid == null)
                {
                    return;
                }

                string spConnLogdir = sp.Directory + Path.DirectorySeparatorChar + connectorLogDirName;
                if (!Directory.Exists(spConnLogdir))
                {
                    Directory.CreateDirectory(spConnLogdir);
                }

                string outfile = spConnLogdir + Path.DirectorySeparatorChar + connectorName.ToLower() + ".log";
                writer = new StreamWriter(outfile, true);
                Regex[] rejectLogPatterns = getRejectLogLevelPattern();

                foreach (FileInfo fi in sourceFiles)
                {
                    fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    reader = new StreamReader(fs);
                    string line = null;
                    int count = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (count > 0 && (count % 100) == 0)
                        {
                            writer.Flush();
                        }
                        if (!Regex.IsMatch(line, "(\\,)("+uid+")"))
                        {
                            continue;
                        }

                        bool writeline = true;
                        foreach (Regex rejectLogPattern in rejectLogPatterns)
                        {
                            if (rejectLogPattern.IsMatch(line))
                            {
                                writeline = false;
                                break;
                            }                                
                        }
                        if (writeline)
                        {
                            writer.WriteLine(line);
                            count++;
                        }
                    }
                    writer.Flush();
                }                
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        public static FileInfo[] getLogFiles(string logDirectory, string fileNamePattern, DateTime fromdate)
        {
            FileInfo[] files = new DirectoryInfo(logDirectory).GetFiles(fileNamePattern, SearchOption.TopDirectoryOnly).Where(file => file.LastWriteTime >= fromdate).ToArray();

            return files;
        }

        public static string getWebLogDir()
        {
            string webLogDirectory = null;
            FileInfo webLogFile = null;
            IAppender[] appenders = LogManager.GetRepository().GetAppenders();

            foreach (IAppender appender in appenders)
            {
                if (appender is RollingFileAppender)
                {
                    webLogFile = new FileInfo(((RollingFileAppender)appender).File);
                    break;
                }
            }

            if (webLogFile != null)
            {
                webLogDirectory = webLogFile.DirectoryName;
            }

            return webLogDirectory;
        }
    }
}