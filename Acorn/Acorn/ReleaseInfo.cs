﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class ReleaseInfo
    {
        public int ID;
        public string Name;
        public string RedirectUrl;
        public string BCRegExp;
        public bool enabled;
    }

    public class ReleaseDetails : GenericResponse
    {
        public ReleaseInfo[] ReleasesInfo;
    }
}
