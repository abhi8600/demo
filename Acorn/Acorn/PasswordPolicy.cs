﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Acorn
{
    public class PasswordPolicy
    {
        public static string HASHALGORITHM = "pbkdf2:hmac-sha1:10000";
        public static int MAXFAILEDLOGINATTEMPTS = 5;
        public static int FAILEDLOGINWINDOW = 10;
        private static HashSet<string> badpasswords = null;
        private static string BADPASSWORDFILE = "BadPasswords.txt";
        private static string disableTRUSTeMinimumPasswordPolicy = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["DisableTRUSTeMinimumPasswordPolicy"];

        public string description;
        public string regularExpression;
        public int minLength;
        public int maxLength;
        public bool containsMixedCase;
        public bool containsSpecial;
        public bool containsNumeric;
        public bool doesnotContainUsername;
        public bool changeOnFirstUse;

        public string hashAlgorithm = HASHALGORITHM;  // bcrypt:cost, pbkdf2:hmac-shaX:iterations
        public int maxFailedLoginAttempts = MAXFAILEDLOGINATTEMPTS;
        public int failedLoginWindow = FAILEDLOGINWINDOW; // minutes

        public int historyLength;  // 0 means no history
        public int expirationDays; // expiration of password in days

        public bool isValid(string password, string username, List<string> history)
        {
            if (password == null || password.Length == 0)
                return false; // NEVER allow empty passwords, no matter what the policy is
            if (regularExpression != null)
            {
                Match match = Regex.Match(password, regularExpression);
                if (!match.Success)
                    return false;
            }
            if (minLength > 0)
            {
                if (password.Length < minLength)
                    return false;
            }
            if (maxLength > 0)
                if (password.Length > maxLength)
                    return false;
            if (doesnotContainUsername)
            {
                if (password.Contains(username))
                    return false;
            }

            bool hasLowerCase = false;
            bool hasUpperCase = false;
            bool hasSpecial = false;
            bool hasNumeric = false;

            for (int i = 0; i < password.Length; i++)
            {
                char c = password[i];
                if (c >= 'a' && c <= 'z')
                    hasLowerCase = true;
                else if (c >= 'A' && c <= 'Z')
                    hasUpperCase = true;
                else if (c >= '0' && c <= '9')
                    hasNumeric = true;
                else
                    hasSpecial = true;
            }

            if (containsMixedCase && (!hasLowerCase || !hasUpperCase))
                return false;

            if (containsNumeric && !hasNumeric)
                return false;

            if (containsSpecial && !hasSpecial)
                return false;

            if (historyLength > 0 && history != null)
            {
                for (int i = 0; i < Math.Min(historyLength, history.Count); i++)
                {
                    if (Password.VerifyPassword(password, history[i], username))
                        return false;
                }
            }

            // For appliance customers who have a minimum password policy less strict than the TRUSTe minimum
            // - the TRUSTe minimum was required for Birst to get the TRUSTe Trusted Cloud Certification
            if (disableTRUSTeMinimumPasswordPolicy != null && bool.Parse(disableTRUSTeMinimumPasswordPolicy))
                return true;

            //
            // validate TRUSTe specific rules for passwords
            //
            // containing only repeated characters
            Regex pattern = new Regex(@"^(.)\1+$");
            if (pattern.IsMatch(password))
                return false;

            // containing only sequences (lexigraphic and keyboard)
            string lower = "abcdefghijklmnopqrstuvwxyz";
            string upper = lower.ToUpper();
            string numbers = "0123456789";
            string qwerty = "qwertyuiopasdfghjklzxcvbnm";
            if (lower.Contains(password) || upper.Contains(password) || numbers.Contains(password) || qwerty.Contains(password))
                return false;

            // compare against a list of known bad passwords
            if (badpasswords != null)
            {
                if (badpasswords.Contains(password))
                    return false;
            }

            return true;
        }

        //
        // load a list of bad passwords
        //
        public static bool loadBadPasswords(string path)
        {
            string filename = Path.Combine(path, BADPASSWORDFILE);
            if (!File.Exists(filename))
            {
                Global.systemLog.Info("No bad passwords file: " + filename);
                return false;
            }
            
            badpasswords = new HashSet<string>();
            Global.systemLog.Info("Loading bad passwords from '" + filename + "'");
            using (StreamReader reader = new StreamReader(filename))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("//") || line.Equals(""))
                        continue;
                    badpasswords.Add(line);
                }
            }
            return true;
        }
    }
}