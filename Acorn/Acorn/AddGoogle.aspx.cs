﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.sforce;
using System.Text;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services.Protocols;
using System.Data.Odbc;
using System.Net;

namespace Acorn
{
    public partial class AddGoogle : LocalizedPage
    {
        public string CUSTOM_DATE_FORMAT = "yyyy" + '/' + "MM" + '/' + "dd";

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string googleAnalyticsAPIKey = System.Web.Configuration.WebConfigurationManager.AppSettings["GoogleAnalyticsAPIKey"];
            if (googleAnalyticsAPIKey == null || googleAnalyticsAPIKey.Equals(""))
            {
                UsernameBox.Enabled = false;
                PasswordBox.Enabled = false;
                CredentialsNextButton.Enabled = false;
                GAKeyNotFoundLblId.Visible = true;
            }
            else
            {
                Util.needToChangePassword(Response, Session);

                User u = Util.validateAuth(Session, Response);
                Space sp = Util.validateSpace(Session, Response, Request);
                if (!Util.isSpaceAdmin(u, sp, Session))
                {
                    Global.systemLog.Warn("redirecting to home page due to not being the space admin (AddGoogle.aspx)");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                if (u.isFreeTrialUser)
                {
                    Global.systemLog.Warn("redirecting to home page due to being a free trial user (AddGoogle.aspx)");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                Util.setUpdatedHeader(Session, sp, u, Master);

                if (!IsPostBack)
                {
                    GoogleAnalyticsSettings settings = GoogleAnalyticsSettings.getSettings(sp.Directory);
                    if (settings != null && settings.profileTableId != null)
                    {
                        GoogleAnalyticsMultiView.SetActiveView(CurrentSettingsView);
                        TitleLabel.Text = settings.profile.title;
                        AccountNameLabel.Text = settings.profile.accountId;
                        PropertyIDLabel.Text = settings.profile.webPropertyId;
                    }
                    else
                    {
                        GoogleAnalyticsMultiView.SetActiveView(Authenticate);
                    }
                    Session["gasettings"] = settings;
                    UsernameBox.Text = settings.Username;
                    PasswordBox.Text = settings.Password;

                }
            }
        }

        protected void CredentialsNext(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            List<GoogleAnalyticsProfile> profiles = null;
            try
            {
                profiles = GoogleAnalyticsUtil.getProfiles(UsernameBox.Text, PasswordBox.Text);
            }
            catch (HttpException hex)
            {
                Global.systemLog.Warn("Google Analytics: " + hex.Message);
                LoginError.Visible = true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Google Analytics: " + ex.Message);
                LoginError.Visible = true;
            }
            if (profiles == null || profiles.Count == 0)
            {
                LoginError.Visible = true;
            }
            else
            {
                GoogleAnalyticsSettings settings = (GoogleAnalyticsSettings)Session["gasettings"];
                if (settings == null)
                    settings = GoogleAnalyticsSettings.getSettings(sp.Directory);
                settings.Username = UsernameBox.Text;
                settings.Password = PasswordBox.Text;
                if (settings.startDate == DateTime.MinValue)
                {
                    StartDateBox.Text = DateTime.Now.Year + "/01/01";
                    EndDateBox.Text = DateTime.Now.Year + "/12/31"; 
                }
                else
                {
                    StartDateBox.Text = settings.startDate.ToString(CUSTOM_DATE_FORMAT);
                    EndDateBox.Text = settings.endDate.ToString(CUSTOM_DATE_FORMAT);
                }
                GoogleAnalyticsMultiView.ActiveViewIndex = GoogleAnalyticsMultiView.Views.IndexOf(ViewSettings);
                SelectedProfile.Items.Clear();
                Session["gaprofiles"] = profiles;
                bool isFirstProfile = true;
                bool isProfileAlreadySelected = false;
                foreach (GoogleAnalyticsProfile gp in profiles)
                {
                    ListItem li = new ListItem(gp.title, gp.tableID);
                    if (settings.profileTableId != null)
                    {
                        li.Selected = settings.profileTableId == gp.tableID;
                        isProfileAlreadySelected = true;
                    }
                    else if (isFirstProfile && !isProfileAlreadySelected)
                    {
                        li.Selected = true;
                        isFirstProfile = false;
                    }
                    SelectedProfile.Items.Add(li);
                 }
            }
        }

        protected void SettingsNext(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            GoogleAnalyticsSettings settings = (GoogleAnalyticsSettings)Session["gasettings"];
            if (settings == null)
                Response.Redirect("~/AddGoogle.aspx");
            settings.profileTableId = SelectedProfile.SelectedValue;
            List<GoogleAnalyticsProfile> profiles = (List<GoogleAnalyticsProfile>)Session["gaprofiles"];
            GoogleAnalyticsProfile gp = null;
            foreach (GoogleAnalyticsProfile sgp in profiles)
            {
                if (sgp.tableID == settings.profileTableId)
                {
                    gp = sgp;
                    break;
                }
            }
            if (gp == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            settings.profile = gp;
            Session.Remove("gaprofiles");
            try
            {
                DateTime dateTime;
                string[] formats = {CUSTOM_DATE_FORMAT};
                bool isStardDateParsed = DateTime.TryParseExact(StartDateBox.Text, formats, null, System.Globalization.DateTimeStyles.None, out dateTime);
                settings.startDate = dateTime;

                bool isEndDateParsed = DateTime.TryParseExact(EndDateBox.Text, formats, null, System.Globalization.DateTimeStyles.None, out dateTime);
                settings.endDate = dateTime;
            }
            catch (Exception)
            {
                settings.startDate = DateTime.Now.Subtract(TimeSpan.FromDays(90));
                settings.endDate = DateTime.Now;
            }
            settings.integrateIntoRepository(maf, Server.MapPath("~"));
            settings.saveSettings(sp.Directory);
            Util.saveApplication(maf, sp, Session, null);
            Response.Redirect(Util.getHomePageURL(Session, Request));
        }

        protected void ModifyGoogle(object sender, EventArgs e)
        {
            GoogleAnalyticsMultiView.SetActiveView(Authenticate);
        }

        protected void DeleteGoogle(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            GoogleAnalyticsSettings settings = (GoogleAnalyticsSettings)Session["gasettings"];
            Session.Remove("gasettings");
            Session.Remove("gaprofiles");
            settings.deleteSettings(sp.Directory);
            settings.removeFromRepository(maf);
            Util.saveApplication(maf, sp, Session, null);
            Response.Redirect(Util.getHomePageURL(Session, Request));
        }
    }
}
