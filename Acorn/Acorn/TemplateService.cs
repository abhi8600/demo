﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using System.Data;
using System.IO;
using Performance_Optimizer_Administration;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{

    public class TemplateService
    {
        public static TemplateCategories getTemplateCategories(HttpSessionState session)
        {
            TemplateCategories response = new TemplateCategories();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            if (!canProceed(session))
            {                
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again.";
                return response;
            }
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            User u = (User)session["user"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = ResponseMessages.ERROR_MSG_SESSION_VARIABLE;
                return response;
            }
            DataTable categories = Database.getCategories(conn, schema, u.ID);
            ConnectionPool.releaseConnection(conn);
            List<TemplateCategory> list = new List<TemplateCategory>();
            if (categories.Rows.Count > 0)
            {
                foreach (DataRow row in categories.Rows)
                {
                    TemplateCategory category = new TemplateCategory();
                    category.CategoryName = (string)row["Category"];
                    category.Count = (int)row["Count"];
                    list.Add(category);                   
                }
            }
            response.Categories = list.ToArray();
            return response;
            
        }

        public static TemplateList getTemplates(HttpSessionState session, string categoryName, string sortexp, bool rate)
        {
            TemplateList response = new TemplateList();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            User u = (User)session["user"];                        
            Guid rating_user_id = Guid.Empty;
            if(rate)
            {
                rating_user_id = u.ID;
            }
            DataTable templates = Database.getTemplateList(conn, schema, categoryName, Guid.Empty, u.ID, rating_user_id, sortexp);
            ConnectionPool.releaseConnection(conn);
            setTemplateDetails(u, templates, response);
            return response;
        }

        private static void setTemplateDetails(User u, DataTable templates, TemplateList response)
        {
            List<TemplateDetail> template = new List<TemplateDetail>();

            if (templates.Rows.Count > 0)
            {
                foreach (DataRow row in templates.Rows)
                {
                    TemplateDetail tempDetail = new TemplateDetail();
                    tempDetail.TemplateID = ((Guid)row["TEMPLATE_ID"]).ToString();
                    tempDetail.CreaterID = ((Guid)row["CREATOR_ID"]).ToString();
                    tempDetail.Name = (string)row["Name"];
                    tempDetail.Category = (string)row["Category"];
                    tempDetail.Comments = (string)row["Comments"];
                    tempDetail.CreateDate = (DateTime)row["CreateDate"];
                    tempDetail.Automatic = (bool)row["Automatic"];
                    tempDetail.Private = (bool)row["Private"];
                    tempDetail.NumUses = (int)row["NumUses"];
                    if (row["AvgRating"] != null && row["AvgRating"].ToString().Length != 0)
                    {
                        tempDetail.AvgRating = (float)row["AvgRating"];
                    }
                    if (row["UserRating"] != null && row["UserRating"].ToString().Length != 0)
                    {
                        tempDetail.UserRating = (float)row["UserRating"];
                    }
                    // logic for if the template could be deleted depending on who is retrieving the list
                    if (tempDetail.CreaterID == u.ID.ToString())
                    {
                        tempDetail.CanDelete = true;
                    }
                    else
                    {
                        tempDetail.CanDelete = false;
                    }
                    template.Add(tempDetail);
                }
            }

            response.TemplateDetails = template.ToArray();
        }

        public static GenericResponse deleteTemplate(HttpSessionState session, string templateID)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            Database.removeTemplate(conn, schema, new Guid(templateID));
            ConnectionPool.releaseConnection(conn);           
            return response;
        }

        public static GenericResponse changeRating(HttpSessionState session, string templateID, int rating)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            User u = (User)session["user"];
            Database.addRating(conn, schema, new Guid(templateID), u.ID, rating);
            ConnectionPool.releaseConnection(conn);

            
            return response;
        }

        public static GenericResponse getAttachmentProcessOutput(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }
            List<string> msgs = new List<string>();
            if (session["ErrorOutput"] != null)
            {
                string errorMsg = (string)session["ErrorOutput"];
                msgs.Add(errorMsg);
                session.Remove("ErrorOutput");
            }

            response.other = msgs.ToArray();
            return response;
        }

        public static GenericResponse removeAttachment(HttpSessionState session, string fileName)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }

            Space sp = Util.getSessionSpace(session);
            if(Directory.Exists(sp.Directory + "\\temp"))
            {
                string filePath = sp.Directory + "\\temp\\" + fileName;
                if(File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            return response;
        }
        /*
        public static GenericResponse sendAttachment(HttpSessionState session, byte[] fileBytes, string fileName )
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            Space sp = Util.getSessionSpace(session);
            if (!canProceed(session) || sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Session Initialization Problem. Please login again";
                return response;
            }

            
            if (fileName.Length == 0)
                return response;

            if (fileBytes.Length > MAX_ATTACHMENT_SIZE)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Exceeded Maximum Allowed Attachment Size";
                return response;
            }

            if (!Directory.Exists(sp.Directory + "\\temp"))
                Directory.CreateDirectory(sp.Directory + "\\temp");
            string fname = sp.Directory + "\\temp\\" + fileName;
            BinaryWriter writer = new BinaryWriter(new FileStream(fname, FileMode.Create));
            writer.Write(fileBytes);
            writer.Close();
            DataTable dt = getSessionAttachTable(session);            
            DataRow dr = dt.NewRow();
            dr[0] = fileName;
            dt.Rows.Add(dr);
            populateAttachmentNames(session, response);            
            return response;
        }*/


        public static GenericResponse getAttachmentNames(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }

            Space sp = Util.getSessionSpace(session);
            if (Directory.Exists(sp.Directory + "\\temp"))
            {
                string[] files = Directory.GetFiles(sp.Directory + "\\temp");
                if (files != null && files.Length > 0)
                {
                    List<string> names = new List<string>();
                    foreach (string file in files)
                    {
                        names.Add(Path.GetFileName(file));
                    }

                    response.other = names.ToArray();
                }

            }
            return response;
        }

        public static GenericResponse createNewTemplate(HttpSessionState session, string categoryName, string templateName, string templateComments, bool isPrivate)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            if (!canProceed(session))
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue.Please login again.";
                return response;
            }
            string contentdir = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];
            string templatedir = contentdir + "\\templates";
            // Create parent template directory if necessary
            if (!Directory.Exists(templatedir))
                Directory.CreateDirectory(templatedir);
            User u = (User)session["user"];
            Template t = new Template(u.ID, templateName, categoryName);
            t.Comments = templateComments;
            t.Private = isPrivate;
            templatedir += "\\" + t.ID;
            // Create template directories
            if (!Directory.Exists(templatedir))
                Directory.CreateDirectory(templatedir);
            if (!Directory.Exists(templatedir + "\\catalog"))
                Directory.CreateDirectory(templatedir + "\\catalog");
            if (!Directory.Exists(templatedir + "\\catalog\\shared"))
                Directory.CreateDirectory(templatedir + "\\catalog\\shared");
            if (!Directory.Exists(templatedir + "\\catalog\\private"))
                Directory.CreateDirectory(templatedir + "\\catalog\\private");
            if (!Directory.Exists(templatedir + "\\attachments"))
                Directory.CreateDirectory(templatedir + "\\attachments");
            if (!Directory.Exists(templatedir + "\\custom-subject-areas"))
                Directory.CreateDirectory(templatedir + "\\custom-subject-areas");
            if (!Directory.Exists(templatedir + "\\" + Util.DCCONFIG_DIR))
                Directory.CreateDirectory(templatedir + "\\" + Util.DCCONFIG_DIR);
            Space sp = (Space)session["space"];
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            // build and save application first
            Util.buildApplication(maf, schema, DateTime.Now, -1, null, sp, null);
            Util.saveApplication(maf, sp, session, u);
            // Copy repository
            if (File.Exists(sp.Directory + "\\repository_dev.xml"))
                Util.FileCopy(sp.Directory + "\\repository_dev.xml", templatedir + "\\repository_dev.xml");
            // Copy data conductor config
            string dcTargetDir = templatedir + "\\" + Util.DCCONFIG_DIR;
            if (Directory.Exists(sp.Directory + "\\" + Util.DCCONFIG_DIR))
            {
                DirectoryInfo source = new DirectoryInfo(sp.Directory + "\\" + Util.DCCONFIG_DIR);
                FileInfo[] sfInfo = source.GetFiles();
                if (sfInfo != null)
                {
                    foreach (FileInfo fi in sfInfo)
                    {
                        Util.FileCopy(source.FullName + "\\" + fi.Name, dcTargetDir + "\\" + fi.Name);
                        Util.removeDCAuthenticationInformation(dcTargetDir + "\\" + fi.Name);
                    }
                }
            }
            else if (File.Exists(sp.Directory + "\\" + Util.DCCONFIG_FILE_NAME))
            {
                Util.FileCopy(sp.Directory + "\\" + Util.DCCONFIG_FILE_NAME, dcTargetDir + "\\" + Util.DCCONFIG_FILE_NAME);
                Util.removeDCAuthenticationInformation(dcTargetDir + "\\" + Util.DCCONFIG_FILE_NAME);
                //move space/dcconfig.xml to space/dcconfig directory
                BirstConnectUtil.moveFileToDirectory(sp, Util.DCCONFIG_DIR, Util.DCCONFIG_FILE_NAME);
            }
            // Copy SFDC config
            if (File.Exists(sp.Directory + "\\sforce.xml"))
            {
                SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                settings.username = null;
                settings.password = null;
                settings.saveAuthentication = false;
                settings.saveSettings(templatedir);
            }
            // Copy catalog
            if (Directory.Exists(sp.Directory + "\\catalog\\shared"))
                Util.copyDir(sp.Directory + "\\catalog\\shared", templatedir + "\\catalog\\shared");
            // Copy attachments
            if (Directory.Exists(sp.Directory + "\\temp"))
                Util.copyDir(sp.Directory + "\\temp", templatedir + "\\attachments");
            // Copy custom subject areas
            if(Directory.Exists(sp.Directory + "\\custom-subject-areas"))
                Util.copyDir(sp.Directory + "\\custom-subject-areas", templatedir + "\\custom-subject-areas");
            QueryConnection conn = ConnectionPool.getConnection();
            Database.addTemplate(conn, (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"], t);
            ConnectionPool.releaseConnection(conn);
            // Cleanup temp directory
            if (Directory.Exists(sp.Directory + "\\temp"))
                Directory.Delete(sp.Directory + "\\temp", true);
            using (log4net.ThreadContext.Stacks["itemid"].Push(t.ID.ToString()))
            {
                Global.userEventLog.Info("NEWTEMPLATE");
            }
            session.Remove("attachtable");            
            return response;
        }

        private static bool canProceed(HttpSessionState session)
        {
            bool success = false;
            User u = (User)session["User"];
            if (u != null)
            {
                success = true;
            }
            return success;
        }
    }
}