﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.IO.Compression;

namespace Acorn
{
    public partial class QueryLog : System.Web.UI.Page
    {
        private static string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            string t1 = Request["minutes"]; // XXX not currently used, defaulting to 60 minutes

            string jsessionid = null; 
            DateTime end = DateTime.Now;
            int increment = -60;
            if (t1 != null && t1.Length >= 0)
            {
                if (!Int32.TryParse(t1, out increment))
                {
                    increment = -60;
                }
            }
            if (increment > 0)
                increment = -increment;

            DateTime start = end.AddMinutes(increment);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);

            if (u.isFreeTrialUser)
            {
                Response.Write("Not Supported for the Free Trial");
                return;
            }

            if (!DefaultUserPage.isAdhocPermitted(Session, u)) // only those with designer access should see this
            {
                Response.StatusCode = 400;
                return;
            }

            string logdir = null;
            string logfileroot = null;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            TrustedService.getLogFilenameResponse resp = ts.getLogFilename();
            if (resp != null)
            {
                string filename = resp.@return;
                int lastslash = filename.LastIndexOf('\\');
                logdir = filename.Substring(0, lastslash);
                logfileroot = filename.Substring(lastslash + 1, filename.Length - 14 - lastslash - 1); // strip off date and .log
            }
            else
            {
                Global.systemLog.Warn("Could not find logdir location");
            }

            if (Request.Cookies["JSESSIONID"] != null)
            {
                if (Request.Params["ignoreSession"] == null)
                    jsessionid = Request.Cookies["JSESSIONID"].Value;
            }

            bool full = u.RepositoryAdmin || u.OperationsFlag; // do not sanitize the log file
            processLogFiles(u, sp, jsessionid, start, end, logdir, logfileroot, full);
        }

        private void processLogFiles(User u, Space sp, string jsessionid, DateTime start, DateTime end, string logdir, string logfileroot, bool full)
        {
            List<string> fnames = new List<string>();
            if (!Directory.Exists(logdir))
                noResult("Log directory does not exist.  Please try again.  If this persists, contact support.");

            // most recent file (get log file(s) for date(s)
            string[] files = Directory.GetFiles(logdir, logfileroot + "*.log", SearchOption.TopDirectoryOnly);
            foreach (string s in files)
            {
                if (s.Length < 14)
                    continue;
                string temp = s.Substring(s.Length - 14, 10);
                DateTime fdate = DateTime.Now;
                if (!DateTime.TryParse(temp, out fdate))
                {
                    fdate = DateTime.Now;
                }
                if (fdate >= start.Date && fdate <= end.Date)
                {
                    fnames.Add(s);
                }
            }
            if (fnames.Count == 0)
                noResult("No matching log files available.");

            processLogFile(u, sp, jsessionid, start, end, fnames, full);
        }

        private void noResult(string message)
        {
            Response.Clear();
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(Response.OutputStream);
                writer.Write(message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            Response.Flush();
            Response.End();
        }

        private string fixupline(string line, string lowerSpaceId, string spaceId, string replacement, string schema)
        {
            if (line.ToLower().IndexOf(lowerSpaceId) >= 0)
            {
                line = line.Replace(spaceId, replacement);
                line = line.Replace(lowerSpaceId, replacement);
            }
            line = line.Replace(schema, replacement);
            return line;
        }

        private static string INFOSTR = "] INFO  - ";

        private void processLogFile(User u, Space sp, string jsessionid, DateTime start, DateTime end, List<string> fnames, bool full)
        {
            // string name = "birstquerylog.txt" + (full ? ".gz" : "");
            string name = "birstquerylog.txt";

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(name));
            // Response.ContentType = full ? "application/x-gzip" : "text/plain";
            Response.ContentType = "text/plain";

            StreamWriter writer = null;
            /*
            GZipStream gzStream = null;

            if (full)
            {
                gzStream = new GZipStream(Response.OutputStream, CompressionMode.Compress);
                writer = new StreamWriter(gzStream);
            }
            else
            {
                writer = new StreamWriter(Response.OutputStream);
            }
             */
            writer = new StreamWriter(Response.OutputStream);

            writer.WriteLine("Query Log for user: " + u.Username + ", space: " + sp.Name + " (" + sp.ID + "), session: " + jsessionid);
            writer.WriteLine("");

            int lines = 0;

            foreach (string filename in fnames)
            {
                FileInfo fi = new FileInfo(filename);
                if (fi.Exists)
                {
                    FileStream fs = null;
                    StreamReader reader = null;
                    try
                    {
                        fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        reader = new StreamReader(fs);

                        string line = null;
                        string spaceId = sp.ID.ToString();
                        string lowerSpaceId = spaceId.ToLower();
                        string key = '[' + u.Username + ',' + lowerSpaceId + ',' + (jsessionid == null ? "" : (jsessionid + ']'));
                        bool lastwrite = false; // if true, allow continutation lines
                        string replacement = '[' + sp.Name + ']';

                        while ((line = reader.ReadLine()) != null)
                        {
                            if (!Response.IsClientConnected) // make sure we are still connected
                                return;
                            if ((++lines % 100) == 0)
                            {
                                writer.Flush();
                                Response.Flush();
                            }
                            DateTime logtime;
                            // skip lines that don't have the datetime header
                            if (line.Length < 20)
                            {
                                if (lastwrite)
                                {
                                    if (!full)
                                        line = fixupline(line, lowerSpaceId, spaceId, replacement, sp.Schema);
                                    if (line.StartsWith("]"))
                                        line = line.Substring(1);
                                    writer.WriteLine(line);
                                }
                                continue;
                            }
                            // skip records outside of the datetime range
                            string dt = line.Substring(0, 19); // get the date and time from the log message - 2011-03-18 13:24:56
                            if (!DateTime.TryParse(dt, out logtime))
                            {
                                if (lastwrite)
                                {
                                    if (!full)
                                        line = fixupline(line, lowerSpaceId, spaceId, replacement, sp.Schema);
                                    if (line.StartsWith("]"))
                                        line = line.Substring(1);
                                    writer.WriteLine(line);
                                }
                                continue;
                            }
                            // before we need to process
                            if (logtime < start)
                            {
                                lastwrite = false;
                                continue;
                            }
                            // no reason to go further
                            if (logtime > end)
                            {
                                break;
                            }
                            // filter on user and space
                            if (line.IndexOf(key) < 0)
                            {
                                lastwrite = false;
                                continue;
                            }

                            // include all information for full dump
                            if (full)
                            {
                                writer.WriteLine(line);
                                lastwrite = true;
                                continue;
                            }

                            // filter for regular users (only INFO log messages and continuations)
                            if (line.IndexOf(INFOSTR) < 0)
                            {
                                lastwrite = false;
                                continue;
                            }

                            // replace detailed space id and schema names with logical values
                            line = fixupline(line, lowerSpaceId, spaceId, replacement, sp.Schema);

                            // write out the sanitized the log message
                            int index = line.IndexOf(INFOSTR);
                            writer.WriteLine(line.Substring(0, 28) + '\t' + line.Substring(index + INFOSTR.Length));
                            lastwrite = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Debug(ex, ex);
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                        if (fs != null)
                            fs.Close();
                    }
                }
            }
            writer.Close();
            Response.Flush();
            Response.End();
        }
    }
}
