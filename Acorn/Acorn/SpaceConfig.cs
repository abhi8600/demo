﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class SpaceConfig
    {
        public int Type;
        public string URL;
        public string LocalURL;
        public string DatabaseConnectString;
        public string DatabaseType;
        public string DatabaseDriver;
        public string DatabaseLoadDir;
        public string AdminUser;
        public string AdminPwd;
        public string EngineCommand;
        public string DeleteDataCommand;
        public string QueryDatabaseConnectString;
        public string QueryDatabaseType;
        public string QueryDatabaseDriver;
        public string QueryUser;
        public string QueryPwd;
        public string QueryConnectionName;

        public string awsAccessKeyId;
        public string awsSecretKey;

        public string Name;
        public string Description;

        public string dbTimeZone;
        public int regionId;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            SpaceConfig other = (SpaceConfig)obj;
            if (Type != other.Type) return false;
            if (!EqualsWithNull(URL, other.URL)) return false;
            if (!EqualsWithNull(LocalURL, other.LocalURL)) return false;
            if (!EqualsWithNull(DatabaseConnectString, other.DatabaseConnectString)) return false;
            if (!EqualsWithNull(DatabaseType, other.DatabaseType)) return false;
            if (!EqualsWithNull(DatabaseDriver, other.DatabaseDriver)) return false;
            if (!EqualsWithNull(DatabaseLoadDir, other.DatabaseLoadDir)) return false;
            if (!EqualsWithNull(AdminUser, other.AdminUser)) return false;
            if (!EqualsWithNull(AdminPwd, other.AdminPwd)) return false;
            if (!EqualsWithNull(EngineCommand, other.EngineCommand)) return false;
            if (!EqualsWithNull(DeleteDataCommand, other.DeleteDataCommand)) return false;
            if (!EqualsWithNull(QueryDatabaseConnectString, other.QueryDatabaseConnectString)) return false;
            if (!EqualsWithNull(QueryDatabaseType, other.QueryDatabaseType)) return false;
            if (!EqualsWithNull(QueryDatabaseDriver, other.QueryDatabaseDriver)) return false;
            if (!EqualsWithNull(QueryUser, other.QueryUser)) return false;
            if (!EqualsWithNull(QueryPwd, other.QueryPwd)) return false;
            if (!EqualsWithNull(QueryConnectionName, other.QueryConnectionName)) return false;
            if (!EqualsWithNull(awsAccessKeyId, other.awsAccessKeyId)) return false;
            if (!EqualsWithNull(awsSecretKey, other.awsSecretKey)) return false;
            if (!EqualsWithNull(dbTimeZone, other.dbTimeZone)) return false;
            if (regionId > 0 && other.regionId > 0 && regionId != other.regionId) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private static Boolean EqualsWithNull(Object one, Object two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (!one.Equals(two)) return false;
            return true;
        }
    }
}
