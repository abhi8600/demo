﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Performance_Optimizer_Administration;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Collections;
using Acorn.Background;
using System.Xml.Linq;

namespace Acorn
{
    public class OldedbWrapper
    {
        private static int BACKGROUND_UPLOADER_TIMEOUT = -1;
        private static int DEFAULT_BACKGROUND_UPLOADER_TIMEOUT = 60; // minutes

        private static string UploaderEngineCommand;
        private static string ERROR_STR = "Error";
        private static string WARNING_STR = "Warning";
        private static char[] currencyprefixes = new char[] { '$', '£', '€', '¥' };

        public Space sp;
        private bool filterExcelRows;
        private string spaceDirectory;
        private string dir;
        private string name;        
        private string oleDBConnectString;
        private List<string[]> restrictions;
        private bool pruneBlank;
        private DataTable errorTable;
        private HashSet<string> processedFiles;
        private int beginSkip;
        // adding endSkip, but it doesn't make sense.  What is the last line in the file?
        private int endSkip;

        public OldedbWrapper(bool filterExcelRows, string spaceDirectory, string dir, string name, string oleDBConnectString, List<string[]> restrictions, bool pruneBlank, DataTable errorTable, HashSet<string> processedFiles, int beginSkip, int endSkip)
        {            
            this.filterExcelRows = filterExcelRows;
            this.spaceDirectory = spaceDirectory;
            this.dir = dir;
            this.name = name;
            this.oleDBConnectString = oleDBConnectString;
            this.restrictions = restrictions;
            this.pruneBlank = pruneBlank;
            this.errorTable = errorTable;
            this.processedFiles = processedFiles;
            this.beginSkip = beginSkip;
            this.endSkip = endSkip;
        }

        public Dictionary<string, ScanSummaryResult> process()
        {
            Dictionary<string, ScanSummaryResult> response = new Dictionary<string, ScanSummaryResult>();
            ScanSummaryList scanSummaryList = null;            
            if (Global.UseBackGroundUploader)
            {
                scanSummaryList = spawnBackgroundUploaderAndWait();                
            }
            else
            {               
                scanSummaryList = processOleDBFile();
            }
             
            if (scanSummaryList != null && scanSummaryList.scanSummaryList != null && scanSummaryList.scanSummaryList.Length > 0)
            {
                ScanSummaryResult[] scanSummaryResults = scanSummaryList.scanSummaryList;
                if (scanSummaryResults != null && scanSummaryResults.Length > 0)
                {
                    foreach (ScanSummaryResult summarySr in scanSummaryResults)
                    {
                        response.Add(summarySr.cleanName, summarySr);
                    }
                }
            }
            return response;
        }

        private void cleanUpOldScanSummaryResults()
        {   
            try
            {
                string pattern = "*" + ScanSummaryList.getScanResultsFileSuffix();
                string[] matchedFiles = Directory.GetFiles(Path.Combine(sp.Directory, "data"), pattern, SearchOption.TopDirectoryOnly);

                foreach (string fileName in matchedFiles)
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.Exists)
                    {
                        Global.systemLog.Debug("Cleaning up scanResults file : " + fileName);
                        fi.Delete();
                    }                    
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Internal Error : deleting the old scan status files - " + ex.Message, ex);
                DataRow errorRow = errorTable.NewRow();
                errorRow[0] = name;
                errorRow[1] = ERROR_STR;
                errorRow[2] = "Internal Error";
                errorTable.Rows.Add(errorRow);
                throw ex;
            }
        }

        private int getBackgroundUploadTimeout()
        {            
            if (BACKGROUND_UPLOADER_TIMEOUT < 0)
            {
                int timeout = DEFAULT_BACKGROUND_UPLOADER_TIMEOUT;
                try
                {
                    string timeoutString = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BackgroundUploaderTimeout"];
                    if (timeoutString != null && timeoutString.Length > 0)
                    {
                        timeout = int.Parse(timeoutString);
                    }
                }
                catch (Exception ex)
                {
                    timeout = DEFAULT_BACKGROUND_UPLOADER_TIMEOUT;
                    Global.systemLog.Warn("Error in getting background uploader timeout from web.config file : Using default value " + DEFAULT_BACKGROUND_UPLOADER_TIMEOUT, ex);
                }
                BACKGROUND_UPLOADER_TIMEOUT = timeout * 60 * 1000; // in milliseconds
            }
            return BACKGROUND_UPLOADER_TIMEOUT;
        }

        private string getUploaderEngineCommand()
        {
            if (UploaderEngineCommand == null)
            {                
                try
                {
                    string cmd = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UploaderEngineCommand"];
                    if (cmd != null && cmd.Trim().Length > 0)
                    {
                        UploaderEngineCommand = cmd;
                    }
                    else
                    {
                        throw new Exception("No UploaderEngineCommand key found in Web.config file");
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error in getting uploader engine command from web.config file : UploaderEngineCommand ", ex);
                }
            }
            return UploaderEngineCommand;
        }

        private ScanSummaryList spawnBackgroundUploaderAndWait()
        {
            try
            {
                cleanUpOldScanSummaryResults();
                OleDBBackgroundModule bm = new OleDBBackgroundModule(sp, filterExcelRows, spaceDirectory, dir, name, oleDBConnectString, restrictions, pruneBlank, errorTable, processedFiles, getUploaderEngineCommand(), beginSkip, endSkip);
                BackgroundTask bt = new BackgroundTask(bm, getBackgroundUploadTimeout());
                BackgroundProcess.startModule(bt);
                bt.waitUntilFinished();
                string scanResultsFileName = ScanSummaryList.getScanResultsFileName(spaceDirectory, name);
                if (File.Exists(scanResultsFileName))
                {
                    ScanSummaryList list = ScanSummaryList.deserializeScanResults(scanResultsFileName);
                    // merge errorTable and processed files
                    if (list.errors != null && list.errors.Length > 0)
                    {
                        foreach (string[] error in list.errors)
                        {
                            DataRow dataRow = errorTable.NewRow();
                            dataRow[0] = error[0];
                            dataRow[1] = error[1];
                            dataRow[2] = error[2];
                            errorTable.Rows.Add(dataRow);
                        }
                    }

                    if (list.processedFiles != null && list.processedFiles.Count > 0)
                    {
                        foreach (string prFile in list.processedFiles)
                        {
                            processedFiles.Add(prFile);
                        }
                    }
                    return list;
                }
                if (bm.getStatus().code != Status.StatusCode.Complete)
                {
                    DataRow dataRow = errorTable.NewRow();
                    dataRow[0] = name;
                    dataRow[1] = ERROR_STR;
                    dataRow[2] = "Internal Error";
                    errorTable.Rows.Add(dataRow);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while uploading through background uploader " + sp.ID, ex);
                errorTable.Rows.Add(name, ERROR_STR, "Internal Error");
            }
            return null;
        }

        public ScanSummaryList processOleDBFile()
        {
            Global.systemLog.Debug("processOleDBFile with '" + name + "' and connect string '" + oleDBConnectString + "'");

            Dictionary<string, ScanResults> createdFiles = new Dictionary<string, ScanResults>();
            string fname = Path.Combine(dir, name);
            Util.validateFileLocation(dir, fname);
            OleDbConnection conn = null;
            bool excel = name.EndsWith(".xls") || name.EndsWith(".xlsx");
            try
            {
                conn = new OleDbConnection(oleDBConnectString);
                conn.Open();
                Global.systemLog.Debug("processOleDBFile opened connection with '" + fname + "' and connect string '" + oleDBConnectString + "'");

                DataTable dt = conn.GetSchema("Tables");
                Global.systemLog.Debug("processOleDBFile found " + ((dt != null && dt.Rows != null) ? dt.Rows.Count : 0) + " tables");
                bool wellFormed = true;
                List<string> tnames = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    string tname = (string)dr["TABLE_NAME"];
                    Global.systemLog.Debug("processOleDBFile: checking table " + tname);
                    if (excel)
                    {
                        bool worksheet = false;
                        // Ignore print areas and named ranges for excel
                        if (tname[0] == '\'' && tname.EndsWith("$'"))
                            worksheet = true;
                        else if (tname[0] != '\'' && tname.EndsWith("$") && !tname.Contains(' '))
                            worksheet = true;
                        if (!worksheet)
                            continue;
                    }
                    bool valid = true;
                    if (restrictions != null)
                    {
                        foreach (string[] restriction in restrictions)
                        {
                            if (((string)dr[restriction[0]]) != restriction[1])
                            {
                                valid = false;
                                break;
                            }
                        }
                    }
                    if (!valid)
                        continue;
                    tnames.Add(tname);
                }
                foreach (string tname in tnames)
                {
                    Global.systemLog.Debug("processOleDBFile: processing table: " + tname);
                    OleDbCommand cmd = null;
                    OleDbDataReader reader = null;
                    try
                    {
                        try
                        {
                            DataTable columnTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, tname });
                            if (columnTable.Rows.Count <= 1)
                            {
                                Global.systemLog.Info("skipping " + tname + " - fewer than 2 columns");
                                DataRow edr = errorTable.NewRow();
                                edr[0] = tname;
                                edr[1] = WARNING_STR;
                                edr[2] = "skipping - fewer than 2 columns";
                                errorTable.Rows.Add(edr);
                                continue;
                            }
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("processOleDBFile: getOldDbSchemaTable for columns " + tname + " threw an exception: " + ex.Message);
                            continue;
                        }
                        DataTable keyTable = null;
                        try
                        {
                            keyTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Primary_Keys, new object[] { null, null, tname });
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("processOleDBFile: getOldDbSchemaTable for primary keys" + tname + " threw an exception: " + ex.Message);
                            continue;
                        }
                        List<string> keyCols = new List<string>();
                        List<int> keyIndices = new List<int>();
                        foreach (DataRow kdr in keyTable.Rows)
                            keyCols.Add((string)kdr[3]);

                        cmd = conn.CreateCommand();
                        cmd.CommandText = "SELECT * FROM [" + tname + "]";
                        Global.systemLog.Debug("processOleDBFile: " + cmd.CommandText);

                        reader = cmd.ExecuteReader();
                        string cleanName = Regex.Replace(tname, @"[^\w\. \-_\u0080-\uFFCF]", "");
                        string tfname = null;
                        int count = 0;
                        string curCleanName = cleanName;
                        while (processedFiles.Contains(cleanName))
                        {
                            cleanName = curCleanName + (count == 0 ? "" : count.ToString());
                            count++;
                        }
                        // If only one worksheet, then use the filename
                        if (excel && tnames.Count == 1)
                        {
                            if (name.EndsWith(".xls"))
                                cleanName = name.Substring(0, name.Length - 4);
                            else
                                cleanName = name.Substring(0, name.Length - 5);
                        }
                        // if cleanname contains leading or trailing spaces, need to replace them with an "_"
                        // otherwise mismatch of names takes place between datasourcename and sourcefile's name and 
                        // datasource becomes inaccessible from Flex UI
                        string trimmedCleanName = cleanName.Trim();
                        if (!cleanName.Equals(trimmedCleanName))
                        {
                            int leading_sp = cleanName.IndexOf(trimmedCleanName);
                            for (int i = 0; i < leading_sp; i++) // replace leading spaces with "_"
                            {
                                trimmedCleanName = "_" + trimmedCleanName;
                            }
                            while (cleanName.Length > trimmedCleanName.Length) // replace trailing spaces with "_"
                            {
                                trimmedCleanName = trimmedCleanName + "_";
                            }
                        }
                        cleanName = trimmedCleanName;
                        processedFiles.Add(cleanName);
                        tfname = dir + "\\" + cleanName + ".txt";
                        if (File.Exists(tfname + ".unfiltered"))
                            File.Delete(tfname + ".unfiltered");
                        ScanResults sr = scanfile(reader, tfname, pruneBlank, excel, keyCols, keyIndices);                        
                        if (excel && sr.rows == 0 && reader.FieldCount == 1)
                        {
                            File.Delete(tfname);
                        }
                        else
                        {
                            if (excel)
                            {
                                // Calculate null probabilities
                                sr.probs = new double[sr.nullCounts.Length];
                                for (int i = 0; i < sr.nullCounts.Length; i++)
                                {
                                    sr.probs[i] = ((double)sr.nullCounts[i]) / sr.rows;
                                }
                                sr.nullCutoff = getNullCutoff(sr);
                                for (int row = 0; row < sr.nullList.Count; row++)
                                {
                                    BitArray ba = sr.nullList[row];
                                    if (!sr.isLikelyPatternWithFilter(ba))
                                    {
                                        wellFormed = false;
                                        sr.wellFormed = false;
                                        break;
                                    }
                                }
                                // Remove any blank columns
                                bool hasBlankCols = false;
                                for (int i = 0; i < sr.nullCounts.Length; i++)
                                {
                                    if (sr.nullCounts[i] == sr.rows)
                                    {
                                        hasBlankCols = true;
                                        break;
                                    }
                                }
                                if (hasBlankCols || (!wellFormed && filterExcelRows))
                                {
                                    StreamReader ereader = null;
                                    StreamWriter fwriter = null;
                                    bool keepUnfiltered = false;
                                    try
                                    {
                                        ereader = new StreamReader(tfname);
                                        fwriter = new StreamWriter(tfname + ".tmp");

                                        string line = null;
                                        int rowNum = 0;
                                        while ((line = SourceFiles.readLine(ereader, null, false, "\"", 0, false, false)) != null)
                                        {
                                            if (line.Length == 0)
                                                continue;                                                
                                            if (!wellFormed && filterExcelRows)
                                            {
                                                BitArray ba = sr.nullList[rowNum++];
                                                if (!sr.isLikelyPattern(ba))
                                                {
                                                    keepUnfiltered = true;
                                                    sr.defectiveRows.Add(rowNum - 1);
                                                    continue;
                                                }
                                            }
                                            string[] cols = line.Split(new char[] { '|' });
                                            bool first = true;
                                            for (int i = 0; i < sr.nullCounts.Length; i++)
                                            {
                                                if (sr.nullCounts[i] < sr.rows)
                                                {
                                                    if (!first)
                                                        fwriter.Write('|');
                                                    else
                                                        first = false;
                                                    fwriter.Write(cols[i]);
                                                }
                                            }
                                            fwriter.WriteLine();
                                        }
                                    }
                                    finally
                                    {
                                        if (fwriter != null)
                                            fwriter.Close();
                                        if (ereader != null)
                                            ereader.Close();
                                    }
                                    if (!wellFormed && filterExcelRows && keepUnfiltered)
                                        File.Move(tfname, tfname + ".unfiltered");
                                    else
                                        File.Delete(tfname);
                                    File.Move(tfname + ".tmp", tfname);
                                }
                            }
                            sr.keys = keyIndices.ToArray();
                            createdFiles.Add(cleanName + ".txt", sr);
                        }
                    }
                    catch (Exception e)
                    {
                        Global.systemLog.Error("processOleDBFile: ExecuteReader on '" + cmd.CommandText + "' threw an exception: " + e.Message);
                        DataRow edr = errorTable.NewRow();
                        edr[0] = tname;
                        edr[1] = ERROR_STR;
                        if (excel)
                            edr[2] = "Could not load worksheet - please make sure it is in tabular format (e.g. no extra rows/colums)";
                        else
                            edr[2] = "Unable to process data - please examine the source for possible format issues";
                        errorTable.Rows.Add(edr);
                        continue;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                        if (cmd != null)
                            cmd.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("processOleDBFile threw an exception: " + e.Message, e);
                if (e.InnerException != null)
                {
                    Global.systemLog.Error("processOleDBFile inner exception message: " + e.InnerException.Message, e.InnerException);
                }
                DataRow dr = errorTable.NewRow();
                dr[0] = name;
                dr[1] = ERROR_STR;
                if (excel && e.Message == "External table is not in the expected format.")
                    dr[2] = "Unable to load spreadsheet. Try loading into Excel and saving as a .csv file.";
                else if (excel && e.Message.StartsWith("The Microsoft Jet database engine cannot open the file ''"))
                    dr[2] = "Unable to open spreadsheet. Please check that it is valid and try again.";
                else
                    dr[2] = e.Message;
                errorTable.Rows.Add(dr);
                return (null);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            try
            {
                File.Delete(fname);
            }
            catch (Exception)
            {
                Global.systemLog.Warn("Unable to clean up the temp file in the data folder: " + fname);
            }
            return (convertToSummaryResults(createdFiles));
        }

        private ScanSummaryList convertToSummaryResults(Dictionary<string, ScanResults> createdFiles)
        {
            ScanSummaryList response = new ScanSummaryList();
            List<ScanSummaryResult> summaries = new List<ScanSummaryResult>();
            if (createdFiles != null && createdFiles.Count > 0)
            {
                foreach (KeyValuePair<string, ScanResults> kvPair in createdFiles)
                {
                    ScanSummaryResult summarySr = new ScanSummaryResult();
                    string key = kvPair.Key;
                    ScanResults value = kvPair.Value;
                    summarySr.wellFormed = value.wellFormed;
                    summarySr.keys = value.keys;
                    summarySr.defectiveRows = value.defectiveRows;
                    summarySr.cleanName = key;
                    summaries.Add(summarySr);
                }                
                response.scanSummaryList = summaries.ToArray();
            }            
            return response;
        }

        private double getNullCutoff(ScanResults sr)
        {
            /*
             * For each row, calculate the probability of it occurring based on individual
             * column probabilities. If the probability of it occurring in a dataset of a 
             * given size is less than a threshold, flag this worksheet as not well formed
             */
            return Math.Sqrt(((double)sr.totalNull) / ((double)sr.nullList.Count * sr.nullCounts.Length));
        }

        private bool isNull(object o, bool excel)
        {
            return (o == null || o == System.DBNull.Value ||
                (excel && typeof(string).Equals(o.GetType()) && ((string)o).Trim().Length == 0));
        }

        private ScanResults scanfile(OleDbDataReader reader, string tfname, bool pruneBlank, bool excel, List<string> keyCols, List<int> keyIndices)
        {
            Global.systemLog.Debug("scanfile: scanning oldbb data source into " + tfname);
            ScanResults sr = new ScanResults();
            sr.values = new object[reader.FieldCount];
            sr.nullCounts = new int[reader.FieldCount];
            sr.nullList = excel ? new List<BitArray>() : null;
            sr.nullPatterns = excel ? new Dictionary<string, int>() : null;
            StreamWriter sw = null;
            bool firstRow = true;
            try
            {
                sw = new StreamWriter(tfname, false);
                int rowsToSkip = this.beginSkip;
                while (reader.HasRows)
                {
                    if (!reader.Read())
                        break;
                    if (rowsToSkip > 0)
                    {
                        rowsToSkip--;
                        continue;
                    }
                    if (firstRow && !excel)
                    {
                        firstRow = false;
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string cn = reader.GetName(i);
                            if (excel)
                                sw.Write((i > 0 ? "|" : "") + cn.Trim());
                            else
                                sw.Write((i > 0 ? "|" : "") + cn);
                            if (keyCols.Contains(cn))
                                keyIndices.Add(i);
                        }
                        sw.WriteLine();
                    }
                    int numVals = reader.GetValues(sr.values);
                    if (numVals == sr.values.Length)
                    {
                        int nullCount = 0;
                        BitArray ba = new BitArray(reader.FieldCount);
                        // Prune null rows
                        if (pruneBlank || excel)
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                                if (isNull(sr.values[i], excel))
                                {
                                    nullCount++;
                                    ba.Set(i, true);
                                }
                        }
                        if (!pruneBlank || nullCount < sr.values.Length)
                        {
                            if (excel)
                            {
                                for (int i = 0; i < ba.Length; i++)
                                    if (ba[i])
                                        sr.nullCounts[i]++;
                                sr.totalNull += nullCount;
                                sr.nullList.Add(ba);
                                string s = ScanResults.getByteString(ba);
                                if (sr.nullPatterns.ContainsKey(s))
                                {
                                    sr.nullPatterns[s]++;
                                }
                                else
                                    sr.nullPatterns.Add(s, 1);
                            }
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                Object o = sr.values[i];
                                /*
                                 * Replace any embedded double quotes with single quotes.
                                 * Could escape double quotes, but then also need to escape when
                                 * escaped versions happen (i.e. '\"'). Doing this adds overhead to
                                 * every column, so just replace with single quotes. Also replace
                                 * any vertical bars with dashes as that is what is used as a delimiter
                                 */
                                string s = o.ToString().Replace('"', '\'').Replace('|', '-');
                                if (s.IndexOf('\r') >= 0 || s.IndexOf('\n') >= 0)
                                {
                                    s = '"' + s + '"';
                                }
                                if (excel)
                                {
                                    s = getStrippedString(s);
                                    sw.Write((i > 0 ? "|" : "") + (o != null ? s.Trim() : ""));
                                }
                                else
                                    sw.Write((i > 0 ? "|" : "") + (o != null ? s : ""));
                            }
                            sw.WriteLine();
                            sr.rows++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex.Message, ex);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
            return (sr);
        }

        private string getStrippedString(string s)
        {
            if (s.Length < 2)
                return (s);
            int start = 0;
            int end = s.Length - 1;
            char[] arr = s.ToCharArray();
            for (int i = 0; i < currencyprefixes.Length; i++)
            {
                if (arr[0] == currencyprefixes[i])
                {
                    start++;
                    break;
                }
            }
            if (start == 0 && arr[end] == '%')
                end--;
            bool validNum = true;
            for (int i = start; i <= end; i++)
            {
                if (arr[i] >= '0' && arr[i] <= '9')
                    continue;
                if (arr[i] == '.' || arr[i] == ',')
                    continue;
                if (i == start && arr[i] == '-')
                    continue;
                validNum = false;
                break;
            }
            if (!validNum)
                return (s);
            return (s.Substring(start, end - start + 1));
        }
    }
}