﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.Exceptions;

namespace Acorn
{
    public class ProcessOptions
    {
        public string LoadNumberProcessed;
        public bool CreateDashboards;
        public bool RecreateDashboards;
        public string ProcessDate;
        public string CurrentState;    
        public ErrorOutput ErrorOutput;
        public string RedirectModule;
        public string progressPercentage;
        public string progressText;
        public string DetailLink;
        public bool StopProcessing;
    }
    
   
    public class PublishProcess12
    {
        public bool currentState; // done, failed, processing, available
        public ErrorOutput Error;
    }

    
    public class PublishHistoryResult
    {
        public PublishHistory[] PublishHistoryOutput;
        public ErrorOutput Error;
    }
    public class PublishHistory
    {
        public string DateTime;
        public string LoadNumber;
        public string Step;
        public string Substep;
        public string ResultCode;
        public string Result;
        public string PublishLoadDate;
        public string LoadGroup;        
        public string PublishStartTime;
        public string PublishEndTime;
        public string ProcessingGroups;
    }

    public class DashboardConfiguration
    {
        public DashboardOption[] DashboardOptions;
        public ErrorOutput ErrorOutput;
    }

    public class DashboardOption
    {
        public string Name;
        public bool SumVisible;
        public bool SumChecked;
        public bool AvgVisible;
        public bool AvgChecked;
        public bool CountVisible;
        public bool CountChecked;
    }

    public class ErrorOutput
    {
        public int ErrorType;
        public string ErrorMessageSource;
        public string ErrorMessage;
        public string WarningMessage;
        public bool canRetryLoad;
        public string retryLoadDate;
        public string retryProcessingGroups;
    }

    public class DeleteDataOutput
    {
        public bool deleteLast;
        public bool spaceInUse;
        public bool isDeleted;
        public ErrorOutput errorOutput;
    }

    public class GenericResponse
    {        
        public string[] other;
        public ErrorOutput Error;

        public void setException(Exception ex)
        {
            if(Error == null)
            {
                Error = new ErrorOutput();
            }

            if (ex is BirstException)
            {
                BirstException bEx = (BirstException)ex;
                Error.ErrorType = bEx.getErrorType();
                if (bEx.getErrorMessage() != null && bEx.getErrorMessage().Length > 0)
                {
                    Error.ErrorMessage = bEx.getErrorMessage();
                }
                else if (bEx.Message != null && bEx.Message.Length > 0)
                {                    
                    Error.ErrorMessage = bEx.Message;
                }
            }
            else
            {
                Global.systemLog.Error("Exception occured " + ex);
                Error.ErrorType = BirstException.ERROR_GENERAL;
                Error.ErrorMessage = BirstException.MSG_GENERAL_ERROR;
            }
        }

    }

    
    public class TemplateCategories
    {
        public TemplateCategory[] Categories;
        public ErrorOutput Error;
    }
    public class TemplateCategory
    {
        public string CategoryName;
        public int Count;
    }

    public class TemplateList
    {
        public TemplateDetail[] TemplateDetails;
        public ErrorOutput Error;              
    }

    public class TemplateDetail
    {
        public string TemplateID;
        public string CreaterID;
        public string Name;
        public string Category;
        public string Comments;
        public int NumUses;
        public float AvgRating;
        public float UserRating;
        public DateTime CreateDate;
        public bool Automatic;
        public bool Private;
        public List<string> Attachments;
        public bool CanDelete;
    }

    public class CreateTemplateOutput
    {
        public string[] Attachments;
        public ErrorOutput Error;
    }

    public class SpaceProperties
    {
        public string SpaceID;
        public string SpaceName;
        public string SpaceComments;
        public int SpaceType;
        public bool EnableConvert;
        public bool TestMode;
        public bool checkTestMode;
        public bool EnableSSO;
        public string SSOPassword;
        public bool checkSSO;
        public bool LookAndFeel;
        public bool UploadData;
        public int LoadNumber;
        public ErrorOutput Error;
        public string[][] AdminSpaces;
        public string SpaceTimeZone;
        // used at the space level to make sure that the user is not allowed
        // to change processing time zone if space has already been processed        
        public bool LockProcessingTimeZone;
        public int MaxQueryRows;
        public int MaxQueryTimeout;
        public int QueryLanguageVersion;
        public bool RepositoryAdmin;
        public bool EnableUsage;
        public PerformanceEngineVersion ProcessVersion;
        public bool EnableAllPEVersions;
        public bool UseDynamicGroups;
        public bool SupportsRetryFailedLoad;
        public bool AllowRetryFailedLoad;
        public int NumThresholdFailedRecords;
        public int MinYear;
        public int MaxYear;
        public string fgcolor;
        public string bgcolor;
        public bool MapNullsToZero;
        public bool Published;
        public string ScheduleFrom;
        public string ScheduleSubject;
        public string[] DBCollations;
        public bool LockDBCollations;
        public string DBCollationName;
        public string DefaultCollationName;
        public string RServerURL;
        public int RServerPort;
        public string RServerUsername;
        public string RServerPassword;
    }

    public class SpaceTypeDetails : GenericResponse
    {
        public SpaceTypeDetail[] SpaceTypes;
    }

    public class SpaceTypeDetail
    {
        public int Type;
        public string Name;
        public string Description;
        public string DatabaseConnectString;
        public string DatabaseType;
        public string AdminUser;
        public string AdminPwd;        
        public int RegionID;
    }

    public class SFDCDetailsInfo : GenericResponse
    {
        // used to populate selected objects
        public SFDCSelectedObject[] SFDCSelectedObjects;
        // used to populate all objects available
        public SFDCObject[] SFDCObjects;
        public DateTime startExtractionTime;
        public DateTime endExtractionTime;
    }

    public class SFDCCredentials : GenericResponse
    {
        // credentials
        public string Username;
        public string Password;
        public string Sitetype;
        public bool savedLogin;
        public bool isAlreadyLoggedIn;
    }

    public class SFDCSelectedObject
    {
        public string Name;
        public string Query;
        public DateTime? LastUpdatedDate;
        public DateTime? LastSystemModStamp;
        public string[] ColumnNames;
        public int LastStatus;
    }

    // generic object defintion on sforce side
    // might add more info to allow user to see before using
    public class SFDCObject
    {
        public string Name;
        public string Label;
        public bool Selected;
    }

    public class ExtractionStatus : GenericResponse
    {
        public int Status;
        public ObjectExtractionStatus[] ObjectsExtractionStatus;
        public string[] failedMessages;
    }

    public class ObjectExtractionStatus
    {
        public int Status;
        public string ObjectName;
        public int Count;
    }
    
}
