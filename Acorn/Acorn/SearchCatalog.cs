﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using Acorn.TrustedService;

namespace Acorn
{
    public class SearchCatalog
    {
        public static SearchResult searchCatalog(HttpSessionState session, string path, string fileNameFilter, string dimValue, string exprValue, 
            string labelValue, string outputFileName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            SearchResult sr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            if (path.StartsWith("/private"))
                path = "/private/" + u.Username + (path.Length > 8 ? path.Substring(8) : "");
            sr = ts.searchReports(sp.Directory, path, fileNameFilter, encode(dimValue), encode(exprValue), encode(labelValue), outputFileName, u.Username, sp.ID.ToString(), importedSpacesInfoArray);
            if (!sr.successSpecified || !sr.success)
            {
                Global.systemLog.Info("TrustedService:searchReports failed: " + sr.errorMessage + " (" + sr.errorCode + ')');
            }
            return sr;
        }

        public static SearchResult replaceCatalog(HttpSessionState session, string[] reportPaths, string[] names, string dimValue, string exprValue, string labelValue,
            string replaceDimValue, string replaceExprValue, string replaceLabelValue)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            SearchResult sr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            sr = ts.replaceReportContent(sp.Directory, reportPaths, names, encode(dimValue), encode(exprValue), encode(labelValue), encode(replaceDimValue), 
                encode(replaceExprValue), encode(replaceLabelValue), u.Username, sp.ID.ToString(), importedSpacesInfoArray);
            if (!sr.successSpecified || !sr.success)
            {
                Global.systemLog.Info("TrustedService:replaceReports failed: " + sr.errorMessage + " (" + sr.errorCode + ')');
            }
            return sr;
        }

        private static string encode(string s)
        {
            while (s.IndexOf("'") >= 0)
                s = s.Replace("\'", "%27");
            return s;
        }

        public static SearchResult undoReplace(HttpSessionState session, string path, string fileNameFilter)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            SearchResult sr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            if (path.StartsWith("/private"))
                path = "/private/" + u.Username + (path.Length > 8 ? path.Substring(8) : "");
            sr = ts.restoreFromReplace(sp.Directory, path, fileNameFilter, u.Username, sp.ID.ToString(), importedSpacesInfoArray);
            if (!sr.successSpecified || !sr.success)
            {
                Global.systemLog.Info("TrustedService:undoreplace failed: " + sr.errorMessage + " (" + sr.errorCode + ')');
            }
            return sr;
        }
    }
}