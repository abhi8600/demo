﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class DataSourceLight
    {
        public string guid;
        public string Name;
        public string DisplayName;
        public int Status;
        public string StatusMessage;
        public bool Enabled;
        public DateTime LastUploadDate;
        public string[] LoadGroups;
        public bool UseBirstLocalForSource;
        public ScriptDefinition Script;
        public string[][] Levels;
        public string HierarchyName;
        public string[] SubGroups;
        public bool ImportedTable;
        public bool LiveAccessTable;
        public int Cardinality;
        public bool UnCached;
    }

    public class VariableWrapper
    {
        public Variable Variable;
        public bool Imported;
        public VariableWrapper() { }

        public VariableWrapper(Variable var, bool imported)
        {
            this.Variable = var;
            this.Imported = imported;
        }        
    }
}