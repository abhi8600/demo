﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Acorn
{
    public class AccountDetails
    {
        public Guid ID;
        public string Name;
        public DateTime ExpirationDate;
        public int MaxConcurrentLogins;
        public int MaxUsers;
        public bool IsEnterprise;
        public string authType;
        public string radiusServer;
        public int radiusPort;
        public string radiusSecret;
        public bool Disabled;
        [XmlIgnore]
        public Dictionary<string, string> OverrideParameters;

        public static readonly string OVERRIDE_PARAM_SPACETYPES = "SpaceTypes";
        private static readonly string KEY_DOMAIN_MATCH = "Domain.Match";
        private static readonly string KEY_DOMAIN_REPLACE = "Domain.Replace";

        // encapulate access to override parameters so we don't have to expose it to the rest of the application
        public bool isRADIUS()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey("AuthType"))
            {
                if (OverrideParameters["AuthType"] == "RADIUS")
                    return true;
            }
            return false;
        }

        public bool hasVisualizerEnabled()
        {
            string value = getOverrideParamValue("EnableVisualizer");
            return value != null && (string.Compare(value, "true", StringComparison.OrdinalIgnoreCase) == 0);
        }

        private string getOverrideParamValue(string keyName)
        {
            if (OverrideParameters != null && OverrideParameters.Count > 0)
            {
                foreach (KeyValuePair<string, string> kvPair in OverrideParameters)
                {
                    string key = kvPair.Key;
                    if (key != null && (string.Compare(key, keyName, StringComparison.OrdinalIgnoreCase) == 0))
                    {
                        return kvPair.Value;
                    }
                }
            }
            return null;
        }

        public string getDomainReplace()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey(KEY_DOMAIN_REPLACE))
                return OverrideParameters[KEY_DOMAIN_REPLACE];
            return null;
        }

        public string getDomainMatch()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey(KEY_DOMAIN_MATCH))
                return OverrideParameters[KEY_DOMAIN_MATCH];
            return null;
        }

        public string getRADIUSHost()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey("RADIUS.Host"))
                return OverrideParameters["RADIUS.Host"];
            return null;
        }

        public string getRADIUSSharedSecret()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey("RADIUS.SharedSecret"))
                return OverrideParameters["RADIUS.SharedSecret"];
            return null;
        }

        public int getDefaultSpaceType()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey("DefaultSpaceType"))
                return Int32.Parse(OverrideParameters["DefaultSpaceType"]);
            return -1;
        }

        public string getSpaceTypes()
        {
            if (OverrideParameters != null && OverrideParameters.ContainsKey(OVERRIDE_PARAM_SPACETYPES))
                return OverrideParameters[OVERRIDE_PARAM_SPACETYPES];
            return null;
        }

        public List<int> getSpaceTypesList()
        {
            string spaceTypesString = getSpaceTypes();
            if (spaceTypesString != null && spaceTypesString.Trim().Length > 0)
            {
                string[] strArr = spaceTypesString.Trim().Split(',');
                if (strArr != null && strArr.Length > 0)
                {
                    List<int> response = new List<int>();
                    foreach (string str in strArr)
                    {
                        response.Add(Int32.Parse(str));
                    }
                    return response;
                }
            }
            return null;
        }
    }
}
