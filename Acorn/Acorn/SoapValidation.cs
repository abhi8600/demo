﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services.Protocols;
using System.Xml.XPath;
using System.Web.Services.Description;
using System.Web.Services.Configuration;
using System.Xml.Serialization;
using System.Xml;
using System.Text;

namespace Acorn
{
    internal class SoapValidationContext
    {
        internal AssertAttribute[] assertAttributes = null;
        internal XPathExpression completeRuleExpression = null;
        internal ValidationAttribute validationAttribute = null;
        internal XmlNamespaceManager namespaceManager = null;
    }

    public class SoapValidation : SoapExtension
    {
        public const string NS = "http://www.birst.com/";
        public const string XMLNS = "http://www.w3.org/2000/xmlns/";

        SoapValidationContext context;

        public override object GetInitializer(Type serviceType)
        {
            return GetInitializerHelper(serviceType, null, null);
        }

        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return GetInitializerHelper(methodInfo.DeclaringType, methodInfo, attribute);
        }

        public override void Initialize(object initializer)
        {
            context = (SoapValidationContext)initializer;
        }

        public override void ProcessMessage(SoapMessage message)
        {
            if (message.Stage == SoapMessageStage.BeforeDeserialize && context != null)
            {
                // evaluate the XPath assertions - if complete expression returns true
                // let the System.Web.Services infrastructure finish processing 
                if ((context.validationAttribute != null) &&
                    (context.validationAttribute.CheckAssertions == true) &&
                    (context.completeRuleExpression != null))
                {
                    try 
                    {
                        // create an XPathNavigator on the request stream
                        XmlReader reader = new XmlTextReader(message.Stream);

                        // turn the message stream into an XPathDocument
                        // (will validate if it's an XmlValidatingReader)
                        XPathDocument doc = new XPathDocument(reader);
                        XPathNavigator nav = doc.CreateNavigator();

                        if (false == (bool)nav.Evaluate(context.completeRuleExpression))
                        {

                            // otherwise generate a SOAP fault indicating exactly which
                            // assertions failed
                            XmlDocument errorDoc = new XmlDocument();
                            XmlElement detailElement = errorDoc.CreateElement(SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
                            errorDoc.AppendChild(detailElement);
                            XmlElement failedRulesElement = errorDoc.CreateElement("b", "failedAssertions", NS);
                            XmlAttribute nsDeclAttribute = null;
                            // generate namespace declarations required by assert expressions
                            foreach (string prefix in context.namespaceManager)
                            {
                                if (prefix.Equals("xml") || prefix.Equals("xmlns") || prefix.Equals(""))
                                    continue;
                                nsDeclAttribute = errorDoc.CreateAttribute("xmlns", prefix, XMLNS);
                                nsDeclAttribute.Value = context.namespaceManager.LookupNamespace(prefix);
                                failedRulesElement.Attributes.Append(nsDeclAttribute);
                            }
                            detailElement.AppendChild(failedRulesElement);

                            // check to see which assertions failed and list in failedRules
                            foreach (AssertAttribute aa in context.assertAttributes)
                            {
                                if (false == (bool)nav.Evaluate(aa.Expression))
                                {
                                    XmlElement assertElement = errorDoc.CreateElement("b", "assert", NS);
                                    failedRulesElement.AppendChild(assertElement);
                                    XmlElement expElement = errorDoc.CreateElement("b", "expression", NS);
                                    assertElement.AppendChild(expElement);
                                    expElement.InnerText = aa.Rule;
                                    XmlElement descElement = errorDoc.CreateElement("b", "description", NS);
                                    assertElement.AppendChild(descElement);
                                    descElement.InnerText = aa.Description;
                                }
                            }

                            // throw a SoapException with the detail element node we just built
                            SoapException error = new SoapException(
                                "Validation failure",
                                SoapException.ClientFaultCode,
                                HttpContext.Current.Request.Url.AbsoluteUri,
                                detailElement);
                            throw error;
                        }
                    }
                    finally
                    {
                        // make sure you leave the stream the way you found it
                        message.Stream.Position = 0L;
                    }
                }
            }
        }

        // called by both versions of GetInitializer to generated initializer object (ValidationContext)
        // which serves as the context for future requests
        public object GetInitializerHelper(Type serviceType, LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            SoapValidationContext ctx = new SoapValidationContext();
            HttpContext httpctx = HttpContext.Current;

            // cache ValidationAttribute for future use
            ctx.validationAttribute = (ValidationAttribute)attribute;

            // temporary document/navigator for compiling XPath expressions
            XmlDocument doc = new XmlDocument();
            XPathNavigator nav = doc.CreateNavigator();
            // namespace manager for holding all namespace bindings
            XmlNamespaceManager ns = new XmlNamespaceManager(nav.NameTable);

            // retrieve (user-provided) namespace binding attributes
            object[] namespaceAtts = serviceType.GetCustomAttributes(typeof(AssertNamespaceBindingAttribute), true);
            foreach (AssertNamespaceBindingAttribute nsa in namespaceAtts)
                ns.AddNamespace(nsa.Prefix, nsa.Namespace);
            // store namespace manager in context for future use
            ctx.namespaceManager = ns;


            // retrieve (user-provided) assertion attributes
            AssertAttribute[] allRuleAtts = null;
            object[] classRuleAtts = serviceType.GetCustomAttributes(typeof(AssertAttribute), true);
            if (methodInfo != null) // enabled via custom SoapExtensionAttribute
            {
                // retrieve (user-provided) method and class-level assert attributes
                object[] methodRuleAtts = methodInfo.GetCustomAttributes(typeof(AssertAttribute));
                allRuleAtts = new AssertAttribute[methodRuleAtts.Length + classRuleAtts.Length];
                methodRuleAtts.CopyTo(allRuleAtts, 0);
                classRuleAtts.CopyTo(allRuleAtts, methodRuleAtts.Length);
            }
            else
                // just retrieve (user-provided) class-level assertion attributes
                allRuleAtts = (AssertAttribute[])classRuleAtts;

            // store all assertions in context
            ctx.assertAttributes = allRuleAtts;

            // generate, compile, and cache XPath expressions for future use
            StringBuilder completeExpression = new StringBuilder();
            string and = "";
            foreach (AssertAttribute ra in allRuleAtts)
            {
                string rule = String.Format("boolean({0})", ra.Rule);
                // cache compiled expression for future use
                ra.Expression = nav.Compile(rule);
                ra.Expression.SetContext(ns);
                completeExpression.Append(and);
                completeExpression.Append(rule);
                and = " and ";
            }
            if (completeExpression.Length != 0)
            {
                // complete expression (combination off all asserts) for quick success/failure check
                ctx.completeRuleExpression = nav.Compile(completeExpression.ToString());
                ctx.completeRuleExpression.SetContext(ns);
            }

            // the System.Web.Services infrastructure will cache this object
            // and supply it in each future call to Initialize
            return ctx;
        }
    }
    // ValidationAttribute triggers schema/assert validation through
    // the ValidationExtension class
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ValidationAttribute : SoapExtensionAttribute
    {
        int priority = 0;
        bool checkAssertions = true;

        // enable assertion processing?
        public bool CheckAssertions
        {
            get { return checkAssertions; }
            set { checkAssertions = value; }
        }

        // used by soap extension to get the type
        // of object to be created
        public override System.Type ExtensionType
        {
            get
            {
                // tell the System.Web.Services infrastructure
                // to use the ValidationExtension class
                return typeof(SoapValidation);
            }
        }
        public override int Priority
        {
            get { return priority; }
            set { priority = value; }
        }
    }

    // specifies an XPath expression evaluated as an assertion (must evaluate to true)
    // can be used on an individual method or the entire class - if used on the class, 
    // the expression applies to all WebMethods
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AssertAttribute : System.Attribute
    {
        string _rule;
        string _description;
        XPathExpression _expression;

        public AssertAttribute() : this("", "") { }

        public AssertAttribute(string rule, string description)
        {
            _rule = rule;
            _description = description;
            _expression = null;
        }
        public AssertAttribute(string rule) : this(rule, "") { }

        [XmlElement("expression")]
        public string Rule
        {
            get { return _rule; }
            set { _rule = value; }
        }
        [XmlElement("description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        internal XPathExpression Expression
        {
            get { return _expression; }
            set { _expression = value; }
        }
    }

    // Specifies namespace bindings required by the XPath (assert)
    // expressions
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class AssertNamespaceBindingAttribute : System.Attribute
    {
        string _prefix;
        string _ns;

        public AssertNamespaceBindingAttribute(string prefix, string ns)
        {
            _prefix = prefix;
            _ns = ns;
        }
        public string Prefix
        {
            get { return _prefix; }
        }
        public string Namespace
        {
            get { return _ns; }
        }
    }

    // format extension used to customize automatic WSDL generation (.asmx?wsdl)
    [XmlFormatExtension("validation", SoapValidation.NS, typeof(InputBinding))]
    [XmlFormatExtensionPrefix("b", SoapValidation.NS)]
    public class ValidationFormatExtension : ServiceDescriptionFormatExtension
    {
        // holds array of AssertAttributes (expression + description)
        [XmlArray("assertions")]
        [XmlArrayItem("assert")]
        public AssertAttribute[] assertions;
    }

    // called by the runtime during WSDL generation (.asmx?wsdl)
    // must be configured in the web/machine.config file
    public class ValidationExtensionReflector : SoapExtensionReflector
    {
        public override void ReflectMethod()
        {
            ProtocolReflector reflector = ReflectionContext;
            // ValidationFormatExtension represents an extension element
            ValidationFormatExtension fe = new ValidationFormatExtension();

            // populate with assertions and namespaces
            object va = reflector.Method.GetCustomAttribute(typeof(ValidationAttribute));
            if (va != null)
            {
                // retrieve assertions
                object[] classRuleAtts = reflector.Method.DeclaringType.GetCustomAttributes(typeof(AssertAttribute), true);
                object[] methodRuleAtts = reflector.Method.GetCustomAttributes(typeof(AssertAttribute));
                fe.assertions = new AssertAttribute[methodRuleAtts.Length + classRuleAtts.Length];
                methodRuleAtts.CopyTo(fe.assertions, 0);
                classRuleAtts.CopyTo(fe.assertions, methodRuleAtts.Length);
            }
            // inject extension element in the binding/input element
            reflector.OperationBinding.Input.Extensions.Add(fe);
        }
    }
}
