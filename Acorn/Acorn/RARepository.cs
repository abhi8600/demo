﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class RARepository
    {
        public RADimension[] Dimensions;
        public RAMeasureColumn[] Measures;
        public RAMeasureTable[] MeasureTables;
        public RAJoin[] Joins;
        public RAConnection[] Connections;
        public Hierarchy[] Hierarchies;
        public int queryLanguageVersion;
        public int DimensionalStructure;

        public class RADimensionColumnMapping
        {
            public string ColumnName;
            public string Formula;
            public bool AutoGenerated;
            public bool ManuallyEdited;
        }

        public class RADimensionColumn
        {
            public string ColumnName;
            public string DataType;
            public string Format;
            public int Width;
            public bool AutoGenerated;
            public bool ManuallyEdited;
        }

        public class RADimensionTable
        {
            public string Name;
            public string Level;
            public int Type;
            public string PhysicalName;
            public TableSource Source;
            public string Query;
            public bool Cacheable;
            public int TTL;
            public RADimensionColumnMapping[] Mappings;
            public bool AutoGenerated;
        }

        public class RADimension
        {
            public string Name;
            public bool Locked;
            public bool UsedByNonLiveAccessSource;
            public RADimensionTable[] Definitions;
            public RADimensionColumn[] Columns;
        }

        public class RAMeasureColumnMapping
        {
            public string ColumnName;
            public string Formula;
            public bool AutoGenerated;
            public bool ManuallyEdited;
        }

        public class RAMeasureColumn
        {
            public string ColumnName;
            public string AggRule;
            public string DataType;
            public string Format;
            public int Width;
            public bool AutoGenerated;
            public bool ManuallyEdited;
        }

        public class RAMeasureTable
        {
            public string Name;
            public int Type;
            public string PhysicalName;
            public TableSource Source;
            public string Query;
            public bool Cacheable;
            public int TTL;
            public double Cardinality;
            public RAMeasureColumnMapping[] Mappings;
            public bool AutoGenerated;
        }

        public class RAJoin
        {
            public string Table1;
            public string Table2;
            public string JoinCondition;
            public bool Redundant;
            public string JoinType;
            public bool Invalid;
            public bool AutoGenerated;
            public bool Federated;
            public string FederatedJoinKey;
        }

        public class RAConnection: Connection
        {
            public RAConnection() {}
            public RAConnection(DatabaseConnection dc) : base(dc)
            {
            }
        }
    }
}
