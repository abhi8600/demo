﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;

namespace Acorn
{
    public class ApplicationUploadService
    {
        private static string COLUMN_DELIMITER = "<td>";
        public static UploaderResponse processUploadedFile(HttpSessionState session, string clientFileName, string beginSkip, string endSkip, bool firstRowNames,
           bool lockFormat, bool ignoreInternalQuotes, string[][] escape, bool filterRows, string encoding, bool consolidate, string separator, bool autoModel, string quoteCharcter, bool ignorecarriagereturn, bool forceNumColumns, bool borwserUpload)
        {
            UploaderResponse response = new UploaderResponse();            
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            User u = (User)session["User"];           
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to continue. Please login again";                
                return response;
            }

            Space sp = (Space)session["Space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details, Please try again";                
                return response;
            }

            int ignoredRows = 0;
            int ignoredLastRows = 0;
            try
            {
                ignoredRows = int.Parse(beginSkip);
                ignoredLastRows = int.Parse(endSkip);
            }
            catch (Exception)
            {
            }
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to proceed, Please try again";
                return response;
            }

            if (encoding != null && encoding.Length == 0)
            {
                encoding = null;
            }

            if (separator != null && separator.Length == 0)
            {
                separator = null;
            }
			String quoteChar = null;
            if(quoteCharcter != null && !quoteCharcter.Equals(""))
            {
                quoteChar = quoteCharcter;
            }
            ApplicationUploader ul = new ApplicationUploader(maf, sp, clientFileName, firstRowNames, lockFormat, ignoreInternalQuotes, escape, forceNumColumns, quoteChar,
                (User)session["user"], session, ignoredRows, ignoredLastRows, filterRows, encoding, separator, autoModel);
            ul.setIgnoreCarriageReturn(ignorecarriagereturn);
            ul.isBrowserUpload = borwserUpload;
            // used later if there is an excel warning
            session["appUploader"] = ul;
            // upload and retreive any error during extension processing
            DataTable errorTable = ul.uploadFile(consolidate, true, false);
            int successCount = ul.getSuccessfullCounts();
            // check for excel warning first, if present return the error
            Dictionary<string, ScanSummaryResult> anyExcelErrors = ul.getExcelWarningDetails();
            if (anyExcelErrors != null)
            {
                List<ExcelWarningError> excelErros = formatExcelWarningErrors(sp, anyExcelErrors);
                if (excelErros != null)
                {
                    response.ExcelErrors = excelErros.ToArray();
                }
            }

            if (errorTable != null)
            {
                formatErrors(response, errorTable);
            }
            return response;
        }        

        public static UploaderResponse processNextStage(HttpSessionState session, string beginSkip, string endSkip, bool firstRowNames,
           bool lockFormat, bool ignoreInternalQuotes, string[][] escape, bool filterRows, string encoding, bool consolidate, string separator, bool autoModel)
        {
            UploaderResponse response = new UploaderResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details, Please try again";               
                return response;
            }

            Space sp = (Space)session["Space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details, Please try again";
                return response;
            }
            if (encoding != null && encoding.Length == 0)
            {
                encoding = null;
            }
            if (separator != null && separator.Length == 0)
            {
                separator = null;
            }

            Acorn.ApplicationUploader.UploadFilesInfo ufi = (Acorn.ApplicationUploader.UploadFilesInfo)session["processfileinfo"];            
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            int ignoredRows = 0;
            int ignoredLastRows = 0;
            try
            {
                ignoredRows = int.Parse(beginSkip);
                ignoredLastRows = int.Parse(endSkip);
            }
            catch (Exception)
            {
            }
            
            ApplicationUploader ul = (ApplicationUploader)session["appUploader"];
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);
            try
            {
                ul.processFiles(ufi, consolidate, true, false, autoModel);
            }
            finally
            {
                maf.setInUberTransaction(previousUberTransaction);
            }
            // check for ufi errors during processing of files 
            // specifically during addFile() operation
            DataTable ufiErrorTable = ufi.errorTable;

            formatErrors(response, ufiErrorTable);
            removeSessionUploader(session);
            return response;
        }

        private static void removeSessionUploader(HttpSessionState session)
        {
            session.Remove("appUploader");
        }

        public static GenericResponse cancelNextStage(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            User u = (User)session["User"];
            if (u == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details, Please try again";                
                return response;
            }

            Space sp = (Space)session["Space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details, Please try again";
                return response;
            }
            
            Acorn.ApplicationUploader.UploadFilesInfo ufi = (Acorn.ApplicationUploader.UploadFilesInfo)session["processfileinfo"];    
            string path = Path.Combine(sp.Directory, "data");
            foreach (string fk in ufi.filenames.Keys)
            {
                string file = Path.Combine(path, fk);
                Util.validateFileLocation(path, file);
                File.Delete(file);
            }
            removeSessionUploader(session);
            return response;
        }

        public GenericResponse getSpaceLoadNumber(HttpSessionState session)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = (Space)session["Space"];

            return response;
        }

        // To do. Need to create a use case. 
        public static List<ExcelWarningError> formatExcelWarningErrors(Space sp, Dictionary<string, ScanSummaryResult> scans)
        {
            List<ExcelWarningError> excelErrors = new List<ExcelWarningError>();
            string path = Path.Combine(sp.Directory, "data");
            foreach (string fname in scans.Keys)
            {
                ScanSummaryResult sr = scans[fname];
                if (!sr.wellFormed)
                {
                    string fnames = Path.Combine(path, fname + ".unfiltered");
                    Util.validateFileLocation(path, fnames);
                    if (!File.Exists(fnames))
                        continue;
                    StreamReader ereader = new StreamReader(fnames);
                    string line = null;
                    int rownum = 0;
                    ExcelWarningError excelError = new ExcelWarningError();
                    List<string> rows = new List<string>();
                    List<bool> defective = new List<bool>();
                    int extIndex = fname.LastIndexOf(".");
                    if (extIndex > 0)
                    {
                        excelError.FileName = fname.Substring(0, extIndex);

                    }
                    while ((line = SourceFiles.readLine(ereader, null, false, "\"", 0, false, false)) != null)
                    {
                        if (line.Length == 0)
                            continue;

                        bool likely = sr.isLikelyPattern(rownum++);
                        /*
                        BitArray ba = sr.nullList[rownum++];
                        bool likely = isLikelyPattern(ba);
                         * */
                        StringBuilder row = new StringBuilder();
                        if (likely)
                            continue; // moving to the next row if row is ok
                        else
                            defective.Add(true);
                        string[] cols = line.Split(new char[] { '|' });
                        foreach (string s in cols)
                        {
                            row.Append(COLUMN_DELIMITER + s );                            
                        }
                       
                        rows.Add(row.ToString());
                        if (rows.Count > 50)
                            break;
                    }
                    ereader.Close();
                    excelError.Rows = rows.ToArray();
                    excelError.Defective = defective.ToArray();
                   
                    excelErrors.Add(excelError);
                }
            }
            return (excelErrors.Count == 0 ? null : excelErrors);
        }

        public static string[][] getEscapes(string s)
        {
            if (s == null || s.Length == 0)
                return null;
            string[] pairs = Performance_Optimizer_Administration.Util.splitString(s, ',');
            List<string[]> results = new List<string[]>();
            foreach (string pair in pairs)
            {
                string[] parts = Performance_Optimizer_Administration.Util.splitString(pair, '=');
                if (parts.Length == 2)
                {
                    parts[0] = parts[0].Trim(new char[] { '"' });
                    parts[1] = parts[1].Trim(new char[] { '"' });
                    results.Add(parts);
                }
            }
            if (results.Count == 0)
                return null;
            return results.ToArray();
        }

        private static void formatErrors(UploaderResponse response, DataTable et)
        {
            if(et != null && et.Rows.Count > 0)
            {
                List<UploaderError> uploaderErrorList = new List<UploaderError>();
                foreach (DataRow dr in et.Rows)
                {
                    UploaderError uploaderError = new UploaderError();
                    uploaderError.Source =   (dr[0] != System.DBNull.Value) ? (string)dr[0] : null;
                    uploaderError.Severity = (dr[1] != System.DBNull.Value) ? (string)dr[1] : null;
                    uploaderError.Issue =    (dr[2] != System.DBNull.Value) ? (string)dr[2] : null;
                    uploaderErrorList.Add(uploaderError);
                }
                response.UploadErrors = uploaderErrorList.ToArray();
            }
        }
    }
}
