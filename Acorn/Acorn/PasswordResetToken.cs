﻿using System;

namespace Acorn
{
    public class PasswordResetToken
    {
        public Guid tokenID;
        public Guid userID;
        public string hostaddr;
        public DateTime endOfLife;
    }
}