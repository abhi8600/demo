﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Net;


namespace Acorn
{
    public class SchedulerLogin : SchedulerAccess.SchedulerAccess
    {
        private NameValueCollection headers;
        private WebRequest request;
        public void setRequestHeaders(NameValueCollection headers)
        {
            this.headers = headers;
        }

        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            System.Net.WebRequest result = base.GetWebRequest(uri);
            if (headers != null)
            {
                result.Headers = new System.Net.WebHeaderCollection();
                result.Headers.Add(headers);
            }
            request = result;
            return result;
        }

        public WebResponse getServiceReturnResponse()
        {
            if (request != null)
                return base.GetWebResponse(request);
            return null;
        }
    }
}
