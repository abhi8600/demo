﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.Odbc;
using System.Security.Cryptography;
using System.IO;
using Performance_Optimizer_Administration;
using Acorn.Utils;
using Acorn.Background;
using System.Threading;
using System.Collections;
using System.Net;
using System.Collections.Specialized;
using Acorn.Exceptions;
using Acorn.DBConnection;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Acorn
{
    public class CommandHelper
    {
        private const int MAX_NUM_TRANSPORT_ROWS = 1000;
        private static Dictionary<User, string> userQueryTokenDictionary = new Dictionary<User, string>();
        private static WebServiceSessionHelper.ClearableDictionary<string, PagedQueryResult> pagedQueryResultSets = new WebServiceSessionHelper.ClearableDictionary<string, PagedQueryResult>();
        private static string DATASOURCE_TMP_DIR = "\\tmpData\\";
        private static string DATASOURCE_FINAL_DIR = "\\data\\";
        private static WebServiceSessionHelper.ClearableDictionary<string, string> uploadFilePath = new WebServiceSessionHelper.ClearableDictionary<string, string>();
        private static WebServiceSessionHelper.ClearableDictionary<string, Space> uploadFileSpace = new WebServiceSessionHelper.ClearableDictionary<string, Space>();
        private static WebServiceSessionHelper.ClearableDictionary<string, UploadFileOptions> uploadFileOptions = new WebServiceSessionHelper.ClearableDictionary<string, UploadFileOptions>();
        // Note that these cookies are for SMIWeb login where session varialbles are not filled. This is meant for faster login for catalog management and custom subject area
        // web services since they do not need session variables.
        private static WebServiceSessionHelper.ClearableDictionary<string, CookieContainer> cookies = new WebServiceSessionHelper.ClearableDictionary<string, CookieContainer>();
        private static HashSet<string> backgroundProcessesRunning = new HashSet<string>();
        private static WebServiceSessionHelper.ClearableDictionary<string, BackgroundModule> backgroundProcesses = new WebServiceSessionHelper.ClearableDictionary<string, BackgroundModule>();

        private static Dictionary<string, Space> loggedInSpaces = new Dictionary<string, Space>();
        private static System.Threading.Timer timer = createTimer();
        private static readonly string ENDPOINT_SUBJECTAREA = "/SMIWeb/services/SubjectArea";
        private static readonly string DEFAULT_SUBJECT_AREA = "Default Subject Area";
        private static readonly string USER_GROUP = "USER$";
        private static string ipRegEx = "^((0|1[0-9]{0,2}|2[0-9]{0,1}|2[0-4][0-9]|25[0-5]|[3-9][0-9]{0,1})\\.){3}(0|1[0-9]{0,2}|2[0-9]{0,1}|2[0-4][0-9]|25[0-5]|[3-9][0-9]{0,1})(?(\\/)\\/([0-9]|[1-2][0-9]|3[0-2])|)$";
        class BackgroundProcessObserver : BackgroundModuleObserver
        {
            #region BackgroundModuleObserver Members

            public void killed(object o)
            {
                removeBackgroundProcessFromRunningList(o);
            }

            public void finished(object o)
            {
                removeBackgroundProcessFromRunningList(o);
            }

            #endregion

            public void removeBackgroundProcessFromRunningList(object o)
            {
                string token = o.ToString();
                lock (backgroundProcessesRunning)
                {
                    backgroundProcessesRunning.Remove(token);
                }
            }

            public void addBackgroundProcessToRunningList(object o)
            {
                string token = o.ToString();
                lock (backgroundProcessesRunning)
                {
                    if (!backgroundProcessesRunning.Contains(token))
                    {
                        backgroundProcessesRunning.Add(token);
                    }
                }
            }

            public bool isStillRunning(string token)
            {
                if (!WebServiceSessionHelper.validToken(token))
                {
                    WebServiceSessionHelper.error("isComplete", "token " + token + " is not valid or has expired.");
                }
                bool ret = false;
                lock (backgroundProcessesRunning)
                {
                    ret = backgroundProcessesRunning.Contains(token);
                }
                return ret;
            }
        }
        private static BackgroundProcessObserver backgroundModuleObserver = new BackgroundProcessObserver();

        private static System.Threading.Timer createTimer()
        {
            return new System.Threading.Timer(new TimerCallback(CommandHelper.clearOldItemsFromDictionaries), null, 1000, 60000);
        }

         static void clearOldItemsFromDictionaries(Object o)
        {
            List<string> oldCookies = cookies.getOldItems();
            if (oldCookies != null && oldCookies.Count > 0)
            {
                foreach (string s in oldCookies)
                {
                    SMILogout(s);
                }
                
            }
            pagedQueryResultSets.clearOldItemsFromDictionary();
            uploadFilePath.clearOldItemsFromDictionary();
            uploadFileSpace.clearOldItemsFromDictionary();
            uploadFileOptions.clearOldItemsFromDictionary();
            backgroundProcesses.clearOldItemsFromDictionary();
            WebServiceSessionHelper.users.clearOldItemsFromDictionary();
            WebServiceSessionHelper.tokens.clearOldItemsFromDictionary();
        }

         private static void SMILogout(string s)
         {
             CookieContainer cc = null;
             Space sp = null;
             if (cookies.TryGetValue(s, out cc) && loggedInSpaces.TryGetValue(s, out sp))
             {
                 // have cookie container - need to logout
                 lock (loggedInSpaces)
                 {
                     loggedInSpaces.Remove(s);
                 }

                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);

                try {
                    String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    string url = localprotocol + sc.LocalURL + Util.sessionServiceEndPoint;

                    SMIWebSession smiWebSession = new SMIWebSession();
                    smiWebSession.Url = url;

                    smiWebSession.CookieContainer = cc;
                    SMIWebSessionService.logoutResponse logoutResponse = smiWebSession.logout();

                    if (logoutResponse != null && logoutResponse.@return == "true")
                    {
                        Global.systemLog.Info("Successful logout for SMIWeb session (CommandHelper): " + s);
                    }
                    else
                    {
                        Global.systemLog.Warn("Problem in logging out for SMIWeb session (CommandHelper): " + s);
                    }
                }
                catch (Exception e)
                {
                    //log the exception
                    Global.systemLog.Error("Exception thrown while logging out of SMIWeb (CommandHelper)", e);
                }
             }

             lock (cookies)
             {
                 cookies.Remove(s);
             }
         }

         public static CookieContainer SMILogin(string token, User u, Space sp)
         {
             CookieContainer cc = null;
             Space loggedInSpace = null;
             if (loggedInSpaces.TryGetValue(token, out loggedInSpace)) {
                 if (! loggedInSpace.ID.Equals(sp.ID)) {
                     SMILogout(token);
                 }
             }
             if (!cookies.TryGetValue(token, out cc))
             {
                 cc = new CookieContainer();
                 lock (cookies)
                 {
                     cookies.Add(token, cc);
                 }
                 lock (loggedInSpaces)
                 {
                     loggedInSpaces.Add(token, sp);
                 }

                 String localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                 SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                 // get superuserflag if any on user object
                 bool isSuperUser = u.RepositoryAdmin;
                 // Only ssoUser is passed in the header. ssoUser is specifically checked from header
                 // on the SMIWeb side for authentication. Once we remove the authentication from SMIWeb
                 // side, this should go away.

                 String ssotoken = Util.encodeForHeader(Util.getSMILoginToken(u.Username, sp));
                 NameValueCollection requestHeaders = new NameValueCollection();
                 requestHeaders.Add("ssoUser", ssotoken);                

                 bool newRepository = false;
                 MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                 SpaceMembership sm = Util.getSpaceMembershipFromDB(u, sp);
                 bool isSpaceAdmin = sm != null ? sm.Administrator || sm.Owner : false;
                 List<String> loginCredentials = Util.getSMIWebLoginCredentials(sp, u, maf, null, isSuperUser, isSpaceAdmin, false, null); 
                 UserAndGroupUtils.updatedUserGroupMappingFile(sp);
                 SMIWebLogin smiWebLogin = new SMIWebLogin();               
                 smiWebLogin.setRequestHeaders(requestHeaders);                 
                 smiWebLogin.Url = localprotocol + sc.LocalURL + Util.loginServiceEndPoint;
                 Global.systemLog.Debug("SMIWeb: " + localprotocol + ", " + sc.LocalURL + ", " + smiWebLogin.Url);
                 Uri serviceUri = new Uri(smiWebLogin.Url);
                 smiWebLogin.CookieContainer = cc;
                 // call the Login webservice and login operation

                 String smiWebResponse = null;
                 try
                 {   
                     smiWebResponse = smiWebLogin.login(loginCredentials.ToArray());
                     if (smiWebResponse != null && smiWebResponse == "true")
                     {
                         Global.systemLog.Info("Successful login (CommandHelper) to SMIWeb for token: " + token);
                     }
                     else
                     {
                         Global.systemLog.Warn("Failed to login (CommandHelper) to SMIWeb (" + smiWebLogin.Url + ") for token: " + token + ", with response: " + smiWebResponse);
                         return null;
                     }
                 }
                 catch (WebException wex)
                 {
                     Global.systemLog.Error("Error while logging in to SMIWeb (CommandHelper): " + smiWebLogin.Url, wex);
                     return null;
                 }
             }
             return cc;
         }



        private static Space getSpace(User u, string spaceID, string prefix, verifyUserSpace func)
        {
            QueryConnection conn = null;
            Space newsp = null;
            try
            {
                conn = ConnectionPool.getConnection();
                try
                {
                    Guid spaceIDGuid = new Guid(spaceID);
                    newsp = Database.getSpace(conn, WebServiceSessionHelper.mainSchema, spaceIDGuid);
                }
                catch (FormatException)
                {
                    // assume spaceID is actually a name
                    newsp = Database.getSpace(conn, spaceID, u, true, WebServiceSessionHelper.mainSchema);
                }
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error(prefix, e.GetBaseException().Message);
            }
            finally
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }
            if (newsp == null)
            {
                WebServiceSessionHelper.warn(prefix, "Space " + spaceID + " does not exist");
            }

            func(prefix, u, newsp);

            return newsp;
        }

        public static void isSpaceAdmin(string prefix, User u, Space sp)
        {
            // operations can do anything
            if (u.OperationsFlag)
                return;

            SpaceMembership sm = Util.getSpaceMembershipFromDB(u, sp);
            bool ret = (sm != null ? sm.Administrator || sm.Owner : false);
            if (ret == false)
            {
                WebServiceSessionHelper.warn(prefix, "User " + u.Username + " is not a space administrator or owner for space " + sp.Name + " and does not have permission to do action.");
            }
        }

        private static void isOperations(string prefix, User u, Space sp)
        {
            // operations can do anything
            if (u.OperationsFlag)
                return;
            WebServiceSessionHelper.warn(prefix, "User " + u.Username + " is not a space administrator or owner for space " + sp.Name + " and does not have permission to do action.");
        }

        private static void anyoneCanDoThis(string prefix, User u, Space sp)
        {
        }

        private static void isAnAccountAdmin(string prefix, User u)
        {
            if (! UserManagementUtils.isUserAuthorized(u))
                WebServiceSessionHelper.warn(prefix, "User " + u.Username + " is not an account administrator and does not have permission to do action.");
        }

        private delegate void verifyUserSpace(string prefix, User u, Space sp);

        private static bool getSpaces(User u, string sp1ID, string sp2ID, out Space sp1, out Space sp2, string prefix, verifyUserSpace func)
        {
            sp1 = null;
            sp2 = null;
            QueryConnection conn = null;
            Guid sp1IDGuid = Guid.Empty;
            Guid sp2IDGuid = Guid.Empty;
            Guid newGuid;
            if (!Guid.TryParse(sp1ID, out newGuid))
                throw new SoapException(prefix + ": invalid space identifier format - " + sp1ID, new System.Xml.XmlQualifiedName());
            sp1IDGuid = newGuid;
            if (!Guid.TryParse(sp2ID, out newGuid))
                throw new SoapException(prefix + ": invalid space identifier format - " + sp2ID, new System.Xml.XmlQualifiedName());
            sp2IDGuid = newGuid;
            try
            {
                conn = ConnectionPool.getConnection();
                sp1 = Database.getSpace(conn, WebServiceSessionHelper.mainSchema, sp1IDGuid);
                if (sp1 == null)
                    return false;
                sp2 = Database.getSpace(conn, WebServiceSessionHelper.mainSchema, sp2IDGuid);
                if (sp2 == null)
                    return false;
            }
            catch (Exception e)
            {
                throw new SoapException(prefix + ": error getting space", new System.Xml.XmlQualifiedName(), e);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            func(prefix, u, sp1);
            func(prefix, u, sp2);
            return true;
        }


        public static bool clearCache(User u, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "clearCache", isSpaceAdmin);
            try
            {
                sp.invalidateDataCache(); // clear out memory, file, and mongodb cache for data
                sp.invalidateDashboardCache();
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("clearCache", "Error clearing the caches");
                Global.systemLog.Info("clearCache: User " + u.Username + " just failed to clear the caches - " + e.Message);
                return false;
            }
            return true;
        }

        public static string swapSpaceContents(User u, string space1ID, string space2ID)
        {
            Space sp1 = null;
            Space sp2 = null;
            CommandHelper.getSpaces(u, space1ID, space2ID, out sp1, out sp2, "swapSpaceContents", isSpaceAdmin);
            bool newRep;
            MainAdminForm maf1 = Util.loadRepository(sp1, out newRep);
			maf1.setRepositoryDirectory(sp1.Directory);
            MainAdminForm maf2 = Util.loadRepository(sp2, out newRep);
			maf2.setRepositoryDirectory(sp2.Directory);
            if (!BirstConnectUtil.equalsLocalConnections(sp1, maf1, sp2, maf2))
            {
                WebServiceSessionHelper.error("swapSpaces", "Unable to swap spaces. You are attempting to swap two spaces that are incompatible. Make sure both spaces have same birst local configuration(s).");
                Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". Make sure both spaces have same birst local configuration(s).");
                return null;
            }
            string token = WebServiceSessionHelper.getToken();
            SwapSpaces ss = new SwapSpaces(sp1, sp2, u);
            associateTokenWithBackgroundProcess(token, ss);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(ss, 0));
            return token;
        }

        class SwapSpaces : SimpleBackgroundModule
        {
            Space sp1;
            Space sp2;
            User u;

            public SwapSpaces(Space s1, Space s2, User user)
                : base()
            {
                sp1 = s1;
                sp2 = s2;
                u = user;
            }

            public override void run()
            {
                try
                {
                    Util.setLogging(u, sp1);
                    CommandHelper.swapSpaces(u, sp1, sp2);
                }
                catch (Exception e)
                {
                    error = e;
                }
            }
        }

		[Serializable]
		public class UserSpace
		{
			public string name;
			public string owner;
			public string id;

			private UserSpace() { }

			public UserSpace(string n, string o, string i)
			{
				name = n;
				owner = o;
				id = i;
			}
		}

		public static List<UserSpace> listSpaces(User u)
        {
            QueryConnection conn = null;
            List<SpaceMembership> mlist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getSpaces(conn, WebServiceSessionHelper.mainSchema, u, true);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("listSpaces", e.InnerException.Message);
            }
            finally
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }

			List<UserSpace> spaces = new List<UserSpace>();

            if (mlist != null)
            {
                foreach (SpaceMembership sm in mlist)
                {
					UserSpace sp = new UserSpace(sm.Space.Name, sm.OwnerUsername, sm.Space.ID.ToString());
					spaces.Add(sp);
                }
            }
            return spaces;
        }

        public static void swapSpaces(User u, Space sp1, Space sp2)
        {
            if (sp1 == null || sp2 == null)
            {
                WebServiceSessionHelper.warn("swapSpaces", "User " + u.Username + " failed to swap spaces " + sp1.Name + " and " + sp2.Name + " (one or both spaces do not exist)");
            }

            bool newRep;
            MainAdminForm maf1 = Util.loadRepository(sp1, out newRep);
            MainAdminForm maf2 = Util.loadRepository(sp2, out newRep);
            if (maf1 == null || maf2 == null)
            {
                WebServiceSessionHelper.warn("swapSpaces", "User " + u.Username + " failed to swap spaces " + sp1.Name + " and " + sp2.Name + " (one or both repositories do not exist)");
            }
			maf1.setRepositoryDirectory(sp1.Directory);
			maf2.setRepositoryDirectory(sp2.Directory);

            try
            {
                if (!BirstConnectUtil.equalsLocalConnections(sp1, maf1, sp2, maf2))
                {
                    WebServiceSessionHelper.error("swapSpaces", "Unable to swap spaces. You are attempting to swap two spaces that are incompatible. Make sure both spaces have same birst local configuration(s).");
                    Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". Make sure both spaces have same birst local configuration(s).");
                    return;
                }

                if (PackageUtils.checkCyclicPackageReference(null, sp1, maf1, sp2, maf2))
                {
                    WebServiceSessionHelper.error("swapSpaces", "Unable to swap spaces. You are attempting to swap two spaces that are incompatible. Found cyclic reference, atleast one package in your spaces is imported from the space you are swapping with. Please correct this before proceeding with the swap.");
                    Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". Found cyclic reference, atleast one package in your spaces is imported from the space you are swapping with.");
                    return;
                }

                if (!PackageUtils.checkIfCreatedPackagesAlign(null, sp1, maf1, sp2, maf2))
                {
                    WebServiceSessionHelper.error("swapSpaces", "Unable to swap spaces. You are attempting to swap two spaces that are incompatible. At least one Package in your spaces does not align with the metadata of the space you are swapping with. Please correct this before proceeding with the swap.");
                    Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". At least one Package in your spaces does not align with the metadata of the space you are swapping with.");
                    return;
                }

                SpaceAdminService.swapSpaces(sp1, sp2, maf1, u, false);
            }
            catch (SpaceUnavailableException spEx)
            {
                Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". " + spEx.getErrorMessage());
                WebServiceSessionHelper.error("swapSpaces", spEx.getErrorMessage());
            }
            catch (Exception ex)
            {
                string errorMsg = null;
                // XXX should really create our own exceptions
                if (ex.Message.Contains("warn:"))
                {
                    errorMsg = "Spaces not swapped: " + sp1.Name + ", " + sp2.Name + " (one of the spaces is currently in use)";
                    Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + " (one of the spaces is currently in use)");
                }
                else
                {
                    errorMsg = "Spaces not swapped: " + sp1.Name + ", " + sp2.Name + ". Please contact Birst Customer Support.";
                    Global.systemLog.Info("swapspacecontents: User " + u.Username + " just failed to swap spaces " + sp1.Name + " and " + sp2.Name + ". Please contact Birst Customer Support.");
                }
                WebServiceSessionHelper.error("swapSpaces", ex.Message);
            }
            Global.systemLog.Info("swapSpaces: User " + u.Username + " just swapped spaces " + sp1.Name + " and " + sp2.Name);
            return;
        }

        /**
         * copy a space from one ID to another ID
         * - both spaces must already exist
         * - options is a set of 'things to copy'
         * - does not deal with local ETL connections
         * 
         * - TODO: custom-subject-area-sync/copy, birstconnect-sync/copy, specific custom subject areas, specific catalog folders/files
         * - copy/sync on single file is replace (for now, sync could be dev to prod later)
         */
        public static string copySpace(User u, string spaceFromID, string spaceToID, string mode, string optString)
        {
            bool replicate = false;
            if (mode == "replicate")
            {
                replicate = true;
            }
            else if (mode != "copy")
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to copy space for " + spaceFromID + " and " + spaceToID + " (bad mode specified - " + mode + " - must be replicate or copy)");
            }
            if (optString == null || optString.Length == 0)
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabel(replicate) + " space for " + spaceFromID + " and " + spaceToID + " (no copy options specified)");
            }
            HashSet<string> options = new HashSet<string>();
            string[] optArray = optString.Split(new char[] { ';' });
            foreach (string opt in optArray)
            {
                validateCopySpaceOption(u, opt);
                options.Add(opt);
            }

            Space spFrom = null;
            Space spTo = null;
            CommandHelper.getSpaces(u, spaceFromID, spaceToID, out spFrom, out spTo, "copySpace", isSpaceAdmin);
            if (spFrom == null || spTo == null)
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabel(replicate) + " space for " + spaceFromID + " and " + spaceToID + " (one or both spaces do not exist)");
            }

            // make sure that neither space is a local ETL space (new copy space can not handle this yet)
            Dictionary<string, LocalETLConfig> fromLEConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(spFrom);
            Dictionary<string, LocalETLConfig> toLEConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(spTo);

            if (fromLEConfigs.Count > 0 || toLEConfigs.Count > 0)
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabel(replicate) + " space for " + spaceFromID + " and " + spaceToID + " (local ETL spaces not supported yet)");
            }

            // check for space availability
            // is the copied from busy in other operations except concurrent copy from. That is ok
            int sp1OpID = SpaceOpsUtils.getSpaceAvailability(spFrom);
            if (!SpaceOpsUtils.isSpaceAvailable(sp1OpID) && sp1OpID != SpaceOpsUtils.OP_COPY_FROM)
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabel(replicate) + " space for " + spaceFromID + " and " + spaceToID + " . " + SpaceOpsUtils.getAvailabilityMessage(spFrom.Name, sp1OpID));
            }

            // is the copied to busy in other operations.
            int sp2OpID = SpaceOpsUtils.getSpaceAvailability(spTo);
            if (!SpaceOpsUtils.isSpaceAvailable(sp2OpID))
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabel(replicate) + " space for " + spaceFromID + " and " + spaceToID + ". " + SpaceOpsUtils.getAvailabilityMessage(spTo.Name, sp1OpID));
            }

            // check for the processing status for from space
            if (Util.checkForPublishingStatus(spFrom))
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed " + Util.copyReplicateLabelPresentTense(replicate) + " space for " + spaceFromID + " and " + spaceToID + ". " + spFrom.Name + " is being published");
            }

            // check for the processing status for to space
            if (Util.checkForPublishingStatus(spTo))
            {
                WebServiceSessionHelper.warn("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabelPresentTense(replicate) + " space for " + spaceFromID + " and " + spaceToID + ". " + spTo.Name + " is being published");
            }

            // don't let the user try to copy data if the connection strings are different (we don't copy data across database instances)
            if (Util.includeDataStore(options) && (!Util.areSpacesInSameDatabase(spFrom, spTo)) && !options.Contains("useunloadfeature"))
            {
                WebServiceSessionHelper.error("copySpace", "User " + u.Username + " failed to " + Util.copyReplicateLabelPresentTense(replicate) + " space for " + spFrom.ID + " and " + spTo.ID + " (different connection strings)");
            }

            CopySpace cs = new CopySpace(spFrom, spTo, u, replicate, options);
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, cs);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(cs, 0));
            return token;
        }

        static private string[] validOptions = { "repository", "settings-basic", "settings-rare", "settings-sso", "settings-other", "settings-permissions", "settings-membership",
                                                 "birst-connect", "custom-subject-areas", "custom-subject-areas-no-permissions", "dashboardstyles", "salesforce", "catalog",
                                                 "datastore-warehouse_tables", "datastore-warehouse", "datastore",
                                                 "datastore-staging_tables", "datastore-staging",
                                                 "datastore-warehouse_indices", "datastore-staging_indices",
                                                 "CustomGeoMaps.xml", "DrillMaps.xml", "spacesettings.xml", "SavedExpressions.xml",
                                                 "attachments", "data", "logs", "slogs", "archive", "output", "connectors", "useunloadfeature","datastore-aggregates","datastore-txntables"
                                               };
        static private string[] validHeaders = { "birst-connect:", "custom-subject-areas:", "custom-subject-areas-no-permissions:", "catalog:", "connectors:" };

        private static void validateCopySpaceOption(User u, string option)
        {
            foreach (string valid in validOptions)
            {
                if (option == valid)
                    return;
            }
            foreach (string valid in validHeaders)
            {
                if (option.StartsWith(valid))
                    return;
            }
            WebServiceSessionHelper.error("copySpace", "User " + u.Username + " - invalid copyspace option: " + option);
        }

        class CopySpace : SimpleBackgroundModule
        {
            Space spFrom;
            Space spTo;
            User u;
            bool sync;
            HashSet<string> options;

            public CopySpace(Space fromSpace, Space toSpace, User user, bool syn, HashSet<string> opts)
                : base()
            {
                spFrom = fromSpace;
                spTo = toSpace;
                u = user;
                sync = syn;
                options = opts;
            }

            public override void run()
            {
                SpaceOperationStatus spOp1 = null;
                SpaceOperationStatus spOp2 = null;
                try
                {
                    Util.setLogging(u, spFrom);
                    // mark the spaces as unavailable
                    spOp1 = new SpaceOperationStatus(spFrom, u);
                    spOp1.logBeginStatus(SpaceOpsUtils.OP_COPY_FROM);
                    spOp2 = new SpaceOperationStatus(spTo, u);
                    spOp2.logBeginStatus(SpaceOpsUtils.OP_COPY_TO_EXISTING);                    
               
                    // the above should return information that says if either of these should be done
                    bool newRep;
                    bool buildApplication = options.Contains("repository");
                    bool saveApplication = options.Contains("repository") || options.Contains("settings-basic") || options.Contains("settings-rare");
                    MainAdminForm maf2 = null;
                    
                    if (buildApplication || saveApplication)
                    {
                        maf2 = Util.loadRepository(spTo, out newRep);
                        maf2.setRepositoryDirectory(spTo.Directory);
                    }
                   
                    //get Degenerate field value from old repository if it's set to true
                    HashSet<string> degenerateLevels = null;
                    if (buildApplication)
                    {
                        if (maf2 != null && Status.hasLoadedData(spTo, null))
                        {
                            degenerateLevels = new HashSet<string>();
                            foreach (Hierarchy h in maf2.hmodule.getHierarchies())
                            {
                                if (h.Children != null)
                                {
                                    foreach (Level l in h.Children)
                                    {
                                        if (l.Degenerate)
                                        {
                                            degenerateLevels.Add(h.Name + "~" + l.Name);
                                        }
                                    }
                                }
                            }
                        }
                    }
                                                                       
                    Database.copySpaceToExistingSpace(spFrom, spTo, u, sync, options); // this is where the magic happens 
                    maf2 = Util.loadRepository(spTo, out newRep);

                    //set original Degenerate field value after copying repository
                    if (degenerateLevels != null)
                    {                           
                       foreach (Hierarchy h in maf2.hmodule.getLatestHierarchies())
                       {
                            if (h.Children != null)
                            {
                                 foreach (Level l in h.Children)
                                 {
                                   string hlevelName = h.Name + "~" + l.Name;
                                   if (degenerateLevels.Contains(hlevelName))
                                   {
                                       l.Degenerate = true;
                                   }
                                   else if (l.Degenerate && !degenerateLevels.Contains(hlevelName))
                                   {
                                       l.Degenerate = false;
                                   }
                                 }
                            }
                        }
                     }  

                    if (buildApplication)
                    {
                        QueryConnection conn = null;
                        Dictionary<string, int> curLoadNumbers = null;
                        try
                        {
                            conn = ConnectionPool.getConnection();
                            MainAdminForm maf1 = Util.loadRepository(spFrom, out newRep);
                            // Get the current publish loadnumbers                           
                            foreach (StagingTable st in maf1.stagingTableMod.getStagingTables())
                            {
                                // If there are loadgroups other than ACORN, get them
                                if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, "ACORN") >= 0 ? 1 : 0))
                                {
                                    curLoadNumbers = Database.getCurrentLoadNumbers(conn, WebServiceSessionHelper.mainSchema, spFrom.ID);
                                    break;
                                }
                            }
                        }
                        finally
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        Util.buildApplication(maf2, WebServiceSessionHelper.mainSchema, DateTime.Now, spTo.LoadNumber, curLoadNumbers, spTo, null);
                    }
                    if (saveApplication)
                    {
                        // save the repository to get SPACE settings
                        Util.saveApplication(maf2, spTo, null, u);
                    }
                    Global.systemLog.Info("copySpace: " + Util.copyReplicateLabel(sync) + " to " + spTo.ID);
                }
                catch (Exception e)
                {
                    error = e;
                }
                finally
                {
                    // free up the unavailable spaces
                    if (spOp1 != null)
                    {
                        spOp1.logEndStatus(SpaceOpsUtils.OP_COPY_FROM);
                    }

                    if (spOp2 != null)
                    {
                        spOp2.logEndStatus(SpaceOpsUtils.OP_COPY_TO_EXISTING);
                    }                   
                }
            }
        }

        /**
         * older version of copyspace, left for backwards compatibility
         */
        public static string copySpaceContents(User u, string spaceFromID, string spaceToID)
        {
            Space spFrom = null;
            Space spTo = null;
            CommandHelper.getSpaces(u, spaceFromID, spaceToID, out spFrom, out spTo, "copySpaceContents", isSpaceAdmin);
            if (spFrom == null || spTo == null)
            {
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spaceFromID + " and " + spaceToID + " (one or both spaces do not exist)");
            }

            // check for space availability
            // is the copied from busy in other operations except concurrent copy from. That is ok
            int sp1OpID = SpaceOpsUtils.getSpaceAvailability(spFrom);
            if (!SpaceOpsUtils.isSpaceAvailable(sp1OpID) && sp1OpID != SpaceOpsUtils.OP_COPY_FROM)
            {
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " . " + SpaceOpsUtils.getAvailabilityMessage(spFrom.Name, sp1OpID));
            }

            // is the copied to busy in other operations.
            int sp2OpID = SpaceOpsUtils.getSpaceAvailability(spTo);
            if (!SpaceOpsUtils.isSpaceAvailable(sp2OpID))
            {   
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " . " + SpaceOpsUtils.getAvailabilityMessage(spTo.Name, sp1OpID));
            }

            // check for the processing status for from space
            if (Util.checkForPublishingStatus(spFrom))
            {
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " . " + spFrom.Name + " is being published");
            }

            // check for the processing status for to space
            if (Util.checkForPublishingStatus(spTo))
            {
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " . " + spTo.Name + " is being published");
            }

            if (!Util.areSpacesInSameDatabase(spFrom, spTo))
            {
                WebServiceSessionHelper.error("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " (different connection strings)");
            }

            bool newRep;
            MainAdminForm maf1 = Util.loadRepository(spFrom, out newRep);
            MainAdminForm maf2 = Util.loadRepository(spTo, out newRep);
            if (maf1 == null || maf2 == null)
            {
                WebServiceSessionHelper.warn("copySpaceContents", "User " + u.Username + " failed to copy space contents for " + spFrom.Name + " and " + spTo.Name + " (one or both repositories do not exist)");
            }
			maf1.setRepositoryDirectory(spFrom.Directory);
			maf2.setRepositoryDirectory(spTo.Directory);
            if (!BirstConnectUtil.equalsLocalConnections(spFrom, maf1, spTo, maf2))
            {
                WebServiceSessionHelper.error("copySpaceContents", "Unable to copySpaceContents. You are attempting to copySpaceContents for two spaces that are incompatible. Make sure both spaces have same birst local configuration(s).");
                Global.systemLog.Info("copySpaceContents: User " + u.Username + " just failed to copySpaceContents " + spFrom.Name + " and " + spTo.Name + ". Make sure both spaces have same birst local configuration(s).");
                return null;
            }
            CopySpaceContents csc = new CopySpaceContents(spFrom, spTo, maf1, maf2, u);
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, csc);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(csc, 0));
            return token;
        }

        class CopySpaceContents : SimpleBackgroundModule
        {
            Space spFrom;
            Space spTo;
            MainAdminForm maf1;
            MainAdminForm maf2;
            User u;

            public CopySpaceContents(Space spf, Space spt, MainAdminForm fFrom, MainAdminForm fTo, User user)
                : base()
            {
                spFrom = spf;
                spTo = spt;
                maf1 = fFrom;
                maf2 = fTo;
				maf1.setRepositoryDirectory(spf.Directory);
				maf2.setRepositoryDirectory(spTo.Directory);
                u = user;
            }
            public override void run()
            {
                try
                {
                    Util.setLogging(u, spFrom);
                    Database.copySpaceToExistingSpace(spFrom, u, spTo,maf1);
                    // Rebuild application
                    QueryConnection conn = null;
                    // Get the current publish loadnumbers
                    Dictionary<string, int> curLoadNumbers = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        foreach (StagingTable st in maf1.stagingTableMod.getStagingTables())
                        {
                            // If there are loadgroups other than ACORN, get them
                            if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, "ACORN") >= 0 ? 1 : 0))
                            {
                                curLoadNumbers = Database.getCurrentLoadNumbers(conn, WebServiceSessionHelper.mainSchema, spFrom.ID);
                                break;
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    // Copy entire repository, but preserve users and groups
                    List<Performance_Optimizer_Administration.User> ulist = maf2.usersAndGroups.usersList;
                    List<Performance_Optimizer_Administration.Group> glist = maf2.usersAndGroups.groupsList;
                    maf2 = maf1;
                    maf2.usersAndGroups.usersList = ulist;
                    maf2.usersAndGroups.groupsList = glist;
                    Util.buildApplication(maf2, WebServiceSessionHelper.mainSchema, DateTime.Now, spTo.LoadNumber, curLoadNumbers, spTo, null);
                    Util.saveApplication(maf2, spTo, null, u);
                    Global.systemLog.Info("copySpaceContents: User " + u.Username + " just copied space contents for " + spFrom.Name + " and " + spTo.Name);
                }
                catch (Exception ex)
                {
                    error = ex;
                }
            }
        }

        public static string copyCatalogDirectory(User u, string spaceFromID, string spaceToID, string directoryName)
        {
            Space sp1 = null;
            Space sp2 = null;
            CommandHelper.getSpaces(u, spaceFromID, spaceToID, out sp1, out sp2, "copyCatalogDirectory", isSpaceAdmin);

            if (sp1 == null || sp2 == null)
            {
                WebServiceSessionHelper.warn("copyCatalogDirectory", "User " + u.Username + " failed to copy the catalog directory for spaces " + sp1.Name + " and " + sp2.Name + " (one or both spaces do not exist)");
            }

            string catalogDirSpace1 = sp1.Directory + "/catalog/" + directoryName;
            string catalogDirSpace2 = sp2.Directory + "/catalog/" + directoryName;
            if (!Directory.Exists(catalogDirSpace1))
            {
                Global.systemLog.Info("copycatalogdirectory: Full Dirs: " + catalogDirSpace1 + ", " + catalogDirSpace2);
                WebServiceSessionHelper.warn("copyCatalogDirectory", "User " + u.Username + " failed to copy the catalog directory for spaces " + sp1.Name + " and " + sp2.Name + " (the source directory (" + directoryName + ") does not exist)");
            }

            string token = WebServiceSessionHelper.getToken();
            CopyDir cd = new CopyDir(catalogDirSpace1, catalogDirSpace2, sp2);
            associateTokenWithBackgroundProcess(token, cd);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(cd, 0));
            return token;
        }

        abstract class SimpleBackgroundModule : BackgroundModule
        {
            protected Exception error;
            protected Dictionary<BackgroundModuleObserver, object> observers;

            public SimpleBackgroundModule()
            {
                observers = new Dictionary<BackgroundModuleObserver, object>();
                error = null;
            }

            #region BackgroundModule Members

            public abstract void run();

            public void kill()
            {
                // can't kill
            }

            public void finish()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }

            public Status.StatusResult getStatus()
            {
                Status.StatusResult result = new Status.StatusResult(Status.StatusCode.Complete);
                if (error != null)
                {
                    result = new Status.StatusResult(Status.StatusCode.Failed);
                    result.message = error.Message;
                }

				return result;
            }

            #endregion
        }
        class CopyDir : SimpleBackgroundModule
        {
            private string fromDir;
            private string toDir;
            private Space spc;

            public CopyDir(string f, string t, Space sp) : base()
            {
                fromDir = f;
                toDir = t;
                spc = sp; 
            }


            public override void run()
            {
                try
                {
                    Util.copyDir(fromDir, toDir);
                    spc.invalidateDashboardCache();
                }
                catch (Exception e)
                {
                    error = e;
                }
            }
        }

        /*
         * get the user object from the database via the userName; if validateUserCreate is true, verify that the user performing the operation can manage the retrieved user
         */
        private static User getUser(User u, string userName, bool validateUserCreated, string prefix)
        {
            return getUser(u, userName, validateUserCreated, prefix, true);
        }

        private static User getUser(User u, string userName, bool validateUserCreated, string prefix, bool filterActiveProducts)
        {
            QueryConnection conn = null;
            User user = null;
            try
            {
                conn = ConnectionPool.getConnection();
                user = Database.getUser(conn, WebServiceSessionHelper.mainSchema, userName, filterActiveProducts);
                if (user == null)
                {
                    WebServiceSessionHelper.warn(prefix, "User " + u.Username + " tried to find a non-existent user " + userName);
                }
                if (validateUserCreated && !u.OperationsFlag)
                {
                    bool res = Database.canUserManageUser(conn, WebServiceSessionHelper.mainSchema, u.ID, user.ID);
                    if (!res)
                    {
                        WebServiceSessionHelper.warn(prefix, "User " + u.Username + " does not manage " + user.Username + ", operation not allowed");
                    }
                }
            }
            finally 
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }
            return user;
        }

        public static void resetPassword(User u, string uname, HttpServerUtility server, HttpRequest request)
        {
            WebServiceSessionHelper.error("resetpassword", "this operation no longer supported via web services or the command line");
        }

        public static void setSpaceOwner(User u, string spaceID, string uname)
        {
            Space newsp = getSpace(u, spaceID, "setSpaceOwner", isOperations);
            if (newsp == null)
            {
                WebServiceSessionHelper.warn("setSpaceOwner", "User " + u.Username + " tried to add user " + uname + " to a non-existant space " + spaceID);
            }
            User newuser = getUser(u, uname, false, "setSpaceOwner");
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("setSpaceOwner", "User " + u.Username + " tried to add a non-existant user " + uname + " to the space " + newsp.Name);
            }
            QueryConnection conn = null;
            List<SpaceMembership> mlist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getUsers(conn, WebServiceSessionHelper.mainSchema, newsp, false);

                foreach (SpaceMembership sm in mlist)
                {
                    if (sm.User == null)
                        continue;
                    if (sm.User.ID != null && newuser.ID == sm.User.ID)
                    {
                        // remove the user if already exists
                        Database.removeSpaceMembership(conn, WebServiceSessionHelper.mainSchema, newuser.ID, newsp.ID);
                    }
                    else if (sm.Owner && sm.User.ID != null && newuser.ID != sm.User.ID)
                    {
                        // remove old owner, keeping existing admin setting
                        Database.removeSpaceMembership(conn, WebServiceSessionHelper.mainSchema, sm.User.ID, newsp.ID);
                        Database.addSpaceMembership(conn, WebServiceSessionHelper.mainSchema, sm.User.ID, newsp.ID, sm.Administrator, false);
                    }
                }
                // add new owner (and set admin)
                Database.addSpaceMembership(conn, WebServiceSessionHelper.mainSchema, newuser.ID, newsp.ID, true, true);
                Database.updateMappingModifiedDate(conn, WebServiceSessionHelper.mainSchema, newsp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        /**
         * is the user the owner of this space
         * - XXX stupid way to specify owner, allows for multiple owners (but the rest of the system does not handle that condition)
         */
        private static bool isUserOwner(QueryConnection conn, string mainSchema, Space sp, User u)
        {
            bool owner = false;
            List<SpaceMembership> mlist = Database.getUsers(conn, mainSchema, sp, false);
            foreach (SpaceMembership sm in mlist)
            {
                if (sm.User == null)
                    continue;
                if (sm.User.ID != null && u.ID == sm.User.ID)
                {
                    owner = sm.Owner;
                    break;
                }
            }
            return owner;
        }

        //addusertospace [username] [spacename] {admin=[true/false]} - Adds a user to a space. Example: addusertospace pinky@acmelabs.com "Acme Labs Space");
        public static void addUserToSpace(User u, string uname, string spaceID, bool admin)
        {
            QueryConnection conn = null;
            Space newsp = getSpace(u, spaceID, "addUserToSpace", isSpaceAdmin);
            if (newsp == null)
            {
                WebServiceSessionHelper.warn("addUserToSpace", "User " + u.Username + " tried to add user " + uname + " to a non-existant space " + spaceID);
            }

            User newuser = getUser(u, uname, false, "addUserToSpace");
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("addUserToSpace", "User " + u.Username + " tried to add a non-existant user " + uname + " to the space " + newsp.Name);
            }
            
            try
            {
                conn = ConnectionPool.getConnection();
                // preserve the ownership information
                bool owner = isUserOwner(conn, WebServiceSessionHelper.mainSchema, newsp, newuser);
                // remove the space membership just to make sure we don't get duplicates
                Database.removeSpaceMembership(conn, WebServiceSessionHelper.mainSchema, newuser.ID, newsp.ID);
                Database.addSpaceMembership(conn, WebServiceSessionHelper.mainSchema, newuser.ID, newsp.ID, admin, owner);

                // Check if space-group-user mapping is done in the db. If not, populate required tables
                Util.checkAndCreateUserGroupDbMappings(newsp);
                SpaceGroup spg = Database.getGroupInfo(conn, WebServiceSessionHelper.mainSchema, newsp, Util.USER_GROUP_NAME);
                if (!UserAndGroupUtils.isUserGroupMember(spg, newuser.ID))
                {
                    Database.addUserToGroup(conn, WebServiceSessionHelper.mainSchema, newsp, spg.ID, newuser.ID, true);
                    Global.systemLog.Debug("addusertospace: User " + u.Username + " added user " + newuser.Username + " to space group " + spg.Name);
                }
            }                        
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }                   

            Global.systemLog.Info("addusertospace: User " + u.Username + " added user " + newuser.Username + " to space " + newsp.Name + ", with permissions:" + (admin ? " admin" : ""));                    
        }

        // listusersinspace [spacename] - List users in the space.\n\tExample: listusersinspace \"Acme Labs Space\"");
        public static List<string> listUsersInSpace(User u, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "listUsersInSpace", isSpaceAdmin);

            QueryConnection conn = null;
            List<SpaceMembership> mlist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                mlist = Database.getUsers(conn, WebServiceSessionHelper.mainSchema, sp, false);
            }
            catch (Exception e) {
                WebServiceSessionHelper.error("listUsersInSpace", e.InnerException.Message);
            }
            finally {
                ConnectionPool.releaseConnection(conn);
            }
            List<string> ret = new List<string>();
            if (mlist != null)
            {
                foreach (SpaceMembership sm in mlist)
                {
                    User user = sm.User;
                    if (user != null)
                    {
                        ret.Add(user.Username);
                    }
                }
            }
            return ret;
        }

        public static void removeUserFromSpace(User u, string userName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "removeUserFromSpace", isSpaceAdmin);
            User newuser = getUser(u, userName, false, "removeUserFromSpace");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                if (isUserOwner(conn, WebServiceSessionHelper.mainSchema, sp, newuser))
                {
                    WebServiceSessionHelper.error("removeUserFromSpace", "User is the owner of the space, can not remove the owner");
                }
                Database.removeSpaceMembership(conn, WebServiceSessionHelper.mainSchema, newuser.ID, sp.ID);

                // Check if space-group-user mapping is done in the db. If not, populate required tables
                Util.checkAndCreateUserGroupDbMappings(sp);
                if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
                {
                    foreach (SpaceGroup spg in sp.SpaceGroups)
                    {
                        if (UserAndGroupUtils.isUserGroupMember(spg, newuser.ID))
                        {
                            Database.removeUserFromGroup(conn, WebServiceSessionHelper.mainSchema, sp, spg.ID, newuser.ID, true);
                        }
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            Global.systemLog.Info("removeUserFromSpace: User " + u.Username + " removed user " + newuser.Username + " from space " + sp.Name);
        }


        public static void deleteGroupFromSpace(User u, string groupName, String spaceID)
        {
            Space sp = getSpace(u, spaceID, "deleteGroupFromSpace", isSpaceAdmin);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                SpaceGroup spg = Database.getGroupInfo(conn, WebServiceSessionHelper.mainSchema, sp, groupName);
                if (spg == null)
                {
                    WebServiceSessionHelper.warn("deleteGroupFromSpace", "User " + u.Username + " tried to delete a group (" + groupName + ") that does not exist in space " + sp.Name);
                }
                if (!Database.removeGroupFromSpace(conn, WebServiceSessionHelper.mainSchema, sp, spg))
                {
                    WebServiceSessionHelper.warn("deleteGroupFromSpace", "User " + u.Username + " tried to delete a group (" + groupName + ") in space " + sp.Name + " but it failed");
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("deleteGroupFromSpace: User " + u.Username + " delete group " + groupName + " from space " + sp.Name);
        }

        public static List<string> listGroupsInSpace(User u, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "listGroupsInSpace", isSpaceAdmin);
            List<SpaceGroup> groups = null;
            List<string> ret = new List<string>();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("listGroupsInSpace","error getting groups in space " + sp.Name + " by user " + u.Username + ": " + e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            foreach (SpaceGroup group in groups)
            {
                ret.Add(group.Name);
            }
            Global.systemLog.Info("listgroups: User " + u.Username + " listed groups for space " + sp.Name);
            return ret;
        }

        public static List<string> listGroupAclsInSpace(User u, string groupName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "listGroupsAclsInSpace", isSpaceAdmin);
            SpaceGroup group = null;
            QueryConnection conn = null;
            List<string> ret = new List<string>();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
                foreach (SpaceGroup gp in groups)
                {
                    if (gp.Name == groupName)
                    {
                        group = gp;
                        break;
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (group == null)
            {
                WebServiceSessionHelper.warn("listGroupAclsInSpace", "User " + u.Username + " tried to list group acls for space " + sp.Name + ", and non existent group " + groupName);
            }

            if (group != null)
            {
                List<int> ACLIds = group.ACLIds;
                if (ACLIds != null)
                {
                    foreach (int aclId in ACLIds)
                    {
                        ret.Add(ACLDetails.getACLTag(aclId));
                    }
                }
            }
            Global.systemLog.Info("listGroupAclsInSpace: User " + u.Username + " listed group acls for space " + sp.Name + ", and group " + groupName);
            return ret;
        }

        public static void addAclToGroupInSpace(User u, string groupName, string aclTag, string spaceID)
        {
            if (!u.OperationsFlag)
            {
                WebServiceSessionHelper.warn("addAclToGroupInSpace", "Only available to operations staff.");
            }
            Space sp = getSpace(u, spaceID, "addAclToGroupInSpace", isSpaceAdmin);
            SpaceGroup group = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
                foreach (SpaceGroup gp in groups)
                {
                    if (gp.Name == groupName)
                    {
                        group = gp;
                        break;
                    }
                }
                if (group == null)
                {
                    WebServiceSessionHelper.warn("addAclToGroupInSpace", "Could not find group named " + groupName + " in space " + sp.Name);
                }

                int aclId = ACLDetails.getACLID(aclTag);
                if (aclId == -1)
                {
                    WebServiceSessionHelper.warn("addAclToGroupInSpace", "User " + u.Username + " tried to add a bad acl " + aclTag + " to space " + sp.Name + ", and group " + groupName);
                }
                List<int> ACLIds = group.ACLIds;
                if (ACLIds == null || !ACLIds.Contains(aclId))
                {
                    Database.addACLToGroup(conn, WebServiceSessionHelper.mainSchema, sp, group.ID, aclId);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("addAclToGroupInSpace: User " + u.Username + " added acl " + aclTag+ " to space " + sp.Name + ", and group " + groupName);
        }

        public static void removeAclFromGroupInSpace(User u, string groupName, string aclTag, string spaceID)
        {
            if (!u.OperationsFlag)
            {
                WebServiceSessionHelper.warn("removeAclFromGroupInSpace", "Only availabel to operations staff.");
            }
            Space sp = getSpace(u, spaceID, "removeAclFromGroupInSpace", isSpaceAdmin);
            SpaceGroup group = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
                foreach (SpaceGroup gp in groups)
                {
                    if (gp.Name == groupName)
                    {
                        group = gp;
                        break;
                    }
                }

                if (group != null)
                {
                    int aclId = ACLDetails.getACLID(aclTag);
                    if (aclId == -1)
                    {
                        WebServiceSessionHelper.warn("removeAclFromGroupInSpace", "User " + u.Username + " tried to remove a bad acl " + aclTag + " from space " + sp.Name + ", and group " + groupName);
                    }
                    List<int> ACLIds = group.ACLIds;
                    if (ACLIds != null && ACLIds.Contains(aclId))
                    {
                        Database.removeACLFromGroup(conn, WebServiceSessionHelper.mainSchema, sp, group.ID, aclId);
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("removeAclFromGroupInSpace: User " + u.Username + " removed acl " + aclTag + " from space " + sp.Name + ", and group " + groupName);
        }

        public static void addUserToGroupInSpace(User u, string userName, string groupName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "addUserToGroupInSpace", isSpaceAdmin);
            QueryConnection conn = null;
            User newuser = null;
            List<SpaceMembership> mlist = null;
            try
            {
                conn = ConnectionPool.getConnection();
                newuser = Database.getUser(conn, WebServiceSessionHelper.mainSchema, userName);
                mlist = Database.getUsers(conn, WebServiceSessionHelper.mainSchema, sp, false);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " tried to add a non-existant user " + userName + " to the space " + sp.Name);
            }

            // determine if the user is a member of the space, necessary for adding to the group
            bool inspace = false;
            foreach (SpaceMembership sm in mlist)
            {
                if (sm.User == null)
                    continue;
                if (sm.User.ID != null && newuser.ID == sm.User.ID)
                {
                    inspace = true;
                    break;
                }
            }

            if (!inspace)
            {
                WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " could not add user " + newuser.Username + " to group " + groupName + " in space " + sp.Name + ": user not a member of the space");
            }

            // Check if space-group-user mapping is done in the db. If not, populate required tables
            Util.checkAndCreateUserGroupDbMappings(sp);

            try
            {
                conn = ConnectionPool.getConnection();
                SpaceGroup toAddUserGroup = Database.getGroupInfo(conn, WebServiceSessionHelper.mainSchema, sp, groupName);
                if (toAddUserGroup == null)
                {
                    WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " could not add user " + newuser.Username + " to group " + groupName + " in space " + sp.Name + ": group not found");
                }

                if (!u.OperationsFlag && toAddUserGroup.InternalGroup)
                {
                    WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " could not add user " + newuser.Username + " to group " + groupName + " in space " + sp.Name + ": can not add to internal Birst groups");
                }

                if (Util.isOwnerGroup(toAddUserGroup))
                {
                    WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " could not add user " + newuser.Username + " to group " + groupName + " in space " + sp.Name + ": can not add to internal Birst group (OWNER$)");
                }

                Database.addUserToGroup(conn, WebServiceSessionHelper.mainSchema, sp, toAddUserGroup.ID, newuser.ID, true);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            Global.systemLog.Info("addUserToGroupInSpace: User " + u.Username + " added user " + newuser.Username + " to group " + groupName + " in space " + sp.Name);
        }

        public static void dropUserFromGroupInSpace(User u, string userName, string groupName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "dropUserFromGroupInSpace", isSpaceAdmin);
            User newuser = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                newuser = Database.getUser(conn, WebServiceSessionHelper.mainSchema, userName);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("dropUserFromGroupInSpace", "User " + u.Username + " tried to drop a non-existant user " + userName + " from the group " + groupName);
            }

            // Check if space-group-user mapping is done in the db. If not, populate required tables
            Util.checkAndCreateUserGroupDbMappings(sp);
            try
            {
                conn = ConnectionPool.getConnection();
                SpaceGroup spg = Database.getGroupInfo(conn, WebServiceSessionHelper.mainSchema, sp, groupName);
                if (spg == null)
                {
                    WebServiceSessionHelper.warn("dropUserFromGroupInSpace", "User " + u.Username + " could not drop " + newuser.Username + " from group " + groupName + " in space " + sp.Name + ": group not found");
                }
                if (!u.OperationsFlag && spg.InternalGroup)
                {
                    WebServiceSessionHelper.warn("addUserToGroupInSpace", "User " + u.Username + " could not remove user " + newuser.Username + " from group " + groupName + " in space " + sp.Name + ": can not remove from internal Birst groups");
                }

                if (UserAndGroupUtils.isUserGroupMember(spg, newuser.ID))
                {
                    Database.removeUserFromGroup(conn, WebServiceSessionHelper.mainSchema, sp, spg.ID, newuser.ID, true);
                }
                else
                {
                    WebServiceSessionHelper.warn("dropUserFromGroupInSpace", "User " + u.Username + " could not drop " + newuser.Username + " from group " + groupName + " in space " + sp.Name + ": user not member of group");
                }

            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            Global.systemLog.Info("dropUserFromGroupInSpace: User " + u.Username + " dropped user " + newuser.Username + " from group " + groupName + " in space " + sp.Name);
        }

        public static List<string> listUsersInGroupInSpace(User u, string groupName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "listUsersInGroupInSpace", isSpaceAdmin);
            SpaceGroup group = null;
            QueryConnection conn = null;
            List<string> ret = new List<string>();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
                foreach (SpaceGroup gp in groups)
                {
                    if (gp.Name == groupName)
                    {
                        group = gp;
                        break;
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (group != null)
            {
                List<User> users = group.GroupUsers;
                if (users != null)
                {
                    foreach (User user in users)
                    {
                        ret.Add(user.Username);
                    }
                }
            }
            else
            {
                WebServiceSessionHelper.warn("listUsersInGroupInSpace", "User " + u.Username + " tried to list group users for space " + sp.Name + ", and non existent group " + groupName);
            }
            Global.systemLog.Info("listUsersInGroupInSpace: User " + u.Username + " listed group users for space " + sp.Name + ", and group " + groupName);
            return ret;
        }

        public static List<string> listUserGroupMembership(User u, string spaceID, string userName, bool includeInternalGroups)
        {
            Space sp = getSpace(u, spaceID, "listUserGroupMembership", isSpaceAdmin);
            QueryConnection conn = null;
            List<string> ret = new List<string>();
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceGroup> groups = Database.getSpaceGroups(conn, WebServiceSessionHelper.mainSchema, sp);
                List<User> users;
                foreach (SpaceGroup group in groups)
                {
                    if (group.InternalGroup)
                    {
                        if (!includeInternalGroups)
                        {
                            continue;
                        }
                        else if (includeInternalGroups && !group.Name.Equals(USER_GROUP, StringComparison.CurrentCultureIgnoreCase))
                        {
                            continue;
                        }
                    }
                    users = group.GroupUsers;
                    foreach (User user in users)
                    {
                        if (user.Username == userName)
                        {
                            ret.Add(group.Name);
                            break;
                        }
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("listUserGroupMembership: User " + u.Username + " listed group membership for space " + sp.Name);
            return ret;
        }

        public struct KeyValueStruct
        {
            private string _key;
            private string _val;

            public string Key
            {
                get { return _key; }
                set { _key = value; }
            }
            public string Value
            {
                get { return _val; }
                set { _val = value; }
            }

            public KeyValueStruct(string key, string value)
            {
                _key = key;
                _val = value;
            }
        }

        public static void addOpenID(User u, string userName, string openID)
        {
            User user = getUser(u, userName, true, "addOpenID");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                if (!Database.addOpenIDForUserId(conn, WebServiceSessionHelper.mainSchema, user.ID, openID))
                {
                    WebServiceSessionHelper.warn("addOpenID", "OpenID already in use: " + openID);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("addOpenID: OpenID added: " + userName + ", " + openID);
        }

        public static void removeOpenID(User u, string userName, string openID)
        {
            User user = getUser(u, userName, true, "removeOpenID");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.removeOpenIDForUserId(conn, WebServiceSessionHelper.mainSchema, user.ID, openID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("removeOpenID: OpenID removed: " + userName + ", " + openID);
        }

        public static List<string> listOpenIDs(User u, string userName)
        {
            User user = getUser(u, userName, true, "listOpenIDs");
            QueryConnection conn = null;
            List<string> ids = null;
            try
            {
                conn = ConnectionPool.getConnection();
                ids = Database.getOpenIDsForUserId(conn, WebServiceSessionHelper.mainSchema, user.ID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("listOpenIDs: " + userName);
            return ids;
        }

        public static string getAuthDescription(User u, string accountID)
        {
            Guid id = Guid.Empty;
            if (!Guid.TryParse(accountID, out id))
                WebServiceSessionHelper.warn("getAuthDescription", "Bad accountID: " + accountID);

            if (id != u.AdminAccountId && !u.OperationsFlag)
                WebServiceSessionHelper.warn("getAuthDescription", "User not allowed to perform this operation (does not manage the account or is not in operations)");

            QueryConnection conn = null;
            string result = null;
            try
            {
                conn = ConnectionPool.getConnection();
                AccountDetails details = Database.getAccountDetails(conn, WebServiceSessionHelper.mainSchema, id, u);
                if (details != null && details.isRADIUS())
                {
                    string radiusHost = details.getRADIUSHost();
                    string radiusSecret = details.getRADIUSSharedSecret();
                    if (radiusHost == null || radiusSecret == null)
                        WebServiceSessionHelper.warn("getAuthDescription", "AuthType set to RADIUS for account, but host/secret null");

                    result = "RADIUS Authentication - Host: " + radiusHost + ", Secret: " + radiusSecret.Substring(0, 1) + "******";
                }
                else
                {
                    result = "Birst Authentication (default)";
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("getAuthDescription: " + accountID);
            return result;
        }

        public static void setAuthentication(User u, string accountID, string type, string host, string secret)
        {
            Guid id = Guid.Empty;
            if (!Guid.TryParse(accountID, out id))
                WebServiceSessionHelper.warn("setAuthentication", "Bad accountID: " + accountID);

            if (id != u.AdminAccountId && !u.OperationsFlag)
                WebServiceSessionHelper.warn("setAuthentication", "User not allowed to perform this operation (does not manage the account or is not in operations)");

            if (type != "BIRST" && type != "RADIUS")
                WebServiceSessionHelper.warn("setAuthentication", "Invalid authentication type: " + type);

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.setOverrideParameter(conn, WebServiceSessionHelper.mainSchema, id, "AuthType", type);
                Database.setOverrideParameter(conn, WebServiceSessionHelper.mainSchema, id, "RADIUS.Host", host);
                Database.setOverrideParameter(conn, WebServiceSessionHelper.mainSchema, id, "RADIUS.SharedSecret", secret);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setAuthentication: " + accountID);
        }

        public static PasswordPolicy getPasswordPolicy(User u, string accountID)
        {
            Guid id = Guid.Empty;
            if (!Guid.TryParse(accountID, out id))
                WebServiceSessionHelper.warn("getPasswordPolicy", "Bad accountID: " + accountID);

            if (id != u.AdminAccountId && !u.OperationsFlag)
                WebServiceSessionHelper.warn("getPasswordPolicy", "User not allowed to perform this operation (does not manage the account or is not in operations)");

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                return Database.getPasswordPolicy(conn, WebServiceSessionHelper.mainSchema, id);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setPasswordPolicy(User u, string accountID, string options, string description, string regularExpression)
        {
            Guid id = Guid.Empty;
            if (!Guid.TryParse(accountID, out id))
                WebServiceSessionHelper.error("setPasswordPolicy", "Bad accountID: " + accountID);

            if (id != u.AdminAccountId && !u.OperationsFlag)
                WebServiceSessionHelper.error("setPasswordPolicy", "User not allowed to perform this operation (user does not manage the account or is not in operations)");

            if (id == Guid.Empty && !u.OperationsFlag)
                WebServiceSessionHelper.error("setPasswordPolicy", "User not allowed to perform this operation (user is not in operations)");

            PasswordPolicy policy = new PasswordPolicy();
            policy.description = (description != null && description.Trim().Length == 0) ? null : description;
            policy.regularExpression = (regularExpression != null && regularExpression.Trim().Length == 0) ? null : regularExpression;

            options = (options != null && options.Trim().Length == 0) ? null : options;

            if (policy.regularExpression == null && options == null)
                WebServiceSessionHelper.error("setPasswordPolicy", "No password policy specified");

            if (options != null)
            {
                string[] tokens = options.ToLower().Split(new char[] { ',' });
                for (int i = 0; i < tokens.Length; i++)
                {
                    string[] parts = tokens[i].Split(new char[] { '=' });
                    if (parts.Length != 2)
                        WebServiceSessionHelper.error("setPasswordPolicy", "Bad password policy specification (no '='): " + tokens[i]);
                    switch (parts[0])
                    {
                        case "minlength":
                            policy.minLength = Int32.Parse(parts[1]);
                            break;
                        case "maxlength":
                            policy.maxLength = Int32.Parse(parts[1]);
                            break;
                        case "historylength":
                            policy.historyLength = Int32.Parse(parts[1]);
                            break;
                        case "expirationdays":
                            policy.expirationDays = Int32.Parse(parts[1]);
                            break;
                        case "containsmixedcase":
                            policy.containsMixedCase = Boolean.Parse(parts[1]);
                            break;
                        case "containsspecial":
                            policy.containsSpecial = Boolean.Parse(parts[1]);
                            break;
                        case "containsnumeric":
                            policy.containsNumeric = Boolean.Parse(parts[1]);
                            break;
                        case "doesnotcontainusername":
                            policy.doesnotContainUsername = Boolean.Parse(parts[1]);
                            break;
                        case "changeonfirstuse":
                            policy.changeOnFirstUse = Boolean.Parse(parts[1]);
                            break;
                        case "hashalgorithm":
                            if (parts[1] == "base")
                            {
                                WebServiceSessionHelper.error("setPasswordPolicy", "Bad hash algorithm specification - 'base' no longer supported, we recommend: pbkdf2:hmac-sha1:10000");
                            }
                            else if (parts[1].StartsWith("bcrypt:"))
                            {
                                string[] subparts = parts[1].Split(new char[] { ':' });
                                if (subparts.Length == 2)
                                {
                                    int costFactor = Int32.Parse(subparts[1]);
                                    if (costFactor >= 10 && costFactor <= 17)
                                        policy.hashAlgorithm = parts[1];
                                    else
                                        WebServiceSessionHelper.error("setPasswordPolicy", "Bad cost factor for BCrypt: " + parts[1] + " (should be between 10 and 17)");
                                }
                                else
                                    WebServiceSessionHelper.error("setPasswordPolicy", "Bad BCrypt specification, missing cost factor: " + parts[1] + " (should be between 10 and 17)");
                            }
                            else if (parts[1].StartsWith("pbkdf2:"))
                            {
                                string[] subparts = parts[1].Split(new char[] { ':' });
                                if (subparts.Length == 3)
                                {
                                    string hashname = subparts[1];
                                    switch (hashname)
                                    {
                                        case "hmac-sha1":
                                        case "hmac-sha256":
                                        case "hmac-sha384":
                                        case "hmac-sha512":
                                            break;
                                        default:
                                            WebServiceSessionHelper.error("setPasswordPolicy", "Bad pbkdf2 specification, bad prf name: " + parts[1] + " (should be one of hmac-sha1, hmac-sha256, hmac-sha384, hmac-sha512)");
                                            break;
                                    }
                                    int iterations = Int32.Parse(subparts[2]);
                                    if (iterations >= 10000 && iterations <= 100000)
                                        policy.hashAlgorithm = parts[1];
                                    else
                                        WebServiceSessionHelper.error("setPasswordPolicy", "Bad pbkdf2 specification, bad cost factor: " + parts[1] + " (should be between 10000 and 100000)");
                                }
                                else
                                    WebServiceSessionHelper.error("setPasswordPolicy", "Bad pbkdf2 specification: " + parts[1]);
                            }
                            else
                                WebServiceSessionHelper.error("setPasswordPolicy", "Bad hash algorithm specification: " + parts[1]);
                            break;
                        case "maxfailedloginattempts":
                            policy.maxFailedLoginAttempts = Int32.Parse(parts[1]);
                            break;
                        case "failedloginwindow":
                            policy.failedLoginWindow =  Int32.Parse(parts[1]);
                            break;
                        default:
                            WebServiceSessionHelper.error("setPasswordPolicy", "Bad password policy specification (unknown option): " + parts[0]);
                            break;
                    }
                }
                if (policy.minLength < 6)
                    WebServiceSessionHelper.error("setPasswordPolicy", "The minimum length of the passwords must be atleast 6 characters (required for TRUSTe certification)");
                if (policy.maxFailedLoginAttempts > 10)
                    WebServiceSessionHelper.error("setPasswordPolicy", "The maximum failed login limit should always be less than or equal to 10 (required for TRUSTe certification)");
            }
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();

                Database.setPasswordPolicy(conn, WebServiceSessionHelper.mainSchema, id, policy);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setUserDefaultSpace(User u, string userName, string spaceID, bool dashboard)
        {
            User user = getUser(u, userName, true, "setUserDefaultSpace");
            Space sp = getSpace(u, spaceID, "setUserDefaultSpace", isSpaceAdmin);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                user.DefaultSpace = sp.Name;
                user.DefaultSpaceId = sp.ID;
                user.DefaultDashboards = dashboard;
                Database.updateUser(conn, WebServiceSessionHelper.mainSchema, user);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setUserDefaultSpace: default space to " + spaceID + " for user " + userName + (dashboard ? " (default to dashboards)" : ""));
        }

        public static void setUserPassword(User u, string userName, string password)
        {
            User user = getUser(u, userName, true, "setUserPassword");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                PasswordPolicy policy = Database.getPasswordPolicy(conn, WebServiceSessionHelper.mainSchema, user.ManagedAccountId);
                if (policy != null)
                {
                    List<string> history = Database.getPasswordHistory(conn, WebServiceSessionHelper.mainSchema, user.ID);
                    if (!policy.isValid(password, userName, history))
                        throw new Exception(policy.description);
                }
                MembershipUser mu = Membership.GetUser(userName, false);
                string tempPassword = mu.ResetPassword();
                mu.ChangePassword(tempPassword, password);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("setUserPassword", "error setting the user password for " + userName + " by user " + u.Username + ": " + e.InnerException.Message); // XXX not sure about innerexception here
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setUserPassword: User " + u.Username + " set the password for " + userName);
        }

        public static List<string> listReleases(User u)
        {
            QueryConnection conn = null;
            List<string> results = new List<string>();
            try
            {
                conn = ConnectionPool.getConnection();
                List<ReleaseInfo> releases = Database.getReleasesInfo(conn, WebServiceSessionHelper.mainSchema);
                if (releases != null)
                {
                    foreach (ReleaseInfo release in releases)
                    {
                        results.Add(release.Name);
                    }
                }
                Global.systemLog.Info("listReleases");
                return results;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static string getUserRelease(User u, string userName)
        {
            User user = getUser(u, userName, true, "getUserRelease");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                ReleaseInfo info = Database.getReleaseInfoForUser(conn, WebServiceSessionHelper.mainSchema, user);
                Global.systemLog.Info("getUserRelease: " + userName);
                return (info != null) ? info.Name : null;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setUserRelease(User u, string userName, string release)
        {
            User user = getUser(u, userName, true, "setUserRelease");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                ReleaseInfo info = Database.getReleaseInfoByName(conn, WebServiceSessionHelper.mainSchema, release);
                if (info == null)
                {
                    WebServiceSessionHelper.error("setUserRelease", "release " + release + " does not exist");
                }
                Database.updateUserReleaseType(conn, WebServiceSessionHelper.mainSchema, user.ID, info.ID);
                user.ReleaseType = info.ID;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setUserRelease: release set: " + userName + ", " + release);
        }

        public static void setUserRenderType(User u, string userName, string type)
        {
            User user = getUser(u, userName, true, "setUserRenderType");
            string[] userIds = new string[1];
            userIds[0] = user.ID.ToString();
            bool toHTML = false;
            if (type.ToLower() == "html")
                toHTML = true;
            else if (type.ToLower() == "flash")
                toHTML = false;
            else
                WebServiceSessionHelper.error("setUserRenderType", "render type " + type + " does not exist (html or flash)");
            UserAdministration.setRenderType(u, userIds, toHTML);
            Global.systemLog.Info("setUserRenderType: render type set: " + userName + ", " + (toHTML ? "html" : "flash"));
        }

        public static void setDenyAddSpace(User u, string userName, bool disable)
        {
            User user = getUser(u, userName, true, "setDenyAddSpace");
            string[] userIds = new string[1];
            userIds[0] = user.ID.ToString();
            UserAdministration.setDenyAddSpace(u, userIds, disable);
            Global.systemLog.Info("setDenyAddSpace: set: " + userName + ", " + (disable.ToString()));
        }        

        public static void enableUser(User u, string userName, bool enable)
        {
            User user = getUser(u, userName, true, "enableUser", false);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.enableUser(conn, WebServiceSessionHelper.mainSchema, user, enable);
                if (enable)
                    Database.updateLastLogin(u.Username); // so that reenabling them after a disable does not trigger the auto disable functionality
                u.Disabled = !enable;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (enable)
                Global.systemLog.Info("enableUser: user " + user.ID + " enabled");
            else
                Global.systemLog.Info("enableUser: user " + user.ID + " disabled");
        }


        public static void unlockUser(User u, string userName)
        {
            User user = getUser(u, userName, true, "unlockUser");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.unlockUser(conn, WebServiceSessionHelper.mainSchema, user.ID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("unlockUser: user " + user.ID);
        }

        public static void deleteUser(User u, string userName)
        {
            User user = getUser(u, userName, true, "deleteUser");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                // make sure the user is not the owner of a space
                List<Acorn.SpaceMembership> spaces = Acorn.Database.getSpaces(conn, WebServiceSessionHelper.mainSchema, user, true);
                foreach (Acorn.SpaceMembership sm in spaces)
                {
                    if (sm.Owner)
                        WebServiceSessionHelper.error("deleteUser", "Can not delete a user that currently owns a space: " + sm.Space.ID + "/" + sm.Space.Name);
                }
                Database.deleteUser(conn, WebServiceSessionHelper.mainSchema, user);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("deleteUser: user " + u.ID);
        }

        public static void enableAccount(User u, string accountID, bool enable)
        {
            if (!u.OperationsFlag)
                WebServiceSessionHelper.error("enableAccount", "Do not have sufficient rights");
            Guid id = Guid.Empty;
            if (!Guid.TryParse(accountID, out id))
                WebServiceSessionHelper.warn("enableAccount", "Bad accountID: " + accountID);

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.enableAccount(conn, WebServiceSessionHelper.mainSchema, id, enable);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (enable)
                Global.systemLog.Info("enableAccount: accountID " + accountID + " enabled");
            else
                Global.systemLog.Info("enableAccount: accountID " + accountID + " disabled");
        }

        public static void addProxyUser(User u, string userName, string proxyUserName, DateTime expiration, bool repAdmin)
        {
            if (!u.OperationsFlag && repAdmin)
                WebServiceSessionHelper.error("addProxyUser", "Do not have sufficient rights");
            User user = getUser(u, userName, false, "addProxyUser");
            User puser = getUser(u, proxyUserName, true, "addProxyUser");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.addProxyUser(conn, WebServiceSessionHelper.mainSchema, user, puser, repAdmin, expiration);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (u.OperationsFlag)
                Global.systemLog.Info("addProxyUser: Proxy user added: " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd") + ", " + repAdmin);
            else
                Global.systemLog.Info("addProxyUser: Proxy user added: " + userName + ", " + proxyUserName + ", " + expiration.ToString("yyyy-MM-dd"));
        }

        public static void removeProxyUser(User u, string userName, string proxyUserName)
        {
            QueryConnection conn = null;
            User user = getUser(u, userName, false, "removeProxyUser");
            User puser = getUser(u, proxyUserName, true, "removeProxyUser");
            try
            {
                conn = ConnectionPool.getConnection();
                if (!Database.removeProxyUser(conn, WebServiceSessionHelper.mainSchema, user, puser))
                {
                    WebServiceSessionHelper.warn("removeProxyUser", "Proxy user not removed: " + userName + ", " + proxyUserName + " (no existing entry)");
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("removeProxyUser: Proxy user removed: " + userName + ", " + proxyUserName);
        }

        public static List<string> listManagedUsers(User u)
        {
            List<string> userNames = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                userNames = Database.getManagedUsernames(conn, WebServiceSessionHelper.mainSchema, u.ID);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("listCreatedUsers", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("listcreatedusers: User " + u.Username + " listed created users");
            return userNames;
        }

        public static List<string> listAllowedIPs(User u, String userName)
        {
            List<string> allowedIPs = null;
            User user = getUser(u, userName, true, "listAllowedIPs");
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                allowedIPs = Database.getAllowedIPs(conn, WebServiceSessionHelper.mainSchema, user.AdminAccountId, user.ID);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("listAllowedIPs", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            Global.systemLog.Info("listAllowedIPs: User " + u.Username + " listed allowed IP addresses for - " + userName);
            return allowedIPs;
        }

        public static void addAllowedIP(User u, string userName, string ipaddress)
        {
            QueryConnection conn = null;
            User user = getUser(u, userName, true, "addAllowedIps");
            try
            {
                conn = ConnectionPool.getConnection();
                Database.addAllowedIP(conn, WebServiceSessionHelper.mainSchema, Guid.Empty, user.ID, ipaddress);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("addAllowedIP", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("addAllowedIP: Allowed IP added: " + userName + ", " + ipaddress);
        }

        public static void removeAllowedIP(User u, string userName, string ipaddress)
        {
            QueryConnection conn = null;
            User user = getUser(u, userName, true, "removeAllowedIP");
            try
            {
                conn = ConnectionPool.getConnection();
                if (!Database.removeAllowedIP(conn, WebServiceSessionHelper.mainSchema, Guid.Empty, user.ID, ipaddress))
                {
                    WebServiceSessionHelper.warn("removeAllowedIP", "Allowed IP not removed: " + userName + ", " + ipaddress + " (no existing entry)");
                }
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("removeAllowedIP", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("removeAllowedIP: Allowed IP removed: " + userName + ", " + ipaddress);
        }

        public static List<string> listAllowedIPAddrsForAccount(User u, string accountId)
        {
            isAnAccountAdmin("listAllowedIPAddrsForAccount", u);

            Guid acctId = u.AdminAccountId;
            if (u.OperationsFlag && accountId == null)
                WebServiceSessionHelper.warn("listAllowedIPAddrsForAccount", "Operations flag set and not account specified");
            if (u.OperationsFlag)
                acctId = new Guid(accountId);

            List<string> allowedIPs = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                allowedIPs = Database.getAllowedIPs(conn, WebServiceSessionHelper.mainSchema, acctId, Guid.Empty);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("listAllowedIPAddrsForAccount", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            Global.systemLog.Info("listAllowedIPAddrsForAccount: User " + u.Username + " listed allowed IP addresses for - " + acctId);
            return allowedIPs;
        }

        public static void addAllowedIPAddrForAccount(User u, string accountId, string ipaddress)
        {
            isAnAccountAdmin("addAllowedIPAddrForAccount", u);

            Guid acctId = u.AdminAccountId;
            if (u.OperationsFlag && accountId == null)
                WebServiceSessionHelper.warn("addAllowedIPAddrForAccount", "Allowed IP address not added: " + acctId + ", " + ipaddress + " (operations flag set and not account specified)");
            if (u.OperationsFlag)
                acctId = new Guid(accountId);

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.addAllowedIP(conn, WebServiceSessionHelper.mainSchema, acctId, Guid.Empty, ipaddress);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("addAllowedIPAddrForAccount", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("addAllowedIPAddrForAccount: Allowed IP added: " + acctId + ", " + ipaddress);
        }

        public static void removeAllowedIPAddrForAccount(User u, string accountId, string ipaddress)
        {
            isAnAccountAdmin("removeAllowedIPAddrForAccount", u);

            Guid acctId = u.AdminAccountId;
            if (u.OperationsFlag && accountId == null)
                WebServiceSessionHelper.warn("removeAllowedIPAddrForAccount", "Allowed IP address not removed: " + acctId + ", " + ipaddress + " (operations flag set and not account specified)");
            if (u.OperationsFlag)
                acctId = new Guid(accountId);

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                if (!Database.removeAllowedIP(conn, WebServiceSessionHelper.mainSchema, acctId, Guid.Empty, ipaddress))
                {
                    WebServiceSessionHelper.warn("removeAllowedIPAddrForAccount", "Allowed IP not removed: " + acctId + ", " + ipaddress + " (no existing entry)");
                }
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("removeAllowedIPAddrForAccount", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("removeAllowedIPAddrForAccount: Allowed IP removed: " + acctId + ", " + ipaddress);
        }

        public static void setVariableInSpace(User u, string varName, string query, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "setVariableInSpace", isSpaceAdmin);
            bool newRep = false;
            MainAdminForm maf = Util.loadRepository(sp, out newRep);
			maf.setRepositoryDirectory(sp.Directory);
            Variable foundv = null;
            if (maf.variableMod != null && maf.getVariables() != null)
                foreach (Variable sv in maf.getVariables())
                {
                    if (varName == sv.Name)
                    {
                        foundv = sv;
                        break;
                    }
                }
            if (foundv != null)
            {
                foundv.Query = query;
                maf.updateVariable(foundv, u.Username);
                Util.saveApplication(maf, sp, null, u);
            }
            else
            {
                WebServiceSessionHelper.warn("setVariableInSpace", "variable " + varName + " not found in space " + sp.Name);
            }
        }

        internal static List<string> listProxyUsers(User u, string userName)
        {
            QueryConnection conn = null;
            List<string> ret = new List<string>();
            User user = getUser(u, userName, true, "listProxyUsers");
            try
            {
                conn = ConnectionPool.getConnection();
                ret = Database.getProxyUsernames(conn, WebServiceSessionHelper.mainSchema, user.ID);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.warn("listProxyUsers", e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("listProxyUsers: listed: " + userName);
            return ret;
        }

        internal static void setSpaceSSOPassword(User u, string spaceID, string password)
        {
            Space sp = getSpace(u, spaceID, "setSpaceSSOPassowrd", isSpaceAdmin);
            sp.SSOPassword = password;
            sp.SSO = true;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        internal static string getSpaceJNLPFile(User u, string spaceID, string configfile, HttpContext context)
        {
            Space sp = getSpace(u, spaceID, "getSpaceJNLPFile", isSpaceAdmin);
            return Util.getSpaceJNLPFile(u, sp, context.Server.MapPath("~/"), Util.getRequestBaseURL(context.Request), configfile);
        }

        internal static SpaceStatistics getSpaceStatistics(User user, string spaceID)
        {
            Space sp = getSpace(user, spaceID, "getSpaceDetails", isSpaceAdmin);
            SpaceStatistics result = new SpaceStatistics();

            // Get number of users for a space.
            List<string> userList = listUsersInSpace(user, spaceID);
            result.UserCount = (userList != null) ? userList.Count : 0;

            // Get total size used for a space.
            AdminService adminService = new AdminService();
            IDictionary<string, string> userPref = ManagePreferences.getPreference(null, user.ID, sp.ID, "Locale");
            CultureInfo locale = null;
            if (userPref != null && userPref["Locale"] != null)
                locale = CultureInfo.GetCultureInfoByIetfLanguageTag(userPref["Locale"]);
            result.SpaceSize = Util.formatBytes(getSpaceSize(sp), locale);

            return result;
        }

        internal static void addGroupToSpace(User u, string groupName, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "addGroupToSpace", isSpaceAdmin);
            if (UserAndGroupUtils.isGroupNameDuplicate(sp, null, groupName))
            {
                WebServiceSessionHelper.warn("addGroupToSpace", "User " + u.Username + " tried to create a group (" + groupName + ") that already exists in the space (" + spaceID + ")");
            }

            List<string> ret = new List<string>();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.addGroupToSpace(conn, WebServiceSessionHelper.mainSchema, sp, groupName, false);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("addGroupToSpace", "error adding group to space " + sp.Name + " by user " + u.Username + ": " + e.InnerException.Message);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("addGroupToSpace: User " + u.Username + " added group " + groupName + " for space " + sp.Name);
        }

        internal static long getSpaceSize(Space sp)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
                return Database.getSpaceSize(conn, mainSchema, sp);
            }
            catch (Exception e)
            {
                Global.systemLog.Debug(e.Message);
                return 0;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        internal static void addUser(User u, string uname, string additionalParams)
        {
            isAnAccountAdmin("addUser", u);

            if (!Command.isValidUsername(uname))
            {
                WebServiceSessionHelper.warn("addUser", "User " + u.Username + " tried to create a username with invalid characters '" + uname + "'");
            }
            MembershipUser mu = Membership.GetUser(uname);
            if (mu == null)
            {
                int pMinRequiredPasswordLength = System.Web.Security.Membership.MinRequiredPasswordLength;
                int minRequiredNonAlphanumericCharacters = System.Web.Security.Membership.MinRequiredNonAlphanumericCharacters;
                string pwd = System.Web.Security.Membership.GeneratePassword(pMinRequiredPasswordLength, minRequiredNonAlphanumericCharacters);
                MembershipCreateStatus mstatus;
                string fname = null;
                string lname = null;
                string email = uname; // email defaults to user name
                List<string> tokens = additionalParams == null ? new List<string>() : Command.gettokens(additionalParams);
                for (int i = 0; i < tokens.Count; i++)
                {
                    string t = tokens[i].ToLower();
                    if (t.StartsWith("password="))
                    {
                        pwd = tokens[i].Substring(9);
                    }
                    else if (t.StartsWith("firstname="))
                    {
                        fname = tokens[i].Substring(10);
                    }
                    else if (t.StartsWith("lastname="))
                    {
                        lname = tokens[i].Substring(9);
                    }
                    else if (t.StartsWith("email="))
                    {
                        email = tokens[i].Substring(6);
                    }
                }
                if (email == null || email.Length == 0)
                    email = uname;
                if (!Command.isValidEmailAddress(u, email))
                {
                    WebServiceSessionHelper.warn("addUser", "User " + u.Username + " has no rights to create a user with the given format, but tried to do so for " + uname);
                }
                mu = Membership.CreateUser(uname, pwd, email, null, null, true, out mstatus);
                string[] productIDs = Command.newUserProductIDs.Split(new char[] { ',' });
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.setupNewUser(conn, WebServiceSessionHelper.mainSchema, uname, fname, lname, email, null, null, null, null, null, null, null, true, 0, "", "", "", "", new List<string>(productIDs), DateTime.MinValue);
                    Database.logCreatedUser(conn, WebServiceSessionHelper.mainSchema, (Guid)mu.ProviderUserKey, u.ID, pwd);
                }
                catch (Exception e)
                {
                    WebServiceSessionHelper.warn("addUser", "Error creating user " + uname + " by " + u.Username + " " + e.InnerException.Message);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                Global.systemLog.Info("addUser: User " + u.Username + " create user " + uname);
                return;
            }
            else
            {
                WebServiceSessionHelper.warn("addUser", "User " + u.Username + " tried to create a user that already exists - " + uname);
            }
        }

        internal static CommandQueryResult executeQueryInSpace(User user, string query, string spaceID)
        {
            string token = null;
            if (userQueryTokenDictionary.TryGetValue(user, out token))
            {
                if (token != null)
                {
                    lock (userQueryTokenDictionary)
                    {
                        userQueryTokenDictionary.Remove(user);
                    }
                    lock (pagedQueryResultSets)
                    {
                        pagedQueryResultSets.Remove(token);
                    }
                }
            }

            try
            {

                Space sp = getSpace(user, spaceID, "executeQueryInSpace", anyoneCanDoThis);
                bool newRep;
                MainAdminForm maf = Util.loadRepository(sp, out newRep);
                Global.systemLog.Info("Webservice executeQueryInSpace request for user " + user.Username + " on space " + sp.Name + " - " + query);

                // Make sure that user and group information is updated.
                UserAndGroupUtils.updatedUserGroupMappingFile(sp);

                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                TrustedService.TrustedService ts = new TrustedService.TrustedService();
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                Acorn.TrustedService.QueryResult tqr = ts.getQueryResults(sp.Directory, sp.ID.ToString(), query, user.Username, importedSpacesInfoArray);
                PagedQueryResult qr = new PagedQueryResult();
                qr.dataTypes = tqr.dataTypes;
                qr.columnNames = tqr.columnNames;
                qr.displayNames = tqr.displayNames;
                StringReader reader = new StringReader(tqr.rows);
                string line = null;
                List<string[]> rows = new List<string[]>();
                while ((line = reader.ReadLine()) != null)
                {
                    string[] row = splitLine(line);
                    rows.Add(row);
                }
                qr.rows = rows;
                CommandQueryResult cqr = new CommandQueryResult(qr);
                if (rows.Count <= MAX_NUM_TRANSPORT_ROWS)
                {
                    cqr.numRowsReturned = rows.Count;
                    cqr.hasMoreRows = false;
                    cqr.rows = qr.rows.ToArray();
                }
                else
                {
                    cqr.numRowsReturned = MAX_NUM_TRANSPORT_ROWS;
                    cqr.hasMoreRows = true;
                    qr.numSent = MAX_NUM_TRANSPORT_ROWS;
                    cqr.rows = qr.rows.GetRange(0, MAX_NUM_TRANSPORT_ROWS).ToArray();
                    cqr.queryToken = WebServiceSessionHelper.getToken();
                    lock (userQueryTokenDictionary)
                    {
                        userQueryTokenDictionary.Add(user, cqr.queryToken);
                    }
                    lock (pagedQueryResultSets)
                    {
                        pagedQueryResultSets.Add(cqr.queryToken, qr);
                    }
                }
                return cqr;
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("executeQueryInSpace", e.Message);
            }
            return null;
        }

        public static string[] splitLine(string s)
        {
            List<string> result = new List<string>();
            int lastIndex = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '|' && (i == 0 || s[i - 1] != '\\'))
                {
                    result.Add(s.Substring(lastIndex, i - lastIndex));
                    lastIndex = i + 1;
                }
            }
            if (lastIndex < s.Length)
                result.Add(s.Substring(lastIndex));
            return result.ToArray();
        }

        internal static CommandQueryResult queryMore(User user, string queryToken)
        {
            if (!WebServiceSessionHelper.validToken(queryToken))
            {
                WebServiceSessionHelper.error("queryMore", "queryToken " + queryToken + " is not valid or has expired.");
            }
            string token = null;
            if (userQueryTokenDictionary.TryGetValue(user, out token))
            {
                if (!token.Equals(queryToken))
                    WebServiceSessionHelper.warn("queryMore", "Invalid query token for user");
            }
            PagedQueryResult pqr = null;
            pagedQueryResultSets.TryGetValue(queryToken, out pqr);
            if (pqr == null)
                WebServiceSessionHelper.warn("queryMore", "Invalid query token for user");

            CommandQueryResult cqr = new CommandQueryResult(pqr);
            int numRowsLeft = pqr.rows.Count - pqr.numSent;
            int size = Math.Min(MAX_NUM_TRANSPORT_ROWS, numRowsLeft);
            cqr.hasMoreRows = numRowsLeft > MAX_NUM_TRANSPORT_ROWS;
            cqr.numRowsReturned = size;
            cqr.rows = pqr.rows.GetRange(pqr.numSent, size).ToArray();
            pqr.numSent += size;
            if (!cqr.hasMoreRows)
            {
                lock (pagedQueryResultSets)
                {
                    pagedQueryResultSets.Remove(queryToken);
                }
                lock (userQueryTokenDictionary)
                {
                    userQueryTokenDictionary.Remove(user);
                }
                cqr.queryToken = null;
            }
            return cqr;
        }

        internal static string beginDataUpload(User user, string spaceID, string sourceName)
        {
            Space sp = getSpace(user, spaceID, "beginDataUpload", isSpaceAdmin);

            if (!Directory.Exists(sp.Directory + DATASOURCE_TMP_DIR))
                Directory.CreateDirectory(sp.Directory + DATASOURCE_TMP_DIR);

            string filePath = sp.Directory + DATASOURCE_TMP_DIR + sourceName;
            if (File.Exists(filePath))
            {
                // delete the file if it already exists. Possible use case of cancelling upload and re uploading
                // or error during scanning
                File.Delete(filePath);
            }
            // Create a new version of file
            FileStream fs = null;
            try
            {
                fs = File.Create(filePath);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.error("beginDataUpload", "Unable to write file : space : " + spaceID + " fileName: " + sourceName + " " + ex);
            }
            finally
            {
                if (fs != null)
                {
                    try
                    {
                        fs.Close();
                    }
                    catch (Exception)
                    {
                        WebServiceSessionHelper.error("beginDataUpload", "Unable to close filestream handler for file :" + filePath);
                    }
                }
            }

            string token = WebServiceSessionHelper.getToken();
            lock (uploadFilePath)
            {
                uploadFilePath.Add(token, sourceName);
            }
            lock (uploadFileSpace)
            {
                uploadFileSpace.Add(token, sp);
            }
            lock (uploadFileOptions)
            {
                uploadFileOptions.Add(token, new UploadFileOptions());
            }
            return token;
        }


        internal static void setDataUploadOptions(User user, string dataUploadToken, string[] options)
        {
            if (!WebServiceSessionHelper.validToken(dataUploadToken))
            {
                WebServiceSessionHelper.error("uploadData", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }

            if (options == null)
                return;

            UploadFileOptions ufo;
            if (uploadFileOptions.TryGetValue(dataUploadToken, out ufo) == false)
            {
                WebServiceSessionHelper.error("setDataUploadOptions", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }

            for (int i = 0; i < options.Length; i++)
            {
                string[] option = options[i].Split('=');
                if (option.Length >= 2)
                {
                    if ("ConsolidateIdenticalStructures".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.consolidateIdenticalStructures = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                    else if ("ColumnNamesInFirstRow".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.columnNamesInFirstRow = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                    else if ("FilterLikelyNoDataRows".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.filterLikelyNoDataRows = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                    else if ("LockDataSourceFormat".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.lockDataSourceFormat = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                    else if ("IgnoreQuotesNotAtStartOrEnd".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.ignoreQuotesNotAtStartOrEnd = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                    else if ("RowsToSkipAtStart".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.rowsToSkipAtStart = int.Parse(option[1]);
                    }
                    else if ("RowsToSkipAtEnd".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.rowsToSkipAtEnd = int.Parse(option[1]);
                    }
                    else if ("CharacterEncoding".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.encoding = option[1];
                    }
                    else if ("Replacements".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.replacements = option[1];
                    }
                    else if ("Separator".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.separator = option[1];
                    }
                    else if ("AutoModel".Equals(option[0], StringComparison.OrdinalIgnoreCase))
                    {
                        ufo.autoModel = ("true".Equals(option[1], StringComparison.OrdinalIgnoreCase)) ? true : false;
                    }
                }
            }
        }

        internal static void uploadData(User user, string dataUploadToken, int numBytes, byte[] data)
        {
            if (!WebServiceSessionHelper.validToken(dataUploadToken))
            {
                WebServiceSessionHelper.error("uploadData", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }

            string sourceName = null;
            string dir = null;
            Space sp = null;
            if (uploadFilePath.TryGetValue(dataUploadToken, out sourceName) == false ||
                uploadFileSpace.TryGetValue(dataUploadToken, out sp) == false)
            {
                WebServiceSessionHelper.error("uploadData", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }
            dir = sp.Directory;

            string filePath = dir + DATASOURCE_TMP_DIR + sourceName;
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.Append, FileAccess.Write);
                fs.Write(data, 0, numBytes);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("uploadData", "Error writing to file " + filePath + " " + e);
            }
            finally
            {
                try
                {
                    fs.Close();
                }
                catch (Exception) { }
            }
        }

        internal static void finishDataUpload(User user, string dataUploadToken)
        {
            if (!WebServiceSessionHelper.validToken(dataUploadToken))
            {
                WebServiceSessionHelper.error("finishDataUpload", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }

            string sourceName = null;
            string dir = null;
            Space sp = null;
            if (uploadFilePath.TryGetValue(dataUploadToken, out sourceName) == false ||
                uploadFileSpace.TryGetValue(dataUploadToken, out sp) == false)
            {
                WebServiceSessionHelper.error("finishDataUpload", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }
            dir = sp.Directory;

            string tempFilePath = dir + DATASOURCE_TMP_DIR + sourceName;
            string finalFilePath = dir + DATASOURCE_FINAL_DIR + sourceName;
            if (!File.Exists(tempFilePath))
            {
                WebServiceSessionHelper.warn("finishDataUpload", "Unable to upload file.  Please try again");
            }

            try
            {
                if (File.Exists(finalFilePath))
                {
                    File.Delete(finalFilePath);
                }
                File.Move(tempFilePath, finalFilePath);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("finishDataUpload", "Unable to move the file from the temp location to the data directory " + e);
            }

            UploadFileOptions ufo;
            if (uploadFileOptions.TryGetValue(dataUploadToken, out ufo) == false)
            {
                WebServiceSessionHelper.error("finishDataUpload", "dataUploadToken " + dataUploadToken + " is not valid or has expired.");
            }
            MainAdminForm maf = Util.getRepositoryDev(sp, user);
			maf.setRepositoryDirectory(sp.Directory);

            ApplicationUploader ul = new ApplicationUploader(maf, sp, sourceName, 
                ufo.columnNamesInFirstRow, ufo.lockDataSourceFormat, ufo.ignoreQuotesNotAtStartOrEnd, 
                ApplicationUploadService.getEscapes(ufo.replacements), false, null,
                user, null, ufo.rowsToSkipAtStart, ufo.rowsToSkipAtEnd, ufo.filterLikelyNoDataRows, 
                ufo.encoding, ufo.separator, ufo.autoModel);

            WSFileUploader wsFileUploader = new WSFileUploader(ul, ufo);
            associateTokenWithBackgroundProcess(dataUploadToken, wsFileUploader);
            backgroundModuleObserver.addBackgroundProcessToRunningList(dataUploadToken);
            BackgroundProcess.startModule(new BackgroundTask(wsFileUploader, 0));
        }

        private static void associateTokenWithBackgroundProcess(string token, BackgroundModule module)
        {
            module.addObserver(backgroundModuleObserver, token);
            lock (backgroundProcesses)
            {
                backgroundProcesses.Add(token, module);
            }
        }

        internal static bool isUploadComplete(User user, string uploadStatusToken)
        {
            return isComplete(uploadStatusToken);
        }

        internal static bool isComplete(string token)
        {
            BackgroundModule bm = null;
            backgroundProcesses.TryGetValue(token, out bm);
            if (bm != null && bm is PUT_Handler.ExecutePublish)
            {   
                PUT_Handler.ExecutePublish exp = (PUT_Handler.ExecutePublish)bm;
                if (exp.didUseExternalScheduler() && backgroundModuleObserver.isStillRunning(token))
                {
                    // Calling status will retreive status from external scheduler and removes from
                    // token from observer map
                    exp.getStatus();
                }
            }

            if (bm != null && bm is PUT_Handler.ExtractSFDC)
            {
                PUT_Handler.ExtractSFDC exp = (PUT_Handler.ExtractSFDC)bm;
                if (exp.didUseExternalScheduler() && backgroundModuleObserver.isStillRunning(token))
                {
                    exp.getStatus();
                }
            }

            return !backgroundModuleObserver.isStillRunning(token);
        }

        internal static string[] getUploadStatus(User user, string uploadStatusToken) 
        {
            if (!isUploadComplete(user, uploadStatusToken))
            {
                string[] ret = new string[1];
                ret[0] = "Upload is not complete";
                return ret;
            }
            if (!WebServiceSessionHelper.validToken(uploadStatusToken))
            {
                WebServiceSessionHelper.error("getUploadStatus", "uploadStatusToken " + uploadStatusToken + " is not valid or has expired.");
            }

            WSFileUploader ul = null;
            BackgroundModule bm = null;
            backgroundProcesses.TryGetValue(uploadStatusToken, out bm);
            if (bm == null || ! (bm is WSFileUploader))
            {
                WebServiceSessionHelper.error("getUploadStatus", "uploadStatusToken not found");
            }
            ul = (WSFileUploader)bm;

            int successCount = ul.getSuccessfullCounts();
            // check for excel warning first, if present return the error
            List<string> errorsAndWarnings = new List<string>();
            Dictionary<string, ScanSummaryResult> anyExcelErrors = ul.getExcelWarningDetails();
            if (anyExcelErrors != null)
            {
                List<ExcelWarningError> excelErros = ApplicationUploadService.formatExcelWarningErrors(ul.getSpace(), anyExcelErrors);
                if (excelErros != null)
                {
                    foreach (ExcelWarningError ewe in excelErros) 
                    {
                        foreach (string s in ewe.Rows) 
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(ewe.FileName);
                            sb.Append(": ");
                            sb.Append(s);
                            errorsAndWarnings.Add(sb.ToString());
                        }
                    }
                }
            }

            if (ul.errorTable != null)
            {
                foreach (DataRow dr in ul.errorTable.Rows)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Source: ");
                    sb.Append((string)dr[0]);
                    sb.Append(" Severity: ");
                    sb.Append((string) dr[1]);
                    sb.Append(" Issue: ");
                    sb.Append((string) dr[2]);
                    errorsAndWarnings.Add(sb.ToString());
                }
            }

            return errorsAndWarnings.ToArray();
        }

        internal static string extractSalesforceData(User user, string spaceID)
        {   
            Space sp = getSpace(user, spaceID, "publishData", isSpaceAdmin);
            SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
            sforce.SforceService binding = new sforce.SforceService();
            if (settings.sitetype == SalesforceSettings.SiteType.Sandbox)
                binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Warn("Bad SFDC binding URL prior to authentication: " + binding.Url);
                return "Problem during login to Salesforce";
            }
            binding.Timeout = 30 * 60 * 1000;
            String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
            if (clientID != null && clientID.Length > 0)
            {
                sforce.CallOptions co = new sforce.CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            // Invoke the login call and save results in LoginResult
            sforce.LoginResult lr = null;
            try
            {
                lr = binding.login(settings.Username, settings.Password);
            }
            catch (SoapException ex)
            {
                Global.systemLog.Error("CommandHelper : Error during extractSalesforceData call : " + ex.Message, ex);
                return "Problem during login to Salesforce";
            }
            catch (WebException ex)
            {
                Global.systemLog.Error("CommandHelper : Error during extractSalesforceData call : " + ex.Message, ex);
                return "Problem during login to Salesforce";
            }
            if (!lr.passwordExpired)
            {
                /*
                 * Can't login using zip until after authentication - errors not caught correctly if zip is on
                 */
                binding = new SforceServiceGzip(true, true);
                if (settings.sitetype == SalesforceSettings.SiteType.Sandbox)
                    binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
                if (!Util.isValidSFDCUrl(binding.Url))
                {
                    Global.systemLog.Warn("Bad SFDC binding URL prior to gzip authentication: " + binding.Url);
                    return "Problem during login to Salesforce";
                }
                binding.Timeout = 30 * 60 * 1000;
                if (clientID != null && clientID.Length > 0)
                {
                    sforce.CallOptions co = new sforce.CallOptions();
                    co.client = clientID;
                    binding.CallOptionsValue = co;
                }
                try
                {
                    lr = binding.login(settings.Username, settings.Password);
                }
                catch (SoapException ex)
                {
                    Global.systemLog.Error("CommandHelper : Error during extractSalesforceData call : " + ex.Message, ex);
                    return "Problem during login to Salesforce";
                }
                catch (WebException ex)
                {
                    Global.systemLog.Error("CommandHelper : Error during extractSalesforceData call : " + ex.Message, ex);
                    return "Problem during login to Salesforce";
                }
                // Reset the SOAP endpoint to the returned server URL
                binding.Url = lr.serverUrl;
                if (!Util.isValidSFDCUrl(binding.Url))
                {
                    Global.systemLog.Warn("Bad SFDC binding URL after authentication: " + binding.Url);
                    return "Problem during login to Salesforce";
                }
                // Create a new session header object
                // Add the session ID returned from the login
                binding.SessionHeaderValue = new sforce.SessionHeader();
                binding.SessionHeaderValue.sessionId = lr.sessionId;
            }
            else
            {
                Global.systemLog.Warn("CommandHelper : Password Expired during extractSalesforceData call : ");
                    return "Your salesforce password has expired";                            
            }

            PUT_Handler.ExtractSFDC extract = new PUT_Handler.ExtractSFDC(sp, user, binding, settings); 
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, extract);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(extract, 0));
            return token;
        }

        internal static string publishData(User user, string spaceID, string[] subgroups, DateTime date)
        {
            Space sp = getSpace(user, spaceID, "publishData", isSpaceAdmin);
            
            MainAdminForm maf = Util.getRepositoryDev(sp, user);
            if (!SpaceAdminService.checkSpacePublishability(maf, sp))
            {
                WebServiceSessionHelper.error("publishData", "Space has not been configured for processing. Please visit the Manage Sources screen to correct.");
            }

            int spOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            if (!SpaceOpsUtils.isSpaceAvailable(spOpID))
            {
                WebServiceSessionHelper.error("publishData", "Space " + sp.Name + " is not available. " + SpaceOpsUtils.getAvailabilityMessage(null, spOpID));                
            }

            if (Util.checkForPublishingStatus(sp))
            {
                WebServiceSessionHelper.warn("publishData", "Space " + sp.Name + " is already publishing");
            }

            PUT_Handler.ExecutePublish publish = new PUT_Handler.ExecutePublish(sp, date, false, user, maf, HttpContext.Current.Server.MapPath("~/"), user.Email, subgroups, null, Util.LOAD_GROUP_NAME);
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, publish);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(publish, 0));
            return token;
        }

        internal static string[] getPublishingStatus(User user, string publishingToken)
        {
            if (!WebServiceSessionHelper.validToken(publishingToken))
            {
                WebServiceSessionHelper.error("isPublishingComplete", "publishingToken " + publishingToken + " is not valid or has expired.");
            }

            List<string> ret = new List<string>();
            Status.StatusResult statusResult = CommandHelper.getStatus(user, publishingToken);
            if (statusResult != null)
            {
                Status.StatusCode code = statusResult.code;
                if (Status.isRunningCode(code))
                {
                    ret.Add("Running");
                }
                else
                {   
                    ret.Add(code.ToString());
                }
            }
           
            /*
            if (backgroundModuleObserver.isStillRunning(publishingToken))
            {
                ret.Add("Running");
            }
            else
            {
                BackgroundModule bm = null;
                backgroundProcesses.TryGetValue(publishingToken, out bm);
                if (bm != null && bm is PUT_Handler.ExecutePublish)
                {
                    PUT_Handler.ExecutePublish pub = (PUT_Handler.ExecutePublish)bm;
                    Status.StatusResult result = Status.getLoadStatus(null, pub.getSpace());
                    if (result.code != Status.StatusCode.Complete && result.code != Status.StatusCode.None)
                    {
                        ret.Add("Failed");
                    }
                }
            }
            */
            return ret.ToArray();
        }

        internal static bool isPublishingComplete(User user, string publishingToken)
        {
            return isComplete(publishingToken);
            //return !backgroundModuleObserver.isStillRunning(publishingToken);
        }

        internal static string[][] getVariablesForSpace(User user, string spaceID)
        {
            Space sp = getSpace(user, spaceID, "getVariablesForSpace", isSpaceAdmin);

            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string variableString = ts.getVariables(sp.Directory, user.Username, sp.ID.ToString(), null);
            ArrayList list = new ArrayList();
            if (variableString != null && variableString.Length > 0)
            {
                string[] vars = variableString.Split(new char[] { '|' });
                for (int i = 0; i < vars.Length; i++)
                {
                    if (vars[i] != null && vars[i].Length > 0)
                    {
                        int index = vars[i].IndexOf('=');
                        if (index > 0)
                        {
                            string[] var = new string[2];
                            string name = vars[i].Substring(0, index);
                            if (name.StartsWith("V{") && name.EndsWith("}"))
                            {
                                name = name.Substring(2, name.Length - 3);
                            }
                            var[0] = name;
                            var[1] = vars[i].Substring(index + 1);
                            list.Add(var);
                        }
                    }
                }
            }

            string[][] ret = new string[list.Count][];
            for (int i = 0; i < list.Count; i++)
            {
                ret[i] = (string[])list[i];
            }
            return ret;
        }

		public class FileNode
		{
			public string name;
			public string label;
			public bool isDirectory = false;
			public bool isWriteable = false;
			public string lastModified;
            public bool isDashboard = false;
            public string createdBy = null;
			public List<FileNode> children;

			private FileNode() { }

			public FileNode(XmlNode node)
			{
				name = node.Attributes["n"].InnerText;
				label = node.Attributes["l"].InnerText;
				if (node.Attributes["dir"] != null)
					isDirectory = true;
				if (node.Attributes["w"] != null)
					isWriteable = true;
                if (node.Attributes["dash"] != null)
                    isDashboard = true;
                createdBy = node.Attributes["CreatedBy"] == null ? null : node.Attributes["CreatedBy"].InnerText;

				lastModified = node.Attributes["m"].InnerText;

				if (node.HasChildNodes)
				{
					XmlNode chs = node.ChildNodes[0];
					if (chs.HasChildNodes)
					{
						children = new List<FileNode>();
						foreach (XmlNode nd in chs.ChildNodes) 
						{
							children.Add(new FileNode(nd));

						}
					}
				}
			}
		}
        internal static FileNode getDirectoryContents(User user, string token, string spaceID, string dir)
        {
            Space sp = getSpace(user, spaceID, "getDirectoryContents", isSpaceAdmin);
            Global.systemLog.Info("Webservice getDirectoryContents request for user " + user.Username + " on space " + sp.Name + " - " + dir);

            CookieContainer cc = SMILogin(token, user, sp);

            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            FileService.File fs = new FileService.File();
            fs.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/File";
            fs.CookieContainer = cc;
            XmlNode[] dirNodes = (XmlNode[])fs.getDirectory(dir, false, true);
			if (dirNodes.Length > 0)
			{
				XmlNode result = dirNodes[0];
				return new FileNode(result.ChildNodes[2].ChildNodes[0]);
			}

            return null;
        }

        public class GroupPermission
        {
            public static string MODIFY = "Modify";
            public static string VIEW = "View";
            public string groupName;
            public bool canView;
            public bool canModify;

            private GroupPermission() { }

            public GroupPermission(XmlNode node)
            {
                groupName = node.Attributes["Name"].InnerText;
                canView = node.Attributes[VIEW].InnerText == "true";
                canModify = node.Attributes[MODIFY].InnerText == "true";
            }
        }

        internal static List<GroupPermission> getDirectoryPermissions(User user, string token, string spaceID, string dir)
        {
            Space sp = getSpace(user, spaceID, "getDirectoryPermissions", isSpaceAdmin);
            Global.systemLog.Info("Webservice getDirectoryPermissions request for user " + user.Username + " on space " + sp.Name + " - " + dir);

            CookieContainer cc = SMILogin(token, user, sp);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            FileService.File fs = new FileService.File();
            fs.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/File";
            fs.CookieContainer = cc;

            XmlNode[] nodes = (XmlNode[])fs.getPermissions(dir);

			List<GroupPermission> permissions = new List<GroupPermission>();
            if (nodes.Length > 0)
            {
                XmlNode node = nodes[0];
                node = node.LastChild;
                // now node is <Result>...
                node = node.FirstChild;
                // now node is <Groups>...
                XmlNodeList children = node.ChildNodes;
                for (int i = 0; i < children.Count; i++)
                {
                    XmlNode child = children.Item(i);
                    XmlAttributeCollection attributes = child.Attributes;
                    if (attributes != null)
                    {
						GroupPermission p = new GroupPermission(child);
						permissions.Add(p);
                    }
                }

                return permissions;
            }


            return null;
        }


        // set a specific group permissions (view or modify) on directory - need a separate method since the old semantics allowed you to update view independent of modify, the new semantics require both
        private static void internalSetDirectoryPermission(FileService.File fs, String dir, string groupName, string permissionName, bool permission)
        {
            XmlNode[] nodes = (XmlNode[])fs.getPermissions(dir);

            if (nodes.Length > 0)
            {
                XmlDocument doc = new XmlDocument();
                XmlNode node = nodes[0];
                doc.ImportNode(node, true);

                node = node.LastChild;
                // now node is <Result>...
                node = node.FirstChild;
                // now node is <Groups>...
                XmlNode groupNode = getGroupNode(doc, node, groupName);
                if (groupNode != null)
                {
                    XmlAttributeCollection attributes = groupNode.Attributes;
                    if (attributes != null)
                    {
                        XmlAttribute attribute = attributes[permissionName];
                        if (attribute == null)
                        {
                            attribute = groupNode.OwnerDocument.CreateAttribute(permissionName);
                            attributes.Append(attribute);
                        }
                        attribute.Value = permission == true ? "true" : "false";
                    }
                }
                fs.savePermissions(dir, node.OuterXml);
            }
        }

        // get or create a group node for groupName
        private static XmlNode getGroupNode(XmlDocument doc, XmlNode node, string groupName)
        {
            XmlNodeList children = node.ChildNodes;
            XmlNode groupNode = null;
            for (int i = 0; i < children.Count; i++)
            {
                XmlNode child = children.Item(i);
                XmlAttributeCollection attributes = child.Attributes;
                if (attributes != null)
                {
                    XmlAttribute attribute = attributes["Name"];
                    if (attribute != null)
                    {
                        if (attribute.Value.Equals(groupName))
                        {
                            groupNode = child;
                            break;
                        }
                    }
                }
            }
            if (groupNode == null)
            {
                groupNode = doc.CreateElement("Group");
                XmlAttribute attribute = doc.CreateAttribute("Name");
                attribute.Value = groupName;
                groupNode.Attributes.Append(attribute);
                groupNode = node.OwnerDocument.ImportNode(groupNode, true);
                node.AppendChild(groupNode);
            }
            return groupNode;
        }

        /*
         * set of list of group permissions on a directory, both view and modify at the same time
         */
        private static void internalSetDirectoryPermission(FileService.File fs, String dir, List<GroupPermission> perms)
        {
            XmlNode[] nodes = (XmlNode[])fs.getPermissions(dir);

            if (nodes.Length > 0)
            {
                XmlDocument doc = new XmlDocument();
                XmlNode node = nodes[0];
                doc.ImportNode(node, true);

                node = node.LastChild;
                // now node is <Result>...
                node = node.FirstChild;
                // now node is <Groups>...
                foreach (GroupPermission perm in perms)
                {
                    XmlNode groupNode = getGroupNode(doc, node, perm.groupName);
                    if (groupNode != null)
                    {
                        XmlAttributeCollection attributes = groupNode.Attributes;
                        if (attributes != null)
                        {
                            internalSetPermission(groupNode, attributes, GroupPermission.VIEW, perm.canView);
                            internalSetPermission(groupNode, attributes, GroupPermission.MODIFY, perm.canModify);
                        }
                    }
                }
                fs.savePermissions(dir, node.OuterXml);
            }
        }

        /*
         * modify/create a specific attribute value
         */
        private static void internalSetPermission(XmlNode groupNode, XmlAttributeCollection attributes, string permissionName, bool permission)
        {
            XmlAttribute attribute = attributes[permissionName];
            if (attribute == null)
            {
                attribute = groupNode.OwnerDocument.CreateAttribute(permissionName);
                attributes.Append(attribute);
            }
            attribute.Value = permission == true ? "true" : "false";
        }

        /*
         * get a handle on the smiweb web service for the Files
         */
        private static FileService.File getFile(User user, string token, Space sp)
        {
            CookieContainer cc = SMILogin(token, user, sp);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            FileService.File fs = new FileService.File();
            fs.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/File";
            fs.CookieContainer = cc;
            return fs;
        }

        /*
         * set a collection of permissions on a directory for a space (a bulk operation)
         */
        internal static void setDirectoryPermissions(User user, string token, string spaceID, List<string> dirs, List<List<GroupPermission>> perms)
        {
            // check arguments
            if (user == null || token == null || spaceID == null || dirs == null || perms == null)
            {
                WebServiceSessionHelper.error("setDirectoryPermissions", "arguments can not be null");
            }
            if (dirs.Count != perms.Count)
            {
                WebServiceSessionHelper.error("setDirectoryPermissions", "dirs and perms must be the same size");
            }
            Space sp = getSpace(user, spaceID, "setDirectoryPermissions", isSpaceAdmin);
            Global.systemLog.Info("Webservice setDirectoryPermissions request for user " + user.Username + " on space " + sp.Name);

            FileService.File fs = getFile(user, token, sp);

            for (int i = 0; i < dirs.Count; i++)
                internalSetDirectoryPermission(fs, dirs[i], perms[i]);
        }

        /*
         * set a single permission for a single group on a single directory in a space
         */
        internal static void setDirectoryPermission(User user, string token, string spaceID, string dir, string groupName, string permissionName, bool permission)
        {
            Space sp = getSpace(user, spaceID, "setDirectoryPermissions", isSpaceAdmin);
            Global.systemLog.Info("Webservice setDirectoryPermission request for user " + user.Username + " on space " + sp.Name + " - " + dir);

            FileService.File fs = getFile(user, token, sp);

            internalSetDirectoryPermission(fs, dir, groupName, permissionName, permission);
        }

        internal static void copyFile(User user, string fromSpaceID, string fromFile, string toSpaceID, string toFile, bool overwrite)
        {
            try
            {
                Space space1, space2;
                getSpaces(user, fromSpaceID, toSpaceID, out space1, out space2, "copyFile", CommandHelper.isSpaceAdmin);
                Global.systemLog.Info("Webservice copyFile request for user " + user.Username + " from space " + space1.Name + " - " + fromFile + " to " + space2.Name + " - " + toFile);

	     		string sp1CatalogDir = Path.Combine(space1.Directory, "catalog");
            	string sp2CatalogDir = Path.Combine(space2.Directory, "catalog");

                string fromFileName = Path.Combine(sp1CatalogDir, fromFile);
                string toFileName = Path.Combine(sp2CatalogDir, toFile);

                Util.validateFileLocation(sp1CatalogDir, fromFileName);
                Util.validateFileLocation(sp2CatalogDir, toFileName);

                if (!File.Exists(fromFileName))
                    WebServiceSessionHelper.error("copyFile", fromFile + " does not exist");
                if (File.Exists(toFileName) && !overwrite)
                     WebServiceSessionHelper.error("copyFile", toFile + " exists and overwrite is false");
                string fileName = toFileName;
                if (Directory.Exists(toFileName))
                {
                    fileName = Path.Combine(toFileName, Path.GetFileName(fromFileName));
                    if (File.Exists(fileName) && !overwrite)
                        WebServiceSessionHelper.error("copyFile", toFile + " exists and overwrite is false");
                }
                Util.FileCopy(fromFileName, fileName, overwrite);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("copyFile", e.Message);
            }
        }

        internal static string copyFileOrDirectory(User user, string fromSpaceID, string fileOrDir, string toSpaceID, string toDir)
        {
            try
            {
                Space space1, space2;
                getSpaces(user, fromSpaceID, toSpaceID, out space1, out space2, "copyFileOrDirectory", CommandHelper.isSpaceAdmin);
                Global.systemLog.Info("Webservice copyFileOrDirectory request for user " + user.Username + " from space " + space1.Name + " - " + fileOrDir + " to " + space2.Name + " - " + toDir);

                string sp1CatalogDir = Path.Combine(space1.Directory, "catalog");
                string sp2CatalogDir = Path.Combine(space2.Directory, "catalog");

                string fromFileName = Path.Combine(sp1CatalogDir, fileOrDir);
                string toDirectory = Path.Combine(sp2CatalogDir, toDir);

                Util.validateFileLocation(sp1CatalogDir, fromFileName);
                Util.validateFileLocation(sp2CatalogDir, toDirectory);

                if (!Directory.Exists(toDirectory))
                    WebServiceSessionHelper.warn("copyFileOrDirectory", toDir + " does not exist or is not a directory in space " + space2.Name);

                string token = WebServiceSessionHelper.getToken();
                CopyFileOrDirectory cd = new CopyFileOrDirectory(fromFileName, toDirectory);
                associateTokenWithBackgroundProcess(token, cd);
                backgroundModuleObserver.addBackgroundProcessToRunningList(token);
                BackgroundProcess.startModule(new BackgroundTask(cd, 0));
                return token;
            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("copyFileOrDirectory", e.Message);
            }
            return null;
        }

        class CopyFileOrDirectory : SimpleBackgroundModule
        {
            string fromFileName;
            string toDirectory;

            public CopyFileOrDirectory(string f, string t) : base()
            {
                fromFileName = f;
                toDirectory = t;
            }
            #region BackgroundModule Members

            public override void run()
            {
                try
                {
                    if (File.Exists(fromFileName))
                    {
                        string toFileName = Path.Combine(toDirectory, Path.GetFileName(fromFileName));
                        Util.FileCopy(fromFileName, toFileName, true);
                    }
                    else if (Directory.Exists(fromFileName))
                    {
                        copyDirectory(fromFileName, toDirectory, false);
                    }
                }
                catch (Exception e)
                {
                    error = e;
                }
            }

            #endregion
        }

        private static void copyDirectory(string fromDir, string toDir, bool overwrite)
        {
            try
            {
                if (!Directory.Exists(toDir))
                    Directory.CreateDirectory(toDir);

                string[] fileNames = Directory.GetFiles(fromDir);
				for (int i = 0; i < fileNames.Length; i++)
				{
					Util.FileCopy(fileNames[i], Path.Combine(toDir, Path.GetFileName(fileNames[i])), overwrite);
				}

                string[] dirs = Directory.GetDirectories(fromDir);
				char[] separators = new char[2];
				separators[0] = Path.DirectorySeparatorChar;
				separators[1] = Path.AltDirectorySeparatorChar;
				for (int i = 0; i < dirs.Length; i++)
				{
					string[] paths = dirs[i].Split(separators);
					if (paths.Length > 0)
						copyDirectory(dirs[i], Path.Combine(toDir, paths[paths.Length - 1]), overwrite);
				}
            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("copyDirectory", e.Message);
            }
        }

        internal static void deleteFileOrDirectory(User user, string spaceID, string fileOrDir)
        {
            Space sp = getSpace(user, spaceID, "deleteFileOrDirectory", CommandHelper.isSpaceAdmin);
            Global.systemLog.Info("Webservice deleteFileOrDirectory request for user " + user.Username + " from space " + sp.Name + " - " + fileOrDir);

            string name = Path.Combine(Path.Combine(sp.Directory, "catalog"), fileOrDir);
            if (File.Exists(name))
                File.Delete(name);
            else if (Directory.Exists(name))
                Directory.Delete(name, true);
            else
                WebServiceSessionHelper.warn("deleteFileOrDirectory", "File or Directory " + fileOrDir + " does not exist in space " + sp.Name);
        }

        internal static void renameFileOrDirectory(User user, string spaceID, string fileOrDir, string newName)
        {
            Space sp = getSpace(user, spaceID, "renameFileOrDirectory", isSpaceAdmin);
            Global.systemLog.Info("Webservice renameFileOrDirectory request for user " + user.Username + " from space " + sp.Name + " - " + fileOrDir + " to " + newName);

            string name = Path.Combine(Path.Combine(sp.Directory, "catalog"), fileOrDir);
            if (File.Exists(name))
                File.Move(name, Path.Combine(Path.Combine(sp.Directory, "catalog"), newName));
            else if (Directory.Exists(name))
                Directory.Move(name, Path.Combine(Path.Combine(sp.Directory, "catalog"), newName));
            else
                WebServiceSessionHelper.warn("renameFileOrDirectory", "File or Directory " + fileOrDir + " does not exist in space " + sp.Name);
        }
                
        internal static bool checkAndCreateDirectory(User user, string spaceID, string parentDir, string newDirectoryName)
        {
            Space sp = getSpace(user, spaceID, "checkAndcreateNewDirectory", isSpaceAdmin);
            Global.systemLog.Info("Webservice checkAndcreateNewDirectory request for user " + user.Username + " from space " + sp.Name + " - " + parentDir + " - " + newDirectoryName);

            string catalogDir = Path.Combine(sp.Directory, "catalog");
            string name = Path.Combine(catalogDir, parentDir);
            Util.validateFileLocation(catalogDir, name);
            if (!Directory.Exists(name))
                WebServiceSessionHelper.warn("checkAndcreateNewDirectory", "Directory " + parentDir + " does not exist in space " + sp.Name);

            string newName = Path.Combine(name, newDirectoryName);
            Util.validateFileLocation(name, newName);
            if (File.Exists(newName))
                WebServiceSessionHelper.warn("checkAndcreateNewDirectory", "A file name " + newDirectoryName + " exists in directory " + parentDir + " in space " + sp.Name + ".  Can not create a directory there.");

            if (Directory.Exists(newName))
            {
                return false;
            }

            Directory.CreateDirectory(newName);
            return true;
        }

        internal static string getLoadStatus(User user, string spaceID)
        {
            Space sp = getSpace(user, spaceID, "getLoadStatus", isSpaceAdmin);
            Status.StatusResult result = Status.getLoadStatus(null, sp, sp.LoadNumber + 1, Util.LOAD_GROUP_NAME);
            Status.StatusCode statusCode = result.code;
            return (statusCode.Equals(Status.StatusCode.None) || statusCode.Equals(Status.StatusCode.Complete) || 
                statusCode.Equals(Status.StatusCode.Failed) || statusCode.Equals(Status.StatusCode.Ready)) ? "Available" : "Processing";
        }

        internal static void createNewDirectory(User user, string spaceID, string parentDir, string newDirectoryName)
        {
            Space sp = getSpace(user, spaceID, "createNewDirectory", isSpaceAdmin);
            Global.systemLog.Info("Webservice createNewDirectory request for user " + user.Username + " from space " + sp.Name + " - " + parentDir + " - " + newDirectoryName);

            string catalogDir = Path.Combine(sp.Directory, "catalog");
            string name = Path.Combine(catalogDir, parentDir);
            Util.validateFileLocation(catalogDir, name);
            if (!Directory.Exists(name))
                WebServiceSessionHelper.warn("createNewDirectory", "Directory " + parentDir + " does not exist in space " + sp.Name);

            string newName = Path.Combine(name, newDirectoryName);
            Util.validateFileLocation(name, newName);
            if (File.Exists(newName))
                WebServiceSessionHelper.warn("createNewDirectory", "A file name " + newDirectoryName + " exists in directory " + parentDir + " in space " + sp.Name + ".  Can not create a directory there.");

            Directory.CreateDirectory(newName);
        }

        internal static string deleteSpace(User user, string spaceId)
        {
            // make a background module
            Space sp = getSpace(user, spaceId, "deleteSpace", isSpaceAdmin);
            Global.systemLog.Info("Webservice delete request for user " + user.Username + " on space " + sp.Name);
            string token = WebServiceSessionHelper.getToken();
            DeleteSpace ds = new DeleteSpace(sp, user);
            associateTokenWithBackgroundProcess(token, ds);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(ds, 0));
            return token;
        }

        class DeleteSpace : SimpleBackgroundModule
        {
            Space sp;
            User user;

            public DeleteSpace(Space space, User u)
                : base()
            {
                sp = space;
                user = u;
            }

            public override void run()
            {
                try
                {
                    Util.setLogging(user, sp);
                    DeleteDataService.deleteSpace(sp, user, null);
                }
                catch (Exception e)
                {
                    error = e;
                }
            }
        }

        internal static void copyDashboardTemplates(User user, string fromSpaceId, string toSpaceId)
        {
            Space fromSpace, toSpace;
            getSpaces(user, fromSpaceId, toSpaceId, out fromSpace, out toSpace, "copyDashboardTemplates", isSpaceAdmin);
            Global.systemLog.Info("Webservice copyDashboardTemplates request for user " + user.Username + " from space " + fromSpace.Name + " to space " + toSpace.Name);

            FileInfo fromSettings = new FileInfo(Path.Combine(fromSpace.Directory, "DashboardStyleSettings.xml"));
            if (fromSettings.Exists)
                Util.FileCopy(fromSettings.FullName, Path.Combine(toSpace.Directory, "DashboardStyleSettings.xml"), true);
            DirectoryInfo fromDir = new DirectoryInfo(Path.Combine(fromSpace.Directory, "DashboardTemplates"));
            if (fromDir.Exists)
            {
                copyDirectory(fromDir.FullName, Path.Combine(toSpace.Directory, "DashboardTemplates"), true);
            }           
        }
        private static string[] invalidSubjectAreaNameChars = new string[] { "\\", "/", "?", "*", "|", ">", "<", ":", "\"" };
        private static void checkInvalidCharsInSubjectAreaName(string prefix, string subjectAreaName)
        {
            if (subjectAreaName != null && subjectAreaName.Trim().Length > 0)
            {
                foreach (string invalidChar in invalidSubjectAreaNameChars)
                {
                    if (subjectAreaName.IndexOf(invalidChar) >= 0)
                    {
                        WebServiceSessionHelper.error(prefix, "Subject Area Name contains one or more invalid characters - " + 
                            "\"" + string.Join(" ", invalidSubjectAreaNameChars) + "\"");
                    }
                }
            }
        }

        internal static void copyCustomSubjectArea(User user, string fromSpaceId, string customSubjectAreaName, string toSpaceId)
        {
            Space fromSpace, toSpace;
            getSpaces(user, fromSpaceId, toSpaceId, out fromSpace, out toSpace, "copyCustomSubjectArea", isSpaceAdmin);
            Global.systemLog.Info("Webservice copyCustomSubjectArea request for user " + user.Username + " on subject area " + customSubjectAreaName + " from space " + fromSpace.Name + " to space " + toSpace.Name);

            string toDir = Path.Combine(toSpace.Directory, "custom-subject-areas");
            if (!Directory.Exists(toDir))
                Directory.CreateDirectory(toDir);

            string fromMetaFileName = Path.Combine(fromSpace.Directory, "custom-subject-areas/subjectarea-metadata_" + customSubjectAreaName + ".xml");
            string fromDataFileName = Path.Combine(fromSpace.Directory, "custom-subject-areas/subjectarea_" + customSubjectAreaName + ".xml");
            string toMetaFileName = Path.Combine(toSpace.Directory, "custom-subject-areas/subjectarea-metadata_" + customSubjectAreaName + ".xml");
            string toDataFileName = Path.Combine(toSpace.Directory, "custom-subject-areas/subjectarea_" + customSubjectAreaName + ".xml");

            Util.FileCopy(fromMetaFileName, toMetaFileName, true);
            Util.FileCopy(fromDataFileName, toDataFileName, true);
        }

        private static string getCSAFileName(Space sp, String customSubjectAreaName)
        {
            return Path.Combine(sp.Directory, "custom-subject-areas", "subjectArea_" + customSubjectAreaName + ".xml");
        }
        
        private static bool checkGroupsExistInSpace(Space sp, string[] groups)
        {
            if(groups != null && groups.Length > 0)
            {
                UserAndGroupUtils.populateSpaceGroups(sp, true);
                List<SpaceGroup> spaceGroups = sp.SpaceGroups;                
                if (spaceGroups != null && spaceGroups.Count > 0)
                {
                    List<string> spaceGroupNames = new List<string>();
                    foreach(SpaceGroup spaceGroup in spaceGroups)
                    {
                        if(!spaceGroupNames.Contains(spaceGroup.Name))
                        {
                            spaceGroupNames.Add(spaceGroup.Name);
                        }
                    }
                    foreach (string group in groups)
                    {
                        if(!spaceGroupNames.Contains(group))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        internal static void validateCSAName(string prefix, string name)
        {
            if (name == null || name.Trim().Length == 0)
            {
                WebServiceSessionHelper.error(prefix, "Name of the custom subject area cannot be blank");
            }
        }

        internal static bool isCustomSubjectAreaDefaultNamed(string name)
        {
            return name != null && name.Equals("Default Subject Area", StringComparison.OrdinalIgnoreCase);
        }
                
        internal static void setSubjectAreaPermissions(User user, string token, string spaceID, string name, string[] groups)
        {
            Space sp = getSpace(user, spaceID, "setSubjectAreaPermissions", isSpaceAdmin);
            Global.systemLog.Info("Webservice setSubjectAreaPermissions request for user " + user.Username + " on space " + sp.Name + " - " + name);
            validateCSAName("setSubjectAreaPermissions", name);
                        
            string csaFileName = getCSAFileName(sp, name);
            if (!isCustomSubjectAreaDefaultNamed(name) && !File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("setSubjectAreaPermissions", "Custom subject area " + name + " does not exist");
            }

            if (groups == null || groups.Length == 0)
            {
                WebServiceSessionHelper.error("setSubjectAreaPermissions", "No groups provided");
            }

            if (!checkGroupsExistInSpace(sp, groups))
            {
                WebServiceSessionHelper.error("setSubjectAreaPermissions", "One or more groups do not exist in the space");
            }

            XmlNode[] obj = (XmlNode[])getSubjectAreaService(token, user, sp).setSubjectAreaPermissions(name, groups);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling setSubjectAreaPermissions - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in setting custom subject area permissions";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("setSubjectAreaPermissions", userMessage);
            }
        }

        internal static string[] getSubjectAreaPermissions(User user, string token, string spaceID, string name)
        {
            Space sp = getSpace(user, spaceID, "getSubjectAreaPermissions", isSpaceAdmin);
            Global.systemLog.Info("Webservice getSubjectAreaPermissions request for user " + user.Username + " on space " + sp.Name + " - " + name);
            validateCSAName("getSubjectAreaPermissions", name);

            string csaFileName = getCSAFileName(sp, name);
            if (!isCustomSubjectAreaDefaultNamed(name) && !File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("getSubjectAreaPermissions", "Custom subject area " + name + " does not exist");
            }
            HashSet<string> response = new HashSet<string>();
            XmlNode[] obj = (XmlNode[])getSubjectAreaService(token, user, sp).getSubjectAreaPermissions(name);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                XElement groupsRoot = WebServiceResponseXml.getFirstElement(wsResponseXml.getResult(), "Groups");
                XElement[] groupsList = WebServiceResponseXml.getElements(groupsRoot, "Group");
                if (groupsList != null && groupsList.Length > 0)
                {
                    foreach(XElement groupElement in groupsList)
                    {
                        response.Add(groupElement.Value);
                    }
                } 
            }
            else
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling getSubjectAreaPermissions - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in getting custom subject area permissions";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("getSubjectAreaPermissions", userMessage);
            }            
            return response.ToArray<string>();
        }

        internal static void renameSubjectArea(User user, string token, string spaceID, string name, string newName)
        {
            Space sp = getSpace(user, spaceID, "renameSubjectArea", isSpaceAdmin);
            Global.systemLog.Info("Webservice renameSubjectArea request for user " + user.Username + " on space " + sp.Name + " - " + name);
            validateCSAName("renameSubjectArea", name);
            if (isCustomSubjectAreaDefaultNamed(name))
            {
                WebServiceSessionHelper.error("renameSubjectArea", "Cannot rename Default Subject Area ");
            }

            if (isCustomSubjectAreaDefaultNamed(newName))
            {
                WebServiceSessionHelper.error("renameSubjectArea", "Cannot rename custom subject area to Default Subject Area ");
            }

            if (name.Trim() == newName.Trim())
            {
                WebServiceSessionHelper.error("renameSubjectArea", "New name is the same as the old name");
            }

            checkInvalidCharsInSubjectAreaName("renameSubjectArea", newName);

            string csaFileName = getCSAFileName(sp, name);
            if (!File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("renameSubjectArea", "Custom subject area " + name + " does not exist");
            }

            string csaFileNewName = getCSAFileName(sp, newName);
            if (File.Exists(csaFileNewName))
            {
                Global.systemLog.Error("Custom subject area already exist - " + csaFileNewName);
                WebServiceSessionHelper.error("renameSubjectArea", "Custom subject area " + newName + " already exists");
            }

            XmlNode[] obj = (XmlNode[])getSubjectAreaService(token, user, sp).renameCustomSubjectArea(name, newName);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling renameSubjectArea - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in renameSubjectArea custom subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("renameSubjectArea", userMessage);
            }
        }

        internal static void setSubjectAreaDescription(User user, string token, string spaceID, string name, string description)
        {
            Space sp = getSpace(user, spaceID, "setSubjectAreaDescription", isSpaceAdmin);
            Global.systemLog.Info("Webservice setSubjectAreaDescription request for user " + user.Username + " on space " + sp.Name + " - " + name);
            validateCSAName("setSubjectAreaDescription", name);

            if (isCustomSubjectAreaDefaultNamed(name))
            {
                WebServiceSessionHelper.error("setSubjectAreaDescription", "Cannot modify the description of default subject area");
            }

            string csaFileName = getCSAFileName(sp, name);
            if (!File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("setSubjectAreaDescription", "Custom subject area " + name + " does not exist");
            }

            XmlNode[] obj = (XmlNode[])getSubjectAreaService(token, user, sp).setSubjectAreaDescription(name, description);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling setSubjectAreaDescription - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in modifying description of custom subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("setSubjectAreaDescription", userMessage);
            }
        }

        internal static string getSubjectAreaDescription(User user, string token, string spaceID, string name)
        {
            Space sp = getSpace(user, spaceID, "getSubjectAreaDescription", isSpaceAdmin);
            Global.systemLog.Info("Webservice getSubjectAreaDescription request for user " + user.Username + " on space " + sp.Name + " - " + name);
            validateCSAName("getSubjectAreaDescription", name);

            string csaFileName = getCSAFileName(sp, name);
            if (!isCustomSubjectAreaDefaultNamed(name) && !File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("getSubjectAreaDescription", "Custom subject area " + name + " does not exist");
            }

            XmlNode[] obj = (XmlNode[])getSubjectAreaService(token, user, sp).getSubjectAreaDescription(name);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (wsResponseXml.isSuccesssful())
            {
                return WebServiceResponseXml.getFirstElementValue(wsResponseXml.getResult(), "Description");                 
            }
            else
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling getSubjectAreaDescription - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in getting custom subject area description";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("getSubjectAreaDescription", userMessage);
            }

            return null;
        }      
        
        internal static void createSubjectArea(User user, string token, string spaceID, string name, string description, string[] groups)
        {
            Space sp = getSpace(user, spaceID, "createSubjectArea", isSpaceAdmin);
            Global.systemLog.Info("Webservice createSubjectArea request for user " + user.Username + " on space " + sp.Name + " - " + name);

            validateCSAName("createSubjectArea", name);
            if (isCustomSubjectAreaDefaultNamed(name))
            {
                WebServiceSessionHelper.error("createSubjectArea", "Custom subject area cannot be named Default Subject Area");
            }

            checkInvalidCharsInSubjectAreaName("createSubjectArea", name);

            if (!Directory.Exists(sp.Directory + "\\custom-subject-areas"))
            {
                Directory.CreateDirectory(sp.Directory + "\\custom-subject-areas");
            }

            string csaFileName = getCSAFileName(sp, name);
            if (File.Exists(csaFileName))
            {
                WebServiceSessionHelper.error("createSubjectArea", "Custom subject area with the same name already exists");
            }


            if (groups == null || groups.Length == 0)
            {
                WebServiceSessionHelper.error("createSubjectArea", "No groups provided");
            }

            // check if there are any groups
            if (!checkGroupsExistInSpace(sp, groups))
            {
                WebServiceSessionHelper.error("createSubjectArea", "One or more groups do not exist in the space");
            }
                        
            SubjectAreaService.SubjectArea subjectAreaService = getSubjectAreaService(token, user, sp);
            XmlNode[] obj = (XmlNode[])subjectAreaService.createCustomSubjectArea(name, groups, description);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling createCustomSubjectArea - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Error in creating custom subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("createSubjectArea", userMessage);
            }
        }
        
        private static SubjectAreaService.SubjectArea getSubjectAreaService(string token, User user, Space sp)
        {
            CookieContainer cc = SMILogin(token, user, sp);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            SubjectAreaService.SubjectArea subjectAreaService = new SubjectAreaService.SubjectArea();
            subjectAreaService.Url = localprotocol + sc.LocalURL + ENDPOINT_SUBJECTAREA;
            subjectAreaService.CookieContainer = cc;
            return subjectAreaService;
        }

        internal static void setSubjectArea(User user, string token, string spaceID, string saContent)
        {
            Space sp = getSpace(user, spaceID, "setSubjectArea", isSpaceAdmin);
            Global.systemLog.Info("Webservice setSubjectArea request for user " + user.Username + " on space " + sp.Name);
            SubjectAreaService.SubjectArea saService = getSubjectAreaService(token, user, sp);
            XmlNode[] obj = (XmlNode[])saService.setCustomSubjectArea(saContent);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling setSubjectArea - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());

                string userMessage = "Error in setting custom subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }
                WebServiceSessionHelper.error("setSubjectArea", userMessage);
            }
        }

        internal static void deleteSubjectArea(User user, string token, string spaceID, string name)
        {
            Space sp = getSpace(user, spaceID, "deleteSubjectArea", isSpaceAdmin);
            Global.systemLog.Info("Webservice deleteSubjectArea request for user " + user.Username + " on space " + sp.Name + " - " + name);
            if (!Directory.Exists(sp.Directory + "\\custom-subject-areas"))
            {
                Directory.CreateDirectory(sp.Directory + "\\custom-subject-areas");
            }

            validateCSAName("deleteSubjectArea", name);
            if (isCustomSubjectAreaDefaultNamed(name))
            {
                WebServiceSessionHelper.error("deleteSubjectArea", "Cannot delete default subject area");
            }

            string csaFileName = getCSAFileName(sp, name);
            if (!File.Exists(csaFileName))
            {
                Global.systemLog.Error("Custom subject area does not exist - " + csaFileName);
                WebServiceSessionHelper.error("deleteSubjectArea", "Custom subject area " + name + " does not exist");
            }

            SubjectAreaService.SubjectArea subjectAreaService = getSubjectAreaService(token, user, sp);
            XmlNode[] obj = (XmlNode[])subjectAreaService.deleteCustomSubjectAreaLite(name);
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling deleteCustomSubjectArea - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());

                string userMessage = "Error in deleting custom subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = errorMessage;
                }

                WebServiceSessionHelper.error("deleteSubjectArea", userMessage);
            }
        }

        internal static string[] listCustomSubjectAreas(User user, string spaceID)
        {
            Space sp = getSpace(user, spaceID, "listCustomSubjectAreas", isSpaceAdmin);
            Global.systemLog.Info("Webservice listCustomSubjectAreas request for user " + user.Username + " from space " + sp.Name);

            string directoryName = sp.Directory + "/custom-subject-areas";
            if (!Directory.Exists(directoryName))
                return new string[0];

            string[] customSubjectAreas = Directory.GetFiles(directoryName, "subjectarea-metadata_*.xml");
            if (customSubjectAreas == null || customSubjectAreas.Length == 0)
                return customSubjectAreas;

            string[] ret = new string[customSubjectAreas.Length];
            for (uint i = 0; i < customSubjectAreas.Length; i++)
            {
                ret[i] = customSubjectAreas[i].Substring(directoryName.Length + 22);
                ret[i] = ret[i].Substring(0, ret[i].Length - 4);
            }

            return ret;
        }

        internal static string getSubjectAreaContent(string token, string spaceID, string name)
        {
            // Prepare helper vars.
            name = name.Trim();
            StringBuilder subjectAreaContent = new StringBuilder();
            User user = WebServiceSessionHelper.getUserForToken(token);
            Space sp = getSpace(user, spaceID, "getSubjectAreaContent", isSpaceAdmin);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            CookieContainer cc = SMILogin(token, user, sp);

            // Build WS vars.
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            SubjectAreaService.SubjectArea saService = new SubjectAreaService.SubjectArea();
            saService.CookieContainer = cc;
            saService.Url = localprotocol + sc.LocalURL + ENDPOINT_SUBJECTAREA;
            
            // Call subject area WS.
            XmlNode[] nodes = (XmlNode[])((name == null || name.Equals(String.Empty) || name.Equals(DEFAULT_SUBJECT_AREA, StringComparison.CurrentCultureIgnoreCase)) ?
                saService.getDefaultSubjectArea().@return : saService.getSubjectAreaByName(name));

            // Check results.
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(nodes[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling getSubjectAreaContent - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());

                string userMessage = "Error in getting subject area";
                if (errorCode == WebServiceResponseXml.ERROR_OTHER)
                {
                    userMessage = (errorMessage != null && errorMessage.Trim().Length > 0) ? errorMessage : wsResponseXml.getResult().Value;
                }
                WebServiceSessionHelper.error("getSubjectAreaContent", userMessage);
            }

            // Write output.
            for (int i = 0; i < nodes.Length; i++)
            {
                subjectAreaContent.Append(nodes[i].InnerText.Remove(0, 1));
            }

            return subjectAreaContent.ToString();
        }

        internal static Status.StatusResult getStatus(User user, string jobToken)
        {
            BackgroundModule bm = null;
            backgroundProcesses.TryGetValue(jobToken, out bm);
            if (bm != null)
            {
                return bm.getStatus();
            }
            return null;
        }

        public static bool isValidIPAddress(string ipAddress)
        {
            bool isValid = false;
            try
            {
                RegexStringValidator validator = new RegexStringValidator(ipRegEx);
                validator.Validate(ipAddress);
                isValid = true;
            }
            catch (Exception)
            {
            }
            return isValid;
        }

        internal static string deleteSpaceData(User user, string spaceID, bool deleteAll)
        {
            Space sp = getSpace(user, spaceID, "deleteSpaceData", isSpaceAdmin);
            Global.systemLog.Info("Webservice deleteSpaceData request for user " + user.Username + " from space " + sp.Name + " delete " + (deleteAll ? "all." : "last."));
                        
            int spOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            bool hasError = false;
            if (!SpaceOpsUtils.isSpaceAvailable(spOpID))
            {
                WebServiceSessionHelper.warn("deleteData", "Space " + spaceID + " is not available. " + SpaceOpsUtils.getAvailabilityMessage(null, spOpID));
                hasError = true;
            }
            
            if (Util.checkForPublishingStatus(sp))
            {
                WebServiceSessionHelper.warn("deleteData", "Space " + spaceID + " is currently publishing");
                hasError = true;
            }
            bool newRep;
            MainAdminForm maf = Util.loadRepository(sp, out newRep);
            if (maf == null)
            {
                WebServiceSessionHelper.warn("deleteData", "Not able to load repository for Space " + spaceID + ".");
                hasError = true;
            }
            if (hasError)
                return null;
			maf.setRepositoryDirectory(sp.Directory);
            DeleteDataThread ddt = new DeleteDataThread(sp, maf, deleteAll, Util.getDeleteDataCommand(sp), null,
                WebServiceSessionHelper.mainSchema, false, sp.LoadNumber, user);
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, ddt);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            if (Util.useExternalSchedulerForDeletion(sp, maf))
            {
                SchedulerUtils.scheduleDeleteJob(sp.ID, user.ID, deleteAll, false, sp.LoadNumber, null);
            }
            else
            {
                BackgroundProcess.startModule(new BackgroundTask(ddt, 0));
            }

            return token;
        }

        class AddHeaderAdhoc : Acorn.AdhocService.Adhoc
        {
           public Dictionary<string, string> additionalHeaders;

            public void addHeader(string key, string value)
            {
                if (additionalHeaders == null)
                    additionalHeaders = new Dictionary<string, string>();

                additionalHeaders.Add(key, value);
            }

            protected override WebRequest GetWebRequest(Uri uri)
            {
                var request = base.GetWebRequest(uri);
                if (additionalHeaders != null)
                {
                    foreach (string key in additionalHeaders.Keys)
                    {
                        request.Headers.Add(key, additionalHeaders[key]);
                    }
                }
                return request;
            }
        }


        class ExportReport : SimpleBackgroundModule
        {
            private User user;
            private string spaceID;
            private string reportPath;
            private List<Filter> reportFilters;
            private string type;
            private string data;
            private static string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

            public string returnData = null;

            public ExportReport(User user, string spaceID, string reportPath, List<Filter> reportFilters, string type, string data)
                : base()
            {
                this.user = user;
                this.spaceID = spaceID;
                this.reportPath = reportPath;
                this.reportFilters = reportFilters;
                this.type = type;
                this.data = data;
            }

            public override void run()
            {
                try
                {
                    Space sp = getSpace(user, spaceID, "exportReport", anyoneCanDoThis);
                    bool newRep;
                    MainAdminForm maf = Util.loadRepository(sp, out newRep);

                    Util.setLogging(user, sp);
                    Global.systemLog.Info("Webservice executeReport request for user " + user.Username + " from space " + sp.Name + " for report " + reportPath);

                    string token = Util.getSMILoginToken(user.Username, sp); 
                    SMIWebLogin login = new SMIWebLogin();
                    CookieContainer cookies = new CookieContainer();
                    SpaceMembership sm = Util.getSpaceMembershipFromDB(user, sp);
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    bool isSpaceAdmin = (sm != null ? sm.Administrator || sm.Owner : false);
                    if (Util.SMILogin(login, token, cookies, user.isSuperUser, isSpaceAdmin, null, true, user, sp, maf, null, null))
                    {
                        AddHeaderAdhoc adhoc = new AddHeaderAdhoc();
                        adhoc.Url = ExportReport.localprotocol + sc.LocalURL + "/SMIWeb/services/Adhoc";

                        String encodedFilters = "";
                        if (reportFilters != null && reportFilters.Count > 0)
                        {
                            XmlSerializer filterSerializer = new XmlSerializer(typeof(Acorn.Filter[]));
                            StringBuilder filters = new StringBuilder();
                            XmlWriter xmlWriter = new XmlTextWriter(new StringWriter(filters));
                            filterSerializer.Serialize(xmlWriter, reportFilters.ToArray());
                            encodedFilters = filters.ToString();
                        }
                        // asp net session id might not be here for a web service, so use JSESSIONID instead for this
                        Uri serviceUri = new Uri(localprotocol + sc.LocalURL + Util.loginServiceEndPoint);
                        foreach (Cookie c in cookies.GetCookies(serviceUri))
                        {
                            if (c.Name.Equals("JSESSIONID"))
                            {
                                adhoc.addHeader("X-XSRF-TOKEN", c.Value);
                                break;
                            }
                        }
                        adhoc.CookieContainer = cookies;
                        object o = adhoc.preExport(type, "", this.reportPath, encodedFilters, true, true, "", ",", "");
                         if (o is XmlNode[] && ((XmlNode[])o).Length > 0)
                        {
                            XmlNode result = ((XmlNode[])o)[0];
                            if (result.HasChildNodes)
                            {
                                string printIndex = result.LastChild.InnerText;
                                if (result.FirstChild.InnerText == "-4")
                                {
                                    // need to start polling
                                    pollForCompleteness(adhoc, printIndex);
                                }
                                else if (result.FirstChild.InnerText != "0")
                                {
                                    // some error
                                    if (result.FirstChild.NextSibling != null)
                                    {
                                        string errorMessage = result.FirstChild.NextSibling.InnerText;
                                        if (errorMessage == null || errorMessage.Length == 0)
                                        {
                                            errorMessage = "Unknown error " + result.FirstChild.InnerText;
                                        }
                                        SoapException ex = new SoapException("Error in call to export report: " + errorMessage, SoapException.ServerFaultCode);
                                        throw ex;
                                    }
                                }

                                string exportServlet = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ExportServlet"];

                                data = data + "&birst.exportType=" + type + "&birst.jasperPrintId=" + printIndex;
                                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                                string uri = localprotocol + sc.LocalURL + "/SMIWeb/ExportServlet.jsp";
                                Uri exportUri = new Uri(uri);
                                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(exportUri);
                                request.Method = WebRequestMethods.Http.Post;
                                request.ContentLength = bytes.Length;
                                // asp net session id might not be here for a web service, so use JSESSIONID instead for this
                                foreach (string key in adhoc.additionalHeaders.Keys) 
                                {
                                    if (key.Equals("X-XSRF-TOKEN"))
                                    {
                                        request.Headers.Add("X-XSRF-TOKEN", adhoc.additionalHeaders[key]);
                                        break;
                                    }
                                }
                                request.CookieContainer = cookies;
                                request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                                Stream os = request.GetRequestStream();
                                os.Write(bytes, 0, bytes.Length);
                                os.Close();

                                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                Stream reader = response.GetResponseStream();

                                int maxLength = 8192;
                                byte[] inputBuffer = new byte[maxLength];
                                MemoryStream output = new MemoryStream();

                                int len = 0;
                                do
                                {
                                    len = reader.Read(inputBuffer, 0, maxLength);
                                    output.Write(inputBuffer, 0, len);
                                } while (len > 0);
                                reader.Close();

                                returnData = System.Convert.ToBase64String(output.ToArray());

                                SMIWebSession smiWebSession = new SMIWebSession();
                                smiWebSession.Url = localprotocol + sc.LocalURL + Util.sessionServiceEndPoint;
                                smiWebSession.CookieContainer = cookies;
                                smiWebSession.logout();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = ex;
                }
            }

            private void pollForCompleteness(AddHeaderAdhoc adhoc, string pollId)
            {
                string[] ids = new string[1];
                ids[0] = pollId;
                do
                {
                    // sleep for a second
                    Thread.Sleep(3000);
                    object o = adhoc.isReportRenderJobComplete(ids);
                    if (o is XmlNode[] && ((XmlNode[])o).Length > 0)
                    {
                            XmlNode result = ((XmlNode[])o)[0];
                            if (result.HasChildNodes)
                            {
                                if (result.FirstChild.InnerText != "0")
                                {
                                    break;
                                }

                                XmlNode asyncJob = result.LastChild.FirstChild.FirstChild;
                                XmlAttribute id = asyncJob.Attributes["Id"];
                                if (id.InnerText == pollId)
                                {
                                    XmlAttribute isComplete = asyncJob.Attributes["IsComplete"];
                                    if (isComplete.InnerText == "true")
                                        return;
                                }
                            }
                    }
                    else
                    {
                        break;
                    }
                } while (true);

                throw new Exception("Error polling for export completeness");
            }
        }

        internal static void executeScheduledReport(User user, string spaceid, string reportScheduleName)
        {
            Space sp = getSpace(user, spaceid, "executeScheduledReport", isSpaceAdmin);
            Global.systemLog.Info("Webservice executeScheduledReport request for user " + user.Username + " from space " + sp.Name);
            if (reportScheduleName == null || reportScheduleName.Trim().Length == 0)
            {
                WebServiceSessionHelper.error("executeScheduledReport", "Name of the schedule cannot be blank");
            }  
            SchedulerUtils.runNowScheduledReport(sp, user, reportScheduleName);
        }

        internal static string exportReportToPNG(User user, string spaceId, string reportPath, List<Filter> reportFilters, int zoomFactor, int page)
        {
            if (zoomFactor <= 0)
                zoomFactor = 1;
            if (page < 0)
                page = 0;
            string data = "birst.exportZoom=" + zoomFactor + "&birst.exportPage=" + page;
            return exportReport(user, spaceId, reportPath, reportFilters, "png", data);
        }

        private static string exportReport(User user, string spaceId, string reportPath, List<Filter> reportFilters, string type, string data)
        {
            ExportReport exportReport = new ExportReport(user, spaceId, reportPath, reportFilters, type, data);
            string token = WebServiceSessionHelper.getToken();
            associateTokenWithBackgroundProcess(token, exportReport);
            backgroundModuleObserver.addBackgroundProcessToRunningList(token);
            BackgroundProcess.startModule(new BackgroundTask(exportReport, 0));
            return token;
        }

        internal static string getExportedData(User user, string exportingToken)
        {
            if (!WebServiceSessionHelper.validToken(exportingToken))
            {
                WebServiceSessionHelper.error("getExportedData", "exportingToken " + exportingToken + " is not valid or has expired.");
            }

            if (backgroundModuleObserver.isStillRunning(exportingToken))
            {
                return null;
            }
            else
            {
                BackgroundModule bm = null;
                backgroundProcesses.TryGetValue(exportingToken, out bm);
                if (bm != null && bm is ExportReport)
                {
                    ExportReport exp = (ExportReport)bm;
                    return exp.returnData;
                }
            }

            return null;
        }

        internal static string exportReportToPDF(User user, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return exportReport(user, spaceId, reportPath, reportFilters, "pdf", null);
        }

        internal static string exportReportToPPT(User user, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return exportReport(user, spaceId, reportPath, reportFilters, "pdf", null);
        }

        internal static string exportReportToXLS(User user, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return exportReport(user, spaceId, reportPath, reportFilters, "xls", null);
        }

        internal static string exportReportToCVS(User user, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return exportReport(user, spaceId, reportPath, reportFilters, "csv", null);
        }

        internal static string exportReportToRTF(User user, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return exportReport(user, spaceId, reportPath, reportFilters, "rtf", null);
        }

        internal static bool setUpGenericJDBCRealTimeConnectionForSpace(User u, string spaceID, string configFileName, string connectionName,
                    bool useDirectConnection, string sqlType, string driverName, string connectionString, string filter, string userName, string password, int timeout)
        {
            Space sp = getSpace(u, spaceID, "setUpGenericJDBCRealTimeConnectionForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setUpGenericJDBCRealTimeConnectionForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
            try
            {
                return BirstConnectUtil.setUpGenericJDBCRealTimeConnectionForSpace(u, sp, configFileName, connectionName, useDirectConnection, sqlType, driverName, connectionString, filter, userName, password, timeout);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.error("setUpGenericJDBCRealTimeConnectionForSpace", ex.GetBaseException().Message);
            }
            return false;
        }

        internal static bool setUpRealTimeConnectionForSpace(User u, string spaceID, string configFileName, string connectionName,
                    string databaseType, bool useDirectConnection, string host, int port, string databaseName, string userName, string password, int timeout)
        {
            Space sp = getSpace(u, spaceID, "setUpRealTimeConnectionForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setUpRealTimeConnectionForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
            try
            {
                return BirstConnectUtil.setUpRealTimeConnectionForSpace(u, sp, configFileName, connectionName, databaseType, useDirectConnection, host, port, databaseName, userName, password, timeout);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.error("setUpRealTimeConnectionForSpace", ex.GetBaseException().Message);
            }
            return false;
        }

        internal static void modifyRealTimeConnectionInformation(User user, string spaceID, string configFileName, string connectionName, string host, int port, string databaseName, string userName, string password, int timeout)
        {
            try
            {
                Space sp = getSpace(user, spaceID, "modifyRealTimeConnectionInformation", isSpaceAdmin);
                Global.systemLog.Info("Webservice modifyRealTimeConnectionInformation request for user " + user.Username + " from space " + sp.Name);

                if (configFileName == null)
                    configFileName = Util.DCCONFIG_FILE_NAME;

                string filename = Path.Combine(sp.Directory, "dcconfig", configFileName);
                if (!File.Exists(filename))
                {
                    WebServiceSessionHelper.error("modifyRealTimeConnectionInformation", "Config file " + filename + " does not exist");
                }
                FileInfo fi = new FileInfo(filename);
                List<Connection> rConnections = BirstConnectUtil.getRealtimeConnectionList(sp, new string[] {fi.FullName});
                bool found = false;
                bool result = false;
                foreach (Connection rc in rConnections)
                {
                    if (rc.VisibleName == connectionName)
                    {
                        found = true;
                        if (host != null)
                            rc.Server = host;
                        if (port > 0)
                            rc.Port = port.ToString();
                        if (databaseName != null)
                            rc.Database = databaseName;
                        if (userName != null)
                            rc.UserName = userName;
                        if (password != null)
                            rc.Password = password;
                        if (timeout > 0)
                            rc.Timeout = timeout.ToString();
                        result = BirstConnectUtil.updateConfigForConnection(sp, filename, rc);
                        break;
                    }
                }
                if (!found)
                {
                    WebServiceSessionHelper.error("modifyRealTimeConnectionInformation", "Connection " + connectionName + " does not exist");
                }
                else if (!result)
                {
                    WebServiceSessionHelper.error("modifyRealTimeConnectionInformation", "Not able to update connection " + connectionName);
                }
            }
            catch (Exception e)
            {
                if (e is SoapException)
                    throw e;

                SoapException ex = new SoapException("Error in call to modifyRealTimeConnectionInformation", SoapException.ServerFaultCode, e);
            }

        }

        internal static bool importCubeMetaDataIntoSpace(User u, string spaceID, string connectionName, string databaseType, string cubeName, int importType, bool cacheable)
        {
            Space sp = getSpace(u, spaceID, "importCubeMetaDataIntoSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("importCubeMetaDataIntoSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
            try
            {
                return BirstConnectUtil.importCubeMetaDataIntoSpace(u, sp, connectionName, databaseType, cubeName, importType, cacheable);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.error("importCubeMetaDataIntoSpace", ex.GetBaseException().Message);
            }
            return false;
        }

        internal static void clearDashboardCache(User user, string token, string spaceID)
        {
            try
            {
                // only space admin can do this
                Space sp = getSpace(user, spaceID, "clearDashboardCache", isSpaceAdmin);
                Global.systemLog.Info("Webservice clearDashboardCache request for user " + user.Username + " from space " + sp.Name);

                CookieContainer cc = SMILogin(token, user, sp);
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SMIAdmin.Admin aa = new SMIAdmin.Admin();
                aa.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/Admin";
                aa.CookieContainer = cc;
                aa.clearDashboards(true, true);
            }
            catch (Exception e)
            {
                if (e is SoapException)
                    throw e;

                SoapException ex = new SoapException("Error in call to clearDashboardCache", SoapException.ServerFaultCode, e);
            }
        }

        static Regex invalidCharacters = new Regex("[\\.\\<\\>\\:\"\\/\\|\\?\\*]");
        internal static void renameDashboard(User user, string token, string spaceID, string dashName, string newDashName)
        {
            try
            {
                if (dashName.Equals(newDashName, StringComparison.OrdinalIgnoreCase))
                {
                    SoapException ex = new SoapException("Error in call to renameDashboard: dashName cannot be the same as newDashName", SoapException.ServerFaultCode);
                    throw ex;
                }

                if (invalidCharacters.IsMatch(newDashName))
                {
                    SoapException ex = new SoapException("Error in call to renameDashboard: dashName cannot contain .<>:\"/|?*", SoapException.ServerFaultCode);
                    throw ex;
                }

                // rename dashboard
                Space sp = getSpace(user, spaceID, "renameDashboardPage", isSpaceAdmin);
                Global.systemLog.Info("Webservice renameDashboardPage request for user " + user.Username + " from space " + sp.Name);

                CookieContainer cc = SMILogin(token, user, sp);
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

                DashboardService.Dashboard dash = new DashboardService.Dashboard();
                dash.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/Dashboard";
                dash.CookieContainer = cc;
                Object o = dash.changeDashboardName(dashName, newDashName);
                if (o is XmlNode[] && ((XmlNode[])o).Length > 0)
                {
                    XmlNode result = ((XmlNode[])o)[0];
                    if (result.HasChildNodes)
                    {
                        if (result.FirstChild.InnerText != "0")
                        {
                            // some error
                            if (result.FirstChild.NextSibling != null)
                            {
                                string errorMessage = result.FirstChild.NextSibling.InnerText;
                                if (errorMessage == null || errorMessage.Length == 0)
                                {
                                    errorMessage = "Unknown error " + result.FirstChild.InnerText;
                                }
                                SoapException ex = new SoapException("Error in call to renameDashboard: " + errorMessage, SoapException.ServerFaultCode);
                                throw ex;
                            }
                        }
                    }
                }

                // clear cache
                clearDashboardCache(user, token, spaceID);
            }
            catch (Exception e)
            {
                if (e is SoapException)
                    throw e;

                SoapException ex = new SoapException("Error in call to renameDashboard", SoapException.ServerFaultCode, e);
            }
        }

        internal static void renameDashboardPage(User user, string token, string spaceID, string dashName, string pageName, string newPageName)
        {
            try
            {
                if (pageName.Equals(newPageName, StringComparison.OrdinalIgnoreCase))
                {
                    SoapException ex = new SoapException("Error in call to renameDashboardPage: pageName cannot be the same as newPageName", SoapException.ServerFaultCode);
                    throw ex;
                }

                if (invalidCharacters.IsMatch(newPageName))
                {
                    SoapException ex = new SoapException("Error in call to renameDashboardPage: newPageName cannot contain .<>:\"/|?*", SoapException.ServerFaultCode);
                    throw ex;
                }

                // rename page
                Space sp = getSpace(user, spaceID, "renameDashboardPage", isSpaceAdmin);
                Global.systemLog.Info("Webservice renameDashboardPage request for user " + user.Username + " from space " + sp.Name);

                CookieContainer cc = SMILogin(token, user, sp);
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

                DashboardService.Dashboard dash = new DashboardService.Dashboard();
                dash.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/Dashboard";
                dash.CookieContainer = cc;

                Object o = dash.changePageName(dashName, pageName, newPageName);
                if (o is XmlNode[] && ((XmlNode[])o).Length > 0)
                {
                    XmlNode result = ((XmlNode[])o)[0];
                    if (result.HasChildNodes)
                    {
                        if (result.FirstChild.InnerText != "0")
                        {
                            // some error
                            if (result.FirstChild.NextSibling != null)
                            {
                                string errorMessage = result.FirstChild.NextSibling.InnerText;
                                if (errorMessage == null || errorMessage.Length == 0)
                                {
                                    errorMessage = "Unknown error " + result.FirstChild.InnerText;
                                }
                                SoapException ex = new SoapException("Error in call to renameDashboardPage: " + errorMessage, SoapException.ServerFaultCode);
                                throw ex;
                            }
                        }
                    }
                }

                // clear cache
                clearDashboardCache(user, token, spaceID);
            }
            catch (Exception e)
            {
                if (e is SoapException)
                    throw e;

                SoapException ex = new SoapException("Error in call to renameDashboardPage", SoapException.ServerFaultCode, e);
            }
        }

        internal static void enableSourceInSpace(User user, string spaceID, string dataSourceName, bool enabled)
        {
            Space sp = getSpace(user, spaceID, "enableSourceInSpace", isSpaceAdmin);
            Global.systemLog.Info("Webservice enableSourceInSpace request for user " + user.Username + " from space " + sp.Name + (enabled ? " enabling " : " disabling ") + dataSourceName);

            // find source with that name
            bool newRepository = false;
            MainAdminForm maf = Util.loadRepository(sp, out newRepository);
            List<StagingTable> sts = maf.stagingTableMod.getAllStagingTables();
            for (int i = 0; i < sts.Count; i++)
            {
                string sfName = sts[i].SourceFile;
                int index = sfName.LastIndexOf('.');
                if (index > 0)
                {
                    sfName = sfName.Substring(0, index);
                }
                sfName = sfName.ToLower();
                if (sfName == dataSourceName.ToLower())
                {
                    sts[i].Disabled = !enabled;
                    Util.saveApplication(maf, sp, null, user);
                    return;
                }
            }

            // if got here, didn't find source
            WebServiceSessionHelper.warn("enableSourceInSpace", "Could not find data source named " + dataSourceName + " in space " + sp.Name);
        }

        internal static List<Language> listLanguages(User user)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                return Database.listLanguages(conn, WebServiceSessionHelper.mainSchema);
            }
            catch (Exception e)
            {
                WebServiceSessionHelper.error("listLanguages ", e.GetBaseException().Message);
            }
            finally
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }
            return null;
        }

        internal static string getLanguageForUser(User user, string username)
        {
            User newuser = getUser(user, username, true, "getLanguageForUser");
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("getLanguageForUser", "User " + user.Username + " tried to get the language for a non-existant user " + username);
            }

            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add("Locale");
            IDictionary<string, string> preferences = ManagePreferences.getUserPreferences(null, newuser.ID, preferenceKeys);
            return preferences["Locale"];
        }

        internal static void setLanguageForUser(User user, string username, string localeId)
        {
            User newuser = getUser(user, username, true, "setLanguageForUser");
            if (newuser == null)
            {
                WebServiceSessionHelper.warn("setLanguageForUser", "User " + user.Username + " tried to set the language for a non-existant user " + username);
            }

            List<Language> supportedLanguages = listLanguages(user);
            bool found = false;
            for (int i = 0; i < supportedLanguages.Count && found == false; i++)
            {
                if (supportedLanguages[i].code.Equals(localeId, StringComparison.InvariantCultureIgnoreCase))
                {
                    localeId = supportedLanguages[i].code;
                    found = true;
                }
            }
            if (!found)
            {
                WebServiceSessionHelper.warn("setLanguageForUser", "User " + user.Username + " tried to set a non-existent language " + localeId + " for a user " + username);
            }
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add("Locale");
            UserPreference localePreference
                = new UserPreference(UserPreference.Type.User, "Locale", localeId);
            List<UserPreference> preferences
                = new List<UserPreference>(new UserPreference[] { localePreference });
            ManagePreferences.updateUserPreferences(null, newuser.ID, Guid.Empty, preferences);
        }
        static Regex spaceNameRegEx = new Regex("^[^\\.<>:\"/\\\\|\\?\\*]{1,255}$");
        internal static void setSpaceName(User u, string spaceID, string spaceName)
        {
            spaceName = spaceName.Trim();
            if (!spaceNameRegEx.IsMatch(spaceName))
            {
                WebServiceSessionHelper.warn("setSpaceName", "Space names can't contain the following characters [.,<,>,:,\",/,\\,|,?,*]" + spaceID);
            }
            Space sp = getSpace(u, spaceID, "setSpaceName", isSpaceAdmin);
            if (sp == null)
			{
                WebServiceSessionHelper.warn("setSpaceName", "User " + u.Username + "Unable to get space details " + spaceID);               
           	}
            Space duplicateSpace = Util.checkForDuplicateSpace(u, spaceName);
            if (duplicateSpace != null && duplicateSpace.ID != sp.ID)
            {              
                WebServiceSessionHelper.warn("setSpaceName", "Duplicate space name for User " + u.Username + " can't exist" +spaceID);
            }
            sp.Name = spaceName;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setSpaceName: Space name for User " + u.Username + "and for space" + sp.Name + " has been created ");
            return;
        }

        internal static void setSpaceComments(User u, string spaceID, string spaceComments)
        {
            Space sp = getSpace(u, spaceID, "setSpaceComments", isSpaceAdmin);
            if (sp == null)
			{
                WebServiceSessionHelper.warn("setSpaceComments", "User " + u.Username + "Unable to get space details "+ spaceID);                           
           	}
            sp.Comments = spaceComments;
                   
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setSpaceComments: Space comments for User " + u.Username + "and for space" + sp.Name + " has been added ");
            return;
        }
            static Regex emailRegEx = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                               @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\"  +
                               @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
      
        internal static void setEmailFromForSpace(User u, string spaceID, string email)
        {
            if (!emailRegEx.IsMatch(email))
            {
                WebServiceSessionHelper.warn("setSpaceName", "Please provide a valid email address for space: " + spaceID);              
            }
            Space sp = getSpace(u, spaceID, "setEmailFromForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setEmailFromForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
          
             sp.ScheduleFrom= email;          
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setEmailFromForSpace: Space email for User " + u.Username + "and for space" + sp.Name + " has been added ");
            return;
        }

        internal static void setEmailSubjectForSpace(User u, string spaceID, string subject)
        {
            Space sp = getSpace(u, spaceID, "setEmailSubjectForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setEmailSubjectForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
             sp.ScheduleSubject= subject;           
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Global.systemLog.Info("setEmailSubjectForSpace: Space email  subject for User " + u.Username + "and for space" + sp.Name + " has been added ");
            return;
        }
        static Regex hexRegEx = new Regex("([A-Fa-f0-9]{6})");
        internal static void setBackgroundColorForSpace(User u, string spaceID, string bgcolor)
        {
            if (!hexRegEx.IsMatch(bgcolor))
            {
                WebServiceSessionHelper.warn("setBackgroundColorForSpace", "Please provide a valid hex string" + spaceID); 
            }
            Space sp = getSpace(u, spaceID, "setBackgroundColorForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setBackgroundColorForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
            
             SpaceAdminService.setBGColorProperties(sp,bgcolor);
             Global.systemLog.Info("setBackgroundColorForSpace: Space background color for User " + u.Username + "and for space" + sp.Name + " has been added ");
             return;
          
        }

        internal static void setForegroundColorForSpace(User u, string spaceID, string fgcolor)
        {
            if (!hexRegEx.IsMatch(fgcolor))
            {
                WebServiceSessionHelper.warn("setForegroundColorForSpace", "Please provide a valid hex string" + spaceID); 
            }
            Space sp = getSpace(u, spaceID, "setForegroundColorForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setForegroundColorForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }           
         
             SpaceAdminService.setFGColorProperties(sp,fgcolor);
             Global.systemLog.Info("setForegroundColorForSpace: Space foreground color for User " + u.Username + "and for space" + sp.Name + " has been added ");
             return;      
            
        }

        internal static void setLogoForSpace(string token, string spaceID, byte[] image)
        {
            User u = WebServiceSessionHelper.getUserForToken(token);
            Space sp = getSpace(u, spaceID, "setLogoForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setLogoForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            }
           
            BrowserUpload.setLogo(image, sp);

            Global.systemLog.Info("setLogoForSpace: Space logo for space for User " + u.Username + "and for space" + sp.Name + " has been added ");
            return;

        }

        internal static void setDefaultLogoForSpace(User u, string spaceID)
        {
            Space sp = getSpace(u, spaceID, "setDefaultLogoForSpace", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setDefaultLogoForSpace", "User " + u.Username + "Unable to get space details " + spaceID);
            } 
            Logo l = new Logo();
            byte[] birstlogo = null;
            birstlogo = l.getBirstLogoBytes(birstlogo);
            BrowserUpload.setDefaultLogo(birstlogo, sp);
            Global.systemLog.Info("setDefaultLogoForSpace: Space default logo for User " + u.Username + "and for space" + sp.Name + " has been added ");
            return;

        }

        internal static List<String> getSourcesList(String token, string spaceID)
        {
            User u = WebServiceSessionHelper.getUserForToken(token);
            Space sp = getSpace(u, spaceID, "getSourcesList", isSpaceAdmin);
            if (sp == null)
            {
                WebServiceSessionHelper.warn("getSourcesList", "User " + u.Username + "Unable to get space details " + spaceID);
            }
            MainAdminForm maf = Util.getRepositoryDev(sp, u);
            if (maf == null)
                return null;
            List<string> sstringlist = new List<string>();

            /*
             * Process staging tables within the current repository
             */
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                SourceFile ssf = null;
                if (!sst.LiveAccess)
                {
                    foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                    {
                        if (sst.SourceFile.ToLower() == sf.FileName.ToLower())
                        {
                            ssf = sf;
                            break;
                        }
                    }
                }
                AdminDataSource ads = ManageSources.getAdminDataSource(maf, maf, sp, sst, ssf, false);
                if (ads != null)
                {
                    sstringlist.Add(ads.DisplayName);
                }
            }
            return sstringlist;
        }
        
        internal static StagingTableSubClass getSourceDetails(String token, string spaceID, string sourceName)
        {
            StagingTableSubClass stl = null;
            SourceColumnSubClass ssc = null;
            List<SourceColumnSubClass> newList = null; 
           try{
                User u = WebServiceSessionHelper.getUserForToken(token);
                Space sp = getSpace(u, spaceID, "getSourceDetails", isSpaceAdmin);
                if (sp == null)
                {
                    WebServiceSessionHelper.warn("getSourceData", "User " + u.Username + "Unable to get space details " + spaceID);
                }
                MainAdminForm maf = Util.getRepositoryDev(sp, u);
                if (maf == null)
                    return null;
                //bool importedSource = false;  
                StagingTable st = maf.stagingTableMod.findTableFromSourceName(sourceName);  
                SourceFile sf = null;           
                if (st != null)
                {
                    if (!st.LiveAccess)
                    {
                        List<SourceFile> sflist = maf.sourceFileMod.getSourceFiles();
                        foreach (SourceFile ssf in sflist)
                        {
                            string fname = ssf.FileName;
                            int extindex = fname.LastIndexOf('.');
                            if (extindex >= 0)
                                fname = fname.Substring(0, extindex);
                            if (fname.ToLower().Equals(sourceName.ToLower()))
                            {
                                sf = ssf;
                                break;
                            }
                        }
                    }
                    if (st.InheritTables != null)
                    {
                        foreach (InheritTable it in st.InheritTables)
                        {
                            if (it.Prefix == null)
                                continue;
                            it.Prefix = it.Prefix.Replace(" ", "&nbsp;");
                        }
                    }
                }
                else
                {
                    // Get table info if imported
                    List<ImportedRepositoryItem> ilist = Util.getImportedTables(maf, null, sourceName);
                    {
                        if (ilist != null && ilist.Count > 0 && ilist[0].st != null)
                        {
                            st = ilist[0].st;
                            //importedSource = true;
                            if (!st.LiveAccess)
                                sf = ilist[0].sf;
                        }
                    }
                }

                string localConnNameForSource = null; //visible name 
                if (st != null)
                {
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    if (st.LoadGroups != null && leConfigs.Count > 0)
                    {
                        foreach (string lg in st.LoadGroups)
                        {
                            if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.Keys.Contains(lg))
                            {
                                LocalETLConfig leconfig = leConfigs[lg];
                                localConnNameForSource = leconfig.getLocalETLConnection();
                                break;
                            }
                        }
                    }
                }
         
               if (st != null)
               {
                   stl = new StagingTableSubClass();
                   stl.Name = st.getDisplayName();
                   stl.OriginalName = st.getDisplayName();
                   stl.Disabled = st.Disabled;
                   if (st.Script != null && st.Script.InputQuery != null)
                   {
                       stl.Script = st.Script;
                   }
                   else
                   {
                       stl.Script = null;
                   }
                   stl.LastModifiedDate = st.LastModifiedDate;
                   stl.SubGroups = st.SubGroups;

                   int sti = st.Columns.Length;
               
                    if (st == null || st.Columns == null)
                        newList = new List<SourceColumnSubClass>();
                    else
                        newList = new List<SourceColumnSubClass>(st.Columns.Length);      
                
                    foreach (StagingColumn foundsc in st.Columns)
                       {
                           SourceFile.SourceColumn sourceColumn = getStagingColumnFromSourceColumn(maf, foundsc, st.SourceFile);
                           ssc = new SourceColumnSubClass();
                           ssc.Name = foundsc.Name;
                           ssc.OriginalName = foundsc.Name;
                           ssc.DataType = foundsc.DataType;                        
                           ssc.Width = foundsc.Width;                       
                           ssc.TargetAggregations = foundsc.TargetAggregations;
                           List<string> ttypes = new List<string>();
                           if (foundsc.TargetTypes != null)
                           {
                               foreach (string s in foundsc.TargetTypes)
                                   if (s != null)
                                   {
                                       string measure = "Measure";
                                       if (s.ToLower().Equals(measure.ToLower()))
                                       {
                                           ssc.Measure = true;
                                       }
                                       else
                                       {
                                           ttypes.Add(s);
                                       }
                                   } 
                           }
                           ssc.TargetTypes = ttypes.ToArray();
                           ssc.HierarchyName = st.HierarchyName;
                           ssc.LevelName = st.LevelName;
                           ssc.Levels = st.Levels;                     
                           ssc.UnknownValue = (foundsc.UnknownValue != null && foundsc.UnknownValue.Length > 0) ? foundsc.UnknownValue : null;

                           ssc.EnableSecutityFilter = foundsc.SecFilter != null && foundsc.SecFilter.Enabled;
                           ssc.AnalyzeMeasure = foundsc.AnalyzeMeasure;
                           ssc.AnalyzeByDate = foundsc.GenerateTimeDimension;
                           ssc.LockType = sourceColumn == null ? false : sourceColumn.PreventUpdate;
                           if (foundsc.SourceFileColumn != null && !foundsc.DataType.StartsWith("Date ID:"))
                                  {
                                      foreach (SourceFile.SourceColumn sfc in sf.Columns)
                                      {
                                          if (sfc.Name != null && sfc.Name.Equals(foundsc.SourceFileColumn))
                                          {
                                         
                                             ssc.Format= sfc.Format;
                                              break;
                                          }
                                      }
                           newList.Add(ssc);                        
                       } 
                          stl.Columns = newList.ToArray();
                  }
                    return stl;
                   }
               }
            catch(Exception)
            {
                Global.systemLog.Error("SourceName is not valid. Please provide valid sourceName.");
            }
           return stl;
         }


        private static SourceFile.SourceColumn getStagingColumnFromSourceColumn(MainAdminForm maf, StagingColumn sc, string sourceFile)
        {
            SourceFile sf = maf.sourceFileMod.getSourceFile(sourceFile);
            if (sf != null)
            {
                foreach (SourceFile.SourceColumn sfc in sf.Columns)
                {
                    if (sfc.Name == sc.SourceFileColumn)
                        return sfc;
                }
            }
            return null;
        }


        internal static void setSourceDetails(User user, string spaceID, StagingTableSubClass data)
        {
            Space sp = getSpace(user, spaceID, "setSourceDetails", isSpaceAdmin);
             StagingColumn sc = null;
             SourceFile.SourceColumn sfc = null;
             string originalName;
            if (sp == null)
            {
                WebServiceSessionHelper.warn("setSourceDetails", "User " + user.Username + "Unable to get space details " + spaceID);
            }
            MainAdminForm maf = Util.getRepositoryDev(sp, user);
            if (maf == null)
            {
                WebServiceSessionHelper.warn("setSourceDetails", "User " + user.Username + "Unable to load repository " + spaceID);
            }

            StagingTable st = maf.stagingTableMod.findTableFromSourceName(data.Name);
            ManageSources.SourceData sd = ManageSources.getSourceData(maf, sp, user, st.Name, data.Name, null);
            if (sd.imported)
            {
                WebServiceSessionHelper.warn("setSourceDetails", "Imported sources can not be changed " + data.Name);
            }

            Dictionary<String, SourceFile.SourceColumn> sdSourceColumns = new Dictionary<string, SourceFile.SourceColumn>();
            foreach (SourceFile.SourceColumn scc in sd.sourceFile.Columns)
            {
                sdSourceColumns.Add(scc.Name, scc);
            }

            Dictionary<String, StagingColumn> sdStagingColumns = new Dictionary<string, StagingColumn>();
            foreach (StagingColumn scc in sd.stagingTable.Columns)
            {
                sdStagingColumns.Add(scc.Name, scc);
            }

            /*
            if (data.Columns == null || data.Columns.Length != sd.sourceFile.Columns.Length)
            {
                WebServiceSessionHelper.error("setSourceDetails", "Number of columns to set is not the same as the number of columns currently in the source.");
            }

            foreach (SourceColumnSubClass sc in data.Columns)
            {
                if (!sdStagingColumns.Keys.Contains(sc.Name))
                {
                    WebServiceSessionHelper.warn("setSourceDetails", "Could not find column name " + sc.Name + " in source to modify.");
                }
            }
             */

            bool isPublished = Status.hasLoadedData(sp, null);
            // validate data       
            foreach (SourceColumnSubClass scsc in data.Columns)
            {
                    originalName = scsc.OriginalName;
                    if (originalName == null || originalName.Length == 0)
                        originalName = scsc.Name;
                    if(sdSourceColumns.ContainsKey(originalName) && sdSourceColumns.ContainsKey(originalName))
                    {
                         sc = sdStagingColumns[originalName];
                        sfc = sdSourceColumns[originalName];
                    }
                    else
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Column name: " + originalName + "is not present in data source");
                    }

                    if (scsc.DataType != sc.DataType && isPublished)
                    {
                        // if published can only change type to less restrictive type
                        if (sc.DataType == "DateTime" && scsc.DataType != "Varchar" && scsc.DataType != "DateTime")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                        else if (sc.DataType == "Date" && scsc.DataType != "Varchar" && scsc.DataType != "DateTime" && scsc.DataType != "Date")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                        else if (sc.DataType == "Varchar" && scsc.DataType != "Varchar")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                        else if (sc.DataType == "Number" && scsc.DataType != "Varchar" && scsc.DataType != "Float" && scsc.DataType != "Number")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                        else if (sc.DataType == "Float" && scsc.DataType != "Varchar" && scsc.DataType != "Float" && scsc.DataType != "Number")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                        else if (sc.DataType == "Integer" && scsc.DataType != "Varchar" && scsc.DataType != "Float" && scsc.DataType != "Number" && scsc.DataType != "Integer")
                        {
                            WebServiceSessionHelper.error("setSourceDetails", "Can not convert column " + scsc.Name + " in source " + data.Name + " from datatype " + sc.DataType + " to " + scsc.DataType + " because the data has been published.");
                        }
                    }

                    if (scsc.AnalyzeByDate != sc.GenerateTimeDimension && scsc.DataType != "Date" && scsc.DataType != "DateTime")
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Can not set Analyze By Date on column " + scsc.Name + " of type " + scsc.DataType);
                    }

                    if (scsc.AnalyzeMeasure != sc.AnalyzeMeasure && scsc.Measure != true)
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Can not set Analyze By Measure on column " + scsc.Name + " that is not a measure column");
                    }

                    if (scsc.UnknownValue != null && !Command.isValidUnknown(scsc.UnknownValue))
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Can not set Unknown Value on column " + scsc.Name + ". The valid unknown characters are [a-zA-Z0-9_\\.\u0080-\uFFCF\\-@\\$#%\\*&:;\\/<>,=\\+|]");
                    }
                }

                // ok everything's valid.  Make changes and save
                foreach (SourceColumnSubClass scsc in data.Columns)
                {
                    originalName = scsc.OriginalName;
                    if (originalName == null || originalName.Length == 0)
                        originalName = scsc.Name;
                    if (sdSourceColumns.ContainsKey(originalName) && sdSourceColumns.ContainsKey(originalName))
                    {
                        sc = sdStagingColumns[originalName];
                        sfc = sdSourceColumns[originalName];
                    }
                    else
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Column name: " + originalName + "is not present in data source");
                    }
                    sc.Name = scsc.Name;
                    sfc.Name = scsc.Name;
                    sc.DataType = scsc.DataType;
                    sc.Width = scsc.Width;
                    sfc.Width = scsc.Width;
                    sc.TargetAggregations = scsc.TargetAggregations;
                    sc.TargetTypes = scsc.TargetTypes;
                    if (scsc.Measure)
                    {
                        if (!sc.TargetTypes.Contains("Measure"))
                        {
                            List<string> l = sc.TargetTypes.ToList<string>();
                            l.Add("Measure");
                            sc.TargetTypes = l.ToArray();
                        }
                    }
                    sd.stagingTable.HierarchyName = scsc.HierarchyName;
                    sd.stagingTable.LevelName = scsc.LevelName;
                    sd.stagingTable.Levels = scsc.Levels;
                    sc.UnknownValue = scsc.UnknownValue == null || scsc.UnknownValue.Length == 0 ? null : scsc.UnknownValue;
                    if (sc.SecFilter != null)
                    {
                        sc.SecFilter.Enabled = scsc.EnableSecutityFilter;
                    }
                    else if (scsc.EnableSecutityFilter)
                    {
                        WebServiceSessionHelper.error("setSourceDetails", "Can not enable security filter on column " + scsc.Name + ". The security filter is not defined on column.");
                    }
                    sc.AnalyzeMeasure = scsc.AnalyzeMeasure;
                    sc.GenerateTimeDimension = scsc.AnalyzeByDate;
                    sfc.Format = scsc.Format;
                    sfc.PreventUpdate = scsc.LockType;
                }
                sd.stagingTable.Disabled = data.Disabled;
                sd.stagingTable.Script = data.Script;
                sd.stagingTable.SubGroups = data.SubGroups;

                ManageSources.saveSourceDataAndHierarchies(sd, null, sp, user, maf, null, user);

                Util.saveApplication(maf, sp, null, user);       
        }    
     
       internal static bool IsImageExtension(string fileName)
        {
           bool allowedExt = false;
            String fileExtension = 
                System.IO.Path.GetExtension(fileName).ToLower();
            String[] allowedExtensions = 
                {".gif", ".png", ".jpeg", ".jpg"};
          for (int i = 0; i < allowedExtensions.Length; i++)
          {
               if (fileExtension == allowedExtensions[i])
               {
                    allowedExt = true;
                    return allowedExt;
               }
          }
          return allowedExt;
        }
        
       internal static void uploadImageToReportCatalog(string token, string spaceID, string directory , string reportFileName, byte[] image)
         {
             User u = WebServiceSessionHelper.getUserForToken(token);
             Space sp = getSpace(u, spaceID, "uploadImageToReportCatalog", isSpaceAdmin);
             bool ext = IsImageExtension(reportFileName.ToLower());
             string priDir = "private";
             if (ext)
             { 
                 if (directory == null || directory == "")
                 {
                     Global.systemLog.Info("uploadImageToReportCatalog: Full Dirs: " + directory);
                     WebServiceSessionHelper.error("uploadImageToReportCatalog", "User " + u.Username + " for spaces " + sp.Name + " (the source directory (" + directory + ") can not be null)");
                 }                 
                 if (directory.ToLower() == priDir)
                 {
                     WebServiceSessionHelper.warn("uploadImageToReportCatalog", "User " + u.Username + " for spaces " + sp.Name + " (the report file (" + reportFileName+ ") can not be upload file to the private folder)");
                 }

                 String dir = sp.Directory + "/catalog/" + directory;               
                 if (sp == null)
                 {
                     WebServiceSessionHelper.warn("uploadImageToReportCatalog", "User " + u.Username + "Unable to get space details " + spaceID);
                 }
                 if (!Directory.Exists(dir))
                 {
                     Global.systemLog.Info("uploadImageToReportCatalog: Full Dirs: " + dir);
                     WebServiceSessionHelper.warn("uploadImageToReportCatalog", "User " + u.Username + " failed to get the directory for spaces " + sp.Name + " (the source directory (" + dir + ") does not exist)");
                 }
                 Global.systemLog.Info("Webservice uploadImageToReportCatalog request for user " + u.Username + " on space " + sp.Name + " - " + reportFileName);

                 CookieContainer cc = SMILogin(token, u, sp);

                 SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                 string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                 FileService.File fs = new FileService.File();
                 fs.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/File";
                 fs.CookieContainer = cc;
                 fs.uploadReportPathImageFile(dir, reportFileName, image);
                 Global.systemLog.Info("uploadImageToReportCatalog: upload image to report catalog folder for User " + u.Username + "and for space" + sp.Name + " has been added ");
                 return;
             }
             else
             {
                 WebServiceSessionHelper.warn("uploadImageToReportCatalog", "User " + u.Username + " for spaces " + sp.Name + " (the report file with only 'gif', 'jpg' and 'png' extentions are allowed)");
             }          
         }
       internal static string getSpaceComments(User u, string spaceID)
       {
           Space sp = getSpace(u, spaceID, "getSpaceComments", isSpaceAdmin);
           string comments = null;
           if (sp == null)
           {
               WebServiceSessionHelper.warn("getSpaceComments", "User " + u.Username + "Unable to get space details " + spaceID);
           }
           if (sp.Comments != null)
           {
               comments = sp.Comments;
           }

           Global.systemLog.Info("setSpaceComments: Space comments for User " + u.Username + "and for space" + sp.Name + " has been added ");
           return comments;
       }
       internal static void setSpaceProcessEngineVersion(User u, string spaceID, string prcessingVersionName)
       {
           Boolean isFound = false;
           Space sp = getSpace(u, spaceID, "setSpaceProcessEngineVersion", isSpaceAdmin);
           if (sp == null)
           {
               WebServiceSessionHelper.warn("setSpaceProcessEngineVersion", "User " + u.Username + "Unable to get space details " + spaceID);
           }           
           QueryConnection conn = null;
           PerformanceEngineVersion[] peVersion = null;
           PerformaceEngineDetails performanceEngineVersionsList = null;
           PerformanceEngineVersion perVersion = new PerformanceEngineVersion();
           try
           {
               conn = ConnectionPool.getConnection();           
               performanceEngineVersionsList = getPerformanceEngineVersions();
               peVersion = performanceEngineVersionsList.PerformanceEngineVersions;
               foreach (PerformanceEngineVersion perEngineVersion in peVersion)
               {
                   if (perEngineVersion.Name == prcessingVersionName)
                   {
                       isFound = true;
                       break;
                   }
               }
               if (isFound)
               {
                   perVersion = Database.getPerformanceEngineVersionForSpaceFromName(conn, WebServiceSessionHelper.mainSchema, prcessingVersionName);
                   sp.ProcessVersionID = perVersion.ID;
                   Database.updateNonDBSpaceParams(conn, WebServiceSessionHelper.mainSchema, sp);
               }
               else
               {
                   WebServiceSessionHelper.error("setSpaceProcessEngineVersion", "processing engine version name " + prcessingVersionName + " not found in space " + sp.Name);
               }
           }          
           finally
           {
               ConnectionPool.releaseConnection(conn);
           }
           Global.systemLog.Info("setSpaceProcessEngineVersion: Space processing version name for User " + u.Username + "and for space" + sp.Name + " has been added ");
           return;
       }
       internal static PerformanceEngineVersion getSpaceProcessEngineVersion(User u, string spaceID)
       {
           Space sp = getSpace(u, spaceID, "getSpaceProcessEngineVersion", isSpaceAdmin);          
           if (sp == null)
           {
               WebServiceSessionHelper.warn("getSpaceProcessEngineVersion", "User " + u.Username + "Unable to get space details " + spaceID);
           }
           QueryConnection conn = null;
           PerformanceEngineVersion peVersion = null;           
           try
           {
               conn = ConnectionPool.getConnection();
               peVersion = Database.getPerformanceEngineVersionForSpace(conn, WebServiceSessionHelper.mainSchema, sp);  
               
           }
           finally
           {
               ConnectionPool.releaseConnection(conn);
           }

           Global.systemLog.Info("getSpaceProcessEngineVersion: Space process Engine Version for User " + u.Username + "and for space" + sp.Name + " has been added ");
           return peVersion;
       }
       internal static PerformaceEngineDetails getPerformanceEngineVersions()
       {
           PerformaceEngineDetails response = new PerformaceEngineDetails();
           try
           {
               List<PerformanceEngineVersion> performanceEngineVersionsList = Util.getPerformanceEngineDetails(false);
               if (performanceEngineVersionsList != null && performanceEngineVersionsList.Count > 0)
               {
                   response.PerformanceEngineVersions = performanceEngineVersionsList.ToArray();
               }
           }
           catch (Exception ex)
           {
               Global.systemLog.Error("Exception thrown in getPerformanceEngineVersions " + ex);
               response.setException(ex);
           }
           return response;
       }
    }

    [Serializable]
    public class Filter
    {
        public string Operator;
        public string multiSelectType;
        public string FilterType;
        public string ParameterName;
        [XmlArray("selectedValues")]
        [XmlArrayItem("selectedValue", typeof(string))]
        public List<string> values;
    }

    internal class PagedQueryResult
    {
        public List<string[]> rows;
        public string[] columnNames;
        public string[] displayNames;
        public int?[] dataTypes;
        public int numSent;
    }
    public class CommandQueryResult : QueryResult
    {
        public string queryToken = null;
        public int numRowsReturned = 0;
        public bool hasMoreRows = false;

        internal CommandQueryResult()
        {
        }

        internal CommandQueryResult(PagedQueryResult pqr) 
        {
            this.columnNames = pqr.columnNames;
            this.displayNames = pqr.displayNames;
            this.dataTypes = pqr.dataTypes;
        }
    }


    internal class WSFileUploader : BackgroundModule
    {
        ApplicationUploader ul;
        bool isFinished = false;
        UploadFileOptions ufo;
        public DataTable errorTable = null;
        private Dictionary<BackgroundModuleObserver, object> observers;

        public WSFileUploader(ApplicationUploader uploader, UploadFileOptions options)
        {
            ul = uploader;
            ufo = options;
            observers = new Dictionary<BackgroundModuleObserver, object>();
        }
        #region BackgroundModule Members

        public void run()
        {
            try
            {
                // upload and retreive any error during extension processing
                errorTable = ul.uploadFile(ufo.consolidateIdenticalStructures, true, false);
            }
            catch (Exception ex)
            {
                // Prevents this thread from causing server crash
                Global.systemLog.Error("Exception during uploading : ", ex);
            }
        }

        public void kill()
        {
            /* uncomment if implemented
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.killed(kvp.Value);
            }
             */
        }

        public void finish()
        {
            isFinished = true;
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.finished(kvp.Value);
            }
        }

        #endregion

        public bool isComplete()
        {
            return isFinished;
        }

        public Space getSpace()
        {
            return ul.getSpace();
        }

        internal int getSuccessfullCounts()
        {
            return ul.getSuccessfullCounts();
        }

        internal Dictionary<string, ScanSummaryResult> getExcelWarningDetails()
        {
            return ul.getExcelWarningDetails();
        }

        #region BackgroundModule Members


        public void addObserver(BackgroundModuleObserver observer, object o)
        {
            observers.Add(observer, o);
        }

		public Status.StatusResult getStatus()
        {
            Status.StatusResult result = Status.getLoadStatus(null, getSpace());
			return result;
        }

        #endregion
    }


    internal class UploadFileOptions
    {
        // Consolidate source with identical structures
        public bool consolidateIdenticalStructures = true;

        // First row contains column names (does not apply to Access databases)
        public bool columnNamesInFirstRow = true;

        // Excel: Filter likely non-data rows in poorly structured spreadsheets
        public bool filterLikelyNoDataRows = true;

        // Lock data source formats - ignore changes to format
        public bool lockDataSourceFormat = false;

        // Only recognize quotes at the start and end of fields
        public bool ignoreQuotesNotAtStartOrEnd = false;

        // # rows to skip at beginning of file (0-100)
        public int rowsToSkipAtStart = 0;

        // # rows to skip at end of file (0-100)
        public int rowsToSkipAtEnd = 0;

        // Character encoding
        public string encoding = "UTF-8";

        // Replacements - e.g. "\N"="","ST"="Street"
        public string replacements = null;

        public string separator = null;

        public bool autoModel = true;
    }

}
