﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.IO;
using System.Data;
using Performance_Optimizer_Administration;
using System.Text;
using System.Text.RegularExpressions;
using Acorn.Utils;

namespace Acorn
{
    public class PublishLogDetailsService
    {
        private static string SEARCH = " ERROR - [DATAERROR:";
        private static string WARN_SEARCH = " WARN  - [DATAWARNING:";

        public static PublishLogResult getPublishLogDetails(HttpSessionState session, int ln, string loadGroup, string loadDate, string processingGroup)
        {
            PublishLogResult response = new PublishLogResult();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = null;
            if (Util.TEST_FLAG)
            {
                Global.systemLog.Error("Test flag should never be enabled in PRODUCTION.");
                sp = Util.mockSession != null ? (Space)Util.mockSession["space"] : null;
            }
            else
            {
                sp = (Space)session["space"];
            }
            if(sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details";
                return response;
            }
            //ShowLists.Visible = true;            
            //int ln = int.Parse(loadNum);       

            List<PublishLogSummary> publishSummaryList = new List<PublishLogSummary>();
            List<MeasureTableResult> measuresResult = new List<MeasureTableResult>();
            List<DimensionTableResult> dimensionsResult = new List<DimensionTableResult>();
            bool ignoreProcessingGroupCheck = (processingGroup == null || processingGroup.Trim().Length == 0) ? true : false;
            DataTable dt = PublishLog.getPublishSummaryData(session, sp, ln, loadGroup, processingGroup, ignoreProcessingGroupCheck);         
            if (dt.Rows.Count > 0)
            {                                
                foreach (DataRow dr in dt.Rows)
                {
                                       
                    PublishLogSummary row = new PublishLogSummary();
                    row.DataSource =(string) dr["DataSource"];
                    row.Processed = dr["Processed"].ToString(); ;
                    row.Errors = dr["Errors"].ToString();
                    row.Warnings = dr["Warnings"].ToString();
                    row.Duration = (long) dr["Duration"];
                    publishSummaryList.Add(row);
                }

            }
            response.Summary = publishSummaryList.ToArray();
            loadSummaryGrid(session, response, sp, loadDate, ln, loadGroup == null ? Util.LOAD_GROUP_NAME : loadGroup, processingGroup, ignoreProcessingGroupCheck);
           
            return response;
        }

        public static GenericResponse getFileDetails(HttpSessionState session, string filename, string loadDate, bool warnings, int loadNumber)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput errorOutput = new ErrorOutput();
            response.Error = errorOutput;

            Space sp = null;

            if (Util.TEST_FLAG)
            {
                Global.systemLog.Error("Test flag should never be enabled in PRODUCTION.");
                sp = Util.mockSession != null ? (Space)Util.mockSession["space"] : null;
            }
            else
            {
                sp = (Space)session["space"];
            }

            if(sp == null)
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details";
                return response;
            }

            // build list of files to search
            List<string> names = new List<string>();

            // first look for specific log file for the load number
            if (loadNumber > -1)
            {
                string logfile = String.Format("smiengine.*.{0:0000000000}.log", loadNumber);
                string[] fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                if (fnames.Length > 0)
                    names.Add(fnames[0]);
            }

            // if no load number specific log, fall back on date log (it will be nice to finally get rid of this XXX)
            if (names.Count == 0)
            {
                DateTime date = DateTime.Parse(loadDate);
                string logfile = String.Format("smiengine.*.{0:yyyy-MM-dd}.log", date);
                string[] fnames = Directory.GetFiles(sp.Directory + "\\logs\\", logfile, SearchOption.TopDirectoryOnly);
                string logfileprev = String.Format("smiengine.*.{0:yyyy-MM-dd}.log", date.AddDays(-1));
                string[] fnamesprev = Directory.GetFiles(sp.Directory + "\\logs\\", logfileprev, SearchOption.TopDirectoryOnly);

                if (fnamesprev.Length > 0)
                {
                    fnamesprev = Util.sortFiles(fnamesprev);
                    foreach (string fileNamePrev in fnamesprev)
                    {
                        names.Add(fileNamePrev);
                    }
                }

                if (fnames.Length > 0)
                {
                    fnames = Util.sortFiles(fnames);
                    foreach (string fileName in fnames)
                    {
                        names.Add(fileName);
                    }
                }
            }

            // could not find any matching log files
            if (names.Count == 0)
                return response;

            // scan the logs for the items of interest
            List<string> issues = new List<string>();
            DateTime currentLogLineTimeStamp = DateTime.MinValue;
            foreach (string fname in names)
            {
                bool foundStartMarker = false;
                StreamReader reader = new StreamReader(File.Open(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                string line = null;
                MainAdminForm maf = null;
                if (Util.TEST_FLAG)
                {
                    Global.systemLog.Error("Test flag should never be enabled in PRODUCTION.");
                    maf = Util.mockSession != null ? (MainAdminForm)Util.mockSession["MAF"] : null;
                }
                else
                {
                    maf = (MainAdminForm)session["MAF"];
                }

                StagingTable st = null;
                foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
                {
                    if (sst.Name == "ST_" + filename)
                    {
                        st = sst;
                        break;
                    }
                }
                if (st == null)
                    return response;
                SourceFile sf = maf.getSourceFile(st);
                string find = (warnings ? WARN_SEARCH : SEARCH) + sf.FileName + "]";
                try
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        // Have we ever found startMarker in the current file
                        bool markerMatchedForDifferentLoad = false;
                        int startMarkerIndexForCurrentLoad = getStartMarkerIndex(line, loadNumber, out markerMatchedForDifferentLoad);
                        foundStartMarker = startMarkerIndexForCurrentLoad >= 0 ? true : foundStartMarker;
                        // if there are multiple load numbers in the same file, make sure that we dont parse the warnings from other load numbers
                        if (markerMatchedForDifferentLoad)
                        {                            
                            foundStartMarker = false;
                        }

                        if (startMarkerIndexForCurrentLoad >= 0)
                        {
                            // if there is another start marker for this load number, make sure this is the most recent one. It might happen
                            // that even after iteration through sorted files by date, most recent load number (for previous iterations) 
                            // might appear earlier than later since sorted files are based on last modified time 
                            bool isUpdated;
                            DateTime dt = getUpdatedLoadLineTimeStamp(line, loadNumber, currentLogLineTimeStamp, out isUpdated);
                            if (isUpdated)
                            {                                
                                currentLogLineTimeStamp = dt;
                                issues.Clear();
                            }
                        }

                        if (foundStartMarker && line.Contains(find))
                        {
                            int index = line.IndexOf(find) + find.Length;
                            string error = line.Substring(index + 1);
                            issues.Add(error);
                            if (issues.Count > 100)
                                break;
                        }
                        
                    }
                }
                finally
                {
                    reader.Close();
                }               
            }
            response.other = issues.ToArray();
            return response;            
        }

        

        public static GenericResponse getQueryDetails(HttpSessionState session, string queryid, string dim,
            string level, string source, string grain)
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            Space sp = null;
            if (Util.TEST_FLAG)
            {
                Global.systemLog.Error("Test flag should never be enabled in PRODUCTION.");
                sp = Util.mockSession != null ? (Space)Util.mockSession["space"] : null;
            }
            else
            {
                sp = (Space)session["space"];
            }
            if(sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to retrieve space details";
                return response;
            }

            List<string> queryText = new List<string>();
            string brText = "<br/>";
            string encodedBrText = "";
            if (HttpContext.Current != null && HttpContext.Current.Server != null)
            {
                encodedBrText = HttpUtility.HtmlEncode(brText);
            }
            if (queryid != null && session["querymap"] != null)
            {   
                try
                {
                    long id = long.Parse(queryid);
                    string q = ((Dictionary<long, string>)session["querymap"])[id];
                    if (q != null)
                    {                       
                        string query = processQuery(session, sp, q, source, dim, level, grain);
                        if (HttpContext.Current != null && HttpContext.Current.Server != null)
                        {
                            query = HttpUtility.HtmlEncode(query);
                            query = query.Replace(encodedBrText, brText);
                        }
                        queryText.Add(query);
                    }
                }
                catch (Exception)
                {
                }                
            }

            response.other = queryText.ToArray();
            return response;

        }

        private static string processQuery(HttpSessionState session, Space sp, string q, string datasource, string dimension, string level, string grain)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            StagingTable sourcest = null;
            DimensionTable targetdt = null;
            MeasureTable targetmt = null;
            if (datasource != null)
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    if (st.Name.Substring(3) == datasource)
                    {
                        sourcest = st;
                        break;
                    }
                }
            if (dimension != null && level != null)
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (dt.AutoGenerated && (dt.InheritTable == null || dt.InheritTable.Length == 0) &&
                        dt.DimensionName == dimension && dt.Level == level)
                    {
                        targetdt = dt;
                        break;
                    }
                }
            if (grain != null)
                foreach (MeasureTable mt in maf.measureTablesList)
                {
                    if (mt.AutoGenerated && (mt.InheritTable == null || mt.InheritTable.Length == 0) &&
                        grainString(maf, mt) == grain)
                    {
                        targetmt = mt;
                        break;
                    }
                }
            q = q.Replace(sp.Schema + ".", "");
            // Create list of from expressions
            Regex r = new Regex(@"FROM \w+ \w\s");
            MatchCollection mc = r.Matches(q);
            Dictionary<string, List<string>> aliasList = new Dictionary<string, List<string>>();
            foreach (Match m in mc)
            {
                int index1 = m.Value.IndexOf(' ');
                int index2 = m.Value.IndexOf(' ', index1 + 1);
                string tname = m.Value.Substring(index1 + 1, index2 - index1 - 1);
                string alias = m.Value.Substring(index2 + 1, m.Value.Length - index2 - 2);
                List<string> list = null;
                if (aliasList.ContainsKey(tname))
                    list = aliasList[tname];
                else
                {
                    list = new List<string>();
                    aliasList.Add(tname, list);
                }
                list.Add(alias);
            }
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.Length > 0) || dt.TableSource.Tables.Length != 1)
                    continue;
                string tname = dt.TableSource.Tables[0].PhysicalName;
                q = q.Replace(tname + "_CKSUM$", "[" + dt.DimensionName + "." + dt.Level + " checksum]");
            }
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                q = q.Replace(st.Name + "_CKSUM$", "[" + st.Name.Substring(3) + " checksum]");
            }
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.Length > 0) || dt.TableSource.Tables.Length != 1)
                    continue;
                string tname = dt.TableSource.Tables[0].PhysicalName;
                List<string> pnames = new List<string>();
                pnames.Add(tname);
                if (aliasList.ContainsKey(tname))
                    pnames.AddRange(aliasList[tname]);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                        foreach (DataRowView dr in drv)
                        {
                            if (!((bool)dr["Qualify"]))
                                continue;
                            if (q.IndexOf(pname + '.' + dr["PhysicalName"]) >= 0)
                            {
                                if (pname == tname)
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + dt.DimensionName + "." + dt.Level + "." + dr["ColumnName"] + "]");
                                else
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + pname + "." + dr["ColumnName"] + "]");
                            }
                        }
                    }
                }
                q = q.Replace(tname, "[" + dt.DimensionName + "." + dt.Level + "]");
            }
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (!mt.AutoGenerated || (mt.InheritTable != null && mt.InheritTable.Length > 0) || mt.TableSource.Tables.Length != 1)
                    continue;
                string tname = mt.TableSource.Tables[0].PhysicalName;
                List<string> pnames = new List<string>();
                pnames.Add(tname);
                if (aliasList.ContainsKey(tname))
                    pnames.AddRange(aliasList[tname]);
                string grainstr = grainString(maf, mt);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { mt.TableName });
                        foreach (DataRowView dr in drv)
                        {
                            if (!((bool)dr["Qualify"]))
                                continue;
                            if (q.IndexOf(pname + '.' + dr["PhysicalName"]) >= 0)
                            {
                                if (pname == tname)
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + grainstr + "." + dr["ColumnName"] + "]");
                                else
                                    q = q.Replace(pname + '.' + dr["PhysicalName"], "[" + pname + "." + dr["ColumnName"] + "]");
                            }
                        }
                    }
                }
                q = q.Replace(" "+tname+" ", " [" + grainstr + "] ");
            }
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                List<string> pnames = new List<string>();
                pnames.Add(st.Name);
                if (aliasList.ContainsKey(st.Name))
                    pnames.AddRange(aliasList[st.Name]);
                foreach (string pname in pnames)
                {
                    if (q.IndexOf(pname + '.') >= 0)
                    {
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                                continue;
                            string cname = pname + '.' + Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name) + '$';
                            if (q.IndexOf(cname) >= 0)
                            {
                                if (pname == st.Name)
                                    q = q.Replace(cname, "[" + pname.Substring(3) + "." + sc.Name + "]");
                                else
                                    q = q.Replace(cname, "[" + pname + "." + sc.Name + "]");
                            }
                        }
                    }
                }
                q = q.Replace(st.Name, "[" + st.Name.Substring(3) + "]");
            }
            if (targetdt != null)
            {
                DataRowView[] ddrv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { targetdt.TableName });
                foreach (DataRowView dr in ddrv)
                {
                    if (!((bool)dr["Qualify"]))
                        continue;
                    if (q.IndexOf((string)dr["PhysicalName"]) >= 0)
                        q = q.Replace((string)dr["PhysicalName"], "[" + dr["ColumnName"] + "]");
                }
            }
            q = q.Replace("dbo.", "");
            return q;
        }

        private static string grainString(MainAdminForm maf, MeasureTable mt)
        {
            List<MeasureTableGrain> mtglist = maf.getGrainInfo(mt);
            StringBuilder sb = new StringBuilder();
            foreach (MeasureTableGrain mtg in mtglist)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(mtg.DimensionName + "." + mtg.DimensionLevel);
            }
            return sb.ToString();
        }

        
        static string[] markers = new string[] { "[INSERTDT:", "[UPDATEDT:", "[INSERTF:", "[UPDATEF:", "[DELETEF:" };

        private class Update
        {
            public long ID;
            public int Type;
            public string TableName;
            public string StagingTableName;
            public string Columns;
            public long Rows;
            public long Duration;
            public string ProcessingGroup;
        }

        private static void loadSummaryGrid(HttpSessionState session, PublishLogResult response, Space sp, string loadDate, int loadNum, string loadGroup, string processingGroup, bool ignoreProcessingGroupCheck)
        {
            DateTime date = DateTime.Parse(loadDate);
            string logfile = String.Format("smiengine.*.{0:0000000000}.log", loadNum);
            string[] fnames = null;
            if (loadGroup != null && loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
            {
                if (!Directory.Exists(sp.Directory + "\\logs"))
                    Directory.CreateDirectory(sp.Directory + "\\logs");
                fnames = LiveAccessUtil.getLogFileNameForLiveAccessConnection(sp, loadNum, loadGroup);
            }
            else
            {
                if (!Directory.Exists(sp.Directory + "\\logs"))
                    return;
                fnames = Util.getMatchedLogFileNames(sp, loadNum, loadDate);
            }
            if (fnames == null || fnames.Length == 0)
                return;
            List<Update> ulist = new List<Update>();
            Dictionary<long, string> querymap = new Dictionary<long, string>();
            
            DateTime currentLogLineTimeStamp = DateTime.MinValue;
            bool containsProcessingGroup = false;
            foreach (string fname in fnames)
            {
                bool foundStartMarker = false;
                StreamReader reader = new StreamReader(File.Open(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                string line = null;
               // string startMarker = "Starting: ResetETLRun " + loadNum;
                try
                {
                    while ((line = reader.ReadLine()) != null)
                    {

                        // Have we ever found startMarker in the current file
                        bool markerMatchedForDifferentLoad = false;
                        int startMarkerIndexForCurrentLoad = getStartMarkerIndex(line, loadNum, out markerMatchedForDifferentLoad);
                        foundStartMarker = startMarkerIndexForCurrentLoad >= 0 ? true : foundStartMarker;
                        // if there are multiple load numbers in the same file, make sure that we dont parse the lines from other load numbers
                        if (markerMatchedForDifferentLoad)
                        {
                            foundStartMarker = false;
                        }

                        if (startMarkerIndexForCurrentLoad > 0)
                        {
                            // if there is another start marker for this load number, make sure this is the most recent one. It might happen
                            // that even after iteration through sorted files by date, most recent load number (for previous iterations) 
                            // might appear earlier than later since sorted files are based on last modified time 
                            bool isUpdated;
                            DateTime dt = getUpdatedLoadLineTimeStamp(line, loadNum, currentLogLineTimeStamp, out isUpdated);
                            if (isUpdated)
                            {
                                currentLogLineTimeStamp = dt;
                                ulist.Clear();
                            }
                        }

                        /*
                        if (line.IndexOf(startMarker) >= 0)
                            ulist.Clear();
                        */

                        if (foundStartMarker)
                        {
                            int qindex = line.IndexOf("[QUERY:");
                            if (qindex > 0)
                            {
                                int index1 = line.IndexOf(":", qindex + 7) + 1;
                                int index2 = line.IndexOf("]", index1) + 1;
                                int count = 0;
                                while (index2 == 0)
                                {
                                    line += "<br/>" + reader.ReadLine();
                                    index2 = line.IndexOf("]", index1) + 1;
                                    if (count++ > 15)
                                        break;
                                }
                                long id = 0;
                                if (index2 > 0)
                                {
                                    try
                                    {
                                        id = long.Parse(line.Substring(qindex + 7, index1 - qindex - 8));
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    string q = line.Substring(index1, index2 - index1 - 1);
                                    if (!querymap.ContainsKey(id))
                                        querymap.Add(id, q);
                                    else
                                        querymap[id] = querymap[id] + "\r\n" + q;
                                }
                            }
                            else
                                for (int i = 0; i < markers.Length; i++)
                                {
                                    string m = markers[i];
                                    int index = line.IndexOf(m);
                                    if (index > 0)
                                    {
                                        int index2 = line.LastIndexOf("]");
                                        if (index2 > 0)
                                        {
                                            Update u = new Update();
                                            u.Type = i;
                                            string[] parts = line.Substring(index + 1, index2 - index - 1).Split(new char[] { ':' });
                                            if (parts.Length < 6)
                                                continue;
                                            int partloadnum = -1;
                                            if (parts.Length == 6)
                                            {
                                                u.TableName = parts[2];
                                                u.StagingTableName = parts[3];
                                                long.TryParse(parts[4], out u.Rows);
                                                u.Columns = parts[5];
                                            }
                                            else
                                            {
                                                u.ID = long.Parse(parts[1]);
                                                if (parts[2] != null)
                                                    int.TryParse(parts[2], out partloadnum);
                                                u.TableName = parts[3];
                                                u.StagingTableName = parts[4];
                                                long.TryParse(parts[5], out u.Rows);
                                                u.Columns = parts[6];
                                                if (parts.Length >= 8)
                                                {
                                                    long.TryParse(parts[7], out u.Duration);
                                                }
                                                if (parts.Length >= 9)
                                                {
                                                    containsProcessingGroup = true;
                                                    u.ProcessingGroup = (parts[8] == "null" ? Status.EMPTY_PROCESSING_GROUP : parts[8]);
                                                }
                                            }
                                            u.Columns = u.Columns.Trim(new char[] { '[', ']' });
                                            if ((!ignoreProcessingGroupCheck) && containsProcessingGroup && (u.ProcessingGroup != processingGroup))
                                                continue;
                                            if (partloadnum < 0 || partloadnum == loadNum)
                                                ulist.Add(u);
                                        }
                                        break;
                                    }
                                }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            session["querymap"] = querymap;
            session["ulist"] = ulist;
            List<DimensionTableResult> dimTableList = new List<DimensionTableResult>();
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            foreach (Update u in ulist)
            {
                if (u.Rows == 0)
                    continue;
                if (u.Type == 0 || u.Type == 1)
                {
                    int index = maf.findDimensionTableIndex(u.TableName);
                    if (index < 0)
                        continue;
                    DimensionTable dt = maf.dimensionTablesList[index];
                    DimensionTableResult dr = new DimensionTableResult();
                    dr.Dimension = maf.hmodule.getDimensionHierarchy(dt.DimensionName).Name;
                    dr.Level = dt.Level;
                    dr.Type = u.Type == 0 ? "Add Records" : "Update Records";
                    String tname = u.StagingTableName.Substring(3);
                    StagingTable st = ViewProcessed.getStagingTableFromName(maf, tname);
                    string staging = (st == null || st.LogicalName == null) ? u.StagingTableName.Substring(3) : st.LogicalName;
                    dr.DataSource = staging;              
                    dr.Rows = u.Rows.ToString();
                    dr.Columns = u.Columns;
                    dr.ID = u.ID.ToString();
                    dr.Load = loadNum;
                    dr.Group = loadGroup;
                    dr.Date = loadDate;
                    dr.Duration = u.Duration;
                    dimTableList.Add(dr);
                }
            }
            
            response.DimensionResult = dimTableList.ToArray();
            List<MeasureTableResult> measureTableList = new List<MeasureTableResult>();
            foreach (Update u in ulist)
            {
                if (u.Rows == 0)
                    continue;
                MeasureTable mt = null;
                foreach (MeasureTable smt in maf.measureTablesList)
                {
                    if (smt.TableName == u.TableName)
                    {
                        mt = smt;
                        break;
                    }
                }
                if (mt == null)
                    continue;
                if (u.Type == 2 || u.Type == 3 || u.Type == 4)
                {
                    MeasureTableResult dr = new MeasureTableResult();
                    dr.Grain = grainString(maf, mt);
                    dr.Type = u.Type == 2 ? "Add Records" : (u.Type == 4 ? "Delete Records" : "Update Records");               
                    String tname = u.StagingTableName.Substring(3);
                    StagingTable st = ViewProcessed.getStagingTableFromName(maf, tname);
                    string staging = (st == null || st.LogicalName == null) ? u.StagingTableName.Substring(3) : st.LogicalName;
                    dr.DataSource = staging;               
                    dr.Rows = u.Rows.ToString();
                    dr.Columns = u.Columns;
                    dr.ID = u.ID.ToString();
                    dr.Load = loadNum;
                    dr.Group = loadGroup;
                    dr.Date = loadDate;
                    dr.Duration = u.Duration;
                    measureTableList.Add(dr);
                }
            }

            response.MeasureResult = measureTableList.ToArray();
        }

        /// <summary>
        /// This method is used to determine if  load log line is the most recent one or not. If it is, 
        /// return the most recent time stamp. This can happen if the same load is processed, deleted and re-processed 
        /// which will result in the same start marker. Everything else will be same except time stamp. This is to make sure
        /// that we get the most recent publish details.        
        /// </summary>
        /// <param name="line">log line</param>
        /// <param name="loadNum">load number</param>
        /// <param name="currentLogLineTimeStamp">most recent time stamp for the log line</param>
        /// <param name="isUpdated">out parameter specifying whether log line is recent</param>
        /// <returns>updated time stamp and updates the isUpdated out parameter</returns>
         
        public static DateTime getUpdatedLoadLineTimeStamp(string line, int loadNum, DateTime currentLogLineTimeStamp, out bool isUpdated)
        {
            isUpdated = false;
            DateTime updatedLoadLineTimeStamp = currentLogLineTimeStamp;
            if (loadNum >= 0)
            {                
                bool matchDifferentLoad = false;
                int indexStartMarker = getStartMarkerIndex(line, loadNum, out matchDifferentLoad);
                if (indexStartMarker >= 0)
                {
                    Match match = Util.getMatchForTimeInSmiEngineLog(line, indexStartMarker);
                    if (match != null && match.Value != null)
                    {
                        try
                        {
                            DateTime logLineTimeStamp;
                            DateTime.TryParse((string)match.Value, out logLineTimeStamp);
                            if (logLineTimeStamp.CompareTo(currentLogLineTimeStamp) == 1)
                            {
                                updatedLoadLineTimeStamp = logLineTimeStamp;
                                isUpdated = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("Error while parsing smi engine log line for time stamp.", ex);
                        }
                    }
                }
            }
            return updatedLoadLineTimeStamp;
        }

        public static int getStartMarkerIndex(string line, int loadNumber, out bool matchDifferentLoadNumber)
        {
            matchDifferentLoadNumber = false;
            string startMarker = "Starting: ResetETLRun ";            
            string startMarkerCurrentLoad = startMarker + loadNumber;            

            int indexStartMarkerCurrentLoad = line.IndexOf(startMarkerCurrentLoad);
            int indexStartMarker = line.IndexOf(startMarker);

            matchDifferentLoadNumber = (indexStartMarkerCurrentLoad < 0 && indexStartMarker >= 0) ? true : false;

            return indexStartMarkerCurrentLoad;
        }    
    }

    
}
