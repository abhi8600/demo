﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Security.Principal;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.SessionState;
using Acorn.Utils;
using Acorn.DBConnection;
using Acorn.SAMLSSO;

namespace Acorn
{
    public partial class SSO : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private string getBirstParam(string newName, string oldName)
        {
            string val = Request.Params[newName];
            if (val == null || val.Length == 0)
                val = Request.Params[oldName];
            return val;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response);

            // clear out the session if this is a logout request
            if (Request["logout"] != null)
            {
                logout(Session, Request, Response);
                Response.Status = "200 Okay";
                return;
            }

            string token = getBirstParam("birst.ssoToken", "BirstSSOToken");
            if (token == null)
            {
                Global.systemLog.Warn("SSO request with no BirstSSOToken");
                Response.Status = "401 Unauthorized";
                return;
            }

            // see if there is an existing BirstSSOToken and if this one is different, if exists and the new one is different, invalidate the current session
            if (Session["BirstSSOToken"] != null && !Session["BirstSSOToken"].Equals(token))
            {
                User u1 = (User)Session["user"];
                if (u1 != null)
                {
                    // Remove the user from current_logins table
                    QueryConnection conn1 = null;
                    try
                    {
                        conn1 = ConnectionPool.getConnection();
                        Database.removeCurrentLogin(conn1, mainSchema, u1.ID, Util.getASPNETSessionID(Request));
                        Global.systemLog.Info("Successful logout for user " + u1.Username + " (changed BirstSSOToken)");
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn1);
                    }
                }
                Util.endSession(Session);
                Util.endSMIWebSession(Session, Request);
                SchedulerUtils.endSchedulerSession(Session, Request);
                Util.setLogging(null, null);
                Session.Clear();
            }

            User u = null;
            Space sp = null;
            bool setSingleSpace = true;
            SSOToken ssoToken = null;

            if (Session["BirstSSOToken"] == null || !Session["BirstSSOToken"].Equals(token))
            {
                // get rid of all authentication information
                Session.Remove("user");
                Session.Remove("auth");
                Session.Remove("ssoAuthentication");
                Session.Remove("birst.module");
                Session.Remove("products");
                Util.setLogging(null, null);

                Guid tokenID = Util.decryptToken(token);
                if (tokenID == Guid.Empty)
                {
                    Global.systemLog.Warn("Bad Request - invalid BirstSSOToken format: " + token);
                    Response.AddHeader("X-Birst-SSO-Fail", "invalid BirstSSOToken format");
                    Response.StatusCode = 400;
                    return;
                }
                QueryConnection conn = null;
                DateTime lastLoginDate = DateTime.MinValue;
                try
                {
                    conn = ConnectionPool.getConnection();
                    ssoToken = Database.getSSOToken(conn, mainSchema, tokenID); // also removes it from the database (one time use)
                    if (ssoToken == null)
                    {
                        Global.systemLog.Warn("SSO request with invalid BirstSSOToken: " + tokenID);
                        Response.AddHeader("X-Birst-SSO-Fail", "invalid BirstSSOToken");
                        Response.StatusCode = 400;
                        return;
                    }
                    if (DateTime.Now > ssoToken.endOfLife)
                    {
                        Global.systemLog.Warn("SSO request with invalid BirstSSOToken: " + ssoToken.endOfLife + " (timestamp too old)");
                        Response.AddHeader("X-Birst-SSO-Fail", "BirstSSOToken expired");
                        Response.StatusCode = 400;
                        return;
                    }

                    u = Database.getUserById(conn, mainSchema, ssoToken.userID);
                    if (u == null)
                    {
                        Global.systemLog.Warn("SSO request failed with invalid user: " + ssoToken.userID + " (not in the database or expired products)");
                        Response.AddHeader("X-Birst-SSO-Fail", "User does not exist or has expired products");
                        Response.StatusCode = 400;
                        return;
                    }

                    lastLoginDate = Database.getLastLogin(u.Username);
                    Util.checkIfUserInactiveAndDisable(conn, u, lastLoginDate);

                    if (u.Disabled)
                    {
                        Global.systemLog.Warn("SSO request failed with invalid user: " + ssoToken.userID + " (disabled)");
                        Response.AddHeader("X-Birst-SSO-Fail", "User has been disabled");
                        Response.StatusCode = 400;
                        return;
                    }
                    Util.setLogging(u, null);

                    if (u.ManagedAccountId == null || (u.ManagedAccountId != null && u.ManagedAccountId != Guid.Empty))
                    {
                        // Logic to check for maxconcurrent limits. Show warning or error
                        int numConcurrentLogins = Database.getCurrentLoginsForAccount(conn, mainSchema, u);
                        // Get account details 
                        AccountDetails actDetails = Database.getAccountDetails(conn, mainSchema, u.ManagedAccountId, u);

                        // Check for warning and then error
                        bool showWarningOrError = actDetails != null ? numConcurrentLogins > actDetails.MaxConcurrentLogins : false;
                        if (showWarningOrError)
                        {
                            string userDetail = "User Details id " + u.ID + " : Username " + u.Username + " : AccountId  " + u.ManagedAccountId + " : ConcurrentLogins " + numConcurrentLogins;
                            bool warn = numConcurrentLogins + 1 <= actDetails.MaxConcurrentLogins + Util.THRESHOLD_WARN_CONCURRENT_LOGINS;
                            if (warn)
                            {
                                // show warning
                                Global.systemLog.Warn("Warning Mode: User account is already above MaxConcurrentLogins. " + userDetail);
                            }
                            else
                            {
                                // show error and prevent user from login
                                Global.systemLog.Warn("Error Mode: User account is above MaxConcurrentLogins. " + userDetail);
                                Util.sendErrorDetailEmail("User Login ERROR. MaxConcurrentLogins Limit Reached ",
                                    ssoToken.type + " Process",
                                    userDetail,
                                    null, null, null);
                                Response.AddHeader("X-Birst-SSO-Fail", "Account concurrent login limit reached");
                                Response.StatusCode = 400;
                                return;
                            }
                        }
                    }
                    else
                    {
                        Global.systemLog.Info("User " + u.Username + " : " + u.ID + " does not have an account id (not a problem, but they probably should be assigned to an account)");
                    }

                    if (ssoToken.spaceID != Guid.Empty)
                    {
                        // true SSO request, not a login redirect
                        sp = (Space)Session["space"]; // XXX what is this?
                        if (sp == null || sp.ID != ssoToken.spaceID)
                        {
                            sp = Util.getSpaceById(conn, mainSchema, u, ssoToken.spaceID);
                        }
                        if (sp == null)
                        {
                            Global.systemLog.Warn("SSO request with invalid space ID - user not allowed to access the space or space does not exist: " + ssoToken.spaceID);
                            Response.AddHeader("X-Birst-SSO-Fail", "User not allowed to access the space or the spaces does not exist");
                            Response.StatusCode = 400;
                            return;
                        }
                    }
                    else
                    {
                        // login redirect request
                        setSingleSpace = false;
                    }
                    Database.addCurrentLogin(conn, mainSchema, u.ID, Request.UserHostAddress, DateTime.Now, Session.SessionID, Util.getHostName());
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }

                FormsAuthentication.SetAuthCookie(u.Username, false);

                // used by HTML5 applications
                // this should NOT be HttpOnly
                HttpCookie csrfCookie = new HttpCookie("XSRF-TOKEN", Util.encryptXSRFToken(Session.SessionID));
                Response.SetCookie(csrfCookie);

                Session["user"] = u;
                Session["auth"] = true;
                Session["LoggedInDate"] = lastLoginDate;
                Session["products"] = Database.getProducts(conn, mainSchema, u.ID);
                Session["BirstSSOToken"] = token;
                Session["SFDC_sessionId"] = Request["sessionid"];
                Session["SFDC_serverURL"] = Request["serverurl"];
                if (ssoToken.sessionVariables != null)
                {
                    Session["sessionVars"] = ssoToken.sessionVariables;
                }
                string temp = Request.Params["birst.masterURL"]; // where we originally came from, before potential redirect
                if (temp != null && temp.Length > 0)
                {
                    Session["birst.masterURL"] = temp;
                }

                Util.setLogging(u, sp);
                Global.systemLog.Info("Login for user - " + ssoToken.type);

                log4net.ThreadContext.Properties["user"] = u.Username;
                using (log4net.ThreadContext.Stacks["itemid"].Push(ssoToken.type))
                {
                    Global.userEventLog.Info("LOGIN");
                }

                if (setSingleSpace) // SSO request
                {
                    Util.setSpace(Session, sp, u, false);
                    Session["ssoAuthentication"] = true; // only allow a single space to be accessed in this session
                }

                Database.updateLastLogin(u.Username);
            }
            else
            {
                // valid session already exists and is set up, just use it
                u = (User)Session["user"];
                sp = (Space)Session["space"];
            }

            configureStandardLogout();
            configureSAMLSpecificSessionParams(ssoToken);
            if (setSingleSpace) // SSO request
            {
                // support the arguments coming via GET and POST
                NameValueCollection arguments = new NameValueCollection();
                arguments.Add(Request.Form);
                arguments.Add(Request.QueryString);
                Util.fixupArguments(arguments);

                string module = arguments["birst.module"];

                // check if swap space is going on. If it is, prevent login
                int spaceOpId = SpaceOpsUtils.getSpaceAvailability(sp);
                if (SpaceOpsUtils.isSwapSpaceInProgress(spaceOpId))
                {
                    Global.systemLog.Warn("SSO request with valid space ID - user not allowed to access the module " + module + " - swap space in progress: " + sp.ID);
                }
                else
                {
                    // if embedded and no nav bar, don't allow anybody to go outside of it
                    string embedded = arguments["birst.embedded"];
                    if (embedded != null && embedded.ToLower().Equals("true"))
                    {
                        Session["birst.module"] = module;
                    }
                    if (module == "export")
                        Acorn.DefaultUserPage.redirectToExport(Session, Response, Server, Request, arguments, sp, u);
                    else if (module == "adhoc" || module == "designer")
                        Acorn.DefaultUserPage.redirectToAdhoc(Session, Response, Server, arguments);
                    else if (module == "dashboard")
                        Acorn.DefaultUserPage.redirectToDashboard(Session, Response, Server, arguments, Request, sp);
                    else if (module == "customupload")
                        Response.Redirect("~/CustomUpload.aspx");
                    else if (module == "visualizer")
                        Response.Redirect("~/Visualizer/visualizer.aspx");
                }
            }
            else
            {
                Session.Remove("MAF");
                Session.Remove("space");

                // check password expiration when logging in via username/password
                if ("BROWSER".Equals(ssoToken.type))
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        PasswordPolicy policy = Database.getPasswordPolicy(conn, mainSchema, u.ManagedAccountId);
                        if (policy != null && policy.expirationDays > 0)
                        {
                            MembershipUser mu = Membership.GetUser(u.Username, false);
                            DateTime lastPasswordChanged = mu.LastPasswordChangedDate;
                            TimeSpan delta = DateTime.Now - lastPasswordChanged;
                            if (delta.TotalDays >= policy.expirationDays)
                            {
                                Session["PasswordChangeRequired"] = true;
                            }
                        }
                        if (policy != null && policy.changeOnFirstUse)
                        {
                            List<string> history = Database.getPasswordHistory(conn, mainSchema, u.ID);
                            if (history == null || history.Count < 2)
                            {
                                Session["PasswordChangeRequired"] = true;
                            }
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }

                bool repAdmin = ssoToken.repAdmin;

                if (repAdmin)
                    u.RepositoryAdmin = true;   // proxy table says that this user can have repository admin - should only ever be set for birst internal                    

                // If default space id is configured, set the space properly
                List<SpaceMembership> mlist = Util.getSpaceMembershipListFromDB(u);
                if (mlist != null && mlist.Count > 0)
                {
                    foreach (SpaceMembership sm in mlist)
                    {
                        if (sm.Space.ID == u.DefaultSpaceId || (u.DefaultSpaceId == Guid.Empty && sm.Space.Name == u.DefaultSpace))
                        {
                            // check if swap space is going on. If it is, prevent login
                            int spaceOpId = SpaceOpsUtils.getSpaceAvailability(sm.Space);
                            if (SpaceOpsUtils.isSwapSpaceInProgress(spaceOpId) || SpaceOpsUtils.isNewSpaceBeingCopied(spaceOpId))
                            {
                                // if its available, then we cannot go to this space. Move to the next one
                                Global.systemLog.Warn("Default space not available. Swapping space in progress : space id :" + sm.Space.ID + " : name : " + sm.Space.Name);
                                Util.resetSessionObjectWithSpace(Session, sm.Space, u);
                            }
                            else
                            {
                                Util.setSpace(Session, sm.Space, u, false);
                                if (redirectForSAMLSpInitiatedRequest(ssoToken, sm.Space))
                                {
                                    return;
                                }
                                if (u.DefaultDashboards)
                                    DefaultUserPage.redirectToDashboard(Session, Response, Server, null, Request, sm.Space);
                                else
                                    break;
                            }
                        }
                    }
                }
            }
            Response.Redirect(Util.getHomePageURL(Session, Request));
        }

        private void configureStandardLogout()
        {
            Session["LOGOUT_PAGE"] = Util.getLogoutRedirectURL(Session);
        }

        private void configureSAMLSpecificSessionParams(SSOToken ssoToken)
        {
            if (isTokenTypeSAML(ssoToken))
            {
                int timeout = ssoToken.timeout;
                string logoutPage = ssoToken.logoutPage;
                // configure params from SSOToken
                
                Acorn.SAMLSSO.SubDomainInfo subDomainInfo = SamlUtils.getSubDomainInfo(Request);
                if (subDomainInfo != null && subDomainInfo.Active)
                {
                    Global.systemLog.Info("Registered subDomain IDP name : " + subDomainInfo.SamlIdpName);
                    IdpMetaData idpInfo = SamlUtils.getIdpMetaDataByName(subDomainInfo.SamlIdpName);
                    if (idpInfo != null)
                    {
                        logoutPage = idpInfo.logoutPage;
                        timeout = idpInfo.timeout;                                                
                        if (string.IsNullOrWhiteSpace(logoutPage))
                        {
                            string standardLogoutPage = Util.getLogoutRedirectURL(Session);
                            logoutPage = standardLogoutPage.Trim(new char['/']) + "?birst.logout=true";
                        }                        
                    }
                }
                
                Session["SAMLTIMEOUT"] = timeout;
                Session["LOGOUT_PAGE"] = logoutPage;
                if (timeout > 0)
                {
                    Global.systemLog.Info("timeout configured to " + timeout);
                }
                if (logoutPage != null)
                {
                    Global.systemLog.Info("logoutpage configured to " + logoutPage);
                }
            }
        }

        private bool isTokenTypeSAML(SSOToken ssoToken)
        {
            return ssoToken != null && ssoToken.type == "SAML SSO";
        }
        private bool redirectForSAMLSpInitiatedRequest(SSOToken ssoToken, Space space)
        {
            if (isTokenTypeSAML(ssoToken))
            {
                NameValueCollection arguments = new NameValueCollection();
                arguments.Add(Request.Form);
                arguments.Add(Request.QueryString);
                Util.fixupArguments(arguments);
                string module = arguments["birst.module"];
                if (module == "dashboard")
                {
                    DefaultUserPage.redirectToDashboard(Session, Response, Server, null, Request, space);
                }
                else if (module == "designer")
                {
                    DefaultUserPage.redirectToAdhoc(Session, Response, Server, null);
                }
                else if (module == "home")
                {
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                else if (module == "admin")
                {
                    Response.Redirect("~/FlexModule.aspx?birst.module=admin");
                }
                else
                {
                    return false;
                }
                return true;
            }
            return false;
        }


        private string toString(NameValueCollection col)
        {
            StringBuilder sb = new StringBuilder();
            foreach (String key in col.Keys)
            {
                if (sb.Length > 0)
                    sb.Append('&');
                sb.Append(key);
                sb.Append('=');
                sb.Append(col[key]);
            }
            return sb.ToString();
        }

        public static void logout(HttpSessionState session, HttpRequest request, HttpResponse response)
        {
            User u = (User)session["user"];
            if (u != null)
            {
                // Remove the user from current_logins table
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.removeCurrentLogin(conn, mainSchema, u.ID, Util.getASPNETSessionID(request));
                    Global.systemLog.Info("Successful logout for SSO user " + u.Username);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex, ex);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            Util.endSession(session);
            Util.endSMIWebSession(session, request);
            SchedulerUtils.endSchedulerSession(session, request);
            FormsAuthentication.SignOut();
            DotNetOpenAuth.OpenId.RelyingParty.OpenIdRelyingPartyAjaxControlBase.LogOff();
            session.Clear();
            session.Abandon();
            session.RemoveAll();
            // To prevent the same session id being sent from browser on a new login request
            // Ref: http://technicalsol.blogspot.com/2008/07/understanding-sessionabandon.html

            clearCookie(request, response, "ASP.NET_SessionId");
            clearCookie(request, response, "JSESSIONID");
            clearCookie(request, response, "BSJSESSIONID");

            // Session.Abandon doesn't destroy all the objects stored in  a session object immediately.
            // When the Abandon method is called, the current Session object is queued for deletion but is not actually deleted until all of the script commands on the current page have been processed.                
            // If after clicking logout, user again logins, it is treated as postback because of that 
            // newly created session will be abandoned. Hence again relogin page will be given to the user.
            // To account for the that, if we explictly redirect it to Login (SSO) page, it will not be treated as PostBack
            // on the new request.
        }

        public static void clearCookie(HttpRequest request, HttpResponse response, string cookieName)
        {
            if (request.Cookies[cookieName] != null)
            {
                response.Cookies[cookieName].Value = string.Empty;
                response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-20);
                response.Cookies[cookieName].HttpOnly = true;
                if (Global.connectionIsViaHTTPS)
                    response.Cookies[cookieName].Secure = true;
            }
        }
    }
}
