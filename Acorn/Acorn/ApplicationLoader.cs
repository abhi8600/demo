﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.SessionState;
using System.Threading;
using System.Text;
using Acorn.Background;
using Acorn.Utils;
using System.Xml;
using System.Xml.Serialization;
using Acorn.DBConnection;
using Acorn.TrustedService;
using Acorn.Exceptions;

namespace Acorn
{
    public class ApplicationLoader
    {
        // For automatic compatibility with Advanced Mode the following both should be true
        public static bool GENERATE_ONLY_BOTTOM_LEVEL_TABLES =
            bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GenerateOnlyBottomLevelTables"]);
        // No shared children
        private static bool GENERATE_SIMPLE_HIERARCHIES =
            bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GenerateSimpleHierarchies"]);
        private static string awsAccessKeyId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsAccessKeyId"];
        private static string awsSecretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsSecretKey"];

        public static int MAX_TIMEOUT = 7 * 24 * 60; // 7 days in minutes

        private static Dictionary<string, MonitorLoad> backgroundProcess = new Dictionary<string, MonitorLoad>();

        //No longer performing vacuum and analyze operation on schema after load is processed
        //Because PADB does not allow these operations to be run by 2 different processes and in multitenant mode we cannot avoid this
        //It is therefore decided that these are admin tasks that will be explicitly performed by Ops whenever required.
        public static bool RunAnalyzeAfterLoadingDataIntoPADB = false;//bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["RunAnalyseAfterLoadingDataIntoPADB"]);
        public static bool RunVacuumAfterLoadingDataIntoPADB = false;//bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["RunVacuumAfterLoadingDataIntoPADB"]);

        MainAdminForm maf;
        Space sp;
        DateTime loadDate;
        string mainSchema;
        string enginecmd;
        string createtimecmd;
        string contentDir;
        LoadControl loadpage;
        User u;
        private BuildDashboards bd;
        private ScanBuffer scanBuff;
        private string loadGroup;
        private string[] subGroups;
        private int loadNumber;
        public Dictionary<string, bool> liveAccessLoadStatusMap = new Dictionary<string,bool>();

        public ApplicationLoader(MainAdminForm maf, Space sp, DateTime loadDate,
            string mainSchema, string enginecmd, string createtimecmd, string contentDir,
            LoadControl loadpage, User u, string loadGroup, int loadNumber)
        {
            this.maf = maf;
            this.sp = sp;
            this.loadDate = loadDate;
            this.mainSchema = mainSchema;
            this.enginecmd = enginecmd;
            this.createtimecmd = createtimecmd;
            this.contentDir = contentDir;
            this.loadpage = loadpage;
            this.u = u;
            this.loadGroup = loadGroup;
            this.loadNumber = loadNumber;
        }

        public int getLoadNumber()
        {
            return loadNumber;
        }

        public string getLoadGroup()
        {
            return loadGroup;
        }

        public DateTime getLoadDate()
        {
            return loadDate;
        }

        public BuildDashboards BuildDashboards
        {
            get
            {
                return bd;
            }
            set
            {
                bd = value;
            }
        }

        public ScanBuffer ScanBuff
        {
            get
            {
                return scanBuff;
            }
            set
            {
                scanBuff = value;
            }
        }

        public string[] SubGroups
        {
            get
            {
                return subGroups;
            }
            set
            {
                subGroups = value;
            }
        }

        public bool retryFailedLoad { get; set; }

        public enum LoadAuthorization { OK, RowLimitExceeded, RowPublishLimitExceeded, DataLimitExceeded, DataPublishLimitExceeded };

        public LoadAuthorization getLoadAuthorization(string loadGroup)
        {
            int newRows = 0;
            long newData = 0;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                {
                    if (st.SourceFile.ToLower() == sf.FileName.ToLower() && st.LoadGroups != null && Array.IndexOf<string>(st.LoadGroups, loadGroup) >= 0)
                    {
                        newRows += sf.NumRows;
                        newData += sf.FileLength;
                        break;
                    }
                }
            }

            return LoadAuthorization.OK; // no longer checking for limits being exceeded
        }

        internal static string setupRedshiftTimeRepository(SpaceConfig sc, string contentDir)
        {
            string mainSchema = Util.getMainSchema();
            DateTime loadDate = DateTime.Now;

            MainAdminForm tmaf = createTimeMAF();
            // dummy space
            Space tsp = new Space();
            tsp.Directory = contentDir;
            tsp.Schema = mainSchema;
            tsp.ConnectString = sc.DatabaseConnectString;
            tsp.AdminUser = sc.AdminUser;
            tsp.AdminPwd = sc.AdminPwd;
            tsp.DatabaseType = sc.DatabaseType;
            tsp.DatabaseDriver = sc.DatabaseDriver;
            tsp.Type = sc.Type;
            Util.buildApplication(tmaf, mainSchema, loadDate, -1, null, tsp, null);
            string timeRepositoriesRootFolder = ApplicationLoader.getTimeRepositoriesRootFolderPath(contentDir);
            if (!Directory.Exists(timeRepositoriesRootFolder))
            {
                Directory.CreateDirectory(timeRepositoriesRootFolder);
            }
            string timeRepositoryPath = getTimeRepositoryFilePath(timeRepositoriesRootFolder, tsp); //contentDir + "\\time_repository.xml";            
            Util.saveApplicationWithoutOwner(tmaf, tsp, null, timeRepositoryPath);
            return timeRepositoryPath;
        }

        private static string getTimeRepositoriesRootFolderPath(string baseDir)
        {
            return Path.Combine(baseDir, "timeRepositories");
        }

        private static string getTimeRepositoryFilePath(string timeReposRootFolder, Space sp)
        {
            string timeRepoFileName = "time_repository";
            if (Util.isSpaceRedshift(sp))
            {
                // get the space type id and suffix it
                timeRepoFileName = timeRepoFileName + "_redshift_" + sp.Type;
            }

            return Path.Combine(timeReposRootFolder, timeRepoFileName + ".xml");
            
        }

        private string getTimeRepositoryCmdArgs(string contentDir, string timeRepositoryFilePath, Space sp)
        {
            StringBuilder args = new StringBuilder();
            args.Append(timeRepositoryFilePath);
            args.Append(" ");
            args.Append(contentDir);
            if (Util.isSpaceRedshift(sp))
            {                
                // get bucket name where time files are available
                args.Append(" ");                
                args.Append(Util.getS3BucketName(Util.getSpaceConfigurationNoException(sp.Type)));

                // get aws credentials
                args.Append(" ");                
                args.Append(Util.getAWSAccessKey());
                args.Append(" ");                
                args.Append(Util.getAWSSecretKey());
                args.Append(" ");                
                args.Append(sp.Type);  // used in place of  -Dsmi.loadid in the log appender to isolate logs for different redshift instances
            }
            if (Util.isSpaceHANA(sp))
            {
                // get bucket name where time files are available
                args.Append(" ");
                args.Append(Util.getTimeDimensionFolder(Util.getSpaceConfigurationNoException(sp.Type)));                
                args.Append(" ");
                args.Append(sp.Type);  // used in place of  -Dsmi.loadid in the log appender to isolate logs for different redshift instances
            }
            return args.ToString();
        }

        private void setupTime(MainAdminForm maf, string connectstring, Dictionary<string, int> curLoadNumbers)
        {
            DimensionTable dt = null;
            foreach (DimensionTable sdt in maf.dimensionTablesList)
            {
                if (sdt.DimensionName == maf.timeDefinition.Name && sdt.Level == "Day")
                {
                    dt = sdt;
                    break;
                }
            }

            if (Util.isSpaceRedshift(sp))
            {
                Status.StatusStep timeCreationStatus = Status.checkTimeDimensionStatus(Util.getSpaceConfigurationNoException(sp.Type));
                if (timeCreationStatus != null)
                {
                    if (timeCreationStatus.result == Status.RUNNING)
                    {
                        throw new Exceptions.BirstException("Time Dimension tables are being currently created. Please try again later.");
                    }
                    else if (timeCreationStatus.result == Status.FAILED)
                    {
                        throw new Exceptions.BirstException("Time Dimension tables were not successfully created. Please recreate the time dimension tables.");
                    }
                    else if (timeCreationStatus.result == Status.NONE)
                    {
                        throw new Exceptions.BirstException("No Time Dimension tables exist. Please create the time dimension tables.");
                    }
                    return;
                }
                else
                {
                    throw new Exceptions.BirstException("No Time Dimension tables exist. Please create the time dimension tables.");
                }
            }
            
            bool timeExists = false;
            QueryConnection timeConn = null;
            try
            {
                timeConn = ConnectionPool.getConnection(connectstring);
                timeExists = dt != null && Database.tableExists(timeConn, mainSchema, dt.TableSource.Tables[0].PhysicalName);
            }
            finally
            {
                ConnectionPool.releaseConnection(timeConn);
            }
            if (!timeExists && maf.stagingTableMod.getAllStagingTables().Count > 0)
            {
                MainAdminForm tmaf = createTimeMAF();
                Util.buildApplication(tmaf, mainSchema, loadDate, sp.LoadNumber + 1, curLoadNumbers, sp, null);                
                string timeRepositoriesRootFolder = getTimeRepositoriesRootFolderPath(contentDir);
                if (!Directory.Exists(timeRepositoriesRootFolder))
                {
                    Directory.CreateDirectory(timeRepositoriesRootFolder);
                }
                string timeRepository = getTimeRepositoryFilePath(timeRepositoriesRootFolder, sp); //contentDir + "\\time_repository.xml";
                Space tsp = sp.clone();
                tsp.Schema = mainSchema;
                Util.saveApplicationWithoutOwner(maf, tsp, null, timeRepository);

                string command = Util.getCreateTimeCommand(sp);
                // Create the ProcessInfo object
                ProcessStartInfo tpsi = new ProcessStartInfo(command);
                tpsi.Arguments = getTimeRepositoryCmdArgs(contentDir, timeRepository, sp); 
                tpsi.WindowStyle = ProcessWindowStyle.Hidden;
                tpsi.RedirectStandardOutput = false;
                tpsi.CreateNoWindow = true;
                tpsi.UseShellExecute = false;
                tpsi.RedirectStandardInput = false;
                tpsi.RedirectStandardError = false;
                tpsi.WorkingDirectory = (new FileInfo(command)).Directory.FullName;

                Global.systemLog.Info("Creating TimeDimension tables");
                // Start the process
                Process tproc = Process.Start(tpsi);
                tproc.WaitForExit();
                if (tproc.HasExited)
                    Global.systemLog.Info("Creating time tables - Exited with code: " + tproc.ExitCode);
                else
                    Global.systemLog.Info("Creating time tables - Running");
                tproc.Close();
            }
        }

        public static bool isSpaceBeingPublished(Space sp)
        {
            bool isBeingPublished = false;
            isBeingPublished = File.Exists(sp.Directory + "\\publish.lock") || File.Exists(Util.getLoadLockFileName(sp));
            if (!isBeingPublished)
            {
                //check to see if space has local etl connections, if so, 
                //make sure none of the local application directories contain publish.lock file
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                foreach (string loadGroup in localETLConfigs.Keys)
                {
                    isBeingPublished = LiveAccessUtil.fileExistsOnLiveAccessConnection(sp, localETLConfigs[loadGroup].getLocalETLConnection(), localETLConfigs[loadGroup].getApplicationDirectory() + "\\" + sp.ID.ToString() + "\\publish.lock");
                    if (isBeingPublished)
                        break;
                }
            }
            return isBeingPublished;
        }

        public void load(bool createOverviewDashboard)
        {
            load(createOverviewDashboard, null);
        }

        public void load(bool createOverviewDashboard, HttpSessionState webServiceSession)
        {
            load(createOverviewDashboard, webServiceSession, false);
        }

        public void load(bool createOverviewDashboard, HttpSessionState webServiceSession, bool backgroundRequest)
        {
            load(createOverviewDashboard, webServiceSession, backgroundRequest, true, true, false);
        }

        public void load(bool createOverviewDashboard, HttpSessionState webServiceSession, bool backgroundRequest,
            bool continueToLoad, bool startLiveAccessLoad, bool reprocessmodified)
        {
            if (scanBuff == null)
                scanBuff = new ScanBuffer();
            try
            {
                if (!File.Exists(sp.Directory + "\\..\\publish.lock"))
                {
                    FileStream fs = File.Create(sp.Directory + "\\..\\publish.lock");
                    fs.Close();
                }
                if (!File.Exists(sp.Directory + "\\..\\" + Util.LOAD_LOCK_FILE))
                {
                    FileStream fs = File.Create(sp.Directory + "\\..\\" + Util.LOAD_LOCK_FILE);
                    fs.Close();
                }
                maf.setRepositoryDirectory(sp.Directory);

            }
            catch (Exception e)
            {
                throw e;
            }

            /* If necessary, retreive any required R server files */
            if (maf.RServer != null && maf.RServer.URL != null && maf.RServer.URL.Trim().Length > 0)
            {
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    if (st.RServerPath == null || st.RServerPath.Trim().Length == 0)
                        continue;
                    if (st.RServerSettings == null || st.RServerSettings.Trim().Length == 0)
                        continue;
                    if (subGroups != null)
                    {
                        bool containsgroup = false;
                        foreach (string s in st.SubGroups)
                        {
                            if (subGroups.Contains(s))
                            {
                                containsgroup = true;
                                break;
                            }
                        }
                        if (!containsgroup)
                            continue;
                    }
                    string[] rserversettings = st.RServerSettings.Split(new char[] { '|' });
                    bool refreshSource = false;
                    foreach (string s in rserversettings)
                    {
                        if (s == "reextract")
                        {
                            refreshSource = true;
                            break;
                        }
                    }
                    if (refreshSource)
                    {
                        RServerResult rsr = RServerUtil.UploadRServerFile(sp, u, maf, st.RServerPath, st.RExpression);
                        if (!rsr.success)
                        {
                            Status.setLoadFailed(sp, loadNumber, loadGroup);
                            if (webServiceSession != null)
                            {
                                Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Failed);
                                sr.message = rsr.errorMessage;
                                webServiceSession["status"] = sr;
                            }
                            Global.systemLog.Error("Problems encountered retrieving data from R server for source [" + st.getDisplayName() + "] in space " + sp.ID.ToString());
                            throw new BirstException("Problems encountered retrieving data from R server for source [" + st.getDisplayName() + "]");
                        }
                    }
                }
            }

            QueryConnection conn = null;
            bool previousUberTransaction = maf.isInUberTransaction();
            try
            {
                conn = ConnectionPool.getConnection();
                maf.setInUberTransaction(true);

                if (sp.Automatic)              
                    processAutomatic(conn);              
                else
                    updateDependencies(maf, sp);

                // Get the current publish loadnumbers
                Dictionary<string, int> curLoadNumbers = null;
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    // If there are loadgroups other than ACORN, get them
                    if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, "ACORN") >= 0 ? 1 : 0))
                    {
                        curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                        break;
                    }
                }

                // Build the application
                if (curLoadNumbers != null && curLoadNumbers.Count > 0 && !reprocessmodified)
                {
                    string[] loadGroupsArray = curLoadNumbers.Keys.ToArray();
                    foreach (string lg in loadGroupsArray)
                    {
                        if (lg == loadGroup || lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                            curLoadNumbers[lg] = curLoadNumbers[lg] + 1;
                    }
                }

                try
                {
                    // Set the StartPublishDate to Now 
                    Util.setProcessInitiatedTime(sp, maf);
                }
                catch (Exception ex)
                {
                    // log the exception
                    Global.systemLog.Error("Exception while setting start publish date in publish table ", ex);
                }

                Util.buildApplication(maf, mainSchema, loadDate, reprocessmodified ? sp.LoadNumber : sp.LoadNumber + 1, curLoadNumbers, sp, null);
                if (!createOverviewDashboard)
                {
                    // Save the application (otherwise, if creating overview dashboard, it will save later)
                    Util.saveApplication(maf, sp, null, u);
                }

                /*
                 * If the universal time dimension tables have not been created,
                 * create them
                 */
                setupTime(maf, sp.getFullConnectString(), curLoadNumbers);
                if (sp.QueryConnectionName != null && sp.QueryConnectionName == "IB Connection")
                    setupTime(maf, sp.getQueryFullConnectString(), curLoadNumbers);

                int numRows = -1;
                if (sp.TestMode)
                    numRows = int.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["TestModeNumRows"]);
                // See if we are managing snapshots for any data sources, if so, build snapshot policy table
                SnapshotPolicyManager spol = new SnapshotPolicyManager(conn, mainSchema, sp, maf);
                spol.setupPolicyTable(loadNumber);

                // See if we need to do this in groups (use a hashset to make sure groups are only added once)
                HashSet<string> subGroupList = null;
                if (subGroups == null)
                {
                    subGroupList = new HashSet<string>();
                    foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                    {
                        if (st.SubGroups != null && st.SubGroups.Length > 0)
                        {
                            foreach (string subgroup in st.SubGroups)
                            {
                                subGroupList.Add(subgroup);
                            }
                        }
                    }
                }
                else
                {
                    subGroupList = new HashSet<string>(subGroups);
                }

                if (!retryFailedLoad)
                {
                    //Delete and reInsert record in FAILED_LOADS_HISTORY table that can be used in case of retrying load
                    int noRowsDeleted = Database.deleteLastLoadHistory(conn, mainSchema, sp.ID, loadGroup);
                    Global.systemLog.Debug("Deleted " + noRowsDeleted + " entries from LAST_LOAD_HISTORY for " + sp.ID.ToString());
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (string sg in subGroupList)
                    {
                        if (first)
                            first = false;
                        else
                            sb.Append(",");
                        sb.Append(sg);
                    }
                    int noRowsInserted = Database.addLastLoadHistory(conn, mainSchema, sp.ID, loadGroup, loadNumber, loadDate, (sb.ToString().Length > 0 ? sb.ToString() : null));
                    Global.systemLog.Debug("Inserted " + noRowsInserted + " entries from LAST_LOAD_HISTORY for " + sp.ID.ToString());
                }

                HashSet<string> loadGroups = Util.getLoadGroups(maf);
                HashSet<string> liveAccessLoadGroups = processLiveAccessLoadGroups(conn, loadGroups, subGroupList, startLiveAccessLoad, createOverviewDashboard, webServiceSession, numRows);

                if ((loadGroups.Contains(loadGroup)) || (liveAccessLoadGroups.Count == 0))
                {
                    PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
                    bool peSupportsRetry = peVersion.SupportsRetryLoad;
                    bool isPeProcessingGroupAware = peVersion.IsProcessingGroupAware;
                    if (retryFailedLoad)
                    {
                        // check if perfengine supports retry, if so, find loadDate and subGroupList for last failed load
                        if (!peSupportsRetry)
                        {
                            Global.systemLog.Error("Space " + sp.ID.ToString() + " loadgroup " + loadGroup + " Processing Engine " + peVersion.Name + " does not support retry failed load operation");
                            throw new Exception("Space " + sp.ID.ToString() + " loadgroup " + loadGroup + " Processing Engine " + peVersion.Name + " does not support retry failed load operation");
                        }
                        if (!sp.AllowRetryFailedLoad)
                        {
                            Global.systemLog.Error("RetryFailedLoad is not enabled for Space " + sp.ID.ToString());
                            throw new Exception("RetryFailedLoad is not enabled for Space " + sp.ID.ToString());
                        }
                        LastLoadHistory llh = Database.getLastLoadHistory(conn, mainSchema, sp.ID, loadGroup, loadNumber);
                        if (llh == null)
                        {
                            Global.systemLog.Error("Could not find failed load history for space " + sp.ID.ToString() + " loadgroup " + loadGroup + " for loadNumber " + loadNumber + ", cannot retry failed load");
                            throw new Exception("Could not find failed load history for space " + sp.ID.ToString() + " loadgroup " + loadGroup + " for loadNumber " + loadNumber + ", cannot retry failed load");
                        }

                        loadDate = llh.LOADDATE;
                        if (llh.PROCESSINGGROUPS != null)
                        {
                            subGroupList = new HashSet<string>(llh.PROCESSINGGROUPS.Split(new char[] { ',' }));
                        }
                        else
                        {
                            subGroupList = new HashSet<string>();
                        }
                    }

                    string dbType = Util.getDBTypeForConnection(maf, Database.DEFAULT_CONNECTION);
                    bool appendDatabasePath = false;
                    if (dbType == Database.PARACCEL || dbType == Database.REDSHIFT)
                        appendDatabasePath = true;
                    else if (dbType == Database.INFOBRIGHT && maf.builderVersion >= 16 && maf.UseNewETLSchemeForIB)
                    {
                        appendDatabasePath = true;
                    }

                    bool hasPartitionedSourceFiles = false;
                    string[] partitionConnectionNames = null;
                    if (maf.IsPartitioned)
                    {
                        partitionConnectionNames = maf.getPartitionConnectionNames("Default Connection");
                        if (partitionConnectionNames != null && partitionConnectionNames.Count() > 0)
                        {
                            //Execute split text files command here
                            StringBuilder ssfCommands = getSplitSourceFileCommands();
                            File.WriteAllText(sp.Directory + "\\splitfiles.cmd", ssfCommands.ToString());
                            // Create the ProcessInfo object
                            ProcessStartInfo psiSplitFiles = new ProcessStartInfo(enginecmd);
                            psiSplitFiles.Arguments = ApplicationLoader.getSplitFilesCommandArgs(sp, loadNumber);
                            psiSplitFiles.WindowStyle = ProcessWindowStyle.Hidden;
                            psiSplitFiles.RedirectStandardOutput = true;
                            psiSplitFiles.CreateNoWindow = true;
                            psiSplitFiles.UseShellExecute = false;
                            psiSplitFiles.RedirectStandardInput = false;
                            psiSplitFiles.RedirectStandardError = true;
                            psiSplitFiles.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;

                            Global.systemLog.Debug("Starting: " + psiSplitFiles.FileName + " " + psiSplitFiles.Arguments);
                            Process splitFilesProc = null;
                            splitFilesProc = Process.Start(psiSplitFiles);
                            //Wait for split process to finish for now, later we can add a monitoring and status reporting for this step.
                            string line = null;
                            while ((line = splitFilesProc.StandardError.ReadLine()) != null)
                            {
                                Global.systemLog.Warn(line);
                            }
                            while ((line = splitFilesProc.StandardOutput.ReadLine()) != null)
                            {
                                Global.systemLog.Info(line);
                            }
                            if (!splitFilesProc.HasExited)
                            {
                                splitFilesProc.WaitForExit();
                            }
                            if (splitFilesProc.ExitCode == 0)
                            {
                                hasPartitionedSourceFiles = true;
                                Global.systemLog.Debug("Splitting files for partitioned space " + sp.ID.ToString() + " completed successfully");
                            }
                            else
                            {
                                Global.systemLog.Error("Problems encountered splitting files for partitioned space " + sp.ID.ToString());
                                throw new Exception("Problems encountered splitting files for partitioned space " + sp.ID.ToString());
                            }
                            splitFilesProc.Close();
                        }
                    }
                    ProcessStartInfo[] psiArray = null;
                    if (hasPartitionedSourceFiles)
                    {
                        //Execute multiple loads in parallel - START
                        psiArray = new ProcessStartInfo[partitionConnectionNames.Count() + 1];
                        int i = 0;
                        foreach (string partitionConnectionName in partitionConnectionNames)
                        {
                            StringBuilder commands = getLoadCommands(subGroupList, numRows, dbType, isPeProcessingGroupAware, retryFailedLoad, appendDatabasePath, partitionConnectionName, reprocessmodified);
                            File.WriteAllText(sp.Directory + "\\load_" + partitionConnectionName  + ".cmd", commands.ToString());
                            ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                            psi.Arguments = ApplicationLoader.getEngineCommandArgs(sp, loadNumber, partitionConnectionName);
                            psi.WindowStyle = ProcessWindowStyle.Hidden;
                            psi.RedirectStandardOutput = true;
                            psi.CreateNoWindow = true;
                            psi.UseShellExecute = false;
                            psi.RedirectStandardInput = false;
                            psi.RedirectStandardError = true;
                            psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                            psiArray[i++] = psi;
                        }
                        //add process that polls txn_command_history of all individual partition connection and updates it for default connection
                        StringBuilder pollCommands = getCommandsForPollingStatusOnPartitionConnections(subGroupList, retryFailedLoad);
                        File.WriteAllText(sp.Directory + "\\pollstatusforpartitionconnections.cmd", pollCommands.ToString());
                        ProcessStartInfo psiPoll = new ProcessStartInfo(enginecmd);
                        psiPoll.Arguments = ApplicationLoader.getPollStatusCommandArgs(sp, loadNumber);
                        psiPoll.WindowStyle = ProcessWindowStyle.Hidden;
                        psiPoll.RedirectStandardOutput = true;
                        psiPoll.CreateNoWindow = true;
                        psiPoll.UseShellExecute = false;
                        psiPoll.RedirectStandardInput = false;
                        psiPoll.RedirectStandardError = true;
                        psiPoll.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        psiArray[i++] = psiPoll;
                    }
                    else
                    {
                        StringBuilder commands = getLoadCommands(subGroupList, numRows, dbType, isPeProcessingGroupAware, retryFailedLoad, appendDatabasePath, null, reprocessmodified);
                        File.WriteAllText(sp.Directory + "\\load.cmd", commands.ToString());
                        // Create the ProcessInfo object
                        ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
                        psi.Arguments = ApplicationLoader.getEngineCommandArgs(sp, loadNumber);
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        psiArray = new ProcessStartInfo[1];
                        psiArray[0] = psi;
                    }
                    
                    if (!continueToLoad)
                    {
                        if (createOverviewDashboard)
                        {
                            if (bd == null)
                                bd = new BuildDashboards(maf, sp.Directory + "\\data\\");
                            bd.build(sp.Directory, scanBuff);
                            // Save the application
                            Util.saveApplication(maf, sp, null, u);
                        }

                        Status.updateTxnStatusWithNextOperation(sp, loadNumber, Status.OP_GEN_SCHEMA, retryFailedLoad);
                        return;
                    }

                    // Monitor the process
                    MonitorLoad ml = new MonitorLoad(createOverviewDashboard, bd, loadNumber, loadGroup, mainSchema);
                    ml.psiArray = psiArray;
                    ml.u = u;
                    if (loadpage != null)
                    {
                        ml.session = loadpage.getSession();
                        ml.u = (User)ml.session["User"];
                    }

                    if (webServiceSession != null)
                    {
                        // for webservice session, monitor load needs access to session object 
                        // to reset the status when done
                        ml.session = webServiceSession;
                    }
                    ml.scanBuff = scanBuff;
                    ml.sp = sp;
                    ml.maf = maf;

                    // calculate the timeout using the products table
                    int min = -1;
                    if (ml.session != null && u.RepositoryAdmin == false)
                    {
                     //   List<Product> plist = (List<Product>)ml.session["products"];
                        List<Product> plist = Database.getProducts(conn, mainSchema, u.ID);
                        foreach (Product p in plist)
                            if (p.MaxPublishTime > min) min = p.MaxPublishTime;
                    }

                    // rationalize the timeout, no timeout can be larger than MAX_TIMEOUT
                    if (min <= 0 || min > MAX_TIMEOUT)
                        min = MAX_TIMEOUT;
                    int milliseconds = min * 60 * 1000;

                    BackgroundTask bt = new BackgroundTask(ml, milliseconds);

                    if (backgroundRequest)
                    {
                        //Assuming this code only executes if Acorn's scheduler is expected to process load - no longer in use
                        // Add to LOAD_SCHEDULE to handle background processing by BirstScheduler
                        string parameters = buildTaskParameters(enginecmd, psiArray[0].Arguments, loadNumber, loadGroup);
                        Database.addTaskSchedule(conn, mainSchema, sp.ID, "SFORCE_PROCESS", parameters);
                    }
                    else
                    {
                        //Bugfix 8767 Before starting the process, update the txn_command_history 
                        // Check the description summary for the following method to understand in detail
                        Status.updateTxnStatusWithNextOperation(sp, loadNumber, Status.OP_GEN_SCHEMA, retryFailedLoad || reprocessmodified);
                        // Otherwise, standard "background" processing for interactive use in Acorn
                        BackgroundProcess.startModule(bt);
                        lock (backgroundProcess)
                        {
                            string spaceID = sp.ID.ToString();
                            if (backgroundProcess.ContainsKey(spaceID))
                            {
                                backgroundProcess[spaceID] = ml;
                            }
                            else
                            {
                                backgroundProcess.Add(spaceID, ml);
                            }
                            //Database.addProcessingServer(mainSchema, sp.ID, "PROCESS_DATA", Util.getServerIP(), Util.getServerPort());
                        }
                    }

                    if (webServiceSession != null)
                    {
                        // for webservice, nothing else to do, not a blocking call                
                        return;
                    }

                    if (loadpage != null)
                    {
                        Status.StatusResult sr = new Status.StatusResult(createOverviewDashboard ? Status.StatusCode.BuildingDashboards : Status.StatusCode.Started);
                        loadpage.getSession()["status"] = sr;
                        loadpage.setRunning(maf, sp, mainSchema);
                    }
                    else
                        if (!backgroundRequest)
                            bt.waitUntilFinished();
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
                maf.setInUberTransaction(previousUberTransaction);
            }
        }

        private void processAutomatic(QueryConnection conn)
        {
            // Find foreign keys (level keys) in staging tables
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                /*
                * Make sure that the columns in this level key match the existing columns in 
                * this dimension (i.e. exact same type)
                */
                List<StagingTable> stlist = new List<StagingTable>();
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    foreach (string[] l in st.Levels)
                    {
                        if (l[0] == h.Name)
                        {
                            stlist.Add(st);
                            break;
                        }
                    }
                }
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    bool found = false;
                    foreach (string[] l in st.Levels)
                    {
                        if (l[0] == h.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    bool allKeys = true;
                    foreach (StagingColumn sc in st.Columns)
                        allKeys = allKeys && (sc.NaturalKey || sc.Width > Database.MAX_VARCHAR_SIZE);
                    /*
                     * If not currently mapped in the grain, see if it can be mapped
                     */
                    if (!found)
                    {
                        // Look for matches
                        Level mappedLevel = null;
                        LevelKey mappedLevelKey = null;
                        // Find the lowest level in the table
                        foreach (Level l in llist)
                        {
                            if (l.GenerateDimensionTable)
                                foreach (LevelKey lk in l.Keys)
                                    if (!lk.SurrogateKey)
                                    {
                                        bool hasKey = true;
                                        foreach (string s in lk.ColumnNames)
                                        {
                                            bool keycolfound = false;
                                            foreach (StagingColumn sc in st.Columns)
                                            {
                                                if (sc.Name == s)
                                                {
                                                    keycolfound = true;
                                                    /*
                                                     * Make sure that the column type matches any existing definitions
                                                     */
                                                    foreach (StagingTable sst in stlist)
                                                    {
                                                        foreach (StagingColumn ssc in sst.Columns)
                                                        {
                                                            if (ssc.NaturalKey && ssc.Name == s)
                                                            {
                                                                if (ssc.DataType != sc.DataType || (ssc.DataType == "Varchar" && !similarWidth(ssc.Width, sc.Width)))
                                                                {
                                                                    keycolfound = false;
                                                                }
                                                                else
                                                                {
                                                                    keycolfound = true;
                                                                    /*
                                                                     * Make sure that if we are going to match
                                                                     * two varchar columns, that widths are large
                                                                     * enough on both sides to be loaded by
                                                                     * each other - bulk load will fail if data
                                                                     * is truncated
                                                                     */
                                                                    if (sc.DataType == "Varchar")
                                                                    {
                                                                        if (sc.Width < ssc.Width)
                                                                            sc.Width = ssc.Width;
                                                                        else if (sc.Width > ssc.Width)
                                                                            ssc.Width = sc.Width;
                                                                    }
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            if (!keycolfound)
                                            {
                                                hasKey = false;
                                                break;
                                            }
                                        }
                                        if (hasKey)
                                        {
                                            if (mappedLevel == null)
                                            {
                                                mappedLevel = l;
                                                mappedLevelKey = lk;
                                            }
                                            else if (mappedLevel.findLevel(h, l.Name) != null)
                                            {
                                                mappedLevel = l;
                                                mappedLevelKey = lk;
                                            }
                                            break;
                                        }
                                    }
                        }
                        if (mappedLevel != null)
                        {
                            bool ok = true;
                            if (st.SourceGroups != null)
                            {
                                if (h.SourceGroups == null || h.SourceGroups.Length == 0)
                                    ok = false;
                                else
                                {
                                    string[] sg = st.SourceGroups.Split(new char[] { ',' });
                                    string[] sg2 = h.SourceGroups.Split(new char[] { ',' });
                                    found = false;
                                    foreach (string s in sg)
                                    {
                                        foreach (string s2 in sg2)
                                        {
                                            if (s == s2)
                                            {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found)
                                            break;
                                    }
                                    if (!found)
                                        ok = false;
                                }
                            }
                            if (ok)
                            {
                                List<string[]> stllist = new List<string[]>(st.Levels);
                                stllist.Add(new string[] { h.Name, mappedLevel.Name });
                                st.Levels = stllist.ToArray();
                                foreach (StagingColumn sc in st.Columns)
                                    if (Array.IndexOf<string>(mappedLevelKey.ColumnNames, sc.Name) >= 0)
                                        sc.NaturalKey = true;
                            }
                        }
                    }
                }
            }
            /*
             * Fold hierarchies - find hierarchies that are functionally dependent on other
             * hierarchies and fold them together. Only do this during the first load
             * because folding hierarchies later will change the dimensional structure
             * and lead to lost historical data (schema will change significantly).
             */
            if (!sp.Folded)
            {
                // Mark as folded
                sp.Folded = true;
                Database.updateNonDBSpaceParams(conn, mainSchema, sp);
                foldHierarchies(maf, sp, scanBuff, null, null, false);
            }
            else if (loadpage != null)
                updateDependencies(maf, sp);

            // Regenerate hierarchies (including tree nodes)
            /*
            List<Hierarchy> hlist = maf.hmodule.getLatestHierarchies();
            maf.hmodule.clearNodes();
            foreach (Hierarchy h in hlist)
                maf.hmodule.addHierarchy(h);
             */
        }

        private HashSet<String> processLiveAccessLoadGroups(QueryConnection conn, HashSet<String> loadGroups, HashSet<string> subGroupList, bool startLiveAccessLoad,
           bool createOverviewDashboard, HttpSessionState webServiceSession, int numRows)
        {
            HashSet<string> liveAccessLoadGroups = new HashSet<string>();           
            if (loadGroup == Util.LOAD_GROUP_NAME)
            {
                liveAccessLoadGroups = LiveAccessUtil.getLiveAccessLoadGroups(maf);
            }

            foreach (string lg in liveAccessLoadGroups)
            {
                liveAccessLoadStatusMap.Add(lg, false);

                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                LocalETLConfig leConfig = null;
                if (localETLConfigs != null && localETLConfigs.ContainsKey(lg))
                {
                    leConfig = localETLConfigs[lg];
                }
                else
                {
                    Global.systemLog.Error("Local ETL configuration issue, encountered empty local ETL config for load group: " + lg);
                    continue;
                }
                PerformanceEngineVersion lePEVersion = LiveAccessUtil.getLocalProcessingEngineVersion(sp, leConfig.getLocalETLConnection());
                bool lePESupportsRetry = lePEVersion != null ? lePEVersion.SupportsRetryLoad : false;
                bool islePEProcessingGroupAware = lePEVersion != null ? lePEVersion.IsProcessingGroupAware : false;
                //XXX TODO: find local processing engine's engine version info
                if (retryFailedLoad)
                {
                    // check if perfengine supports retry, if so, find loadDate and subGroupList for last failed load
                    if (!lePESupportsRetry)
                    {
                        Global.systemLog.Error("Space " + sp.ID.ToString() + " loadgroup " + lg + " Processing Engine " + lePEVersion.Name + " does not support retry failed load operation");
                        throw new Exception("Space " + sp.ID.ToString() + " loadgroup " + lg + " Processing Engine " + lePEVersion.Name + " does not support retry failed load operation");
                    }
                    if (!sp.AllowRetryFailedLoad)
                    {
                        Global.systemLog.Error("RetryFailedLoad is not enabled for Space " + sp.ID.ToString());
                        throw new Exception("RetryFailedLoad is not enabled for Space " + sp.ID.ToString());
                    }
                    LastLoadHistory llh = Database.getLastLoadHistory(conn, mainSchema, sp.ID, lg, loadNumber);
                    if (llh == null)
                    {
                        Global.systemLog.Error("Could not find failed load history for space " + sp.ID.ToString() + " loadgroup " + lg + " for loadNumber " + loadNumber + ", cannot retry failed load");
                        throw new Exception("Could not find failed load history for space " + sp.ID.ToString() + " loadgroup " + lg + " for loadNumber " + loadNumber + ", cannot retry failed load");
                    }

                    loadDate = llh.LOADDATE;
                    if (llh.PROCESSINGGROUPS != null)
                    {
                        subGroupList = new HashSet<string>(llh.PROCESSINGGROUPS.Split(new char[] { ',' }));
                    }
                    else
                    {
                        subGroupList = new HashSet<string>();
                    }
                }

                string localDbType = Util.getDBTypeForConnection(maf, leConfig.getLocalETLConnection());
                bool appendDatabasePath = false;
                if (localDbType == Database.PARACCEL)
                    appendDatabasePath = true;
                else if (localDbType == Database.INFOBRIGHT && maf.builderVersion >= 16 && maf.UseNewETLSchemeForIB)
                {
                    appendDatabasePath = true;
                }

                StringBuilder timeCommands = BirstConnectUtil.getLiveAccessTimeCommands(sp, loadNumber, leConfig.getApplicationDirectory());
                File.WriteAllText(sp.Directory + "\\generatetime." + lg + ".properties", timeCommands.ToString());
                StringBuilder timeOverrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), true);
                File.WriteAllText(sp.Directory + "\\customer.time." + lg + ".properties", timeOverrideProperties.ToString());
                StringBuilder commands = getLiveAccessLoadCommands(subGroupList, numRows, lg, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), localDbType, islePEProcessingGroupAware, retryFailedLoad, appendDatabasePath);
                File.WriteAllText(sp.Directory + "\\load." + lg + ".properties", commands.ToString());
                StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                File.WriteAllText(sp.Directory + "\\customer." + lg + ".properties", overrideProperties.ToString());

                QueryConnection spconn = null;
                try
                {
                    spconn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    if (!Database.tableExists(spconn, sp.Schema, Status.tableName))
                    {
                        bool isIB = Database.isDBTypeIB(sp.ConnectString);
                        Database.createTxnCommandHistoryTable(spconn, sp, isIB);
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(spconn);
                }

                //Bugfix 8767 Before starting the process, update the txn_command_history
                Status.updateTxnStatusWithNextOperation(sp, loadNumber, null, retryFailedLoad);
                Status.updateTxnStatusWithNextOperationOnLiveAccessConnection(sp, loadNumber, Status.OP_GEN_SCHEMA, retryFailedLoad, leConfig);

                if (createOverviewDashboard)
                {
                    Status.setBuildDashboardsOnLiveAccessConnection(sp, Status.RUNNING, leConfig);
                    // Build automatic dashboards
                    if (bd == null)
                        bd = new BuildDashboards(maf, sp.Directory + "\\data");
                    bd.build(sp.Directory, scanBuff);
                    // Save the application
                    Util.saveApplication(maf, sp, loadpage != null ? loadpage.getSession() : webServiceSession, u);
                    Status.setBuildDashboardsOnLiveAccessConnection(sp, Status.COMPLETE, leConfig);
                }

                if (startLiveAccessLoad)
                {
                    bool loadStarted = LiveAccessUtil.sendCommandToLiveAccess(sp, leConfig.getLocalETLConnection(), "initiateliveaccessload\r\n" + loadNumber);
                    if (!loadStarted)
                    {
                        Global.systemLog.Error("Live access load could not be started for load group: " + lg);
                        DatabaseConnection liveAccessDc = null;
                        string liveAccessConnectionName = leConfig.getLocalETLConnection();
                        foreach (DatabaseConnection dc in maf.connection.connectionList)
                        {
                            if (dc.Name != null && dc.Name == liveAccessConnectionName)
                            {
                                liveAccessDc = dc;
                                break;
                            }
                        }
                        if (liveAccessDc == null)
                        {
                            continue;
                        }
                        string liveAccessConnSchemaName = liveAccessDc.Schema == null ? sp.Schema : liveAccessDc.Schema;
                        bool liveAccessTableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, liveAccessConnectionName, liveAccessConnSchemaName, Status.tableName);
                        if (!liveAccessTableExists)
                        {
                            continue;
                        }
                        Global.systemLog.Info("Deleting previous for spaceId = " + sp.ID + " and iteration = " + loadNumber + " on liveaccessconnection:" + liveAccessConnectionName);
                        int noRowsDeleted = LiveAccessUtil.executeUpdateLiveAccessQuery(sp, liveAccessConnectionName, "DELETE FROM " + liveAccessConnSchemaName + "." + Status.tableName + " WHERE iteration = " + loadNumber);
                        if (noRowsDeleted == -1)
                        {
                            Global.systemLog.Warn("Could not delete status entries for loadnumber " + loadNumber + " on live access connection:" + liveAccessConnectionName);
                            continue;
                        }
                        Global.systemLog.Info("Deletion done from " + sp.Schema + "." + Status.tableName + ". Rows deleted = " + noRowsDeleted + " on liveaccessconnection:" + liveAccessConnectionName);
                    }
                    else
                    {
                        liveAccessLoadStatusMap[lg] = true;
                    }
                }
            }
            return liveAccessLoadGroups;
        }

        public static String getEngineCommandArgs(Space sp, int loadNumber)
        {
            return sp.Directory + "\\load.cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber);
        }

        public static String getEngineCommandArgs(Space sp, int loadNumber, string partitionConnectionName)
        {
            return sp.Directory + "\\load_" + partitionConnectionName + ".cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber) + " \"." + partitionConnectionName + "\"";
        }

        public static String getPollStatusCommandArgs(Space sp, int loadNumber)
        {
            return sp.Directory + "\\pollstatusforpartitionconnections.cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber);
        }

        public static String getSplitFilesCommandArgs(Space sp, int loadNumber)
        {
            return sp.Directory + "\\splitfiles.cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber);
        }

        private StringBuilder getSplitSourceFileCommands()
        {
            StringBuilder ssfCommands = new StringBuilder();
            ssfCommands.Append("repository \"" + sp.Directory + "\\repository_dev.xml\"\n");
            ssfCommands.Append("splitsourcefilesforpartitions \"" + sp.Directory + "\\data\"\n");
            return ssfCommands;
        }

        private StringBuilder getCommandsForPollingStatusOnPartitionConnections(HashSet<string> subGroupList, bool retryFailedLoad)
        {
            string subGroups = null;
            if (subGroupList != null && subGroupList.Count > 0)
            {
                // can not sort hashsets, so need to turn it into a list
                List<string> temp = new List<string>(subGroupList);
                temp.Sort();
                StringBuilder subGroupsSB = new StringBuilder();
                bool first = true;
                foreach (string sg in temp)
                {
                    if (first)
                        first = false;
                    else
                        subGroupsSB.Append(",");
                    subGroupsSB.Append(sg);
                }
                subGroups = subGroupsSB.ToString();
            }
            StringBuilder commands = new StringBuilder();
            commands.Append("repository \"" + sp.Directory + "\\repository_dev.xml\"\n");
            if (!retryFailedLoad)
            {
                commands.Append("resetetlrun " + loadNumber + "\n");
            }
            commands.Append("consolidatetxncmdhistory " + loadNumber + " loadgroup " + loadGroup + " runinpollingmode" + (subGroups != null ? " subgroups \"" + subGroups + "\"" : "") + "\n");
            return commands;
        }

        
        private StringBuilder getLoadCommands(HashSet<string> subGroupList, int numRows, string dbType, bool isPeProcessingGroupAware, bool retryFailedLoad, bool appendDatabasePath,
            string partitionConnectionName, bool reprocessmodified)
        {

            int engineVersionInfo = Util.getProcessingEngineVersion(sp);
            StringBuilder commands = new StringBuilder();
            // add the check in file name. Used to update the check in time for the external scheduler
            commands.Append("exec \"copy " + sp.Directory + "\\..\\" + Util.LOAD_LOCK_FILE + " " + sp.Directory + "\"\n");
            if (isPeProcessingGroupAware)
            {
                string checkInFileName = sp.Directory + "\\checkIn-load.txt";
                commands.Append("checkIn " + checkInFileName + "\n");
            }
            if (partitionConnectionName != null)
            {
                commands.Append("setpartitionconnection \"" + partitionConnectionName + "\"\n");
            }
            string stagingLoadDir = sp.Directory + "\\data";
            string databaseLoadDir = sp.DatabaseLoadDir + "\\data";
            if (partitionConnectionName != null)
            {
                PartitionConnectionLoadInfo pcli = maf.getPartitionConnectionLoadInfo(partitionConnectionName);
                if (pcli != null)
                {
                    if (pcli.StagingLoadDir != null)
                    {
                        stagingLoadDir = pcli.StagingLoadDir;
                    }
                    if (pcli.DatabaseLoadDir != null)
                    {
                        databaseLoadDir = pcli.DatabaseLoadDir;
                    }
                }
            }
            // passing the updated allowed packges details
            List<PackageSpaceProperties> packageSpacesList = Util.getImportedPackageSpaces(sp, maf);
            if (packageSpacesList != null && packageSpacesList.Count > 0)
            {
                foreach (PackageSpaceProperties packageSpaceProperties in packageSpacesList)
                {
                    string spaceProps = "packageID=" + packageSpaceProperties.PackageID + " packageName=" + packageSpaceProperties.PackageName +
                        " spaceID=" + packageSpaceProperties.SpaceID.ToString() + " spaceDirectory=" + packageSpaceProperties.SpaceDirectory + " spaceName=" + packageSpaceProperties.SpaceName;
                    if (engineVersionInfo >= 1) // 5.2.3 or later processing engine
                        commands.Append("setProperties start packageSpaceProperties " + spaceProps + "  end\n");
                }
            }

            commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
            if (sp.QueryConnectString != null && sp.QueryDatabaseType == "Infobright")
            {
                if (!Directory.Exists(sp.Directory + "\\dump"))
                {
                    Directory.CreateDirectory(sp.Directory + "\\dump");
                }
                commands.Append("setdefaultconnection \"" + Database.DEFAULT_CONNECTION + "\"\n");
            }
            commands.Append("setvariable LoadDate " + Util.getShortDateString(loadDate, dbType) + "\n");
            if (u != null && u.Username != null)
            {
                commands.Append("setvariable USER \"" + u.Username + "\"\n");
            }
            //Infobright with engine processing engine 5.5 or later - write a command to drop temp tables
            if ((sp.DatabaseType != null) && (sp.DatabaseType == "Infobright") && (engineVersionInfo >= 3))
            {
                commands.Append("droptemptables birstx_temp " + "\n");
            }

            if (!retryFailedLoad)
            {
                commands.Append("resetetlrun " + loadNumber + ((reprocessmodified && maf.builderVersion >= 26 && engineVersionInfo>=4) ? " onlymodifiedsources" : "") + "\n");
            }
            // Adding the marker for start of the load -- for getting accurate start time
            if (isPeProcessingGroupAware)
            {
                commands.Append("loadMarker " + loadNumber + " " + loadGroup + " status " + Status.RUNNING + "\n");
            }
            else
            {
                commands.Append("loadMarker " + loadGroup + " status " + Status.RUNNING + "\n");
            }

            bool spaceHasS3Sources = false;
            foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
            {
                spaceHasS3Sources = (sf.S3Bucket != null);
                if (spaceHasS3Sources)
                    break;
            }

            string awsCredentials = null;
            if (spaceHasS3Sources)
                awsCredentials = Acorn.Utils.AWSUtils.getAWSCredentials(awsAccessKeyId, awsSecretKey); // should access S3 bucket scopes to this eventually XXX
            else if (dbType != null && dbType == Database.REDSHIFT)
                awsCredentials = Acorn.Utils.AWSUtils.getAWSCredentials(dbType, sp);

            // Generate schema - only copy lock file if schema has changed
            commands.Append("generateschema " + loadNumber + " notime \"execonchange=copy " + sp.Directory + "\\..\\publish.lock " + sp.Directory + "\"" 
                + (appendDatabasePath ? " \"databasepath=" + sp.DatabaseLoadDir + "\\data\"" : "")
                + awsCredentials
                 + "\n");
            // Don't do deletes to enforce snapshot policy (i.e. delete records that don't fit) now - to be safe
            // commands.Append("enforcesnapshotpolicy\n");
            if (subGroups == null)
            {
                commands.Append("loadstaging " + stagingLoadDir + " " + loadNumber + " loadgroup " + loadGroup + (subGroupList.Count > 0 ? " subgroups NONE" : "") 
                    + " databasepath " + databaseLoadDir + " " + "numrows " + numRows
                    + awsCredentials
                    + "\n");
            }
            if (!maf.disablePublishLock && (sp.QueryConnectString == null || sp.QueryConnectString.Length == 0))
            {
                // Copy lock file for sure
                commands.Append("exec \"copy " + sp.Directory + "\\..\\publish.lock " + sp.Directory + "\"\n");
            }
            if (subGroups == null)
            {
                //scd date is always in MM/dd/yyyy format.. PerfEngine is expected to parse that format..
                commands.Append("loadwarehouse " + loadNumber + " loadgroup " + loadGroup + (subGroupList.Count > 0 ? " subgroups NONE" : "") + " scddate " + loadDate.ToString(Util.DEFAULT_DATE_FORMAT) + (appendDatabasePath ? " \"databasepath=" + sp.DatabaseLoadDir + "\\data\"" : "")
                    + ((reprocessmodified && maf.builderVersion >= 26 && engineVersionInfo >= 4) ? " onlymodifiedsources" : "")
                    + awsCredentials
                    + "\n");
            }
            if (subGroupList.Count > 0)
            {
                // can not sort hashsets, so need to turn it into a list
                List<string> temp = new List<string>(subGroupList);
                temp.Sort();
                foreach (string s in temp)
                {
                    if (!isPeProcessingGroupAware)
                    {
                        //old perfengine
                        commands.Append("continueetlrun " + loadNumber + "\n");
                    }
                    commands.Append("loadstaging " + stagingLoadDir + " " + loadNumber + " loadgroup " + loadGroup + " subgroups '" + s
                        + "' databasepath " + databaseLoadDir + " " + "numrows " + numRows
                        + awsCredentials
                        + "\n");
                    //scd date is always in MM/dd/yyyy format.. PerfEngine is expected to parse that format.. 
                    commands.Append("loadwarehouse " + loadNumber + " loadgroup " + loadGroup + " subgroups '" + s + "' keepfacts scddate " + loadDate.ToString(Util.DEFAULT_DATE_FORMAT) + (appendDatabasePath ? " \"databasepath=" + sp.DatabaseLoadDir + "\\data\"" : "") + "\n");
                    if ((subGroups != null && subGroups.Length > 0) || engineVersionInfo == 0) //For earlier processing engine, always write persistaggregate command for each subgroup
                    {
                        if (engineVersionInfo >= 1) // 5.2.3 or later processing engine needs additional parameters for processing inmemory aggregates
                            commands.Append("persistaggregates loadgroup=" + loadGroup + " subgroup='" + s + "' datadir=" + sp.Directory + "\\data databasepath=" + sp.DatabaseLoadDir + "\\data\n");
                        else
                            commands.Append("persistaggregates loadgroup=" + loadGroup + " subgroup='" + s + "'\n");
                    }
                }
            }
            if (subGroups == null || subGroups.Length==0)
            {
                if (engineVersionInfo >= 1) // 5.2.3 or later processing engine needs additional parameters for processing inmemory aggregates
                    commands.Append("persistaggregates loadgroup=" + loadGroup + " datadir=" + sp.Directory + "\\data databasepath=" + sp.DatabaseLoadDir + "\\data\n");
                else
                    commands.Append("persistaggregates loadgroup=" + loadGroup + "\n");
            }
            if (sp.QueryConnectionName != null && sp.QueryConnectionName == "IB Connection")
            {
                commands.Append("exec \"copy " + sp.Directory + "\\..\\publish.lock " + sp.Directory + "\"\n");
                commands.Append("setdefaultconnection \"" + sp.QueryConnectionName + "\"\n");
                commands.Append("generateschema 1 notime skipmissingvalues\n");
                commands.Append("setdefaultconnection \"" + Database.DEFAULT_CONNECTION + "\"\n");
                commands.Append("exportwarehouse ALL " + sp.Directory + "\\dump \"connection=" + sp.QueryConnectionName + "\"\n");
                commands.Append("loadinfobright " + sp.Directory + "\\dump\n");
                commands.Append("errorcontinuepoint\n");
                commands.Append("exec \"del /Q " + sp.Directory + "\\dump\\*.*\"\n");
            }

            if (dbType != null && (dbType == Database.PARACCEL || dbType == Database.REDSHIFT))
            {
                if (RunAnalyzeAfterLoadingDataIntoPADB)
                    commands.Append("performanalyzeschema " + loadNumber + " " + loadGroup + "\n");
                if (RunVacuumAfterLoadingDataIntoPADB)
                    commands.Append("performvacuumschema " + loadNumber + " " + loadGroup + "\n");
            }
            
            commands.Append("errorcontinuepoint\n");
            // executescriptgroup needed for both "All" or any subgroup
            commands.Append("executescriptgroup " + loadGroup + "\n");
            // marker to depict end of load. This is needed because of status issues around multiple processing groups. So 
            // we add a single marker to give that feedback
            commands.Append("errorcontinuepoint\n");
            if (isPeProcessingGroupAware)
            {
                commands.Append("loadMarker " + loadNumber + " " + loadGroup + " status " + Status.COMPLETE + "\n");
            }
            else
            {
                commands.Append("loadMarker " + loadGroup + " status " + Status.COMPLETE + "\n");
            }
            // Copy the repository to production just in case (if Admin dies, at least this will still happen, but the requirespublish flag may not be reset)
            commands.Append("exec \"copy " + sp.Directory + "\\repository_dev.xml " + sp.Directory + "\\repository.xml\"\n");
            commands.Append("exec \"Touch \"" + sp.Directory + "\\repository.xml\"\"\n");
            
            // Delete the publish lock
            commands.Append("exec \"del " + sp.Directory + "\\publish.lock\"\n");
            commands.Append("exec \"del " + sp.Directory + "\\" + Util.LOAD_LOCK_FILE + "\"\n");
            return commands;
        }

        private StringBuilder getLiveAccessLoadCommands(HashSet<string> subGroupList, int numRows, string lg, string applicationPath, string liveAccessConnectionName, string dbType, bool isPeProcessingGroupAware, bool retryFailedLoad, bool appendDatabasePath)
        {
            StringBuilder commands = new StringBuilder();
            string liveAccessDirectory = applicationPath + "\\" + sp.ID;
            commands.Append("repository \"" + liveAccessDirectory + "\\repository_dev.xml\"\n");
            commands.Append("setvariable LoadDate " + Util.getShortDateString(loadDate, dbType) + "\n");
            if (u != null && u.Username != null)
            {
                commands.Append("setvariable USER " + u.Username + "\n");
            }
            if (!retryFailedLoad)
            {
                commands.Append("resetetlrun " + loadNumber + "\n");                
            }
            // Adding the marker for start of the load -- for getting accurate start time
            if (isPeProcessingGroupAware)
            {
                commands.Append("loadMarker " + loadNumber + " " + lg + " status " + Status.RUNNING + "\n");
            }
            else
            {
                commands.Append("loadMarker " + lg + " status " + Status.RUNNING + "\n");
            }            
            // Generate schema - only copy lock file if schema has changed
            commands.Append("generateschema " + loadNumber + " notime \"execonchange=copy \\\"" + liveAccessDirectory + "\\..\\publish.lock\\\" \\\"" + liveAccessDirectory + "\\\"\"" + (appendDatabasePath ? " \"databasepath=" + liveAccessDirectory + "\\data\"" : "") + "\n");
            // Don't do deletes to enforce snapshot policy (i.e. delete records that don't fit) now - to be safe
            // commands.Append("enforcesnapshotpolicy\n");
            if (subGroups == null)
            {
                commands.Append("loadstaging \"" + liveAccessDirectory + "\\data\" " + loadNumber + " loadgroup " + lg + (subGroupList.Count > 0 ? " subgroups NONE" : "") + " databasepath \"" + liveAccessDirectory + "\\data\" numrows " + numRows + "\n");
            }
            if (!maf.disablePublishLock && (sp.QueryConnectString == null || sp.QueryConnectString.Length == 0))
            {
                // Copy lock file for sure
                commands.Append("exec \"copy \\\"" + liveAccessDirectory + "\\..\\publish.lock\\\" \\\"" + liveAccessDirectory + "\\\"\"\n");
            }
            if (subGroups == null)
            {
                commands.Append("loadwarehouse " + loadNumber + " loadgroup " + lg + (subGroupList.Count > 0 ? " subgroups NONE" : "") + (appendDatabasePath ? " \"databasepath=" + liveAccessDirectory + "\\data\"" : "") + "\n");
                commands.Append("persistaggregates loadgroup=" + lg + "\n");
            }
            if (subGroupList.Count > 0)
            {
                // can not sort hashsets, so need to turn it into a list
                List<string> temp = new List<string>(subGroupList);
                temp.Sort();
                foreach (string s in temp)
                {
                    if (!isPeProcessingGroupAware)
                    {
                        //old perfengine
                        commands.Append("continueetlrun " + loadNumber + "\n");
                    }
                    commands.Append("loadstaging \"" + liveAccessDirectory + "\\data\" " + loadNumber + " loadgroup " + lg + " subgroups '" + s + "' databasepath \"" + liveAccessDirectory + "\\data\" numrows " + numRows + "\n");
                    commands.Append("loadwarehouse " + loadNumber + " loadgroup " + lg + " subgroups '" + s + "' keepfacts" + (appendDatabasePath ? " \"databasepath=" + liveAccessDirectory + "\\data\"" : "") + "\n");
                    commands.Append("persistaggregates loadgroup=" + lg + " subgroup='" + s + "'\n");
                }
            }

            commands.Append("errorcontinuepoint\n");
            // executescriptgroup needed for both "All" or any subgroup
            commands.Append("executescriptgroup " + lg + "\n");
            // marker to depict end of load. This is needed because of status issues around multiple processing groups. So 
            // we add a single marker to give that feedback
            commands.Append("errorcontinuepoint\n");
            if (isPeProcessingGroupAware)
            {
                commands.Append("loadMarker " + loadNumber + " " + lg + " status " + Status.COMPLETE + "\n");
            }
            else
            {
                commands.Append("loadMarker " + lg + " status " + Status.COMPLETE + "\n");
            }
            // Copy the repository to production just in case (if Admin dies, at least this will still happen, but the requirespublish flag may not be reset)
            commands.Append("exec \"copy \\\"" + liveAccessDirectory + "\\repository_dev.xml\\\" \\\"" + liveAccessDirectory + "\\repository.xml\\\"\"\n");
            // Delete the publish lock
            commands.Append("exec \"del \\\"" + liveAccessDirectory + "\\publish.lock\\\"\"\n");
            return commands;
        }

        /*
         * If keys have both varchars, compare the widths - if they are similar (e.g. due 
         * to scan differences) then consider them the same
         */
        public static bool similarWidth(int a, int b)
        {
            if (a == b)
                return (true);
            if (a > b)
            {
                return ((double)a / (double)b < 1.75);
            }
            else
            {
                return ((double)b / (double)a < 1.75);
            }
        }


        public static void foldHierarchies(MainAdminForm maf, Space sp, ScanBuffer scanBuffer, List<Hierarchy[]> hierarchyMatchList, List<Hierarchy> oldList, bool isUpdatingLogicalModel)
        {
            List<Hierarchy[]> matches = null;
            Dictionary<Hierarchy, Level> examinedLevels = new Dictionary<Hierarchy, Level>();
            /*
             * Find hierarchy dependency matches. "matches" contains each set of dependent hierarchies.
             * The first element is the key hierarchy, and the second is the hierarchy that is dependent
             * on the first. For example, in a master-detail situation where there is an Order hierarchy
             * and an Order Detail hierarchy, since every Order Detail is tied to an order, Order is
             * dependent on Order detail. As a result there would be a match [Order Detail, Order]
             */
            if (hierarchyMatchList == null)
            {
                matches = new List<Hierarchy[]>();
                getMatches(maf, sp, matches, examinedLevels, scanBuffer, isUpdatingLogicalModel);
            }
            else
            {
                // Only add hierarchies that haven't been processed yet
                matches = new List<Hierarchy[]>();
                foreach (Hierarchy[] match in hierarchyMatchList)
                {
                    bool found = false;
                    if (oldList != null)
                    {
                        foreach (Hierarchy h in oldList)
                        {
                            if (match[0].Name == h.Name || match[1].Name == h.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                        matches.Add(match);
                }

                foreach (Hierarchy[] match in matches)
                {
                    string lname = maf.hmodule.getDimensionKeyLevel(match[0].DimensionName);
                    if (!examinedLevels.ContainsKey(match[0]))
                        examinedLevels.Add(match[0], match[0].findLevel(lname));
                    lname = maf.hmodule.getDimensionKeyLevel(match[1].DimensionName);
                    if (!examinedLevels.ContainsKey(match[1]))
                        examinedLevels.Add(match[1], match[1].findLevel(lname));
                }
            }

            List<HierarchyLevel[]> dependencies = new List<HierarchyLevel[]>();
            /*
             * Prune any hierarchies that are dependent on more than one other hierarchy.
             * These hierarchies should be separate dimensions.
             * 
             * Because fact tables can only be declared to be at the grain of one level
             * in a given dimension, only allow a hierarchy to have one hierarchy folded above it (assumes 
             * that levels in that hierarchy are parallel). Cannot allow a level to be a shared child if that
             * level has a fact.
             * 
             * Additional dependencies will be treated with snowflakes
             */
            List<Hierarchy[]> folds = new List<Hierarchy[]>();
            List<Hierarchy[]> mergeCandidates = new List<Hierarchy[]>();
            foreach (Hierarchy[] match in matches)
            {
                bool found = false;
                foreach (Hierarchy[] match2 in matches)
                {
                    if (match != match2 && match[1] == match2[1])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    foreach (Hierarchy[] match2 in matches)
                    {
                        /*
                         * Because of the UI limitation for advanced mode that all hierarchies must be strict parent-child (i.e.
                         * you cannot create shared children) we must allow only one dependency match for a given hierarchy.
                         * Choose the dependent hierarchy with the highest cardinality (most likely to be a master-level
                         * fact table and we want keys to be carried down). When/if more advanced UI is enabled, this code can be 
                         * removed. 
                         * 
                         * In the case of the new model wizard, use the hierarchy that was designated as parent if that's the case
                         */
                        if (match != match2 && match[0] == match2[0])
                        {
                            bool done = false;
                            if (match[0].ModelWizard)
                            {
                                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                                {
                                    if (st.HierarchyName == match[0].Name && st.ParentForeignKeySource != null)
                                    {
                                        done = true;
                                        StagingTable st2 = maf.stagingTableMod.findTable(st.ParentForeignKeySource);
                                        if (st2 != null && match[1].Name != st2.HierarchyName)
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!done)
                            {
                                Level l = match[1].findLevelContainingDimensionKey();
                                Level l2 = match2[1].findLevelContainingDimensionKey();
                                if (l == null)
                                    Global.systemLog.Error("ApplicationLoader.foldHierarchies: Could not find level containing dimension key in hierarchy " + match[1].Name + " where dimension key is " + match[1].DimensionKeyLevel);
                                if (l2 == null)
                                    Global.systemLog.Error("ApplicationLoader.foldHierarchies: Could not find level containing dimension key in hierarchy " + match2[1].Name + " where dimension key is " + match2[1].DimensionKeyLevel);
                                if (l != null && l2 != null)
                                {
                                    int card = l.Cardinality;
                                    int card2 = l2.Cardinality;
                                    if (card2 > card || (card == card2 && match2[1].DimensionName.CompareTo(match[1].DimensionName) > 0))
                                    {
                                        found = GENERATE_SIMPLE_HIERARCHIES;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!found)
                    folds.Add(match);
                else
                {
                    HierarchyLevel[] dep = new HierarchyLevel[] { new HierarchyLevel(match[0].Name,match[0].DimensionKeyLevel), 
                                new HierarchyLevel(match[1].Name, match[1].DimensionKeyLevel)};
                    dependencies.Add(dep);
                }
            }
            // Progressively fold hierarchies together
            Dictionary<Hierarchy, Hierarchy> foldedHierarchies = new Dictionary<Hierarchy, Hierarchy>();
            for (int i = 0; i < folds.Count; i++)
            {
                Hierarchy deph = folds[i][1];
                Hierarchy keyh = folds[i][0];
                Level origKeyLevel = examinedLevels[keyh];
                /*
                 * If a key hierarchy hasn't been folded into another, and another hierarchy is dependent on it,
                 * then based on the scanning, the other hierarchies dimension key level is dependent on this one's.
                 * If that is the case. Proceed one, by one, through each single-level hierarchy and try to fold it
                 * into another hierarchy.
                 */
                if (foldedHierarchies.ContainsKey(keyh))
                    keyh = foldedHierarchies[keyh];
                Level keyLevel = examinedLevels[keyh];
                Level depLevel = examinedLevels[deph];
                if (keyLevel == depLevel)
                    continue;
                List<Level> ancestors = new List<Level>();
                keyh.findAncestors(keyLevel.Name, ancestors);
                if (ancestors.Count == 0)
                {
                    bool result = foldHierarchy(maf, deph, keyh, folds);
                    if (result)
                    {
                        bool hasMatch = false;
                        if (maf.builderVersion >= 2)
                        {
                            /*
                             * Check for key being the same
                             */
                            foreach (LevelKey keylk in keyLevel.Keys)
                            {
                                if (keylk.SurrogateKey)
                                    continue;
                                foreach (LevelKey deplk in depLevel.Keys)
                                {
                                    if (deplk.SurrogateKey)
                                        continue;
                                    bool same = keylk.ColumnNames.Length == deplk.ColumnNames.Length;
                                    if (same)
                                        foreach (string s in keylk.ColumnNames)
                                        {
                                            bool found = false;
                                            foreach (string s2 in deplk.ColumnNames)
                                            {
                                                if (s == s2)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (!found)
                                            {
                                                same = false;
                                                break;
                                            }
                                        }
                                    if (same)
                                    {
                                        hasMatch = true;
                                        break;
                                    }
                                }
                                if (hasMatch)
                                    break;
                            }
                        }
                        List<Level> depAncestors = new List<Level>();
                        if (!hasMatch)
                        {
                            keyh.Children = deph.Children;
                            depLevel.Children = new Level[] { keyLevel };
                        }
                        if (GENERATE_ONLY_BOTTOM_LEVEL_TABLES)
                            clearLevel(depLevel);
                        clearColumnsAtLowerLevels(depLevel);
                        foldedHierarchies.Add(deph, keyh);
                        maf.hmodule.replaceHierarchy(keyh);
                    }
                    else
                    {
                        HierarchyLevel[] dep = new HierarchyLevel[] { new HierarchyLevel(folds[i][0].Name,folds[i][0].DimensionKeyLevel), 
                                new HierarchyLevel(folds[i][1].Name, folds[i][1].DimensionKeyLevel)};
                        dependencies.Add(dep);
                    }
                }
                else
                {
                    bool result = foldHierarchy(maf, deph, keyh, folds);
                    if (result)
                    {
                        List<Level> parentList = new List<Level>();
                        keyh.findParents(origKeyLevel.Name, parentList);
                        if (parentList.Count > 0)
                        {
                            /*
                             * Find the parent that is dependent on this dependent level (i.e. 
                             * where this level should fall below)
                             */
                            Level parent = null;
                            foreach (Level l in parentList)
                            {
                                // Walk up and see if there's a dependent level, if not, add a new shared child
                                foreach (Hierarchy[] fold in folds)
                                {
                                    if (fold[1].Name == keyh.Name && fold[1].DimensionKeyLevel == l.Name &&
                                        fold[0].Name == deph.Name && fold[0].DimensionKeyLevel == depLevel.Name)
                                    {
                                        parent = l;
                                        break;
                                    }
                                }
                                if (parent != null)
                                    break;
                            }
                            if (parent == null)
                            {
                                // No parent found, put at top level in hierarchy
                                List<Level> llist = new List<Level>(keyh.Children);
                                Boolean found = false;
                                foreach (Level l in llist)
                                {
                                    if (l.Name == depLevel.Name)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                    
                                if (!found)
                                    llist.Add(depLevel);
                                keyh.Children = llist.ToArray();
                                depLevel.Children = new Level[] { };
                                depLevel.SharedChildren = new string[] { origKeyLevel.Name };
                                if (GENERATE_ONLY_BOTTOM_LEVEL_TABLES)
                                    clearLevel(depLevel);
                            }
                            else
                            {
                                // Parent found, insert below parent
                                for (int j = 0; j < parent.Children.Length; j++)
                                {
                                    if (parent.Children[j].Name == origKeyLevel.Name)
                                    {
                                        parent.Children[j] = depLevel;
                                        depLevel.Children = new Level[] { origKeyLevel };
                                        if (GENERATE_ONLY_BOTTOM_LEVEL_TABLES)
                                            clearLevel(depLevel);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Currently no levels other than dimension key - insert above
                            for (int j = 0; j < keyh.Children.Length; j++)
                            {
                                if (keyh.Children[j].Name == origKeyLevel.Name)
                                {
                                    keyh.Children[j] = depLevel;
                                    depLevel.Children = new Level[] { origKeyLevel };
                                    if (GENERATE_ONLY_BOTTOM_LEVEL_TABLES)
                                        clearLevel(depLevel);
                                }
                            }
                        }
                        clearColumnsAtLowerLevels(depLevel);
                        foldedHierarchies.Add(deph, keyh);
                        maf.hmodule.replaceHierarchy(keyh);
                    }
                    else
                    {
                        HierarchyLevel[] dep = new HierarchyLevel[] { new HierarchyLevel(folds[i][0].Name,folds[i][0].DimensionKeyLevel), 
                                new HierarchyLevel(folds[i][1].Name, folds[i][1].DimensionKeyLevel)};
                        dependencies.Add(dep);
                    }
                }
            }
            if (dependencies.Count > 0)
            {
                // Rename dependencies based on folds
                foreach (Hierarchy deph in foldedHierarchies.Keys)
                {
                    Hierarchy newh = foldedHierarchies[deph];
                    foreach (HierarchyLevel[] dep in dependencies)
                    {
                        if (dep[0].HierarchyName == deph.Name)
                            dep[0].HierarchyName = newh.Name;
                        if (dep[1].HierarchyName == deph.Name)
                            dep[1].HierarchyName = newh.Name;
                    }
                }
                maf.dependencies = dependencies;
            }
        }

        /*
         * Make sure any columns that are selected at this level, are
         * not selected at lower levels
         */
        private static void clearColumnsAtLowerLevels(Level l)
        {
            foreach (string s in l.ColumnNames)
            {
                foreach (Level cl in l.Children)
                {
                    Level fl = cl.findLevelWithColumn(s);
                    if (fl != null)
                    {
                        List<string> colList = new List<string>(fl.ColumnNames);
                        colList.Remove(s);
                        fl.ColumnNames = colList.ToArray();
                    }
                }
            }
        }

        private static void getMatches(MainAdminForm maf, Space sp, List<Hierarchy[]> matches, Dictionary<Hierarchy, Level> examinedLevels, ScanBuffer scanBuffer, bool isUpdatingLogicalModel)
        {
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                if (isUpdatingLogicalModel && h.ModelWizard == false)
                    continue;

                if (h.DimensionName == maf.timeDefinition.Name)
                    continue;
                if (h.ProcessedDependencies)
                    continue;
                h.ProcessedDependencies = true;
                // Find staging tables at the dimension key level
                List<StagingTable> stlist = new List<StagingTable>();
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    foreach (string[] l in st.Levels)
                    {
                        if (l[0] == h.DimensionName && l[1] == h.DimensionKeyLevel)
                        {
                            stlist.Add(st);
                            break;
                        }
                    }
                }
                // Mark that this level has been examined
                Level dklevel = h.findLevelContainingDimensionKey();
                if (examinedLevels != null && !examinedLevels.ContainsKey(h))
                    examinedLevels.Add(h, dklevel);
                Dictionary<Hierarchy, StagingTable> foldList = new Dictionary<Hierarchy, StagingTable>();
                // See if any of these contain the dimension key of any other dimension
                foreach (StagingTable st in stlist)
                {
                    if (h.SourceGroups != null && (st.SourceGroups == null || h.SourceGroups != st.SourceGroups))
                        continue;
                    foreach (string[] l in st.Levels)
                    {
                        if (l[0] != h.Name && l[0] != maf.timeDefinition.Name)
                        {
                            foreach (Hierarchy sh in maf.hmodule.getHierarchies())
                            {
                                if (l[0] == sh.DimensionName
                                    && l[1] == sh.DimensionKeyLevel)
                                {
                                    if (!foldList.ContainsKey(sh))
                                        foldList.Add(sh, st);
                                }
                            }
                        }
                    }
                }
                // Check each possibility for dependence
                if (foldList.Count > 0)
                {
                    foreach (Hierarchy fh in foldList.Keys)
                    {
                        if (isUpdatingLogicalModel && fh.ModelWizard == false)
                            continue;

                        StagingTable st = foldList[fh];
                        Level keyLevel = h.findLevelContainingDimensionKey();
                        LevelKey key = null;
                        foreach (LevelKey sk in keyLevel.Keys)
                            if (!sk.SurrogateKey)
                            {
                                key = sk;
                                break;
                            }
                        Level depLevel = fh.findLevelContainingDimensionKey();
                        LevelKey dep = null;
                        foreach (LevelKey sk in depLevel.Keys)
                            if (!sk.SurrogateKey)
                            {
                                dep = sk;
                                break;
                            }
                        if (key != null && dep != null)
                        {
                            if (isDependent(maf, sp.Directory + "\\data", st, key, dep, scanBuffer))
                            {
                                matches.Add(new Hierarchy[] { h, fh });
                            }
                        }
                    }
                }
            }
        }

        private static void clearLevel(Level l)
        {
            l.GenerateDimensionTable = false;
            if (l.Keys[0].SurrogateKey)
            {
                List<LevelKey> lklist = new List<LevelKey>();
                foreach (LevelKey lk in l.Keys)
                    if (!lk.SurrogateKey)
                        lklist.Add(lk);
                l.Keys = lklist.ToArray();
            }
        }

        private static Dictionary<StagingColumn, StagingTable> getStagingColumns(MainAdminForm maf, string dimName)
        {
            Dictionary<StagingColumn, StagingTable> sclist = new Dictionary<StagingColumn, StagingTable>();
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                bool match = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == dimName)
                    {
                        match = true;
                        break;
                    }
                }
                if (match)
                {
                    foreach (StagingColumn sc in st.Columns)
                        if (Array.IndexOf<string>(sc.TargetTypes, dimName) >= 0)
                            sclist.Add(sc, st);
                }
            }
            return (sclist);
        }

        /*
         * Folds one hierarchy into another with the dimension key as a parent level
         * to an existing hierarchy dimension key (a new level is created in the
         * target hierarchy)
         */
        private static bool foldHierarchy(MainAdminForm maf, Hierarchy oldh, Hierarchy newh, List<Hierarchy[]> folds)
        {
            if (oldh.SourceGroups != null)
            {
                if (newh.SourceGroups == null)
                    return false;
                string[] sg = oldh.SourceGroups.Split(new char[] { ',' });
                string[] sg2 = newh.SourceGroups.Split(new char[] { ',' });
                bool found = false;
                foreach (string s in sg)
                {
                    foreach (string s2 in sg2)
                    {
                        if (s == s2)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        break;
                }
                if (!found)
                    return false;
            }
            /*
             * First, check to see if the new hierarchy has columns with the same name. If this is the case
             * rename them to be explicit.
             * It is likely that there isn't a strong dependence relationship (inferred through
             * something trivial like every order has a customer).
             */
            Dictionary<StagingColumn, StagingTable> newhsclist = getStagingColumns(maf, newh.Name);
            Dictionary<StagingColumn, StagingTable> oldhsclist = getStagingColumns(maf, oldh.Name);
            foreach (StagingColumn sc in oldhsclist.Keys)
            {
                foreach (StagingColumn sc2 in newhsclist.Keys)
                {
                    if (sc == sc2)
                        continue;
                    if (!sc.NaturalKey && !sc2.NaturalKey && sc.Name == sc2.Name)
                    {
                        //return (false);
                        oldhsclist[sc].renameColumn(sc.Name, oldh.Name + " " + sc.Name);
                        newhsclist[sc2].renameColumn(sc2.Name, newh.Name + " " + sc2.Name);
                    }
                }
            }
            // First, remove the old hierarchy
            maf.hmodule.removeHierarchy(oldh);
            // save new hierarchy
            maf.hmodule.updateHierarchy(newh);

            // Delete the old dimension
            Dimension dim = null;
            foreach (Dimension sdim in maf.dimensionsList)
            {
                if (sdim.Name == oldh.Name)
                {
                    dim = sdim;
                    break;
                }
            }
            maf.dimensionsList.Remove(dim);
            maf.renameDimension(oldh.Name, newh.Name, false);


            // Rename targets and grains
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                /*
                 * Delete grain level because it is being folded into new hierarchy
                 * and it's dimension key level is lower
                 */
                List<string[]> llist = new List<string[]>();
                bool oneFound = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] != oldh.Name)
                        llist.Add(level);
                    else
                    {
                        oneFound = true;
                        bool found = false;
                        foreach (string[] level2 in st.Levels)
                        {
                            if (level2[0] == newh.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            llist.Add(level);
                            level[0] = newh.Name;
                        }
                    }
                }
                if (oneFound)
                {
                    st.Levels = llist.ToArray();
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null)
                            for (int i = 0; i < sc.TargetTypes.Length; i++)
                            {
                                if (sc.TargetTypes[i] == oldh.Name)
                                    sc.TargetTypes[i] = newh.Name;
                            }
                    }
                    maf.stagingTableMod.updateStagingTable(st, "");
                }
            }
            return (true);
        }

        private static int MIN_KEYS = 3;
        private static int MIN_LINES = 5;

        /*
         * Check whether one level key is functionally dependent on another (i.e. if you know one key
         * then you automatically know the other - e.g. if you know the order detail record ID, you
         * know the order id) 
         */
        private static bool isDependent(MainAdminForm maf, string directory, StagingTable st, LevelKey key, LevelKey dep, ScanBuffer scanBuff)
        {
            /*
             * Check trivial case that dep is subset of key
             */
            int matches = 0;
            foreach (string s in dep.ColumnNames)
                if (Array.IndexOf<string>(key.ColumnNames, s) >= 0)
                    matches++;
            if (matches == dep.ColumnNames.Length)
                return (true);
            SourceFile sf = maf.getSourceFile(st);
            List<string[]> buff = scanBuff.getBuffer(sf, directory);
            if (buff == null)
                return (false);
            List<int> keyColIndices = new List<int>();
            List<int> depColIndices = new List<int>();
            foreach (StagingColumn sc in st.Columns)
            {
                bool isKey = false;
                bool isDep = false;
                bool lookup = false;
                for (int i = 0; i < key.ColumnNames.Length; i++)
                    if (sc.Name == key.ColumnNames[i])
                    {
                        lookup = true;
                        isKey = true;
                        break;
                    }
                for (int i = 0; i < dep.ColumnNames.Length; i++)
                    if (sc.Name == dep.ColumnNames[i])
                    {
                        lookup = true;
                        isDep = true;
                        break;
                    }
                if (lookup)
                    for (int i = 0; i < sf.Columns.Length; i++)
                    {
                        if (sf.Columns[i].Name == sc.SourceFileColumn)
                        {
                            if (isKey)
                                keyColIndices.Add(i);
                            if (isDep)
                                depColIndices.Add(i);
                            break;
                        }
                    }
            }
            if (keyColIndices.Count < key.ColumnNames.Length || depColIndices.Count < dep.ColumnNames.Length)
                return (false);
            Dictionary<string, string> testMap = new Dictionary<string, string>();
            foreach (string[] cols in buff)
            {
                string keystr = "";
                foreach (int index in keyColIndices)
                    keystr += cols[index];
                string depstr = "";
                foreach (int index in depColIndices)
                    depstr += cols[index];
                if (testMap.ContainsKey(keystr))
                {
                    if (testMap[keystr] != depstr)
                        return (false);
                }
                else
                    testMap.Add(keystr, depstr);
            }
            /*
             * Apply some heuristics to make sure we don't use any pathologically 
             * small files to infer relationships
             */
            return (buff.Count > MIN_LINES && testMap.Keys.Count > MIN_KEYS);
        }

        private void checkLoadStatusAndAddRecords(MainAdminForm maf, Space sp, string mainSchema, QueryConnection conn, Status.StatusResult sr, int loadNumber, string loadGroup)
        {
            if (sr.code == Status.StatusCode.Complete)
            {
                updateLoadSuccess(maf, sp, mainSchema, conn, sr, loadNumber, loadGroup);
            }
            else if (sr.code == Status.StatusCode.Failed)
            {
                Global.systemLog.Error("Load failed, space: " + sp.ToString());
            }
        }

        private void updateDependencies(MainAdminForm maf, Space sp)
        {
            List<Hierarchy[]> matches = new List<Hierarchy[]>();
            Dictionary<Hierarchy, Level> examinedLevels = new Dictionary<Hierarchy, Level>();
            /*
             * Find hierarchy dependency matches. "matches" contains each set of dependent hierarchies.
             * The first element is the key hierarchy, and the second is the hierarchy that is dependent
             * on the first. For example, in a master-detail situation where there is an Order hierarchy
             * and an Order Detail hierarchy, since every Order Detail is tied to an order, Order is
             * dependent on Order detail. As a result there would be a match [Order Detail, Order]
             */
            getMatches(maf, sp, matches, null, scanBuff, false);
            foreach (Hierarchy[] match in matches)
            {
                HierarchyLevel[] dep = new HierarchyLevel[] { new HierarchyLevel(match[0].Name,match[0].DimensionKeyLevel), 
                                new HierarchyLevel(match[1].Name, match[1].DimensionKeyLevel)};
                if (maf.dependencies == null)
                    maf.dependencies = new List<HierarchyLevel[]>();
                maf.dependencies.Add(dep);
            }
        }

        public static bool updateLoadSuccess(MainAdminForm maf, Space sp, string mainSchema, QueryConnection conn, Status.StatusResult status, int loadNumber, string loadGroup)
        {
            try
            {
                /* 
                 * Make sure publish is complete - even after loadwarehouse is done, the repository
                 * needs to be copied to it's production version
                 */
                int count = 0;
                while (File.Exists(sp.Directory + "\\publish.lock") && count++ < 40)
                    Thread.Sleep(500);
                // Mark the start of updating
                try
                {
                    FileStream fs = File.Open(sp.Directory + "\\update.lock", FileMode.CreateNew);
                    fs.Close();
                }
                catch (IOException)
                {
                    // If more than 2 minutes old, assume death and reap
                    FileInfo f = new FileInfo(sp.Directory + "\\update.lock");
                    if (DateTime.Now.Subtract(f.CreationTime).TotalSeconds > 120)
                    {
                        File.Delete(sp.Directory + "\\update.lock");
                        FileStream fs = File.Open(sp.Directory + "\\update.lock", FileMode.CreateNew);
                        fs.Close();
                    }
                    else
                    {
                        Global.systemLog.Error("Unable to open update lock on space " + sp.ID.ToString() + ", not updating load success");
                        return false;
                    }
                }
                try
                {
                    status.code = Status.StatusCode.Ready;
                    int newLoad = Status.getMaxSuccessfulLoadID(sp, loadGroup);
                    // If already updated return
                    if (newLoad <= loadNumber)
                    {
                        Global.systemLog.Info("Not updating load number for space " + sp.ID.ToString() + " - max successful load: " + newLoad + ", current number: " + loadNumber);
                        return true;
                    }
                    if (loadGroup == Util.LOAD_GROUP_NAME || loadGroup.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                        sp.LoadNumber = newLoad;
                    Variable dateVar = Util.getDateVariable(maf);
                    if (dateVar == null)
                    {
                        Global.systemLog.Warn("Problem with space " + sp.ID + '/' + sp.Name + ", no or invalid LoadDate variable, not updating load success");
                        return false;
                    }
                    DateTime loadDate;
                    Performance_Optimizer_Administration.Util.tryDateTimeParse(dateVar.Query, out loadDate);
                    List<Publish> plist = Database.getPublishes(conn, mainSchema, sp.ID);
                    int maxRecordedPublish = -1;
                    foreach (Publish p in plist)
                    {
                        if (p.LoadGroup == loadGroup && p.LoadNumber > maxRecordedPublish)
                            maxRecordedPublish = p.LoadNumber;
                    }
                    if (maxRecordedPublish < newLoad)
                    {
                        Global.systemLog.Info("Updating publish table for space " + sp.ID);
                        updatePublishes(sp, maf, mainSchema, conn, loadGroup, newLoad, loadDate);
                    }
                    Global.systemLog.Info("Updating space " + sp.ID + " with load " + sp.LoadNumber);
                    bool result = Database.updateSpaceLoadNumber(conn, mainSchema, sp, sp.LoadNumber);
                    Global.systemLog.Info("Space " + sp.ID + " updated: " + result);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error during updating of session status", ex);
                    return false;
                }
                finally
                {
                    try
                    {
                        File.Delete(sp.Directory + "\\update.lock");
                    }
                    catch (IOException ex)
                    {
                        Global.systemLog.Error("Unable to delete update lock on space " + sp.ID.ToString(), ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error during updating of session status", ex);
                return false;
            }
            return true;
        }

        public static void updatePublishes(Space sp, MainAdminForm maf, string mainSchema, QueryConnection conn, string loadGroup, int loadNumber, DateTime loadDate)
        {
            DateTime startPublishDate = Status.getPublishStartTime(sp, loadNumber, loadGroup);//Util.getProcessInitiatedTime(sp, maf);
            DateTime endPublishDate = Status.getPublishEndTime(sp, loadNumber, loadGroup); //TimeZoneUtils.getProcessingTimeZoneAdjustedDateTime(sp, maf, DateTime.Now);
            if (endPublishDate == DateTime.MinValue)
            {                
                endPublishDate = DateTime.Now;
            }

            // The two Proces start times are because of backward compatibility. Previos LoadInitiatedTime was in a different 
            // format that ProcessStartTime which is standard ISO format. To prevent from existing Reports/ETL scripts from breaking, introduced a new variable
            Util.setProcessInitiatedTime(sp, maf, startPublishDate);
            Util.setProcessStartTime(sp, maf, startPublishDate);
            Util.setProcessCompletionTimeVariable(sp, maf, endPublishDate);
            DataTable prows = Status.getPublishedRows(sp, loadNumber);
            bool isPublishAdded = false;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.LoadGroups == null || Array.IndexOf<string>(st.LoadGroups, loadGroup) < 0)
                    continue;
                // Don't update non-targeted sources. For discovery source go ahead.
                if (!st.DiscoveryTable
                    && (st.Levels == null || st.Levels.Length == 0 || (st.Levels.Length == 1 && st.Levels[0][0] == maf.timeDefinition.Name)))
                {
                    continue;
                }
                foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                {
                    if (st.SourceFile.ToLower() == sf.FileName.ToLower())
                    {
                        DataRow[] rows = prows.Select("TableName='" + loadGroup + ": " + st.Name + "'");
                        long numRows = rows.Length == 0 ? 0 : (long)rows[0][1];
                        if (numRows == 0)
                        {
                            // meaning the data source was not used. In this case, we should skip the calculation altogether
                            continue;
                        }
                        string sfFileName = sp.Directory + "\\data\\" + sf.FileName;
                        FileInfo fi = new FileInfo(sfFileName);
                        long dataSize = 0L;
                        if(!fi.Exists && !Util.isScriptedSource(st))
                        {
                            // could happen for scripted source
                            Global.systemLog.Warn("File Not Found. Skipping the population of datasize in publish table for " + sfFileName); 
                            continue;
                        }

                        if (fi.Exists)
                        {
                            dataSize = fi.Length;
                        }
                        if (st.TruncateOnLoad)
                        {
                            // Reduce quota by amount previously loaded                        
                            List<Publish> plist = Database.getTablePublishes(conn, mainSchema, sp.ID, st.Name);
                            if (plist != null)
                            {
                                int curRows = 0;
                                long curData = 0;
                                foreach (Publish p in plist)
                                {
                                    curRows += p.NumRows;
                                    curData += p.DataSize;
                                }
                                if (curRows > 0 || curData > 0)
                                {
                                    Database.addPublish(conn, mainSchema, sp.ID, loadNumber, -curRows, -curData, loadDate, endPublishDate, st.Name, loadGroup, startPublishDate);
                                    // bugfix 7876. Adding the current load information -- numRows and dataSize before inserting 
                                    // publish record. Otherwise, the result of total number of records will be keep on 
                                    //flipping on each even load numbers
                                    Database.addPublish(conn, mainSchema, sp.ID, loadNumber, (int)numRows, dataSize, loadDate, endPublishDate, st.Name, loadGroup, startPublishDate);
                                    isPublishAdded = true;
                                    break;
                                }
                            }
                        }
                        Database.addPublish(conn, mainSchema, sp.ID, loadNumber, (int)numRows, dataSize, loadDate, endPublishDate, st.Name, loadGroup, startPublishDate);
                        isPublishAdded = true;
                        break;
                    }
                }
                if (!isPublishAdded && maf.IsPartitioned && loadNumber == 1)
                {
                    string[] partitionConnections = maf.getPartitionConnectionNames("Default Connection");
                    if (partitionConnections != null && partitionConnections.Length > 0)
                    {
                        //add dummy entry in publish table for enabling designer/dashboard
                        Database.addPublish(conn, mainSchema, sp.ID, loadNumber, 0, 0L, loadDate, endPublishDate, "Dummy", loadGroup, startPublishDate);
                    }
                }
            }
        }

        private string buildTaskParameters(string enginecmd, string processArguments, int loadNumber, string loadGroup)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("engine.cmd=");
            stringBuilder.Append(enginecmd);
            stringBuilder.Append("&process.arguments=");
            stringBuilder.Append(processArguments);
            stringBuilder.Append("&load.number=");
            stringBuilder.Append(loadNumber);
            stringBuilder.Append("&load.group=");
            stringBuilder.Append(loadGroup);
            return stringBuilder.ToString();
        }

        public static BackgroundModule getBackgroundModule(string spaceID)
        {
            MonitorLoad result = null;
            lock (backgroundProcess)
            {
                if (backgroundProcess.ContainsKey(spaceID))
                    result = backgroundProcess[spaceID];
            }
            return result;
        }

        public static void clearBackgroundModule(string spaceID)
        {
            lock (backgroundProcess)
            {
                if (backgroundProcess.ContainsKey(spaceID))
                    backgroundProcess.Remove(spaceID);
            }
        }

        public static MainAdminForm createTimeMAF()
        {
            MainAdminForm tmaf = new MainAdminForm();
            tmaf.setHeadless(true);
            tmaf.setLogger(Global.systemLog);
            tmaf.setupTree();
            tmaf.setupBlankRepository();
            tmaf.timeDefinition = new TimeDefinition();
            tmaf.timeDefinition.Day = true;
            tmaf.timeDefinition.Week = true;
            tmaf.timeDefinition.Month = true;
            tmaf.timeDefinition.Quarter = true;
            tmaf.timeDefinition.Halfyear = false;
            tmaf.timeDefinition.Year = true;
            tmaf.timeDefinition.GenerateTimeDimension = true;
            tmaf.timeDefinition.Periods = new TimePeriod[7];
            tmaf.timeDefinition.Periods[0] = new TimePeriod("TTM", TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 12, 0, 0, false);
            tmaf.timeDefinition.Periods[1] = new TimePeriod("T3M", TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 3, 0, 0, false);
            tmaf.timeDefinition.Periods[2] = new TimePeriod("MAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_MONTH, true);
            tmaf.timeDefinition.Periods[3] = new TimePeriod("QAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_QUARTER, true);
            tmaf.timeDefinition.Periods[4] = new TimePeriod("YAGO", TimePeriod.AGG_TYPE_NONE, 0, 0, 1, TimePeriod.PERIOD_YEAR, true);
            tmaf.timeDefinition.Periods[5] = new TimePeriod("QTD", TimePeriod.AGG_TYPE_TO_DATE, TimePeriod.PERIOD_QUARTER, 1, 0, 0, false);
            tmaf.timeDefinition.Periods[6] = new TimePeriod("YTD", TimePeriod.AGG_TYPE_TO_DATE, TimePeriod.PERIOD_YEAR, 1, 0, 0, false);
            return tmaf;
        }
    }
}
