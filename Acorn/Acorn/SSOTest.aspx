﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSOTest.aspx.cs" Inherits="Acorn.SSOTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Copyright (C) 2009-2012 Birst, Inc. All rights reserved.
BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms. 
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP.NET Birst SSO Test Page</title>
</head>
<body>
    <h1>ASP.NET Birst SSO Test Page</h1>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>
                Login Service Base URL:
            </td>
            <td>
                <asp:TextBox ID="baseUrl" MaxLength="64" Width="300" runat="server" />
                <asp:RegularExpressionValidator id="BaseUrlValidator" runat="server" 
                    ControlToValidate="baseUrl" 
                    ValidationExpression="^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$" 
                    ErrorMessage="Invalid URL" />
            </td>
            <td>
                For production this would be https://login.bws.birst.com
            </td>
        </tr>
        <tr>
            <td>
                Birst user name:
            </td>
            <td>
                <asp:TextBox ID="username" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Space Id:
            </td>
            <td>
                <asp:TextBox ID="BirstSpaceId" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                This space must be enabled for SSO and the Birst user must have access to it
            </td>
        </tr>        
        <tr>
            <td>
                Space name:
            </td>
            <td>
                <asp:TextBox ID="spacename" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
            Deprecated (users can have access to multiple spaces with the same name), please use Space Id (BirstSpaceId)
            </td>
        </tr>
        <tr>
            <td>
                SSO password:
            </td>
            <td>
                <asp:TextBox ID="ssopassword" MaxLength="128" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                The SSO password is space specific (this is not your account password). You need
                to enable your space for SSO access and then request an SSO password (you will receive
                it via email).
            </td>
        </tr>
        <tr>
            <td>
                Module:
            </td>
            <td>
                <asp:DropDownList ID="module" runat="server">
                    <asp:ListItem Value="designer" />
                    <asp:ListItem Value="export" />
                    <asp:ListItem Value="dashboard" />
                    <asp:ListItem Value="customupload" />
                </asp:DropDownList>           
            </td>
            <td>
                designer, dashboard, export, customupload
            </td>
        </tr>
        <tr>
            <td>
                Dashboard name:
            </td>
            <td>
                <asp:TextBox ID="dashboardName2" MaxLength="128" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Dashboard name.
            </td>            
        </tr>
        <tr>
            <td>
                Dashboard page:
            </td>
            <td>
                <asp:TextBox ID="pageName" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Page name.
            </td>            
        </tr>
        <tr>
            <td>
                Open For Edit:
            </td>
            <td>
                <asp:CheckBox ID="openForEdit" runat="server" />
            </td>
            <td>
                Open dashboard page for editing.
            </td>            
        </tr>
        <tr>
            <td>
                Report name:
            </td>
            <td>
                <asp:TextBox ID="reportName" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Report name.
            </td>            
        </tr>   
        <asp:Panel ID="designerOptionsPanel" Visible="false" runat="server">  
        <tr>
            <td>
                Designer options:
            </td>
            <td>
                <asp:TextBox ID="designerOptions" MaxLength="64" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Designer options
            </td>            
        </tr> 
        </asp:Panel>
        <asp:TextBox ID="dashboardname" MaxLength="64" Width="300" runat="server" Visible="false"></asp:TextBox>                           
        <tr>
            <td>
                Extra parameters (optional):
            </td>
            <td>
                <asp:TextBox ID="extraParameters" MaxLength="2048" Width="300" runat="server" />
            </td>
            <td>
                This is for dashboard parameters, must start with &amp; and the names and values
                must be properly URL encoded (e.g., &amp;Time.Year=1998&amp;Account.Name=ACME%20Labs)
                .&nbsp; See <a href="http://en.wikipedia.org/wiki/Query_string" target="_blank">http://en.wikipedia.org/wiki/Query_string</a></td>
        </tr>
        <tr>
            <td>
                Security session variable (optional):
            </td>
            <td>
                <asp:TextBox ID="sessionVars" Width="300" MaxLength="2048" runat="server"></asp:TextBox><br />
            </td>
        </tr>
        <tr>
            <td>
                Embedded:
            </td>
            <td>
                <asp:CheckBox ID="embedded" runat="server" />
            </td>
            <td>
                Set up the page for embedding within another page (via IFRAME or IMG).  This hides the top level navigation bar. For this test page, it will also render the result in an IFRAME or IMG.
            </td>
        </tr>
        <tr>
            <td>
                Display in an image tag:
            </td>
            <td>
                <asp:CheckBox ID="imageTag" runat="server" />
            </td>
            <td>
                Must also check 'Embedded'. Instead of an IFRAME, use an IMG tag.
            </td>
        </tr>
        <tr>
            <td>
                Export Type:
            </td>
            <td>
                <asp:DropDownList ID="exportTypeList" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="png" />
                    <asp:ListItem Value="pdf" />
                    <asp:ListItem Value="csv" />
                    <asp:ListItem Value="xls" />
                    <asp:ListItem Value="ppt" />
                </asp:DropDownList> 
            </td> 
            <td>
                Export Type (png, pdf, etc.)
            </td>
        </tr>
        <tr>
            <td>
                Zoom:
            </td>
            <td>
               <asp:TextBox ID="imageZoom" Width="300" MaxLength="16" runat="server" Value="2"></asp:TextBox>
            </td>
            <td>
                Image Zoom (defaults to 2.0)
            </td>
        </tr>
        <tr>
            <td>
                Width
            </td>
            <td>
               <asp:TextBox ID="Width" Width="300" MaxLength="16" runat="server" Value="1024"></asp:TextBox>
            </td>
            <td>
                Width of embedded item (IFRAME or IMG)
            </td>
        </tr>
        <tr>
            <td>
                Height
            </td>
            <td>
               <asp:TextBox ID="Height" Width="300" MaxLength="16" runat="server" Value="400"></asp:TextBox>
            </td>
            <td>
                Height of embedded item (IFRAME or IMG)
            </td>
        </tr>
        <tr>
            <td>
                Hide dashboard navigation:
            </td>
            <td>
                <asp:CheckBox ID="hideDashboardNavigation" runat="server" />
            </td>
            <td>
                This hides the dashboard navigation tabs
            </td>
        </tr>
        <tr>
            <td>
                Hide dashboard prompts:
            </td>
            <td>
                <asp:CheckBox ID="hideDashboardPrompts" runat="server" />
            </td>
            <td>
                This hides the dashboard prompts
            </td>
        </tr>    
        <tr>
            <td>
                View Mode:
            </td>
            <td>
                <asp:DropDownList ID="viewMode" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="borderless" />
                    <asp:ListItem Value="full" />
                    <asp:ListItem Value="headerless" />
                </asp:DropDownList>
            </td>
            <td>
                Set the dashboard view mode (borderless, full, headerless)
            </td>
        </tr>    
        <tr>
            <td>
                wmode:
            </td>
            <td>
                <asp:DropDownList ID="wmode" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="window" />
                    <asp:ListItem Value="opaque" />
                    <asp:ListItem Value="transparent" />
                    <asp:ListItem Value="direct" />  
                    <asp:ListItem Value="gpu" />                                                          
                </asp:DropDownList>
            </td>
            <td>
                Set the Adobe Flash wmode parameter (if you don't know what this is, don't set it)
            </td>
        </tr>   
        <tr>
            <td>
                filterLayout:
            </td>
            <td>
                <asp:DropDownList ID="filterLayout" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="left" />
                    <asp:ListItem Value="top" />                                                          
                </asp:DropDownList>
            </td>
            <td>
                Set the filter layout mode - top is classic, left is new
            </td>
        </tr>                           
        <tr>
            <td>Sleep time (msecs): </td>
            <td><asp:TextBox ID="Sleep" MaxLength="16" Width="300" runat="server"/>
             <asp:RangeValidator id="Range1"
                ControlToValidate="Sleep"
                MinimumValue="0"
                MaximumValue="100000"
                Type="Integer"
                Text="Must be a positive integer"
                runat="server"/>
            </td>
            <td>For debugging, setting to 30001 should trigger token end of life check</td>
        </tr>
        <tr>
            <td>
                Help URL:
            </td>
            <td>
                <asp:TextBox ID="HelpURLID" MaxLength="256" Width="300" runat="server"></asp:TextBox>
                <asp:CheckBox ID="HelpURLIncludeEmptyID" runat="server" />
            </td>
            <td>
                Help URL (checking in the box will force 'birst.helpURL=' even if the field is empty (to disable help))
            </td>            
        </tr>
        <tr>
            <td>Test 'Used': </td>
            <td><asp:CheckBox ID="TestUsed" runat="server" /></td>
            <td>For debugging, checking this will mark the token as used</td>
        </tr>     
    </table>
    <p />
    <asp:Button ID="Submit" runat="server" Text="Submit SSO request" OnClick="SSOTest_Submit" />
    </form>
    <div class="copyright">
        <asp:Literal ID="Copyright" runat="server"></asp:Literal>
        &nbsp;
        <asp:Literal ID="Version" runat="server"></asp:Literal>
    </div>
</body>
</html>
