﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Acorn.DBConnection;
using Acorn.Background;
using System.Net;
using System.Net.Mail;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Threading;

namespace Acorn
{
    public partial class UserSignup : System.Web.UI.Page
    {
        private static string discoveryTrialProductIDs = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialProductIDs"];
        private static string discoveryTrialDaysString = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialDays"];
        private static string discoveryTrialReleaseTypeString = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialReleaseType"];
        private static string discoveryTrialAccountAdmin = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialAccountAdmin"];
        private static string discoveryTrialCreationErrorEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialCreationErrorEmail"];
        public static string discoveryTrialVerificationFromEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["DiscoveryTrialVerficationFromEmail"];
        private static string netsuiteDiscoveryTrialProductIDs = System.Web.Configuration.WebConfigurationManager.AppSettings["NetsuiteDiscoveryTrialProductIDs"];
        private static string netsuiteDiscoveryTrialDaysString = System.Web.Configuration.WebConfigurationManager.AppSettings["NetsuiteDiscoveryTrialDays"];
        private static string netsuiteDiscoveryTrialReleaseTypeString = System.Web.Configuration.WebConfigurationManager.AppSettings["NetsuiteDiscoveryTrialReleaseType"];     

        private static int DEFAULT_DISCOVERY_TRIAL_DAYS = 30;

        private const string ACTION_KEY = "actionkey";
        private const string ACTION_KEY_VALIDATE_USER = "validateuser";
        private const string ACTION_KEY_VALIDATE_PWD = "validatepassword";
        private const string ACTION_KEY_CREATE_DISCOVER_TRIAL_USER = "createtrialuser";
        private const string ACTION_PARAM_UNAME = "username";
        private const string ACTION_PARAM_PWD = "password";
        private const string ACTION_KEY_CREATE_DISCOVER_NETSUITE_TRIAL_USER = "netsuitecreatetrialuser";

        // return codes in response
        public const int CODE_SUCCESS = 0;
        public const int CODE_INVALID_USER = 100;
        public const int CODE_INVALID_USER_EXISTS = 101;
        public const int CODE_INVALID_PWD = 102;
        public const int CODE_INTERNAL_ERROR = 103;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Util.setUpHeaders(Response, false);
                
                string UserAddress = Context.Request.UserHostAddress;
                List<string> allowedIPs = Util.getTrustedTrialRequestIPs();
                if (!Util.IsValidIP(UserAddress, allowedIPs))
                {
                    Global.systemLog.Warn("UserSignUp.aspx: bad request IP: " + UserAddress);
                    Response.StatusCode = 500;
                    return;
                }

                if (Request.RequestType != "POST")
                {
                    Response.StatusCode = 405;
                    Global.systemLog.Warn("UserSignup: non-POST request (" + Request.UserHostAddress + ")");
                    return;
                }

                string actionKey = Request.Params[ACTION_KEY];
                ReturnMessage returnMessage = null;
                if (actionKey != null && actionKey.Trim().Length > 0)
                {

                    switch (actionKey.ToLower())
                    {
                        case ACTION_KEY_VALIDATE_USER:
                            returnMessage = validateUser();
                            break;
                        case ACTION_KEY_VALIDATE_PWD:
                            returnMessage = validatePassword();
                            break;
                        case ACTION_KEY_CREATE_DISCOVER_TRIAL_USER:
                            returnMessage = createDiscoveryTrialUser();
                            break;                    
                        default:
                            Global.systemLog.Warn("UserSignup: Invalid action provided (" + actionKey + " , " + Request.UserHostAddress + ")");
                            returnMessage = new ReturnMessage(CODE_INTERNAL_ERROR, "Action not supported");
                            break;
                    }
                }
                else
                {
                    Global.systemLog.Warn("UserSignup: No action provided (" + Request.UserHostAddress + ")");
                    returnMessage = new ReturnMessage(CODE_INTERNAL_ERROR, "Invalid action requested");
                }
                writeResponse(returnMessage);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserSignup.aspx " + ex.Message + " ", ex);
                ReturnMessage errorMessage = new ReturnMessage(CODE_INTERNAL_ERROR, "Error while creating user. Please try again");
            }
        }

        private bool isValidTrialEmail(string emailAddress)
        {
            Regex regex = Util.getRegexForTrialUserEmail();
            return regex.Match(emailAddress).Success;
        }

        private ReturnMessage validateUser()
        {
            string username = Request.Params[ACTION_PARAM_UNAME];
            if (username == null || username.Trim().Length == 0)
            {
                return new ReturnMessage(CODE_INTERNAL_ERROR, "No username supplied");
            }

            if (!isValidTrialEmail(username))
            {
                return new ReturnMessage(CODE_INVALID_USER, "Invalid username format. Please specify a different username.");
            }

            MembershipUser mu = Membership.GetUser(username);
            if (mu != null)
            {
                return new ReturnMessage(CODE_INVALID_USER_EXISTS, "Username already exists. Please specify a different username.");
            }

            return new ReturnMessage(CODE_SUCCESS, "Username available");
        }

        private string getRequestParam(string paramName)
        {
            string value = Request.Params[paramName];
            if (value == null || value.Trim().Length == 0)
                return null;
            return value;
        }

        private ReturnMessage validatePassword()
        {
            string pwd = getRequestParam(ACTION_PARAM_PWD);
            if (pwd == null || pwd.Trim().Length == 0)
            {
                return new ReturnMessage(CODE_INVALID_PWD, "No password supplied");
            }

            string username = getRequestParam(ACTION_PARAM_UNAME);

            QueryConnection conn = null;
            PasswordPolicy policy = null;
            try
            {                
                conn = ConnectionPool.getConnection();
                policy = Database.getPasswordPolicy(conn, Util.mainSchema, getDiscoveryTrialAdminAccountID());
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            if (policy == null)
            {
                Global.systemLog.Error("UserSignup.aspx : No password policy found for creating user");
                return new ReturnMessage(CODE_INTERNAL_ERROR, "Problem during validation");
            }

            if (!policy.isValid(pwd, username, null))
            {
                Global.systemLog.Error("UserSignup.aspx : Invalid password policy found for creating trial user : " + policy.description);
                return new ReturnMessage(CODE_INVALID_PWD, policy.description);
            }

            return new ReturnMessage(CODE_SUCCESS, "Valid Password");
        }

        public static Guid getDiscoveryTrialAdminAccountID()
        {
            if (discoveryTrialAccountAdmin != null && discoveryTrialAccountAdmin.Trim().Length > 0)
            {
                Guid guid = Guid.Empty;
                if(!Guid.TryParse(discoveryTrialAccountAdmin.Trim(), out guid)){
                    Global.systemLog.Warn("Invalid DiscoveryTrialAccountAdmin given in Web.config file. Not using any account definition for discovery trial");
                    return Guid.Empty;
                }
                return guid;
            }
            Global.systemLog.Info("DiscoveryTrialAdminAccount parameter not set in Web.config.");
            return Guid.Empty;
        }

        public static int getDiscoveryTrialDays()
        {
            if (discoveryTrialDaysString != null)
            {
                return int.Parse(discoveryTrialDaysString);
            }
            Global.systemLog.Info("DiscoveryTrialDays parameter not set in Web.config. Using default of " + DEFAULT_DISCOVERY_TRIAL_DAYS);
            return DEFAULT_DISCOVERY_TRIAL_DAYS;
        }

        public static int getDiscoveryTrialReleaseType()
        {
            if (discoveryTrialReleaseTypeString != null)
            {
                return int.Parse(discoveryTrialReleaseTypeString);
            }
            Global.systemLog.Info("DiscoveryTrialReleaseType parameter not set in Web.config. No release type set on user ");
            return -1;
        }       

        public static int getNetsuiteDiscoveryTrialDays()
        {
            if (netsuiteDiscoveryTrialDaysString != null)
            {
                return int.Parse(netsuiteDiscoveryTrialDaysString);
            }
            Global.systemLog.Info("NetsuiteDiscoveryTrialDays parameter not set in Web.config. Using default of " + DEFAULT_DISCOVERY_TRIAL_DAYS);
            return DEFAULT_DISCOVERY_TRIAL_DAYS;
        }

        public static int getNetsuiteDiscoveryTrialReleaseType()
        {
            if (netsuiteDiscoveryTrialReleaseTypeString != null)
            {
                return int.Parse(netsuiteDiscoveryTrialReleaseTypeString);
            }
            Global.systemLog.Info("NetsuiteDiscoveryTrialReleaseType parameter not set in Web.config. No release type set on user ");
            return -1;
        }
        public static string getNetsuiteDiscoveryTrialProductIDs()
        {
            if (netsuiteDiscoveryTrialProductIDs != null)
            {
                return netsuiteDiscoveryTrialProductIDs;
            }
            Global.systemLog.Info("NetsuiteDiscoveryTrialProductIDs parameter not set in Web.config.");
            return null;
        }

        private ReturnMessage createDiscoveryTrialUser()
        {           
            ReturnMessage validate = validateUser();
            if (!validate.isSuccess())
            {
                return validate;
            }
            validate = validatePassword();
            if (!validate.isSuccess())
            {
                return validate;
            }
            string username = getRequestParam(ACTION_PARAM_UNAME);
            string password = getRequestParam(ACTION_PARAM_PWD);
            CreateTrialUser createTrialUser = new CreateTrialUser(username, password, getDiscoveryTrialAdminAccountID(), discoveryTrialProductIDs,
                getDiscoveryTrialDays(), getDiscoveryTrialReleaseType(), Util.getRequestBaseURL(Request), Session, false);
            BackgroundProcess.startModule(new BackgroundTask(createTrialUser, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
            return new ReturnMessage(CODE_SUCCESS, "Success");            
        }

        public ReturnMessage createDiscoveryTrialUserForNetsuite(string username, string password, Guid accountID)
        {
            CreateTrialUser createTrialUser = new CreateTrialUser(username, password, accountID, netsuiteDiscoveryTrialProductIDs,
                getNetsuiteDiscoveryTrialDays(), getNetsuiteDiscoveryTrialReleaseType(), null, Session, true);
            BackgroundProcess.startModule(new BackgroundTask(createTrialUser, ApplicationLoader.MAX_TIMEOUT *0));

            Status.StatusResult result = createTrialUser.getStatusUser();
                int i = 0;
                try
                {
                    while (true)
                    {   
                        Thread.Sleep(1000 * 10);
                        i++;
                        result = createTrialUser.getStatusUser();
                        if (result != null && (result.code == Status.StatusCode.Complete || result.code == Status.StatusCode.Failed))
                        {
                            result.code = (result.code == Status.StatusCode.Complete || result.code == Status.StatusCode.Failed) ? result.code : Status.StatusCode.Complete;
                            return new ReturnMessage(CODE_SUCCESS, "Success");
                        }
                    }
                }
                finally
                {
                  
                }     
        }
        
        private void writeResponse(ReturnMessage returnMessage)
        {
            int returnCode = returnMessage.code;
            string returnMsg = returnMessage.message;

            Response.Write("responsecode=" + returnCode + "\n");
            if (returnMsg != null && returnMsg.Trim().Length > 0)
            {
                Response.Write("message=" + returnMsg);
            }
        }
     
        public class ReturnMessage
        {
            public int code;
            public string message;

            public ReturnMessage(int code, string message)
            {
                this.code = code;
                this.message = message;
            }

            public bool isSuccess()
            {
                return code == CODE_SUCCESS;
            }
        }

        private class CreateTrialUser : BackgroundModule
        {
            string userEmail;
            string password;
            string requestBaseURL;
            Guid trialAdminAccountID;
            int trialDays;
            string trialProductIDs;
            int trialReleaseType;
            HttpSessionState session;
            bool checkForNonAdminUser;
            private Dictionary<BackgroundModuleObserver, object> observers;
            private Status.StatusCode processStatus = Status.StatusCode.None;

            public CreateTrialUser(string userEmail, string password, 
                Guid trialAdminAccountID, string trialProductIDs, int trialDays, int trialReleaseType,
                string requestBaseURL, HttpSessionState session, bool checkForNonAdminUser)
            {
                observers = new Dictionary<BackgroundModuleObserver, object>();
                this.userEmail = userEmail;
                this.password = password;
                this.trialAdminAccountID = trialAdminAccountID;
                this.trialProductIDs = trialProductIDs;
                this.trialDays = trialDays;
                this.trialReleaseType = trialReleaseType;
                this.requestBaseURL = requestBaseURL;
                this.session = session;
                this.checkForNonAdminUser = checkForNonAdminUser;
            }
            public Status.StatusResult getStatusUser()
            {
                return new Status.StatusResult(processStatus);
            }

            public void setStatusUser()
            {
                processStatus = Status.StatusCode.Complete;
            }
            public void run()
            {
                try
                {
                    UserCreation userCreation = new UserCreation(userEmail, password,
                            trialAdminAccountID, trialProductIDs, trialDays, trialReleaseType, TrialUserSignup.sampleSpaceComment,
                            requestBaseURL, session);

                    if (checkForNonAdminUser && getNetsuiteDiscoveryTrialProductIDs() == trialProductIDs)
                    {
                        bool isAdmin = session != null ? session["adminUser"] != null : false;
                        if (isAdmin)
                        {
                            MembershipUser mu = userCreation.startUserCreation();
                        }
                        else
                        {
                            MembershipUser mu = userCreation.startNonAdminUserCreation();
                        }

                        Global.systemLog.Info("Netsuite discovery trial user created " + userEmail);
                    }
                    else
                    {
                        MembershipUser mu = userCreation.startUserCreation();
                        string emailTemplate = UserCreation.getVerificationEmailTemplateBody(Acorn.User.DISCOVERY_FREE_TRIAL);
                        MailMessage mm = new MailMessage();
                        mm.IsBodyHtml = true;
                        mm.Body = emailTemplate;
                        mm.To.Add(new MailAddress(userEmail));
                        UserCreation.sendSignUpVerificationEmail(mm, Acorn.User.DISCOVERY_FREE_TRIAL, requestBaseURL, (Guid)mu.ProviderUserKey);
                        Global.systemLog.Info("Verification email sent to user " + userEmail);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while creating trial user ", ex);
                    Util.sendErrorDetailEmail("[Problem while creating discovery trial user : " + userEmail + " ]",
                            "Discovery Trial User Creation",
                            "Exception in creating discovery trial account for " + userEmail + " : " + ex.Message,
                            ex, null, discoveryTrialCreationErrorEmail);
                }
                setStatusUser();
            }

            public void kill()
            {
            }

            public void finish()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            #region BackgroundModule Members


            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }
            
            public Status.StatusResult getStatus()
            {
                return new Status.StatusResult(processStatus);
            }
           
            #endregion
        }
    }
}