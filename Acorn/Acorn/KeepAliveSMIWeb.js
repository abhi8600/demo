﻿var http_request = false;
var debug = false;
var oldSessionId = '';

ajax('POST', 'PollSession.aspx', '');
setInterval("ajax('POST', 'PollSession.aspx', '')", 8 * 60 * 1000);

function ajax(httpRequestMethod, url, parameters) {
    http_request = false;
    if (window.XMLHttpRequest) {
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/plain');
        }
    }
    else if (window.ActiveXObject) {
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e)
      { }
        }
    }
    if (!http_request) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    http_request.onreadystatechange = function() { return true; };
    if (httpRequestMethod == 'GET') {
        http_request.open('GET', url + '?' + parameters, true);
        http_request.send(null);
        ser = ser + 1;
    }
    else if (httpRequestMethod == 'POST') {
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type',
      'application/x-www-form-urlencoded');
        http_request.send(parameters);
    }
    else {
        alert('Sorry, unsupported HTTP method');
    }
}