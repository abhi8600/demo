﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class ScheduledItem
    {
        public Guid SpaceID;
        public Guid ID;
        public string Module;
        public string Parameters;
        public DateTime NextDate;
        public string Status;
        public string WorkingServer;
        public DateTime WorkTime;
    }
}
