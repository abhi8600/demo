﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.IO;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net.Security;
using System.Collections.Specialized;
using Acorn.DBConnection;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using Acorn.Utils;

namespace Acorn
{
    public partial class Login : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string proxyuserName = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ProxyuserName"];

        private static string supportOpenID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SupportOpenID"];
        private static string showCertifications = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ShowCertifications"];
        private static string termsOfServiceURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TermsOfServiceLink"];
        private static string privacyPolicyURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PrivacyPolicyLink"];
        private static string contactUsURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContactUsLink"];
        private static string loginFailureText = "<div style='color:Red;padding:5px'>Login attempt was not successful. There are a number of potential reasons for this: invalid username, expired user account, user account has been disabled, invalid password, {min} user lockout or IP restriction. Please try again.</div>";

        private string redirectURL = null;
        private string token = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SSO.clearCookie(Request, Response, "ASP.NET_SessionId");
            SSO.clearCookie(Request, Response, "JSESSIONID");
            SSO.clearCookie(Request, Response, "BSJSESSIONID");

            Util.setUpHeaders(Response, false);
            if (!Global.initialized)
                RegistrationKeyLabel.Visible = true;

            bool useOpenID = (supportOpenID != null && supportOpenID.ToLower().Equals("true"));
            OpenIDPlaceHolder.Visible = useOpenID;

            if (useOpenID)
            {
                if (Request["birst.OpenID"] == "true")
                {
                    Util.dumpHeaders(Request);
                    OpenIDFormSubmission();
                    return;
                }
            }

            if (redirectForSAML(Request))
            {
                Global.systemLog.Info("Redirecting for SAML User" + Request.Url);
                return;
            }

            // update the copyright and version strings
            Copyright.Text = Util.getCopyright();
            Version.Text = ""; // XXX for now, until login service has it's own version number Util.getAppVersion();

            CertificationPlaceHolder.Visible = (showCertifications != null && showCertifications.ToLower().Equals("true"));

            if (termsOfServiceURL != null)
                TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
            else
                TermsOfServicePlaceHolder.Visible = false;
            if (privacyPolicyURL != null)
                PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
            else
                PrivacyPolicyPlaceHolder.Visible = false;
            if (contactUsURL != null)
                ContactUsLink.NavigateUrl = contactUsURL;
            else
                ContactUsPlaceHolder.Visible = false;

            // set initial input focus on the username text field
            Control ctrl = LoginForm2.FindControl("UserName");
            if (ctrl != null)
                SetFocus(ctrl);

            // handle the new user first login
            string newuser = Request["newuser"];
            if (newuser != null)
            {
                MembershipUser mu = Membership.GetUser(new Guid(newuser));
                if (mu != null && !mu.IsApproved)
                {
                    SuccessfulLabel.Visible = true;
                    mu.IsApproved = true;
                    Membership.UpdateUser(mu);                    
                    QueryConnection conn = null;
                    User user = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        user = Database.getUser(conn, Util.getMainSchema(), mu.UserName);
                        if (user != null && user.isDiscoveryFreeTrial)
                        {
                            // update the end date relative to verification day
                            Database.resetEndDate(conn, Util.mainSchema, user, UserSignup.getDiscoveryTrialDays());
                            // no need to send any get started email for discovery user
                            return;
                        }                        
                    }                    
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    
                    // Send acknowledgement
                    MailMessage mm = new MailMessage();
                    mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
                    mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
                    mm.To.Add(new MailAddress(mu.Email));
                    mm.IsBodyHtml = true;
                    StreamReader reader = new StreamReader(MapPath("~/") + "Welcome to Birst Express.htm");
                    mm.Body = reader.ReadToEnd();
                    reader.Close();
                    mm.Body = mm.Body.Replace("${ACCOUNT}", mu.UserName);
                    mm.Body = mm.Body.Replace("${LINK}", Util.getMasterURL(Session, Request));
                    mm.Subject = "Welcome to Birst!";
                    SmtpClient sc = new SmtpClient();
                    bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
                    if (useSSL)
                        sc.EnableSsl = true;
                    sc.Send(mm);
                }
            }
        }

        private bool redirectForSAML(HttpRequest Request)
        {
            string standardLogOut = Request.Params["birst.logout"];
            if (!string.IsNullOrWhiteSpace(standardLogOut) && standardLogOut.Trim().ToLower() == "true")
                return false;

            if (SamlUtils.isSAMLRequest(Request))
            {
                string requestedResource = "";
                if (Request.QueryString != null)
                {
                    string returnURL = Request.QueryString["ReturnUrl"];
                    if (returnURL != null && returnURL.Length > 0)
                    {
                        requestedResource = returnURL.Trim();
                    }
                }
                string redirectURL = Util.getRequestBaseURL(Request) + "/SAMLSSO/Services.aspx?" + Acorn.Utils.SamlUtils.KEY_RELAY_SAML_STATE + "=" + requestedResource;
                Response.Redirect(redirectURL);
                return true;
            }
            return false;
        }


        protected void LoginForm2_Authenticate(object sender, AuthenticateEventArgs e)
        {            
            string lockedoutTimeString = null;
            QueryConnection conn = null;
            try
            {
                // remove all authentication state
                Session.Remove("user");
                Session.Remove("auth");
                Session.Remove("MAF");
                Session.Remove("space");
                Session.Remove("ssoAuthentication");
                Session.Remove("proxyIsOperation");

                e.Authenticated = false;

                // get the security token
                string username = LoginForm2.UserName;
                string password = LoginForm2.Password;

                if (username == null || username.Length == 0 || password == null || password.Length == 0)
                {
                    return;
                }

                // check for proxyuser
                bool proxyuser = false;
                bool repAdmin = false;
                string hostaddr = Request.UserHostAddress;
                string authenticatedUser = null;
                bool redirect = true;

                if (username.StartsWith("noredirect/"))
                {
                    username = username.Substring(11);
                    redirect = false;
                }

                if (username.StartsWith(proxyuserName + "/"))
                {
                    // get the two users
                    string[] tokens = username.Split('/');
                    if (tokens.Length != 3)
                    {
                        Global.systemLog.Warn("Invalid proxy user specification: " + LoginForm2.UserName);
                        return;
                    }
                    authenticatedUser = tokens[1];
                    username = tokens[2];
                    if (!Membership.ValidateUser(authenticatedUser, password))
                    {
                        MembershipUser mu = Membership.GetUser(authenticatedUser, false);
                        if (mu != null && mu.IsLockedOut)
                        {                            
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (locked out) - " + LoginForm2.UserName);
                        }
                        else
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (not authenticated) - " + LoginForm2.UserName);
                        }
                        return;
                    }
                    // user is authenticated, validate that the user has proxy access to the proxy user
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        User authUser = Database.getUser(conn, mainSchema, authenticatedUser);
                        if (authUser == null)
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (not in the database or products expired) - " + LoginForm2.UserName);
                            return;
                        }
                        if (authUser.Disabled)
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (disabled) - " + LoginForm2.UserName);
                            return;
                        }
                        if (authUser.actDetails != null && authUser.actDetails.Disabled)
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (account disabled) - " + LoginForm2.UserName);
                            return;
                        }
                        // verify user against IP list
                        List<String> userAllowedIPs = Database.getAllowedIPs(conn, mainSchema, authUser.ManagedAccountId, authUser.ID);
                        if (!Util.IsValidIP(hostaddr, userAllowedIPs))
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (invalid ip adddress) - " + LoginForm2.UserName + " - " + hostaddr);
                            return;
                        }
                        User proxy = Database.getUser(conn, mainSchema, username);
                        if (proxy == null)
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (proxy user not in the database) - " + LoginForm2.UserName);
                            return;
                        }
                        // check if the user is account admin for the proxy user. In that case, we allow it to go into the account
                        proxyuser = false;
                        if (authUser.AdminAccountId != Guid.Empty && authUser.AdminAccountId.ToString() == proxy.ManagedAccountId.ToString())
                        {
                            proxyuser = true;
                        }
                        else
                        {
                            bool inTrustedNetblock = Util.IsValidIP(hostaddr);
                            if (Database.isProxy(conn, mainSchema, authUser, proxy, inTrustedNetblock, ref repAdmin)) // proxy check second
                            {
                                proxyuser = true;
                            }
                        }
                        if (!proxyuser)
                        {
                            Global.systemLog.Warn("Failed proxy login attempt for user " + authenticatedUser + " (not allowed to proxy for the user or not in trusted netblock) - " + LoginForm2.UserName);
                            return;
                        }
                        Session["proxyIsOperation"] = authUser.OperationsFlag;
                        Global.systemLog.Info("Proxy user login request for " + LoginForm2.UserName);
                    }
                    finally
                    {                        
                        ConnectionPool.releaseConnection(conn);
                    }
                }

                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, mainSchema, username);
                if (u == null)
                {
                    Global.systemLog.Info("Failed login attempt for user " + username + " (not in the database or products expired)");
                    return;
                }
                if (u.Disabled)
                {
                    Global.systemLog.Info("Failed login attempt for user " + username + " (user disabled)");
                    return;
                }
                if (u.actDetails != null && u.actDetails.Disabled)
                {
                    Global.systemLog.Warn("Failed login attempt for user " + username + " (account disabled)");
                    return;
                }
                Util.setLogging(u, null);
                string userRedirectedSite = null;
                if (redirect)
                    userRedirectedSite = Util.getUserRedirectedSite(Request, u);
                // no site means go to the original URL (backwards compatibility)
                if (userRedirectedSite == null || userRedirectedSite.Length == 0)
                {
                    redirectURL = Util.getRequestBaseURL(Request);
                }
                else
                {
                    redirectURL = userRedirectedSite;
                }
                bool lockedout = false;
                string id = Request["birst.spaceId"];
                Guid spaceId = Guid.Empty;
                if (id != null && id.Length > 0)
                {
                    try
                    {
                        spaceId = new Guid(id);
                    }
                    catch (Exception)
                    {
                        Global.systemLog.Warn("Bad space id - " + id);
                        return;
                    }
                }
                string sessionVarsParam = Request["birst.sessionVars"];
                token = TokenGenerator.getLoginSecurityToken(conn, mainSchema, u, password, hostaddr, proxyuser, repAdmin, spaceId, sessionVarsParam, ref lockedout);
                if (token == null || token.Length == 0)
                {
                    if (lockedout)
                    {
                        lockedoutTimeString = getLockedOutMessage(conn, mainSchema, u);                        
                    }
                    return;
                }
                token = Util.encryptToken(token);
                e.Authenticated = true; // will allow LoginForm2_LoggedIn to be called
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex, ex);
            }
            finally
            {                
                ConnectionPool.releaseConnection(conn);
                string replaceText = lockedoutTimeString == null ? "" : lockedoutTimeString;
                LoginForm2.FailureText = loginFailureText.Replace("{min}", replaceText);
            }
        }

        private string getLockedOutMessage(QueryConnection conn, string mainSchema, User u)
        {
            string lockedOutMsg = "";
            if (u != null && u.isDiscoveryFreeTrial)
            {
                PasswordPolicy policy = Database.getPasswordPolicy(conn, mainSchema, UserSignup.getDiscoveryTrialAdminAccountID());
                if (policy != null && policy.failedLoginWindow > 0)
                {
                    lockedOutMsg = policy.failedLoginWindow + "-minute";                    
                }                
            }            
            return lockedOutMsg;
        }

        protected void LoginForm2_Init(object sender, EventArgs e)
        {
            // intentionally left blank
        }

        protected void LoginForm2_LoggedIn(object sender, EventArgs e)
        {
            // once authenticated, redirect to the SSO servlet
            string redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString);
            Response.Redirect(redirectUrl);
        }

        // OpenID code

        // DotNetOpenAuth has built-in support for SSL appliances when they add these special HTTP headers to the 
        // forwarded HTTP request: X_FORWARDED_PROTO and/or HTTP_HOST. When these are present, the auto-detection of the
        // outside-facing URL is correct. If you can configure your SSL appliance to do this, that's probably the best option.
        //
        // The alternative is to call OpenIdRelyingParty.GetResponse(HttpRequestInfo) instead of the overload that takes no parameters. 
        // You construct the HttpRequestInfo yourself using the outward-facing URL that you know is the real one. Then the URL matching 
        // logic inside DotNetOpenAuth won't fail the request

        protected HtmlInputControl openid_identifier;
        private static OpenIdRelyingParty openid = new OpenIdRelyingParty();

        private void OpenIDFormSubmission()
        {
            var response = openid.GetResponse();
            if (response == null)
            {
                Identifier id;
                string openIdIdentifier = Request.Form["openid_identifier"];
                if (Identifier.TryParse(openIdIdentifier, out id))
                {
                    try
                    {
                        IAuthenticationRequest request = openid.CreateRequest(openIdIdentifier);
                        FetchRequest freq = new FetchRequest();
                        freq.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
                        request.AddExtension(freq);
                        request.RedirectToProvider();
                    }
                    catch (ProtocolException ex)
                    {
                        writeError(ex.Message);
                    }
                }
                return;
            }

            switch (response.Status)
            {
                case AuthenticationStatus.Authenticated:
                    FetchResponse fresponse = response.GetExtension<FetchResponse>();
                    string openIdEndpoint = response.ClaimedIdentifier.ToString();
                    loginUser(openIdEndpoint, fresponse);
                    break;
                case AuthenticationStatus.Canceled:
                    writeError("OpenID authentication request canceled at the OpenID provider");
                    break;
                case AuthenticationStatus.Failed:
                    Global.systemLog.Info(response.Exception.Message);
                    writeError("OpenID authenication request failed");
                    break;
            }
        }

        private void writeError(string message)
        {
            OpenIDLabel.Text = HttpUtility.HtmlEncode(message);
            OpenIDLabel.Visible = true;
        }

        private void loginUser(string openIdEndpoint, FetchResponse fresponse)
        {
            QueryConnection conn = null;
            try
            {
                User u = null;
                conn = ConnectionPool.getConnection();
                // get the OpenID/Birst User association
                Guid userId = Database.getUserIdForOpenId(conn, mainSchema, openIdEndpoint);
                if (userId == Guid.Empty)
                {
                    // see if the user (email) already exists in our DB
                    if (fresponse != null)
                    {
                        string email = fresponse.GetAttributeValue(WellKnownAttributes.Contact.Email);
                        u = Database.getUser(conn, mainSchema, email);
                        if (u != null)
                        {
                            // user already exists, add the mapping and go on
                            userId = u.ID;
                            Database.addOpenIDForUserId(conn, mainSchema, userId, openIdEndpoint);
                        }
                        else
                        {
                            writeError("The OpenID endpoint (" + openIdEndpoint + ") is not associated with a Birst user and the Birst user (" + email + ") does not exist");
                            Global.systemLog.Info("The OpenID endpoint (" + openIdEndpoint + ") is not associated with a Birst user and the Birst user (" + email + ") does not exist");
                            return;
                        }
                    }
                    else
                    {
                        writeError("The OpenID endpoint (" + openIdEndpoint + ") is not associated with a Birst user and the OpenID provider did not provide the email address");
                        Global.systemLog.Info("The OpenID endpoint (" + openIdEndpoint + ") is not associated with a Birst user and the OpenID provider did not provide the email address");
                        return;
                    }
                }
                // get the actual Birst User
                u = Database.getUserById(conn, mainSchema, userId);

                if (u == null)
                {
                    writeError("The Birst user associated with the OpenID endpoint is not in the database or has expired products - " + openIdEndpoint);
                    Global.systemLog.Info("The Birst user associated with the OpenID endpoint is not in the database or has expired products - " + userId.ToString() + "/" + openIdEndpoint);
                    return;
                }
                if (u.Disabled)
                {
                    writeError("The Birst user associated with the OpenID endpoint has been disabled - " + openIdEndpoint);
                    Global.systemLog.Info("The Birst user associated with the OpenID endpoint has been disabled - " + userId.ToString() + "/" + openIdEndpoint);
                    return;
                }
                Util.setLogging(u, null);
                // determine the site redirection
                string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
                string redirectURL = null;
                // no site means go to the original URL (backwards compatibility)
                if (userRedirectedSite == null || userRedirectedSite.Length == 0)
                {
                    redirectURL = Util.getRequestBaseURL(Request);
                }
                else
                {
                    redirectURL = userRedirectedSite;
                }
                // get the Birst security token and validate the user (products, IP restrictions)
                bool lockedout = false;
                string token = TokenGenerator.getOpenIDLoginSecurityToken(conn, mainSchema, u, Request.UserHostAddress, ref lockedout);
                if (token == null || token.Length == 0)
                {
                    if (lockedout)
                    {
                        writeError("Login has been locked after too many unsuccessful attempts, please contact support to unlock");
                    }
                    return;
                }
                // once authenticated, redirect to the SSO servlet
                token = Util.encryptToken(token);
                string redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString);
                Response.Redirect(redirectUrl);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
