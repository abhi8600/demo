﻿Ext.ns("Birst.core");

function xml2Str(xmlNode) {
    try {
        // Gecko- and Webkit-based browsers (Firefox, Chrome), Opera.
        return (new XMLSerializer()).serializeToString(xmlNode);
    }
    catch (e) {
        try {
            // Internet Explorer.
            return xmlNode.xml;
        }
        catch (e) {
            //Other browsers without XML Serializer
            alert('Xmlserializer not supported');
        }
    }
    return false;
}

function loadXmlFile(filename, callback) {
    try {
        var xmlDoc = document.implementation.createDocument("", "", null);

    } catch (e) {
        try {

        } catch (e) {
            alert('load file unsupported');
        }
    }
}

function Str2Xml(str) {
    if (window.DOMParser) {
        var parser = new DOMParser();
        return parser.parseFromString(str, 'text/xml');
    }

    var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async = false;
    xmlDoc.loadXML(str);
    return xmlDoc;
}

function getXmlChildValue(node, childName) {
    var child = node.getElementsByTagName(childName);
    if (child && child.length > 0) {
        return getXmlValue(child[0]);
    }
    return null;
}

function getXmlValue(node) {
    var child = node.firstChild;
    if (child && child.nodeName == '#text') {
        if (Ext.isIE)
            return child.nodeValue;
        return child.wholeText;

    }

    return null;
}

Ext.define('Birst.core.Webservice', {
    statics: {
        escapeXml: function (str) {
            return (str) ? str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') : '';
        },
        createEnvelope: function (service, fnName, args) {
            
            var soap = '<SOAP-ENV:Envelope  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><tns:';
            soap += fnName + ' xmlns:tns="http://www.birst.com/">';
                             
            //arguments 
            for (var i = 0; i < args.length > 0; i++) {
                var key = args[i].key;
                var value = args[i].value;
                if (key && value) {
                    soap += '<tns:' + key + '>' + Birst.core.Webservice.escapeXml(value) + '</tns:' + key + '>';
                }
            }

            soap += '</tns:' + fnName + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';
            return soap;
        },

        request: function (service, fnName, args, scope, successFn, failFn, additionalParam) {
            var envelope = Birst.core.Webservice.createEnvelope(service, fnName, args);
            var serviceUpperCase = service.charAt(0).toUpperCase() + service.slice(1);

            Ext.Ajax.request({
                url: '/AdminService.asmx',
                method: 'POST',
                headers: {
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': 'http://www.birst.com/'+ fnName
                },
                scope: scope,
                params: envelope,
                additionalParam: additionalParam,
                success: function (response, options) {
                    Ext.getBody().unmask();
                    if (successFn != null) {
                        successFn.call(scope, response, options);
                    }
                },
                failure: (failFn != null) ?
					function (response, options) {
					    Ext.getBody.unmask();
					    failFn.call(scope, response, options);
					}
						:
					function (response, options) {
					    Ext.getBody().unmask();
					    alert('failure - ' + response.responseText);
					}
            });

            Ext.getBody().mask('Loading');
        },

        dashboardRequest: function (fnName, args, scope, successFn, failFn, additionalParam) {
            Birst.core.Webservice.request('dashboard', fnName, args, scope, successFn, failFn, additionalParam);
        },
        adhocRequest: function (fnName, args, scope, successFn, failFn, additionalParam) {
            Birst.core.Webservice.request('adhoc', fnName, args, scope, successFn, failFn, additionalParam);
        },
        acornRequest: function (fnName, args, scope, successFn, failFn, additionalParam) {

            Birst.core.Webservice.request('acorn', fnName, args, scope, successFn, failFn, additionalParam);
        }
    }
});

