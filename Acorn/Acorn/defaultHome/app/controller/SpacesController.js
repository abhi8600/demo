﻿Ext.define('DefaultHome.controller.SpacesController', {

    extend: 'Ext.app.Controller',

    stores: ['SoapWebService'],

    models: ['Space'],

    views: ['SpacesListPanel', 'SpaceOptionsPanel'],

    // views the controller cares about
    refs: [
    	{
    	    selector: 'viewport #idHeaderContainer',
    	    ref: 'headerContainer'
    	},
        {
            selector: 'viewport #idLoggedUserOptionsContainer',
            ref: 'loggedUserContainer'
        },
    	{
    	    selector: 'viewport #idMySpacesMainContainer',
    	    ref: 'spacesMainContainer'
    	},
        {
            selector: 'viewport #idSpacesListPanel',
            ref: 'spacesListContainer'
        },
        {
            selector: 'viewport #idSpaceOptionsContainer',
            ref: 'spaceOptionsContainer'
        },
        {
            selector: 'spaceOpts',
            ref: 'spaceOptionsPanel'
        }
    ],

    userSettings: {
        userName: null,
        userMode: null,
        lastLoginDate: null,
        selectedSpace: null,
        selectedSpaceId: null,
        enableCopy: null,
        showFreeTrialPage: null,
        canCreateNewSpace: null,
        enableUserManagement: null,
        showTemplate: null
    },

    spaceSettings: {
        spaceName: null,
        spaceID: null,
        owner: null,
        ownerUserName: null,
        administrator: null,
        adhoc: null,
        dashboards: null,
        quickDashboard: null,
        enableAdhoc: null,
        enableDashboards: null,
        enableQuickDashBoards: null,
        availabilityCode: null,
        spaceMode: null,
        adminAccess: {
            manageCatalog: null,
            publish: null,
            useTemplatetrue: null,
            servicesAdmin: null,
            copySpace: null,
            customFormula: null,
            shareSpace: null,
            quartzSchedulerEnabled: null,
            showOldScheduler: null
        }
    },

    store: null,

    model: null,

    grid: null,

    opts: null,

    init: function () {

        var sstore = this.getSoapWebServiceStore();
        var smodel = this.getSpaceModel();

        store = Ext.create('Ext.data.ArrayStore', {
            autoDestroy: false,
            idIndex: 0,
            fields: [
				'SpaceName',
				'SpaceID',
                'OwnerUsername',
                'Owner',
                'AvailabilityCode'
			]
        });

        grid = Ext.create('Ext.grid.Panel', {
            store: store,
            layout: 'fit',
            autoScroll: true,
            //selModel: Ext.create('Ext.selection.CheckboxModel'), //*
            columns: [
            //Ext.create('Ext.grid.RowNumberer'), //*
                        {text: 'Space', dataIndex: 'SpaceName', sortable: true, width: 200, renderer: function (v) { return '<img src="resources/style/images/datasource-green.png">' + ' ' + v; } },
                        { text: 'User', dataIndex: 'OwnerUsername', sortable: true, width: 140 },
                        { text: 'Owner', dataIndex: 'Owner', sortable: true, xtype: 'booleancolumn', width: 540 }
            ],
            //padding: 10,
            viewConfig: {

                forceFit: true,
                listeners: {
                    scope: this,
                    itemclick: function (dataview, record, item, index, e) {

                        this.processSpaceSelected(record);
                    }
                }
            }

        });

        // Create the opts & nav (my-spaces's right-hand pane) panel

        opts = Ext.widget('spaceOpts');

        grid.on('rowselect',
                 function (rowIndex, columnIndex, e) {
                     // Todo:  
                 },
                this);

        this.control({
            '#idMySpacesMainContainer': {
                render: this.onSpacesRender
            }
        });


        this.control({
            '#idSpacesListPanel': {
                render: this.onSpacesListContainerRender
            }
        });

        this.control({
            'idSpaceOptionsContainer': {
                render: this.onSpacesOptionsContainerRender
            }
        });

        this.control({
            'spaceOpts': {
                render: function () { /*alert('View was rendred.'); */ }
            }
        });


    },

    onSpacesRender: function () {
        //alert('Main container rendered');
        var spaces = this.getSpacesMainContainer();
        //spaces.add(grid);
    },

    onSpacesListContainerRender: function () {
        //alert('left container rendered');
        var spaces = this.getSpacesListContainer();
        spaces.add(grid);
        //spaces.add(opts);
        var optsp = this.getSpaceOptionsContainer();
        optsp.add(opts);
    },

    onSpacesOptionsContainerRender: function () {
        //alert('right container rendered');
        var optsp = this.getSpaceOptionsContainer();

    },
    start: function () {

        this.getSpacesFromService();
    },

    hanleAllSpacesResponse: function (xmlDoc) {
        // Parse response [todo: check for error statuses]

        // Selected space   
        var response = xmlDoc.getElementsByTagName('GetAllSpacesResponse');
        var userSettings = {};

        if (response) {
            var option = response[0].firstChild;
            while (option != null) {

                userSettings.userName = getXmlChildValue(option, 'UserName');
                userSettings.userMode = getXmlChildValue(option, 'UserMode');
                userSettings.lastLoginDate = getXmlChildValue(option, 'LastLoginDate');
                userSettings.selectedSpace = getXmlChildValue(option, 'Selected');
                userSettings.enableCopy = getXmlChildValue(option, 'EnableCopy');
                userSettings.showFreeTrialPage = getXmlChildValue(option, 'ShowFreeTrialPage');
                userSettings.canCreateNewSpace = getXmlChildValue(option, 'canCreateNewSpace');
                userSettings.enableUserManagement = getXmlChildValue(option, 'EnableUserManagement');
                userSettings.showTemplate = getXmlChildValue(option, 'showTemplate');

                option = option.nextSibling;
            }
        }
        this.userSettings = userSettings;

        // Add spaces to store
        var spaceName;
        var spaceId;
        var ownerName;
        var availCode;

        var spaces = xmlDoc.getElementsByTagName('SpacesList');
        if (spaces) {
            var space = spaces[0].firstChild;
            var cntSpcs = 0;

            while (space != null) {
                spaceName = getXmlChildValue(space, 'SpaceName');
                spaceId = getXmlChildValue(space, 'SpaceID');
                ownerName = getXmlChildValue(space, 'OwnerUsername');
                owner = getXmlChildValue(space, 'Owner');
                availCode = getXmlChildValue(space, 'AvailabilityCode');
                if (spaceName && spaceId && ownerName && owner && availCode) {
                    store.add({ SpaceName: spaceName, SpaceID: spaceId, OwnerUsername: ownerName, Owner: owner, AvailabilityCode: availCode });

                }
                space = space.nextSibling;
                // is this a selected space 
                if (cntSpcs == parseInt(userSettings.selectedSpace)) {
                    userSettings.selectedSpaceId = spaceId;
                }
                cntSpcs++;
            }
        }
        // set corresponding row in grid  
        grid.getView().select(parseInt(userSettings.selectedSpace));

        // update space nav/space options view
        // this.getSpaceOptionsPanel().updateView(this.spaceSettings, this.userSettings);

        // update logged user / user options view
        this.updateUserView();

        // get further user/space deatils from server 

        Birst.core.Webservice.acornRequest('GetSpaceDetails',
										    [{ key: 'spaceID', value: spaceId },
                                             { key: 'spaceName', value: spaceName }
                                            ],
										   	this,
										    function (response, options) {
										        this.handleSpaceDetailsResponse(response.responseXML);
										    }
        );
    },

    updateUserView: function () {

        // welcome message
        var welcomeMsg = Ext.create('Ext.form.Label', {
            text: 'Welcome, ' + this.userSettings.userName,
            width: 500,
            //cls: 'userinfo-txt-cls'
            margin: '20 20 30 40'
        });

        this.getLoggedUserContainer().add(welcomeMsg);
    },

    // recieved response from server 
    handleSpaceDetailsResponse: function (xmlDoc) {

        // parse a response and update the view accordingly 

        var response = xmlDoc.getElementsByTagName('GetSpaceDetailsResponse');
        var spaceSettings = {};
        spaceSettings.adminAccess = {};

        if (response) {
            var option = response[0].firstChild;
            while (option != null) {
                spaceSettings.spaceName = getXmlChildValue(option, 'SpaceName');
                spaceSettings.spaceID = getXmlChildValue(option, 'SpaceID');
                spaceSettings.owner = getXmlChildValue(option, 'Owner');
                spaceSettings.ownerUserName = getXmlChildValue(option, 'OwnerUserName');
                spaceSettings.administrator = getXmlChildValue(option, 'Administrator');
                spaceSettings.adhoc = getXmlChildValue(option, 'Adhoc');
                spaceSettings.dashboards = getXmlChildValue(option, 'Dashboards');
                spaceSettings.enableAdhoc = getXmlChildValue(option, 'EnableAdhoc');
                spaceSettings.enableDashboards = getXmlChildValue(option, 'EnableDashboards');

                option = option.nextSibling;
            }
        }
        this.spaceSettings = spaceSettings;

        // unmask view 
        this.getSpaceOptionsContainer().unmask();

        // update space nav/space options view
        this.getSpaceOptionsPanel().updateView(this.spaceSettings, this.userSettings);
    },

    processSpaceSelected: function (record) {

        // Get a space's config from service. Update view based on config returned.
        // Mask view/buttons while request is being processed

        this.getSpaceOptionsContainer().mask();
        this.getSpaceDetailsFromService(record);
    },

    getSpacesFromService: function () {

        Birst.core.Webservice.acornRequest('GetAllSpaces',
										    [],
										   	this,
										    function (response, options) {
										        this.hanleAllSpacesResponse(response.responseXML);
										    }
        );
    },

    getSpaceDetailsFromService: function (record) {


        Birst.core.Webservice.acornRequest('GetSpaceDetails',
										    [{ key: 'spaceID', value: record.data.SpaceID },
                                             { key: 'spaceName', value: record.data.SpaceName }
                                            ],
										   	this,
										    function (response, options) {
										        this.handleSpaceDetailsResponse(response.responseXML);
										    }
        );
    }

});