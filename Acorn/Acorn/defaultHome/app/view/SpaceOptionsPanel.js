﻿Ext.define('DefaultHome.view.SpaceOptionsPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.spaceOpts',
    //renderTo: 'idSpaceOptionsContainer',
    height: 450,
    border: false,
    flex: 1,
    layout: {

        type: 'vbox'
    },
    items: [
              {
                  xtype: 'button',
                  align: 'center',
                  text: 'Dashboards',
                  cls: 'button-nav',
                  handler: function () {
                      // Todo: integrate with SAPI
                      //alert('test redirect to dashboard.');
                      window.location = 'http://localhost:6103/DashboardApp_530/app.html';
                  }
              },
              {
                  xtype: 'button',
                  align: 'center',
                  text: 'Designer',
                  cls: 'button-nav'
              }

     ],
    initComponent: function () {

        this.callParent(arguments);
    },

    updateView: function (spaceCfg, userCfg) {

        // render elements that apply to this particular user/space 
        this.removeAll();

        var viewControls = [];

        var selectSpacelabel = Ext.create('Ext.form.Label', {
            text: 'For the selected space on the left, choose from the following options:',
            width: 500,
            cls: 'x-form-item-label x-form-item',
            margin: '20 20 0 40'
        });


        this.add(selectSpacelabel);

        var bttnDashboards = Ext.create('Ext.Button', {
            text: 'Dashboards',
            id: 'idButtNavDashboards',
            cls: 'button-nav',
            iconCls: 'dashboard-icn-cls',
            handler: function () {
                window.location = 'http://localhost:6103/DashboardApp_530/app.html';
            }
        });

        this.add(bttnDashboards);

        var bttnDesigner = Ext.create('Ext.Button', {
            text: 'Designer',
            id: 'idButtNavDesigner',
            cls: 'button-nav',
            iconCls: 'designer-icn-cls',
            handler: function () {

            }
        });

        this.add(bttnDesigner);

        var bttAdmin = Ext.create('Ext.Button', {
            text: 'Admin',
            id: 'idButtNavAdmin',
            cls: 'button-nav',
            handler: function () {
            }
        });

        this.add(bttAdmin);

        var bttnCopy = Ext.create('Ext.Button', {
            text: 'Copy Space',
            id: 'idButtNavCopy',
            cls: 'button-nav',
            iconCls: 'copy-icn-cls',
            handler: function () {

            }
        });

        this.add(bttnCopy);

        var bttnDelete = Ext.create('Ext.Button', {
            text: 'Delete Space',
            id: 'idButtNavDelete',
            cls: 'button-nav',
            iconCls: 'delete-icn-cls',
            handler: function () {

            }
        });

        this.add(bttnDelete);

        var spaceInfo = Ext.create('Ext.form.Label', {
            text: 'Name: ' + spaceCfg.spaceName,
            width: 500,
            //cls: 'userinfo-txt-cls'
            margin: '60 60 30 60'
        });

        this.add(spaceInfo);

        /*
        var label = Ext.create('Ext.form.Panel', {
        width: 600,
        //bodyPadding: 10,
        layout: {
        border: false,
        align: 'middle'
        },
        items: [
        {
        xtype: 'label',
        //forId: 'myFieldId',
        text: 'For the selected space on the left, choose from the following options:',
        margin: '0 0 0 10'
        }]
        });

        */
    }

});