﻿Ext.define('DefaultHome.view.SpacesListPanel', {

    extend: 'Ext.grid.Panel',

    alias: 'widget.spacesGrid',

    initComponent: function () {

        //alert('init view:DefaultHome.view.SpacesListPanel');

        this.store = 'SoapWebService';
        this.columns = [
                    { text: 'Space', dataIndex: 'SpaceName', sortable: true },
                    { text: 'User', dataIndex: 'OwnerUsername', sortable: true },
                    { text: 'Owner', dataIndex: 'Owner', sortable: true }

            ];

        this.callParent(argument);
    }



});