﻿Ext.define('DefaultHome.model.Space', {
    extend: 'Ext.data.Model',
    fields: [
				'SpaceName',
				'SpaceID',
                'OwnerUsername',
                'Owner',
                'AvailabilityCode'
	],
    init: function () {

        alert('Init Model');
    }
 
});
