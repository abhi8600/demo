﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.Utils;
using System.IO;
using System.Text.RegularExpressions;

namespace Acorn.html
{
    public partial class dashboards : System.Web.UI.Page
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);
            string logoutScript = Util.getInactivityTimeoutScript(Session);
            logoutScript = string.IsNullOrWhiteSpace(logoutScript) ? "" : logoutScript;

            if (!Util.validateSpace(sp, u, Session))
            {
                Global.systemLog.Debug("redirecting to home page due to space validation failure");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (sp != null)
            {
                Util.saveDirty(Session);
                createSMIWebSession(Session, Request, Response, Server, sp, u);
            }
            Util.injectIntoPlaceholders(Response, Session, MapPath("~/html/dashboards.html"), logoutScript);  
            Response.Flush();
            Response.End();
        }

        private void createSMIWebSession(System.Web.SessionState.HttpSessionState session, HttpRequest request,
            HttpResponse response, HttpServerUtility server, Space sp, User u)
        {
            string token = Util.getSMILoginToken(u.Username, sp);
            if (Util.getSMILoginSession(session, sp, token, response, request))
            {
                try
                {
                    SchedulerUtils.loginIntoSchedulerIfAllowed(session, response, request);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Error while logging into the scheduler", ex2);
                }

            }
            else
            {
                Global.systemLog.Error("Not able to login SMIWeb");
                response.Redirect(Util.getHomePageURL(session, request));
            }
        }
    }
}