﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.Utils;
using System.IO;
using System.Text.RegularExpressions;

namespace Acorn.html
{
    public partial class home : System.Web.UI.Page
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            User u = Util.validateAuth(Session, Response);
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            string logoutScript = Util.getInactivityTimeoutScript(Session);
            logoutScript = string.IsNullOrWhiteSpace(logoutScript) ? "" : logoutScript;
            Util.injectIntoPlaceholders(Response, Session, MapPath("~/html/home.html"), logoutScript);  
            Response.Flush();
            Response.End();
        }
    }
}