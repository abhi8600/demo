﻿using System;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using Acorn;
using System.Data.Odbc;

public interface ILoad
{
    void setRunning(MainAdminForm maf, Space sp, string mainSchema);
    HttpSessionState getSession();
}
