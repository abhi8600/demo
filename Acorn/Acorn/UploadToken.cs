﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class UploadToken
    {
        public Guid tokenID;
        public Guid userID;
        public string hostaddr;
        public Guid spaceID;
        public DateTime endOfLife;
        public string parameters;
        public string type; // Logo, Browser, other types
    }
}