﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using Acorn.DBConnection;
using System.Web.SessionState;
using System.Web.Security;
using System.IO;
using System.Web.Hosting;

namespace Acorn
{
    public class UserCreation
    {
        string userEmail;
        string password;
        
        string freeTrialProductIDs;
        int freeTrialDays;
        int freeTrialReleaseType;
        
        string sampleSpaceComment;

        HttpSessionState session;
        string requestBaseURL;

        Guid adminAccountID;

        string mainSchema = Util.mainSchema;
        MembershipUser mu;

        public UserCreation(string userEmail, string password, 
            Guid trialAdminAccountID, string trialProductIDs, int trialDays, int trialReleaseType, 
            string sampleSpaceComment, string requestBaseURL, HttpSessionState session)
        {
            this.userEmail = userEmail;
            this.password = password;
            this.adminAccountID = trialAdminAccountID;
     
            this.freeTrialProductIDs = trialProductIDs;
            this.freeTrialDays = trialDays;
            this.freeTrialReleaseType = trialReleaseType;          
            
            this.sampleSpaceComment = sampleSpaceComment;
            
            this.session = session;
            this.requestBaseURL = requestBaseURL;            
        }

        public MembershipUser startUserCreation()
        {
            createUser();
            configureNewUser();
            return mu;
        }
        public MembershipUser startNonAdminUserCreation()
        {
            createUser();
            configureNewNonAdminUser();
            return mu;
        }
        public void createUser()
        {
            MembershipCreateStatus mstatus;
            mu = Membership.CreateUser(userEmail, password, userEmail, null, null, false, out mstatus);            
            if (mstatus != MembershipCreateStatus.Success)
            {
                Global.systemLog.Error("Unable to create trial user : " + userEmail + " : "  + mstatus);
                throw new Exception("Unable to create trial user : " + userEmail + " : " + mstatus);
            }
            Global.systemLog.Info("New User Created : " + userEmail);           
        }
       
        public void configureNewUser()
        {
            bool sfdcTrial = session != null ? session["sfdctrial"] != null : false;
            if (freeTrialProductIDs == null || freeTrialProductIDs.Trim().Length == 0)
            {
                Global.systemLog.Error("UserCreation: No product ids found to assign to trial user - " + userEmail + " : product ids - " + freeTrialProductIDs);
                return;
            }
            string[] productIDs = freeTrialProductIDs.Trim().Split(new char[] { ',' });            
            DateTime enddate = DateTime.Now.AddDays(freeTrialDays);
            QueryConnection conn = null;
            User u = null;
            try
            {                
                conn = ConnectionPool.getConnection();
                Database.setupNewUser(conn, mainSchema, userEmail, userEmail, new List<string>(productIDs), enddate);
                u = Database.getUser(conn, mainSchema, userEmail);                
                if (sfdcTrial)
                {
                    u.DefaultDashboards = true;
                    Database.updateUser(conn, mainSchema, u);
                }
                if (freeTrialReleaseType > 0)
                {
                    u.ReleaseType = freeTrialReleaseType;
                    Database.updateUserReleaseType(conn, mainSchema, u.ID, u.ReleaseType);
                }
                if (adminAccountID != Guid.Empty && password != null)
                {
                    Database.updateCreatedUserParams(conn, mainSchema, u.ID, u.Username, adminAccountID, password);
                }
                Database.updateRenderType(conn, mainSchema, u.ID, true);
                if(session != null)
                    session["user"] = u;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            List<NewUserSpace> newUserSpaces = Util.getNewUserSpaces();
            foreach (NewUserSpace space in newUserSpaces)
            {
                if (space.valid == false)
                    continue;                
                if (Array.IndexOf<int>(space.accountTypes, u.AccountType) < 0)
                    continue;
                if (sfdcTrial && space.type != 1)
                    continue;
                Space oldsp = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    oldsp = Database.getSpace(conn, mainSchema, space.ID);
                    if (oldsp == null)
                    {
                        Global.systemLog.Error("Free Trial space does not exist - " + space.ID);
                        continue;
                    }

                    if (space.spaceSetUp == 2)
                    {
                        addTrialUserToSharedSpace(conn, mainSchema, u, oldsp);
                        addSharedUserToSpaceGroupWithACLs(conn, mainSchema, u, oldsp, space.acls);                        
                        continue;
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                
                if (sampleSpaceComment == null)
                    sampleSpaceComment = "This sample space allows you to explore pre-built reports and dashboards. To upload and analyze your own data, first create a new space.";
                Space sp = Database.copySpaceToNewSpace(oldsp, session, oldsp.Name, null, sampleSpaceComment);

                session["space"] = sp;

                if (sfdcTrial)
                {
                    u.DefaultSpace = sp.Name;
                    u.DefaultSpaceId = sp.ID;
                }
                bool netSuiteTrial = session != null ? session["netSuiteTrial"] != null : false;
                if (netSuiteTrial)
                {
                    u.DefaultSpace = sp.Name;
                    u.DefaultSpaceId = sp.ID;
                }
                MainAdminForm maf = Util.setSpace(session, sp, null);               
             
                // Setup user
                Util.addUserToSpace(maf, sp, u.Username, null);
                Util.setGroupPrivileges(sp, u.Username, u.AccountType);
                addACLsForOwnerSpaceGroup(sp, space.acls);
                Util.updateRequiresPublishFlag(maf, false);
                Util.saveApplication(maf, sp, session, null);
            }
        }

        public void configureNewNonAdminUser()
        {
            bool sfdcTrial = session != null ? session["sfdctrial"] != null : false;
            if (freeTrialProductIDs == null || freeTrialProductIDs.Trim().Length == 0)
            {
                Global.systemLog.Error("UserCreation: No product ids found to assign to trial user - " + userEmail + " : product ids - " + freeTrialProductIDs);
                return;
            }
            string[] productIDs = freeTrialProductIDs.Trim().Split(new char[] { ',' });
            DateTime enddate = DateTime.Now.AddDays(freeTrialDays);
            QueryConnection conn = null;
            User u = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.setupNewUser(conn, mainSchema, userEmail, userEmail, new List<string>(productIDs), enddate);
                u = Database.getUser(conn, mainSchema, userEmail);
                if (sfdcTrial)
                {
                    u.DefaultDashboards = true;
                    Database.updateUser(conn, mainSchema, u);
                }
                if (freeTrialReleaseType > 0)
                {
                    u.ReleaseType = freeTrialReleaseType;
                    Database.updateUserReleaseType(conn, mainSchema, u.ID, u.ReleaseType);
                }
                if (adminAccountID != Guid.Empty && password != null)
                {
                    Database.updateCreatedUserParams(conn, mainSchema, u.ID, u.Username, adminAccountID, password);
                }
                Database.updateRenderType(conn, mainSchema, u.ID, true);
                if (session != null)
                    session["user"] = u;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Space sp = null;
            Guid spaceID = Guid.Empty;
            try
                {
                    conn = ConnectionPool.getConnection();
                    spaceID = Database.getSpaceIDForAccount(conn, mainSchema, u.ID);
                    if (spaceID == null || spaceID == Guid.Empty)
                    {
                        Global.systemLog.Error("Space not found for Account");
                        return;
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            
             try
                {
                    conn = ConnectionPool.getConnection();

                    sp = Database.getSpace(conn, mainSchema, spaceID);
                    if (sp == null)
                    {
                        Global.systemLog.Error("Free Trial space does not exist for Account");
                        return;
                    }

                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }

                 session["space"] = sp;

                 if (sfdcTrial)
                 {
                     u.DefaultSpace = sp.Name;
                     u.DefaultSpaceId = sp.ID;
                 }
                 bool netSuiteTrial = session != null ? session["netSuiteTrial"] != null : false;
                 if (netSuiteTrial)
                 {
                     u.DefaultSpace = sp.Name;
                     u.DefaultSpaceId = sp.ID;
                 }
               MainAdminForm maf = Util.setSpace(session, sp, null);

               Util.addUserToSpace(maf, sp, u.Username, Util.USER_GROUP_NAME);
               Util.setGroupPrivileges(sp, u.Username, u.AccountType);
        //       addACLsForOwnerSpaceGroup(sp, space.acls);
          //      Util.updateRequiresPublishFlag(maf, false);
          //      Util.saveApplication(maf, oldsp, session, null);
            
        }

        private void addTrialUserToSharedSpace(QueryConnection conn, String schema, User toAddUser, Space sp)
        {
            Database.addSpaceMembership(conn, schema, toAddUser.ID, sp.ID, false, false);
        }

        /// <summary>
        /// add user to non-internal space groups with supplied acls
        /// </summary>
        /// <param name="toAddUser"></param>
        /// <param name="sp"></param>
        /// <param name="acls"></param>
        private void addSharedUserToSpaceGroupWithACLs(QueryConnection conn, string schema, User toAddUser, Space sp, int[] acls)
        {
            if (toAddUser != null && sp != null
                && acls != null && acls.Length > 0)
            {
                List<SpaceGroup> spaceGroups = Database.getSpaceGroups(conn, schema, sp);
                if (spaceGroups != null)
                {
                    List<int> aclsList = new List<int>(acls);
                    foreach (SpaceGroup spg in spaceGroups)
                    {
                        if (spg.InternalGroup)
                            continue;

                        if (spg.ACLIds != null)
                        {
                            foreach (int groupAclId in spg.ACLIds)
                            {
                                if (aclsList.Contains(groupAclId) && !UserAndGroupUtils.isUserGroupMember(spg, toAddUser.ID))
                                {
                                    Database.addUserToGroup(conn, schema, sp, spg, toAddUser);
                                }
                            }
                        }
                    }

                    // Make sure shared user is added to User$ group
                    SpaceGroup internalUserGroup = UserAndGroupUtils.getSpaceGroup(spaceGroups, Util.USER_GROUP_NAME, true);
                    if (internalUserGroup != null && !UserAndGroupUtils.isUserGroupMember(internalUserGroup, toAddUser.ID))
                    {
                        Database.addUserToGroup(conn, schema, sp, internalUserGroup, toAddUser);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the acls to the owner$ space group if it does not exist
        /// </summary>
        /// <param name="toAddUser"></param>
        /// <param name="sp"></param>
        /// <param name="acls"></param>
        private void addACLsForOwnerSpaceGroup(Space sp, int[] acls)
        {
            if ( sp != null && acls != null && acls.Length > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    List<SpaceGroup> spaceGroups = Database.getSpaceGroups(conn, mainSchema, sp);
                    if (spaceGroups != null)
                    {
                        foreach (SpaceGroup spg in spaceGroups)
                        {
                            if (Util.isOwnerGroup(spg) && spg.ACLIds != null)
                            {
                                foreach (int groupAclId in acls)
                                {
                                    if (ACLDetails.getACLTag(groupAclId) != null && !spg.ACLIds.Contains(groupAclId))
                                    {
                                        Database.addACLToGroup(conn, mainSchema, sp, spg.ID, groupAclId);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        /// <summary>
        /// get template verification email based on account type if different
        /// </summary>
        /// <param name="userAccountType"></param>
        /// <returns></returns>
        public static string getVerificationEmailTemplateBody(int userAccountType)
        {
            StreamReader reader = null;
            try
            {
                string verificationEmailTemplate = "Thank You for Registering Birst Express.htm";
                if (userAccountType == User.DISCOVERY_FREE_TRIAL)
                {
                    verificationEmailTemplate = "DiscoveryFreeTrialVerification.htm";
                }
                reader = new StreamReader(HostingEnvironment.MapPath("~/" + verificationEmailTemplate));
                return reader.ReadToEnd();
            }
            finally
            {
                if(reader != null)
                    reader.Close();
            }
        }

        public static void sendSignUpVerificationEmail(MailMessage mm, int userType, string requestBaseURL, Guid providerUserKey)
        {
            string replyToAddress = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"];
            string fromAddress = replyToAddress;
            if (userType == User.DISCOVERY_FREE_TRIAL)
            {
                string discoveryFreeReplyToAddress = UserSignup.discoveryTrialVerificationFromEmail;
                if (discoveryFreeReplyToAddress != null && discoveryFreeReplyToAddress.Trim().Length > 0)
                {
                    replyToAddress = fromAddress = discoveryFreeReplyToAddress;
                }
            }
            mm.ReplyToList.Add(new MailAddress(replyToAddress));
            mm.From = new MailAddress(fromAddress);
            mm.Subject = "Confirm Your Birst Signup Request";
            if (mm.Body != null)
            {
                mm.Body = mm.Body.Replace("${LINK}", requestBaseURL + "/Login.aspx?newuser=" + providerUserKey);
            }
            SmtpClient sc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                sc.EnableSsl = true;
            sc.Send(mm);            
        }
    }
}