﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Configuration.Provider;

namespace Acorn
{
    /// <summary>
    /// Handle PBKDF2 password hashing - see NIST SP 800-132
    /// 
    /// Parts of this are from:
    /// 
    /// Salted password hashing with PBKDF2-SHA1
    /// Author: havoc AT defuse.ca
    /// www: http://crackstation.net/hashing-security.htm
    /// Compatibility: .NET 3.0 and later.
    /// </summary>
    public class PBKDF2_Hash
    {
        // This can be changed without breaking existing hashes
        public const int SALT_BYTES = 32;   // 256 bits

        // convert the human readable Pseudo Random Function name to the name used in the database representation
        private static string internalPrfName(string prf)
        {
            switch (prf)
            {
                case "hmac-sha1":
                    return "1";
                case "hmac-sha256":
                    return "2";
                case "hmac-sha384":
                    return "3";
                case "hmac-sha512":
                    return "4";
                default:
                    throw new ProviderException("Bad prf name - " + prf);
            }
        }

        // convert the Pseudo Random Function used in the database representation to a human readable name
        private static string externalPrfName(string prf)
        {
            switch (prf)
            {
                case "1":
                    return "hmac-sha1";
                case "2":
                    return "hmac-sha256";
                case "3":
                    return "hmac-sha384";
                case "4":
                    return "hmac-sha512";
                default:
                    throw new ProviderException("Bad internal prf name- " + prf);
            }
        }

        // generate the hash value
        private static byte[] getHash(byte[] password, byte[] salt, string prf, int iterations)
        {
            switch (prf)
            {
                case "hmac-sha1":
                    using (var hmac = new HMACSHA1())
                    {
                        PBKDF2 df = new PBKDF2(hmac, password, salt, iterations);
                        return df.GetBytes(hmac.HashSize / 8);
                    }
                case "hmac-sha256":
                    using (var hmac = new HMACSHA256())
                    {
                        PBKDF2 df = new PBKDF2(hmac, password, salt, iterations);
                        return df.GetBytes(hmac.HashSize / 8);
                    }
                case "hmac-sha384":
                    using (var hmac = new HMACSHA384())
                    {
                        PBKDF2 df = new PBKDF2(hmac, password, salt, iterations);
                        return df.GetBytes(hmac.HashSize / 8);
                    }
                case "hmac-sha512":
                    using (var hmac = new HMACSHA512())
                    {
                        PBKDF2 df = new PBKDF2(hmac, password, salt, iterations);
                        return df.GetBytes(hmac.HashSize / 8);
                    }
                default:
                    throw new ProviderException("Bad prf name - " + prf);
            }
        }

        /// <summary>
        /// Creates a salted PBKDF2 hash of the password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>The hash of the password.</returns>
        public static string CreateHash(string pword, string prf, int iterations)
        {
            // Generate a random salt
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SALT_BYTES];
            csprng.GetBytes(salt);
            byte[] password = UTF8Encoding.UTF8.GetBytes(pword);

            byte[] hash = getHash(password, salt, prf.ToLower(), iterations);
            return "$3$" + internalPrfName(prf) + "$" + iterations + "$" + Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="goodHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public static bool ValidatePassword(string pword, string goodHash)
        {
            try
            {
                // Extract the parameters from the hash  $pbkdf2$hashname$iterations$salt:hash
                int pos = goodHash.LastIndexOf('$');
                string hashAlgorithm = goodHash.Substring(0, pos);  // $pbkdf2$hashname$iterations
                string[] algorithm = goodHash.Split(new char[] { '$' });  // { null, 3, prf, iterations }
                if (algorithm[1] != "3")
                    throw new ProviderException("Bad internal hash algorithm specification");
                string prf = algorithm[2];
                string iterationsString = algorithm[3];
                int iterations = Int32.Parse(iterationsString);

                string saltAndHash = goodHash.Substring(pos + 1);   // salt:hash
                string[] split = saltAndHash.Split(new char[] { ':' });
                byte[] salt = Convert.FromBase64String(split[0]);
                byte[] hash = Convert.FromBase64String(split[1]);
                byte[] password = UTF8Encoding.UTF8.GetBytes(pword);
                byte[] testHash = getHash(password, salt, externalPrfName(prf), iterations);
                return SlowEquals(hash, testHash);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Malformed hashvalue - " + goodHash + " (" + ex.Message + ")");
                return false;
            }
        }

        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }
    }
}
