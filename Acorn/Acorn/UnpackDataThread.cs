﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Data.Odbc;
using System.Diagnostics;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using Acorn.Background;
using System.Xml;
using Acorn.Utils;

namespace Acorn
{
    public class UnpackDataThread : BackgroundModule
    {
        private Space sp;
        private User u;
        private FileInfo fi;
        private DataTable errorTable = null;
        private Dictionary<String, Status.StatusResult> status;
        private Dictionary<BackgroundModuleObserver, object> observers;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public UnpackDataThread(Space sp, User u, FileInfo fi, Dictionary<String, Status.StatusResult> status)
        {
            this.sp = sp;
            this.u = u;
            this.fi = fi;
            this.status = status;
            observers = new Dictionary<BackgroundModuleObserver, object>();
        }

        public void addObserver(BackgroundModuleObserver bmo, object o)
        {
            observers.Add(bmo, o);
        }
        
        public void run()
        {
            if (u != null)
            {
                log4net.ThreadContext.Properties["user"] = u.Username;
                Util.setLogging(u, sp);
            } 
            UnpackFiles(); // unpack the file
        }

        public void kill()
        {
            Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Failed);
            sr.message = "Task killed, probably due to system timeout";
            lock (status)
            {
                status.Remove(fi.FullName);
                status.Add(fi.FullName, sr);
            }
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.killed(kvp.Value);
            }
        }

        public void finish()
        {
            // nothing to do here
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.finished(kvp.Value);
            }
        }

        private DataTable ScanFiles()
        {
            bool firstRowHasHeaders = true;
            int beginSkip = 0;
            int endSkip = 0;
            bool consolidateLikeSources = true;
            bool lockformat = false;
            bool ignoreinternalquotes = false;
            bool ignorecarriagereturn = false;
            bool forcenumcolumns = false;
            bool automodel = true;
            string encoding = null;
            string replacements = null;
            string quotechar = null;
            bool isLocalETLSource = false;
            string localetlloadgroup = null;
            string localetlconnection = null;
            string separator = null;
            if (File.Exists(sp.Directory + "//data//birst.upload"))
            {
                StreamReader reader = new StreamReader(sp.Directory + "//data//birst.upload", Encoding.UTF8);
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.ToLower().StartsWith("firstrowhasheaders="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(19), out result))
                        {
                            firstRowHasHeaders = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("beginskip="))
                    {
                        int result;
                        if (int.TryParse(line.Substring(10), out result))
                        {
                            beginSkip = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("endskip="))
                    {
                        int result;
                        if (int.TryParse(line.Substring(8), out result))
                        {
                            endSkip = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("consolidatelikesources="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(23), out result))
                        {
                            consolidateLikeSources = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("encoding="))
                    {
                        encoding = line.Substring(9);
                    }
                    else if (line.ToLower().StartsWith("lockformat="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(11), out result))
                        {
                            lockformat = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("ignoreinternalquotes="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(21), out result))
                        {
                            ignoreinternalquotes = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("ignorecarriagereturn="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(21), out result))
                        {
                            ignorecarriagereturn = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("forcenumcolumns="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(16), out result))
                        {
                            forcenumcolumns = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("automodel="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(10), out result))
                        {
                            automodel = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("quotechar="))
                    {
                        quotechar = line.Substring(10);
                    }
                    else if (line.ToLower().StartsWith("replacements="))
                    {
                        replacements = line.Substring(13);
                    }
                    else if (line.ToLower().StartsWith("localetlsource="))
                    {
                        isLocalETLSource = true;
                    }
                    else if (line.ToLower().StartsWith("localetlloadgroup="))
                    {
                        localetlloadgroup = line.Substring(18); ;
                    }
                    else if (line.ToLower().StartsWith("localetlconnection="))
                    {
                        localetlconnection = line.Substring(19);
                    }
                    else if (line.ToLower().StartsWith("separator="))
                    {
                        separator = line.Substring(10);
                        if (separator != null && separator.Length == 0)
                        {
                            separator = null;
                        }
                    }
                }
                reader.Close();
            }
            MainAdminForm maf = Util.getRepositoryDev(sp, u);
            ApplicationUploader uploader =
                new ApplicationUploader(maf, sp, fi.Name, firstRowHasHeaders, lockformat, ignoreinternalquotes, Util.getEscapes(replacements),
                    forcenumcolumns, quotechar, u, null, beginSkip, endSkip, false, encoding, separator, automodel);
            uploader.setIgnoreCarriageReturn(ignorecarriagereturn);
            if (isLocalETLSource && localetlloadgroup != null)
            {
                uploader.LoadGroups = localetlloadgroup;
                Util.addLoadGroup(maf, localetlloadgroup);
                Util.addScriptGroup(maf, localetlloadgroup);
            }
            errorTable = uploader.uploadFile(consolidateLikeSources, false);
            Util.saveApplication(maf, sp, null, u);
            return errorTable;
        }

        private void UnpackFiles()
        {
            // 'upload' the files
            StringBuilder response = new StringBuilder();
            Global.systemLog.Debug("Unpack: " + fi.Name);
            try
            {
                bool spaceNotAvailable = false;
                int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);
                if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                {
                    string message = SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID);
                    Global.systemLog.Error("Error in SaveFileOld: " + message);
                    response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                    response.Append(message + "\r\n");
                    spaceNotAvailable = true;
                }
                
                if (Util.checkForPublishingStatus(sp))
                {
                    Global.systemLog.Error("Space is already publishing" + sp.ID);
                    response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                    response.Append("Space is already publishing" + "\r\n");
                    spaceNotAvailable = true;
                }

                if (spaceNotAvailable)
                {
                    Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Failed);
                    sr.message = response.ToString();
                    lock (status)
                    {
                        status.Remove(fi.FullName);
                        status.Add(fi.FullName, sr);
                    }
                    return;
                }
                errorTable = ScanFiles();
                int returnValue = (errorTable.Rows.Count == 0) ? 0 : -1; ;
                if (returnValue == -1)
                {
                    returnValue = 0;
                    foreach (DataRow dr in errorTable.Rows)
                    {
                        if ((string) dr[1] == ApplicationUploader.ERROR_STR)
                        {
                            returnValue = -1;
                            break;
                        }
                    }
                }
                response.Append("Status " + String.Format("{0:000}", returnValue) + "\r\n");
                if (returnValue == -1)
                {
                    Global.systemLog.Debug("The unpacking and scanning of the files failed due to errors:");
                    response.Append("The unpacking and scanning of the files failed due to errors:\r\n");
                    for (int i = 0; i < errorTable.Rows.Count; i++)
                    {
                        DataRow dr = errorTable.Rows[i];
                        if ((string) dr[1] == ApplicationUploader.ERROR_STR)
                        {
                            Global.systemLog.Debug(dr[0] + ": " + dr[1] + ": " + dr[2]);
                            response.Append(dr[0] + ": " + dr[1] + ": " + dr[2] + "\r\n");
                        }
                    }
                    Global.systemLog.Debug("Failed to unpack/scan " + fi.FullName);
                    Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Failed);
                    sr.message = response.ToString();
                    lock (status)
                    {
                        status.Remove(fi.FullName);
                        status.Add(fi.FullName, sr);
                    }
                }
                else
                {
                    response.Append("Successfully unpacked/scanned " + fi.FullName + "\r\n");
                    for (int i = 0; i < errorTable.Rows.Count; i++)
                    {
                        DataRow dr = errorTable.Rows[i];
                        Global.systemLog.Debug(dr[0] + ": " + dr[1] + ": " + dr[2]);
                        response.Append(dr[0] + ": " + dr[1] + ": " + dr[2] + "\r\n");                        
                    }
                    Global.systemLog.Debug("Successfully unpacked/scanned " + fi.FullName);
                    Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Complete);
                    sr.message = response.ToString();
                    lock (status)
                    {
                        status.Remove(fi.FullName);
                        status.Add(fi.FullName, sr);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("Exception in UnpackFiles: " + ex.Message, ex);
                response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                response.Append("Exception in UnpackFiles: " + ex.Message);
                Global.systemLog.Debug("Failed to unpack/scan " + fi.FullName);
                Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Failed);
                sr.message = response.ToString();
                lock (status)
                {
                    status.Remove(fi.FullName);
                    status.Add(fi.FullName, sr);
                }
            }
            return;
        }

        #region BackgroundModule Members


		public Status.StatusResult getStatus()
        {
            Status.StatusResult sr = null;
            lock (status)
            {
                status.TryGetValue(fi.FullName, out sr);
            }
            return sr;
        }

        #endregion
    }
}

