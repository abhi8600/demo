﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response);
            string logoutRedirectPage = null;
            System.Collections.Generic.Dictionary<String, String> overrideParameters = null;
            FlexModule flexModule = new FlexModule(); // XXX there needs to be a better way to get this than via 'FlexModule'
            string Locale = flexModule.GetUserLocale();
            User u = Util.validateAuth(Session, Response);
            overrideParameters = Util.getLocalizedOverridenParams(Locale, u);

            if (overrideParameters != null && overrideParameters.Count > 0 && overrideParameters.ContainsKey(OverrideParameterNames.LOGOUT_URL))
            {
                if (overrideParameters[OverrideParameterNames.LOGOUT_URL] != "")
                {
                    logoutRedirectPage = overrideParameters[OverrideParameterNames.LOGOUT_URL];
                }
                else
                {
                    logoutRedirectPage = Util.getLogoutRedirectURL(Session);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Session["LOGOUT_PAGE"] as string))
                {
                    logoutRedirectPage = (String)Session["LOGOUT_PAGE"];
                }
                else
                {
                    logoutRedirectPage = Util.getLogoutRedirectURL(Session);
                }
            }
            SSO.logout(Session, Request, Response);
            Response.Redirect(logoutRedirectPage);
        }
    }
}
