﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration.Provider;

namespace Acorn
{
    /// <summary>
    /// Support the ability for customers to choose among a selection of password hashing functions
    /// - currently PBKDF2 (using SHA1 to SHA512) and BCrypt
    /// </summary>
    public class Password
    {
        public static bool VerifyPassword(string password, string hashedvalue, string username)
        {
            string hashVersion = getHashAlgorithm(hashedvalue);

            if (hashVersion != null)
            {
                if (hashVersion.StartsWith("$2a$"))
                    return BCryptHash.ValidatePassword(password, hashedvalue);
                else if (hashVersion.StartsWith("$3$"))
                    return PBKDF2_Hash.ValidatePassword(password, hashedvalue);
                else if (hashVersion.StartsWith("$base$"))
                    return MD5Hash.ValiatePassword(password, hashedvalue, username);
                else
                    throw new ProviderException("Unsupported password hash algorithm: " + hashVersion);
            }

            // old version (md5 salted with the username and REALM) - this will eventuall go away as this form of hashing is removed
            return MD5Hash.ValiatePassword(password, hashedvalue, username);
        }

        // get the hash version
        //
        //   newer versions use the OpenBSD format: $[HashAlgorithmIdentifier]$[AlgorithmSpecificData]
        //
        //   base:      MD5 - deprecated
        //   2a:        BCrypt, which specifies passwords as UTF-8 encoded
        //   3:         PBKDF2, with prf and iterations (NIST SP 800-132)
        //
        //   Cost is a cost factor used when computing the hash. The "current" value is 10, meaning the internal key setup goes through 1,024 rounds
        //
        //   10: 2^10 = 1,024 iterations
        //   11: 2^11 = 2,048 iterations
        //   12: 2^12 = 4,096 iterations
        //
        //   The base64 algorithm used by the OpenBSD password file is not the same Base64 encoding that everybody else uses; they have their own:
        //
        //   Regular Base64 Alphabet: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/
        //   BSD Base64 Alphabet: ./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
        //
        //   bcrypt is $2a$[Cost]$[Base64Salt][Base64Hash]
        //   http://codahale.com/how-to-safely-store-a-password/
        //   https://en.wikipedia.org/wiki/Bcrypt
        //   http://www.openbsd.org/papers/bcrypt-paper.pdf
        //
        private static string getHashAlgorithm(string hashedvalue)
        {
            int pos = hashedvalue.LastIndexOf('$');
            if (pos < 0)
                return null; // old version
            return hashedvalue.Substring(0, pos + 1);
        }

        // hash the password using a specific hash version
        // hash algorithm is alg[:parameters...]
        public static string HashPassword(string password, string username, string hashAlgorithm)
        {
            if (hashAlgorithm == null || hashAlgorithm.Equals("base")) // base is now 'pbkdf2:hmac-sha1:10000'
                return CreatePBKDF2Hash(password, PasswordPolicy.HASHALGORITHM);
            else if (hashAlgorithm.StartsWith("bcrypt:"))   // bcrypt:costFactor
            {
                // get cost factor
                int pos = hashAlgorithm.LastIndexOf(':');
                if (pos < 0)
                    throw new ProviderException("Bad parameter for bcrypt:costFactor - " + hashAlgorithm);
                string temp = hashAlgorithm.Substring(pos + 1);
                int costFactor = Int32.Parse(temp);
                return BCryptHash.CreateHash(password, costFactor);
            }
            else if (hashAlgorithm.StartsWith("pbkdf2:"))   // pbkdf2:prf:iterations
            {
                return CreatePBKDF2Hash(password, hashAlgorithm);
            }
            throw new ProviderException("Unsupported password hash version: " + hashAlgorithm);
        }

        private static string CreatePBKDF2Hash(string password, string hashAlgorithm)
        {
            // get iterations
            string[] split = hashAlgorithm.Split(new char[] { ':' });
            if (split.Length != 3)
                throw new ProviderException("Bad parameters for pbkdf2:pdf:iterations - " + hashAlgorithm);
            string prf = split[1];
            int iterations = Int32.Parse(split[2]);
            return PBKDF2_Hash.CreateHash(password, prf, iterations);
        }
    }
}