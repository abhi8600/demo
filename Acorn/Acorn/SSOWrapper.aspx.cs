﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.IO;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net.Security;
using System.Collections.Specialized;
using Acorn.DBConnection;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;

namespace Acorn
{
    public partial class SSOWrapper : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response, true);

            if (Request["birst.OpenID"] == "true")
            {
                OpenIDFormSubmission();
                return;
            }
            Response.StatusCode = 400;
            Global.systemLog.Warn("SSOWrapper: no SSO method chosen");
            return;
        }

        // OpenID code
        protected HtmlInputControl openid_identifier;
        private static OpenIdRelyingParty openid = new OpenIdRelyingParty();

        private void OpenIDFormSubmission()
        {
            var response = openid.GetResponse();
            if (response == null)
            {
                Identifier id;
                string openid_identifier = Request["birst.OpenIDURL"];
                if (Identifier.TryParse(openid_identifier, out id))
                {
                    try
                    {
                        IAuthenticationRequest request = openid.CreateRequest(openid_identifier);
                        request.RedirectToProvider();
                    }
                    catch (ProtocolException ex)
                    {
                        writeError(ex.Message);
                    }
                }
                return;
            }

            switch (response.Status)
            {
                case AuthenticationStatus.Authenticated:
                    UriIdentifier id = (UriIdentifier)response.ClaimedIdentifier;
                    ClaimsResponse claimsResponse = response.GetExtension<ClaimsResponse>();
                    string userName = response.ClaimedIdentifier.ToString();
                    loginUser(userName, claimsResponse);
                    break;
                case AuthenticationStatus.Canceled:
                    writeError("OpenID authentication request canceled at the OpenID provider");
                    break;
                case AuthenticationStatus.Failed:
                    writeError("OpenID authentication request failed - " + response.Exception.Message);
                    break;
            }
        }

        private void writeError(string message)
        {
            OpenIDLabel.Text = HttpUtility.HtmlEncode(message);
            OpenIDLabel.Visible = true;
            Global.systemLog.Info(message);
            Response.Status = "400 Bad Request";
        }

        private void loginUser(string userName, ClaimsResponse claimsResponse)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                // get the OpenID/Birst User association
                Guid userId = Database.getUserIdForOpenId(conn, mainSchema, userName);
                if (userId == Guid.Empty)
                {
                    writeError("The OpenID (" + userName + ") is not associated with a Birst user");
                    return;
                }
                // get the actual Birst User
                User u = Database.getUserById(conn, mainSchema, userId);
                if (u == null)
                {
                    writeError("The Birst user associated with the OpenID is not in the database or has expired products - " + userName);
                    return;
                }
                if (u.Disabled)
                {
                    writeError("The Birst user associated with the OpenID has been disabled - " + userName);
                    return;
                }
                Util.setLogging(u, null);
                // determine the site redirection
                string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
                string redirectURL = null;
                // no site means go to the original URL (backwards compatibility)
                if (userRedirectedSite == null || userRedirectedSite.Length == 0)
                {
                    redirectURL = Util.getRequestBaseURL(Request);
                }
                else
                {
                    redirectURL = userRedirectedSite;
                }

                string id = Request["birst.spaceId"];
                Guid spaceId = Guid.Empty;
                if (id != null && id.Length > 0)
                {
                    try
                    {
                        spaceId = new Guid(id);
                    }
                    catch (Exception)
                    {
                        writeError("Bad Birst Space Id - " + id);
                        return;
                    }
                }

                // get the Birst security token and validate the user (products, IP restrictions)
                bool lockedout = false;
                string sessionVarsParam = Request.Params["birst.sessionVars"];
                string token = TokenGenerator.getOpenIDSSOSecurityToken(conn, mainSchema, u, spaceId, sessionVarsParam, Request.UserHostAddress, ref lockedout);
                if (token == null || token.Length == 0)
                {
                    if (lockedout)
                    {
                        writeError("Login has been locked after too many unsuccessful attempts, please contact support to unlock");
                    }
                    return;
                }
                // once authenticated, redirect to the SSO servlet
                token = Util.encryptToken(token);
                string redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString);
                Response.Redirect(redirectUrl);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
