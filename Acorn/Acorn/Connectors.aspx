﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Connectors.aspx.cs" Inherits="Acorn.Connectors"
    Title="Birst" MasterPageFile="~/WebAdmin.Master" %>

<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div style="height: 10px">
        &nbsp;</div>
    <div class="pagepadding">
            <div style="font-size: 9pt">
                <a href="FlexModule.aspx?birst.module=admin">Admin</a>
            </div>
        <div style="height: 10px">
            &nbsp;</div>
        <div class="pageheader">
            Application Connectors
        </div>
        <div style="height: 10px">
            &nbsp;</div>
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="GoogleAnalyticsButton" Enabled="true" runat="server" ImageUrl="~/Images/Google_Connect.png"
                        PostBackUrl="~/AddGoogle.aspx" />
                </td>
                <td>
                    <asp:LinkButton ID="GoogleAnalyticsLink" Enabled="true" runat="server" PostBackUrl="~/AddGoogle.aspx"
                        CssClass="defaultlinkstyle">Connect to Google Analytics to analyze and report on your web interactions.</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="SalesforceButton" runat="server" ImageUrl="~/Images/SFDC_Connect.jpg"
                        PostBackUrl="~/AddSalesforce.aspx" />
                </td>
                <td>
                    <asp:LinkButton ID="SalesforceLink" runat="server" PostBackUrl="~/AddSalesforce.aspx"
                        CssClass="defaultlinkstyle">Connect to your Salesforce.com account to analyze both standard and custom objects.</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
