﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Performance_Optimizer_Administration;
using System.IO;
using System.Data;
using System.Web.SessionState;
using Acorn.Utils;
using System.Drawing;
using System.Drawing.Imaging;

namespace Acorn
{
    public partial class BrowserUpload : System.Web.UI.Page
    {
        public static long MAX_BYTE_SIZE = 10485760;
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response);
            UploadToken uploadToken = Util.validateAndGetUploadToken(Request, Response);
            if (uploadToken == null)
            {
                // validate method will write the corresponding status codes and messages
                return;
            }
            Global.systemLog.Debug("Received valid uploadToken : " + uploadToken.tokenID);
            User u = Util.getUserById(uploadToken.userID);

            string uploadType = uploadToken.type;
            if (uploadType == "data" || uploadType == "logo" || uploadType == "template" || uploadType == "privatekey")
            {
                Space sp = Util.getSpaceById(uploadToken.spaceID);
                if (!Util.isSpaceAdmin(u, sp, null))
                {
                    Global.systemLog.Debug("redirecting to home page due to not being the space admin (admin)");
                    Response.Redirect(Util.getHomePageURL(null, Request));
                }
                if (uploadType == "data")
                {
                    uploadDataFiles(sp);
                }
                else if (uploadType == "logo")
                {
                    uploadLogo(sp);
                }
                else if (uploadType == "template")
                {
                    uploadTemplateAttachment(sp);
                }
                else if (uploadType == "privatekey")
                {
                    uploadPrivateKeyFile(sp);
                }
            }
            else if (uploadType == "command")
            {
                uploadCommandFile(u);
            }
           
        }

        private void uploadDataFiles(Space sp)
        {
            HttpFileCollection uploadedFiles = Request.Files;

            if (uploadedFiles.Count == 0)
            {
                Response.Clear();
                Response.Write("error=true");
                return;
            }

            HttpPostedFile postedFile = uploadedFiles[0];

            if (postedFile != null && postedFile.ContentLength > 0)
            {
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                if (fileName.Length == 0)
                {
                    Response.Clear();
                    Response.Write("error=true");
                    return;
                }

                try
                {
                    string dataPath = Path.Combine(sp.Directory, "data");
                    string savedFileName = Path.Combine(dataPath, postedFile.FileName);
                    Util.validateFileLocation(dataPath, savedFileName);
                    postedFile.SaveAs(savedFileName);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex, ex);
                    Response.Clear();
                    Response.Write("error=Error during upload, Please try again");
                    return;
                }
                Response.Clear();
                Response.Write("success=true");
            }
            else 
            {
                Response.Clear();
                Response.Write("error=Error during upload, Please try again");
                return;
            }
        }

        private void uploadLogo(Space sp)
        {
            HttpPostedFile postedFile = Request.Files[0];
            Response.Clear();
            if (postedFile != null)
            {       
                using (BinaryReader reader = new BinaryReader(postedFile.InputStream))
                {
                    byte[] image = reader.ReadBytes(postedFile.ContentLength);
                    setLogo(image, sp);
                    Response.Write("success=true");
                }
            }        
        }

        public static System.Drawing.Image byteArrayToImage(byte[] bmpBytes)
        {
            System.Drawing.Image image = null;
            using (MemoryStream stream = new MemoryStream(bmpBytes))
            {
                image = System.Drawing.Image.FromStream(stream);
            }

            return image;
        }      
        public static void setLogo(byte[] image, Space sp)
        {
                if (image.Length > MAX_BYTE_SIZE)
                {
                    WebServiceSessionHelper.warn("setLogo", "Image size is larger than " + MAX_BYTE_SIZE);
                    return;
                }

                sp.getSettings();
                if (sp.Settings == null)
                    sp.Settings = new SpaceSettings();
                sp.Settings.LogoImage = image;
                System.Drawing.Image convertedImage =  byteArrayToImage(image);
                string type = "jpeg";
                 if(ImageFormat.Jpeg.Equals(convertedImage.RawFormat))
                      type = "jpeg";
                 else if(ImageFormat.Jpeg.Equals(convertedImage.RawFormat))                    
                      type = "png";
                 else if (ImageFormat.Jpeg.Equals(convertedImage.RawFormat))
                      type = "gif";
                sp.Settings.LogoImageType = type;
                sp.saveSettings();         
            
        }   
         public static void setDefaultLogo(byte[] birstlogo, Space sp)
        {               
                sp.getSettings();
                if (sp.Settings == null)
                    sp.Settings = new SpaceSettings();
                sp.Settings.LogoImage = birstlogo;
                System.Drawing.Image convertedImage =  byteArrayToImage(birstlogo);
                String type = "png";
                sp.Settings.LogoImageType = type;
                sp.saveSettings();         
            
        }   
        private static long TEMPLATE_MAX_ATTACHMENT_SIZE = 10000000;
        private void uploadTemplateAttachment(Space sp)
        {
            HttpPostedFile postedFile = Request.Files[0];
            //Response.Clear();
            if (postedFile != null)
            {
                string fileName = System.IO.Path.GetFileName(Request.Files[0].FileName);
                if (fileName.Length == 0)
                    return;

                if (postedFile.ContentLength > TEMPLATE_MAX_ATTACHMENT_SIZE)
                {
                    Session["ErrorOutput"] = "Exceeded Maximum Allowed Attachment Size";
                    Response.Write("error=Exceeded Maximum Allowed Attachment Size");
                    return;
                }

                string dir = Path.Combine(sp.Directory, "temp");
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                string fname = Path.Combine(dir, fileName);
                Util.validateFileLocation(dir, fname);
                postedFile.SaveAs(fname);
                Response.Clear();
                Response.Write("success=true");
            }
        }

        private static long PRIVATE_KEY_MAX_ATTACHMENT_SIZE = 512000;
        private void uploadPrivateKeyFile(Space sp)
        {
            HttpPostedFile postedFile = Request.Files[0];
            if (postedFile != null)
            {
                Response.Clear();
             
                string fileName = System.IO.Path.GetFileName(Request.Files[0].FileName);
                if (fileName.Length == 0)
                    return;

                if (postedFile.ContentLength > PRIVATE_KEY_MAX_ATTACHMENT_SIZE)
                {
                    Response.Write("error=Exceeded Maximum Allowed Attachment Size");
                    return;
                }

                if (!postedFile.FileName.EndsWith(".p12"))
                {
                    Response.Write("error=Invalid file type. Only \"Private Key (.p12)\" file allowed");
                    return;
                }

                string dir = Path.Combine(sp.Directory, Util.CONNECTOR_CONFIG_DIR);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                string fname = Path.Combine(dir, fileName);
                Util.validateFileLocation(dir, fname);
                postedFile.SaveAs(fname);
                Response.Write("success=true");
            }
        }


        private void uploadCommandFile(User user)
        {
            HttpPostedFile postedFile = Request.Files[0];

            if (postedFile != null && postedFile.ContentLength > 0)
            {
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                try
                {
                    string savedFileName = UserManagementUtils.getTempFileName(user.ID, postedFile.FileName);
                    postedFile.SaveAs(savedFileName);
                    // This will later retrieved and deleted during command processing
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex, ex);
                    Response.Clear();
                    Response.Write("error=Error during upload, Please try again");
                    return;
                }
                Response.Clear();
                Response.Write("success=true");
            }
            else
            {
                Response.Clear();
                Response.Write("error=Error during upload, Please try again");
                return;
            }
        }
    }    
}
