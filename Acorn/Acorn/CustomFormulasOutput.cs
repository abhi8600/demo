﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class CustomFormulasOutput
    {
        public LogicalExpression LogicalExpression;
        public string[] Sources;
        public bool Valid;
        public bool Imported;
    }
}
