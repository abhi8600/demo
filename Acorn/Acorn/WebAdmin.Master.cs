using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Performance_Optimizer_Administration;
using System.IO;
using System.Data.Odbc;
using Acorn.DBConnection;
using Acorn.Utils;
using Acorn.SAMLSSO;

namespace Acorn
{
    public partial class MainSite : System.Web.UI.MasterPage
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private static string termsOfServiceURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TermsOfServiceLink"];
        private static string privacyPolicyURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PrivacyPolicyLink"];
        private static string contactUsURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContactUsLink"];
        private static string copyRight = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["copyRight"];


        public string fgcolor;

        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlGenericControl objScript = new HtmlGenericControl("script");
            objScript.Attributes.Add("type", "text/javascript");
            objScript.Attributes.Add("src", "KeepAliveSMIWeb.js");
            if (objScript != null)
            {
                Control keepAliveCtrl = Page.FindControl("KeepAlive");
                if (keepAliveCtrl != null)
                    keepAliveCtrl.Controls.Add(objScript);
            }
            if (!string.IsNullOrEmpty(Session["SAMLTIMEOUT"] as string) && Session["SAMLTIMEOUT"].Equals("true"))
            {
                //Session timeout redirect script
                String logoutScript = "<script src=\"SAMLSSO/jquery-2.0.0.min.js\"></script>\n" +
                    "<script type=\"text/javascript\">\n\tvar tTimeOut= 30 * 60; //secs to timeout\n\tvar lTimrOutURL=\"Logout.aspx\"; //redirect to URL when timeout happens\n" +
                    "var idleInterval = setInterval(\"timerIncrement()\", 1000);\n\tvar idleTime = 0;\n    //Zero the idle timer on mouse down.\n    $(this).mousedown(function (e) {\n    idleTime = 0;\n" +
                    "    }); //zero out timer on keydown\n    $(this).keypress(function (e) {\n        idleTime = 0;\n    });\n\tfunction timerIncrement() {\n" +
                    "\t\tidleTime++;\n\t\tif (idleTime > tTimeOut) {\n\t\t\tclearInterval(idleInterval);\n\t\t\tdocument.location = lTimrOutURL; \n\t\t}\n\t}\n</script>\n";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "TimeoutRedirect", logoutScript);
            }

            User u = Util.validateAuth(Session, Response);

            //check permissions
            AccountHeaderPlaceHolder.Visible = DefaultUserPage.isSettingsLinkVisible(Session, u);
            UpdatedHome.Visible = DefaultUserPage.isHomePermitted(Session, u);

            if ((!UpdatedHome.Visible || !AccountHeaderPlaceHolder.Visible) && (!AdhocHeaderPlaceHolder.Visible && !DashboardHeaderPlaceHolder.Visible))
            {
                HeaderBlank.Visible = true;
            }


            if (u.isFreeTrialUser)
                HelpLink.NavigateUrl = "Help/BXE/index.htm";

            SupportLinkPlaceHolder.Visible = UserManagementUtils.isUserAuthorized(u);


            //set override parameters if any
            System.Collections.Generic.Dictionary<String, String> overrideParameters = null;
            FlexModule flexModule = new FlexModule();
            string Locale = flexModule.GetUserLocale();

            overrideParameters = Util.getLocalizedOverridenParams(Locale, u);

            if (overrideParameters != null && overrideParameters.Count > 0)
            {
                if (overrideParameters.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                {
                    if (overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL] != "")
                    {
                        TermsOfServiceLink.NavigateUrl = overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL];
                    }
                    else
                    {
                        TermsOfServicePlaceHolder.Visible = false;
                    }
                }
                else
                {
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                {
                    if (overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL] != "")
                    {
                        PrivacyPolicyLink.NavigateUrl = overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL];
                    }
                    else
                    {
                        PrivacyPolicyPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                {
                    if (overrideParameters[OverrideParameterNames.CONTACT_US_URL] != "")
                    {
                        ContactUsLink.NavigateUrl = overrideParameters[OverrideParameterNames.CONTACT_US_URL];
                    }

                    else
                    {
                        ContactUsPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    ContactUsLink.NavigateUrl = contactUsURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                {
                    if (overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT] != "")
                    {
                        copyRight = overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT];
                        Copyright.Text = HttpUtility.HtmlEncode(copyRight);

                    }
                    else
                    {
                        Copyright.Text = "";
                    }

                }
                else
                {
                    Copyright.Text = HttpUtility.HtmlEncode(copyRight);
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                {
                    if (overrideParameters[OverrideParameterNames.SUPPORT_URL] != "")
                    {
                        SupportLink.NavigateUrl = overrideParameters[OverrideParameterNames.SUPPORT_URL];
                    }

                    else
                    {
                        SupportLinkPlaceHolder.Visible = false;
                    }
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                {
                    if (overrideParameters[OverrideParameterNames.PAGE_TITLE] != "")
                    {
                        WebAdminMasterHeadTag.Title = overrideParameters[OverrideParameterNames.PAGE_TITLE];
                    }
                    else
                    {
                        WebAdminMasterHeadTag.Title = "";
                    }
                }
                else
                {
                    WebAdminMasterHeadTag.Title = "Birst";
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.ERROR_URL))
                {
                    if (overrideParameters[OverrideParameterNames.ERROR_URL] != "")
                    {
                        Session[OverrideParameterNames.ERROR_URL] = overrideParameters[OverrideParameterNames.ERROR_URL];
                    }
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.HELP_URL))
                {
                    if (overrideParameters[OverrideParameterNames.HELP_URL] != "")
                    {
                        HelpLink.NavigateUrl = overrideParameters[OverrideParameterNames.HELP_URL];
                    }
                    else
                    {
                        HelpLink.Visible = false;
                    }
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                {
                    if (overrideParameters[OverrideParameterNames.LOGOUT_URL] != "")
                    {
                        LogoutLink.NavigateUrl = overrideParameters[OverrideParameterNames.LOGOUT_URL];                        
                    }
                    else
                    {
                        LogoutLink.Visible = false;
                    }
                }
            }
            else
            {
                string copyRight = Util.getCopyright();

                if (termsOfServiceURL != null)
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                else
                    TermsOfServicePlaceHolder.Visible = false;
                if (privacyPolicyURL != null)
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                else
                    PrivacyPolicyPlaceHolder.Visible = false;
                if (contactUsURL != null)
                    ContactUsLink.NavigateUrl = contactUsURL;
                else
                    ContactUsPlaceHolder.Visible = false;

                LogoutLink.NavigateUrl = "/Logout.aspx";

                HelpLink.NavigateUrl = "/Help/Full/index.htm";

                SupportLink.NavigateUrl = "/Support.aspx";

                WebAdminMasterHeadTag.Title = "Birst";

                Copyright.Text = HttpUtility.HtmlEncode(copyRight);
            }



            Version.Text = HttpUtility.HtmlEncode(Util.getAppVersion());

            Space sp = (Space)Session["space"];

            Util.formatPage(sp, u, LogoLiteral, HeaderColorStyle, out fgcolor);

            styleID.DataBind();
        }
    }
}
