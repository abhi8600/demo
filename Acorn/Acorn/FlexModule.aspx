﻿<%@ Page Title="Birst" Language="C#" AutoEventWireup="True" CodeBehind="FlexModule.aspx.cs" Inherits="Acorn.FlexModule" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   
    <head id="header" runat="server" >
        <title><asp:Literal ID="TitleLiteral" runat="server" Text="Birst"/></title>
        <asp:Literal ID="TitleLinkLiteral" runat="server">
           <link rel="shortcut icon" href="/FaviconIconUrl.aspx" />
            <link rel="icon" href="/FaviconIconUrl.aspx"/>
       </asp:Literal>  
       <link href="global.css" type="text/css" rel="Stylesheet" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'/>
       <link rel="Stylesheet" type="text/css" href="/html/resources/css/ext-all.css" />
       <link rel="Stylesheet" type="text/css" href="/html/resources/css/header.css" />

    <asp:Literal ID="HeaderColorStyle" runat="server"><style type="text/css">
                                                          #headerstyle
                                                          {
                                                              background-color: #E7E1D8;
                                                          }
                                                      </style></asp:Literal>
    <style type="text/css">
        #logostyle
        {
          width: 20%;   
            vertical-align: middle;
            background-repeat: no-repeat;
            padding: 5px 10px;          
            background-image: url(<asp:Literal ID="LogoLiteral" runat="server" >/Logo.aspx</asp:Literal>);
        }
       
    </style>
    <asp:Literal ID="SwfSizeLiteral" runat="server">
        <style type="text/css">
        #moduleDiv
        {
            height :100%;
        }
        </style>
    </asp:Literal>
        <style type="text/css">
        html, body { 
            height: 100%;
            width:100%;
            margin: 0px; 
            overflow:hidden;
            zoom: 1;
        }
        
        #mainForm {
            height:100%;
        }
        
        #headerTable {
            height: 44px; 
            width: 100%; 
            padding: 0 5px 0 5px;
            font-size: 10pt; 
            font-weight: normal; 
            text-align: right;
        }
        
        #headerTable A 
        {
            color: <%= this.fgcolor %>;            
            text-decoration: none;
        }
        
        .headerTd {
            width: 20%; 
            color: <%= this.fgcolor %>;
            font-size: 10pt; 
            font-weight: bold;
            text-align: left; 
            padding: 0 5px 0 5px;
        }
        
        .headerSpaceName, #idHeaderContainer .x-btn-inner 
        {
            color: <%= this.fgcolor %>;
        }
        
        .linkTd {
            color: <%= this.fgcolor %>; 
            font-size: 10pt;
            font-weight: bold;
            padding: 0 5px 0 5px; 
        }

        .centerTd 
        {
            color: <%= this.fgcolor %>;
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            padding: 0 5px 0 5px; 
        }
    </style>

    <script type="text/javascript" src="/html/ext-all.js" ></script>
    <script type="text/javascript" src="/html/header.js" ></script> 
   <script type="text/javascript">
       function IEVersion() {
           var _n = navigator, _w = window, _d = document;
           var version = "NA";
           var na = _n.userAgent;
           var ieDocMode = "NA";
           var ie8BrowserMode = "NA";
           var isIE = false;
           if (/msie/i.test(na) && (!_w.opera)) {
               isIE = true;
               if (_w.attachEvent && _w.ActiveXObject) {
                   version = (na.match(/.+ie\s([\d.]+)/i) || [])[1];
                   if (parseInt(version) == 7) {
                       if (_d.documentMode) {
                           version = 8;
                           if (/trident\/\d/i.test(na)) {
                               ie8BrowserMode = "Compat Mode";
                           } else {
                               ie8BrowserMode = "IE 7 Mode";
                           }
                       }
                   } else if (parseInt(version) == 8) {
                       if (_d.documentMode) { ie8BrowserMode = "IE 8 Mode"; }
                   }

                   if (parseInt(version) >= 9) {
                       replaceHeaderWithHtmlHeader();
                   }
                   else {
                       var header = document.getElementById("headerstyle");
                       if (header) {
                           header.style.display = "block";
                       }
                   }
                   ieDocMode = (_d.documentMode) ? _d.documentMode : (_d.compatMode && _d.compatMode == "CSS1Compat") ? 7 : 5;
               }
           }

           if (isIE == false) {
               replaceHeaderWithHtmlHeader();
           }

           return {
               "isIE": isIE,
               "UserAgent": na,
               "Version": version,
               "BrowserMode": ie8BrowserMode,
               "DocMode": ieDocMode
           }
       }

       function getCookies() {
           return document.cookies;
       }

       function getCookie(c_name) {
           var i, x, y, ARRcookies = document.cookie.split(";");
           for (i = 0; i < ARRcookies.length; i++) {
               x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
               y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
               x = x.replace(/^\s+|\s+$/g, "");
               if (x == c_name) return unescape(y);
           }
       }

       function replaceHeaderWithHtmlHeader() {

           if (document.headerUpdated === undefined) {
               document.headerUpdated = 1;
               var header = document.getElementById("headerstyle");
               if (header) {
                   var headerLabel = document.getElementById("HeaderUserName");
                   var userName = "";
                   if (headerLabel) {
                       userName = headerLabel.innerHTML;
                   }
                   var helpLink = document.getElementById("HelpLink");
                   var helpUrl = document.getElementById("BirstHelpUrl");
                   if (helpLink && helpLink.href && helpUrl) {
                       helpUrl.value = helpLink.href;
                   }

                   var supportLink = document.getElementById("SupportLink");
                   var supportUrl = document.getElementById("BirstSupportUrl");
                   if (supportLink && supportLink.href && supportUrl) {
                       supportUrl.value = supportLink.href;
                   }

                   var logoutLink = document.getElementById("LogoutLink");
                   var logOutUrl = document.getElementById("BirstLogoutUrl");
                   if (logoutLink && logoutLink.href && logOutUrl) {
                       logOutUrl.value = logoutLink.href;
                   }

                   while (header.firstChild) {
                       header.removeChild(header.firstChild);
                   }

                   header.style.display = "block";

                   Birst.LM.Locale.load(this, function () {
                       var panel = Ext.create('Birst.marginals.header', {
                           id: 'idHeaderContainer',
                           autoHeight: true,
                           bodyBorder: false//,
                           //                    renderTo: header
                       });
                       panel.render(header);
                       var menu = Ext.getCmp('headerMenuLabel');
                       if (userName) {
                           //				menu.btnInnerEl.dom.innerHTML = userName;
                           menu.setText(userName);
                           Ext.getCmp('headerToolbar').doLayout();
                       }
                   }, 'home');
               }
           }
       }

       function fixupModuleDiv() {
           var b = IEVersion();
           var main = document.getElementById("main");
           var header = document.getElementById("headerstyle");
           var footer = document.getElementById("footerstyle");
           var percentage = 100;
           var offset = 0;
           if (header != null) {
               offset = offset + header.offsetHeight;
           }
           if (footer != null) {
               offset = offset + footer.offsetHeight;
           }
           if (main != null) {
               percentage = ((main.offsetHeight - offset) / (main.offsetHeight)) * 100;
           }
           var div = document.getElementById("moduleDiv");
           div.style.height = percentage + '%';
       }
    </script>
    <!-- <script type="text/javascript" src="history.js"></script> -->
    <script type="text/javascript" src="embeddedIEHelper.js"></script>
    <link href="history.css" type="text/css" rel="Stylesheet" />
</head>
<body onload="fixupModuleDiv();">
    <input id="birst-locale-plchldr" type="hidden" value="<% =GetUserLocale() %>"/> 
    <input id="BirstSupportUrl" type="hidden" runat="server" value="Support.aspx" />
    <input id="BirstHelpUrl" type="hidden" runat="server" value="Help/Full/index.htm"/>   
     <input id="BirstLogoutUrl" type="hidden" runat="server" value="Logout.aspx"/> 

   <div id="main" style="height:100%">
<%       
if (Page.Request.QueryString["showHeader"] != "false"){
%>
     
       <div id="headerstyle" runat="server" style="height:50px;display:none;" >
       <form id="form1" runat="server">
        <table id="headerTable" >
            <tr>
                <td id="logostyle">
                </td>
                <td class="headerTd">                    
                    <asp:Label ID="HeaderUserName" runat="server"/>
                </td>
                <td class="headerTd">
                    <asp:Label ID="HeaderSpaceName" runat="server"/>
                </td>
                <asp:PlaceHolder ID="UpdatedHome" runat="server">
                    <td class="centerTd">
                        <asp:HyperLink ID="HomeLink" NavigateUrl="FlexModule.aspx?birst.module=home" Target="_top" Text="Home" runat="server" />
                    </td>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="HeaderBlank" runat="server" Visible="false">
                </asp:PlaceHolder>             
                <asp:PlaceHolder ID="VisualizationPlaceHolder" runat="server" Visible="false">
                    <td class="centerTd">
                        <asp:HyperLink ID="Visualizer" NavigateUrl="FlexModule.aspx?birst.module=visualization" Target="_top" Text="Explorer" runat="server" />
                    </td>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="AdhocHeaderPlaceHolder" runat="server" Visible="false">
                    <td class="centerTd">
                        <asp:HyperLink ID="AdhocLink2" NavigateUrl="FlexModule.aspx?birst.module=designer" Target="_top" Text="Designer" runat="server" />
                    </td>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="DashboardHeaderPlaceHolder" runat="server" Visible="false">
                    <td class="centerTd">
                        <asp:HyperLink ID="DashboardLink" NavigateUrl="FlexModule.aspx?birst.module=dashboard" Target="_top" Text="Dashboards" runat="server" />
                    </td>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="AdminHeaderPlaceHolder" runat="server" Visible="false">
                    <td class="centerTd">
                        <asp:HyperLink ID="AdminLink" NavigateUrl="FlexModule.aspx?birst.module=admin" Target="_top" Text="Admin" runat="server" />
                    </td>
                </asp:PlaceHolder> 
                <asp:PlaceHolder ID="AccountHeaderPlaceHolder" runat="server">               
                    <td class="centerTd">
                      <asp:HyperLink ID="AccountLink" NavigateUrl="Account.aspx" target="_top" Text="Settings" runat="server" />
                  </td>
                </asp:PlaceHolder>  
                <td class="centerTd">
                    <asp:HyperLink ID='HelpLink' runat='server' NavigateUrl="Help/Full/index.htm" Target="Help" Text="Help" />
                </td>
                <asp:PlaceHolder ID="SupportLinkPlaceHolder" runat="server">               
                    <td class="centerTd">
                        <asp:HyperLink ID='SupportLink' runat='server' NavigateUrl="Support.aspx" Text="Support" Target="_blank" />
                    </td>
                </asp:PlaceHolder>
                <td class="centerTd">
                    <asp:HyperLink ID='LogoutLink' runat='server' NavigateUrl="Logout.aspx" Target="_top" Text="Logout" />                
                </td>
            </tr>
        </table>
        </form>
    </div>
     <asp:PlaceHolder ID="DemoHeader" runat="server" Visible="false">
         <div style="background-image: url(Images/HeaderBackground.png)">
             <table width="100%" cellpadding="0" cellspacing="0" style="padding-left: 5px; padding-right: 5px;
                 font-size: 10pt; font-weight: bold; text-align: right">
                 <tr>
                     <td style="width: 20%; color: White; font-size: 11pt; height: 44px; text-align: center;
                         vertical-align: top; background-image: url(/Logo.aspx); background-repeat: no-repeat;
                         padding-top: 5px; padding-left: 170px; padding-right: 10px; color: #F03000">                         
                     </td>
                     <td style="width: 20%; color: #F5F9FA; font-size: 10pt; text-align: center; padding-right: 5px;
                    padding-left: 5px;">
                        <asp:Label ID="DemoSpaceLabel" runat="server" Text="" CssClass="headerLabel"></asp:Label>
                    </td>
                     <td style="width: 60%; text-align: right; color: #BFBFBF">
                         <a href="http://www.birst.com/products/tours-demos.shtml" style="color: #F5F9FA;
                             text-decoration: none" target="_top">
                             <asp:Label ID="BackToDemosLabel" runat="server" Text="Back to Demos Page" meta:resourcekey="BackToDemosPageResource" />
                             </a> &nbsp;|&nbsp; <a href="FlexModule.aspx?birst.module=dashboard" target="_top" style="color: #F5F9FA; text-decoration: none">
                                 <asp:Label ID="ViewPrebuiltDashboardsLabel" runat="server" Text="<%$ Resources:HeaderLabels, ViewPrebuiltDashboards %>"/>
                             </a>
                         &nbsp;|&nbsp;
                         <asp:PlaceHolder ID="adhocLink" runat="server">
                             <a href="FlexModule.aspx?birst.module=designer" target="_top" style="color: #F5F9FA; text-decoration: none">
                                <asp:Label ID="BuildYourOwnReportLabel" runat="server" Text="<%$ Resources:HeaderLabels, BuildYourOwnReport %>"/>
                             </a> &nbsp;|&nbsp;
                         </asp:PlaceHolder>
                         <a href="Help/index.htm" target="_blank"
                             style="color: #F5F9FA; text-decoration: none">
                             <asp:Label ID="HelpLabel2" runat="server" Text="<%$ Resources:HeaderLabels, HelpLabel %>"/>
                         </a>
                     </td>
                 </tr>
             </table>
         </div>
     </asp:PlaceHolder>
<%
  }
%>
    <div  id="moduleDiv" runat="server" >
        <script src="AC_OETags.js" language="javascript" type="text/javascript"></script>
        <script language="JavaScript" type="text/javascript">

            var requiredMajorVersion = 9;
            var requiredMinorVersion = 0;
            var requiredRevision = 0;
        </script>       

        <asp:Label ID="KeepAlive" runat="server"/>
     
        <script language="JavaScript" type="text/javascript">
            var hasProductInstall = DetectFlashVer(6, 0, 65);
            var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

            if (hasProductInstall && !hasRequestedVersion) {
                var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
                var MMredirectURL = window.location;
                document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                var MMdoctitle = document.title;

                AC_FL_RunContent(
    "src", "playerProductInstall",
		"FlashVars", 'MMredirectURL=' + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + '&dummy=a<%= this.smiroot %>&v=<%= this.version %><%= this.locale %><%= this.extraParams %>&noheader=<%= this.noheader %>',
    "width", "100%",
    "height", "100%",
    "align", "middle",
    "id", "<%= this.moduleName %>",
    "quality", "high",
    "bgcolor", "#869ca7",
    "name", "<%= this.moduleName %>",
    "wmode", "transparent",
    "allowScriptAccess", "sameDomain",
    "type", "application/x-shockwave-flash",
    "pluginspage", "http://www.adobe.com/go/getflashplayer"
  );
            } else if (hasRequestedVersion) {
                AC_FL_RunContent(
      "src", "/swf/<%= this.moduleName %>?v=<%= this.version %>",
			"flashvars", "dummy=a<%= this.smiroot %><%= this.locale %><%= this.extraParams %>&noheader=<%= this.noheader %>",
      "width", "100%",
      "height", "100%",
      "align", "middle",
      "id", "<%= this.moduleName %>",
      "quality", "high",
      "bgcolor", "#869ca7",
      "name", "<%= this.moduleName %>",
      "wmode", "transparent",
      "allowScriptAccess", "sameDomain",
      "type", "application/x-shockwave-flash",
      "pluginspage", "http://www.adobe.com/go/getflashplayer"
  );
            } else {
                var alternateContent = 'This content requires the Adobe Flash Player. '
    + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
                document.write(alternateContent);
            }

        </script>
    </div>
    <asp:PlaceHolder ID="FooterPanel" runat="server" Visible="false">
        <div id="footerstyle" style="background-image: url(Images/FooterBackground.png); height: 40px; margin-left:auto; margin-right:auto" class="footerstyle">
            <table width="75%" cellpadding="0" cellspacing="20" style="padding-top: 0px; padding-bottom:20px">
                <tr>
                    <td style="color: #555555; text-align: center">
                        <asp:Literal ID="CopyrightID" runat="server" 
                            meta:resourcekey="CopyrightResource1"></asp:Literal>
                        &nbsp;
                        <asp:Literal ID="VersionID" runat="server" meta:resourcekey="VersionResource1"></asp:Literal>
                        &nbsp;
                    </td>
                    <asp:PlaceHolder ID="TermsOfServicePlaceHolder" runat="server">
                        <td style="text-align:center ">
                            <asp:HyperLink ID="TermsOfServiceLink" runat='server' NavigateUrl="http://www.birst.com/terms-of-service" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Terms of Service" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="PrivacyPolicyPlaceHolder" runat="server">
                        <td style="text-align: center">
                            <asp:HyperLink ID="PrivacyPolicyLink" runat='server' NavigateUrl="http://www.birst.com/privacy-policy" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Privacy Policy" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="ContactUsPlaceHolder" runat="server">
                        <td style="text-align: center">
                            <asp:HyperLink ID="ContactUsLink" runat='server' NavigateUrl="http://www.birst.com/company/contact" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Contact Us" />
                        </td>
                    </asp:PlaceHolder>
                </tr>
            </table>
        </div>
    </asp:PlaceHolder>
  </div>
</body>
</html>
