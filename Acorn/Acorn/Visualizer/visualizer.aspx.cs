﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.Utils;
using System.IO;
using System.Text.RegularExpressions;

namespace Acorn.html
{
    public partial class visualizer : System.Web.UI.Page
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);
            string logoutScript = Util.getInactivityTimeoutScript(Session);
            logoutScript = string.IsNullOrWhiteSpace(logoutScript) ? "" : logoutScript;

            if (!Util.validateSpace(sp, u, Session))
            {
                Global.systemLog.Debug("redirecting to home page due to space validation failure");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (!validateVisualizerAccess(sp, u))
            {
                Global.systemLog.Debug("redirecting to home page due to permissions failure (visualizer)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (sp != null)
            {
                Util.saveDirty(Session);
                createSMIWebSession(Session, Request, Response, Server, sp, u);
            }
            Util.injectCSRFToken(Response, Session, MapPath("~/Visualizer/index.html"), logoutScript);
            Response.Flush();
            Response.End();
        }

        private bool validateVisualizerAccess(Space sp, User u)
        {
            UserAndGroupUtils.populateSpaceGroups(sp, false);
            if (Util.isFeatureFlagEnabled(Util.FEATURE_FLAG_VIZ_GA, true) && Util.isOwner(sp, u))
            {
                return true;
            }
            return UserAndGroupUtils.isVisualizerPermittedByACL(u, sp.SpaceGroups);
        }

        private void createSMIWebSession(System.Web.SessionState.HttpSessionState session, HttpRequest request,
            HttpResponse response, HttpServerUtility server, Space sp, User u)
        {
            string token = Util.getSMILoginToken(u.Username, sp);
            if (!Util.getSMILoginSession(session, sp, token, response, request))
            {   
                Global.systemLog.Error("Not able to login SMIWeb");
                response.Redirect(Util.getHomePageURL(session, request));
            }
        }
    }
}