﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using System.Xml.Serialization;
using System.Xml;

namespace Acorn
{
    /// <summary>
    /// Summary description for CommandWebService
    /// </summary>
    [WebService(Namespace = "http://www.birst.com/", 
        Description="A webservice which performs provisioning and execution for Birst")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [AssertNamespaceBinding("s", "http://schemas.xmlsoap.org/soap/envelope/")]
    [AssertNamespaceBinding("b", "http://www.birst.com/")]
    public class CommandWebService : System.Web.Services.WebService
    {
        [WebMethod(Description="Log out of this web service.  Pass the Login token as the argument")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:Logout/b:token) = 32", "Invalid token")]
        public void Logout(string token)
        {
            WebServiceSessionHelper.Logout(token);
        }

        [WebMethod (Description="Log in to this web service.  Pass the user name and password of the administrator.  A token will be returned that should be used for all remaining calls.  The token will expire in after a certain amount of inactivity or after logout")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:Login/b:username) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:username) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:password) > 5", "password must be longer than 5 characters")]
        [Assert("string-length(//b:password) <= 32", "password must be shorter than 32 characters")]
        public string Login(string username, string password)
        {
            return WebServiceSessionHelper.Login(username, password);
        }

        [WebMethod (Description="Clears the cache in the space specified.  The arguments are the Login token and the space Id to clear") ]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:clearCacheInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public Boolean clearCacheInSpace(string token, string spaceID)
        {
            return CommandHelper.clearCache(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod (Description="Swaps 2 spaces.  The arguments are the Login token, and the names of the 2 spaces. Returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:swapSpaceContents/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:sp1ID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:sp2ID) = 36", "Space ID must be 36 characters")]
        public string swapSpaceContents(string token, string sp1ID, string sp2ID)
        {
            return CommandHelper.swapSpaceContents(WebServiceSessionHelper.getUserForToken(token), sp1ID, sp2ID);
        }

        [WebMethod (Description="Copy the contents from one space into another.  The arguments are the Login token, the space Id to copy from and the space Id to copy to. It returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copySpaceContents/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spFromID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spToID) = 36", "Space ID must be 36 characters")]
        public string copySpaceContents(string token, string spFromID, string spToID)
        {
            User u = WebServiceSessionHelper.getUserForToken(token);

            return CommandHelper.copySpaceContents(u, spFromID, spToID);
        }

        [WebMethod(Description = "Copy the contents from one space into another.  The arguments are the Login token, the space Id to copy from, the space Id to copy to, 'replicate' or 'copy' mode, and a command separated list of items to copy. It returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copySpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spFromID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spToID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:options) <= 4096", "options must be less than or equal to 4096 characters")]
        public string copySpace(string token, string spFromID, string spToID, string mode, string options)
        {
            User u = WebServiceSessionHelper.getUserForToken(token);

            return CommandHelper.copySpace(u, spFromID, spToID, mode, options);
        }

        [WebMethod (Description="Copy the contents of the catalog from one space to another.  The arguments are the Login token, the space Id to copy from, the space Id to copy to and the name of the directory to start the copy.  It returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copyCatalogDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spFromID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spToID) = 36", "Space ID must be 36 characters")]
        public string copyCatalogDirectory(string token, string spFromID, string spToID, string directoryName)
        {
            User u = WebServiceSessionHelper.getUserForToken(token);

            return CommandHelper.copyCatalogDirectory(u, spFromID, spToID, directoryName);
        }

        [WebMethod (Description="Resets the password for a user.  The arguments are the Login token and the name of the user to reset.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:resetPassword/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:username) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:username) < 256", "username must be shorter than 256 characters")]
        [Obsolete("resetPassword is no longer supported.", true)]
        public void resetPassword(string token, string username)
        {
            WebServiceSessionHelper.error("resetPassword", "resetPassword has been obsoleted.  It is no longer supported as a web method.");
//            CommandHelper.resetPassword(WebServiceSessionHelper.getUserForToken(token), username, HttpContext.Current.Server, HttpContext.Current.Request);
        }

        [WebMethod (Description="Gives a user access to a space. The arguments are the Login token, the name of the user to add, the space Id, and if the user is an admin.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addUserToSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void addUserToSpace(string token, string userName, string spaceID, bool hasAdmin)
        {
            CommandHelper.addUserToSpace(WebServiceSessionHelper.getUserForToken(token), userName, spaceID, hasAdmin);
        }

        [WebMethod (Description="Lists all users who have access to a space.  Arguments are the Login token and the space Id.  A list of user names will be returned.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listUsersInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public List<string> listUsersInSpace(string token, string spaceID)
        {
            return CommandHelper.listUsersInSpace(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod (Description="Removes a user from a space (that user will no longer have access to the space).  Arguments are the Login token, the name of the user and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeUserFromSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void removeUserFromSpace(string token, string userName, string spaceID)
        {
            CommandHelper.removeUserFromSpace(WebServiceSessionHelper.getUserForToken(token), userName, spaceID);
        }

        [WebMethod(Description = "Creates a group in a space.  Argments are the Login token, the name of the group and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addGroupToSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void addGroupToSpace(string token, string groupName, string spaceID)
        {
            CommandHelper.addGroupToSpace(WebServiceSessionHelper.getUserForToken(token), groupName, spaceID);
        }

        [WebMethod(Description="Removes a group from a space.  Arguments are the Login token, the name of the group and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeGroupFromSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void removeGroupFromSpace(string token, string groupName, string spaceID)
        {
            CommandHelper.deleteGroupFromSpace(WebServiceSessionHelper.getUserForToken(token), groupName, spaceID);
        }

        [WebMethod(Description="List the names of the group in a space.  Arguments are the Login token and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listGroupsInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public List<string> listGroupsInSpace(string token, string spaceID)
        {
            return CommandHelper.listGroupsInSpace(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description="List group ACLs in a space.  The method will return a list of ACLs that a given group has.  Arguments are the Login token, the name of the group and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listGroupAclsInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public List<string> listGroupAclsInSpace(string token, string groupName, string spaceID)
        {
            return CommandHelper.listGroupAclsInSpace(WebServiceSessionHelper.getUserForToken(token), groupName, spaceID);
        }

        [WebMethod(Description="Adds an ACL to a group in a space. Arguments are the Login token, the name of the group, the name of the ACL and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addAclToGroupInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:aclTag) > 3", "ACL tag name must be longer than 3 characters")]
        [Assert("string-length(//b:aclTag) <= 64", "ACL tag name must be shorter than 64 characters")]
        public void addAclToGroupInSpace(string token, string groupName, string aclTag, string spaceID)
        {
            CommandHelper.addAclToGroupInSpace(WebServiceSessionHelper.getUserForToken(token), groupName, aclTag, spaceID);
        }

        [WebMethod(Description="Removes an ACL from a group in a space. Arguments are the Login token, the name of the group, the name of the ACL and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeAclFromGroupInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        [Assert("string-length(//b:aclTag) > 3", "ACL tag name must be longer than 3 characters")]
        [Assert("string-length(//b:aclTag) <= 64", "ACL tag name must be shorter than 64 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void removeAclFromGroupInSpace(string token, string groupName, string aclTag, string spaceID)
        {
            CommandHelper.removeAclFromGroupInSpace(WebServiceSessionHelper.getUserForToken(token), groupName, aclTag, spaceID);
        }

        [WebMethod(Description="Adds a user to a group in a space.  Arguments are the Login token, the name of the user to add, the name of the group and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addUserToGroupInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        public void addUserToGroupInSpace(string token, string userName, string groupName, string spaceID)
        {
            CommandHelper.addUserToGroupInSpace(WebServiceSessionHelper.getUserForToken(token), userName, groupName, spaceID);
        }

        [WebMethod(Description = "Removes a user from a group.  Arguments are the Login token, the name of the user to remove, the name of the group and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeUserFromGroupInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        public void removeUserFromGroupInSpace(string token, string userName, string groupName, string spaceID)
        {
            CommandHelper.dropUserFromGroupInSpace(WebServiceSessionHelper.getUserForToken(token), userName, groupName, spaceID);
        }

        [WebMethod(Description = "Lists all users in a group for a space. Returns a list of user names.  Arguments are Login token, name of group and space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listUsersInGroupInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:groupName) > 3", "Group name must be longer than 3 characters")]
        [Assert("string-length(//b:groupName) < 256", "Group name must be shorter than 256 characters")]
        public List<string> listUsersInGroupInSpace(string token, string groupName, string spaceID)
        {
            return CommandHelper.listUsersInGroupInSpace(WebServiceSessionHelper.getUserForToken(token), groupName, spaceID);
        }

        [WebMethod(Description = "Lists all groups that a user belongs to within a space. Returns a list of group names.  Arguments are Login token, space Id, user name, include system groups flag.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listUserGroupMembership/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public List<string> listUserGroupMembership(string token, string spaceID, string userName, bool includeInternalGroups)
        {
            return CommandHelper.listUserGroupMembership(WebServiceSessionHelper.getUserForToken(token), spaceID, userName, includeInternalGroups);
        }


        [WebMethod(Description="List all users who can proxy for a specific user.  Returns a list of proxy user names.  Arguments are the Login token and the name of the user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listProxyUsers/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public List<string> listProxyUsers(string token, string userName)
        {
            return CommandHelper.listProxyUsers(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description = "List all OpenIDs for a user.  Returns a list of OpenIDs. Arguments are the Login token and the name of the user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listOpenIDs/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public List<string> listOpenIDs(string token, string userName)
        {
            return CommandHelper.listOpenIDs(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description = "Adds an OpenID for a user. Arguments are the Login token, the name of the user and the OpenID identifier.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addOpenID/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:openID) > 3", "proxyUserName must be longer than 3 characters")]
        [Assert("string-length(//b:openID) < 256", "proxyUserName must be shorter than 256 characters")]
        public void addOpenID(string token, string userName, string openID)
        {
            CommandHelper.addOpenID(WebServiceSessionHelper.getUserForToken(token), userName, openID);
        }

        [WebMethod(Description = "Removes an OpenID for a user. Arguments are the Login token, the name of the user and the OpenID identifier.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeOpenID/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:openID) > 3", "proxyUserName must be longer than 3 characters")]
        [Assert("string-length(//b:openID) < 256", "proxyUserName must be shorter than 256 characters")]
        public void removeOpenID(string token, string userName, string openID)
        {
            CommandHelper.removeOpenID(WebServiceSessionHelper.getUserForToken(token), userName, openID);
        }

        [WebMethod(Description = "Sets the default space for a user. Arguments are the Login token, the name of the user, the name of the space, and if the user should default to dashboards.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setUserDefaultSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setUserDefaultSpace(string token, string userName, string spaceID, bool dashboards)
        {
            CommandHelper.setUserDefaultSpace(WebServiceSessionHelper.getUserForToken(token), userName, spaceID, dashboards);
        }

        [WebMethod(Description = "List all releases.  Returns a list of releases. Argument is the Login token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listReleases/b:token) = 32", "Invalid token")]
        public List<string> listReleases(string token)
        {
            return CommandHelper.listReleases(WebServiceSessionHelper.getUserForToken(token));
        }

        [WebMethod(Description = "Gets the release for a user. Arguments are the Login token, and the name of the user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getUserRelease/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public string getUserRelease(string token, string userName)
        {
            return CommandHelper.getUserRelease(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description = "Sets the release for a user. Arguments are the Login token, the name of the user and the release.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setUserReleaseType/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:release) > 1", "release must be longer than 1 characters")]
        [Assert("string-length(//b:release) < 32", "release must be shorter than 32 characters")]
        public void setUserRelease(string token, string userName, string release)
        {
            CommandHelper.setUserRelease(WebServiceSessionHelper.getUserForToken(token), userName, release);
        }

        [WebMethod(Description = "Enables/Disables a User. Arguments are the Login token, a username, and a boolean, if true enable, if false disable")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:enableUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public void enableUser(string token, string userName, bool enable)
        {
            CommandHelper.enableUser(WebServiceSessionHelper.getUserForToken(token), userName, enable);
        }

        [WebMethod(Description = "Unlocks a User. Arguments are the Login token and a username")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:unlockUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public void unlockUser(string token, string userName)
        {
            CommandHelper.unlockUser(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description = "Deletes a User. Arguments are the Login token and a username")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public void deleteUser(string token, string userName)
        {
            CommandHelper.deleteUser(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description = "Enables/Disables an Account. Arguments are the Login token, an AccountID, and a boolean, if true enable, if false disable")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:enableAccount/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:accountID) = 36", "Account ID must be 36 characters")]
        public void enableAccount(string token, string accountID, bool enable)
        {
            CommandHelper.enableAccount(WebServiceSessionHelper.getUserForToken(token), accountID, enable);
        }

        [WebMethod(Description="Adds a new proxy user for a user. Arguments are the Login token, the name of the user and the name of the user who will proxy for the first user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addProxyUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:proxyUserName) > 3", "proxyUserName must be longer than 3 characters")]
        [Assert("string-length(//b:proxyUserName) < 256", "proxyUserName must be shorter than 256 characters")]
        public void addProxyUser(string token, string userName, string proxyUserName, DateTime expiration)
        {
            CommandHelper.addProxyUser(WebServiceSessionHelper.getUserForToken(token), userName, proxyUserName, expiration, false);
        }

        [WebMethod(Description="Removes a proxy user for a user.  Arguments are the Login token, the name of the user who has a proxy user and the name of the proxy user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeProxyUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:proxyUserName) > 3", "proxyUserName must be longer than 3 characters")]
        [Assert("string-length(//b:proxyUserName) < 256", "proxyUserName must be shorter than 256 characters")]
        public void removeProxyUser(string token, string userName, string proxyUserName)
        {
            CommandHelper.removeProxyUser(WebServiceSessionHelper.getUserForToken(token), userName, proxyUserName);
        }

        [WebMethod(Description = "List all users managed by the logged in user.  Returns a list of user names. Arguments are the Login token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listCreatedUsers/b:token) = 32", "Invalid token")]
        public List<string> listCreatedUsers(string token)
        {
            return CommandHelper.listManagedUsers(WebServiceSessionHelper.getUserForToken(token));
        }

        [WebMethod(Description = "List all users managed by the logged in user.  Returns a list of user names. Arguments are the Login token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listManagedUsers/b:token) = 32", "Invalid token")]
        public List<string> listManagedUsers(string token)
        {
            return CommandHelper.listManagedUsers(WebServiceSessionHelper.getUserForToken(token));
        }

        [WebMethod(Description="List IPs that a user is allowed to log in from.  Returns a list of IP addresses / CIDR netblocks.  Arguments are the Login token and the name of the user.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listAllowedIps/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public List<string> listAllowedIps(string token, string userName)
        {
            return CommandHelper.listAllowedIPs(WebServiceSessionHelper.getUserForToken(token), userName);
        }

        [WebMethod(Description="Add an IP address / CIDR netblock for a user to be able to log in from.  Arguments are the Login token, the name of the user and the IP address / CIDR netblock.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addAllowedIp/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:ip) > 5", "IP address length must be longer than 5 characters")]
        public void addAllowedIp(string token, string userName, string ip)
        {
            CommandHelper.addAllowedIP(WebServiceSessionHelper.getUserForToken(token), userName, ip);
        }

        [WebMethod(Description="Removes and IP address / CIDR netblock for a user to be able to log in from.  Arguments are the Login token, the name of the user and the IP address / CIDR netblock.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeAllowedIp/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:ip) > 5", "IP address length must be longer than 5 characters")]
        public void removeAllowedIp(string token, string userName, string ip)
        {
            CommandHelper.removeAllowedIP(WebServiceSessionHelper.getUserForToken(token), userName, ip);
        }

        [WebMethod(Description = "List IPs that a users in the account is allowed to log in from.  Returns a list of IP addresses / CIDR netblocks.  Argument is the Login token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listAllowedIps/b:token) = 32", "Invalid token")]
        public List<string> listAllowedIPAddrsForAccount(string token)
        {
            return CommandHelper.listAllowedIPAddrsForAccount(WebServiceSessionHelper.getUserForToken(token), null);
        }

        [WebMethod(Description = "Add an IP address / CIDR netblock for users in the account to be able to log in from.  Arguments are the Login token and the IP address / CIDR netblock.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addAllowedIp/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:ip) > 5", "IP address length must be longer than 5 characters")]
        public void addAllowedIPAddrForAccount(string token, string ip)
        {
            CommandHelper.addAllowedIPAddrForAccount(WebServiceSessionHelper.getUserForToken(token), ip, null);
        }

        [WebMethod(Description = "Removes and IP address / CIDR netblock for users in the account to be able to log in from.  Arguments are the Login token and the IP address / CIDR netblock.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:removeAllowedIp/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:ip) > 5", "IP address length must be longer than 5 characters")]
        public void removeAllowedIPAddrForAccount(string token, string ip)
        {
            CommandHelper.removeAllowedIPAddrForAccount(WebServiceSessionHelper.getUserForToken(token), ip, null);
        }

        [WebMethod(Description="Sets a variable value.  Arguments are the Login token, the name of the variable, the value (query) of the variable and the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setVariableInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:varName) > 3", "Variable name must be longer than 3 characters")]
        [Assert("string-length(//b:varName) < 256", "Variable name must be shorter than 256 characters")]
        [Assert("string-length(//b:query) > 0", "Query must contain a value")]
        [Assert("string-length(//b:query) < 32768", "Query length must shorter than 32k")]
        public void setVariableInSpace(string token, string varName, string query, string spaceID)
        {
            CommandHelper.setVariableInSpace(WebServiceSessionHelper.getUserForToken(token), varName, query, spaceID);
        }

		[WebMethod(Description = "Creates a new user for the Birst system. Arguments are the Login token, the name of the user and additional parameters. The username must be unique in the Birst system. Additional parameters are a list of name=value pairs separated by spaces. Valid names are 'password=', 'email=', for example 'password=foo email=foo@bar.com'.  If an email address is specified it must be unique. If an email address is not specified, the username is used.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:addUser/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        public void addUser(string token, string userName, string additionalParams)
        {
            CommandHelper.addUser(WebServiceSessionHelper.getUserForToken(token), userName, additionalParams);
        }

        [WebMethod(Description = "Execute a Birst Logical Query.  Arguments are the Login token, the Birst logical query, and the space Id. Returns the first 1000 results.  See queryMore for retrieving the remainder.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:executeQueryInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:query) > 0", "Query must contain a value")]
        [Assert("string-length(//b:query) < 32768", "Query length must shorter than 32k")]
        public CommandQueryResult executeQueryInSpace(string token, string query, string spaceID)
        {
            return CommandHelper.executeQueryInSpace(WebServiceSessionHelper.getUserForToken(token), query, spaceID);
        }

        [WebMethod(Description = "Return additional results from a previously executed Birst Logical Query.  Arguments are the Login token and the query token from the previous logical query. Returns the next 1000 results.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:queryMore/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:queryToken) = 32", "Invalid token")]
        public CommandQueryResult queryMore(string token, string queryToken)
        {
            return CommandHelper.queryMore(WebServiceSessionHelper.getUserForToken(token), queryToken);
        }

        [WebMethod(Description="Tells Birst that a new source data (.csv file, .xls file) is about to be uploaded.  Arguments are the Login token, the Space ID and the name of the source. Returns a data upload token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:beginDataUpload/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string beginDataUpload(string token, string spaceID, string sourceName)
        {
            return CommandHelper.beginDataUpload(WebServiceSessionHelper.getUserForToken(token), spaceID, sourceName);
        }

        [WebMethod(Description = "Sets Data upload options for a particular source.  Arguments are the Login token, the data upload token and an array of options in the form of optionName=optionValue.  Valid options are ConsolidateIdenticalStructures=[true|false], ColumnNamesInFirstRow=[true|false], FilterLikelyNoDataRows=[true|false], LockDataSourceFormat=[true|false], IgnoreQuotesNotAtStartOrEnd=[true|false], RowsToSkipAtStart=n, RowsToSkipAtEnd=n, CharacterEncoding=enc")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setDataUploadOptions/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:dataUploadToken) = 32", "Invalid token")]
        public void setDataUploadOptions(string token, string dataUploadToken, string[] options)
        {
            CommandHelper.setDataUploadOptions(WebServiceSessionHelper.getUserForToken(token), dataUploadToken, options);
        }

        [WebMethod(Description="Uploads a chunk of data from a particular source.  Arguments are Login token, data upload token, the number of bytes sent and a byte array of data.  Typically the data would be chunked instead of sending the entire file for performance.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:uploadData/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:dataUploadToken) = 32", "Invalid token")]
        public void uploadData(string token, string dataUploadToken, int numBytes, byte[] data) 
        {
            CommandHelper.uploadData(WebServiceSessionHelper.getUserForToken(token), dataUploadToken, numBytes, data);
        }

        [WebMethod(Description="Called after the last chunk of data is uploaded for a particular data source.  This informs Birst to start analyzing the data, which could take some time.  This web service will return immediately, but status can be polled with isDataUploadComplete and getDataUploadStatus. Arguments are Login token, and data upload token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:finishDataUpload/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:dataUploadToken) = 32", "Invalid token")]
        public void finishDataUpload(string token, string dataUploadToken) 
        {
            CommandHelper.finishDataUpload(WebServiceSessionHelper.getUserForToken(token), dataUploadToken);
        }

        [WebMethod(Description="Called to see if the data that was uploaded has finished processing.  Arguments are the Login token and the data upload token. Returns true if processing is complete and false otherwise.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:isDataUploadComplete/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:dataUploadToken) = 32", "Invalid token")]
        [Obsolete("isDataUploadComplete has been replaced by isJobComplete", false)]
        public bool isDataUploadComplete(string token, string dataUploadToken)
        {
            return CommandHelper.isUploadComplete(WebServiceSessionHelper.getUserForToken(token), dataUploadToken);
        }

        [WebMethod(Description="Called the get the status after data upload processing is complete.  Arguments include the Login token and the data upload token.  Returns an array of statuses, some of which could be warnings and some of which could be errors.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getDataUploadStatus/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:dataUploadToken) = 32", "Invalid token")]
        [Obsolete("getDataUploadStatus has been replaced by getJobStatus", false)]
        public string[] getDataUploadStatus(string token, string dataUploadToken)
        {
            return CommandHelper.getUploadStatus(WebServiceSessionHelper.getUserForToken(token), dataUploadToken);
        }

        [WebMethod(Description = "Called to see if a job that runs in the background has finished processing.  Arguments are the Login token and the job token. Returns true if processing is complete and false otherwise.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:isJobComplete/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:jobToken) = 32", "Invalid token")]
        public bool isJobComplete(string token, string jobToken)
        {
            // just do this so that the login token doesn't expire...
            WebServiceSessionHelper.getUserForToken(token);

            return CommandHelper.isComplete(jobToken);
        }

        [WebMethod(Description = "Called the get the status after a background job is complete.  Arguments include the Login token and the job token.  Returns an XML representation of the status.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getJobStatus/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:jobToken) = 32", "Invalid token")]
        public Acorn.Status.StatusResult getJobStatus(string token, string jobToken)
        {
            return CommandHelper.getStatus(WebServiceSessionHelper.getUserForToken(token), jobToken);
        }

        [WebMethod (Description="Gets Birst to process the data sources that have been defined.  Arguments are Login token, space id, subgroups (if defined in the space), and the date to use as the load date.  Returns a publishing token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:publishData/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string publishData(string token, string spaceID, string[] subgroups, DateTime date)
        {
            return CommandHelper.publishData(WebServiceSessionHelper.getUserForToken(token), spaceID, subgroups, date);
        }

        [WebMethod(Description="Determines if publishing has completed. Arguments are Login token and publishing token.  Return true if publishing has completed and false otherwise.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:isPublishingComplete/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:publishingToken) = 32", "Invalid token")]
        public bool isPublishingComplete(string token, string publishingToken)
        {
            return CommandHelper.isPublishingComplete(WebServiceSessionHelper.getUserForToken(token), publishingToken);
        }
        [WebMethod(Description = "Called the get the status after publishing processing is complete.  Arguments include the Login token and the publishing token.  Returns an array of statuses, some of which could be warnings and some of which could be errors.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getPublishingStatus/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:publishingToken) = 32", "Invalid token")]
        public string[] getPublishingStatus(string token, string publishingToken)
        {
            return CommandHelper.getPublishingStatus(WebServiceSessionHelper.getUserForToken(token), publishingToken);
        }

        [WebMethod(Description="Get a list of variables and values defined for a space.  Arguments are Login token and space id.  Returns an array of arrays, each of which is a name value pair") ]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getVariablesForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string[][] getVariablesForSpace(string token, string spaceID)
        {
            return CommandHelper.getVariablesForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }


        [WebMethod(Description="Gets the files and directories in a directory for a space.  Arguments are Login token, space id and directory (null value means top level).  Returns an XML representation of the contents of the directory.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getDirectoryContents/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public Acorn.CommandHelper.FileNode getDirectoryContents(string token, string spaceID, string dir)
        {
            return CommandHelper.getDirectoryContents(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dir);
        }

        [WebMethod(Description="Gets the permissions for a particular directory.  Arguments are Login token, space Id and directory (a null value means top level).  Returns an XML representation of the directory permissions.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getDirectoryPermissions/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public List<Acorn.CommandHelper.GroupPermission> getDirectoryPermissions(string token, string spaceID, string dir)
        {
            return CommandHelper.getDirectoryPermissions(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dir);
        }

        [WebMethod(Description="Sets a permission on a directory.  Arguments are Login token, space id, directory, group name, permission and value for permission.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setDirectoryPermission/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setDirectoryPermission(string token, string spaceID, string dir, string groupName, string permissionName, bool permission)
        {
            CommandHelper.setDirectoryPermission(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dir, groupName, permissionName, permission);
        }

        [WebMethod(Description = "Sets permissions for a user on a space (bulk operation).  Arguments are Login token, space id, directories, group names, permissions and values for permissions.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setDirectoryPermissions/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setDirectoryPermissions(string token, string spaceID, List<string> dirs, List<List<Acorn.CommandHelper.GroupPermission>> perms)
        {
            CommandHelper.setDirectoryPermissions(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dirs, perms);
        }

        [WebMethod(Description = "Copy a report or folder from one space folder to another folder.  Arguments are Login token, from space id, from report or directory name, to space id and to directory name.  It returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copyFileOrDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:fromSpaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:toSpaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:fileOrDir) < 1024", "From object must be less than 1024 characters")]
        [Assert("string-length(//b:toDir) < 1024", "To object must be less than 1024 characters")]
        public string copyFileOrDirectory(string token, string fromSpaceID, string fileOrDir, string toSpaceID, string toDir)
        {
            return CommandHelper.copyFileOrDirectory(WebServiceSessionHelper.getUserForToken(token), fromSpaceID, fileOrDir, toSpaceID, toDir);
        }

        [WebMethod(Description = "Copy a report from one space folder to another folder or file.  Arguments are Login token, from space id, from report name, to space id, to directory or report name, and overwrite flag")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copyFile/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:fromSpaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:toSpaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:fromFileOrDir) < 1024", "From object must be less than 1024 characters")]
        [Assert("string-length(//b:toFileOrDir) < 1024", "To object must be less than 1024 characters")]
        public void copyFile(string token, string fromSpaceID, string fromFileOrDir, string toSpaceID, string toFileOrDir, bool overwrite)
        {
            CommandHelper.copyFile(WebServiceSessionHelper.getUserForToken(token), fromSpaceID, fromFileOrDir, toSpaceID, toFileOrDir, overwrite);
        }

        [WebMethod(Description="Delete a file or an entire directory.  Arguments are login token, space id and name of file or folder.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteFileOrDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:fileOrDir) < 1024", "Name must be less than 1024 characters")]
        public void deleteFileOrDirectory(string token, string spaceID, string fileOrDir)
        {
            CommandHelper.deleteFileOrDirectory(WebServiceSessionHelper.getUserForToken(token), spaceID, fileOrDir);
        }

        [WebMethod(Description = "Rename a file or an entire directory.  Arguments are login token, space id, name of file or folder and the new name.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:renameFileOrDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:fileOrDir) < 1024", "Location must be less than 1024 characters")]
        [Assert("string-length(//b:newName) < 256", "New name must be less than 256 characters")]
        public void renameFileOrDirectory(string token, string spaceID, string fileOrDir, string newName)
        {
            CommandHelper.renameFileOrDirectory(WebServiceSessionHelper.getUserForToken(token), spaceID, fileOrDir, newName);
        }

        [WebMethod(Description = "Create a new directory.  Arguments are login token, space id, name of parent folder and the new directory name.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:createNewDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:parentDir) < 1024", "Parent directory must be less than 1024 characters")]
        [Assert("string-length(//b:parentDir) < 256", "New directory must be less than 256 characters")]
        public void createNewDirectory(string token, string spaceID, string parentDir, string newDirectoryName)
        {
            CommandHelper.createNewDirectory(WebServiceSessionHelper.getUserForToken(token), spaceID, parentDir, newDirectoryName);
        }

        [WebMethod(Description = "Create a new directory. If it already exists, returns false else returns true. Arguments are login token, space id, name of parent folder and the new directory name.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:checkAndCreateDirectory/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:parentDir) < 1024", "Parent directory must be less than 1024 characters")]
        [Assert("string-length(//b:parentDir) < 256", "New directory must be less than 256 characters")]
        public bool checkAndCreateDirectory(string token, string spaceID, string parentDir, string newDirectoryName)
        {
           return CommandHelper.checkAndCreateDirectory(WebServiceSessionHelper.getUserForToken(token), spaceID, parentDir, newDirectoryName);
        }

        [WebMethod(Description = "Check if a space is in an extraction, upload, or processing stage.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getLoadStatus/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string getLoadStatus(string token, string spaceID)
        {
            return CommandHelper.getLoadStatus(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description = "Create a new space.  Arguments are Login token, name of space, comments, and automatic.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:createNewSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceName) < 256", "Space names must be less than 256 characters")]
        [Assert("string-length(//b:comments) < 2048", "Space comments must be less than 2048 characters")]
//        [Assert("matches(//b:spaceName, '^[^\\.<>:\"/\\\\|\\?\\*]{1,255}$') = true, Space names can not contain the characters (\\.<>:\"/\\\\|\\?\\@)")]
        public string createNewSpace(string token, string spaceName, string comments, bool automatic)
        {
            int spaceMode = automatic ? Space.SPACE_MODE_AUTOMATIC : Space.SPACE_MODE_ADVANCED;
            Guid spID = NewSpaceService.createSpaceBase(null, spaceName, null, comments, spaceMode, false, 1, null, WebServiceSessionHelper.getUserForToken(token), -1);
            return spID.ToString();
        }

        [WebMethod(Description = "Create a new space.  Arguments are Login token, name of space, name of schema, comments, and automatic.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:createNewSpaceUsingSchema/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceName) < 256", "Space names must be less than 256 characters")]
        [Assert("string-length(//b:schemaName) < 256", "Schema names must be less than 256 characters")]
        [Assert("string-length(//b:comments) < 2048", "Space comments must be less than 2048 characters")]
        //        [Assert("matches(//b:spaceName, '^[^\\.<>:\"/\\\\|\\?\\*]{1,255}$') = true, Space names can not contain the characters (\\.<>:\"/\\\\|\\?\\@)")]
        public string createNewSpaceUsingSchema(string token, string spaceName, string schemaName, string comments, bool automatic)
        {
            if (schemaName == null || schemaName.Trim().Length == 0)
            {
                throw new Exception("SchemaName not provided");
            }
            else
            {
                ServerConfig config = Util.getServerConfig();
                if (!config.IsApplianceMode || !config.AllowOverrideSpaceSchema)
                {
                    throw new Exception("SchemaName Override is not allowed");
                }
            }
            try
            {
                int spaceMode = automatic ? Space.SPACE_MODE_AUTOMATIC : Space.SPACE_MODE_ADVANCED;
                Guid spID = NewSpaceService.createSpaceBase(null, spaceName, schemaName, comments, spaceMode, false, 1, null, WebServiceSessionHelper.getUserForToken(token), -1);
                return spID.ToString();
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "createNewSpaceUsingSchema", "Error in creating space using schemaname '" + schemaName + "': " + ex.Message);
            }
            return null;
        }

        [WebMethod(Description="Delete a space.  Arguments are Login token, and space id. Returns a job token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string deleteSpace(string token, string spaceId)
        {
            return CommandHelper.deleteSpace(WebServiceSessionHelper.getUserForToken(token), spaceId);
        }

        // placeholder for delete datas

        [WebMethod(Description="Copy a custom subject area from one space to another. Arguments include Login token, from space id, name of the custom subject area to copy and the destination space id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:copyCustomSubjectArea/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:fromSpaceId) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:toSpaceId) = 36", "Space ID must be 36 characters")]
        public void copyCustomSubjectArea(string token, string fromSpaceId, string customSubjectAreaName, string toSpaceId)
        {
            CommandHelper.copyCustomSubjectArea(WebServiceSessionHelper.getUserForToken(token), fromSpaceId, customSubjectAreaName, toSpaceId);
        }

        [WebMethod(Description="List all custom subject areas in a space.  Arguments are the login token and the space id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listCustomSubjectAreas/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string[] listCustomSubjectAreas(string token, string spaceID)
        {
            return CommandHelper.listCustomSubjectAreas(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description = "List all subject areas in a space.  Arguments are the login token, space id, and subject area name.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSubjectAreaContent/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string getSubjectAreaContent(string token, string spaceID, string name)
        {
            return CommandHelper.getSubjectAreaContent(token, spaceID, name);
        }

        [WebMethod(Description = "Creates a custom subject area in a space. Arguments are login token, space id, subject area name, description and permissions")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:createSubjectArea/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:description) > 0", "Description is required")]
        public void createSubjectArea(string token, string spaceID, string name, string description, string[] groups)
        {
            try
            {
                CommandHelper.createSubjectArea(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name, description, groups);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "createSubjectArea", "Error in creating subject area");
            }
        }

        [WebMethod(Description = "Renames the custom subject area in a space. Arguments are login token, space id, custom subject area old name and new name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:renameSubjectArea/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:name) > 0", "Name is required")]
        [Assert("string-length(//b:newName) > 0", "New Name is required")]
        public void renameSubjectArea(string token, string spaceID, string name, string newName)
        {
            try
            {
                CommandHelper.renameSubjectArea(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name, newName);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "renameSubjectArea", "Error in editing subject area " + name);
            }
        }

        [WebMethod(Description = "Sets the permissions for a subject area in a space. Arguments are login token, space id, subject area name and group names")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSubjectAreaPermissions/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:name) > 0", "Name is required")]        
        public void setSubjectAreaPermissions(string token, string spaceID, string name, string[] groups)
        {
            try
            {
                CommandHelper.setSubjectAreaPermissions(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name, groups);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "setSubjectAreaPermissions", "Error in editing subject area " + name);
            }
        }


        [WebMethod(Description = "Gets the permissions for a subject area in a space. Arguments are login token, space id, subject area name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSubjectAreaPermissions/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:name) > 0", "Name is required")]        
        public string[] getSubjectAreaPermissions(string token, string spaceID, string name)
        {            
            try
            {
                return CommandHelper.getSubjectAreaPermissions(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "getSubjectAreaPermissions", "Error in editing subject area " + name);
            }
            return null;
        }

        [WebMethod(Description = "Sets the description for a subject area in a space. Arguments are login token, space id, subject area name and description")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSubjectAreaDescription/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:name) > 0", "Name is required")]
        [Assert("string-length(//b:description) > 0", "Description is required")]        
        public void setSubjectAreaDescription(string token, string spaceID, string name, string description)
        {
            try
            {
                CommandHelper.setSubjectAreaDescription(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name, description);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "setSubjectAreaDescription", "Error in editing subject area " + name);
            }
        }

        [WebMethod(Description = "Gets the description for a subject area in a space. Arguments are login token, space id, subject area name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSubjectAreaDescription/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:name) > 0", "Name is required")]        
        public string getSubjectAreaDescription(string token, string spaceID, string name)
        {            
            try
            {
                return CommandHelper.getSubjectAreaDescription(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "getSubjectAreaDescription", "Error in editing subject area " + name);
            }
            return null;
        }

        [WebMethod(Description = "Sets the subject area for a space. Arguments are login token, space id, subject area content")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSubjectArea/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:saContent) > 0", "Subject area content is required")]
        public void setSubjectArea(string token, string spaceID, string saContent)
        {
            try
            {
                CommandHelper.setSubjectArea(WebServiceSessionHelper.getUserForToken(token), token, spaceID, saContent);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "setSubjectArea", "Error in setting subject area for space ID " + spaceID);
            }
        }

        [WebMethod(Description = "Delete a custom subject area in a space.  Arguments are login token, space id and name of subject area.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteSubjectArea/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void deleteSubjectArea(string token, string spaceID, string name)
        {
            try
            {
                CommandHelper.deleteSubjectArea(WebServiceSessionHelper.getUserForToken(token), token, spaceID, name);
            }
            catch (Exception ex)
            {
                WebServiceSessionHelper.filterNonSoapException(ex, "deleteSubjectArea", "Error in deleting subject area " + name);
            }
        }

        [WebMethod(Description="List all spaces that this user has access to.  Arguments are the login token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listSpaces/b:token) = 32", "Invalid token")]
		public List<Acorn.CommandHelper.UserSpace> listSpaces(string token)
        {
            return CommandHelper.listSpaces(WebServiceSessionHelper.getUserForToken(token));
        }

        [WebMethod(Description = "Delete the last load data.  Arguments are the login token and the space id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteLastDataFromSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string deleteLastDataFromSpace(string token, string spaceID)
        {
            return CommandHelper.deleteSpaceData(WebServiceSessionHelper.getUserForToken(token), spaceID, false);
        }


        [WebMethod(Description = "Delete all loaded data.  Arguments are the login token and the space id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:deleteAllDataFromSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string deleteAllDataFromSpace(string token, string spaceID)
        {
            return CommandHelper.deleteSpaceData(WebServiceSessionHelper.getUserForToken(token), spaceID, true);
        }

        [WebMethod(Description = "Runs the scheduled report.  Arguments are the login token, the space id and schedule name.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:executeScheduledReport/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public void executeScheduledReport(string token, string spaceId, string reportScheduleName)
        {
            string apiName = "executeScheduledReport";
            try
            {
                CommandHelper.executeScheduledReport(WebServiceSessionHelper.getUserForToken(token), spaceId, reportScheduleName);
            }
            catch (Exceptions.BirstException ex1)
            {
                WebServiceSessionHelper.filterNonSoapException(ex1, apiName, ex1.Message);
            }
            catch (Exception ex2)
            {
                WebServiceSessionHelper.filterNonSoapException(ex2, apiName, "Error in executing the schduled report");
            }
        }


        [WebMethod(Description = "Executes a report to be exportedn to PNG.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToPNG/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToPNG(string token, string spaceId, string reportPath, List<Filter> reportFilters, int zoomFactor, int page)
        {
            return CommandHelper.exportReportToPNG(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters, zoomFactor, page);
        }

        [WebMethod(Description = "Executes a report to be exported to PDF.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToPDF/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToPDF(string token, string spaceId, string reportPath, List<Filter> reportFilters)
        {            
            return CommandHelper.exportReportToPDF(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters);
        }

        [WebMethod(Description = "Executes a report to be exported to PPT.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToPPT/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToPPT(string token, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return CommandHelper.exportReportToPPT(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters);
        }

        [WebMethod(Description = "Executes a report to be exported to XLS.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToXLS/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToXLS(string token, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return CommandHelper.exportReportToXLS(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters);
        }

        [WebMethod(Description = "Executes a report to be exported to CSV.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToCSV/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToCSV(string token, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return CommandHelper.exportReportToCVS(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters);
        }

        [WebMethod(Description = "Executes a report to be exported to RTF.  Arguments are the login token, the space id, the report name, a list of filters and whether to turn off pagination or not.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:exportReportToRTF/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceId) = 36", "Space ID must be 36 characters")]
        public string exportReportToRTF(string token, string spaceId, string reportPath, List<Filter> reportFilters)
        {
            return CommandHelper.exportReportToRTF(WebServiceSessionHelper.getUserForToken(token), spaceId, reportPath, reportFilters);
        }

        [WebMethod(Description = "Gets the result of a report export call (exportReportToPNG...)")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getExportData/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:exportToken) = 32", "Invalid export token")]
        public string getExportData(string token, string exportToken)
        {
            return CommandHelper.getExportedData(WebServiceSessionHelper.getUserForToken(token), exportToken);
        }

        [WebMethod(Description = "Sets the SSO Password for a space.  Argments are the Login token, the space Id, and the password. The password must be atleast 8 characters.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSpaceSSOPassword/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:password) > 7", "Password must be atleast 8 characters")]
        [Assert("string-length(//b:password) < 256", "Password length must shorter than 256 characters")]
        public void setSpaceSSOPassword(string token, string spaceID, string password)
        {
            CommandHelper.setSpaceSSOPassword(WebServiceSessionHelper.getUserForToken(token), spaceID, password);
        }

        [WebMethod(Description = "Gets Birst to extract the data sources from salesforce.  Arguments are Login token, and the space id.  Returns an extraction token.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:extractSalesforceData/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public string extractSalesforceData(string token, string spaceID)
        {
            return CommandHelper.extractSalesforceData(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description = "Gets a JNLP file for a space.  Argments are the Login token, the space Id, and the name of the configuration file (normally <configurationName>_dconfig.xml, defaults to dcconfig.xml).")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSpaceJNLPFile/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:configfile) > 11", "Configuration file name length must be 12 or more characters")]
        [Assert("string-length(//b:configfile) < 256", "Configuration file name length must shorter than 256 characters")]
        public string getSpaceJNLPFile(string token, string spaceID, string configfile)
        {
            return CommandHelper.getSpaceJNLPFile(WebServiceSessionHelper.getUserForToken(token), spaceID, configfile, Context);
        }

        [WebMethod(Description = "Gets space detail information. Arguments are login token and space id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSpaceStatistics/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public SpaceStatistics getSpaceStatistics(string token, string spaceID)
        {
            return CommandHelper.getSpaceStatistics(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description = "Add generic JDBC database type real time connection information. Arguments are the Login token, the space Id, configFileName, connectionName, useDirectConnection, sqlType, driverName, connectionString, filter, userName, password and timeout.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setUpGenericJDBCRealTimeConnectionForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:configFileName) > 11", "Configuration file name length must be 12 or more characters")]
        [Assert("string-length(//b:configFileName) < 256", "Configuration file name length must shorter than 256 characters")]
        public bool setUpGenericJDBCRealTimeConnectionForSpace(string token, string spaceID, string configFileName, string connectionName,
                    bool useDirectConnection, string sqlType, string driverName, string connectionString, string filter, string userName, string password, int timeout)
        {
            return CommandHelper.setUpGenericJDBCRealTimeConnectionForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID,
                        configFileName, connectionName, useDirectConnection, sqlType, driverName, connectionString, filter, userName, password, timeout);
        }

        [WebMethod(Description = "Add real time connection information. Arguments are the Login token, the space Id, configFileName, connectionName, databaseType, useDirectConnection, host, port, databaseName, userName, password and timeout.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setUpRealTimeConnectionForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:configFileName) > 11", "Configuration file name length must be 12 or more characters")]
        [Assert("string-length(//b:configFileName) < 256", "Configuration file name length must shorter than 256 characters")]
        public bool setUpRealTimeConnectionForSpace(string token, string spaceID, string configFileName, string connectionName, 
                    string databaseType, bool useDirectConnection, string host, int port, string databaseName, string userName, string password, int timeout)
        {
            return CommandHelper.setUpRealTimeConnectionForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, configFileName,
                        connectionName, databaseType, useDirectConnection, host, port, databaseName, userName, password, timeout);
        }

        [WebMethod(Description = "Modify real time connection information.  Arguments are the Login token, the space id, the name of the connection, the host, the port, the name of the database, the user name, the password and the timeout.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:modifyRealTimeConnectionInformation/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:configFileName) > 11", "Configuration file name length must be 12 or more characters")]
        [Assert("string-length(//b:configFileName) < 256", "Configuration file name length must shorter than 256 characters")]
        public void modifyRealTimeConnectionInformation(string token, string spaceID, string configFileName, string connectionName, string host, int port, string databaseName, string userName, string password, int timeout)
        {
            CommandHelper.modifyRealTimeConnectionInformation(WebServiceSessionHelper.getUserForToken(token), spaceID, configFileName, connectionName, host, port, databaseName, userName, password, timeout);
        }

        [WebMethod(Description = "Import/Reimport cube using realtime connection connection. Arguments are the Login token, the space Id, connectionName, databaseType, cubeName, importType and cacheable.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:importCubeMetaDataIntoSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public bool importCubeMetaDataIntoSpace(string token, string spaceID, string connectionName, string databaseType, string cubeName, int importType, bool cacheable)
        {
            return CommandHelper.importCubeMetaDataIntoSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, connectionName, databaseType, cubeName, importType, cacheable);
        }
        
        [WebMethod(Description = "Sets the password for a user.  Argments are the Login token, the user name, and the password")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setUserPassword/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:userName) > 3", "username must be longer than 3 characters")]
        [Assert("string-length(//b:userName) < 256", "username must be shorter than 256 characters")]
        [Assert("string-length(//b:password) > 0", "password length must be 1 or more characters")]
        [Assert("string-length(//b:password) < 256", "password length must be shorter than 256 characters")]
        public void setUserPassword(string token, string userName, string password)
        {
            CommandHelper.setUserPassword(WebServiceSessionHelper.getUserForToken(token), userName, password);
        }

        [WebMethod(Description = "Clears the dashboard cache.  Arguments are the Login token and the space id")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:clearDashboardCache/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void clearDashboardCache(string token, string spaceID)
        {
            CommandHelper.clearDashboardCache(WebServiceSessionHelper.getUserForToken(token), token, spaceID);
        }

        [WebMethod(Description = "Renames a dashboard.  Arguments are the Login token, the space id, the old dashboard name and the new dashboard name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:renameDashboard/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:newDashName) > 0", "new dashboard name length must be 1 or more characters")]
        [Assert("string-length(//b:newDashName) <= 64", "new dashboard name length must be shorter than 64 characters")]
        public void renameDashboard(string token, string spaceID, string dashName, string newDashName)
        {
            CommandHelper.renameDashboard(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dashName, newDashName);
        }

        [WebMethod(Description = "Renames a dashboard page.  Arguments are the Login token, the space id, the dashboard name, the old page name and the new page name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:renameDashboardPage/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:newPageName) > 0", "new page name length must be 1 or more characters")]
        [Assert("string-length(//b:newPageName) <= 64", "new page name length must be shorter than 64 characters")]
        public void renameDashboardPage(string token, string spaceID, string dashName, string pageName, string newPageName)
        {
            CommandHelper.renameDashboardPage(WebServiceSessionHelper.getUserForToken(token), token, spaceID, dashName, pageName, newPageName);
        }


        [WebMethod(Description = "Enables/disables a data source.  Arguments are the Login token, the space id, the name of the data source, and an enabled flag.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:enableSourceInSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void enableSourceInSpace(string token, string spaceID, string dataSourceName, bool enabled)
        {
            CommandHelper.enableSourceInSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, dataSourceName, enabled);
        }

        
        [WebMethod(Description = "Returns a list of Languages in the sytem.  Arguments are the Login token")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:listLanguages/b:token) = 32", "Invalid token")]
        public List<Language> listLanguages(string token)
        {
            return CommandHelper.listLanguages(WebServiceSessionHelper.getUserForToken(token));
        }

        [WebMethod(Description = "Returns the language a specific user is set to. Arguments are the Login token, and the user name")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getLanguageForUser/b:token) = 32", "Invalid token")]
        public string getLanguageForUser(string token, string username)
        {
            return CommandHelper.getLanguageForUser(WebServiceSessionHelper.getUserForToken(token), username);
        }

        [WebMethod(Description = "Sets the language for a specific user. Arguments are the Login token, the user name and the language id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setLanguageForUser/b:token) = 32", "Invalid token")]
        public void setLanguageForUser(string token, string username, string localeId)
        {
            CommandHelper.setLanguageForUser(WebServiceSessionHelper.getUserForToken(token), username, localeId);
        }

        [WebMethod(Description = "Sets the space name. Argments are the Login token, the space Id, and the spaceName. The spaceName must be less than 256 characters and required.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSpaceName/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spaceName) > 0", "Space name is required")]
        [Assert("string-length(//b:spaceName) < 256", "Space names must be less than 256 characters")]    
        public void setSpaceName(string token, string spaceID, string spaceName)
        {
            CommandHelper.setSpaceName(WebServiceSessionHelper.getUserForToken(token), spaceID, spaceName);
        }

        [WebMethod(Description = "Sets the space comments.  Argments are the Login token, the space Id, and the spaceComments. The spaceName must be less than 2048 characters.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSpaceComments/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spaceComments) < 2048", "Space names must be less than 256 characters")]
        public void setSpaceComments(string token, string spaceID, string spaceComments)
        {
            CommandHelper.setSpaceComments(WebServiceSessionHelper.getUserForToken(token), spaceID, spaceComments);
        }

        [WebMethod(Description = "Sets the Email.  Argments are the Login token, the space Id, and the email. The email must be in valid format.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setEmailFromForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:email) > 0", "Email is required")]
       
        public void setEmailFromForSpace(string token, string spaceID, string email)
        {
            CommandHelper.setEmailFromForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, email);
        }

        [WebMethod(Description = "Sets the email subject.  Argments are the Login token, the space Id, and the subject.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setEmailSubjectForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setEmailSubjectForSpace(string token, string spaceID, string subject)
        {
            CommandHelper.setEmailSubjectForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, subject);
        }

        [WebMethod(Description = "Sets the background color for space.  Argments are the Login token, the space Id, and the background color.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setBackgroundColorForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:bgcolor) = 6", "For Background color, 6 digit hex value is required")]
        public void setBackgroundColorForSpace(string token, string spaceID, string bgcolor)
        {
            CommandHelper.setBackgroundColorForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, bgcolor);
        }

        [WebMethod(Description = "Sets the foreground color for space.  Argments are the Login token, the space Id, and the foreground color.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setForegroundColorForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:fgcolor) = 6", "Forground color, 6 digit hex value is required")]
        public void setForegroundColorForSpace(string token, string spaceID, string fgcolor)
        {
            CommandHelper.setForegroundColorForSpace(WebServiceSessionHelper.getUserForToken(token), spaceID, fgcolor);
        }

        [WebMethod(Description = "Sets the logo for space.  Argments are the Login token, the space Id, and the byte array.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setLogoForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]      
        public void setLogoForSpace(string token, string spaceID, byte[] image)
        {
            CommandHelper.setLogoForSpace(token, spaceID,image);
        }


        [WebMethod(Description = "Sets the default logo for space.  Argments are the Login token, the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setDefaultLogoForSpace/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]    
        public void setDefaultLogoForSpace(string token,string spaceID)
        {
            CommandHelper.setDefaultLogoForSpace(WebServiceSessionHelper.getUserForToken(token),spaceID);
        }

        [WebMethod(Description = "Gets sources list.  Argments are the Login token, the space Id.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSourcesList/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]    
        public List<String> getSourcesList(string token, string spaceID)
        {
            return CommandHelper.getSourcesList(token, spaceID);                       
        }
        [WebMethod(Description = "Gets source data.  Argments are the Login token, the space Id and the sourceName")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSourceDetails/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public Acorn.StagingTableSubClass getSourceDetails(string token, string spaceID, string sourceName)
        {
            return CommandHelper.getSourceDetails(token, spaceID, sourceName);
        }


        [WebMethod(Description = "Sets source data.  Argments are the Login token, the space Id and the source data")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSourceDetails/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setSourceDetails(string token, string spaceID, Acorn.StagingTableSubClass data) 
        {
            CommandHelper.setSourceDetails(WebServiceSessionHelper.getUserForToken(token), spaceID, data);
        }


        [WebMethod(Description = "Upload image to report catalog.  Argments are the Login token, the space Id, and the byte array.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:uploadImageToReportCatalog/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void uploadImageToReportCatalog(string token, string spaceID, string dir, string reportFileName, byte[] image)
        {
            CommandHelper.uploadImageToReportCatalog(token, spaceID, dir, reportFileName, image);
        }

        [WebMethod(Description = "Gets the space comments.  Argments are the Login token and the space Id. The spaceName must be less than 2048 characters.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSpaceComments/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spaceComments) < 2048", "Space names must be less than 256 characters")]
        public string getSpaceComments(string token, string spaceID)
        {
            return CommandHelper.getSpaceComments(WebServiceSessionHelper.getUserForToken(token), spaceID);
        }

        [WebMethod(Description = "Sets the space processing version.  Argments are the Login token, the space Id, and the processingVersion. The spaceName must be less than 2048 characters.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:setSpaceProcessEngineVersion/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        public void setSpaceProcessEngineVersion(string token, string spaceID, string processingVersionName)
        {
            CommandHelper.setSpaceProcessEngineVersion(WebServiceSessionHelper.getUserForToken(token), spaceID, processingVersionName);
        }
        [WebMethod(Description = "Gets the space processign engine version.  Argments are the Login token and the space Id. The spaceName must be less than 2048 characters.")]
        [Validation]
        [Assert("string-length(/s:Envelope/s:Body/b:getSpaceProcessEngineVersion/b:token) = 32", "Invalid token")]
        [Assert("string-length(//b:spaceID) = 36", "Space ID must be 36 characters")]
        [Assert("string-length(//b:spaceProcessEngineVersion) < 2048", "Space names must be less than 256 characters")]
        public string getSpaceProcessEngineVersion(string token, string spaceID)
        {
            string perVersion = null;
            PerformanceEngineVersion peVersion = null;
            peVersion = CommandHelper.getSpaceProcessEngineVersion(WebServiceSessionHelper.getUserForToken(token), spaceID);
            if(peVersion.Name != null)
            {
                perVersion = peVersion.Name;
            }
            return perVersion;
        }
    }
}
