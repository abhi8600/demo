using System;
using System.Collections.Generic;
using System.Text;

namespace Acorn
{
    class Version
    {
        public static int buildNumber = 1;
        public static string releaseNumber = "4.1";

        public static string majorMinorNumber
        {
            get
            {
                string rN = releaseNumber.TrimStart(' ');
                int i1 = rN.IndexOf('.');
                int i2 = rN.Substring(i1 + 1).IndexOf('.');
                return rN.Substring(0, i1 + i2 + 1);
            }
        }
    }
}
