﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class AdminRepository
    {   
        public string TimeDimensionName;
        public Hierarchy[] Hierarchies;
        public AdminDataSource[] Sources;
        public string RepositoryPath;
        public bool Published;
        public int queryLanguageVersion;
        public bool HasAdvancedAdmin;
        public PackageImport[] Imports;
        public string[] LoadGroups;
        public string[] ArrayOfSourceStrings;
        public bool SupportsFullOuterJoin;
        public int BuilderVersion;
        public int EngineVersion;
        public bool ShowNewConnectorsUI;
        public string RServerURL;
    }
}
