﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Net.Sockets;
using Acorn.WebDav;
using Acorn.Exceptions;
using Acorn.TrustedService;
using Acorn.DBConnection;
using System.Xml.Serialization;
using System.Xml;

namespace Acorn
{
    /// <summary>
    /// Summary description for AdminService
    /// </summary>
    [WebService(Namespace = "http://www.birst.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminService : System.Web.Services.WebService
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        [WebMethod(EnableSession = true)]
        public AdminRepository GetSources()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.GetSources(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting sources " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public DataSourceLight[] getSourcesList()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.getSourcesList(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting sources list" + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public AdminDataSource GetSourceDetails(string stagingTableName, string sourceFileName)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.getSourceDetails(Session, stagingTableName, sourceFileName);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown while getSourceDetails" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public Hierarchy[] GetHierarchies()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.getHierarchies(Session);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown while getHierarchies" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetSourceData(string stagingTableName, string sourceFileName)
        {
            try
            {
                Util.validateAdminUser(Session);
                Acorn.ManageSources.SourceData sourceData = ManageSources.getSourceData(Session, stagingTableName, sourceFileName);
                XmlSerializer serializer = new XmlSerializer(sourceData.GetType());
                StringWriter stream = new StringWriter();
                bool changeSeparator = false;
                if (sourceData.sourceFile != null && sourceData.sourceFile.Separator == "\t")
                {
                    changeSeparator = true;
                    sourceData.sourceFile.Separator = "TAB";
                }
                serializer.Serialize(stream, sourceData);
                if (changeSeparator)
                    sourceData.sourceFile.Separator = "\t";
                return stream.ToString();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown while getSourceData" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveSourceDataAndHierarchies(string el, Hierarchy[] hierarchiesThatChanged)
        {
            try
            {
                Util.validateAdminUser(Session);
                Acorn.ManageSources.SourceData sourceData = new ManageSources.SourceData();
                XmlSerializer serializer = new XmlSerializer(sourceData.GetType());
                MemoryStream stream = new MemoryStream();
                XmlDocument doc = new XmlDocument();
                doc.XmlResolver = null;
                doc.LoadXml(el);
                doc.Save(stream);
                stream.Position = 0;
                sourceData = (Acorn.ManageSources.SourceData)serializer.Deserialize(stream);
                if (sourceData.sourceFile != null && sourceData.sourceFile.Separator == "TAB")
                    sourceData.sourceFile.Separator = "\t";
                ManageSources.saveSourceDataAndHierarchies(Session, sourceData, hierarchiesThatChanged);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown while saveSourceDataAndHierarchies" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public StagingTable GetStagingTable(string stagingTableName, string sourceFileName)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.getStagingTable(Session, stagingTableName, sourceFileName);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetStagingTable" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveRepository()
        {
            try
            {
                Util.validateAdminUser(Session);
                Util.saveDirty(Session);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveRepository " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveStagingColumn(string tableName, StagingColumn sc)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.saveStagingColumn(Session, null, tableName, sc);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveStagingColumn" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveStagingColumns(string tableName, StagingColumn[] scList)
        {
            try
            {
                Util.validateAdminUser(Session);
                if (scList != null && scList.Length > 0)
                {
                    foreach (StagingColumn sc in scList)
                    {
                        ManageSources.saveStagingColumn(Session, null, tableName, sc);
                    }
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveStagingColumns" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveColumns(string tableName, string[] scList)
        {
            try
            {
                Util.validateAdminUser(Session);
                if (scList != null && scList.Length > 0)
                {

                    MainAdminForm maf = (MainAdminForm)Session["MAF"];
                    if (maf == null)
                        return;
                    bool isInUberTransaction = maf.isInUberTransaction();
                    maf.setInUberTransaction(true);
                    try
                    {
                        StagingTable st = maf.stagingTableMod.findTable(tableName);
                        if (st != null)
                        {
                            st.Columns = new StagingColumn[0];
                        }
                        SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                        if (sf != null)
                        {
                            sf.Columns = new SourceFile.SourceColumn[0];
                        }
                        StagingColumn sc = new StagingColumn();
                        StagingColumn scDayId = null;
                        List<StagingColumn> timeStagingColumns = new List<StagingColumn>();
                        XmlSerializer serializer = new XmlSerializer(sc.GetType());
                        foreach (string scString in scList)
                        {
                            MemoryStream stream = new MemoryStream();
                            XmlDocument doc = new XmlDocument();
                            doc.XmlResolver = null;
                            doc.LoadXml(scString);
                            doc.Save(stream);
                            stream.Position = 0;
                            sc = (StagingColumn)serializer.Deserialize(stream);
                            if (sc.Name == ApplicationBuilder.DayIDName)
                                scDayId = sc;
                            else if (sc.DataType.StartsWith("Date ID:"))
                                timeStagingColumns.Add(sc);
                            else
                                ManageSources.saveStagingColumn(Session, st, tableName, sc);
                        }

                        // add each Date ID:* column now
                        foreach (StagingColumn sc1 in timeStagingColumns)
                        {
                            string rootName = sc1.Name.Substring(0, sc1.Name.Length - ApplicationBuilder.DayIDName.Length - 1);
                            foreach (StagingColumn sc2 in st.Columns)
                            {
                                if ((sc2.DataType == "DateTime" || sc2.DataType == "Date") &&
                                    ((sc2.Name == rootName || st.Name.Substring(3) + " " + sc2.Name == rootName)))
                                {
                                    sc1.NaturalKey = true;
                                    if (sc2.GenerateTimeDimension == true)
                                        ManageSources.saveStagingColumn(Session, st, tableName, sc1);

                                    break;
                                }
                            }
                        }

                        // finally add Day ID column
                        if (scDayId != null)
                        {
                            scDayId.NaturalKey = true;
                            ManageSources.saveStagingColumn(Session, st, tableName, scDayId);
                        }

                    }
                    finally
                    {

                        Space sp = (Space)Session["space"];
                        User u = (User)Session["user"];
                        if (sp != null && u != null)
                            Util.saveApplication(maf, sp, Session, u);
                        maf.setInUberTransaction(isInUberTransaction);
                    }

                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveColumns " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void removeStagingColumn(string tableName, string name)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.removeStagingColumn(Session, tableName, name);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in removeStagingColumn" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void renameStagingColumn(string tableName, string oldName, string newName)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.renameStagingColumn(Session, tableName, oldName, newName);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in renameStagingColumn" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool saveHierarchy(Hierarchy h, string oldName)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.saveHierarchy(Session, h, oldName);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveHierarchy " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void removeHierarchy(string hname)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.removeHierarchy(Session, hname);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in removeHierarcy " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool saveLevel(string hierarchyName, Level l, string oldLevelName, bool rebuildApplication)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.saveLevel(Session, hierarchyName, l, oldLevelName, rebuildApplication);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveLevel " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveDataSource(string name, string displayName, string[][] levels, bool enabled, bool transactional, bool truncateonload, bool lockformat, bool schemalock,
            bool AllowAddColumns, bool AllowNullBindingRemovedColumns, bool AllowUpcast, bool AllowVarcharExpansion, bool AllowValueTruncationOnLoad, bool FailUploadOnVarcharExpansion,
            ScriptDefinition script, string[] loadGroups, string sourceGroups, string customTimeColumn, string customTimePrefix, CustomTimeShift[] timeShifts, string[] subGroups,
            SnapshotPolicy snapshots, string newColumnTarget, string dataSourceTimeZone, string[] sourceKey, string foreignKeys, string factForeignKeys, string parentForeignKeySource, string hierarchyName,
            string levelName, bool excludeFromModel, bool[] SFColPreventUpdate, bool customUpload, bool incrementalSnapshotFact, string[] snapshotDeleteKeys, OverrideLevelKey[] OverrideLevelKeys,
            bool onlyQuoteAfterSeparator, bool useBirstLocalForSource, string localConnForSource, SourceLocation[] locations, string encoding, string separator, bool replaceWithNull,
            bool skipRecord, string[] SFColFormats, bool discoveryTable, bool liveAccess, string tableSource, string[] inheritTables, bool unCached, string rServerPath, string rExpression, string rSettings)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.saveDataSource(Session, name, displayName, levels, enabled, transactional, truncateonload, lockformat, schemalock, AllowAddColumns, AllowNullBindingRemovedColumns,
                    AllowUpcast, AllowVarcharExpansion, AllowValueTruncationOnLoad, FailUploadOnVarcharExpansion, script, loadGroups, sourceGroups, customTimeColumn, customTimePrefix, timeShifts,
                    subGroups, snapshots, newColumnTarget, dataSourceTimeZone, sourceKey, foreignKeys, factForeignKeys, parentForeignKeySource, hierarchyName, levelName, excludeFromModel,
                    SFColPreventUpdate, customUpload, incrementalSnapshotFact, snapshotDeleteKeys, OverrideLevelKeys, onlyQuoteAfterSeparator, useBirstLocalForSource, localConnForSource,
                    locations, encoding, separator, replaceWithNull, skipRecord, SFColFormats, discoveryTable, liveAccess, tableSource, inheritTables, unCached, rServerPath, rExpression, rSettings);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveDataSource " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void saveDataSourceAndColumns(string name, string displayName, string[][] levels, bool enabled, bool transactional, bool truncateonload, bool lockformat, bool schemalock,
            bool AllowAddColumns, bool AllowNullBindingRemovedColumns, bool AllowUpcast, bool AllowVarcharExpansion, bool AllowValueTruncationOnLoad, bool FailUploadOnVarcharExpansion,
            ScriptDefinition script, string[] loadGroups, string sourceGroups, string customTimeColumn, string customTimePrefix, CustomTimeShift[] timeShifts, string[] subGroups,
            StagingColumn[] colList, SnapshotPolicy snapshots, string newColumnTarget, string dataSourceTimeZone, string[] sourceKey, string foreignKeys, string factForeignKeys, string parentForeignKeySource,
            string hierarchyName, string levelName, bool excludeFromModel, bool[] SFColPreventUpdate, bool customUpload, bool incrementalSnapshotFact, string[] snapshotDeleteKeys,
            OverrideLevelKey[] OverrideLevelKeys, bool onlyQuoteAfterSeparator, bool useBirstLocalForSource, string localConnForSource, string encoding, string separator, bool replaceWithNull,
            bool skipRecord, string[] SFColFormats, bool discoveryTable, bool liveAccess, string tableSource, string[] inheritTables, bool unCached, string rServerPath, string rExpression, string rSettings)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.saveDataSourceAndColumns(Session, name, displayName, levels, enabled, transactional, truncateonload, lockformat, schemalock, AllowAddColumns, AllowNullBindingRemovedColumns,
                    AllowUpcast, AllowVarcharExpansion, AllowValueTruncationOnLoad, FailUploadOnVarcharExpansion, script, loadGroups, sourceGroups, customTimeColumn, customTimePrefix, timeShifts,
                    subGroups, colList, snapshots, newColumnTarget, dataSourceTimeZone, sourceKey, foreignKeys, factForeignKeys, parentForeignKeySource, hierarchyName, levelName, excludeFromModel,
                    SFColPreventUpdate, customUpload, incrementalSnapshotFact, snapshotDeleteKeys, OverrideLevelKeys, onlyQuoteAfterSeparator, useBirstLocalForSource, localConnForSource, null,
                    encoding, separator, replaceWithNull, skipRecord, SFColFormats, discoveryTable, liveAccess, tableSource, inheritTables, unCached, rServerPath, rExpression, rSettings);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveDataSourceAndColumns " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public Acorn.TrustedService.WizardResult scriptWizard(string name, ScriptDefinition script)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ExecuteScripts.scriptWizard(Session, name, script);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in scriptWizard " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public Acorn.TrustedService.WizardResult formatScript(string name, ScriptDefinition script)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ExecuteScripts.formatScript(Session, name, script);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in formatScript " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public Acorn.TrustedService.ValidateResult saveAndValidateScript(string name, ScriptDefinition script, string part)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ExecuteScripts.validateScript(Session, name, script, part);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in saveAndValidateScript " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public Acorn.TrustedService.ExecuteResult executeScript(string name, ScriptDefinition script)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ExecuteScripts.executeScript(Session, name, script);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in executeScript " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void removeDataSource(string name)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.removeDataSource(Session, name);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in removeDataSource " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void EmptyDataSource(string name)
        {
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.emptyDataSource(Session, name);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in EmptyDataSource " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[][] getRawData(string sourceName, int startRow, int endRow)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSources.getRawData(Session, sourceName, startRow, endRow);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getRawData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public RARepository GetRepository()
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                return Util.getRepository(maf);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetRepository " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void SetRepository(RARepository rap)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                Util.setRepository(rap, maf, sp, Session, null);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in SetRepository " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public object[][] getSchemaTables(string connection)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                return LiveAccessUtil.getSchemaTables(Session, null, maf, sp, connection, null);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getSchemaTables " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public object[][] getCubes(string connection)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                return LiveAccessUtil.getCubes(Session, null, sp, connection);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getCubes " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public object[][] getCube(string connection, string name, int dimStructureImportType)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                return LiveAccessUtil.getCube(Session, null, sp, connection, name, dimStructureImportType);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getCube " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public object[][] getTableSchema(string connection, string[] tables)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                return LiveAccessUtil.getTableSchema(Session, null, maf, sp, connection, tables, null);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getTableSchema " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool isConnected(string[] connections)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return false;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return false;
                bool result = true;
                foreach (string s in connections)
                {
                    if (maf.EnableLiveAccessToDefaultConnection && s == Database.DEFAULT_CONNECTION)
                        continue;
                    bool isDirectConn = Utils.LiveAccessUtil.isDirectConnection(sp, s);
                    if (isDirectConn)
                        result = result && isDirectConn;
                    else
                        result = result && ConnectionRelay.isConnected(sp, s);
                }
                return result;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in isConnected" + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void updateScheduledReport(string id, string deliveryType, string reportPath, string triggerReportPath, string toReportPath, string[] toList, string subject, int interval, int dayofweek, int dayofmonth, int hour, int minute, string emailbody)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.deleteScheduledReport(conn, mainSchema, new Guid(id));
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                addScheduledReport(deliveryType, reportPath, triggerReportPath, toReportPath, toList, subject, interval, dayofweek, dayofmonth, hour, minute, emailbody);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateScheduledReport " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse runScheduledReport(string reportId)
        {
            try
            {
                Util.validateAdminUser(Session);
                GenericResponse response = new GenericResponse();
                QueryConnection conn = null;
                try
                {
                    User u = Util.getSessionUser(Session);
                    if (u == null)
                    {
                        Global.systemLog.Warn("Session User unavailable");
                        throw new UserNotLoggedInException();
                    }

                    Space sp = Util.getSessionSpace(Session);
                    if (sp == null)
                    {
                        Global.systemLog.Warn("Space Not found for logged in user : " + u.ID + " : " + u.Username);
                        throw new BirstException();
                    }

                    conn = ConnectionPool.getConnection();
                    ScheduledReport runNowReport = Database.getScheduledReport(conn, mainSchema, new Guid(reportId));
                    // Creates a new Guid for new entry into scheduled report table
                    runNowReport.ID = Guid.NewGuid().ToString();
                    runNowReport.Interval = ScheduledReport.INTERVAL_ONCE;

                    Database.addScheduledReport(conn, mainSchema, runNowReport);
                    Database.updateTaskSchedule(conn, mainSchema, sp.ID, new Guid(runNowReport.ID), ScheduledReport.MODULE_NAME, DateTime.Now, null);
                    TaskScheduler.wakeUpScheduler();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error during configurting scheduled report now ");
                    response.setException(ex);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in runScheduledReport " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void addScheduledReport(string deliveryType, string reportPath, string triggerReportPath, string toReportPath, string[] toList, string subject, int interval, int dayofweek, int dayofmonth, int hour, int minute, string emailbody)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                User u = (User)Session["user"];
                if (u == null)
                    return;
                QueryConnection conn = null;
                ScheduledReport sr = new ScheduledReport();
                sr.Type = deliveryType;
                sr.SpaceID = sp.ID.ToString();
                sr.ID = Guid.NewGuid().ToString();
                sr.User = u.ID.ToString();
                sr.ReportPath = "V{CatalogDir}" + reportPath;
                if (subject.Length > 255)
                    subject = subject.Substring(0, 255);
                sr.Subject = subject;
                sr.Interval = interval;
                sr.DayOfWeek = dayofweek;
                sr.DayOfMonth = dayofmonth;
                sr.Hour = hour;
                sr.Minute = minute;
                sr.EmailBody = XmlUtils.trimCData(emailbody);
                // Filter out any blank addresses
                List<string> toListFilter = new List<string>();
                foreach (string s in toList)
                {
                    if (s.Trim().Length > 0)
                        toListFilter.Add(s.Trim());
                }
                if (toListFilter.Count > 0)
                    sr.ToList = toListFilter.ToArray();
                if (triggerReportPath != null && triggerReportPath.Length > 0)
                    sr.TriggerReportPath = "V{CatalogDir}" + triggerReportPath;
                if (toReportPath != null && toReportPath.Length > 0)
                    sr.ToReportPath = "V{CatalogDir}" + toReportPath;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.addScheduledReport(conn, mainSchema, sr);
                    Database.updateTaskSchedule(conn, mainSchema, sp.ID, new Guid(sr.ID), ScheduledReport.MODULE_NAME, sr.calcNextTime(), null);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in addScheduledReport " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ScheduledReport[] getScheduledReports()
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                QueryConnection conn = null;
                List<ScheduledReport> rlist = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    rlist = Database.getScheduledReports(conn, mainSchema, sp.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                if (rlist == null)
                {
                    return null;
                }
                else
                {
                    // Filter all the reports with instant interval
                    List<ScheduledReport> updatedList = new List<ScheduledReport>();
                    foreach (ScheduledReport sItem in rlist)
                    {
                        if (sItem.Interval == ScheduledReport.INTERVAL_ONCE)
                        {
                            continue;
                        }
                        updatedList.Add(sItem);
                    }
                    return updatedList.ToArray();
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getScheduledReports " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ScheduledReport getScheduledReport(string id)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    ScheduledReport sr = Database.getScheduledReport(conn, mainSchema, new Guid(id));
                    return sr;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getScheduledReport " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteScheduledReport(string id)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Database.deleteScheduledReport(conn, mainSchema, new Guid(id));
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                return;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteScheduledReport " + e);
                throw e;
            }
        }

        public Acorn.TrustedService.ExecuteResult generateBulkQueryResults(Space sp, User u, string query, string reportPath,
            string filename, int numRows, bool useLogicalQuery, MainAdminForm maf)
        {
            clearExportDirectory(sp);
            IDictionary<string, string> preference = ManagePreferences.getUserPreference(null, u.ID, "TimeZone");
            string timezone = null;
            if (preference.ContainsKey("TimeZone"))
            {
                timezone = preference["TimeZone"];
            }
            // Make sure that user and group information is updated.
            UserAndGroupUtils.updatedUserGroupMappingFile(sp);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            Acorn.TrustedService.ExecuteResult er = null;
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            if (reportPath != null)
            {
                er = ts.exportBulkReportData(sp.Directory, sp.ID.ToString(), u.Username, timezone, "V{CatalogDir}" + reportPath + ".jasper", numRows, true, filename, useLogicalQuery, true, importedSpacesInfoArray);
            }
            else
            {
                er = ts.exportQueryData(sp.Directory, sp.ID.ToString(), u.Username, timezone, query, numRows, true, filename, useLogicalQuery, true, importedSpacesInfoArray);
            }
            if (er.success)
            {
                string exportFileName = filename;
                if (exportFileName == null && reportPath != null)
                {
                    int index = reportPath.LastIndexOf('\\');
                    exportFileName = reportPath;
                    if (index >= 0)
                        exportFileName = reportPath.Substring(index + 1);
                }
                moveToOutputDirectory(sp, exportFileName, filename);
            }
            else if (!er.success)
            {
                Global.systemLog.Info("TrustedService:exportData failed: " + er.errorMessage + " (" + er.errorCode + ')');
            }
            return er;
        }

        private static void moveToOutputDirectory(Space sp, string exportFileName, string outputFileName)
        {
            string path = Path.Combine(sp.Directory, "output");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (outputFileName == null)
                outputFileName = exportFileName;
            string file = Path.Combine(path, outputFileName + ".zip");
            string export = Path.Combine(sp.Directory, "data", "export", exportFileName + ".zip");
            Util.validateFileLocation(path, file);
            Util.validateFileLocation(Path.Combine(sp.Directory, "data", "export"), export);
            if (File.Exists(file))
                File.Delete(file);
            //clear OutputDirectory - keep maximum 10 files
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] files = di.GetFiles();
            Array.Sort(files, CompareFilesByDate);
            List<FileInfo> lstFiles = files.ToList();
            while (lstFiles.Count >= 10)
            {
                lstFiles[0].Delete();
                lstFiles.RemoveAt(0);
            }
            File.Move(export, file);
            Global.systemLog.Info("Moved " + export + " to " + file);
        }

        private static int CompareFilesByDate(FileInfo f1, FileInfo f2)
        {
            return DateTime.Compare(f1.LastWriteTime, f2.LastWriteTime);
        }

        private static void clearExportDirectory(Space sp)
        {
            /*
             * Delete any old files if beyond 10MB
             */
            DirectoryInfo di = new DirectoryInfo(Path.Combine(sp.Directory, "data", "export"));
            if (di.Exists)
            {
                List<FileInfo> filist = new List<FileInfo>();
                long length = 0;
                foreach (FileInfo fi in di.GetFiles())
                {
                    length += fi.Length;
                    filist.Add(fi);
                }
                while (length > 10000000)
                {
                    FileInfo oldestFI = null;
                    foreach (FileInfo fi in filist)
                    {
                        if (oldestFI == null || oldestFI.CreationTime > fi.CreationTime)
                            oldestFI = fi;
                    }
                    if (oldestFI != null)
                    {
                        length -= oldestFI.Length;
                        filist.Remove(oldestFI);
                        try
                        {
                            oldestFI.Delete();
                        }
                        catch (Exception e)
                        {
                            Global.systemLog.Error(e);
                        }
                    }
                }
            }
        }

        public Acorn.TrustedService.ExecuteResult exportData(Space sp, User u, string reportPath, bool moveToOutput, MainAdminForm maf, string rServerPath)
        {
            clearExportDirectory(sp);
            IDictionary<string, string> preference = ManagePreferences.getUserPreference(null, u.ID, "TimeZone");
            string timezone = null;
            if (preference.ContainsKey("TimeZone"))
            {
                timezone = preference["TimeZone"];
            }
            // Make sure that user and group information is updated.
            UserAndGroupUtils.updatedUserGroupMappingFile(sp);
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            Acorn.TrustedService.ExecuteResult er = ts.exportData(sp.Directory, sp.ID.ToString(), u.Username, timezone, "V{CatalogDir}" + reportPath + ".jasper", 250000, true, rServerPath, importedSpacesInfoArray);
            if (er.success && moveToOutput)
            {
                int index = reportPath.LastIndexOf('\\');
                string fname = reportPath;
                if (index >= 0)
                    fname = reportPath.Substring(index + 1);
                moveToOutputDirectory(sp, fname, fname);
            }
            else if (!er.success)
            {
                Global.systemLog.Info("TrustedService:exportData failed: " + er.errorMessage + " (" + er.errorCode + ')');
            }
            return er;
        }

        [WebMethod(EnableSession = true)]
        public Acorn.TrustedService.ExecuteResult exportData(string reportPath, string rServerPath)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                User u = (User)Session["user"];
                if (u == null)
                    return null;
                MainAdminForm maf = Util.getSessionMAF(Session);
                if (maf == null)
                    return null;
                Acorn.TrustedService.ExecuteResult er = exportData(sp, u, reportPath, false, maf, rServerPath);
                return er;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in exportData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getExportedDataFiles()
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                DirectoryInfo di = new DirectoryInfo(Path.Combine(sp.Directory, "data", "export"));
                if (!di.Exists)
                    return null;
                List<string> fnames = new List<string>();
                foreach (FileInfo fi in di.GetFiles())
                {
                    fnames.Add(fi.Name);
                }
                return fnames.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getExportedDataFiles " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getReportList()
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                List<string> rlist = new List<string>();
                if (Directory.Exists(sp.Directory + "\\catalog"))
                {
                    getReportFiles(sp, "", rlist);
                }
                return rlist.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getReportList " + e);
                throw e;
            }
        }

        private void getReportFiles(Space sp, string directory, List<string> curList)
        {
            DirectoryInfo di = new DirectoryInfo(sp.Directory + "\\catalog" + directory);
            foreach (FileInfo fi in di.GetFiles())
            {
                if (fi.Name.EndsWith(".AdhocReport") || fi.Name.EndsWith(".jrxml"))
                {
                    curList.Add(directory + "\\" + fi.Name.Substring(0, fi.Name.LastIndexOf('.')));
                }
            }
            foreach (DirectoryInfo cdi in di.GetDirectories())
            {
                getReportFiles(sp, directory + "\\" + cdi.Name, curList);
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse validateFilterExpression(string filterExpression)
        {
            try
            {
                Util.validateAdminUser(Session);
                GenericResponse response = new GenericResponse();
                try
                {
                    User u = Util.getSessionUser(Session);
                    if (u == null)
                    {
                        Global.systemLog.Warn("Session User unavailable");
                        throw new UserNotLoggedInException();
                    }

                    Space sp = Util.getSessionSpace(Session);
                    if (sp == null)
                    {
                        Global.systemLog.Warn("Space Not found for logged in user : " + u.ID + " : " + u.Username);
                        throw new BirstException();
                    }
                    MainAdminForm maf = Util.getSessionMAF(Session);
                    if (maf == null)
                    {
                        Global.systemLog.Warn("Maf : repository object : not found for logged in user : " + u.ID + " : " + u.Username);
                        throw new BirstException();
                    }

                    string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    Acorn.TrustedService.TrustedService trustedService = new Acorn.TrustedService.TrustedService();
                    trustedService.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                    Acorn.TrustedService.ExecuteResult result = trustedService.validateFilterExpression(u.Username, sp.ID.ToString(), sp.Directory, filterExpression, importedSpacesInfoArray);
                    // output from the result is passed to GenericResponse object in "other" array
                    if (result.errorCode != 0)
                    {
                        response.other = new string[] { result.errorMessage };
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Exception in filterexpression validation ", ex);
                    response.setException(ex);
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in validateFilterExpression " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse validateQuery(string query, string filterExpression, string buildFilterExpression, string connectionName)
        {
            try
            {
                Util.validateAdminUser(Session);
                GenericResponse response = new GenericResponse();
                try
                {
                    User u = Util.getSessionUser(Session);
                    if (u == null)
                    {
                        Global.systemLog.Warn("Session User unavailable");
                        throw new UserNotLoggedInException();
                    }

                    Space sp = Util.getSessionSpace(Session);
                    if (sp == null)
                    {
                        Global.systemLog.Warn("Space Not found for logged in user : " + u.ID + " : " + u.Username);
                        throw new BirstException();
                    }
                    MainAdminForm maf = Util.getSessionMAF(Session);
                    if (maf == null)
                    {
                        Global.systemLog.Warn("Maf : repository object : not found for logged in user : " + u.ID + " : " + u.Username);
                        throw new BirstException();
                    }

                    string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    Acorn.TrustedService.TrustedService trustedService = new Acorn.TrustedService.TrustedService();
                    trustedService.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                    Acorn.TrustedService.ExecuteResult result = trustedService.validateQuery(u.Username, sp.ID.ToString(), sp.Directory, query, filterExpression, buildFilterExpression, connectionName, importedSpacesInfoArray);
                    // output from the result is passed to GenericResponse object in "other" array
                    if (result.errorCode != 0)
                    {
                        response.other = new string[] { result.errorMessage };
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Exception in query validation ", ex);
                    response.setException(ex);
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in validateQuery " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public BirstConnectConfig[] getBirstConnectConfigs()
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                return BirstConnectUtil.getBirstConnectConfigs(sp);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getBirstConnectConfigs " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteBirstConnectConfig(string filename)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                BirstConnectUtil.deleteBirstConnectConfig(sp, filename);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteBirstConnectConfig " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public int canDeleteBirstConnectConfig(string filename)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                return BirstConnectUtil.hasAnyRealtimeConnectionInUse(sp, maf, filename);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in canDeleteBirstConnectConfig " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void addBirstConnectConfig(string filename)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                BirstConnectUtil.addBirstConnectConfig(sp, filename);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in addBirstConnectConfig " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void renameBirstConnectConfig(string oldname, string newname)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                BirstConnectUtil.renameBirstConnectConfig(sp, oldname, newname);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in renameBirstConnectConfig " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public VariableWrapper[] getVariables()
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;

                maf.setRepositoryDirectory(sp.Directory);
                List<Variable> vars = maf.getVariables();
                List<VariableWrapper> nonSystemVars = new List<VariableWrapper>();
                foreach (Variable var in vars)
                {
                    if (var.Name.StartsWith("Load:"))
                        continue;
                    VariableWrapper varWarapper = new VariableWrapper();
                    varWarapper.Variable = var;
                    nonSystemVars.Add(varWarapper);
                }

                List<VariableWrapper> importedVariables = PackageUtils.getImportedVariables(maf, Session);
                if (importedVariables != null && importedVariables.Count > 0)
                {
                    nonSystemVars.AddRange(importedVariables);
                }
                return nonSystemVars.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getVariables " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public String updateVariable(Variable v)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                User u = (User)Session["user"];
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;

                try
                {
                    maf.setRepositoryDirectory(sp.Directory);
                    maf.updateVariable(v, u.Username);
                    Session["AdminDirty"] = true;
                    saveRepository();
                }
                catch (Exception e)
                {
                    return e.Message;
                }
                return null;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateVariable " + e);
                return e.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteVariable(Variable var)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;

                maf.setRepositoryDirectory(sp.Directory);
                maf.deleteVariable(var);
                Session["AdminDirty"] = true;
                saveRepository();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteVariable " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public CustomFormulasOutput[] getLogicalExpressions()
        {
            try
            {
                Util.validateAdminUser(Session);
                List<CustomFormulasOutput> response = new List<CustomFormulasOutput>();
                List<CustomFormulasOutput> nativeCustomFormulas = CustomCalculations.getLogicalExpressions(Session);
                if (nativeCustomFormulas != null && nativeCustomFormulas.Count > 0)
                {
                    response.AddRange(nativeCustomFormulas);
                }
                List<CustomFormulasOutput> importedCustomFormulas = CustomCalculations.getImportedLogicalExpressions(Session);
                if (importedCustomFormulas != null && importedCustomFormulas.Count > 0)
                {
                    response.AddRange(importedCustomFormulas);
                }
                return response.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getLogicalExpressions " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public AggregateOutput[] getAggregates()
        {
            try
            {
                Util.validateAdminUser(Session);
                return CustomCalculations.getAggregates(Session);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getAggregates " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string updateAggregate(Aggregate ag)
        {
            try
            {
                Util.validateAdminUser(Session);
                string ret = CustomCalculations.updateAggregate(Session, ag);
                if (ret == null)
                    saveRepository();
                return ret;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateAggregate " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteAggregate(Aggregate ag)
        {
            try
            {
                Util.validateAdminUser(Session);
                CustomCalculations.deleteAggregate(Session, ag);
                saveRepository();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteAggregate " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string updateCustomMeasure(LogicalExpression le, string grain)
        {
            try
            {
                Util.validateAdminUser(Session);
                string ret = CustomCalculations.updateLogicalExpression(Session, le, grain, "Custom measure");
                if (ret == null)
                    saveRepository();

                return ret;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateCustomMeasure " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string updateCustomAttribute(LogicalExpression le, string level)
        {
            try
            {
                Util.validateAdminUser(Session);
                string ret = CustomCalculations.updateLogicalExpression(Session, le, level, "Custom attribute");
                if (ret == null)
                    saveRepository();
                return ret;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateCustomAttribute " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteLogicalExpression(LogicalExpression le, string grainorlevel)
        {
            try
            {
                Util.validateAdminUser(Session);
                CustomCalculations.deleteLogicalExpression(Session, le, grainorlevel);
                saveRepository();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteLogicalExpression " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public VirtualColumn[] getBucketedMeasures()
        {
            try
            {
                Util.validateAdminUser(Session);
                return CustomCalculations.getBucketedMeasures(Session);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getBucketedMeasure " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public String updateBucketedMeasure(VirtualColumn vc)
        {
            try
            {
                Util.validateAdminUser(Session);
                string ret = CustomCalculations.updateBucketedMeasure(Session, vc);
                if (ret == null)
                    saveRepository();
                return ret;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in updateBucketedMeasure " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public void deleteBucketedMeasure(VirtualColumn vc)
        {
            try
            {
                Util.validateAdminUser(Session);
                CustomCalculations.deleteBucketedMeasure(Session, vc);
                saveRepository();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in deleteBucketedMeasure " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getMeasurePrefixes()
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null || maf.aggAvailableMeasures == null)
                    return null;
                return maf.aggAvailableMeasures.Keys.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getMeasurePrefixes " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getMeasureList(string prefix)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null || maf.aggAvailableMeasures == null)
                    return null;
                if (prefix == null || prefix.Length == 0)
                    prefix = "<blank>";
                return maf.aggAvailableMeasures[prefix].ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getMeasureList " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults getDataTables()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getDataTables(Session);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getDataTables " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults getColumnProperties(string type, string tableName)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getColumnProperties(Session, type, tableName);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getColumnProperties " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults convertColumn(string type, string tableName, string name, string dataType, int width, bool disk)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.convertColumn(Session, type, tableName, name, dataType, width, disk);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in convertColumn " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults getLevelData(string name, bool profile, string query)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getLevelData(Session, name, profile, query);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getLevelData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults getMeasureTableData(string name, bool profile, string query)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getMeasureTableData(Session, name, profile, query);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getMeasureTableData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ViewProcessedResults getStagingTableData(string name, bool profile, string query)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getStagingTableData(Session, name, profile, query);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getStagingTableData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string getTableQueryString(string name, string type)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ViewProcessed.getTableQueryString(Session, name, type);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getTableQueryString " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getGrainList()
        {
            try
            {
                Util.validateAdminUser(Session);
                List<string> result = new List<string>();
                result.AddRange(CustomCalculations.getGrains(Session).Keys);
                result.AddRange(CustomCalculations.getDiscoverySources(Session));
                return result.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getGrainList " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getGrainColumnList(string grain)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                CustomCalculations.setMetricColumnMap(Session, grain, sp.Automatic);
                Dictionary<string, string> columnMap = (Dictionary<string, string>)Session["columnMap"];
                if (columnMap == null)
                    return null;
                return columnMap.Keys.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getGrainColumnList " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getLevelList()
        {
            try
            {
                Util.validateAdminUser(Session);
                return CustomCalculations.getLevels(Session).ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getLevelList " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[] getLevelColumnList(string levelname)
        {
            try
            {
                Util.validateAdminUser(Session);
                int index = levelname.IndexOf('.');
                string dim = levelname.Substring(0, index);
                string level = levelname.Substring(index + 1);
                CustomCalculations.setAttributeColumnMap(Session, dim, level);
                Dictionary<string, string> columnMap = (Dictionary<string, string>)Session["columnMap"];
                if (columnMap == null)
                    return null;
                return columnMap.Keys.ToArray();
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getLevelColumnList " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public string[][] getColumnsAtLowerGrains(string sourceName)
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                return ManageSources.getColumnsAtLowerGrains(Session, sourceName, sp.Automatic);
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in getColumnsAtLowerGrain " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public bool ping()
        {
            // Ping to keep session alive
            return true;
        }

        [WebMethod(EnableSession = true)]
        public ProcessOptions GetProcessInitData(bool automode, String loadGroup)
        {
            try
            {
                Util.validateAdminUser(Session);
                ProcessOptions response = null;
                try
                {
                    response = ProcessLoad.getProcessInitData(Session, automode);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new ProcessOptions();
                    ErrorOutput error = new ErrorOutput();
                    response.ErrorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetProcessInitData " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public ProcessOptions GetProgress()
        {
            try
            {
                Util.validateAdminUser(Session);
                ProcessOptions response = null;
                try
                {
                    // this makes sure that both the manual and scheduled processing
                    bool fetchOldWay = true;
                    if (Util.useExternalSchedulerForProcess(Util.getSessionSpace(Session), Util.getSessionMAF(Session)))
                    {
                        response = ProcessLoad.getProgressStatus2(Session);
                        if (response.CurrentState == ResponseMessages.STATUS_PROCESSING)
                        {
                            fetchOldWay = false;
                        }
                    }

                    if (fetchOldWay)
                    {
                        response = ProcessLoad.getProgressStatus(Session);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new ProcessOptions();
                    ErrorOutput error = new ErrorOutput();
                    response.ErrorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetProgress " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse publish(String loadDate, String loadGroup, string[] subgroups, bool retryFailedLoad, bool independentMode, bool reprocessmodified)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                bool isInUberProcess = false;
                if (maf != null)
                {
                    isInUberProcess = maf.isInUberTransaction();
                    maf.setInUberTransaction(true);
                }
                GenericResponse response = null;
                try
                {
                    subgroups = subgroups != null && subgroups.Length == 0 ? null : subgroups;
                    response = ProcessLoad.startPublishing(Session, HttpContext.Current.Request, loadDate, loadGroup, subgroups, retryFailedLoad, independentMode, reprocessmodified);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new GenericResponse();
                    if (ex is BirstException)
                    {
                        response.setException(ex);
                    }
                    else
                    {
                        ErrorOutput error = new ErrorOutput();
                        response.Error = error;
                        error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    }
                }
                finally
                {
                    if (maf != null)
                        maf.setInUberTransaction(isInUberProcess);
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in publish " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DashboardConfiguration ConfigureDashboards(String loadDate, String loadGroup, string[] subGroups, bool retryFailedLoad, bool independentMode)
        {
            try
            {
                Util.validateAdminUser(Session);
                DashboardConfiguration response = null;
                try
                {
                    response = ProcessLoad.configureDashboardOptions(Session, HttpContext.Current.Request, loadDate, loadGroup, subGroups, retryFailedLoad, independentMode);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DashboardConfiguration();
                    ErrorOutput error = new ErrorOutput();
                    response.ErrorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in ConfigureDashboards " + e);
                throw e;
            }
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse PublishWithDashboards(string[] measureOptions)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                bool isInUberTransaction = false;
                if (maf != null)
                {
                    isInUberTransaction = maf.isInUberTransaction();
                    maf.setInUberTransaction(true);
                }
                GenericResponse response = null;
                try
                {
                    response = ProcessLoad.publishWithDashboard(Session, measureOptions);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new GenericResponse();
                    ErrorOutput error = new ErrorOutput();
                    response.Error = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                finally
                {
                    if (maf != null)
                        maf.setInUberTransaction(isInUberTransaction);
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in PublishWithDashboards " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public PublishHistoryResult GetPublishHistory()
        {
            try
            {
                Util.validateAdminUser(Session);
                PublishHistoryResult response = null;
                try
                {
                    response = ProcessLoad.getPublishHistory(Session);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new PublishHistoryResult();
                    ErrorOutput error = new ErrorOutput();
                    response.Error = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetPublishHistory " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DeleteDataOutput CanDeleteLast()
        {
            try
            {
                Util.validateAdminUser(Session);
                DeleteDataOutput response = null;
                try
                {
                    response = DeleteDataService.canDeleteLast(Session);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DeleteDataOutput();
                    ErrorOutput error = new ErrorOutput();
                    response.errorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in CanDeleteLast " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DeleteDataOutput DeleteLast(bool restore)
        {
            try
            {
                Util.validateAdminUser(Session);
                DeleteDataOutput response = null;
                try
                {
                    response = DeleteDataService.deleteLast(Session, HttpContext.Current.Request, restore);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DeleteDataOutput();
                    ErrorOutput error = new ErrorOutput();
                    response.errorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in DeleteLast " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DeleteDataOutput DeleteAll()
        {
            try
            {
                Util.validateAdminUser(Session);
                DeleteDataOutput response = null;
                try
                {
                    response = DeleteDataService.deleteAll(Session, HttpContext.Current.Request);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DeleteDataOutput();
                    ErrorOutput error = new ErrorOutput();
                    response.errorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in DeleteAll " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DeleteDataOutput DeleteSpace()
        {
            try
            {
                Util.validateAdminUser(Session);
                DeleteDataOutput response = null;
                try
                {
                    response = DeleteDataService.deleteSpace(Session, HttpContext.Current.Request);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DeleteDataOutput();
                    ErrorOutput error = new ErrorOutput();
                    response.errorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in DeleteSpace " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public DeleteDataOutput DeleteOverview()
        {
            try
            {
                Util.validateAdminUser(Session);
                DeleteDataOutput response = new DeleteDataOutput();
                try
                {
                    response = DeleteDataService.deleteOverview(Session);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new DeleteDataOutput();
                    ErrorOutput error = new ErrorOutput();
                    response.errorOutput = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in DeleteOverview " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public TemplateCategories GetTemplateCategories()
        {
            try
            {
                TemplateCategories response = new TemplateCategories();
                try
                {
                    response = TemplateService.getTemplateCategories(Session);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex);
                    response = new TemplateCategories();
                    ErrorOutput error = new ErrorOutput();
                    response.Error = error;
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in GetTemplateCategories " + e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true)]
        public TemplateList GetTemplates(string categoryName, string sortExpression, bool rate)
        {
            TemplateList response = null;
            try
            {
                string sortExp = sortExpression.Trim().Length == 0 ? null : sortExpression;
                response = TemplateService.getTemplates(Session, categoryName, sortExp, rate);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new TemplateList();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetAttachmentList()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = TemplateService.getAttachmentNames(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetAttachmentProcessOuput()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = TemplateService.getAttachmentProcessOutput(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            return response;
        }

        /*
         * Start of SFDC related webservices
        */

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorCredentials(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getConnectorCredentials(Session, user, sp, connectorName);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorMetaData(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getMetaData(Session, user, sp, connectorName);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector meta data ", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse saveConnectorQueryObject(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.saveQueryObject(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in saving query object", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse saveConnectorObject(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.saveObject(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in saving object", ex);
                response.setException(ex);
            }
            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorObject(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getObject(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting query object details", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorObjectQuery(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getObjectQuery(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting query object details", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse validateConnectorObjectQuery(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.validateObjectQuery(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in validating query object", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse validateConnectorObject(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.validateObject(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in validating object", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorObjectDetails(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getObjectDetails(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting query object details", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorSavedObjectDetails(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getSavedObjectDetails(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting saved object details", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse saveConnectorCredentials(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.saveConnectorCredentials(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse testConnection(string connectorConnectionString, string credentialsXML)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.testConnection(Session, user, sp, connectorConnectionString, credentialsXML);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in testConnection", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse saveConnection(string connectorConnectionString, string connectionDetailsXML)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.saveConnection(Session, user, sp, connectorConnectionString, connectionDetailsXML);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in saveConnection", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorsList(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getListofConnectors(Session, user, sp);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getCloudConnectorConfigXML()
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                if (sp == null)
                    return null;
                XElement elem = ConnectorUtils.getCloudConnectorConfigXML(Session, sp);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting CloudConnections list" + ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorSelectedObjects(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getSelectedObjects(Session, user, sp, connectorName);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getAllConnectorObjects(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getAllObjects(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getAllConnectorObjects", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getAllObjectsForCloudConnection(string connectorID, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getAllObjectsForCloudConnection(Session, user, sp, connectorID, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getAllObjectsForCloudConnection", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse saveSelectedConnectorObjects(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.saveSelectedObjects(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updateSelectedConnectorObjects(string connectionName, string connectorType, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.updateSelectedConnectorObjects(Session, user, sp, connectionName, connectorType, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in updateSelectedConnectorObjects", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse startConnectorExtract(string connectorName, string xmlData, string extractGroups)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                string[] selectedExtractGroups = null;
                if (extractGroups != null && extractGroups != "All")
                {
                    selectedExtractGroups = extractGroups.Split(',');
                }
                XElement elem = ConnectorUtils.startExtract(Session, user, sp, connectorName, xmlData, selectedExtractGroups);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorExtractStatus(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getExtractionStatus(Session, user, sp, connectorName);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse cancelConnectorExtract(string connectorName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                ConnectorUtils.cancelExtract(Session, user, sp, connectorName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector credentials", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getConnectorDynamicParameters(string connectorName, string xmlData)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getConnectorDynamicParameters(Session, user, sp, connectorName, xmlData);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting connector dynamic parameters", ex);
                response.setException(ex);
            }
            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse killExtraction()
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.validateAndGetSpace(Session);
                SalesforceLoader.clearLoader(sp.ID);
                Util.deleteSFDCExtractFile(sp.Directory);
                User u = Util.validateAndGetUser(Session);
                ConnectorUtils.deleteAllConnectorSpecificExtractFiles(Session, sp, u);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :  killExtraction : ", ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetCurrentSession()
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;

            try
            {
                Util.validateAdminUser(Session);
                HttpCookieCollection httpCookies = HttpContext.Current.Request.Cookies;
                List<string> responseCookies = new List<string>();
                for (int i = 0; i < httpCookies.Count; i++)
                {
                    string incomingCookie = httpCookies[i].Name + "=" + httpCookies[i].Value;
                    responseCookies.Add(incomingCookie);
                }

                response.other = responseCookies.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to get session details";
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse CreateNewTemplate(string categoryName, string templateName, string templateComments, bool isPrivate)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = TemplateService.createNewTemplate(Session, categoryName, templateName, templateComments, isPrivate);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse RemoveAttachment(string fileName)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = TemplateService.removeAttachment(Session, fileName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GenerateSSOCredentials()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = SpaceAdminService.generateSSOCredentials(Session, HttpContext.Current.Server);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse ConvertSpace()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = SpaceAdminService.convertSpace(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse ResetSpaceSettings()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = SpaceAdminService.resetSpaceSettings(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public SpaceProperties GetSpaceProperties()
        {
            SpaceProperties response = null;
            try
            {
                response = SpaceAdminService.getSpaceProperties(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new SpaceProperties();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                if (ex is BirstException)
                {
                    error.ErrorMessage = ex.Message;
                }

                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse ModifySpaceProperties(string spaceName, string spaceComments, bool testMode, bool enableSSO,
            string bgcolor, string fgcolor, string spaceProcessingTimeZone, int maxQueryRows, int maxQueryTime, int queryLanguageVersion,
            bool enableUsage, int peVersionID, bool useDynamicGroups, bool allowRetryFailedLoad, int numThresholdFailedRecords, 
            int minYear, int maxYear, bool mapNullsToZero, string scheduleFrom, string scheduleSubject,string databaseCollation,
            string rServerURL, int rServerPort, string rServerUsername, string rServerPassword)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = SpaceAdminService.modifySpaceProperties(Session, HttpContext.Current.Server, spaceName, spaceComments, testMode, enableSSO,
                    bgcolor, fgcolor, spaceProcessingTimeZone, maxQueryRows, maxQueryTime, queryLanguageVersion, enableUsage, peVersionID, useDynamicGroups, 
                    allowRetryFailedLoad, numThresholdFailedRecords, minYear, maxYear, mapNullsToZero, scheduleFrom, scheduleSubject,databaseCollation,
                    rServerURL, rServerPort, rServerUsername, rServerPassword);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                if (ex is BirstException)
                {
                    error.ErrorMessage = ex.Message;
                }
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public UploaderResponse ProcessUploadedFile(string clientFileName, string beginSkip, string endSkip, bool firstRowNames, bool lockFormat,
            bool ignoreInternalQuotes, string[][] escape, bool filterRows, string encoding, bool consolidate, string separator, bool autoModel, string quoteCharcter, bool ignorecarriagereturn, bool forceNumColumns, bool browserUpload)
        {
            UploaderResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ApplicationUploadService.processUploadedFile(Session, clientFileName, beginSkip, endSkip, firstRowNames, lockFormat,
                    ignoreInternalQuotes, escape, filterRows, encoding, consolidate, separator, autoModel, quoteCharcter, ignorecarriagereturn, forceNumColumns, browserUpload);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new UploaderResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public UploaderResponse ProcessNextStage(string beginSkip, string endSkip, bool firstRowNames, bool lockFormat,
            bool ignoreInternalQuotes, string[][] escape, bool filterRows, string encoding, bool consolidate, string separator, bool autoModel)
        {
            UploaderResponse response = null;

            try
            {
                Util.validateAdminUser(Session);
                response = ApplicationUploadService.processNextStage(Session, beginSkip, endSkip, firstRowNames, lockFormat,
                    ignoreInternalQuotes, escape, filterRows, encoding, consolidate, separator, autoModel);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new UploaderResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse CancelNextStage()
        {
            GenericResponse response = null;

            try
            {
                Util.validateAdminUser(Session);
                response = ApplicationUploadService.cancelNextStage(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;

        }

        [WebMethod(EnableSession = true)]
        public DashboardConfiguration AutoPublish()
        {
            DashboardConfiguration response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ProcessLoad.autoPublish(Session, HttpContext.Current.Request);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new DashboardConfiguration();
                ErrorOutput error = new ErrorOutput();
                response.ErrorOutput = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public UserSpaces GetAllSpaces()
        {
            UserSpaces response = null;

            try
            {
                response = DefaultUserPage.getAllSpaces(Session, HttpContext.Current.Request);
                // cleanup for external use (zero out the SpacePath setting)
                if (response.SpacesList != null)
                {
                    foreach (SpaceSummary ss in response.SpacesList)
                    {
                        ss.SpacePath = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new UserSpaces();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;

        }

        [WebMethod(EnableSession = true)]
        public SpaceDetails GetSpaceDetails(string spaceID, string spaceName)
        {
            SpaceDetails response = null;

            try
            {
                response = DefaultUserPage.getSpaceDetails(Session, HttpContext.Current.Request, spaceID, spaceName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new SpaceDetails();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public SpaceDetails GetLoggedInSpaceDetails()
        {
            SpaceDetails response = null;

            try
            {
                response = DefaultUserPage.getLoggedInSpaceDetails(Session, HttpContext.Current.Request, HttpContext.Current.Response);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new SpaceDetails();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse RemoveMember()
        {
            GenericResponse response = new GenericResponse();
            try
            {
                response = DefaultUserPage.removeMember(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse SendInvitationResponse(bool accept, string spaceID, string userEmail)
        {
            GenericResponse response = null;
            try
            {
                response = DefaultUserPage.sendInvitationResponse(Session, accept, spaceID, userEmail);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse CreateSpaceCopy(string spaceName, string schemaName, string copyComments)
        {
            GenericResponse response = null;
            try
            {                
                response = SpaceAdminService.createCopy(Session, spaceName, schemaName, copyComments);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                if (ex is SchemaException)
                {
                    error.ErrorMessage = ex.Message;
                    error.ErrorType = ((SchemaException)ex).getErrorType();
                }
                else
                {
                response.setException(ex);
            }
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse CreateNewSpace(string spaceName, string schemaName, string spaceComments, int spaceMode, bool useTemplate, int queryLanguageVersion, string templateID, int spaceType)
        {
            GenericResponse response = null;

            try
            {
                string tid = templateID.Trim().Length == 0 ? null : templateID;
                response = NewSpaceService.createSpace(Session, spaceName, schemaName, spaceComments, spaceMode, useTemplate, queryLanguageVersion, tid, (User)Session["User"], spaceType);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                if (ex is SchemaException)
                {
                    Global.systemLog.Error(ex);
                    error.ErrorMessage = ex.Message;
                    error.ErrorType = ((SchemaException)ex).getErrorType();
                }
                else
                {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            }

            return response;
        }

        /// <summary>
        /// Returns the redshift space types for a particular user
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public SpaceTypeDetails getSpaceTypes()
        {
            SpaceTypeDetails response = null;
            try
            {
                response = new SpaceTypeDetails();
                User user = Util.validateAndGetUser(Session);
                if (user.ExternalDBEnabled)
                {
                    List<SpaceTypeDetail> list = UserAdministration.getAllAvailableRedShiftClusters(user);
                    if (list != null && list.Count > 0)
                    {
                        response.SpaceTypes = list.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);                
                response.setException(ex);                
            }

            return response;
        }

        /// <summary>
        /// Returns all the region details supported for Redshift
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public AWSRegions getRedshiftAvailableRegions()
        {
            AWSRegions response = new AWSRegions();            
            try
            {
                User u = Util.validateAndGetUser(Session);
                UserAdministration.validateUserManagementAuthorization(u);
                List<AWSRegion> activeRegions = UserAdministration.getAvailableRegionsForRedshift();
                if (activeRegions != null && activeRegions.Count > 0)
                {
                    response.SupportedRegions = activeRegions.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getRedshiftAvailableRegions ",  ex);
                response.setException(ex);
            }
            return response;
        }

        /// <summary>
        /// Returns the redshift space types for a particular user
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public SpaceTypeDetails getRedShiftClusters()
        {
            SpaceTypeDetails response = null;
            try
            {
                response = new SpaceTypeDetails();
                User user = Util.validateAndGetUser(Session);
                List<SpaceTypeDetail> list = UserAdministration.getAllAdminRedShiftClusters(user);
                if (list != null && list.Count > 0)
                {
                    response.SpaceTypes = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }

            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse startTimeCreation(int type, bool dropIfExists)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.validateAndGetUser(Session);
                UserAdministration.startTimeCreation(user, type, dropIfExists);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse validateTimeDimensions(int type)
        {
            GenericResponse response = null;            
            try
            {
                response = new GenericResponse();
                User user = Util.validateAndGetUser(Session);
                Status.StatusStep step = UserAdministration.validateTimeDimensions(type);
                if (step != null)
                {
                    if (step.message != null && step.message.Length > 0)
                    {
                        response.other = new string[] { step.result.ToString(), step.message };
                    }
                    else{
                        response.other = new string[] { step.result.ToString() };
                    }                    
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse addRedShiftCluster(string name, string description, int regionId, string jdbcUrl, 
            string dbUser, string dbPwd)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.validateAndGetUser(Session);
                Util.validateAndGetODBCDBPwd(dbPwd);    //validates password against driver version
                int timeCreation = 0;
                UserAdministration.addRedShiftCluster(user, name, description, regionId, jdbcUrl, dbUser, dbPwd, out timeCreation);
                response.other = new string[] { timeCreation.ToString() };                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse modifyRedShiftCluster(int type, string name, string description, int regionId, string jdbcUrl, 
            string dbUser, string dbPwd)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.validateAndGetUser(Session);
                Util.validateAndGetODBCDBPwd(dbPwd);    //validates password against driver version
                UserAdministration.modifyRedShiftCluster(user, type, name, description, regionId, jdbcUrl, dbUser, dbPwd);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse removeRedShiftCluster(int typeID)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.validateAndGetUser(Session);
                UserAdministration.deleteRedShiftCluster(Session, user, typeID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public SpaceSummaryList getRedShiftClusterUsages(int spaceType)
        {
            SpaceSummaryList response = null;
            try
            {
                User user = Util.validateAndGetUser(Session);
                response = new SpaceSummaryList();
                List<SpaceSummary> usageList = UserAdministration.getSpaceTypesUsage(user, spaceType);
                if (usageList != null && usageList.Count > 0)
                {
                    response.SummaryList = usageList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response.setException(ex);
            }

            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse DeleteTemplate(string templateID)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = TemplateService.deleteTemplate(Session, templateID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PublishLogResult GetPublishLogDetails(int loadNumber, string loadGroup, string loadDate, string processingGroup)
        {
            PublishLogResult response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = PublishLogDetailsService.getPublishLogDetails(Session, loadNumber, loadGroup, loadDate, processingGroup);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new PublishLogResult();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetFileDetails(string fileName, string loadDate, bool warnings, int loadNumber)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = PublishLogDetailsService.getFileDetails(Session, fileName, loadDate, warnings, loadNumber);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;

            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetUserAndSpaceDetails()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = new GenericResponse();
                User u = Util.getSessionUser(Session);
                Space sp = Util.getSessionSpace(Session);
                List<String> details = new List<string>();
                if (u != null)
                {
                    details.Add("userID=" + u.ID.ToString());
                    details.Add("userName=" + u.Username);
                }

                if (sp != null)
                {
                    details.Add("spaceID=" + sp.ID.ToString());
                    details.Add("spaceName=" + sp.Name);
                    //details.Add("physicalQueryable=" + sp.PhysicalQueryable);
                }

                response.other = details.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetQueryDetails(string queryid, string dimension, string level, string source, string grain)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                if (dimension != null && dimension.Length == 0)
                {
                    dimension = null;
                }
                if (level != null && level.Length == 0)
                {
                    level = null;
                }
                if (grain != null && grain.Length == 0)
                {
                    grain = null;
                }


                response = PublishLogDetailsService.getQueryDetails(Session, queryid, dimension, level, source, grain);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public DynamicGroupsResult GetDynamicGroups(string selectedGroup)
        {
            DynamicGroupsResult response = null;
            try
            {
                if (selectedGroup != null && selectedGroup.Length == 0)
                {
                    selectedGroup = null;
                }

                Util.validateAdminUser(Session);
                response = GroupManagementService.getDynamicGroups(Session, selectedGroup);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new DynamicGroupsResult();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse GetMAFGroups()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.getMAFGroups(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GroupDetails GetGroupDetails(string groupName)
        {
            GroupDetails response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.getGroupDetails(Session, groupName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GroupDetails();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse AddNewGroup()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.addNewGroup(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse DeleteGroup(string name)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.deleteGroup(Session, name);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse SaveGroup(string oldName, string newName, string[] userNames, bool[] isMember, bool adhocAccess,
            bool dashboardAccess, bool enableDownloadAccess, bool enableSelfScheduleAccess, bool enableModifySavedExpressions,
            bool enableEditReportCatalog, bool enableVisualizerAccess)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.saveGroup(Session, oldName, newName, userNames, isMember, adhocAccess,
                    dashboardAccess, enableDownloadAccess, enableSelfScheduleAccess, enableModifySavedExpressions, 
                    enableEditReportCatalog, enableVisualizerAccess);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse AssignDynamicGroups(string groupHierarchy, string userColumn, string groupColumn)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.assignDynamicGroups(Session, groupHierarchy, userColumn, groupColumn);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse CompleteGroupManagement()
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = GroupManagementService.completeGroupManagement(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public ShareSpaceResult GetSpaceShareDetails()
        {
            ShareSpaceResult response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ShareSpaceService.getSpaceShareDetails(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new ShareSpaceResult();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse SendInvite(string emailId, string emailText)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ShareSpaceService.sendInvite(Session, HttpContext.Current.Request, HttpContext.Current.Server, emailId, emailText);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse DeleteInvite(string emailId)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ShareSpaceService.deleteInvite(Session, emailId);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse UpdateAdminAccess(string[][] accessMembers)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ShareSpaceService.updateAdminAccess(Session, accessMembers);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse RemoveAccess(string emailId) // XXX not really emailId (it's username, but need to regen WS to change the name)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = ShareSpaceService.removeAccess(Session, emailId);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse createSMIWebLogin(bool fillSessionVars)
        {
            try
            {
                Util.validateAdminUser(Session);
                GenericResponse response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                User u = (User)Session["User"];
                Space sp = Util.getSessionSpace(Session);
                if (u == null || sp == null)
                {
                    error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                    error.ErrorMessage = "Unable to continue. Please login again";
                    return response;
                }

                string token = Util.getSMILoginToken(u.Username, sp);
                try
                {
                    MainAdminForm maf = Util.getSessionMAF(Session);
                    Util.getSMILoginSession(Session, sp, token, HttpContext.Current.Response, HttpContext.Current.Request, fillSessionVars, maf);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to create session for SMIWeb");
                    Global.systemLog.Error(ex);
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Exception thrown in createSMIWebLogin " + e);
                throw e;
            }
        }
        [WebMethod(EnableSession = true)]
        public GenericResponse swapContentWithSpace(string id)
        {
            GenericResponse response = null;
            try
            {
                Util.validateAdminUser(Session);
                response = SpaceAdminService.swapWithSpace(Session, id);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getTimeZones()
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                response.Error = new ErrorOutput();
                List<string> timeZoneList = TimeZoneUtils.getTimeZoneDisplayNames();
                if (timeZoneList != null && timeZoneList.Count > 0)
                {
                    response.other = timeZoneList.ToArray();
                }
                else
                {
                    Global.systemLog.Error("Not able to return time zones");
                    response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Not able to return time zones", ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getTimeZoneDisplayAndValues()
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                response.Error = new ErrorOutput();
                response.other = TimeZoneUtils.getTimeZoneValueLabelPairs();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Not able to return time zones", ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public string executeCommand(string command)
        {
            try
            {
                Util.validateAdminUser(Session);
                User u = (User)Session["User"];
                Space sp = Util.getSessionSpace(Session);
                if (u == null || sp == null)
                {
                    return "Unable to continue. Please login again";
                }
                List<string> log = new List<string>();
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                Command co = new Command(sp, u, log, Context.Request, Context.Server, Session, maf);
                co.processCommand(command);
                if (co.isRefreshSpaceRequired())
                {
                    // refresh the logged in space, so that right information is
                    // seen on the client side
                    DefaultUserPage.getSpaceDetails(Session, HttpContext.Current.Request, sp.ID.ToString(), sp.Name, true);
                }
                StringBuilder sb = new StringBuilder();
                foreach (string s in log)
                    sb.Append(s + "\r");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to execute command", ex);
                return "General error, unable to execute command";
            }
        }

        [WebMethod(EnableSession = true)]
        public AdminRepository updateLogicalModel()
        {
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            bool isInUberProcess = false;
            if (maf != null)
            {
                isInUberProcess = maf.isInUberTransaction();
                maf.setInUberTransaction(true);
            }
            try
            {
                Util.validateAdminUser(Session);
                ManageSources.updateLogicalModel(Session);
                return ManageSources.GetSources(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting sources " + ex);
                return null;
            }
            finally
            {
                if (maf != null)
                    maf.setInUberTransaction(isInUberProcess);
            }
        }

        // get Account details - no. of active users, total users, max users, expiration date etc 
        [WebMethod(EnableSession = true)]
        public AccountSummary getAccountSummary()
        {
            // use the "other" member to populate the results
            AccountSummary response = new AccountSummary();
            try
            {
                response = UserAdministration.getAccountSummary(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getAccountSummary " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getNumAccountUsers()
        {
            GenericResponse response = new GenericResponse();
            try
            {
                int[] numUsers = UserAdministration.getNumberUsers(Session);
                int numActiveUsers = numUsers[0];
                int numInactiveUsers = numUsers[1];
                if (numActiveUsers >= 0 && numActiveUsers >= 0)
                {
                    int totalNumUsers = numActiveUsers + numInactiveUsers;
                    response.other = new string[] { numActiveUsers.ToString(), totalNumUsers.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getNumAccountUsers " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public UsersDetails getUsersToManage(string searchString, string spaceID)
        {
            UsersDetails response = new UsersDetails();
            try
            {
                if (searchString != null && searchString.Length == 0)
                {
                    searchString = null;
                }

                if (spaceID != null && spaceID.Length == 0)
                {
                    spaceID = null;
                }

                List<UserSummary> userSummaryList = UserAdministration.getUsersToManage(Session, searchString, spaceID);
                if (userSummaryList != null && userSummaryList.Count > 0)
                {
                    response.Users = userSummaryList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getUsersToManage " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public SpacesToManage getSpacesToManage()
        {
            SpacesToManage response = new SpacesToManage();
            try
            {
                List<SpaceSummary> spaceSummaryList = UserAdministration.getSpacesToManage(Session);
                if (spaceSummaryList != null && spaceSummaryList.Count > 0)
                {
                    response.Spaces = spaceSummaryList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getSpacesToManage " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse addUserToSpace(string[] toAddUsersIds, string spaceID, bool admin)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.addUserToSpace(Session, toAddUsersIds, spaceID, admin);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in addUserToSpace " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse setHtmlUser(string[] userIds, bool toHtml)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.setHtmlUser(Session, userIds, toHtml);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in setHtmlUser " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse setDenyAddSpace(string[] userIds, bool denyAddSpace)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.setDenyCreateSpace(Session, userIds, denyAddSpace);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in setHtmlUser " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse setExternalDBFlag(string[] userIds, bool enable)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.setExternalDBOption(Session, userIds, enable);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in setExternalDBFlag " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updatePassword(string[] usersIds, string newPassword)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.updatePassword(Session, usersIds, HttpContext.Current.Server, HttpContext.Current.Request);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in updatePassword " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updateUserStatus(string[] usersIds, bool enable)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.updateUsersStatus(Session, usersIds, enable);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in updateUserStatus " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse unlockUsers(string[] usersIds)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.unlockUsers(Session, usersIds);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in unlockUsers " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updateAccountAdminPriveleges(string[] usersIds, bool isAdmin)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.updateAccountAdminPriveleges(Session, usersIds, isAdmin);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in updateAccountAdminPriveleges " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updateReleasesForUsers(string[] usersIds, int releaseType)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                UserAdministration.updateReleasesForUsers(Session, usersIds, releaseType);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in updateReleasesForUsers " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse setVisualizerOptOut(bool optOut)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.getSessionUser(Session);
                UserPreference userPreference = new UserPreference(UserPreference.Type.User, ManagePreferences.PREFERNCE_OPTOUT_VISUALIZER_BANNER, optOut ? "true" : "false");
                ManagePreferences.updateUserPreferences(null, user.ID, Guid.Empty, new List<UserPreference>() { userPreference });
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse processCreateUserFile(string clientFileName)
        {
            GenericResponse response = new GenericResponse();
            try
            {
                List<string> log = UserAdministration.processCreateUserFile(Session, HttpContext.Current.Server, HttpContext.Current.Request, clientFileName);
                if (log != null && log.Count > 0)
                {
                    response.other = log.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in processCreateUserFile " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse setVisualizerHelpPreference(bool value)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.getSessionUser(Session);
                UserPreference userPreference = new UserPreference(UserPreference.Type.User, ManagePreferences.PREFERNCE_VISUALIZER_HELP, value ? "true" : "false");
                ManagePreferences.updateUserPreferences(null, user.ID, Guid.Empty, new List<UserPreference>() { userPreference });
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = getGeneralErrorResponse();
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getVisualizerHelpPreference()
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                User user = Util.getSessionUser(Session);
                bool value = ManagePreferences.isUserPreferenceEnabled(user, ManagePreferences.PREFERNCE_VISUALIZER_HELP, true);
                response.other = new string[] { value.ToString().ToLower() };
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
                response = getGeneralErrorResponse();
            }

            return response;
        }


        private GenericResponse getGeneralErrorResponse()
        {
            GenericResponse response = new GenericResponse();
            ErrorOutput error = new ErrorOutput();
            response.Error = error;
            error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getCreateUserAllowedDomains()
        {
            GenericResponse response = new GenericResponse();

            try
            {
                List<string> masks = UserAdministration.getCreateUserAllowedDomains(Session);
                if (masks != null && masks.Count > 0)
                {
                    response.other = masks.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getCreateUserAllowedDomains " + ex);
                response.setException(ex);
            }

            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse createAccount(string accountName)
        {
            GenericResponse response = new GenericResponse();
            QueryConnection conn = null;
            try
            {
                Util.validateAdminUser(Session);
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                Database.createAccount(conn, schema, accountName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in createAccount " + ex);
                response.setException(ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public UsersDetails getLoggedInUserDetails()
        {
            UsersDetails response = new UsersDetails();
            try
            {
                User u = Util.getSessionUser(Session);
                if (u != null)
                {
                    // Note not all fields are not populated

                    UserSummary uSummary = new UserSummary();
                    uSummary.FirstName = u.FirstName;
                    uSummary.LastName = u.LastName;
                    uSummary.UserID = u.ID.ToString();
                    uSummary.OperationFlag = u.OperationsFlag;
                    uSummary.RepositoryAdmin = u.RepositoryAdmin;
                    uSummary.FreeTrial = u.isFreeTrialUser;
                    uSummary.DiscoveryFreeTrial = u.isDiscoveryFreeTrial;
                    if (Session["proxyIsOperation"] != null)
                        uSummary.ProxyIsOperation = (bool)Session["proxyIsOperation"];
                    response.Users = new UserSummary[] { uSummary };

                    // "other" placeholder can be used to define test components 
                    // in limited scope. Eg. scheduler component can be tested 
                    // in 1 version prior to the release to achieve a stable component
                    bool useQuartzScheduler = Util.isQuartzSchedulerEnabled();
                    String[] properties = new String[] { "UseQuartzScheduler=" + useQuartzScheduler.ToString().ToLower() };
                    response.other = properties;
                }

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getLoggedInUserDetails " + ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public Connection[] getConnections()
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                Space sp = (Space)Session["space"];
                return Util.getConnections(sp, maf);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getConnections " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse createSchedulerLogin(bool enableSMIWebSession)
        {
            try
            {
                Util.validateAdminOrAccountUser(Session, null, null);
                GenericResponse response = new GenericResponse();
                ErrorOutput error = new ErrorOutput();
                response.Error = error;
                User u = Util.getSessionUser(Session);
                Space sp = Util.getSessionSpace(Session);
                if (u == null || (!u.OperationsFlag && sp == null))
                {
                    // if user is in operations, we should generically login for all the spaces
                    // otherwise for a particular user, we should always login
                    // in the context of user and space

                    error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                    error.ErrorMessage = "Unable to continue. Please login again";
                    return response;
                }

                try
                {
                    SchedulerUtils.getSchedulerLogin(Session, HttpContext.Current.Response, HttpContext.Current.Request);
                    if (enableSMIWebSession && sp != null)
                    {
                        string token = Util.getSMILoginToken(u.Username, sp);
                        MainAdminForm maf = Util.getSessionMAF(Session);
                        Util.getSMILoginSession(Session, sp, token, HttpContext.Current.Response, HttpContext.Current.Request, false, maf);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to create session for SMIWeb");
                    Global.systemLog.Error(ex);
                    error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                }

                return response;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in createSchedulerLogin " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getAppConfigParameters()
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                response.Error = new ErrorOutput();
                response.other = new string[] { Util.FEATURE_FLAG_VIZ_GA + "=" + (Util.isFeatureFlagEnabled(Util.FEATURE_FLAG_VIZ_GA,true) ? "true" : "false")};
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting app config parameters", ex);
                response = new GenericResponse();
                response.Error = new ErrorOutput();
                response.Error.ErrorType = BirstException.ERROR_GENERAL;
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public ReleaseDetails getReleaseInfoDetails()
        {
            ReleaseDetails response = new ReleaseDetails();
            QueryConnection conn = null;
            try
            {
                Util.validateAdminOrAccountUser(Session, null, null);
                conn = ConnectionPool.getConnection();
                List<ReleaseInfo> releaseInfoList = Database.getReleasesInfo(conn, Util.getMainSchema());
                if (releaseInfoList == null)
                {
                    releaseInfoList = new List<ReleaseInfo>();
                }

                // Add a dummy entry to denote the default entry "Null" release type
                ReleaseInfo defaultReleaseInfo = new ReleaseInfo();
                defaultReleaseInfo.ID = -1;
                defaultReleaseInfo.Name = "Default";
                releaseInfoList.Add(defaultReleaseInfo);

                response.ReleasesInfo = releaseInfoList.ToArray();

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getReleaseInfoDetails " + ex);
                response.setException(ex);
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                catch (Exception)
                {
                }
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PerformaceEngineDetails getPerformanceEngineVersions()
        {
            PerformaceEngineDetails response = new PerformaceEngineDetails();
            try
            {
                Util.validateAdminOrAccountUser(Session, null, null);
                List<PerformanceEngineVersion> performanceEngineVersionsList = Util.getPerformanceEngineDetails(Session);
                if (performanceEngineVersionsList != null && performanceEngineVersionsList.Count > 0)
                {
                    response.PerformanceEngineVersions = performanceEngineVersionsList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getPerformanceEngineVersions " + ex);
                response.setException(ex);
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getUploadToken(string type, string parameters)
        {
            GenericResponse response = null;
            string spaceId = null;
            string spaceName = null;
            string userName = null;
            QueryConnection conn = null;
            try
            {
                response = new GenericResponse();
                User u = Util.validateAndGetUser(Session);
                userName = u.Username;
                Space sp = null;
                if (type == "command")
                {
                    // Check if the user is authorized to perform user management operations
                    if (!UserManagementUtils.isUserAuthorized(u))
                    {
                        Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                        throw new UserManagementNotAuthorizedException();
                    }
                }
                else
                {
                    sp = Util.validateAndGetSpace(Session);
                    spaceId = sp.ID.ToString();
                    spaceName = sp.Name;
                    Util.validateAdminUser(Session);
                }

                conn = ConnectionPool.getConnection();
                string toEncrypt = TokenGenerator.getUploadSecurityToken(conn, mainSchema, u, sp, HttpContext.Current.Request.UserHostAddress, type, parameters);
                string token = Util.encryptToken(toEncrypt);
                Global.systemLog.Debug("UploadToken generated and returned " + token + " (" + toEncrypt + ")");
                response.other = new string[] { token };
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in fetching upload token space ID : " + spaceId + " : spaceName : " + spaceName + " : userName : " + userName + "  ", ex);
                response.setException(ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return response;
        }

        private bool containsKey(StagingTable source, string[] key)
        {
            int count = 0;
            for (int i = 0; i < key.Length; i++)
            {
                bool found = false;
                foreach (StagingColumn sc in source.Columns)
                {
                    if (key[i] == sc.SourceFileColumn)
                    {
                        found = true;
                        break;
                    }
                    if (key[i] == sc.Name)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    count++;
            }
            if (count < key.Length)
            {
                return false;
            }
            return true;
        }

        private bool containsKey(StagingTable source, MeasureTable mt)
        {
            int count = 0;
            for (int i = 0; i < mt.MeasureColumns.Length; i++)
            {
                bool found = false;
                foreach (string s in source.SourceKey)
                {
                    if (mt.MeasureColumns[i].RootMeasure != null)
                    {
                        if (mt.MeasureColumns[i].RootMeasure == s)
                        {
                            found = true;
                            break;
                        }
                    }
                    else
                    {
                        string cname = mt.MeasureColumns[i].ColumnName;
                        int index = cname.IndexOf('.');
                        if (index >= 0)
                        {
                            if (cname.Substring(index + 1) == s)
                            {
                                found = true;
                                break;
                            }
                        }
                        else
                        {
                            if (cname == s)
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                }
                if (found)
                    count++;
            }
            if (count < source.SourceKey.Length)
            {
                return false;
            }
            return true;
        }

        private bool containsKey(StagingTable source, DimensionTable dt)
        {
            int count = 0;
            for (int i = 0; i < dt.DimensionColumns.Length; i++)
            {
                bool found = false;
                foreach (string s in source.SourceKey)
                {
                    if (dt.DimensionColumns[i].ColumnName == s)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    count++;
            }
            if (count < source.SourceKey.Length)
            {
                return false;
            }
            return true;
        }

        private bool containsKey(DimensionTable dt, string[] key)
        {
            int count = 0;
            for (int i = 0; i < key.Length; i++)
            {
                bool found = false;
                foreach (DimensionColumn dc in dt.DimensionColumns)
                {
                    if (key[i] == dc.ColumnName)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    count++;
            }
            if (count < key.Length)
            {
                return false;
            }
            return true;
        }

        [WebMethod(EnableSession = true)]
        public string[] getJoinableSources(string sourceName)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                List<string> result = new List<string>();
                StagingTable primarySt = maf.stagingTableMod.findTable(sourceName);
                if (primarySt == null)
                    return null;
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    if (primarySt.SourceKey != null && containsKey(st, primarySt.SourceKey))
                        // Child data source
                        result.Add("cads:" + st.Name);
                    else if (st.SourceKey != null && containsKey(primarySt, st.SourceKey))
                        // Parent data source
                        result.Add("pads:" + st.Name);
                }
                // Imported staging tables
                if (maf.Imports != null)
                {
                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.hide)
                            continue;
                        if (it.st != null)
                        {
                            if (primarySt.SourceKey != null && containsKey(it.st, primarySt.SourceKey))
                                // Child data source
                                result.Add("cads:" + it.st.Name);
                            else if (it.st.SourceKey != null && containsKey(primarySt, it.st.SourceKey))
                                // Parent data source
                                result.Add("pads:" + it.st.Name);
                        }
                        else if (it.mt != null)
                        {
                            if (primarySt.SourceKey != null && containsKey(primarySt, it.mt))
                                result.Add("cmt:" + getGrainString(it.ispace.maf.getGrainInfo(it.mt)));
                        }
                        else if (it.dt != null)
                        {
                            if (primarySt.SourceKey != null && containsKey(primarySt, it.dt))
                                result.Add("cdt:" + it.dt.DimensionName + "." + it.dt.Level);
                            else
                            {
                                Hierarchy h = it.ispace.maf.hmodule.getDimensionHierarchy(it.dt.DimensionName);
                                if (h == null)
                                    continue;
                                Level l = h.findLevel(it.dt.Level);
                                if (l == null)
                                    continue;
                                LevelKey lk = l.getNaturalKey();
                                if (lk == null)
                                    continue;
                                if (containsKey(primarySt, lk.ColumnNames))
                                    result.Add("pdt:" + it.dt.DimensionName + "." + it.dt.Level);
                            }
                        }
                    }
                }
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    // Snowflake relationships
                    if (primarySt.SourceKey != null && containsKey(dt, primarySt.SourceKey))
                        result.Add("dims:" + dt.DimensionName + "." + dt.Level);
                    // Dimension relationships
                    Hierarchy h = maf.hmodule.getDimensionHierarchy(dt.DimensionName);
                    if (h == null)
                        continue;
                    Level l = h.findLevel(dt.Level);
                    if (l == null)
                        continue;
                    LevelKey lk = l.getNaturalKey();
                    if (lk != null && containsKey(primarySt, lk.ColumnNames))
                        result.Add("dimd:" + dt.DimensionName + "." + dt.Level);
                }
                return result.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getJoinableSources " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public void addMeasureTableForeignKey(string grainString, string source, int type)
        {
            try
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                if (maf.Imports != null && maf.Imports.Length > 0)
                {
                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.mt == null)
                            continue;
                        string grainstr = getGrainString(it.ispace.maf.getGrainInfo(it.mt));
                        if (grainstr != grainString)
                            continue;
                        ImportedItem iitem = it.ispace.pi.getImportedItem(it.mt.TableName, true);
                        ForeignKey fk = new ForeignKey();
                        fk.Source = source;
                        fk.Type = type;
                        if (iitem.addForeignKey(fk))
                        {
                            it.ispace.pi.updatePackageImport(maf);
                            Session["AdminDirty"] = true;
                            // this only sets the flag in the repository and do not rebuild it now
                            Session["RequiresRebuild"] = true;
                            Util.saveDirty(Session);
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in addMeasureTableForeignKey " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public void removeMeasureTableForeignKey(string grainString, string source)
        {
            try
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                if (maf.Imports != null && maf.Imports.Length > 0)
                {
                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.mt == null)
                            continue;
                        string grainstr = getGrainString(it.ispace.maf.getGrainInfo(it.mt));
                        if (grainstr != grainString)
                            continue;
                        ImportedItem iitem = it.ispace.pi.getImportedItem(it.mt.TableName, false);
                        if (iitem == null)
                            return;
                        if (iitem.removeForeignKey(source))
                        {
                            Session["AdminDirty"] = true;
                            // this only sets the flag in the repository and do not rebuild it now
                            Session["RequiresRebuild"] = true;
                            Util.saveDirty(Session);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in removeMeasureTableForeignKey " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public void addDimensionTableForeignKey(string dimension, string level, string source, int type)
        {
            try
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                if (maf.Imports != null && maf.Imports.Length > 0)
                {
                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.dt == null)
                            continue;
                        if (it.dt.DimensionName == dimension && it.dt.Level == level)
                        {
                            ImportedItem iitem = it.ispace.pi.getImportedItem(it.dt.TableName, true);
                            ForeignKey fk = new ForeignKey();
                            fk.Source = source;
                            fk.Type = type;
                            if (iitem.addForeignKey(fk))
                            {
                                it.ispace.pi.updatePackageImport(maf);
                                Session["AdminDirty"] = true;
                                // this only sets the flag in the repository and do not rebuild it now
                                Session["RequiresRebuild"] = true;
                                Util.saveDirty(Session);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in addDimensionTableForeignKey " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public void removeDimensionTableForeignKey(string dimension, string level, string source)
        {
            try
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                if (maf.Imports != null && maf.Imports.Length > 0)
                {
                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.dt == null)
                            continue;
                        if (it.dt.DimensionName == dimension && it.dt.Level == level)
                        {
                            ImportedItem iitem = it.ispace.pi.getImportedItem(it.dt.TableName, false);
                            if (iitem == null)
                                return;
                            if (iitem.removeForeignKey(source))
                            {
                                Session["AdminDirty"] = true;
                                // this only sets the flag in the repository and do not rebuild it now
                                Session["RequiresRebuild"] = true;
                                Util.saveDirty(Session);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in removeDimensionTableForeignKey " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string getColumns()
        {
            try
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                StringBuilder columns = new StringBuilder();
                Dictionary<string, string> cmap = new Dictionary<string, string>();
                foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                {
                    foreach (StagingColumn ssc in st.Columns)
                    {
                        if (!cmap.ContainsKey(ssc.Name))
                        {
                            cmap.Add(ssc.Name, null);
                            columns.Append("|" + ssc.Name);
                        }
                    }
                }
                return columns.ToString();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getColumns " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string getDataFlow(string loadgroup, string subgroup)
        {
            try
            {
                Space sp = (Space)Session["space"];
                User u = (User)Session["User"];
                if (u == null || sp == null)
                    return null;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                List<string> sources = null;
                List<string> targets = null;
                List<string> targetTableNames = null;
                List<string> types = null;
                TrustedService.TrustedService ts = null;
                ts = new TrustedService.TrustedService();
                // 5 Minute timeout
                ts.Timeout = 5 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
                {
                    return null;
                }
                string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);

                FlowResult result = null;
                try
                {
                    result = ts.getDataFlow(sp.Directory, sp.ID.ToString(), u.Username, null, importedSpacesInfoArray, null);
                }
                catch (Exception)
                {
                    Global.systemLog.Warn("Could not get data flow information, SMIWeb tomcat is probably down");
                }
                if (result != null && result.sources != null && result.targets != null)
                {
                    sources = new List<string>(result.sources);
                    targets = new List<string>(result.targets);
                    targetTableNames = new List<string>();
                    foreach (string s in targets)
                        targetTableNames.Add(null);
                    types = new List<string>();
                    foreach (string s in sources)
                        types.Add("datasources");
                    ApplicationBuilder ab = new ApplicationBuilder(maf);
                    Dictionary<string, string> lcache = new Dictionary<string, string>();
                    // Make sure all grains have appropriate connection set
                    foreach (MeasureTable mt in maf.measureTablesList)
                    {
                        if (mt.Grain != null && mt.TableSource != null)
                            mt.Grain.connection = mt.TableSource.Connection;
                    }
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        if (st.Disabled)
                            continue;
                        MeasureGrain mg = ab.getStagingTableGrain(maf, st);
                        foreach (StagingColumn scol in st.Columns)
                        {
                            if (scol.TargetTypes != null && scol.TargetTypes.Length > 0)
                            {
                                foreach (string s in scol.TargetTypes)
                                {
                                    if (s == "Time")
                                        continue;
                                    if (s == "Measure")
                                    {
                                        MeasureTable mt = ApplicationBuilder.findMeasureTableAtGrain(maf, mg);
                                        if (mt != null)
                                        {
                                            string grainstr = getGrainString(st.Levels);
                                            if (!lcache.ContainsKey("M" + st.Name + "|" + grainstr))
                                            {
                                                sources.Add(st.Name);
                                                targets.Add(grainstr);
                                                targetTableNames.Add(mt.TableName);
                                                types.Add("measure");
                                                lcache["M" + st.Name + "|" + grainstr] = "";
                                            }
                                        }
                                        continue;
                                    }
                                    foreach (DimensionTable dt in maf.dimensionTablesList)
                                    {
                                        if (dt.DimensionName != s)
                                            continue;
                                        Hierarchy h = maf.hmodule.getDimensionHierarchy(s);
                                        if (h == null)
                                            continue;
                                        Level l = h.findLevelWithColumn(scol.Name);
                                        if (l != null && l.Name != dt.Level)
                                            continue;
                                        if (l == null)
                                        {
                                            bool found = false;
                                            foreach (string[] stlevel in st.Levels)
                                            {
                                                if (stlevel[0] == dt.DimensionName && stlevel[1] == dt.Level)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (!found)
                                                continue;
                                        }
                                        DataRowView[] drv = maf.dimensionColumnMappingsView.FindRows(new object[] { scol.Name, dt.TableName });
                                        if (drv.Length > 0)
                                        {
                                            if (!lcache.ContainsKey(st.Name + "|" + dt.DimensionName + "." + dt.Level))
                                            {
                                                sources.Add(st.Name);
                                                targets.Add(dt.DimensionName + "." + dt.Level);
                                                targetTableNames.Add(dt.TableName);
                                                types.Add("level");
                                                lcache[st.Name + "|" + dt.DimensionName + "." + dt.Level] = "";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // Add fact-dimension joins
                    foreach (MeasureTable mt in maf.measureTablesList)
                    {
                        string grainstr = getMeasureGrainString(mt.Grain);
                        foreach (DimensionTable dt in maf.dimensionTablesList)
                        {
                            if (!lcache.ContainsKey(grainstr + "." + dt.DimensionName + "." + dt.Level))
                            {
                                if (maf.joinExists(mt, dt))
                                {
                                    sources.Add(grainstr);
                                    targets.Add(dt.DimensionName + "." + dt.Level);
                                    targetTableNames.Add(null);
                                    types.Add("join");
                                    lcache[grainstr + "." + dt.DimensionName + "." + dt.Level] = "";
                                }
                            }
                        }
                    }
                }
                if (maf.Imports != null && maf.Imports.Length > 0)
                {
                    if (sources == null)
                    {
                        sources = new List<string>();
                        targets = new List<string>();
                        targetTableNames = new List<string>();
                        types = new List<string>();
                    }
                    /*
                     * Process imported measure and dimension tables
                     */
                    Dictionary<string, string> lcache = new Dictionary<string, string>();

                    List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, Session, null);
                    foreach (ImportedRepositoryItem it in itlist)
                    {
                        if (it.hide)
                            continue;
                        if (it.mt != null)
                        {
                            List<MeasureTableGrain> grainList = it.ispace.maf.getGrainInfo(it.mt);
                            if (grainList == null || grainList.Count == 0)
                                continue;
                            string grainstr = getGrainString(grainList);
                            if (!lcache.ContainsKey("M" + it.mt.TableName + "|" + grainstr))
                            {
                                sources.Add(it.displayName);
                                targets.Add(grainstr);
                                targetTableNames.Add(it.mt.TableName);
                                types.Add("measure");
                                lcache["M" + it.mt.TableName + "|" + grainstr] = "";
                            }
                            // Add joins
                            foreach (ImportedRepositoryItem jit in itlist)
                            {
                                if (jit.dt == null)
                                    continue;
                                if (lcache.ContainsKey(grainstr + "." + jit.dt.DimensionName + "." + jit.dt.Level))
                                    continue;
                                if (jit.ispace.maf.joinExists(it.mt, jit.dt))
                                {
                                    sources.Add(grainstr);
                                    targets.Add(jit.dt.DimensionName + "." + jit.dt.Level);
                                    targetTableNames.Add(null);
                                    types.Add("join");
                                    lcache[grainstr + "." + jit.dt.DimensionName + "." + jit.dt.Level] = "";
                                }
                            }
                        }
                        else if (it.dt != null)
                        {
                            if (!lcache.ContainsKey("|" + it.dt.DimensionName + "." + it.dt.Level))
                            {
                                sources.Add(it.displayName);
                                targets.Add(it.dt.DimensionName + "." + it.dt.Level);
                                targetTableNames.Add(it.dt.TableName);
                                types.Add("level");
                                lcache["|" + it.dt.DimensionName + "." + it.dt.Level] = "";
                            }
                        }
                        else if (it.st != null)
                        {
                            bool found = false;
                            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                            {
                                if (st.Name == it.st.Name)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                sources.Add(null);
                                targets.Add(it.st.Name);
                                targetTableNames.Add(null);
                                types.Add("datasources");
                            }
                        }
                    }
                }
                if (sources == null || sources.Count == 0)
                    return null;
                StringBuilder flow = new StringBuilder();
                for (int i = 0; i < sources.Count; i++)
                {
                    flow.Append(sources[i] + "\x8" + targets[i] + "\x8" + targetTableNames[i] + "\x8" + types[i] + "\r");
                }
                Object lastLoadDate = DefaultUserPage.getLastLoadDate(sp);

                return (lastLoadDate != null ? "lastload:" + ((DateTime)lastLoadDate).ToString("d") : "") + "\r" + flow.ToString();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getDataFlow " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string getDataFlowHistory()
        {
            try
            {
                Util.validateAdminUser(Session);
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return null;
                DataTable hist = Status.getPublishHistory(Session, sp, -1, null, true, false, -1, true, true, false, true);
                StringBuilder history = new StringBuilder();
                for (int i = 0; i < hist.Rows.Count; i++)
                {
                    history.Append(((DateTime)hist.Rows[i][0]).ToString("yyyy/mm/dd HH:mm:ss") + "\t");
                    history.Append(((int)hist.Rows[i][1]).ToString() + "\t");
                    history.Append(((string)hist.Rows[i][2]) + "\t");
                    if (hist.Rows[i][3] != null && hist.Rows[i][3] != System.DBNull.Value)
                        history.Append((string)hist.Rows[i][3]);
                    history.Append("\t");
                    if (hist.Rows[i][8] != null && hist.Rows[i][8] != System.DBNull.Value)
                        history.Append(((long)hist.Rows[i][8]).ToString());
                    history.Append("\t");
                    if (hist.Rows[i][9] != null && hist.Rows[i][9] != System.DBNull.Value)
                        history.Append(((long)hist.Rows[i][9]).ToString());
                    history.Append("\t");
                    if (hist.Rows[i][10] != null && hist.Rows[i][10] != System.DBNull.Value)
                        history.Append(((long)hist.Rows[i][10]).ToString());
                    history.Append("\t");
                    if (hist.Rows[i][11] != null && hist.Rows[i][11] != System.DBNull.Value)
                        history.Append(((long)hist.Rows[i][11]).ToString());
                    history.Append("\r");
                }
                return history.ToString();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getDataFlowHistory " + ex);
                throw ex;
            }
        }

        private string getGrainString(string[][] levels)
        {
            string grainstr = "";
            foreach (string[] aos in levels)
            {
                if (aos[0] == "Time")
                    continue;
                if (grainstr.Length > 0)
                    grainstr += "|";
                grainstr += aos[0] + "." + aos[1];
            }
            return grainstr;
        }

        private string getGrainString(List<MeasureTableGrain> grainlist)
        {
            string grainstr = "";
            foreach (MeasureTableGrain mtg in grainlist)
            {
                if (mtg.DimensionName == "Time")
                    continue;
                if (grainstr.Length > 0)
                    grainstr += "|";
                grainstr += mtg.DimensionName + "." + mtg.DimensionLevel;
            }
            return grainstr;
        }

        private string getMeasureGrainString(MeasureGrain mg)
        {
            string grainstr = "";
            if (mg == null || mg.measureTableGrains == null)
                return grainstr;
            foreach (MeasureTableGrain mtg in mg.measureTableGrains)
            {
                if (mtg.DimensionName == "Time")
                    continue;
                if (grainstr.Length > 0)
                    grainstr += "|";
                grainstr += mtg.DimensionName + "." + mtg.DimensionLevel;
            }
            return grainstr;
        }

        public class TablePropertiesResult
        {
            public int numRows;
            public string physicalName;
            public string logicalName;
            public string ErrorMessage;
        }

        [WebMethod(EnableSession = true)]
        public TablePropertiesResult getTableProperties(string type, string name)
        {
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                Space sp = (Space)Session["space"];
                QueryConnection conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                TablePropertiesResult tpr = new TablePropertiesResult();
                DatabaseConnection dc = null;
                try
                {
                    if (type == "ADS")
                    {
                        StagingTable st = maf.stagingTableMod.findTable(name);
                        tpr.physicalName = st.Name;
                        Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                        foreach (string lg in st.LoadGroups)
                        {
                            if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                            {
                                dc = maf.connection.findConnection(leConfigs[lg].getLocalETLConnection());
                                tpr.logicalName = st.Name;
                                break;
                            }
                        }
                    }
                    else if (type == "DIM")
                    {
                        foreach (DimensionTable dt in maf.dimensionTablesList)
                        {
                            if (dt.DimensionName + "." + dt.Level == name)
                            {
                                tpr.physicalName = dt.TableSource.Tables[0].PhysicalName;
                                dc = maf.connection.findConnection(dt.TableSource.Connection);
                                tpr.logicalName = dt.TableName;
                                break;
                            }
                        }
                    }
                    else if (type == "MEASURE")
                    {
                        foreach (MeasureTable mt in maf.measureTablesList)
                        {
                            if (getMeasureGrainString(mt.Grain) == name)
                            {
                                tpr.physicalName = mt.TableSource.Tables[0].PhysicalName;
                                dc = maf.connection.findConnection(mt.TableSource.Connection);
                                tpr.logicalName = mt.TableName;
                                break;
                            }
                        }
                    }
                    string query = "SELECT COUNT(*) FROM " + sp.Schema + "." + tpr.physicalName;
                    if (dc != null && dc.Realtime)
                    {
                        Object[][] res = LiveAccessUtil.executeLiveAccessQuery(Session, null, sp, dc.Name, query, true);
                        if (res.Length > 0 && res[0].Length > 0)
                        {
                            int.TryParse((string)res[0][0], out tpr.numRows);
                        }
                    }
                    else
                    {
                        QueryDataAdapter adapt = new QueryDataAdapter(query, conn);
                        DataTable dt = new DataTable(name);
                        adapt.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            tpr.numRows = (int)dt.Rows[0][0];
                        }
                    }

                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception thrown in getTableProperties " + ex);
                    tpr.ErrorMessage = "Unable to retrieve results of query";
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                return tpr;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getTableProperties " + ex);
                throw ex;
            }
        }

        [WebMethod(EnableSession = true)]
        public string KillRunningProcess()
        {
            try
            {
                Util.validateAdminUser(Session);
                User u = (User)Session["User"];
                Space sp = Util.getSessionSpace(Session);
                if (u == null || sp == null)
                {
                    return "Unable to continue. Please login again";
                }
                return ProcessHandler.killRunningProcess(sp.ID.ToString(), u.Username, true, false);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in killRunningProcess " + ex);
                throw ex;
            }
        }


        /* ==============================
         * Birst DBA related webservices
        */


        [WebMethod(EnableSession = true)]
        public BaseSourceObjectList getObjectsAvailableForPackages()
        {
            BaseSourceObjectList response = null;
            try
            {
                response = new BaseSourceObjectList();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                List<BaseSourceObject> objectsList = PackageUtils.getAllAvailableSourceObjectsForPackageExport(sp, maf, user);
                if (objectsList != null && objectsList.Count > 0)
                {
                    List<BaseSourceObject> stagingTables = new List<BaseSourceObject>();
                    List<BaseSourceObject> dimTables = new List<BaseSourceObject>();
                    List<BaseSourceObject> measureTables = new List<BaseSourceObject>();
                    List<BaseSourceObject> variables = new List<BaseSourceObject>();
                    List<BaseSourceObject> aggregates = new List<BaseSourceObject>();
                    List<BaseSourceObject> CSAs = new List<BaseSourceObject>();
                    foreach (BaseSourceObject bsObject in objectsList)
                    {
                        switch (bsObject.Type)
                        {
                            case PackageUtils.PACKAGE_OBJECT_TYPE_STAGING_TABLE:
                                stagingTables.Add(bsObject);
                                break;
                            case PackageUtils.PACKAGE_OBJECT_TYPE_DIMENSION_TABLE:
                                dimTables.Add(bsObject);
                                break;
                            case PackageUtils.PACKAGE_OBJECT_TYPE_MEASURE_TABLE:
                                measureTables.Add(bsObject);
                                break;
                            case PackageUtils.PACKAGE_OBJECT_TYPE_VARIABLE:
                                variables.Add(bsObject);
                                break;
                            case PackageUtils.PACKAGE_OBJECT_TYPE_AGGREGATE:
                                aggregates.Add(bsObject);
                                break;
                            case PackageUtils.PACKAGE_OBJECT_TYPE_CUSTOM_SUBJECT_AREA:
                                CSAs.Add(bsObject);
                                break;

                            default:
                                break;
                        }
                    }

                    response.StagingTables = stagingTables.ToArray();
                    response.DimensionTables = dimTables.ToArray();
                    response.MeasureTables = measureTables.ToArray();
                    response.Variables = variables.ToArray();
                    response.Aggregates = aggregates.ToArray();
                    response.CustomSubjectAreas = CSAs.ToArray();
                }

            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getObjectsAvailableForPackages ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse retrieveSupportedScheduleTypes()
        {
            /* 
             * Schedule Types:
             * 1 - BDS
             * 2 - SFDC
             * 3 - Netsuite
             */
            GenericResponse response = new GenericResponse();
            if (ConnectorUtils.UseNewConnectorFramework())
            {
                try
                {
                    Util.validateAdminUser(Session);
                    Space sp = Util.getSessionSpace(Session);
                    User user = Util.getSessionUser(Session);
                    HashSet<string> scheduleTypes = ConnectorUtils.retrieveSupportedScheduleTypes(Session, user, sp);
                    response.other = scheduleTypes.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error(ex.ToString(), ex);
                    response.setException(ex);
                }
            }
            else
            {
                try
                {
                    bool test = false;
                    HashSet<string> scheduleTypes = new HashSet<string>();
                    Util.validateAdminUser(Session);
                    Space sp = Util.getSessionSpace(Session);
                    User user = Util.getSessionUser(Session);
                    String configDir = ConnectorUtils.getConnectorsDirectoryName(sp);

                    // BDS schedule type. For now add for every space. 
                    // TODO: add logic to determine bds source.
                    // scheduleTypes.Add("1");

                    if (Directory.Exists(configDir) || Directory.Exists(sp.Directory))
                    {
                        foreach (KeyValuePair<string, string> pair in ConnectorUtils.connectorNameToSettingsFileNames)
                        {
                            // Salesforce schedule type.
                            string salesForceFilePath = (pair.Key == ConnectorUtils.CONNECTOR_SFDC) ? Path.Combine(configDir, pair.Value) : "";
                            if (test || File.Exists(salesForceFilePath) || File.Exists(Path.Combine(sp.Directory, ConnectorUtils.CONNECTOR_OLD_SFDC)))
                            {
                                scheduleTypes.Add("2");
                            }

                            // Netsuite schedule type.
                            string netsuiteFilePath = (pair.Key == ConnectorUtils.CONNECTOR_NETSUITE) ? Path.Combine(configDir, pair.Value) : "";
                            if (test || File.Exists(netsuiteFilePath))
                            {
                                scheduleTypes.Add("3");
                            }
                        }
                    }
                    response.other = scheduleTypes.ToArray();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error : " + System.Reflection.MethodInfo.GetCurrentMethod() + " ", ex);
                    response.setException(ex);
                }
            }
            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse getAllExtractAndProcessingGroups()
        {
            GenericResponse response = new GenericResponse();
            try
            {
                Util.validateAdminUser(Session);
                Space sp = Util.getSessionSpace(Session);
                User user = Util.getSessionUser(Session);
                XElement elem = ConnectorUtils.getAllExtractAndProcessingGroups(Session, user, sp);
                if (elem != null)
                {
                    response.other = new string[] { elem.ToString() };
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex.ToString(), ex);
                response.setException(ex);
            }
            return response;
        }

        /// <summary>
        /// Get alls the packages both created and imported in a space
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public PackagesInfo getAllPackages()
        {
            PackagesInfo response = null;
            try
            {
                response = new PackagesInfo();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                List<PackageInfoLite> packagesInfoLiteList = PackageUtils.getAllPackages(Session, sp, maf);
                if (packagesInfoLiteList != null && packagesInfoLiteList.Count > 0)
                {
                    // to make hashset case contains insensitive
                    HashSet<String> packageIdsWithCollisions = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    List<PackageCollision> collisionsDetected = PackageUtils.getAllCollisions(Session, user, sp, maf);
                    if (collisionsDetected != null && collisionsDetected.Count > 0)
                    {
                        PackageCollision[] filteredCollisions = PackageUtils.filterPackageCollisions(collisionsDetected);
                        if (filteredCollisions != null && filteredCollisions.Length > 0)
                        {
                            foreach (PackageCollision collision in filteredCollisions)
                            {
                                packageIdsWithCollisions.Add(collision.PackageID);
                            }
                        }
                    }

                    if (packageIdsWithCollisions != null && packageIdsWithCollisions.Count > 0)
                    {
                        // Update the status to unavailable
                        foreach (PackageInfoLite packageInfo in packagesInfoLiteList)
                        {
                            if (packageIdsWithCollisions.Contains(packageInfo.ID))
                            {
                                packageInfo.Status = PackageUtils.PACKAGE_STATUS_COLLISION;
                            }
                        }
                    }
                }
                response.PackagesLiteList = packagesInfoLiteList != null && packagesInfoLiteList.Count > 0 ? packagesInfoLiteList.ToArray() : null;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getAllPackages ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse createPackage(PackageDetails packageDetails)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                PackageUtils.createPackageDetails(Session, user, sp, packageDetails);
                Util.touchRepository(sp, Util.REPOSITORY_PROD);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :createPackage ", ex);
                response.setException(ex);
            }

            return response;
        }


        [WebMethod(EnableSession = true)]
        public GenericResponse modifyPackage(PackageDetails packageDetails)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                PackageUtils.modifyPackage(Session, user, sp, packageDetails);
                Util.touchRepository(sp, Util.REPOSITORY_PROD);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :modifyPackage ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse deletePackage(string packageID)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                PackageUtils.deletePackage(Session, user, sp, new Guid(packageID));
                Util.touchRepository(sp, Util.REPOSITORY_PROD);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :deletePackage ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PackageGroupsList getAllGroupsAvailableForPackages()
        {
            PackageGroupsList response = null;
            try
            {
                response = new PackageGroupsList();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                UserAndGroupUtils.populateSpaceGroups(sp, false);
                if (sp.SpaceGroups != null)
                {
                    List<PackageGroup> packageGroups = new List<PackageGroup>();
                    foreach (SpaceGroup spg in sp.SpaceGroups)
                    {
                        if (spg.InternalGroup && spg.Name == "Administrators")
                            continue;
                        if (spg.Name == Util.OWNER_GROUP_NAME || spg.Name == Util.USER_GROUP_NAME)
                            continue;
                        PackageGroup pg = new PackageGroup();
                        pg.GroupAllowed = false;
                        pg.GroupID = spg.ID.ToString();
                        pg.GroupName = spg.Name;
                        packageGroups.Add(pg);
                    }
                    response.PackageGroups = packageGroups.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getAllPackageGroups ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PackageDetailsResponse getPackageDetails(string spaceID, string packageID, bool filterHiddenObjects)
        {
            PackageDetailsResponse response = null;
            try
            {
                response = new PackageDetailsResponse();
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Space packageSpace = Util.getSpaceById(new Guid(spaceID));
                PackageDetails packageDetails = PackageUtils.getPackageDetails(Session, packageSpace, new Guid(packageID), filterHiddenObjects);

                response.PackageDetails = packageDetails;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getPackageDetails ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PackageObjectDependency getPackageObjectDependency(string packageSpaceID, string packageID, BaseSourceObject sourceBaseSourceObject)
        {
            PackageObjectDependency response = null;
            try
            {
                response = new PackageObjectDependency();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                PackageDetail package = PackageUtils.getPackage(Session, Util.getSpaceById(new Guid(packageSpaceID)), new Guid(packageID));
                if (package == null)
                {
                    Global.systemLog.Error("Package not found : packageSpaceID  : " + packageSpaceID + " : " + packageID);
                    throw new PackageException(BirstException.ERROR_PACKAGE_NOT_FOUND);
                }

                response.PackageID = package.ID.ToString();
                response.PackageName = package.Name;
                response.SourceObject = sourceBaseSourceObject;
                List<BaseSourceObject> dependentObjectsList = PackageUtils.getDependentObjects(Session, new Guid(packageSpaceID), new Guid(packageID), sourceBaseSourceObject);
                if (dependentObjectsList != null && dependentObjectsList.Count > 0)
                {
                    response.DependentObjects = dependentObjectsList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getPackageObjectDependency ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public PackageObjectDependency getDependentObjects(BaseSourceObject sourceBaseSourceObject)
        {
            // Note although response datastructure has package id and package name, but 
            // this response will not contain the package name. It is used to get dependent objects
            // during package creation project where is no package created yet
            PackageObjectDependency response = null;
            try
            {
                response = new PackageObjectDependency();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                Util.validateAdminUser(Session, sp, user);
                response.SourceObject = sourceBaseSourceObject;
                List<BaseSourceObject> dependentObjectsList = PackageUtils.getDependentObjects(maf, sourceBaseSourceObject);
                if (dependentObjectsList != null && dependentObjectsList.Count > 0)
                {
                    response.DependentObjects = dependentObjectsList.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getDependentObject ", ex);
                response.setException(ex);
            }

            return response;
        }

        /// <summary>
        /// Get all the packages that a child space can import
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public PackagesInfo getAllPackagesToImport()
        {
            PackagesInfo response = null;
            try
            {
                response = new PackagesInfo();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                List<PackageInfoLite> packageInfoLiteList = PackageUtils.getAllowedPackagesForImport(Session, user, sp);
                response.PackagesLiteList = packageInfoLiteList != null && packageInfoLiteList.Count > 0 ? packageInfoLiteList.ToArray() : null;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getAllPackagesToImport ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public Collision importPackage(string toImportSpaceID, string toImportPackageID, bool checkForDBMisMatch)
        {
            Collision response = null;
            try
            {
                response = new Collision();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                List<PackageCollision> collisionsDetected = PackageUtils.importPackage(Session, user, sp, maf, new Guid(toImportSpaceID), new Guid(toImportPackageID), checkForDBMisMatch);
                if (collisionsDetected != null && collisionsDetected.Count > 0)
                {
                    response.PackageCollisions = PackageUtils.filterPackageCollisions(collisionsDetected);
                }
                else
                {
                    // import is successful
                    // set the RebuildApplication flag if required so that time dim is created for a native space
                    // which does not have any sources
                    Session["AdminDirty"] = true;
                    Session["RequiresRebuild"] = true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :importPackage ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse removeImportedPackage(string toImportSpaceID, string toImportPackageID)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                PackageUtils.removeImportedPackage(Session, user, sp, maf, new Guid(toImportSpaceID), new Guid(toImportPackageID));
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :removeImportedPackage ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public GenericResponse updatePackageUsage(string childSpaceID, string packageID, bool available)
        {
            GenericResponse response = null;
            try
            {
                response = new GenericResponse();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                PackageUtils.updatePackageUsage(user, sp, new Guid(childSpaceID), new Guid(packageID), available);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :updatePackageUsage ", ex);
                response.setException(ex);
            }

            return response;
        }


        [WebMethod(EnableSession = true)]
        public Collision getAllCollisions()
        {
            Collision response = null;
            try
            {
                response = new Collision();
                Space sp = Util.validateAndGetSpace(Session);
                User user = Util.validateAndGetUser(Session);
                Util.validateAdminUser(Session, sp, user);
                MainAdminForm maf = Util.validateAndGetMAF(Session);
                List<PackageCollision> collisionsDetected = PackageUtils.getAllCollisions(Session, user, sp, maf);
                if (collisionsDetected != null && collisionsDetected.Count > 0)
                {
                    response.PackageCollisions = PackageUtils.filterPackageCollisions(collisionsDetected);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :getAllCollisions ", ex);
                response.setException(ex);
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public bool createSMIWebSession()
        {
            try
            {
                Space sp = Util.validateSpace(Session, Context.Response, Context.Request);
                User user = Util.validateAndGetUser(Session);

                if (!Util.validateSpace(sp, user, Session))
                {
                    Global.systemLog.Debug("redirecting to home page due to space validation failure");
                    return false;
                }
                string token = Util.getSMILoginToken(user.Username, sp);
                if (Util.getSMILoginSession(Session, sp, token, this.Context.Response, this.Context.Request))
                {
                    try
                    {
                        SchedulerUtils.loginIntoSchedulerIfAllowed(Session, this.Context.Response, this.Context.Request);
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Error while logging into the scheduler", ex2);
                    }

                }
                else
                {
                    Global.systemLog.Error("Not able to login SMIWeb");
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error :createSMIWebSession ", ex);
                return false;
            }
            return true;
        }

        [WebMethod(EnableSession = true)]
        public NameValuePair[] getClientVariables()
        {
            try
            {
                List<NameValuePair> nvps = new List<NameValuePair>();
                string url = Util.getRequestBaseURL(this.Context.Request);
                nvps.Add(new NameValuePair("url", url));

                Space sp = null;
                try
                {
                    sp = Util.validateAndGetSpace(Session);
                }
                catch (Exception)
                {
                }

                string useisapi = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["isapiEnabled"];
                string smiroot = url;
                string schedulerRoot = url;
                if (useisapi != null && !bool.Parse(useisapi))
                {
                    if (sp != null)
                    {
                        SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                        smiroot = Util.getRequestScheme(this.Context.Request) + "://" + sc.URL;
                    }
                    schedulerRoot = Acorn.Utils.SchedulerUtils.getSchedulerRootIpAddress(this.Context.Request);
                }
                nvps.Add(new NameValuePair("smiroot", smiroot));
                nvps.Add(new NameValuePair("schedulerRoot", schedulerRoot));
                nvps.Add(new NameValuePair("acornroot", Server.MapPath("~").Replace('\\', '/')));

                // user specific items
                string locale = "en_us";
                string bingMaps = "false";
                if ((User)Session["user"] != null)
                {
                    Guid userID = ((User)Session["user"]).ID;
                    IDictionary<string, string> preferences = ManagePreferences.getPreference(null, userID, "Locale");
                    if (preferences["Locale"] != null)
                        locale = preferences["Locale"].Replace('-', '_');
                    bingMaps = Util.hasBingMaps(userID).ToString().ToLower();
                }
                nvps.Add(new NameValuePair("locale", locale));
                nvps.Add(new NameValuePair("bingMaps", bingMaps));
                string noheader = "false";
                if (Util.isNoHeader(this.Context.Request))
                {
                    noheader = "true";
                }
                nvps.Add(new NameValuePair("noheader", noheader));
                nvps.Add(new NameValuePair("gaAccount", FlexModule.gaTrackingAccount));
                string wmode;
                string extraParams = Util.getExtraParams(Server, this.Context.Request, out wmode);
                if (sp != null)
                    extraParams += FlexModule.setPageSequenceParams(sp.Directory);

                nvps.Add(new NameValuePair("extraParams", extraParams));

                return nvps.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in getClientVariables " + ex);
                throw ex;
            }
        }

        /*
         * End of Birst DBA related webservices
         * ==================================
         */
        public class TableSizesResult
        {
            public string[] types;
            public string[] names;
            public int[] numrows;
            public long[] loadTimes;
            public string ErrorMessage;
        }

        [WebMethod(EnableSession = true)]
        public TableSizesResult getTableSizes()
        {
            TableSizesResult tsr = new TableSizesResult();
            QueryConnection conn = null; 
            try
            {
                Util.validateAdminUser(Session);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return null;
                Space sp = (Space)Session["space"];
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                List<string> types = new List<string>();
                List<string> names = new List<string>();
                List<int> rows = new List<int>();
                Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                DatabaseConnection defaultConnection = maf.connection.connectionList[0];
                foreach (DatabaseConnection dc in maf.connection.connectionList)
                {
                    List<string> dctypes = new List<string>();
                    List<string> dcnames = new List<string>();
                    List<string> dcpnames = new List<string>();
                    List<int> dcrows = new List<int>();
                    Dictionary<string, string> ptables = new Dictionary<string, string>();
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        bool isliveaccess = false;
                        foreach (string lg in st.LoadGroups)
                        {
                            if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                            {
                                if (dc == maf.connection.findConnection(leConfigs[lg].getLocalETLConnection()))
                                {
                                    if (!ptables.ContainsKey(st.Name))
                                    {
                                        dctypes.Add("ADS");
                                        dcnames.Add(st.Name);
                                        dcrows.Add(0);
                                        isliveaccess = true;
                                        ptables.Add(st.Name, null);
                                    }
                                    break;
                                }
                            }
                        }
                        if (!isliveaccess && dc == defaultConnection)
                        {
                            if (!ptables.ContainsKey(st.Name))
                            {
                                dctypes.Add("ADS");
                                dcnames.Add(st.Name);
                                dcpnames.Add(st.Name);
                                dcrows.Add(0);
                                ptables.Add(st.Name, null);
                            }
                        }
                    }
                    foreach (MeasureTable mt in maf.measureTablesList)
                    {
                        if (mt.TableSource == null || mt.TableSource.Tables == null || mt.TableSource.Tables.Length == 0)
                            continue;
                        if (ptables.ContainsKey(mt.TableSource.Tables[0].PhysicalName))
                            continue;
                        string mgs = getMeasureGrainString(mt.Grain);
                        if (dc == maf.connection.findConnection(mt.TableSource.Connection))
                        {
                            dctypes.Add("MEASURE");
                            dcnames.Add(mgs);
                            dcpnames.Add(mt.TableSource.Tables[0].PhysicalName);
                            dcrows.Add(0);
                            ptables.Add(mt.TableSource.Tables[0].PhysicalName, null);
                        }
                    }
                    foreach (DimensionTable dt in maf.dimensionTablesList)
                    {
                        if (dt.TableSource == null || dt.TableSource.Tables == null || dt.TableSource.Tables.Length == 0)
                            continue;
                        if (dt.DimensionName == "Time")
                            continue;
                        if (ptables.ContainsKey(dt.TableSource.Tables[0].PhysicalName))
                            continue;
                        if (dc == maf.connection.findConnection(dt.TableSource.Connection))
                        {
                            dctypes.Add("DIM");
                            dcnames.Add(dt.DimensionName + "." + dt.Level);
                            dcpnames.Add(dt.TableSource.Tables[0].PhysicalName);
                            dcrows.Add(0);
                            ptables.Add(dt.TableSource.Tables[0].PhysicalName, null);
                        }
                    }
                    List<string> tables = new List<string>();
                    if (dc != null && dc.Realtime)
                    {
                        object[][] latables = LiveAccessUtil.getSchemaTables(Session, null, maf, sp, dc.Name, sp.Schema);
                        foreach (object[] row in latables)
                        {
                            tables.Add(((string)row[2]).ToLower());
                        }
                    }
                    else
                    {
                        List<string> resultTables = Database.getTables(conn, sp.Schema);
                        foreach (string s in resultTables)
                            tables.Add(s.ToLower());
                    }
                    StringBuilder union = new StringBuilder();
                    for (int i = 0; i < dctypes.Count; i++)
                    {
                        if (!tables.Contains(dcpnames[i].ToLower()))
                            continue;
                        string query = "SELECT '" + dcnames[i] + "',COUNT(*) FROM " + sp.Schema + "." + dcpnames[i];
                        if (union.Length > 0)
                            union.Append(" UNION ");
                        union.Append(query);
                    }
                    if (dc != null && dc.Realtime && union.Length > 0)
                    {
                        Object[][] res = LiveAccessUtil.executeLiveAccessQuery(Session, null, sp, dc.Name, union.ToString(), true);
                        if (res.Length > 0 && res[0].Length > 0)
                        {
                            for (int i = 0; i < dcnames.Count; i++)
                            {
                                for (int j = 0; j < res.Length; j++)
                                {
                                    if (dcnames[i] == res[j][0].ToString())
                                    {
                                        int result = 0;
                                        int.TryParse(res[j][1].ToString(), out result);
                                        dcrows[i] = result;
                                    }
                                }
                            }
                        }
                    }
                    else if (union.Length > 0)
                    {
                        QueryCommand cmd = conn.CreateCommand();
                        cmd.CommandTimeout = 240;
                        cmd.CommandText = union.ToString();
                        QueryReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            for (int i = 0; i < dcnames.Count; i++)
                            {
                                if (dcnames[i] == reader.GetString(0))
                                {
                                    Object o = reader.GetValue(1);
                                    if (o is int)
                                        dcrows[i] = (int)o;
                                    else if (o is long)
                                        dcrows[i] = (int)((long)o);
                                }
                            }
                        }
                    }
                    types.AddRange(dctypes);
                    names.AddRange(dcnames);
                    rows.AddRange(dcrows);
                }
                tsr.types = types.ToArray();
                tsr.names = names.ToArray();
                tsr.numrows = rows.ToArray();
            }
            catch (Exception ex)
            {
                tsr.ErrorMessage = "Unable to retrieve results of query";
                Global.systemLog.Error(ex);
            }
            finally
            {
                if (conn != null)
                    ConnectionPool.releaseConnection(conn);
            }
            return tsr;
        }

        [WebMethod(EnableSession = true)]
        public SearchResult searchCatalog(string path, string fileNameFilter, string dimValue, string exprValue, string labelValue)
        {
            try
            {
                Util.validateAdminUser(Session);
                return SearchCatalog.searchCatalog(Session, path, fileNameFilter, dimValue, exprValue, labelValue, null);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while searching catalog " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public SearchResult replaceCatalog(string[] reportPaths, string[] names, string dimValue, string exprValue, string labelValue, 
            string replaceDimValue, string replaceExprValue, string replaceLabelValue)
        {
            try
            {
                Util.validateAdminUser(Session);
                return SearchCatalog.replaceCatalog(Session, reportPaths, names, dimValue, exprValue, labelValue, replaceDimValue, replaceExprValue, replaceLabelValue);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while replacing in catalog " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public SearchResult undoReplace(string path, string fileNameFilter)
        {
            try
            {
                Util.validateAdminUser(Session);
                return SearchCatalog.undoReplace(Session, path, fileNameFilter);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while undoing catalog replace " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public AvailableRepositories getRepositoriesToCompare()
        {
            try
            {
                Util.validateAdminUser(Session);
                return CompareRepository.getRepositoriesToCompare(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while searching for repositories to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public RepositoryCompare compareToRepository(string path, string[] objecttypes)
        {
            try
            {
                Util.validateAdminUser(Session);
                return CompareRepository.compareToRepository(Session, path, objecttypes);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public RepositoryPullResult pullFromRepository(string repositoryPath, string[] objectPaths)
        {
            try
            {
                Util.validateAdminUser(Session);
                return CompareRepository.pullFromRepository(Session, repositoryPath, objectPaths);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to pull from: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult getSampleMetadata()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.getSampleMetadata(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult createSample(string dimension, string level, string source, double percent)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.createSample(Session, dimension, level, source, percent);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult deriveSample(string sourceDimension, string sourceLevel, string targetDimension, string targetLevel, string source)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.deriveSample(Session, sourceDimension, sourceLevel, targetDimension, targetLevel, source);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult applySample(string dimension, string level, string source)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.applySample(Session, dimension, level, source);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult removeSample(string dimension, string level, string source)
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.removeSample(Session, dimension, level, source);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public ManageSamplesResult deleteSamples()
        {
            try
            {
                Util.validateAdminUser(Session);
                return ManageSamples.deleteSamples(Session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while loading repository to compare to: " + ex);
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public XmlNode[] getMetadataRulesForValidation()
        {
            XmlNode[] obj = null;
            TrustedService.TrustedService ts = null;
            ts = new TrustedService.TrustedService();
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            User u = (User)Session["user"];
            if (u == null)
                return null;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";            

            try
            {
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, u.ID, "Locale");
                string userLocalePref = localePref["Locale"];
                obj = (XmlNode[])ts.getMetadataRulesForValidation(u.Username, u.isSuperUser || u.RepositoryAdmin, true, sp.ID.ToString(), sp.Directory, userLocalePref);
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Could not fetch metadata rules for validation ", e);
            }
            return obj;
        }

        [WebMethod(EnableSession = true)]
        public XmlNode[] validateRepositoryMetadata(string ruleSet, string ruleOrGrpName)
        {
            XmlNode[] obj = null;
            TrustedService.TrustedService ts = null;
            ts = new TrustedService.TrustedService();
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            User u = (User)Session["user"];
            if (u == null)
                return null;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
            {
                return null;
            }
            
            try
            {
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, u.ID, "Locale");
                string userLocalePref = localePref["Locale"];
                obj = (XmlNode[])ts.validateRepositoryMetadata(u.Username, u.isSuperUser || u.RepositoryAdmin, true, sp.ID.ToString(), sp.Directory, ruleSet, ruleOrGrpName, userLocalePref);                                
                if (obj != null && obj.Length > 0)
                {
                    obj[0] = saveMetadataValidationData(u.Username, sp.Directory, obj);
                }
                return obj;
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Could not validate repository metadata ",e);
            }
            return null;
        }

        const string METADATA_VALIDATION_FILE = "MetaDataValidation.xml";

        [WebMethod(EnableSession = true)]
        public XmlNode[] getLastMetadataValidationData()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            string filename = Path.Combine(sp.Directory, METADATA_VALIDATION_FILE);
            XmlDocument doc = new XmlDocument();
            try
            {
                if (File.Exists(filename))
                {
                    doc.Load(filename);
                    addLastRunDateToMetadataValidationData(doc, File.GetLastWriteTime(filename));
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Could not load metadata validation data ", e);
            }
            XmlNode[] ret = new XmlNode[1];
            ret[0] = doc;
            return ret;
        }

        [WebMethod(EnableSession = true)]
        public RServerResult uploadRServerFile(string path, string rexpression)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            User u = (User)Session["user"];
            if (u == null)
                return null;
                    MainAdminForm maf = (MainAdminForm)Session["MAF"];
                    if (maf == null)
                return null;
            return RServerUtil.UploadRServerFile(sp, u, maf, path, rexpression);
        }

         private XmlNode saveMetadataValidationData(string username, string directory, XmlNode[] data) 
        {
            if (data != null && data.Length > 0)
            {
                string filename = Path.Combine(directory, METADATA_VALIDATION_FILE);
                XmlNode root = data[0];
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(root.OuterXml);
                doc.Save(filename);
                addLastRunDateToMetadataValidationData(doc, File.GetLastWriteTime(filename));
                return doc;
            }
            return null;
        }

         private void addLastRunDateToMetadataValidationData(XmlDocument doc, DateTime date)
         {
             XmlAttribute attrib = doc.CreateAttribute("LastRunDate");
             attrib.Value = date.ToString("u");
             doc.FirstChild.Attributes.Append(attrib);
         }


        [WebMethod(EnableSession = true)]
        public XmlNode[] fixRepositoryMetadata(string ruleSet, string ruleOrGrpName)
        {
            XmlNode[] obj = null;
            TrustedService.TrustedService ts = null;
            ts = new TrustedService.TrustedService();
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            User u = (User)Session["user"];
            if (u == null)
                return null;
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            // 5 Minute timeout
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
            {
                return null;
            }

            try
            {
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, u.ID, "Locale");
                string userLocalePref = localePref["Locale"];
                obj = (XmlNode[])ts.fixRepositoryMetadata(u.Username, sp.ID.ToString(), sp.Directory, ruleSet, ruleOrGrpName, userLocalePref);
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Could not validate repository metadata ", e);
            }
            return obj;
        }
        [WebMethod(EnableSession = true)]
        public  string[][] getLocalizedOverridenParams(string Locale, User u)
        {
            string[][] ret = null;
            Dictionary<String, String> overrideParameters = Util.getLocalizedOverridenParams(Locale, u);
            ret = new string[overrideParameters.Count][];
            string [] keys = overrideParameters.Keys.ToArray();
            for (int i = 0; i < overrideParameters.Count; i++)
            {
                string[] t = new string[2];
                t[0] = keys[i];
                t[1] = overrideParameters[keys[i]];
                ret[i] = t;
            }
            return ret;
        }

        [WebMethod(EnableSession = true)]
        public RServerResult TestRServerConnection()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return null;
            User u = (User)Session["user"];
            if (u == null)
                return null;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return null;
            return RServerUtil.TestRServerConnection(sp);
        }
    }
}