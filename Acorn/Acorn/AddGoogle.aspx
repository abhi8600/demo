﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddGoogle.aspx.cs" Inherits="Acorn.AddGoogle"
    Title="Birst" MasterPageFile="~/WebAdmin.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <div style="font-size: 9pt">
            <a href="FlexModule.aspx?birst.module=admin">Back to Admin</a>
        </div>         
        <div style="height: 10px">
            &nbsp;</div>
        <asp:MultiView ID="GoogleAnalyticsMultiView" runat="server" ActiveViewIndex="0">
            <asp:View ID="Authenticate" runat="server">
                <div class="pageheader">
                    Google Analytics Connection Settings
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <div>
                    To connect to Google Analytics, please enter your Google Analytics account credentials:</div>
                <div style="height: 10px">
                    &nbsp;</div>
                <asp:Label ID="LoginError" runat="server" Visible="false" ForeColor="Red">Unable to connect to Google Analytics using this username/password - please try again</asp:Label>
                <table>
                    <tr>
                        <td>
                            Username:
                        </td>
                        <td>
                            <asp:TextBox ID="UsernameBox" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            <asp:RegularExpressionValidator id="EmailValidator" runat="server" 
                                ControlToValidate="UsernameBox" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                ErrorMessage="Invalid Google email address">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="PasswordBox" runat="server" MaxLength="64" Width="200px" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="CredentialsNextButton" runat="server" OnClick="CredentialsNext"
                    ImageUrl="~/Images/Next.png" />
                <br /><br />
                <asp:Label id="GAKeyNotFoundLblId" runat="server" Visible="false" ForeColor="Red">
                    Warning: You need to have a valid Google API Key before using this connector.
                </asp:Label>
                                                    
            </asp:View>
            <asp:View ID="ViewSettings" runat="server">
                <div class="pageheader">
                    View Current Google Analytics Connection Settings
                </div>
                <div style="height: 20px">
                    &nbsp;</div>
                <div style="font-weight: bold">
                    Select Google Analytics Profile and Date Range</div>
                <div style="height: 20px">
                    &nbsp;</div>
                <table width="55%">
                    <tr>
                        <td colspan="2">
                            <table runat="server" style="background-color: White; width: 98%; border: solid 1px #555555;
                                padding: 0px; border-collapse: collapse">
                                <tr>
                                    <td class="listviewheader">
                                        Profiles
                                    </td>
                                </tr>
                                <tr runat="server" id="groupPlaceholder">
                                    <td style="width: 20%; padding-left: 5px">
                                        <asp:RadioButtonList ID="SelectedProfile" runat="server">
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 15px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">
                            Start Date:
                        </td>
                        <td>
                            <asp:TextBox ID="StartDateBox" runat="server"></asp:TextBox><asp:ImageButton ID="CalImageButton1"
                                ImageUrl="~/Images/Calendar.png" runat="server" />
                            <cc1:CalendarExtender ID="StartDateBox_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="StartDateBox" PopupButtonID="CalImageButton1" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                            <asp:CompareValidator ID="CompareValidator1"  ValueToCompare="2005/01/01"  Type="Date"  Operator="GreaterThanEqual"  runat="server"
                                    ControlToValidate="StartDateBox"  ErrorMessage="Start date must be greater than or equal to 2005/01/01"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Date:
                        </td>
                        <td>
                            <asp:TextBox ID="EndDateBox" runat="server"></asp:TextBox><asp:ImageButton ID="CalImageButton2"
                                ImageUrl="~/Images/Calendar.png" runat="server" />
                            <cc1:CalendarExtender ID="EndDateBox_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="EndDateBox" PopupButtonID="CalImageButton2" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>

                            <asp:CompareValidator ID="CompareValidator2" ControlToCompare="StartDateBox"  Type="Date"  Operator="GreaterThan"  runat="server"
                                    ControlToValidate="EndDateBox"  ErrorMessage="End date must be greater than start date"  />

                        </td>
                    </tr>
                </table>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="SettingsNextButton" runat="server" OnClick="SettingsNext" ImageUrl="~/Images/Proceed.png" />
            </asp:View>
            <asp:View ID="CurrentSettingsView" runat="server">
                <div class="pageheader">
                    Google Analytics Connection Settings
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <div>
                    You are currently connected to Google Analytics
                    <div style="height: 10px">
                        &nbsp;</div>
                    <table>
                        <tr>
                            <td>
                                Title:
                            </td>
                            <td>
                                <asp:Label ID="TitleLabel" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Account ID:
                            </td>
                            <td>
                                <asp:Label ID="AccountNameLabel" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Web Property ID:
                            </td>
                            <td>
                                <asp:Label ID="PropertyIDLabel" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div style="height: 20px">
                        &nbsp;</div>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ModifyButton" runat="server" ImageUrl="~/Images/Modify_Copy_Share.png"
                                    OnClick="ModifyGoogle" />
                            </td>
                            <td>
                                <asp:LinkButton ID="ModifyLink" runat="server" OnClick="ModifyGoogle"
                                    CssClass="defaultlinkstyle">Modify your Google Analytics connection settings.</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/Images/Delete_Icon.png"
                                     OnClick="DeleteGoogle" />
                            </td>
                            <td>
                                <asp:LinkButton ID="DeleteLink" runat="server" OnClick="DeleteGoogle"
                                    CssClass="defaultlinkstyle">Remove your current connection to Google Analytics.</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
