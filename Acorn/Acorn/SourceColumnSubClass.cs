﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Xml.Serialization;
using Performance_Optimizer_Administration;

namespace Acorn
{
     [Serializable]
    public class SourceColumnSubClass
    {
         public SourceColumnSubClass()
         { 
         }
         public string Name;
         public string OriginalName;

        [DefaultValue(false)]
        public bool AnalyzeMeasure; 
        public string DataType;        
        public string Format;      
        public bool EnableSecutityFilter;
        public string SourceFileColumn;       
        public string[] TargetAggregations;
        public string[] TargetTypes;       
        public string UnknownValue;
        public int Width;
        public string HierarchyName;
        public string LevelName;
        public string[][] Levels;
        public bool AnalyzeByDate;
        public bool Measure;
        public bool LockType;
    }
}