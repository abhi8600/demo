﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Acorn.NetSuiteSSO
{
    public partial class EndUserLicenseAgreement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Accept_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/NetSuiteSSO/WebForm4.aspx");
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/NetSuiteSSO/WebForm1.aspx");
        }
    }
}