﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.DBConnection;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using Acorn.Background;
using Acorn.Utils;

namespace Acorn.NetSuiteSSO
{
    public partial class AdminsHomePage : System.Web.UI.Page
    {
       // public const string SPACE_TO_USE_GUID = "d64473db-47d9-4b5e-a74e-80679079076a";
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private bool dashboardWasClicked = false;
        string value = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //show progress UI??

        }

        public string getRedirectedURL()
        {
            string userName = null;
            bool lockedout = false;
            Guid spaceID;
            string module;
            string redirectUrl = null;
            Space sp;

            QueryConnection conn = ConnectionPool.getConnection();
            if (Session["username"] != null && !String.IsNullOrEmpty((string)Session["username"]))
            {
                userName = Session["username"].ToString();
            }

            User u = Database.getUser(conn, mainSchema, userName);
            if (u == null)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + userName);
                Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
            }
            else if (u.Disabled)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + userName);
                Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
            }
            Session["user"] = u;
            Util.setLogging(u, null);
            // determine the site redirection
            string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
            string redirectURL = null;
            // no site means go to the original URL (backwards compatibility)
            if (userRedirectedSite == null || userRedirectedSite.Length == 0)
            {
                redirectURL = Util.getRequestBaseURL(Request);
            }
            else
            {
                redirectURL = userRedirectedSite;
            }
              
            // get the Birst security token and validate the user (products, IP restrictions)           
            try
            {
                conn = ConnectionPool.getConnection();
                spaceID = Database.getSpaceForUser(conn, mainSchema, u.ID);
                if (spaceID == null)
                {
                    Global.systemLog.Error("Space ID does not exist for user - " + userName);
                    return null;
                }
                sp = Util.getSpaceById(conn, mainSchema, u, spaceID);
                if (sp == null)
                {
                    Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceID + " or the space does not exist");
                    return null;
                }
                Session["space"] = sp;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            string token = Outbound.getSecurityToken(conn, u, spaceID, "", ref lockedout);

            if (token == null || token.Length == 0)
            {
                if (lockedout)
                {
                    Global.systemLog.Debug("Login has been locked after too many unsuccessful attempts");
                    Response.Write("Login has been locked after too many unsuccessful attempts");
                }
                else
                {
                    Global.systemLog.Debug("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                    Response.Write("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                }
                return null;

            }
            string token1 = Util.getSMILoginToken(u.Username, sp);
            if (token1 == null || token1.Length == 0)
            {
                if (lockedout)
                {
                    Global.systemLog.Debug("Login has been locked after too many unsuccessful attempts");
                    Response.Write("Login has been locked after too many unsuccessful attempts");
                }
                else
                {
                    Global.systemLog.Debug("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                    Response.Write("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                }
                return null;

            }
            MainAdminForm maf = Util.getSessionMAF(Session);
            Util.getSMILoginSession(Session, sp, token1, HttpContext.Current.Response, HttpContext.Current.Request, false, maf);
            if (dashboardWasClicked)
                module = "dashboard";
            else
                module = "visualizer";
            Session["BirstSSOToken"] = token;
            // once authenticated, redirect to the SSO servlet`
            token = Util.encryptToken(token);

            redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.embedded=true&birst.module=" + module;

            Global.systemLog.Debug("Redirecting to: " + redirectUrl + " Response Status Code is: " + Response.StatusCode);

            return redirectUrl;
        }

        protected void RefreshData_Click(object sender, EventArgs e)
        {
            string[] passValues = Request.Form.GetValues("admin-pass");
            if (passValues != null && passValues.Length == 1 && !String.IsNullOrEmpty(passValues[0]))
            {
                Session["passwd"] = passValues[0];
            }
            else
            {
                Global.systemLog.Debug("Admin Password not provided");
                Response.Write("Admin Password not provided");
                Response.StatusCode = 400;
                return;
            }
            NSUtil.startExtractAndProcessData(Session, Util.getSessionSpace(Session), Util.getSessionUser(Session), null);
            //show progress UI
        }

        public string getStatus(object sender, EventArgs e)
        {
            string statusString = null;
            XElement element = NSUtil.getStatus(Session, Util.getSessionSpace(Session), Util.getSessionUser(Session));
            if (element != null)
            {
                XElement[] bwsResultElement = WebServiceResponseXml.getElements(element, "BirstWebServiceResult");
                XElement[] resultElement = WebServiceResponseXml.getElements(bwsResultElement[0], "Result");
                XElement[] extractionStatusElement = WebServiceResponseXml.getElements(resultElement[0], "ExtractionStatus");
                XElement[] sourceElement = WebServiceResponseXml.getElements(extractionStatusElement[0], "Source");
                XElement[] statusElement = WebServiceResponseXml.getElements(extractionStatusElement[0], "Status");
                //XElement[] runningConnectorElement = WebServiceResponseXml.getElements(extractionStatusElement[0], "RunningConnector");
                bool isExtractionSource = false;
                bool isProcessSource = false;
                if (sourceElement != null && sourceElement.Length > 0 && sourceElement[0] != null && sourceElement[0].Value == "Extract")
                {
                    isExtractionSource = true;
                }
                else if (sourceElement != null && sourceElement.Length > 0 && sourceElement[0] != null && sourceElement[0].Value == "Process")
                {
                    isProcessSource = true;
                }
                statusString += isExtractionSource ? "Extraction " : "Data Processing ";
                int status = (statusElement != null && statusElement.Length > 0 && statusElement[0] != null ? Int32.Parse(statusElement[0].Value) : -1);
                if (status != -1)
                {
                    if (status == Status.RUNNING)
                    {
                        statusString += Status.RUNNING_STR;
                    }
                    else if (status == Status.COMPLETE)
                    {
                        statusString += Status.COMPLETE_STR;                        
                    }
                    else if (status == Status.FAILED)
                    {
                        statusString += Status.FAILED_STR;                        
                    }
                    else if (status == Status.CANCELLED)
                    {
                        statusString += Status.FAILED_STR;                        
                    }
                    if (isExtractionSource && status != Status.RUNNING)
                    {
                        NSUtil.cleanupConnectorCredentials(Util.getSessionUser(Session), Util.getSessionSpace(Session), Session);
                    }
                }
            }
            return statusString;
        }

        protected void Dashboards_Click(object sender, EventArgs e)
        {
            dashboardWasClicked = true;
            if (getRedirectedURL() != null)
            {
                value = getRedirectedURL();
            }
            Response.Redirect(value);
        }

        protected void Visualizer_Click(object sender, EventArgs e)
        {
            dashboardWasClicked = false;
            if (getRedirectedURL() != null)
            {
                value = getRedirectedURL();
            }
            Response.Redirect(value);

        }
    }
}