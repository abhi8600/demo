﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Acorn.NetSuiteSSO
{
    public partial class AdminsInitialLandingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void Go_Click(object sender, EventArgs e)
        {
            string[] passValues = Request.Form.GetValues("admin-pass");
            if (passValues != null && passValues.Length == 1 && !String.IsNullOrEmpty(passValues[0]))
            {
                Session["passwd"] = passValues[0];
            }
            else
            {
                Global.systemLog.Debug("Admin Password not provided");
                Response.Write("Admin Password not provided");
                Response.StatusCode = 400;
                return;
            }
            Response.Redirect("~/NetSuiteSSO/BirstAnalyticConfigurations.aspx", false);
            
        }
    }
}