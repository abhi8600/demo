﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.Messaging;
using System.IO;
using System.Xml;
using Acorn.DBConnection;
using System.Web.Security;
using DotNetOpenAuth.Messaging.Bindings;
using DotNetOpenAuth.OAuth.Messages;
using System.Security.Cryptography;
using System.Net;

namespace Acorn.NetSuiteSSO
{
    public partial class Outbound : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
        string consumerKey = System.Web.Configuration.WebConfigurationManager.AppSettings["NSConsumerKey"];
        string sharedSecret = System.Web.Configuration.WebConfigurationManager.AppSettings["NSSharedSecret"];

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Generate the timestamp for the signature        
        /// </summary>
        /// <returns></returns>
        public virtual string GenerateTimeStamp()
        {
            // Default implementation of UNIX time of the current UTC time
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// Generate a nonce
        /// </summary>
        /// <returns></returns>
        public virtual string GenerateNonce()
        {
            byte[] rnd = new byte[4];
            rngCsp.GetBytes(rnd);
            int val = Math.Abs(BitConverter.ToInt32(rnd, 0));
            return val.ToString();
        }


        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            string token = Request.QueryString["oauth_token"];
            string birstVars = Request.QueryString["sessionvars"];
            string dc = Request.QueryString["dc"];
            string env = Request.QueryString["env"];
            string verifyURLPath = "/app/common/integration/ssoapplistener.nl";
            string SystemDomain = "";
            string SSOVerifyUrl = "";

            Global.systemLog.Debug("--Netsuite OAuth-- NetSuite DataCenter ID: " + dc + " and System Domain: " + env + " For this netSuite account.");
            if (dc == "001")
            {
                if (env == "PRODUCTION")
                {
                    SystemDomain = "https://system.netsuite.com";
                }
                else if (env == "SANDBOX")
                {
                    SystemDomain = "https://system.sandbox.netsuite.com";
                }
                else if (env == "BETA")
                {
                    SystemDomain = "https://system.beta.netsuite.com";
                }
                else 
                {
                    Global.systemLog.Error("--Netsuite OAuth-- Environment Type: " + env + " is invalid.");
                    return;
                }
            }
            else if(dc == "002")
            {
                if (env == "PRODUCTION")
                {
                    SystemDomain = "https://system.na1.netsuite.com";
                }
                else if (env == "SANDBOX")
                {
                    SystemDomain = "https://system.na1.sandbox.netsuite.com";
                }
                else if (env == "BETA")
                { 
                    SystemDomain="https://system.na1.beta.netsuite.com";
                }
                else
                {
                    Global.systemLog.Error("--Netsuite OAuth-- Environment Type: " + env + " is invalid.");
                    return;
                }
            }
            else
            {
                Global.systemLog.Error("--Netsuite OAuth-- Data Center: " + dc + " is invalid.");
                return;
            }
            SSOVerifyUrl = SystemDomain + verifyURLPath;
            Global.systemLog.Debug("--Netsuite OAuth-- NetSuite SSO URL: " + SSOVerifyUrl);

            Global.systemLog.Info("--Netsuite OAuth-- Access Token: " + token);

            if (token == null || token.Equals(""))
            {
                Global.systemLog.Error("--Netsuite OAuth-- Access Token is invalid or missing");
                return;
            }

            var auth = "OAuth oauth_token=\"" + token + "\",oauth_consumer_key=\"" + consumerKey + "\",oauth_signature_method=\"PLAINTEXT\",oauth_signature=\"" + HttpUtility.UrlEncode(sharedSecret) + "&\",oauth_timestamp=\"" + GenerateTimeStamp() + "\",oauth_nonce=\"" + GenerateNonce() + "\"";
            Global.systemLog.Info("--Netsuite OAuth-- Authorization Header: " + auth);

            try
            {
                WebResponse response = null;
                try
                {
                    response = generateOAuthRequest(SSOVerifyUrl, auth).GetResponse();
                }
                catch (System.Net.WebException exception)
                {
                    //If env or dc is invalid then Wrong URL will be generated
                    Global.systemLog.Info(exception.ToString());
                    Global.systemLog.Error("--Netsuite OAuth-- invalid SSO URL.");
                    return;
                }

                Global.systemLog.Info("--Netsuite OAuth-- Response URI: " + response.ResponseUri.AbsolutePath + " headers: " + response.Headers.ToString());
                StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                String sXMLRequest = reader.ReadToEnd();
                Global.systemLog.Info("--Netsuite OAuth-- Response Stream: " + sXMLRequest);

                XmlDocument xmlRequest = new XmlDocument();
                xmlRequest.XmlResolver = null;
                xmlRequest.LoadXml(sXMLRequest);

                var email = xmlRequest.GetElementsByTagName("ENTITYEMAIL").Item(0).InnerText;
                Global.systemLog.Info("--Netsuite OAuth-- Email address of the Netsuite user " + email);

                ProccessResponse(email, birstVars);
            }
            catch (System.Exception exception)
            {
                Global.systemLog.Info(exception.ToString());
                return;
            }

        }

        public HttpWebRequest generateOAuthRequest(string url, string authHeader)
        {
            HttpWebRequest request = System.Net.WebRequest.Create(url) as HttpWebRequest;
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.AllowWriteStreamBuffering = true;

            request.PreAuthenticate = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

            HttpUtility.UrlEncode(sharedSecret);
            request.Headers.Add("Authorization", authHeader);

            return request;
        }

        /// <summary>
        /// Processes a successful response.
        /// </summary>
        private void ProccessResponse(string email, string birstVals)
        {

            birstVals = birstVals.Replace('|', '&');   //Only for the first pass integration

            string module, hidePrompt, hideNav, dashboard, dashPage,renderType,viewMode;

            System.Collections.Specialized.NameValueCollection parameters = HttpUtility.ParseQueryString(birstVals);

            //If request contains parameter but user has not entered value for parameter than javascript returns null as a string hence adding a condition to check if string was null or not
            module = parameters.Get("Birst.Module");
            module = (module == null || module == "null" || module.Equals("")) ? "dashboard" : module;

            hidePrompt = parameters.Get("birst.hideDashboardPrompts");
            hidePrompt = (hidePrompt == null || hidePrompt == "null" || hidePrompt.Equals("")) ? "false" : hidePrompt;

            hideNav = parameters.Get("birst.hideDashboardNavigation");
            hideNav = (hideNav == null || hideNav == "null" || hideNav.Equals("")) ? "false" : hideNav;

            dashboard = parameters.Get("birst.dashboard") == null ? "" : parameters.Get("birst.dashboard");
            dashPage = parameters.Get("birst.page") == null ? "" : parameters.Get("birst.page");

            renderType = getValueIfNullString(parameters.Get("birst.renderType"));

            viewMode = getValueIfNullString(parameters.Get("birst.viewMode"));

            Guid spaceId = Guid.Parse(parameters.Get("Birst.SpaceId"));

            QueryConnection conn = ConnectionPool.getConnection();
            bool lockedout = false;
            
            // getting the Birst User
            User u = Database.getUser(conn, mainSchema, email);

            if (u == null)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                Response.Write("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                Response.StatusCode = 400;
                return;
            }
            if (u.Disabled)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                Response.Write("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                Response.StatusCode = 400;
                return;
            }

            Util.setLogging(u, null);
            // determine the site redirection
            string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
            string redirectURL = null;
            // no site means go to the original URL (backwards compatibility)
            if (userRedirectedSite == null || userRedirectedSite.Length == 0)
            {
                redirectURL = Util.getRequestBaseURL(Request);
            }
            else
            {
                redirectURL = userRedirectedSite;
            }
            // get the Birst security token and validate the user (products, IP restrictions)

            string token = getSecurityToken(conn, u, spaceId, "", ref lockedout);
            if (token == null || token.Length == 0)
            {
                if (lockedout)
                {
                    Global.systemLog.Debug("Login has been locked after too many unsuccessful attempts");
                    Response.Write("Login has been locked after too many unsuccessful attempts");
                }
                else
                {
                    Global.systemLog.Debug("The SpaceID " + spaceId + " is not associated with the user  " + email);
                    Response.Write("The SpaceID " + spaceId + " is not associated with the user  " + email);
                }
                return;

            }

            // once authenticated, redirect to the SSO servlet
            token = Util.encryptToken(token);
            string redirectUrl=null;
           
                if (!string.IsNullOrEmpty(renderType) && string.IsNullOrEmpty(viewMode))
                {
                    redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.module=" + module + "&birst.embedded=true" +
                                     "&birst.hideDashboardPrompts=" + hidePrompt + "&birst.hideDashboardNavigation=" + hideNav + "&birst.dashboard=" + dashboard + "&birst.page=" + dashPage + "&birst.renderType=" + renderType;
                }
                else if (string.IsNullOrEmpty(renderType) && !string.IsNullOrEmpty(viewMode))
                {
                    redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.module=" + module + "&birst.embedded=true" +
                                     "&birst.hideDashboardPrompts=" + hidePrompt + "&birst.hideDashboardNavigation=" + hideNav + "&birst.dashboard=" + dashboard + "&birst.page=" + dashPage + "&birst.viewMode=" + viewMode;
                }
                else if (string.IsNullOrEmpty(renderType) && string.IsNullOrEmpty(viewMode))
                {
                    redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.module=" + module + "&birst.embedded=true" +
                                     "&birst.hideDashboardPrompts=" + hidePrompt + "&birst.hideDashboardNavigation=" + hideNav + "&birst.dashboard=" + dashboard + "&birst.page=" + dashPage;
                }
                else
                {
                    redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.module=" + module + "&birst.embedded=true" +
                                     "&birst.hideDashboardPrompts=" + hidePrompt + "&birst.hideDashboardNavigation=" + hideNav + "&birst.dashboard=" + dashboard + "&birst.page=" + dashPage + "&birst.viewMode=" + viewMode + "&birst.renderType=" + renderType;
                }
                Global.systemLog.Debug("Redirecting to: " + redirectUrl + " Response Status Code is: " + Response.StatusCode);
                Response.Redirect(redirectUrl);
           
        }

        public static string getSecurityToken(QueryConnection conn, User u, Guid spaceId, string sessionVarParams, ref bool lockedout)
        {
            Global.systemLog.Info("Creating Security Token. UserName: " + u.ToString() + " Space ID: " + spaceId.ToString() + " Session vars: " + sessionVarParams);
            if ((lockedout = isLockedOut(u)) == true)
                return null;
            Space sp = null;
            if (spaceId != Guid.Empty)
            {
                sp = Util.getSpaceById(conn, mainSchema, u, spaceId);
                if (sp == null)
                {
                    Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceId + " or the space does not exist");
                    return null;
                }
            }

            if (!Global.initialized)
                return "";

            // create a token and store the parameters in the database
            SSOToken ssoToken = new SSOToken();
            ssoToken.tokenID = Guid.NewGuid();
            ssoToken.userID = u.ID;
            ssoToken.spaceID = sp != null ? sp.ID : Guid.Empty;
            ssoToken.sessionVariables = sessionVarParams;
            ssoToken.repAdmin = u.RepositoryAdmin;
            ssoToken.type = "Netsuite SSO";
            Database.createSSOToken(conn, mainSchema, ssoToken);
            return ssoToken.tokenID.ToString();

        }

        private static bool isLockedOut(User u)
        {
            MembershipUser mu = Membership.GetUser(u.Username, false);
            if (mu != null && mu.IsLockedOut)
            {
                Global.systemLog.Warn("Security token request failed - user locked out");
                return true;
            }
            return false;
        }

        //Request may contain null string value so checking it
        public static string getValueIfNullString(String Param)
        {
            if (Param != null && Param.Equals("null"))
            {
                Param = null;
            }
            return Param;
        }
    }
}
