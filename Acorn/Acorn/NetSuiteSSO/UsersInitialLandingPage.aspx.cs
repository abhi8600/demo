﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Acorn.NetSuiteSSO
{
    public partial class UserInitialLandingPage : System.Web.UI.Page
    {
         protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["birstAnalyticConfigured"] != null && !String.IsNullOrEmpty(Session["birstAnalyticConfigured"].ToString()))
            {
                string value = Session["birstAnalyticConfigured"].ToString();              
                 if (value == "true")
                   Go.Visible = true;
                else
                    Go.Visible = false; 
            }
        }

        protected void Go_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/NetSuiteSSO/BirstAnalyticConfigurations.aspx");         
        }
    }
   }
