﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.DBConnection;

namespace Acorn.NetSuiteSSO
{
    public partial class UsersHomePage : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private bool dashboardWasClicked = false;
        string value = null;

        protected void Page_Load(object sender, EventArgs e)
        {


        }
        public string getRedirectedURL()
        {
            string userName = null;
            bool lockedout = false;
            Guid spaceID;
            string module;
            string redirectUrl = null;

            QueryConnection conn = ConnectionPool.getConnection();
            if (Session["username"] != null && !String.IsNullOrEmpty((string)Session["username"]))
            {
                userName = Session["username"].ToString();
            }

            User u = Database.getUser(conn, mainSchema, userName);
            if (u == null)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + userName);
                Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
            }
            else if (u.Disabled)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + userName);
                Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
            }

            Util.setLogging(u, null);
            // determine the site redirection
            string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
            string redirectURL = null;
            // no site means go to the original URL (backwards compatibility)
            if (userRedirectedSite == null || userRedirectedSite.Length == 0)
            {
                redirectURL = Util.getRequestBaseURL(Request);
            }
            else
            {
                // redirectURL = userRedirectedSite;
                redirectURL = "https://localhost:44300";
            }
            // get the Birst security token and validate the user (products, IP restrictions)           
            try
            {
                conn = ConnectionPool.getConnection();
                spaceID = Database.getSpaceForUser(conn, mainSchema, u.ID);
                if (spaceID == null)
                {
                    Global.systemLog.Error("Space ID does not exist - " + spaceID);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            string token = Outbound.getSecurityToken(conn, u, spaceID, "", ref lockedout);

            if (token == null || token.Length == 0)
            {
                if (lockedout)
                {
                    Global.systemLog.Debug("Login has been locked after too many unsuccessful attempts");
                    Response.Write("Login has been locked after too many unsuccessful attempts");
                }
                else
                {
                    Global.systemLog.Debug("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                    Response.Write("The SpaceID " + spaceID + " is not associated with the user  " + userName);
                }
                return null;

            }
            if (dashboardWasClicked)
                module = "dashnoard";
            else
                module = "visualizer";

            // once authenticated, redirect to the SSO servlet`
            token = Util.encryptToken(token);

            redirectUrl = TokenGenerator.getSSORedirectURL(redirectURL, token, Request, Request.QueryString) + "&birst.module=" + module;

            Global.systemLog.Debug("Redirecting to: " + redirectUrl + " Response Status Code is: " + Response.StatusCode);

            return redirectUrl;
        }

        protected void Dashboards_Click(object sender, EventArgs e)
        {
            dashboardWasClicked = true;
            if (getRedirectedURL() != null)
            {
                value = getRedirectedURL();
            }
            Response.Redirect(value);
        }

        protected void Visualizer_Click(object sender, EventArgs e)
        {
            dashboardWasClicked = false;
            if (getRedirectedURL() != null)
            {
                value = getRedirectedURL();
            }
            Response.Redirect(value);
        }
    }
}