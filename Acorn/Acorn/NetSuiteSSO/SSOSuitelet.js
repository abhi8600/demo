/**
 * Module Description
 * 
 * Version    Date              Author           Remarks
 * 1.3          January 21 2014   Hardik       Added two new parameters and null check for module
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function buildSuitelet(request, response)
{
	var title = nlapiGetContext().getSetting('SCRIPT', 'custscriptmodule') == null ? "Dashboard" : nlapiGetContext().getSetting('SCRIPT', 'custscriptmodule');
	var form = nlapiCreateForm(title.charAt(0).toUpperCase() + title.slice(1));

	var contentHeight = 700, contentWidth = 1200;

    	// set default width and height for iframe
	var contentStyle = "width: " + contentWidth;
    	contentStyle += "; height: " + contentHeight;
    	contentStyle += "; margin:0; border:0; padding:0";

	var spaceId = 'birst.spaceId=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptsid');
	var hideDashboardNavigation = 'birst.hideDashboardNavigation=' + nlapiGetContext().getSetting('SCRIPT', 'custscripthidenavigation');
	var hideDashboardPrompts = 'birst.hideDashboardPrompts=' + nlapiGetContext().getSetting('SCRIPT', 'custscripthideprmpt');
	var module = 'birst.module=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptmodule');
	var dashboard = 'birst.dashboard=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptdash');
	var page = 'birst.page=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptpage');
	var viewMode = 'birst.viewMode=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptviewmode');
	var renderType = 'birst.renderType=' + nlapiGetContext().getSetting('SCRIPT', 'custscriptrendertype');
	var sessionVars = 'sessionvars=' + spaceId + '|' + hideDashboardNavigation + '|' + hideDashboardPrompts + '|' + module + '|' + dashboard+ '|' + page + '|' + viewMode + '|' + renderType;

	nlapiLogExecution('AUDIT', 'nlapiOutboundSSO is called', 'customsso1');
	var url = nlapiOutboundSSO('customsso1' ) + '&' + sessionVars;
	nlapiLogExecution('AUDIT', 'url is returned', 'return url= ' + url );

	var content = '<iframe id="oa-sso-iframe" src="'+url+'" align="center" style="' + contentStyle +'" seamless></iframe>';
	var iFrame = form.addField('custpage_sso', 'inlinehtml', 'SSO', null);
	form.setScript('customscript_framesize');
	iFrame.setDefaultValue (content);
	iFrame.setLayoutType('startrow', 'startcol');

	response.writePage(form);
}