﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Acorn.DBConnection;
using Acorn.Utils;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Text;
using System.Collections;
using System.IO;

namespace Acorn.NetSuiteSSO
{
    public partial class BirstAnalyticConfigurations : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        
        protected void Page_Load(object sender, EventArgs e)
        {
            QueryConnection conn = ConnectionPool.getConnection();
            string userName = null;
            string accountName = null;
            Guid accountId;
            string email = null;
            string role = null;
            string password = null;
            Space sp = null;

            UserSignup signUp = new UserSignup();

            if (Session["username"] != null && !String.IsNullOrEmpty(Session["username"].ToString()))
            {
                userName = Session["username"].ToString();
            }
            if (Session["accountName"] != null && !String.IsNullOrEmpty(Session["accountName"].ToString()))
            {
                accountName = Session["accountName"].ToString();
            }
            if (Session["role"] != null && !String.IsNullOrEmpty(Session["role"].ToString()))
            {
                role = Session["role"].ToString();
            }
            if (Session["email"] != null && !String.IsNullOrEmpty(Session["email"].ToString()))
            {
                email = Session["email"].ToString();
            }
            if (Session["passwd"] != null && !String.IsNullOrEmpty(Session["passwd"].ToString()))
            {
                password = Session["passwd"].ToString();
            }
            if (Session["space"] != null && !String.IsNullOrEmpty(Session["space"].ToString()))
            {
                sp = (Space)Session["space"];
            }

            Session["netSuiteTrial"] = true;
            bool accountExists = Database.accountExists(conn, mainSchema, accountName);
            if (accountExists)
            {
                accountId = Database.getAccountID(conn, mainSchema, accountName);
            }
            else
            {
                Database.createAccount(conn, mainSchema, accountName);
                accountId = Database.getAccountID(conn, mainSchema, accountName);
            }
            User u = Database.getUser(conn, mainSchema, userName);
            if (u == null)
            {
                string randomPasswd = System.Web.Security.Membership.GeneratePassword(32, 8);
                UserSignup.ReturnMessage message = signUp.createDiscoveryTrialUserForNetsuite(userName, randomPasswd, accountId);
                u = Database.getUser(conn, mainSchema, userName);
                u.Email = email;
                Database.updateUser(conn, mainSchema, u);
                bool isAdmin = Session["adminUser"] != null;
                if (isAdmin)
                {
                    Guid managedUserID = u.ManagedAccountId;
                    Database.updateAdminUserParams(conn, mainSchema, managedUserID);
                }               
            }
            if (u == null)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                Response.Write("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                Response.StatusCode = 400;
                return;
            }
            else if (u.Disabled)
            {
                Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                Response.Write("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                Response.StatusCode = 400;
                return;
            }

            Guid spaceID = Database.getSpaceIDForAccount(conn, mainSchema, u.ID);
            if (spaceID == null || spaceID == Guid.Empty)
            {
                Global.systemLog.Error("Space ID does not exist for user - " + email);
                Response.Write("Space ID does not exist for user - " + email);
                Response.StatusCode = 400;
                return;
            }
            sp = Database.getSpace(conn, mainSchema, spaceID);
            if (sp == null)
            {
                Global.systemLog.Error("Space does not exist for user - " + email);
                Response.Write("Space does not exist for user - " + email);
                Response.StatusCode = 400;
                return;
            }
            Session["user"] = u;
            Session["space"] = sp;
            Util.setSpace(Session, sp, u);

            if (role == "3")
            {
                NSUtil.startExtractAndProcessData(Session, Util.getSessionSpace(Session), Util.getSessionUser(Session), null);
                Response.Redirect("~/NetSuiteSSO/AdminsHomePage.aspx", false);
            }
            else
            {
                Response.Redirect("~/NetSuiteSSO/UsersHomePage.aspx", false);
            }
        }

    }

}

