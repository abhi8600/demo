﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsersHomePage.aspx.cs" Inherits="Acorn.NetSuiteSSO.UsersHomePage" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="NetSuite for Birst">
	<title>NetSuite</title>
	
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="css/main.css">
</head>
<body>



<!-- Modal: Dashboards Video Modal -->
<div id="db-video-modal" class="modal fade video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
	      	<iframe width="853" height="480" src="https://www.youtube.com/embed/AqyE0gid0Jc" frameborder="0" allowfullscreen></iframe>
    	</div>
  	</div>
</div>

<!-- Modal: Visualizer Video Modal -->
<div id="viz-video-modal" class="modal fade video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
	      	<iframe width="853" height="480" src="https://www.youtube.com/embed/AqyE0gid0Jc" frameborder="0" allowfullscreen></iframe>
    	</div>
  	</div>
</div>
<div class="wrapper">

	<!--<div class="header">
		<img src="image/header.png">
	</div> -->


	<div class="upper">
        <div id="birst-enabled">
    		<div class="title">Welcome to Your Birst Home Page</div>
    		<div class="intro">
    			Birst helps companies optimize key business processes by providing greater insight into financial operations. Enjoy our 100% free edition, Birst Express, and access a library of pre-built KPIs, reports and dashboards that will accelerate your decision-making. Analyze booking-to-cash data across products, customer types, geographies and more. Identify your most profitable products, and understand pricing and discounting trends. Gain insight into subscription business KPIs such as deferred revenue and renewal rates. You can also upgrade to our Birst Discovery and Birst Enterprise editions and combine data from all your corporate systems, like marketing and sales operations, for a complete end-to-end view of your business.
    		</div> 
        </div>
		
	</div>


	<div class="lower">

        <form id="form1" runat="server">

     <div id="birst-lower">
        	<div class="lower-double" id="lower-db">
            	<div class="lower-product-preview video-preview" data-toggle="modal" data-target="#db-video-modal"><img src="../images/db_preview.png"></div>
            	<div class="lower-product-btn">
            		<img src="../images/db_icon.png">
            		<div class="lower-product-name"><asp:Button ID="Dashboards" runat="server" onclick="Dashboards_Click" 
        Text="Dashboards" /></div>
            	</div>
            	<div class="lower-product-text">Click Dashboards to view pre-built dashboards containing key business metrics.</div>
            </div>
            <div class="lower-double" id="lower-viz">
            	<div class="lower-product-preview video-preview"  data-toggle="modal" data-target="#viz-video-modal"><img src="../images/viz_preview.png"></div>
            	<div class="lower-product-btn">
            		<img src="../images/viz_icon.png">
            		<div class="lower-product-name"><asp:Button ID="Visualizer" runat="server" onclick="Visualizer_Click" 
            Text="Visualizer" /></div>
            	</div>
            	<div class="lower-product-text">Click Visualizer to explore your NetSuite data and create visualizations to help you answer business questions quickly.</div>
            </div>

            <div id="birst-bottom">Your data was last refreshed on 04/14/2014</div>
        </div>

        </form>
        
	</div>


	<div class="footer"></div>
</div>


<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>


</body>
</html>


