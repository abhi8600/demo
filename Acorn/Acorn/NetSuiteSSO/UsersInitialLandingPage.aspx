﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsersInitialLandingPage.aspx.cs" Inherits="Acorn.NetSuiteSSO.UserInitialLandingPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="~/NetSuiteSSO/css/main.css">   
     <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
</head>
<body>
   <!-- Modal: General Video Modal -->
   <div id="general-video-modal" class="modal fade video-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	    <div class="modal-dialog modal-lg">
    	    <div class="modal-content">
	      	    <iframe width="853" height="480" src="https://www.youtube.com/embed/AqyE0gid0Jc" frameborder="0" allowfullscreen></iframe>
    	    </div>
  	    </div>
    </div>
    <!-- Modal: Confirm Modal -->
<div id="confirm-box-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
    	    <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirmModalLabel">Confirm</h4>
            </div>
            <div class="modal-body">
                <p>Please read the Birst Analytics FAQ document before proceeding. Do you want to continue configuring Birst Analytics?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <form id="form1" runat="server">
                    <asp:Button ID="Go" runat="server"  class="btn btn-primary btn-go" OnClick="Go_Click" Text="Continue" />
                    <!--<button data-dismiss="modal" data-toggle="modal" data-target="#progress-modal" type="button" class="btn btn-primary">Continue</button>-->
                </form>
            </div>
    	</div>
  	</div>
</div>
    <div class="wrapper">
	<div class="upper">
   <!-- <div id="birst-enabled"> -->
    		<div class="title">Welcome to Birst Analytics</div>
    		<div class="intro">
    			Birst helps companies optimize key business processes by providing greater insight into financial operations. Enjoy our 100% free edition, Birst Express, and access a library of pre-built KPIs, reports and dashboards that will accelerate your decision-making. Analyze booking-to-cash data across products, customer types, geographies and more. Identify your most profitable products, and understand pricing and discounting trends. Gain insight into subscription business KPIs such as deferred revenue and renewal rates. You can also upgrade to our Birst Discovery and Birst Enterprise editions and combine data from all your corporate systems, like marketing and sales operations, for a complete end-to-end view of your business.
    		</div>
    		<div class="float-image">
	            <img src="image/video_text.png">
	        </div>
			<div class="mac-image video-preview" data-toggle="modal" data-target="#general-video-modal">
	            <img src="image/mac_preview.png">
	        </div>
  </div>   
  
	<div class="lower">  
        <div id="inital-admin">
    		<div class="subtitle">Yes! Enable Birst Analytics</div>
    		<div class="infomation">
    			Click Go and enter your administrator password to enable Birst Analytics.
    		</div>
    	<!--	<div id="password-form" class="input-form form-group">
                <div class="form-inline" role="form"> -->
    				<!--<input id="admin-pass" type="password" class="form-control" placeholder="Enter the password">-->
                    <div>
                    <button id="go-button" class="btn btn-primary btn-go" data-target="#confirm-box-modal">Go!</button>			
    			</div>
    		</div>           
      </div>
  </div>
     <!--   </div>  -->
</body>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('#go-button').on('click', function () {
            $('#confirm-box-modal').modal('show');
        });
    });
</script>

</html>
