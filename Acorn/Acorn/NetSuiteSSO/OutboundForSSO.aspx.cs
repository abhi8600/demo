﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.Messaging;
using System.IO;
using System.Xml;
using Acorn.DBConnection;
using System.Web.Security;
using DotNetOpenAuth.Messaging.Bindings;
using DotNetOpenAuth.OAuth.Messages;
using System.Security.Cryptography;
using System.Net;

namespace Acorn.NetSuiteSSO
{
    public partial class OutboundForSSO : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
        private static string consumerKey = System.Web.Configuration.WebConfigurationManager.AppSettings["NSFremiumConsumerKey"];
        private static string sharedSecret = System.Web.Configuration.WebConfigurationManager.AppSettings["NSFremiumSharedSecret"];
        private static string enableNSFremium = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableNSFremium"];
        public static bool NSFreemiumEnabled = (enableNSFremium != null && enableNSFremium.Equals("true"));
        

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Generate the timestamp for the signature        
        /// </summary>
        /// <returns></returns>
        public virtual string GenerateTimeStamp()
        {
            // Default implementation of UNIX time of the current UTC time
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// Generate a nonce
        /// </summary>
        /// <returns></returns>
        public virtual string GenerateNonce()
        {
            byte[] rnd = new byte[4];
            rngCsp.GetBytes(rnd);
            int val = Math.Abs(BitConverter.ToInt32(rnd, 0));
            return val.ToString();
        }


        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            if (!NSFreemiumEnabled)
            {
                Global.systemLog.Error("Netsuite Freemium not enabled for Environment.");
                return;
            }

            string token = Request.QueryString["oauth_token"];
            string dc = Request.QueryString["dc"];
            string env = Request.QueryString["env"];
            string role = Request.QueryString["role"];
            string encryptedKey = Request.QueryString["enc"];
            string accountName = Request.QueryString["cn"];
            string verifyURLPath = "/app/common/integration/ssoapplistener.nl";
            string SystemDomain = "";
            string SSOVerifyUrl = "";

            Global.systemLog.Debug("--Netsuite OAuth-- NetSuite DataCenter ID: " + dc + " and System Domain: " + env + " For this netSuite account.");
            if (dc == "001")
            {
                if (env == "PRODUCTION")
                {
                    SystemDomain = "https://system.netsuite.com";
                }
                else if (env == "SANDBOX")
                {
                    SystemDomain = "https://system.sandbox.netsuite.com";
                }
                else if (env == "BETA")
                {
                    SystemDomain = "https://system.beta.netsuite.com";
                }
                else
                {
                    Global.systemLog.Error("--Netsuite Freemium OAuth-- Environment Type: " + env + " is invalid.");
                    return;
                }
            }
            else if (dc == "002")
            {
                if (env == "PRODUCTION")
                {
                    SystemDomain = "https://system.na1.netsuite.com";
                }
                else if (env == "SANDBOX")
                {
                    SystemDomain = "https://system.na1.sandbox.netsuite.com";
                }
                else if (env == "BETA")
                {
                    SystemDomain = "https://system.na1.beta.netsuite.com";
                }
                else
                {
                    Global.systemLog.Error("--Netsuite Freemium OAuth-- Environment Type: " + env + " is invalid.");
                    return;
                }
            }
            else
            {
                Global.systemLog.Error("--Netsuite Freemium OAuth-- Data Center: " + dc + " is invalid.");
                return;
            }
            SSOVerifyUrl = SystemDomain + verifyURLPath;
            Global.systemLog.Debug("--Netsuite Freemium OAuth-- NetSuite SSO URL: " + SSOVerifyUrl);

            //Global.systemLog.Info("--Netsuite OAuth-- Access Token: " + token);

            if (token == null || token.Equals(""))
            {
                Global.systemLog.Error("--Netsuite Freemium OAuth-- Access Token is invalid or missing");
                return;
            }

            var auth = "OAuth oauth_token=\"" + token + "\",oauth_consumer_key=\"" + consumerKey + "\",oauth_signature_method=\"PLAINTEXT\",oauth_signature=\"" + HttpUtility.UrlEncode(sharedSecret) + "&\",oauth_timestamp=\"" + GenerateTimeStamp() + "\",oauth_nonce=\"" + GenerateNonce() + "\"";
            //Global.systemLog.Info("--Netsuite OAuth-- Authorization Header: " + auth);

            try
            {
                WebResponse response = null;
                try
                {
                    response = generateOAuthRequest(SSOVerifyUrl, auth).GetResponse();
                }
                catch (System.Net.WebException exception)
                {
                    //If env or dc is invalid then Wrong URL will be generated
                    Global.systemLog.Info(exception.ToString());
                    Global.systemLog.Error("--Netsuite Freemium OAuth-- invalid SSO URL.");
                    return;
                }

                //Global.systemLog.Info("--Netsuite Freemium OAuth-- Response URI: " + response.ResponseUri.AbsolutePath + " headers: " + response.Headers.ToString());
                StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                String sXMLRequest = reader.ReadToEnd();
                //Global.systemLog.Info("--Netsuite Freemium OAuth-- Response Stream: " + sXMLRequest);

                XmlDocument xmlRequest = new XmlDocument();
                xmlRequest.XmlResolver = null;
                xmlRequest.LoadXml(sXMLRequest);

                var email = xmlRequest.GetElementsByTagName("ENTITYEMAIL").Item(0).InnerText;

                var accountID = xmlRequest.GetElementsByTagName("ENTITYACCOUNT").Item(0).InnerText;

                Global.systemLog.Info("--Netsuite Freemium OAuth-- Email address of the Netsuite user " + email);

                accountName = accountName + "_" + accountID + "_" + env;
                string userName = email + "_" + accountName;

                ProccessResponse(userName, email, accountName, role, accountID);
            }
            catch (System.Exception exception)
            {
                Global.systemLog.Info(exception.ToString());
                return;
            }

        }

        public HttpWebRequest generateOAuthRequest(string url, string authHeader)
        {
            HttpWebRequest request = System.Net.WebRequest.Create(url) as HttpWebRequest;
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.AllowWriteStreamBuffering = true;

            request.PreAuthenticate = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

            HttpUtility.UrlEncode(sharedSecret);
            request.Headers.Add("Authorization", authHeader);

            return request;
        }

        /// <summary>
        /// Processes a successful response.
        /// </summary>
        private void ProccessResponse(string userName, string email, string accountName, string role, string accountID)
        {
            QueryConnection conn = ConnectionPool.getConnection();
            bool adminUser = false;
            Session["username"] = userName;
            Session["email"] = email;
            Session["accountName"] = accountName;
            Session["role"] = role;
            Session["accountID"] = accountID;
            bool accountExists = false;
            if (role == "3")
            {
                adminUser = true;
                Session["adminUser"] = adminUser;
            }

            if (adminUser)
            {
                accountExists = Database.accountExists(conn, mainSchema, accountName);

                if (accountExists)
                {
                    User u = Database.getUser(conn, mainSchema, userName);
                    if (u == null)
                    {
                        Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                        Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
                    }
                    else if (u.Disabled)
                    {
                        Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                        Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
                    }
                    else
                    {
                        Guid spaceID = Database.getSpaceForUser(conn, mainSchema, u.ID);
                        if (spaceID == null || spaceID == Guid.Empty)
                        {
                            Global.systemLog.Error("Space ID does not exist for user - " + email);
                            Response.Write("Space ID does not exist for user - " + email);
                            Response.StatusCode = 400;
                            return;
                        }
                        Space sp = Database.getSpace(conn, mainSchema, spaceID);
                        if (sp == null)
                        {
                            Global.systemLog.Error("Space does not exist for user - " + email);
                            Response.Write("Space does not exist for user - " + email);
                            Response.StatusCode = 400;
                            return;
                        }
                        Session["user"] = u;
                        Session["space"] = sp;
                        Util.setSpace(Session, sp, u);
                        Response.Redirect("~/NetSuiteSSO/AdminsHomePage.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect("~/NetSuiteSSO/AdminsInitialLandingPage.aspx", false);
                }
            }
            else
            {
                accountExists = Database.accountExists(conn, mainSchema, accountName);
                if (!accountExists)
                    Response.Redirect("~/NetSuiteSSO/AccountNotConfigured.aspx", false);
                else
                {
                    User u = Database.getUser(conn, mainSchema, userName);
                    if (u == null)
                    {
                        Global.systemLog.Debug("The Birst user associated with the Netsuite SSO is not in the database or has expired products - " + email);
                        Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
                    }
                    else if (u.Disabled)
                    {
                        Global.systemLog.Debug("The Birst user associated with the Netsuite SSO has been disabled - " + email);
                        Response.Redirect("~/NetSuiteSSO/UsersInitialLandingPage.aspx", false);
                    }
                    else
                    {
                        Guid spaceID = Database.getSpaceIDForAccount(conn, mainSchema, u.ID);
                        if (spaceID == null)
                        {
                            Global.systemLog.Error("Space ID does not exist for user - " + email);
                            Response.Write("Space ID does not exist for user - " + email);
                            Response.StatusCode = 400;
                            return;
                        }
                        Space sp = Database.getSpace(conn, mainSchema, spaceID);
                        if (sp == null)
                        {
                            Global.systemLog.Error("Space does not exist for user - " + email);
                            Response.Write("Space does not exist for user - " + email);
                            Response.StatusCode = 400;
                            return;
                        }
                        Session["user"] = u;
                        Session["space"] = sp;
                        Util.setSpace(Session, sp, u);
                        Response.Redirect("~/NetSuiteSSO/UsersHomePage.aspx", false);
                    }

                }
            }
        }

        public static string getSecurityToken(QueryConnection conn, User u, Guid spaceId, string sessionVarParams, ref bool lockedout)
        {
            Global.systemLog.Info("Creating Security Token. UserName: " + u.ToString() + " Space ID: " + spaceId.ToString() + " Session vars: " + sessionVarParams);
            if ((lockedout = isLockedOut(u)) == true)
                return null;
            Space sp = null;
            if (spaceId != Guid.Empty)
            {
                sp = Util.getSpaceById(conn, mainSchema, u, spaceId);
                if (sp == null)
                {
                    Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceId + " or the space does not exist");
                    return null;
                }
            }

            if (!Global.initialized)
                return "";

            // create a token and store the parameters in the database
            SSOToken ssoToken = new SSOToken();
            ssoToken.tokenID = Guid.NewGuid();
            ssoToken.userID = u.ID;
            ssoToken.spaceID = sp != null ? sp.ID : Guid.Empty;
            ssoToken.sessionVariables = sessionVarParams;
            ssoToken.repAdmin = u.RepositoryAdmin;
            ssoToken.type = "Netsuite SSO";
            Database.createSSOToken(conn, mainSchema, ssoToken);
            return ssoToken.tokenID.ToString();

        }

        private static bool isLockedOut(User u)
        {
            MembershipUser mu = Membership.GetUser(u.Username, false);
            if (mu != null && mu.IsLockedOut)
            {
                Global.systemLog.Warn("Security token request failed - user locked out");
                return true;
            }
            return false;
        }

        //Request may contain null string value so checking it
        public static string getValueIfNullString(String Param)
        {
            if (Param != null && Param.Equals("null"))
            {
                Param = null;
            }
            return Param;
        }

        public void createAccount(string accountName)
        {
            QueryConnection conn = null;
            try
            {
                Util.validateAdminUser(Session);
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                Database.createAccount(conn, schema, accountName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown in createAccount " + ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
