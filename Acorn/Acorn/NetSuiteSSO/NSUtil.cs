﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using Acorn.Background;

namespace Acorn.NetSuiteSSO
{
    public class NSUtil
    {
        public static string connectorConnectionString = "connector:birst://netsuitejdbc:2014.1";

        public static void startExtractAndProcessData(HttpSessionState session, Space sp, User user, string[] extractGroups)
        {
            XElement currentCredentials = ConnectorUtils.getConnectorCredentials(session, user, sp, connectorConnectionString);
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.Load(currentCredentials.CreateReader());
            string sourceFilePrefix = getSourceNameFilePrefix(doc);
            saveConnectorCredentials((string)session["email"], (string)session["passwd"], (string)session["accountID"], (string)session["role"], sourceFilePrefix, user, sp, session);
            if (Util.isLoadLockFilePresent(sp))
            {
                Global.systemLog.Error("Cannot start extract : Load Lock file is present for space : " + (sp != null ? sp.ID.ToString() : ""));
                return;
            }

            if (Util.checkForPublishingStatus(sp))
            {
                Global.systemLog.Error("Cannot extract : Space loading in progress : " + (sp != null ? sp.ID.ToString() : ""));
                return;
            }

            if (Util.isSFDCExtractRunning(sp))
            {
                Global.systemLog.Error("Cannot extract : Another extraction in progress : " + (sp != null ? sp.ID.ToString() : ""));
                return;
            }
            ConnectorUtils.startExtractWithoutScheduler(session, user, sp, null, connectorConnectionString, "<XmlData><ProcessAfter>true</ProcessAfter></XmlData>", extractGroups);            
        }

        public static void cleanupConnectorCredentials(User user, Space sp, HttpSessionState session)
        {
            XElement currentCredentials = ConnectorUtils.getConnectorCredentials(session, user, sp, connectorConnectionString);
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.Load(currentCredentials.CreateReader());
            string sourceFilePrefix = getSourceNameFilePrefix(doc);
            saveConnectorCredentials("", "", "", "", sourceFilePrefix, user, sp, session);
        }

        private static void saveConnectorCredentials(string email, string password, string accountID, string roleID, string sourceFilePrefix, User user, Space sp, HttpSessionState session)
        {
            XElement credentialsPostExtraction = new XElement("Credentials",
                     new XElement("Email", email),
                     new XElement("Password", password),
                     new XElement("AccountID", accountID),
                     new XElement("RoleInternalID", roleID),
                     new XElement("EnvironmentType", "odbcserver.na1.netsuite.com:1708"),//XXX this should be set from url?
                     new XElement("SaveAuthentication", "true"),
                     new XElement("SourceFileNamePrefix", (sourceFilePrefix != null ? sourceFilePrefix : ""))
                );
            ConnectorUtils.saveConnectorCredentials(session, user, sp, "connector:birst://netsuitejdbc:2014.1", credentialsPostExtraction.ToString());
        }

        public static XElement getStatus(HttpSessionState session, Space sp, User user)
        {
            return ConnectorUtils.getExtractionStatus(session, user, sp, connectorConnectionString); 
        }

        private static string getSourceNameFilePrefix(XmlDocument xDocRoot)
        {
            foreach (XmlLinkedNode xDoc in xDocRoot.ChildNodes)
            {
                if (xDoc.Name == "ConnectorConfig")
                {
                    foreach (XmlNode xnode in xDoc.ChildNodes)
                    {
                        if (xnode.Name == "ConnectionProperties")
                        {
                            if (xnode.ChildNodes != null && xnode.ChildNodes.Count > 0)
                            {
                                XmlNode foundNode = null;
                                foreach (XmlNode connProperty in xnode.ChildNodes)
                                {
                                    if (connProperty.ChildNodes != null && connProperty.ChildNodes.Count > 0)
                                    {
                                        foreach (XmlNode connectorPropNode in connProperty.ChildNodes)
                                        {
                                            string paramName = connectorPropNode.Name;
                                            string propValue = connectorPropNode.InnerText;
                                            if (paramName == "Name" && propValue == "SourceFileNamePrefix")
                                            {
                                                foundNode = connProperty;
                                                break;
                                            }
                                        }
                                        if (foundNode != null)
                                            break;
                                    }
                                }
                                if (foundNode != null)
                                {
                                    foreach (XmlNode propParam in foundNode.ChildNodes)
                                    {
                                        if (propParam.Name == "Value")
                                        {
                                            return propParam.InnerText != null && propParam.InnerText.Length > 0 ? propParam.InnerText : null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }
    }
}