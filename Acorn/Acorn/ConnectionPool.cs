﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Threading;
using Acorn.DBConnection;
using Acorn.Utils;

namespace Acorn
{
    public class ConnectionPool
    {
        private static Dictionary<string, Dictionary<QueryConnection, DateTime>> connections = new Dictionary<string, Dictionary<QueryConnection, DateTime>>();

        public static QueryConnection getConnection()
        {
            ConnectionStringSettings cs = ConfigUtils.getConnectionString();
                /*
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings["DatabaseConnection"];
            if (cs == null)//which happens when accessing web.config from AcornTestConsole, find ConnectionString from AppSettings
                cs = new ConnectionStringSettings("DatabaseConnection",System.Web.Configuration.WebConfigurationManager.AppSettings["AdminConnectionString"]);
            if (Util.TEST_FLAG)
            {
                return (getConnection(Acorn.tests.TestUtil.TEST_CONNECT_STRING));
            }*/
            return (getConnection(cs.ConnectionString));
        }

        private static string getConnectionPoolKey(string connectString)
        {
            int index = connectString.IndexOf("Pwd=");
            string cstr = connectString;
            if (index >= 0)
            {
                int index2 = connectString.IndexOf(';', index);
                cstr = connectString.Remove(index, index2 >= 0 ? index2 - index : connectString.Length - index).Replace(";", "");
            }
            else
            {
                index = connectString.IndexOf("Password=");
                if (index >= 0)
                {
                    int index2 = connectString.IndexOf(';', index);
                    cstr = connectString.Remove(index, index2 >= 0 ? index2 - index : connectString.Length - index).Replace(";", "");
                }
            }
            return cstr;
        }

        public static QueryConnection getConnection(string connectString)
        {
            return getConnection(connectString, true);
        }

        private static Dictionary<QueryConnection, DateTime> getConnList(string cstr)
        {
            Dictionary<QueryConnection, DateTime> connList = null;
            if (connections.ContainsKey(cstr))
                connList = connections[cstr];
            else
            {
                Global.systemLog.Debug("Adding new connection pool - " + cstr);
                connList = new Dictionary<QueryConnection, DateTime>();
                connections.Add(cstr, connList);
            }
            return connList;
        }

        public static QueryConnection getConnection(string connectString, bool retryconnection)
        {
            return getConnection(connectString, retryconnection, 15);
        }

        public static QueryConnection getConnection(string connectString, bool retryconnection, int retryCount)
        {
            QueryConnection result = null;
            string cstr = getConnectionPoolKey(connectString);
            int count = 0;
            bool retry = true;
            while (retry && ++count < retryCount)
            {
                retry = retryconnection;
                lock (connections)
                {
                    Dictionary<QueryConnection, DateTime> connList = getConnList(cstr);
                    List<QueryConnection> removeList = new List<QueryConnection>();
                    // try to find an open and not in use connection
                    foreach (QueryConnection conn in connList.Keys)
                    {
                        if (connList[conn] == DateTime.MaxValue) // MaxValue means not in use
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                removeList.Add(conn);
                            }
                            else
                            {
                                connList[conn] = DateTime.Now; // mark as in use
                                result = conn;
                                break;
                            }
                        }
                    }

                    // reap the bad connections
                    foreach (QueryConnection conn in removeList)
                    {
                        try
                        {
                            conn.Close();
                            conn.Dispose();
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Info("Failed to close/dispose a bad connection from connection pool - " + cstr + " - " + ex.Message);
                        }
                        connList.Remove(conn);
                        Global.systemLog.Info("Removing bad connection from connection pool - " + cstr + " - count is " + connList.Count);
                    }

                    // found one in the pool, validate it
                    if (result != null)
                    {
                        if (!validConnection(result))
                        {
                            // bad, allocate a new one
                            removeList.Add(result);
                        }
                        else
                        {
                            return result;
                        }
                    }
                }

                // did not find one or the one that was found was not valid, allocate another and add it to the pool
                try
                {
                    result = new QueryConnection(connectString);
                    result.ConnectionTimeout = 5 * 60;
                    result.Open(); // oddly enough, open might not fail, need to validate it
                    if (validConnection(result))
                    {
                        lock (connections)
                        {
                            Dictionary<QueryConnection, DateTime> connList = getConnList(cstr);
                            connList.Add(result, DateTime.Now);
                            Global.systemLog.Debug("Growing connection pool - " + cstr + " - count is " + connList.Count);
                        }
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Failed to allocate a connection for pool - " + cstr + " - " + ex.Message, ex);
                    Exception inner = ex.InnerException;
                    if (inner != null)
                        Global.systemLog.Warn(inner.Message, inner);
                }
                // DB might be down, wait for a while
                Global.systemLog.Warn("Sleeping for 30 seconds before trying another connection in this pool - " + cstr);
                Thread.Sleep(30 * 1000);
            }
            Global.systemLog.Error("Failed to allocate a connection for pool - " + cstr);
            return null;
        }

        /**
         * make sure that the connection is still valid
         */
        private static bool validConnection(QueryConnection conn)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            bool valid = false;
            DateTime start = DateTime.UtcNow;
            string cstr = getConnectionPoolKey(conn.ConnectionString);
            try
            {
                cmd = conn.CreateCommand();
                string selQuery = "select 1";
                if (conn.isDBTypeOracle())
                    selQuery = selQuery + " from dual";
                else if (conn.isDBTypeSAPHana())
                    selQuery = selQuery + " from dummy";
                cmd.CommandText = selQuery; // same ping query as SMI
                reader = cmd.ExecuteReader();
                valid = true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Ping to DB failed - " + cstr + " - " + ex.Message);
                valid = false;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Close of ping to DB failed - " + cstr + " - " + ex.Message);
                    valid = false;
                }
            }
            if (!valid)
                return false;
            if (!conn.ConnectionString.Contains("jdbc:memdb")) // do not warn if memdb, it seems to have long ping times for some reason
            {
                TimeSpan ts = new TimeSpan(DateTime.UtcNow.Ticks - start.Ticks);
                if (ts.TotalMilliseconds > 100) // 100 is arbitrary, same value as in SMI
                {
                    Global.systemLog.Warn("Ping to database - " + cstr + " - " + ts.TotalMilliseconds + " milliseconds.");
                }
            }
            return true;
        }

        public static void releaseConnection(QueryConnection conn)
        {
            if (conn == null)
                return;
            string connectString = conn.ConnectionString;
            string cstr = connectString.Replace(";", "");
            lock (connections)
            {
                Dictionary<QueryConnection, DateTime> connList = connections[cstr];
                if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                {
                    conn.Dispose();
                    connList.Remove(conn);
                    Global.systemLog.Info("Removing bad connection from connection pool - " + getConnectionPoolKey(connectString) + " - count is " + connList.Count);
                }
                else if (conn.State == ConnectionState.Connecting || conn.State == ConnectionState.Executing || conn.State == ConnectionState.Fetching)
                {
                    conn.Close();
                    conn.Dispose();
                    connList.Remove(conn);
                    Global.systemLog.Info("Removing active (bad) connection from connection pool - " + getConnectionPoolKey(connectString) + " - count is " + connList.Count);
                }
                else
                {
                    connList[conn] = DateTime.MaxValue; // MaxValue means not in use
                }
            }
        }
    }
}
