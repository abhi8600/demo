﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Xml;
using System.Data.Odbc;
using System.Security.Cryptography;
using System.Text;
using Acorn.DBConnection;

namespace Acorn
{
    public class WebServiceSessionHelper
    {
        public class ClearableDictionary<T, K> : Dictionary<string, K>
        {
            public void clearOldItemsFromDictionary()
            {
                List<string> items = getOldItems();

                lock (this)
                {
                    foreach (string s in items)
                    {
                        Remove(s);
                    }
                }
            }

            public List<string> getOldItems()
            {
                List<string> items = new List<string>();
                foreach (string s in new List<string>(this.Keys))
                {
                    if (WebServiceSessionHelper.isValidToken(s) == false)
                        items.Add(s);
                }
                return items;
            }
        }
        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private static int LIFETIME_BETWEEN_CALLS = 10; // lifetime of token between web service calls (not session lifetime)
        public static ClearableDictionary<string, DateTime> tokens = new ClearableDictionary<string, DateTime>();
        public static ClearableDictionary<string, User> users = new ClearableDictionary<string, User>();

        public const string BIRST = "http://www.birst.com/";

        public static void Logout(string token)
        {
            if (!validToken(token))
            {
                Global.systemLog.Warn("Logging out with invalid web services token");
                throw new SoapException("Logging out with invalid web services token", new XmlQualifiedName());
            }
            if (tokens.ContainsKey(token))
            {
                tokens.Remove(token);
            }
            if (users.ContainsKey(token))
            {
                Global.systemLog.Info("Logout of webservice for user " + users[token].Username);
                users.Remove(token);
            }
        }

        public static string Login(string username, string password)
        {
            QueryConnection conn = null;
            bool hasWebServices = false;
            try
            {
                conn = ConnectionPool.getConnection();
                User u = Database.getUser(conn, mainSchema, username);
                if (u != null)
                {
                    DateTime lastLoginDate = Database.getLastLogin(u.Username);
                    Util.checkIfUserInactiveAndDisable(conn, u, lastLoginDate);
                    if (u.Disabled)
                    {
                        Global.systemLog.Warn("Failed web service login attempt for user " + username + " (disabled)");
                        warn("Login", "Failed web service login attempt for user " + username);
                    }

                    // check for allowed ips
                    string hostaddr = HttpContext.Current.Request.UserHostAddress;
                    List<string> allowedIPs = Database.getAllowedIPs(conn, mainSchema, u.ManagedAccountId, u.ID);
                    if (!Util.IsValidIP(hostaddr, allowedIPs))
                    {
                        Global.systemLog.Warn("Failed web service login attempt for user " + username + " (invalid ip adddress) - " + hostaddr);
                        warn("Login", "Failed web service login attempt for user " + username);
                    }

                    if (Membership.ValidateUser(u.Username, password))
                    {
                        // Make sure the user has WebDAV access
                        List<Product> plist = Database.getProducts(conn, mainSchema, u.ID);
                        foreach (Product p in plist)
                        {
                            if (p.Type == Product.TYPE_WEBSERVICE)
                            {
                                hasWebServices = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        MembershipUser mu = Membership.GetUser(username, false);
                        if (mu != null && mu.IsLockedOut)
                        {
                            Global.systemLog.Warn("Failed web services login attempt for user " + username + " (locked out)");
                            warn("Login", "Failed web services login attempt for user " + username);
                        }
                        else
                        {
                            Global.systemLog.Warn("Failed web services login attempt for user " + username + " (not authenticated)");
                            warn("Login", "Failed web services login attempt for user " + username);
                        }
                    }
                }

                if (!hasWebServices)
                {
                    Global.systemLog.Warn("Failed web services login attempt for user " + username + " (no webservice permissions)");
                    warn("Login", "Failed web service login request for user - " + username);
                }
                else
                {
                    string token = getToken();
                    users.Add(token, u);
                    Global.systemLog.Info("Login of webservice for user " + users[token].Username);
                    log4net.ThreadContext.Properties["user"] = u.Username;
                    using (log4net.ThreadContext.Stacks["itemid"].Push("WEB SERVICE"))
                    {
                        Global.userEventLog.Info("LOGIN");
                    }
                    Database.updateLastLogin(u.Username);
                    return token;
                }

            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return null;
        }


        public static string getToken()
        {
            Guid g = Guid.NewGuid();
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] HA1 = md5Hasher.ComputeHash(g.ToString());
            string token = Util.getHexString(HA1);
            lock (tokens)
            {
                tokens.Add(token, DateTime.Now);
            }
            return token;
        }

        public static bool validToken(string token)
        {
            if (tokens.ContainsKey(token))
            {
                DateTime lastTime = tokens[token];
                if (lastTime.AddMinutes(LIFETIME_BETWEEN_CALLS) > DateTime.Now)
                {
                    lock (tokens)
                    {
                        tokens[token] = DateTime.Now;
                    }
                    return true;
                }
            }
            return false;
        }

        public static bool isValidToken(string token)
        {
            if (tokens.ContainsKey(token))
            {
                DateTime lastTime = tokens[token];
                if (lastTime.AddMinutes(LIFETIME_BETWEEN_CALLS) > DateTime.Now)
                {
                    return true;
                }
            }
            return false;
        }

        public static User getUserForToken(string token)
        {
            if (!validToken(token))
            {
                error("getUserForToken", "Token is invalid or token has expired");
            }
            User value;
            if (users.TryGetValue(token, out value))
                return value;

            return null;
        }
        private static XmlElement getDetailErrorMessage(string message)
        {
            XmlDocument errorDoc = new XmlDocument();
            XmlElement detailElement = errorDoc.CreateElement(SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
            errorDoc.AppendChild(detailElement);
            XmlElement error = errorDoc.CreateElement("b", "error", BIRST);
            XmlAttribute nsDeclAttribute = null;
            nsDeclAttribute = errorDoc.CreateAttribute("xmlns", "b", "http://www.w3.org/2000/xmlns/");
            nsDeclAttribute.Value = BIRST;
            error.Attributes.Append(nsDeclAttribute);

            XmlElement expElement = errorDoc.CreateElement("b", "message", BIRST);
            error.AppendChild(expElement);
            expElement.InnerText = message;

            detailElement.AppendChild(error);
            return detailElement;
        }

        public static void filterNonSoapException(Exception ex, string apiName, string message)
        {
            if (ex is SoapException)
            {
                throw ex;
            }
            else
            {
                Global.systemLog.Error("Exception in calling : " + apiName, ex);
                WebServiceSessionHelper.error(apiName, message);
            }
        }

        public static void error(string prefix, string message)
        {
            if (prefix == null)
                prefix = "";
            if (message == null)
                message = "";
            try
            {
                string actor = "";
                if (HttpContext.Current != null)
                {
                    actor = HttpContext.Current.Request.Url.AbsoluteUri;
                }
                Global.systemLog.Error(prefix + ": " + message);
                throw new SoapException(prefix + ": " + message,
                    SoapException.ServerFaultCode,
                    actor,
                    getDetailErrorMessage(message));
            }
            catch (NullReferenceException)
            {
                throw new SoapException();
            }
        }
        public static void warn(string prefix, string message)
        {
            if (prefix == null)
                prefix = "";
            if (message == null)
                message = "";
            try
            {
                string actor = "";
                if (HttpContext.Current != null)
                {
                    actor = HttpContext.Current.Request.Url.AbsoluteUri;
                }
                Global.systemLog.Warn(prefix + ": " + message);
                throw new SoapException(prefix + ": " + message,
                    SoapException.ClientFaultCode,
                    actor,
                    getDetailErrorMessage(message));
            }
            catch (NullReferenceException)
            {
                throw new SoapException();
            }
        }

    }
}
