﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class ServerInfo
    {
        public string serverID;
        public string description;
        public string acornLog;
        public string smiwebLog;
        public string schedulerwebLog;
        public string connectorwebLog;
    }

    public class LogInfo
    {
        public int ID;
        public string Name;
        public string Description;
        public string LogDir;
        public string LogPrefix;
    }
}