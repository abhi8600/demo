﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using System.IO;
using System.Text;
using Acorn.TrustedService;
using Acorn.Utils;

namespace Acorn
{
    public class CompareRepository
    {
        public static AvailableRepositories getRepositoriesToCompare(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            AvailableRepositories ar = new AvailableRepositories();
            // First get saved copies of the repository
            DirectoryInfo di = new DirectoryInfo(sp.Directory);
            FileInfo[] files = di.GetFiles("repository_dev.xml.*.save");
            List<string> names = new List<string>();
            List<string> spaceNames = new List<string>();
            List<DateTime> lastModifiedDates = new List<DateTime>();
            List<string> paths = new List<string>();
            foreach (FileInfo fi in files)
            {
                string name = fi.Name;
                string loadnumber = name.Substring(19, name.Length - 24);
                name = "Saved Version From Load " + loadnumber;
                string path = fi.FullName;
                names.Add(name);
                spaceNames.Add(sp.Name);
                lastModifiedDates.Add(fi.LastWriteTime);
                paths.Add(path);
            }
            // Add other space repositories
            UserSpaces spaces = DefaultUserPage.getAllSpaces(session, null);
            List<SpaceMembership> smlist = Util.getSpaceMembershipListFromDB(u);
            foreach (SpaceSummary ss in spaces.SpacesList)
            {
                if (ss.SpaceName == sp.Name)
                    continue;
                // Make sure admin
                bool isAdmin = false;
                foreach (SpaceMembership sm in smlist)
                {
                    if (ss.SpaceID == sm.Space.ID.ToString())
                    {
                        if (sm.Administrator)
                        {
                            isAdmin = true;
                            break;
                        }
                    }
                }
                if (!isAdmin)
                    continue;
                names.Add("Current Repository");
                spaceNames.Add(ss.SpaceName);
                FileInfo fi = new FileInfo(ss.SpacePath + "\\repository_dev.xml");
                lastModifiedDates.Add(fi.LastWriteTime);
                paths.Add(ss.SpacePath + "\\repository_dev.xml");
            }
            ar.Names = names.ToArray();
            ar.SpaceNames = spaceNames.ToArray();
            ar.LastModifiedDates = lastModifiedDates.ToArray();
            ar.Paths = paths.ToArray();
            return ar;
        }

        public static RepositoryCompare compareToRepository(HttpSessionState session, string path, string[] objecttypes)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            RepositoryCompare rc = new RepositoryCompare();
            List<string> itemnames = new List<string>();
            List<string> itempaths = new List<string>();
            List<string> itemtypes = new List<string>();
            List<string[]> differences = new List<string[]>();
            MainAdminForm cmaf = new MainAdminForm();
            cmaf.setHeadless(true);
            cmaf.setLogger(Global.systemLog);
            cmaf.loadRepositoryFromFile(path); // let exceptions be thrown and shown to user (for now)
            cmaf.setupTree();

            // Compare data sources
            if (objecttypes.Contains<string>("sources"))
                compareSources(maf, cmaf, itemnames, itempaths, itemtypes, differences);

            // Compare hierarchies
            if (objecttypes.Contains<string>("hierarchies"))
                compareHierarchies(maf, cmaf, itemnames, itempaths, itemtypes, differences);

            // Compare custom attributes and measures
            if (objecttypes.Contains<string>("custom"))
                compareCustomAttributesAndMeasures(maf, cmaf, itemnames, itempaths, itemtypes, differences);

            // Compare variables
            if (objecttypes.Contains<string>("variables"))
                compareVariables(maf, cmaf, itemnames, itempaths, itemtypes, differences);

            // Compare aggregates
            if (objecttypes.Contains<string>("aggregates"))
                compareAggregates(maf, cmaf, itemnames, itempaths, itemtypes, differences);

            // Compare reports
            if (objecttypes.Contains<string>("catalog"))
            {
                if (path.IndexOf('.') >= 0)
                {
                    int index = path.LastIndexOf("\\");
                    path = path.Substring(0, index);
                }
                compareCatalog(session, sp.Directory, path, itemnames, itempaths, itemtypes, differences);
            }

            rc.ItemNames = itemnames.ToArray();
            rc.ItemPaths = itempaths.ToArray();
            rc.ItemTypes = itemtypes.ToArray();
            rc.Differences = differences.ToArray();
            return rc;
        }

        private static void compareSources(MainAdminForm maf, MainAdminForm cmaf, 
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;
            // Find sources not in the compare repository
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                StagingTable currentst = null;
                foreach (StagingTable sst in cmaf.stagingTableMod.getStagingTables())
                {
                    if (sst.Name == st.Name)
                    {
                        currentst = sst;
                        break;
                    }
                }
                if (currentst == null)
                {
                    itemnames.Add(st.getDisplayName());
                    itempaths.Add(st.Name);
                    itemtypes.Add("Data Source");
                    diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                    differences.Add(diffarr);
                }
            }
            // Go through sources in the compare repository and compare to current repository
            foreach (StagingTable st in cmaf.stagingTableMod.getStagingTables())
            {
                StagingTable currentst = null;
                foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
                {
                    if (sst.Name == st.Name)
                    {
                        currentst = sst;
                        break;
                    }
                }
                if (currentst == null)
                {
                    // List sources in compare but not in current
                    itemnames.Add(st.getDisplayName());
                    itempaths.Add(st.Name);
                    itemtypes.Add("Data Source");
                    diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                    differences.Add(diffarr);
                    continue;
                }

                if (!st.Equals(currentst))
                    currentst.getDifferences(st, itemnames, itempaths, itemtypes, differences);
            }
        }

        private static void compareHierarchies(MainAdminForm maf, MainAdminForm cmaf,
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;
            // Find hierarchies not in the compare repository
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                Hierarchy currenth = null;
                foreach (Hierarchy sh in cmaf.hmodule.getHierarchies())
                {
                    if (sh.Name == h.Name)
                    {
                        currenth = sh;
                        break;
                    }
                }
                if (currenth == null)
                {
                    itemnames.Add(h.Name);
                    itempaths.Add(h.Name);
                    itemtypes.Add("Hierarchy");
                    diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                    differences.Add(diffarr);
                }
            }
            // Go through sources in the compare repository and compare to current repository
            foreach (Hierarchy h in cmaf.hmodule.getHierarchies())
            {
                Hierarchy currenth = null;
                foreach (Hierarchy sh in maf.hmodule.getHierarchies())
                {
                    if (sh.Name == h.Name)
                    {
                        currenth = sh;
                        break;
                    }
                }
                if (currenth == null)
                {
                    // List sources in compare but not in current
                    itemnames.Add(h.Name);
                    itempaths.Add(h.Name);
                    itemtypes.Add("Hierarchy");
                    diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                    differences.Add(diffarr);
                    continue;
                }

                if (!h.Equals(currenth))
                    currenth.getDifferences(h, itemnames, itempaths, itemtypes, differences);
            }
        }

        private static void compareCustomAttributesAndMeasures(MainAdminForm maf, MainAdminForm cmaf,
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;
            // Find attributes and measures not in the compare repository
            LogicalExpression[] expressions = maf.getLogicalExpressions();
            LogicalExpression[] cexpressions = cmaf.getLogicalExpressions();
            if (expressions != null)
            {
                foreach (LogicalExpression le in expressions)
                {
                    LogicalExpression currentle = null;
                    if (cexpressions != null)
                    {
                        foreach (LogicalExpression sle in cexpressions)
                        {
                            if (sle.Name == le.Name && sle.Type == le.Type)
                            {
                                currentle = sle;
                                break;
                            }
                        }
                    }
                    if (currentle == null)
                    {
                        itemnames.Add(le.Name);
                        itempaths.Add((le.Type == LogicalExpression.TYPE_DIMENSION ? "Attributes" : "Measures") + "|" + le.Name);
                        itemtypes.Add(le.Type == LogicalExpression.TYPE_DIMENSION ? "Custom Attribute" : "Custom Measure");
                        diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                        differences.Add(diffarr);
                    }
                }
            }
            // Go through logical expressions in the compare repository and compare to current repository           
            if (cexpressions != null)
            {
                foreach (LogicalExpression le in cexpressions)
                {
                    LogicalExpression currentle = null;
                    if (expressions != null)
                    {
                        foreach (LogicalExpression sle in expressions)
                        {
                            if (sle.Name == le.Name && sle.Type == le.Type)
                            {
                                currentle = sle;
                                break;
                            }
                        }
                    }
                    if (currentle == null)
                    {
                        // List sources in compare but not in current
                        itemnames.Add(le.Name);
                        itempaths.Add((le.Type == LogicalExpression.TYPE_DIMENSION ? "Attributes" : "Measures") + "|" + le.Name);
                        itemtypes.Add(le.Type == LogicalExpression.TYPE_DIMENSION ? "Custom Attribute" : "Custom Measure");
                        diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                        differences.Add(diffarr);
                        continue;
                    }

                    if (!le.Equals(currentle))
                        currentle.getDifferences(le, itemnames, itempaths, itemtypes, differences);
                }
            }
            // Find virtual columns not in the compare repository
            VirtualColumn[] vcs = maf.getVirtualColumnList();
            VirtualColumn[] cvcs = cmaf.getVirtualColumnList();
            if (vcs != null)
            {
                foreach (VirtualColumn vc in vcs)
                {
                    VirtualColumn currentvc = null;
                    if (cvcs != null)
                    {
                        foreach (VirtualColumn svc in cvcs)
                        {
                            if (svc.Name == vc.Name && svc.Type == vc.Type)
                            {
                                currentvc = svc;
                                break;
                            }
                        }
                    }
                    if (currentvc == null)
                    {
                        itemnames.Add(vc.Name);
                        itempaths.Add((vc.Type == VirtualColumn.TYPE_BUCKETED ? "Bucketed Measure" : "Pivot") + "|" + vc.Name);
                        itemtypes.Add(vc.Type == VirtualColumn.TYPE_BUCKETED ? "Bucketed Measure" : "Pivot");
                        diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                        differences.Add(diffarr);
                    }
                }
            }
            // Go through logical expressions in the compare repository and compare to current repository
            if (cvcs != null)
            {
                foreach (VirtualColumn vc in cvcs)
                {
                    VirtualColumn currentvc = null;
                    if (vcs != null)
                    {
                        foreach (VirtualColumn svc in vcs)
                        {
                            if (svc.Name == vc.Name && svc.Type == vc.Type)
                            {
                                currentvc = svc;
                                break;
                            }
                        }
                    }
                    if (currentvc == null)
                    {
                        // List sources in compare but not in current
                        itemnames.Add(vc.Name);
                        itempaths.Add((vc.Type == VirtualColumn.TYPE_BUCKETED ? "Bucketed Measure" : "Pivot") + "|" + vc.Name);
                        itemtypes.Add(vc.Type == VirtualColumn.TYPE_BUCKETED ? "Bucketed Measure" : "Pivot");
                        diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                        differences.Add(diffarr);
                        continue;
                    }

                    if (!vc.Equals(currentvc))
                        currentvc.getDifferences(vc, itemnames, itempaths, itemtypes, differences);
                }
            }
        }

        private static void compareVariables(MainAdminForm maf, MainAdminForm cmaf,
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;
            // Find attributes and measures not in the compare repository
            List<Variable> vars = maf.getVariables();
            List<Variable> cvars = cmaf.getVariables();
            if (vars != null)
            {
                foreach (Variable var in vars)
                {
                    Variable currentvar = null;
                    if (cvars != null)
                    {
                        foreach (Variable svar in cvars)
                        {
                            if (svar.Name == var.Name)
                            {
                                currentvar = svar;
                                break;
                            }
                        }
                    }
                    if (currentvar == null)
                    {
                        itemnames.Add(var.Name);
                        itempaths.Add("Variable|" + var.Name);
                        itemtypes.Add("Variable");
                        diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                        differences.Add(diffarr);
                    }
                }
            }
            // Go through logical expressions in the compare repository and compare to current repository
            if (cvars != null)
            {
                foreach (Variable var in cvars)
                {
                    Variable currentvar = null;
                    if (vars != null)
                    {
                        foreach (Variable svar in vars)
                        {
                            if (svar.Name == var.Name)
                            {
                                currentvar = svar;
                                break;
                            }
                        }
                    }
                    if (currentvar == null)
                    {
                        // List sources in compare but not in current
                        itemnames.Add(var.Name);
                        itempaths.Add("Variable|" + var.Name);
                        itemtypes.Add("Variable");
                        diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                        differences.Add(diffarr);
                        continue;
                    }

                    if (!var.Equals(currentvar))
                        currentvar.getDifferences(var, itemnames, itempaths, itemtypes, differences);
                }
            }
        }

        private static void compareAggregates(MainAdminForm maf, MainAdminForm cmaf,
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;
            // Find attributes and measures not in the compare repository
            Aggregate[] aggs = maf.aggModule.getAggregateList();
            Aggregate[] caggs = cmaf.aggModule.getAggregateList();
            if (aggs != null)
            {
                foreach (Aggregate agg in aggs)
                {
                    Aggregate currentagg = null;
                    if (caggs != null)
                    {
                        foreach (Aggregate sagg in caggs)
                        {
                            if (sagg.Name == agg.Name)
                            {
                                currentagg = sagg;
                                break;
                            }
                        }
                    }
                    if (currentagg == null)
                    {
                        itemnames.Add(agg.Name);
                        itempaths.Add("Aggregate|" + agg.Name);
                        itemtypes.Add("Aggregate");
                        diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                        differences.Add(diffarr);
                    }
                }
            }
            // Go through logical expressions in the compare repository and compare to current repository
            if (caggs != null)
            {
                foreach (Aggregate agg in caggs)
                {
                    Aggregate currentagg = null;
                    if (aggs != null)
                    {
                        foreach (Aggregate sagg in aggs)
                        {
                            if (sagg.Name == agg.Name)
                            {
                                currentagg = sagg;
                                break;
                            }
                        }
                    }
                    if (currentagg == null)
                    {
                        // List sources in compare but not in current
                        itemnames.Add(agg.Name);
                        itempaths.Add("Aggregate|" + agg.Name);
                        itemtypes.Add("Aggregate");
                        diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                        differences.Add(diffarr);
                        continue;
                    }

                    if (!agg.Equals(currentagg))
                        currentagg.getDifferences(agg, itemnames, itempaths, itemtypes, differences);
                }
            }
        }

        private static void compareCatalog(HttpSessionState session, string spaceDirectory, string spaceDirectoryCompare,
            List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            CatalogCompareResult ccr = CompareCatalog.compareCatalogs(session, spaceDirectory, spaceDirectoryCompare);
            itemnames.AddRange(ccr.itemNames);
            itempaths.AddRange(ccr.itemPaths);
            itemtypes.AddRange(ccr.itemTypes);
            foreach (string s in ccr.differences)
            {
                string[] arr = s.Split(new char[] {'~'});
                differences.Add(arr);
            }
        }

        public static RepositoryPullResult pullFromRepository(HttpSessionState session, string repositoryPath, string[] objectPaths)
        {
            RepositoryPullResult result = new RepositoryPullResult();
            result.Success = false;
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return result;
            Space sp = (Space)session["space"];
            if (sp == null)
                return result;
            User u = (User)session["user"];
            if (u == null)
                return result;
            if (objectPaths == null || objectPaths.Length == 0)
                return result;
            if (ApplicationLoader.isSpaceBeingPublished(sp))
            {
                result.Message = "Cannot pull changes: space is currently being processed";
                result.Success = false;
                return result;
            }
            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
            {
                result.Success = false;
                result.Message = "Cannot pull changes: " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID);
                return result;
            }
            MainAdminForm cmaf = new MainAdminForm();
            cmaf.setHeadless(true);
            cmaf.setLogger(Global.systemLog);
            cmaf.loadRepositoryFromFile(repositoryPath); // let exceptions be thrown and shown to user (for now)
            cmaf.setupTree();

            maf.setInUberTransaction(true);
            List<string> catalogPaths = new List<string>();
            List<Variable> vars = maf.getVariables();
            List<Variable> cvars = cmaf.getVariables();
            bool varChanged = false;
            foreach (string objectPath in objectPaths)
            {
                string[] parts = objectPath.Split(new char[] {'|'});
                if (parts.Length <= 1)
                    continue;
                string type = parts[0];
                if (type == "Data Source" || type == "Source Column")
                {
                    string sourcename = parts[1];
                    StagingTable cst = cmaf.stagingTableMod.findTable(sourcename);
                    StagingTable st = maf.stagingTableMod.findTable(sourcename);
                    SourceFile csf = null;
                    if (cst != null && cst.SourceFile != null)
                        csf = cmaf.sourceFileMod.getSourceFile(cst.SourceFile);
                    SourceFile sf = null;
                    if (st != null && st.SourceFile != null)
                        sf = cmaf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (type == "Data Source")
                    {
                        if (cst == null)
                        {
                            if (st != null)
                            {
                                // Delete staging table
                                maf.stagingTableMod.removeStagingTable(st);
                                if (sf != null)
                                    maf.sourceFileMod.removeSourceFile(sf);
                            }
                        }
                        else
                        {
                            // Add/Replace staging table
                            maf.stagingTableMod.updateStagingTable(cst, u.Username);
                            if (csf != null)
                                maf.sourceFileMod.updateSourceFile(csf, u.Username);
                        }
                    }
                    else if (parts.Length >= 3 && type == "Source Column")
                    {
                        string columname = parts[2];
                        int index = st.getStagingColumnIndex(columname);
                        int cindex = cst.getStagingColumnIndex(columname);
                        int sfindex = index >= 0 && sf != null && st.Columns[index].SourceFileColumn != null ? sf.getColumnIndex(st.Columns[index].SourceFileColumn) : -1;
                        int csfindex = cindex >=0 && csf != null && cst.Columns[cindex].SourceFileColumn != null ? csf.getColumnIndex(cst.Columns[cindex].SourceFileColumn) : -1;
                        if (index < 0 && cindex >= 0)
                        {
                            // Add column
                            StagingColumn[] newcolumns = new StagingColumn[st.Columns.Length + 1];
                            SourceFile.SourceColumn[] newsourcecolumns = new SourceFile.SourceColumn[sf.Columns.Length + 1];
                            SourceFile.SourceColumn[] newimportsourcecolumns = new SourceFile.SourceColumn[sf.ImportColumns.Length + 1];
                            for (int i = 0; i < st.Columns.Length; i++)
                            {
                                newcolumns[i] = st.Columns[i];
                            }
                            for (int i = 0; i < sf.Columns.Length; i++)
                            {
                                newsourcecolumns[i] = sf.Columns[i];
                                newimportsourcecolumns[i] = sf.ImportColumns[i];
                            }
                            newcolumns[newcolumns.Length - 1] = cst.Columns[cindex];
                            if (csfindex >= 0)
                            {
                                newsourcecolumns[newsourcecolumns.Length - 1] = csf.Columns[csfindex];
                                newimportsourcecolumns[newimportsourcecolumns.Length - 1] = csf.ImportColumns[csfindex];
                            }
                            st.Columns = newcolumns;
                            sf.Columns = newsourcecolumns;
                            sf.ImportColumns = newimportsourcecolumns;
                        }
                        else if (index > 0)
                        {
                            if (cindex < 0)
                            {
                                // Remove column
                                List<StagingColumn> newcolumns = new List<StagingColumn>();
                                List<SourceFile.SourceColumn> newsourcecolumns = new List<SourceFile.SourceColumn>();
                                List<SourceFile.SourceColumn> newimportsourcecolumns = new List<SourceFile.SourceColumn>();
                                for (int i = 0; i < sf.Columns.Length; i++)
                                {
                                    if (i == sfindex)
                                        continue;
                                    newsourcecolumns.Add(sf.Columns[i]);
                                    newimportsourcecolumns.Add(sf.ImportColumns[i]);
                                }
                                for (int i = 0; i < st.Columns.Length; i++)
                                {
                                    if (i == index)
                                        continue;
                                    newcolumns.Add(st.Columns[i]);
                                }
                                st.Columns = newcolumns.ToArray();
                                sf.Columns = newsourcecolumns.ToArray();
                                sf.ImportColumns = newimportsourcecolumns.ToArray();
                            }
                            else
                            {
                                // Replace column
                                st.Columns[index] = cst.Columns[cindex];
                                if (sf != null && csf != null)
                                {
                                    sf.Columns[sfindex] = csf.Columns[csfindex];
                                    sf.ImportColumns[sfindex] = csf.ImportColumns[csfindex];
                                }
                            }
                        }
                    }
                }
                else if (type == "Hierarchy")
                {
                    string hierarchy = parts[1];
                    Hierarchy ch = cmaf.hmodule.getHierarchy(hierarchy);
                    if (ch != null)
                    {
                        Hierarchy h = maf.hmodule.getHierarchy(hierarchy);
                        if (h != null)
                            maf.hmodule.removeHierarchy(h);
                        maf.hmodule.updateHierarchy(ch);
                    }
                }
                else if (type == "Variable")
                {
                    string varname = parts[2];
                    int index = -1;
                    for (int i = 0; i < vars.Count; i++)
                    {
                        if (vars[i].Name == varname)
                        {
                            index = i;
                            break;
                        }
                    }
                    Variable cvar = null;
                    foreach (Variable svar in cvars)
                    {
                        if (svar.Name == varname)
                        {
                            cvar = svar;
                            break;
                        }
                    }
                    if (cvar != null)
                    {
                        if (index >= 0)
                            vars[index] = cvar;
                        else
                            vars.Add(cvar);
                        varChanged = true;
                    }
                }
                else if (type == "Aggregate")
                {
                    string aggname = parts[2];
                    Aggregate agg = maf.aggModule.findAggregate(aggname);
                    Aggregate cagg = cmaf.aggModule.findAggregate(aggname);
                    if (cagg != null)
                    {
                        if (agg != null)
                            maf.aggModule.deleteAggregate(agg);
                        maf.aggModule.updateAggregate(cagg, u.Username);
                    }
                }
                else if (type == "Custom Attribute" || type == "Custom Measure")
                {
                    LogicalExpression[] expressions = maf.getLogicalExpressions();
                    LogicalExpression[] cexpressions = cmaf.getLogicalExpressions();
                    string name = parts[2];
                    LogicalExpression currentle = null;
                    if (expressions != null)
                    {
                        foreach (LogicalExpression le in expressions)
                        {
                            if (le.Name == name && ((le.Type == LogicalExpression.TYPE_DIMENSION && type == "Custom Attribute")
                            || (le.Type == LogicalExpression.TYPE_MEASURE && type == "Custom Measure")))
                            {
                                currentle = le;
                                break;
                            }
                        }
                    }
                    LogicalExpression comparele = null;
                    if (cexpressions != null)
                    {
                        foreach (LogicalExpression le in cexpressions)
                        {
                            if (le.Name == name && ((le.Type == LogicalExpression.TYPE_DIMENSION && type == "Custom Attribute")
                            || (le.Type == LogicalExpression.TYPE_MEASURE && type == "Custom Measure")))
                            {
                                comparele = le;
                                break;
                            }
                        }
                    }
                    if (comparele != null)
                    {
                        if (currentle != null)
                            maf.deleteLogicalExpression(currentle);
                        maf.updateLogicalExpression(comparele, u.Username, comparele.Name);
                    }
                }
                else if (type == "Bucketed Measure" || type == "Pivot")
                {
                    VirtualColumn[] vcs = maf.getVirtualColumnList();
                    VirtualColumn[] cvcs = cmaf.getVirtualColumnList();
                    string name = parts[2];
                    VirtualColumn currentvc = null;
                    if (vcs != null)
                    {
                        foreach (VirtualColumn vc in vcs)
                        {
                            if (vc.Name == name && ((vc.Type == VirtualColumn.TYPE_BUCKETED && type == "Bucketed Measure")
                            || (vc.Type == VirtualColumn.TYPE_PIVOT && type == "Pivot")))
                            {
                                currentvc = vc;
                                break;
                            }
                        }
                    }
                    VirtualColumn comparevc = null;
                    if (cvcs != null)
                    {
                        foreach (VirtualColumn vc in cvcs)
                        {
                            if (vc.Name == name && ((vc.Type == VirtualColumn.TYPE_BUCKETED && type == "Bucketed Measure")
                            || (vc.Type == VirtualColumn.TYPE_PIVOT && type == "Pivot")))
                            {
                                comparevc = vc;
                                break;
                            }
                        }
                    }
                    if (comparevc != null)
                    {
                        if (currentvc != null)
                            maf.deleteVirtualColumn(currentvc);
                        maf.updateVirtualColumn(comparevc, u.Username);
                    }
                }
                else if (type == "Report" || type == "Page")
                {
                    catalogPaths.Add(parts[1]);
                }
            }
            if (catalogPaths.Count > 0)
            {
                string path = repositoryPath;
                if (path.IndexOf('.') >= 0)
                {
                    int index = path.LastIndexOf("\\");
                    path = path.Substring(0, index);
                }
                CatalogCompareResult ccr = CompareCatalog.pullFromCatalog(session, sp.Directory, path, catalogPaths.ToArray());
            }
            if (varChanged)
            {
                foreach (Variable v in vars)
                {
                    maf.updateVariable(v, u.Username);
                }
            }
            maf.setInUberTransaction(false);
            result.Success = true;
            session["AdminDirty"] = true;
            session["RequiresRebuild"] = true;
            Util.saveDirty(session);
            return result;
        }
    }

    public class RepositoryCompare
    {
        public string[] ItemNames;
        public string[] ItemPaths;
        public string[] ItemTypes;
        public string[][] Differences;
    }

    public class RepositoryPullResult
    {
        public bool Success;
        public string Message;
    }

    public class AvailableRepositories
    {
        public string[] Names;
        public string[] SpaceNames;
        public DateTime[] LastModifiedDates;
        public string[] Paths;
    }
}