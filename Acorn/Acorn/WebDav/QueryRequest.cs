﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Sockets;
using System.IO;

namespace Acorn.WebDav
{
    public class QueryRequest
    {
        private string id;
        private TcpClient client;
        private StreamReader reader;

        public QueryRequest(string id, TcpClient client, StreamReader reader)
        {
            this.id = id;
            this.client = client;
            this.reader = reader;
        }

        public char[] getRequestQuery()
        {
            string line = reader.ReadLine();
            int length = int.Parse(line);
            char[] buff = new char[length];
            int totalReadCount = 0;
            int readCount = 0;
            do
            {
                readCount = reader.Read(buff, totalReadCount, length - totalReadCount);
                if (readCount <= 0)
                {
                    break;
                }
                totalReadCount += readCount;
            }
            while (totalReadCount < length);

            if (totalReadCount != length)
            {
                Global.systemLog.Error("characters read (" + totalReadCount + ") less than the specified length (" + length + ")");
            }

            return buff;
        }

        public void returnResults(byte[] buff)
        {
            NetworkStream ns = client.GetStream();
            byte b = (byte)(buff.Length >> 24);
            ns.WriteByte(b);
            b = (byte)((buff.Length & ((1 << 24) - 1)) >> 16);
            ns.WriteByte(b);
            b = (byte)((buff.Length & ((1 << 16) - 1)) >> 8);
            ns.WriteByte(b);
            b = (byte)(buff.Length & ((1 << 8) - 1));
            ns.WriteByte(b);
            ns.Write(buff, 0, buff.Length);
            ns.Close();
            client.Close();
        }

        public bool valid()
        {
            if (!client.Connected)
            {
                reader.Close();
                client.Close();
                return false;
            }
            return true;
        }
    }
}
