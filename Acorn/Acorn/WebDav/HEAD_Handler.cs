﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Microsoft.Win32;

namespace Acorn
{
    public class HEAD_Handler : IMethodHandler
    {
        private HttpApplication httpApplication;
        private string requestPath;

        private Space sp;
        private User u;

        public HEAD_Handler(HttpApplication app, User u, Space sp)
        {
            this.httpApplication = app;
            this.sp = sp;
            this.u = u;
        }

        #region IMethod_Handler Interface
        public string ResponseXml
        {
            get
            {
                return "";
            }
        }
        public string RequestXml
        {
            get
            {
                return "";
            }
        }
        public string ErrorXml
        {
            get
            {
                return "";
            }
        }

        public int Handle()
        {
            if (httpApplication == null)
                return (int)DavModule.ServerResponseCode.BadRequest;

            this.requestPath = DavModule.getRelativePath(this.httpApplication, this.httpApplication.Request.FilePath);

            if (!Directory.Exists(sp.Directory + "\\data"))
                return (int)DavModule.ServerResponseCode.BadRequest;

            string filename = this.requestPath;
            int folderSeparator = filename.IndexOf('/');
            if (folderSeparator >= 0)
                filename = filename.Substring(folderSeparator + 1);
            FileInfo fi = new FileInfo(sp.Directory + "\\data\\" + filename);
            if (!fi.Exists)
            {
                FileInfo ufi = new FileInfo(sp.Directory + "\\data\\" + filename + ".uploaded");
                if (!ufi.Exists)
                    return (int)DavModule.ServerResponseCode.NotFound;
            }
            return (int)DavModule.ServerResponseCode.Ok;
        }
        #endregion
    }
}
