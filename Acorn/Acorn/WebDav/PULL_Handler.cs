﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Zip;
using Acorn.Utils;

namespace Acorn
{
    public class PULL_Handler : IMethodHandler
    {
        private HttpApplication httpApplication;
        private Space sp;
        private User u;
        private string responseString = "";
        private string requestPath;
        private static long SEGMENT_SIZE = 50 * 200 * 1024; // 10MB
        private static int CHUNK_SIZE = 100 * 1024;
        
        public PULL_Handler(HttpApplication app, User u, Space sp)
        {
            this.httpApplication = app;
            this.sp = sp;
            this.u = u;
        }

        public string ResponseXml
        {
            get
            {
                return responseString;
            }
        }

        public string RequestXml
        {
            get
            {
                return "";
            }
        }

        public string ErrorXml
        {
            get
            {
                return "";
            }
        }

        public int Handle()
        {
            if (httpApplication == null)
            {
                Global.systemLog.Debug("httpApplication is null");
                return (int)DavModule.ServerResponseCode.BadRequest;
            }

            //if (DavModule.getRequestLength(httpApplication) == 0)
                //return (int)PUT_Handler.DavPutResponseCode.Created;

            this.requestPath = DavModule.getRelativePath(this.httpApplication, this.httpApplication.Request.FilePath);

            string filename = this.requestPath;
            int folderSeparator = filename.IndexOf('/');
            string lname = filename.ToLower();
            if (folderSeparator >= 0)
            {
                lname = lname.Substring(folderSeparator + 1);
                filename = filename.Substring(folderSeparator + 1);
            }
            if (lname.StartsWith("pullclouddata/"))
            {
                lname = lname.Substring("pullclouddata/".Length);
                filename = filename.Substring("pullclouddata/".Length);
            }

            if (lname.StartsWith("birst.downloadclouddata") || lname.StartsWith("birst.downloadbulkqueryresults")
                   || lname.StartsWith("birst.downloadmigrationdata"))
            {
                string requestMode = httpApplication.Request.Headers["RequestMode"].ToString();
                string localConnection = null;
                string localLoadGroup = null;
                if (lname.StartsWith("birst.downloadclouddata"))
                {
                    localConnection = httpApplication.Request.Headers["LocalConnection"].ToString();
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    foreach (LocalETLConfig config in leConfigs.Values)
                    {
                        if (config.getLocalETLConnection().Equals(localConnection))
                        {
                            localLoadGroup = config.getLocalETLLoadGroup();
                            break;
                        }
                    }
                }
                string source = null;
                int result = (int)DavModule.ServerResponseCode.None;
                switch (requestMode)
                {
                    case "DiscoverMetaData":
                        {
                            if (!lname.StartsWith("birst.downloadclouddata"))
                            {
                                responseString = "Error: Invalid RequestMode : " + requestMode;
                                result = -1;
                                break;
                            }
                            MainAdminForm maf = Util.getRepositoryDev(sp, u);
                            List<string> sFiles = new List<string>();
                            bool retriveAllLocalSources = bool.Parse(httpApplication.Request.Headers["retriveAllLocalSources"].ToString());                                
                            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                            {
                                if (st.LoadGroups.Contains(localLoadGroup))
                                {
                                    if (!retriveAllLocalSources && (!st.UseBirstLocalForSource || st.PreviousLoadGroup == null))
                                        continue;
                                    string fname = maf.getSourceFile(st).FileName;
                                    if (File.Exists(sp.Directory + "\\data\\" + fname))
                                        sFiles.Add(maf.getSourceFile(st).FileName);
                                    else
                                        Global.systemLog.Error("Sourcefile " + fname + " does not exist, cannot download for local processing.");
                                }
                            }
                            StringBuilder sb = new StringBuilder();
                            if (sFiles.Count > 0)
                            {
                                sb.Append("CLOUD_SOURCES_FOR_DOWNLOAD_FOUND=").Append("true").AppendLine();
                                sb.Append("NO_OF_SOURCES=").Append(sFiles.Count).AppendLine();
                                sb.Append("SOURCES=");
                                bool first = true;
                                foreach (string fname in sFiles)
                                {
                                    if (!first)
                                        sb.Append("/");
                                    sb.Append(fname);
                                    first = false;
                                }
                                sb.AppendLine();
                            }
                            else
                            {
                                sb.Append("CLOUD_SOURCES_FOR_DOWNLOAD_FOUND=").Append("false").AppendLine();
                            }
                            responseString = sb.ToString();
                            break;
                        }
                    case "GetSourceMetaData":
                        {
                            source = httpApplication.Request.Headers["Source"].ToString();
                            string sourceFullPath = null;
                            if (lname.StartsWith("birst.downloadclouddata"))
                                sourceFullPath = sp.Directory + "\\data\\" + source;
                            else if (lname.StartsWith("birst.downloadbulkqueryresults"))
                                sourceFullPath = sp.Directory + "\\output\\" + source;
                            else if (lname.StartsWith("birst.downloadmigrationdata"))
                                sourceFullPath = sp.Directory + "\\data\\migrate\\" + source;
                            if (source == null || !File.Exists(sourceFullPath))
                            {
                                responseString = "Error: File " + sourceFullPath + " does not exist, cannot download.";
                                Global.systemLog.Error("File " + sourceFullPath + " does not exist, cannot download.");
                                result = -1;
                                break;
                            }
                            getSourceMetaData(sp, sourceFullPath);
                            break;
                        }
                    case "DownloadData":
                        {
                            source = httpApplication.Request.Headers["Source"].ToString();
                            string sourceFullPath = null;
                            if (lname.StartsWith("birst.downloadclouddata"))
                                sourceFullPath = sp.Directory + "\\data\\" + source;
                            else if (lname.StartsWith("birst.downloadbulkqueryresults"))
                                sourceFullPath = sp.Directory + "\\output\\" + source;
                            else if (lname.StartsWith("birst.downloadmigrationdata"))
                                sourceFullPath = sp.Directory + "\\data\\migrate\\" + source;
                            int segment;
                            if (!int.TryParse(httpApplication.Request.Headers["Segment"].ToString(), out segment))
                            {
                                responseString = "Error: Invalid Segment No. : " + httpApplication.Request.Headers["Segment"].ToString();
                                Global.systemLog.Error("Invalid Segment No. : " + httpApplication.Request.Headers["Segment"].ToString() + " found " + source + ", cannot download for local processing.");
                                result = -1;
                                break;
                            }
                            writeSegmentToStream(sp, sourceFullPath, segment);
                            break;
                        }
                    default:
                        {
                            responseString = "Error: Invalid RequestMode : " + requestMode;
                            result = -1;
                            break;
                        }
                }
                if (result != (int)DavModule.ServerResponseCode.None)
                    return result;
            }
            else
            {
                return (int)DavModule.ServerResponseCode.BadRequest;
            }

            return (int)Acorn.DavModule.ServerResponseCode.Ok;
        }

        private string getSourceMetaData(Space sp, string source)
        {
            Util.validateFileLocation(sp.Directory, source);
            FileInfo fi = new FileInfo(source);
            int numSegments = (int)Math.Ceiling((double)fi.Length / SEGMENT_SIZE);
            responseString = "SEGMENTS=" + numSegments;
            return responseString;
        }

        private void writeSegmentToStream(Space sp, string source, int segment)
        {
            Util.validateFileLocation(sp.Directory, source);
            FileInfo fi = new FileInfo(source);
            // determine if a full segment or a final partial segment (or just a small file)
            long remainder = fi.Length - ((segment - 1) * SEGMENT_SIZE);
            long length = SEGMENT_SIZE;
            if (remainder < 0)
                length = Math.Min(fi.Length, length);
            else
                length = Math.Min(remainder, length);
            // read the segment into memory (in order to calculate the hash)
            FileStream fis = null;
            byte[] data = null;
            try
            {
                fis = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                if (segment > 0)
                    fis.Seek(SEGMENT_SIZE * (segment - 1), SeekOrigin.Begin);// skip to the correct segment within the file
                int pos = 0;
                int chunk = CHUNK_SIZE;
                data = new byte[(int)length];
                byte[] buf = new byte[chunk];
                do
                {
                    int len = fis.Read(buf, 0, chunk);
                    Array.Copy(buf, 0, data, pos, len);
                    pos += len;
                } while (pos < length);

                // calculate SHA1 (backwards compatibility) and SHA256 (new) hash value for the segment
                string serverHash1 = null;
                SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
                serverHash1 = BitConverter.ToString(sha1.ComputeHash(data, 0, pos)).Replace("-", "").ToLower();

                string serverHash256 = null;
                HashAlgorithm sha256 = null;
                try
                {
                    sha256 = new SHA256CryptoServiceProvider();
                }
                catch (PlatformNotSupportedException)
                {
                    sha256 = new SHA256Managed();
                }
                serverHash256 = BitConverter.ToString(sha256.ComputeHash(data, 0, pos)).Replace("-", "").ToLower();

                // compress bigger data using gzip
                byte[] zippedData = null;
                using (MemoryStream mem = new MemoryStream())
                {
                    GZipOutputStream zos = new GZipOutputStream(mem);
                    zos.Write(data, 0, pos);
                    zos.Finish();
                    zippedData = mem.ToArray();
                    zos.Close();
                }
                httpApplication.Response.AddHeader("Hash", Util.removeCRLF(serverHash1));
                httpApplication.Response.AddHeader("Hash256", Util.removeCRLF(serverHash256));
                httpApplication.Response.AddHeader("Content-Length", zippedData.Length.ToString());
                httpApplication.Response.AddHeader("ContentEncoding", "gzip");                
                httpApplication.Response.OutputStream.Write(zippedData, 0, zippedData.Length);
                httpApplication.Response.OutputStream.Flush();
            }
            finally
            {
                if (fis != null)
                    fis.Close();
            }
        }
    }
}
