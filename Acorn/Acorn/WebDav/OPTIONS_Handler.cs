﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class OPTIONS_Handler : IMethodHandler
    {
        private HttpApplication app;

        public OPTIONS_Handler(HttpApplication app)
        {
            this.app = app;
        }
        #region IMethod_Handler Interface
        public string ResponseXml
        {
            get
            {
                return "";
            }
        }
        public string RequestXml
        {
            get
            {
                return "";
            }
        }
        public string ErrorXml
        {
            get
            {
                return "";
            }
        }
        public int Handle()
        {
            if (app == null)
                return (int)DavModule.ServerResponseCode.BadRequest;
            app.Response.AppendHeader("DAV", "1,2");
            app.Response.AppendHeader("MS-Author-Via", "DAV");
            app.Response.AppendHeader("Versioning-Support", "DAV:basicversioning");
            app.Response.AppendHeader("DASL", "<DAV:sql>");
            app.Response.AppendHeader("Public", "OPTIONS, GET, HEAD, PROPFIND, PUT");
            app.Response.AppendHeader("Allow",  "OPTIONS, GET, HEAD, PROPFIND, PUT");
            return (int)DavModule.ServerResponseCode.Ok;
        }
        #endregion
    }
}
