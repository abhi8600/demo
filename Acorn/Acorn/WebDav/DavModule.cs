﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.Utils;

namespace Acorn
{
    using System;
    using System.Web;
    using System.IO;
    using System.Text;
    using System.Data.Odbc;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Web.SessionState;
    using Acorn.LRUCache;
    using Acorn.DBConnection;

    public class DavModule : IHttpModule
    {
        private static bool DIGEST_AUTHENTICATION = true;
        private static SymmetricAlgorithm sa;
        private HttpApplication context = null;
        private static string REALM = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AuthenticationRealm"];
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string birstConnectVersion = System.Web.Configuration.WebConfigurationManager.AppSettings["birstConnectProtocolVersion"];

        private static LruCache nonceCache = new LruCache(200);

        public enum ServerResponseCode : int
        {
            /// <summary>
            /// 0: None
            /// </summary>
            /// <remarks>
            ///		Default enumerator value
            /// </remarks>
            None = 0,
            /// <summary>
            ///	200: Ok 
            /// </summary>
            Ok = 200,
            /// <summary>
            /// 207: Multi Status
            /// </summary>
            /// <remarks>Used by PropFind</remarks>
            MultiStatus = 207,
            /// <summary>
            /// 400: Bad Request
            /// </summary>
            BadRequest = 400,
            /// <summary>
            /// 401: Not authorized
            /// </summary>
            Unauthorized = 401,
            /// <summary>
            /// 404: Not Found
            /// </summary>
            NotFound = 404,
            /// <summary>
            /// 412: Precondition Failed
            /// </summary>
            /// <remarks></remarks>
            PreconditionFailed = 412,
            /// <summary>
            /// 501: Method Not Implemented
            /// </summary>
            MethodNotImplemented = 501,
            /// <remarks>
            ///		Currently processing another load
            /// </remarks>
            CurrentlyProcessing = 600,
            /// <summary>
            /// Used when space is busy in other operations - Swapping, Deleting, Copying
            /// </summary>
            SpaceNotAvailable=601,
            /// <summary>
            /// 999: End connection
            /// </summary>
            EndConnection = 999            
        }

        public enum PropertyRequestType
        {
            /// <summary>
            /// No properties requested
            /// </summary>
            None,
            /// <summary>
            /// Specific properties requested
            /// <seealso cref="DavResourceFindBase.RequestProperties"/>
            /// </summary>
            NamedProperties,
            /// <summary>
            /// A summary of all the available properties
            /// </summary>
            PropertyNames,
            /// <summary>
            /// All properties requested
            /// </summary>
            AllProperties
        }

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            try
            {
                // Global.systemLog.Info("Initializing Birst WebDAV Module");
                this.context = context;
                this.context.BeginRequest += new EventHandler(context_BeginRequest);
                if (sa == null)
                {
                    DESCryptoServiceProvider dsp = new DESCryptoServiceProvider();
                    dsp.GenerateKey();
                    dsp.GenerateIV();
                    sa = dsp;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex, ex);
            }
        }

        private void sendAuthenticationResponse(HttpApplication app, string realm)
        {
            if (!DIGEST_AUTHENTICATION)
            {
                // Basic authentication
                app.Response.AddHeader("WWW-Authenticate", "Basic realm=\"" + Util.removeCRLF(realm) + "\"");
            }
            else
            {
                try
                {
                    ICryptoTransform crypt = sa.CreateEncryptor();
                    byte[] noncebytes = new byte[40];
                    char[] toencode = DateTime.Now.ToString("s").ToCharArray();
                    Encoding.ASCII.GetBytes(toencode, 0, Math.Min(40, toencode.Length), noncebytes, 0);
                    byte[] encryptednonce = new byte[40];
                    crypt.TransformBlock(noncebytes, 0, noncebytes.Length, encryptednonce, 0);
                    string nonce = Util.getHexString(encryptednonce);
                    Guid opaqueg = Guid.NewGuid();
                    string opaque = opaqueg.ToString();
                    app.Response.AddHeader("WWW-Authenticate", "Digest realm=\"" + Util.removeCRLF(realm)
                        + "\",qop=\"auth,auth-int\",nonce=\"" + Util.removeCRLF(nonce) + "\",opaque=\"" + Util.removeCRLF(opaque) + "\"");
                }
                catch (Exception encex)
                {
                    Global.systemLog.Debug("Exception in formulating WebDAV authentication response: " + encex.ToString());
                }
            }
            app.Response.ContentType = "text/html";
            app.Response.StatusCode = (int)ServerResponseCode.Unauthorized;
            app.Response.ClearContent();
            app.Response.Write(getHtmlErrorMsg("Requires authentication"));
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        private bool isValidNonce(string nonce)
        {
            ICryptoTransform decrypt = sa.CreateDecryptor();
            byte[] noncebytes = new byte[40];
            byte[] encryptednonce = Util.getBytesFromHex(nonce);
            decrypt.TransformBlock(encryptednonce, 0, encryptednonce.Length, noncebytes, 0);
            string noncestr = Encoding.ASCII.GetString(noncebytes);
            int index = noncestr.IndexOf((char)0);
            try
            {
                DateTime noncetime = DateTime.Parse(noncestr.Substring(0, index));
                // Nonce is only valid for 12 hours (to make sure full uploads work)
                TimeSpan ts = DateTime.Now - noncetime;
                if (ts.TotalHours > 12)
                {
                    Global.systemLog.Warn("isValidNonce: nonce timed out: " + ts.TotalMinutes);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("isValidNonce: exception in nonce processing: " + ex.Message);
                return false;
            }
            return true;
        }

        private User authenticate(HttpApplication app, out Space sp)
        {
            bool authenticated = false;
            sp = null;
            string realm = REALM;
            if (app.Request.Headers["Authorization"] == null)
            {
                sendAuthenticationResponse(app, realm);
                return null;
            }
            else
            {
                QueryConnection conn = null;
                string username = null;
                string password = null;
                string authorization = app.Request.Headers["Authorization"];
                User u = null;
                string spacename = DavModule.getRelativePath(app, app.Request.FilePath);
                int folderSeparator = spacename.IndexOf('/');
                if (folderSeparator >= 0)
                    spacename = spacename.Substring(0, folderSeparator);
                if (!DIGEST_AUTHENTICATION)
                {
                    if (!authorization.StartsWith("Basic"))
                    {
                        Global.systemLog.Warn("WEBDAV authorization header not Basic - " + authorization);
                        sendAuthenticationResponse(app, realm);
                        return null;
                    }
                    byte[] tempConverted = Convert.FromBase64String(authorization.Substring(6));
                    string userInfo = new ASCIIEncoding().GetString(tempConverted);
                    string[] usernamePassword = userInfo.Split(new char[] { ':' });
                    username = usernamePassword[0];
                    password = usernamePassword[1];
                    authenticated = Membership.ValidateUser(username, password);
                    if (authenticated)
                    {
                        Global.systemLog.Info("WEBDAV Basic authentication request succeeded for " + username);
                    }
                    else
                    {
                        Global.systemLog.Warn("WEBDAV Basic authentication request failed for " + username);
                        writeErrorResponse(app, 4, "Basic authentication failedt - " + username);
                        throw new Exception("Basic authentication failed - " + username);
                    }
                }
                else
                {
                    if (!authorization.StartsWith("Digest"))
                    {
                        Global.systemLog.Warn("WEBDAV authorization header not Digest - " + authorization);
                        sendAuthenticationResponse(app, realm);
                        return null;
                    }
                    List<string> pieces = new List<string>();
                    StringBuilder sb = null;
                    bool quote = false;
                    for (int i = 7; i < authorization.Length; i++)
                    {
                        if (sb == null)
                        {
                            sb = new StringBuilder();
                            sb.Append(authorization[i]);
                        }
                        else
                        {
                            if (authorization[i] == '"')
                                quote = !quote;
                            else
                            {
                                if (!quote && authorization[i] == ',')
                                {
                                    pieces.Add(sb.ToString());
                                    sb = null;
                                }
                                else
                                    sb.Append(authorization[i]);
                            }
                        }
                    }
                    if (sb.Length > 0)
                        pieces.Add(sb.ToString());
                    string c_username = null;
                    string c_realm = null;
                    string c_nonce = null;
                    string c_cnonce = null;
                    string c_uri = null;
                    string c_nc = null;
                    string c_response = null;
                    string c_auth = "auth,auth-int";
                    foreach (string ss in pieces)
                    {
                        string s = ss.Trim();
                        if (s.StartsWith("username="))
                            c_username = s.Substring(9, s.Length - 9);
                        else if (s.StartsWith("realm="))
                            c_realm = s.Substring(6, s.Length - 6);
                        else if (s.StartsWith("nonce="))
                            c_nonce = s.Substring(6, s.Length - 6);
                        else if (s.StartsWith("cnonce="))
                            c_cnonce = s.Substring(7, s.Length - 7);
                        else if (s.StartsWith("uri="))
                            c_uri = s.Substring(4, s.Length - 4);
                        else if (s.StartsWith("qop=") && s.Length > 4)
                            c_auth = s.Substring(4, s.Length - 4);
                        else if (s.StartsWith("nc="))
                            c_nc = s.Substring(3, s.Length - 3);
                        else if (s.StartsWith("response="))
                            c_response = s.Substring(9, s.Length - 9);
                    }
                    // Make sure the nonce is still valid
                    bool validNonce = isValidNonce(c_nonce);
                    if (!validNonce)
                    {
                        Global.systemLog.Warn("WEBDAV nonce is not valid - " + c_nonce + " for user " + c_username);
                        sendAuthenticationResponse(app, realm);
                        return null;
                    }
                    bool hasWebDAV = true;
                    object o;
                    string key = c_username + spacename + c_nonce;
                    nonceCache.TryGetValue(key, out o);
                    u = (User) o;
                    if (u == null)
                    {
                        List<Product> plist = null;
                        hasWebDAV = false;
                        try
                        {
                            conn = ConnectionPool.getConnection();
                            u = Database.getUser(conn, mainSchema, c_username);
                            if (u == null)
                            {
                                Global.systemLog.Warn("WEBDAV Can not find the user - " + c_username);
                                writeErrorResponse(app, 4, "Can not find the user - " + c_username);
                                throw new Exception("Can not find the user - " + c_username);
                            }
                            // Make sure the user has WebDAV access
                            plist = Database.getProducts(conn, mainSchema, u.ID);
                            foreach (Product p in plist)
                            {
                                if (p.Type == Product.TYPE_WEBDAV)
                                {
                                    hasWebDAV = true;
                                    break;
                                }
                            }
                        }
                        finally
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                        nonceCache[key] = u;
                    }
                    if (!hasWebDAV)
                    {
                        Global.systemLog.Warn("Account does not have Birst Connect privileges - " + c_username);
                        writeErrorResponse(app, 4, "Account does not have Birst Connect privileges - " + c_username);
                        throw new Exception("Account does not have Birst Connect privileges - " + c_username);
                    }
                    // check for allowed ips
                    string hostaddr = app.Request.UserHostAddress;
                    List<string> allowedIPs = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        allowedIPs = Database.getAllowedIPs(conn, mainSchema, u.ManagedAccountId, u.ID);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    if (!Util.IsValidIP(hostaddr, allowedIPs))
                    {
                        Global.systemLog.Warn("User is not allowed to access from this IP - " + c_username + ", " + hostaddr);
                        writeErrorResponse(app, 4, "User is not allowed to access from this IP - " + c_username + ", " + hostaddr);
                        throw new Exception("User is not allowed to access from this IP - " + c_username + ", " + hostaddr);
                    }
                    MembershipUser mu = Membership.GetUser(c_username, false);
                    if (mu == null)
                    {
                        Global.systemLog.Warn("User does not exist - " + c_username);
                        writeErrorResponse(app, 4, "User does not exist - " + c_username);
                        throw new Exception("User does not exist - " + c_username);
                    }
                    if (mu.IsLockedOut)
                    {
                        Global.systemLog.Warn("User is locked out - " + c_username);
                        writeErrorResponse(app, 4, "User is locked out - " + c_username);
                        throw new Exception("User is locked out - " + c_username);
                    }
                    MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
                    byte[] HA1 = md5Hasher.ComputeHash(c_username + ":" + c_realm + ":" + u.Password);
                    byte[] HA2 = md5Hasher.ComputeHash(app.Request.HttpMethod + ":" + c_uri);
                    string tohash = Util.getHexString(HA1) + ":" + c_nonce + ":" + Util.getHexString(HA2);
                    byte[] response = md5Hasher.ComputeHash(tohash);
                    string responsestr = Util.getHexString(response);
                    if (responsestr == c_response)
                    {
                        authenticated = true;
                        username = c_username;
                    }
                    else
                    {
                        // try pre-4.3 style (not standards/Java authenticator compliant)
                        HA2 = md5Hasher.ComputeHash(app.Request.HttpMethod + ":" + c_uri);
                        tohash = u.Password + ":" + c_nonce + ":" + c_nc + ":" + c_cnonce + ":" + c_auth + ":" + Util.getHexString(HA2);
                        response = md5Hasher.ComputeHash(tohash);
                        responsestr = Util.getHexString(response);
                        if (responsestr == c_response)
                        {
                            authenticated = true;
                            username = c_username;
                        }
                        else
                        {
                            Membership.ValidateUser(c_username, "1234"); // force an increment to lock out
                            Global.systemLog.Warn("WEBDAV digest authentication request failed for " + c_username);
                            writeErrorResponse(app, 4, "Digest authentication request failed for " + c_username);
                            throw new Exception("Digest authentication request failed for " + c_username);
                        }
                    }
                }
                if (authenticated)
                {
                    List<SpaceMembership> mlist = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        if (u == null)
                        {
                            u = Database.getUser(conn, mainSchema, username);
                            // verify that they are not expired
                            List<Product> plist = Database.getProducts(conn, mainSchema, u.ID);
                            bool hasEdition = false;
                            foreach (Product p in plist)
                            {
                                if (p.Type == Product.TYPE_EDITION)
                                {
                                    hasEdition = true;
                                    break;
                                }
                            }
                            if (!hasEdition)
                            {
                                Global.systemLog.Warn("WEBDAV digest authentication request failed for " + username + " (expired account - no products of type 0)");
                                writeErrorResponse(app, 3, "Expired Account");
                                throw new Exception("Expired Account - " + username);
                            }
                        }
                        mlist = Database.getSpaces(conn, mainSchema, u, true);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }

                    // backwards compatibility
                    Guid spaceId = Guid.Empty;
                    try
                    {
                        spaceId = new Guid(spacename);
                    }
                    catch (Exception)
                    {
                        // space name is not a GUID, old style Birst Connect space identifier (name)
                        spaceId = Guid.Empty;
                    }

                    foreach (SpaceMembership sm in mlist)
                    {
                        // backwards compatibility (check for name if spaceId is empty)
                        if (spaceId == Guid.Empty && sm.Space.Name == spacename)
                        {
                            sp = sm.Space;
                            break;
                        }
                        // if spaceId is not empty, it's the new style
                        if (spaceId != Guid.Empty && sm.Space.ID == spaceId)
                        {
                            sp = sm.Space;
                            break;
                        }
                    }
                    if (sp == null)
                    {
                        Global.systemLog.Warn("WEBDAV Space not found - " + spacename + ", for user " + username);
                        writeErrorResponse(app, 7, "Space not found - " + spacename + ", for user " + username);
                        throw new Exception("Space not found - " + spacename + ", for user " + username);
                    }

                    Util.setLogging(u, sp);
                    /*
                    log4net.ThreadContext.Properties["user"] = u.Username; 
                    using (log4net.ThreadContext.Stacks["itemid"].Push("BIRST CONNECT"))
                    {
                        Global.userEventLog.Info("LOGIN");
                    }
                    */
                }
                else
                {
                    Global.systemLog.Warn("WEBDAV Unable to authenticate user");
                    writeErrorResponse(app, 3, "Unable to authenticate user");
                    throw new Exception("Unable to authenticate user");
                }
                return u;
            }
        }

        private void writeErrorResponse(HttpApplication app, int code, string message)
        {
            app.Response.Write("<html><body>Error " + String.Format("{0:000}", code) + ": " + HttpUtility.HtmlEncode(message) + "</body></html>\n");
            Global.systemLog.Warn("WEBDAV Error " + String.Format("{0:000}", code) + ": " + message);
        }

        /**
         * for testing out Birst Connect error handling
         */
        private void writeHttpError(HttpApplication app, int code, string message)
        {
            app.Response.ClearContent();
            app.Response.StatusCode = code;
            app.Response.Write(message);
            app.CompleteRequest();
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            //Set the status code to NotImplemented by default
            int statusCode = 501;
            string responseXml = "";
            string requestXml = "";
            string ErrorXml = "";
            string httpMethod = "";
            IMethodHandler MethodHandler = null;
            try
            {
                //Don't handle anything with the _MWDRes query string, this means that this is a request
                //for a local resource and should not be handled by this Handler. It should fall through
                //to the asp .net engine to handle.
                if (app.Request.QueryString["_MWDRes"] != null)
                    return;

                bool ignoreSpaceInUseCheck = false;
                if (app.Request.Headers["IgnoreSpaceInUseCheck"] != null)
                    Boolean.TryParse(app.Request.Headers["IgnoreSpaceInUseCheck"], out ignoreSpaceInUseCheck);

                string userAgent = app.Request.Headers["User-Agent"];
                //Make sure we don't handle anything from the IDE.
                if (userAgent != null && !userAgent.StartsWith("Microsoft-Visual-Studio.NET"))
                {
                    // validate version
                    if (userAgent.StartsWith("Birst-WebDAV"))
                    {
                        if (birstConnectVersion != null)
                        {
                            // get the version
                            int idx = userAgent.IndexOf("/");
                            if (idx > 0)
                            {
                                string ver = userAgent.Substring(idx + 1);
                                if (!birstConnectVersion.Equals(ver))
                                {
                                    Global.systemLog.Warn("Outdated version of Birst Connect being used, client version is " + ver + ", server version is " + birstConnectVersion + ", Hostname: " + app.Request.UserHostName + ", IP Address: " + app.Request.UserHostAddress);
                                    app.Response.ClearContent();
                                    app.Response.StatusCode = 412;
                                    app.Response.Write("Version mismatch. Expected Birst-WebDAV/" + HttpUtility.HtmlEncode(birstConnectVersion) + ", client is " + HttpUtility.HtmlEncode(userAgent) + ".  Please contact Support to upgrade.");
                                    app.CompleteRequest();
                                    return;
                                }
                            }
                            else
                            {
                                Global.systemLog.Warn("Birst Connect User-Agent did not have a version: " + userAgent);
                            }
                        }
                        else
                        {
                            Global.systemLog.Warn("web.config does not have a value for the key 'birstConnectProtocolVersion'");
                        }
                    }
                    else if (userAgent.StartsWith("Birst DAV"))
                    {
                        Global.systemLog.Warn("Outdated version of Birst Connect being used, client version is old Birst DAV user agent, Hostname: " + app.Request.UserHostName + ", IP Address: " + app.Request.UserHostAddress);
                        app.Response.ClearContent();
                        app.Response.StatusCode = 412;
                        app.Response.Write("Version mismatch. Expected Birst-WebDAV/<Version>, client is " + HttpUtility.HtmlEncode(userAgent) + ".  Please contact Support to upgrade.");
                        app.CompleteRequest();
                        return;
                    }
                }
                httpMethod = app.Request.HttpMethod.ToUpper();

                // wait until the server is properly up and running
                if (!Global.initialized)
                {
                    return;
                }

                switch (httpMethod)
                {
                    case "POST":
                        Space sp = null;
                        User u = null;
                        if (app.Request.FilePath.IndexOf("/BirstConnect") >= 0)
                        {
                            sp = null;
                            u = authenticate(app, out sp);
                            if (u == null)
                            {
                                app.CompleteRequest();
                                return;
                            }

                            if (!ignoreSpaceInUseCheck && !allowSpaceOperations(sp))
                            {
                                app.Response.StatusCode = (int)ServerResponseCode.SpaceNotAvailable;
                                app.CompleteRequest();
                                return;
                            }

                            if (app.Request.FilePath.IndexOf("/PullCloudData") >= 0)
                                MethodHandler = new PULL_Handler(app, u, sp);
                            else
                                MethodHandler = new PUT_Handler(app, u, sp);
                        }
                        else
                            return;
                        break;
                     case "GET":
                        if (app.Request.FilePath.IndexOf("/output/") >= 0)
                        {
                            string fpath = HttpUtility.UrlDecode(app.Request.FilePath);
                            sp = null;
                            u = authenticate(app, out sp);
                            if (u == null)
                            {
                                Global.systemLog.Debug("No valid user in GET /output/ request");
                                app.CompleteRequest();
                                return;
                            }

                            if (!ignoreSpaceInUseCheck && !allowSpaceOperations(sp))
                            {
                                app.Response.StatusCode = (int)ServerResponseCode.SpaceNotAvailable;
                                app.CompleteRequest();
                                return;
                            }
                            if (fpath.StartsWith("/BirstConnect/" + sp.Name + "/output/") || fpath.StartsWith("/BirstConnect/" + sp.ID + "/output/"))
                            {
                                if (Directory.Exists(sp.Directory + "\\output"))
                                {
                                    MethodHandler = new GET_Handler(app, u, sp);
                                }
                            }
                            else
                            {
                                Global.systemLog.Debug("Unexpected path in GET /output/ request: " + app.Request.FilePath);
                                return;
                            }
                        }
                        else if (app.Request.FilePath.IndexOf("/BirstConnect") >= 0)
                        {
                            sp = null;
                            u = authenticate(app, out sp);
                            if (u == null)
                            {
                                app.CompleteRequest();
                                return;
                            }

                            if (!ignoreSpaceInUseCheck && !allowSpaceOperations(sp))
                            {
                                app.Response.StatusCode = (int)ServerResponseCode.SpaceNotAvailable;
                                app.CompleteRequest();
                                return;
                            }
                            if (app.Request.FilePath.IndexOf(".cmd") >= 0 || app.Request.FilePath.IndexOf(".properties") >= 0 || app.Request.FilePath.IndexOf(Util.REPOSITORY_DEV) >= 0)
                            {
                                MethodHandler = new GET_Handler(app, u, sp);
                            }
                            else
                            {
                                MethodHandler = new PROPFIND_Handler(app, u, sp);
                                requestXml = MethodHandler.RequestXml;
                            }
                            break;
                        }
                        else
                            return;
                        break;
                    default:
                        return;
                }
                if (MethodHandler != null)
                {
                    statusCode = MethodHandler.Handle();
                    if (statusCode == (int)DavModule.ServerResponseCode.EndConnection)
                    {
                        app.CompleteRequest();
                        return;
                    }
                    if (statusCode != (int)PUT_Handler.DavPutResponseCode.Created && typeof(PUT_Handler).Equals(MethodHandler.GetType()) &&
                        ((PUT_Handler)MethodHandler).errorTable != null && ((PUT_Handler)MethodHandler).errorTable.Rows.Count > 0)
                    {
                        // Write error results
                        app.Response.ClearContent();
                        app.Response.StatusCode = statusCode;
                        app.Response.ContentType = "text/html";
                        writeErrorResponse(app, 8, ((PUT_Handler)MethodHandler).errorTable.Rows[0][0] + " - " +
                            ((PUT_Handler)MethodHandler).errorTable.Rows[0][2]);
                        app.CompleteRequest();
                        return;
                    }
                    else if (statusCode != (int)ServerResponseCode.Ok && typeof(PROPFIND_Handler).Equals(MethodHandler.GetType()) &&
                      ((PROPFIND_Handler)MethodHandler).errorTable != null && ((PROPFIND_Handler)MethodHandler).errorTable.Rows.Count > 0)
                    {
                        app.Response.ClearContent();
                        app.Response.StatusCode = statusCode;
                        app.Response.ContentType = "text/html";
                        writeErrorResponse(app, 9, ((PROPFIND_Handler)MethodHandler).errorTable.Rows[0][0] + " - " +
                            ((PROPFIND_Handler)MethodHandler).errorTable.Rows[0][2]);
                        app.CompleteRequest();
                        return;
                    }
                    else
                    {
                        responseXml = MethodHandler.ResponseXml;
                        ErrorXml = MethodHandler.ErrorXml;
                    }
                }
                app.Response.StatusCode = statusCode;
                if (ErrorXml.Length != 0)
                {
                    app.Response.StatusCode = (int)ServerResponseCode.MultiStatus;
                    app.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    app.Response.ContentType = "text/xml";
                    app.Response.Write(ErrorXml);
                }
                else
                {
                    if (responseXml.Length != 0)
                    {
                        app.Response.ContentEncoding = System.Text.Encoding.UTF8;
                        app.Response.ContentType = "text/xml";
                        app.Response.Write(responseXml);
                    }
                }
                app.CompleteRequest();
            }
            catch (System.Threading.ThreadAbortException)
            {
                return;
            }
            catch (Exception ex)
            {
                try
                {
                    Global.systemLog.Debug(ex, ex);
                    app.Response.Clear();
                    app.Response.StatusCode = (int)ServerResponseCode.BadRequest;
                    app.Response.StatusDescription = ex.Message;
                    app.CompleteRequest();
                    Global.systemLog.Warn("WebDAV Error: " + ex.Message, ex);
                    return;
                }
                catch (System.Threading.ThreadAbortException)
                {
                    return;
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Debug(ex2, ex2);
                }
            }
            finally
            {
                log4net.NDC.Clear();
            }
        }

        private bool allowSpaceOperations(Space sp)
        {
            bool allowSpaceOperations = true;
            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);
            if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID) && !SpaceOpsUtils.isSpaceBeingCopiedFrom(spaceOpID))
            {
                allowSpaceOperations = false;
                Global.systemLog.Error("Space unavailable : " + SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID));
            }
            return allowSpaceOperations;
        }

        private string getHtmlErrorMsg(string msg)
        {
            string retval = "<HTML><HEAD>";
            retval += "<TITLE>Resource Error</TITLE>\r\n";
            retval += "\r\n<STYLE>.Error{font-size:9pt;font-family:'trebuchet ms',helvetica,sans-serif;}</STYLE>\r\n";
            retval += "</HEAD>\r\n<BODY>\r\n<H3 class=\"Error\">" + msg + "</h3>\r\n";
            retval += "</BODY></HTML>";
            return retval;
        }

        private static string getNonPathPart(HttpApplication app)
        {
            if (app == null)
                throw new ArgumentNullException("HttpApplication", "Cannot find handle to HTTP Application");
            string completePath = app.Request.Url.AbsoluteUri;
            string relativePath = app.Request.Url.AbsolutePath;
            return completePath.Substring(0, completePath.Length - relativePath.Length);
        }

        public static string getRelativePath(HttpApplication app, string URIPath)
        {
            if (app == null)
                throw new ArgumentNullException("HttpApplication", "Cannot find handle to HTTP Application");
            string nonPathPart = getNonPathPart(app);
            string retValue;
            if (URIPath.ToLower().StartsWith(nonPathPart.ToLower()))
                retValue = URIPath.Remove(0, nonPathPart.Length);
            else
                retValue = URIPath;

            //Remove the application path
            string appPath = app.Request.ApplicationPath;
            if (retValue.ToLower().StartsWith(appPath.ToLower()))
                retValue = retValue.Remove(0, appPath.Length);
            if (retValue.StartsWith("/BirstConnect/PullCloudData"))
            {
                retValue = retValue.Substring("/BirstConnect/PullCloudData".Length);
            }
            else if (retValue.StartsWith("BirstConnect"))
            {
                retValue = retValue.Substring(12);
            }
            return HttpUtility.UrlDecode(retValue.Trim('/'));
        }

        public static string StreamtoString(Stream stream)
        {
            string retval = "";
            using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
            {
                //Go to the begining of the stream
                streamReader.BaseStream.Position = 0;
                retval = streamReader.ReadToEnd();
            }
            return retval;
        }

        private static bool ValidateEnumType(Enum enumToValidate)
        {
            if (enumToValidate.GetTypeCode() != TypeCode.Int32)
                throw new Exception("Invalid Enum Type");

            return true;
        }

        private static int getEnumValue(Enum statusCode)
        {
            int enumValue = 0;
            if (ValidateEnumType(statusCode))
                enumValue = (int)System.Enum.Parse(statusCode.GetType(), statusCode.ToString(), true);
            return enumValue;
        }

        public static string getEnumHttpResponse(Enum statusCode)
        {
            string httpResponse = "";
            switch (getEnumValue(statusCode))
            {
                case 200:
                    httpResponse = "HTTP/1.1 200 OK";
                    break;
                case 400:
                    httpResponse = "HTTP/1.1 400 Bad Request";
                    break;
                case 404:
                    httpResponse = "HTTP/1.1 404 Not Found";
                    break;
                case 423:
                    httpResponse = "HTTP/1.1 423 Locked";
                    break;
                case 424:
                    httpResponse = "HTTP/1.1 424 Failed Dependency";
                    break;
                case 507:
                    httpResponse = "HTTP/1.1 507 Insufficient Storage";
                    break;
                default:
                    break;
            }
            return httpResponse;
        }

        public static long getRequestLength(HttpApplication httpApplication)
        {
            if (httpApplication == null)
                throw new ArgumentNullException("HttpApplication", "Cannot find handle to HTTP Application");
            return httpApplication.Request.InputStream.Length;
        }
    }
}
