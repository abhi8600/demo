﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace Acorn
{
    public class GET_Handler : IMethodHandler
    {
        private HttpApplication app;
        private string errorxml = "";
        private User u;
        private Space sp;

        private HttpCacheability responseCache = HttpCacheability.NoCache;

        public GET_Handler(HttpApplication app, User u, Space sp)
        {
            this.app = app;
            this.u = u;
            this.sp = sp;
        }

        #region IMethod_Handler Interface
        public string ResponseXml
        {
            get
            {
                return "";
            }
        }
        public string RequestXml
        {
            get
            {
                return "";
            }
        }
        public string ErrorXml
        {
            get
            {
                return errorxml;
            }
        }
        public int Handle()
        {
            if (app == null)
            {
                Global.systemLog.Debug("app is null");
                return (int)DavModule.ServerResponseCode.BadRequest;
            }

            string fpath = HttpUtility.UrlDecode(this.app.Request.FilePath);
            Global.systemLog.Debug("GET (1): " + fpath);
            int index = fpath.LastIndexOf('/');
            if (index < 0)
                return (int)DavModule.ServerResponseCode.NotFound;
            string fname = fpath.Substring(index + 1);
            Global.systemLog.Debug("GET (2): " + fname);
            FileStream fs = null;

            app.Response.Cache.SetCacheability(responseCache);
            app.Response.ContentType = "text/plain";
            if (fpath.IndexOf("/output/") >= 0)
            {
                app.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.HtmlEncode(fname) + "\"");
                long size = new FileInfo(sp.Directory + "\\output\\" + fname).Length;
                if (fname.EndsWith(".zip"))
                {
                    ZipFile zf = null;
                    try
                    {
                        zf = new ZipFile(sp.Directory + "\\output\\" + fname);
                        size = 0;
                        foreach (ZipEntry zen in zf)
                        {
                            size += zen.Size;
                        }
                    }
                    finally
                    {
                        if (zf != null)
                            zf.Close();
                    }
                    Global.systemLog.Debug("GET(3): size is: " + size);
                    app.Response.AddHeader("Content-Length", size.ToString());
                    FileStream zfs = null;
                    FileInfo fi = new FileInfo(sp.Directory + "\\output\\" + fname);
                    zfs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    ZipInputStream zs = null;
                    try
                    {
                        zs = new ZipInputStream(zfs);
                        ZipEntry ze = zs.GetNextEntry();
                        if (ze != null)
                        {
                            int read;
                            while ((read = zs.ReadByte()) >= 0)
                            {
                                app.Response.OutputStream.WriteByte((byte)read);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Debug(ex, ex);
                    }
                    finally
                    {
                        if (zs != null)
                            zs.Close();
                    }
                    return (int)DavModule.ServerResponseCode.Ok;
                }
                else
                {
                    app.Response.AddHeader("Content-Length", size.ToString());
                    fs = new FileStream(sp.Directory + "\\output\\" + fname, FileMode.Open, FileAccess.Read);
                }
            }
            else if (fpath.IndexOf(".cmd") >= 0 || fpath.IndexOf(".properties") >= 0 || fpath.IndexOf(Util.REPOSITORY_DEV) >= 0)
            {
                app.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.HtmlEncode(fname));
                app.Response.AddHeader("Content-Length", (new FileInfo(sp.Directory + "\\" + fname)).Length.ToString());
                fs = new FileStream(sp.Directory + "\\" + fname, FileMode.Open, FileAccess.Read);
            }            

            if(fs == null)
            {
                Global.systemLog.Debug("fs is null");
                return (int) DavModule.ServerResponseCode.BadRequest;
            }

            int b;
            while ((b = fs.ReadByte()) >= 0)
            {
                app.Response.OutputStream.WriteByte((byte) b);
            }
            fs.Close();
            return (int)DavModule.ServerResponseCode.Ok;
        }
        #endregion
    }
}
