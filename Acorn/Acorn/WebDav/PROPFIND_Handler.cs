﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;
using System.Data.Odbc;
using Acorn.WebDav;
using System.Threading;
using System.Net;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class PROPFIND_Handler : IMethodHandler
    {
        private HttpApplication app;
        private DavModule.PropertyRequestType requestPropertyType;
        private RequestedPropertyCollection requestedProperties;
        private HttpCacheability responseCache = HttpCacheability.NoCache;
        private string requestPath;
        private int httpResponseCode = (int)DavModule.ServerResponseCode.Ok;
        private string responseXml = "";
        private string errorXml = "";
        private XPathNavigator requestXmlNavigator = null;
        private User u = null;
        private Space sp = null;
        public DataTable errorTable = null;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public PROPFIND_Handler(HttpApplication app, User u, Space sp)
        {
            this.app = app;
            this.u = u;
            this.sp = sp;
            if (app.Request.InputStream.Length != 0)
            {
                requestXmlNavigator = new XPathDocument(app.Request.InputStream).CreateNavigator();
            }
        }

        #region IMethod_Handler Interface

        public string RequestXml
        {
            get
            {
                if (requestXmlNavigator == null) return "";
                return requestXmlNavigator.InnerXml;
            }
        }
        public string ResponseXml
        {
            get
            {
                return responseXml;
            }
        }
        public string ErrorXml
        {
            get
            {
                return errorXml;
            }
        }

        public int Handle()
        {
            string userAgent = app.Request.Headers["User-Agent"];
            /*
             * Get the config file if specified
             */
            string fpath = HttpUtility.UrlDecode(this.app.Request.FilePath);
            string fileName = new FileInfo(fpath).Name;
            if (fileName.EndsWith(Util.DCCONFIG_FILE_NAME) && 
                (fpath == "/BirstConnect/" + sp.Name + "/" + fileName || 
                 fpath == "/BirstConnect/" + sp.ID + "/" + fileName ||
                 fpath == "/" + sp.Name + "/" + fileName))
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int) HttpStatusCode.Forbidden;
                string config = null;
                string path = sp.Directory + "\\" + Util.DCCONFIG_DIR;
                //moving dccofig.xml file to dcconfig directory
                BirstConnectUtil.moveFileToDirectory(sp, Util.DCCONFIG_DIR, Util.DCCONFIG_FILE_NAME);
                if (File.Exists(path + "\\" + fileName))
                {
                    StreamReader reader = new StreamReader(sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + fileName);
                    config = reader.ReadToEnd();
                    reader.Close();
                }
                else
                {
                    this.responseXml = "";
                    try
                    {
                        bool exists = Util.checkFileExistsWithException(sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + fileName);                        
                    }
                    catch (Exception ex)
                    {
                        StringBuilder errorRes = new StringBuilder();
                        string errorMsg = "Exception in locating dcconfig file : " + ex.Message;
                        Global.systemLog.Error(errorMsg, ex);
                        errorRes.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                        errorXml = errorRes.ToString();
                        return -1;
                    }                    
                }
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    Acorn.Database.SpaceUsage su = Database.getTotalSpaceUsage(conn, mainSchema, u, true);
                    this.responseXml = "encryptionkey=" + Performance_Optimizer_Administration.MainAdminForm.SecretPassword + "\r\n\r\n";
                    if (config != null)
                        this.responseXml += config;
                    return httpResponseCode;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            else if (fpath == "/BirstConnect/" + sp.Name + "/birst.processing" ||
                     fpath == "/BirstConnect/" + sp.ID + "/birst.processing" ||
                     fpath == "/" + sp.Name + "/birst.processing")
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int) HttpStatusCode.Forbidden;
                Status.StatusResult sr = Status.getLoadStatus(null, sp);
                if (Status.isRunningCode(sr.code))
                {
                    errorTable = ApplicationUploader.getNewErrorTable();
                    DataRow dr = errorTable.NewRow();
                    dr[0] = "Birst Connect";
                    dr[1] = ApplicationUploader.ERROR_STR;
                    dr[2] = "Cannot initiate processing, Birst is currently processing data)";
                    errorTable.Rows.Add(dr);
                    return (int)DavModule.ServerResponseCode.CurrentlyProcessing;
                }
                return httpResponseCode;
            }
            else if (fpath == "/BirstConnect/" + sp.Name + "/birst.checkstatus" ||
                     fpath == "/BirstConnect/" + sp.ID + "/birst.checkstatus" || 
                     fpath == "/" + sp.Name + "/birst.checkstatus")
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int)HttpStatusCode.Forbidden;
                bool newRep = false;
                Performance_Optimizer_Administration.MainAdminForm maf = Util.loadRepository(sp, out newRep);
                bool fetchOldWay = true;
                Status.StatusResult sr = null;
                if (Util.useExternalSchedulerForProcess(sp, maf))
                {
                    sr = Status.getStatusUsingExternalScheduler(sp);
                    // this is a hack for now.
                    // To avoid the confusion between different processing engine
                    // we get the status both the old and the new way. Will phase out completly 
                    // once users move to >= 5.1 processing engine.
                    fetchOldWay = sr != null && Status.isRunningCode(sr.code) ? false : true;
                }

                if (fetchOldWay)
                {
                    sr = Status.getLoadStatus(null, sp);
                    if (sr.code == Status.StatusCode.Complete)
                    {
                        QueryConnection conn = null;
                        try
                        {
                            conn = ConnectionPool.getConnection();
                            ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, sp.LoadNumber, Util.LOAD_GROUP_NAME);                            
                        }
                        finally
                        {
                            ConnectionPool.releaseConnection(conn);
                        }
                    }
                }
                if (Status.isRunningCode(sr.code))
                    writeStatusResponse(app, 1, "Currently Processing", "Last Processing Status");
                else if (sr.code == Status.StatusCode.Failed)
                    writeStatusResponse(app, 2, "Last Process Failed", "Last Processing Status");
                else 
                    writeStatusResponse(app, 0, "Last Process Successfully Completed", "Last Processing Status");
                return httpResponseCode;                
            }
            else if (fpath == "/BirstConnect/" + sp.Name + "/birst.birstconnect.serverversion" ||
                     fpath == "/BirstConnect/" + sp.ID + "/birst.birstconnect.serverversion" ||
                     fpath == "/" + sp.Name + "/birst.birstconnect.serverversion")
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int)HttpStatusCode.Forbidden;
                string version = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BirstConnectProductVersion"];
                if (version != null)
                    writeStatusResponse(app, 0, version, "Birst Connect Version");
                else
                    writeStatusResponse(app, -1, "-1", "Birst Connect Version");
                return httpResponseCode;
            }
            else if (fpath.StartsWith("/BirstConnect/" + sp.Name + "/dcrealtimeconnection/") ||
                     fpath.StartsWith("/BirstConnect/" + sp.ID + "/dcrealtimeconnection/") || 
                     fpath.StartsWith("/" + sp.Name + "/dcrealtimeconnection/"))
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int) HttpStatusCode.Forbidden;
                int index = fpath.LastIndexOf('/');
                string id = sp.ID.ToString() + "/" + fpath.Substring(index + 1);
                // Make sure the server is started XXX timing issue here, request comes in before base ConnectionRelay has started
                if (!ConnectionRelay.Started)
                    ConnectionRelay.startServer(false);
                QueryServer qs = new QueryServer(id, app); // new query server for the request
                ConnectionRelay.processQueryServer(qs, mainSchema, QueryServer.getHostIPAddress().ToString()); // process the request
                return (int)DavModule.ServerResponseCode.EndConnection;
            }
            else if (fpath.StartsWith("/BirstConnect/" + sp.ID + "/supportsparallelupload"))
            {
                // do not do this if not from Birst Connect
                if (!userAgent.StartsWith("Birst-WebDAV") && !userAgent.StartsWith("Birst DAV"))
                    return (int)HttpStatusCode.Forbidden;
                string MaxNoOfParallelUploadThreads = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MaxNoOfParallelUploadThreads"];
                int noThreads = 1;
                if (MaxNoOfParallelUploadThreads != null)
                    noThreads = Int32.Parse(MaxNoOfParallelUploadThreads);
                writeStatusResponse(app, noThreads, noThreads.ToString(), "SupportsParallelUpload");
                return httpResponseCode;
            }
            httpResponseCode = GetRequestType();
            if (httpResponseCode == (int)DavModule.ServerResponseCode.MultiStatus)
            {
                if (this.requestPropertyType == DavModule.PropertyRequestType.NamedProperties)
                    this.requestedProperties = getRequestedProps();
                this.requestPath = DavModule.getRelativePath(this.app, this.app.Request.FilePath);
                app.Response.Cache.SetCacheability(responseCache);
                httpResponseCode = BuildResponse(this.app.Request.Url.ToString());
            }
            return httpResponseCode;
        }

        #endregion

        #region Private Handler Methods
        private int GetRequestType()
        {
            int returnCode = (int)DavModule.ServerResponseCode.MultiStatus;
            //NOTE: An empty PROPFIND request body MUST be treated as a request for the names 
            //	and values of all properties.
            if (requestXmlNavigator == null)
                this.requestPropertyType = DavModule.PropertyRequestType.AllProperties;

            else
            {
                XPathNodeIterator propFindNodeIterator = requestXmlNavigator.SelectDescendants("propfind", "DAV:", false);
                if (propFindNodeIterator.MoveNext())
                {
                    if (propFindNodeIterator.Current.MoveToFirstChild())
                    {
                        switch (propFindNodeIterator.Current.LocalName.ToLower(CultureInfo.InvariantCulture))
                        {
                            case "propnames":
                                this.requestPropertyType = DavModule.PropertyRequestType.PropertyNames;
                                break;
                            case "allprop":
                                this.requestPropertyType = DavModule.PropertyRequestType.AllProperties;
                                break;
                            default:
                                this.requestPropertyType = DavModule.PropertyRequestType.NamedProperties;
                                break;
                        }
                    }
                    else
                        returnCode = (int)DavModule.ServerResponseCode.BadRequest;
                }
                else
                    returnCode = (int)DavModule.ServerResponseCode.BadRequest;
            }
            return returnCode;
        }

        private RequestedPropertyCollection getRequestedProps()
        {
            RequestedPropertyCollection davProperties = new RequestedPropertyCollection();
            if (requestXmlNavigator != null)
            {
                XPathNodeIterator propNodeIterator = requestXmlNavigator.SelectDescendants("prop", "DAV:", false);
                if (propNodeIterator.MoveNext())
                {
                    XPathNodeIterator nodeChildren = propNodeIterator.Current.SelectChildren(XPathNodeType.All);
                    while (nodeChildren.MoveNext())
                    {
                        XPathNavigator currentNode = nodeChildren.Current;

                        if (currentNode.NodeType == XPathNodeType.Element)
                            davProperties.Add(new RequestedProperty(currentNode.LocalName, currentNode.NamespaceURI));
                    }
                }
            }
            if (davProperties.Count == 0)
                return null;

            return davProperties;
        }

        private int BuildResponse(string url)
        {
            using (Stream responseStream = new MemoryStream())
            {
                string fpath = HttpUtility.UrlDecode(this.app.Request.FilePath);
                string dir = "\\data";
                if (fpath == "/" + sp.Name + "/output/" && Directory.Exists(sp.Directory + "\\output"))
                {
                    dir = "\\output";
                }
                XmlTextWriter xmlWriter = new XmlTextWriter(responseStream, Encoding.UTF8);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.IndentChar = '\t';
                xmlWriter.Indentation = 1;
                xmlWriter.WriteStartDocument();
                //Set the Multistatus
                xmlWriter.WriteStartElement("D", "multistatus", "DAV:");
                if (Directory.Exists(sp.Directory))
                {
                    string depthstr = app.Request.Headers["depth"];
                    int depth = depthstr == null || depthstr.Length == 0 ? 0 : int.Parse(depthstr);
                    GetFolderXML(url, sp.Directory + dir, depth > 0, xmlWriter, this.requestedProperties, this.requestPropertyType);
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Flush();
                this.responseXml = DavModule.StreamtoString(responseStream);
                xmlWriter.Close();
            }
            return (int)DavModule.ServerResponseCode.MultiStatus;
        }
        #endregion

        public static void GetFileXML(FileInfo fi, string url, XmlTextWriter xmlWriter, RequestedPropertyCollection ReqProps, Acorn.DavModule.PropertyRequestType RequestType)
        {
            //Load the Valid Properties for the file resource
            ArrayList ValidProps = new ArrayList();
            ValidProps.Add("getcontenttype");
            ValidProps.Add("getlastmodified");
            ValidProps.Add("ishidden");
            ValidProps.Add("displayname");
            ValidProps.Add("getcontentlanguage");
            ValidProps.Add("getcontentlength");
            ValidProps.Add("creationdate");
            ValidProps.Add("resourcetype");

            RequestedPropertyCollection InValidProps = new RequestedPropertyCollection();
            if (RequestType == Acorn.DavModule.PropertyRequestType.PropertyNames && ReqProps != null) return;
            //Open the response element
            xmlWriter.WriteStartElement("response", "DAV:");
            //Load the valid items HTTP/1.1 200 OK
            xmlWriter.WriteElementString("href", "DAV:", url);
            //Open the propstat element section
            xmlWriter.WriteStartElement("propstat", "DAV:");
            xmlWriter.WriteElementString("status", "DAV:", DavModule.getEnumHttpResponse(DavModule.ServerResponseCode.Ok));
            //Open the prop element section
            xmlWriter.WriteStartElement("prop", "DAV:");
            //If there are no requested Properties then return all props for File.
            if (ReqProps == null)
            {
                ReqProps = new RequestedPropertyCollection();
                for (int vPe = 0; vPe < ValidProps.Count; vPe++)
                {
                    ReqProps.Add(new RequestedProperty((string)ValidProps[vPe], "DAV:"));
                }
            }
            foreach (RequestedProperty ReqProp in ReqProps)
            {
                string propertyName = ReqProp.LocalName;
                //if (propertyName.ToLower(CultureInfo.InvariantCulture).StartsWith("get"))
                //    propertyName = propertyName.Substring(3);
                if ((ReqProp.NS != "DAV:") || (ValidProps.IndexOf(propertyName.ToLower()) == -1))
                {
                    InValidProps.Add(ReqProp);
                }
                else
                {
                    if (RequestType == Acorn.DavModule.PropertyRequestType.PropertyNames)
                    {
                        //if this is a request for property names only then just return the named elements:
                        xmlWriter.WriteElementString(propertyName, ReqProp.NS, "");
                    }
                    else
                    {
                        //Map the property to the Row Data and return the PropStat XML.
                        switch (propertyName.ToLower())
                        {
                            case "getcontentlanguage":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "en-us");
                                break;
                            case "getcontentlength":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", fi.Length.ToString());
                                break;
                            case "getcontenttype":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "application/octet-stream");
                                break;
                            case "displayname":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", fi.Name);
                                break;
                            case "ishidden":
                                //May adjust later to allow hidden files
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "0");
                                break;
                            case "resourcetype":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "creationdate":
                                xmlWriter.WriteStartElement(ReqProp.LocalName, "DAV:");
                                //xmlWriter.WriteAttributeString("b:dt", "dateTime.tz");
                                xmlWriter.WriteString(DateTime.Now.ToUniversalTime().ToString("s", CultureInfo.InvariantCulture));
                                xmlWriter.WriteEndElement();
                                break;
                            default:
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                        }
                    }
                }
            }
            //Close the prop element section
            xmlWriter.WriteEndElement();
            //Close the propstat element section
            xmlWriter.WriteEndElement();
            //END Load the valid items HTTP/1.1 200 OK
            //Load the invalid items HTTP/1.1 404 Not Found
            if (InValidProps.Count > 0)
            {
                xmlWriter.WriteStartElement("propstat", "DAV:");
                xmlWriter.WriteElementString("status", "DAV:", DavModule.getEnumHttpResponse(DavModule.ServerResponseCode.NotFound));
                //Open the prop element section
                xmlWriter.WriteStartElement("prop", "DAV:");
                //Load all the invalid properties
                foreach (RequestedProperty InValidProp in InValidProps)
                    xmlWriter.WriteElementString(InValidProp.LocalName, InValidProp.NS, "");
                //Close the prop element section
                xmlWriter.WriteEndElement();
                //Close the propstat element section
                xmlWriter.WriteEndElement();
            }
            //END Load the invalid items HTTP/1.1 404 Not Found
            //Close the response element
            xmlWriter.WriteEndElement();
        }

        public static void GetFolderXML(string url, string dirName, bool includeChildren, XmlTextWriter xmlWriter, RequestedPropertyCollection ReqProps, Acorn.DavModule.PropertyRequestType RequestType)
        {
            //Load the Valid Properties for the file resource
            ArrayList ValidProps = new ArrayList();
            ValidProps.Add("getcontentlanguage");
            ValidProps.Add("getcontentlength");
            ValidProps.Add("getcontenttype");
            ValidProps.Add("creationdate");
            ValidProps.Add("displayname");
            ValidProps.Add("ishidden");
            ValidProps.Add("iscollection");
            ValidProps.Add("collection");
            ValidProps.Add("getlastmodified");
            ValidProps.Add("resourcetype");
            ValidProps.Add("lockdiscovery");
            ValidProps.Add("supportedlock");
            ValidProps.Add("getetag");

            RequestedPropertyCollection InValidProps = new RequestedPropertyCollection();

            if (RequestType == Acorn.DavModule.PropertyRequestType.PropertyNames && ReqProps != null) return;

            if (dirName == null) return;

            //Open the response element
            xmlWriter.WriteStartElement("response", "DAV:");
            //Load the valid items HTTP/1.1 200 OK
            xmlWriter.WriteElementString("href", "DAV:", url);
            //Open the propstat element section
            xmlWriter.WriteStartElement("propstat", "DAV:");
            xmlWriter.WriteElementString("status", "DAV:", DavModule.getEnumHttpResponse(DavModule.ServerResponseCode.Ok));
            //Open the prop element section
            xmlWriter.WriteStartElement("prop", "DAV:");
            //If there are no requested Properties then return all props for File.
            bool allprops = false;
            if (ReqProps == null)
            {
                allprops = true;
                ReqProps = new RequestedPropertyCollection();
                for (int vPe = 0; vPe < ValidProps.Count; vPe++)
                {
                    ReqProps.Add(new RequestedProperty((string)ValidProps[vPe], "DAV:"));
                }
            }
            foreach (RequestedProperty ReqProp in ReqProps)
            {
                string propertyName = ReqProp.LocalName;
                //if (propertyName.ToLower(CultureInfo.InvariantCulture).StartsWith("get"))
                //    propertyName = propertyName.Substring(3);
                if ((ReqProp.NS != "DAV:") || (ValidProps.IndexOf(propertyName.ToLower()) == -1))
                {
                    InValidProps.Add(ReqProp);
                }
                else
                {
                    if (RequestType == Acorn.DavModule.PropertyRequestType.PropertyNames)
                    {
                        //if this is a request for property names only then just return the named elements:
                        xmlWriter.WriteElementString(propertyName, ReqProp.NS, "");
                    }
                    else
                    {
                        //Map the property to the Row Data and return the PropStat XML.
                        switch (propertyName.ToLower())
                        {
                            case "getcontentlanguage":
                                //xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "en-us");
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "getcontentlength":
                                //To do, calculate Contenlength
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "0");
                                break;
                            case "getcontenttype":
                                //xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "application/webdav-collection");
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "displayname":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "/");
                                break;
                            case "ishidden":
                                //May adjust later to allow hidden files
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "0");
                                break;
                            case "iscollection":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "1");
                                break;
                            case "lockdiscovery":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "supportedlock":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "getetag":
                                xmlWriter.WriteElementString(ReqProp.LocalName, "DAV:", "");
                                break;
                            case "resourcetype":
                                //May adjust later to allow hidden files
                                xmlWriter.WriteStartElement(ReqProp.LocalName, "DAV:");
                                xmlWriter.WriteElementString("collection", "DAV:", "");
                                xmlWriter.WriteEndElement();
                                break;
                            case "getlastmodified":
                                xmlWriter.WriteStartElement(ReqProp.LocalName, "DAV:");
                                //xmlWriter.WriteAttributeString("b:dt", "dateTime.rfc1123");
                                xmlWriter.WriteString(DateTime.Now.ToUniversalTime().ToString("r", CultureInfo.InvariantCulture));
                                xmlWriter.WriteEndElement();
                                break;
                            case "creationdate":
                                xmlWriter.WriteStartElement(ReqProp.LocalName, "DAV:");
                                //xmlWriter.WriteAttributeString("b:dt", "dateTime.tz"); -- seems to break webfolders
                                xmlWriter.WriteString(DateTime.Now.ToUniversalTime().ToString("s", CultureInfo.InvariantCulture));
                                xmlWriter.WriteEndElement();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            //Close the prop element section
            xmlWriter.WriteEndElement();
            //Close the propstat element section
            xmlWriter.WriteEndElement();
            //END Load the valid items HTTP/1.1 200 OK
            //Load the invalid items HTTP/1.1 404 Not Found
            if (InValidProps.Count > 0 && !allprops)
            {
                xmlWriter.WriteStartElement("propstat", "DAV:");
                xmlWriter.WriteElementString("status", "DAV:", DavModule.getEnumHttpResponse(Acorn.DavModule.ServerResponseCode.NotFound));

                //Open the prop element section
                xmlWriter.WriteStartElement("prop", "DAV:");

                //Load all the invalid properties
                foreach (RequestedProperty InValidProp in InValidProps)
                    xmlWriter.WriteElementString(InValidProp.LocalName, InValidProp.NS, "");

                //Close the prop element section
                xmlWriter.WriteEndElement();
                //Close the propstat element section
                xmlWriter.WriteEndElement();
            }
            //END Load the invalid items HTTP/1.1 404 Not Found
            //Close the response element
            xmlWriter.WriteEndElement();
            // If requested, display files
            if (includeChildren)
            {
                DirectoryInfo di = new DirectoryInfo(dirName);
                foreach (FileInfo fi in di.GetFiles())
                {
                    GetFileXML(fi, url + (url.EndsWith("/") ? "" : "/") + fi.Name, xmlWriter, ReqProps, RequestType);
                }
            }
        }
        
        private void writeStatusResponse(HttpApplication app, int code, string message, string logPrefix)
        {
            app.Response.Write("<html><body>Status " + String.Format("{0:000}", code) + ": " + HttpUtility.HtmlEncode(message) + "</body></html>\n");
            Global.systemLog.Info(logPrefix + " : " + String.Format("{0:000}", code) + ": " + message);
        }

    }
}
