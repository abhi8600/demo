﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Microsoft.Win32;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Net.Mail;
using System.Text;
using System.Threading;
using Acorn.WebDav;
using Acorn.Background;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO.Compression;
using Acorn.DBConnection;
using Acorn.Utils;

namespace Acorn
{
    public class PUT_Handler : IMethodHandler
    {
        public static int MAX_COMMAND_LINES = 1000;

        public const int VERSION_OLD = 0;
        public const int VERSION_NEW = 1;
        public const int PHASE_UPLOAD = 1;
        public const int PHASE_UNPACK = 2;
        public const int PHASE_POLL = 3;
        public const int PHASE_INIT_PARALLEL_UPLOAD = 4;
        public const int PHASE_ASSEMBLE_PARALLEL_UPLOAD = 5;
        public const int PHASE_POLL_FINISH_PARALLEL_UPLOAD = 6;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public enum DavPutResponseCode : int
        {
            /// <summary>
            /// 0: None
            /// </summary>
            /// <remarks>
            ///		Default enumerator value
            /// </remarks>
            None = 0,

            /// <summary>
            /// 201: Created
            /// </summary>
            /// <remarks>
            ///		Resource completed successfully
            /// </remarks>
            Created = 201,

            /// <summary>
            /// 400: Bad Request
            /// </summary>
            BadRequest = 400,

            /// <summary>
            /// 403: Forbidden
            /// </summary>
            /// <remarks>
            ///		This indicates one of two conditions: 
            ///			- The server does not allow the creation of the resource at the given 
            ///				location in its namespace
            ///			- The parent collection of the Request-URI exists but cannot accept members.
            /// </remarks>
            Forbidden = 403,

            /// <summary>
            /// 409: Conflict
            /// </summary>
            /// <remarks>
            ///		If all ancestor collections do not exist
            /// </remarks>
            Conflict = 409,

            /// <summary>
            /// 423: Resource Locked
            /// </summary>
            /// <remarks>
            ///		If the resource is already locked with an exclusive lock or if the resource
            ///		is already locked with a shared lock and the client requests and exclusive lock
            /// </remarks>
            Locked = 423,

            /// <remarks>
            ///		The resource does not have sufficient space to record the state of the 
            ///		resource after the execution of this method.
            /// </remarks>
            InsufficientStorage = 507,
        }

        private HttpApplication httpApplication;

        private string requestPath;
        private Space sp;
        private User u;
        public DataTable errorTable = null;
        private string responseString = "";
        
        // used for checking if a file is still being unpacked/scanned
        public static Dictionary<string, Status.StatusResult> fileUploadStatus = new Dictionary<string, Status.StatusResult>();

        public static Dictionary<string, Status.StatusResult> commandfileExecutionStatus = new Dictionary<string, Status.StatusResult>();

        public static Dictionary<string, Status.StatusResult> fileAssembleStatus = new Dictionary<string, Status.StatusResult>();

        public PUT_Handler(HttpApplication app, User u, Space sp)
        {
            this.httpApplication = app;
            this.sp = sp;
            this.u = u;
        }

        #region IMethod_Handler Interface
        public string ResponseXml
        {
            get
            {
                return responseString;
            }
        }

        public string RequestXml
        {
            get
            {
                return "";
            }
        }

        public string ErrorXml
        {
            get
            {
                return "";
            }
        }

        public int Handle()
        {
            if (httpApplication == null)
            {
                Global.systemLog.Debug("httpApplication is null");
                return (int)DavModule.ServerResponseCode.BadRequest;
            }

            if (DavModule.getRequestLength(httpApplication) == 0)
                return (int)DavPutResponseCode.Created;

            this.requestPath = DavModule.getRelativePath(this.httpApplication, this.httpApplication.Request.FilePath);

            if (!Directory.Exists(sp.Directory + "\\data"))
            {
                Global.systemLog.Debug("no data directory for the space");
                return (int)DavModule.ServerResponseCode.BadRequest;
            }

            string filename = this.requestPath;
            int folderSeparator = filename.IndexOf('/');
            string lname = filename.ToLower();
            if (folderSeparator >= 0)
            {
                lname = lname.Substring(folderSeparator + 1);
                filename = filename.Substring(folderSeparator + 1);
            }
            if (lname == "birst.process")
            {
                int result = process();
                if (result != (int)DavPutResponseCode.None)
                    return result;
            }
            else if (lname == "birst.getrealtimeconnectionstatus")
            {
                int result = getRealtimeConnectionStatus();
                if (result != (int)DavPutResponseCode.None)
                    return result;
            }
            else if (lname == "birst.getencryptionkey")
            {
                int result = getEncryptionKey();
                if (result != (int)DavPutResponseCode.None)
                    return result;
            }
            else if (lname.EndsWith(Util.DCCONFIG_FILE_NAME))
            {
                long segmentSize = this.httpApplication.Request.Headers["Segment-size"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Segment-size"]);
                int segment = this.httpApplication.Request.Headers["Segment"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Segment"]);
                int numSegments = this.httpApplication.Request.Headers["Num-segments"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Num-segments"]);
                int version = this.httpApplication.Request.Headers["Version"] == null ? VERSION_OLD : int.Parse(this.httpApplication.Request.Headers["Version"]);
                int phase = this.httpApplication.Request.Headers["Phase"] == null ? 0 : int.Parse(this.httpApplication.Request.Headers["Phase"]);
                string hash = this.httpApplication.Request.Headers["Hash"]; // backwards compatibility with old hashing
                string hash256 = this.httpApplication.Request.Headers["Hash256"];
                if (version == VERSION_OLD)
                {
                    if (getDataConductorSettings(filename) == -1)
                    {
                        Global.systemLog.Warn("getDataConductorSettings returned -1");
                        return (int)DavPutResponseCode.BadRequest;
                    }
                    else
                    {
                        Global.systemLog.Debug("BirstConnect Configuration saved successfully by user " + u.Username);
                        return (int)Acorn.DavModule.ServerResponseCode.Ok;
                    }
                }
                else if (version == VERSION_NEW)
                {
                    bool isParallelUpload = this.httpApplication.Request.Headers["isParallelUpload"] == null ? false : bool.Parse(this.httpApplication.Request.Headers["isParallelUpload"]);
                    if (phase == PHASE_UPLOAD)
                    {
                        string dcTargetDir = sp.Directory + "\\" + Util.DCCONFIG_DIR;
                        string dcTargetFile = dcTargetDir + "\\" + filename;
                        FileInfo fi = new FileInfo(dcTargetFile);
                        if (SaveFile(fi, segment, numSegments, segmentSize, hash, hash256, isParallelUpload, false) == -1)
                        {
                            Global.systemLog.Warn("Cannot save BirstConnect Configuration, SaveFile returned -1");
                            return (int)DavPutResponseCode.BadRequest;
                        }
                        Global.systemLog.Debug("BirstConnect Configuration saved successfully by user " + u.Username);
                        return (int)Acorn.DavModule.ServerResponseCode.Ok;
                    }
                    else
                    {
                        Global.systemLog.Warn("unexpected phase encountered when saving BirstConnect Configuration: " + phase);
                        return (int)DavPutResponseCode.BadRequest;
                    }
                }
                else
                {
                    Global.systemLog.Warn("unexpected upload version number encountered when saving BirstConnect Configuration: " + version);
                    return (int)DavPutResponseCode.BadRequest;
                }
            }
            else if (lname.StartsWith("dcrealtimedata"))
            {
                int connectionNameSeparator = filename.IndexOf('/');
                string connectionName = filename.Substring(connectionNameSeparator + 1);
                int index = int.Parse(httpApplication.Request.Headers["RequestIndex"].ToString());
                processData(connectionName, index);
            }
            else if (lname.EndsWith(".command"))
            {
                if (lname.EndsWith("birst.generatebulkqueryresults.command"))
                {
                    //asynchronous request for processing command
                    int phase = int.Parse(this.httpApplication.Request.Headers["Phase"]);
                    switch (phase)
                    {
                        case PHASE_UPLOAD:
                            //simply save the file and start executing commands from it
                            StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);
                            StreamWriter sw = File.CreateText(sp.Directory + "\\data\\birst.generatebulkqueryresults.command");
                            string line = null;
                            while ((line = inputStream.ReadLine()) != null)
                            {
                                sw.WriteLine(line);
                            }
                            inputStream.Close();
                            sw.Flush();
                            sw.Close();
                            //start async processing
                            ProcessAsynchronousCommand pa = new ProcessAsynchronousCommand();
                            pa.sp = sp;
                            pa.u = u;
                            pa.filename = sp.Directory + "\\data\\birst.generatebulkqueryresults.command";
                            Thread t = new Thread(pa.processCommandAsync);
                            t.IsBackground = true;
                            t.Start();
                            Status.StatusResult result = new Status.StatusResult(Status.StatusCode.Executing);
                            lock (commandfileExecutionStatus)
                            {
                                if (!commandfileExecutionStatus.ContainsKey(sp.Directory + "\\data\\birst.generatebulkqueryresults.command"))
                                {
                                    commandfileExecutionStatus.Add(sp.Directory + "\\data\\birst.generatebulkqueryresults.command", result);
                                }
                            }
                            break;
                        case PHASE_POLL:
                            lock (commandfileExecutionStatus)
                            {
                                if (commandfileExecutionStatus.ContainsKey(sp.Directory + "\\data\\birst.generatebulkqueryresults.command"))
                                {
                                    Status.StatusResult sr = commandfileExecutionStatus[sp.Directory + "\\data\\birst.generatebulkqueryresults.command"];
                                    if (sr.code == Status.StatusCode.Executing)
                                    {
                                        responseString = "Status " + String.Format("{0:000}", 1) + "\r\n";
                                        return (int)Acorn.DavModule.ServerResponseCode.Ok;
                                    }
                                    else if (sr.code == Status.StatusCode.Complete)
                                    {
                                        responseString = sr.message;
                                        commandfileExecutionStatus.Remove(sp.Directory + "\\data\\birst.generatebulkqueryresults.command");
                                        return (int)Acorn.DavModule.ServerResponseCode.Ok;
                                    }
                                }
                                else
                                {
                                    Status.StatusResult srNone = new Status.StatusResult(Status.StatusCode.None);
                                    srNone.message = "No such file";
                                    return (int)Acorn.DavModule.ServerResponseCode.BadRequest;
                                }
                            }
                            break;
                    }
                }
                else
                {
                    List<string> log = new List<string>();
                    int result = processCommand(log);
                    if (result != (int)DavPutResponseCode.None)
                        return result;
                }
            }
            else if (lname.EndsWith("birst.variables"))
            {
                TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
                // 5 Minute timeout
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                ts.Timeout = 5 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);
                string variableStr = null;
                string line;
                while ((line = inputStream.ReadLine()) != null)
                {
                    if (line.ToLower().StartsWith("variables="))
                    {
                        variableStr = line.Substring(10);
                        break;
                    }
                }
                String variables = ts.getVariables(sp.Directory, u.Username, sp.ID.ToString(), variableStr);
                Global.systemLog.Debug("birst.variables for space " + sp.Name + ":" + variables);
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] buff = encoding.GetBytes(variables);
                httpApplication.Response.OutputStream.Write(buff, 0, buff.Length);
            }
            else if (lname.ToUpper().StartsWith("TXN_COMMAND_HISTORY.LIVEACCESS_"))
            {
                int loadNumber = -1;
                string loadgroup = null;
                string[] names = lname.ToUpper().Split('.');
                if (names.Length == 4)
                {
                    loadgroup = names[1];
                    int.TryParse(names[2], out loadNumber);                    
                }
                if (loadNumber == -1)
                    return (int)DavPutResponseCode.BadRequest;

                int result = updateTxnCommandHistory(loadNumber, loadgroup);
                if (result != (int)DavPutResponseCode.None)
                    return result;
            }
            else
            {
                FileInfo fi = new FileInfo(sp.Directory + "\\data\\" + filename);
                if (!OverwriteExistingResource())
                {
                    // Check to see if the resource already exists
                    if (fi.Exists)
                        return (int)DavPutResponseCode.Conflict;
                    else
                        SaveFileOld(fi, -1, -1, -1);                        
                }
                else
                {
                    // begin ugly hacks
                    long segmentSize = this.httpApplication.Request.Headers["Segment-size"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Segment-size"]);
                    int segment = this.httpApplication.Request.Headers["Segment"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Segment"]);
                    int numSegments = this.httpApplication.Request.Headers["Num-segments"] == null ? -1 : int.Parse(this.httpApplication.Request.Headers["Num-segments"]);
                    int version = this.httpApplication.Request.Headers["Version"] == null ? VERSION_OLD : int.Parse(this.httpApplication.Request.Headers["Version"]);
                    int phase = this.httpApplication.Request.Headers["Phase"] == null ? 0 : int.Parse(this.httpApplication.Request.Headers["Phase"]);
                    string hash = this.httpApplication.Request.Headers["Hash"]; // backwards compatibility with old hashing
                    string hash256 = this.httpApplication.Request.Headers["Hash256"];

                    if (version == VERSION_OLD)
                    {
                        SaveFileOld(fi, segment, numSegments, segmentSize); // XXX not sure if this is called anymore (assuming up to date clients)
                    }
                    else if (version == VERSION_NEW)
                    {
                        // new way of uploading files
                        //   phase 1: upload zip file in chunks (1 to N chunks, each 5MB, takes a few seconds each)
                        //   phase 2: kick up unpacking/scanning (immediate response)
                        //   phase 3: check the status of phase 2 (really should be a GET XXX)
                        // XXX really should do real form POSTs, create a servlet and do a proper post (form values for file, phase, version, etc), nothing would 
                        // come this way after that and it would be cleaner - FileUpload.aspx, Scan.aspx, CheckStatus.aspx
                        
                        bool isParallelUpload = this.httpApplication.Request.Headers["isParallelUpload"] == null ? false : bool.Parse(this.httpApplication.Request.Headers["isParallelUpload"]);
                        /*
                         * if isParallelUpload, SaveFile cannot assume that the segments will arrive serially. It has to create folder for file and store all segments into
                         * it as they arrive as <filename>.part[segment] files.
                         * UnpackFile needs to then first merge all partfiles from folder in correct order and save file correctly before starting to unpack them.
                         * For this - PHASE_INIT_PARALLEL_UPLOAD request will first be sent that will clean up any old directory for file and make new directory in place
                         * PHASE_FINISH_PARALLEL_UPLOAD request will be sent after all segments are uploaded in order to merge them to make complete file.
                         */
                        Status.StatusResult sr = null;
                        switch (phase)
                        {
                            case PHASE_INIT_PARALLEL_UPLOAD:
                                {
                                    if (InitUpload(fi) == -1)
                                    {
                                        Global.systemLog.Debug("InitUpload returned -1");
                                        return (int)DavPutResponseCode.BadRequest;
                                    }
                                }
                                return (int)Acorn.DavModule.ServerResponseCode.Ok;
                            case PHASE_UPLOAD:
                                if (SaveFile(fi, segment, numSegments, segmentSize, hash, hash256, isParallelUpload, true) == -1)
                                {
                                    Global.systemLog.Debug("SaveFile returned -1");
                                    return (int)DavPutResponseCode.BadRequest;
                                }                                
                                return (int)Acorn.DavModule.ServerResponseCode.Ok;
                            case PHASE_ASSEMBLE_PARALLEL_UPLOAD:
                                {
                                    AssembleFile(fi, numSegments); // starts a thread that does the assembling, returns immediately
                                    return (int)Acorn.DavModule.ServerResponseCode.Ok;
                                }
                            case PHASE_POLL_FINISH_PARALLEL_UPLOAD:
                                {
                                    sr = CheckAssembleStatus(fi);
                                    if (sr.code == Status.StatusCode.Assembling)
                                        responseString = "Status " + String.Format("{0:000}", 1) + "\r\n";
                                    else if (sr.code == Status.StatusCode.Failed || sr.code == Status.StatusCode.Complete)
                                        responseString = sr.message;
                                    return (int)Acorn.DavModule.ServerResponseCode.Ok;
                                }
                            case PHASE_UNPACK:
                                UnpackFile(fi); // starts a thread that does the unzipping and scanning, returns immediately
                                return (int)Acorn.DavModule.ServerResponseCode.Ok;
                            case PHASE_POLL:
                            	sr = CheckUnpackStatus(fi);
                                if (sr.code == Status.StatusCode.Unpacking)
                                    responseString = "Status " + String.Format("{0:000}", 1) + "\r\n";
                                else if (sr.code == Status.StatusCode.Failed || sr.code == Status.StatusCode.Complete)
                                    responseString = sr.message;
                                return (int) Acorn.DavModule.ServerResponseCode.Ok;
                            default:
                                Global.systemLog.Warn("unexpected upload phase type: " + phase);
                                return (int)DavPutResponseCode.BadRequest;
                        }
                    }
                    else
                    {
                        Global.systemLog.Warn("unexpected upload version number: " + version);
                        return (int)DavPutResponseCode.BadRequest;
                    }
                }
            }
            return (int)Acorn.DavModule.ServerResponseCode.Ok;
        }

        #endregion

        #region private handler methods

        private int updateTxnCommandHistory(int loadNumber, string loadGroup)
        {
            StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);
            StringBuilder response = new StringBuilder();
            QueryConnection conn = null;
            try
            {
                bool isIB = Database.isDBTypeIB(sp.ConnectString);
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (!Database.tableExists(conn, sp.Schema, Status.tableName))
                {
                    Database.createTxnCommandHistoryTable(conn, sp, isIB);
                }
                else
                {
                    DataTable dt = null;
                    if (isIB)
                        dt = conn.GetSchema("Columns", new string[] { sp.Schema, null, Status.tableName });
                    else
                        dt = conn.GetSchema("Columns", new string[] { null, sp.Schema, Status.tableName });
                    if (dt != null && !dt.Columns.Contains("PROCESSINGGROUP"))
                    {
                        Database.addProcessingGroupToTxnCommandHistory(sp);                        
                    }
                }
                QueryCommand cmd = null;
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + sp.Schema + "." + Status.tableName +
                    " WHERE ITERATION=" + loadNumber + " AND SUBSTEP LIKE ('" + loadGroup + "%')";
                int rowsDeleted = cmd.ExecuteNonQuery();
                cmd.Dispose();
                Global.systemLog.Debug(rowsDeleted + " rows deleted from " + sp.Schema + "." + Status.tableName + " for loadGroup = " + loadGroup + " and loadNumber = " + loadNumber);

                string line = null;
                int rowsInserted = 0;
                while ((line = inputStream.ReadLine()) != null)
                {
                    string[] row = line.Split('|');
                    if (row.Length < 11)
                        throw new Exception("Invalid format for TXN_COMMAND_HISTORY uploaded.");
                    object[] rowObj = new object[row.Length];
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i].ToString() != "")
                            rowObj[i] = row[i];
                    }
                    Database.addTxnCommandHistory(conn, isIB, sp.Schema, Status.tableName, loadGroup, rowObj);
                    rowsInserted++;
                }
                Global.systemLog.Debug(rowsInserted + " rows inserted into " + sp.Schema + "." + Status.tableName + " for loadGroup = " + loadGroup + " and loadNumber = " + loadNumber);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in updating TXN_COMMAND_HISTORY: " + ex.Message, ex);
                response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                response.Append("Exception in updating TXN_COMMAND_HISTORY: " + ex.Message);
                responseString = response.ToString();
                return -1;
            }
            finally
            {
                if (inputStream != null)
                {
                    try
                    {
                        inputStream.Close();
                    }
                    catch (Exception) { /* do nothing */}
                }
                ConnectionPool.releaseConnection(conn);
            }
            return (int)DavPutResponseCode.None;
        }
        private int process()
        {
            StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);
            string line = null;
            DateTime loadDate = DateTime.Now;
            bool createAutoDashboards = false;
            string targetEmail = u.Email;
            string[] subgroups = null;
            string dcconfigFileName = null;
            while ((line = inputStream.ReadLine()) != null)
            {
                if (line.ToLower().StartsWith("loaddate="))
                {
                    DateTime result;
                    if (DateTime.TryParse(line.Substring(9), out result))
                    {
                        loadDate = result;
                    }
                }
                if (line.ToLower().StartsWith("createquickdashboards="))
                {
                    bool result;
                    if (Boolean.TryParse(line.Substring(22), out result))
                    {
                        createAutoDashboards = result;
                    }
                }
                if (line.ToLower().StartsWith("email="))
                {
                    targetEmail = line.Substring(6);
                }
                if (line.ToLower().StartsWith("subgroups="))
                {
                    subgroups = line.Substring(10).Split('\t');
                }
                if (line.ToLower().StartsWith("dcconfigfilename="))
                {
                    dcconfigFileName = line.Substring(17);
                }                
            }
            inputStream.Close();

            // Execute processing asynchronously
            ExecutePublish ep = new ExecutePublish(sp, loadDate, createAutoDashboards, u, httpApplication.Request.MapPath("~/"), targetEmail, subgroups, dcconfigFileName);
            BackgroundProcess.startModule(new BackgroundTask(ep, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));

            return (int)DavPutResponseCode.None;
        }

        private int getRealtimeConnectionStatus()
        {
            StreamReader inputStream = null;
            try
            {
                inputStream = new StreamReader(httpApplication.Request.InputStream);
                string line = null;
                string visiblename = null;
                string name = null;
                string operation = null;
                StringBuilder response = new StringBuilder();
                while ((line = inputStream.ReadLine()) != null)
                {
                    if (line.ToLower().StartsWith("operation="))
                        operation =  line.Substring(10);
                    else if (line.ToLower().StartsWith("visiblename="))
                        visiblename = line.Substring(12);
                    else if (line.ToLower().StartsWith("name="))
                        name = line.Substring(5);
                }
                if (operation != null)
                {
                    int responseCode = 1;
                    MainAdminForm maf = Util.getRepositoryDev(sp, u);
                    if (operation.ToLower().Equals("addnew"))
                    {
                        bool visiblenameExist = maf.hasConnectionWithVisibleName(visiblename);
                        responseCode = visiblenameExist ? 1 : 0;
                        if (!visiblenameExist)
                        {
                            //trying to keep name and visible name same for the connection
                            string newname = maf.isConnectionExist(visiblename) ? visiblename + "_" + DateTime.Now.ToString("MMddyyyyHHmmssfff") : visiblename;
                            response.Append("name=" + newname + "\r\n");
                        }
                    }
                    else if (operation.ToLower().Equals("edit"))
                    {
                        bool visiblenameExist = maf.hasConnectionWithVisibleName(visiblename);
                        if (visiblenameExist)
                        {
                            string cname = maf.getConnectionName(visiblename);
                            if (cname != null && cname == name)
                                responseCode = 0;
                        }
                        else
                        {
                            responseCode = 0;
                        }
                    }
                    else if (operation.ToLower().Equals("delete"))
                    {
                        bool isConnectionExist = maf.isConnectionExist(name);
                        if (isConnectionExist)
                            responseCode = maf.isConnectionInUse(name) ? 1 : 0;
                        else
                            responseCode = 0;
                    }
                    response.Append("Status " + String.Format("{0:000}", responseCode) + "\r\n");
                    System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                    byte[] buff = encoding.GetBytes(response.ToString());
                    httpApplication.Response.OutputStream.Write(buff, 0, buff.Length);  
                }
            }
            finally
            {
                if (inputStream != null)
                {
                    try
                    {
                        inputStream.Close();
                    }
                    catch (Exception) { /* do nothing */}
                }
            }
            return (int)DavPutResponseCode.None;
        }

        private int getEncryptionKey()
        {
            StringBuilder response = new StringBuilder();
            response.Append("encryptionkey=" + Performance_Optimizer_Administration.MainAdminForm.SecretPassword + "\r\n");
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] buff = encoding.GetBytes(response.ToString());
            httpApplication.Response.OutputStream.Write(buff, 0, buff.Length);
            return (int)DavPutResponseCode.None;
        }

        public class ExtractSFDC : BackgroundModule
        {
            Space sp;
            User u;
            MainAdminForm maf;
            SalesforceSettings settings;
            sforce.SforceService service;
            private Dictionary<BackgroundModuleObserver, object> observers;
            private bool usedExternalScheduler = false;
            private string exceptionMsg;

            public ExtractSFDC(Space sp, User u, sforce.SforceService service, SalesforceSettings settings)
            {
                this.maf = Util.getRepositoryDev(sp, u);
                this.sp = sp;
                this.u = u;
                this.service = service;
                this.settings = settings;
                observers = new Dictionary<BackgroundModuleObserver, object>();
            }


            public bool didUseExternalScheduler()
            {
                return usedExternalScheduler;
            }

            public void run()
            {

                try
                {
                    if (Util.useExternalSchedulerExtract(sp))
                    {
                        usedExternalScheduler = true;
                        Acorn.Utils.SchedulerUtils.runNowScheduleExtract(sp.ID, u.ID, false);
                    }
                    else
                    {
                        string connectorName = "sfdc";
                        if (ConnectorUtils.UseNewConnectorFramework())
                        {
                            connectorName = AddSalesforce.getSalesForceConnectorString(sp, u);
                        }
                        ConnectorUtils.startExtractWithoutScheduler(null, u, sp, maf, connectorName, SchedulerUtils.getStartExtractOptionsXml(false).ToString(), null);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while running extractSFDC : " + ex.Message, ex);
                    exceptionMsg = ex.Message != null && ex.Message.Trim().Length > 0 ? ex.Message : "Error";
                }
            }

            public virtual void kill()
            { 
            }

            public virtual void finish()
            {
                if (!usedExternalScheduler)
                {
                    callObserversWhenDone();
                }
            }

            public void callObserversWhenDone()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            #region BackgroundModule Members


            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }

            public Status.StatusResult getStatus()
            {
                Status.StatusResult result = null;
                if (Util.isSFDCExtractStatusFilePresent(sp))
                {
                    result = new Status.StatusResult(Status.StatusCode.SFDCExtract);
                }
                else if (exceptionMsg != null && exceptionMsg.Length > 0)
                {
                    if (usedExternalScheduler)
                    {
                        callObserversWhenDone();
                    }

                    result = new Status.StatusResult(Status.StatusCode.Failed);
                }
                else
                {
                    // if it is complete and external scheduler was used
                    if (usedExternalScheduler)
                    {
                        Status.SpaceStatus spaceStatus = Acorn.Utils.SchedulerUtils.getCurrentStatus(sp, -1, null);
                        if (spaceStatus.available && Status.isExtractStep(spaceStatus.stepName))
                        {
                            int status = spaceStatus.stepStatus;
                            if (status == Status.FAILED)
                            {
                                result = new Status.StatusResult(Status.StatusCode.Failed);
                            }
                            else
                            {
                                result = getExtractStatus();
                            }

                            callObserversWhenDone();
                        }
                    }
                    else
                    {
                        result = getExtractStatus();
                    }
                }

                return result;
            }


            private Status.StatusResult getExtractStatus()
            {
                int resultCode = ConnectorUtils.getExtractStatusViaFileName(sp, "sfdc");
                if (resultCode == Status.FAILED)
                {
                    return new Status.StatusResult(Status.StatusCode.Failed);
                }
                else
                {
                    return new Status.StatusResult(Status.StatusCode.Complete);
                }
            }
            #endregion
        }


        public class ExecutePublish : BackgroundModule
        {
            Space sp;
            DateTime loadDate;
            bool createAutoDashboards;
            User u;
            MainAdminForm maf;
            string path;
            string email;
            string[] subgroups;
            string dcconfigFileName;
            protected bool backgroundRequest = false;
            protected bool sendEmail = true;
            private Dictionary<BackgroundModuleObserver, object> observers;
            private bool usedExternalScheduler = false;
            private string loadGroup = null;
            int loadNumber = -1; // computed during the run
            bool error = false;

            public ExecutePublish(Space sp, DateTime loadDate, bool createAutoDashboards, User u, string path, string email, string[] subgroups, string dcconfigFileName) 
                : this(sp, loadDate, createAutoDashboards, u, path, email, subgroups, dcconfigFileName, Util.LOAD_GROUP_NAME)
            {   
            }

            public ExecutePublish(Space sp, DateTime loadDate, bool createAutoDashboards, User u, string path, string email, string[] subgroups, string dcconfigFileName, string loadGroup)
                : this(sp, loadDate, createAutoDashboards, u, Util.getRepositoryDev(sp, u), path, email, subgroups, dcconfigFileName, loadGroup)
            {
            }

            public ExecutePublish(Space sp, DateTime loadDate, bool createAutoDashboards, User u, MainAdminForm maf, string path, string email, string[] subgroups, string dcconfigFileName, string loadGroup)
            {   
                this.sp = sp;
                this.loadDate = loadDate;
                this.createAutoDashboards = createAutoDashboards;
                this.u = u;
                this.maf = maf;
                this.path = path;
                this.email = email;
                this.subgroups = subgroups;
                this.dcconfigFileName = dcconfigFileName;
                this.loadGroup = loadGroup;
                observers = new Dictionary<BackgroundModuleObserver, object>();
            }

            public Space getSpace()
            {
                return sp;
            }

            public bool didUseExternalScheduler()
            {
                return usedExternalScheduler;
            }

            private MailMessage getMailMessage()
            {
                string body = ProcessData.getProcessEmailBody(path);
                MailMessage mm = ProcessData.getMailMessage(sp, u.Email, body);
                return mm;
            }

            private bool validateSpaceAvailable()
            {
                
                int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);                
                
                if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                {                    
                    string message = SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID);
                    Global.systemLog.Error("Unable to start publish data: " + message);                   
                    MailMessage mm = ProcessData.fixUpMessageOnProcessFailure(getMailMessage(), true, false, true, false, false, false, sp);
                    ProcessData.sendSecureEmailIfNeeded(mm);
                    return false;
                }
                
                if(Util.checkForPublishingStatus(sp))
                {
                    Global.systemLog.Error("Space is already publishing" + sp.ID);                    
                    MailMessage mm = ProcessData.fixUpMessageOnProcessFailure(getMailMessage(), true, true, false, false, false, false, sp);
                    ProcessData.sendSecureEmailIfNeeded(mm);
                    return false;
                }
                
                return true;
            }

            public void run()
            {
                try
                {
                    if (u != null)
                        log4net.ThreadContext.Properties["user"] = u.Username;                    
                    Util.setLogging(u, sp);
                    if (!validateSpaceAvailable())
                    {
                        Global.systemLog.Error("Space is not available for processing");
                        error = true;
                        return;
                    }
                    int currentLoadNumber = ProcessLoad.getLoadNumber(sp, loadGroup);
                    loadNumber = currentLoadNumber + 1;
                    if (Util.useExternalSchedulerForProcess(sp, maf))
                    {   
                        ApplicationLoader al = new ApplicationLoader(maf, sp, loadDate, ProcessData.mainSchema, Util.getEngineCommand(sp), ProcessData.createtimecmd,
                            ProcessData.contentDir, null, u, loadGroup, loadNumber);
                        if (subgroups != null && subgroups.Length > 0)
                            al.SubGroups = subgroups;
                        // This ensures that the txn_command_history table gets cleaned out in case of "Failed" scneario
                        // Also dashboards are build for the live access connection. It doesn't kick off the processing which is done through the External Scheduler
                        al.load(createAutoDashboards, null, false, false, false, false);
                        Acorn.Utils.SchedulerUtils.schedulePublishJob(sp, u, createAutoDashboards, loadNumber, loadGroup, loadDate, subgroups, al.retryFailedLoad, sendEmail);
                        usedExternalScheduler = true;
                    }
                    else
                    {
                        ProcessData.Process(maf, sp, u, email, path, loadDate, loadGroup, currentLoadNumber, createAutoDashboards, subgroups, backgroundRequest, sendEmail);
                    }
                    if (File.Exists(sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + dcconfigFileName))
                        File.SetLastWriteTime(sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + dcconfigFileName, DateTime.Now);
                }
                catch (Exception ex)
                {
                    error = true;
                    Global.systemLog.Warn(ex);
                }
            }

            public virtual void kill()
            {
                /*
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.killed(kvp.Value);
                }
                 */
            }

            public virtual void finish()
            {
                if (!usedExternalScheduler)
                {
                    callObserversWhenDone();
                }
            }

            public void callObserversWhenDone()
            {
                foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
                {
                    kvp.Key.finished(kvp.Value);
                }
            }

            #region BackgroundModule Members


            public void addObserver(BackgroundModuleObserver observer, object o)
            {
                observers.Add(observer, o);
            }

            public Status.StatusResult getStatus()
            {
                if (error)
                {
                    return new Status.StatusResult(Status.StatusCode.Failed);
                }
                Status.StatusResult result = null;
                if (usedExternalScheduler)
                {
                    result = Status.getStatusUsingExternalScheduler(sp, loadNumber, loadGroup, true);
                    bool fetchStatusFromDB = result != null && Status.isRunningCode(result.code) ? false : true;
                    if (fetchStatusFromDB)
                    {
                        result = Status.getLoadStatus(null, sp, loadNumber, loadGroup);
                    }
                    result = filterStatusResult(result);
                    if(result !=null && !Status.isRunningCode(result.code))
                    {
                        callObserversWhenDone();
                    }
                }
                else
                {
                    result = Status.getLoadStatus(null, sp, loadNumber, loadGroup);
                }
                
				return result;
            }

            /// <summary>
            /// This hides the status like "Ready, None" and instead sends Complete status
            /// </summary>
            /// <param name="result"></param>
            /// <returns></returns>
            private Status.StatusResult filterStatusResult(Status.StatusResult result)
            {
                if (result != null && !Status.isRunningCode(result.code)) 
                {   
                    result.code = (result.code == Status.StatusCode.Complete || result.code == Status.StatusCode.Failed) ? result.code : Status.StatusCode.Complete;
                    return result;
                }
                else
                {
                    return result;
                }
            }

            #endregion
        }

        private bool OverwriteExistingResource()
        {
            if (this.httpApplication.Request.Headers["If-None-Match"] != null)
                return false;

            return true;
        }

        private byte[] GetRequestInput()
        {
            string req = httpApplication.Request.Headers["Content-Encoding"];
            if (req == "gzip")
                return GetRequestInputGZIP();

            StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);

            long inputSize = inputStream.BaseStream.Length;

            byte[] inputBytes = new byte[inputSize];
            inputStream.BaseStream.Read(inputBytes, 0, (int)inputSize);
            return inputBytes;
        }

        private byte[] GetRequestInputGZIP()
        {
            GZipStream inputStream = new GZipStream(httpApplication.Request.InputStream, CompressionMode.Decompress, true);
            using (MemoryStream mem = new MemoryStream())
            {
                byte[] data = new byte[16384];
                int size = 0;
                while ((size = inputStream.Read(data, 0, 16384)) > 0)
                {
                    mem.Write(data, 0, size);
                }
                return mem.ToArray();
            }
        }

        private int SaveFileOld(FileInfo fi, int segment, int numSegments, long segmentSize)
        {
            Global.systemLog.Debug("SaveFileOld: " + fi.Name + ", segment: " + segment + ", numSegments: " + numSegments + ", segmentSize: " + segmentSize);
            byte[] requestInput = GetRequestInput();
            FileStream fs = null;
            FileInfo partFi = new FileInfo(fi.FullName + ".part");
            Global.systemLog.Debug("Request to write " + requestInput.Length + " bytes to the file " + partFi.Name);
            StringBuilder response = new StringBuilder();
            try
            {
                try
                {
                    if (segment <= 0 || segmentSize <= 0)
                    {
                        if (partFi.Exists)
                            partFi.Delete();
                        fs = partFi.Create();
                    }
                    else
                    {
                        fs = partFi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                        fs.Seek(segment * segmentSize, SeekOrigin.Begin);
                    }
                    fs.Write(requestInput, 0, requestInput.Length);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }
                if (segment >= 0 && numSegments > 0 && segment < numSegments - 1) // still have more segments to receive
                    return 0;

                // finished the file, rename it
                if (fi.Exists)
                    fi.Delete();
                partFi.MoveTo(fi.FullName);
                Global.systemLog.Debug("Finished saving the file " + fi.Name);
                if (fi.Name.ToLower() == "birst.upload")
                    return 0;
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("Exception in SaveFileOld: " + ex.Message, ex);
                response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                response.Append("Exception in SaveFileOld: " + ex.Message);
                responseString = response.ToString();
                return -1;
            }
               
            // 'upload' the files
            Global.systemLog.Debug("Unpack: " + fi.Name);
            try
            {
             
                int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);
                if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                {
                    string message = SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID);
                    Global.systemLog.Error("Error in SaveFileOld: " + message);
                    response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                    response.Append(message + "\r\n");
                }
                if (Util.checkForPublishingStatus(sp))
                {
                    Global.systemLog.Error("Space is already publishing" + sp.ID);
                    response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                    response.Append("Space is already publishing" + "\r\n");
                }

                errorTable = UploadFile(fi);
                int returnValue = (errorTable.Rows.Count == 0) ? 0 : -1; ;
                if (returnValue == -1)
                {
                    returnValue = 0;
                    foreach(DataRow dr in errorTable.Rows)
                    {
                        if ((string) dr[1] == ApplicationUploader.ERROR_STR)
                        {
                            returnValue = -1;
                            break;
                        }
                    }
                }
                response.Append("Status " + String.Format("{0:000}", returnValue) + "\r\n");
                if (returnValue == -1)
                {
                    Global.systemLog.Debug("SaveFileOld failed due to records in the errorTable:");
                    response.Append("SaveFileOld failed due to records in the errorTable:\r\n");
                    for (int i = 0; i < errorTable.Rows.Count; i++)
                    {
                        DataRow dr = errorTable.Rows[i];
                        if ((string)dr[1] == ApplicationUploader.ERROR_STR)
                        {
                            Global.systemLog.Debug(dr[0] + ": " + dr[1] + ": " + dr[2]);
                            response.Append(dr[0] + ": " + dr[1] + ": " + dr[2] + "\r\n");
                        }
                    }
                }
                else
                {
                    response.Append("Successfully unpacked/scanned " + fi.FullName + "\r\n");
                    for (int i = 0; i < errorTable.Rows.Count; i++)
                    {
                        DataRow dr = errorTable.Rows[i];
                        Global.systemLog.Debug(dr[0] + ": " + dr[1] + ": " + dr[2]);
                        response.Append(dr[0] + ": " + dr[1] + ": " + dr[2] + "\r\n");
                    }
                }
                responseString = response.ToString();
                return returnValue;
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("Exception in SaveFileOld: " + ex.Message, ex);
                response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                response.Append("Exception in SaveFileOld: " + ex.Message);
                responseString = response.ToString();
                return -1;
            }
        }

        private DataTable UploadFile(FileInfo fi)
        {
            bool firstRowHasHeaders = true;
            int beginSkip = 0;
            int endSkip = 0;
            bool consolidateLikeSources = true;
            bool lockformat = false;
            bool ignoreinternalquotes = false;
            bool forcenumcolumns = false;
            bool automodel = false;
            string encoding = null;
            string replacements = null;
            string quotechar = null;
            string separator = null;
            if (File.Exists(sp.Directory + "//data//birst.upload"))
            {
                StreamReader reader = new StreamReader(sp.Directory + "//data//birst.upload", Encoding.UTF8);
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.ToLower().StartsWith("firstrowhasheaders="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(19), out result))
                        {
                            firstRowHasHeaders = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("beginskip="))
                    {
                        int result;
                        if (int.TryParse(line.Substring(10), out result))
                        {
                            beginSkip = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("endskip="))
                    {
                        int result;
                        if (int.TryParse(line.Substring(8), out result))
                        {
                            endSkip = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("consolidatelikesources="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(23), out result))
                        {
                            consolidateLikeSources = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("encoding="))
                    {
                        encoding = line.Substring(9);
                    }
                    else if (line.ToLower().StartsWith("lockformat="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(11), out result))
                        {
                            lockformat = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("ignoreinternalquotes="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(21), out result))
                        {
                            ignoreinternalquotes = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("forcenumcolumns="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(16), out result))
                        {
                            forcenumcolumns = result;
                        }
                    }
                    else if (line.ToLower().StartsWith("quotechar="))
                    {
                        quotechar = line.Substring(10);
                    }
                    else if (line.ToLower().StartsWith("replacements="))
                    {
                        replacements = line.Substring(13);
                    }
                    else if (line.ToLower().StartsWith("separator="))
                    {
                        separator = line.Substring(10);
                        if (separator != null && separator.Length == 0)
                        {
                            separator = null;

                        }
                    }
                    else if (line.ToLower().StartsWith("automodel="))
                    {
                        bool result;
                        if (Boolean.TryParse(line.Substring(10), out result))
                        {
                            automodel = result;
                        }
                    }
                }
                reader.Close();
            }
            MainAdminForm maf = Util.getRepositoryDev(sp, u);
            ApplicationUploader uploader =
                new ApplicationUploader(maf, sp, fi.Name, firstRowHasHeaders, lockformat, ignoreinternalquotes, Util.getEscapes(replacements), forcenumcolumns, quotechar, u,
                    null, beginSkip, endSkip, false, encoding, separator, automodel);
            errorTable = uploader.uploadFile(consolidateLikeSources, false);
            Util.saveApplication(maf, sp, null, u);
            return errorTable;
        }

        private int InitUpload(FileInfo fi)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Path.Combine(fi.DirectoryName, (fi.Name.Substring(0, fi.Name.Length - (fi.Extension == null ? 0 : fi.Extension.Length)))));
                if (di.Exists)
                    di.Delete(true);
                di.Create();
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("Exception in InitUpload: " + ex.Message, ex);
                return -1;
            }
            return 0;
        }

        private int SaveFile(FileInfo fi, int segment, int numSegments, long segmentSize, string hash, string hash256, bool isParallelUpload, bool checkAvailabilityOnCompletion)
        {
            Global.systemLog.Debug("SaveFile: " + fi.Name + ", segment: " + segment + ", numSegments: " + numSegments + ", segmentSize: " + segmentSize + ", hash: " + hash + ", hash256: " + hash256 + ", isParallelUpload: " + isParallelUpload);
            StringBuilder response = new StringBuilder();
            try
            {
                byte[] requestInput = GetRequestInput();

                // calculate hash and check it, fail-safe if the hash is not found in the header
                if (hash256 != null)
                {
                    HashAlgorithm sha256 = null;
                    try
                    {
                        sha256 = new SHA256CryptoServiceProvider();
                    }
                    catch (PlatformNotSupportedException)
                    {
                        sha256 = new SHA256Managed();
                    } 
                    string serverHash = BitConverter.ToString(sha256.ComputeHash(requestInput)).Replace("-", "").ToLower();
                    if (serverHash != hash256)
                    {
                        Global.systemLog.Warn("Client and server hash values (SHA-256) do not match; client: " + hash256 + ", server: " + serverHash);
                        throw new Exception("Client and server hash values (SHA-256) do not match; client: " + hash256 + ", server: " + serverHash);
                    }
                }
                else if (hash != null) // backwards compatibility
                {
                    SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
                    string serverHash = BitConverter.ToString(sha1.ComputeHash(requestInput)).Replace("-", "").ToLower();
                    if (serverHash != hash)
                    {
                        Global.systemLog.Warn("Client and server hash values (SHA-1) do not match; client: " + hash + ", server: " + serverHash);
                        throw new Exception("Client and server hash values (SHA-1) do not match; client: " + hash + ", server: " + serverHash);
                    }
                }
                else
                {
                    Global.systemLog.Warn("No hash value found in the client header");
                }

                if (!isParallelUpload)
                {
                    FileStream fs = null;
                    FileInfo partFi = new FileInfo(fi.FullName + ".part");
                    Global.systemLog.Debug("Request to write " + requestInput.Length + " bytes to the file " + partFi.Name);

                    try
                    {
                        if (segment <= 0 || segmentSize <= 0)
                        {
                            if (partFi.Exists)
                                partFi.Delete();
                            fs = partFi.Create();
                        }
                        else
                        {
                            // sanity check, segments should come in sequence - if not, one was lost (network or client issue)
                            if ((segment * segmentSize) > partFi.Length)
                            {
                                Global.systemLog.Warn("Seek position not at the end of the zip file; seek = " + segment * segmentSize + ", size = " + partFi.Length);
                                throw new Exception("Seek position not at the end of the zip file; seek = " + segment * segmentSize + ", size = " + partFi.Length);
                            }
                            fs = partFi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                            fs.Seek(segment * segmentSize, SeekOrigin.Begin);
                        }
                        fs.Write(requestInput, 0, requestInput.Length);
                    }
                    finally
                    {
                        if (fs != null)
                            fs.Close();
                    }
                    if (segment >= 0 && numSegments > 0 && segment < numSegments - 1) // still have more segments to receive
                        return 0;

                    if (checkAvailabilityOnCompletion && !isFileBirstUploadOptions(fi.Name))
                    {
                        // check if there are any other op or space processing going on
                        string errorMessage = checkSpaceBusyInOtherOp(sp);
                        if (errorMessage == null || errorMessage.Trim().Length == 0)
                        {
                            errorMessage = checkSpaceProcessing(sp);
                        }
                        if (errorMessage != null && errorMessage.Trim().Length > 0)
                        {
                            // if an error happens, throw an error and do not proceed to move the part file to original file location
                            Global.systemLog.Error("Space is not available : " + errorMessage);
                            // delete the uploaded part
                            partFi.Delete();
                            // kinda hack to pass the status back properly to birst connect client
                            // if an exception is thrown here, birst connect considers it as just a bad request which is incorrect.
                            // Hence properly formatting the error and signalling the response as OK
                            response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                            response.Append("Exception in SaveFile: " + errorMessage);
                            responseString = response.ToString();
                            return 0;
                        }
                    }
                    // finished the file, rename it
                    if (fi.Exists)
                        fi.Delete();
                    partFi.MoveTo(fi.FullName);
                    Global.systemLog.Debug("Finished saving the file " + fi.Name);
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(Path.Combine(fi.DirectoryName, (fi.Name.Substring(0, fi.Name.Length - (fi.Extension == null ? 0 : fi.Extension.Length)))));
                    if (!di.Exists)
                    {
                        Global.systemLog.Warn("Directory for storing segments - " + di.FullName + " does not exist");
                        throw new Exception("Directory for storing segments - " + di.FullName + " does not exist");
                    }
                    FileStream fs = null;
                    FileInfo partFi = new FileInfo(Path.Combine(di.FullName, fi.Name + ".part." + segment));
                    if (partFi.Exists)
                    {
                        Global.systemLog.Warn("PartFile - " + partFi.FullName + " already exists");
                        throw new Exception("PartFile - " + partFi.FullName + " already exists");
                    }
                    try
                    {
                        fs = partFi.Create();
                        fs.Write(requestInput, 0, requestInput.Length);
                    }
                    finally
                    {
                        if (fs != null)
                            fs.Close();
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("Exception in SaveFile: " + ex.Message, ex);
                response.Append("Status " + String.Format("{0:000}", -1) + "\r\n");
                response.Append("Exception in SaveFile: " + ex.Message);
                responseString = response.ToString();
                DataTable err = ApplicationUploader.getNewErrorTable();
                DataRow dr = err.NewRow();
                dr[0] = fi.Name;
                dr[1] = "Exception in SaveFile: " + ex.Message;
                err.Rows.Add(dr);
                return -1;
            }
        }

        private bool isFileBirstUploadOptions(string fileName)
        {
            return fileName != null && fileName.EndsWith("birst.upload");
        }

        public static string checkSpaceBusyInOtherOp(Space sp)
        {            
            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);
            if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
            {
                return SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID);                
            }
            return null;
        }

        public static string checkSpaceProcessing(Space sp)
        {
            if (Util.checkForPublishingStatus(sp))
            {
                return "Space is already publishing";
            }
            return null;
        }

        private class AssembleParallelUploadFile
        {
            public FileInfo fi;
            public int numSegments;
            public Space sp;

            public void assembleFile()
            {
                bool isComplete = false;
                FileStream fos = null;
                string errorMessage = null;
                try
                {
                    Global.systemLog.Debug("Assemble: " + fi.Name);
                    DirectoryInfo di = new DirectoryInfo(Path.Combine(fi.DirectoryName, (fi.Name.Substring(0, fi.Name.Length - (fi.Extension == null ? 0 : fi.Extension.Length)))));
                    if (!di.Exists)
                    {
                        Global.systemLog.Warn("Directory for storing segments - " + di.FullName + " does not exist");
                        throw new Exception("Directory for storing segments - " + di.FullName + " does not exist");
                    }

                    // check if there are any other op or space processing going on
                    errorMessage = checkSpaceBusyInOtherOp(sp);
                    if (errorMessage == null || errorMessage.Trim().Length == 0)
                    {
                        errorMessage = checkSpaceProcessing(sp);
                    }
                    if (errorMessage != null && errorMessage.Trim().Length > 0)
                    {
                        // if an error happens, throw an error and do not proceed to move the part file to original file location
                        Global.systemLog.Error("Space is not available : " + errorMessage);
                        di.Delete(true);
                        return;
                    }

                    if (fi.Exists)
                        fi.Delete();
                    fos = fi.Create();
                    for (int i = 0; i < numSegments; i++)
                    {
                        FileInfo partFi = new FileInfo(Path.Combine(di.FullName, fi.Name + ".part." + i));
                        if (!partFi.Exists)
                        {
                            Global.systemLog.Warn("PartFile - " + partFi.FullName + " does not exist");
                            throw new Exception("PartFile - " + partFi.FullName + " does not exist");
                        }
                        FileStream fis = partFi.OpenRead();
                        byte[] buffer = new byte[10000];
                        int bytesRead = 0;
                        while ((bytesRead = fis.Read(buffer, 0, 10000)) > 0)
                        {
                            fos.Write(buffer, 0, bytesRead);
                        }
                        fos.Flush();
                        fis.Close();
                    }

                    di.Delete(true);
                    isComplete = true;
                }
                catch (Exception ex)
                {
                    Global.systemLog.Debug("Exception in assembleFile: " + ex.Message, ex);
                }
                finally
                {
                    if (fos != null)
                        fos.Close();
                    if (fileAssembleStatus.ContainsKey(fi.FullName))
                    {
                        Status.StatusResult status = new Status.StatusResult(isComplete ? Status.StatusCode.Complete : Status.StatusCode.Failed);
                        if (isComplete)
                        {
                            status.message = "Status " + String.Format("{0:000}", 0) + "\r\n";
                            status.message += "Assembling file " + fi.Name + " completed successfully";
                        }
                        else
                        {
                            status.message = "Status " + String.Format("{0:000}", -1) + "\r\n";
                            status.message += "Assembling file " + fi.Name + " failed. ";
                            if (errorMessage != null && errorMessage.Trim().Length > 0)
                            {
                                status.message += errorMessage;
                            }
                        }
                        lock (fileAssembleStatus)
                        {
                            fileAssembleStatus[fi.FullName] = status;
                        }
                    }
                }
            }
        }

        // start the assembling thread
        private void AssembleFile(FileInfo fi, int numSegments)
        {
            Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Assembling);
            lock (fileAssembleStatus)
            {
                if (fileAssembleStatus.ContainsKey(fi.FullName))
                {
                    // double post, ignore
                    return;
                }
                fileAssembleStatus.Add(fi.FullName, sr);
            }
            AssembleParallelUploadFile apf = new AssembleParallelUploadFile();
            apf.fi = fi;
            apf.numSegments = numSegments;
            apf.sp = sp;
            Thread t = new Thread(apf.assembleFile);
            t.IsBackground = true;
            t.Start();            
        }

        private Status.StatusResult CheckAssembleStatus(FileInfo fi)
        {
            Global.systemLog.Debug("CheckAssembleStatus request for " + fi.FullName);
            lock (fileAssembleStatus)
            {
                if (fileAssembleStatus.ContainsKey(fi.FullName))
                {
                    Status.StatusResult sr = fileAssembleStatus[fi.FullName];
                    if (sr.code != Status.StatusCode.Assembling)
                        fileAssembleStatus.Remove(fi.FullName); // no longer assembling, succeeded for failed, remove the entry
                    return sr;
                }
            }
            // no entry, bad file or already received a failure/success response
            Status.StatusResult srNone = new Status.StatusResult(Status.StatusCode.None);
            srNone.message = "No such file";
            return srNone;
        }

        // start the unpacking/scanning thread
        private void UnpackFile(FileInfo fi)
        {
            Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Unpacking);
            lock (fileUploadStatus)
            {
                if (fileUploadStatus.ContainsKey(fi.FullName))
                {
                    // double post, ignore
                    return;
                }
                fileUploadStatus.Add(fi.FullName, sr);
            }

            UnpackDataThread udt = new UnpackDataThread(sp, u, fi, fileUploadStatus);
            BackgroundProcess.startModule(new BackgroundTask(udt, 120 * 60 * 1000)); // 120 minute timeout on the unpack/scan
        }

        private Status.StatusResult CheckUnpackStatus(FileInfo fi)
        {
            Global.systemLog.Debug("CheckUnpackStatus request for " + fi.FullName);
            lock (fileUploadStatus)
            {
                if (fileUploadStatus.ContainsKey(fi.FullName))
                {
                    Status.StatusResult sr = fileUploadStatus[fi.FullName];
                    if (sr.code != Status.StatusCode.Unpacking)
                        fileUploadStatus.Remove(fi.FullName); // no longer unpacking, succeeded for failed, remove the entry
                    return sr;
                }
            }
            // no entry, bad file or already received a failure/success response
            Status.StatusResult srNone = new Status.StatusResult(Status.StatusCode.None);
            srNone.message = "No such file";
            return srNone;
        }

        private int getDataConductorSettings(string filename)
        {
            byte[] requestInput = GetRequestInput();
            FileStream fs = null;
            string dcTargetDir = sp.Directory + "\\" + Util.DCCONFIG_DIR;
            string dcTargetFile = dcTargetDir + "\\" + filename;
            try
            {
                if (!Directory.Exists(dcTargetDir))
                    Directory.CreateDirectory(dcTargetDir);
                FileInfo fi = new FileInfo(dcTargetFile);
                if (fi.Exists)
                    fi.Delete();
                fs = fi.Create();
                fs.Write(requestInput, 0, requestInput.Length);
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(dcTargetFile, ex);
                return -1;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            return 0;
        }
        #endregion

        private void processData(string connectionName, int index)
        {
            byte[] requestInput = GetRequestInput();
            string id = sp.ID.ToString() + "/" + connectionName;
            QueryServer qs = ConnectionRelay.getQueryServer(id, index);
            if (qs != null)
                qs.returnResults(requestInput);
        }

        public static string sendCommandEmail(User u, Space sp, string path, List<string> log)
        {
            string responseString = null;
            // Send acknowledgement
            MailMessage mm = new MailMessage();
            mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
            mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
            mm.To.Add(new MailAddress(u.Email));
            mm.IsBodyHtml = true;            
            StreamReader reader = new StreamReader(path + "CommandEmail.htm");
            mm.Body = reader.ReadToEnd();
            reader.Close();
            mm.Body = mm.Body.Replace("{SPACE}", sp.Name);
            mm.Subject = "Birst Confirmation Email";
            mm.Body = mm.Body.Replace("{ERROR}", "");
            StringBuilder sb = new StringBuilder();
            StringBuilder response = new StringBuilder();
            if (log.Count > 0)
            {
                sb.Append("<table style=\"font-family: Courier; font-size: 10pt\">");
                sb.Append("<tr><td>Command History</td></tr>");
                foreach (string s in log)
                {
                    sb.Append("<tr><td>" + s + "</td></tr>");
                    response.Append(s + "\r\n");
                }
                sb.Append("</table>");
            }
            else
            {
                sb = new StringBuilder("<p>No commands processed</p>");
            }
            mm.Body = mm.Body.Replace("{RESULTS}", sb.ToString());
            responseString = response.ToString();
            SmtpClient smtpc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                smtpc.EnableSsl = true;
            smtpc.Send(mm);
            return responseString;
        }

        private class ProcessAsynchronousCommand
        {
            public Space sp;
            public User u;
            public string filename;
            
            public void processCommandAsync()
            {
                List<string> log = new List<string>();
                StreamReader sr = null;
                int exitStatus = Command.NO_ERROR;
                try
                {
                    sr = File.OpenText(filename);
                    string line = null;
                    int linenum = 0;
                    Command co = new Command(sp, u, log, null, null, null, null);                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (++linenum > MAX_COMMAND_LINES)
                        {
                            log.Add("Stopping command file execution, exceeded maximum number of allowable commands: " + linenum);
                            break;
                        }
                        co.exitStatus = Command.NO_ERROR;
                        line = line.Trim();
                        co.processCommand(line);
                        log.Add("Command \"" + line + "\" returned status code: " + co.exitStatus);
                        if (co.exitStatus != Command.NO_ERROR)
                            exitStatus = co.exitStatus;
                    }
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
                StringBuilder response = new StringBuilder();
                response.Append("Status " + String.Format("{0:000}", exitStatus) + "\r\n");
                if (log.Count > 0)
                {
                    foreach (string s in log)
                    {
                        response.Append(s + "\r\n");
                    }
                }
                if (commandfileExecutionStatus.ContainsKey(sp.Directory + "\\data\\birst.generatebulkqueryresults.command"))
                {
                    Status.StatusResult status = new Status.StatusResult(Status.StatusCode.Complete);
                    status.message = response.ToString();
                    lock (commandfileExecutionStatus)
                    {
                        commandfileExecutionStatus[sp.Directory + "\\data\\birst.generatebulkqueryresults.command"] = status;
                    }
                }
            }
        }

        private int processCommand(List<string> log)
        {
            StreamReader inputStream = new StreamReader(httpApplication.Request.InputStream);
            string line = null;
            int linenum = 0;
            Command co = new Command(sp, u, log, httpApplication.Request, httpApplication.Server, null, null);
            bool mail = true;
            int exitStatus = Command.NO_ERROR;
            while ((line = inputStream.ReadLine()) != null)
            {
                if (++linenum > MAX_COMMAND_LINES)
                {
                    log.Add("Stopping command file execution, exceeded maximum number of allowable commands: " + linenum);
                    break;
                }
                co.exitStatus = Command.NO_ERROR;
                line = line.Trim();
                if (line.ToLower().Equals("nomail"))
                    mail = false;
                else
                    co.processCommand(line);
                log.Add("Command \"" + line + "\" returned status code: " + co.exitStatus);
                if (co.exitStatus != Command.NO_ERROR)
                    exitStatus = co.exitStatus;
            }
            inputStream.Close();
            if (mail)
            {
                string path = httpApplication.Request.MapPath("~/");
                log.Insert(0, "Status " + String.Format("{0:000}", exitStatus));
                responseString = sendCommandEmail(u, sp, path, log);
            }
            else
            {
                StringBuilder response = new StringBuilder();
                response.Append("Status " + String.Format("{0:000}", exitStatus) + "\r\n");
                if (log.Count > 0)
                {
                    foreach (string s in log)
                    {
                        response.Append(s + "\r\n");
                    }
                }
                responseString = response.ToString();
            }
            return (int)DavPutResponseCode.None;
        }
    }
}
