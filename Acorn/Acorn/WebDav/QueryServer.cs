﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.IO;
using System.Text;
using System.Net;

namespace Acorn.WebDav
{
    public class QueryServer
    {
        public static int TIME_OUT_SECONDS = 65;
        private string id;
        private HttpApplication app;
        private AutoResetEvent are;
        private QueryRequest qr;
        private int index;
        private DateTime created = DateTime.Now;

        public QueryServer(string id, HttpApplication app)
        {
            this.id = id;
            this.app = app;
            are = new AutoResetEvent(false);
        }

        public string ID
        {
            get
            {
                return id;
            }
        }

        public int Index
        {
            get
            {
                return index;
            }

            set
            {
                index = value;
            }
        }

        public bool TimedOut
        {
            get
            {
                if (DateTime.Now.Subtract(created).TotalSeconds > TIME_OUT_SECONDS)
                    return true;
                return false;
            }
        }

        public HttpApplication App
        {
            get
            {
                return app;
            }
            set
            {
                app = value;
            }
        }

        public void waitUntilHandled()
        {
            are.WaitOne();
        }

        public void handle(QueryRequest qr)
        {
            string request = index + "\r\n" + (new string(qr.getRequestQuery()));
            string result = request.Length + "\r\n" + request;
            app.Response.Write(result);
            app.Response.Flush();
            this.qr = qr;
            are.Set();
        }

        public void returnResults(byte[] buff)
        {
            if (qr != null)
                qr.returnResults(buff);
        }

        public void release()
        {
            are.Set();
        }

        public bool valid()
        {
            try
            {
                HttpResponse response = app.Response;
                if (response == null)
                    return false;
                return app.Response.IsClientConnected;
               // XXX this is causing an IIS application shutdown, I believe this is due to the following:
               //     - app object is in the QueryServer
               //     - two request from SMIWeb come in at the same time, first one processes and sends back, second one comes and tries to use a totally invalid request...
               //     at System.Web.Hosting.HostingEnvironment.InitiateShutdownInternal()
               //     at System.Web.UnsafeNativeMethods.EcbIsClientConnected(IntPtr pECB)
               //     at System.Web.Hosting.ISAPIWorkerRequestInProc.IsClientConnectedCore()
               //     at System.Web.Hosting.ISAPIWorkerRequest.IsClientConnected()
               //     at System.Web.HttpResponse.get_IsClientConnected()
               //     at Acorn.WebDav.QueryServer.valid() in C:\clean_builds_HEAD\Acorn\Acorn\WebDav\QueryServer.cs:line 125
               //    at Acorn.WebDav.ConnectionRelay.ProcessThread.acceptClient() in C:\clean_builds_HEAD\Acorn\Acorn\WebDav\ConnectionRelay.cs:line 465
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("valid: " + ex.Message);
                return false;
            }
        }

        public static IPAddress getHostIPAddress()
        {
            string hostName = System.Net.Dns.GetHostName();
            IPAddress hostIP = null;
            foreach (IPAddress ip in System.Net.Dns.GetHostEntry(hostName).AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    hostIP = ip;
                    break;
                }
            }
            return hostIP;
        }
    }
}
