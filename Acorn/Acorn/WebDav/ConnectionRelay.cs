﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Text;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace Acorn.WebDav
{
    public class ConnectionRelay
    {
        private static int connectionPort = 11000;
        private static int PORT_END = 11999;
        private static bool START_SERVER = Boolean.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["EnableConnectionRelay"]);
        // query requests from SMIWeb, keyed by connection id
        private static Dictionary<string, LinkedList<QueryRequest>> requests = new Dictionary<string, LinkedList<QueryRequest>>();
        // work requests from Live Access, keyed by connection id
        private static Dictionary<string, QueryServer> queryservers = new Dictionary<string, QueryServer>();
        private static Dictionary<string, SortedList<int, QueryServer>> pending = new Dictionary<string, SortedList<int, QueryServer>>();
        private static Thread t; // accept thread
        private static Thread sw;
        private static TcpListener listener;
        private static bool started = false;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        /*
         * start the connection relay, there should only be one of these
         */
        public static void startServer(bool retry)
        {
            if (!START_SERVER)
            {
                Global.systemLog.Info("Connection relay not enabled");
                return;
            }
            Global.systemLog.Info("Starting connection relay");

            // find a port we can listen on for SMIWeb query requests
            String temp = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionRelayStartPort"];
            if (temp != null)
            {
                if (!int.TryParse(temp, out connectionPort))
                {
                    Global.systemLog.Info("ConnectionRelayStartPort in web.config has been set to an invalid value, Connection relay not enabled");
                    return;
                }
                if (connectionPort < 1024 || connectionPort > 65535)
                {
                    Global.systemLog.Info("ConnectionRelayStartPort in web.config must be between 1024 and 65535, Connection relay not enabled");
                    return;
                }
            }
            else
            {
                Global.systemLog.Info("ConnectionRelayStartPort in web.config has not been set, setting to the default (" + connectionPort + ")");
            }
            temp = System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionRelayEndPort"];
            if (temp != null)
            {
                if (!int.TryParse(temp, out PORT_END))
                {
                    PORT_END = connectionPort + 999;
                    Global.systemLog.Info("ConnectionRelayEndPort in web.config has been set to an invalid value, setting to the default (" + PORT_END + ")");
                }
            }
            else
            {
                PORT_END = connectionPort + 999;
                Global.systemLog.Info("ConnectionRelayEndPort in web.config has not been set, setting to the default (" + PORT_END + ")");
            }

            // kill any existing listeners
            if (listener != null)
            {
                listener.Stop();
            }
            // abort any existing accept threads
            if (t != null)
            {
                try
                {
                    t.Abort();
                }
                catch (Exception e)
                {
                    Global.systemLog.Error(e);
                }
            }
            // abort any existing sweep (query server reaper) threads
            if (sw != null)
            {
                try
                {
                    sw.Abort();
                }
                catch (Exception e)
                {
                    Global.systemLog.Error(e);
                }
            }

            // keep starting listeners and threads
            while (retry)
            {
                // start a listener
                try
                {
                    listener = new TcpListener(IPAddress.Any, connectionPort);
                    listener.ExclusiveAddressUse = false;
                    listener.Server.LingerState = new LingerOption(false, 0);
                    listener.Start();
                    Global.systemLog.Debug("Listener started");
                    break;
                }
                catch (SocketException ex)
                {
                    Global.systemLog.Debug(ex, ex);
                    if (!retry)
                        return;
                    // Couldn't obtain, try to kill any other server on the port
                    try
                    {
                        Global.systemLog.Info("Attempting to kill server on port: " + connectionPort);
                        TcpClient client = new TcpClient(new IPEndPoint(IPAddress.Loopback, connectionPort));
                        NetworkStream ns = client.GetStream();
                        if (ns != null)
                        {
                            StreamWriter writer = new StreamWriter(ns,Encoding.UTF8);
                            writer.WriteLine("kill");
                            writer.Close();
                            ns.Close();
                        }
                    }
                    catch (Exception ex1)
                    {
                        Global.systemLog.Error(ex1, ex1);
                        connectionPort = connectionPort + 1;
                        if (connectionPort > PORT_END)
                        {
                            Global.systemLog.Info("Cycled through allowed connection relay ports and could not find one that works.  Connection relay disabled.");
                            return;
                        }
                    }
                    Thread.Sleep(10000);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Debug(ex2, ex2);
                    if (!retry)
                        return;
                    Thread.Sleep(10000);
                }
            }
            // start the various threads
            started = true;
            AcceptThread at = new AcceptThread(listener);
            t = new Thread(new ThreadStart(at.begin));
            t.IsBackground = true;
            t.Start();
            SweepThread st = new SweepThread();
            sw = new Thread(new ThreadStart(st.begin));
            sw.IsBackground = true;
            sw.Start();
            Global.systemLog.Info("Started connection relay on port " + connectionPort);
        }

        // stop the server, should only be called on Application exit
        public static void stopServer()
        {
            Global.systemLog.Info("Stopping connection relay on port " + connectionPort);
            if (listener == null)
            {
                Global.systemLog.Info("Can not stop the connection relay as it was not started - no listener");
                return;
            }
            try
            {
                listener.Stop();
                Global.systemLog.Debug("Listener stopped");
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to stop connection relay listener: " + ex.Message, ex);
            }
            finally
            {
                try
                {
                    Global.systemLog.Debug("Expect to see a thread aborted error");
                    t.Abort(); // XXX why don't we stop the sweep thread?
                    started = false;
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to abort connection relay thread: " + ex.Message, ex);
                }
                finally
                {
                    foreach (QueryServer qs in queryservers.Values)
                    {
                        try
                        {
                            qs.release();
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("Unable to release query server: " + ex.Message, ex);
                        }
                    }
                    foreach (SortedList<int, QueryServer> qslist in pending.Values)
                    {
                        foreach (QueryServer qs in qslist.Values)
                        {
                            try
                            {
                                qs.release();
                            }
                            catch (Exception ex)
                            {
                                Global.systemLog.Error("Unable to release pending query server: " + ex.Message, ex);
                            }
                        }
                    }
                    Global.systemLog.Info("Stopped connection relay");
                }
            }
        }

        public static bool Started
        {
            get
            {
                return started;
            }
        }

        /*
         * live access query server has requested work to do
         * - if they are not running multiple live accesses, since live access is not multiple threaded, there should only be one call at a time
         */
        public static void processQueryServer(QueryServer qs, string schema, string serverName)
        {
            string id = qs.ID;
            // Global.systemLog.Debug("processQueryServer (BC requested more to do): " + qs.ID + ", " + serverName);
            QueryRequest qr = null;
            lock (typeof(ConnectionRelay))
            {
                // see if there are any pending SMIWeb query requests for this live access connection (id)
                if (requests.ContainsKey(id))
                {
                    // grab the first valid query request for the id
                    LinkedList<QueryRequest> qrlist = requests[id];
                    for (int i = 0; i < qrlist.Count; i++)
                    {
                        qr = qrlist.First.Value;
                        qrlist.RemoveFirst();
                        if (qr.valid())
                            break;
                        qr = null;
                    }
                }
            }
            if (qr != null)
            {
                // if there is a valid query request
                int index;
                lock (typeof(ConnectionRelay))
                {
                    index = addPending(id, qs); // add the request
                }
                qs.Index = index;
                try
                {
                    qs.handle(qr); // handle the request
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to handle query server " + qs.ID + ": " + ex.Message, ex);
                }
            }
            else
            {
                // no valid query requests
                lock (typeof(ConnectionRelay))
                {
                    // if there is an existing query server for this connection, remove it since we just got a new one
                    if (queryservers.ContainsKey(id))
                    {
                        try
                        {
                            QueryServer oldqs = queryservers[id];
                            queryservers.Remove(id);
                            oldqs.release(); // trigger the query server
                        }
                        catch (Exception ex)
                        {
                            Global.systemLog.Error("Unable to release old version of query server " + id + ": " + ex.Message, ex);
                        }
                    }
                    queryservers[id] = qs;
                }
                // register the server with the DB so that SMIWeb knows about it
                try
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        ProxyRegistration pr = Database.getProxyRegistration(conn, schema, id);
                        if (pr != null)
                        {
                            Database.updateProxyRegistration(conn, schema, serverName, connectionPort, id);
                        }
                        else
                        {
                            Database.addProxyRegistration(conn, schema, serverName, connectionPort, id);
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error(ex, ex);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    // Wait for a request to be ready (releasing will cause an immediate re-query by the client)
                    try
                    {
                        qs.waitUntilHandled(); // go to sleep until a request comes from SMIWeb
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error("Handling of query server " + id + " terminated: " + ex.Message, ex);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Unable to registor query server " + id + ": " + ex.Message,  ex);
                    lock (typeof(ConnectionRelay))
                    {
                        queryservers.Remove(id);
                    }
                }
            }
        }

        /*
         * add a query server to the list of pending servers for the id, returning index in the pending list
         */
        private static int addPending(string id, QueryServer qs)
        {
            SortedList<int, QueryServer> qslist = null;
            lock (typeof(ConnectionRelay))
            {
                // add the server to the list of pending servers for the id
                if (!pending.ContainsKey(id))
                {
                    qslist = new SortedList<int, QueryServer>();
                    pending.Add(id, qslist);
                }
                else
                    qslist = pending[id];
            }
            int result = 0;
            lock (qslist)
            {
                result = qslist.Count == 0 ? 0 : qslist.Keys.Max() + 1;
                qslist.Add(result, qs);
            }
            return result;
        }

        // get the query server for id, at 'index' in the pending list
        public static QueryServer getQueryServer(string id, int index)
        {
            if (!pending.ContainsKey(id))
                return null;
            SortedList<int, QueryServer> qslist = pending[id];
            if (qslist.Count == 0)
                return null;
            QueryServer result = null;
            lock (qslist)
            {
                if (qslist.ContainsKey(index))
                {
                    result = qslist[index];
                    qslist.Remove(index);
                }
            }
            return result;
        }

        /*
         * accept query request from SMIWeb
         */
        private class AcceptThread
        {
            private TcpListener listener;

            public AcceptThread(TcpListener listener)
            {
                this.listener = listener;
            }

            // 'run' method for the thread, loop around handling requests from SMIWeb
            public void begin()
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Accept");
                while (started)
                {
                    try
                    {
                        // if there is a request from SMIWeb, accept them and spawn off a thread to process it
                        if (listener.Pending())
                        {
                            TcpClient client = listener.AcceptTcpClient();
                            ProcessThread pt = new ProcessThread(client);
                            Thread t = new Thread(new ThreadStart(pt.acceptClient));
                            t.IsBackground = true;
                            t.Start();
                        }
                        else
                            Thread.Sleep(50); // no pending requests, sleep for a whlie
                    }
                    catch (ThreadAbortException tex)
                    {
                        Global.systemLog.Info("Aborting Connection Relay AcceptThread (ThreadAbortException): " + tex.Message, tex);
                        break;
                    }
                    catch (SocketException sex)
                    {
                        if (sex.Message.IndexOf("WSACancelBlockingCall") >= 0)
                        {
                            Global.systemLog.Info("Aborting Connection Relay AcceptThread (SocketException): " + sex.Message, sex);
                            break;
                        }
                        else
                        {
                            Global.systemLog.Warn("SocketException in AcceptThread Listener, sleeping 5 seconds: " + sex.Message, sex);
                            Thread.Sleep(5000);
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Warn("Exception in AcceptThread Listener, sleeping 5 seconds: " + ex.Message, ex);
                        Thread.Sleep(5000);
                    }
                }
            }
        }

        /*
         * thread to read a query request from SMIWeb
         */
        private class ProcessThread
        {
            TcpClient client;

            public ProcessThread(TcpClient client)
            {
                this.client = client;
            }

            public void acceptClient()
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Process - " + Thread.CurrentThread.ManagedThreadId);
                try
                {
                    // get the request input stream and read the request ID (associated with the Live Access connection)
                    StreamReader reader = new StreamReader(client.GetStream(), Encoding.UTF8);
                    string id = reader.ReadLine();
                    // Global.systemLog.Debug("received from SMIWeb request id: " + id);
                    if (id != null && id.Length > 0)
                    {
                        if (id == "kill")
                        {
                            stopServer(); // XXX this can't be right...
                            return;
                        }
                        QueryServer qs = null;
                        QueryRequest qr = new QueryRequest(id, client, reader); // create a new query request object
                        lock (typeof(ConnectionRelay))
                        {
                            if (queryservers.ContainsKey(id))
                            {
                                // If a server is available to handle the request, get it
                                qs = queryservers[id];
                                queryservers.Remove(id);  // remove it as soon as it is grabbed
                                if (!qs.valid()) // XXX what if app.Request has gone away... two requests from SMIWeb get the same query server
                                    qs = null;
                            }
                            // queue the request
                            LinkedList<QueryRequest> rlist = null;
                            if (!requests.ContainsKey(id))
                            {
                                rlist = new LinkedList<QueryRequest>();
                                requests.Add(id, rlist);
                            }
                            else
                                rlist = requests[id];
                            rlist.AddLast(qr);
                        }
                        // if there is an existing query server available, signal that it should be used
                        if (qs != null) // XXX outside of lock, shouldn't we also remove it..
                            qs.release();  // wake it up if it is waiting
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error handling connection client: " + ex.Message, ex);
                }
            }
        }

        /*
         * thread for cleaning up 'dead' query servers
         * - any query servers that have not had any activity in 'N' seconds (via the TimedOut method)
         */
        private class SweepThread
        {
            public SweepThread()
            {
            }

            public void begin()
            {
                log4net.NDC.Clear();
                log4net.NDC.Push("Sweep");
                while (true)
                {
                    try
                    {
                        Thread.Sleep(30000);
                        List<string> toRemove = null;
                        foreach (string key in queryservers.Keys)
                        {
                            QueryServer qs = queryservers[key];
                            if (qs.TimedOut)
                            {
                                if (toRemove == null) toRemove = new List<string>();
                                toRemove.Add(key);
                            }
                        }
                        lock (typeof(ConnectionRelay))
                        {
                            if (toRemove != null)
                                foreach (string key in toRemove)
                                {
                                    if (queryservers.ContainsKey(key) && queryservers[key].TimedOut)
                                    {
                                        QueryServer qs = queryservers[key];
                                        queryservers.Remove(key);
                                        qs.release();
                                    }
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Info("Aborting Sweeper Thread: " + ex.Message, ex);
                        break;
                    }
                }
            }
        }

        public static string getKey(Space sp, string connection)
        {
            return sp.ID.ToString() + "/" + connection;
        }

        /*
         * is there a connected (and not timed out) live access query server for this space and connection
         */
        public static bool isConnected(Space sp, string connection)
        {
            if (sp == null)
                return false;
            string key = getKey(sp, connection);
            if (queryservers.ContainsKey(key) && !queryservers[key].TimedOut)
                return true;
            QueryConnection conn = null;
            ProxyRegistration pr = null;
            try
            {
                conn = ConnectionPool.getConnection();
                pr = Database.getProxyRegistration(conn, mainSchema, key);
                if (pr == null)
                {
                    Global.systemLog.Debug("Failed to obtain ProxyRegistration for: " + sp.ID.ToString() + "/" + connection + " for checking if LiveAccess connection is alive.");
                    return false;
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (DateTime.Now.Subtract(pr.RegistrationDate).TotalSeconds < QueryServer.TIME_OUT_SECONDS)
                return true;
            return false;
        }
    }
}
