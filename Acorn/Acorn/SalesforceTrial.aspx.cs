﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.sforce;
using System.Text;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services.Protocols;
using System.Data.Odbc;
using System.Net;
using Acorn.Background;
using System.Web.SessionState;

namespace Acorn
{
    public partial class SalesforceTrial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response, false);
        }

        protected void CredentialsNextButton_Click(object sender, ImageClickEventArgs e)
        {
            // Create service object
            SforceService binding = new SforceService();
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Warn("Bad SFDC binding URL prior to authentication: " + binding.Url);
                return;
            }
            binding.Timeout = 30 * 60 * 1000;
            String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
            if (clientID != null && clientID.Length > 0)
            {
                CallOptions co = new CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            // Invoke the login call and save results in LoginResult
            LoginResult lr = null;
            try
            {
                lr = binding.login(UsernameBox.Text, PasswordBox.Text);
            }
            catch (SoapException ex)
            {
                LoginError.Text = ex.Message;
                LoginError.Visible = true;
                return;
            }
            catch (WebException ex)
            {
                LoginError.Text = ex.Message;
                LoginError.Visible = true;
                return;
            }
            if (!lr.passwordExpired)
            {
                Session["sftrialbinding"] = binding;
                // Reset the SOAP endpoint to the returned server URL
                binding.Url = lr.serverUrl;
                // Add the session ID returned from the login
                binding.SessionHeaderValue = new SessionHeader();
                binding.SessionHeaderValue.sessionId = lr.sessionId;
                Response.Redirect("~/NewUser.aspx?sfdctrial=true&freetrial=true");
            }
            else
            {
                LoginError.Text = "Your password has expired";
                LoginError.Visible = true;
                return;
            }
        }
    }
}
