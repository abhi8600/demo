﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using Acorn.TrustedService;

namespace Acorn
{
    public class CompareCatalog
    {
        public static CatalogCompareResult compareCatalogs(HttpSessionState session, string spaceDirectory, string spaceDirectoryCompare)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            CatalogCompareResult ccr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            ccr = ts.compareCatalogs(spaceDirectory, spaceDirectoryCompare, u.Username, sp.ID.ToString());
            if (!ccr.successSpecified || !ccr.success)
            {
                Global.systemLog.Info("TrustedService:CompareCatalogs failed: " + ccr.errorMessage + " (" + ccr.errorCode + ')');
            }
            return ccr;
        }

        public static CatalogCompareResult pullFromCatalog(HttpSessionState session, string spaceDirectory, string spaceDirectoryCompare, string[] paths)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            CatalogCompareResult ccr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
            ccr = ts.pullFromCatalog(spaceDirectory, spaceDirectoryCompare, u.Username, sp.ID.ToString(), paths);
            if (!ccr.successSpecified || !ccr.success)
            {
                Global.systemLog.Info("TrustedService:CompareCatalogs failed: " + ccr.errorMessage + " (" + ccr.errorCode + ')');
            }
            return ccr;
        }
    }
}