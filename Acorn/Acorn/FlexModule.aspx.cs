﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.SAMLSSO;

namespace Acorn
{
    public partial class FlexModule : LocalizedPage
    {
        private static string termsOfServiceURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TermsOfServiceLink"];
        private static string privacyPolicyURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PrivacyPolicyLink"];
        private static string contactUsURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContactUsLink"];
        public static string gaTrackingAccount = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["GATrackingAccount"];
        private static string copyRight = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["copyRight"];


        public string version = null;
        public string controlHeight;
        public string smiroot;
        public string locale;
        public string extraParams;
        public string wmode = "window";
        public string moduleName = "AdhocLayout";
        public string fgcolor;
        public string noheader = "false";

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            HtmlGenericControl objScript = new HtmlGenericControl("script");
            objScript.Attributes.Add("type", "text/javascript");
            objScript.Attributes.Add("src", "KeepAliveSMIWeb.js");
            if (objScript != null)
            {
                Control keepAliveCtrl = Page.FindControl("KeepAlive");
                if (keepAliveCtrl != null)
                    keepAliveCtrl.Controls.Add(objScript);
            }

            string logoutScript = Util.getInactivityTimeoutScript(Session);
            if (!string.IsNullOrWhiteSpace(logoutScript))
            {
                wmode = "transparent";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "TimeoutRedirect", logoutScript);
            }

            bool forceEmbed = false;
            Util.validateSpaceId(Request, Response, Session);
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            string module = Request["birst.module"];

            if (module == "dashboard")
                moduleName = "Dashboards";
            else if (module == "visualization")
                moduleName = "VisualizationLayout";
            else if (module == "designer")
                moduleName = "AdhocLayout";
            else if (module == "usermanagement")
                moduleName = "UserManagement";
            else if (module == "catalog")
                moduleName = "CatalogManagement";
            else if (module == "admin")
                moduleName = "Navigation";
            else if (module == "home")
                moduleName = "DefaultHome";
            else
            {
                Global.systemLog.Debug("invalid module for FlexModule.aspx: " + module);
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            // Check for Demo space and return
            if (Util.isDemoSpace(Session))
            {
                configureForDemo(Session, Request, Response, Server, module);
                return;
            }

            DemoHeader.Visible = false;
            User u = Util.validateAuth(Session, Response);
            Space sp = null;

            // if this is an embedded sso session, don't let them go outside of the module they were originally sent to (unless they have permission)
            string ssoModule = (string)Session["birst.module"];
            if (ssoModule != null)
            {
                if (!ssoModule.Equals(module))
                {
                    if (module != "designer" || (module == "designer" && !DefaultUserPage.isAdhocPermitted(Session, u)))
                        module = ssoModule;
                }
                forceEmbed = true;
            }

            if (module != "home")
            {
                sp = Util.validateSpace(Session, Response, Request);

                if (!Util.validateSpace(sp, u, Session))
                {
                    Global.systemLog.Debug("redirecting to home page due to space validation failure");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                FooterPanel.Visible = false;
            }
            else
            {
                FooterPanel.Visible = true;
            }


            if (module == "home")
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool redirect = true;

                if (browser.Browser == "IE")
                {
                    if (browser.MajorVersion < 9)
                        redirect = false;
                }
                else if (browser.Browser == "Unknown")
                {
                    redirect = false;
                }
                if (redirect)
                {
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
            }
            else if (u.HTMLInterface && module == "dashboard" && Request["birst.openPageForEditHTML"] != "true" && Request["birst.addNewPageHTML"] != "true")
            {
                Response.Redirect("~/html/dashboards.aspx");
            }

            // evaluate the preconditions

            if ((module == "designer" || module == "visualization") && !DefaultUserPage.isAdhocPermitted(Session, u))
            {
                Global.systemLog.Debug("redirecting to home page due to permissions failure (designer)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (module == "usermanagement" && !UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Debug("redirecting to home page due to permissions failure (usermanagement)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (module == "catalog")
            {
                if (!u.CanManageCatalog)
                {
                    Global.systemLog.Debug("redirecting to home page due to permissions failure (catalog)");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
            }

            if (module == "admin")
            {
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                {
                    Global.systemLog.Debug("redirecting to home page due to missing MAF (admin)");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                if (!Util.isSpaceAdmin(u, sp, Session))
                {
                    Global.systemLog.Debug("redirecting to home page due to not being the space admin (admin)");
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                }
                // even if there is a saleforceloader reference check to make sure it is not bogus one that has not been cleared properly
                if (!Util.isSFDCExtractRunning(sp))
                {
                    SalesforceLoader.clearLoader(sp.ID);
                }
                Util.backupMetadata(sp, u);
            }

            if (Util.isNoHeader(Request))
            {
                noheader = "true";
            }

            bool doEmbed = forceEmbed || Util.isEmbedded(Request);
            if (!doEmbed)
            {
                Page.Header.Visible = true;
                headerstyle.Visible = true;
                Util.formatPage(sp, u, LogoLiteral, HeaderColorStyle, out fgcolor);

                AccountHeaderPlaceHolder.Visible = DefaultUserPage.isSettingsLinkVisible(Session, u);
                UpdatedHome.Visible = DefaultUserPage.isHomePermitted(Session, u);

                if ((!UpdatedHome.Visible || !AccountHeaderPlaceHolder.Visible) && !DefaultUserPage.isAdhocPermitted(Session, u))
                {
                    HeaderBlank.Visible = true;
                }

                Object adhocOK = Session["AdhocPermitted"];
                Object dashOK = Session["DashboardPermitted"];
                AdhocHeaderPlaceHolder.Visible = (sp != null) && DefaultUserPage.isAdhocPermitted(Session, u) && (adhocOK != null && (bool)adhocOK);
                //                VisualizationPlaceHolder.Visible = (sp != null) && DefaultUserPage.isAdhocPermitted(Session, u) && (adhocOK != null && (bool)adhocOK) && !u.isFreeTrialUser; // only viz mode available for free trial
                DashboardHeaderPlaceHolder.Visible = (sp != null) && (dashOK != null && (bool)dashOK);
                HeaderUserName.Text = HttpUtility.HtmlEncode(u.Username);
                if (sp != null)
                {
                    AdminHeaderPlaceHolder.Visible = Util.isSpaceAdmin(u, sp, Session);
                    HeaderSpaceName.Text = Util.shortSpaceName(sp.Name);
                    HeaderSpaceName.ToolTip = sp.Name;
                    if (u.isSuperUser || u.RepositoryAdmin)
                        HeaderSpaceName.ToolTip += HttpUtility.HtmlEncode("\nID: " + sp.ID + "\nServer: " + Util.getHostName() + "\nVersion: " + Util.getAppVersion());
                }
            }
            else
            {
                TitleLiteral.Visible = false;
                TitleLinkLiteral.Visible = false;
                HeaderColorStyle.Visible = false;
                headerstyle.Visible = false;
            }
            if (u.isFreeTrialUser)
                HelpLink.NavigateUrl = "Help/BXE/index.htm";

            //set override parameters if any
            System.Collections.Generic.Dictionary<String, String> overrideParameters = null;

            string Locale = GetUserLocale();

            overrideParameters = Util.getLocalizedOverridenParams(Locale, u);

            if (overrideParameters != null && overrideParameters.Count > 0)
            {
                if (overrideParameters.ContainsKey(OverrideParameterNames.HELP_URL))
                {
                    if (overrideParameters[OverrideParameterNames.HELP_URL] != "")
                    {
                        HelpLink.NavigateUrl = overrideParameters[OverrideParameterNames.HELP_URL];
                    }
                    else
                    {
                        BirstHelpUrl.Visible = false;
                    }
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                {
                    if (overrideParameters[OverrideParameterNames.PAGE_TITLE] != "")
                    {
                        TitleLiteral.Text = overrideParameters[OverrideParameterNames.PAGE_TITLE];
                    }
                    else
                    {
                        TitleLiteral.Text = "";
                    }
                }
                else
                {
                    TitleLiteral.Text = "Birst";
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.ERROR_URL))
                {
                    if (overrideParameters[OverrideParameterNames.ERROR_URL] != "")
                    {
                        Session[OverrideParameterNames.ERROR_URL] = overrideParameters[OverrideParameterNames.ERROR_URL];
                    }
                }
                if (overrideParameters.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                {
                    if (overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL] != "")
                    {
                        TermsOfServiceLink.NavigateUrl = overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL];
                    }
                    else
                    {
                        TermsOfServicePlaceHolder.Visible = false;
                    }
                }
                else
                {
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                {
                    if (overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL] != "")
                    {
                        PrivacyPolicyLink.NavigateUrl = overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL];
                    }
                    else
                    {
                        PrivacyPolicyPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                {
                    if (overrideParameters[OverrideParameterNames.CONTACT_US_URL] != "")
                    {
                        ContactUsLink.NavigateUrl = overrideParameters[OverrideParameterNames.CONTACT_US_URL];
                    }

                    else
                    {
                        ContactUsPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    ContactUsLink.NavigateUrl = contactUsURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                {
                    if (overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT] != "")
                    {
                        copyRight = overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT];
                        CopyrightID.Text = HttpUtility.HtmlEncode(copyRight);

                    }
                    else
                    {
                        CopyrightID.Text = "";
                    }

                }
                else
                {
                    CopyrightID.Text = HttpUtility.HtmlEncode(copyRight);
                }
                if (SupportLinkPlaceHolder.Visible && overrideParameters.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                {
                    if (overrideParameters[OverrideParameterNames.SUPPORT_URL] != "")
                    {
                        SupportLink.NavigateUrl = overrideParameters[OverrideParameterNames.SUPPORT_URL];
                    }

                    else
                    {
                        BirstSupportUrl.Visible = false;
                    }

                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                {
                    if (overrideParameters[OverrideParameterNames.LOGOUT_URL] != "")
                    {
                        LogoutLink.NavigateUrl = overrideParameters[OverrideParameterNames.LOGOUT_URL];
                    }
                    else
                    {
                        BirstLogoutUrl.Visible = false;
                    }
                }
            }
            else
            {
                string copyRight = Util.getCopyright();

                if (termsOfServiceURL != null)
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                else
                    TermsOfServicePlaceHolder.Visible = false;
                if (privacyPolicyURL != null)
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                else
                    PrivacyPolicyPlaceHolder.Visible = false;
                if (contactUsURL != null)
                    ContactUsLink.NavigateUrl = contactUsURL;
                else
                    ContactUsPlaceHolder.Visible = false;

                LogoutLink.NavigateUrl = "/Logout.aspx";

                HelpLink.NavigateUrl = "/Help/Full/index.htm";

                SupportLink.NavigateUrl = "/Support.aspx";

                TitleLiteral.Text = "Birst";

                CopyrightID.Text = HttpUtility.HtmlEncode(copyRight);
            }
            VersionID.Text = HttpUtility.HtmlEncode(Util.getAppVersion());

            if (sp != null)
            {
                Util.saveDirty(Session);
                if (module == "designer" || module == "dashboard" || module == "visualization")
                    createSMIWebSession(Session, Request, Response, Server, sp, u);
            }
            populateFlashVars(sp, doEmbed);
        }

        private void populateFlashVars(Space sp, bool embed)
        {
            version = Util.getAppVersion();

            this.controlHeight = "100%";

            if (!embed)
            {
                SwfSizeLiteral.Text = "<style type=\"text/css\">#moduleDiv{height:92%;}</style>";
            }
            else
            {
                SwfSizeLiteral.Text = "<style type=\"text/css\">#moduleDiv{height:100%;}</style>";
            }

            string useisapi = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["isapiEnabled"];
            if (useisapi != null && !bool.Parse(useisapi))
            {
                if (sp != null)
                {
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    smiroot = "&smiroot=" + Util.getRequestScheme(Request) + "://" + sc.URL;
                }
            }

            extraParams = Util.getExtraParams(Server, Request, out wmode);
            if (wmode == null)
            {
                wmode = "transparent";
            }

            if (moduleName == "Dashboards")
                extraParams += setPageSequenceParams(sp.Directory);

            if (extraParams != null && extraParams.Length > 0)
                extraParams = "&extraParams=" + extraParams;

            // user specific items
            if ((User)Session["user"] != null)
            {
                Guid userID = ((User)Session["user"]).ID;
                IDictionary<string, string> preferences = ManagePreferences.getPreference(null, userID, "Locale");
                if (preferences["Locale"] != null)
                    locale = "&locale=" + preferences["Locale"].Replace('-', '_');
            }
        }

        private void createSMIWebSession(System.Web.SessionState.HttpSessionState session, HttpRequest request,
            HttpResponse response, HttpServerUtility server, Space sp, User u)
        {
            string token = Util.getSMILoginToken(u.Username, sp);
            if (Util.getSMILoginSession(session, sp, token, response, request))
            {
                moduleDiv.Visible = true;
                try
                {
                    SchedulerUtils.loginIntoSchedulerIfAllowed(session, response, request);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Error while logging into the scheduler", ex2);
                }

            }
            else
            {
                moduleDiv.Visible = false;
                Global.systemLog.Error("Not able to login SMIWeb");
                response.Redirect(Util.getHomePageURL(session, request));
            }
        }

        private void configureForDemo(System.Web.SessionState.HttpSessionState session, HttpRequest request, HttpResponse response, HttpServerUtility server, string module)
        {
            Space sp = Util.getSessionSpace(session);
            string token = Util.getSMILoginToken(Demo.DEMO_USER_NAME, sp);

            if (module == "designer" && Session["showDesigner"] == null)
            {
                Global.systemLog.Warn("DemoUser " + module + ": No showDesigner flag found on the incoming Demo space request");
                response.Redirect("http://www.birst.com");
                return;
            }

            if (!Util.getSMILoginSession(session, sp, token, response, request))
            {
                Global.systemLog.Error("DemoUser " + module + ": Not able to login SMIWeb");
                response.Redirect("http://www.birst.com");
                return;
            }

            try
            {
                SchedulerUtils.loginIntoSchedulerIfAllowed(session, response, request);
            }
            catch (Exception ex2)
            {
                Global.systemLog.Error("DemoUser " + module + ": Error while logging into the scheduler", ex2);
            }

            HeaderColorStyle.Visible = false;
            headerstyle.Visible = false;
            DemoHeader.Visible = true;
            DemoSpaceLabel.Text = HttpUtility.HtmlEncode(sp.Name);
            string showDesigner = (string)Session["showDesigner"];
            if (showDesigner != null && showDesigner == "true")
                adhocLink.Visible = true;
            else
                adhocLink.Visible = false;
            populateFlashVars(sp, false);
        }

        public static string setPageSequenceParams(string dir)
        {
            // read from sp.Directory/pageSequence.txt
            string extraParams = "";
            FileInfo fi = new FileInfo(Path.Combine(dir, "pageSequence.txt"));
            if (!fi.Exists)
                return extraParams;
            string text = System.IO.File.ReadAllText(fi.FullName).TrimEnd(Environment.NewLine.ToCharArray());

            if (text != null && text.Length > 0)
            {
                if (extraParams != null)
                {
                    extraParams = extraParams + ",birst.pageList=" + text;
                }
                else
                {
                    extraParams = "birst.pageList=" + text;
                }
            }
            return extraParams;
        }

        public string GetUserLocale()
        {
            return Util.userLocale(Session);
        }
        public static string GetUserLocale(User u)
        {
            return Util.userLocale(u);
        }
    }
}
