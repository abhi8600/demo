﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Acorn.DBConnection;
using System.IO;
using System.Collections.Generic;
using System.Drawing;


namespace Acorn
{
    public partial class FaviconIconUrl : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static byte[] birstfavicon = null;
        Dictionary<string, string> overrideParametersfavicon = new Dictionary<string, string>();
        private byte[] getBirstfavicon()
        {
            string faviconImage = "";
            Logo logo = new Logo();

            if (birstfavicon == null)
            {
                FileStream fs = null;
                try
                {
                    overrideParametersfavicon = logo.getLocalizedOverridenParamsLogo();
                    if (overrideParametersfavicon != null && overrideParametersfavicon.Count > 0 && overrideParametersfavicon.ContainsKey(OverrideParameterNames.FAVICON_ICON))
                    {
                        if (overrideParametersfavicon[OverrideParameterNames.FAVICON_ICON] != "")
                        {
                            faviconImage = overrideParametersfavicon[OverrideParameterNames.FAVICON_ICON];
                            birstfavicon = logo.getImageFromUrl(faviconImage);
                        }                       
                    }
                    else
                    {
                        faviconImage = "Images/favicon.ico";
                        fs = new FileStream(Server.MapPath(faviconImage), FileMode.Open, FileAccess.Read);
                        BinaryReader br = new BinaryReader(fs);
                        birstfavicon = br.ReadBytes((int)fs.Length);
                    }
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
            return birstfavicon;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            User u = (User)Session["user"];

            Response.Clear();
            Response.ContentType = "image/png";
            Response.BinaryWrite(getBirstfavicon());
            return;

        }
    }
}