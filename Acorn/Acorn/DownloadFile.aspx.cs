﻿using System;
using System.IO;

namespace Acorn
{
    public partial class DownloadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (DownloadFile.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            if (u.isFreeTrialUser)
            {
                Global.systemLog.Warn("redirecting to home page due to being a free trial user (DownloadFile.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            string filename = Request["filename"];
            if (filename == null || filename.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) != -1)
            {
                Global.systemLog.Warn("DownloadFile : filename(" + filename + ") does not match with valid file name pattern.");
                return;
            }
            string getfile = MapPath("~/downloads/") + filename;
            FileInfo fi = new FileInfo(getfile);
            try
            {
                if (fi.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                    Response.AddHeader("Content-Length", fi.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.TransmitFile(fi.FullName);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (System.Threading.ThreadAbortException tae)
            {
                Global.systemLog.Debug("DownloadFile: " + tae.Message);
            }
            catch (Exception ex) 
            {
                Global.systemLog.Error("DownloadFile: " + ex.Message);
            }
        }
    }
}