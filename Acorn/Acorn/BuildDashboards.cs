﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace Acorn
{
    public class BuildDashboards
    {
        MainAdminForm maf;
        string directory;

        private static int MAX_MEASURES = 10;
        private static double NULL_THRESHOLD = 0.7;
        // Because of resultset size limit, cannot do ranks on very large result sets
        private static int MAX_ATTRIBUTE_CARDINALITY = 100000;
        private static string REPORT_SLICE_BY_WORD = " by ";
        private static int MAX_PROMPT_CATEGORIES = 200;
        // Ideal varchar column width for display (not too long, but also descriptive)
        private static int IDEAL_VARCHAR_WIDTH = 20;
        public static bool RESTRICT_DIMENSION_PROMPT_NAMES = false;
        public static string OverviewDashboardName = "Overview";
        private static string TOTAL_STR = "Total";
        private static string COUNT_STR = "Count of";
        private static string AVERAGE_STR = "Average";
        private static string DISTINCT_STR = "Distinct";
        private EvaluatedMeasure[] measures = null;

        public BuildDashboards(MainAdminForm maf, string directory)
        {
            this.maf = maf;
            this.directory = directory;
        }

        public class EvaluatedMeasure
        {
            public double score;
            public StagingTable st;
            public StagingColumn sc;
            public long cardinality;
            public bool sequenceNumber;
            public string aggregationRule;
            public bool ignore;
        }

        public static string[] AggList = new string[] { "SUM", "AVG", "COUNT" };

        public EvaluatedMeasure[] getMeasures(string directory, ScanBuffer scanBuff)
        {
            if (measures != null)
                return measures;
            bool debug = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["DebugAutoDashboard"]);
            StreamWriter writer = null;
            try
            {
                if (debug)
                {
                    writer = new StreamWriter(directory + "\\debugautodashboards.txt");
                }
                // Find significant metrics
                measures = getMeasures(scanBuff);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex.ToString());
            }
            finally
            {
                if (debug && writer != null)
                    writer.Close();
            }
            return measures;
        }

        public void build(string directory, ScanBuffer scanBuff)
        {
            bool debug = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["DebugAutoDashboard"]);
            StreamWriter writer = null;
            try
            {
                if (debug)
                {
                    writer = new StreamWriter(directory + "\\debugautodashboards.txt");
                }
                int version = Util.getQueryLanguageVersion(maf);
                // Find significant metrics
                if (measures == null)
                    measures = getMeasures(scanBuff);

                List<QuickDashboard> dashboards = new List<QuickDashboard>();
                // Get attributes for each metric
                int maxMeasures = MAX_MEASURES;
                for (int index = 0; index < measures.Length && index < maxMeasures; index++)
                {
                    if (measures[index].ignore)
                    {
                        maxMeasures++;
                        continue;
                    }
                    // Get the mappings to confirm it is an inherited time table
                    DataRowView[] drv = null;
                    foreach (string agg in AggList)
                    {
                        drv = maf.measureColumnMappingsNamesView.FindRows(ApplicationBuilder.getAggregatedMeasureName(measures[index].sc.Name, agg));
                        if (drv.Length > 0)
                            break;
                    }
                    bool lowerCardinalityExists = false;
                    SortedList<double, List<ColumnInfo>> columnsToAnalyze = getAttributesForMeasure(scanBuff, measures[index], out lowerCardinalityExists);
                    if (columnsToAnalyze.Count == 0)
                    {
                        maxMeasures++;
                        continue;
                    }
                    // Pick the columns to use
                    List<ColumnInfo> pickedColumns = new List<ColumnInfo>();
                    // Find any date columns
                    SourceFile.SourceColumn dateIDColumn = null;
                    SourceFile dateIDSourceFile = null;
                    string dateIDPrefix = null;
                    if (drv.Length > 0 && !lowerCardinalityExists)
                    {
                        Dictionary<string, SourceFile.SourceColumn> dateIDList = new Dictionary<string, SourceFile.SourceColumn>();
                        Dictionary<SourceFile.SourceColumn, SourceFile> sflist = new Dictionary<SourceFile.SourceColumn, SourceFile>();
                        getTimeMeasure(maf, drv, dateIDList, sflist);
                        if (dateIDList.Count > 0)
                        {
                            int curValue = 0;
                            SourceFile.SourceColumn sfc = null;
                            foreach (string dkey in dateIDList.Keys)
                            {
                                SourceFile.SourceColumn ssfc = dateIDList[dkey];
                                bool found = false;
                                // Make sure there aren't any other time reports on the same measure
                                foreach (QuickDashboard qd in dashboards)
                                {
                                    foreach (QuickReport qr in qd.Reports)
                                    {
                                        if (qr.Measures[0].MeasureName ==
                                            dkey + ApplicationBuilder.TIME_PREFIX_SEPARATOR + (string)drv[0]["ColumnName"] &&
                                            qr.DimensionColumns[0].DimensionName == maf.timeDefinition.Name)
                                        {
                                            found = true;
                                            break;
                                        }
                                        if (found)
                                            break;
                                    }
                                }
                                if (!found && ssfc.NumDistinctValues > curValue)
                                {
                                    curValue = ssfc.NumDistinctValues;
                                    sfc = ssfc;
                                    dateIDPrefix = dkey;
                                }
                            }
                            dateIDColumn = sfc;
                            dateIDSourceFile = sflist[sfc];
                        }
                    }
                    HashSet<string> dimensionColumnsToQualify = new HashSet<string>();
                    // Work through the other columns
                    foreach (double val in columnsToAnalyze.Keys)
                    {
                        List<ColumnInfo> cilist = columnsToAnalyze[val];
                        cilist.Sort();
                        ColumnInfo topci = cilist[cilist.Count - 1];
                        /* 
                         * Pick the columns from the list. Generally, you can assume that all
                         * columns with the same score are different categories for the same thing so pick 
                         * the best one (i.e. pick a name over an ID). However, if there's a tie 
                         * for first (most likely because there are two categories that are very similar)
                         * Then pick both
                         */
                        List<ColumnInfo> topList = new List<ColumnInfo>();
                        topList.Add(topci);
                        for (int i = cilist.Count - 2; i >= 0; i--)
                        {
                            bool similar = false;
                            if (topci.sc.DataType == cilist[i].sc.DataType &&
                                (float)Math.Abs(topci.categories.Count - cilist[i].categories.Count) /
                                (float)Math.Max(topci.categories.Count, cilist[i].categories.Count) < 0.1)
                            {
                                if (topci.sc.DataType == "Varchar")
                                {
                                    if ((float)Math.Abs(topci.sc.Width - cilist[i].sc.Width) /
                                        (float)Math.Max(topci.sc.Width, cilist[i].sc.Width) < 0.1)
                                        similar = true;
                                }
                                else
                                    similar = true;
                            }
                            if (similar)
                                topList.Add(cilist[i]);
                            else
                                break;
                        }
                        // Now, loop through top ties
                        foreach (ColumnInfo ci in topList)
                        {
                            /* 
                             * Make sure that this column isn't a dupe of another - i.e. it doesn't have
                             * the same categories, or isn't the same dimension/column. Note, this is 
                             * doing a comparison based on target dimension. If a target dimension
                             * is hidden and snowflaked off of another, this won't catch the dupe.
                             * That will be caught downstream.
                             */
                            bool found = false;
                            foreach (ColumnInfo sci in pickedColumns)
                            {
                                if (sci.sc.Name == ci.sc.Name)
                                {
                                    bool match = true;
                                    foreach (string s in sci.sc.TargetTypes)
                                    {
                                        if (s == "Measure")
                                            continue;
                                        bool colMatch = false;
                                        foreach (string s2 in ci.sc.TargetTypes)
                                        {
                                            if (s2 == "Measure")
                                                continue;
                                            if (s == s2)
                                            {
                                                colMatch = true;
                                                break;
                                            }
                                        }
                                        if (!colMatch)
                                        {
                                            match = false;
                                            break;
                                        }
                                    }
                                    if (match)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (sci.categories == null || ci.categories == null)
                                    continue;
                                if (sci.categories.Count == ci.categories.Count)
                                {
                                    bool match = false;
                                    foreach (string c1 in sci.categories.Keys)
                                    {
                                        foreach (string c2 in ci.categories.Keys)
                                        {
                                            if (c1 == c2)
                                            {
                                                match = true;
                                                break;
                                            }
                                        }
                                        if (!match)
                                            break;
                                    }
                                    if (match)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                else if (ci.categories.Count > 10 || (sci.sc.Name == ci.sc.Name && ci.categories.Count > 3))
                                {
                                    // See if categories are mostly similar
                                    double samecount = 0;
                                    foreach (string s1 in ci.categories.Keys)
                                    {
                                        foreach (string s2 in sci.categories.Keys)
                                        {
                                            if (s1 == s2)
                                            {
                                                samecount++;
                                                break;
                                            }
                                        }
                                    }
                                    if (samecount / ci.categories.Count > 0.8)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            // Make sure the same report isn't on another dashboard
                            if (!found)
                                foreach (QuickDashboard qd in dashboards)
                                {
                                    foreach (QuickReport qr in qd.Reports)
                                    {
                                        if (ci.sc.TargetAggregations != null)
                                            foreach (string agg in ci.sc.TargetAggregations)
                                            {
                                                if (qr.Type == QuickReport.TYPE_MEASURE_BY_DIMENSION &&
                                                    qr.Measures[0].MeasureName == ApplicationBuilder.getAggregatedMeasureName(measures[index].sc.Name, agg) &&
                                                    qr.DimensionColumns[0].ColumnName == ci.sc.Name &&
                                                    Array.IndexOf<string>(ci.sc.TargetTypes, qr.DimensionColumns[0].DimensionName) >= 0)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                        if (found)
                                            break;
                                    }
                                    if (found)
                                        break;
                                }
                            if (!found && ci.categories != null)
                                pickedColumns.Add(ci);
                        }
                    }
                    if (writer != null)
                    {
                        writer.WriteLine(measures[index].sc.Name + ": " + measures[index].score + "/" + measures[index].cardinality + " (" + measures[index].st.Name + ")");
                        foreach (double d in columnsToAnalyze.Keys)
                        {
                            foreach (ColumnInfo ci in columnsToAnalyze[d])
                            {
                                writer.Write("\t" + ci.sc.Name + " (" + d + ")");
                                int col = 0;
                                if (ci.categories != null)
                                {
                                    writer.Write(": " + ci.categories.Count + " categories");
                                    foreach (string cat in ci.categories.Keys)
                                    {
                                        Category c = ci.categories[cat];
                                        if (col % 5 == 0)
                                        {
                                            writer.WriteLine();
                                            writer.Write("\t");
                                        }
                                        writer.Write("\t");
                                        writer.Write(cat + "(" + c.sum / c.count + "," + c.sum + "," + c.sumsq + "," + c.count + ")");
                                        col++;
                                    }
                                }
                                else
                                    writer.Write("\tNo categories");
                                writer.WriteLine();
                            }
                        }
                    }
                    if (pickedColumns.Count > 0 || dateIDColumn != null)
                    {
                        QuickDashboard qb = new QuickDashboard();
                        qb.DashboardName = OverviewDashboardName;
                        qb.Type = QuickDashboard.TYPE_FOUR_REPORTS;
                        qb.PageName = measures[index].sc.Name;
                        int count = 2;
                        string name = qb.PageName;
                        bool found = false;
                        do
                        {
                            found = false;
                            foreach (QuickDashboard sqd in dashboards)
                            {
                                if (sqd.PageName == qb.PageName)
                                {
                                    found = true;
                                    qb.PageName = name + " " + (count++);
                                    break;
                                }
                            }
                        } while (found);
                        if (dateIDColumn != null && dateIDColumn.NumDistinctValues <= 2)
                            dateIDColumn = null;
                        int numReports = Math.Min(pickedColumns.Count, 4 - (dateIDColumn != null ? 1 : 0));
                        int numPrompts = Math.Min(pickedColumns.Count, 5);
                        bool pickedPie = false;
                        List<QuickReport> qrlist = new List<QuickReport>();
                        List<QuickPrompt> qplist = new List<QuickPrompt>();
                        // Add time report if needed
                        if (dateIDColumn != null)
                        {
                            QuickReport qr = new QuickReport();
                            qr.DimensionColumns = new QuickReportDimension[1];
                            qr.DimensionColumns[0] = new QuickReportDimension();
                            qr.DimensionColumns[0].DimensionName = maf.timeDefinition.Name;
                            if (dateIDColumn.NumDistinctValues < 30)
                            {
                                qr.DimensionColumns[0].ColumnName = ApplicationBuilder.DateName;
                            }
                            else
                            {
                                // Figure out how many periods there are
                                List<string[]> buff = scanBuff.getBuffer(dateIDSourceFile, directory);
                                int colindex = 0;
                                for (; colindex < dateIDSourceFile.Columns.Length; colindex++)
                                {
                                    if (dateIDSourceFile.Columns[colindex] == dateIDColumn)
                                        break;
                                }
                                HashSet<int> months = new HashSet<int>();
                                foreach (string[] row in buff)
                                {
                                    try
                                    {
                                        DateTime dt = DateTime.Parse(row[colindex]);
                                        int month = dt.Year * 100 + dt.Month;
                                        months.Add(month);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                if (months.Count < 36)
                                {
                                    qr.DimensionColumns[0].ColumnName = ApplicationBuilder.YearMonth;
                                }
                                else if (months.Count < 36 * 4)
                                {
                                    qr.DimensionColumns[0].ColumnName = ApplicationBuilder.YearQuarter;
                                }
                                else
                                {
                                    qr.DimensionColumns[0].ColumnName = ApplicationBuilder.Year;
                                }
                            }
                            qr.Type = QuickReport.TYPE_MEASURE_BY_DIMENSION;
                            qr.ShowTable = false;
                            qr.Measures = new QuickReportMeasure[1];
                            qr.Measures[0] = new QuickReportMeasure();
                            qr.Measures[0].MeasureLabel = measures[index].sc.Name;
                            qr.Measures[0].MeasureName = dateIDPrefix + ApplicationBuilder.TIME_PREFIX_SEPARATOR + (string)drv[0]["ColumnName"];
                            qr.Measures[0].BaseMeasure = measures[index].sc.Name;
                            qr.Measures[0].TimePrefix = dateIDPrefix;
                            qr.DimensionColumns[0].OrderBy = QuickReport.SORT_ASC;
                            qr.Title = qr.Measures[0].MeasureLabel + REPORT_SLICE_BY_WORD + dateIDPrefix;
                            if (!setReportMeasure(qr, measures[index], false))
                                continue;
                            qr.ChartType = ChartType.Line;
                            qrlist.Add(qr);
                        }
                        // Add attribute prompts
                        for (int i = 0; i < numPrompts; i++)
                        {
                            if (pickedColumns[i].categories.Count > MAX_PROMPT_CATEGORIES)
                                continue;
                            // Add prompt
                            string dimName = null;
                            foreach (string s in pickedColumns[i].sc.TargetTypes)
                            {
                                if (s != "Measure")
                                {
                                    dimName = s;
                                    break;
                                }
                            }
                            QuickPrompt qp = new QuickPrompt();
                            qp.DimensionColumn = new QuickReportDimension();
                            string prefix = null;
                            dimName = getTargetDimension(maf, dimName, pickedColumns[i].sc.Name, out prefix);
                            qp.DimensionColumn.DimensionName = dimName;
                            qp.DimensionColumn.ColumnName = (prefix == null ? "" : prefix) + pickedColumns[i].sc.Name;
                            qp.DimensionColumn.OrderBy = QuickReport.SORT_ASC;
                            qp.VisibleName = qp.DimensionColumn.ColumnName;
                            bool invalid = false;
                            // Make sure prompt names are unique
                            foreach (QuickPrompt sqp in qplist)
                                if (sqp.DimensionColumn.DimensionName == qp.DimensionColumn.DimensionName && sqp.DimensionColumn.ColumnName == qp.DimensionColumn.ColumnName)
                                {
                                    invalid = true;
                                    break;
                                }
                            if (!invalid)
                                foreach (QuickPrompt sqp in qplist)
                                    if (sqp.VisibleName == qp.VisibleName)
                                    {
                                        if (RESTRICT_DIMENSION_PROMPT_NAMES)
                                            invalid = true;
                                        else
                                        {
                                            sqp.VisibleName = sqp.DimensionColumn.DimensionName + "/" + sqp.VisibleName;
                                            qp.VisibleName = qp.DimensionColumn.DimensionName + "/" + qp.VisibleName;
                                        }
                                        break;
                                    }
                            if (!invalid)
                                qplist.Add(qp);
                        }
                        /*
                         * Find out potential duplicate report titles so that they can be fully qualified with dimension 
                         * and attribute names
                         */
                        List<string> reportAttributeNames = new List<string>();
                        List<string> duplicateReportAttributeNames = new List<string>();
                        for (int i = 0; i < numReports; i++)
                        {
                            if (reportAttributeNames.Contains(pickedColumns[i].sc.Name))
                            {
                                duplicateReportAttributeNames.Add(pickedColumns[i].sc.Name);
                            }
                            else
                            {
                                reportAttributeNames.Add(pickedColumns[i].sc.Name);
                            }
                        }

                        // Add attribute reports
                        for (int i = 0; i < numReports; i++)
                        {
                            string dimName = null;
                            foreach (string s in pickedColumns[i].sc.TargetTypes)
                            {
                                if (s != "Measure")
                                {
                                    dimName = s;
                                    break;
                                }
                            }
                            /*
                             * Make sure it's not in a hidden dimension. If so, find dimension
                             * that it is snowflaked from
                             */
                            string prefix = null;
                            dimName = getTargetDimension(maf, dimName, pickedColumns[i].sc.Name, out prefix);
                            // Add columns
                            QuickReport qr = new QuickReport();
                            qr.DimensionColumns = new QuickReportDimension[1];
                            qr.DimensionColumns[0] = new QuickReportDimension();
                            qr.DimensionColumns[0].DimensionName = dimName;
                            qr.DimensionColumns[0].ColumnName = (prefix == null ? "" : prefix) + pickedColumns[i].sc.Name;
                            qr.Type = QuickReport.TYPE_MEASURE_BY_DIMENSION;
                            qr.ShowTable = false;
                            qr.Measures = new QuickReportMeasure[1];
                            qr.Measures[0] = new QuickReportMeasure();
                            qr.Measures[0].MeasureLabel = measures[index].sc.Name;
                            qr.Measures[0].BaseMeasure = measures[index].sc.Name;
                            if (duplicateReportAttributeNames.Contains(qr.DimensionColumns[0].ColumnName))
                            {
                                qr.Title = qr.Measures[0].MeasureLabel + REPORT_SLICE_BY_WORD + qr.DimensionColumns[0].DimensionName + "/" + qr.DimensionColumns[0].ColumnName;
                            }
                            else
                            {
                                qr.Title = qr.Measures[0].MeasureLabel + REPORT_SLICE_BY_WORD + qr.DimensionColumns[0].ColumnName;
                            }
                            if (!setReportMeasure(qr, measures[index], true))
                                continue;
                            qr.Measures[0].OrderBy = QuickReport.SORT_DESC;
                            // Setup chart
                            if (pickedColumns[i].categories.Count < 10 && !pickedPie && measures[index].aggregationRule != "AVG")
                            {
                                qr.ChartType = ChartType.Pie;
                                pickedPie = true;
                            }
                            else if (pickedColumns[i].categories.Count < 7)
                                qr.ChartType = ChartType.Column;
                            else
                                qr.ChartType = ChartType.Bar;
                            if (pickedColumns[i].categories.Count > 10)
                            {
                                addTopNToReport(qr, 10, version);
                            }
                            bool invalid = false;
                            foreach (QuickReport sqr in qrlist)
                                if (sqr.DimensionColumns[0].DimensionName == qr.DimensionColumns[0].DimensionName && sqr.DimensionColumns[0].ColumnName == qr.DimensionColumns[0].ColumnName)
                                {
                                    invalid = true;
                                    break;
                                }
                            if (invalid)
                            {
                                if (numReports < pickedColumns.Count)
                                    numReports++;
                            }
                            else
                                qrlist.Add(qr);
                        }
                        qb.Prompts = qplist.ToArray();
                        qb.Reports = qrlist.ToArray();
                        dashboards.Add(qb);
                    }
                }
                maf.quickDashboards = dashboards;
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex.ToString());
            }
            finally
            {
                if (debug && writer != null)
                    writer.Close();
            }
        }

        private bool setReportMeasure(QuickReport qr, EvaluatedMeasure em, bool name)
        {
            if (em.aggregationRule == "SUM" || (em.aggregationRule == null && Array.IndexOf<string>(em.sc.TargetAggregations, "SUM") >= 0))
            {
                qr.Title = TOTAL_STR + " " + qr.Title;
                if (name)
                    qr.Measures[0].MeasureName = ApplicationBuilder.getStagingColumnMeasureName(em.sc, "SUM", em.st.MeasureNamePrefix);
                qr.Measures[0].AggregationRule = "SUM";
                return true;
            }
            else if (em.aggregationRule == "COUNT" || (em.aggregationRule == null && Array.IndexOf<string>(em.sc.TargetAggregations, "COUNT") >= 0))
            {
                qr.Title = COUNT_STR + " " + qr.Title;
                if (name)
                    qr.Measures[0].MeasureName = ApplicationBuilder.getStagingColumnMeasureName(em.sc, "COUNT", em.st.MeasureNamePrefix);
                qr.Measures[0].AggregationRule = "COUNT";
                return true;
            }
            else if (em.aggregationRule == "AVG" || (em.aggregationRule == null && Array.IndexOf<string>(em.sc.TargetAggregations, "AVG") >= 0))
            {
                qr.Title = AVERAGE_STR + " " + qr.Title;
                if (name)
                    qr.Measures[0].MeasureName = ApplicationBuilder.getStagingColumnMeasureName(em.sc, "AVG", em.st.MeasureNamePrefix);
                qr.Measures[0].AggregationRule = "AVG";
                return true;
            }
            else if (em.aggregationRule == "COUNT DISTINCT" || (em.aggregationRule == null && Array.IndexOf<string>(em.sc.TargetAggregations, "COUNT DISTINCT") >= 0))
            {
                qr.Title = DISTINCT_STR + " " + qr.Title;
                if (name)
                    qr.Measures[0].MeasureName = ApplicationBuilder.getStagingColumnMeasureName(em.sc, "COUNT DISTINCT", em.st.MeasureNamePrefix);
                qr.Measures[0].AggregationRule = "COUNT DISTINCT";
                return true;
            }
            else
                return false;
        }

        public static string getTargetDimension(MainAdminForm maf, string dimName, string colName, out string prefix)
        {
            /*
             * Make sure it's not in a hidden dimension. If so, find dimension
             * that it is snowflaked from
             */
            Hierarchy h = maf.hmodule.getDimensionHierarchy(dimName);
            prefix = null;
            if (h.Hidden)
            {
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    bool founddt = false;
                    if (dt.DimensionName == dimName)
                    {
                        DataRowView[] drv = maf.dimensionColumnMappingsView.FindRows(new object[] { colName, dt.TableName });
                        if (drv.Length == 0)
                            continue;
                        foreach (DimensionTable dt2 in maf.dimensionTablesList)
                        {
                            if (dt2.InheritTable != null && dt2.InheritTable.Length > 0 && dt.TableName == dt2.InheritTable)
                            {
                                dimName = dt2.DimensionName;
                                prefix = dt2.InheritPrefix;
                                founddt = true;
                                break;
                            }
                        }
                    }
                    if (founddt)
                        break;
                }
            }
            return (dimName);
        }

        public static void addTopNToReport(QuickReport qr, int n, int queryLanguageVersion)
        {
            qr.Expressions = new QuickReportExpression[1];
            qr.Expressions[0] = new QuickReportExpression();
            string formula = "DRANK([" + qr.Measures[0].MeasureName + "])";
            if (queryLanguageVersion == 0)
            {
                formula = "MDRANK{" + qr.Measures[0].MeasureName + "}";
            }
            qr.Expressions[0].Formula = formula;
            string rankName = "Rank";
            while (qr.Measures[0].MeasureLabel == rankName || qr.DimensionColumns[0].ColumnName == rankName)
                rankName += "_";
            qr.Expressions[0].Label = rankName;
            qr.DisplayFilter = new DisplayFilter();
            qr.DisplayFilter.Type = DisplayFilter.TYPE_PREDICATE;
            qr.DisplayFilter.ColumnName = rankName;
            qr.DisplayFilter.TableName = "EXPR";
            qr.DisplayFilter.DataType = "Integer";
            qr.DisplayFilter.Operator = DisplayFilter.OPERATOR_LTE;
            qr.DisplayFilter.Operand = n.ToString();
        }

        public static void getTimeMeasure(MainAdminForm maf, DataRowView[] tables, Dictionary<string, SourceFile.SourceColumn> dateIDList, Dictionary<SourceFile.SourceColumn, SourceFile> sfList)
        {
            // Figure out if there's a time-transformed version of this metric
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (mt.InheritTable != null && mt.InheritTable.Length > 0)
                    continue;
                bool found = false;
                foreach (DataRowView dr in tables)
                {
                    if ((string)dr["TableName"] == mt.TableName)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    continue;
                foreach (MeasureTable imt in maf.measureTablesList)
                {
                    if (imt.InheritTable != null && imt.InheritTable == mt.TableName && imt.InheritPrefix.EndsWith(ApplicationBuilder.TIME_PREFIX_SEPARATOR) && imt.InheritPrefix.Length > 2)
                    {
                        // Get the mappings to confirm it is an inherited time table
                        DataRowView[] drv2 = maf.measureColumnMappingsTablesView.FindRows(imt.TableName);
                        // Inherited time tables only have one mapped column
                        if (drv2.Length != 1)
                            continue;
                        string colName = (string)drv2[0]["ColumnName"];
                        if (!(colName.StartsWith(maf.timeDefinition.Name) && colName.EndsWith(ApplicationBuilder.DayIDName)))
                            continue;
                        string prefixCol = imt.InheritPrefix.Substring(0, imt.InheritPrefix.Length - 2);
                        if (dateIDList.ContainsKey(prefixCol))
                            continue;
                        // Find staging table with column in it
                        string sfcol = null;
                        StagingTable sst = null;
                        foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                        {
                            foreach (StagingColumn sc in st.Columns)
                            {
                                if (sc.Name == prefixCol && (sc.DataType == "Date" || sc.DataType == "DateTime") && !sc.NaturalKey && sc.SourceFileColumn != null)
                                {
                                    sst = st;
                                    sfcol = sc.SourceFileColumn;
                                    break;
                                }
                            }
                            if (sst != null)
                                break;
                        }
                        if (sst != null)
                        {
                            foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                            {
                                if (sf.FileName == sst.SourceFile)
                                {
                                    foreach (SourceFile.SourceColumn sfc in sf.Columns)
                                    {
                                        if (sfc.Name == sfcol)
                                        {
                                            dateIDList.Add(prefixCol, sfc);
                                            sfList.Add(sfc, sf);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private EvaluatedMeasure[] getMeasures(ScanBuffer scanBuff)
        {
            // Find significant metrics
            SortedDictionary<double, EvaluatedMeasure> sortedmeasures = new SortedDictionary<double, EvaluatedMeasure>();
            List<EvaluatedMeasure> toRemove = new List<EvaluatedMeasure>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                SourceFile sf = maf.getSourceFile(st);
                foreach (StagingColumn sc in st.Columns)
                {
                    bool isSequenceNumber = false;
                    if (sc.NaturalKey && sc.SourceFileColumn == null)
                    {
                        bool found = false;
                        foreach (string[] level in st.Levels)
                        {
                            Hierarchy h = maf.hmodule.getDimensionHierarchy(level[0]);
                            if (h != null)
                            {
                                Level l = h.findLevel(st.Name);
                                if (l != null && level[1] == l.Name)
                                {
                                    bool hasKey = false;
                                    foreach (LevelKey lk in l.Keys)
                                    {
                                        if (!lk.SurrogateKey && Array.IndexOf<string>(lk.ColumnNames, sc.Name) >= 0)
                                        {
                                            hasKey = true;
                                            break;
                                        }
                                    }
                                    if (hasKey)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (found)
                            isSequenceNumber = true;
                    }
                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0 && (sc.SourceFileColumn != null || isSequenceNumber) && sc.DataType != "DateTime")
                    {
                        List<string[]> buff = scanBuff.getBuffer(sf, directory);
                        if (buff.Count < 10)
                            continue;
                        int index = 0;
                        for (; index < sf.Columns.Length; index++)
                        {
                            if (sf.Columns[index].Name == sc.SourceFileColumn)
                            {
                                break;
                            }
                        }
                        if (index >= sf.Columns.Length && !isSequenceNumber)
                            continue;
                        // Convert to doubles
                        List<double> arr = new List<double>();
                        if (isSequenceNumber)
                        {
                            for (int i = 0; i < buff.Count; i++)
                            {
                                arr.Add(i);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < buff.Count; i++)
                            {
                                try
                                {
                                    if (buff[i][index].Length > 0)
                                    {
                                        arr.Add(double.Parse(buff[i][index]));
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        // Mostly non-null
                        if ((double)arr.Count / (double)buff.Count < 0.75)
                            continue;
                        // Calculate mean
                        double u = 0;
                        foreach (double d in arr) u += d;
                        u /= arr.Count;
                        // Calculate the Kurtosis (degree of spread)
                        double m4 = 0;
                        double m2 = 0;
                        foreach (double d in arr)
                        {
                            double d2 = (d - u);
                            m4 += d2 * d2 * d2 * d2;
                            m2 += d2 * d2;
                        }
                        m4 /= arr.Count;
                        m2 = (m2 / arr.Count) * (m2 / arr.Count);
                        double k = m4 / m2; // Kurtosis (modified - normally would subtract 3)
                        // If not somewhat peaked, skip
                        if (Math.Abs(k - 3) < 0.3 && sc.NaturalKey)
                        {
                            /*
                             * If there are other measures, ignore this one
                             */
                            bool found = false;
                            foreach (StagingColumn ssc in st.Columns)
                            {
                                if (ssc == sc)
                                    continue;
                                if (Array.IndexOf<string>(ssc.TargetTypes, "Measure") >= 0 && ssc.SourceFileColumn != null && !ssc.NaturalKey)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                continue;
                        }
                        // Value is Kurtosis times ((# unique - 1)/# total)^1/4 - slight bias to more values
                        double val = 0;
                        if (isSequenceNumber)
                        {
                            val = -1;
                        }
                        else
                        {
                            val = Math.Abs(k - 3) * Math.Pow((float)(sf.Columns[index].NumDistinctValues - 1) / (float)arr.Count, 0.25);
                            // Bias towards more detailed facts
                            val *= Math.Log(sf.NumRows, 10);
                        }
                        EvaluatedMeasure em = new EvaluatedMeasure();
                        em.score = val;
                        em.st = st;
                        em.sc = sc;
                        em.cardinality = sf.NumRows;
                        em.sequenceNumber = isSequenceNumber;
                        if (!sortedmeasures.ContainsKey(em.score))
                        {
                            bool ok = true;
                            foreach (EvaluatedMeasure sem in sortedmeasures.Values)
                            {
                                if (sem.sc.Name == em.sc.Name)
                                {
                                    if (sem.cardinality > em.cardinality && !(em.sc.NaturalKey && !sem.sc.NaturalKey))
                                        ok = false;
                                    else
                                        toRemove.Add(sem);
                                    break;
                                }
                            }
                            if (ok)
                                sortedmeasures.Add(em.score, em);
                        }
                    }
                }
            }
            foreach (EvaluatedMeasure em in toRemove)
                sortedmeasures.Remove(em.score);
            // Get attributes for each metric
            EvaluatedMeasure[] resultmeasures = new EvaluatedMeasure[sortedmeasures.Count];
            int count = sortedmeasures.Count - 1;
            foreach (EvaluatedMeasure em in sortedmeasures.Values)
                resultmeasures[count--] = em;
            return (resultmeasures);
        }

        private class ColumnInfo : IComparable<ColumnInfo>
        {
            public StagingColumn sc;
            public Dictionary<string, Category> categories;

            #region IComparable<ColumnInfo> Members

            private bool similarCount(int a, int b)
            {
                if (a == b)
                    return (true);
                if (a > b)
                {
                    return ((double)a / (double)b < 1.25);
                }
                else
                {
                    return ((double)b / (double)a < 1.25);
                }
            }

            // Category selection logic for categories with same f-test
            public int CompareTo(ColumnInfo other)
            {
                if (!sc.NaturalKey && other.sc.NaturalKey)
                    return (1);
                else if (sc.NaturalKey && !other.sc.NaturalKey)
                    return (-1);
                if (sc.DataType == "Varchar" && other.sc.DataType != "Varchar")
                    return (1);
                else if (sc.DataType != "Varchar" && other.sc.DataType == "Varchar")
                    return (-1);
                if (sc.Name == other.sc.Name)
                {
                    // If same name, return the one from the bigger table
                    int counta = 0;
                    int countb = 0;
                    if (categories != null)
                        foreach (Category c in categories.Values)
                            counta += c.count;
                    if (other.categories != null)
                        foreach (Category c in other.categories.Values)
                            countb += c.count;
                    return (counta - countb);
                }
                if (sc.DataType == "Varchar" && other.sc.DataType == "Varchar")
                {
                    if ((categories == null && other.categories == null) || (similarCount(categories.Count, other.categories.Count)))
                        // If both varchar, choose the one with the most "modest" width
                        return (Math.Abs(other.sc.Width - IDEAL_VARCHAR_WIDTH) - Math.Abs(sc.Width - IDEAL_VARCHAR_WIDTH));
                }
                // Else choose the one with the fewest categories
                if (categories == null && other.categories == null)
                    return (0);
                else if (categories == null && other.categories != null)
                    return (-1);
                else if (categories != null && other.categories == null)
                    return (1);
                return (other.categories.Count - categories.Count);
            }

            #endregion
        }

        public static Dictionary<StagingColumn, StagingTable> getColumns(MainAdminForm maf, string[] level, StagingTable emst, StagingColumn emsc, out Dictionary<StagingTable, List<Lookup>> lookups)
        {
            Dictionary<StagingColumn, StagingTable> columns = new Dictionary<StagingColumn, StagingTable>();
            Hierarchy h = maf.hmodule.getHierarchy(level[0]);
            lookups = null;
            if (h == null)
                return null;
            if (maf.timeDefinition != null && maf.timeDefinition.Name == h.DimensionName)
                return null;
            Level l = h.findLevel(level[1]);
            if (l == null)
                return null;
            lookups = new Dictionary<StagingTable, List<Lookup>>(new IdentityComparer<StagingTable>());
            getLookupChains(maf, emst, h, l, lookups, new List<Lookup>(0));
            // Get the columns in this dimension
            HashSet<StagingTable> tables = new HashSet<StagingTable>(new IdentityComparer<StagingTable>());
            tables.Add(emst);
            foreach (StagingTable st in lookups.Keys)
                if (!tables.Contains(st)) tables.Add(st);
            foreach (StagingTable st in tables)
            {
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.TargetTypes == null || (Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0 && sc.Name == emsc.Name))
                        continue;
                    // If in the dimension, or snowflaked from the dimension
                    bool OK = false;
                    if (Array.IndexOf<string>(sc.TargetTypes, h.DimensionName) >= 0)
                        OK = true;
                    else
                    {
                        foreach (DimensionTable dt in maf.dimensionTablesList)
                        {
                            bool hasCol = false;
                            if (Array.IndexOf<string>(sc.TargetTypes, dt.DimensionName) >= 0)
                            {
                                if (dt.DimensionColumns != null)
                                    foreach (DimensionColumn dc in dt.DimensionColumns)
                                    {
                                        if (dc.ColumnName == sc.Name)
                                        {
                                            hasCol = true;
                                            break;
                                        }
                                    }
                            }
                            if (hasCol)
                            {
                                foreach (DimensionTable sdt in maf.dimensionTablesList)
                                {
                                    if (sdt.InheritTable == dt.TableName && sdt.DimensionName == h.DimensionName)
                                    {
                                        OK = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!OK)
                        continue;
                    bool found = false;
                    foreach (StagingColumn ssc in columns.Keys)
                    {
                        if (ssc == sc)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (sc != emsc && !found && !sc.DataType.StartsWith("Date ID:") &&
                        sc.SourceFileColumn != null && sc.DataType != "DateTime" && sc.DataType != "Date")
                    {
                        // Make sure source column cardinality is OK
                        SourceFile.SourceColumn sfc = null;
                        SourceFile ssf = maf.getSourceFile(st);
                        foreach (SourceFile.SourceColumn ssfc in ssf.Columns)
                        {
                            if (ssfc.Name == sc.SourceFileColumn)
                            {
                                sfc = ssfc;
                                break;
                            }
                        }
                        if (sfc != null && sfc.NumDistinctValues < MAX_ATTRIBUTE_CARDINALITY)
                            columns.Add(sc, st);
                    }

                }
            }
            return (columns);
        }

        private SortedList<double, List<ColumnInfo>> getAttributesForMeasure(ScanBuffer scanBuff, EvaluatedMeasure em, out bool lowerCardinalityExists)
        {
            int count = 0;
            SourceFile sf = maf.getSourceFile(em.st);
            SortedList<double, List<ColumnInfo>> columnsToAnalyze = new SortedList<double, List<ColumnInfo>>();
            // Get attributes for each grain level
            foreach (string[] level in em.st.Levels)
            {
                Dictionary<StagingTable, List<Lookup>> lookups;
                Dictionary<StagingColumn, StagingTable> columns = getColumns(maf, level, em.st, em.sc, out lookups);
                if (columns == null)
                    continue;

                List<string[]> buff = scanBuff.getBuffer(sf, directory);
                object[][] arr = new object[buff.Count][];
                int mindex = 0;
                for (; mindex < sf.Columns.Length; mindex++)
                {
                    if (em.sc.SourceFileColumn == sf.Columns[mindex].Name)
                    {
                        break;
                    }
                }
                for (int i = 0; i < buff.Count; i++)
                {
                    arr[i] = new object[columns.Count + 1];
                    /*
                     * Populate the metric
                     */
                    try
                    {
                        if (em.sequenceNumber)
                            arr[i][0] = (double)i;
                        else
                        {
                            arr[i][0] = double.Parse(buff[i][mindex]);
                        }
                    }
                    catch (Exception)
                    {
                        arr[i][0] = 0d;
                    }
                    /*
                     * Populate the attributes
                     */
                    count = 1;
                    foreach (StagingColumn sc in columns.Keys)
                    {
                        StagingTable curst = columns[sc];
                        object o = getValue(scanBuff, Object.ReferenceEquals(curst, em.st) ? null : lookups[curst],
                            0, sf, buff[i], sc, directory);
                        arr[i][count] = o;
                        count++;
                    }
                }
                bool[] valid = new bool[columns.Keys.Count];
                double[] tests = new double[columns.Keys.Count];
                /*
                 * Go through each attribute - make sure it satisfies a maximum % of null,
                 * then calculate the F-statistic for relevance
                 */
                count = 0;
                foreach (StagingColumn sc in columns.Keys)
                {
                    int nullCount = 0;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i][count + 1] == null || ((string)arr[i][count + 1]).Length == 0)
                        {
                            nullCount++;
                        }
                    }
                    if ((double)nullCount / (double)arr.Length < (1 - NULL_THRESHOLD))
                    {
                        tests[count] = 1;
                        valid[count] = true;
                    }
                    count++;
                }
                count = 0;
                Dictionary<StagingColumn, Dictionary<string, Category>> categoryinfo = new Dictionary<StagingColumn, Dictionary<string, Category>>();
                bool[] nullKeys = new bool[columns.Keys.Count];
                foreach (StagingColumn sc in columns.Keys)
                {
                    Dictionary<string, Category> categories = getCategories(arr, count, ref nullKeys[count]);
                    categoryinfo.Add(sc, categories);
                    count++;
                }
                count = 0;
                foreach (StagingColumn sc in columns.Keys)
                {
                    if (!valid[count])
                    {
                        count++;
                        continue;
                    }
                    // Calculate overall mean
                    double sum = 0;
                    double sumsq = 0;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        double d = (double)arr[i][0];
                        sum += d;
                        sumsq += d * d;
                    }
                    double mean = sum / arr.Length;
                    // Calculate the mean of each group
                    Dictionary<string, Category> categories = categoryinfo[sc];
                    bool nullKey = nullKeys[count];
                    // Single value columns don't count
                    if (categories.Keys.Count == (nullKey ? 2 : 1))
                    {
                        count++;
                        continue;
                    }
                    // Filter columns of high cardinality that are almost the same cardinality as the measure
                    // Require more cardinality reduction if the category type isn't varchar or a key
                    if (((double)em.cardinality) / ((double)categories.Keys.Count) <
                        Math.Log10(em.cardinality) * (sc.DataType == "Varchar" || sc.NaturalKey ? 3 : 20))
                    {
                        count++;
                        continue;
                    }
                    // Unique values/keys don't count (unless they are the only ones)
                    if (categories.Keys.Count == arr.Length)
                    {
                        bool found = false;
                        // If no other smaller column exists, keep
                        foreach (StagingColumn ssc in columns.Keys)
                        {
                            Dictionary<string, Category> scats = categoryinfo[ssc];
                            if (scats.Keys.Count < categories.Keys.Count)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            count++;
                            continue;
                        }
                    }
                    // 
                    // Calculate the variance within each group
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string catName = (string)arr[i][count + 1];
                        if (catName == null)
                            catName = "null";
                        Category cat = categories[catName];
                        double e = ((double)arr[i][0]) - (cat.sum / cat.count);
                        cat.sumsq += e * e;
                    }
                    // Calculate the variance of the means (sum of sqares of condition)
                    double sumsqCond = 0;
                    // Calculate the sum of squares for the error
                    double sumsqErr = 0;
                    foreach (Category cat in categories.Values)
                    {
                        sumsqCond += cat.count * ((cat.sum / cat.count - mean) * (cat.sum / cat.count - mean));
                        sumsqErr += cat.sumsq;
                    }
                    // Degrees of freedom
                    int df1 = categories.Keys.Count - 1;
                    int df2 = arr.Length - df1;
                    sumsqCond /= df1;
                    sumsqErr /= df2;
                    /* 
                     * Calculate f-test - the result tests the hypothesis that the means and variances
                     * are the same as that of the population. If this test is low, then there is a
                     * low probability that they are the same - hence they are more interesting
                     */
                    double f = 1 - fdistr.fdistribution(df1, df2, sumsqCond / sumsqErr);
                    tests[count] = f;
                    count++;
                }
                count = 0;
                foreach (StagingColumn sc in columns.Keys)
                {
                    if (!valid[count])
                    {
                        count++;
                        continue;
                    }
                    List<ColumnInfo> sclist = null;
                    if (columnsToAnalyze.ContainsKey(tests[count]))
                        sclist = columnsToAnalyze[tests[count]];
                    else
                    {
                        sclist = new List<ColumnInfo>();
                        columnsToAnalyze.Add(tests[count], sclist);
                    }
                    bool found = false;
                    foreach (ColumnInfo ci in sclist)
                    {
                        if (ci.sc == sc)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        ColumnInfo ci = new ColumnInfo();
                        ci.sc = sc;
                        if (categoryinfo.ContainsKey(sc))
                            ci.categories = categoryinfo[sc];
                        sclist.Add(ci);
                    }
                    count++;
                }
            }
            lowerCardinalityExists = false;
            // Filter out any columns that would navigate to a higher level fact
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                sf = maf.getSourceFile(st);
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0 && sc.SourceFileColumn != null &&
                        sc.Name == em.sc.Name &&
                        !Object.ReferenceEquals(st, em.st))
                    {
                        SourceFile cursf = maf.getSourceFile(em.st);
                        if (cursf.NumRows > sf.NumRows)
                        {
                            foreach (string[] level in em.st.Levels)
                            {
                                Dictionary<StagingTable, List<Lookup>> lookups;
                                Dictionary<StagingColumn, StagingTable> columns = getColumns(maf, level, st, sc, out lookups);
                                if (columns == null)
                                    continue;
                                List<double> keysToRemove = new List<double>();
                                foreach (double key in columnsToAnalyze.Keys)
                                {
                                    List<ColumnInfo> cilist = columnsToAnalyze[key];
                                    List<ColumnInfo> toRemove = new List<ColumnInfo>();
                                    foreach (ColumnInfo ci in cilist)
                                    {
                                        foreach (StagingColumn othersc in columns.Keys)
                                        {
                                            if (ci.sc.Name == othersc.Name)
                                            {
                                                toRemove.Add(ci);
                                                break;
                                            }
                                        }
                                    }
                                    if (toRemove.Count > 0)
                                    {
                                        foreach (ColumnInfo ci in toRemove)
                                            cilist.Remove(ci);
                                        if (cilist.Count == 0)
                                            keysToRemove.Add(key);
                                        lowerCardinalityExists = true;
                                    }
                                }
                                foreach (double key in keysToRemove)
                                    columnsToAnalyze.Remove(key);
                            }
                        }
                    }
                }
            }
            return (columnsToAnalyze);
        }

        private Dictionary<string, Category> getCategories(object[][] arr, int count, ref bool nullKey)
        {
            Dictionary<string, Category> categories = new Dictionary<string, Category>();
            for (int i = 0; i < arr.Length; i++)
            {
                string catName = (string)arr[i][count + 1];
                if (catName == null)
                    catName = "null";
                Category cat = null;
                if (categories.ContainsKey(catName))
                    cat = categories[catName];
                else
                {
                    cat = new Category();
                    categories.Add(catName, cat);
                    if (catName == null || catName.Length == 0)
                        nullKey = true;
                }
                cat.sum += (double)arr[i][0];
                cat.count++;
            }
            return (categories);
        }

        private class Category
        {
            public double sum;
            public double sumsq;
            public int count;
        }

        public class Lookup
        {
            public StagingTable sourceTable;
            public SourceFile sourceFile;
            public int[] sourceKey;
            public StagingTable targetTable;
            public SourceFile targetFile;
            public int[] targetKey;
            public Level targetLevel;
        }

        private static Lookup getLookup(MainAdminForm maf, StagingTable source, StagingTable target, Level targetLevel, LevelKey lk)
        {
            Lookup lookup = new Lookup();
            lookup.sourceFile = maf.getSourceFile(source);
            lookup.sourceKey = findKey(source, lookup.sourceFile, lk);
            lookup.sourceTable = source;
            if (lookup.sourceKey == null)
                return (null);
            lookup.targetFile = maf.getSourceFile(target);
            lookup.targetKey = findKey(target, lookup.targetFile, lk);
            lookup.targetTable = target;
            if (lookup.targetKey == null)
                return (null);
            lookup.targetLevel = targetLevel;
            return (lookup);
        }

        private static List<Lookup> getLookups(MainAdminForm maf, StagingTable st, Hierarchy h, Level l)
        {
            List<Lookup> lookupList = new List<Lookup>();
            // Find all levels above current level
            List<Level> ancestors = h.getAncestors(l.Name);
            List<Hierarchy> hierarchies = new List<Hierarchy>();
            foreach (Level cl in ancestors)
                hierarchies.Add(h);
            ancestors.Add(l);
            hierarchies.Add(h);
            if (maf.dependencies != null)
            {
                addSnowflakedLevels(maf, hierarchies, ancestors);
            }
            for (int i = 0; i < ancestors.Count; i++)
            {
                Level al = ancestors[i];
                Hierarchy ah = hierarchies[i];
                foreach (LevelKey lk in al.Keys)
                {
                    if (lk.SurrogateKey)
                        continue;
                    foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
                    {
                        if (((object)st) == ((object)sst))
                            continue;
                        bool found = false;
                        foreach (string[] level in sst.Levels)
                        {
                            if (level[0] == ah.Name)
                            {
                                Level sl = ah.findLevel(level[1]);
                                if (sl != null)
                                {
                                    if (sl == al || sl.findLevel(ah, al.Name) != null)
                                        found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                            continue;
                        Lookup lookup = getLookup(maf, st, sst, al, lk);
                        if (lookup != null)
                        {
                            lookupList.Add(lookup);
                        }
                    }
                }
            }
            return (lookupList);
        }

        private static void addSnowflakedLevels(MainAdminForm maf, List<Hierarchy> hierarchies, List<Level> levels)
        {
            List<Hierarchy> toAddH = new List<Hierarchy>();
            List<Level> toAddL = new List<Level>();
            for (int i = 0; i < levels.Count; i++)
            {
                Hierarchy h = hierarchies[i];
                Level l = levels[i];
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (dt.DimensionName == h.DimensionName && dt.Level == l.Name
                        && dt.InheritTable != null && dt.InheritTable.Length > 0)
                    {
                        foreach (DimensionTable sdt in maf.dimensionTablesList)
                        {
                            if (dt.InheritTable == sdt.TableName && sdt.DimensionName != h.DimensionName)
                            {
                                Hierarchy toaddh = maf.hmodule.getDimensionHierarchy(sdt.DimensionName);
                                if (toaddh != null)
                                {
                                    Level toaddl = toaddh.findLevel(sdt.Level);
                                    if (toaddl != null)
                                    {
                                        toAddH.Add(toaddh);
                                        toAddL.Add(toaddl);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            hierarchies.AddRange(toAddH);
            levels.AddRange(toAddL);
        }

        private static void getLookupChains(MainAdminForm maf, StagingTable st, Hierarchy h, Level l, Dictionary<StagingTable, List<Lookup>> results, List<Lookup> curList)
        {
            List<Lookup> lookupList = getLookups(maf, st, h, l);
            foreach (Lookup lookup in lookupList)
            {
                if (!results.ContainsKey(lookup.targetTable))
                {
                    List<Lookup> newList = new List<Lookup>(curList);
                    newList.Add(lookup);
                    results.Add(lookup.targetTable, newList);
                    getLookupChains(maf, lookup.targetTable, h, lookup.targetLevel, results, newList);
                }
            }
        }

        private static int[] findKey(StagingTable st, SourceFile sf, LevelKey lk)
        {
            int[] key = new int[lk.ColumnNames.Length];
            // See if key is in source
            int count = 0;
            foreach (string s in lk.ColumnNames)
            {
                bool match = false;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.Name == s && sc.SourceFileColumn != null)
                    {
                        match = true;
                        for (int i = 0; i < sf.Columns.Length; i++)
                        {
                            if (sf.Columns[i].Name == sc.SourceFileColumn)
                            {
                                key[count++] = i;
                                break;
                            }
                        }
                        break;
                    }
                }
                if (!match)
                    return (null);
            }
            return (key);
        }

        Dictionary<Lookup, Dictionary<string, string[]>> index = new Dictionary<Lookup, Dictionary<string, string[]>>(new IdentityComparer<Lookup>());
        Dictionary<StagingColumn, int> colIndex = new Dictionary<StagingColumn, int>(new IdentityComparer<StagingColumn>());

        private object getValue(ScanBuffer scanBuff, List<Lookup> lookups, int offset, SourceFile sf, string[] row, StagingColumn sc, string directory)
        {
            if (lookups == null || offset >= lookups.Count)
            {
                if (colIndex.ContainsKey(sc))
                    return (row[colIndex[sc]]);
                else
                {
                    int count = 0;
                    foreach (SourceFile.SourceColumn sfc in sf.Columns)
                    {
                        if (sfc.Name == sc.SourceFileColumn)
                        {
                            colIndex.Add(sc, count);
                            return (row[count]);
                        }
                        count++;
                    }
                    return (null);
                }
            }
            Lookup lk = lookups[offset];
            Dictionary<string, string[]> lindex = null;
            if (index.ContainsKey(lk))
                lindex = index[lk];
            else
            {
                lindex = new Dictionary<string, string[]>();
                index.Add(lk, lindex);
            }
            List<string[]> buff = scanBuff.getBuffer(lk.targetFile, directory);
            string key = "";
            for (int i = 0; i < lk.sourceKey.Length; i++)
            {
                key += "{" + row[lk.sourceKey[i]] + "}";
            }
            if (lindex.ContainsKey(key))
                return (getValue(scanBuff, lookups, offset + 1, lk.targetFile, lindex[key], sc, directory));
            else
            {
                foreach (string[] newrow in buff)
                {
                    bool match = true;
                    for (int i = 0; i < lk.sourceKey.Length; i++)
                    {
                        string s1 = newrow[lk.targetKey[i]];
                        string s2 = row[lk.sourceKey[i]];
                        if (s1.Length != s2.Length || !string.Equals(s1, s2))
                        {
                            match = false;
                            break;
                        }
                    }
                    if (match)
                    {
                        lindex.Add(key, newrow);
                        return (getValue(scanBuff, lookups, offset + 1, lk.targetFile, newrow, sc, directory));
                    }
                }
            }
            return (null);
        }
    }
}
