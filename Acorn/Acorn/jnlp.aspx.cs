﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using System.Collections.Generic;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class jnlp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);

            // causes failure on first time on IE: Util.setUpHeaders(Response);
            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (jnlp.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            if (u.isFreeTrialUser)
            {
                Global.systemLog.Warn("redirecting to home page due to being a free trial user (jnlp.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            string codebase = Util.getRequestBaseURL(Request);
            string jnlp = Util.getSpaceJNLPFile(u, sp, MapPath("~/"), codebase, Request["configfile"]);
 
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(sp.ID + ".jnlp"));
            Response.AddHeader("Content-Length", jnlp.Length.ToString());
            Response.AddHeader("Expires", "-1000");
            Response.ContentType = "application/x-java-jnlp-file";
            Response.Write(jnlp);
            Response.End();
        }
    }
}
