﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomUpload.aspx.cs" 
        Inherits="Acorn.CustomUpload" MasterPageFile="~/CustomUpload.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %> 
    
<asp:content id="headerPlaceHolder" contentplaceholderid="HeaderContentPlaceHolder" runat="server">
    <div style="padding-left: 10px">
        <iframe id="CustomUploadFrame" name="CustomUploadFrame" src="<%= this.customUploadHeaderURL %>" allowTransparency="true" width="100%" height="300px" frameborder="0">
        </iframe>
    </div>
</asp:content>
    
<asp:content id="Content1" contentplaceholderid="mainPlaceholder" runat="server">
    
    <asp:Panel ID="UnableToAccessPanel" runat="server" Visible="false">
        <div style="height: 25px">
            &nbsp;</div>
        <div style="text-align: center">
            <asp:Label runat="server" Font-Bold="true" ID="UnableToAccessMessage" 
                    Text="">
            </asp:Label>
        </div>
        <div style="height: 10px">
            &nbsp;</div>
    </asp:Panel>

        
    <div style="height: 10px">
                        &nbsp;</div>
        
    <div style="padding-left: 10px; padding-right: 10px">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="8%" align="left">
                    <asp:Label runat="server" ID="custoUploadSourcesLabel" Text="Data Source"></asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="custoUploadSourcesList" runat="server" AutoPostBack="true" 
                                OnSelectedIndexChanged="custoUploadSourcesList_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>   
     
    <div style="height: 5px">
           &nbsp;</div>
                        
        
        
    <div style="height:30px; padding-left: 10px">
            <asp:Label runat="server" ID="requiredFormatText" Texomt="Required Data Format"></asp:Label>
    </div>
            
    <div id="ScrollList" style="height: 200px; width: 400px; overflow: auto; padding-left: 10px">            
            <asp:GridView ID="SourceColumns" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="#333333" Width="380px" GridLines="None">
                <RowStyle BackColor="White" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Column Name" HeaderStyle-Width="60%">
                            <HeaderStyle HorizontalAlign="Center" Width="60%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DataType" HeaderText="DataType" HeaderStyle-Width="40%">
                            <HeaderStyle HorizontalAlign="Center" Width="40%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" Height="22px" />
                <AlternatingRowStyle BackColor="LightGray" ForeColor="#333333" />                        
            </asp:GridView>
    </div>
    
    <div style="height: 10px">
          &nbsp;</div>
     
    <div style="padding-left: 10px; padding-right: 10px">                                            
    <table width="100%">
        <tr valign="top">
            <td width="100%" align="left">
                <div>
                    <asp:FileUpload id="Uploader" runat="server" Width="300px" Height="20px"/>
                    <asp:Button runat="server" id="UploadButton" text="Upload" OnClick="Uploader_FileUploaded" />
                </div>
            </td>
        </tr>                    
    </table>
    <asp:UpdatePanel ID="UploadPanel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateScanProgress" runat="server">
                <ProgressTemplate>
                    <div style="height: 30px">
                        &nbsp;</div>
                    <div style="padding-left: 0px">
                        <table width="100%">
                            <tr style="vertical-align: middle;">
                                <td width="100%" align="center">
                                    <table width="100%">
                                        <tr>
                                            <td width="10%" align="left">
                                                Scanning Data
                                            </td>
                                            <td align="left">
                                                <img src="Images/processing.gif" alt="Scanning Data" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <asp:Panel ID="SuccessPanel" runat="server" Visible="false">
                <div style="height: 25px">
                    &nbsp;</div>
                
                <asp:Label runat="server" Font-Bold="true" ID="CurrentStage"></asp:Label>
                
                <div style="height: 10px">
                    &nbsp;</div>
            </asp:Panel>
            
            <asp:UpdatePanel ID="LoadPanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:panel runat="server" ID="InnerLoadPanel" Visible="false" Width="100%">
                        <table width="100%">
		                    <tr style="height: 150px">
			                    <td colspan="3" style="text-align: left; font-size: 12pt; font-weight: bold; width: 100%" align="left">
				                    <img alt="Loading Data" src="Images/wait.gif" style="padding-left: 125px"/>
				                    <div style="height: 20px">
                                        &nbsp;</div>
				                    <div style="height: 20px">
                                        &nbsp;</div>
				                    <div style="font-size: 9pt; font-weight: normal">
					                    <asp:Label ID="PhaseLabel" runat="server"></asp:Label>
				                    </div>
				                    <div style="font-size: 9pt; font-weight: normal">
					                    <asp:Label ID="PercentComplete" runat="server"></asp:Label>
				                    </div>				
			                    </td>
		                    </tr>
	                    </table>
                        <asp:Timer ID="updateTimer" runat="server" OnTick="updateStatus" Interval="5000" Enabled="false"></asp:Timer>
                    </asp:panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Panel ID="UploadIssuesPanel" runat="server" Visible="false">
                <div style="height: 20px">
                    &nbsp;</div>
                <div style="height: 20px">
                    <strong>Warning:</strong> There were issues encountered in loading some data sources,
                    please review:</div>
                <div style="height: 10px">
                    &nbsp;</div>
                <asp:GridView ID="IssueView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="#333333" GridLines="None" Width="600px">
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="Source" HeaderText="Source">
                            <HeaderStyle Width="18%" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Severity" HeaderText="Severity">
                            <HeaderStyle Width="12%" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Error" HeaderText="Issue">
                            <HeaderStyle Width="70%" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" Height="22px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </asp:Panel>            
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    
    <div style="height: 30px">
                    &nbsp;</div>
    <asp:UpdatePanel ID="ErrorPanel" runat="server">
        <ContentTemplate>
          <div style="padding-left: 10px">
            <asp:Panel ID="InnerErrorPanel" runat="server" Width="300px" Visible="false">
                <div style="height: 20px">
                    &nbsp;</div>
                <table width="100%" style="background-color: White" cellpadding="0" cellspacing="0">
                    <tr align="center" style="height: 30px; background-image: url(Images/WarningGradient.gif)"
                        class="warningheader">
                        <td align="center">
                            <asp:Label ID="ErrorHeader" runat="server" />
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="center" style="height: 30px">
                        <td align="center" style="padding-left: 10px; padding-right: 10px">
                            <asp:Label ID="ErrorMessage" runat="server" />                            
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:ImageButton ID="CloseButton" runat="server" ImageUrl="~/Images/Close.png" OnClick="Error_OK_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>   
            </div>         
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="ErrorButton" runat="server" Visible="false"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="ErrorPanelExtender" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="False" Enabled="True" PopupControlID="ErrorPanel" TargetControlID="ErrorButton">
    </cc1:ModalPopupExtender>
    
    
    <asp:UpdatePanel ID="MaintainSessionPanel" runat="server">
    <ContentTemplate>
        <asp:Timer ID="MaintainSessionTimer" runat="server" Interval="600000" OnTick="SessionTick">
        </asp:Timer>
    </ContentTemplate>
    </asp:UpdatePanel>
    
 </asp:content>   