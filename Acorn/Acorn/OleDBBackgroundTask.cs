﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.Background;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Acorn
{
    public class OleDBBackgroundModule : BackgroundModule
    {
        // easy for log monitoring since "finish()" is called by a parent thread -- different ids
        private Guid uploaderID = Guid.NewGuid();
        private Space sp;
        private bool filterExcelRows;
        private string spaceDirectory;
        private string dir;
        private string name;
        private string oleDBConnectString;
        private List<string[]> restrictions;
        private bool pruneBlank;
        private DataTable errorTable;
        Process proc;
        private HashSet<string> processedFiles;
        Dictionary<BackgroundModuleObserver, object> observers;
        private DateTime startTime;
        private Status.StatusCode statusCode = Status.StatusCode.None;
        public readonly string ARG_DELIMITER = " ";
        private string uploaderEngineCommand;
        private int beginSkip;
        private int endSkip;

        public OleDBBackgroundModule(Space sp, bool filterExcelRows, string spaceDirectory, string dir, string name, string oleDBConnectString, 
            List<string[]> restrictions, bool pruneBlank, DataTable errorTable, HashSet<string> processedFiles, string uploaderEngineCommand, int beginSkip, int endSkip)
        {
            this.sp = sp;
            this.filterExcelRows = filterExcelRows;
            this.spaceDirectory = spaceDirectory;
            this.dir = dir;
            this.name = name;
            this.oleDBConnectString = oleDBConnectString;
            this.restrictions = restrictions;
            this.pruneBlank = pruneBlank;
            this.errorTable = errorTable;
            this.processedFiles = processedFiles;
            this.beginSkip = beginSkip;
            this.endSkip = endSkip;
            observers = new Dictionary<BackgroundModuleObserver, object>();
            this.uploaderEngineCommand = uploaderEngineCommand;
        }

        private string escapeArgument(string argument)
        {
            return "\"" + argument.Replace("\"", "\\\"") + "\"";            
        }

        private string getArgs()
        {   
            StringBuilder sb = new StringBuilder();
            sb.Append("filterExcelRows=");
            sb.Append(filterExcelRows);
            
            sb.Append(ARG_DELIMITER);
            sb.Append("spaceDirectory=");            
            sb.Append(escapeArgument(spaceDirectory));

            sb.Append(ARG_DELIMITER);
            sb.Append("dir=");            
            sb.Append(escapeArgument(dir));

            sb.Append(ARG_DELIMITER);
            sb.Append("name=");
            sb.Append(escapeArgument(name));

            
            if(restrictions != null && restrictions.Count > 0)
            {
                StringBuilder restrictionBuilder = new StringBuilder();                
                for(int i=0; i < restrictions.Count; i++)
                {           
                    if(i > 0)
                            restrictionBuilder.Append(";");
                    string[] strArray = restrictions[i];
                    for(int j=0; j < strArray.Length; j++)
                    {
                        if(j > 0)
                            restrictionBuilder.Append(",");
                        string str = strArray[j];
                        restrictionBuilder.Append(str);
                        
                    }
                }
                sb.Append(ARG_DELIMITER);
                sb.Append("restrictions=");
                sb.Append(escapeArgument(restrictionBuilder.ToString()));
            }

            sb.Append(ARG_DELIMITER);
            sb.Append("pruneBlank=");
            sb.Append(pruneBlank);

            if (processedFiles != null && processedFiles.Count > 0)
            {
                StringBuilder processedFilesBuilder = new StringBuilder();
                int count = 0;
                foreach(string str in processedFiles)
                {
                    if (count > 0)
                    {
                        processedFilesBuilder.Append(",");
                    }
                    processedFilesBuilder.Append(str);
                    count++;
                }
                sb.Append(ARG_DELIMITER);
                sb.Append("processedFiles=");
                sb.Append(escapeArgument(processedFilesBuilder.ToString()));
            }
            
            sb.Append(ARG_DELIMITER);
            sb.Append("oleDBConnectString=");
            sb.Append(escapeArgument(oleDBConnectString));

            sb.Append(ARG_DELIMITER);
            sb.Append("beginSkip=");
            sb.Append(beginSkip);

            sb.Append(ARG_DELIMITER);
            sb.Append("endSkip=");
            sb.Append(endSkip);

            return sb.ToString();
        }

        private ProcessStartInfo getProcessStartInfo()
        {
            // Create the ProcessInfo object
            ProcessStartInfo psi = new ProcessStartInfo(uploaderEngineCommand);
            psi.Arguments = getArgs();
            psi.WindowStyle = ProcessWindowStyle.Hidden;            
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;
            psi.WorkingDirectory = (new FileInfo(uploaderEngineCommand)).Directory.FullName;
            return psi;

        }

        #region BackgroundModule Members
        public void run1()
        {
            statusCode = Status.StatusCode.Started;
            UploaderWrapper uWrapper = new UploaderWrapper();
            uWrapper.uploadAndInitScan(getArgs().Split(new string[] { ARG_DELIMITER }, StringSplitOptions.RemoveEmptyEntries));
            statusCode = Status.StatusCode.Complete;
        }

        private static void stOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            Global.systemLog.Info(outLine.Data);
        }

        private static void stErrorHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            Global.systemLog.Error(outLine.Data);
        }

        public void run()
        {
            try
            {
                // Start the process
                // Create the ProcessInfo object
                ProcessStartInfo psi = getProcessStartInfo();
                Global.systemLog.Debug("Starting : uploaderID : " + uploaderID + " : command : " + psi.FileName + " " + psi.Arguments);
                startTime = DateTime.Now;
                proc = Process.Start(psi);
                statusCode = Status.StatusCode.Started;
                proc.BeginOutputReadLine();
                proc.OutputDataReceived += new DataReceivedEventHandler(stOutputHandler);
                proc.BeginErrorReadLine();
                proc.ErrorDataReceived += new DataReceivedEventHandler(stErrorHandler);                                
                proc.WaitForExit();
                if (proc.HasExited)
                {
                    Global.systemLog.Info("Background uploader : space (" + sp.ID + ") - Exited with code: " + proc.ExitCode);
                }
                else
                {
                    Global.systemLog.Info("Background uploader (" + sp.ID + ") - Running");
                    proc.WaitForExit();
                    Global.systemLog.Info("Background uploader finished: " + sp.ID);
                }


                if (proc.ExitCode == 0)
                {
                    statusCode = Status.StatusCode.Complete;
                    Global.systemLog.Info("Background uploader finished successfully : uploaderID : " + uploaderID + " : spaceID : " + sp.ID);
                }
                else
                {
                    statusCode = Status.StatusCode.Failed;
                    Global.systemLog.Info("Background uploader finished with errors : uploaderID : " + uploaderID + " : spaceID : " + sp.ID);
                }
                proc.Close();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while running background uploader : uploaderID : " + uploaderID + " : spaceID : " + sp.ID, ex);
            }
        }

        public void kill()
        {
            try
            {
                if(proc != null)
                {
                    ProcessHandler.KillProcessAndChildren(proc.Id);
                    statusCode = Status.StatusCode.Failed;
                }
                Global.systemLog.Debug("Finished with killing background uploader process  : uploaderID : " + uploaderID + " : spaceID : " + sp.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }            
        }

        public void finish()
        {
            try
            {
                TimeSpan timeTaken = DateTime.Now - startTime;
                Global.systemLog.Info("Total time take for Background Uploader : uploaderID : " + uploaderID + " : spaceID : " + sp.ID + " : " + timeTaken.TotalSeconds);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while calling finish for Background uploader : uploaderID : " + uploaderID + " : spaceID : " + sp.ID, ex);
            }
        }

        public void addObserver(BackgroundModuleObserver observer, object o)
        {
            observers.Add(observer, o);
        }

        public Status.StatusResult getStatus()
        {
            return new Status.StatusResult(statusCode);
        }

        #endregion
    }

    public class UploaderWrapper
    {   
        bool filterExcelRows;
        string spaceDirectory;
        string dir;
        string name;
        string oleDBConnectString;
        List<string[]> restrictions = new List<string[]>();
        bool pruneBlank;        
        HashSet<string> processedFiles = new HashSet<string>();
        int beginSkip = 0;
        int endSkip = 0;

        public void uploadAndInitScan(string[] args)
        {
            validateAndExtractArgs(args);
            DataTable newErrorTable = ApplicationUploader.getNewErrorTable();
            OldedbWrapper oledbwrapper = new OldedbWrapper(filterExcelRows, spaceDirectory, dir, name, oleDBConnectString, restrictions, pruneBlank, newErrorTable, processedFiles, beginSkip, endSkip);
            ScanSummaryList list = oledbwrapper.processOleDBFile();
            if (list == null)
            {
                list = new ScanSummaryList();
            }

            list.processedFiles = processedFiles;
            list.errors = formatErrorTableToArray(newErrorTable);
            ScanSummaryList.serializeScanResults(list, ScanSummaryList.getScanResultsFileName(spaceDirectory, name));
        }

        private string[][] formatErrorTableToArray(DataTable errorTable)
        {
            string[][] response = null;
            if (errorTable != null && errorTable.Rows != null && errorTable.Rows.Count > 0)
            {
                int numErrors = errorTable.Rows.Count;
                response = new string[numErrors][];
                for (int i = 0; i < numErrors; i++)
                {
                    DataRow errorRow = errorTable.Rows[i];
                    response[i] = new string[] { (string)errorRow[0], (string)errorRow[1], (string)errorRow[2] };
                }
            }

            return response;
        }

        private void validateAndExtractArgs(string[] args)
        {
            foreach (string arg in args)
            {
                int index = arg.IndexOf("=");
                string key = arg.Substring(0, index);
                string value = arg.Substring(index + 1);
                switch (key)
                {
                    case "filterExcelRows":
                        filterExcelRows = bool.Parse(value.Trim());
                        break;
                    case "spaceDirectory":
                        spaceDirectory = value;
                        break;
                    case "dir":
                        dir = value;
                        break;
                    case "name":
                        name = value;
                        break;
                    case "oleDBConnectString":
                        oleDBConnectString = value;
                        break;
                    case "restrictions":
                        string[] allRestricts = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (allRestricts != null && allRestricts.Length > 0)
                        {
                            foreach (string str in allRestricts)
                            {
                                string[] restrict = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                restrictions.Add(restrict);
                            }
                        }                        
                        break;
                    case "pruneBlank":
                        pruneBlank = bool.Parse(value);
                        break;
                    case "processedFiles":
                        string[] files = value.Split(',');
                        if (files != null && files.Length > 0)
                        {
                            foreach (string file in files)
                            {
                                processedFiles.Add(file);
                            }
                        }
                        break;
                    case "beginSkip":
                        try
                        {
                            beginSkip = int.Parse(value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "endSkip":
                        try
                        {
                            endSkip = int.Parse(value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}