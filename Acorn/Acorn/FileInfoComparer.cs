﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.IO;

namespace Acorn
{
    public class FileInfoComparer : IComparer<FileInfo>
    {

        #region IComparer Members

        public int Compare(FileInfo x, FileInfo y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }
            
            if (x.LastWriteTime < y.LastWriteTime)
            {
                return -1;
            }
            else if (x.LastWriteTime > y.LastWriteTime)
            {
                return 1;
            }
            
            return 0;
        }

        #endregion
    }
}
