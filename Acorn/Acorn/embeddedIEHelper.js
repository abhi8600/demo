/**
 * @author agarrison
 */
function submitRequest(params) {
	var form1 = document.createElement("form");
	form1.setAttribute("method", "post");
	form1.setAttribute("action", params['birstUrl']);
	for (var key in params) {
		if (params.hasOwnProperty(key) && params[key]) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "birst." + key);
			hiddenField.setAttribute("value", params[key]);
			form1.appendChild(hiddenField);
		}
	}
	document.body.appendChild(form1);
	form1.submit();
	document.body.removeChild(form1);
}

function birstEditDashDone(){
	if(window.parent){
		window.parent.postMessage('birstEditDashDone', '*');
	}
}
