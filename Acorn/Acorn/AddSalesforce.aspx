﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSalesforce.aspx.cs"
    Inherits="Acorn.AddSalesforce" Title="Birst" MasterPageFile="~/WebAdmin.Master" %>

<%@ Register TagPrefix="lc" TagName="LoadControl" Src="~/LoadControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">       
        <asp:PlaceHolder ID="UpdatedDataSourcePlaceHolder" runat="server" Visible="true">
            <div style="font-size: 9pt">
                <a href="FlexModule.aspx?birst.module=admin">Back to Admin</a></div>
        </asp:PlaceHolder>
        <div style="height: 10px">
            &nbsp;</div>
        <asp:MultiView ID="SalesForceMultiView" runat="server">
            <asp:View ID="Authenticate" runat="server">
                <div class="pageheader">
                    Enter Salesforce.com Credentials
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <div>
                    To connect to Salesforce.com, please enter your Salesforce.com account credentials:</div>
                <div style="height: 10px">
                    &nbsp;</div>
                <asp:Label ID="LoginError" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                <table>
                    <tr>
                        <td>
                            Username:
                        </td>
                        <td>
                            <asp:TextBox ID="UsernameBox" MaxLength="64" runat="server" Width="200px"></asp:TextBox>
                            <asp:RegularExpressionValidator id="EmailValidator" runat="server" 
                                ControlToValidate="UsernameBox" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                ErrorMessage="Invalid Salesforce user">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="PasswordBox" runat="server" MaxLength="64" Width="200px" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Save Login:
                        </td>
                        <td>
                            <asp:CheckBox ID="SaveLoginBox" runat="server" />&nbsp;<span style="font-size: 9pt">Allows
                                for automated updating of data</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SFDC Web Service Site:
                        </td>                    
                        <td>
                            <asp:DropDownList id="URLs" runat="server">
                                <asp:ListItem Text="Production" Value="Production"></asp:ListItem>
                                <asp:ListItem Text="Sandbox" Value="Sandbox"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="CredentialsNextButton" runat="server" OnClick="CredentialsNext"
                    ImageUrl="~/Images/Next.png" />
            </asp:View>
            <asp:View ID="ViewSettings" runat="server" OnLoad="SettingsLoad">
                <div class="pageheader">
                    View Current Salesforce Connection Settings
                </div>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:Label ID="LastScheduledRunError" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                <asp:Label ID="DeleteLastError" runat="server" Visible="false" ForeColor="Red"></asp:Label>    
                <asp:PlaceHolder ID="SettingsPlaceholder" runat="server">
                    <div style="font-weight: bold">
                        Currently Connected Objects</div>
                    <div style="height: 10px">
                        &nbsp;</div>
                    <table width="100%">
                        <tr>
                            <td width="460px">
                                <asp:GridView ID="CurrentObjectsView" runat="server" AutoGenerateColumns="False"
                                    CellPadding="0" ForeColor="#333333" GridLines="None" Width="450px">
                                    <RowStyle BackColor="White" ForeColor="#333333" CssClass="listviewcell" />
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Width="50%"></asp:BoundField>
                                        <asp:BoundField DataField="Date" HeaderText="Last Updated" HeaderStyle-Width="50%"
                                            DataFormatString="{0:f}" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <div style="height: 20px">
                        &nbsp;</div>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" style="width: 50px; padding: 5px">
                                <asp:ImageButton ID="UpdateCredentialsButton" runat="server" ImageUrl="~/Images/SFDC_Credentials.png"
                                    ToolTip="Update Credentials" OnClick="ChangeCredentialsClick" />
                            </td>
                            <td>
                                <asp:LinkButton ID="UpdateCredentials" runat="server" OnClick="ChangeCredentialsClick"
                                    CssClass="defaultlinkstyle">Change Credentials</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 50px; padding: 5px">
                                <asp:ImageButton ID="UpdateDataButton" runat="server" ImageUrl="~/Images/SFDC_Retrieve_data.png"
                                    ToolTip="Retrieve Salesforce.com Data" OnClick="UpdateDataClick" />
                            </td>
                            <td>
                                <asp:LinkButton ID="UpdateDataLink" runat="server" OnClick="UpdateDataClick" CssClass="defaultlinkstyle">Retrieve Salesforce.com Data</asp:LinkButton>
                            </td>                            
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>                            
                            <asp:CheckBox ID="ProcessPostExtract" runat="server" Text="Process Data After Extraction is Complete" Font-Size="Smaller" CssClass="defaultlinkstyle"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 50px; padding: 5px">
                                <asp:ImageButton ID="SettingsNextButton" runat="server" ImageUrl="~/Images/SFDC_Modify_selections.png"
                                    ToolTip="Modify Salesforce.com object selection and retrieve data" OnClick="SettingsNext" />
                            </td>
                            <td>
                                <asp:LinkButton ID="SettingsNextLink" runat="server" OnClick="SettingsNext" CssClass="defaultlinkstyle">Modify Salesforce.com object selection and retrieve data</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 50px; padding: 5px">
                                <asp:ImageButton ID="DeleteLastButton" runat="server" ImageUrl="~/Images/Delete_Source.png"
                                    ToolTip="Delete last set of Salesforce.com data processed" OnClick="DeleteLastClick" />
                            </td>
                            <td>
                                <asp:LinkButton ID="DeleteLastLink" runat="server" OnClick="DeleteLastClick" CssClass="defaultlinkstyle">Delete last set of Salesforce.com data processed</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:PlaceHolder>
                <lc:LoadControl id="Loader" runat="server" CreateDashboards="false" LoadButtonOnly="true">
                </lc:LoadControl>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:GridView ID="IssueView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="#333333" GridLines="None" Width="600px">
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="Source" HeaderText="Source">
                            <HeaderStyle Width="18%" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Severity" HeaderText="Severity">
                            <HeaderStyle Width="12%" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Error" HeaderText="Issue">
                            <HeaderStyle Width="70%" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" Height="22px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="SelectObjects" runat="server">
                <asp:MultiView ID="SelectObjectsViews" runat="server" ActiveViewIndex="0">
                    <asp:View ID="SelectObjectsView" runat="server">
                        <div class="pageheader">
                            Select Salesforce.com Objects
                        </div>
                        <div style="height: 5px">
                            &nbsp;</div>
                        <asp:ListView ID="SforceObjects" runat="server" GroupItemCount="4">
                            <LayoutTemplate>
                                <table id="ObjectTable" runat="server" style="background-color: White; width: 98%;
                                    border: solid 1px #555555; padding: 0px; border-collapse: collapse">
                                    <tr>
                                        <td class="listviewheader" colspan="5">
                                            Objects
                                        </td>
                                    </tr>
                                    <tr runat="server" id="groupPlaceholder">
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <GroupTemplate>
                                <tr id="Tr1" runat="server">
                                    <td runat="server" id="itemPlaceholder" />
                                </tr>
                            </GroupTemplate>
                            <ItemTemplate>
                                <td id="Td1" runat="server" style="width: 20%; padding-left: 5px">
                                    <asp:CheckBox runat="server" ID="SelectedBox" Checked='<%# Eval("Selected") %>'>
                                    </asp:CheckBox>&nbsp;<%# Eval("Name") %>
                                </td>
                            </ItemTemplate>
                        </asp:ListView>
                        <div style="height: 10px">
                            &nbsp;</div>
                        <div class="pageheader">
                            Sources Based on Salesforce.com Queries
                        </div>
                        <div style="height: 5px">
                            &nbsp;</div>
                        <asp:GridView ID="QueriesView" runat="server" AutoGenerateColumns="False" OnRowEditing="EditQueryClick"
                            OnRowDeleting="DeleteQueryClick" BorderWidth="0px" Width="">
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="listviewcell" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Width="125px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="Query" HeaderText="Query" HeaderStyle-Width="375px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="QueryEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                            Text="Edit"></asp:LinkButton>&nbsp;
                                        <asp:LinkButton ID="QueryDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                            Text="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                            <AlternatingRowStyle BackColor="White" ForeColor="#555555" />
                            <EmptyDataTemplate>
                                <div style="padding-left: 10px">
                                    No sources based on Salesforce.com queries defined -
                                    <asp:LinkButton ID="NewLink" runat="server" OnClick="AddQueryClick">click here</asp:LinkButton>
                                    to create one</div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <div style="height: 10px">
                            &nbsp;</div>
                        <asp:ImageButton ID="AddQuery" runat="server" OnClick="AddQueryClick" ImageUrl="~/Images/New_Group.png"
                            Visible="false"></asp:ImageButton>
                    </asp:View>
                    <asp:View ID="EditQueryView" runat="server">
                        <div class="pageheader">
                            Edit Salesforce.com Query
                        </div>
                        <div style="height: 10px">
                            &nbsp;</div>
                        <div class="wizard">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                                        Name:
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="QueryNameBox" runat="server"></asp:TextBox>&nbsp;<asp:LinkButton runat="server" ID="GetQueryForObject" Text="Get query for object" OnClick="GetQueryForObjectClick"></asp:LinkButton><asp:Label ID="NonUnique"
                                            runat="server" ForeColor="Red" Visible="false">&nbsp;* query name already exists (choose another)</asp:Label>&nbsp;<asp:Label
                                                ID="InvalidQueryName" runat="server" ForeColor="Red" Visible="false">Invalid Query Name</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Query:
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="QueryBox" runat="server" Width="600px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Label Mapping:
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="LabelMappingBox" runat="server" Width="600px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="UpdateQuery" runat="server" OnClick="UpdateQueryClick" ImageUrl="~/Images/Save_Changes.png">
                            </asp:ImageButton>&nbsp;
                            <asp:ImageButton ID="CancelQueryUpdate" runat="server" OnClick="CancelQueryUpdateClick"
                                ImageUrl="~/Images/Cancel.png"></asp:ImageButton>
                        </div>
                    </asp:View>
                </asp:MultiView>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="SelectNextButton" runat="server" OnClick="SelectNext" ImageUrl="~/Images/Next.png" />&nbsp;
                <asp:ImageButton ID="SaveSelections" runat="server" OnClick="SaveSelectionsClick"
                    ImageUrl="~/Images/Save_Changes.png" />&nbsp;
                <asp:ImageButton ID="CancelSelections" runat="server" OnClick="CancelSelectionsClick"
                    ImageUrl="~/Images/Cancel.png" />
            </asp:View>
            <asp:View ID="ProcessModeStep" runat="server">
                <div class="pageheader">
                    Select Desired Method for Processing Salesforce.com Data
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <asp:RadioButton ID="IntegratedButton" runat="server" Checked="true" GroupName="ModeGroup" />&nbsp;<b>Integrated:</b>
                        </td>
                        <td valign="top">
                            Your Salesforce.com data will be processed at the same time as your other data.
                            To update your Salesforce.com data you will need to manually return to this point
                            and update your data.
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:RadioButton ID="AutomaticButton" runat="server" GroupName="ModeGroup" />&nbsp;<b>Independent:</b>
                        </td>
                        <td valign="top">
                            Your Salesforce.com data may be updated on an automatic schedule independently of
                            your other data. You can also choose to manually update your Salesforce.com data
                            at any point independently of how you update your other data.
                        </td>
                    </tr>
                </table>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="ProcessPrevButton" runat="server" OnClick="ProcessPrev" ImageUrl="~/Images/Previous.png" />&nbsp;
                <asp:ImageButton ID="ScheduleNextButton" runat="server" OnClick="ScheduleNext" ImageUrl="~/Images/ProceedSmall.png" />
            </asp:View>
            <asp:View ID="ScheduleStep" runat="server">
                <div class="pageheader">
                    Select how you would like to update your Salesforce.com data
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <asp:RadioButton ID="ManualButton" runat="server"
                                GroupName="ScheduleGroup" oncheckedchanged="ManualButton_CheckedChanged" />&nbsp;<b>Manual:</b>
                        </td>
                        <td valign="top">
                            To update your Salesforce.com data you will need to manually return to Salesforce.com
                            Connect, retrieve new sets of data and process them.
                        </td>
                    </tr>
                    <tr><td></td></tr>
                   <tr> 
                        <td></td>
                        <td valign="top" >
                            <b><u>In Birst 5.1 and up, these schedules are view only.
                            These schedules will continue to run but any modifications will require the user to login to a version of Birst prior to 5.1.</u></b>  
                        </td>
                    </tr>
                    <tr style="color:#808080">
                        <td valign="top">
                            <asp:RadioButton ID="ScheduledButton" runat="server" GroupName="ScheduleGroup"
                                oncheckedchanged="ScheduledButton_CheckedChanged" />&nbsp;<b>Scheduled (Old):</b>
                        </td>
                        <td valign="top">
                            Your Salesforce.com data will be extracted and loaded based on the following schedule(s):
                            <br />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 180px">
                                        <asp:CheckBox ID="ScheduleDailyButton" runat="server"
                                            oncheckedchanged="ScheduleDailyButton_CheckedChanged" />
                                        &nbsp;<b>Daily:</b>
                                    </td>
                                    <td>
                                        Default nightly automated load schedule
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="width: 180px">
                                        <asp:CheckBox ID="ScheduleMonthlyButton" runat="server"
                                            oncheckedchanged="ScheduleMonthlyButton_CheckedChanged" />
                                        &nbsp;<b>Monthly:</b>
                                    </td>
                                    <td>
                                        Your Salesforce.com data will be updated monthly.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 180px" valign="top">
                                        <asp:CheckBox ID="ScheduleCustomButton" runat="server" 
                                            oncheckedchanged="ScheduleCustomButton_CheckedChanged" />
                                        &nbsp;<b>Custom:</b>
                                    </td>
                                    <td>
                                        Set up multiple custom loads during the week<br />
                                        <table style="width: 100%">
                                            <tr  style="height:1px;">
                                                <td style="height: 1px;" colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 103px">
                                                    Day of Week
                                                </td>
                                                <td style="width: 186px">
                                                    Time of Day (US Central)
                                                </td>
                                                <td style="width: 43px">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>                                            
                                            <tr>
                                                <td style="width: 103px" valign="top">
                                                    <asp:DropDownList ID="ScheduleDayID" runat="server">
                                                        <asp:ListItem>Monday</asp:ListItem>
                                                        <asp:ListItem>Tuesday</asp:ListItem>
                                                        <asp:ListItem>Wednesday</asp:ListItem>
                                                        <asp:ListItem>Thursday</asp:ListItem>
                                                        <asp:ListItem>Friday</asp:ListItem>
                                                        <asp:ListItem>Saturday</asp:ListItem>
                                                        <asp:ListItem>Sunday</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 186px" valign="top">
                                                    <asp:DropDownList ID="ScheduleHourID" runat="server">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:DropDownList ID="ScheduleMinuteID" runat="server">
                                                        <asp:ListItem>00</asp:ListItem>
                                                        <asp:ListItem>30</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;
                                                    <asp:DropDownList ID="ScheduleAMPMID" runat="server">
                                                        <asp:ListItem>AM</asp:ListItem>
                                                        <asp:ListItem>PM</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <asp:UpdatePanel ID="ScheduleListUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <td style="width: 43px" valign="top">
                                                            <asp:Button ID="ScheduleAddID" runat="server" OnClick="ScheduleAddID_Click" Text=">>"
                                                                Width="25px" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="ScheduleDeleteID" runat="server" OnClick="ScheduleDeleteID_Click"
                                                                Text="<<" Width="25px" />
                                                            <br />
                                                        </td>
                                                        <td valign="top">
                                                            <asp:ListBox ID="ScheduleList" runat="server" SelectionMode="Single" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ScheduleList_SelectedIndexChanged" Width="228px" Height="82px">
                                                            </asp:ListBox>
                                                        </td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr> 
                        <td></td>
                        <td valign="top">
                            <b><u>Schedules created with this scheduler can only be done using Birst version 5.1 and up. 
                            New schedules created with this option will not appear when logging into versions of Birst prior to 5.1.<br/>
                            Please note, if this option is selected and Done is clicked, schedules created with the Scheduled (Old) option will be deactivated.</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:RadioButton ID="ScheduledButtonNewScheduler" runat="server" GroupName="ScheduleGroup"
                                oncheckedchanged="ScheduledButtonNewScheduler_CheckedChanged" />&nbsp;<b>Scheduled:</b>
                                <br />&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                <b><asp:Label runat="server" ID="SchedulerVersionNameID"></asp:Label></b>
                        </td>
                        <td valign="top">
                            Your Salesforce.com data will be extracted and loaded based on the following schedule(s):
                            <br />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 180px">
                                        <asp:CheckBox ID="ScheduleDailyButtonNewScheduler" runat="server"
                                            oncheckedchanged="ScheduleDailyButtonNewScheduler_CheckedChanged" />
                                        &nbsp;<b>Daily:</b>
                                    </td>
                                    <td>
                                        Default nightly automated load schedule
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="width: 180px">
                                        <asp:CheckBox ID="ScheduleMonthlyButtonNewScheduler" runat="server"
                                            oncheckedchanged="ScheduleMonthlyButtonNewScheduler_CheckedChanged" />
                                        &nbsp;<b>Monthly:</b>
                                    </td>
                                    <td>
                                        Your Salesforce.com data will be updated monthly.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 180px" valign="top">
                                        <asp:CheckBox ID="ScheduleCustomButtonNewScheduler" runat="server" 
                                            oncheckedchanged="ScheduleCustomButtonNewScheduler_CheckedChanged" />
                                        &nbsp;<b>Custom:</b>
                                    </td>
                                    <td>
                                        Set up multiple custom loads during the week<br />
                                        <table style="width: 100%">
                                            <tr  style="height:1px;">
                                                <td style="height: 1px;" colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 103px">
                                                    Day of Week
                                                </td>
                                                <td style="width: 186px">
                                                    Time of Day (US Central)
                                                </td>
                                                <td style="width: 43px">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>                                            
                                            <tr>
                                                <td style="width: 103px" valign="top">
                                                    <asp:DropDownList ID="ScheduleDayIDNewScheduler" runat="server">
                                                        <asp:ListItem>Monday</asp:ListItem>
                                                        <asp:ListItem>Tuesday</asp:ListItem>
                                                        <asp:ListItem>Wednesday</asp:ListItem>
                                                        <asp:ListItem>Thursday</asp:ListItem>
                                                        <asp:ListItem>Friday</asp:ListItem>
                                                        <asp:ListItem>Saturday</asp:ListItem>
                                                        <asp:ListItem>Sunday</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 186px" valign="top">
                                                    <asp:DropDownList ID="ScheduleHourIDNewScheduler" runat="server">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:DropDownList ID="ScheduleMinuteIDNewScheduler" runat="server">
                                                       <asp:ListItem>00</asp:ListItem>
                                                       <asp:ListItem>30</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;
                                                    <asp:DropDownList ID="ScheduleAMPMIDNewScheduler" runat="server">
                                                        <asp:ListItem>AM</asp:ListItem>
                                                        <asp:ListItem>PM</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <asp:UpdatePanel ID="ScheduleListUpdatePanelNewScheduler" runat="server">
                                                    <ContentTemplate>
                                                        <td style="width: 43px" valign="top">
                                                            <asp:Button ID="ScheduleAddIDNewScheduler" runat="server" OnClick="ScheduleAddIDNewScheduler_Click" Text=">>"
                                                                Width="25px" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="ScheduleDeleteIDNewScheduler" runat="server" OnClick="ScheduleDeleteIDNewScheduler_Click"
                                                                Text="<<" Width="25px" />
                                                            <br />
                                                        </td>
                                                        <td valign="top">
                                                            <asp:ListBox ID="ScheduleListNewScheduler" runat="server" SelectionMode="Single" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ScheduleListNewScheduler_SelectedIndexChanged" Width="228px" Height="82px">
                                                            </asp:ListBox>
                                                        </td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <asp:PlaceHolder runat="server" Visible="false" ID="MigrateSchedulesID">
                                    <td></td>
                                
                                    
                                        <td >
                                            <asp:CheckBox ID="MigrateCheckBoxID" runat="server"/>
                                            &nbsp;<asp:Label runat="server" ID="MigrateLabelID" ></asp:Label>
                                        </td>
                                     </asp:PlaceHolder>               
                               </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:TextBox ID="NextDateBox" runat="server"></asp:TextBox><asp:ImageButton ID="CalImageButton"
                                ImageUrl="~/Images/Calendar.png" runat="server" />
                            <cc1:CalendarExtender ID="NextDateBox_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="NextDateBox" PopupButtonID="CalImageButton" Format="MMMM d, yyyy">
                            </cc1:CalendarExtender>
                        </td>
                        <td valign="top">
                            The date to start schedule (if not manual)
                        </td>
                    </tr>
                </table>
                <div style="height: 20px">
                    &nbsp;</div>
                <asp:ImageButton ID="ProcessingNextButton" runat="server" OnClick="ProcessingNext"
                    ImageUrl="~/Images/Proceed_to_retrieve_data.png" />
                    
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="DoneButtonID" runat="server" 
                    ImageUrl="~/Images/DoneBigger.png" onclick="ScheduleDone_Click" />

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                <asp:ImageButton ID="ScheduleCancelButtonID" runat="server" 
                  ImageUrl="~/Images/CancelBigger.png" OnClick="ScheduleCancel_Click"/>
            </asp:View>
            <asp:View ID="StatusStep" runat="server">
                <div class="pageheader">
                    Retrieving Data
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <asp:UpdatePanel ID="StatusPanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%; text-align: center">
                            <div>
                                <img src="Images/processing.gif" alt="Retrieving Data" /></div>
                            <div style="height: 10px">
                                &nbsp;</div>
                            <div>
                                <asp:Label ID="StatusPrefix" runat="server">Loading Object:</asp:Label>
                                <asp:Label ID="StatusLabel" runat="server"></asp:Label></div>
                            <div>
                                <asp:LinkButton ID="CancelSFDCLoadLink" OnClick="CancelSFDCLoad" runat="server">cancel</asp:LinkButton>
                            </div>
                        </div>
                        <asp:Timer ID="StatusTimer" runat="server" OnTick="StatusTick" Interval="5000">
                        </asp:Timer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:View>
            <asp:View ID="DeletingData" runat="server">
                <div class="pageheader">
                    Deleting Data
                </div>
                <div style="height: 10px">
                    &nbsp;</div>
                <asp:Timer ID="Timer1" runat="server" Interval="2000" OnTick="DeleteDataStatus_Tick">
                </asp:Timer>
                <table width="100%">
                    <tr>
                        <td style="width: 100%" align="center">
                            <img alt="Loading Data" src="Images/wait.gif" /><br />
                            <br />
                            Currently Deleting Data<br />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="ErrorView" runat="server">
                 <table width="100%">
                    <tr valign="middle" style="height: 300px">
                        <td style="text-align: center; font-size: 12pt; font-weight: bold" align="center">
                            <asp:Label ID="GeneralErrorLabel" runat="server" Visible="false" ForeColor="Black"></asp:Label>
                            <br />
                            <asp:HyperLink ID="ContinueLink" runat="server">Click To Continue</asp:HyperLink>
                        </td>
                   </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
