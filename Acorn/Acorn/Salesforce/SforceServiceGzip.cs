﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;

namespace Acorn
{
    public class SforceServiceGzip : sforce.SforceService
    {
        public SforceServiceGzip(bool enableGzip, bool keepalive)
        {
            this.gzip = enableGzip;
            this.keepalive = keepalive;
        }

        private bool gzip, keepalive;

        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            WebRequest wr = base.GetWebRequest(uri);
            if (!keepalive)
                ((HttpWebRequest)wr).KeepAlive = false;
            // sforce support compression in both directions.
            return new GzipWebRequest(wr, gzip, gzip);
        }
    }
}
