﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data.Odbc;
using System.Text;
using Acorn.DBConnection;
using Acorn.Utils;
using System.Xml.Linq;

namespace Acorn
{
    public class SalesforceSettings
    {
        public static int CurrentVersion = 2;
        public static string AMTime = "AM";
        public static string PMTime = "PM";
        public int version;
        public enum UpdateSchedule { Manual, Daily, Weekly, Monthly };
        public string username;
        public string password;
        public bool saveAuthentication;
        public bool automatic;
        // SFDC can have multiple schedules
        public UpdateSchedule schedule;
        public ScheduleInfo[] scheduleDetails;
        public DateTime nextDate = DateTime.Now;
        public DateTime startDateNewScheduler = DateTime.Now;
        public SalesforceObjectConnection[] objects;
        public enum SiteType { Production, Sandbox };
        public SiteType sitetype;
        public ScheduleSFDCJob scheduledSFDCJob;
        //public ScheduleInfoExternalScheduler[] schedulerDetailsNewScheduler;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        public DateTime startExtractionTime = DateTime.Now;
        public DateTime endExtractionTime = DateTime.Now;

        [XmlIgnore]
        public string Username
        {
            get
            {
                if (username == null || password.Length == 0)
                    return null;
                return Performance_Optimizer_Administration.MainAdminForm.decrypt(username, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
            set
            {
                username = Performance_Optimizer_Administration.MainAdminForm.encrypt(value, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
        }

        [XmlIgnore]
        public string Password
        {
            get
            {
                if (password == null || password.Length == 0)
                    return null;
                return Performance_Optimizer_Administration.MainAdminForm.decrypt(password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
            set
            {
                password = Performance_Optimizer_Administration.MainAdminForm.encrypt(value, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            }
        }

        public bool Automatic
        {
            get
            {
                return automatic;
            }
            set
            {
                automatic = value;
            }
        }

        public bool SaveAuthentication
        {
            get
            {
                return saveAuthentication;
            }
            set
            {
                saveAuthentication = value;
            }
        }

        public SalesforceObjectConnection[] Objects
        {
            get
            {
                return objects;
            }
        }

        public bool HasObjects()
        {
            return (objects != null && objects.Length > 0);
        }

        public class ScheduleInfo
        {
            public string id;
            public UpdateSchedule type;
            public string Day;
            public int Hour;
            public int Minute;
            public string AmPm;
            public DateTime nextScheduleTime = DateTime.MinValue;
            public bool enable;
            // only used for the new scheduler
        }

        public class ScheduleSFDCJob
        {
            public string id;
            public string clientId;
            public string sandBoxUrl;
            public bool processAfterExtraction;
            public ScheduleInfoExternalScheduler[] schedulerDetailsNewScheduler;
            public SchedulerVersion schedulerVersion;
        }

        public class ScheduleInfoExternalScheduler
        {
            public string id;
            public UpdateSchedule type;
            public string Day;
            public int Hour;
            public int Minute;
            public string AmPm;
            public DateTime startTime;
            public bool enable;

            // name of the release for which this schedule is configured
            [XmlIgnore]
            public string scheduleReleaseName;
            // is migration allowed for this schedule
            [XmlIgnore]
            public string migrationAllowed;
            // is this schedule configured on the primary scheduler
            [XmlIgnore]
            public string isPrimaryScheduler;
            // primary scheduler name
            [XmlIgnore]
            public string primarySchedulerName;
            // only used for the new scheduler
        }
        
        public class SalesforceObjectConnection
        {
            public string name;
            public string query;
            public DateTime lastUpdatedDate;
            public DateTime lastSystemModStamp;
            public string[] columnNames;

            public SalesforceObjectConnection()
            {
            }

            public SalesforceObjectConnection(string name)
            {
                this.name = name;
            }

            public void addColumnName(string s)
            {
                if (columnNames == null)
                {
                    columnNames = new string[1];
                    columnNames[0] = s;
                }
                else
                {
                    bool found = false;
                    foreach (string cs in columnNames)
                    {
                        if (s == cs)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        List<string> old = new List<string>(columnNames);
                        old.Add(s);
                        columnNames = old.ToArray();
                    }
                }
            }
        }

        public static List<SalesforceSettings.ScheduleInfo> getScheduleInfo(List<SalesforceSettings.ScheduleInfo> scheduleDetails, SalesforceSettings.UpdateSchedule findType)
        {
            List<SalesforceSettings.ScheduleInfo> response = new List<ScheduleInfo>();
            if (scheduleDetails != null && scheduleDetails.Count > 0)
            {
                foreach (ScheduleInfo si in scheduleDetails)
                {
                    if (si.type == findType)
                    {
                        response.Add(si);
                    }
                }
            }

            response = response.Count > 0 ? response : null;
            return response;
        }
        

        public static SalesforceSettings getSalesforceSettings(string filename)
        {
            if (!File.Exists(filename))
            {
                // Global.systemLog.Info("Did not find salesforce settings file: " + filename);
                SalesforceSettings sfs = new SalesforceSettings();
                sfs.objects = new SalesforceObjectConnection[0];
                return sfs;
            }
            TextReader reader = new StreamReader(filename);
            XmlReader xreader = new XmlTextReader(reader);
            XmlSerializer serializer = new XmlSerializer(typeof(SalesforceSettings));
            try
            {
                SalesforceSettings settings = (SalesforceSettings)serializer.Deserialize(xreader);
                // check the version number first and see if it is equal to the Current version, if not
                // update the current settings
                int version = settings.version;
                if (version < 2)
                {
                    xreader.Close();
                    reader.Close();
                    // confirm to the new schedule format, save it and re-read it                     
                    ScheduleInfo[] newFormatSchedule = settings.scheduleDetails;
                    if (newFormatSchedule == null || newFormatSchedule.Length == 0)
                    {
                        // confirm to new standard
                        ScheduleInfo scheduleInfo = new ScheduleInfo();
                        scheduleInfo.id = Guid.Empty.ToString();
                        scheduleInfo.type = settings.schedule;                        
                        // Midnight
                        // Get the day from the next date
                        scheduleInfo.Day = settings.nextDate.DayOfWeek.ToString();                        
                        scheduleInfo.Hour = 12;
                        scheduleInfo.Minute = 0;
                        scheduleInfo.AmPm = AMTime;
                        scheduleInfo.nextScheduleTime = settings.nextDate;
                        scheduleInfo.enable = true;
                        settings.scheduleDetails = new ScheduleInfo[] { scheduleInfo };
                        settings.saveSettings(filename, false);
                        // Reload it again now
                        reader = new StreamReader(filename);
                        xreader = new XmlTextReader(reader);
                        serializer = new XmlSerializer(typeof(SalesforceSettings));
                        settings = (SalesforceSettings)serializer.Deserialize(xreader);                         
                    }
                }

                return settings;
            }
            catch (System.InvalidOperationException ex)
            {
                Global.systemLog.Error(ex);
                SalesforceSettings sfs = new SalesforceSettings();
                sfs.objects = new SalesforceObjectConnection[0];
                return sfs;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public ScheduleInfo getScheduleInfo(Guid scheduleId)
        {
            return getScheduleInfo(scheduleId.ToString());
        }

        public ScheduleInfo getScheduleInfo(string scheduleId)
        {
            ScheduleInfo scheduleInfo = null;
            if (scheduleDetails != null && scheduleDetails.Length > 0)
            {
                foreach (ScheduleInfo si in scheduleDetails)
                {
                    if (si.id == scheduleId)
                    {
                        scheduleInfo = si;
                        break;
                    }
                }
            }

            return scheduleInfo;
        }

        public static ScheduleInfoExternalScheduler getScheduleInfoNewScheduler(List<ScheduleInfoExternalScheduler> schedulerDetailsNewScheduler, string scheduleId)
        {
            ScheduleInfoExternalScheduler scheduleInfo = null;

            if (schedulerDetailsNewScheduler != null && schedulerDetailsNewScheduler.Count > 0)
            {
                foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in schedulerDetailsNewScheduler)
                {
                    if (si.id == scheduleId)
                    {
                        scheduleInfo = si;
                        break;
                    }
                }
            }

            return scheduleInfo;
        }

        public void saveSettings(string directory)
        {
            saveSettings(directory, true);
        }
        
        /// <summary>
        /// save sfdc settings into external file.
        /// if isDirectory flag is true, the supplied name is directory in which case 
        /// settings will be stored in sforce.xml.
        /// if isDirectory flag is false, settings will be stored in the given file name
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="isDirectory"></param>
        public void saveSettings(string directoryOrFileName, bool isDirectory)
        {
            version = CurrentVersion;
            string tusername = null;
            string tpassword = null;
            if (!saveAuthentication)
            {
                tusername = username;
                tpassword = password;
                username = null;
                password = null;
            }
            StreamWriter writer = null;
            try
            {
                string fileName = null;
                if (isDirectory)
                {
                    fileName = directoryOrFileName + "//sforce.xml";
                }
                else
                {
                    fileName = directoryOrFileName;
                }
                writer = new StreamWriter(fileName);
                XmlSerializer serializer = new XmlSerializer(typeof(SalesforceSettings));
                serializer.Serialize(writer, this);
                if (!saveAuthentication)
                {
                    username = tusername;
                    password = tpassword;
                }
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public void saveSettings(Space sp, bool updateSchedules)
        {
            if (updateSchedules)
            {
                updateSchedule(sp);
            }
            saveSettings(sp.Directory);
        }

        public void updateScheduleInExternalScheduler(Space sp, User user)
        {
            XElement scheduleListElem = new XElement("ScheduleList");

            // all the scheduled jobs are currently processed after extraction. Could be changed in future
            scheduledSFDCJob.processAfterExtraction = true;
            if (scheduledSFDCJob.schedulerDetailsNewScheduler != null && scheduledSFDCJob.schedulerDetailsNewScheduler.Length > 0)
            {
                foreach (ScheduleInfoExternalScheduler si in scheduledSFDCJob.schedulerDetailsNewScheduler)
                {

                    if (si.type == UpdateSchedule.Manual)
                    {
                        continue;
                    }

                    ScheduleCronExpression cronExpression = new ScheduleCronExpression();
                    if (si.type == UpdateSchedule.Daily)
                    {
                        cronExpression.interval = ScheduleCronExpression.INTERVAL_DAILY;
                        cronExpression.amPm = ScheduleCronExpression.TIME_AM;
                    }
                    else if (si.type == UpdateSchedule.Monthly)
                    {
                        cronExpression.interval = ScheduleCronExpression.INTERVAL_MONTHLY;
                        cronExpression.amPm = ScheduleCronExpression.TIME_AM;
                    }
                    else if (si.type == UpdateSchedule.Weekly)
                    {
                        cronExpression.interval = ScheduleCronExpression.INTERVAL_WEEKLY;
                        cronExpression.amPm = si.AmPm;
                        cronExpression.hour = si.Hour;
                        cronExpression.minutes = si.Minute;
                        cronExpression.daysOfWeek = new int[] { ScheduleCronExpression.getIndexIntDayOfWeek(si.Day) };
                    }
                    else
                    {
                        // some other interval we don't understand
                        Global.systemLog.Warn("Unable to understand interval type " + si.type);
                        continue;
                    }

                    string cron = cronExpression.create();
                    XElement scheduleElem =
                        new XElement(Constants.NODE_SCHEDULE,
                            new XElement(Constants.NODE_SCHEDULE_ID, si.id),
                            new XElement(Constants.NODE_CRON, cron),
                            new XElement(Constants.NODE_TIMEZONE, "CST"),
                            new XElement(Constants.NODE_START_TIME, si.startTime.ToShortDateString()),
                            new XElement(Constants.NODE_ENABLE, si.enable)
                             );
                    scheduleListElem.Add(scheduleElem);
                }
                SchedulerUtils.addOrUpdateSFDCSchedules(sp, user, scheduledSFDCJob, scheduleListElem);
            }
            else
            {
                SchedulerUtils.removeSFDCJob(sp, user, scheduledSFDCJob);
            }
        }

        private void updateSchedule(Space sp)
        {
            if (scheduleDetails != null && scheduleDetails.Length > 0)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    foreach (ScheduleInfo si in scheduleDetails)
                    {
                        UpdateSchedule scheduleType = si.type;
                        if (scheduleType == UpdateSchedule.Manual)
                        {
                            // if schedule type is manual, remove everything else
                            Database.removeTaskSchedule(conn, mainSchema, sp.ID, Guid.Empty, SalesforceLoader.SOURCE_GROUP_NAME);
                        }
                        else
                        {
                            if (si.enable)
                            {
                                si.nextScheduleTime = SalesforceLoader.getNextDate(si, si.nextScheduleTime);
                                Database.updateTaskSchedule(conn, mainSchema, sp.ID, new Guid(si.id), SalesforceLoader.SOURCE_GROUP_NAME, si.nextScheduleTime, null);
                            }
                            else
                            {
                                Database.removeTaskSchedule(conn, mainSchema, sp.ID, new Guid(si.id), SalesforceLoader.SOURCE_GROUP_NAME);
                            }
                        }
                    }
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        public SalesforceObjectConnection getObjectConnection(string name)
        {
            SalesforceObjectConnection soc = null;
            if (objects == null)
            {
                objects = new SalesforceObjectConnection[1];
                soc = new SalesforceObjectConnection(name);
                objects[0] = soc;
                return soc;
            }
            foreach (SalesforceObjectConnection ssoc in objects)
            {
                if (ssoc.name == name)
                    return ssoc;
            }
            soc = new SalesforceObjectConnection(name);
            List<SalesforceObjectConnection> old = new List<SalesforceObjectConnection>(objects);
            old.Add(soc);
            objects = old.ToArray();
            return soc;
        }

        public void updateNonqueryObjects(List<string> objectList)
        {
            if (objects == null)
                return;
            List<SalesforceObjectConnection> newlist = new List<SalesforceObjectConnection>();
            foreach (SalesforceObjectConnection soc in objects)
            {
                if (soc.query != null && soc.query.Length > 0)
                    newlist.Add(soc);
                else
                {
                    if (objectList.IndexOf(soc.name) >= 0)
                        newlist.Add(soc);
                }
            }
            objects = newlist.ToArray();
        }
    }
}
