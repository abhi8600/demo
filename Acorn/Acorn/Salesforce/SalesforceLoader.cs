﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using Acorn.sforce;
using Performance_Optimizer_Administration;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.Data.Odbc;
using System.Web.Services.Protocols;
using System.Net;
using Acorn.Background;
using System.Web.SessionState;
using Acorn.DBConnection;
using Acorn.Utils;
using System.Text.RegularExpressions;

namespace Acorn
{
    public class SalesforceLoader: BackgroundModule, IExecutableTask
    {
        public static string LOAD_GROUP_NAME = "SFORCE";
        public static string SOURCE_GROUP_NAME = "SFORCE";        
        public static int BASE_SFORCE_LOAD_NUMBER = 100000;
        private static Dictionary<Guid, SalesforceLoader> status = new Dictionary<Guid, SalesforceLoader>();
        private List<string> objects;
        private MainAdminForm maf;
        private Space sp;
        private SforceService service;
        private User u;
        private string currentObject;
        private int numRecords;
        private SalesforceSettings settings = new SalesforceSettings();
        private DataTable errors;
        private bool killed = false;
        private int maxRecords = 10000;
        private bool processAfterExtraction;
        private HttpSessionState session;
        // retryCount which is incremented at each retries
        private int retryCount = 0;
        // Flag to signal failure of object
        private bool objectExtractFailed = false;
        // max retry count for any object retrieval
        private int maxRetries = 4;
        // flag to signal failure of full extract on any object failure
        private bool terminateExtractOnObjectFailure = false;
        private Dictionary<BackgroundModuleObserver, object> observers;
        private bool updateScheduleTimes;
        private bool postExtractOperation = false;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string sfdcClientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
        private static string supportValidationUsername = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCsupportValidationUsername"];
        private static string supportValidationPassword = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCsupportValidationPassword"];
        private static string supportSOQLQuery = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCSupportSOQLQuery"];

        // exponential back off params
        // exp base
        public static int retryExpBase = 3;
        // exp min power
        public static int retryPowerMin = 1;
        // exp max power
        public static int retryPowerMax = 4;
        
        public const string SALESFORCE_LOADING_SUCCESSFUL = "SFORCE Loading Successful";
        public const string SALESFORCE_LOADING_SKIPPED = "SFORCE Loading Skipped";
        public const string SALESFORCE_LOADING_FAILED = "SFORCE Loading Failed";
        public static char[] new_lang_allowable = new char[] { 
                    '_', ' ', ':', '$', '#', '%', '-', '/', '&', '!', 
                    '^', '@', ',', ';', '>', '<'};
        public static char[] old_lang_allowable = new char[] { 
                    '!', '@', '#', '$', '%', '^', '&', '*', '/', '<', 
                    '>', ',', '|', '=', '+', '-', '_', ';',':'};

        public static SalesforceLoader getLoader(Guid spaceID)
        {
            SalesforceLoader result = null;
            lock (status)
            {
                if (status.ContainsKey(spaceID))
                    result = status[spaceID];
            }
            return result;
        }

        public static void clearLoader(Guid spaceID)
        {
            lock (status)
            {
                status.Remove(spaceID);
            }
        }

        public SalesforceLoader()
        {
            observers = new Dictionary<BackgroundModuleObserver, object>();
        }

        public SalesforceLoader(List<string> objects, MainAdminForm maf, Space sp, User u, SforceService service, 
            SalesforceSettings settings, bool loadAfterExtracttion, HttpSessionState httpSession, bool updateScheduleTimes)
        {
            this.objects = objects;
            this.maf = maf;
            this.sp = sp;
            maxRecords = int.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["TestModeNumRows"]);
            this.service = service;
            this.u = u;
            this.settings = settings;
            this.processAfterExtraction = loadAfterExtracttion;
           
            lock (status)
            {
                if (status.ContainsKey(sp.ID))
                {
                    status[sp.ID] = this;
                }
                else
                {
                    status.Add(sp.ID, this);
                }
            }
           
            this.session = httpSession;
            this.maxRetries = Global.SFDCObjectRetriesOnFailure;
            this.terminateExtractOnObjectFailure = Global.SFDCFailExtractOnObjectFailure;
            observers = new Dictionary<BackgroundModuleObserver, object>();
            this.updateScheduleTimes = updateScheduleTimes;
        }

        public Space getSpace()
        {
            return sp;
        }

        public User getUser()
        {
            return u;
        }

        public bool getProcessAfterExtraction()
        {
            return processAfterExtraction;
        }


        public void addObserver(BackgroundModuleObserver bmo, object o)
        {
            observers.Add(bmo, o);
        }

        private void markObjectExtractFailure()
        {
            objectExtractFailed = true;            
        }

        private void resetRetryCountAndObjectFailureFlag()
        {
            resetObjectFailureFlag();
            retryCount = 0;
        }

        private void resetObjectFailureFlag()
        {
            objectExtractFailed = false;
        }

        private bool maxRetriesReached()
        {
            return retryCount >= maxRetries;
        }

        private bool isPostExtractOperationHalted()
        {
            return postExtractOperation;
        }

        /// <summary>
        /// called when object failure happens after all the retries
        /// </summary>
        /// <returns></returns>
        private void haltPostExtractOperation()
        {
            postExtractOperation = true;
        }

        public void getData()
        {
            bool saveSettingsForNextExtraction = false;
            bool previousUberTransaction = maf.isInUberTransaction();
            try
            {
                maf.setInUberTransaction(true);
                settings.startExtractionTime = DateTime.Now;
                // Delete any old remnant kill file which was not cleaned
                Util.deleteSFDCKillLockFile(sp);
                if (Util.isLoadLockFilePresent(sp))
                {
                    Global.systemLog.Error("Cannot run extraction. Space is loading. Load Lock file is present for the space : " + (sp != null ? sp.ID.ToString() : " "));
                    return;
                }
                // Create a blank file to signal the start of extract
                Util.createIfNotExistsSFDCExtractStatusFilePresent(sp);
                resetSFDCQueryLog(sp);
                foreach (string name in objects)
                {
                    resetRetryCountAndObjectFailureFlag();                    
                    if (!isTerminated())
                    {
                        do
                        {   
                            // This condition will be bypassed on the very first try
                            if (objectExtractFailed)
                            {                                
                                retryCount++;
                                int delayTime = getDelayForRetry(retryCount);
                                Thread.Sleep(delayTime * 1000);
                                Global.systemLog.Warn("Retry Number : " + retryCount +  " after waiting : " + delayTime + " seconds for extraction of object " + name);
                            }

                            resetObjectFailureFlag();

                            try
                            {
                                getObject(name, service);                                
                            }
                            catch (Exception ex)
                            {
                                Global.systemLog.Error(ex);                                
                            }                            

                            // anytime maxRetries reached, mark this extract to not allow processing later
                            if (maxRetriesReached())
                            {
                                haltPostExtractOperation();
                            }

                            if(isTerminated())
                            {
                                break;
                            }
                        } while (objectExtractFailed && !maxRetriesReached());

                        // if the extract failed after retries, see if the entire extract needs to be cancelled fully
                        if (objectExtractFailed && terminateExtractOnObjectFailure)
                        {
                            kill();
                            break;
                        }
                    }
                }

                // flip the flag at the end of the extraction. This takes care of a weird intermittent
                // use case where during server restart when thread gets aborted but goes directly to finally block
                saveSettingsForNextExtraction = true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);                
            }
            finally
            {
                try
                {
                    settings.endExtractionTime = DateTime.Now;
                    // delete the extract lock file
                    Util.deleteSFDCExtractStatusFileIfItExists(sp, true, -1);
                    // delete the kill lock file if any
                    Util.deleteSFDCKillLockFile(sp);
                    if (saveSettingsForNextExtraction)
                    {
                        settings.saveSettings(sp, updateScheduleTimes);
                        if (errors == null || errors.Rows == null || errors.Rows.Count == 0)
                        {
                            setSFDCExtractTimes(sp, maf, settings, true);
                        }
                        SalesforceLoader.addLoadAndScriptGroups(maf);
                        Util.saveApplication(maf, sp, null, u);
                    }
                    maf.setInUberTransaction(previousUberTransaction);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Error during SFDC extraction ", ex2);
                }
                lock (status)
                {
                    status.Remove(sp.ID);
                }
            }
        }

        /// <summary>
        ///  DO NOT REMOVE THIS METHOD ..called by AcorExecute -- wrapper around Acorn during Salesforce scheduled extration
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="maf"></param>
        /// <param name="settings"></param>
        public static void setSFDCExtractTimes(Space sp, MainAdminForm maf, SalesforceSettings settings)
        {
            setSFDCExtractTimes(sp, maf, settings, false);
        }

        public static void setSFDCExtractTimes(Space sp, MainAdminForm maf, SalesforceSettings settings, bool transientUbserTransactionFlag)
        {
            bool previousUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(transientUbserTransactionFlag);
            Util.setSFDCExtractInitiatedTimeVariable(sp, maf, settings.startExtractionTime);
            Util.setSFDCExtractCompletionTimeVariable(sp, maf, settings.endExtractionTime);
            maf.setInUberTransaction(previousUberTransaction);
        }

        public static void addLoadAndScriptGroups(MainAdminForm maf)
        {
            Util.addLoadGroup(maf, SalesforceLoader.LOAD_GROUP_NAME);
            Util.addScriptGroup(maf, SalesforceLoader.LOAD_GROUP_NAME);
        }

        char[] allowable = new_lang_allowable;

        public void setAllowable(int queryLanguageVersion)
        {
            allowable = getAllowableChars(queryLanguageVersion);
        }

        public static char[] getAllowableChars(int queryLanguageVersion)
        {
            char[] allowableChars = null;
            if (queryLanguageVersion == 0)
                allowableChars = old_lang_allowable;
            else
                allowableChars = new_lang_allowable;
            return allowableChars;
        }

        private string getAllowable(String f, bool allowSpaces)
        {
            return getAllowableValue(f, allowSpaces, allowable);
        }

        private static string getAllowableValue(string f, bool allowSpaces, Space sp)
        {
            return getAllowableValue(f, allowSpaces, getAllowableChars(sp.QueryLanguageVersion));
        }
        
        private static string getAllowableValue(string f, bool allowSpaces, char[] allowableChars)
        {
            StringBuilder sb = new StringBuilder();
            // Restrict the characters that are allowed
            for (int i = 0; i < f.Length; i++)
            {
                if ((f[i] >= 'a' && f[i] <= 'z') ||
                    (f[i] >= 'A' && f[i] <= 'Z') ||
                    (f[i] >= '0' && f[i] <= '9') ||
                    (allowSpaces && f[i] == ' '))
                {
                    sb.Append(f[i]);
                    continue;
                }
                bool ok = false;
                foreach (char c in allowableChars)
                    if (f[i] == c)
                    {
                        ok = true;
                        break;
                    }
                if (!ok)
                    sb.Append('_');
                else
                    sb.Append(f[i]);
            }
            return sb.ToString();
        }

        private void getObject(string name, SforceService service)
        {
            Global.systemLog.Info("Space: " + sp.Name + " - object retrieval process started for SFDC object: " + name);
            
            StringBuilder query = new StringBuilder();
            bool first = true;
            int lastModifiedField = -1;
            StagingTable curst = null;
            sforce.QueryResult qr = null;
            SalesforceSettings.SalesforceObjectConnection soc = null;
            sforce.DescribeSObjectResult dresult = null;
            if (sp != null)
                setAllowable(sp.QueryLanguageVersion);
            try
            {
                numRecords = 0;
                currentObject = name;
                soc = settings.getObjectConnection(name);
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (st.Name == "ST_" + name)
                    {
                        curst = st;
                        break;
                    }
                }
                bool noObject = false;
                if (soc.query == null || soc.query.Length == 0)
                {
                    try
                    {
                        dresult = service.describeSObject(name);
                    }
                    catch (SoapException se)
                    {
                        if (se.Message.StartsWith("INVALID_TYPE:"))
                        {
                            Global.systemLog.Warn("Space: " + sp.Name + " - invalid Type Object : " + name, se);
                            // Object not valid - most likely because security doesn't allow it
                            noObject = true;
                        }
                        else
                        {
                            Global.systemLog.Error("Space: " + sp.Name + " - SoapException in fetching object description for : " + name, se);
                            markObjectExtractFailure();
                            if (!maxRetriesReached())
                            {
                                return;
                            }
                            
                            DataRow dr = errors.NewRow();
                            dr["Source"] = "Salesforce.com";
                            dr["Severity"] = "Error";
                            dr["Error"] = se.Message;
                            errors.Rows.Add(dr);
                            sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + se.Message);
                            Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space " 
                                + (sp != null ? sp.ID.ToString() : null) +
                                " and user " + (u != null ? u.Username : null) + " ]",
                                "SalesforceLoader DataExtraction",
                                "SoapException in fetching object description for " + name + " : " + se.Message,
                                se, sp, null);                            
                            return;
                        }
                    }
                    catch (WebException we)
                    {
                        Global.systemLog.Error("Space: " + sp.Name + " - WebException in fetching object description for : " + name, we);
                        markObjectExtractFailure();
                        if (!maxRetriesReached())
                        {                            
                            return;
                        }
                        DataRow dr = errors.NewRow();
                        dr["Source"] = "Salesforce.com";
                        dr["Severity"] = "Error";
                        dr["Error"] = we.Message;
                        errors.Rows.Add(dr);
                        sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + we.Message);
                        Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                            + (sp != null ? sp.ID.ToString() : null) +
                            " and user " + (u != null ? u.Username : null) + " ]",
                            "SalesforceLoader DataExtraction",
                            "WebException in fetching object description for " + name + " : " + we.Message,
                            we, sp, null);        
                        return;
                    }
                    if (!noObject)
                    {
                        query.Append("SELECT ");
                        foreach (Field f in dresult.fields)
                        {
                            if (first)
                                first = false;
                            else
                                query.Append(',');
                            query.Append(f.name);
                        }
                        query.Append(" FROM " + name);
                        for (int i = 0; i < dresult.fields.Length; i++)
                        {
                            Field f = dresult.fields[i];
                            if (f.name == "SystemModstamp")
                            {
                                lastModifiedField = i;
                                break;
                            }
                        }
                        if (soc.lastSystemModStamp != DateTime.MinValue && lastModifiedField >= 0 && curst != null && curst.Transactional)
                        {
                            // Incremental load only
                            query.Append(" WHERE SystemModstamp > " + soc.lastSystemModStamp.ToString("yyyy-MM-ddThh:mm:sszzz"));
                        }
                    }
                }
                else
                {
                    // replace any variable in the soc sql query
                    String socQuery = Util.replaceVariables(sp, u, soc.query, true);
                    query.Append(socQuery);
                }
                if (!noObject)
                {
                    service.QueryOptionsValue = new sforce.QueryOptions();
                    service.QueryOptionsValue.batchSize = 2000;
                    service.QueryOptionsValue.batchSizeSpecified = true;
                    soc.lastUpdatedDate = DateTime.Now;
                    try
                    {
                        writeSFDCQuery(name, dresult, query.ToString(), sp);
                        qr = service.query(query.ToString());
                    }
                    catch (SoapException se)
                    {
                        Global.systemLog.Error("Space: " + sp.Name + " - SoapException while querying: " + name, se);
                        markObjectExtractFailure();
                        bool doNotRetry = se.GetBaseException().GetType().Name == "INVALID_TYPE_FOR_OPERATION" || se.Message.StartsWith("MALFORMED_QUERY") || se.Message.StartsWith("INVALID_FIELD");
                        if (doNotRetry)
                            retryCount = maxRetries; // this will never get better
                        if (!maxRetriesReached())
                        {                            
                            return;
                        }
                        DataRow dr = errors.NewRow();
                        dr["Source"] = "Salesforce.com";
                        dr["Severity"] = "Error";
                        dr["Error"] = se.Message;
                        dr["Object"] = name;
                        errors.Rows.Add(dr);
                        // do not send operations email for objects that can not be queried
                        if (!doNotRetry)
                        {
                            sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + se.Message);
                            Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                                + (sp != null ? sp.ID.ToString() : null) +
                                " and user " + (u != null ? u.Email : null) + " ]",
                                "SalesforceLoader DataExtraction ",
                                "SoapException in executing query " + query.ToString() + " : " + se.Message,
                                se, sp, null);
                        }
                        return;
                    }
                    catch (WebException we)
                    {
                        Global.systemLog.Error("Space: " + sp.Name + " - WebException while querying: " + name, we);
                        markObjectExtractFailure();
                        if (!maxRetriesReached())
                        {                            
                            return;
                        }
                        DataRow dr = errors.NewRow();
                        dr["Source"] = "Salesforce.com";
                        dr["Severity"] = "Error";
                        dr["Error"] = we.Message;
                        dr["Object"] = name;
                        errors.Rows.Add(dr);
                        sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + we.Message);
                        Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                            + (sp != null ? sp.ID.ToString() : null) +
                            " and user " + (u != null ? u.Username : null) + " ]",
                            "SalesforceLoader DataExtraction",
                            "WebException in executing query " + query.ToString() + " : " + we.Message,
                            we, sp, null);        
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Space: " + sp.Name + " - Exception while loading SFDC object: " + name, e);
                markObjectExtractFailure();
                if (!maxRetriesReached())
                {                    
                    return;
                }
                // ops email alert has already been sent for SoapException and WebException
                if (!(e is SoapException) && !(e is WebException))
                {
                    sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + e.Message);
                    Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                        + (sp != null ? sp.ID.ToString() : null) +
                        " and user " + (u != null ? u.Username : null) + " ]",
                        "SalesforceLoader DataExtraction",
                        "Exception in loading SFDC object " + name + " : " + e.Message,
                        e, sp, null);
                    return;
                }
            }
            DateTime maxLastModified = DateTime.MinValue;
            StreamWriter writer = null;
            try
            {
                string fname = sp.Directory + "\\data\\" + name + ".txt";
                try
                {
                    writer = new StreamWriter(fname, false, Encoding.UTF8);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Space: " + sp.Name + " - Exception while saving results for: " + name, ex);
                    markObjectExtractFailure();
                    if (!maxRetriesReached())
                    {                        
                        return;
                    }
                    DataRow dr = errors.NewRow();
                    dr["Source"] = "Salesforce.com";
                    dr["Severity"] = "Error";
                    dr["Error"] = "Could not save results - please make sure no other Salesforce.com data retrieval process is running";
                    errors.Rows.Add(dr);
                    return;
                }
                if (qr != null && qr.size > 0)
                {
                    first = true;
                    List<string> cnames = new List<string>();
                    foreach (XmlElement xe in qr.records[0].Any)
                    {
                        if (first)
                            first = false;
                        else
                            writer.Write('|');
                        string f = xe.Name.Substring(3);
                        string n = getAllowable(f, false);
                        int count = 0;
                        // Fix to deal with duplicate column names (which SFDC allows)
                        while (cnames.Contains(count == 0 ? n : (n + count)))
                        {
                            count++;
                            if (count > 100)
                                break;
                        }
                        n = count == 0 ? n : (n + count);
                        cnames.Add(n);
                        writer.Write(n);
                        soc.addColumnName(n);
                    }
                    writer.WriteLine();
                    bool done = false;
                    while (!done && !isTerminated())
                    {
                        for (int i = 0; i < qr.records.Length; i++)
                        {
                            numRecords++;
                            if (sp.TestMode && numRecords > maxRecords)
                                break;
                            first = true;
                            for (int j = 0; j < qr.records[i].Any.Length; j++)
                            {
                                if (first)
                                    first = false;
                                else
                                    writer.Write('|');
                                string s = qr.records[i].Any[j].InnerText;
                                if (s.IndexOfAny(new char[] { '\r', '\n', '"', '|', '\\' }) >= 0)
                                    writer.Write('"' + s.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("|", @"\|") + '"');
                                else
                                    writer.Write(s);
                            }
                            if (lastModifiedField >= 0)
                            {
                                try
                                {
                                    DateTime dt = DateTime.Parse(qr.records[i].Any[lastModifiedField].InnerText);
                                    if (dt > maxLastModified)
                                        maxLastModified = dt;
                                }
                                catch (Exception)
                                {
                                }
                            }
                            writer.WriteLine();
                        }
                        if (sp.TestMode && numRecords > maxRecords)
                            break;
                        if (qr.done)
                            done = true;
                        else
                        {
                            try
                            {
                                qr = service.queryMore(qr.queryLocator);
                            }
                            catch (SoapException se)
                            {
                                Global.systemLog.Error("Space: " + sp.Name + " - SoapException while querying more: " + name, se);
                                if (writer != null)
                                {
                                    try
                                    {
                                        writer.Close();
                                    }
                                    catch (Exception writerCloseException)
                                    {
                                        Global.systemLog.Warn("Space: " + sp.Name + " - Exception in closing writer for " + name, writerCloseException);
                                    }
                                }
                                markObjectExtractFailure();
                                if (!maxRetriesReached())
                                {                                    
                                    return;
                                }
                                DataRow dr = errors.NewRow();
                                dr["Source"] = "Salesforce.com";
                                dr["Severity"] = "Error";
                                dr["Error"] = se.Message;
                                errors.Rows.Add(dr);
                                sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + se.Message);
                                Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                                    + (sp != null ? sp.ID.ToString() : null) +
                                    " and user " + (u != null ? u.Username : null) + " ]",
                                    "SalesforceLoader DataExtraction",
                                    "SoapException while querying more for SFDC object " + name + " : " + se.Message,
                                    se, sp, null);
                                return;
                            }
                            catch (WebException we)
                            {
                                Global.systemLog.Error("Space: " + sp.Name + " - WebException while querying more: " + name, we);
                                if (writer != null)
                                {
                                    writer.Close();
                                }

                                markObjectExtractFailure();
                                if (!maxRetriesReached())
                                {                                    
                                    return;
                                }
                                DataRow dr = errors.NewRow();
                                dr["Source"] = "Salesforce.com";
                                dr["Severity"] = "Error";
                                dr["Error"] = we.Message;
                                errors.Rows.Add(dr);
                                sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + we.Message);
                                Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                                    + (sp != null ? sp.ID.ToString() : null) +
                                    " and user " + (u != null ? u.Username : null) + " ]",
                                    "SalesforceLoader DataExtraction",
                                    "WebException while querying more for SFDC object " + name + " : " + we.Message,
                                    we, sp, null);
                                return;
                            }
                        }
                    }
                    writer.Close();
                    if (isTerminated())
                        return;
                    soc.lastSystemModStamp = maxLastModified;
                    SalesforceLoader.mapAndUploadFile(settings.automatic, maf, sp, u, name, dresult, errors);                    
                }
                else
                {
                    Global.systemLog.Info("Space: " + sp.Name + " - no data returned for SFDC object " + name + " (creating empty stub file)");
                }
                    
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Space: " + sp.Name + " - Exception in SFDC getObject " + name, ex);
                markObjectExtractFailure();
                if (!maxRetriesReached())
                {
                    return;
                }               
                
                DataRow dr = errors.NewRow();
                dr["Source"] = "Salesforce Extraction";
                dr["Severity"] = "Error";
                dr["Error"] = ex.Message;
                errors.Rows.Add(dr);
                sendFailureEmail(u.Email, "Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + sp.Name + ": " + ex.Message);
                Util.sendErrorDetailEmail("[Problem during Salesforce extraction for space "
                    + (sp != null ? sp.ID.ToString() : null) +
                    " and user " + (u != null ? u.Username : null) + " ]",
                    "SalesforceLoader DataExtraction",
                    "WebException while querying more for SFDC object " + name + " : " + ex.Message,
                    ex, sp, null);
            }
            finally
            {

                if (writer != null)
                    try
                    {

                        writer.Close();
                    }
                    catch (Exception writerCloseException)
                    {
                        Global.systemLog.Warn("Space: " + sp.Name + " - Exception in closing writer for " + name, writerCloseException);
                    }
            }
        }

        public class SFDCField
        {
            string name;
            string label;
            bool naturalKey;

            public SFDCField(string name, string label, bool naturalKey)
            {
                this.name = name;
                this.label = label;
                this.naturalKey = naturalKey;
            }

            public string getName()
            {
                return name;
            }

            public string getLabel()
            {
                return label;
            }

            public bool isNatualKey()
            {
                return naturalKey;
            }
        }

        public const string SFDC_CMD_FILE = "sfdc.cmd";
        public static string getSFDCEngineCommandArgs(Space sp)
        {
            return sp.Directory + "\\" + SFDC_CMD_FILE + " "  + sp.Directory + " " + "0";
        }

        public static void writeSFDCCommandFile(Guid uid, string connectorType, Space sp, String clientId, String sandBoxUrl, int numParallelExtracts, bool writeEmailInfo)
        {
            writeSFDCCommandFile(uid, connectorType, sp, clientId, sandBoxUrl, numParallelExtracts, null, -1, null);
        }

        public static void writeSFDCCommandFile(Guid uid, string connectorType, Space sp, String clientId, String sandBoxUrl, int numParallelExtracts, string mailHost, int port, string toEmail)
        {
            StringBuilder commands = new StringBuilder();
            PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp);
            if (peVersion != null && peVersion.IsProcessingGroupAware)
            {
                string checkFileName = sp.Directory + "\\checkIn-sfdc.txt";
                commands.Append("checkIn " + checkFileName + "\n");
            }
            commands.Append("repository " + sp.Directory + "\\repository_dev.xml\n");
            if (clientId == null || clientId.Trim().Length == 0)
            {
                clientId = Util.getAppSettingProperty("SFDCClientID");
            }

            string birstParams = "null";

            if (connectorType == ConnectorUtils.CONNECTOR_SFDC)
            {
                // sandBoxUrl is based on the site type in sforce.xml
                if (sandBoxUrl == null)
                {
                    // check if from site type stored in sforce.xml
                    SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
                    if (settings.sitetype == SalesforceSettings.SiteType.Sandbox)
                        sandBoxUrl = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
                }
                birstParams = "SFDCClientID=" + clientId;
            }

            if (mailHost != null && toEmail != null && port > 0)
            {
                string from = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"];
                if(from != null)
                {
                    commands.Append("emailer " + mailHost + ":" + port + " " + from + " " + toEmail + "\n"); 
               }
            }
            
            //("extractconnectordata <ConnectorName> <BIRST_Parameters> <sandBoxURL> <noExtractionThreads>;
            commands.Append("extractconnectordata " + connectorType + " " + birstParams + " " + (sandBoxUrl != null && sandBoxUrl.Length > 0 ? sandBoxUrl : "null") + " " + numParallelExtracts + " " + (uid != Guid.Empty ? uid.ToString() : "null"));
            File.WriteAllText(sp.Directory + "\\" + SFDC_CMD_FILE, commands.ToString());
        }


        public static string getMappingFileName(Space sp, string objectName)
        {
            string mappingFileName = null;
            try
            {
                string mappingDirectory = sp.Directory + "\\mapping";
                if (!Directory.Exists(mappingDirectory))
                {
                    Directory.CreateDirectory(mappingDirectory);
                }

                mappingFileName = mappingDirectory + "\\" + objectName + "-mapping.txt";
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while retreiving mapping file name " + objectName, ex);
            }


            return mappingFileName;
        }

        public static void postSFDCExtractUploader(SalesforceSettings settings, MainAdminForm maf, Space sp, User u, String name, sforce.DescribeSObjectResult dresult, DataTable errors)
        {
            // if it's empty stub file, do not continue
            FileInfo fileInfo = new FileInfo(sp.Directory + "\\data\\" + name + ".txt");
            if (fileInfo.Length == 0)
            {
                Global.systemLog.Info("Skipping the upoader for " + name + ".txt : File size is 0");
                return;
            }
            mapAndUploadFile(settings.automatic, maf, sp, u, name, dresult, errors);
        }

        
        public static void mapAndUploadFile(bool automatic, MainAdminForm maf, Space sp, User u, String name, sforce.DescribeSObjectResult dresult, DataTable errors)
        {   
            List<SFDCField> sfdcFields = new List<SFDCField>();
            if (dresult != null && dresult.fields.Length > 0)
            {
                foreach (Field f in dresult.fields)
                {
                    SFDCField sfdcField = new SFDCField(f.name, f.label, f.type == fieldType.id);
                    sfdcFields.Add(sfdcField);
                }
            }
            else
            {
                // get the mapping file 
                string mappingFileName = SalesforceLoader.getMappingFileName(sp, name); // sp.Directory + "\\mapping\\" + name + "-mapping.txt";
                // check if it exists
                if (File.Exists(mappingFileName))
                {
                    string nameLabel = File.ReadAllText(mappingFileName, Encoding.UTF8);
                    if (nameLabel != null && nameLabel.Trim().Length > 0)
                    {
                        sfdcFields = getMappedFields(nameLabel);
                        /*
                        string[] nameLabelArray = nameLabel.Split(',');
                        if (nameLabelArray != null && nameLabelArray.Length > 0)
                        {
                            foreach (string str in nameLabelArray)
                            {
                            	if (str == null || str.Length == 0)
                            		continue;

                                string[] fieldDescription = str.Split('=');
                                if (fieldDescription == null || fieldDescription.Length < 2)
                                    continue;

                                bool fieldnatualKey = false;
                                if (fieldDescription.Length == 3)
                                {
                                    fieldnatualKey = bool.Parse(fieldDescription[2]);
                                }
                                string fieldName = fieldDescription[0];
                                string fieldLabel = fieldDescription[1];
                                SFDCField sfdcField = new SFDCField(fieldName, fieldLabel, fieldnatualKey);
                                sfdcFields.Add(sfdcField);
                            }
                        }
                         * */
                    }
                    else
                    {
                        Global.systemLog.Warn("Mapping file exists but seems to be empty : " + mappingFileName);
                    }
                }
                else
                {
                    Global.systemLog.Warn("Mapping file does not exist : " + mappingFileName);
                }
            }
            mapAndUploadFile(automatic, maf, sp, u, name, errors, sfdcFields);
        }

        public static List<SFDCField> getMappedFields(string nameLabel)
        {
            List<SFDCField> sfdcFields = new List<SFDCField>();
            if (nameLabel != null && nameLabel.Trim().Length > 0)
            {
                string[] nameLabelArray = nameLabel.Split(',');
                if (nameLabelArray != null && nameLabelArray.Length > 0)
                {
                    foreach (string str in nameLabelArray)
                    {
                        if (str == null || str.Length == 0)
                            continue;

                        string[] fieldDescription = str.Split('=');
                        if (fieldDescription == null || fieldDescription.Length < 2)
                            continue;

                        bool fieldnatualKey = false;
                        if (fieldDescription.Length == 3)
                        {
                            fieldnatualKey = bool.Parse(fieldDescription[2]);
                        }
                        string fieldName = fieldDescription[0];
                        string fieldLabel = fieldDescription[1];
                        SFDCField sfdcField = new SFDCField(fieldName, fieldLabel, fieldnatualKey);
                        sfdcFields.Add(sfdcField);
                    }
                }
            }
            return sfdcFields;
        }

        public static void mapAndUploadFile(bool automatic, MainAdminForm maf, Space sp, User u, String name, DataTable errors, List<SFDCField> sfdcFields)
        {
            StagingTable curst = findStagingTable(maf, name);
            maf.sourceFileMod.getSourceFiles();
            bool exists = curst != null;
            ApplicationUploader ul = new ApplicationUploader(maf, sp, name + ".txt", true, false, false, null, false, null, u, null, 0, 0,
                        false, null, null, false);
            if (automatic)
            {
                ul.SourceGroups = SOURCE_GROUP_NAME;
                ul.LoadGroups = LOAD_GROUP_NAME;
            }
            else
            {
                ul.LoadGroups = Util.LOAD_GROUP_NAME;
            }
            ul.checkDupStagingCols = true;
            DataTable et = null;
            bool done = false;
            int numRetries = 0;
            int maxRetries = 5;
            do
            {
                try
                {
                    et = ul.uploadFile(false, true);
                    done = true;
                }
                catch (IOException ex)
                {
                    if (++numRetries < maxRetries)
                    {
                        Global.systemLog.Debug("Problem encountered uploading file - " + name + ", retrying");
                    }
                    else
                    {
                        Global.systemLog.Error("Problem encountered uploading file - " + name + ", maximum number of retries exhausted");
                        throw ex;
                    }
                }
            } while (!done);
            foreach (DataRow dr in et.Rows)
            {
                string errorType = null;
                if (dr["Severity"] != null)
                {
                    errorType = (string)dr["Severity"];
                }
                if (errorType != null && errorType == ApplicationUploader.ERROR_STR)
                {
                    errors.Rows.Add(new object[] { dr["Source"], dr["Severity"], dr["Error"] });
                    Global.systemLog.Error("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                }
                else
                {
                    Global.systemLog.Warn("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                }
            }
            curst = findStagingTable(maf, name);
            if (curst != null)
            {
                // Add logical names and setup metadta
                if (automatic)
                {
                    curst.SourceGroups = SOURCE_GROUP_NAME;
                    curst.LoadGroups = new string[] { LOAD_GROUP_NAME };
                }
                Dictionary<string, string> map = new Dictionary<string, string>();
                List<StagingColumn> keylist = new List<StagingColumn>();
                bool updated = false;
                string unsupportedChars = sp.QueryLanguageVersion == 0 ? Performance_Optimizer_Administration.Util.OLD_QUERY_LANGUAGE_UNSUPPORTED_CHARS : Performance_Optimizer_Administration.Util.NEW_QUERY_LANGUAGE_UNSUPPORTED_CHARS;
                bool useNewCharReplaceExpression = maf.builderVersion >= 27 ? true : false;
                foreach (StagingColumn sc in curst.Columns)
                {
                    if (sfdcFields != null)
                    {
                        foreach (SFDCField sfdcField in sfdcFields)
                        {
                            string fieldName = null;
                            if (useNewCharReplaceExpression)
                            {
                                fieldName = Regex.Replace(sfdcField.getName(), unsupportedChars, "_");                                
                            }
                            else
                            {
                                fieldName = getAllowableValue(sfdcField.getName(), false, sp);
                            }
                            if (sc.Name == fieldName)
                            {
                                // make sure no dupes
                                bool found = false;
                                string l = null;
                                if (useNewCharReplaceExpression)
                                {
                                    l = Regex.Replace(sfdcField.getLabel(), unsupportedChars, "_");
                                }
                                else
                                {
                                    l = getAllowableValue(sfdcField.getLabel(), true, sp);
                                }
                                foreach (StagingColumn ssc in curst.Columns)
                                {
                                    if (ssc.Name == l)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    // Update the logical name of the column, but also update the hierarchy                                    
                                    curst.renameColumn(sc.Name, l);
                                    map.Add(sfdcField.getName(), l);
                                    updated = true;
                                }
                                sc.NaturalKey = sfdcField.isNatualKey();
                                if (sc.NaturalKey)
                                    keylist.Add(sc);
                                break;
                            }
                        }
                    }
                }
                string hname = ApplicationUploader.getHName(maf, curst);
                Hierarchy h = maf.hmodule.getHierarchy(hname);
                if (h != null)
                {
                    List<Level> clevels = new List<Level>();
                    Level l = h.getLowestGeneratedLevel();
                    if (l != null)
                    {
                        if (l.ColumnNames != null)
                        {
                            for (int i = 0; i < l.ColumnNames.Length; i++)
                            {
                                foreach (string s in map.Keys)
                                    if (l.ColumnNames[i] == s)
                                        l.ColumnNames[i] = map[s];
                            }
                        }
                        else
                        {
                            Global.systemLog.Info("No column names for level " + l.Name + " of hierarchy " + h.Name + " (staging table " + curst.Name + ")");
                        }
                        if (l.Keys != null)
                        {
                            foreach (LevelKey lk in l.Keys)
                            {
                                if (lk.SurrogateKey)
                                    continue;
                                bool hasKey = keylist.Count > 0;
                                // Set the dimension key with the one supplied by SFDC
                                if (hasKey)
                                {
                                    lk.ColumnNames = new string[keylist.Count];
                                    for (int i = 0; i < lk.ColumnNames.Length; i++)
                                        lk.ColumnNames[i] = keylist[i].Name;
                                }
                                else
                                {
                                    // If not, just replace the columns
                                    for (int i = 0; i < lk.ColumnNames.Length; i++)
                                    {
                                        foreach (string s in map.Keys)
                                            if (lk.ColumnNames[i] == s)
                                                lk.ColumnNames[i] = map[s];
                                    }
                                }
                            }
                        }
                        else
                        {
                            Global.systemLog.Info("No keys for level " + l.Name + " of hierarchy " + h.Name + " (staging table " + curst.Name + ")");
                        }
                    }
                    else
                    {
                        Global.systemLog.Info("Could not find the lowest level for " + h.Name + " (staging table " + curst.Name + ")");
                    }
                }
                if (updated)
                    Util.updateStagingTable(maf, curst, maf.sourceFileMod.getSourceFile(curst.SourceFile), u.Username, !exists);
            }
        }

        public string CurrentObject
        {
            get
            {
                return currentObject;
            }
        }

        public int NumRecords
        {
            get
            {
                return numRecords;
            }
        }

        public SalesforceSettings Settings
        {
            get
            {
                return settings;
            }
        }

        private void resetSFDCQueryLog(Space sp)
        {
            if (sp == null)
            {
                return;
            }

            string spaceLogsDirectory = getSpaceLogDirectory(sp);
            if (!Directory.Exists(spaceLogsDirectory))
            {
                Directory.CreateDirectory(spaceLogsDirectory);
            }

            string spaceQueryLogFileName = getSFDCQueryLogFilePath(sp);
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(spaceQueryLogFileName, false, Encoding.UTF8);                
                writer.Write(sp.Name + " : " + sp.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in clearing file " + spaceQueryLogFileName, ex);
            }
            finally
            {
                if (writer != null)
                {
                    try
                    {
                        writer.Close();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Error("Error in releasing resource for " + spaceQueryLogFileName, ex2);
                    }
                }
            }
        }

        public static String getSFDCQueryLogFilePath(Space sp)
        {
            return getSpaceLogDirectory(sp) +  "\\" + "sfdcQueryLog.txt";
        }

        public static String getSpaceLogDirectory(Space sp)
        {
            return sp.Directory + "\\" + "logs" ;
        }

        private void writeSFDCQuery(String objectName, DescribeSObjectResult describeObjectResult, String query, Space sp)
        {
            StreamWriter writer = null;
            String queryLogFileName = null;
            try
            {
                string spaceLogsDirectory = getSpaceLogDirectory(sp);
                if (!Directory.Exists(spaceLogsDirectory))
                {
                    Directory.CreateDirectory(spaceLogsDirectory);
                }
                queryLogFileName = getSFDCQueryLogFilePath(sp);                
                writer = new StreamWriter(queryLogFileName, true, Encoding.UTF8);

                writer.WriteLine("");
                writer.WriteLine("Object Name : " +  objectName);
                writer.WriteLine("Time : " + DateTime.Now);
                writer.WriteLine("Query :");                
                writer.WriteLine(query);                
                
                if(describeObjectResult != null)
                {
                    writer.WriteLine("Field Descriptions : (Name, Label, Type, Length, Precision, Scale)");
                    foreach (Field field in describeObjectResult.fields)
                    {                        
                        writer.Write(field.name);
                        writer.Write(",");
                        writer.Write(field.label);
                        writer.Write(",");
                        writer.Write(field.type.ToString());                        
                        writer.Write(",");
                        writer.Write(field.length);
                        writer.Write(",");
                        writer.Write(field.precision);
                        writer.Write(",");
                        writer.Write(field.scale);
                        writer.WriteLine();
                    }
                }
                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in writing query information to the " + queryLogFileName, ex);
            }
            finally
            {
                if (writer != null)
                {
                    try
                    {
                        writer.Close();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to close writer for " + queryLogFileName, ex2);
                    }
                }
            }
        }

        public static DateTime getNextDate(SalesforceSettings.ScheduleInfo scheduleInfo, DateTime nextDate)
        {
            SalesforceSettings.UpdateSchedule schedule = scheduleInfo.type;            
            
            while (nextDate <= DateTime.Now)
            {
                if (schedule == SalesforceSettings.UpdateSchedule.Daily)
                {
                    // Make next scheduled event at midnight of next day
                    nextDate = nextDate.AddDays(1);
                    nextDate = nextDate.Subtract(TimeSpan.FromMinutes(nextDate.Hour * 60 + nextDate.Minute));                    
                }
                else if (schedule == SalesforceSettings.UpdateSchedule.Weekly)
                {
                    // configure the weekly times starting from the date given
                    /*
                    nextDate = nextDate.AddDays(7);
                    nextDate = nextDate.Subtract(TimeSpan.FromMinutes(nextDate.Hour * 60 + nextDate.Minute));
                    */
                    // keep on adding day till it reached the required day
                    int hour = scheduleInfo.Hour;

                    // Edge case for 12
                    if (scheduleInfo.AmPm == SalesforceSettings.AMTime && hour == 12)
                    {
                        hour = 0;
                    }
                    else if (hour < 12 && scheduleInfo.AmPm == SalesforceSettings.PMTime)
                    {
                        hour = hour + 12;
                    }

                    // check the current scheduled time                    
                    for (int i = 0; i < 7; i++)
                    {
                        nextDate = nextDate.AddDays(1);
                        if (nextDate.DayOfWeek.ToString() == scheduleInfo.Day)
                        {
                            break;
                        }
                    }                    
                    DateTime newDateTime = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, hour, scheduleInfo.Minute, 0);
                    nextDate = newDateTime;                    
                }
                else if (schedule == SalesforceSettings.UpdateSchedule.Monthly)
                {
                    // Make next scheduled event at midnight of next month
                    nextDate = nextDate.AddMonths(1);
                    nextDate = nextDate.Subtract(TimeSpan.FromMinutes(nextDate.Hour * 60 + nextDate.Minute));                    
                }
                else
                    break;
            }
            return nextDate;
        }

        public TaskResults ExecuteTask(MainAdminForm maf, User u, QueryConnection conn, string schema, Space sp, DateTime curTime, Guid ID, string parameters)
        {
            return processSFDC(maf, u, conn, schema, sp, curTime, ID, parameters);
        }

        public static TaskResults processSFDC(MainAdminForm maf, User u, QueryConnection conn, string schema, Space sp, DateTime curTime, Guid ID, string parameters)
        {
            Global.systemLog.Info("Processing space " + sp.Name + " for SFDC");
            List<string> objects = new List<string>();
            SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");                       

            SalesforceSettings.ScheduleInfo si = settings.getScheduleInfo(ID);
            if(si == null)
            {
                Global.systemLog.Error("Unable to find SFDC schedule with id " + ID);
                Util.sendErrorDetailEmail("[Problem during Salesforce schedule start. ID not found " 
                                + (sp != null ? sp.ID.ToString() : null) + 
                                " and user " +  (u!= null ? u.Email : null) + " ]",
                                "SalesforceLoader Schedule Start",
                                "Schedule ID is not found in settings: ID = " + ID,
                                null, sp, null);                            
                return null;
            }

            // if the id is empty, create a new non-empty guid and update the same
            /*
            if (ID == Guid.Empty)
            {
                Guid newID = Guid.NewGuid();
                si.id = newID.ToString();
                Database.updateScheduleTaskObjectID(conn, schema, SalesforceLoader.SOURCE_GROUP_NAME, ID, newID);
                Global.systemLog.Info("Updated the object id for SFORCE from  " + ID + " to " + newID);
                settings.saveSettings(sp.Directory, true);
                si = settings.getScheduleInfo(newID);
                ID = newID;
            }*/

            // We do pick up the task in such a way that we only have one SFORCE extraction going at one time            
            if (Util.isSFDCExtractStatusFilePresent(sp))
            {
                Global.systemLog.Warn("Space SFDC extraction in progress. Skipping this space. Setting the retry time to " + DateTime.Now.AddHours(1));
                if (si.type != SalesforceSettings.UpdateSchedule.Manual)
                {
                    Database.updateTaskSchedule(conn, schema, sp.ID, ID, SalesforceLoader.SOURCE_GROUP_NAME, DateTime.Now.AddHours(1), null, SalesforceLoader.SALESFORCE_LOADING_SKIPPED);
                }
                return null;
            }


            for (int i = 0; i < settings.Objects.Length; i++)
                objects.Add(settings.Objects[i].name);

            SforceService binding = createBinding(sp, u, settings);
            if (binding == null)
            {
                updateTaskScheduleOnFailure(conn, schema, sp, ID, si);
                return null;
            }

            SalesforceLoader sl = new SalesforceLoader(objects, maf, sp, u, binding, settings, false, null, true);
            DataTable et = new DataTable();
            et.Columns.Add("Source");
            et.Columns.Add("Severity");
            et.Columns.Add("Error");
            et.Columns.Add("Object");
            sl.Errors = et;
            sl.getData();
            si.nextScheduleTime = getNextDate(si, si.nextScheduleTime);            
            settings.saveSettings(sp, true);                       
            
            TaskResults results = new TaskResults();
            if (settings.Automatic)
            {
                results.LoadGroupName = SalesforceLoader.LOAD_GROUP_NAME;
                Dictionary<string, int> curLoadNumbers = Database.getCurrentLoadNumbers(conn, schema, sp.ID);
                results.LoadNumber = curLoadNumbers.ContainsKey(results.LoadGroupName) ? curLoadNumbers[results.LoadGroupName] : SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
            }
            else
            {
                results.LoadGroupName = Util.LOAD_GROUP_NAME;
                results.LoadNumber = sp.LoadNumber;
            }

            string completedStatus = SalesforceLoader.SALESFORCE_LOADING_SUCCESSFUL;
            // if you get any errors, flag the processing flag
            if (et != null && et.Rows != null && et.Rows.Count > 0)
            {
                bool ignoreErrors = ignoreUploaderErrors(sp, et);

                if (ignoreErrors && !sl.isPostExtractOperationHalted())
                {   
                    results.Process = true;
                }
                else
                {
                    Global.systemLog.Error("Errors Encountered during Extraction. Setting the process flag to false for space : name= : " +
                        sp.Name + " : id=" + sp.ID);
                    results.Process = false;
                    completedStatus = SalesforceLoader.SALESFORCE_LOADING_FAILED;
                }
            }
            else
            {
                // Make sure that the current load is not cancelled, if it is check for killed flag and mark the process as false
                if (sl != null && sl.isTerminated())
                {
                    Global.systemLog.Warn("It seems that the salesforce extraction was cancelled. Marking the flag to cancel processing after extraction");
                    results.Process = false;
                }
                else if (sl.isPostExtractOperationHalted())
                {
                    Global.systemLog.Warn("It seems that the atleast one object completly failed on extraction. Marking the flag to cancel processing after extraction");
                    results.Process = false;
                }
                else
                {
                    results.Process = true;
                }
            }

            if (si.type != SalesforceSettings.UpdateSchedule.Manual)
                Database.updateTaskSchedule(conn, schema, sp.ID, ID, SalesforceLoader.SOURCE_GROUP_NAME, si.nextScheduleTime, null, completedStatus);
            return results;
        }

        public static bool ignoreUploaderErrors(Space sp, DataTable et)
        {
            bool ignoreErrors = true;
            if (et != null && et.Rows != null && et.Rows.Count > 0)
            {
                logErrors(et, sp);
                foreach (DataRow dr in et.Rows)
                {
                    string errorType = null;
                    if (dr["Severity"] != null)
                    {
                        errorType = (string)dr["Severity"];
                    }
                    if (errorType != null && errorType == ApplicationUploader.ERROR_STR)
                    {
                        ignoreErrors = false;
                        break;
                    }
                }
            }
            return ignoreErrors;
        }

        public static void logErrors(DataTable dt, Space sp)
        {
            if (dt == null || sp == null)
            {
                return;
            }
            // print out all the errors first
            foreach (DataRow dr in dt.Rows)
            {
                string errorType = null;
                if (dr["Severity"] != null)
                {
                    errorType = (string)dr["Severity"];
                }

                if (errorType == null)
                {
                    continue;
                }

                if (errorType == ApplicationUploader.ERROR_STR)
                {
                    Global.systemLog.Error("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                }
                else
                {
                    Global.systemLog.Warn("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                }
            }
        }

        /// <summary>
        /// This method is used to update the load_schedule table for SFORCE to update the next schedule on failure.
        /// Basically, when failure happens, we need to make sure that we "null"ify WorkTime and WorkingServer so that it could
        /// be picked again in the next schedule time
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="sp">Space</param>
        /// <param name="ID">Object_ID in the load_schedule table</param>
        /// <param name="si">ScheduleInfo for ID</param>
        private static void updateTaskScheduleOnFailure(QueryConnection conn, string schema, Space sp, Guid ID, SalesforceSettings.ScheduleInfo si)
        {
            si.nextScheduleTime = getNextDate(si, si.nextScheduleTime);
            if (si.type != SalesforceSettings.UpdateSchedule.Manual)
                Database.updateTaskSchedule(conn, schema, sp.ID, ID, SalesforceLoader.SOURCE_GROUP_NAME, si.nextScheduleTime, null, SalesforceLoader.SALESFORCE_LOADING_FAILED);
        }

        public static void sendFailureEmail(string email, string subject, string msg)
        {
            MailMessage mm = new MailMessage();
            mm.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
            mm.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
            mm.To.Add(new MailAddress(email));
            mm.Subject = subject;
            mm.Body = msg;
            SmtpClient smtpc = new SmtpClient();
            bool useSSL = bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseSSLforEmail"]);
            if (useSSL)
                smtpc.EnableSsl = true;
            smtpc.Send(mm);
        }

        // return value is in seconds
        public static int getDelayForRetry(int retryNumber)
        {
            int delay = retryExpBase * 10; // 30 seconds, wait for SFDC oddities to settle
            /*
            int retryPower = retryNumber;
            if (retryNumber <= 0)
            {
                retryPower = retryPowerMin;
            }
            else if (retryNumber > 4)
            {
                retryPower = retryPowerMax;
            }

            int delay = (int)Math.Pow(retryExpBase, retryPower);
            delay = delay * 10; // Multiple by 10 seconds
            // so the delay will be 30s, 90s, 270s, 810s
            */
            return delay;
        }

        public DataTable Errors
        {
            get
            {
                return errors;
            }
            set
            {
                errors = value;
            }
        }


        /**
         * does the user exist in the SFDC support portal (for determining where to redirect the Support link)
         */
        public static bool isSupportPortalUser(User u)
        {
            try
            {
                string username = u.Username;
                // login to SFDC
                SforceService service = SalesforceLoader.createBinding(u, supportValidationUsername,
                                                                        Performance_Optimizer_Administration.MainAdminForm.decrypt(supportValidationPassword, Performance_Optimizer_Administration.MainAdminForm.SecretPassword),
                                                                        SalesforceSettings.SiteType.Production, false);
                if (service == null) // authentication failed
                    return false;
                // run query on USER, assumes that Username is the same as birst user or email as appropriate
                string query = supportSOQLQuery.Replace("${USERNAME}", u.Username).Replace("${EMAIL}", u.Email);
                Global.systemLog.Debug("SSO SFDC query: " + query);
                sforce.QueryResult qr = service.query(query);
                return (qr != null && qr.size > 0); // if a result was returned and is greater than zero (== 1 is probably the right thing), then it's a valid portal user (need more)
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex, ex);
                return false;
            }
        }

        public static SforceService createBinding(Space sp, User u, SalesforceSettings settings)
        {
            return createBinding(u, settings.Username, settings.Password, settings.sitetype, true);
        }

        public static SforceService createBinding(User u, string username, string password, SalesforceSettings.SiteType sitetype, bool gzip)
        {
            // Create service object
            SforceService binding = new SforceService();
            if (sitetype == SalesforceSettings.SiteType.Sandbox)
                binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Error("Bad SFDC binding URL prior to authentication: " + binding.Url);
                return null;
            }
            binding.Timeout = 30 * 60 * 1000;
            String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
            if (clientID != null && clientID.Length > 0)
            {
                CallOptions co = new CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            // Invoke the login call and save results in LoginResult
            LoginResult lr = null;
            try
            {
                lr = binding.login(username, password);
            }
            catch (SoapException ex)
            {
                Global.systemLog.Error(ex);
                // sendFailureEmail(u.Email, "Birst SFDC login failure", "SFDC login failure for user " + username + "\n\nMessage From SFDC:\n\n" + ex.Message);
                return null;
            }
            if (lr.passwordExpired)
            {
                Global.systemLog.Warn("SFDC password expired for user " + username);
                // sendFailureEmail(u.Email, "Birst SFDC login failure", "SFDC login failure for user " + username);
                return null;
            }

            if (gzip)
            {
                /*
                 * Can't login using zip until after authentication - errors not caught correctly if zip is on
                 */
                binding = new SforceServiceGzip(true, true);
                if (sitetype == SalesforceSettings.SiteType.Sandbox)
                    binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
                if (!Util.isValidSFDCUrl(binding.Url))
                {
                    Global.systemLog.Error("Bad SFDC binding URL prior to gzip authentication: " + binding.Url);
                    return null;
                }
                binding.Timeout = 30 * 60 * 1000;
                if (clientID != null && clientID.Length > 0)
                {
                    CallOptions co = new CallOptions();
                    co.client = clientID;
                    binding.CallOptionsValue = co;
                }
                try
                {
                    lr = binding.login(username, password);
                }
                catch (SoapException ex)
                {
                    Global.systemLog.Error(ex);
                    return null;
                }
            }
            // Reset the SOAP endpoint to the returned server URL
            binding.Url = lr.serverUrl;
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Error("Bad SFDC binding URL after authentication: " + binding.Url);
                return null;
            }
            // Create a new session header object
            // Add the session ID returned from the login
            binding.SessionHeaderValue = new SessionHeader();
            binding.SessionHeaderValue.sessionId = lr.sessionId;
            return binding;
        }

        private static StagingTable findStagingTable(MainAdminForm maf, string name)
        {
            StagingTable st = maf.stagingTableMod.findTable("ST_" + name);
            if (st == null && maf.builderVersion >= 27)
            {
                foreach(StagingTable sTable in maf.stagingTableMod.getAllStagingTables())
                {
                    if (sTable.getDisplayName() != null && sTable.getDisplayName().Equals(name))
                    {
                       st = sTable;
                       break;
                    }
                }
            }
            return st;
        }


        #region BackgroundModule Members

        public void run()
        {
            killed = false;
            getData();
            if (processAfterExtraction && !isTerminated())
            {
                QueryConnection conn = null;
                Dictionary<string, int> curLoadNumbers = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
                int loadNumber = curLoadNumbers.ContainsKey(SalesforceLoader.LOAD_GROUP_NAME) ? curLoadNumbers[SalesforceLoader.LOAD_GROUP_NAME] : SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                if (session != null)
                {
                    Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Started); //new Status.StatusResult(Status.StatusCode.GenerateSchema);
                    session["status"] = sr;
                }
                ProcessData.Process(maf, sp, u, null, null, DateTime.Now, SalesforceLoader.LOAD_GROUP_NAME, loadNumber , false, null, false, false);
            }
        }

        public void kill()
        {
            killed = true;
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.killed(kvp.Value);
            }
        }

        public bool isTerminated()
        {           
            // if killed value is not true, check for the existince of kill lock file
            if (!killed && sp != null)
            {
                killed = Util.isSFDCKillLockFilePresent(sp);
            }
            return killed;
        }

        public void finish()
        {
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.finished(kvp.Value);
            }
        }

        #endregion

        #region BackgroundModule Members


        public Status.StatusResult getStatus()
        {
			string errorList = "";
			string warningList = "";
			string informationList = "";
            foreach (DataRow dr in errors.Rows)
            {
                string errorType = null;
                if (dr["Severity"] != null)
                {
                    errorType = (string)dr["Severity"];
                }

                if (errorType == null)
                {
                    continue;
                }


				String line = "Source: " + dr["Source"].ToString();
                line = line + "Message: " + dr["Error"].ToString() + "\n";

				if (errorType == ApplicationUploader.ERROR_STR)
					errorList = errorList + line;
				else if (errorType == ApplicationUploader.WARNING_STR)
					warningList = warningList + line;
				else
					informationList = informationList + line;
            }
			Status.StatusResult sr = new Status.StatusResult(errorList.Length > 0 ? Status.StatusCode.Failed : Status.StatusCode.Complete);
			string message = "";
			if (errorList.Length > 0) 
				message = message + "Errors: " + errorList;
			if (warningList.Length > 0)
				message = message + "Warnings: " + warningList;
			if (informationList.Length > 0)
				message = message + "Information: " + informationList;
			sr.message = message;
			return sr;
        }

        #endregion
    }
}
