﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class UserPreference
    {
        public UserPreference()
        {
        }

        public UserPreference(Type type, string key, string value)
        {
            this.type = type;
            this.key = key;
            this.value = value;
        }

        /**
         * These must be in the reverse precedence order: 
         *   UserSpace overrides User overrides Global
         */
        public enum Type { 
            Global, 
            User, 
            UserSpace 
        };

        private Type type;

        public Type PreferenceType
        {
            get { return type; }
            set { type = value; }
        }

        private string key;

        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        private string value;

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string toString()
        {
            return "'userPreference' - 'type': " + type + "; 'key': " + key + "; 'value': " + value;
        }
    }
}
