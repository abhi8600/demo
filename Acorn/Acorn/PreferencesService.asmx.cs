﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

namespace Acorn
{
    /// <summary>
    /// Retrieves the requested preferences.
    /// </summary>
    /// <returns>Returns a map of key/value pairs.</returns>
    [WebService(Namespace = "http://www.birst.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class PreferencesService : System.Web.Services.WebService
    {
        /// <summary>
        /// Updates the user preferences. There can be one or more user preference passed
        /// to the service method.  Depending on the type of preference (UserPreference.Type)
        /// the 'userID' and 'spaceID' may or may not be required.
        /// If the preference already exists, its value is updated.  If the preference
        /// does not exist, then it is added with its value.
        /// </summary>
        /// <param name="userPreferences">Is a list of one or more UserPreference
        /// instances that are to added or updated.</param>
        [WebMethod(EnableSession = true)]
        public void updateUserPreferences(List<UserPreference> userPreferences)
        {
            /*
            Global.systemLog.Debug("'updateUserPreferences()' webservice with:");
            foreach (UserPreference preference in userPreferences)
            {
                Global.systemLog.Debug("...'type': " + preference.PreferenceType + "; 'key': "
                    + preference.Key + "; 'value': " + preference.Value);
            }
            */
            // Must always have a valid session user
            User user = (User)Session["user"];
            if (user == null)
            {
                return;
            }
            // Check to see if we have a valid session space
            Space space = (Space)Session["space"];
            if (space == null)
            {
                // Create a space with an empty guid for space id
                space = new Space();
                space.ID = Guid.Empty;
            }
            // Do the update
            ManagePreferences.updateUserPreferences(null, user.ID, space.ID, userPreferences);
        }

        /// <summary>
        /// Retrieves the set of key/value pairs for the set of keys specified.
        /// If there is a single key entry in 'preferenceKeys' with a value of "ALL"
        /// all preferences will be retrieved.
        /// Whether "ALL" or a subset of preferences, the UserPreference.Type.UserSpace
        /// takes precedence over any UserPreference.Type.User values which in turn 
        /// takes precedence over any UserPreference.Type.Global values.
        /// </summary>
        /// <param name="preferenceKeys">Cannot be null or empty.  Can contain one or 
        /// more entries.  Each entry specifies a key of a key/value pair for a
        /// preference.  If there is one entry and its value is "ALL" then all preference 
        /// key/value pairs are returned.</param>
        /// <returns>Returns one or more key/value pairs, each representing a preference.
        /// </returns>
        [WebMethod(EnableSession = true)]
        public List<UserPreference> getUserPreferences(List<string> preferenceKeys)
        {
            User user = (User)Session["user"];
            Space space = (Space)Session["space"];
            IDictionary<string, string> keyValuePairs = null;
            // Level of search depends on availability of 'user' and 'space'
            if (user == null)
            {
                // Get preferences from global level
                keyValuePairs = ManagePreferences.getGlobalPreferences(null, preferenceKeys);
            }
            else
            {
                if (space == null)
                {
                    keyValuePairs = ManagePreferences.getPreferences(null, user.ID, preferenceKeys);
                }
                else
                {
                    keyValuePairs = ManagePreferences.getPreferences(null, user.ID, space.ID, preferenceKeys);
                }
            }

            // Translate to a list of UserPreferences
            List<UserPreference> userPreferences = new List<UserPreference>();
            foreach (KeyValuePair<string, string> keyValuePair in keyValuePairs)
            {
                UserPreference userPreference = new UserPreference();
                userPreference.Key = keyValuePair.Key;
                userPreference.Value = keyValuePair.Value;
                userPreferences.Add(userPreference);
            }

            return userPreferences;
        }
    }
}
